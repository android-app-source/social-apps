.class public LX/2F4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile s:LX/2F4;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:Landroid/content/pm/PackageManager;

.field private final d:Landroid/app/ActivityManager;

.field public final e:Landroid/telephony/TelephonyManager;

.field public final f:Landroid/net/wifi/WifiManager;

.field private final g:LX/0TL;

.field private final h:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/analytics/reporters/periodic/DeviceInfoPeriodicReporterAdditionalInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/2F7;

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/lang/String;

.field private final l:LX/0e8;

.field private final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Lcom/facebook/compactdisk/DiskSizeCalculator;

.field public final p:Landroid/content/pm/ApplicationInfo;

.field private final q:LX/294;

.field private r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/pm/PackageManager;Landroid/app/ActivityManager;Landroid/telephony/TelephonyManager;Landroid/net/wifi/WifiManager;LX/0TL;LX/0Or;Ljava/util/Set;LX/2F7;Ljava/lang/String;LX/0e8;LX/0Or;LX/0Or;Lcom/facebook/compactdisk/DiskSizeCalculator;Landroid/content/pm/ApplicationInfo;LX/294;)V
    .locals 2
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/analytics/config/IsAppStoreDetectionEnabled;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/common/android/PackageName;
        .end annotation
    .end param
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/compactdiskmodule/IsDiskSizeCalculationEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Landroid/content/pm/PackageManager;",
            "Landroid/app/ActivityManager;",
            "Landroid/telephony/TelephonyManager;",
            "Landroid/net/wifi/WifiManager;",
            "LX/0TL;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/analytics/reporters/periodic/DeviceInfoPeriodicReporterAdditionalInfo;",
            ">;",
            "LX/2F7;",
            "Ljava/lang/String;",
            "LX/0e8;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/compactdisk/DiskSizeCalculator;",
            "Landroid/content/pm/ApplicationInfo;",
            "LX/294;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 386245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386246
    iput-object p1, p0, LX/2F4;->a:Landroid/content/Context;

    .line 386247
    iput-object p2, p0, LX/2F4;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 386248
    iput-object p3, p0, LX/2F4;->c:Landroid/content/pm/PackageManager;

    .line 386249
    iput-object p4, p0, LX/2F4;->d:Landroid/app/ActivityManager;

    .line 386250
    iput-object p5, p0, LX/2F4;->e:Landroid/telephony/TelephonyManager;

    .line 386251
    iput-object p6, p0, LX/2F4;->f:Landroid/net/wifi/WifiManager;

    .line 386252
    iput-object p7, p0, LX/2F4;->g:LX/0TL;

    .line 386253
    iput-object p8, p0, LX/2F4;->j:LX/0Or;

    .line 386254
    invoke-static {p9}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v1

    iput-object v1, p0, LX/2F4;->h:Ljava/lang/Iterable;

    .line 386255
    iput-object p10, p0, LX/2F4;->i:LX/2F7;

    .line 386256
    iput-object p11, p0, LX/2F4;->k:Ljava/lang/String;

    .line 386257
    iput-object p12, p0, LX/2F4;->l:LX/0e8;

    .line 386258
    iput-object p13, p0, LX/2F4;->m:LX/0Or;

    .line 386259
    move-object/from16 v0, p14

    iput-object v0, p0, LX/2F4;->n:LX/0Or;

    .line 386260
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2F4;->o:Lcom/facebook/compactdisk/DiskSizeCalculator;

    .line 386261
    move-object/from16 v0, p16

    iput-object v0, p0, LX/2F4;->p:Landroid/content/pm/ApplicationInfo;

    .line 386262
    move-object/from16 v0, p17

    iput-object v0, p0, LX/2F4;->q:LX/294;

    .line 386263
    return-void
.end method

.method public static a(LX/2Fr;)LX/0m9;
    .locals 3

    .prologue
    .line 386264
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 386265
    const-string v1, "package_name"

    iget-object v2, p0, LX/2Fr;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 386266
    const-string v1, "version"

    iget v2, p0, LX/2Fr;->b:I

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 386267
    const-string v1, "installation_status"

    iget-object v2, p0, LX/2Fr;->c:LX/2Fs;

    invoke-virtual {v2}, LX/2Fs;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 386268
    return-object v0
.end method

.method public static a(LX/0QB;)LX/2F4;
    .locals 3

    .prologue
    .line 386269
    sget-object v0, LX/2F4;->s:LX/2F4;

    if-nez v0, :cond_1

    .line 386270
    const-class v1, LX/2F4;

    monitor-enter v1

    .line 386271
    :try_start_0
    sget-object v0, LX/2F4;->s:LX/2F4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 386272
    if-eqz v2, :cond_0

    .line 386273
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2F4;->b(LX/0QB;)LX/2F4;

    move-result-object v0

    sput-object v0, LX/2F4;->s:LX/2F4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 386274
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 386275
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 386276
    :cond_1
    sget-object v0, LX/2F4;->s:LX/2F4;

    return-object v0

    .line 386277
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 386278
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2F4;Ljava/util/List;)LX/2Fr;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/2Fr;"
        }
    .end annotation

    .prologue
    .line 386279
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 386280
    :try_start_0
    iget-object v2, p0, LX/2F4;->c:Landroid/content/pm/PackageManager;

    const/16 v3, 0x40

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 386281
    :try_start_1
    iget-object v3, p0, LX/2F4;->c:Landroid/content/pm/PackageManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    .line 386282
    iget-boolean v1, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v1, :cond_0

    .line 386283
    new-instance v1, LX/2Fr;

    sget-object v3, LX/2Fs;->SERVICE_DISABLED:LX/2Fs;

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-direct {v1, v0, v3, v2}, LX/2Fr;-><init>(Ljava/lang/String;LX/2Fs;I)V

    move-object v0, v1

    .line 386284
    :goto_1
    return-object v0

    .line 386285
    :cond_0
    new-instance v1, LX/2Fr;

    sget-object v3, LX/2Fs;->SERVICE_ENABLED:LX/2Fs;

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-direct {v1, v0, v3, v2}, LX/2Fr;-><init>(Ljava/lang/String;LX/2Fs;I)V

    move-object v0, v1

    goto :goto_1

    .line 386286
    :cond_1
    new-instance v0, LX/2Fr;

    const-string v1, ""

    sget-object v2, LX/2Fs;->SERVICE_MISSING:LX/2Fs;

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, LX/2Fr;-><init>(Ljava/lang/String;LX/2Fs;I)V

    goto :goto_1

    .line 386287
    :catch_0
    goto :goto_0

    .line 386288
    :catch_1
    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/2F4;
    .locals 19

    .prologue
    .line 386289
    new-instance v1, LX/2F4;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageManager;

    invoke-static/range {p0 .. p0}, LX/0VU;->a(LX/0QB;)Landroid/app/ActivityManager;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager;

    invoke-static/range {p0 .. p0}, LX/0e7;->a(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v6

    check-cast v6, Landroid/telephony/TelephonyManager;

    invoke-static/range {p0 .. p0}, LX/0kt;->a(LX/0QB;)Landroid/net/wifi/WifiManager;

    move-result-object v7

    check-cast v7, Landroid/net/wifi/WifiManager;

    invoke-static/range {p0 .. p0}, LX/0TK;->a(LX/0QB;)LX/0TL;

    move-result-object v8

    check-cast v8, LX/0TL;

    const/16 v9, 0x143d

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/2F6;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/2F7;->a(LX/0QB;)LX/2F7;

    move-result-object v11

    check-cast v11, LX/2F7;

    invoke-static/range {p0 .. p0}, LX/0dF;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-static/range {p0 .. p0}, LX/0e8;->a(LX/0QB;)LX/0e8;

    move-result-object v13

    check-cast v13, LX/0e8;

    const/16 v14, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    const/16 v15, 0x307

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/2F8;->a(LX/0QB;)Lcom/facebook/compactdisk/DiskSizeCalculator;

    move-result-object v16

    check-cast v16, Lcom/facebook/compactdisk/DiskSizeCalculator;

    invoke-static/range {p0 .. p0}, LX/0s7;->a(LX/0QB;)Landroid/content/pm/ApplicationInfo;

    move-result-object v17

    check-cast v17, Landroid/content/pm/ApplicationInfo;

    invoke-static/range {p0 .. p0}, LX/294;->a(LX/0QB;)LX/294;

    move-result-object v18

    check-cast v18, LX/294;

    invoke-direct/range {v1 .. v18}, LX/2F4;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/pm/PackageManager;Landroid/app/ActivityManager;Landroid/telephony/TelephonyManager;Landroid/net/wifi/WifiManager;LX/0TL;LX/0Or;Ljava/util/Set;LX/2F7;Ljava/lang/String;LX/0e8;LX/0Or;LX/0Or;Lcom/facebook/compactdisk/DiskSizeCalculator;Landroid/content/pm/ApplicationInfo;LX/294;)V

    .line 386290
    return-object v1
.end method

.method private b(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 386291
    new-array v0, v2, [Ljava/lang/CharSequence;

    aput-object p3, v0, v3

    invoke-static {v0}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 386292
    iget-object v0, p0, LX/2F4;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    invoke-static {p3}, LX/0uQ;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 386293
    :cond_0
    iget-object v0, p0, LX/2F4;->q:LX/294;

    const-string v1, "device_info"

    invoke-virtual {v0, p1, p2, p3, v1}, LX/294;->a(JLjava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 386294
    const-string v0, "supported_fb4a_locales"

    const-string v1, ","

    iget-object v5, p0, LX/2F4;->l:LX/0e8;

    invoke-virtual {v5}, LX/0e8;->c()LX/0Py;

    move-result-object v5

    invoke-virtual {v5}, LX/0Py;->toArray()[Ljava/lang/Object;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386295
    iget-object v0, p0, LX/2F4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    .line 386296
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 386297
    iget-object v0, p0, LX/2F4;->a:Landroid/content/Context;

    const-string v6, "window"

    invoke-virtual {v0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 386298
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 386299
    move-object v6, v1

    .line 386300
    iget-object v0, p0, LX/2F4;->h:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2FG;

    .line 386301
    const/4 p3, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 386302
    :try_start_0
    new-instance v10, LX/0m9;

    sget-object v9, LX/0mC;->a:LX/0mC;

    invoke-direct {v10, v9}, LX/0m9;-><init>(LX/0mC;)V

    .line 386303
    iget-object v9, v0, LX/2FG;->d:Landroid/content/ContentResolver;

    const-string p1, "font_scale"

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-static {v9, p1, p2}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v9

    .line 386304
    const-string p1, "font_scale"

    invoke-virtual {v10, p1, v9}, LX/0m9;->a(Ljava/lang/String;F)LX/0m9;

    .line 386305
    iget-object v9, v0, LX/2FG;->e:Landroid/content/res/Configuration;

    iget v9, v9, Landroid/content/res/Configuration;->keyboard:I

    .line 386306
    iget-object p1, v0, LX/2FG;->e:Landroid/content/res/Configuration;

    iget p1, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    .line 386307
    if-ne p1, v7, :cond_2

    if-eq v9, p3, :cond_1

    const/4 p1, 0x3

    if-ne v9, p1, :cond_2

    .line 386308
    :cond_1
    if-ne v9, p3, :cond_16

    const-string v9, "qwerty"

    .line 386309
    :goto_1
    const-string p1, "hardware_keyboard"

    invoke-virtual {v10, p1, v9}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 386310
    :cond_2
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p1, 0x15

    if-lt v9, p1, :cond_3

    .line 386311
    iget-object v9, v0, LX/2FG;->d:Landroid/content/ContentResolver;

    const-string p1, "accessibility_display_inversion_enabled"

    const/4 p2, 0x0

    invoke-static {v9, p1, p2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v9

    .line 386312
    const-string p1, "display_inversion"

    if-eqz v9, :cond_17

    :goto_2
    invoke-virtual {v10, p1, v7}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 386313
    :cond_3
    iget-object v7, v0, LX/2FG;->b:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v7}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 386314
    iget-object v7, v0, LX/2FG;->b:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v7}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 386315
    const-string v7, "accessibility_enabled"

    const/4 v8, 0x1

    invoke-virtual {v10, v7, v8}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 386316
    const/4 v9, 0x0

    .line 386317
    const-string v7, "touch_exploration_enabled"

    iget-object v8, v0, LX/2FG;->b:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v8}, LX/1d8;->b(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v8

    invoke-virtual {v10, v7, v8}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 386318
    iget-object v7, v0, LX/2FG;->b:Landroid/view/accessibility/AccessibilityManager;

    const/4 v8, -0x1

    .line 386319
    sget-object p1, LX/1d8;->a:LX/1dB;

    invoke-interface {p1, v7, v8}, LX/1dB;->a(Landroid/view/accessibility/AccessibilityManager;I)Ljava/util/List;

    move-result-object p1

    move-object v7, p1

    .line 386320
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move v8, v9

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/accessibilityservice/AccessibilityServiceInfo;

    .line 386321
    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "enabled_service_"

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v7}, LX/2FG;->a(Landroid/accessibilityservice/AccessibilityServiceInfo;)LX/0m9;

    move-result-object v7

    invoke-virtual {v10, p2, v7}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 386322
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    .line 386323
    goto :goto_3

    .line 386324
    :cond_4
    iget-object v7, v0, LX/2FG;->b:Landroid/view/accessibility/AccessibilityManager;

    .line 386325
    sget-object v8, LX/1d8;->a:LX/1dB;

    invoke-interface {v8, v7}, LX/1dB;->a(Landroid/view/accessibility/AccessibilityManager;)Ljava/util/List;

    move-result-object v8

    move-object v7, v8

    .line 386326
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/accessibilityservice/AccessibilityServiceInfo;

    .line 386327
    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "installed_service_"

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v7}, LX/2FG;->a(Landroid/accessibilityservice/AccessibilityServiceInfo;)LX/0m9;

    move-result-object v7

    invoke-virtual {v10, p1, v7}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 386328
    add-int/lit8 v9, v9, 0x1

    .line 386329
    goto :goto_4

    .line 386330
    :cond_5
    const-string v7, "accessibility"

    invoke-virtual {v4, v7, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 386331
    :goto_5
    goto/16 :goto_0

    .line 386332
    :cond_6
    const-string v0, "cpu_abi"

    sget-object v1, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386333
    const-string v0, "cpu_abi2"

    sget-object v1, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386334
    const-string v0, "unreliable_core_count"

    iget-object v1, p0, LX/2F4;->g:LX/0TL;

    invoke-virtual {v1}, LX/0TL;->c()I

    move-result v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386335
    const-string v0, "reliable_core_count"

    iget-object v1, p0, LX/2F4;->g:LX/0TL;

    invoke-virtual {v1}, LX/0TL;->b()I

    move-result v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386336
    const-string v0, "cpu_max_freq"

    iget-object v1, p0, LX/2F4;->g:LX/0TL;

    .line 386337
    iget v7, v1, LX/0TL;->d:I

    if-nez v7, :cond_7

    .line 386338
    invoke-static {v1}, LX/0TL;->g(LX/0TL;)V

    .line 386339
    :cond_7
    iget v7, v1, LX/0TL;->d:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v1, v7

    .line 386340
    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386341
    iget-object v0, p0, LX/2F4;->g:LX/0TL;

    invoke-virtual {v0}, LX/0TL;->f()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_8

    .line 386342
    const-string v0, "low_power_cpu_max_freq"

    iget-object v1, p0, LX/2F4;->g:LX/0TL;

    invoke-virtual {v1}, LX/0TL;->f()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386343
    :cond_8
    const-string v0, "cgroups_supported"

    .line 386344
    :try_start_1
    new-instance v1, Ljava/io/File;

    const-string v7, "/dev/cpuctl/tasks"

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result v1

    .line 386345
    :goto_6
    move v1, v1

    .line 386346
    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386347
    iget-object v0, p0, LX/2F4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_15

    move v0, v2

    .line 386348
    :goto_7
    if-eqz v0, :cond_9

    .line 386349
    const-string v0, "is_debuggable"

    invoke-virtual {v4, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386350
    :cond_9
    :try_start_2
    iget-object v0, p0, LX/2F4;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, LX/2F4;->k:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v0, v1, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 386351
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 386352
    iget-wide v8, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    invoke-virtual {v1, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 386353
    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v1

    .line 386354
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    .line 386355
    iget-wide v8, v0, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-virtual {v7, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 386356
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v0

    .line 386357
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x15

    if-lt v7, v8, :cond_a

    .line 386358
    iget-object v7, p0, LX/2F4;->c:Landroid/content/pm/PackageManager;

    iget-object v8, p0, LX/2F4;->k:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    .line 386359
    new-instance v8, LX/0s6;

    iget-object v9, p0, LX/2F4;->c:Landroid/content/pm/PackageManager;

    invoke-direct {v8, v9, v7}, LX/0s6;-><init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)V

    const-string v7, "com.google.android.webview"

    .line 386360
    const/4 v9, 0x0

    invoke-virtual {v8, v7, v9}, LX/01H;->e(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    .line 386361
    if-eqz v9, :cond_18

    .line 386362
    iget-object v9, v9, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 386363
    :goto_8
    move-object v7, v9

    .line 386364
    if-eqz v7, :cond_a

    .line 386365
    const-string v8, "webview_version"

    invoke-virtual {v4, v8, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    .line 386366
    :cond_a
    :goto_9
    const-string v7, "first_install_time"

    invoke-virtual {v4, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386367
    const-string v1, "last_upgrade_time"

    invoke-virtual {v4, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386368
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_b

    .line 386369
    iget-object v0, p0, LX/2F4;->c:Landroid/content/pm/PackageManager;

    const-string v1, "android.permission.READ_PHONE_STATE"

    iget-object v7, p0, LX/2F4;->a:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v1, v7}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_b

    .line 386370
    const-string v0, "GID1"

    iget-object v1, p0, LX/2F4;->e:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getGroupIdLevel1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386371
    :cond_b
    const-string v0, "density"

    iget v1, v5, Landroid/util/DisplayMetrics;->density:F

    float-to-double v8, v1

    invoke-virtual {v4, v0, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386372
    const-string v0, "screen_width"

    iget v1, v6, Landroid/graphics/Point;->x:I

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386373
    const-string v0, "screen_height"

    iget v1, v6, Landroid/graphics/Point;->y:I

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386374
    const-string v0, "front_camera"

    iget-object v1, p0, LX/2F4;->c:Landroid/content/pm/PackageManager;

    const-string v5, "android.hardware.camera.front"

    invoke-virtual {v1, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386375
    const-string v0, "rear_camera"

    iget-object v1, p0, LX/2F4;->c:Landroid/content/pm/PackageManager;

    const-string v5, "android.hardware.camera"

    invoke-virtual {v1, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386376
    const-string v0, "allows_non_market_installs"

    iget-object v1, p0, LX/2F4;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v5, "install_non_market_apps"

    invoke-static {v1, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386377
    const-string v0, "android_id"

    iget-object v1, p0, LX/2F4;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v5, "android_id"

    invoke-static {v1, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386378
    iget-object v0, p0, LX/2F4;->d:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    move-result-object v0

    .line 386379
    if-eqz v0, :cond_c

    .line 386380
    const-string v1, "opengl_version"

    iget v0, v0, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    invoke-virtual {v4, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386381
    :cond_c
    const/4 v6, 0x0

    .line 386382
    :try_start_3
    iget-object v0, p0, LX/2F4;->f:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "isWifiApEnabled"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 386383
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 386384
    iget-object v1, p0, LX/2F4;->f:Landroid/net/wifi/WifiManager;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 386385
    const-string v0, "wifi_hotspot_supported"

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_4

    .line 386386
    :goto_a
    :try_start_4
    iget-object v0, p0, LX/2F4;->c:Landroid/content/pm/PackageManager;

    const-string v1, "com.android.vending"

    const/16 v5, 0x40

    invoke-virtual {v0, v1, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_5

    .line 386387
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v5, "com.google.android.gms"

    aput-object v5, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p0, v0}, LX/2F4;->a(LX/2F4;Ljava/util/List;)LX/2Fr;

    move-result-object v0

    :goto_b
    move-object v0, v0

    .line 386388
    const-string v1, "google_play_services_installation"

    iget-object v5, v0, LX/2Fr;->c:LX/2Fs;

    invoke-virtual {v5}, LX/2Fs;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386389
    const-string v1, "google_play_services_version"

    iget v0, v0, LX/2Fr;->b:I

    invoke-virtual {v4, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386390
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 386391
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 386392
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v5, "com.android.vending"

    aput-object v5, v0, v8

    const-string v5, "com.google.market"

    aput-object v5, v0, v9

    const/4 v5, 0x2

    const-string v6, "com.google.android.finsky"

    aput-object v6, v0, v5

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 386393
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_d
    :goto_c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 386394
    new-array v6, v9, [Ljava/lang/String;

    aput-object v0, v6, v8

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, LX/2F4;->a(LX/2F4;Ljava/util/List;)LX/2Fr;

    move-result-object v0

    .line 386395
    iget-object v6, v0, LX/2Fr;->c:LX/2Fs;

    sget-object v7, LX/2Fs;->SERVICE_MISSING:LX/2Fs;

    if-eq v6, v7, :cond_d

    .line 386396
    invoke-static {v0}, LX/2F4;->a(LX/2Fr;)LX/0m9;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_c

    .line 386397
    :cond_e
    const-string v0, "google_play_store"

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386398
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v5, "com.google.android.gsf"

    aput-object v5, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, LX/2F4;->a(LX/2F4;Ljava/util/List;)LX/2Fr;

    move-result-object v0

    .line 386399
    const-string v1, "gsf_installation_status"

    invoke-static {v0}, LX/2F4;->a(LX/2Fr;)LX/0m9;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386400
    iget-object v0, p0, LX/2F4;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 386401
    iget-object v1, p0, LX/2F4;->a:Landroid/content/Context;

    const-string v5, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v1, v5}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    .line 386402
    if-nez v1, :cond_10

    .line 386403
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 386404
    const-string v0, "google_accounts"

    array-length v5, v1

    invoke-virtual {v4, v0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386405
    new-instance v5, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v0}, LX/162;-><init>(LX/0mC;)V

    .line 386406
    array-length v6, v1

    const/4 v0, 0x0

    :goto_d
    if-ge v0, v6, :cond_f

    aget-object v7, v1, v0

    .line 386407
    new-instance v8, LX/0m9;

    sget-object v9, LX/0mC;->a:LX/0mC;

    invoke-direct {v8, v9}, LX/0m9;-><init>(LX/0mC;)V

    .line 386408
    const-string v9, "google_account_email"

    iget-object v7, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v8, v9, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 386409
    invoke-virtual {v5, v8}, LX/162;->a(LX/0lF;)LX/162;

    .line 386410
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 386411
    :cond_f
    const-string v0, "google_account_emails"

    invoke-virtual {v4, v0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386412
    :cond_10
    :try_start_5
    iget-object v0, p0, LX/2F4;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, LX/2F4;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 386413
    if-nez v0, :cond_11

    .line 386414
    const-string v0, ""

    .line 386415
    :cond_11
    iget-object v1, p0, LX/2F4;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0uQ;->k:LX/0Tn;

    invoke-interface {v1, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 386416
    iget-object v1, p0, LX/2F4;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v5, LX/0uQ;->k:LX/0Tn;

    invoke-interface {v1, v5, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_1

    .line 386417
    :cond_12
    :goto_e
    const-string v1, "installer"

    invoke-virtual {v4, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386418
    const-string v0, "oldest_known_installer"

    iget-object v1, p0, LX/2F4;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0uQ;->k:LX/0Tn;

    const-string v6, "unknown"

    invoke-interface {v1, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386419
    iget-object v0, p0, LX/2F4;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 386420
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.amazon.venezia"

    aput-object v1, v0, v3

    const-string v1, "com.amazon.mShop.android"

    aput-object v1, v0, v2

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p0, v0}, LX/2F4;->a(LX/2F4;Ljava/util/List;)LX/2Fr;

    move-result-object v0

    .line 386421
    iget-object v1, v0, LX/2Fr;->c:LX/2Fs;

    sget-object v2, LX/2Fs;->SERVICE_MISSING:LX/2Fs;

    if-eq v1, v2, :cond_13

    .line 386422
    const-string v1, "amazon_app_store_installation_status"

    invoke-static {v0}, LX/2F4;->a(LX/2Fr;)LX/0m9;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386423
    :cond_13
    iget-object v0, p0, LX/2F4;->i:LX/2F7;

    invoke-virtual {v0}, LX/2F7;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386424
    const-string v0, "notifications_system_setting"

    iget-object v1, p0, LX/2F4;->a:Landroid/content/Context;

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 386425
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x12

    if-lt v2, v6, :cond_14

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x17

    if-le v2, v6, :cond_19

    .line 386426
    :cond_14
    sget-object v2, LX/03R;->UNSET:LX/03R;

    .line 386427
    :goto_f
    move-object v1, v2

    .line 386428
    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386429
    const-string v0, "channel_id"

    invoke-static {p0}, LX/2F4;->c(LX/2F4;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386430
    return-object v4

    :cond_15
    move v0, v3

    .line 386431
    goto/16 :goto_7

    .line 386432
    :catch_0
    const-string v1, "unknown"

    .line 386433
    const-string v0, "unknown"

    goto/16 :goto_9

    .line 386434
    :catch_1
    const-string v0, "unknown"

    goto :goto_e

    .line 386435
    :cond_16
    :try_start_6
    const-string v9, "12key"
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_1

    :cond_17
    move v7, v8

    .line 386436
    goto/16 :goto_2

    .line 386437
    :catch_2
    move-exception v7

    .line 386438
    iget-object v8, v0, LX/2FG;->c:LX/03V;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, LX/2FG;->a:Ljava/lang/Class;

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "_putAdditionalInfo_exception"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5

    :catch_3
    const/4 v1, 0x0

    goto/16 :goto_6

    :cond_18
    :try_start_7
    const/4 v9, 0x0

    goto/16 :goto_8
    :try_end_7
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7 .. :try_end_7} :catch_0

    .line 386439
    :catch_4
    const-string v0, "wifi_hotspot_supported"

    invoke-virtual {v4, v0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_a

    .line 386440
    :catch_5
    new-instance v0, LX/2Fr;

    const-string v1, "com.android.vending"

    sget-object v5, LX/2Fs;->SERVICE_MISSING:LX/2Fs;

    const/4 v6, -0x1

    invoke-direct {v0, v1, v5, v6}, LX/2Fr;-><init>(Ljava/lang/String;LX/2Fs;I)V

    goto/16 :goto_b

    .line 386441
    :cond_19
    :try_start_8
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 386442
    sget-object v2, LX/2Fz;->a:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_1c

    move v2, v6

    .line 386443
    :goto_10
    move v2, v2

    .line 386444
    if-nez v2, :cond_1a

    .line 386445
    sget-object v2, LX/03R;->UNSET:LX/03R;

    goto :goto_f

    .line 386446
    :cond_1a
    const-string v2, "appops"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AppOpsManager;

    .line 386447
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    .line 386448
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    .line 386449
    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 386450
    sget-object v8, LX/2Fz;->a:Ljava/lang/reflect/Method;

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    sget p1, LX/2Fz;->b:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v9, v10

    const/4 v10, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v10

    const/4 v6, 0x2

    aput-object v7, v9, v6

    invoke-virtual {v8, v2, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_1b

    move v2, v3

    .line 386451
    :goto_11
    invoke-static {v2}, LX/03R;->valueOf(Z)LX/03R;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_8 .. :try_end_8} :catch_6

    move-result-object v2

    goto/16 :goto_f

    :cond_1b
    move v2, v5

    .line 386452
    goto :goto_11

    .line 386453
    :catch_6
    :goto_12
    sget-object v2, LX/03R;->UNSET:LX/03R;

    goto/16 :goto_f

    .line 386454
    :catch_7
    goto :goto_12

    .line 386455
    :cond_1c
    :try_start_9
    const-class v2, Landroid/app/AppOpsManager;

    const-string v8, "checkOpNoThrow"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Class;

    const/4 v10, 0x0

    sget-object p1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object p1, v9, v10

    const/4 v10, 0x1

    sget-object p1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object p1, v9, v10

    const/4 v10, 0x2

    const-class p1, Ljava/lang/String;

    aput-object p1, v9, v10

    invoke-virtual {v2, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    .line 386456
    const-class v2, Landroid/app/AppOpsManager;

    const-string v9, "OP_POST_NOTIFICATION"

    invoke-virtual {v2, v9}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 386457
    const-class v9, Ljava/lang/Integer;

    invoke-virtual {v2, v9}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 386458
    sput-object v8, LX/2Fz;->a:Ljava/lang/reflect/Method;

    .line 386459
    sput v2, LX/2Fz;->b:I
    :try_end_9
    .catch Ljava/lang/NoSuchFieldException; {:try_start_9 .. :try_end_9} :catch_a
    .catch Ljava/lang/NoSuchMethodException; {:try_start_9 .. :try_end_9} :catch_9
    .catch Ljava/lang/IllegalAccessException; {:try_start_9 .. :try_end_9} :catch_8
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_9 .. :try_end_9} :catch_6

    move v2, v6

    .line 386460
    goto :goto_10

    :catch_8
    :goto_13
    move v2, v7

    .line 386461
    goto :goto_10

    .line 386462
    :catch_9
    goto :goto_13

    :catch_a
    goto :goto_13
.end method

.method private static c(LX/2F4;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 386463
    iget-object v0, p0, LX/2F4;->r:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 386464
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "/META-INF/cid"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 386465
    if-eqz v0, :cond_0

    .line 386466
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 386467
    :try_start_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2F4;->r:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 386468
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 386469
    :cond_0
    :goto_0
    iget-object v0, p0, LX/2F4;->r:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 386470
    const-string v0, ""

    iput-object v0, p0, LX/2F4;->r:Ljava/lang/String;

    .line 386471
    :cond_1
    iget-object v0, p0, LX/2F4;->r:Ljava/lang/String;

    return-object v0

    .line 386472
    :catch_0
    move-exception v0

    .line 386473
    :try_start_2
    const-string v2, "DeviceInfoPeriodicReporter"

    const-string v3, "Failed to read app distribution channel id"

    invoke-static {v2, v3, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 386474
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 386475
    :catch_1
    goto :goto_0

    .line 386476
    :catchall_0
    move-exception v0

    .line 386477
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 386478
    :goto_1
    throw v0

    .line 386479
    :catch_2
    goto :goto_0

    :catch_3
    goto :goto_1
.end method


# virtual methods
.method public final a()J
    .locals 5

    .prologue
    const-wide/32 v2, 0x5265c00

    const/4 v4, 0x0

    .line 386480
    iget-object v0, p0, LX/2F4;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move-wide v0, v2

    .line 386481
    :goto_0
    return-wide v0

    .line 386482
    :cond_0
    iget-object v0, p0, LX/2F4;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0uQ;->e:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/CharSequence;

    iget-object v0, p0, LX/2F4;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    aput-object v0, v1, v4

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 386483
    iget-object v1, p0, LX/2F4;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, LX/2F4;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/0uQ;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-interface {v1, v0, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 386484
    if-eqz v0, :cond_1

    .line 386485
    const-wide/16 v0, 0x1388

    goto :goto_0

    .line 386486
    :cond_1
    iget-object v0, p0, LX/2F4;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0uQ;->e:LX/0Tn;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 4

    .prologue
    .line 386487
    iget-object v0, p0, LX/2F4;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 386488
    :try_start_0
    iget-object v0, p0, LX/2F4;->o:Lcom/facebook/compactdisk/DiskSizeCalculator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskSizeCalculator;->setAnalytics(Z)V

    .line 386489
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/2F4;->p:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    .line 386490
    iget-object v1, p0, LX/2F4;->o:Lcom/facebook/compactdisk/DiskSizeCalculator;

    invoke-virtual {v1, v0}, Lcom/facebook/compactdisk/DiskSizeCalculator;->a(Ljava/lang/String;)V

    .line 386491
    iget-object v0, p0, LX/2F4;->o:Lcom/facebook/compactdisk/DiskSizeCalculator;

    iget-object v1, p0, LX/2F4;->p:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskSizeCalculator;->a(Ljava/lang/String;)V

    .line 386492
    iget-object v0, p0, LX/2F4;->o:Lcom/facebook/compactdisk/DiskSizeCalculator;

    invoke-virtual {v0}, Lcom/facebook/compactdisk/DiskSizeCalculator;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 386493
    :cond_0
    :goto_0
    invoke-direct {p0, p1, p2, p3}, LX/2F4;->b(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    move-result-object v0

    return-object v0

    .line 386494
    :catch_0
    move-exception v0

    .line 386495
    const-string v1, "DeviceInfoPeriodicReporter"

    const-string v2, "Error starting disk size calculation"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
