.class public LX/2RC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/2RC;


# instance fields
.field private final b:Landroid/content/ContentResolver;

.field private final c:LX/2RE;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 409553
    const-class v0, LX/2RC;

    sput-object v0, LX/2RC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/2RE;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "LX/2RE;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409555
    iput-object p1, p0, LX/2RC;->b:Landroid/content/ContentResolver;

    .line 409556
    iput-object p2, p0, LX/2RC;->c:LX/2RE;

    .line 409557
    iput-object p3, p0, LX/2RC;->d:LX/0Or;

    .line 409558
    return-void
.end method

.method public static a(LX/0QB;)LX/2RC;
    .locals 6

    .prologue
    .line 409559
    sget-object v0, LX/2RC;->e:LX/2RC;

    if-nez v0, :cond_1

    .line 409560
    const-class v1, LX/2RC;

    monitor-enter v1

    .line 409561
    :try_start_0
    sget-object v0, LX/2RC;->e:LX/2RC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 409562
    if-eqz v2, :cond_0

    .line 409563
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 409564
    new-instance v5, LX/2RC;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/2RE;->a(LX/0QB;)LX/2RE;

    move-result-object v4

    check-cast v4, LX/2RE;

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/2RC;-><init>(Landroid/content/ContentResolver;LX/2RE;LX/0Or;)V

    .line 409565
    move-object v0, v5

    .line 409566
    sput-object v0, LX/2RC;->e:LX/2RC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 409567
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 409568
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 409569
    :cond_1
    sget-object v0, LX/2RC;->e:LX/2RC;

    return-object v0

    .line 409570
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 409571
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/2RR;LX/2RV;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 409572
    iget-object v0, p0, LX/2RC;->c:LX/2RE;

    invoke-virtual {v0}, LX/2RE;->a()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, LX/2RC;->a(LX/2RR;LX/2RV;Ljava/util/Set;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2RR;LX/2RV;Ljava/util/Set;)Landroid/database/Cursor;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2RR;",
            "LX/2RV;",
            "Ljava/util/Set",
            "<",
            "LX/0PH;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 409573
    sget-object v0, LX/2RV;->CONTACT:LX/2RV;

    if-ne p2, v0, :cond_e

    .line 409574
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "data"

    aput-object v1, v0, v5

    const-string v1, "phonebook_section_key"

    aput-object v1, v0, v6

    const-string v1, "_id"

    aput-object v1, v0, v7

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 409575
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v2, v1, [Ljava/lang/String;

    .line 409576
    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 409577
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 409578
    iget-object v0, p1, LX/2RR;->b:Ljava/util/Collection;

    move-object v0, v0

    .line 409579
    if-eqz v0, :cond_0

    .line 409580
    const-string v0, "type"

    .line 409581
    iget-object v1, p1, LX/2RR;->b:Ljava/util/Collection;

    move-object v1, v1

    .line 409582
    new-instance v3, LX/2RW;

    invoke-direct {v3, p0}, LX/2RW;-><init>(LX/2RC;)V

    invoke-static {v1, v3}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 409583
    :cond_0
    iget-object v0, p1, LX/2RR;->c:Ljava/util/Collection;

    move-object v0, v0

    .line 409584
    if-eqz v0, :cond_1

    .line 409585
    const-string v0, "link_type"

    .line 409586
    iget-object v1, p1, LX/2RR;->c:Ljava/util/Collection;

    move-object v1, v1

    .line 409587
    new-instance v3, LX/6N0;

    invoke-direct {v3, p0}, LX/6N0;-><init>(LX/2RC;)V

    invoke-static {v1, v3}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 409588
    :cond_1
    iget-boolean v0, p1, LX/2RR;->f:Z

    move v0, v0

    .line 409589
    if-eqz v0, :cond_2

    .line 409590
    const-string v1, "fbid"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v0, p0, LX/2RC;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 409591
    iget-object p2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p2

    .line 409592
    aput-object v0, v3, v5

    invoke-static {v1, v3}, LX/0uu;->b(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 409593
    :cond_2
    iget-boolean v0, p1, LX/2RR;->h:Z

    move v0, v0

    .line 409594
    if-eqz v0, :cond_3

    .line 409595
    const-string v0, "is_messenger_user"

    const-string v1, "false"

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 409596
    :cond_3
    iget-boolean v0, p1, LX/2RR;->g:Z

    move v0, v0

    .line 409597
    if-eqz v0, :cond_4

    .line 409598
    const-string v0, "is_mobile_pushable"

    const-string v1, "1"

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 409599
    :cond_4
    iget-boolean v0, p1, LX/2RR;->i:Z

    move v0, v0

    .line 409600
    if-eqz v0, :cond_5

    .line 409601
    const-string v0, "is_messenger_user"

    const-string v1, "true"

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 409602
    :cond_5
    iget-boolean v0, p1, LX/2RR;->j:Z

    move v0, v0

    .line 409603
    if-eqz v0, :cond_6

    .line 409604
    const-string v0, "is_on_viewer_contact_list"

    const-string v1, "true"

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 409605
    :cond_6
    iget-object v0, p1, LX/2RR;->m:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-object v0, v0

    .line 409606
    if-eqz v0, :cond_7

    .line 409607
    iget-object v0, p1, LX/2RR;->m:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-object v0, v0

    .line 409608
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CONNECTED:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    if-eq v0, v1, :cond_12

    .line 409609
    const-string v1, "viewer_connection_status"

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 409610
    :cond_7
    :goto_1
    iget-object v0, p1, LX/2RR;->d:Ljava/util/Collection;

    move-object v0, v0

    .line 409611
    if-eqz v0, :cond_8

    .line 409612
    iget-object v0, p1, LX/2RR;->d:Ljava/util/Collection;

    move-object v0, v0

    .line 409613
    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->c(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    .line 409614
    const-string v1, "fbid"

    invoke-static {v1, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 409615
    :cond_8
    iget-boolean v0, p1, LX/2RR;->k:Z

    move v0, v0

    .line 409616
    if-eqz v0, :cond_9

    .line 409617
    const-string v0, "communication_rank"

    const-string v1, "0"

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 409618
    :cond_9
    iget-boolean v0, p1, LX/2RR;->l:Z

    move v0, v0

    .line 409619
    if-nez v0, :cond_a

    .line 409620
    new-array v0, v7, [LX/0ux;

    const-string v1, "is_memorialized"

    invoke-static {v1}, LX/0uu;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v5

    const-string v1, "is_memorialized"

    const-string v3, "1"

    invoke-static {v1, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    invoke-static {v0}, LX/0uu;->a(LX/0ux;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 409621
    :cond_a
    const/4 v0, 0x0

    .line 409622
    iget-object v1, p1, LX/2RR;->n:LX/2RS;

    move-object v1, v1

    .line 409623
    sget-object v3, LX/2RS;->NO_SORT_ORDER:LX/2RS;

    if-eq v1, v3, :cond_c

    .line 409624
    sget-object v0, LX/2RS;->PHAT_RANK:LX/2RS;

    if-ne v1, v0, :cond_b

    .line 409625
    sget-object v0, LX/2RC;->a:Ljava/lang/Class;

    const-string v3, "Trying to use PHAT rank to sort a legacy contacts query. Falling back to communication rank"

    invoke-static {v0, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 409626
    :cond_b
    iget-object v1, v1, LX/2RS;->mLegacyIndexColumnName:Ljava/lang/String;

    .line 409627
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 409628
    iget-boolean v0, p1, LX/2RR;->o:Z

    move v0, v0

    .line 409629
    if-eqz v0, :cond_f

    const-string v0, " DESC"

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 409630
    invoke-static {v1}, LX/0uu;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 409631
    :cond_c
    iget v1, p1, LX/2RR;->p:I

    move v1, v1

    .line 409632
    if-ltz v1, :cond_11

    .line 409633
    if-nez v0, :cond_d

    .line 409634
    const-string v0, "_id"

    .line 409635
    :cond_d
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIMIT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 409636
    iget v1, p1, LX/2RR;->p:I

    move v1, v1

    .line 409637
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 409638
    :goto_3
    iget-object v0, p1, LX/2RR;->e:Ljava/lang/String;

    move-object v0, v0

    .line 409639
    if-eqz v0, :cond_10

    .line 409640
    iget-object v0, p0, LX/2RC;->c:LX/2RE;

    iget-object v0, v0, LX/2RE;->f:LX/2RO;

    .line 409641
    iget-object v1, p1, LX/2RR;->e:Ljava/lang/String;

    move-object v1, v1

    .line 409642
    const-string v3, ","

    invoke-static {v3}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v3

    invoke-virtual {v3, p3}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    .line 409643
    iget-object v6, v0, LX/2RO;->c:Landroid/net/Uri;

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object v1, v3

    .line 409644
    :goto_4
    iget-object v0, p0, LX/2RC;->b:Landroid/content/ContentResolver;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 409645
    :cond_e
    sget-object v0, LX/2RL;->a:LX/0Rf;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    goto/16 :goto_0

    .line 409646
    :cond_f
    const-string v0, ""

    goto :goto_2

    .line 409647
    :cond_10
    iget-object v0, p0, LX/2RC;->c:LX/2RE;

    iget-object v0, v0, LX/2RE;->c:LX/2RK;

    iget-object v1, v0, LX/2RK;->c:Landroid/net/Uri;

    goto :goto_4

    :cond_11
    move-object v5, v0

    goto :goto_3

    .line 409648
    :cond_12
    const/4 v1, 0x3

    new-array v1, v1, [LX/0ux;

    const/4 v3, 0x0

    const-string v8, "viewer_connection_status"

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CONNECTED:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->name()Ljava/lang/String;

    move-result-object p2

    invoke-static {v8, p2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v8

    aput-object v8, v1, v3

    const/4 v3, 0x1

    const-string v8, "viewer_connection_status"

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->name()Ljava/lang/String;

    move-result-object p2

    invoke-static {v8, p2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v8

    aput-object v8, v1, v3

    const/4 v3, 0x2

    const-string v8, "viewer_connection_status"

    invoke-static {v8}, LX/0uu;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v8

    invoke-static {v8}, LX/0uu;->a(LX/0ux;)LX/0ux;

    move-result-object v8

    aput-object v8, v1, v3

    invoke-static {v1}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v1

    .line 409649
    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    goto/16 :goto_1
.end method
