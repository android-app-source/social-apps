.class public interface abstract LX/2nf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# virtual methods
.method public abstract a()J
.end method

.method public abstract a(LX/2kb;)V
.end method

.method public abstract b()J
.end method

.method public abstract c()Lcom/facebook/flatbuffers/Flattenable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract close()V
.end method

.method public abstract d()I
.end method

.method public abstract getCount()I
.end method

.method public abstract getExtras()Landroid/os/Bundle;
.end method

.method public abstract getPosition()I
.end method

.method public abstract isClosed()Z
.end method

.method public abstract moveToFirst()Z
.end method

.method public abstract moveToLast()Z
.end method

.method public abstract moveToNext()Z
.end method

.method public abstract moveToPosition(I)Z
.end method

.method public abstract moveToPrevious()Z
.end method
