.class public LX/29w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/29w;


# instance fields
.field public a:LX/2A5;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Landroid/content/ContentResolver;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 376776
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/29w;
    .locals 9

    .prologue
    .line 376777
    sget-object v0, LX/29w;->c:LX/29w;

    if-nez v0, :cond_1

    .line 376778
    const-class v1, LX/29w;

    monitor-enter v1

    .line 376779
    :try_start_0
    sget-object v0, LX/29w;->c:LX/29w;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 376780
    if-eqz v2, :cond_0

    .line 376781
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 376782
    new-instance v5, LX/29w;

    invoke-direct {v5}, LX/29w;-><init>()V

    .line 376783
    new-instance v6, LX/2A5;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    .line 376784
    new-instance v8, LX/2A6;

    invoke-direct {v8}, LX/2A6;-><init>()V

    .line 376785
    invoke-static {v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v4

    check-cast v4, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    const/16 v7, 0xdf4

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    .line 376786
    iput-object v4, v8, LX/2A6;->b:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    iput-object p0, v8, LX/2A6;->c:LX/0Ot;

    iput-object v7, v8, LX/2A6;->a:Landroid/content/Context;

    .line 376787
    move-object v4, v8

    .line 376788
    check-cast v4, LX/2A6;

    invoke-direct {v6, v3, v4}, LX/2A5;-><init>(Landroid/os/Handler;LX/2A6;)V

    .line 376789
    move-object v3, v6

    .line 376790
    check-cast v3, LX/2A5;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v4

    check-cast v4, Landroid/content/ContentResolver;

    .line 376791
    iput-object v3, v5, LX/29w;->a:LX/2A5;

    iput-object v4, v5, LX/29w;->b:Landroid/content/ContentResolver;

    .line 376792
    move-object v0, v5

    .line 376793
    sput-object v0, LX/29w;->c:LX/29w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 376794
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 376795
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 376796
    :cond_1
    sget-object v0, LX/29w;->c:LX/29w;

    return-object v0

    .line 376797
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 376798
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 4

    .prologue
    .line 376799
    iget-object v0, p0, LX/29w;->b:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, LX/29w;->a:LX/2A5;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 376800
    return-void
.end method
