.class public LX/2Qc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;",
        "Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 408647
    const-class v0, LX/2Qc;

    sput-object v0, LX/2Qc;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 408645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 408646
    return-void
.end method

.method private a(LX/0lF;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 408648
    invoke-virtual {p1, p2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 408649
    iget-object v0, p0, LX/2Qc;->b:LX/03V;

    const-string v1, "GraphAuthProxyLoginPostMethod"

    const-string v2, "access token not found in response"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 408650
    const/4 v0, 0x0

    .line 408651
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 408610
    check-cast p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;

    .line 408611
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 408612
    iget-object v0, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 408613
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 408614
    const-string v1, "proxied_app_id"

    iget-object v2, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 408615
    iget-object v1, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->b:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 408616
    new-instance v1, Lorg/json/JSONArray;

    iget-object v2, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 408617
    const-string v2, "permissions"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 408618
    :cond_0
    iget-object v1, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 408619
    const-string v1, "write_privacy"

    iget-object v2, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 408620
    :cond_1
    const-string v1, "is_refresh_only"

    iget-boolean v2, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->d:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 408621
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 408622
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "type"

    const-string v3, "graphauthproxyloginpost"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 408623
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "proxied_app_id"

    iget-object v3, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 408624
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "ix_url"

    iget-object v3, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 408625
    iget-object v1, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->b:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 408626
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "permissions"

    const-string v3, ","

    iget-object v4, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->b:Ljava/util/List;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 408627
    :cond_2
    iget-object v1, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 408628
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "write_privacy"

    iget-object v3, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 408629
    :cond_3
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_refresh_only"

    iget-boolean v3, p1, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->d:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 408630
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "graphAuthProxyLoginPost"

    .line 408631
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 408632
    move-object v1, v1

    .line 408633
    const-string v2, "POST"

    .line 408634
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 408635
    move-object v1, v1

    .line 408636
    const-string v2, "/v2.6/auth/proxy_login"

    .line 408637
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 408638
    move-object v1, v1

    .line 408639
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 408640
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 408641
    move-object v1, v1

    .line 408642
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 408643
    move-object v0, v1

    .line 408644
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 408595
    const/4 v0, 0x0

    .line 408596
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    .line 408597
    if-nez v1, :cond_1

    .line 408598
    iget-object v1, p0, LX/2Qc;->b:LX/03V;

    const-string v2, "GraphAuthProxyLoginPostMethod"

    const-string v3, "null graph response"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 408599
    :cond_0
    :goto_0
    return-object v0

    .line 408600
    :cond_1
    const-string v2, "access_token"

    invoke-direct {p0, v1, v2}, LX/2Qc;->a(LX/0lF;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "expires"

    invoke-direct {p0, v1, v2}, LX/2Qc;->a(LX/0lF;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "permissions"

    invoke-direct {p0, v1, v2}, LX/2Qc;->a(LX/0lF;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 408601
    const-string v2, "access_token"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 408602
    const-string v3, "expires"

    invoke-virtual {v1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 408603
    const-string v4, "permissions"

    invoke-virtual {v1, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 408604
    instance-of v4, v1, LX/162;

    if-nez v4, :cond_2

    .line 408605
    iget-object v1, p0, LX/2Qc;->b:LX/03V;

    const-string v2, "GraphAuthProxyLoginPostMethod"

    const-string v3, "Invalid graph response"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 408606
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 408607
    invoke-virtual {v1}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 408608
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 408609
    :cond_3
    new-instance v0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;

    invoke-virtual {v2}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v4}, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method
