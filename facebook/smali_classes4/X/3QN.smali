.class public LX/3QN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/2uW;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/3QO;

.field private final d:LX/3QP;

.field public final e:LX/0lC;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/3Ec;

.field private final h:LX/3QQ;

.field private final i:LX/3QR;

.field private final j:LX/3QS;

.field private final k:LX/2NL;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    .line 564305
    const-class v0, LX/3QN;

    sput-object v0, LX/3QN;->a:Ljava/lang/Class;

    .line 564306
    const-string v0, "log:thread-name"

    sget-object v1, LX/2uW;->SET_NAME:LX/2uW;

    const-string v2, "log:thread-image"

    sget-object v3, LX/2uW;->SET_IMAGE:LX/2uW;

    const-string v4, "log:unsubscribe"

    sget-object v5, LX/2uW;->REMOVE_MEMBERS:LX/2uW;

    const-string v6, "log:subscribe"

    sget-object v7, LX/2uW;->ADD_MEMBERS:LX/2uW;

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/3QN;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/3QO;LX/3QP;LX/0lC;LX/0Or;LX/3Ec;LX/3QQ;LX/3QR;LX/3QS;LX/2NL;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3QO;",
            "LX/3QP;",
            "LX/0lC;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKeyFactory;",
            "LX/3QQ;",
            "LX/3QR;",
            "LX/3QS;",
            "LX/2NL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 564415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 564416
    iput-object p1, p0, LX/3QN;->c:LX/3QO;

    .line 564417
    iput-object p2, p0, LX/3QN;->d:LX/3QP;

    .line 564418
    iput-object p3, p0, LX/3QN;->e:LX/0lC;

    .line 564419
    iput-object p4, p0, LX/3QN;->f:LX/0Or;

    .line 564420
    iput-object p5, p0, LX/3QN;->g:LX/3Ec;

    .line 564421
    iput-object p6, p0, LX/3QN;->h:LX/3QQ;

    .line 564422
    iput-object p7, p0, LX/3QN;->i:LX/3QR;

    .line 564423
    iput-object p8, p0, LX/3QN;->j:LX/3QS;

    .line 564424
    iput-object p9, p0, LX/3QN;->k:LX/2NL;

    .line 564425
    return-void
.end method

.method private static a(JLX/0lF;)J
    .locals 2

    .prologue
    .line 564412
    const-string v0, "action_id"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564413
    const-string v0, "action_id"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->c(LX/0lF;)J

    move-result-wide v0

    .line 564414
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0, p1}, LX/6fa;->a(J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(LX/0lF;LX/0lF;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            "LX/0lF;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 564403
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 564404
    invoke-virtual {p0}, LX/0lF;->e()I

    move-result v2

    .line 564405
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 564406
    invoke-virtual {p0, v0}, LX/0lF;->a(I)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->c(LX/0lF;)J

    move-result-wide v4

    .line 564407
    invoke-virtual {p1, v0}, LX/0lF;->a(I)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 564408
    new-instance v6, Lcom/facebook/user/model/UserKey;

    sget-object v7, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v7, v4}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 564409
    new-instance v4, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-direct {v4, v6, v3}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 564410
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 564411
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/3QN;
    .locals 11

    .prologue
    .line 564388
    new-instance v1, LX/3QN;

    invoke-static {p0}, LX/3QO;->a(LX/0QB;)LX/3QO;

    move-result-object v2

    check-cast v2, LX/3QO;

    .line 564389
    new-instance v3, LX/3QP;

    invoke-direct {v3}, LX/3QP;-><init>()V

    .line 564390
    move-object v3, v3

    .line 564391
    move-object v3, v3

    .line 564392
    check-cast v3, LX/3QP;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    const/16 v5, 0x15e7

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/3Ec;->b(LX/0QB;)LX/3Ec;

    move-result-object v6

    check-cast v6, LX/3Ec;

    .line 564393
    new-instance v7, LX/3QQ;

    invoke-direct {v7}, LX/3QQ;-><init>()V

    .line 564394
    move-object v7, v7

    .line 564395
    move-object v7, v7

    .line 564396
    check-cast v7, LX/3QQ;

    .line 564397
    new-instance v8, LX/3QR;

    invoke-direct {v8}, LX/3QR;-><init>()V

    .line 564398
    move-object v8, v8

    .line 564399
    move-object v8, v8

    .line 564400
    check-cast v8, LX/3QR;

    invoke-static {p0}, LX/3QS;->a(LX/0QB;)LX/3QS;

    move-result-object v9

    check-cast v9, LX/3QS;

    invoke-static {p0}, LX/2NL;->a(LX/0QB;)LX/2NL;

    move-result-object v10

    check-cast v10, LX/2NL;

    invoke-direct/range {v1 .. v10}, LX/3QN;-><init>(LX/3QO;LX/3QP;LX/0lC;LX/0Or;LX/3Ec;LX/3QQ;LX/3QR;LX/3QS;LX/2NL;)V

    .line 564401
    move-object v0, v1

    .line 564402
    return-object v0
.end method

.method private static c(LX/0lF;)LX/2uW;
    .locals 2

    .prologue
    .line 564377
    sget-object v0, LX/2uW;->REGULAR:LX/2uW;

    .line 564378
    const-string v1, "l"

    invoke-virtual {p0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 564379
    const-string v0, "l"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 564380
    sget-object v0, LX/2uW;->UNKNOWN:LX/2uW;

    .line 564381
    :cond_0
    :goto_0
    return-object v0

    .line 564382
    :pswitch_0
    sget-object v0, LX/2uW;->ADMIN:LX/2uW;

    goto :goto_0

    .line 564383
    :pswitch_1
    sget-object v0, LX/2uW;->ADD_MEMBERS:LX/2uW;

    goto :goto_0

    .line 564384
    :pswitch_2
    sget-object v0, LX/2uW;->REMOVE_MEMBERS:LX/2uW;

    goto :goto_0

    .line 564385
    :pswitch_3
    sget-object v0, LX/2uW;->SET_NAME:LX/2uW;

    goto :goto_0

    .line 564386
    :pswitch_4
    sget-object v0, LX/2uW;->SET_IMAGE:LX/2uW;

    goto :goto_0

    .line 564387
    :pswitch_5
    sget-object v0, LX/2uW;->CALL_LOG:LX/2uW;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static d(LX/3QN;LX/0lF;)Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 564358
    const-string v1, "ga_t"

    invoke-virtual {p1, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "ga_p"

    invoke-virtual {p1, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 564359
    const-string v1, "ga_t"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 564360
    const-string v2, "ga_p"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 564361
    :try_start_0
    iget-object v3, p0, LX/3QN;->e:LX/0lC;

    invoke-virtual {v3, v2}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 564362
    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    move-result-object v1

    .line 564363
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->STARTED_SHARING_VIDEO:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-eq v1, v3, :cond_0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PARTICIPANT_JOINED_GROUP_CALL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-ne v1, v3, :cond_1

    .line 564364
    :cond_0
    if-eqz v2, :cond_1

    const-string v3, "server_info_data"

    invoke-virtual {v2, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 564365
    :cond_1
    :goto_0
    return-object v0

    .line 564366
    :cond_2
    const-string v3, "server_info_data"

    invoke-virtual {v2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 564367
    const-string v4, "group_call_type"

    invoke-virtual {v2, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 564368
    const-string v0, "group_call_type"

    invoke-virtual {v2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 564369
    :cond_3
    if-eqz v0, :cond_4

    const-string v2, "1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    const/4 v0, 0x1

    .line 564370
    :goto_1
    invoke-static {}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->newBuilder()LX/6ev;

    move-result-object v2

    .line 564371
    iput-object v1, v2, LX/6ev;->a:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 564372
    iput-object v3, v2, LX/6ev;->l:Ljava/lang/String;

    .line 564373
    iput-boolean v0, v2, LX/6ev;->m:Z

    .line 564374
    invoke-virtual {v2}, LX/6ev;->a()Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    move-result-object v0

    goto :goto_0

    .line 564375
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 564376
    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Lcom/facebook/messaging/model/messages/Message;
    .locals 16

    .prologue
    .line 564307
    const-string v2, "uid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v7

    .line 564308
    const-string v2, "0"

    invoke-static {v7, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    if-nez v7, :cond_1

    .line 564309
    :cond_0
    const/4 v2, 0x0

    .line 564310
    :goto_0
    return-object v2

    .line 564311
    :cond_1
    const-string v2, "g"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->d(LX/0lF;)I

    move-result v2

    .line 564312
    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    .line 564313
    const-string v2, "f"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 564314
    const-string v2, "f"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->c(LX/0lF;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 564315
    :goto_1
    invoke-static/range {p2 .. p2}, LX/3QN;->c(LX/0lF;)LX/2uW;

    move-result-object v8

    .line 564316
    const-string v3, "l_m"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->d(LX/0lF;)I

    move-result v3

    if-eqz v3, :cond_9

    const/4 v3, 0x1

    move v6, v3

    .line 564317
    :goto_2
    if-eqz p1, :cond_c

    .line 564318
    sget-object v3, LX/2uW;->ADMIN:LX/2uW;

    if-eq v8, v3, :cond_2

    if-eqz v6, :cond_a

    .line 564319
    :cond_2
    const-string v3, ""

    .line 564320
    :goto_3
    const-string v4, "n"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v9

    .line 564321
    new-instance v10, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v4, Lcom/facebook/user/model/UserKey;

    sget-object v5, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v4, v5, v7}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v11, "@facebook.com"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v10, v4, v3, v5}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/lang/String;)V

    .line 564322
    const/4 v3, 0x0

    .line 564323
    const-string v4, "o"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 564324
    const-string v4, "o"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->d(LX/0lF;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 564325
    :cond_3
    :goto_4
    const-string v4, "s"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->c(LX/0lF;)J

    move-result-wide v12

    .line 564326
    move-object/from16 v0, p2

    invoke-static {v12, v13, v0}, LX/3QN;->a(JLX/0lF;)J

    move-result-wide v14

    .line 564327
    const-string v4, "s_i"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    const-string v4, "s_i"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 564328
    :goto_5
    const-string v5, "ttl"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    const-string v5, "ttl"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->d(LX/0lF;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 564329
    :goto_6
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v11

    invoke-virtual {v11, v9}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    move-result-object v9

    invoke-virtual {v9, v2}, LX/6f7;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/6f7;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/6f7;->b(Ljava/lang/String;)LX/6f7;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, LX/6f7;->a(J)LX/6f7;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, LX/6f7;->c(J)LX/6f7;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/6f7;->c(Ljava/lang/String;)LX/6f7;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6f7;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, LX/6f7;->b(Z)LX/6f7;

    move-result-object v2

    sget-object v4, LX/6f2;->C2DM:LX/6f2;

    invoke-virtual {v2, v4}, LX/6f7;->a(LX/6f2;)LX/6f7;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/6f7;->e(Ljava/lang/String;)LX/6f7;

    move-result-object v2

    invoke-virtual {v2, v8}, LX/6f7;->a(LX/2uW;)LX/6f7;

    move-result-object v2

    sget-object v3, Lcom/facebook/messaging/model/messages/Publicity;->c:Lcom/facebook/messaging/model/messages/Publicity;

    invoke-virtual {v2, v3}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Publicity;)LX/6f7;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/6f7;->a(Ljava/lang/Integer;)LX/6f7;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/6f7;->f(Z)LX/6f7;

    move-result-object v2

    .line 564330
    const-string v3, "m_f"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->d(LX/0lF;)I

    move-result v3

    .line 564331
    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 564332
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;

    const/4 v4, 0x0

    new-instance v5, Lcom/facebook/messaging/model/messagemetadata/MarketplaceTabPlatformMetadata;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Lcom/facebook/messaging/model/messagemetadata/MarketplaceTabPlatformMetadata;-><init>(Z)V

    aput-object v5, v3, v4

    invoke-static {v3}, LX/5dt;->a([Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;)LX/0P1;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6f7;->b(Ljava/util/Map;)LX/6f7;

    .line 564333
    :cond_4
    invoke-virtual {v2}, LX/6f7;->l()LX/2uW;

    move-result-object v3

    sget-object v4, LX/2uW;->CALL_LOG:LX/2uW;

    if-ne v3, v4, :cond_5

    .line 564334
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v7, v12, v13, v1}, LX/6hU;->a(Ljava/lang/String;Ljava/lang/String;JLX/0lF;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v3

    .line 564335
    invoke-virtual {v2, v3}, LX/6f7;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)LX/6f7;

    .line 564336
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, LX/3QN;->d(LX/3QN;LX/0lF;)Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    move-result-object v3

    .line 564337
    if-eqz v3, :cond_6

    .line 564338
    sget-object v4, LX/2uW;->ADMIN:LX/2uW;

    invoke-virtual {v2, v4}, LX/6f7;->a(LX/2uW;)LX/6f7;

    .line 564339
    invoke-virtual {v2, v3}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;)LX/6f7;

    .line 564340
    :cond_6
    invoke-virtual {v2}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 564341
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3QN;->j:LX/3QS;

    sget-object v4, LX/6fK;->PUSH_C2DM_DELIVERY:LX/6fK;

    invoke-virtual {v3, v4, v2}, LX/3QS;->a(LX/6fK;Lcom/facebook/messaging/model/messages/Message;)V

    goto/16 :goto_0

    .line 564342
    :cond_7
    sget-object v2, LX/3QN;->a:Ljava/lang/Class;

    const-string v3, "Received C2DM push for group without threadFbId."

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 564343
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 564344
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3QN;->g:LX/3Ec;

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, LX/3Ec;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    goto/16 :goto_1

    .line 564345
    :cond_9
    const/4 v3, 0x0

    move v6, v3

    goto/16 :goto_2

    .line 564346
    :cond_a
    const-string v3, ":"

    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    .line 564347
    array-length v3, v4

    const/4 v5, 0x2

    if-ne v3, v5, :cond_b

    .line 564348
    const/4 v3, 0x0

    aget-object v3, v4, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 564349
    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_3

    .line 564350
    :cond_b
    const-string v3, ""

    goto/16 :goto_3

    .line 564351
    :cond_c
    const-string v3, ""

    .line 564352
    const-string p1, ""

    goto/16 :goto_3

    .line 564353
    :pswitch_0
    const-string v3, "web"

    goto/16 :goto_4

    .line 564354
    :pswitch_1
    const-string v3, "mobile"

    goto/16 :goto_4

    .line 564355
    :pswitch_2
    const-string v3, "messenger"

    goto/16 :goto_4

    .line 564356
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 564357
    :cond_e
    const/4 v5, 0x0

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
