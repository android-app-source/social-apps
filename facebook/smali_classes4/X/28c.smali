.class public LX/28c;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 374328
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 374329
    return-void
.end method

.method public static a(LX/Gwg;)LX/1qM;
    .locals 2
    .annotation runtime Lcom/facebook/inject/ContextScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/server/SimpleDataFetchQueue;
    .end annotation

    .prologue
    .line 374330
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    .line 374331
    const-string v1, "fetchFwComponents"

    invoke-virtual {v0, v1, p0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 374332
    new-instance v1, LX/2mF;

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    invoke-direct {v1, v0}, LX/2mF;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method public static a(Ljava/lang/Boolean;Lcom/facebook/katana/server/handler/Fb4aAuthHandler;Lcom/facebook/auth/login/AuthServiceHandler;)LX/1qM;
    .locals 1
    .param p0    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/katana/login/UseAuthLogin;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/fbservice/service/AuthQueue;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 374333
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p2

    :cond_0
    move-object p2, p1

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 374334
    return-void
.end method
