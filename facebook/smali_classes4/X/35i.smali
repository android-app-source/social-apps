.class public LX/35i;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:LX/1qa;

.field public final d:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1qa;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 497514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497515
    const v0, 0x7f0b00c5

    invoke-static {p1, v0}, LX/0tP;->b(Landroid/content/res/Resources;I)I

    move-result v0

    iput v0, p0, LX/35i;->a:I

    .line 497516
    const v0, 0x7f0b00c8

    invoke-static {p1, v0}, LX/0tP;->b(Landroid/content/res/Resources;I)I

    move-result v0

    iput v0, p0, LX/35i;->b:I

    .line 497517
    iput-object p2, p0, LX/35i;->c:LX/1qa;

    .line 497518
    iput-object p1, p0, LX/35i;->d:Landroid/content/res/Resources;

    .line 497519
    return-void
.end method

.method public static a(LX/0QB;)LX/35i;
    .locals 5

    .prologue
    .line 497520
    const-class v1, LX/35i;

    monitor-enter v1

    .line 497521
    :try_start_0
    sget-object v0, LX/35i;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 497522
    sput-object v2, LX/35i;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 497523
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497524
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 497525
    new-instance p0, LX/35i;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v4

    check-cast v4, LX/1qa;

    invoke-direct {p0, v3, v4}, LX/35i;-><init>(Landroid/content/res/Resources;LX/1qa;)V

    .line 497526
    move-object v0, p0

    .line 497527
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 497528
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/35i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 497529
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 497530
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 497531
    invoke-virtual {p0, p1}, LX/35i;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    const/4 v2, 0x0

    .line 497532
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 497533
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 497534
    iget-object v3, p0, LX/35i;->d:Landroid/content/res/Resources;

    int-to-float v4, v0

    invoke-static {v3, v4}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v3

    .line 497535
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    if-nez v4, :cond_0

    move v1, v2

    .line 497536
    :goto_0
    move v0, v1

    .line 497537
    return v0

    .line 497538
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1, v3}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 497539
    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 497540
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 497541
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/35i;->b:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/35i;->a:I

    goto :goto_0
.end method
