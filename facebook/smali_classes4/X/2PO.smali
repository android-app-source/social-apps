.class public LX/2PO;
.super LX/2PI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2PI",
        "<",
        "Lcom/facebook/messaging/tincan/outbound/TincanDeviceRegistrationListener;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2PO;


# instance fields
.field private final c:LX/2PJ;

.field private final d:LX/0SF;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 406393
    const-class v0, LX/2PO;

    sput-object v0, LX/2PO;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2PJ;LX/0SF;LX/0Or;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2PJ;",
            "LX/0SF;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406394
    const/16 v0, 0xc

    invoke-direct {p0, v0}, LX/2PI;-><init>(I)V

    .line 406395
    iput-object p1, p0, LX/2PO;->c:LX/2PJ;

    .line 406396
    iput-object p2, p0, LX/2PO;->d:LX/0SF;

    .line 406397
    iput-object p3, p0, LX/2PO;->e:LX/0Or;

    .line 406398
    return-void
.end method

.method public static a(LX/0QB;)LX/2PO;
    .locals 6

    .prologue
    .line 406399
    sget-object v0, LX/2PO;->f:LX/2PO;

    if-nez v0, :cond_1

    .line 406400
    const-class v1, LX/2PO;

    monitor-enter v1

    .line 406401
    :try_start_0
    sget-object v0, LX/2PO;->f:LX/2PO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406402
    if-eqz v2, :cond_0

    .line 406403
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 406404
    new-instance v5, LX/2PO;

    invoke-static {v0}, LX/2PJ;->a(LX/0QB;)LX/2PJ;

    move-result-object v3

    check-cast v3, LX/2PJ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SF;

    const/16 p0, 0x15e8

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/2PO;-><init>(LX/2PJ;LX/0SF;LX/0Or;)V

    .line 406405
    move-object v0, v5

    .line 406406
    sput-object v0, LX/2PO;->f:LX/2PO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406407
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406408
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406409
    :cond_1
    sget-object v0, LX/2PO;->f:LX/2PO;

    return-object v0

    .line 406410
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406411
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 406412
    sget-object v0, LX/2PO;->b:Ljava/lang/Class;

    const-string v1, "Malformed StoredProcedureResponse"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 406413
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PA;

    .line 406414
    invoke-virtual {v0}, LX/2PA;->c()V

    goto :goto_0

    .line 406415
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 406416
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PA;

    .line 406417
    invoke-static {v0}, LX/2PA;->m(LX/2PA;)V

    .line 406418
    goto :goto_0

    .line 406419
    :cond_0
    return-void
.end method

.method public final a(LX/2PA;)V
    .locals 1

    .prologue
    .line 406420
    invoke-super {p0, p1}, LX/2PI;->a(Ljava/lang/Object;)V

    .line 406421
    invoke-virtual {p0}, LX/2PI;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406422
    invoke-static {p1}, LX/2PA;->m(LX/2PA;)V

    .line 406423
    :cond_0
    return-void
.end method

.method public final a(LX/Dph;)V
    .locals 4

    .prologue
    .line 406424
    if-nez p1, :cond_1

    .line 406425
    invoke-direct {p0}, LX/2PO;->c()V

    .line 406426
    :cond_0
    :goto_0
    return-void

    .line 406427
    :cond_1
    iget-object v0, p1, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_2

    .line 406428
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PA;

    .line 406429
    invoke-virtual {v0}, LX/2PA;->b()V

    goto :goto_1

    .line 406430
    :cond_2
    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    if-eqz v0, :cond_3

    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/6kT;->a(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 406431
    :cond_3
    invoke-direct {p0}, LX/2PO;->c()V

    goto :goto_0

    .line 406432
    :cond_4
    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    invoke-virtual {v0}, LX/Dpi;->c()LX/DpX;

    move-result-object v0

    iget-object v0, v0, LX/DpX;->is_primary_device:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 406433
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PA;

    .line 406434
    invoke-virtual {v0, v1}, LX/2PA;->a(Z)V

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 406435
    check-cast p1, LX/2PA;

    invoke-virtual {p0, p1}, LX/2PO;->a(LX/2PA;)V

    return-void
.end method

.method public final declared-synchronized a([BLjava/lang/String;[BI[B[BI[B)Z
    .locals 12

    .prologue
    .line 406436
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/2PI;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 406437
    sget-object v2, LX/2PO;->b:Ljava/lang/Class;

    const-string v3, "No stored procedure sender for registerDevice"

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406438
    const/4 v2, 0x0

    .line 406439
    :goto_0
    monitor-exit p0

    return v2

    .line 406440
    :cond_0
    :try_start_1
    new-instance v2, LX/Dpf;

    new-instance v3, LX/DpU;

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-direct {v3, v0, v4}, LX/DpU;-><init>([BLjava/lang/Integer;)V

    move-object/from16 v0, p6

    invoke-direct {v2, v3, v0}, LX/Dpf;-><init>(LX/DpU;[B)V

    .line 406441
    new-instance v3, LX/DpU;

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p8

    invoke-direct {v3, v0, v4}, LX/DpU;-><init>([BLjava/lang/Integer;)V

    .line 406442
    new-instance v9, LX/DpW;

    invoke-direct {v9, p2, p3, v2, v3}, LX/DpW;-><init>(Ljava/lang/String;[BLX/Dpf;LX/DpU;)V

    .line 406443
    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, LX/DpM;

    iget-object v2, p0, LX/2PO;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v6, p0, LX/2PO;->c:LX/2PJ;

    invoke-virtual {v6}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v2, v6}, LX/DpM;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    iget-object v2, p0, LX/2PO;->d:LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v6

    const-wide/16 v10, 0x3e8

    mul-long/2addr v6, v10

    const/16 v8, 0xa

    invoke-static {v9}, LX/DpO;->a(LX/DpW;)LX/DpO;

    move-result-object v9

    move-object v10, p1

    invoke-static/range {v3 .. v10}, LX/Dpm;->a(LX/Dpe;LX/DpM;LX/DpM;JILX/DpO;[B)LX/DpN;

    move-result-object v2

    .line 406444
    invoke-static {v2}, LX/Dpn;->a(LX/1u2;)[B

    move-result-object v2

    invoke-virtual {p0, v2}, LX/2PI;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406445
    const/4 v2, 0x1

    goto :goto_0

    .line 406446
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
