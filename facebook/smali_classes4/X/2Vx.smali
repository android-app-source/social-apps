.class public abstract LX/2Vx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;
.implements LX/0c5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<KEY:",
        "LX/2WG;",
        "VA",
        "LUE:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/2F1;",
        "LX/0c5;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0SG;

.field private final c:LX/03V;

.field public final d:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<TKEY;",
            "LX/2Vx",
            "<TKEY;TVA",
            "LUE;",
            ">.InMemoryCachedValue<TKEY;TVA",
            "LUE;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mInMemoryWriteLock for writes to keep in sync with mBytesInMemory"
    .end annotation
.end field

.field public final e:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<TKEY;TVA",
            "LUE;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/2W2;

.field private final g:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<TKEY;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/lang/Object;

.field private final i:Ljava/lang/String;

.field public final j:Z

.field private k:I

.field private l:I

.field private final m:I

.field private final n:I

.field public o:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mInMemoryWriteLock"
    .end annotation
.end field

.field private final p:LX/0pk;

.field public final q:LX/0pk;

.field public final r:LX/0pk;

.field private final s:LX/0pk;

.field public final t:LX/1GQ;

.field private final u:LX/1Gm;

.field private final v:LX/0rf;

.field private final w:LX/0rb;

.field public final x:LX/1Ha;

.field public final y:LX/2W4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2W4",
            "<TKEY;TVA",
            "LUE;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 418491
    const-class v0, LX/2Vx;

    sput-object v0, LX/2Vx;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0pi;LX/1Gm;LX/03V;LX/2W2;LX/0rb;LX/1Ha;LX/2W4;LX/1GQ;)V
    .locals 6
    .param p6    # LX/0rb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0pi;",
            "LX/1Gm;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2W2;",
            "LX/0rb;",
            "LX/1Ha;",
            "LX/2W4",
            "<TKEY;TVA",
            "LUE;",
            ">;",
            "LX/1GQ;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v5, 0x80

    const/4 v4, 0x4

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 418441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418442
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, LX/2Vx;->h:Ljava/lang/Object;

    .line 418443
    iput-object p1, p0, LX/2Vx;->b:LX/0SG;

    .line 418444
    iput-object p4, p0, LX/2Vx;->c:LX/03V;

    .line 418445
    iget-object v1, p5, LX/2W2;->b:Ljava/lang/String;

    move-object v1, v1

    .line 418446
    iput-object v1, p0, LX/2Vx;->i:Ljava/lang/String;

    .line 418447
    iget v1, p5, LX/2W2;->f:I

    move v1, v1

    .line 418448
    iput v1, p0, LX/2Vx;->m:I

    .line 418449
    iget v1, p5, LX/2W2;->g:I

    move v1, v1

    .line 418450
    iput v1, p0, LX/2Vx;->n:I

    .line 418451
    iget-boolean v1, p5, LX/2W2;->c:Z

    move v1, v1

    .line 418452
    iput-boolean v1, p0, LX/2Vx;->j:Z

    .line 418453
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iput-object v1, p0, LX/2Vx;->g:Ljava/util/concurrent/ConcurrentMap;

    .line 418454
    iput-object p9, p0, LX/2Vx;->t:LX/1GQ;

    .line 418455
    iput-object p7, p0, LX/2Vx;->x:LX/1Ha;

    .line 418456
    iput-object p8, p0, LX/2Vx;->y:LX/2W4;

    .line 418457
    iput-object p5, p0, LX/2Vx;->f:LX/2W2;

    .line 418458
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 418459
    iget-object v2, p5, LX/2W2;->a:Ljava/lang/String;

    move-object v2, v2

    .line 418460
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_overall"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/0pi;->b(Ljava/lang/String;)LX/0pk;

    move-result-object v1

    iput-object v1, p0, LX/2Vx;->p:LX/0pk;

    .line 418461
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 418462
    iget-object v2, p5, LX/2W2;->a:Ljava/lang/String;

    move-object v2, v2

    .line 418463
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_disk"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/0pi;->b(Ljava/lang/String;)LX/0pk;

    move-result-object v1

    iput-object v1, p0, LX/2Vx;->s:LX/0pk;

    .line 418464
    iget-boolean v1, p0, LX/2Vx;->j:Z

    if-eqz v1, :cond_2

    .line 418465
    new-instance v1, LX/0S8;

    invoke-direct {v1}, LX/0S8;-><init>()V

    invoke-virtual {v1, v5}, LX/0S8;->a(I)LX/0S8;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/0S8;->c(I)LX/0S8;

    move-result-object v1

    invoke-virtual {v1}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iput-object v1, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    .line 418466
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 418467
    iget-object v2, p5, LX/2W2;->a:Ljava/lang/String;

    move-object v2, v2

    .line 418468
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_memory"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/0pi;->b(Ljava/lang/String;)LX/0pk;

    move-result-object v1

    iput-object v1, p0, LX/2Vx;->q:LX/0pk;

    .line 418469
    new-instance v1, LX/0S8;

    invoke-direct {v1}, LX/0S8;-><init>()V

    invoke-virtual {v1, v5}, LX/0S8;->a(I)LX/0S8;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/0S8;->c(I)LX/0S8;

    move-result-object v1

    invoke-virtual {v1}, LX/0S8;->g()LX/0S8;

    move-result-object v1

    invoke-virtual {v1}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iput-object v1, p0, LX/2Vx;->e:Ljava/util/concurrent/ConcurrentMap;

    .line 418470
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 418471
    iget-object v2, p5, LX/2W2;->a:Ljava/lang/String;

    move-object v2, v2

    .line 418472
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_weakmem"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/0pi;->b(Ljava/lang/String;)LX/0pk;

    move-result-object v1

    iput-object v1, p0, LX/2Vx;->r:LX/0pk;

    .line 418473
    iget v1, p5, LX/2W2;->d:I

    move v1, v1

    .line 418474
    iput v1, p0, LX/2Vx;->k:I

    .line 418475
    iget v1, p5, LX/2W2;->e:I

    move v1, v1

    .line 418476
    iput v1, p0, LX/2Vx;->l:I

    .line 418477
    iget v1, p0, LX/2Vx;->l:I

    iget v2, p0, LX/2Vx;->k:I

    if-gt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 418478
    new-instance v0, LX/4nM;

    invoke-direct {v0, p0}, LX/4nM;-><init>(LX/2Vx;)V

    iput-object v0, p0, LX/2Vx;->v:LX/0rf;

    .line 418479
    :goto_0
    iput-object p3, p0, LX/2Vx;->u:LX/1Gm;

    .line 418480
    iput-object p6, p0, LX/2Vx;->w:LX/0rb;

    .line 418481
    iget-object v0, p0, LX/2Vx;->w:LX/0rb;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/2Vx;->j:Z

    if-eqz v0, :cond_1

    .line 418482
    iget-object v0, p0, LX/2Vx;->w:LX/0rb;

    iget-object v1, p0, LX/2Vx;->v:LX/0rf;

    invoke-interface {v0, v1}, LX/0rb;->a(LX/0rf;)V

    .line 418483
    :cond_1
    return-void

    .line 418484
    :cond_2
    iput-object v3, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    .line 418485
    iput-object v3, p0, LX/2Vx;->e:Ljava/util/concurrent/ConcurrentMap;

    .line 418486
    iput-object v3, p0, LX/2Vx;->q:LX/0pk;

    .line 418487
    iput-object v3, p0, LX/2Vx;->r:LX/0pk;

    .line 418488
    iput-object v3, p0, LX/2Vx;->v:LX/0rf;

    .line 418489
    iput v0, p0, LX/2Vx;->k:I

    .line 418490
    iput v0, p0, LX/2Vx;->l:I

    goto :goto_0
.end method

.method public static a(LX/2Vx;)V
    .locals 2

    .prologue
    .line 418432
    iget-boolean v0, p0, LX/2Vx;->j:Z

    if-nez v0, :cond_0

    .line 418433
    :goto_0
    return-void

    .line 418434
    :cond_0
    iget-object v1, p0, LX/2Vx;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 418435
    :try_start_0
    iget-object v0, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 418436
    iget-object v0, p0, LX/2Vx;->e:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 418437
    const/4 v0, 0x0

    iput v0, p0, LX/2Vx;->o:I

    .line 418438
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418439
    invoke-static {p0}, LX/2Vx;->g(LX/2Vx;)V

    goto :goto_0

    .line 418440
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(LX/2Vx;LX/2WG;Ljava/lang/Object;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKEY;TVA",
            "LUE;",
            ")V"
        }
    .end annotation

    .prologue
    .line 418414
    iget-boolean v0, p0, LX/2Vx;->j:Z

    if-nez v0, :cond_1

    .line 418415
    :cond_0
    :goto_0
    return-void

    .line 418416
    :cond_1
    invoke-virtual {p0, p2}, LX/2Vx;->a(Ljava/lang/Object;)I

    move-result v1

    .line 418417
    iget v0, p0, LX/2Vx;->l:I

    if-lez v0, :cond_0

    .line 418418
    iget-object v2, p0, LX/2Vx;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 418419
    :try_start_0
    iget-object v0, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    new-instance v3, LX/4nL;

    invoke-direct {v3, p0, p1, p2, v1}, LX/4nL;-><init>(LX/2Vx;LX/2WG;Ljava/lang/Object;I)V

    invoke-interface {v0, p1, v3}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4nL;

    .line 418420
    if-eqz v0, :cond_2

    .line 418421
    iget v3, p0, LX/2Vx;->o:I

    iget v0, v0, LX/4nL;->d:I

    sub-int v0, v3, v0

    iput v0, p0, LX/2Vx;->o:I

    .line 418422
    :cond_2
    iget v0, p0, LX/2Vx;->o:I

    add-int/2addr v0, v1

    iput v0, p0, LX/2Vx;->o:I

    .line 418423
    iget-object v0, p0, LX/2Vx;->q:LX/0pk;

    int-to-long v4, v1

    .line 418424
    sget-object v6, LX/17N;->INSERTION_TIME_MS:LX/17N;

    invoke-static {v0, v6}, LX/0pk;->b(LX/0pk;LX/17N;)V

    .line 418425
    sget-object v6, LX/17N;->INSERTION_ITEM:LX/17N;

    const-wide/16 v8, 0x1

    invoke-static {v0, v6, v8, v9}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 418426
    sget-object v6, LX/17N;->INSERTION_SIZE:LX/17N;

    invoke-static {v0, v6, v4, v5}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 418427
    iget v0, p0, LX/2Vx;->o:I

    iget v1, p0, LX/2Vx;->k:I

    if-gt v0, v1, :cond_3

    iget-object v0, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v0

    iget v1, p0, LX/2Vx;->m:I

    if-le v0, v1, :cond_4

    .line 418428
    :cond_3
    iget v0, p0, LX/2Vx;->l:I

    iget v1, p0, LX/2Vx;->n:I

    invoke-virtual {p0, v0, v1}, LX/2Vx;->a(II)V

    .line 418429
    :cond_4
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418430
    invoke-static {p0}, LX/2Vx;->g(LX/2Vx;)V

    goto :goto_0

    .line 418431
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(LX/2Vx;Ljava/io/IOException;LX/43W;)V
    .locals 4

    .prologue
    .line 418410
    instance-of v0, p1, Ljava/io/FileNotFoundException;

    if-eqz v0, :cond_0

    .line 418411
    iget-object v0, p0, LX/2Vx;->t:LX/1GQ;

    sget-object v1, LX/43W;->READ_FILE_NOT_FOUND:LX/43W;

    sget-object v2, LX/2Vx;->a:Ljava/lang/Class;

    const-string v3, "getMedia"

    invoke-interface {v0, v1, v2, v3, p1}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 418412
    :goto_0
    return-void

    .line 418413
    :cond_0
    iget-object v0, p0, LX/2Vx;->t:LX/1GQ;

    sget-object v1, LX/2Vx;->a:Ljava/lang/Class;

    const-string v2, "getMedia"

    invoke-interface {v0, p2, v1, v2, p1}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private d()I
    .locals 2

    .prologue
    .line 418407
    iget-object v1, p0, LX/2Vx;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 418408
    :try_start_0
    iget v0, p0, LX/2Vx;->o:I

    monitor-exit v1

    return v0

    .line 418409
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private f()I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 418406
    iget-object v0, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v0

    goto :goto_0
.end method

.method private f(LX/2WG;)Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKEY;)Z"
        }
    .end annotation

    .prologue
    .line 418403
    iget-boolean v0, p0, LX/2Vx;->j:Z

    if-nez v0, :cond_0

    .line 418404
    const/4 v0, 0x0

    .line 418405
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private g(LX/2WG;)Ljava/lang/Object;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKEY;)TVA",
            "LUE;"
        }
    .end annotation

    .prologue
    .line 418379
    iget-object v0, p0, LX/2Vx;->s:LX/0pk;

    invoke-virtual {v0}, LX/0pk;->a()V

    .line 418380
    iget-object v0, p0, LX/2Vx;->x:LX/1Ha;

    invoke-virtual {p1}, LX/2WG;->a()LX/1bh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Ha;->a(LX/1bh;)LX/1gI;

    move-result-object v0

    .line 418381
    if-nez v0, :cond_0

    .line 418382
    iget-object v0, p0, LX/2Vx;->s:LX/0pk;

    invoke-virtual {v0}, LX/0pk;->c()V

    .line 418383
    const/4 v0, 0x0

    .line 418384
    :goto_0
    return-object v0

    .line 418385
    :cond_0
    const/4 v1, 0x0

    .line 418386
    :try_start_0
    invoke-interface {v0}, LX/1gI;->b()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 418387
    :try_start_1
    iget-object v3, p0, LX/2Vx;->y:LX/2W4;

    invoke-interface {v3, p1, v2}, LX/2W4;->a(LX/2WG;[B)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v1

    .line 418388
    :goto_1
    if-nez v1, :cond_1

    .line 418389
    sget-object v2, LX/2Vx;->a:Ljava/lang/Class;

    const-string v3, "Decode from memory failed. Reading from disk for key %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 418390
    :cond_1
    :goto_2
    move-object v1, v1

    .line 418391
    if-nez v1, :cond_2

    .line 418392
    :try_start_2
    iget-object v2, p0, LX/2Vx;->y:LX/2W4;

    invoke-interface {v2, p1, v0}, LX/2W4;->a(LX/2WG;LX/1gI;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    .line 418393
    :goto_3
    if-eqz v0, :cond_3

    .line 418394
    iget-object v1, p0, LX/2Vx;->s:LX/0pk;

    invoke-virtual {v1}, LX/0pk;->b()V

    .line 418395
    invoke-static {p0, p1, v0}, LX/2Vx;->a(LX/2Vx;LX/2WG;Ljava/lang/Object;)V

    goto :goto_0

    .line 418396
    :catch_0
    move-exception v0

    .line 418397
    sget-object v2, LX/43W;->READ_DECODE:LX/43W;

    invoke-static {p0, v0, v2}, LX/2Vx;->a(LX/2Vx;Ljava/io/IOException;LX/43W;)V

    :cond_2
    move-object v0, v1

    goto :goto_3

    .line 418398
    :cond_3
    iget-object v1, p0, LX/2Vx;->s:LX/0pk;

    invoke-virtual {v1}, LX/0pk;->c()V

    goto :goto_0

    .line 418399
    :catch_1
    move-exception v2

    .line 418400
    sget-object v3, LX/43W;->READ_FILE:LX/43W;

    invoke-static {p0, v2, v3}, LX/2Vx;->a(LX/2Vx;Ljava/io/IOException;LX/43W;)V

    goto :goto_2

    .line 418401
    :catch_2
    move-exception v2

    .line 418402
    iget-object v3, p0, LX/2Vx;->t:LX/1GQ;

    sget-object v4, LX/43W;->READ_DECODE:LX/43W;

    sget-object v5, LX/2Vx;->a:Ljava/lang/Class;

    const-string v6, "getMedia"

    invoke-interface {v3, v4, v5, v6, v2}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static g(LX/2Vx;)V
    .locals 5

    .prologue
    .line 418370
    iget-boolean v0, p0, LX/2Vx;->j:Z

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 418371
    invoke-direct {p0}, LX/2Vx;->d()I

    move-result v0

    .line 418372
    invoke-direct {p0}, LX/2Vx;->f()I

    move-result v1

    .line 418373
    if-lez v1, :cond_0

    .line 418374
    iget-object v2, p0, LX/2Vx;->c:LX/03V;

    iget-object v3, p0, LX/2Vx;->q:LX/0pk;

    sget-object v4, LX/17N;->BYTES_COUNT:LX/17N;

    invoke-virtual {v3, v4}, LX/0pk;->a(LX/17N;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 418375
    iget-object v0, p0, LX/2Vx;->c:LX/03V;

    iget-object v2, p0, LX/2Vx;->q:LX/0pk;

    sget-object v3, LX/17N;->ENTRIES_COUNT:LX/17N;

    invoke-virtual {v2, v3}, LX/0pk;->a(LX/17N;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 418376
    :goto_0
    return-void

    .line 418377
    :cond_0
    iget-object v0, p0, LX/2Vx;->c:LX/03V;

    iget-object v1, p0, LX/2Vx;->q:LX/0pk;

    sget-object v2, LX/17N;->BYTES_COUNT:LX/17N;

    invoke-virtual {v1, v2}, LX/0pk;->a(LX/17N;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(Ljava/lang/String;)V

    .line 418378
    iget-object v0, p0, LX/2Vx;->c:LX/03V;

    iget-object v1, p0, LX/2Vx;->q:LX/0pk;

    sget-object v2, LX/17N;->ENTRIES_COUNT:LX/17N;

    invoke-virtual {v1, v2}, LX/0pk;->a(LX/17N;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private h(LX/2WG;)Z
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKEY;)Z"
        }
    .end annotation

    .prologue
    .line 418369
    iget-object v0, p0, LX/2Vx;->x:LX/1Ha;

    invoke-virtual {p1}, LX/2WG;->a()LX/1bh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Ha;->b(LX/1bh;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVA",
            "LUE;",
            ")I"
        }
    .end annotation
.end method

.method public final a(LX/2WG;Ljava/io/InputStream;)LX/1gI;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKEY;",
            "Ljava/io/InputStream;",
            ")",
            "LX/1gI;"
        }
    .end annotation

    .prologue
    .line 418366
    new-instance v0, LX/43Z;

    invoke-direct {v0, p2}, LX/43Z;-><init>(Ljava/io/InputStream;)V

    move-object v0, v0

    .line 418367
    iget-object v1, p0, LX/2Vx;->x:LX/1Ha;

    invoke-virtual {p1}, LX/2WG;->a()LX/1bh;

    move-result-object p2

    invoke-interface {v1, p2, v0}, LX/1Ha;->a(LX/1bh;LX/2uu;)LX/1gI;

    move-result-object v1

    move-object v0, v1

    .line 418368
    return-object v0
.end method

.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 4

    .prologue
    .line 418361
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "media_cache_size"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 418362
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/2Vx;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_memory_cache_size"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/2Vx;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 418363
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/2Vx;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_memory_cache_count"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/2Vx;->f()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 418364
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/2Vx;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_file_cache_size"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2Vx;->x:LX/1Ha;

    invoke-interface {v2}, LX/1Ha;->c()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 418365
    return-object v0
.end method

.method public final a(II)V
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 418347
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cleaning out in memory cache: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/2Vx;->o:I

    div-int/lit16 v1, v1, 0x400

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " KB with "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " values"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418348
    iget-object v0, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 418349
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4nL;

    .line 418350
    iget-wide v4, v0, LX/4nL;->e:J

    .line 418351
    iput-wide v4, v0, LX/4nL;->f:J

    .line 418352
    goto :goto_0

    .line 418353
    :cond_0
    new-instance v0, LX/4nK;

    invoke-direct {v0, p0}, LX/4nK;-><init>(LX/2Vx;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 418354
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4nL;

    .line 418355
    iget-object v2, p0, LX/2Vx;->e:Ljava/util/concurrent/ConcurrentMap;

    iget-object v3, v0, LX/4nL;->b:LX/2WG;

    iget-object v4, v0, LX/4nL;->c:Ljava/lang/Object;

    invoke-interface {v2, v3, v4}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 418356
    iget-object v2, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    iget-object v3, v0, LX/4nL;->b:LX/2WG;

    invoke-interface {v2, v3}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 418357
    iget v2, p0, LX/2Vx;->o:I

    iget v0, v0, LX/4nL;->d:I

    sub-int v0, v2, v0

    iput v0, p0, LX/2Vx;->o:I

    .line 418358
    iget v0, p0, LX/2Vx;->o:I

    if-ge v0, p1, :cond_1

    iget-object v0, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v0

    if-gt v0, p2, :cond_1

    .line 418359
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Finished cleaning out in memory cache: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/2Vx;->o:I

    div-int/lit16 v1, v1, 0x400

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " KB with "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " values"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418360
    return-void
.end method

.method public final a(LX/2WG;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKEY;J)V"
        }
    .end annotation

    .prologue
    .line 418345
    iget-object v0, p0, LX/2Vx;->g:Ljava/util/concurrent/ConcurrentMap;

    iget-object v1, p0, LX/2Vx;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    add-long/2addr v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 418346
    return-void
.end method

.method public final a(LX/2WG;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKEY;)Z"
        }
    .end annotation

    .prologue
    .line 418344
    invoke-direct {p0, p1}, LX/2Vx;->f(LX/2WG;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Vx;->x:LX/1Ha;

    invoke-virtual {p1}, LX/2WG;->a()LX/1bh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Ha;->d(LX/1bh;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/2WG;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKEY;)TVA",
            "LUE;"
        }
    .end annotation

    .prologue
    .line 418310
    iget-object v0, p0, LX/2Vx;->p:LX/0pk;

    invoke-virtual {v0}, LX/0pk;->a()V

    .line 418311
    const/4 v3, 0x0

    .line 418312
    iget-boolean v2, p0, LX/2Vx;->j:Z

    if-nez v2, :cond_2

    move-object v2, v3

    .line 418313
    :goto_0
    move-object v0, v2

    .line 418314
    if-eqz v0, :cond_0

    .line 418315
    invoke-direct {p0, p1}, LX/2Vx;->h(LX/2WG;)Z

    .line 418316
    :goto_1
    if-eqz v0, :cond_1

    .line 418317
    iget-object v1, p0, LX/2Vx;->p:LX/0pk;

    invoke-virtual {v1}, LX/0pk;->b()V

    .line 418318
    :goto_2
    return-object v0

    .line 418319
    :cond_0
    invoke-direct {p0, p1}, LX/2Vx;->g(LX/2WG;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    .line 418320
    :cond_1
    iget-object v1, p0, LX/2Vx;->p:LX/0pk;

    invoke-virtual {v1}, LX/0pk;->c()V

    goto :goto_2

    .line 418321
    :cond_2
    iget-object v2, p0, LX/2Vx;->q:LX/0pk;

    invoke-virtual {v2}, LX/0pk;->a()V

    .line 418322
    iget-object v2, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4nL;

    .line 418323
    if-eqz v2, :cond_4

    .line 418324
    iget-object v4, v2, LX/4nL;->c:Ljava/lang/Object;

    .line 418325
    const/4 v5, 0x1

    move v5, v5

    .line 418326
    if-eqz v5, :cond_3

    .line 418327
    iget-object v8, v2, LX/4nL;->a:LX/2Vx;

    iget-object v8, v8, LX/2Vx;->b:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    iput-wide v8, v2, LX/4nL;->e:J

    .line 418328
    iget-object v2, p0, LX/2Vx;->q:LX/0pk;

    invoke-virtual {v2}, LX/0pk;->b()V

    move-object v2, v4

    .line 418329
    goto :goto_0

    .line 418330
    :cond_3
    iget-object v2, p0, LX/2Vx;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p1, v4}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 418331
    sget-object v2, LX/2Vx;->a:Ljava/lang/Class;

    const-string v4, "Invalid media found in cache for %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v2, v4, v5}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 418332
    iget-object v2, p0, LX/2Vx;->t:LX/1GQ;

    sget-object v4, LX/43W;->READ_INVALID_ENTRY:LX/43W;

    sget-object v5, LX/2Vx;->a:Ljava/lang/Class;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getCachedMediaFromStrongAndWeakCaches("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v4, v5, v6, v3}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 418333
    :cond_4
    iget-object v2, p0, LX/2Vx;->q:LX/0pk;

    invoke-virtual {v2}, LX/0pk;->c()V

    .line 418334
    iget-object v2, p0, LX/2Vx;->r:LX/0pk;

    invoke-virtual {v2}, LX/0pk;->a()V

    .line 418335
    iget-object v2, p0, LX/2Vx;->e:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 418336
    if-eqz v2, :cond_6

    .line 418337
    const/4 v4, 0x1

    move v4, v4

    .line 418338
    if-eqz v4, :cond_5

    .line 418339
    invoke-static {p0, p1, v2}, LX/2Vx;->a(LX/2Vx;LX/2WG;Ljava/lang/Object;)V

    .line 418340
    iget-object v3, p0, LX/2Vx;->r:LX/0pk;

    invoke-virtual {v3}, LX/0pk;->b()V

    goto/16 :goto_0

    .line 418341
    :cond_5
    iget-object v4, p0, LX/2Vx;->e:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4, p1, v2}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 418342
    :cond_6
    iget-object v2, p0, LX/2Vx;->r:LX/0pk;

    invoke-virtual {v2}, LX/0pk;->c()V

    move-object v2, v3

    .line 418343
    goto/16 :goto_0
.end method

.method public final c(LX/2WG;)LX/1gI;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKEY;)",
            "LX/1gI;"
        }
    .end annotation

    .prologue
    .line 418308
    iget-object v0, p0, LX/2Vx;->x:LX/1Ha;

    invoke-virtual {p1}, LX/2WG;->a()LX/1bh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Ha;->a(LX/1bh;)LX/1gI;

    move-result-object v0

    .line 418309
    return-object v0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 418305
    invoke-static {p0}, LX/2Vx;->a(LX/2Vx;)V

    .line 418306
    iget-object v0, p0, LX/2Vx;->x:LX/1Ha;

    invoke-interface {v0}, LX/1Ha;->d()V

    .line 418307
    return-void
.end method

.method public final d(LX/2WG;)Landroid/net/Uri;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKEY;)",
            "Landroid/net/Uri;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 418299
    iget-object v0, p0, LX/2Vx;->x:LX/1Ha;

    invoke-virtual {p1}, LX/2WG;->a()LX/1bh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Ha;->a(LX/1bh;)LX/1gI;

    move-result-object v0

    check-cast v0, LX/1gH;

    .line 418300
    const/4 v1, 0x0

    .line 418301
    if-eqz v0, :cond_0

    .line 418302
    iget-object v1, v0, LX/1gH;->a:Ljava/io/File;

    move-object v0, v1

    .line 418303
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 418304
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final e(LX/2WG;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKEY;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 418289
    iget-object v0, p0, LX/2Vx;->g:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 418290
    if-nez v0, :cond_0

    move v0, v1

    .line 418291
    :goto_0
    return v0

    .line 418292
    :cond_0
    iget-object v2, p0, LX/2Vx;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 418293
    iget-object v0, p0, LX/2Vx;->u:LX/1Gm;

    .line 418294
    invoke-static {v0}, LX/1Gm;->f(LX/1Gm;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 418295
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 418296
    :cond_1
    iget-object v2, p0, LX/2Vx;->g:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move v0, v1

    .line 418297
    goto :goto_0

    .line 418298
    :cond_2
    const-string v6, "ignored_request_due_to_cache_failure"

    const-wide/16 v8, 0x1

    invoke-virtual {v0, v6, v8, v9}, LX/0Vx;->a(Ljava/lang/String;J)V

    goto :goto_1
.end method
