.class public LX/297;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field public final d:LX/0Zb;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;Landroid/content/Context;LX/0Zb;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/notification/settings/IsGlobalNotificationPreferenceEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/content/Context;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 375702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 375703
    iput-object p1, p0, LX/297;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 375704
    iput-object p2, p0, LX/297;->b:LX/0Or;

    .line 375705
    iput-object p3, p0, LX/297;->c:Landroid/content/Context;

    .line 375706
    iput-object p4, p0, LX/297;->d:LX/0Zb;

    .line 375707
    return-void
.end method

.method public static a(LX/0QB;)LX/297;
    .locals 1

    .prologue
    .line 375701
    invoke-static {p0}, LX/297;->b(LX/0QB;)LX/297;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/297;
    .locals 5

    .prologue
    .line 375708
    new-instance v3, LX/297;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v1, 0x153a

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-direct {v3, v0, v4, v1, v2}, LX/297;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;Landroid/content/Context;LX/0Zb;)V

    .line 375709
    return-object v3
.end method


# virtual methods
.method public final a()Lcom/facebook/messaging/model/threads/NotificationSetting;
    .locals 4

    .prologue
    .line 375698
    iget-object v0, p0, LX/297;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 375699
    sget-object v0, Lcom/facebook/messaging/model/threads/NotificationSetting;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 375700
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/297;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0db;->J:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b(J)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/NotificationSetting;
    .locals 4

    .prologue
    .line 375681
    if-nez p1, :cond_0

    .line 375682
    sget-object v0, Lcom/facebook/messaging/model/threads/NotificationSetting;->a:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 375683
    :goto_0
    return-object v0

    .line 375684
    :cond_0
    invoke-static {p1}, LX/0db;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Tn;

    move-result-object v0

    .line 375685
    iget-object v1, p0, LX/297;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 375686
    iget-object v1, p0, LX/297;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v2, 0x0

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b(J)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v0

    goto :goto_0

    .line 375687
    :cond_1
    sget-object v0, Lcom/facebook/messaging/model/threads/NotificationSetting;->a:Lcom/facebook/messaging/model/threads/NotificationSetting;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/NotificationSetting;)Ljava/lang/String;
    .locals 6
    .param p1    # Lcom/facebook/messaging/model/threads/NotificationSetting;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 375688
    if-nez p1, :cond_0

    .line 375689
    const/4 v0, 0x0

    .line 375690
    :goto_0
    return-object v0

    .line 375691
    :cond_0
    sget-object v0, LX/FJh;->a:[I

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->c()LX/6fk;

    move-result-object v1

    invoke-virtual {v1}, LX/6fk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 375692
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 375693
    :pswitch_0
    iget-object v0, p0, LX/297;->c:Landroid/content/Context;

    const v1, 0x7f080392

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 375694
    :pswitch_1
    iget-object v0, p0, LX/297;->c:Landroid/content/Context;

    const v1, 0x7f080393

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 375695
    :pswitch_2
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p1, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 375696
    iget-object v1, p0, LX/297;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 375697
    iget-object v1, p0, LX/297;->c:Landroid/content/Context;

    const v2, 0x7f080394

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
