.class public LX/2pI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2pI;


# instance fields
.field public final a:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 467947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 467948
    const v0, 0x3e6b851f    # 0.23f

    const v1, 0x3ea3d70a    # 0.32f

    invoke-static {v0, v2, v1, v2}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, LX/2pI;->a:Landroid/view/animation/Interpolator;

    .line 467949
    return-void
.end method

.method public static a(LX/0QB;)LX/2pI;
    .locals 3

    .prologue
    .line 467950
    sget-object v0, LX/2pI;->b:LX/2pI;

    if-nez v0, :cond_1

    .line 467951
    const-class v1, LX/2pI;

    monitor-enter v1

    .line 467952
    :try_start_0
    sget-object v0, LX/2pI;->b:LX/2pI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 467953
    if-eqz v2, :cond_0

    .line 467954
    :try_start_1
    new-instance v0, LX/2pI;

    invoke-direct {v0}, LX/2pI;-><init>()V

    .line 467955
    move-object v0, v0

    .line 467956
    sput-object v0, LX/2pI;->b:LX/2pI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 467957
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 467958
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 467959
    :cond_1
    sget-object v0, LX/2pI;->b:LX/2pI;

    return-object v0

    .line 467960
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 467961
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;II)V
    .locals 4

    .prologue
    .line 467962
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, LX/2pI;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/7LR;

    invoke-direct {v1, p0, p1, p3}, LX/7LR;-><init>(LX/2pI;Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 467963
    return-void
.end method
