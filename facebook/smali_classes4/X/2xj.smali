.class public LX/2xj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final b:Ljava/lang/String;

.field private static volatile k:LX/2xj;


# instance fields
.field public a:Ljava/util/List;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/0oB;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D7d;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Qv;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3AW;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1CC;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 478989
    const-class v0, LX/2xj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2xj;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/D7d;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7Qv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3AW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1CC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 478978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478979
    iput-object p2, p0, LX/2xj;->c:LX/0Ot;

    .line 478980
    iput-object p3, p0, LX/2xj;->d:LX/0Ot;

    .line 478981
    iput-object p4, p0, LX/2xj;->e:LX/0Ot;

    .line 478982
    iput-object p5, p0, LX/2xj;->f:LX/0Ot;

    .line 478983
    iput-object p6, p0, LX/2xj;->g:LX/0Ot;

    .line 478984
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2xj;->a:Ljava/util/List;

    .line 478985
    iput-object p7, p0, LX/2xj;->h:LX/0Ot;

    .line 478986
    iput-object p8, p0, LX/2xj;->i:LX/0Ot;

    .line 478987
    sget-object v0, LX/2sa;->a:LX/0Tn;

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/2xj;->j:Z

    .line 478988
    return-void
.end method

.method public static a(LX/0QB;)LX/2xj;
    .locals 12

    .prologue
    .line 478876
    sget-object v0, LX/2xj;->k:LX/2xj;

    if-nez v0, :cond_1

    .line 478877
    const-class v1, LX/2xj;

    monitor-enter v1

    .line 478878
    :try_start_0
    sget-object v0, LX/2xj;->k:LX/2xj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 478879
    if-eqz v2, :cond_0

    .line 478880
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 478881
    new-instance v3, LX/2xj;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v5, 0x3840

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x383d

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1369

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1388

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x97

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2db

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x259

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, LX/2xj;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 478882
    move-object v0, v3

    .line 478883
    sput-object v0, LX/2xj;->k:LX/2xj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 478884
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 478885
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 478886
    :cond_1
    sget-object v0, LX/2xj;->k:LX/2xj;

    return-object v0

    .line 478887
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 478888
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2xj;LX/D7e;)V
    .locals 3

    .prologue
    .line 478962
    iget-object v0, p0, LX/2xj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 478963
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oB;

    .line 478964
    if-eqz v0, :cond_0

    .line 478965
    sget-object v2, LX/D7e;->STARTED:LX/D7e;

    if-ne p1, v2, :cond_1

    .line 478966
    invoke-interface {v0}, LX/0oB;->kQ_()V

    goto :goto_0

    .line 478967
    :cond_1
    sget-object v2, LX/D7e;->RESUMED:LX/D7e;

    if-ne p1, v2, :cond_2

    .line 478968
    invoke-interface {v0}, LX/0oB;->c()V

    goto :goto_0

    .line 478969
    :cond_2
    sget-object v2, LX/D7e;->PAUSED:LX/D7e;

    if-ne p1, v2, :cond_3

    .line 478970
    invoke-interface {v0}, LX/0oB;->b()V

    goto :goto_0

    .line 478971
    :cond_3
    sget-object v2, LX/D7e;->ENDED:LX/D7e;

    if-ne p1, v2, :cond_4

    .line 478972
    invoke-interface {v0}, LX/0oB;->d()V

    goto :goto_0

    .line 478973
    :cond_4
    sget-object v2, LX/D7e;->BACKGROUNDED:LX/D7e;

    if-ne p1, v2, :cond_5

    .line 478974
    invoke-interface {v0}, LX/0oB;->e()V

    goto :goto_0

    .line 478975
    :cond_5
    sget-object v2, LX/D7e;->FOREGROUNDED:LX/D7e;

    if-ne p1, v2, :cond_0

    .line 478976
    invoke-interface {v0}, LX/0oB;->f()V

    goto :goto_0

    .line 478977
    :cond_6
    return-void
.end method

.method public static l(LX/2xj;)V
    .locals 7

    .prologue
    .line 478942
    iget-boolean v0, p0, LX/2xj;->j:Z

    if-eqz v0, :cond_0

    .line 478943
    iget-object v0, p0, LX/2xj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D7d;

    iget-object v1, p0, LX/2xj;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1CC;

    const p0, 0x26d012

    const/4 v6, 0x0

    .line 478944
    const-string v2, "VH Session Status: "

    .line 478945
    invoke-virtual {v1}, LX/1CC;->i()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 478946
    invoke-virtual {v1}, LX/1CC;->k()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 478947
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Backgrounded"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 478948
    :goto_0
    new-instance v3, LX/2HB;

    iget-object v4, v0, LX/D7d;->b:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 478949
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v4

    new-array v5, v6, [J

    invoke-virtual {v4, v5}, LX/2HB;->a([J)LX/2HB;

    move-result-object v4

    const/4 v5, 0x2

    .line 478950
    iput v5, v4, LX/2HB;->j:I

    .line 478951
    move-object v4, v4

    .line 478952
    const v5, 0x1080024

    invoke-virtual {v4, v5}, LX/2HB;->a(I)LX/2HB;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    .line 478953
    iget-object v4, v1, LX/1CC;->d:Ljava/lang/String;

    move-object v4, v4

    .line 478954
    invoke-virtual {v2, v4}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/2HB;->b(Z)LX/2HB;

    .line 478955
    invoke-virtual {v3}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v2

    .line 478956
    iget-object v3, v0, LX/D7d;->a:Landroid/app/NotificationManager;

    invoke-virtual {v3, p0, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 478957
    :cond_0
    :goto_1
    return-void

    .line 478958
    :cond_1
    invoke-virtual {v1}, LX/1CC;->j()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 478959
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Paused"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 478960
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Active"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 478961
    :cond_3
    iget-object v2, v0, LX/D7d;->a:Landroid/app/NotificationManager;

    invoke-virtual {v2, p0}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0J9;ZIZ)V
    .locals 11

    .prologue
    .line 478918
    iget-object v0, p0, LX/2xj;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CC;

    .line 478919
    iget-object v8, v0, LX/1CC;->d:Ljava/lang/String;

    if-eqz v8, :cond_0

    .line 478920
    iget-object v8, v0, LX/1CC;->b:LX/03V;

    iget-object v9, v0, LX/1CC;->a:Ljava/lang/String;

    const-string v10, "Start a new session before previous end."

    invoke-virtual {v8, v9, v10}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 478921
    :cond_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v8, v8

    .line 478922
    iput-object v8, v0, LX/1CC;->d:Ljava/lang/String;

    .line 478923
    iget-object v8, v0, LX/1CC;->c:LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v8

    iput-wide v8, v0, LX/1CC;->g:J

    .line 478924
    iget-object v0, p0, LX/2xj;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 478925
    iget-object v0, p0, LX/2xj;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Qv;

    const-string v1, "START"

    invoke-virtual {v0, v1}, LX/7Qv;->a(Ljava/lang/String;)V

    .line 478926
    iget-object v0, p0, LX/2xj;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3AW;

    iget-object v0, p0, LX/2xj;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    .line 478927
    iget-object v4, v0, LX/0gh;->x:Ljava/lang/String;

    move-object v0, v4

    .line 478928
    const-string v4, "swipe"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 478929
    const-string v4, "swipe"

    .line 478930
    :goto_0
    move-object v4, v4

    .line 478931
    move-object v5, p1

    move v6, p3

    move v7, p4

    invoke-virtual/range {v1 .. v7}, LX/3AW;->a(JLjava/lang/String;LX/0J9;IZ)V

    .line 478932
    sget-object v0, LX/D7e;->STARTED:LX/D7e;

    invoke-static {p0, v0}, LX/2xj;->a(LX/2xj;LX/D7e;)V

    .line 478933
    invoke-static {p0}, LX/2xj;->l(LX/2xj;)V

    .line 478934
    return-void

    .line 478935
    :cond_1
    const-string v4, "cold_start"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 478936
    const-string v4, "cold_start"

    goto :goto_0

    .line 478937
    :cond_2
    const-string v4, "tap_top_jewel_bar"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 478938
    const-string v4, "tab"

    goto :goto_0

    .line 478939
    :cond_3
    if-eqz p2, :cond_4

    .line 478940
    const-string v4, "push"

    goto :goto_0

    .line 478941
    :cond_4
    const-string v4, "tab"

    goto :goto_0
.end method

.method public final a(LX/0oB;)V
    .locals 2

    .prologue
    .line 478915
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 478916
    iget-object v0, p0, LX/2xj;->a:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 478917
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 478990
    iget-object v0, p0, LX/2xj;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Qv;

    const-string v1, "END"

    invoke-virtual {v0, v1}, LX/7Qv;->a(Ljava/lang/String;)V

    .line 478991
    iget-object v0, p0, LX/2xj;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CC;

    invoke-virtual {v0}, LX/1CC;->g()J

    move-result-wide v2

    .line 478992
    iget-object v0, p0, LX/2xj;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AW;

    .line 478993
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/0JT;->VIDEO_HOME_SESSION_END:LX/0JT;

    iget-object v4, v4, LX/0JT;->value:Ljava/lang/String;

    invoke-direct {v1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 478994
    sget-object v4, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    sget-object v5, LX/04D;->VIDEO_HOME:LX/04D;

    iget-object v5, v5, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 478995
    sget-object v4, LX/0JS;->SESSION_DURATION:LX/0JS;

    iget-object v4, v4, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v1, v4, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 478996
    invoke-static {v0, v1}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 478997
    iget-object v1, v0, LX/3AW;->g:Landroid/util/LruCache;

    invoke-virtual {v1}, Landroid/util/LruCache;->evictAll()V

    .line 478998
    iget-object v1, v0, LX/3AW;->f:LX/04K;

    const v2, 0x1d000a

    .line 478999
    iget-object v4, v1, LX/04K;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v4, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 479000
    iget-boolean v4, v1, LX/04K;->b:Z

    if-eqz v4, :cond_2

    iget-boolean v4, v1, LX/04K;->c:Z

    if-eqz v4, :cond_2

    iget-boolean v4, v1, LX/04K;->d:Z

    if-eqz v4, :cond_2

    .line 479001
    iget-object v4, v1, LX/04K;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v5, 0x2

    invoke-interface {v4, v2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 479002
    :cond_0
    :goto_0
    iget-object v1, v0, LX/3AW;->n:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 479003
    sget-object v0, LX/D7e;->ENDED:LX/D7e;

    invoke-static {p0, v0}, LX/2xj;->a(LX/2xj;LX/D7e;)V

    .line 479004
    iget-object v0, p0, LX/2xj;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CC;

    .line 479005
    iget-object v1, v0, LX/1CC;->d:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 479006
    iget-object v1, v0, LX/1CC;->b:LX/03V;

    iget-object v2, v0, LX/1CC;->a:Ljava/lang/String;

    const-string v3, "Session is already ended."

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 479007
    :cond_1
    invoke-static {v0}, LX/1CC;->m(LX/1CC;)V

    .line 479008
    invoke-static {p0}, LX/2xj;->l(LX/2xj;)V

    .line 479009
    return-void

    .line 479010
    :cond_2
    iget-object v4, v1, LX/04K;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v5, 0x3

    invoke-interface {v4, v2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_0
.end method

.method public final b(LX/0oB;)V
    .locals 2

    .prologue
    .line 478908
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 478909
    iget-object v0, p0, LX/2xj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 478910
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 478911
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 478912
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 478913
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 478914
    :cond_1
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 478899
    iget-object v0, p0, LX/2xj;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CC;

    .line 478900
    iget-object v1, v0, LX/1CC;->d:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 478901
    iget-object v1, v0, LX/1CC;->b:LX/03V;

    iget-object v2, v0, LX/1CC;->a:Ljava/lang/String;

    const-string v3, "Pause a session before session start."

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 478902
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1CC;->e:Z

    .line 478903
    :goto_0
    sget-object v0, LX/D7e;->PAUSED:LX/D7e;

    invoke-static {p0, v0}, LX/2xj;->a(LX/2xj;LX/D7e;)V

    .line 478904
    invoke-static {p0}, LX/2xj;->l(LX/2xj;)V

    .line 478905
    return-void

    .line 478906
    :cond_1
    iget-boolean v1, v0, LX/1CC;->e:Z

    if-eqz v1, :cond_0

    .line 478907
    iget-object v1, v0, LX/1CC;->b:LX/03V;

    iget-object v2, v0, LX/1CC;->a:Ljava/lang/String;

    const-string v3, "Session is already paused."

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 478890
    iget-object v0, p0, LX/2xj;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CC;

    .line 478891
    iget-object v1, v0, LX/1CC;->d:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 478892
    iget-object v1, v0, LX/1CC;->b:LX/03V;

    iget-object v2, v0, LX/1CC;->a:Ljava/lang/String;

    const-string v3, "Resume a session before session start."

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 478893
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/1CC;->e:Z

    .line 478894
    :goto_0
    sget-object v0, LX/D7e;->RESUMED:LX/D7e;

    invoke-static {p0, v0}, LX/2xj;->a(LX/2xj;LX/D7e;)V

    .line 478895
    invoke-static {p0}, LX/2xj;->l(LX/2xj;)V

    .line 478896
    return-void

    .line 478897
    :cond_1
    iget-boolean v1, v0, LX/1CC;->e:Z

    if-nez v1, :cond_0

    .line 478898
    iget-object v1, v0, LX/1CC;->b:LX/03V;

    iget-object v2, v0, LX/1CC;->a:Ljava/lang/String;

    const-string v3, "Session is already resumed."

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 478889
    iget-object v0, p0, LX/2xj;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CC;

    invoke-virtual {v0}, LX/1CC;->i()Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 478875
    iget-object v0, p0, LX/2xj;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CC;

    invoke-virtual {v0}, LX/1CC;->k()Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 478874
    iget-object v0, p0, LX/2xj;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CC;

    invoke-virtual {v0}, LX/1CC;->j()Z

    move-result v0

    return v0
.end method
