.class public final enum LX/3Oq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3Oq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3Oq;

.field public static final ALL:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation
.end field

.field public static final CONNECTIONS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FRIEND:LX/3Oq;

.field public static final FRIENDS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation
.end field

.field public static final FRIENDS_AND_ME:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation
.end field

.field public static final FRIENDS_AND_PAGES:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ME:LX/3Oq;

.field public static final MESSAGEABLES:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PAGE:LX/3Oq;

.field public static final PAGES:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum UNMATCHED:LX/3Oq;

.field public static final USERS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum USER_CONTACT:LX/3Oq;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 560890
    new-instance v0, LX/3Oq;

    const-string v1, "ME"

    invoke-direct {v0, v1, v2}, LX/3Oq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Oq;->ME:LX/3Oq;

    .line 560891
    new-instance v0, LX/3Oq;

    const-string v1, "FRIEND"

    invoke-direct {v0, v1, v3}, LX/3Oq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Oq;->FRIEND:LX/3Oq;

    .line 560892
    new-instance v0, LX/3Oq;

    const-string v1, "USER_CONTACT"

    invoke-direct {v0, v1, v4}, LX/3Oq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Oq;->USER_CONTACT:LX/3Oq;

    .line 560893
    new-instance v0, LX/3Oq;

    const-string v1, "UNMATCHED"

    invoke-direct {v0, v1, v5}, LX/3Oq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Oq;->UNMATCHED:LX/3Oq;

    .line 560894
    new-instance v0, LX/3Oq;

    const-string v1, "PAGE"

    invoke-direct {v0, v1, v6}, LX/3Oq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Oq;->PAGE:LX/3Oq;

    .line 560895
    const/4 v0, 0x5

    new-array v0, v0, [LX/3Oq;

    sget-object v1, LX/3Oq;->ME:LX/3Oq;

    aput-object v1, v0, v2

    sget-object v1, LX/3Oq;->FRIEND:LX/3Oq;

    aput-object v1, v0, v3

    sget-object v1, LX/3Oq;->USER_CONTACT:LX/3Oq;

    aput-object v1, v0, v4

    sget-object v1, LX/3Oq;->UNMATCHED:LX/3Oq;

    aput-object v1, v0, v5

    sget-object v1, LX/3Oq;->PAGE:LX/3Oq;

    aput-object v1, v0, v6

    sput-object v0, LX/3Oq;->$VALUES:[LX/3Oq;

    .line 560896
    sget-object v0, LX/3Oq;->FRIEND:LX/3Oq;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3Oq;->FRIENDS:LX/0Px;

    .line 560897
    sget-object v0, LX/3Oq;->FRIEND:LX/3Oq;

    sget-object v1, LX/3Oq;->ME:LX/3Oq;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3Oq;->FRIENDS_AND_ME:LX/0Px;

    .line 560898
    sget-object v0, LX/3Oq;->FRIEND:LX/3Oq;

    sget-object v1, LX/3Oq;->PAGE:LX/3Oq;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3Oq;->FRIENDS_AND_PAGES:LX/0Px;

    .line 560899
    sget-object v0, LX/3Oq;->ME:LX/3Oq;

    sget-object v1, LX/3Oq;->FRIEND:LX/3Oq;

    sget-object v2, LX/3Oq;->USER_CONTACT:LX/3Oq;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3Oq;->USERS:LX/0Px;

    .line 560900
    sget-object v0, LX/3Oq;->FRIENDS_AND_PAGES:LX/0Px;

    sput-object v0, LX/3Oq;->CONNECTIONS:LX/0Px;

    .line 560901
    sget-object v0, LX/3Oq;->FRIEND:LX/3Oq;

    sget-object v1, LX/3Oq;->USER_CONTACT:LX/3Oq;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3Oq;->MESSAGEABLES:LX/0Px;

    .line 560902
    sget-object v0, LX/3Oq;->PAGE:LX/3Oq;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3Oq;->PAGES:LX/0Px;

    .line 560903
    invoke-static {}, LX/3Oq;->values()[LX/3Oq;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3Oq;->ALL:LX/0Px;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 560904
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 560905
    return-void
.end method

.method public static getFromContact(Lcom/facebook/contacts/graphql/Contact;Ljava/lang/String;)LX/3Oq;
    .locals 2

    .prologue
    .line 560906
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 560907
    sget-object v0, LX/3hL;->b:[I

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->A()LX/2RU;

    move-result-object v1

    invoke-virtual {v1}, LX/2RU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 560908
    sget-object v0, LX/3Oq;->UNMATCHED:LX/3Oq;

    :goto_0
    return-object v0

    .line 560909
    :pswitch_0
    sget-object v0, LX/3Oq;->PAGE:LX/3Oq;

    goto :goto_0

    .line 560910
    :pswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->x()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 560911
    sget-object v0, LX/3Oq;->FRIEND:LX/3Oq;

    goto :goto_0

    .line 560912
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 560913
    sget-object v0, LX/3Oq;->ME:LX/3Oq;

    goto :goto_0

    .line 560914
    :cond_1
    sget-object v0, LX/3Oq;->USER_CONTACT:LX/3Oq;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getFromDbValue(I)LX/3Oq;
    .locals 1

    .prologue
    .line 560915
    packed-switch p0, :pswitch_data_0

    .line 560916
    :pswitch_0
    sget-object v0, LX/3Oq;->UNMATCHED:LX/3Oq;

    .line 560917
    :goto_0
    return-object v0

    .line 560918
    :pswitch_1
    sget-object v0, LX/3Oq;->ME:LX/3Oq;

    goto :goto_0

    .line 560919
    :pswitch_2
    sget-object v0, LX/3Oq;->FRIEND:LX/3Oq;

    goto :goto_0

    .line 560920
    :pswitch_3
    sget-object v0, LX/3Oq;->USER_CONTACT:LX/3Oq;

    goto :goto_0

    .line 560921
    :pswitch_4
    sget-object v0, LX/3Oq;->PAGE:LX/3Oq;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/3Oq;
    .locals 1

    .prologue
    .line 560922
    const-class v0, LX/3Oq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3Oq;

    return-object v0
.end method

.method public static values()[LX/3Oq;
    .locals 1

    .prologue
    .line 560923
    sget-object v0, LX/3Oq;->$VALUES:[LX/3Oq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3Oq;

    return-object v0
.end method


# virtual methods
.method public final getDbValue()I
    .locals 2

    .prologue
    .line 560924
    sget-object v0, LX/3hL;->a:[I

    invoke-virtual {p0}, LX/3Oq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 560925
    :pswitch_0
    const/4 v0, 0x4

    .line 560926
    :goto_0
    return v0

    .line 560927
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 560928
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 560929
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 560930
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
