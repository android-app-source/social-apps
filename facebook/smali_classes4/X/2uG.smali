.class public LX/2uG;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3B5;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 475643
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 475644
    new-instance v0, LX/2uM;

    invoke-direct {v0}, LX/2uM;-><init>()V

    sput-object v0, LX/2uG;->a:LX/3B5;

    .line 475645
    :goto_0
    return-void

    .line 475646
    :cond_0
    new-instance v0, LX/3s2;

    invoke-direct {v0}, LX/3s2;-><init>()V

    sput-object v0, LX/2uG;->a:LX/3B5;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 475647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 475648
    return-void
.end method

.method public static a(Landroid/view/VelocityTracker;I)F
    .locals 1

    .prologue
    .line 475649
    sget-object v0, LX/2uG;->a:LX/3B5;

    invoke-interface {v0, p0, p1}, LX/3B5;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/VelocityTracker;I)F
    .locals 1

    .prologue
    .line 475650
    sget-object v0, LX/2uG;->a:LX/3B5;

    invoke-interface {v0, p0, p1}, LX/3B5;->b(Landroid/view/VelocityTracker;I)F

    move-result v0

    return v0
.end method
