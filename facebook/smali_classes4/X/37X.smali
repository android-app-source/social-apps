.class public final LX/37X;
.super LX/1NB;
.source ""


# instance fields
.field public a:Z

.field public final synthetic h:LX/0tc;


# direct methods
.method public constructor <init>(LX/0tc;LX/3Bw;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 501227
    iput-object p1, p0, LX/37X;->h:LX/0tc;

    .line 501228
    invoke-direct {p0, p1, p2, v0}, LX/1NB;-><init>(LX/0tc;LX/1ND;Z)V

    .line 501229
    iput-boolean v0, p0, LX/37X;->a:Z

    .line 501230
    return-void
.end method

.method private j()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 501221
    iget-boolean v0, p0, LX/37X;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1NB;->f:LX/1ND;

    check-cast v0, LX/3Bw;

    invoke-virtual {v0}, LX/3Bw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501222
    iget-object v0, p0, LX/37X;->h:LX/0tc;

    iget-object v0, v0, LX/0tc;->i:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 501223
    :goto_0
    iget-object v2, p0, LX/37X;->h:LX/0tc;

    iget-object v2, v2, LX/0tc;->h:LX/0t5;

    const/4 v3, -0x1

    iget-object v4, p0, LX/1NB;->f:LX/1ND;

    invoke-interface {v4}, LX/1ND;->a()LX/0Rf;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, LX/0t5;->a(ILjava/util/Set;I)V

    .line 501224
    iput-boolean v1, p0, LX/37X;->a:Z

    .line 501225
    :cond_0
    return-void

    .line 501226
    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/quicklog/QuickPerformanceLogger;II)I
    .locals 5
    .param p1    # Lcom/facebook/quicklog/QuickPerformanceLogger;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 501205
    iget-object v0, p0, LX/1NB;->f:LX/1ND;

    check-cast v0, LX/3Bw;

    const/4 v1, 0x0

    iput-boolean v1, v0, LX/3Bw;->b:Z

    .line 501206
    invoke-direct {p0}, LX/37X;->j()V

    .line 501207
    const/16 v0, 0x9e

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 501208
    if-eqz p1, :cond_0

    .line 501209
    if-eq p2, v4, :cond_2

    move v1, v2

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 501210
    if-eq p3, v4, :cond_3

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 501211
    invoke-interface {p1, p2, p3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 501212
    :cond_0
    const/4 v1, 0x0

    .line 501213
    sget-object v0, LX/1NE;->NETWORK:LX/1NE;

    invoke-virtual {p0, v0}, LX/1NB;->a(LX/1NE;)LX/1NB;

    move-result-object v0

    .line 501214
    :goto_2
    if-eqz v0, :cond_1

    .line 501215
    invoke-virtual {v0}, LX/1NB;->f()V

    .line 501216
    sget-object v0, LX/1NE;->NETWORK:LX/1NE;

    invoke-virtual {p0, v0}, LX/1NB;->a(LX/1NE;)LX/1NB;

    move-result-object v0

    .line 501217
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 501218
    :cond_1
    return v1

    :cond_2
    move v1, v3

    .line 501219
    goto :goto_0

    :cond_3
    move v2, v3

    .line 501220
    goto :goto_1
.end method

.method public final a(LX/1NE;)LX/1NB;
    .locals 1

    .prologue
    .line 501203
    invoke-direct {p0}, LX/37X;->j()V

    .line 501204
    invoke-super {p0, p1}, LX/1NB;->a(LX/1NE;)LX/1NB;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 501231
    iget-object v0, p0, LX/1NB;->f:LX/1ND;

    check-cast v0, LX/3Bw;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/3Bw;->b:Z

    .line 501232
    iget-object v1, p0, LX/37X;->h:LX/0tc;

    monitor-enter v1

    .line 501233
    :try_start_0
    iget-object v0, p0, LX/37X;->h:LX/0tc;

    const v2, 0x6d285aff

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 501234
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/3Bq;)V
    .locals 2

    .prologue
    .line 501198
    iget-object v0, p0, LX/1NB;->f:LX/1ND;

    check-cast v0, LX/3Bw;

    .line 501199
    iput-object p1, v0, LX/3Bw;->a:LX/3Bq;

    .line 501200
    iget-object v1, v0, LX/3Bw;->a:LX/3Bq;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/3Bw;->a:LX/3Bq;

    invoke-interface {v1}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 501201
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    iget-object p0, v0, LX/3Bw;->c:LX/0Rf;

    invoke-virtual {v1, p0}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v1

    iget-object p0, v0, LX/3Bw;->a:LX/3Bq;

    invoke-interface {p0}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v1

    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    iput-object v1, v0, LX/3Bw;->c:LX/0Rf;

    .line 501202
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    .line 501185
    iget-object v0, p0, LX/37X;->h:LX/0tc;

    iget-object v0, v0, LX/0tc;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 501186
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/37X;->e:Z

    .line 501187
    iget-object v1, p0, LX/37X;->h:LX/0tc;

    monitor-enter v1

    .line 501188
    :try_start_0
    iget-object v0, p0, LX/37X;->h:LX/0tc;

    iget-wide v2, v0, LX/0tc;->b:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, LX/0tc;->b:J

    .line 501189
    iget-object v0, p0, LX/37X;->h:LX/0tc;

    iget-wide v2, v0, LX/0tc;->b:J

    iput-wide v2, p0, LX/37X;->c:J

    .line 501190
    if-eqz p1, :cond_0

    .line 501191
    iget-object v0, p0, LX/37X;->h:LX/0tc;

    iget-object v0, v0, LX/0tc;->g:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 501192
    :cond_0
    iget-object v0, p0, LX/37X;->h:LX/0tc;

    iget-object v0, v0, LX/0tc;->f:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 501193
    if-eqz p1, :cond_1

    .line 501194
    iget-object v0, p0, LX/37X;->h:LX/0tc;

    iget-object v0, v0, LX/0tc;->m:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 501195
    iget-object v0, p0, LX/37X;->h:LX/0tc;

    iget-object v0, v0, LX/0tc;->l:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 501196
    :cond_1
    iget-object v0, p0, LX/37X;->h:LX/0tc;

    const v2, -0x4f00325a

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 501197
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final g()I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 501184
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1, v1}, LX/37X;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;II)I

    move-result v0

    return v0
.end method

.method public final h()LX/3Bq;
    .locals 1

    .prologue
    .line 501183
    iget-object v0, p0, LX/1NB;->f:LX/1ND;

    check-cast v0, LX/3Bw;

    iget-object v0, v0, LX/3Bw;->a:LX/3Bq;

    return-object v0
.end method
