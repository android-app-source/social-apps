.class public abstract LX/2GH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public b:I

.field public c:I

.field public d:LX/0d3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0d3",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public e:Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "LX/0qF",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public f:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public g:LX/2GI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0cn",
            "<TK;TV;>.WriteThroughEntry;"
        }
    .end annotation
.end field

.field public h:LX/2GI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0cn",
            "<TK;TV;>.WriteThroughEntry;"
        }
    .end annotation
.end field

.field public final synthetic i:LX/0cn;


# direct methods
.method public constructor <init>(LX/0cn;)V
    .locals 1

    .prologue
    .line 388009
    iput-object p1, p0, LX/2GH;->i:LX/0cn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388010
    iget-object v0, p1, LX/0cn;->c:[LX/0d3;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/2GH;->b:I

    .line 388011
    const/4 v0, -0x1

    iput v0, p0, LX/2GH;->c:I

    .line 388012
    invoke-direct {p0}, LX/2GH;->b()V

    .line 388013
    return-void
.end method

.method private a(LX/0qF;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 388014
    :try_start_0
    invoke-interface {p1}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 388015
    iget-object v1, p0, LX/2GH;->i:LX/0cn;

    const/4 v2, 0x0

    .line 388016
    invoke-interface {p1}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    .line 388017
    :cond_0
    :goto_0
    move-object v1, v2

    .line 388018
    if-eqz v1, :cond_1

    .line 388019
    new-instance v2, LX/2GI;

    iget-object v3, p0, LX/2GH;->i:LX/0cn;

    invoke-direct {v2, v3, v0, v1}, LX/2GI;-><init>(LX/0cn;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v2, p0, LX/2GH;->g:LX/2GI;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388020
    iget-object v0, p0, LX/2GH;->d:LX/0d3;

    invoke-virtual {v0}, LX/0d3;->b()V

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    iget-object v0, p0, LX/2GH;->d:LX/0d3;

    invoke-virtual {v0}, LX/0d3;->b()V

    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2GH;->d:LX/0d3;

    invoke-virtual {v1}, LX/0d3;->b()V

    throw v0

    .line 388021
    :cond_2
    invoke-interface {p1}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v3

    invoke-interface {v3}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v3

    .line 388022
    if-eqz v3, :cond_0

    .line 388023
    invoke-virtual {v1}, LX/0cn;->b()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v1, p1}, LX/0cn;->c(LX/0qF;)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_3
    move-object v2, v3

    .line 388024
    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 388025
    const/4 v0, 0x0

    iput-object v0, p0, LX/2GH;->g:LX/2GI;

    .line 388026
    invoke-direct {p0}, LX/2GH;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388027
    :cond_0
    :goto_0
    return-void

    .line 388028
    :cond_1
    invoke-direct {p0}, LX/2GH;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388029
    :cond_2
    iget v0, p0, LX/2GH;->b:I

    if-ltz v0, :cond_0

    .line 388030
    iget-object v0, p0, LX/2GH;->i:LX/0cn;

    iget-object v0, v0, LX/0cn;->c:[LX/0d3;

    iget v1, p0, LX/2GH;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, LX/2GH;->b:I

    aget-object v0, v0, v1

    iput-object v0, p0, LX/2GH;->d:LX/0d3;

    .line 388031
    iget-object v0, p0, LX/2GH;->d:LX/0d3;

    iget v0, v0, LX/0d3;->count:I

    if-eqz v0, :cond_2

    .line 388032
    iget-object v0, p0, LX/2GH;->d:LX/0d3;

    iget-object v0, v0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, LX/2GH;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 388033
    iget-object v0, p0, LX/2GH;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/2GH;->c:I

    .line 388034
    invoke-direct {p0}, LX/2GH;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 388035
    iget-object v0, p0, LX/2GH;->f:LX/0qF;

    if-eqz v0, :cond_1

    .line 388036
    iget-object v0, p0, LX/2GH;->f:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getNext()LX/0qF;

    move-result-object v0

    iput-object v0, p0, LX/2GH;->f:LX/0qF;

    :goto_0
    iget-object v0, p0, LX/2GH;->f:LX/0qF;

    if-eqz v0, :cond_1

    .line 388037
    iget-object v0, p0, LX/2GH;->f:LX/0qF;

    invoke-direct {p0, v0}, LX/2GH;->a(LX/0qF;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388038
    const/4 v0, 0x1

    .line 388039
    :goto_1
    return v0

    .line 388040
    :cond_0
    iget-object v0, p0, LX/2GH;->f:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getNext()LX/0qF;

    move-result-object v0

    iput-object v0, p0, LX/2GH;->f:LX/0qF;

    goto :goto_0

    .line 388041
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d()Z
    .locals 3

    .prologue
    .line 388042
    :cond_0
    iget v0, p0, LX/2GH;->c:I

    if-ltz v0, :cond_2

    .line 388043
    iget-object v0, p0, LX/2GH;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, LX/2GH;->c:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, LX/2GH;->c:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    iput-object v0, p0, LX/2GH;->f:LX/0qF;

    if-eqz v0, :cond_0

    .line 388044
    iget-object v0, p0, LX/2GH;->f:LX/0qF;

    invoke-direct {p0, v0}, LX/2GH;->a(LX/0qF;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, LX/2GH;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388045
    :cond_1
    const/4 v0, 0x1

    .line 388046
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/2GI;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0cn",
            "<TK;TV;>.WriteThroughEntry;"
        }
    .end annotation

    .prologue
    .line 388047
    iget-object v0, p0, LX/2GH;->g:LX/2GI;

    if-nez v0, :cond_0

    .line 388048
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 388049
    :cond_0
    iget-object v0, p0, LX/2GH;->g:LX/2GI;

    iput-object v0, p0, LX/2GH;->h:LX/2GI;

    .line 388050
    invoke-direct {p0}, LX/2GH;->b()V

    .line 388051
    iget-object v0, p0, LX/2GH;->h:LX/2GI;

    return-object v0
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 388052
    iget-object v0, p0, LX/2GH;->g:LX/2GI;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 388053
    iget-object v0, p0, LX/2GH;->h:LX/2GI;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 388054
    iget-object v0, p0, LX/2GH;->i:LX/0cn;

    iget-object v1, p0, LX/2GH;->h:LX/2GI;

    invoke-virtual {v1}, LX/2GI;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0cn;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 388055
    const/4 v0, 0x0

    iput-object v0, p0, LX/2GH;->h:LX/2GI;

    .line 388056
    return-void

    .line 388057
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
