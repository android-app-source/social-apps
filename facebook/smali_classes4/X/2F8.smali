.class public LX/2F8;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/compactdisk/DiskSizeCalculator;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/compactdisk/DiskSizeCalculator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 386570
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/compactdisk/DiskSizeCalculator;
    .locals 3

    .prologue
    .line 386571
    sget-object v0, LX/2F8;->a:Lcom/facebook/compactdisk/DiskSizeCalculator;

    if-nez v0, :cond_1

    .line 386572
    const-class v1, LX/2F8;

    monitor-enter v1

    .line 386573
    :try_start_0
    sget-object v0, LX/2F8;->a:Lcom/facebook/compactdisk/DiskSizeCalculator;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 386574
    if-eqz v2, :cond_0

    .line 386575
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 386576
    invoke-static {v0}, LX/2F9;->a(LX/0QB;)Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    move-result-object p0

    check-cast p0, Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    invoke-static {p0}, LX/2FB;->a(Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;)Lcom/facebook/compactdisk/DiskSizeCalculator;

    move-result-object p0

    move-object v0, p0

    .line 386577
    sput-object v0, LX/2F8;->a:Lcom/facebook/compactdisk/DiskSizeCalculator;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 386578
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 386579
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 386580
    :cond_1
    sget-object v0, LX/2F8;->a:Lcom/facebook/compactdisk/DiskSizeCalculator;

    return-object v0

    .line 386581
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 386582
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 386583
    invoke-static {p0}, LX/2F9;->a(LX/0QB;)Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    invoke-static {v0}, LX/2FB;->a(Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;)Lcom/facebook/compactdisk/DiskSizeCalculator;

    move-result-object v0

    return-object v0
.end method
