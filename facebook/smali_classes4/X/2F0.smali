.class public LX/2F0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2F0;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 386191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386192
    iput-object p1, p0, LX/2F0;->a:Landroid/content/Context;

    .line 386193
    return-void
.end method

.method public static a(LX/0QB;)LX/2F0;
    .locals 4

    .prologue
    .line 386194
    sget-object v0, LX/2F0;->b:LX/2F0;

    if-nez v0, :cond_1

    .line 386195
    const-class v1, LX/2F0;

    monitor-enter v1

    .line 386196
    :try_start_0
    sget-object v0, LX/2F0;->b:LX/2F0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 386197
    if-eqz v2, :cond_0

    .line 386198
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 386199
    new-instance p0, LX/2F0;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/2F0;-><init>(Landroid/content/Context;)V

    .line 386200
    move-object v0, p0

    .line 386201
    sput-object v0, LX/2F0;->b:LX/2F0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 386202
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 386203
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 386204
    :cond_1
    sget-object v0, LX/2F0;->b:LX/2F0;

    return-object v0

    .line 386205
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 386206
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2F0;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 386207
    const-wide/16 v2, 0x0

    .line 386208
    new-instance v5, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v1}, LX/162;-><init>(LX/0mC;)V

    .line 386209
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v4, p0, LX/2F0;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    const-string v6, "databases"

    invoke-direct {v1, v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 386210
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 386211
    array-length v1, v6

    invoke-static {v1}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v7

    .line 386212
    array-length v8, v6

    move v4, v0

    :goto_0
    if-ge v4, v8, :cond_0

    aget-object v9, v6, v4

    .line 386213
    invoke-virtual {v9}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 386214
    invoke-virtual {v9}, Ljava/io/File;->length()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    add-long/2addr v0, v2

    .line 386215
    :try_start_1
    new-instance v2, LX/2F2;

    invoke-direct {v2, v9}, LX/2F2;-><init>(Ljava/io/File;)V

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 386216
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v0

    goto :goto_0

    .line 386217
    :cond_0
    :try_start_2
    sget-object v0, LX/2F2;->c:Ljava/util/Comparator;

    invoke-static {v7, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 386218
    const/16 v0, 0x32

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 386219
    const/4 v1, 0x0

    invoke-interface {v7, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 386220
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2F2;

    .line 386221
    new-instance v10, LX/0m9;

    sget-object v11, LX/0mC;->a:LX/0mC;

    invoke-direct {v10, v11}, LX/0m9;-><init>(LX/0mC;)V

    .line 386222
    const-string v11, "name"

    iget-object v12, v0, LX/2F2;->a:Ljava/lang/String;

    invoke-virtual {v10, v11, v12}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 386223
    const-string v11, "size"

    iget-wide v12, v0, LX/2F2;->b:J

    invoke-virtual {v10, v11, v12, v13}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 386224
    move-object v0, v10

    .line 386225
    invoke-virtual {v5, v0}, LX/162;->a(LX/0lF;)LX/162;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 386226
    :catch_0
    :cond_1
    :goto_3
    const-string v0, "db_folder_size"

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386227
    const-string v0, "db_top_sizes"

    invoke-virtual {p1, v0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 386228
    return-void

    :catch_1
    move-wide v2, v0

    goto :goto_3

    :cond_2
    move-wide v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 5

    .prologue
    .line 386229
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "db_size_info"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 386230
    iput-wide p1, v1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 386231
    iput-object p3, v1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->f:Ljava/lang/String;

    .line 386232
    const-string v2, "device"

    .line 386233
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 386234
    invoke-static {p0, v1}, LX/2F0;->a(LX/2F0;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 386235
    move-object v0, v1

    .line 386236
    return-object v0
.end method
