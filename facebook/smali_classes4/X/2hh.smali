.class public LX/2hh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2hi;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2hh;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 450344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2hh;
    .locals 3

    .prologue
    .line 450345
    sget-object v0, LX/2hh;->a:LX/2hh;

    if-nez v0, :cond_1

    .line 450346
    const-class v1, LX/2hh;

    monitor-enter v1

    .line 450347
    :try_start_0
    sget-object v0, LX/2hh;->a:LX/2hh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 450348
    if-eqz v2, :cond_0

    .line 450349
    :try_start_1
    new-instance v0, LX/2hh;

    invoke-direct {v0}, LX/2hh;-><init>()V

    .line 450350
    move-object v0, v0

    .line 450351
    sput-object v0, LX/2hh;->a:LX/2hh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 450352
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 450353
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 450354
    :cond_1
    sget-object v0, LX/2hh;->a:LX/2hh;

    return-object v0

    .line 450355
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 450356
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0P1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 450357
    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->FEED_PYMK:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    const-class v1, Lcom/facebook/feedplugins/pymk/quickpromotion/QuickPromotionFeedPYMKFragment;

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    return-object v0
.end method
