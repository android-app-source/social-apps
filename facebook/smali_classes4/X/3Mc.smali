.class public final LX/3Mc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Md;


# instance fields
.field public final synthetic a:Lcom/facebook/divebar/contacts/DivebarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V
    .locals 0

    .prologue
    .line 555145
    iput-object p1, p0, LX/3Mc;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/3OQ;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 555146
    instance-of v0, p1, Lcom/facebook/user/model/User;

    if-eqz v0, :cond_0

    .line 555147
    iget-object v0, p0, LX/3Mc;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    iget-object v2, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->h:LX/3Kv;

    move-object v0, p1

    check-cast v0, Lcom/facebook/user/model/User;

    sget-object v3, LX/3OH;->SEARCH_RESULT:LX/3OH;

    iget-object v1, p0, LX/3Mc;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    iget-object v1, v1, Lcom/facebook/divebar/contacts/DivebarFragment;->U:LX/0P1;

    check-cast p1, Lcom/facebook/user/model/User;

    .line 555148
    iget-object v4, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 555149
    invoke-virtual {v1, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Lx;

    invoke-virtual {v2, v0, v3, v1}, LX/3Kv;->a(Lcom/facebook/user/model/User;LX/3OH;LX/6Lx;)LX/3OO;

    move-result-object v0

    .line 555150
    :goto_0
    return-object v0

    .line 555151
    :cond_0
    instance-of v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v0, :cond_1

    .line 555152
    iget-object v0, p0, LX/3Mc;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    iget-object v0, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->h:LX/3Kv;

    check-cast p1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    sget-object v1, LX/DAT;->SEARCH_RESULT:LX/DAT;

    invoke-virtual {v0, p1, v1}, LX/3Kv;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/DAT;)LX/DAU;

    move-result-object v0

    goto :goto_0

    .line 555153
    :cond_1
    sget-object v0, Lcom/facebook/divebar/contacts/DivebarFragment;->a:Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected rowData of type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 555154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method
