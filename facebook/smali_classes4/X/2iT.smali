.class public LX/2iT;
.super LX/2hY;
.source ""


# instance fields
.field public final c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Ck;LX/2dj;LX/2do;LX/2hZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 451724
    invoke-direct {p0, p2, p3, p4}, LX/2hY;-><init>(LX/2dj;LX/2do;LX/2hZ;)V

    .line 451725
    iput-object p1, p0, LX/2iT;->c:LX/1Ck;

    .line 451726
    return-void
.end method

.method public static b(LX/0QB;)LX/2iT;
    .locals 5

    .prologue
    .line 451727
    new-instance v4, LX/2iT;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {p0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v1

    check-cast v1, LX/2dj;

    invoke-static {p0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v2

    check-cast v2, LX/2do;

    invoke-static {p0}, LX/2hZ;->b(LX/0QB;)LX/2hZ;

    move-result-object v3

    check-cast v3, LX/2hZ;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2iT;-><init>(LX/1Ck;LX/2dj;LX/2do;LX/2hZ;)V

    .line 451728
    return-object v4
.end method


# virtual methods
.method public final a(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)V
    .locals 11

    .prologue
    .line 451729
    const-string v0, "You need to provide a valid FriendingLocation."

    invoke-static {p3, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451730
    sget-object v0, LX/84T;->a:[I

    invoke-virtual {p4}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 451731
    :goto_0
    return-void

    .line 451732
    :pswitch_0
    iget-object v0, p0, LX/2hY;->a:LX/2dj;

    iget-object v1, p3, LX/2h7;->removeFriendRef:LX/2hB;

    invoke-virtual {v0, p1, p2, v1}, LX/2dj;->a(JLX/2hB;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 451733
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 451734
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "REMOVE_FRIEND"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 451735
    :goto_1
    iget-object v9, p0, LX/2iT;->c:LX/1Ck;

    new-instance v10, LX/84O;

    invoke-direct {v10, p0, v1}, LX/84O;-><init>(LX/2iT;Lcom/google/common/util/concurrent/ListenableFuture;)V

    new-instance v1, LX/84P;

    move-object v2, p0

    move-object/from16 v3, p5

    move-wide v4, p1

    move-object v7, p4

    move-object v8, p3

    invoke-direct/range {v1 .. v8}, LX/84P;-><init>(LX/2iT;LX/5P5;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/2h7;)V

    invoke-virtual {v9, v0, v10, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0

    .line 451736
    :pswitch_1
    iget-object v1, p0, LX/2hY;->a:LX/2dj;

    iget-object v4, p3, LX/2h7;->friendRequestHowFound:LX/2h8;

    iget-object v5, p3, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    const/4 v6, 0x0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, LX/2dj;->b(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 451737
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 451738
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "ADD_FRIEND"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 451739
    :pswitch_2
    iget-object v0, p0, LX/2hY;->a:LX/2dj;

    sget-object v1, LX/2na;->CONFIRM:LX/2na;

    iget-object v2, p3, LX/2h7;->friendRequestResponseRef:LX/2hA;

    invoke-virtual {v0, p1, p2, v1, v2}, LX/2dj;->a(JLX/2na;LX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 451740
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 451741
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "ACCEPT_FRIEND"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 451742
    :pswitch_3
    iget-object v0, p0, LX/2hY;->a:LX/2dj;

    iget-object v1, p3, LX/2h7;->friendRequestCancelRef:LX/2h9;

    invoke-virtual {v0, p1, p2, v1}, LX/2dj;->a(JLX/2h9;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 451743
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 451744
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "CANCEL_REQUEST"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(JLjava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 7
    .param p4    # LX/2h7;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 451745
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v1 .. v6}, LX/2iT;->a(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)V

    .line 451746
    return-void
.end method
