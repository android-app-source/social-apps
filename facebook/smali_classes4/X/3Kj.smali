.class public LX/3Kj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HL",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3Ki;


# direct methods
.method public constructor <init>(LX/3Ki;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 548395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 548396
    iput-object p1, p0, LX/3Kj;->a:LX/3Ki;

    .line 548397
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/4VT;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 548398
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;

    const/4 v0, 0x0

    .line 548399
    invoke-static {p1}, LX/6AF;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;)LX/0Px;

    move-result-object v1

    .line 548400
    if-nez v1, :cond_1

    .line 548401
    :cond_0
    :goto_0
    return-object v0

    .line 548402
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;->k()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;

    move-result-object v2

    invoke-static {v2}, LX/6AF;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;)Ljava/lang/String;

    move-result-object v2

    .line 548403
    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 548404
    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v0

    .line 548405
    if-nez v0, :cond_2

    .line 548406
    const/4 v3, 0x0

    .line 548407
    :goto_1
    move-object v0, v3

    .line 548408
    invoke-static {v2, v0, v1}, LX/3Ki;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;LX/0Px;)LX/6A8;

    move-result-object v0

    goto :goto_0

    .line 548409
    :cond_2
    new-instance v5, LX/4Vu;

    invoke-direct {v5}, LX/4Vu;-><init>()V

    .line 548410
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->d()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 548411
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 548412
    const/4 v3, 0x0

    move v4, v3

    :goto_2
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_3

    .line 548413
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel;

    .line 548414
    if-nez v3, :cond_5

    .line 548415
    const/4 v7, 0x0

    .line 548416
    :goto_3
    move-object v3, v7

    .line 548417
    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548418
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 548419
    :cond_3
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 548420
    iput-object v3, v5, LX/4Vu;->d:LX/0Px;

    .line 548421
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 548422
    iput-object v3, v5, LX/4Vu;->q:Ljava/lang/String;

    .line 548423
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->c()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    move-result-object v3

    .line 548424
    if-nez v3, :cond_13

    .line 548425
    const/4 v4, 0x0

    .line 548426
    :goto_4
    move-object v3, v4

    .line 548427
    iput-object v3, v5, LX/4Vu;->x:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 548428
    invoke-virtual {v5}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v3

    goto :goto_1

    .line 548429
    :cond_5
    new-instance v7, LX/39x;

    invoke-direct {v7}, LX/39x;-><init>()V

    .line 548430
    invoke-virtual {v3}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    move-result-object v8

    const/4 p0, 0x0

    .line 548431
    if-nez v8, :cond_6

    .line 548432
    const/4 v9, 0x0

    .line 548433
    :goto_5
    move-object v8, v9

    .line 548434
    iput-object v8, v7, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 548435
    invoke-virtual {v7}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v7

    goto :goto_3

    .line 548436
    :cond_6
    new-instance p1, LX/4XR;

    invoke-direct {p1}, LX/4XR;-><init>()V

    .line 548437
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->d()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    .line 548438
    iput-object v9, p1, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 548439
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->e()Z

    move-result v9

    .line 548440
    iput-boolean v9, p1, LX/4XR;->bc:Z

    .line 548441
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->bA_()Z

    move-result v9

    .line 548442
    iput-boolean v9, p1, LX/4XR;->bA:Z

    .line 548443
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->b()LX/0Px;

    move-result-object v9

    if-eqz v9, :cond_8

    .line 548444
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    move v10, p0

    .line 548445
    :goto_6
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->b()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    if-ge v10, v9, :cond_7

    .line 548446
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->b()LX/0Px;

    move-result-object v9

    invoke-virtual {v9, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    invoke-static {v9}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548447
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto :goto_6

    .line 548448
    :cond_7
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    .line 548449
    iput-object v9, p1, LX/4XR;->cm:LX/0Px;

    .line 548450
    :cond_8
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->bB_()LX/0Px;

    move-result-object v9

    if-eqz v9, :cond_a

    .line 548451
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    move v10, p0

    .line 548452
    :goto_7
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->bB_()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    if-ge v10, v9, :cond_9

    .line 548453
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->bB_()LX/0Px;

    move-result-object v9

    invoke-virtual {v9, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    invoke-static {v9}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548454
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto :goto_7

    .line 548455
    :cond_9
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    .line 548456
    iput-object v9, p1, LX/4XR;->cn:LX/0Px;

    .line 548457
    :cond_a
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->j()Ljava/lang/String;

    move-result-object v9

    .line 548458
    iput-object v9, p1, LX/4XR;->fO:Ljava/lang/String;

    .line 548459
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->k()LX/0Px;

    move-result-object v9

    if-eqz v9, :cond_c

    .line 548460
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    move v10, p0

    .line 548461
    :goto_8
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->k()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    if-ge v10, v9, :cond_b

    .line 548462
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->k()LX/0Px;

    move-result-object v9

    invoke-virtual {v9, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    invoke-static {v9}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;)Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548463
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto :goto_8

    .line 548464
    :cond_b
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    .line 548465
    iput-object v9, p1, LX/4XR;->hE:LX/0Px;

    .line 548466
    :cond_c
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->l()LX/0Px;

    move-result-object v9

    if-eqz v9, :cond_e

    .line 548467
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    move v10, p0

    .line 548468
    :goto_9
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->l()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    if-ge v10, v9, :cond_d

    .line 548469
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->l()LX/0Px;

    move-result-object v9

    invoke-virtual {v9, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;

    invoke-static {v9}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;)Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548470
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto :goto_9

    .line 548471
    :cond_d
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    .line 548472
    iput-object v9, p1, LX/4XR;->jA:LX/0Px;

    .line 548473
    :cond_e
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->c()LX/0Px;

    move-result-object v9

    if-eqz v9, :cond_10

    .line 548474
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    move v10, p0

    .line 548475
    :goto_a
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->c()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    if-ge v10, v9, :cond_f

    .line 548476
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->c()LX/0Px;

    move-result-object v9

    invoke-virtual {v9, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    invoke-static {v9}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548477
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto :goto_a

    .line 548478
    :cond_f
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    .line 548479
    iput-object v9, p1, LX/4XR;->jB:LX/0Px;

    .line 548480
    :cond_10
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->m()LX/0Px;

    move-result-object v9

    if-eqz v9, :cond_12

    .line 548481
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 548482
    :goto_b
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->m()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    if-ge p0, v9, :cond_11

    .line 548483
    invoke-virtual {v8}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->m()LX/0Px;

    move-result-object v9

    invoke-virtual {v9, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    invoke-static {v9}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v9

    invoke-virtual {v10, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548484
    add-int/lit8 p0, p0, 0x1

    goto :goto_b

    .line 548485
    :cond_11
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    .line 548486
    iput-object v9, p1, LX/4XR;->jC:LX/0Px;

    .line 548487
    :cond_12
    invoke-virtual {p1}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v9

    goto/16 :goto_5

    .line 548488
    :cond_13
    new-instance v4, LX/3dM;

    invoke-direct {v4}, LX/3dM;-><init>()V

    .line 548489
    invoke-virtual {v3}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 548490
    iput-object v6, v4, LX/3dM;->D:Ljava/lang/String;

    .line 548491
    invoke-virtual {v4}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    goto/16 :goto_4
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 548492
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 548493
    const/4 v0, 0x0

    return-object v0
.end method
