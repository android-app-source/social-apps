.class public LX/2Qs;
.super LX/2QZ;
.source ""


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/2Qt;

.field private final d:LX/0en;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2Qt;LX/0en;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409006
    const-string v0, "platform_copy_platform_app_content"

    invoke-direct {p0, v0}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 409007
    iput-object p1, p0, LX/2Qs;->b:Landroid/content/Context;

    .line 409008
    iput-object p2, p0, LX/2Qs;->c:LX/2Qt;

    .line 409009
    iput-object p3, p0, LX/2Qs;->d:LX/0en;

    .line 409010
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 409011
    iget-object v0, p0, LX/2Qs;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 409012
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 409013
    const-string v1, "platform_copy_platform_app_content_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;

    .line 409014
    iget-object v1, v0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->b:Ljava/util/ArrayList;

    move-object v3, v1

    .line 409015
    iget-object v1, v0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->a:Ljava/lang/String;

    move-object v4, v1

    .line 409016
    iget-object v1, v0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;->c:Ljava/lang/String;

    move-object v5, v1

    .line 409017
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 409018
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v7, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 409019
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 409020
    iget-object v9, p0, LX/2Qs;->c:LX/2Qt;

    invoke-virtual {v9, v4, v8}, LX/2Qt;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v8

    .line 409021
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v9

    .line 409022
    iget-object v10, p0, LX/2Qs;->d:LX/0en;

    invoke-virtual {v10, v9, v8}, LX/0en;->a(Ljava/io/InputStream;Ljava/io/File;)V

    .line 409023
    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 409024
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 409025
    :cond_0
    invoke-static {v6}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method
