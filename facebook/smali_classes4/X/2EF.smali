.class public LX/2EF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Uh;

.field public final b:Landroid/content/Context;

.field public final c:Landroid/os/Handler;

.field public final d:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final e:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/facebook/quicklog/QuickPerformanceLogger;Ljava/util/concurrent/ScheduledExecutorService;LX/0Uh;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 385105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385106
    iput-object p1, p0, LX/2EF;->b:Landroid/content/Context;

    .line 385107
    iput-object p2, p0, LX/2EF;->c:Landroid/os/Handler;

    .line 385108
    iput-object p3, p0, LX/2EF;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 385109
    iput-object p4, p0, LX/2EF;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 385110
    iput-object p5, p0, LX/2EF;->a:LX/0Uh;

    .line 385111
    return-void
.end method
