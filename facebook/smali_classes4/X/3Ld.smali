.class public LX/3Ld;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/3Ld;


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private final b:LX/2RE;

.field private final c:LX/3LP;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Lv;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2RQ;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;LX/2RE;LX/3LP;LX/0Ot;LX/2RQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "LX/2RE;",
            "LX/3LP;",
            "LX/0Ot",
            "<",
            "LX/3Lv;",
            ">;",
            "LX/2RQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 551630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 551631
    iput-object p1, p0, LX/3Ld;->a:Landroid/content/ContentResolver;

    .line 551632
    iput-object p2, p0, LX/3Ld;->b:LX/2RE;

    .line 551633
    iput-object p3, p0, LX/3Ld;->c:LX/3LP;

    .line 551634
    iput-object p4, p0, LX/3Ld;->d:LX/0Ot;

    .line 551635
    iput-object p5, p0, LX/3Ld;->e:LX/2RQ;

    .line 551636
    return-void
.end method

.method public static a(LX/0QB;)LX/3Ld;
    .locals 9

    .prologue
    .line 551584
    sget-object v0, LX/3Ld;->f:LX/3Ld;

    if-nez v0, :cond_1

    .line 551585
    const-class v1, LX/3Ld;

    monitor-enter v1

    .line 551586
    :try_start_0
    sget-object v0, LX/3Ld;->f:LX/3Ld;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 551587
    if-eqz v2, :cond_0

    .line 551588
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 551589
    new-instance v3, LX/3Ld;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v4

    check-cast v4, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/2RE;->a(LX/0QB;)LX/2RE;

    move-result-object v5

    check-cast v5, LX/2RE;

    invoke-static {v0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v6

    check-cast v6, LX/3LP;

    const/16 v7, 0xda1

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v8

    check-cast v8, LX/2RQ;

    invoke-direct/range {v3 .. v8}, LX/3Ld;-><init>(Landroid/content/ContentResolver;LX/2RE;LX/3LP;LX/0Ot;LX/2RQ;)V

    .line 551590
    move-object v0, v3

    .line 551591
    sput-object v0, LX/3Ld;->f:LX/3Ld;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551592
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 551593
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551594
    :cond_1
    sget-object v0, LX/3Ld;->f:LX/3Ld;

    return-object v0

    .line 551595
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 551596
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/3Ld;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 551621
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 551622
    iget-object v0, p0, LX/3Ld;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/3Ld;->b:LX/2RE;

    iget-object v1, v1, LX/2RE;->d:LX/2RM;

    iget-object v1, v1, LX/2RM;->a:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "fbid"

    aput-object v4, v2, v5

    const-string v4, "display_order"

    aput-object v4, v2, v7

    const-string v5, "display_order ASC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 551623
    if-eqz v1, :cond_1

    .line 551624
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551625
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 551626
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 551627
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v6, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 551628
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 551629
    :cond_1
    return-object v6
.end method

.method private static c(LX/3Ld;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 551612
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 551613
    iget-object v0, p0, LX/3Ld;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/3Ld;->b:LX/2RE;

    iget-object v1, v1, LX/2RE;->e:LX/2RN;

    iget-object v1, v1, LX/2RN;->a:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "raw_phone_number"

    aput-object v4, v2, v5

    const-string v4, "display_order"

    aput-object v4, v2, v7

    const-string v5, "display_order ASC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 551614
    if-eqz v1, :cond_1

    .line 551615
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551616
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 551617
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    .line 551618
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v6, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 551619
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 551620
    :cond_1
    return-object v6
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551597
    invoke-static {p0}, LX/3Ld;->b(LX/3Ld;)Ljava/util/Map;

    move-result-object v1

    .line 551598
    iget-object v0, p0, LX/3Ld;->c:LX/3LP;

    iget-object v2, p0, LX/3Ld;->e:LX/2RQ;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2RQ;->b(Ljava/util/Collection;)LX/2RR;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/3LP;->b(LX/2RR;)Ljava/util/List;

    move-result-object v2

    .line 551599
    invoke-static {p0}, LX/3Ld;->c(LX/3Ld;)Ljava/util/Map;

    move-result-object v3

    .line 551600
    iget-object v0, p0, LX/3Ld;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Lv;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    sget-object v5, LX/3Oo;->PEOPLE_TAB:LX/3Oo;

    const/4 v11, 0x0

    .line 551601
    invoke-static {v0}, LX/3Lv;->a(LX/3Lv;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 551602
    sget-object v6, LX/0Q7;->a:LX/0Px;

    move-object v6, v6

    .line 551603
    :goto_0
    move-object v0, v6

    .line 551604
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 551605
    invoke-interface {v4, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 551606
    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 551607
    new-instance v0, LX/3Op;

    invoke-direct {v0, v1, v3}, LX/3Op;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 551608
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 551609
    :cond_0
    const/4 v6, 0x2

    new-array v6, v6, [LX/0ux;

    sget-object v7, LX/3Lv;->d:LX/0ux;

    aput-object v7, v6, v11

    const/4 v7, 0x1

    const-string v8, "data1"

    invoke-static {v8, v4}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v8

    .line 551610
    sget-object v7, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const/16 v9, 0x7d0

    const-string v10, "contact_id"

    move-object v6, v0

    move-object v12, v5

    invoke-static/range {v6 .. v12}, LX/3Lv;->b(LX/3Lv;Landroid/net/Uri;LX/0ux;ILjava/lang/String;ZLX/3Oo;)Ljava/util/List;

    move-result-object v6

    .line 551611
    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    goto :goto_0
.end method
