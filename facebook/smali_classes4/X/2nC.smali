.class public LX/2nC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static final b:Ljava/lang/String;

.field private static volatile o:LX/2nC;


# instance fields
.field public final c:LX/0Zb;

.field private final d:LX/0ka;

.field private final e:LX/0kb;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Uh;

.field private final h:LX/0YR;

.field public i:Lcom/facebook/analytics/logger/HoneyClientEvent;

.field private j:J

.field public k:Landroid/content/Context;

.field private l:Landroid/os/Handler;

.field public m:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 463555
    const-class v0, LX/2nC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2nC;->b:Ljava/lang/String;

    .line 463556
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "fb_android"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "browser_alive_marker"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2nC;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0ka;LX/0kb;Landroid/content/Context;Landroid/os/Handler;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Uh;LX/0YR;)V
    .locals 2
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0ka;",
            "LX/0kb;",
            "Landroid/content/Context;",
            "Landroid/os/Handler;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0YR;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463544
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/2nC;->j:J

    .line 463545
    iput-object p1, p0, LX/2nC;->c:LX/0Zb;

    .line 463546
    iput-object p2, p0, LX/2nC;->d:LX/0ka;

    .line 463547
    iput-object p3, p0, LX/2nC;->e:LX/0kb;

    .line 463548
    iput-object p4, p0, LX/2nC;->k:Landroid/content/Context;

    .line 463549
    iput-object p5, p0, LX/2nC;->l:Landroid/os/Handler;

    .line 463550
    iput-object p6, p0, LX/2nC;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 463551
    iput-object p7, p0, LX/2nC;->f:LX/0Or;

    .line 463552
    iput-object p8, p0, LX/2nC;->g:LX/0Uh;

    .line 463553
    iput-object p9, p0, LX/2nC;->h:LX/0YR;

    .line 463554
    return-void
.end method

.method public static a(LX/0QB;)LX/2nC;
    .locals 13

    .prologue
    .line 463530
    sget-object v0, LX/2nC;->o:LX/2nC;

    if-nez v0, :cond_1

    .line 463531
    const-class v1, LX/2nC;

    monitor-enter v1

    .line 463532
    :try_start_0
    sget-object v0, LX/2nC;->o:LX/2nC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 463533
    if-eqz v2, :cond_0

    .line 463534
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 463535
    new-instance v3, LX/2nC;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v5

    check-cast v5, LX/0ka;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v6

    check-cast v6, LX/0kb;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v10, 0x259

    invoke-static {v0, v10}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {v0}, LX/0YQ;->a(LX/0QB;)LX/0YQ;

    move-result-object v12

    check-cast v12, LX/0YR;

    invoke-direct/range {v3 .. v12}, LX/2nC;-><init>(LX/0Zb;LX/0ka;LX/0kb;Landroid/content/Context;Landroid/os/Handler;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Uh;LX/0YR;)V

    .line 463536
    move-object v0, v3

    .line 463537
    sput-object v0, LX/2nC;->o:LX/2nC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463538
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 463539
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 463540
    :cond_1
    sget-object v0, LX/2nC;->o:LX/2nC;

    return-object v0

    .line 463541
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 463542
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 463526
    if-eqz p0, :cond_1

    const-string v1, "_ads"

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 463527
    :cond_0
    :goto_0
    return v0

    .line 463528
    :cond_1
    if-eqz p1, :cond_2

    const-string v1, "\\\"ei\\\":"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 463529
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/2nC;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 463504
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, LX/2nC;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    const-string v1, "browser_open_url_logger"

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 463505
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 463506
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_1

    .line 463507
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 463508
    :cond_0
    :goto_0
    return-void

    .line 463509
    :cond_1
    const/4 v0, 0x0

    .line 463510
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    move-result v1

    if-nez v1, :cond_2

    .line 463511
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 463512
    :catch_0
    :goto_1
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 463513
    if-eqz v0, :cond_0

    .line 463514
    :try_start_2
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 463515
    :catch_1
    goto :goto_0

    .line 463516
    :cond_2
    :try_start_3
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 463517
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 463518
    :try_start_4
    invoke-virtual {v1, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 463519
    :try_start_5
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 463520
    :catch_2
    goto :goto_0

    .line 463521
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_2
    if-eqz v1, :cond_3

    .line 463522
    :try_start_6
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 463523
    :cond_3
    :goto_3
    throw v0

    :catch_3
    goto :goto_3

    .line 463524
    :catchall_1
    move-exception v0

    goto :goto_2

    :catchall_2
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_2

    .line 463525
    :catch_4
    move-object v0, v1

    goto :goto_1
.end method

.method public static e(LX/2nC;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 463488
    new-instance v3, Ljava/io/File;

    iget-object v0, p0, LX/2nC;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "browser_open_url_logger"

    invoke-direct {v3, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 463489
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 463490
    :goto_0
    return-object v0

    .line 463491
    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 463492
    new-instance v2, Ljava/io/ObjectInputStream;

    invoke-direct {v2, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463493
    :try_start_1
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 463494
    :try_start_2
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    goto :goto_0

    .line 463495
    :catch_1
    move-object v0, v1

    :goto_1
    const/4 v2, 0x1

    :try_start_3
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v3

    aput-object v3, v2, v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 463496
    if-eqz v0, :cond_1

    .line 463497
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :cond_1
    :goto_2
    move-object v0, v1

    .line 463498
    goto :goto_0

    .line 463499
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_2

    .line 463500
    :try_start_5
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 463501
    :cond_2
    :goto_4
    throw v0

    :catch_2
    goto :goto_2

    :catch_3
    goto :goto_4

    .line 463502
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catchall_2
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_3

    .line 463503
    :catch_4
    move-object v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 463486
    iget-object v0, p0, LX/2nC;->l:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$2;

    invoke-direct {v1, p0}, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$2;-><init>(LX/2nC;)V

    const v2, -0x2d724c72

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 463487
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;JJJJJJZZZLjava/util/Map;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p18    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 463455
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 463456
    :goto_0
    monitor-exit p0

    return-void

    .line 463457
    :cond_0
    :try_start_1
    invoke-virtual {p0}, LX/2nC;->a()V

    .line 463458
    if-eqz p1, :cond_1

    iget-object v2, p0, LX/2nC;->g:LX/0Uh;

    const/16 v3, 0xcd

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 463459
    iget-object v2, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "session_id"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463460
    :cond_1
    iget-object v2, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "current_url"

    invoke-virtual {v2, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463461
    iget-object v2, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "load_starts_ms"

    invoke-virtual {v2, v3, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463462
    iget-object v2, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "response_end_ms"

    invoke-virtual {v2, v3, p5, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463463
    iget-object v2, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "dom_content_loaded_ms"

    invoke-virtual {v2, v3, p7, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463464
    iget-object v2, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "load_event_end_ms"

    invoke-virtual {v2, v3, p9, p10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463465
    iget-object v2, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "scroll_ready_ms"

    move-wide/from16 v0, p11

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463466
    const-wide/16 v2, 0x0

    cmp-long v2, p13, v2

    if-eqz v2, :cond_2

    .line 463467
    iget-object v2, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "first_url_error"

    move-wide/from16 v0, p13

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463468
    :cond_2
    iget-object v3, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "refresh"

    if-eqz p15, :cond_4

    const-string v2, "1"

    :goto_1
    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463469
    iget-object v3, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "exit"

    if-eqz p16, :cond_5

    const-string v2, "1"

    :goto_2
    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463470
    iget-object v3, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "amp"

    if-eqz p17, :cond_6

    const-string v2, "1"

    :goto_3
    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463471
    if-eqz p18, :cond_3

    .line 463472
    iget-object v2, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object/from16 v0, p18

    invoke-virtual {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463473
    :cond_3
    iget-object v3, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "warmup"

    if-eqz p19, :cond_7

    const-string v2, "1"

    :goto_4
    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463474
    iget-boolean v2, p0, LX/2nC;->n:Z

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/2nC;->g:LX/0Uh;

    const/16 v3, 0xc9

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 463475
    iget-object v2, p0, LX/2nC;->c:LX/0Zb;

    iget-object v3, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v2, v3}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 463476
    :goto_5
    invoke-static/range {p19 .. p19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 463477
    const/4 v2, 0x0

    iput-object v2, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463478
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/2nC;->n:Z

    .line 463479
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/2nC;->j:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 463480
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 463481
    :cond_4
    :try_start_2
    const-string v2, "0"

    goto :goto_1

    .line 463482
    :cond_5
    const-string v2, "0"

    goto :goto_2

    .line 463483
    :cond_6
    const-string v2, "0"

    goto :goto_3

    .line 463484
    :cond_7
    const-string v2, "0"

    goto :goto_4

    .line 463485
    :cond_8
    iget-object v2, p0, LX/2nC;->c:LX/0Zb;

    iget-object v3, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 463420
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, LX/2nC;->j:J

    .line 463421
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "fb4a_iab_open_url"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463422
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 463423
    const-string v2, "initial_url"

    invoke-interface {v3, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463424
    const-string v2, "handler_time"

    iget-wide v4, p0, LX/2nC;->j:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463425
    if-eqz p2, :cond_6

    .line 463426
    const-string v2, "prefetch"

    invoke-interface {v3, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463427
    :goto_0
    const-string v2, "click_source"

    invoke-interface {v3, v2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463428
    const-string v2, "show_preview"

    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463429
    if-eqz p8, :cond_0

    .line 463430
    const-string v2, "no_preview_reason"

    move-object/from16 v0, p8

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463431
    :cond_0
    invoke-static {p3, p4}, LX/2nC;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 463432
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/2nC;->n:Z

    .line 463433
    if-eqz p4, :cond_7

    .line 463434
    const-string v2, "tracking_codes"

    invoke-interface {v3, v2, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463435
    :cond_1
    :goto_1
    if-eqz p5, :cond_2

    .line 463436
    const-string v2, "browser_type"

    const-string v4, "1"

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463437
    :cond_2
    iget-object v2, p0, LX/2nC;->e:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    if-nez v2, :cond_8

    .line 463438
    const-string v2, "client_network"

    const-string v4, "offline"

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463439
    :goto_2
    iget-object v2, p0, LX/2nC;->h:LX/0YR;

    invoke-interface {v2}, LX/0YR;->a()LX/1hM;

    move-result-object v2

    .line 463440
    if-eqz v2, :cond_3

    .line 463441
    const-string v4, "rtt_avg"

    invoke-virtual {v2}, LX/1hM;->a()Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463442
    :cond_3
    if-lez p9, :cond_4

    .line 463443
    const-string v2, "prefetched_cookies"

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463444
    :cond_4
    if-eqz p10, :cond_5

    .line 463445
    const-string v2, "browser_metrics_join_key"

    move-object/from16 v0, p10

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463446
    :cond_5
    iget-object v2, p0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463447
    iget-object v2, p0, LX/2nC;->l:Landroid/os/Handler;

    new-instance v4, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;

    invoke-direct {v4, p0, p5, v3}, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;-><init>(LX/2nC;ZLjava/util/Map;)V

    const v3, 0x1d86ee04

    invoke-static {v2, v4, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463448
    monitor-exit p0

    return-void

    .line 463449
    :cond_6
    :try_start_1
    const-string v2, "prefetch_code"

    invoke-interface {v3, v2, p7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 463450
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 463451
    :cond_7
    :try_start_2
    iget-object v2, p0, LX/2nC;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v4, "android_browser_ads_missing_tracking_code"

    const-string v5, "Click source is %s but tracking code is null!"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p3, v6, v7

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/IllegalArgumentException;

    invoke-direct {v6}, Ljava/lang/IllegalArgumentException;-><init>()V

    invoke-virtual {v2, v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 463452
    :cond_8
    iget-object v2, p0, LX/2nC;->d:LX/0ka;

    invoke-virtual {v2}, LX/0ka;->b()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/2nC;->e:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->h()Z

    move-result v2

    if-nez v2, :cond_9

    .line 463453
    const-string v2, "client_network"

    const-string v4, "wifi"

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 463454
    :cond_9
    const-string v2, "client_network"

    const-string v4, "mobile"

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method
