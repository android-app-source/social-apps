.class public LX/2fh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2fi;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2fh;


# instance fields
.field private final a:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/03V;

.field private final c:LX/2fj;


# direct methods
.method public constructor <init>(LX/1Ck;LX/03V;LX/2fj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 447259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447260
    iput-object p1, p0, LX/2fh;->a:LX/1Ck;

    .line 447261
    iput-object p2, p0, LX/2fh;->b:LX/03V;

    .line 447262
    iput-object p3, p0, LX/2fh;->c:LX/2fj;

    .line 447263
    return-void
.end method

.method public static a(LX/0QB;)LX/2fh;
    .locals 6

    .prologue
    .line 447264
    sget-object v0, LX/2fh;->d:LX/2fh;

    if-nez v0, :cond_1

    .line 447265
    const-class v1, LX/2fh;

    monitor-enter v1

    .line 447266
    :try_start_0
    sget-object v0, LX/2fh;->d:LX/2fh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 447267
    if-eqz v2, :cond_0

    .line 447268
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 447269
    new-instance p0, LX/2fh;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/2fj;->b(LX/0QB;)LX/2fj;

    move-result-object v5

    check-cast v5, LX/2fj;

    invoke-direct {p0, v3, v4, v5}, LX/2fh;-><init>(LX/1Ck;LX/03V;LX/2fj;)V

    .line 447270
    move-object v0, p0

    .line 447271
    sput-object v0, LX/2fh;->d:LX/2fh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447272
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 447273
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 447274
    :cond_1
    sget-object v0, LX/2fh;->d:LX/2fh;

    return-object v0

    .line 447275
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 447276
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private c(LX/2fp;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2fp;",
            "LX/1oP;",
            "Lcom/facebook/story/UpdateTimelineAppCollectionParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 447277
    iget-object v0, p0, LX/2fh;->c:LX/2fj;

    invoke-virtual {v0, p3}, LX/2fj;->a(Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 447278
    iget-object v1, p0, LX/2fh;->a:LX/1Ck;

    .line 447279
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "task_key_update_timeline_collection_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 447280
    iget-object v3, p3, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 447281
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 447282
    iget-object v3, p3, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    move-object v3, v3

    .line 447283
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 447284
    iget-object v3, p3, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->i:Ljava/lang/String;

    move-object v3, v3

    .line 447285
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 447286
    new-instance v3, LX/ADx;

    invoke-direct {v3, p0, p1, p2, p3}, LX/ADx;-><init>(LX/2fh;LX/2fp;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 447287
    return-object v0
.end method


# virtual methods
.method public final a(LX/2fp;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2fp;",
            "LX/1oP;",
            "Lcom/facebook/story/UpdateTimelineAppCollectionParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 447288
    new-instance v0, LX/5vM;

    invoke-direct {v0, p3}, LX/5vM;-><init>(Lcom/facebook/story/UpdateTimelineAppCollectionParams;)V

    sget-object v1, LX/5vL;->ADD:LX/5vL;

    .line 447289
    iput-object v1, v0, LX/5vM;->c:LX/5vL;

    .line 447290
    move-object v0, v0

    .line 447291
    invoke-virtual {v0}, LX/5vM;->a()Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    move-result-object v0

    .line 447292
    invoke-direct {p0, p1, p2, v0}, LX/2fh;->c(LX/2fp;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/2fp;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2fp;",
            "LX/1oP;",
            "Lcom/facebook/story/UpdateTimelineAppCollectionParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 447293
    new-instance v0, LX/5vM;

    invoke-direct {v0, p3}, LX/5vM;-><init>(Lcom/facebook/story/UpdateTimelineAppCollectionParams;)V

    sget-object v1, LX/5vL;->REMOVE:LX/5vL;

    .line 447294
    iput-object v1, v0, LX/5vM;->c:LX/5vL;

    .line 447295
    move-object v0, v0

    .line 447296
    invoke-virtual {v0}, LX/5vM;->a()Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    move-result-object v0

    .line 447297
    invoke-direct {p0, p1, p2, v0}, LX/2fh;->c(LX/2fp;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
