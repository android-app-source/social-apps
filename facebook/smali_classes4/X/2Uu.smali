.class public LX/2Uu;
.super LX/1Eg;
.source ""

# interfaces
.implements LX/1Eh;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final i:Ljava/lang/Object;


# instance fields
.field private final a:J

.field private final b:LX/0Uh;

.field private final c:LX/0TD;

.field private final d:LX/0SG;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6cy;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FOp;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 416873
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2Uu;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/0TD;LX/0SG;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 2
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerSyncEnabled;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/login/annotations/IsLoggedOutRemotely;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0TD;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/6cy;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416874
    const-string v0, "SYNC_FETCH_USER_INFO"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 416875
    const-wide/32 v0, 0xa4cb800

    iput-wide v0, p0, LX/2Uu;->a:J

    .line 416876
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416877
    iput-object v0, p0, LX/2Uu;->h:LX/0Ot;

    .line 416878
    iput-object p1, p0, LX/2Uu;->b:LX/0Uh;

    .line 416879
    iput-object p2, p0, LX/2Uu;->c:LX/0TD;

    .line 416880
    iput-object p3, p0, LX/2Uu;->d:LX/0SG;

    .line 416881
    iput-object p4, p0, LX/2Uu;->e:LX/0Or;

    .line 416882
    iput-object p5, p0, LX/2Uu;->f:LX/0Or;

    .line 416883
    iput-object p6, p0, LX/2Uu;->g:LX/0Or;

    .line 416884
    return-void
.end method

.method public static a(LX/0QB;)LX/2Uu;
    .locals 14

    .prologue
    .line 416885
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 416886
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 416887
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 416888
    if-nez v1, :cond_0

    .line 416889
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 416890
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 416891
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 416892
    sget-object v1, LX/2Uu;->i:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 416893
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 416894
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 416895
    :cond_1
    if-nez v1, :cond_4

    .line 416896
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 416897
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 416898
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 416899
    new-instance v7, LX/2Uu;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, LX/0TD;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    const/16 v11, 0x2744

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x14ea

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x1523

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-direct/range {v7 .. v13}, LX/2Uu;-><init>(LX/0Uh;LX/0TD;LX/0SG;LX/0Or;LX/0Or;LX/0Or;)V

    .line 416900
    const/16 v8, 0x2a3c

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 416901
    iput-object v8, v7, LX/2Uu;->h:LX/0Ot;

    .line 416902
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 416903
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 416904
    if-nez v1, :cond_2

    .line 416905
    sget-object v0, LX/2Uu;->i:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Uu;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 416906
    :goto_1
    if-eqz v0, :cond_3

    .line 416907
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 416908
    :goto_3
    check-cast v0, LX/2Uu;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 416909
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 416910
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 416911
    :catchall_1
    move-exception v0

    .line 416912
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 416913
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 416914
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 416915
    :cond_2
    :try_start_8
    sget-object v0, LX/2Uu;->i:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Uu;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 416916
    const-class v0, Lcom/facebook/messaging/background/annotations/MessagesDataTaskTag;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final f()J
    .locals 4

    .prologue
    .line 416917
    iget-object v0, p0, LX/2Uu;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/32 v2, 0xa4cb800

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416918
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    sget-object v1, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 416919
    iget-object v0, p0, LX/2Uu;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 416920
    :goto_0
    return v0

    .line 416921
    :cond_0
    iget-object v0, p0, LX/2Uu;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 416922
    goto :goto_0

    .line 416923
    :cond_1
    iget-object v0, p0, LX/2Uu;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cy;

    sget-object v2, LX/6cx;->h:LX/2bA;

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v2, v4, v5}, LX/48u;->a(LX/0To;J)J

    move-result-wide v2

    .line 416924
    const-wide/32 v4, 0xa4cb800

    add-long/2addr v2, v4

    iget-object v0, p0, LX/2Uu;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416925
    iget-object v0, p0, LX/2Uu;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cy;

    sget-object v1, LX/6cx;->h:LX/2bA;

    iget-object v2, p0, LX/2Uu;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/48u;->b(LX/0To;J)V

    .line 416926
    iget-object v0, p0, LX/2Uu;->c:LX/0TD;

    new-instance v1, LX/FOo;

    invoke-direct {v1, p0}, LX/FOo;-><init>(LX/2Uu;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
