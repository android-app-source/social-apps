.class public LX/2Pw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/2Pw;


# instance fields
.field private final a:LX/0Uh;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0Xl;

.field private final d:LX/2LM;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/01T;

.field private final g:LX/0c8;

.field private final h:LX/0Xl;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/0cJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;LX/2LM;LX/0Or;LX/01T;LX/0c8;LX/0Xl;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerAppIconBadgingEnabled;
        .end annotation
    .end param
    .param p8    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Xl;",
            "Lcom/facebook/launcherbadges/LauncherBadgesController;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/01T;",
            "LX/0c8;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 407445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407446
    iput-object p1, p0, LX/2Pw;->a:LX/0Uh;

    .line 407447
    iput-object p2, p0, LX/2Pw;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 407448
    iput-object p3, p0, LX/2Pw;->c:LX/0Xl;

    .line 407449
    iput-object p4, p0, LX/2Pw;->d:LX/2LM;

    .line 407450
    iput-object p5, p0, LX/2Pw;->e:LX/0Or;

    .line 407451
    iput-object p6, p0, LX/2Pw;->f:LX/01T;

    .line 407452
    iput-object p7, p0, LX/2Pw;->g:LX/0c8;

    .line 407453
    iput-object p8, p0, LX/2Pw;->h:LX/0Xl;

    .line 407454
    iput-object p9, p0, LX/2Pw;->i:LX/0Or;

    .line 407455
    return-void
.end method

.method public static a(LX/0QB;)LX/2Pw;
    .locals 13

    .prologue
    .line 407432
    sget-object v0, LX/2Pw;->k:LX/2Pw;

    if-nez v0, :cond_1

    .line 407433
    const-class v1, LX/2Pw;

    monitor-enter v1

    .line 407434
    :try_start_0
    sget-object v0, LX/2Pw;->k:LX/2Pw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 407435
    if-eqz v2, :cond_0

    .line 407436
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 407437
    new-instance v3, LX/2Pw;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {v0}, LX/2LM;->a(LX/0QB;)LX/2LM;

    move-result-object v7

    check-cast v7, LX/2LM;

    const/16 v8, 0x14e3

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v9

    check-cast v9, LX/01T;

    invoke-static {v0}, LX/0c8;->a(LX/0QB;)LX/0c8;

    move-result-object v10

    check-cast v10, LX/0c8;

    invoke-static {v0}, LX/0aQ;->a(LX/0QB;)LX/0aQ;

    move-result-object v11

    check-cast v11, LX/0Xl;

    const/16 v12, 0x15e7

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, LX/2Pw;-><init>(LX/0Uh;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;LX/2LM;LX/0Or;LX/01T;LX/0c8;LX/0Xl;LX/0Or;)V

    .line 407438
    move-object v0, v3

    .line 407439
    sput-object v0, LX/2Pw;->k:LX/2Pw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 407440
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 407441
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 407442
    :cond_1
    sget-object v0, LX/2Pw;->k:LX/2Pw;

    return-object v0

    .line 407443
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 407444
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/2Pw;)LX/Juh;
    .locals 3

    .prologue
    .line 407410
    iget-object v0, p0, LX/2Pw;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 407411
    sget-object v0, LX/Juh;->None:LX/Juh;

    .line 407412
    :goto_0
    return-object v0

    .line 407413
    :cond_0
    iget-object v0, p0, LX/2Pw;->a:LX/0Uh;

    const/16 v1, 0x10e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 407414
    sget-object v0, LX/Juh;->UnreadThreadsOnClient:LX/Juh;

    goto :goto_0

    .line 407415
    :cond_1
    sget-object v0, LX/Juh;->UnseenThreads:LX/Juh;

    goto :goto_0
.end method

.method public static declared-synchronized a$redex0(LX/2Pw;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 407425
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2Pw;->a(LX/2Pw;)LX/Juh;

    move-result-object v0

    sget-object v1, LX/Juh;->UnreadThreadsOnClient:LX/Juh;

    if-ne v0, v1, :cond_1

    .line 407426
    const-string v0, "EXTRA_BADGE_COUNT"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 407427
    invoke-static {p0, v0}, LX/2Pw;->b(LX/2Pw;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407428
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 407429
    :cond_1
    :try_start_1
    invoke-static {p0}, LX/2Pw;->a(LX/2Pw;)LX/Juh;

    move-result-object v0

    sget-object v1, LX/Juh;->None:LX/Juh;

    if-ne v0, v1, :cond_0

    .line 407430
    const-string v0, "EXTRA_BADGE_COUNT"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {p0, v0}, LX/2Pw;->c(LX/2Pw;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 407431
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/2Pw;I)V
    .locals 2

    .prologue
    .line 407416
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Pw;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 407417
    iget-object v0, p0, LX/2Pw;->d:LX/2LM;

    invoke-virtual {v0, p1}, LX/2LM;->a(I)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 407418
    iget-object v0, p0, LX/2Pw;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0db;->l:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 407419
    :cond_0
    :goto_0
    invoke-static {p0, p1}, LX/2Pw;->c(LX/2Pw;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407420
    monitor-exit p0

    return-void

    .line 407421
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2Pw;->f:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/2Pw;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0db;->l:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407422
    iget-object v0, p0, LX/2Pw;->d:LX/2LM;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2LM;->a(I)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 407423
    iget-object v0, p0, LX/2Pw;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0db;->l:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 407424
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b$redex0(LX/2Pw;)LX/0cJ;
    .locals 4

    .prologue
    .line 407456
    iget-object v0, p0, LX/2Pw;->j:LX/0cJ;

    if-nez v0, :cond_0

    .line 407457
    iget-object v0, p0, LX/2Pw;->g:LX/0c8;

    sget-object v1, LX/2PT;->a:Ljava/lang/String;

    iget-object v2, p0, LX/2Pw;->h:LX/0Xl;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/0c8;->a(Ljava/lang/String;LX/0Xl;Z)LX/0cJ;

    move-result-object v0

    iput-object v0, p0, LX/2Pw;->j:LX/0cJ;

    .line 407458
    :cond_0
    iget-object v0, p0, LX/2Pw;->j:LX/0cJ;

    return-object v0
.end method

.method private static declared-synchronized c(LX/2Pw;I)V
    .locals 2

    .prologue
    .line 407392
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Pw;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0db;->m:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 407393
    invoke-static {p0}, LX/2Pw;->b$redex0(LX/2Pw;)LX/0cJ;

    move-result-object v0

    invoke-static {p0, p1}, LX/2Pw;->d(LX/2Pw;I)Landroid/os/Message;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0cJ;->a(Landroid/os/Message;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407394
    monitor-exit p0

    return-void

    .line 407395
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static d(LX/2Pw;I)Landroid/os/Message;
    .locals 4

    .prologue
    .line 407382
    const/4 v0, 0x0

    sget v1, LX/2PT;->b:I

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 407383
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 407384
    const-string v0, "key_message_action"

    const-string v3, "action_badge_count_update"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 407385
    const-string v3, "key_user_id"

    iget-object v0, p0, LX/2Pw;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 407386
    const-string v0, "key_messenger_badge_count"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 407387
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 407388
    return-object v1
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 407389
    invoke-static {p0}, LX/2Pw;->a(LX/2Pw;)LX/Juh;

    move-result-object v0

    sget-object v1, LX/Juh;->UnseenThreads:LX/Juh;

    if-ne v0, v1, :cond_0

    .line 407390
    invoke-static {p0, p1}, LX/2Pw;->b(LX/2Pw;I)V

    .line 407391
    :cond_0
    return-void
.end method

.method public final clearUserData()V
    .locals 4

    .prologue
    .line 407396
    const/4 v0, 0x0

    sget v1, LX/2PT;->b:I

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 407397
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 407398
    const-string v2, "key_message_action"

    const-string v3, "action_messenger_user_log_out"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 407399
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 407400
    invoke-static {p0}, LX/2Pw;->b$redex0(LX/2Pw;)LX/0cJ;

    move-result-object v1

    invoke-interface {v1, v0}, LX/0cJ;->a(Landroid/os/Message;)V

    .line 407401
    return-void
.end method

.method public final init()V
    .locals 4

    .prologue
    .line 407402
    new-instance v0, LX/2Px;

    invoke-direct {v0, p0}, LX/2Px;-><init>(LX/2Pw;)V

    .line 407403
    iget-object v1, p0, LX/2Pw;->c:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    sget-object v2, LX/0aY;->r:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    .line 407404
    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 407405
    invoke-static {p0}, LX/2Pw;->b$redex0(LX/2Pw;)LX/0cJ;

    move-result-object v0

    .line 407406
    new-instance v1, LX/2Py;

    invoke-direct {v1}, LX/2Py;-><init>()V

    invoke-interface {v0, v1}, LX/0cJ;->a(LX/0cN;)V

    .line 407407
    sget v1, LX/2PT;->b:I

    new-instance v2, LX/2Pz;

    invoke-direct {v2, p0}, LX/2Pz;-><init>(LX/2Pw;)V

    invoke-interface {v0, v1, v2}, LX/0cJ;->a(ILX/0cM;)V

    .line 407408
    invoke-interface {v0}, LX/0Up;->init()V

    .line 407409
    return-void
.end method
