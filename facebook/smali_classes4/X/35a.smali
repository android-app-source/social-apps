.class public final enum LX/35a;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/35a;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/35a;

.field public static final enum COLD:LX/35a;

.field public static final enum DEAD:LX/35a;

.field public static final enum GOOD:LX/35a;

.field public static final enum OVERHEAT:LX/35a;

.field public static final enum OVER_VOLTAGE:LX/35a;

.field public static final enum UNKNOWN:LX/35a;

.field public static final enum UNSPECIFIED_FAILURE:LX/35a;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 497208
    new-instance v0, LX/35a;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/35a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/35a;->UNKNOWN:LX/35a;

    .line 497209
    new-instance v0, LX/35a;

    const-string v1, "GOOD"

    invoke-direct {v0, v1, v4}, LX/35a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/35a;->GOOD:LX/35a;

    .line 497210
    new-instance v0, LX/35a;

    const-string v1, "OVERHEAT"

    invoke-direct {v0, v1, v5}, LX/35a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/35a;->OVERHEAT:LX/35a;

    .line 497211
    new-instance v0, LX/35a;

    const-string v1, "DEAD"

    invoke-direct {v0, v1, v6}, LX/35a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/35a;->DEAD:LX/35a;

    .line 497212
    new-instance v0, LX/35a;

    const-string v1, "OVER_VOLTAGE"

    invoke-direct {v0, v1, v7}, LX/35a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/35a;->OVER_VOLTAGE:LX/35a;

    .line 497213
    new-instance v0, LX/35a;

    const-string v1, "UNSPECIFIED_FAILURE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/35a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/35a;->UNSPECIFIED_FAILURE:LX/35a;

    .line 497214
    new-instance v0, LX/35a;

    const-string v1, "COLD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/35a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/35a;->COLD:LX/35a;

    .line 497215
    const/4 v0, 0x7

    new-array v0, v0, [LX/35a;

    sget-object v1, LX/35a;->UNKNOWN:LX/35a;

    aput-object v1, v0, v3

    sget-object v1, LX/35a;->GOOD:LX/35a;

    aput-object v1, v0, v4

    sget-object v1, LX/35a;->OVERHEAT:LX/35a;

    aput-object v1, v0, v5

    sget-object v1, LX/35a;->DEAD:LX/35a;

    aput-object v1, v0, v6

    sget-object v1, LX/35a;->OVER_VOLTAGE:LX/35a;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/35a;->UNSPECIFIED_FAILURE:LX/35a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/35a;->COLD:LX/35a;

    aput-object v2, v0, v1

    sput-object v0, LX/35a;->$VALUES:[LX/35a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 497216
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/35a;
    .locals 1

    .prologue
    .line 497217
    const-class v0, LX/35a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/35a;

    return-object v0
.end method

.method public static values()[LX/35a;
    .locals 1

    .prologue
    .line 497218
    sget-object v0, LX/35a;->$VALUES:[LX/35a;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/35a;

    return-object v0
.end method
