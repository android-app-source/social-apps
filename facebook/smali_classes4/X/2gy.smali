.class public LX/2gy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0lC;

.field public final b:LX/0lp;


# direct methods
.method public constructor <init>(LX/0lC;LX/0lp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 449649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 449650
    iput-object p1, p0, LX/2gy;->a:LX/0lC;

    .line 449651
    iput-object p2, p0, LX/2gy;->b:LX/0lp;

    .line 449652
    return-void
.end method

.method public static a(LX/0lF;)LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 449653
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    .line 449654
    invoke-virtual {p0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 449655
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 449656
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 449657
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2gy;
    .locals 3

    .prologue
    .line 449658
    new-instance v2, LX/2gy;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v1

    check-cast v1, LX/0lp;

    invoke-direct {v2, v0, v1}, LX/2gy;-><init>(LX/0lC;LX/0lp;)V

    .line 449659
    return-object v2
.end method
