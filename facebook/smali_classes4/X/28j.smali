.class public final LX/28j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0c5;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0c5;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 374437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374438
    iput-object p1, p0, LX/28j;->a:LX/0QB;

    .line 374439
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 374440
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/28j;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 374441
    packed-switch p2, :pswitch_data_0

    .line 374442
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 374443
    :pswitch_0
    invoke-static {p1}, LX/2u7;->b(LX/0QB;)LX/2u7;

    move-result-object v0

    .line 374444
    :goto_0
    return-object v0

    .line 374445
    :pswitch_1
    invoke-static {p1}, LX/0go;->a(LX/0QB;)LX/0go;

    move-result-object v0

    goto :goto_0

    .line 374446
    :pswitch_2
    invoke-static {p1}, Lcom/facebook/bookmark/client/BookmarkClient;->a(LX/0QB;)Lcom/facebook/bookmark/client/BookmarkClient;

    move-result-object v0

    goto :goto_0

    .line 374447
    :pswitch_3
    invoke-static {p1}, LX/2GQ;->a(LX/0QB;)LX/2GQ;

    move-result-object v0

    goto :goto_0

    .line 374448
    :pswitch_4
    new-instance p0, LX/HnS;

    const-class v0, Landroid/content/Context;

    const-class v1, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p1, v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/1Bf;->a(LX/0QB;)LX/1Bf;

    move-result-object v1

    check-cast v1, LX/1Bf;

    invoke-direct {p0, v0, v1}, LX/HnS;-><init>(Landroid/content/Context;LX/1Bf;)V

    .line 374449
    move-object v0, p0

    .line 374450
    goto :goto_0

    .line 374451
    :pswitch_5
    invoke-static {p1}, LX/1Be;->a(LX/0QB;)LX/1Be;

    move-result-object v0

    goto :goto_0

    .line 374452
    :pswitch_6
    invoke-static {p1}, LX/2Bv;->a(LX/0QB;)LX/2Bv;

    move-result-object v0

    goto :goto_0

    .line 374453
    :pswitch_7
    invoke-static {p1}, LX/1qR;->a(LX/0QB;)LX/1qR;

    move-result-object v0

    goto :goto_0

    .line 374454
    :pswitch_8
    invoke-static {p1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v0

    goto :goto_0

    .line 374455
    :pswitch_9
    invoke-static {p1}, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->a(LX/0QB;)Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    move-result-object v0

    goto :goto_0

    .line 374456
    :pswitch_a
    invoke-static {p1}, LX/7mK;->a(LX/0QB;)LX/7mK;

    move-result-object v0

    goto :goto_0

    .line 374457
    :pswitch_b
    invoke-static {p1}, LX/7mW;->a(LX/0QB;)LX/7mW;

    move-result-object v0

    goto :goto_0

    .line 374458
    :pswitch_c
    invoke-static {p1}, LX/7mf;->a(LX/0QB;)LX/7mf;

    move-result-object v0

    goto :goto_0

    .line 374459
    :pswitch_d
    invoke-static {p1}, LX/HwZ;->a(LX/0QB;)LX/HwZ;

    move-result-object v0

    goto :goto_0

    .line 374460
    :pswitch_e
    invoke-static {p1}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v0

    goto :goto_0

    .line 374461
    :pswitch_f
    invoke-static {p1}, LX/2re;->a(LX/0QB;)LX/2re;

    move-result-object v0

    goto :goto_0

    .line 374462
    :pswitch_10
    invoke-static {p1}, LX/6Nf;->a(LX/0QB;)LX/6Nf;

    move-result-object v0

    goto :goto_0

    .line 374463
    :pswitch_11
    invoke-static {p1}, LX/94q;->a(LX/0QB;)LX/94q;

    move-result-object v0

    goto :goto_0

    .line 374464
    :pswitch_12
    invoke-static {p1}, LX/2K9;->a(LX/0QB;)LX/2K9;

    move-result-object v0

    goto :goto_0

    .line 374465
    :pswitch_13
    invoke-static {p1}, LX/2KA;->a(LX/0QB;)LX/2KA;

    move-result-object v0

    goto :goto_0

    .line 374466
    :pswitch_14
    invoke-static {p1}, LX/Bl2;->a(LX/0QB;)LX/Bl2;

    move-result-object v0

    goto :goto_0

    .line 374467
    :pswitch_15
    invoke-static {p1}, LX/I5f;->b(LX/0QB;)LX/I5f;

    move-result-object v0

    goto :goto_0

    .line 374468
    :pswitch_16
    invoke-static {p1}, LX/7yd;->b(LX/0QB;)LX/7yd;

    move-result-object v0

    goto :goto_0

    .line 374469
    :pswitch_17
    new-instance v0, LX/98v;

    const-class v1, Landroid/content/Context;

    invoke-interface {p1, v1}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x53c

    invoke-static {p1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/98v;-><init>(LX/0Ot;LX/0Ot;)V

    .line 374470
    move-object v0, v0

    .line 374471
    goto/16 :goto_0

    .line 374472
    :pswitch_18
    invoke-static {p1}, LX/0oX;->a(LX/0QB;)LX/0oX;

    move-result-object v0

    goto/16 :goto_0

    .line 374473
    :pswitch_19
    invoke-static {p1}, LX/3iP;->a(LX/0QB;)LX/3iP;

    move-result-object v0

    goto/16 :goto_0

    .line 374474
    :pswitch_1a
    invoke-static {p1}, LX/Ezx;->a(LX/0QB;)LX/Ezx;

    move-result-object v0

    goto/16 :goto_0

    .line 374475
    :pswitch_1b
    invoke-static {p1}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a(LX/0QB;)Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    move-result-object v0

    goto/16 :goto_0

    .line 374476
    :pswitch_1c
    invoke-static {p1}, LX/1N8;->a(LX/0QB;)LX/1N8;

    move-result-object v0

    goto/16 :goto_0

    .line 374477
    :pswitch_1d
    invoke-static {p1}, LX/0tA;->a(LX/0QB;)LX/0tA;

    move-result-object v0

    goto/16 :goto_0

    .line 374478
    :pswitch_1e
    invoke-static {p1}, LX/2k0;->a(LX/0QB;)LX/2k0;

    move-result-object v0

    goto/16 :goto_0

    .line 374479
    :pswitch_1f
    invoke-static {p1}, LX/2kG;->a(LX/0QB;)LX/2kG;

    move-result-object v0

    goto/16 :goto_0

    .line 374480
    :pswitch_20
    invoke-static {p1}, LX/0sh;->a(LX/0QB;)LX/0sh;

    move-result-object v0

    goto/16 :goto_0

    .line 374481
    :pswitch_21
    invoke-static {p1}, LX/DNR;->b(LX/0QB;)LX/DNR;

    move-result-object v0

    goto/16 :goto_0

    .line 374482
    :pswitch_22
    invoke-static {p1}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v0

    goto/16 :goto_0

    .line 374483
    :pswitch_23
    new-instance v1, LX/GzO;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/GzO;-><init>(Landroid/content/Context;)V

    .line 374484
    move-object v0, v1

    .line 374485
    goto/16 :goto_0

    .line 374486
    :pswitch_24
    invoke-static {p1}, LX/18Q;->a(LX/0QB;)LX/18Q;

    move-result-object v0

    goto/16 :goto_0

    .line 374487
    :pswitch_25
    invoke-static {p1}, LX/CJU;->a(LX/0QB;)LX/CJU;

    move-result-object v0

    goto/16 :goto_0

    .line 374488
    :pswitch_26
    invoke-static {p1}, LX/2Vw;->a(LX/0QB;)LX/2Vw;

    move-result-object v0

    goto/16 :goto_0

    .line 374489
    :pswitch_27
    invoke-static {p1}, LX/6cz;->a(LX/0QB;)LX/6cz;

    move-result-object v0

    goto/16 :goto_0

    .line 374490
    :pswitch_28
    invoke-static {p1}, LX/6e7;->a(LX/0QB;)LX/6e7;

    move-result-object v0

    goto/16 :goto_0

    .line 374491
    :pswitch_29
    invoke-static {p1}, LX/6e8;->a(LX/0QB;)LX/6e8;

    move-result-object v0

    goto/16 :goto_0

    .line 374492
    :pswitch_2a
    invoke-static {p1}, LX/2My;->a(LX/0QB;)LX/2My;

    move-result-object v0

    goto/16 :goto_0

    .line 374493
    :pswitch_2b
    invoke-static {p1}, LX/3QS;->a(LX/0QB;)LX/3QS;

    move-result-object v0

    goto/16 :goto_0

    .line 374494
    :pswitch_2c
    invoke-static {p1}, LX/FNh;->a(LX/0QB;)LX/FNh;

    move-result-object v0

    goto/16 :goto_0

    .line 374495
    :pswitch_2d
    invoke-static {p1}, LX/2PJ;->a(LX/0QB;)LX/2PJ;

    move-result-object v0

    goto/16 :goto_0

    .line 374496
    :pswitch_2e
    invoke-static {p1}, LX/Iub;->a(LX/0QB;)LX/Iub;

    move-result-object v0

    goto/16 :goto_0

    .line 374497
    :pswitch_2f
    invoke-static {p1}, LX/Iuc;->a(LX/0QB;)LX/Iuc;

    move-result-object v0

    goto/16 :goto_0

    .line 374498
    :pswitch_30
    invoke-static {p1}, LX/Iul;->a(LX/0QB;)LX/Iul;

    move-result-object v0

    goto/16 :goto_0

    .line 374499
    :pswitch_31
    invoke-static {p1}, LX/296;->a(LX/0QB;)LX/296;

    move-result-object v0

    goto/16 :goto_0

    .line 374500
    :pswitch_32
    invoke-static {p1}, LX/0c4;->a(LX/0QB;)LX/0c4;

    move-result-object v0

    goto/16 :goto_0

    .line 374501
    :pswitch_33
    invoke-static {p1}, LX/H1w;->a(LX/0QB;)LX/H1w;

    move-result-object v0

    goto/16 :goto_0

    .line 374502
    :pswitch_34
    invoke-static {p1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v0

    goto/16 :goto_0

    .line 374503
    :pswitch_35
    invoke-static {p1}, LX/2PX;->a(LX/0QB;)LX/2PX;

    move-result-object v0

    goto/16 :goto_0

    .line 374504
    :pswitch_36
    invoke-static {p1}, LX/Dr0;->a(LX/0QB;)LX/Dr0;

    move-result-object v0

    goto/16 :goto_0

    .line 374505
    :pswitch_37
    invoke-static {p1}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v0

    goto/16 :goto_0

    .line 374506
    :pswitch_38
    invoke-static {p1}, LX/1rp;->a(LX/0QB;)LX/1rp;

    move-result-object v0

    goto/16 :goto_0

    .line 374507
    :pswitch_39
    invoke-static {p1}, LX/2Pe;->a(LX/0QB;)LX/2Pe;

    move-result-object v0

    goto/16 :goto_0

    .line 374508
    :pswitch_3a
    invoke-static {p1}, LX/2L2;->a(LX/0QB;)LX/2L2;

    move-result-object v0

    goto/16 :goto_0

    .line 374509
    :pswitch_3b
    invoke-static {p1}, LX/2Pk;->getInstance__com_facebook_omnistore_module_OmnistoreComponentManager__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pk;

    move-result-object v0

    goto/16 :goto_0

    .line 374510
    :pswitch_3c
    invoke-static {p1}, LX/2Pw;->a(LX/0QB;)LX/2Pw;

    move-result-object v0

    goto/16 :goto_0

    .line 374511
    :pswitch_3d
    invoke-static {p1}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v0

    goto/16 :goto_0

    .line 374512
    :pswitch_3e
    invoke-static {p1}, LX/3f6;->a(LX/0QB;)LX/3f6;

    move-result-object v0

    goto/16 :goto_0

    .line 374513
    :pswitch_3f
    invoke-static {p1}, LX/HR7;->a(LX/0QB;)LX/HR7;

    move-result-object v0

    goto/16 :goto_0

    .line 374514
    :pswitch_40
    invoke-static {p1}, LX/6oh;->a(LX/0QB;)LX/6oh;

    move-result-object v0

    goto/16 :goto_0

    .line 374515
    :pswitch_41
    invoke-static {p1}, LX/IzN;->a(LX/0QB;)LX/IzN;

    move-result-object v0

    goto/16 :goto_0

    .line 374516
    :pswitch_42
    invoke-static {p1}, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->a(LX/0QB;)Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;

    move-result-object v0

    goto/16 :goto_0

    .line 374517
    :pswitch_43
    invoke-static {p1}, LX/8Gn;->a(LX/0QB;)LX/8Gn;

    move-result-object v0

    goto/16 :goto_0

    .line 374518
    :pswitch_44
    invoke-static {p1}, LX/8HH;->a(LX/0QB;)LX/8HH;

    move-result-object v0

    goto/16 :goto_0

    .line 374519
    :pswitch_45
    invoke-static {p1}, LX/FS7;->a(LX/0QB;)LX/FS7;

    move-result-object v0

    goto/16 :goto_0

    .line 374520
    :pswitch_46
    invoke-static {p1}, LX/Dux;->a(LX/0QB;)LX/Dux;

    move-result-object v0

    goto/16 :goto_0

    .line 374521
    :pswitch_47
    invoke-static {p1}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v0

    goto/16 :goto_0

    .line 374522
    :pswitch_48
    invoke-static {p1}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v0

    goto/16 :goto_0

    .line 374523
    :pswitch_49
    invoke-static {p1}, LX/9k6;->a(LX/0QB;)LX/9k6;

    move-result-object v0

    goto/16 :goto_0

    .line 374524
    :pswitch_4a
    invoke-static {p1}, LX/3lh;->b(LX/0QB;)LX/3lh;

    move-result-object v0

    goto/16 :goto_0

    .line 374525
    :pswitch_4b
    invoke-static {p1}, LX/FSc;->a(LX/0QB;)LX/FSc;

    move-result-object v0

    goto/16 :goto_0

    .line 374526
    :pswitch_4c
    invoke-static {p1}, LX/2sP;->a(LX/0QB;)LX/2sP;

    move-result-object v0

    goto/16 :goto_0

    .line 374527
    :pswitch_4d
    invoke-static {p1}, LX/13N;->a(LX/0QB;)LX/13N;

    move-result-object v0

    goto/16 :goto_0

    .line 374528
    :pswitch_4e
    invoke-static {p1}, LX/EDI;->a(LX/0QB;)LX/EDI;

    move-result-object v0

    goto/16 :goto_0

    .line 374529
    :pswitch_4f
    invoke-static {p1}, LX/EGA;->a(LX/0QB;)LX/EGA;

    move-result-object v0

    goto/16 :goto_0

    .line 374530
    :pswitch_50
    invoke-static {p1}, LX/09G;->a(LX/0QB;)LX/09G;

    move-result-object v0

    goto/16 :goto_0

    .line 374531
    :pswitch_51
    invoke-static {p1}, LX/1Sd;->a(LX/0QB;)LX/1Sd;

    move-result-object v0

    goto/16 :goto_0

    .line 374532
    :pswitch_52
    invoke-static {p1}, LX/Fa6;->a(LX/0QB;)LX/Fa6;

    move-result-object v0

    goto/16 :goto_0

    .line 374533
    :pswitch_53
    invoke-static {p1}, LX/2SO;->a(LX/0QB;)LX/2SO;

    move-result-object v0

    goto/16 :goto_0

    .line 374534
    :pswitch_54
    invoke-static {p1}, LX/2So;->a(LX/0QB;)LX/2So;

    move-result-object v0

    goto/16 :goto_0

    .line 374535
    :pswitch_55
    invoke-static {p1}, LX/A5P;->a(LX/0QB;)LX/A5P;

    move-result-object v0

    goto/16 :goto_0

    .line 374536
    :pswitch_56
    invoke-static {p1}, LX/Fqs;->a(LX/0QB;)LX/Fqs;

    move-result-object v0

    goto/16 :goto_0

    .line 374537
    :pswitch_57
    invoke-static {p1}, LX/BQT;->b(LX/0QB;)LX/BQT;

    move-result-object v0

    goto/16 :goto_0

    .line 374538
    :pswitch_58
    invoke-static {p1}, LX/D2T;->a(LX/0QB;)LX/D2T;

    move-result-object v0

    goto/16 :goto_0

    .line 374539
    :pswitch_59
    invoke-static {p1}, LX/FhZ;->a(LX/0QB;)LX/7Hq;

    move-result-object v0

    goto/16 :goto_0

    .line 374540
    :pswitch_5a
    invoke-static {p1}, LX/EQa;->a(LX/0QB;)LX/EQa;

    move-result-object v0

    goto/16 :goto_0

    .line 374541
    :pswitch_5b
    invoke-static {p1}, LX/2TH;->a(LX/0QB;)LX/2TH;

    move-result-object v0

    goto/16 :goto_0

    .line 374542
    :pswitch_5c
    invoke-static {p1}, LX/ETB;->a(LX/0QB;)LX/ETB;

    move-result-object v0

    goto/16 :goto_0

    .line 374543
    :pswitch_5d
    invoke-static {p1}, LX/0hn;->a(LX/0QB;)LX/0hn;

    move-result-object v0

    goto/16 :goto_0

    .line 374544
    :pswitch_5e
    invoke-static {p1}, LX/EVa;->a(LX/0QB;)LX/EVa;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_5a
        :pswitch_5b
        :pswitch_5c
        :pswitch_5d
        :pswitch_5e
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 374545
    const/16 v0, 0x5f

    return v0
.end method
