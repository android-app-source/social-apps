.class public final LX/3AJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3AK;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HxS;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/I77;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GdL;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Etb;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HPd;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FWb;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HxS;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/I77;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/GdL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Etb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HPd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FWb;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/datafetcher/TimelineEarlyFetcher;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 525110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 525111
    iput-object p1, p0, LX/3AJ;->a:LX/0Ot;

    .line 525112
    iput-object p2, p0, LX/3AJ;->b:LX/0Ot;

    .line 525113
    iput-object p3, p0, LX/3AJ;->c:LX/0Ot;

    .line 525114
    iput-object p4, p0, LX/3AJ;->d:LX/0Ot;

    .line 525115
    iput-object p5, p0, LX/3AJ;->e:LX/0Ot;

    .line 525116
    iput-object p6, p0, LX/3AJ;->f:LX/0Ot;

    .line 525117
    iput-object p7, p0, LX/3AJ;->g:LX/0Ot;

    .line 525118
    return-void
.end method

.method public static a(LX/0QB;)LX/3AJ;
    .locals 11

    .prologue
    .line 525090
    const-class v1, LX/3AJ;

    monitor-enter v1

    .line 525091
    :try_start_0
    sget-object v0, LX/3AJ;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 525092
    sput-object v2, LX/3AJ;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 525093
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525094
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 525095
    new-instance v3, LX/3AJ;

    const/16 v4, 0x1afc

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1b63

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1c93

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2221

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2c00

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x3297

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x362f

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/3AJ;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 525096
    move-object v0, v3

    .line 525097
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 525098
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3AJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 525099
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 525100
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0cQ;)LX/98h;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 525101
    invoke-virtual {p1}, LX/0cQ;->ordinal()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 525102
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 525103
    :sswitch_0
    iget-object v0, p0, LX/3AJ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/98h;

    goto :goto_0

    .line 525104
    :sswitch_1
    iget-object v0, p0, LX/3AJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/98h;

    goto :goto_0

    .line 525105
    :sswitch_2
    iget-object v0, p0, LX/3AJ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/98h;

    goto :goto_0

    .line 525106
    :sswitch_3
    iget-object v0, p0, LX/3AJ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/98h;

    goto :goto_0

    .line 525107
    :sswitch_4
    iget-object v0, p0, LX/3AJ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/98h;

    goto :goto_0

    .line 525108
    :sswitch_5
    iget-object v0, p0, LX/3AJ;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/98h;

    goto :goto_0

    .line 525109
    :sswitch_6
    iget-object v0, p0, LX/3AJ;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/98h;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_6
        0x9 -> :sswitch_4
        0x1b -> :sswitch_3
        0x4d -> :sswitch_1
        0x53 -> :sswitch_5
        0x56 -> :sswitch_0
        0xf8 -> :sswitch_2
    .end sparse-switch
.end method
