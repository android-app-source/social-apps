.class public LX/3As;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0lp;

.field private final b:LX/0lC;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lp;LX/0lC;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 526435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 526436
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 526437
    iput-object v0, p0, LX/3As;->c:LX/0Px;

    .line 526438
    iput-object p1, p0, LX/3As;->a:LX/0lp;

    .line 526439
    iput-object p2, p0, LX/3As;->b:LX/0lC;

    .line 526440
    return-void
.end method

.method private static a(LX/15w;LX/0lC;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0lC;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 526441
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 526442
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 526443
    const-class v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p1, p0, v0}, LX/0lD;->a(LX/15w;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 526444
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 526445
    :goto_0
    return-object v0

    .line 526446
    :cond_0
    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_1

    .line 526447
    new-instance v0, LX/82Y;

    invoke-direct {v0}, LX/82Y;-><init>()V

    invoke-virtual {p1, p0, v0}, LX/0lD;->a(LX/15w;LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0

    .line 526448
    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "JSON feed injection data does not start with { or ["

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/3As;LX/82W;)V
    .locals 1

    .prologue
    .line 526449
    :try_start_0
    invoke-interface {p1}, LX/82W;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 526450
    return-void

    .line 526451
    :catch_0
    move-exception v0

    .line 526452
    invoke-virtual {p0}, LX/3As;->a()V

    .line 526453
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public static a$redex0(LX/3As;LX/15w;)V
    .locals 1

    .prologue
    .line 526454
    iget-object v0, p0, LX/3As;->b:LX/0lC;

    invoke-static {p1, v0}, LX/3As;->a(LX/15w;LX/0lC;)Ljava/util/List;

    move-result-object v0

    .line 526455
    if-eqz v0, :cond_0

    .line 526456
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3As;->c:LX/0Px;

    .line 526457
    :goto_0
    return-void

    .line 526458
    :cond_0
    invoke-virtual {p0}, LX/3As;->a()V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/3As;
    .locals 3

    .prologue
    .line 526459
    new-instance v2, LX/3As;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v0

    check-cast v0, LX/0lp;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-direct {v2, v0, v1}, LX/3As;-><init>(LX/0lp;LX/0lC;)V

    .line 526460
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 526461
    invoke-virtual {p0}, LX/3As;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 526462
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 526463
    iput-object v0, p0, LX/3As;->c:LX/0Px;

    .line 526464
    :cond_0
    return-void
.end method

.method public final a(Ljava/io/File;)Z
    .locals 4

    .prologue
    .line 526465
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 526466
    :cond_0
    invoke-virtual {p0}, LX/3As;->a()V

    .line 526467
    const/4 v0, 0x0

    .line 526468
    :goto_0
    return v0

    .line 526469
    :cond_1
    new-instance v0, LX/82X;

    invoke-direct {v0, p0, p1}, LX/82X;-><init>(LX/3As;Ljava/io/File;)V

    invoke-static {p0, v0}, LX/3As;->a(LX/3As;LX/82W;)V

    .line 526470
    invoke-virtual {p0}, LX/3As;->b()Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 526471
    iget-object v0, p0, LX/3As;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
