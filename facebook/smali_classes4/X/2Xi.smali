.class public final LX/2Xi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/auth/component/AuthComponent;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/auth/component/AuthComponent;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 420075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420076
    iput-object p1, p0, LX/2Xi;->a:LX/0QB;

    .line 420077
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/auth/component/AuthComponent;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 420078
    new-instance v0, LX/2Xi;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2Xi;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 420079
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/auth/component/AuthComponent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 420080
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/2Xi;

    invoke-direct {v2, p0}, LX/2Xi;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 420081
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2Xi;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 420082
    packed-switch p2, :pswitch_data_0

    .line 420083
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420084
    :pswitch_0
    invoke-static {p1}, LX/28e;->a(LX/0QB;)LX/1qj;

    move-result-object v0

    .line 420085
    :goto_0
    return-object v0

    .line 420086
    :pswitch_1
    invoke-static {p1}, LX/28g;->a(LX/0QB;)LX/16A;

    move-result-object v0

    goto :goto_0

    .line 420087
    :pswitch_2
    invoke-static {p1}, LX/28i;->a(LX/0QB;)LX/28i;

    move-result-object v0

    goto :goto_0

    .line 420088
    :pswitch_3
    invoke-static {p1}, LX/28k;->a(LX/0QB;)LX/28k;

    move-result-object v0

    goto :goto_0

    .line 420089
    :pswitch_4
    new-instance v1, LX/28n;

    invoke-static {p1}, LX/284;->b(LX/0QB;)LX/284;

    move-result-object v0

    check-cast v0, LX/284;

    const/16 p0, 0xf4

    invoke-static {p1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 p2, 0xea

    invoke-static {p1, p2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-direct {v1, v0, p0, p2}, LX/28n;-><init>(LX/284;LX/0Ot;LX/0Ot;)V

    .line 420090
    move-object v0, v1

    .line 420091
    goto :goto_0

    .line 420092
    :pswitch_5
    invoke-static {p1}, LX/28o;->a(LX/0QB;)LX/28o;

    move-result-object v0

    goto :goto_0

    .line 420093
    :pswitch_6
    invoke-static {p1}, LX/2Zy;->a(LX/0QB;)LX/2Zy;

    move-result-object v0

    goto :goto_0

    .line 420094
    :pswitch_7
    invoke-static {p1}, LX/2a4;->a(LX/0QB;)LX/2a4;

    move-result-object v0

    goto :goto_0

    .line 420095
    :pswitch_8
    invoke-static {p1}, LX/27t;->b(LX/0QB;)LX/27t;

    move-result-object v0

    goto :goto_0

    .line 420096
    :pswitch_9
    new-instance p2, LX/28s;

    const-class v0, Landroid/content/Context;

    const-class v1, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p1, v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/28t;->a(LX/0QB;)LX/28t;

    move-result-object v1

    check-cast v1, LX/28t;

    invoke-static {p1}, LX/0aQ;->a(LX/0QB;)LX/0aQ;

    move-result-object p0

    check-cast p0, LX/0Xl;

    invoke-direct {p2, v0, v1, p0}, LX/28s;-><init>(Landroid/content/Context;LX/28t;LX/0Xl;)V

    .line 420097
    move-object v0, p2

    .line 420098
    goto :goto_0

    .line 420099
    :pswitch_a
    new-instance v1, LX/28u;

    const-class v0, Landroid/content/Context;

    const-class p0, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p1, v0, p0}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/28u;-><init>(Landroid/content/Context;)V

    .line 420100
    move-object v0, v1

    .line 420101
    goto :goto_0

    .line 420102
    :pswitch_b
    new-instance v1, LX/28v;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v0}, LX/28v;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 420103
    move-object v0, v1

    .line 420104
    goto :goto_0

    .line 420105
    :pswitch_c
    new-instance p0, LX/28w;

    invoke-static {p1}, LX/28x;->a(LX/0QB;)LX/28x;

    move-result-object v0

    check-cast v0, LX/28x;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v0, v1}, LX/28w;-><init>(LX/28x;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 420106
    move-object v0, p0

    .line 420107
    goto/16 :goto_0

    .line 420108
    :pswitch_d
    invoke-static {p1}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v0

    goto/16 :goto_0

    .line 420109
    :pswitch_e
    invoke-static {p1}, LX/2Bz;->a(LX/0QB;)LX/2Bz;

    move-result-object v0

    goto/16 :goto_0

    .line 420110
    :pswitch_f
    invoke-static {p1}, LX/2C1;->a(LX/0QB;)LX/2C1;

    move-result-object v0

    goto/16 :goto_0

    .line 420111
    :pswitch_10
    invoke-static {p1}, LX/29A;->a(LX/0QB;)LX/29A;

    move-result-object v0

    goto/16 :goto_0

    .line 420112
    :pswitch_11
    invoke-static {p1}, LX/08i;->a(LX/0QB;)LX/08i;

    move-result-object v0

    goto/16 :goto_0

    .line 420113
    :pswitch_12
    invoke-static {p1}, LX/07y;->a(LX/0QB;)LX/07y;

    move-result-object v0

    goto/16 :goto_0

    .line 420114
    :pswitch_13
    invoke-static {p1}, LX/29Z;->a(LX/0QB;)LX/29Z;

    move-result-object v0

    goto/16 :goto_0

    .line 420115
    :pswitch_14
    invoke-static {p1}, LX/29a;->a(LX/0QB;)LX/29a;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 420116
    const/16 v0, 0x15

    return v0
.end method
