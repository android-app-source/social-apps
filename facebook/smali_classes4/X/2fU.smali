.class public final LX/2fU;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/2fU;

.field public static final b:LX/2fU;


# instance fields
.field public final c:LX/Gd7;

.field public final d:LX/Gd6;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 446562
    new-instance v0, LX/2fU;

    sget-object v1, LX/Gd7;->NOOP:LX/Gd7;

    invoke-direct {v0, v1}, LX/2fU;-><init>(LX/Gd7;)V

    sput-object v0, LX/2fU;->a:LX/2fU;

    .line 446563
    new-instance v0, LX/2fU;

    sget-object v1, LX/Gd7;->REVERT:LX/Gd7;

    invoke-direct {v0, v1}, LX/2fU;-><init>(LX/Gd7;)V

    sput-object v0, LX/2fU;->b:LX/2fU;

    return-void
.end method

.method private constructor <init>(LX/Gd7;)V
    .locals 1

    .prologue
    .line 446564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 446565
    iput-object p1, p0, LX/2fU;->c:LX/Gd7;

    .line 446566
    const/4 v0, 0x0

    iput-object v0, p0, LX/2fU;->d:LX/Gd6;

    .line 446567
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/util/Date;Ljava/lang/String;ILjava/util/Map;)V
    .locals 1
    .param p3    # Ljava/util/Date;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 446568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 446569
    sget-object v0, LX/Gd7;->UPDATE:LX/Gd7;

    iput-object v0, p0, LX/2fU;->c:LX/Gd7;

    .line 446570
    new-instance v0, LX/Gd6;

    invoke-direct {v0}, LX/Gd6;-><init>()V

    iput-object v0, p0, LX/2fU;->d:LX/Gd6;

    .line 446571
    iget-object v0, p0, LX/2fU;->d:LX/Gd6;

    .line 446572
    iput-object p1, v0, LX/Gd6;->a:Ljava/lang/String;

    .line 446573
    iget-object v0, p0, LX/2fU;->d:LX/Gd6;

    .line 446574
    iput p2, v0, LX/Gd6;->b:I

    .line 446575
    iget-object v0, p0, LX/2fU;->d:LX/Gd6;

    .line 446576
    iput-object p3, v0, LX/Gd6;->c:Ljava/util/Date;

    .line 446577
    iget-object v0, p0, LX/2fU;->d:LX/Gd6;

    .line 446578
    iput-object p4, v0, LX/Gd6;->d:Ljava/lang/String;

    .line 446579
    iget-object v0, p0, LX/2fU;->d:LX/Gd6;

    .line 446580
    iput p5, v0, LX/Gd6;->e:I

    .line 446581
    iget-object v0, p0, LX/2fU;->d:LX/Gd6;

    .line 446582
    iput-object p6, v0, LX/Gd6;->f:Ljava/util/Map;

    .line 446583
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 446584
    iget-object v0, p0, LX/2fU;->d:LX/Gd6;

    if-nez v0, :cond_0

    .line 446585
    const/4 v0, 0x0

    .line 446586
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2fU;->d:LX/Gd6;

    iget-object v0, v0, LX/Gd6;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 446587
    iget-object v0, p0, LX/2fU;->d:LX/Gd6;

    if-nez v0, :cond_0

    .line 446588
    const/4 v0, 0x0

    .line 446589
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2fU;->d:LX/Gd6;

    iget v0, v0, LX/Gd6;->b:I

    goto :goto_0
.end method

.method public final d()I
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 446590
    iget-object v0, p0, LX/2fU;->d:LX/Gd6;

    if-nez v0, :cond_0

    .line 446591
    const/4 v0, 0x0

    .line 446592
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2fU;->d:LX/Gd6;

    iget v0, v0, LX/Gd6;->e:I

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 446593
    iget-object v0, p0, LX/2fU;->d:LX/Gd6;

    if-nez v0, :cond_0

    .line 446594
    iget-object v0, p0, LX/2fU;->c:LX/Gd7;

    invoke-virtual {v0}, LX/Gd7;->toString()Ljava/lang/String;

    move-result-object v0

    .line 446595
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LX/2fU;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/2fU;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 446596
    iget-object v1, p0, LX/2fU;->d:LX/Gd6;

    if-nez v1, :cond_1

    .line 446597
    const/4 v1, 0x0

    .line 446598
    :goto_1
    move-object v1, v1

    .line 446599
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 446600
    iget-object v1, p0, LX/2fU;->d:LX/Gd6;

    if-nez v1, :cond_2

    .line 446601
    const/4 v1, 0x0

    .line 446602
    :goto_2
    move-object v1, v1

    .line 446603
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/2fU;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, LX/2fU;->d:LX/Gd6;

    iget-object v1, v1, LX/Gd6;->d:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v1, p0, LX/2fU;->d:LX/Gd6;

    iget-object v1, v1, LX/Gd6;->c:Ljava/util/Date;

    goto :goto_2
.end method
