.class public abstract LX/3G2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:J

.field public d:J

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 540355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 540356
    sget-wide v0, LX/3G3;->a:J

    iput-wide v0, p0, LX/3G2;->d:J

    .line 540357
    const/16 v0, 0x3e8

    iput v0, p0, LX/3G2;->f:I

    return-void
.end method


# virtual methods
.method public final a(I)LX/3G2;
    .locals 0

    .prologue
    .line 540358
    iput p1, p0, LX/3G2;->e:I

    .line 540359
    return-object p0
.end method

.method public final a(J)LX/3G2;
    .locals 1

    .prologue
    .line 540360
    iput-wide p1, p0, LX/3G2;->d:J

    .line 540361
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/3G2;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 540362
    iput-object p1, p0, LX/3G2;->a:Ljava/lang/String;

    .line 540363
    return-object p0
.end method

.method public abstract a()LX/3G3;
.end method

.method public final b(I)LX/3G2;
    .locals 0

    .prologue
    .line 540364
    iput p1, p0, LX/3G2;->f:I

    .line 540365
    return-object p0
.end method

.method public final b(J)LX/3G2;
    .locals 1

    .prologue
    .line 540366
    iput-wide p1, p0, LX/3G2;->c:J

    .line 540367
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/3G2;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 540368
    iput-object p1, p0, LX/3G2;->b:Ljava/lang/String;

    .line 540369
    return-object p0
.end method
