.class public LX/2e2;
.super LX/25J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/25J",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile n:LX/2e2;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0rq;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/03V;

.field private final e:LX/0bH;

.field public final f:Ljava/lang/String;

.field public final g:LX/189;

.field public final h:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final i:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/2dl;

.field public final l:LX/2e3;

.field public final m:LX/2di;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/03V;LX/0rq;Ljava/util/concurrent/ExecutorService;LX/189;LX/0bH;LX/1Ck;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Ot;LX/2dl;LX/2e3;LX/2di;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0rq;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/189;",
            "LX/0bH;",
            "LX/1Ck;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;",
            "LX/2dl;",
            "LX/2e3;",
            "LX/2di;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444900
    invoke-direct {p0}, LX/25J;-><init>()V

    .line 444901
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/2e2;->a:Ljava/util/Set;

    .line 444902
    iput-object p2, p0, LX/2e2;->d:LX/03V;

    .line 444903
    iput-object p3, p0, LX/2e2;->b:LX/0rq;

    .line 444904
    iput-object p4, p0, LX/2e2;->c:Ljava/util/concurrent/Executor;

    .line 444905
    iput-object p5, p0, LX/2e2;->g:LX/189;

    .line 444906
    iput-object p6, p0, LX/2e2;->e:LX/0bH;

    .line 444907
    iput-object p7, p0, LX/2e2;->i:LX/1Ck;

    .line 444908
    iput-object p8, p0, LX/2e2;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 444909
    const v0, 0x7f0b0f5d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2e2;->f:Ljava/lang/String;

    .line 444910
    iput-object p9, p0, LX/2e2;->j:LX/0Ot;

    .line 444911
    iput-object p10, p0, LX/2e2;->k:LX/2dl;

    .line 444912
    iput-object p11, p0, LX/2e2;->l:LX/2e3;

    .line 444913
    iput-object p12, p0, LX/2e2;->m:LX/2di;

    .line 444914
    return-void
.end method

.method public static a(LX/0QB;)LX/2e2;
    .locals 3

    .prologue
    .line 444890
    sget-object v0, LX/2e2;->n:LX/2e2;

    if-nez v0, :cond_1

    .line 444891
    const-class v1, LX/2e2;

    monitor-enter v1

    .line 444892
    :try_start_0
    sget-object v0, LX/2e2;->n:LX/2e2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 444893
    if-eqz v2, :cond_0

    .line 444894
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2e2;->b(LX/0QB;)LX/2e2;

    move-result-object v0

    sput-object v0, LX/2e2;->n:LX/2e2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444895
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 444896
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 444897
    :cond_1
    sget-object v0, LX/2e2;->n:LX/2e2;

    return-object v0

    .line 444898
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 444899
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)Z
    .locals 2

    .prologue
    .line 444888
    invoke-static {p0}, LX/2e2;->d(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 444889
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/2e2;
    .locals 13

    .prologue
    .line 444855
    new-instance v0, LX/2e2;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v3

    check-cast v3, LX/0rq;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v5

    check-cast v5, LX/189;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v6

    check-cast v6, LX/0bH;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v8

    check-cast v8, Lcom/facebook/performancelogger/PerformanceLogger;

    const/16 v9, 0x474

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {p0}, LX/2dl;->a(LX/0QB;)LX/2dl;

    move-result-object v10

    check-cast v10, LX/2dl;

    invoke-static {p0}, LX/2e3;->a(LX/0QB;)LX/2e3;

    move-result-object v11

    check-cast v11, LX/2e3;

    invoke-static {p0}, LX/2di;->b(LX/0QB;)LX/2di;

    move-result-object v12

    check-cast v12, LX/2di;

    invoke-direct/range {v0 .. v12}, LX/2e2;-><init>(Landroid/content/res/Resources;LX/03V;LX/0rq;Ljava/util/concurrent/ExecutorService;LX/189;LX/0bH;LX/1Ck;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Ot;LX/2dl;LX/2e3;LX/2di;)V

    .line 444856
    return-object v0
.end method

.method private static d(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 1

    .prologue
    .line 444887
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;I)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 444875
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->o()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 444876
    invoke-static {p1}, LX/2e2;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)Z

    move-result v3

    .line 444877
    if-eqz v3, :cond_0

    if-ne p2, v2, :cond_0

    .line 444878
    iget-object v4, p0, LX/2e2;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v5, LX/0Yj;

    const v6, 0x2e0001

    const-string v7, "PaginatedPymkFeedUnitTTI"

    invoke-direct {v5, v6, v7}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v6, v0, [Ljava/lang/String;

    const-string v7, "native_newsfeed"

    aput-object v7, v6, v1

    invoke-virtual {v5, v6}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    .line 444879
    :cond_0
    iget-object v4, p0, LX/2e2;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->g()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, LX/2e2;->l:LX/2e3;

    const/16 v5, 0xa

    .line 444880
    iget-boolean v6, v4, LX/2e3;->c:Z

    if-nez v6, :cond_2

    .line 444881
    :goto_0
    move v4, v5

    .line 444882
    sub-int/2addr v2, v4

    if-lt p2, v2, :cond_1

    if-eqz v3, :cond_1

    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 444883
    :cond_2
    sget-object v6, LX/33c;->a:[I

    iget-object v7, v4, LX/2e3;->a:LX/0oz;

    invoke-virtual {v7}, LX/0oz;->c()LX/0p3;

    move-result-object v7

    invoke-virtual {v7}, LX/0p3;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 444884
    :pswitch_0
    iget-object v6, v4, LX/2e3;->b:LX/0ad;

    sget v7, LX/2e4;->g:I

    invoke-interface {v6, v7, v5}, LX/0ad;->a(II)I

    move-result v5

    goto :goto_0

    .line 444885
    :pswitch_1
    iget-object v6, v4, LX/2e3;->b:LX/0ad;

    sget v7, LX/2e4;->e:I

    invoke-interface {v6, v7, v5}, LX/0ad;->a(II)I

    move-result v5

    goto :goto_0

    .line 444886
    :pswitch_2
    iget-object v6, v4, LX/2e3;->b:LX/0ad;

    sget v7, LX/2e4;->c:I

    invoke-interface {v6, v7, v5}, LX/0ad;->a(II)I

    move-result v5

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Z
    .locals 1

    .prologue
    .line 444874
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-static {p1}, LX/2e2;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)Z
    .locals 1

    .prologue
    .line 444873
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-virtual {p0, p1, p2}, LX/2e2;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;I)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)V
    .locals 5

    .prologue
    .line 444868
    invoke-static {p1}, LX/2e2;->d(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 444869
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 444870
    :cond_0
    :goto_0
    return-void

    .line 444871
    :cond_1
    iget-object v1, p0, LX/2e2;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 444872
    iget-object v1, p0, LX/2e2;->i:LX/1Ck;

    const-string v2, "FETCH_PAGINATED_PYMK"

    new-instance v3, LX/DEq;

    invoke-direct {v3, p0, p1, v0}, LX/DEq;-><init>(LX/2e2;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    new-instance v4, LX/DEr;

    invoke-direct {v4, p0, p1, v0}, LX/DEr;-><init>(LX/2e2;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public final bridge synthetic b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 0

    .prologue
    .line 444867
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-virtual {p0, p1}, LX/2e2;->b(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)V

    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)V
    .locals 4

    .prologue
    .line 444857
    iget-object v0, p0, LX/2e2;->g:LX/189;

    .line 444858
    new-instance v1, LX/4Xr;

    invoke-direct {v1}, LX/4Xr;-><init>()V

    new-instance v2, LX/17L;

    invoke-direct {v2}, LX/17L;-><init>()V

    const/4 v3, 0x0

    .line 444859
    iput-boolean v3, v2, LX/17L;->d:Z

    .line 444860
    move-object v2, v2

    .line 444861
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 444862
    iput-object v2, v1, LX/4Xr;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 444863
    move-object v1, v1

    .line 444864
    invoke-virtual {v1}, LX/4Xr;->a()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    move-result-object v1

    move-object v0, v1

    .line 444865
    iget-object v1, p0, LX/2e2;->e:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 444866
    return-void
.end method
