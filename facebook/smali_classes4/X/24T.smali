.class public LX/24T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aQ;",
        ">",
        "Ljava/lang/Object;",
        "LX/24J",
        "<",
        "LX/24P;",
        "TV;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/1Rg;

.field private final c:LX/AkM;

.field public final d:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Rg;LX/AkM;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 367210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367211
    iput-object p1, p0, LX/24T;->a:Landroid/content/Context;

    .line 367212
    iput-object p2, p0, LX/24T;->b:LX/1Rg;

    .line 367213
    iput-object p3, p0, LX/24T;->c:LX/AkM;

    .line 367214
    iput-object p4, p0, LX/24T;->d:Landroid/view/View$OnClickListener;

    .line 367215
    return-void
.end method

.method private static a(LX/24T;)F
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 367216
    iget-object v0, p0, LX/24T;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 367217
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const v0, 0x3dcccccd    # 0.1f

    :goto_0
    return v0

    :cond_0
    const v0, 0x3f666666    # 0.9f

    goto :goto_0
.end method

.method private b(Landroid/view/View;)LX/8sj;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "LX/8sj;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const-wide/16 v2, 0xc8

    .line 367218
    iget-object v0, p0, LX/24T;->b:LX/1Rg;

    move-object v1, p1

    check-cast v1, LX/1aQ;

    invoke-interface {v1}, LX/1aQ;->getFlyoutView()Landroid/view/View;

    move-result-object v1

    const/4 v4, 0x0

    move-object v5, p1

    check-cast v5, LX/1aQ;

    invoke-interface {v5}, LX/1aQ;->getExpandedFlyoutHeight()I

    move-result v5

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v7

    .line 367219
    iget-object v0, p0, LX/24T;->b:LX/1Rg;

    move-object v1, p1

    check-cast v1, LX/1aQ;

    invoke-interface {v1}, LX/1aQ;->getFlyoutView()Landroid/view/View;

    move-result-object v1

    const v4, 0x3e4ccccd    # 0.2f

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Ljava/lang/Object;JFFLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    .line 367220
    iget-object v1, p0, LX/24T;->b:LX/1Rg;

    invoke-static {v7, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    new-instance v4, LX/Ak8;

    const/4 v5, 0x1

    invoke-static {p0}, LX/24T;->a(LX/24T;)F

    move-result v6

    iget-object v7, p0, LX/24T;->c:LX/AkM;

    invoke-direct {v4, p1, v5, v6, v7}, LX/Ak8;-><init>(Landroid/view/View;ZFLX/AkM;)V

    invoke-virtual {v1, v0, v2, v3, v4}, LX/1Rg;->a(LX/0Px;JLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    return-object v0
.end method

.method private c(Landroid/view/View;)LX/8sj;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "LX/8sj;"
        }
    .end annotation

    .prologue
    .line 367221
    iget-object v0, p0, LX/24T;->b:LX/1Rg;

    move-object v1, p1

    check-cast v1, LX/1aQ;

    invoke-interface {v1}, LX/1aQ;->getFlyoutView()Landroid/view/View;

    move-result-object v1

    const-wide/16 v2, 0xc8

    move-object v4, p1

    check-cast v4, LX/1aQ;

    invoke-interface {v4}, LX/1aQ;->getExpandedFlyoutHeight()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v7

    .line 367222
    iget-object v0, p0, LX/24T;->b:LX/1Rg;

    move-object v1, p1

    check-cast v1, LX/1aQ;

    invoke-interface {v1}, LX/1aQ;->getFlyoutView()Landroid/view/View;

    move-result-object v1

    const-wide/16 v2, 0xc8

    const/high16 v4, 0x3f800000    # 1.0f

    const v5, 0x3e4ccccd    # 0.2f

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Ljava/lang/Object;JFFLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    .line 367223
    iget-object v1, p0, LX/24T;->b:LX/1Rg;

    invoke-static {v7, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    const-wide/16 v2, 0xc8

    new-instance v4, LX/Ak8;

    const/4 v5, 0x0

    invoke-static {p0}, LX/24T;->a(LX/24T;)F

    move-result v6

    iget-object v7, p0, LX/24T;->c:LX/AkM;

    invoke-direct {v4, p1, v5, v6, v7}, LX/Ak8;-><init>(Landroid/view/View;ZFLX/AkM;)V

    invoke-virtual {v1, v0, v2, v3, v4}, LX/1Rg;->a(LX/0Px;JLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)Ljava/lang/Runnable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 367224
    new-instance v0, Lcom/facebook/feed/inlinecomposer/multirow/common/animations/PromptFlyoutAnimationBuilder$1;

    invoke-direct {v0, p0, p3}, Lcom/facebook/feed/inlinecomposer/multirow/common/animations/PromptFlyoutAnimationBuilder$1;-><init>(LX/24T;Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Ljava/util/List;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 367225
    check-cast p2, LX/24P;

    check-cast p3, LX/24P;

    .line 367226
    if-ne p2, p3, :cond_1

    .line 367227
    :cond_0
    :goto_0
    return-void

    .line 367228
    :cond_1
    sget-object v0, LX/24P;->MAXIMIZED:LX/24P;

    if-ne p2, v0, :cond_2

    .line 367229
    invoke-direct {p0, p4}, LX/24T;->c(Landroid/view/View;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367230
    :cond_2
    sget-object v0, LX/24P;->MAXIMIZED:LX/24P;

    if-ne p3, v0, :cond_0

    .line 367231
    iget-object v0, p0, LX/24T;->b:LX/1Rg;

    const-wide/16 v2, 0xc8

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, LX/1Rg;->a(JLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367232
    invoke-direct {p0, p4}, LX/24T;->b(Landroid/view/View;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
