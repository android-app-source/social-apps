.class public LX/2M5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2M5;


# instance fields
.field public final a:LX/0ad;

.field private final b:Landroid/content/Context;

.field public final c:Landroid/content/res/Resources;

.field public final d:LX/2M6;

.field public final e:LX/2MA;


# direct methods
.method public constructor <init>(LX/0ad;Landroid/content/res/Resources;Landroid/content/Context;LX/0Or;LX/0Or;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "Landroid/content/res/Resources;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/8C6;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2M9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396212
    iput-object p1, p0, LX/2M5;->a:LX/0ad;

    .line 396213
    iput-object p2, p0, LX/2M5;->c:Landroid/content/res/Resources;

    .line 396214
    iput-object p3, p0, LX/2M5;->b:Landroid/content/Context;

    .line 396215
    new-instance v0, LX/2M6;

    invoke-direct {v0}, LX/2M6;-><init>()V

    iput-object v0, p0, LX/2M5;->d:LX/2M6;

    .line 396216
    sget-short v0, LX/2M8;->i:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396217
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2MA;

    iput-object v0, p0, LX/2M5;->e:LX/2MA;

    .line 396218
    :goto_0
    return-void

    .line 396219
    :cond_0
    invoke-interface {p5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2MA;

    iput-object v0, p0, LX/2M5;->e:LX/2MA;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2M5;
    .locals 9

    .prologue
    .line 396220
    sget-object v0, LX/2M5;->f:LX/2M5;

    if-nez v0, :cond_1

    .line 396221
    const-class v1, LX/2M5;

    monitor-enter v1

    .line 396222
    :try_start_0
    sget-object v0, LX/2M5;->f:LX/2M5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 396223
    if-eqz v2, :cond_0

    .line 396224
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 396225
    new-instance v3, LX/2M5;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    const/16 v7, 0x2708

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0xd01

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/2M5;-><init>(LX/0ad;Landroid/content/res/Resources;Landroid/content/Context;LX/0Or;LX/0Or;)V

    .line 396226
    move-object v0, v3

    .line 396227
    sput-object v0, LX/2M5;->f:LX/2M5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396228
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 396229
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 396230
    :cond_1
    sget-object v0, LX/2M5;->f:LX/2M5;

    return-object v0

    .line 396231
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 396232
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2M5;CI)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 396204
    iget-object v0, p0, LX/2M5;->a:LX/0ad;

    const-string v1, "error"

    invoke-interface {v0, p1, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 396205
    const-string v1, "error"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 396206
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, LX/2M5;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a024d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 396207
    :goto_0
    return-object v0

    .line 396208
    :cond_0
    const-string v1, "warning"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 396209
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, LX/2M5;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a024c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0

    .line 396210
    :cond_1
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, LX/2M5;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0
.end method


# virtual methods
.method public final b()LX/6LR;
    .locals 4

    .prologue
    .line 396190
    iget-object v0, p0, LX/2M5;->a:LX/0ad;

    sget-char v1, LX/2M8;->a:C

    iget-object v2, p0, LX/2M5;->c:Landroid/content/res/Resources;

    const v3, 0x7f080585

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 396191
    sget-char v1, LX/2M8;->c:C

    const v2, 0x7f0a0247

    invoke-static {p0, v1, v2}, LX/2M5;->a(LX/2M5;CI)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 396192
    iget-object v2, p0, LX/2M5;->d:LX/2M6;

    .line 396193
    iput-object v0, v2, LX/2M6;->a:Ljava/lang/CharSequence;

    .line 396194
    move-object v0, v2

    .line 396195
    iput-object v1, v0, LX/2M6;->c:Landroid/graphics/drawable/Drawable;

    .line 396196
    move-object v0, v0

    .line 396197
    sget-object v1, LX/2M7;->ALWAYS:LX/2M7;

    .line 396198
    iput-object v1, v0, LX/2M6;->h:LX/2M7;

    .line 396199
    move-object v0, v0

    .line 396200
    const/4 v1, 0x0

    .line 396201
    iput-boolean v1, v0, LX/2M6;->b:Z

    .line 396202
    move-object v0, v0

    .line 396203
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2M6;->a(Ljava/lang/String;)LX/2M6;

    move-result-object v0

    invoke-virtual {v0}, LX/2M6;->a()LX/6LR;

    move-result-object v0

    return-object v0
.end method
