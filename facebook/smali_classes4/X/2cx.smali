.class public final enum LX/2cx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2cx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2cx;

.field public static final enum BLE:LX/2cx;

.field public static final enum FORCE_OFF:LX/2cx;

.field public static final enum GPS:LX/2cx;

.field public static final enum INJECT:LX/2cx;

.field public static final enum POST_COMPOSE:LX/2cx;


# instance fields
.field public final foundPriority:I

.field public final isPersistent:Z

.field public final nothingFoundPriority:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v3, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 442531
    new-instance v0, LX/2cx;

    const-string v1, "POST_COMPOSE"

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, LX/2cx;-><init>(Ljava/lang/String;IIIZ)V

    sput-object v0, LX/2cx;->POST_COMPOSE:LX/2cx;

    .line 442532
    new-instance v0, LX/2cx;

    const-string v1, "BLE"

    invoke-direct {v0, v1, v6, v7, v2}, LX/2cx;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/2cx;->BLE:LX/2cx;

    .line 442533
    new-instance v0, LX/2cx;

    const-string v1, "GPS"

    invoke-direct {v0, v1, v7, v6, v2}, LX/2cx;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/2cx;->GPS:LX/2cx;

    .line 442534
    new-instance v0, LX/2cx;

    const-string v1, "FORCE_OFF"

    const/high16 v4, -0x80000000

    const v5, 0x7fffffff

    invoke-direct {v0, v1, v3, v4, v5}, LX/2cx;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/2cx;->FORCE_OFF:LX/2cx;

    .line 442535
    new-instance v0, LX/2cx;

    const-string v1, "INJECT"

    const v4, 0x7ffffffe

    const v5, 0x7fffffff

    invoke-direct {v0, v1, v8, v4, v5}, LX/2cx;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/2cx;->INJECT:LX/2cx;

    .line 442536
    const/4 v0, 0x5

    new-array v0, v0, [LX/2cx;

    sget-object v1, LX/2cx;->POST_COMPOSE:LX/2cx;

    aput-object v1, v0, v2

    sget-object v1, LX/2cx;->BLE:LX/2cx;

    aput-object v1, v0, v6

    sget-object v1, LX/2cx;->GPS:LX/2cx;

    aput-object v1, v0, v7

    sget-object v1, LX/2cx;->FORCE_OFF:LX/2cx;

    aput-object v1, v0, v3

    sget-object v1, LX/2cx;->INJECT:LX/2cx;

    aput-object v1, v0, v8

    sput-object v0, LX/2cx;->$VALUES:[LX/2cx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 442537
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, LX/2cx;-><init>(Ljava/lang/String;IIIZ)V

    .line 442538
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIZ)V"
        }
    .end annotation

    .prologue
    .line 442539
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 442540
    iput p3, p0, LX/2cx;->foundPriority:I

    .line 442541
    iput p4, p0, LX/2cx;->nothingFoundPriority:I

    .line 442542
    iput-boolean p5, p0, LX/2cx;->isPersistent:Z

    .line 442543
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2cx;
    .locals 1

    .prologue
    .line 442544
    const-class v0, LX/2cx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2cx;

    return-object v0
.end method

.method public static values()[LX/2cx;
    .locals 1

    .prologue
    .line 442545
    sget-object v0, LX/2cx;->$VALUES:[LX/2cx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2cx;

    return-object v0
.end method


# virtual methods
.method public final getNameForAnalytics()Ljava/lang/String;
    .locals 2

    .prologue
    .line 442546
    sget-object v0, LX/3FE;->a:[I

    invoke-virtual {p0}, LX/2cx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 442547
    invoke-virtual {p0}, LX/2cx;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 442548
    :pswitch_0
    const-string v0, "composer"

    goto :goto_0

    .line 442549
    :pswitch_1
    const-string v0, "Bluetooth LE"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
