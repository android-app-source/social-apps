.class public LX/2tM;
.super LX/0rB;
.source ""


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/81E;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 474937
    sget-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->i:Lcom/facebook/api/feedtype/FeedType$Name;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, p1, v1, v2}, LX/0rB;-><init>(Lcom/facebook/api/feedtype/FeedType$Name;LX/0Ot;ZZ)V

    .line 474938
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Lcom/facebook/api/feedtype/FeedType;
    .locals 3

    .prologue
    .line 474939
    new-instance v0, Lcom/facebook/api/feedtype/FeedType;

    const-string v1, "reaction_feed_story_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->i:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    return-object v0
.end method

.method public final a(Landroid/content/Intent;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 474940
    const-string v0, "reaction_feed_title"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
