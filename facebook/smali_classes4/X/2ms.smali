.class public LX/2ms;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Uh;

.field private final b:LX/0Zb;

.field private final c:LX/1Ay;

.field private final d:LX/0bH;

.field private final e:LX/2mt;

.field private final f:LX/17V;

.field private final g:LX/0tX;

.field private final h:Ljava/util/concurrent/Executor;

.field public final i:Lcom/facebook/content/SecureContextHelper;

.field public final j:Landroid/content/Context;

.field public final k:LX/03V;


# direct methods
.method public constructor <init>(LX/0Uh;LX/2mt;LX/0Zb;LX/1Ay;LX/0bH;LX/17V;LX/0tX;Landroid/content/Context;LX/03V;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p11    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 462253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462254
    iput-object p1, p0, LX/2ms;->a:LX/0Uh;

    .line 462255
    iput-object p2, p0, LX/2ms;->e:LX/2mt;

    .line 462256
    iput-object p3, p0, LX/2ms;->b:LX/0Zb;

    .line 462257
    iput-object p4, p0, LX/2ms;->c:LX/1Ay;

    .line 462258
    iput-object p5, p0, LX/2ms;->d:LX/0bH;

    .line 462259
    iput-object p9, p0, LX/2ms;->k:LX/03V;

    .line 462260
    iput-object p7, p0, LX/2ms;->g:LX/0tX;

    .line 462261
    iput-object p8, p0, LX/2ms;->j:Landroid/content/Context;

    .line 462262
    iput-object p10, p0, LX/2ms;->i:Lcom/facebook/content/SecureContextHelper;

    .line 462263
    iput-object p11, p0, LX/2ms;->h:Ljava/util/concurrent/Executor;

    .line 462264
    iput-object p6, p0, LX/2ms;->f:LX/17V;

    .line 462265
    return-void
.end method

.method public static a(LX/0QB;)LX/2ms;
    .locals 1

    .prologue
    .line 462252
    invoke-static {p0}, LX/2ms;->b(LX/0QB;)LX/2ms;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 462245
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 462246
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->l()Ljava/lang/String;

    move-result-object v0

    .line 462247
    :goto_0
    return-object v0

    .line 462248
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->k()Lcom/facebook/graphql/model/GraphQLAd;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 462249
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->k()Lcom/facebook/graphql/model/GraphQLAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAd;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 462250
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2ms;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 462251
    if-eqz v0, :cond_2

    const-string v1, "ad_id"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 462244
    sget-object v0, LX/0ax;->am:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2ms;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 462234
    const v0, 0x7f0d0083

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 462235
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462236
    iget-object v0, p0, LX/2ms;->k:LX/03V;

    const-string v1, "MessengerAdsActionHandler"

    const-string v2, "Null url passed when logging messenger ad click"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 462237
    :goto_0
    return-void

    .line 462238
    :cond_0
    invoke-static {p1}, LX/2mt;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    invoke-static {p1}, LX/2ms;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    const-string v4, "native_newsfeed"

    .line 462239
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v0

    .line 462240
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/1VO;->z(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v6

    move-object v1, p3

    invoke-static/range {v1 .. v6}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 462241
    invoke-static {v0}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 462242
    invoke-static {v0, p2}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 462243
    :cond_1
    iget-object v1, p0, LX/2ms;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_0
.end method

.method public static a(LX/2ms;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/7gH;)V
    .locals 3
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/attachments/angora/actionbutton/messengerads/MessengerAdsActionHandler$Listener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 462203
    new-instance v0, LX/4Dy;

    invoke-direct {v0}, LX/4Dy;-><init>()V

    .line 462204
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 462205
    move-object v0, v0

    .line 462206
    const-string v1, "ad_id"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 462207
    move-object v0, v0

    .line 462208
    new-instance v1, LX/6AN;

    invoke-direct {v1}, LX/6AN;-><init>()V

    move-object v1, v1

    .line 462209
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 462210
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 462211
    iget-object v1, p0, LX/2ms;->g:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 462212
    new-instance v1, LX/7gE;

    invoke-direct {v1, p0, p5}, LX/7gE;-><init>(LX/2ms;LX/7gH;)V

    iget-object v2, p0, LX/2ms;->h:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 462213
    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 462214
    iget-object v0, p0, LX/2ms;->k:LX/03V;

    const-string v1, "MessengerAdsActionHandler"

    const-string v2, "Null url passed when sending message"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 462215
    :cond_0
    :goto_0
    return-void

    .line 462216
    :cond_1
    invoke-static {p3}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 462217
    if-nez v0, :cond_2

    .line 462218
    iget-object v0, p0, LX/2ms;->k:LX/03V;

    const-string v1, "MessengerAdsActionHandler"

    const-string v2, "Parent Story is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 462219
    :cond_2
    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 462220
    if-eqz v0, :cond_3

    .line 462221
    iget-object v1, p0, LX/2ms;->c:LX/1Ay;

    invoke-virtual {v1, v0, p4}, LX/1Ay;->a(LX/0lF;Ljava/lang/String;)V

    .line 462222
    :cond_3
    invoke-static {p3}, LX/2ms;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 462223
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v0

    if-lez v0, :cond_0

    .line 462224
    const/4 v0, 0x0

    .line 462225
    invoke-static {p3}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 462226
    if-nez v2, :cond_4

    .line 462227
    :goto_1
    move-object v0, v0

    .line 462228
    if-eqz v0, :cond_0

    .line 462229
    iget-object v1, p0, LX/2ms;->d:LX/0bH;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 462230
    :cond_4
    invoke-static {p3}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 462231
    if-eqz v1, :cond_5

    if-eq v1, v2, :cond_5

    .line 462232
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 462233
    :cond_5
    new-instance v1, LX/1ZS;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/1ZS;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1
.end method

.method private static b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 462266
    invoke-static {p0}, LX/1WF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 462267
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2ms;
    .locals 12

    .prologue
    .line 462201
    new-instance v0, LX/2ms;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0}, LX/2mt;->a(LX/0QB;)LX/2mt;

    move-result-object v2

    check-cast v2, LX/2mt;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {p0}, LX/1Ay;->a(LX/0QB;)LX/1Ay;

    move-result-object v4

    check-cast v4, LX/1Ay;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v5

    check-cast v5, LX/0bH;

    invoke-static {p0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v6

    check-cast v6, LX/17V;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    const-class v8, Landroid/content/Context;

    invoke-interface {p0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/Executor;

    invoke-direct/range {v0 .. v11}, LX/2ms;-><init>(LX/0Uh;LX/2mt;LX/0Zb;LX/1Ay;LX/0bH;LX/17V;LX/0tX;Landroid/content/Context;LX/03V;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/Executor;)V

    .line 462202
    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 462170
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 462171
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    .line 462172
    :goto_0
    return-object v0

    .line 462173
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2ms;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 462174
    if-eqz v0, :cond_1

    const-string v1, "page_id"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 462193
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 462194
    :cond_0
    :goto_0
    return-object v0

    .line 462195
    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 462196
    const-string v2, "href"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 462197
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 462198
    :try_start_0
    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 462199
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 462200
    :catch_0
    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 462191
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2ms;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 462192
    if-eqz v0, :cond_0

    const-string v1, "behavior"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 462186
    iget-object v0, p0, LX/2ms;->a:LX/0Uh;

    const/16 v1, 0x1f5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462187
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 462188
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->A(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462189
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/2ms;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/7gH;)V

    .line 462190
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/7gH;)V
    .locals 6
    .param p3    # LX/7gH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/attachments/angora/actionbutton/messengerads/MessengerAdsActionHandler$Listener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 462175
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 462176
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v1, -0x1e53800c

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 462177
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 462178
    invoke-static {v0}, LX/2ms;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v1

    .line 462179
    invoke-static {v0}, LX/2ms;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v2

    .line 462180
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v4

    .line 462181
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 462182
    :cond_0
    iget-object v0, p0, LX/2ms;->k:LX/03V;

    const-string v1, "MessengerAdsActionHandler"

    const-string v2, "send message failed: To send a messenger ad, a valid adid, pageid and url is needed"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 462183
    :goto_0
    return-void

    .line 462184
    :cond_1
    invoke-static {p0, p2, p1, v4}, LX/2ms;->a(LX/2ms;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;)V

    move-object v0, p0

    move-object v3, p2

    move-object v5, p3

    .line 462185
    invoke-static/range {v0 .. v5}, LX/2ms;->a(LX/2ms;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/7gH;)V

    goto :goto_0
.end method
