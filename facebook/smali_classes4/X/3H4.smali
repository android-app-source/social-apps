.class public LX/3H4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3H4;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 542659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 542660
    iput-object p1, p0, LX/3H4;->a:LX/0Zb;

    .line 542661
    return-void
.end method

.method public static a(LX/0QB;)LX/3H4;
    .locals 4

    .prologue
    .line 542662
    sget-object v0, LX/3H4;->b:LX/3H4;

    if-nez v0, :cond_1

    .line 542663
    const-class v1, LX/3H4;

    monitor-enter v1

    .line 542664
    :try_start_0
    sget-object v0, LX/3H4;->b:LX/3H4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 542665
    if-eqz v2, :cond_0

    .line 542666
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 542667
    new-instance p0, LX/3H4;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/3H4;-><init>(LX/0Zb;)V

    .line 542668
    move-object v0, p0

    .line 542669
    sput-object v0, LX/3H4;->b:LX/3H4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 542670
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 542671
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 542672
    :cond_1
    sget-object v0, LX/3H4;->b:LX/3H4;

    return-object v0

    .line 542673
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 542674
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/BSX;LX/BSQ;)V
    .locals 5

    .prologue
    .line 542675
    iget-object v0, p2, LX/BSQ;->b:LX/2oN;

    iget-object v1, p2, LX/BSQ;->c:LX/2oN;

    .line 542676
    sget-object v2, LX/BSO;->d:[I

    invoke-virtual {v0}, LX/2oN;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 542677
    sget-object v2, LX/BSV;->LIVE:LX/BSV;

    :goto_0
    move-object v1, v2

    .line 542678
    sget-object v0, LX/BSO;->c:[I

    invoke-virtual {p1}, LX/BSX;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    .line 542679
    const/4 v0, 0x0

    .line 542680
    :goto_1
    if-nez v0, :cond_0

    .line 542681
    :goto_2
    return-void

    .line 542682
    :pswitch_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "commercial_break_inline_to_fullscreen"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 542683
    :pswitch_1
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "commercial_break_fullscreen_to_inline"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 542684
    :pswitch_2
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "commercial_break_scrolled_away"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 542685
    :pswitch_3
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "commercial_break_scrolled_into"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 542686
    :pswitch_4
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "commercial_break_hide_ad"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 542687
    :pswitch_5
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "commercial_break_pause_ad"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 542688
    :pswitch_6
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "commercial_break_play_ad"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 542689
    :cond_0
    const-string v2, "commercial_break"

    .line 542690
    iput-object v2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 542691
    move-object v2, v0

    .line 542692
    const-string v3, "host_video_id"

    iget-object v4, p2, LX/BSQ;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "instream_video_ad_type"

    iget-object v4, p2, LX/BSQ;->d:LX/3H0;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "commercial_break_status"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "pause_ad_event_trigger_type"

    iget-object v3, p2, LX/BSQ;->e:LX/04g;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "is_sponsored"

    iget-boolean v3, p2, LX/BSQ;->f:Z

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "player_origin"

    iget-object v3, p2, LX/BSQ;->g:LX/04D;

    iget-object v3, v3, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ad_break_index"

    iget v3, p2, LX/BSQ;->h:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 542693
    iget-object v1, p0, LX/3H4;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_2

    .line 542694
    :pswitch_7
    sget-object v2, LX/2oN;->NONE:LX/2oN;

    if-ne v1, v2, :cond_1

    .line 542695
    sget-object v2, LX/BSV;->PRE_AD:LX/BSV;

    goto/16 :goto_0

    .line 542696
    :cond_1
    sget-object v2, LX/BSV;->POST_AD:LX/BSV;

    goto/16 :goto_0

    .line 542697
    :pswitch_8
    sget-object v2, LX/BSV;->COUNTDOWN:LX/BSV;

    goto/16 :goto_0

    .line 542698
    :pswitch_9
    sget-object v2, LX/BSV;->VIDEO_AD:LX/BSV;

    goto/16 :goto_0

    .line 542699
    :pswitch_a
    sget-object v2, LX/BSV;->WAIT_FOR:LX/BSV;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;LX/BSS;LX/BSP;LX/3H0;)V
    .locals 4

    .prologue
    .line 542700
    sget-object v0, LX/BSO;->b:[I

    invoke-virtual {p2}, LX/BSS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 542701
    const/4 v0, 0x0

    .line 542702
    :goto_0
    if-nez v0, :cond_0

    .line 542703
    :goto_1
    return-void

    .line 542704
    :pswitch_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "commercial_break_start"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "playback_trigger_event"

    iget-object v2, p3, LX/BSP;->d:LX/BSU;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "timestamp_latency_ms"

    iget-wide v2, p3, LX/BSP;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "timestamp_delta_ms"

    iget-wide v2, p3, LX/BSP;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "streaming_format"

    iget-object v2, p3, LX/BSP;->f:LX/BSW;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "viewer_count"

    iget v2, p3, LX/BSP;->h:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    goto :goto_0

    .line 542705
    :pswitch_1
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "commercial_break_start_ad"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "commercial_break_ad_client_token"

    iget-object v2, p3, LX/BSP;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    goto :goto_0

    .line 542706
    :pswitch_2
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "commercial_break_transit"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 542707
    :pswitch_3
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "commercial_break_wait_for"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 542708
    :pswitch_4
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "commercial_break_static_countdown"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "commercial_break_no_ad_reason"

    iget-object v2, p3, LX/BSP;->e:LX/BST;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    goto :goto_0

    .line 542709
    :pswitch_5
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "commercial_break_end"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "ending_reason"

    iget-object v2, p3, LX/BSP;->c:LX/BSR;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    goto :goto_0

    .line 542710
    :pswitch_6
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "commercial_break_no_ad_transition"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "commercial_break_no_ad_reason"

    iget-object v2, p3, LX/BSP;->e:LX/BST;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    goto/16 :goto_0

    .line 542711
    :pswitch_7
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "commercial_break_start_show_nothing"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 542712
    :cond_0
    const-string v1, "commercial_break"

    .line 542713
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 542714
    move-object v1, v0

    .line 542715
    const-string v2, "host_video_id"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "instream_video_ad_type"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "is_sponsored"

    iget-boolean v3, p3, LX/BSP;->g:Z

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "player_origin"

    iget-object v3, p3, LX/BSP;->j:LX/04D;

    iget-object v3, v3, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ad_break_index"

    iget v3, p3, LX/BSP;->k:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 542716
    iget-object v1, p0, LX/3H4;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;ZZ)V
    .locals 3

    .prologue
    .line 542717
    if-eqz p2, :cond_0

    const-string v0, "commercial_break_skywalker_subscription_success"

    .line 542718
    :goto_0
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "commercial_break"

    .line 542719
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 542720
    move-object v0, v1

    .line 542721
    const-string v1, "host_video_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "instream_video_ad_type"

    sget-object v2, LX/3H0;->LIVE:LX/3H0;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "is_sponsored"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 542722
    iget-object v1, p0, LX/3H4;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 542723
    return-void

    .line 542724
    :cond_0
    const-string v0, "commercial_break_skywalker_subscription_failure"

    goto :goto_0
.end method
