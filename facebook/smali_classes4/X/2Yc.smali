.class public LX/2Yc;
.super Lcom/facebook/http/tigon/Tigon4aHttpService;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private final a:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0s9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 421109
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2Yc;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0Or;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "LX/0Or",
            "<",
            "LX/0s9;",
            ">;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 421110
    invoke-direct {p0, p1}, Lcom/facebook/http/tigon/Tigon4aHttpService;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    .line 421111
    iput-object p2, p0, LX/2Yc;->b:LX/0Or;

    .line 421112
    iput-object p3, p0, LX/2Yc;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 421113
    return-void
.end method

.method private a(Lcom/facebook/tigon/iface/TigonRequestBuilder;)V
    .locals 4

    .prologue
    .line 421114
    iget-object v0, p0, LX/2Yc;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 421115
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, v1

    .line 421116
    if-nez v0, :cond_0

    .line 421117
    new-instance v0, LX/4cl;

    const-string v1, "Auth token is null; user logged out?"

    invoke-direct {v0, v1}, LX/4cl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 421118
    :cond_0
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OAuth "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 421119
    return-void
.end method

.method public static b(LX/0QB;)LX/2Yc;
    .locals 9

    .prologue
    .line 421120
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 421121
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 421122
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 421123
    if-nez v1, :cond_0

    .line 421124
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 421125
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 421126
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 421127
    sget-object v1, LX/2Yc;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 421128
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 421129
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 421130
    :cond_1
    if-nez v1, :cond_4

    .line 421131
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 421132
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 421133
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 421134
    new-instance v8, LX/2Yc;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v1

    check-cast v1, Lcom/facebook/http/common/FbHttpRequestProcessor;

    const/16 v7, 0xb5a

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v7

    check-cast v7, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct {v8, v1, p0, v7}, LX/2Yc;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0Or;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 421135
    move-object v1, v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 421136
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 421137
    if-nez v1, :cond_2

    .line 421138
    sget-object v0, LX/2Yc;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Yc;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 421139
    :goto_1
    if-eqz v0, :cond_3

    .line 421140
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 421141
    :goto_3
    check-cast v0, LX/2Yc;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 421142
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 421143
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 421144
    :catchall_1
    move-exception v0

    .line 421145
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 421146
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 421147
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 421148
    :cond_2
    :try_start_8
    sget-object v0, LX/2Yc;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Yc;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/tigon/iface/TigonRequest;Lorg/apache/http/HttpEntity;LX/2Ys;)LX/1j2;
    .locals 2
    .param p2    # Lorg/apache/http/HttpEntity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/tigon/iface/TigonRequest;",
            "Lorg/apache/http/HttpEntity;",
            "LX/2Ys;",
            ")",
            "LX/1j2",
            "<",
            "LX/2aS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 421149
    new-instance v0, Lcom/facebook/tigon/iface/TigonRequestBuilder;

    invoke-direct {v0, p1}, Lcom/facebook/tigon/iface/TigonRequestBuilder;-><init>(Lcom/facebook/tigon/iface/TigonRequest;)V

    .line 421150
    invoke-direct {p0, v0}, LX/2Yc;->a(Lcom/facebook/tigon/iface/TigonRequestBuilder;)V

    .line 421151
    iget-object v1, p0, LX/2Yc;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0s9;

    .line 421152
    invoke-interface {v1}, LX/0s9;->i()Ljava/lang/String;

    move-result-object v1

    .line 421153
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 421154
    const-string p1, "X-FB-Connection-Type"

    invoke-virtual {v0, p1, v1}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 421155
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a()Lcom/facebook/tigon/iface/TigonRequest;

    move-result-object v0

    invoke-super {p0, v0, p2, p3}, Lcom/facebook/http/tigon/Tigon4aHttpService;->a(Lcom/facebook/tigon/iface/TigonRequest;Lorg/apache/http/HttpEntity;LX/2Ys;)LX/1j2;

    move-result-object v0

    return-object v0
.end method
