.class public LX/2Pq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/omnistore/OmnistoreErrorReporter;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile sInstance__com_facebook_omnistore_logger_FbOmnistoreErrorReporter__INJECTED_BY_TemplateInjector:LX/2Pq;


# instance fields
.field private final mFbErrorReporter:LX/03V;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 407244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407245
    iput-object p1, p0, LX/2Pq;->mFbErrorReporter:LX/03V;

    .line 407246
    return-void
.end method

.method public static getInstance__com_facebook_omnistore_logger_FbOmnistoreErrorReporter__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pq;
    .locals 4

    .prologue
    .line 407247
    sget-object v0, LX/2Pq;->sInstance__com_facebook_omnistore_logger_FbOmnistoreErrorReporter__INJECTED_BY_TemplateInjector:LX/2Pq;

    if-nez v0, :cond_1

    .line 407248
    const-class v1, LX/2Pq;

    monitor-enter v1

    .line 407249
    :try_start_0
    sget-object v0, LX/2Pq;->sInstance__com_facebook_omnistore_logger_FbOmnistoreErrorReporter__INJECTED_BY_TemplateInjector:LX/2Pq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 407250
    if-eqz v2, :cond_0

    .line 407251
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 407252
    new-instance p0, LX/2Pq;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/2Pq;-><init>(LX/03V;)V

    .line 407253
    move-object v0, p0

    .line 407254
    sput-object v0, LX/2Pq;->sInstance__com_facebook_omnistore_logger_FbOmnistoreErrorReporter__INJECTED_BY_TemplateInjector:LX/2Pq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 407255
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 407256
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 407257
    :cond_1
    sget-object v0, LX/2Pq;->sInstance__com_facebook_omnistore_logger_FbOmnistoreErrorReporter__INJECTED_BY_TemplateInjector:LX/2Pq;

    return-object v0

    .line 407258
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 407259
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public reportError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 407260
    iget-object v0, p0, LX/2Pq;->mFbErrorReporter:LX/03V;

    invoke-virtual {v0, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 407261
    return-void
.end method
