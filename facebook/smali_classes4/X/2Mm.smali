.class public LX/2Mm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2Mm;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 397285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397286
    iput-object p1, p0, LX/2Mm;->a:LX/0Zb;

    .line 397287
    return-void
.end method

.method public static a(LX/0QB;)LX/2Mm;
    .locals 4

    .prologue
    .line 397272
    sget-object v0, LX/2Mm;->b:LX/2Mm;

    if-nez v0, :cond_1

    .line 397273
    const-class v1, LX/2Mm;

    monitor-enter v1

    .line 397274
    :try_start_0
    sget-object v0, LX/2Mm;->b:LX/2Mm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 397275
    if-eqz v2, :cond_0

    .line 397276
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 397277
    new-instance p0, LX/2Mm;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/2Mm;-><init>(LX/0Zb;)V

    .line 397278
    move-object v0, p0

    .line 397279
    sput-object v0, LX/2Mm;->b:LX/2Mm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397280
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 397281
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 397282
    :cond_1
    sget-object v0, LX/2Mm;->b:LX/2Mm;

    return-object v0

    .line 397283
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 397284
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2Mm;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 397288
    iget-object v0, p0, LX/2Mm;->a:LX/0Zb;

    invoke-interface {v0, p1, p2}, LX/0Zb;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 397289
    return-void
.end method

.method public static b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 397268
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 397269
    const-string v1, "offline_threading_id"

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397270
    const-string v1, "media_type"

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v2}, LX/2MK;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397271
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 3

    .prologue
    .line 397263
    invoke-static {p1}, LX/2Mm;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object v0

    .line 397264
    const-string v1, "media_source"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v2}, LX/5zj;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397265
    const-string v1, "media_mime_type"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397266
    const-string v0, "messenger_media_upload_request_started"

    invoke-static {p1}, LX/2Mm;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/2Mm;->a(LX/2Mm;Ljava/lang/String;Ljava/util/Map;)V

    .line 397267
    return-void
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 397259
    invoke-static {p1}, LX/2Mm;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object v0

    .line 397260
    const-string v1, "result_path"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397261
    const-string v1, "messenger_media_upload_request_finished"

    invoke-static {p0, v1, v0}, LX/2Mm;->a(LX/2Mm;Ljava/lang/String;Ljava/util/Map;)V

    .line 397262
    return-void
.end method
