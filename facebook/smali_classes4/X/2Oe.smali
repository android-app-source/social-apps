.class public LX/2Oe;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2OQ;

.field public final b:LX/2Of;

.field private final c:LX/2CH;

.field private final d:LX/2Ou;

.field private final e:LX/0SI;

.field private final f:LX/1mR;

.field private final g:LX/2Ov;

.field private final h:LX/2Ow;

.field private final i:LX/2Mk;

.field private final j:LX/2MR;

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/2Oi;


# direct methods
.method public constructor <init>(LX/2OQ;LX/2Of;LX/2CH;LX/2Ou;LX/0SI;LX/1mR;LX/2Ov;LX/2Ow;LX/2Mk;LX/2MR;LX/0Or;LX/2Oi;)V
    .locals 0
    .param p1    # LX/2OQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2OQ;",
            "LX/2Of;",
            "Lcom/facebook/presence/PresenceManager;",
            "LX/2Ou;",
            "LX/0SI;",
            "LX/1mR;",
            "LX/2Ov;",
            "LX/2Ow;",
            "LX/2Mk;",
            "LX/2MR;",
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;",
            "LX/2Oi;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 402666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402667
    iput-object p1, p0, LX/2Oe;->a:LX/2OQ;

    .line 402668
    iput-object p2, p0, LX/2Oe;->b:LX/2Of;

    .line 402669
    iput-object p3, p0, LX/2Oe;->c:LX/2CH;

    .line 402670
    iput-object p4, p0, LX/2Oe;->d:LX/2Ou;

    .line 402671
    iput-object p5, p0, LX/2Oe;->e:LX/0SI;

    .line 402672
    iput-object p6, p0, LX/2Oe;->f:LX/1mR;

    .line 402673
    iput-object p7, p0, LX/2Oe;->g:LX/2Ov;

    .line 402674
    iput-object p8, p0, LX/2Oe;->h:LX/2Ow;

    .line 402675
    iput-object p9, p0, LX/2Oe;->i:LX/2Mk;

    .line 402676
    iput-object p10, p0, LX/2Oe;->j:LX/2MR;

    .line 402677
    iput-object p11, p0, LX/2Oe;->k:LX/0Or;

    .line 402678
    iput-object p12, p0, LX/2Oe;->l:LX/2Oi;

    .line 402679
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 402616
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v0, p1, p2, p3}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0P1;)Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402617
    new-instance v2, Ljava/util/HashSet;

    invoke-virtual {p1}, LX/0P1;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 402618
    invoke-virtual {p1}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 402619
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v1, v4, v5}, LX/2Oe;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 402620
    if-eqz v0, :cond_0

    .line 402621
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 402622
    :cond_1
    return-object v2
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 402623
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v0}, LX/2OQ;->a()V

    .line 402624
    iget-object v0, p0, LX/2Oe;->b:LX/2Of;

    invoke-virtual {v0}, LX/2Of;->a()V

    .line 402625
    return-void
.end method

.method public final a(ILcom/facebook/messaging/service/model/FetchThreadResult;Z)V
    .locals 9

    .prologue
    .line 402626
    iget-object v2, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 402627
    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    if-eqz v0, :cond_0

    .line 402628
    iget-object v0, p0, LX/2Oe;->l:LX/2Oi;

    iget-object v1, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    invoke-virtual {v0, v1}, LX/2Oi;->a(Ljava/util/Collection;)V

    .line 402629
    :cond_0
    iget-object v0, p0, LX/2Oe;->b:LX/2Of;

    invoke-virtual {v0}, LX/2Of;->a()V

    .line 402630
    if-eqz v2, :cond_4

    .line 402631
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    iget-wide v4, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    invoke-virtual {v0, v2, v4, v5}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 402632
    if-lez p1, :cond_1

    .line 402633
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    iget-object v1, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v0, v1}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;)V

    .line 402634
    :cond_1
    iget-object v0, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v0, v1, :cond_2

    .line 402635
    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 402636
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v3, v1

    .line 402637
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 402638
    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    .line 402639
    iget-object v6, p0, LX/2Oe;->e:LX/0SI;

    invoke-interface {v6}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v6

    .line 402640
    iget-object v7, v6, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v6, v7

    .line 402641
    new-instance v7, Lcom/facebook/user/model/UserKey;

    sget-object v8, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v7, v8, v6}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 402642
    invoke-static {v5, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 402643
    iget-object v1, p0, LX/2Oe;->b:LX/2Of;

    iget-wide v4, v0, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v1, v7, v4, v5}, LX/2Of;->a(Lcom/facebook/user/model/UserKey;J)V

    .line 402644
    :cond_2
    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    sget-object v1, LX/6ek;->MONTAGE:LX/6ek;

    if-eq v0, v1, :cond_6

    .line 402645
    :cond_3
    :goto_1
    if-eqz p3, :cond_4

    .line 402646
    iget-object v0, p0, LX/2Oe;->h:LX/2Ow;

    iget-object v1, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    .line 402647
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 402648
    sget-object v5, LX/0aY;->c:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0, v5, v1, v2, v3}, LX/2Ow;->a(LX/2Ow;Ljava/lang/String;Ljava/util/ArrayList;J)V

    .line 402649
    :cond_4
    return-void

    .line 402650
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 402651
    :cond_6
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v0, p2}, LX/2OQ;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    goto :goto_1
.end method

.method public final a(LX/6ek;J)V
    .locals 8

    .prologue
    .line 402652
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v0, p1}, LX/2OQ;->e(LX/6ek;)Lcom/facebook/messaging/model/folders/FolderCounts;

    move-result-object v0

    .line 402653
    if-eqz v0, :cond_0

    .line 402654
    new-instance v1, Lcom/facebook/messaging/model/folders/FolderCounts;

    iget v2, v0, Lcom/facebook/messaging/model/folders/FolderCounts;->b:I

    const/4 v3, 0x0

    iget-wide v6, v0, Lcom/facebook/messaging/model/folders/FolderCounts;->e:J

    move-wide v4, p2

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/model/folders/FolderCounts;-><init>(IIJJ)V

    .line 402655
    invoke-virtual {p0, p1, v1}, LX/2Oe;->a(LX/6ek;Lcom/facebook/messaging/model/folders/FolderCounts;)V

    .line 402656
    :cond_0
    return-void
.end method

.method public final a(LX/6ek;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ek;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 402657
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v0, p1, p2}, LX/2OQ;->a(LX/6ek;LX/0Px;)V

    .line 402658
    iget-object v0, p0, LX/2Oe;->h:LX/2Ow;

    invoke-virtual {v0, p2}, LX/2Ow;->c(LX/0Px;)V

    .line 402659
    return-void
.end method

.method public final a(LX/6ek;Lcom/facebook/messaging/model/folders/FolderCounts;)V
    .locals 2

    .prologue
    .line 402660
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v0, p1, p2}, LX/2OQ;->a(LX/6ek;Lcom/facebook/messaging/model/folders/FolderCounts;)V

    .line 402661
    iget-object v0, p0, LX/2Oe;->h:LX/2Ow;

    .line 402662
    new-instance v1, Landroid/content/Intent;

    sget-object p0, LX/0aY;->O:Ljava/lang/String;

    invoke-direct {v1, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 402663
    const-string p0, "folder_name"

    invoke-virtual {p1}, LX/6ek;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 402664
    invoke-static {v0, v1}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 402665
    return-void
.end method

.method public final a(LX/6ek;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 1

    .prologue
    .line 402680
    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/2Oe;->a(LX/6ek;LX/0Px;)V

    .line 402681
    return-void
.end method

.method public final a(LX/6ek;Lcom/facebook/messaging/service/model/DeleteMessagesResult;)V
    .locals 4

    .prologue
    .line 402682
    iget-object v0, p2, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 402683
    if-nez v0, :cond_1

    .line 402684
    :cond_0
    :goto_0
    return-void

    .line 402685
    :cond_1
    iget-object v1, p2, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->d:LX/0Rf;

    .line 402686
    iget-object v2, p0, LX/2Oe;->j:LX/2MR;

    iget-object v3, p2, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->f:LX/0Rf;

    invoke-virtual {v2, v3}, LX/2MR;->a(Ljava/util/Set;)V

    .line 402687
    iget-object v2, p0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v2, v0, v1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Set;)V

    .line 402688
    iget-boolean v1, p2, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->g:Z

    if-eqz v1, :cond_2

    .line 402689
    invoke-virtual {p0, p1, v0}, LX/2Oe;->a(LX/6ek;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_0

    .line 402690
    :cond_2
    iget-object v0, p2, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v0, :cond_0

    .line 402691
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    iget-object v1, p2, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v0, v1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 1

    .prologue
    .line 402692
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v0, p1}, LX/2OQ;->b(Lcom/facebook/messaging/model/messages/Message;)V

    .line 402693
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;J)V
    .locals 8

    .prologue
    .line 402694
    iget-object v1, p0, LX/2Oe;->a:LX/2OQ;

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v2, p1

    move-wide v4, p2

    invoke-virtual/range {v1 .. v6}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLjava/lang/Boolean;)V

    .line 402695
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 5

    .prologue
    .line 402606
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    new-instance v1, LX/6ia;

    invoke-direct {v1}, LX/6ia;-><init>()V

    .line 402607
    iput-object p1, v1, LX/6ia;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 402608
    move-object v1, v1

    .line 402609
    const-wide/16 v2, 0x0

    .line 402610
    iput-wide v2, v1, LX/6ia;->c:J

    .line 402611
    move-object v1, v1

    .line 402612
    invoke-virtual {v1}, LX/6ia;->a()Lcom/facebook/messaging/service/model/MarkThreadFields;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2OQ;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)V

    .line 402613
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJ)V
    .locals 6

    .prologue
    .line 402614
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJ)V

    .line 402615
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Z)V
    .locals 1

    .prologue
    .line 402491
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v0, p1, p2}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Z)V

    .line 402492
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V
    .locals 2

    .prologue
    .line 402495
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v0, p1, p2, p3}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 402496
    return-void
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;Z)V
    .locals 4

    .prologue
    .line 402497
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 402498
    :goto_0
    if-nez v0, :cond_1

    .line 402499
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->b:J

    invoke-virtual {v0, v1, v2, v3, p2}, LX/2OQ;->a(Ljava/util/List;JZ)V

    .line 402500
    :goto_1
    return-void

    .line 402501
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 402502
    :cond_1
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->b:J

    invoke-virtual {v0, v2, v3}, LX/2OQ;->a(J)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 402503
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->b:LX/6ek;

    .line 402504
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->d:LX/0Px;

    .line 402505
    iget-object v2, p0, LX/2Oe;->l:LX/2Oi;

    invoke-virtual {v2, v1}, LX/2Oi;->a(Ljava/util/Collection;)V

    .line 402506
    iget-object v1, p0, LX/2Oe;->a:LX/2OQ;

    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    invoke-virtual {v1, v0, v2}, LX/2OQ;->a(LX/6ek;Lcom/facebook/messaging/model/folders/FolderCounts;)V

    .line 402507
    iget-object v1, p0, LX/2Oe;->a:LX/2OQ;

    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->b:LX/6ek;

    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    iget-wide v4, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->l:J

    invoke-virtual/range {v1 .. v6}, LX/2OQ;->a(LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;JZ)V

    .line 402508
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->b:LX/6ek;

    sget-object v1, LX/6ek;->INBOX:LX/6ek;

    if-ne v0, v1, :cond_3

    .line 402509
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 402510
    iget-object v0, p0, LX/2Oe;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->fZ:J

    const/16 v4, 0xa

    invoke-interface {v0, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v2

    .line 402511
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 402512
    iget-object v3, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v3, v3

    .line 402513
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_0
    if-ge v6, v4, :cond_1

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 402514
    iget-object v5, p0, LX/2Oe;->d:LX/2Ou;

    invoke-virtual {v5, v0}, LX/2Ou;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    .line 402515
    if-eqz v0, :cond_0

    .line 402516
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 402517
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v2, :cond_1

    .line 402518
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 402519
    :cond_1
    iget-object v0, p0, LX/2Oe;->c:LX/2CH;

    .line 402520
    const/4 v2, 0x0

    .line 402521
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/UserKey;

    .line 402522
    iget-object v3, v0, LX/2CH;->B:LX/0aq;

    invoke-virtual {v3, v2, v2}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/UserKey;

    .line 402523
    if-nez v3, :cond_4

    iget-object v3, v0, LX/2CH;->A:LX/0aq;

    invoke-virtual {v3, v2}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_4

    iget-object v3, v0, LX/2CH;->C:LX/0UE;

    invoke-virtual {v3, v2}, LX/0UE;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, v0, LX/2CH;->D:LX/0UE;

    invoke-virtual {v3, v2}, LX/0UE;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 402524
    const/4 v4, 0x1

    move v2, v4

    :goto_2
    move v4, v2

    .line 402525
    goto :goto_1

    .line 402526
    :cond_2
    if-eqz v4, :cond_3

    .line 402527
    invoke-virtual {v0}, LX/2CH;->b()V

    .line 402528
    :cond_3
    iget-object v0, p0, LX/2Oe;->b:LX/2Of;

    invoke-virtual {v0}, LX/2Of;->a()V

    .line 402529
    return-void

    :cond_4
    move v2, v4

    goto :goto_2
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadResult;)V
    .locals 4

    .prologue
    .line 402530
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 402531
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    if-eqz v1, :cond_0

    .line 402532
    iget-object v1, p0, LX/2Oe;->l:LX/2Oi;

    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    invoke-virtual {v1, v2}, LX/2Oi;->a(Ljava/util/Collection;)V

    .line 402533
    :cond_0
    iget-object v1, p0, LX/2Oe;->a:LX/2OQ;

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    invoke-virtual {v1, v0, v2, v3}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 402534
    iget-object v1, p0, LX/2Oe;->a:LX/2OQ;

    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v1, v0, v2}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/MessagesCollection;)V

    .line 402535
    iget-object v1, p0, LX/2Oe;->b:LX/2Of;

    invoke-virtual {v1}, LX/2Of;->a()V

    .line 402536
    iget-object v1, p0, LX/2Oe;->h:LX/2Ow;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v0}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 402537
    return-void
.end method

.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;)V
    .locals 7

    .prologue
    .line 402538
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v2, v0

    .line 402539
    iget-object v1, p0, LX/2Oe;->a:LX/2OQ;

    .line 402540
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-object v3, v0

    .line 402541
    const-wide/16 v4, -0x1

    .line 402542
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->d:Ljava/lang/Boolean;

    move-object v6, v0

    .line 402543
    invoke-virtual/range {v1 .. v6}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLjava/lang/Boolean;)V

    .line 402544
    iget-object v0, p0, LX/2Oe;->b:LX/2Of;

    iget-object v1, v2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    iget-wide v2, v2, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/2Of;->a(Lcom/facebook/user/model/UserKey;J)V

    .line 402545
    iget-object v0, p0, LX/2Oe;->h:LX/2Ow;

    invoke-virtual {v0}, LX/2Ow;->c()V

    .line 402546
    return-void
.end method

.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;J)V
    .locals 2

    .prologue
    .line 402493
    sget-object v0, LX/6jT;->a:LX/6jT;

    invoke-virtual {p0, p1, p2, p3, v0}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/NewMessageResult;JLX/6jT;)V

    .line 402494
    return-void
.end method

.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;JLX/6jT;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 402547
    iget-object v1, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v2, v1

    .line 402548
    iget-object v1, p0, LX/2Oe;->a:LX/2OQ;

    .line 402549
    iget-object v3, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-object v3, v3

    .line 402550
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    move-wide v4, p2

    move-object v6, p4

    invoke-virtual/range {v1 .. v7}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLX/6jT;Ljava/lang/Boolean;)V

    .line 402551
    iget-object v3, v2, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 402552
    iget-object v0, v0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    if-eqz v0, :cond_4

    .line 402553
    iget-object v0, p0, LX/2Oe;->g:LX/2Ov;

    iget-object v1, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Ov;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0zO;

    move-result-object v0

    .line 402554
    iget-object v1, p0, LX/2Oe;->f:LX/1mR;

    invoke-virtual {v1, v0}, LX/1mR;->a(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 402555
    :cond_0
    iget-object v0, p0, LX/2Oe;->i:LX/2Mk;

    invoke-virtual {v0, v2}, LX/2Mk;->s(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 402556
    iget-object v0, p0, LX/2Oe;->b:LX/2Of;

    iget-object v1, v2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    iget-wide v4, v2, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v0, v1, v4, v5}, LX/2Of;->a(Lcom/facebook/user/model/UserKey;J)V

    .line 402557
    iget-object v0, p0, LX/2Oe;->h:LX/2Ow;

    invoke-virtual {v0}, LX/2Ow;->c()V

    .line 402558
    :cond_1
    invoke-static {v2}, LX/2Mk;->z(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 402559
    iget-object v0, p0, LX/2Oe;->h:LX/2Ow;

    iget-object v1, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Ow;->e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 402560
    :cond_2
    invoke-static {v2}, LX/2Mk;->B(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 402561
    iget-object v0, p0, LX/2Oe;->h:LX/2Ow;

    iget-object v1, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 402562
    new-instance v2, Landroid/content/Intent;

    sget-object v3, LX/0aY;->N:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 402563
    const-string v3, "thread_key"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 402564
    invoke-static {v0, v2}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 402565
    :cond_3
    return-void

    .line 402566
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/user/model/User;)V
    .locals 2

    .prologue
    .line 402567
    iget-object v0, p0, LX/2Oe;->l:LX/2Oi;

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Oi;->a(Ljava/util/Collection;)V

    .line 402568
    return-void
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 402569
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 402570
    :cond_0
    :goto_0
    return-object v0

    .line 402571
    :cond_1
    iget-object v6, p0, LX/2Oe;->l:LX/2Oi;

    invoke-virtual {v6}, LX/2Oi;->a()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/user/model/User;

    .line 402572
    iget-wide v8, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    .line 402573
    iget-wide v12, v6, Lcom/facebook/user/model/User;->N:J

    move-wide v10, v12

    .line 402574
    cmp-long v8, v8, v10

    if-nez v8, :cond_2

    .line 402575
    :goto_1
    move-object v1, v6

    .line 402576
    if-eqz v1, :cond_3

    .line 402577
    iget-object v2, p0, LX/2Oe;->l:LX/2Oi;

    new-instance v3, LX/0XI;

    invoke-direct {v3}, LX/0XI;-><init>()V

    invoke-virtual {v3, v1}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    move-result-object v1

    const-wide/16 v4, 0x0

    .line 402578
    iput-wide v4, v1, LX/0XI;->X:J

    .line 402579
    move-object v1, v1

    .line 402580
    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, LX/2Oi;->a(Ljava/util/Collection;Z)V

    .line 402581
    :cond_3
    iget-object v1, p0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v1, p1}, LX/2OQ;->h(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 402582
    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    goto :goto_0

    :cond_4
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public final b(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V
    .locals 2

    .prologue
    .line 402583
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v0, p1, p2, p3}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 402584
    return-void
.end method

.method public final b(Lcom/facebook/messaging/service/model/FetchThreadResult;)V
    .locals 7

    .prologue
    .line 402585
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 402586
    iget-object v1, p0, LX/2Oe;->a:LX/2OQ;

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    invoke-virtual {v1, v0, v2, v3}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 402587
    iget-object v1, p0, LX/2Oe;->l:LX/2Oi;

    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    invoke-virtual {v1, v2}, LX/2Oi;->a(Ljava/util/Collection;)V

    .line 402588
    iget-object v1, p0, LX/2Oe;->d:LX/2Ou;

    const/4 v3, 0x0

    .line 402589
    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 402590
    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v2

    iget-object p1, v1, LX/2Ou;->b:Lcom/facebook/user/model/UserKey;

    invoke-static {v2, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 402591
    const/4 v2, 0x1

    .line 402592
    :goto_1
    move v1, v2

    .line 402593
    if-nez v1, :cond_0

    .line 402594
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p0, v1, v2}, LX/2Oe;->a(LX/6ek;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 402595
    :cond_0
    iget-object v1, p0, LX/2Oe;->b:LX/2Of;

    invoke-virtual {v1}, LX/2Of;->a()V

    .line 402596
    iget-object v1, p0, LX/2Oe;->h:LX/2Ow;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v0}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 402597
    return-void

    .line 402598
    :cond_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    :cond_2
    move v2, v3

    .line 402599
    goto :goto_1
.end method

.method public final b(Lcom/facebook/messaging/service/model/NewMessageResult;)V
    .locals 2

    .prologue
    .line 402600
    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, v0, v1}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)V

    .line 402601
    return-void
.end method

.method public final c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V
    .locals 2

    .prologue
    .line 402602
    iget-object v0, p0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v0, p1, p2, p3}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 402603
    iget-object v0, p0, LX/2Oe;->b:LX/2Of;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 402604
    iget-object p0, v0, LX/2Of;->b:LX/0QI;

    invoke-interface {p0, v1}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 402605
    return-void
.end method
