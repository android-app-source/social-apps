.class public LX/3Hp;
.super LX/3Ga;
.source ""


# instance fields
.field public b:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/resources/ui/FbFrameLayout;

.field public d:LX/3Hq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/AVY;

.field private f:Lcom/facebook/widget/text/BetterTextView;

.field private o:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 545023
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3Hp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 545024
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 545021
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3Hp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545022
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 545015
    invoke-direct {p0, p1, p2, p3}, LX/3Ga;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545016
    const-class v0, LX/3Hp;

    invoke-static {v0, p0}, LX/3Hp;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 545017
    invoke-virtual {p0}, LX/3Hp;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 545018
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bwb;

    invoke-direct {v1, p0, p0}, LX/Bwb;-><init>(LX/3Hp;LX/2oy;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545019
    :cond_0
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Hr;

    invoke-direct {v1, p0}, LX/3Hr;-><init>(LX/3Hp;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545020
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/3Hp;

    invoke-static {v2}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v1

    check-cast v1, LX/1b4;

    const-class p0, LX/3Hq;

    invoke-interface {v2, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/3Hq;

    iput-object v1, p1, LX/3Hp;->b:LX/1b4;

    iput-object v2, p1, LX/3Hp;->d:LX/3Hq;

    return-void
.end method

.method public static c(LX/3Hp;I)V
    .locals 2

    .prologue
    .line 545011
    iget-object v0, p0, LX/3Hp;->c:Lcom/facebook/resources/ui/FbFrameLayout;

    if-eqz v0, :cond_0

    .line 545012
    iget-object v1, p0, LX/3Hp;->c:Lcom/facebook/resources/ui/FbFrameLayout;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbFrameLayout;->setVisibility(I)V

    .line 545013
    :cond_0
    return-void

    .line 545014
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static j(LX/3Hp;)V
    .locals 3

    .prologue
    .line 545003
    iget-object v0, p0, LX/3Hp;->f:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 545004
    iget-object v0, p0, LX/3Hp;->o:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->setVisibility(I)V

    .line 545005
    iget-object v0, p0, LX/3Hp;->e:LX/AVY;

    .line 545006
    invoke-virtual {v0}, LX/AVY;->b()V

    .line 545007
    iget-object v1, v0, LX/AVY;->j:Ljava/lang/Runnable;

    if-nez v1, :cond_0

    .line 545008
    new-instance v1, Lcom/facebook/facecast/audio/broadcast/FacecastAudioAnimationHelper$1;

    invoke-direct {v1, v0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioAnimationHelper$1;-><init>(LX/AVY;)V

    iput-object v1, v0, LX/AVY;->j:Ljava/lang/Runnable;

    .line 545009
    :cond_0
    iget-object v1, v0, LX/AVY;->k:Landroid/os/Handler;

    iget-object v2, v0, LX/AVY;->j:Ljava/lang/Runnable;

    const p0, -0x20e7d270

    invoke-static {v1, v2, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 545010
    return-void
.end method

.method public static k(LX/3Hp;)V
    .locals 2

    .prologue
    .line 544999
    iget-object v0, p0, LX/3Hp;->f:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 545000
    iget-object v0, p0, LX/3Hp;->o:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->setVisibility(I)V

    .line 545001
    iget-object v0, p0, LX/3Hp;->e:LX/AVY;

    invoke-virtual {v0}, LX/AVY;->b()V

    .line 545002
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 545025
    if-eqz p2, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_1

    .line 545026
    :cond_0
    :goto_0
    return-void

    .line 545027
    :cond_1
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 545028
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 545029
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 545030
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bQ()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 545031
    invoke-virtual {p0}, LX/3Hp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-static {p0, v0}, LX/3Hp;->c(LX/3Hp;I)V

    .line 545032
    invoke-virtual {p0}, LX/3Hp;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 545033
    invoke-static {p0}, LX/3Hp;->j(LX/3Hp;)V

    goto :goto_0

    .line 545034
    :cond_2
    invoke-static {p0}, LX/3Hp;->k(LX/3Hp;)V

    goto :goto_0
.end method

.method public final b(LX/2pa;)Z
    .locals 1

    .prologue
    .line 544998
    iget-object v0, p0, LX/3Hp;->b:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->w()Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 544994
    iget-object v0, p0, LX/3Hp;->c:Lcom/facebook/resources/ui/FbFrameLayout;

    if-eqz v0, :cond_0

    .line 544995
    iget-object v0, p0, LX/3Hp;->c:Lcom/facebook/resources/ui/FbFrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbFrameLayout;->setVisibility(I)V

    .line 544996
    iget-object v0, p0, LX/3Hp;->e:LX/AVY;

    invoke-virtual {v0}, LX/AVY;->b()V

    .line 544997
    :cond_0
    return-void
.end method

.method public getLayoutToInflate()I
    .locals 1

    .prologue
    .line 544993
    const v0, 0x7f0309f9

    return v0
.end method

.method public getStubLayout()I
    .locals 1

    .prologue
    .line 544992
    const v0, 0x7f0309fa

    return v0
.end method

.method public i()Z
    .locals 3

    .prologue
    .line 544989
    iget-object v0, p0, LX/3Hp;->b:LX/1b4;

    .line 544990
    iget-object v1, v0, LX/1b4;->a:LX/0ad;

    sget-short v2, LX/1v6;->j:S

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 544991
    return v0
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 0

    .prologue
    .line 544988
    return-void
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 544982
    const v0, 0x7f0d193b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    iput-object v0, p0, LX/3Hp;->c:Lcom/facebook/resources/ui/FbFrameLayout;

    .line 544983
    iget-object v0, p0, LX/3Hp;->c:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {p0}, LX/3Hp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/Ac1;->a(Landroid/content/res/Resources;)LX/Ac1;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbFrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 544984
    const v0, 0x7f0d193c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/3Hp;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 544985
    const v0, 0x7f0d193d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    iput-object v0, p0, LX/3Hp;->o:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    .line 544986
    iget-object v0, p0, LX/3Hp;->d:LX/3Hq;

    iget-object v1, p0, LX/3Hp;->o:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    invoke-virtual {p0}, LX/3Hp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b053f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/3Hq;->a(Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;I)LX/AVY;

    move-result-object v0

    iput-object v0, p0, LX/3Hp;->e:LX/AVY;

    .line 544987
    return-void
.end method
