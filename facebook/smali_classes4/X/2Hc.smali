.class public LX/2Hc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/2Hc;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0aU;

.field private final c:LX/2GB;

.field private final d:LX/2Cx;

.field private final e:Ljava/lang/Object;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/location/LocationSignalDataPackage;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private g:Lcom/facebook/location/LocationSignalDataPackage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final i:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0aU;LX/2GB;LX/2Cx;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390757
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/2Hc;->e:Ljava/lang/Object;

    .line 390758
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2Hc;->f:Ljava/util/List;

    .line 390759
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2Hc;->h:Ljava/util/List;

    .line 390760
    new-instance v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingDataSaver$1;

    invoke-direct {v0, p0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingDataSaver$1;-><init>(LX/2Hc;)V

    iput-object v0, p0, LX/2Hc;->i:Ljava/lang/Runnable;

    .line 390761
    iput-object p1, p0, LX/2Hc;->a:Landroid/content/Context;

    .line 390762
    iput-object p2, p0, LX/2Hc;->b:LX/0aU;

    .line 390763
    iput-object p3, p0, LX/2Hc;->c:LX/2GB;

    .line 390764
    iput-object p4, p0, LX/2Hc;->d:LX/2Cx;

    .line 390765
    return-void
.end method

.method public static a(LX/0QB;)LX/2Hc;
    .locals 7

    .prologue
    .line 390743
    sget-object v0, LX/2Hc;->j:LX/2Hc;

    if-nez v0, :cond_1

    .line 390744
    const-class v1, LX/2Hc;

    monitor-enter v1

    .line 390745
    :try_start_0
    sget-object v0, LX/2Hc;->j:LX/2Hc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390746
    if-eqz v2, :cond_0

    .line 390747
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 390748
    new-instance p0, LX/2Hc;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v4

    check-cast v4, LX/0aU;

    invoke-static {v0}, LX/2GB;->a(LX/0QB;)LX/2GB;

    move-result-object v5

    check-cast v5, LX/2GB;

    invoke-static {v0}, LX/2Cx;->a(LX/0QB;)LX/2Cx;

    move-result-object v6

    check-cast v6, LX/2Cx;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2Hc;-><init>(Landroid/content/Context;LX/0aU;LX/2GB;LX/2Cx;)V

    .line 390749
    move-object v0, p0

    .line 390750
    sput-object v0, LX/2Hc;->j:LX/2Hc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390751
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390752
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390753
    :cond_1
    sget-object v0, LX/2Hc;->j:LX/2Hc;

    return-object v0

    .line 390754
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390755
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2Hc;)V
    .locals 7

    .prologue
    .line 390733
    iget-object v1, p0, LX/2Hc;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 390734
    :try_start_0
    iget-object v0, p0, LX/2Hc;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390735
    monitor-exit v1

    .line 390736
    :goto_0
    return-void

    .line 390737
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 390738
    iget-object v2, p0, LX/2Hc;->b:LX/0aU;

    const-string v3, "BACKGROUND_LOCATION_REPORTING_ACTION_WRITE_FINISHED"

    invoke-virtual {v2, v3}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 390739
    new-instance v2, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;

    iget-object v3, p0, LX/2Hc;->f:Ljava/util/List;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    iget-object v4, p0, LX/2Hc;->h:Ljava/util/List;

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;-><init>(LX/0Px;LX/0Px;)V

    .line 390740
    iget-object v3, p0, LX/2Hc;->d:LX/2Cx;

    invoke-virtual {v3, v2}, LX/2Cx;->a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;)V

    .line 390741
    iget-object v3, p0, LX/2Hc;->a:Landroid/content/Context;

    iget-object v4, p0, LX/2Hc;->a:Landroid/content/Context;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v4, v5, v0, v6}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-static {v3, v2, v0}, LX/HlP;->a(Landroid/content/Context;Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;Landroid/app/PendingIntent;)V

    .line 390742
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;)V
    .locals 8
    .param p1    # Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 390715
    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->a:Z

    if-nez v0, :cond_2

    .line 390716
    :cond_0
    iget-object v1, p0, LX/2Hc;->d:LX/2Cx;

    if-nez p1, :cond_1

    const-string v0, "No response from location update is available"

    :goto_0
    const-string v2, "background_location_reporting_data_saver"

    invoke-virtual {v1, p1, v0, v3, v2}, LX/2Cx;->a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 390717
    :goto_1
    if-nez p1, :cond_3

    .line 390718
    :goto_2
    return-void

    .line 390719
    :cond_1
    const-string v0, "The update did not succeed"

    goto :goto_0

    .line 390720
    :cond_2
    iget-object v0, p0, LX/2Hc;->d:LX/2Cx;

    const-string v1, "background_location_reporting_data_saver"

    invoke-virtual {v0, v3, v1}, LX/2Cx;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 390721
    :cond_3
    iget-object v3, p0, LX/2Hc;->e:Ljava/lang/Object;

    monitor-enter v3

    .line 390722
    :try_start_0
    iget-object v0, p2, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->a:LX/0Px;

    invoke-static {v0}, LX/0Ph;->g(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/LocationSignalDataPackage;

    iput-object v0, p0, LX/2Hc;->g:Lcom/facebook/location/LocationSignalDataPackage;

    .line 390723
    iget-object v4, p2, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_3
    if-ge v2, v5, :cond_6

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/LocationSignalDataPackage;

    .line 390724
    iget-object v1, p0, LX/2Hc;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 390725
    :cond_4
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 390726
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/location/LocationSignalDataPackage;

    .line 390727
    iget-object v1, v1, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v1

    iget-object v7, v0, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v7}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v7

    invoke-virtual {v1, v7}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 390728
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 390729
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 390730
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 390731
    :cond_6
    :try_start_1
    iget-object v0, p0, LX/2Hc;->h:Ljava/util/List;

    iget-object v1, p2, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->b:LX/0Px;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 390732
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public final a(Lcom/facebook/location/LocationSignalDataPackage;)V
    .locals 6

    .prologue
    .line 390705
    iget-object v0, p1, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    if-nez v0, :cond_0

    .line 390706
    const-string v0, "background_location_reporting_data_saver"

    const-string v1, "Discarding location package because this uploader requires a location to be set"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 390707
    :goto_0
    return-void

    .line 390708
    :cond_0
    iget-object v1, p0, LX/2Hc;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 390709
    :try_start_0
    iget-object v0, p0, LX/2Hc;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390710
    iget-object v0, p0, LX/2Hc;->g:Lcom/facebook/location/LocationSignalDataPackage;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2Hc;->g:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v0, v0, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    iget-object v2, p1, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-static {v0, v2}, LX/3GC;->b(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v2

    const-wide/32 v4, 0xcd140

    cmp-long v0, v2, v4

    if-ltz v0, :cond_3

    .line 390711
    :cond_1
    invoke-static {p0}, LX/2Hc;->a$redex0(LX/2Hc;)V

    .line 390712
    :cond_2
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 390713
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/2Hc;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 390714
    iget-object v0, p0, LX/2Hc;->c:LX/2GB;

    iget-object v2, p0, LX/2Hc;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, LX/2GB;->a(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/location/LocationSignalDataPackage;Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;)V
    .locals 9
    .param p1    # Lcom/facebook/location/LocationSignalDataPackage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 390690
    if-nez p1, :cond_0

    .line 390691
    iget-object v0, p0, LX/2Hc;->d:LX/2Cx;

    .line 390692
    iget-object v1, v0, LX/2Cx;->b:LX/0Zb;

    const-string v2, "background_location_obtain_single_location_failure"

    invoke-static {v2}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string p0, "trace_id"

    iget-object p1, p2, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->b:Ljava/lang/String;

    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 390693
    :goto_0
    return-void

    .line 390694
    :cond_0
    iget-object v0, p0, LX/2Hc;->d:LX/2Cx;

    iget-object v1, p1, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    .line 390695
    iget-object v3, v0, LX/2Cx;->b:LX/0Zb;

    const-string v4, "background_location_obtain_single_location_success"

    invoke-static {v4}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "trace_id"

    iget-object v6, p2, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "age_ms"

    iget-object v6, v0, LX/2Cx;->a:LX/0yD;

    invoke-virtual {v6, v1}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v7

    invoke-virtual {v4, v5, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "accuracy_meters"

    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v6

    invoke-virtual {v6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 390696
    iget-object v1, p0, LX/2Hc;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 390697
    :try_start_0
    iget-object v0, p0, LX/2Hc;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390698
    iget-object v0, p2, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 390699
    iget-object v0, p0, LX/2Hc;->h:Ljava/util/List;

    iget-object v2, p2, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390700
    :cond_1
    iget-object v0, p0, LX/2Hc;->g:Lcom/facebook/location/LocationSignalDataPackage;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/2Hc;->g:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v0, v0, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    iget-object v2, p1, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-static {v0, v2}, LX/3GC;->a(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;)F

    move-result v0

    iget v2, p2, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->a:F

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_4

    .line 390701
    :cond_2
    invoke-static {p0}, LX/2Hc;->a$redex0(LX/2Hc;)V

    .line 390702
    :cond_3
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 390703
    :cond_4
    :try_start_1
    iget-object v0, p0, LX/2Hc;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 390704
    iget-object v0, p0, LX/2Hc;->c:LX/2GB;

    iget-object v2, p0, LX/2Hc;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, LX/2GB;->a(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 390686
    iget-object v1, p0, LX/2Hc;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 390687
    :try_start_0
    iget-object v0, p0, LX/2Hc;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 390688
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Hc;->g:Lcom/facebook/location/LocationSignalDataPackage;

    .line 390689
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
