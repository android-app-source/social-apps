.class public LX/3BP;
.super Landroid/widget/FrameLayout;
.source ""


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final d:Landroid/graphics/Typeface;

.field private static n:[F


# instance fields
.field public b:I

.field public c:I

.field public final e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

.field public f:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

.field private g:Landroid/view/View;

.field public h:Landroid/widget/TextView;

.field public i:Landroid/graphics/drawable/Drawable;

.field public j:Landroid/net/Uri;

.field public k:I

.field private l:I

.field public m:LX/3BT;

.field private final o:Landroid/graphics/Paint;

.field private p:I

.field private q:I

.field public r:J

.field private final s:[F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 528418
    const-string v0, "https://maps.googleapis.com/maps/api/staticmap?"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/3BP;->a:Landroid/net/Uri;

    .line 528419
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    sput-object v0, LX/3BP;->d:Landroid/graphics/Typeface;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 528410
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 528411
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/3BP;->e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 528412
    iput v2, p0, LX/3BP;->k:I

    .line 528413
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/3BP;->o:Landroid/graphics/Paint;

    .line 528414
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3BP;->r:J

    .line 528415
    new-array v0, v2, [F

    iput-object v0, p0, LX/3BP;->s:[F

    .line 528416
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/3BP;->a(Landroid/util/AttributeSet;)V

    .line 528417
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 528402
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 528403
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/3BP;->e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 528404
    iput v2, p0, LX/3BP;->k:I

    .line 528405
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/3BP;->o:Landroid/graphics/Paint;

    .line 528406
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3BP;->r:J

    .line 528407
    new-array v0, v2, [F

    iput-object v0, p0, LX/3BP;->s:[F

    .line 528408
    invoke-direct {p0, p2}, LX/3BP;->a(Landroid/util/AttributeSet;)V

    .line 528409
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 528394
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 528395
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/3BP;->e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 528396
    iput v2, p0, LX/3BP;->k:I

    .line 528397
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/3BP;->o:Landroid/graphics/Paint;

    .line 528398
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3BP;->r:J

    .line 528399
    new-array v0, v2, [F

    iput-object v0, p0, LX/3BP;->s:[F

    .line 528400
    invoke-direct {p0, p2}, LX/3BP;->a(Landroid/util/AttributeSet;)V

    .line 528401
    return-void
.end method

.method private static a(I)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 528390
    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    .line 528391
    invoke-static {}, LX/3BU;->a()V

    .line 528392
    sget-object v0, LX/3BU;->l:LX/3BV;

    iget-object v0, v0, LX/3BV;->c:Ljava/lang/String;

    move-object v0, v0

    .line 528393
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/3BP;->a:Landroid/net/Uri;

    goto :goto_0
.end method

.method public static a(IIILandroid/content/res/Resources;Ljava/lang/String;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 528376
    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 528377
    invoke-static {p2}, LX/3BP;->a(I)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "size"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    div-int v4, p0, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    div-int v4, p1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "scale"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "language"

    invoke-virtual {v0, v1, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "format"

    iget-object v0, p5, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "jpg"

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 528378
    const-string v0, "visible"

    iget-object v1, p5, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->g:Ljava/lang/String;

    invoke-static {v2, v0, v1}, LX/3BP;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 528379
    const-string v0, "circle"

    iget-object v1, p5, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->h:Ljava/lang/String;

    invoke-static {v2, v0, v1}, LX/3BP;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 528380
    const-string v0, "markers"

    iget-object v1, p5, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->e:Ljava/lang/String;

    invoke-static {v2, v0, v1}, LX/3BP;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 528381
    const-string v0, "path"

    iget-object v1, p5, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->f:Ljava/lang/String;

    invoke-static {v2, v0, v1}, LX/3BP;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 528382
    const-string v0, "center"

    iget-object v1, p5, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->c:Ljava/lang/String;

    invoke-static {v2, v0, v1}, LX/3BP;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 528383
    const-string v0, "zoom"

    iget-object v1, p5, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->b:Ljava/lang/String;

    invoke-static {v2, v0, v1}, LX/3BP;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 528384
    iget-object v0, p5, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 528385
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_1

    .line 528386
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "marker_list["

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "]"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p5, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v4, v0}, LX/3BP;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 528387
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 528388
    :cond_0
    iget-object v0, p5, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->d:Ljava/lang/String;

    goto :goto_1

    .line 528389
    :cond_1
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v0, 0x2

    goto/16 :goto_0
.end method

.method private static a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 528373
    if-eqz p2, :cond_0

    .line 528374
    invoke-virtual {p0, p1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 528375
    :cond_0
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    const/high16 v4, 0x3f000000    # 0.5f

    .line 528340
    if-eqz p1, :cond_0

    .line 528341
    const-string v0, "http://schemas.android.com/apk/facebook"

    const-string v1, "centeredMapPinDrawable"

    invoke-interface {p1, v0, v1, v6}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 528342
    const-string v1, "http://schemas.android.com/apk/facebook"

    const-string v2, "centeredMapPinDrawableAnchorU"

    invoke-interface {p1, v1, v2, v4}, Landroid/util/AttributeSet;->getAttributeFloatValue(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v1

    .line 528343
    const-string v2, "http://schemas.android.com/apk/facebook"

    const-string v3, "centeredMapPinDrawableAnchorV"

    invoke-interface {p1, v2, v3, v4}, Landroid/util/AttributeSet;->getAttributeFloatValue(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v2

    .line 528344
    if-eqz v0, :cond_0

    .line 528345
    invoke-virtual {p0}, LX/3BP;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0, v1, v2}, LX/3BP;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 528346
    :cond_0
    invoke-virtual {p0, v6}, LX/3BP;->setWillNotDraw(Z)V

    .line 528347
    invoke-virtual {p0}, LX/3BP;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 528348
    const/high16 v1, 0x41800000    # 16.0f

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, LX/3BP;->p:I

    .line 528349
    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/3BP;->q:I

    .line 528350
    iget-object v0, p0, LX/3BP;->o:Landroid/graphics/Paint;

    iget v1, p0, LX/3BP;->q:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 528351
    iget-object v0, p0, LX/3BP;->o:Landroid/graphics/Paint;

    const v1, -0x1c1f28

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 528352
    invoke-virtual {p0}, LX/3BP;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3BP;->g:Landroid/view/View;

    .line 528353
    iget-object v0, p0, LX/3BP;->g:Landroid/view/View;

    invoke-virtual {p0, v0, v5, v5}, LX/3BP;->addView(Landroid/view/View;II)V

    .line 528354
    const/4 p1, 0x0

    const/4 v6, -0x2

    const/high16 v5, 0x41000000    # 8.0f

    const/4 v4, 0x0

    .line 528355
    invoke-virtual {p0}, LX/3BP;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 528356
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, LX/3BP;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/3BP;->h:Landroid/widget/TextView;

    .line 528357
    iget-object v1, p0, LX/3BP;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/3BP;->getReportButtonText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 528358
    iget-object v1, p0, LX/3BP;->h:Landroid/widget/TextView;

    sget-object v2, LX/3BP;->d:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 528359
    iget-object v1, p0, LX/3BP;->h:Landroid/widget/TextView;

    const/high16 v2, -0x66000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 528360
    iget-object v1, p0, LX/3BP;->h:Landroid/widget/TextView;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 528361
    iget-object v1, p0, LX/3BP;->h:Landroid/widget/TextView;

    iget-object v2, p0, LX/3BP;->h:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v2

    or-int/lit8 v2, v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 528362
    iget-object v1, p0, LX/3BP;->h:Landroid/widget/TextView;

    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v2, v0

    const v3, -0x3f000001    # -7.9999995f

    invoke-virtual {v1, v2, v4, v4, v3}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 528363
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 528364
    mul-float v2, v5, v0

    float-to-int v2, v2

    mul-float/2addr v0, v5

    float-to-int v0, v0

    invoke-virtual {v1, p1, p1, v2, v0}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 528365
    const/16 v0, 0x55

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 528366
    iget-object v0, p0, LX/3BP;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 528367
    iget-object v0, p0, LX/3BP;->h:Landroid/widget/TextView;

    new-instance v1, LX/3BQ;

    invoke-direct {v1, p0}, LX/3BQ;-><init>(LX/3BP;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 528368
    iget-object v0, p0, LX/3BP;->h:Landroid/widget/TextView;

    move-object v0, v0

    .line 528369
    iput-object v0, p0, LX/3BP;->h:Landroid/widget/TextView;

    .line 528370
    iget-object v0, p0, LX/3BP;->h:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, LX/3BP;->addView(Landroid/view/View;)V

    .line 528371
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/3BP;->setReportButtonVisibility(I)V

    .line 528372
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    .line 528330
    iget v0, p0, LX/3BP;->b:I

    if-eqz v0, :cond_2

    iget v0, p0, LX/3BP;->c:I

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3BP;->e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 528331
    iget-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->c:Ljava/lang/String;

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->h:Ljava/lang/String;

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->g:Ljava/lang/String;

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->e:Ljava/lang/String;

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    :cond_1
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 528332
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3BP;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    .line 528333
    :cond_2
    :goto_1
    return-void

    .line 528334
    :cond_3
    iget v0, p0, LX/3BP;->b:I

    iget v1, p0, LX/3BP;->c:I

    iget v2, p0, LX/3BP;->k:I

    invoke-virtual {p0}, LX/3BP;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0}, LX/3BP;->getLanguageCode()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/3BP;->e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-static/range {v0 .. v5}, LX/3BP;->a(IIILandroid/content/res/Resources;Ljava/lang/String;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)Landroid/net/Uri;

    move-result-object v0

    .line 528335
    iget-object v1, p0, LX/3BP;->j:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 528336
    iput-object v0, p0, LX/3BP;->j:Landroid/net/Uri;

    .line 528337
    sget-object v1, LX/31U;->y:LX/31U;

    invoke-virtual {v1}, LX/31U;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 528338
    sget-object v1, LX/31U;->y:LX/31U;

    new-instance v2, LX/3Bk;

    invoke-direct {v2, p0}, LX/3Bk;-><init>(LX/3BP;)V

    invoke-virtual {v1, v2}, LX/31U;->a(Ljava/util/Map;)V

    .line 528339
    :cond_4
    iget-object v1, p0, LX/3BP;->g:Landroid/view/View;

    iget-object v2, p0, LX/3BP;->e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    iget-object v2, v2, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2}, LX/3BP;->a(Landroid/view/View;Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 7

    .prologue
    .line 528323
    iget-object v0, p0, LX/3BP;->i:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 528324
    :goto_0
    return-void

    .line 528325
    :cond_0
    iget-object v0, p0, LX/3BP;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 528326
    iget-object v1, p0, LX/3BP;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 528327
    invoke-virtual {p0}, LX/3BP;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, LX/3BP;->getWidth()I

    move-result v3

    invoke-virtual {p0}, LX/3BP;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, LX/3BP;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v3, v0

    iget-object v4, p0, LX/3BP;->s:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-float/2addr v3, v4

    float-to-int v3, v3

    sub-int/2addr v2, v3

    .line 528328
    invoke-virtual {p0}, LX/3BP;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, LX/3BP;->getHeight()I

    move-result v4

    invoke-virtual {p0}, LX/3BP;->getPaddingTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, LX/3BP;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v4, v1

    iget-object v5, p0, LX/3BP;->s:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    mul-float/2addr v4, v5

    float-to-int v4, v4

    sub-int/2addr v3, v4

    .line 528329
    iget-object v4, p0, LX/3BP;->i:Landroid/graphics/drawable/Drawable;

    add-int/2addr v0, v2

    add-int/2addr v1, v3

    invoke-virtual {v4, v2, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 2

    .prologue
    .line 528320
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, LX/3BP;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 528321
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 528322
    return-object v0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;FF)V
    .locals 2

    .prologue
    .line 528314
    iget-object v0, p0, LX/3BP;->s:[F

    const/4 v1, 0x0

    aput p2, v0, v1

    .line 528315
    iget-object v0, p0, LX/3BP;->s:[F

    const/4 v1, 0x1

    aput p3, v0, v1

    .line 528316
    iput-object p1, p0, LX/3BP;->i:Landroid/graphics/drawable/Drawable;

    .line 528317
    invoke-direct {p0}, LX/3BP;->c()V

    .line 528318
    invoke-virtual {p0}, LX/3BP;->invalidate()V

    .line 528319
    return-void
.end method

.method public a(Landroid/view/View;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 528420
    invoke-static {}, LX/31U;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3BP;->r:J

    .line 528421
    iget-object v0, p0, LX/3BP;->f:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    if-eqz v0, :cond_1

    .line 528422
    iget-object v0, p0, LX/3BP;->f:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 528423
    sget-object v1, LX/31l;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 528424
    if-ne v1, v0, :cond_0

    .line 528425
    sget-object v3, LX/31l;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3, v1}, Ljava/util/concurrent/BlockingQueue;->remove(Ljava/lang/Object;)Z

    .line 528426
    invoke-virtual {v1}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;->a()V

    goto :goto_0

    .line 528427
    :cond_1
    move-object v0, p1

    .line 528428
    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 528429
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$1;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/facebook/android/maps/StaticMapView$1;-><init>(LX/3BP;Landroid/view/View;Ljava/lang/String;Landroid/net/Uri;)V

    iput-object v0, p0, LX/3BP;->f:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 528430
    iget-object v0, p0, LX/3BP;->f:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    invoke-static {v0}, LX/31l;->a(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V

    .line 528431
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 528310
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 528311
    iget-object v0, p0, LX/3BP;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3BP;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 528312
    iget-object v0, p0, LX/3BP;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 528313
    :cond_0
    return-void
.end method

.method public getLanguageCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 528309
    sget-object v0, LX/3BU;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getReportButtonText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 528308
    const-string v0, "Report"

    return-object v0
.end method

.method public getStaticMapBaseUrl()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 528307
    iget v0, p0, LX/3BP;->k:I

    invoke-static {v0}, LX/3BP;->a(I)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 528306
    iget-object v0, p0, LX/3BP;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x0

    .line 528275
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 528276
    iget-object v0, p0, LX/3BP;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 528277
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v5

    .line 528278
    invoke-virtual {p0}, LX/3BP;->getPaddingLeft()I

    move-result v0

    .line 528279
    invoke-virtual {p0}, LX/3BP;->getPaddingTop()I

    move-result v1

    .line 528280
    invoke-virtual {p0}, LX/3BP;->getWidth()I

    move-result v2

    invoke-virtual {p0}, LX/3BP;->getPaddingRight()I

    move-result v4

    sub-int/2addr v2, v4

    .line 528281
    invoke-virtual {p0}, LX/3BP;->getHeight()I

    move-result v4

    invoke-virtual {p0}, LX/3BP;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v4, v6

    .line 528282
    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 528283
    const v6, -0xf121b

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 528284
    sub-int v6, v2, v0

    .line 528285
    sub-int v7, v4, v1

    .line 528286
    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 528287
    iget v0, p0, LX/3BP;->p:I

    add-int/2addr v0, v6

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, LX/3BP;->p:I

    div-int/2addr v0, v1

    iget v1, p0, LX/3BP;->p:I

    add-int/2addr v1, v7

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, LX/3BP;->p:I

    div-int/2addr v1, v2

    add-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0x2

    .line 528288
    sget-object v1, LX/3BP;->n:[F

    if-eqz v1, :cond_0

    sget-object v1, LX/3BP;->n:[F

    array-length v1, v1

    if-ge v1, v0, :cond_1

    .line 528289
    :cond_0
    new-array v0, v0, [F

    sput-object v0, LX/3BP;->n:[F

    .line 528290
    :cond_1
    iget v0, p0, LX/3BP;->p:I

    iget v1, p0, LX/3BP;->q:I

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    move v2, v0

    move v1, v3

    .line 528291
    :goto_0
    if-ge v2, v6, :cond_2

    .line 528292
    sget-object v4, LX/3BP;->n:[F

    add-int/lit8 v8, v1, 0x1

    int-to-float v9, v2

    aput v9, v4, v1

    .line 528293
    sget-object v1, LX/3BP;->n:[F

    add-int/lit8 v4, v8, 0x1

    aput v10, v1, v8

    .line 528294
    sget-object v1, LX/3BP;->n:[F

    add-int/lit8 v8, v4, 0x1

    int-to-float v9, v2

    aput v9, v1, v4

    .line 528295
    sget-object v1, LX/3BP;->n:[F

    add-int/lit8 v4, v8, 0x1

    int-to-float v9, v7

    aput v9, v1, v8

    .line 528296
    iget v1, p0, LX/3BP;->p:I

    add-int/2addr v1, v2

    move v2, v1

    move v1, v4

    goto :goto_0

    .line 528297
    :cond_2
    :goto_1
    if-ge v0, v7, :cond_3

    .line 528298
    sget-object v2, LX/3BP;->n:[F

    add-int/lit8 v4, v1, 0x1

    aput v10, v2, v1

    .line 528299
    sget-object v1, LX/3BP;->n:[F

    add-int/lit8 v2, v4, 0x1

    int-to-float v8, v0

    aput v8, v1, v4

    .line 528300
    sget-object v1, LX/3BP;->n:[F

    add-int/lit8 v4, v2, 0x1

    int-to-float v8, v6

    aput v8, v1, v2

    .line 528301
    sget-object v2, LX/3BP;->n:[F

    add-int/lit8 v1, v4, 0x1

    int-to-float v8, v0

    aput v8, v2, v4

    .line 528302
    iget v2, p0, LX/3BP;->p:I

    add-int/2addr v0, v2

    goto :goto_1

    .line 528303
    :cond_3
    sget-object v0, LX/3BP;->n:[F

    iget-object v2, p0, LX/3BP;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    .line 528304
    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 528305
    :cond_4
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 528266
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 528267
    iget v0, p0, LX/3BP;->b:I

    .line 528268
    iget v1, p0, LX/3BP;->c:I

    .line 528269
    iget-object v2, p0, LX/3BP;->g:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iput v2, p0, LX/3BP;->b:I

    .line 528270
    iget-object v2, p0, LX/3BP;->g:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iput v2, p0, LX/3BP;->c:I

    .line 528271
    iget v2, p0, LX/3BP;->b:I

    if-ne v0, v2, :cond_0

    iget v0, p0, LX/3BP;->c:I

    if-eq v1, v0, :cond_1

    .line 528272
    :cond_0
    invoke-direct {p0}, LX/3BP;->b()V

    .line 528273
    :cond_1
    invoke-direct {p0}, LX/3BP;->c()V

    .line 528274
    return-void
.end method

.method public setCenteredMapPinDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    const/high16 v0, 0x3f000000    # 0.5f

    .line 528264
    invoke-virtual {p0, p1, v0, v0}, LX/3BP;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 528265
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 528257
    if-eqz p1, :cond_0

    .line 528258
    iget-object v0, p0, LX/3BP;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 528259
    iget-object v0, p0, LX/3BP;->h:Landroid/widget/TextView;

    iget v1, p0, LX/3BP;->l:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 528260
    invoke-direct {p0}, LX/3BP;->b()V

    .line 528261
    :goto_0
    return-void

    .line 528262
    :cond_0
    iget-object v0, p0, LX/3BP;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 528263
    iget-object v0, p0, LX/3BP;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V
    .locals 2

    .prologue
    .line 528244
    iget-object v0, p0, LX/3BP;->e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {v0, p1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528245
    :goto_0
    return-void

    .line 528246
    :cond_0
    iget-object v0, p0, LX/3BP;->e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 528247
    iget-object v1, p1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a:Ljava/lang/String;

    .line 528248
    iget-object v1, p1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->b:Ljava/lang/String;

    .line 528249
    iget-object v1, p1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->c:Ljava/lang/String;

    .line 528250
    iget-object v1, p1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->g:Ljava/lang/String;

    .line 528251
    iget-object v1, p1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->h:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->h:Ljava/lang/String;

    .line 528252
    iget-object v1, p1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->d:Ljava/lang/String;

    .line 528253
    iget-object v1, p1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->e:Ljava/lang/String;

    .line 528254
    iget-object v1, p1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->f:Ljava/lang/String;

    .line 528255
    iget-object v1, p1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->i:Ljava/util/List;

    iput-object v1, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->i:Ljava/util/List;

    .line 528256
    invoke-direct {p0}, LX/3BP;->b()V

    goto :goto_0
.end method

.method public setMapReporterLauncher(LX/3BT;)V
    .locals 0

    .prologue
    .line 528242
    iput-object p1, p0, LX/3BP;->m:LX/3BT;

    .line 528243
    return-void
.end method

.method public setReportButtonVisibility(I)V
    .locals 1

    .prologue
    .line 528238
    iput p1, p0, LX/3BP;->l:I

    .line 528239
    invoke-virtual {p0}, LX/3BP;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528240
    iget-object v0, p0, LX/3BP;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 528241
    :cond_0
    return-void
.end method
