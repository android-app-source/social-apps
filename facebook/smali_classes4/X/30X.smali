.class public LX/30X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/30V;


# instance fields
.field private final b:LX/30Y;

.field private final c:LX/30Z;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/EjJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/30Y;LX/30Z;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/30Y;",
            "LX/30Z;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "LX/EjJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 484549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484550
    iput-object p1, p0, LX/30X;->b:LX/30Y;

    .line 484551
    iput-object p2, p0, LX/30X;->c:LX/30Z;

    .line 484552
    iput-object p3, p0, LX/30X;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 484553
    iput-object p4, p0, LX/30X;->e:LX/0Or;

    .line 484554
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 484548
    iget-object v0, p0, LX/30X;->b:LX/30Y;

    invoke-virtual {v0}, LX/30Y;->a()Z

    move-result v0

    return v0
.end method

.method public final b()LX/2Jm;
    .locals 1

    .prologue
    .line 484555
    sget-object v0, LX/2Jm;->INTERVAL:LX/2Jm;

    return-object v0
.end method

.method public final c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<+",
            "LX/EjJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 484547
    iget-object v0, p0, LX/30X;->e:LX/0Or;

    return-object v0
.end method

.method public final d()LX/2Jl;
    .locals 2

    .prologue
    .line 484546
    new-instance v0, LX/2Jk;

    invoke-direct {v0}, LX/2Jk;-><init>()V

    sget-object v1, LX/2Im;->CONNECTED:LX/2Im;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Im;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Jq;->LOGGED_IN:LX/2Jq;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jq;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Jh;->BACKGROUND:LX/2Jh;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jh;)LX/2Jk;

    move-result-object v0

    invoke-virtual {v0}, LX/2Jk;->a()LX/2Jl;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 484542
    iget-object v0, p0, LX/30X;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0uQ;->d:LX/0Tn;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 484543
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    iget-object v0, p0, LX/30X;->c:LX/30Z;

    .line 484544
    iget-object v1, v0, LX/30Z;->t:LX/35c;

    move-object v0, v1

    .line 484545
    iget-wide v0, v0, LX/35c;->i:J

    :cond_0
    return-wide v0
.end method
