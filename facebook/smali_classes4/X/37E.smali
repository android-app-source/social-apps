.class public final enum LX/37E;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/37E;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/37E;

.field public static final enum CACHE_FULL:LX/37E;

.field public static final enum CACHE_MANAGER_TRIMMED:LX/37E;

.field public static final enum CONTENT_STALE:LX/37E;

.field public static final enum USER_FORCED:LX/37E;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 500871
    new-instance v0, LX/37E;

    const-string v1, "CACHE_FULL"

    invoke-direct {v0, v1, v2}, LX/37E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/37E;->CACHE_FULL:LX/37E;

    .line 500872
    new-instance v0, LX/37E;

    const-string v1, "CONTENT_STALE"

    invoke-direct {v0, v1, v3}, LX/37E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/37E;->CONTENT_STALE:LX/37E;

    .line 500873
    new-instance v0, LX/37E;

    const-string v1, "USER_FORCED"

    invoke-direct {v0, v1, v4}, LX/37E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/37E;->USER_FORCED:LX/37E;

    .line 500874
    new-instance v0, LX/37E;

    const-string v1, "CACHE_MANAGER_TRIMMED"

    invoke-direct {v0, v1, v5}, LX/37E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/37E;->CACHE_MANAGER_TRIMMED:LX/37E;

    .line 500875
    const/4 v0, 0x4

    new-array v0, v0, [LX/37E;

    sget-object v1, LX/37E;->CACHE_FULL:LX/37E;

    aput-object v1, v0, v2

    sget-object v1, LX/37E;->CONTENT_STALE:LX/37E;

    aput-object v1, v0, v3

    sget-object v1, LX/37E;->USER_FORCED:LX/37E;

    aput-object v1, v0, v4

    sget-object v1, LX/37E;->CACHE_MANAGER_TRIMMED:LX/37E;

    aput-object v1, v0, v5

    sput-object v0, LX/37E;->$VALUES:[LX/37E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 500876
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/37E;
    .locals 1

    .prologue
    .line 500877
    const-class v0, LX/37E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/37E;

    return-object v0
.end method

.method public static values()[LX/37E;
    .locals 1

    .prologue
    .line 500878
    sget-object v0, LX/37E;->$VALUES:[LX/37E;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/37E;

    return-object v0
.end method
