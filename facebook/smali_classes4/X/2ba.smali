.class public LX/2ba;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 437062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 437063
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 437064
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 437065
    :goto_0
    return v1

    .line 437066
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 437067
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 437068
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 437069
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 437070
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 437071
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 437072
    invoke-static {p0, p1}, LX/2bM;->b(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 437073
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 437074
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 437075
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 437076
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 437077
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 437078
    if-eqz v0, :cond_0

    .line 437079
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 437080
    invoke-static {p0, v0, p2, p3}, LX/2bM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 437081
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 437082
    return-void
.end method
