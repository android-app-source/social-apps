.class public abstract LX/2s5;
.super LX/0gG;
.source ""


# instance fields
.field private final a:LX/0gc;

.field private b:LX/0hH;

.field private c:Landroid/support/v4/app/Fragment;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0gc;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 472326
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 472327
    iput-object v0, p0, LX/2s5;->b:LX/0hH;

    .line 472328
    iput-object v0, p0, LX/2s5;->c:Landroid/support/v4/app/Fragment;

    .line 472329
    iput-object p1, p0, LX/2s5;->a:LX/0gc;

    .line 472330
    return-void
.end method

.method private static a(IJ)Ljava/lang/String;
    .locals 3

    .prologue
    .line 472325
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android:switcher:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a(I)Landroid/support/v4/app/Fragment;
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 472279
    iget-object v0, p0, LX/2s5;->b:LX/0hH;

    if-nez v0, :cond_0

    .line 472280
    iget-object v0, p0, LX/2s5;->a:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iput-object v0, p0, LX/2s5;->b:LX/0hH;

    .line 472281
    :cond_0
    invoke-virtual {p0, p2}, LX/2s5;->b(I)J

    move-result-wide v4

    .line 472282
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    invoke-static {v0, v4, v5}, LX/2s5;->a(IJ)Ljava/lang/String;

    move-result-object v0

    .line 472283
    iget-object v1, p0, LX/2s5;->a:LX/0gc;

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 472284
    if-eqz v0, :cond_3

    .line 472285
    iget-object v1, p0, LX/2s5;->b:LX/0hH;

    invoke-virtual {v1, v0}, LX/0hH;->e(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 472286
    :goto_0
    iget-object v1, p0, LX/2s5;->d:Ljava/util/List;

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    .line 472287
    :goto_1
    if-eqz v1, :cond_1

    .line 472288
    iget-object v3, p0, LX/2s5;->d:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 472289
    :cond_1
    iget-object v3, p0, LX/2s5;->c:Landroid/support/v4/app/Fragment;

    if-eq v0, v3, :cond_2

    .line 472290
    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 472291
    if-nez v1, :cond_2

    .line 472292
    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 472293
    :cond_2
    return-object v0

    .line 472294
    :cond_3
    invoke-virtual {p0, p2}, LX/2s5;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 472295
    iget-object v1, p0, LX/2s5;->b:LX/0hH;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v6

    invoke-static {v6, v4, v5}, LX/2s5;->a(IJ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v0, v4}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    goto :goto_0

    :cond_4
    move v1, v2

    .line 472296
    goto :goto_1
.end method

.method public final a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0

    .prologue
    .line 472324
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 472322
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2s5;->d:Ljava/util/List;

    .line 472323
    return-void
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 472331
    iget-object v0, p0, LX/2s5;->b:LX/0hH;

    if-nez v0, :cond_0

    .line 472332
    iget-object v0, p0, LX/2s5;->a:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iput-object v0, p0, LX/2s5;->b:LX/0hH;

    .line 472333
    :cond_0
    iget-object v0, p0, LX/2s5;->b:LX/0hH;

    check-cast p3, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p3}, LX/0hH;->d(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 472334
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 472319
    check-cast p2, Landroid/support/v4/app/Fragment;

    .line 472320
    iget-object v0, p2, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 472321
    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)J
    .locals 2

    .prologue
    .line 472318
    int-to-long v0, p1

    return-wide v0
.end method

.method public b(Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 472308
    iget-object v0, p0, LX/2s5;->d:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 472309
    iget-object v0, p0, LX/2s5;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 472310
    iget-object v2, p0, LX/2s5;->c:Landroid/support/v4/app/Fragment;

    if-eq v0, v2, :cond_0

    .line 472311
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    goto :goto_0

    .line 472312
    :cond_1
    iput-object v3, p0, LX/2s5;->d:Ljava/util/List;

    .line 472313
    :cond_2
    iget-object v0, p0, LX/2s5;->b:LX/0hH;

    if-eqz v0, :cond_3

    .line 472314
    iget-object v0, p0, LX/2s5;->b:LX/0hH;

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 472315
    iput-object v3, p0, LX/2s5;->b:LX/0hH;

    .line 472316
    iget-object v0, p0, LX/2s5;->a:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 472317
    :cond_3
    return-void
.end method

.method public b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 472298
    check-cast p3, Landroid/support/v4/app/Fragment;

    .line 472299
    iget-object v0, p0, LX/2s5;->c:Landroid/support/v4/app/Fragment;

    if-eq p3, v0, :cond_2

    .line 472300
    iget-object v0, p0, LX/2s5;->c:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 472301
    iget-object v0, p0, LX/2s5;->c:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 472302
    iget-object v0, p0, LX/2s5;->c:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 472303
    :cond_0
    if-eqz p3, :cond_1

    .line 472304
    invoke-virtual {p3, v2}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 472305
    invoke-virtual {p3, v2}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 472306
    :cond_1
    iput-object p3, p0, LX/2s5;->c:Landroid/support/v4/app/Fragment;

    .line 472307
    :cond_2
    return-void
.end method

.method public final lf_()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 472297
    const/4 v0, 0x0

    return-object v0
.end method
