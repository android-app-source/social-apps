.class public final LX/2a4;
.super LX/16B;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile h:LX/2a4;


# instance fields
.field private final b:LX/1IW;

.field private final c:LX/03V;

.field private final d:LX/1BA;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0fW;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 423350
    const-class v0, LX/2a4;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2a4;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1IW;LX/03V;LX/1BA;LX/0Or;LX/0fW;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1IW;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0fW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 423315
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 423316
    iput-object p1, p0, LX/2a4;->b:LX/1IW;

    .line 423317
    iput-object p2, p0, LX/2a4;->c:LX/03V;

    .line 423318
    iput-object p3, p0, LX/2a4;->d:LX/1BA;

    .line 423319
    iput-object p4, p0, LX/2a4;->e:LX/0Or;

    .line 423320
    iput-object p5, p0, LX/2a4;->f:LX/0fW;

    .line 423321
    return-void
.end method

.method public static a(LX/0QB;)LX/2a4;
    .locals 9

    .prologue
    .line 423337
    sget-object v0, LX/2a4;->h:LX/2a4;

    if-nez v0, :cond_1

    .line 423338
    const-class v1, LX/2a4;

    monitor-enter v1

    .line 423339
    :try_start_0
    sget-object v0, LX/2a4;->h:LX/2a4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 423340
    if-eqz v2, :cond_0

    .line 423341
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 423342
    new-instance v3, LX/2a4;

    invoke-static {v0}, LX/1I4;->a(LX/0QB;)LX/1IW;

    move-result-object v4

    check-cast v4, LX/1IW;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v6

    check-cast v6, LX/1BA;

    const/16 v7, 0x15e7

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0fW;->a(LX/0QB;)LX/0fW;

    move-result-object v8

    check-cast v8, LX/0fW;

    invoke-direct/range {v3 .. v8}, LX/2a4;-><init>(LX/1IW;LX/03V;LX/1BA;LX/0Or;LX/0fW;)V

    .line 423343
    move-object v0, v3

    .line 423344
    sput-object v0, LX/2a4;->h:LX/2a4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423345
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 423346
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 423347
    :cond_1
    sget-object v0, LX/2a4;->h:LX/2a4;

    return-object v0

    .line 423348
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 423349
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/auth/component/AuthenticationResult;)V
    .locals 3
    .param p1    # Lcom/facebook/auth/component/AuthenticationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 423331
    if-nez p1, :cond_0

    .line 423332
    iget-object v0, p0, LX/2a4;->c:LX/03V;

    sget-object v1, LX/2a4;->a:Ljava/lang/String;

    const-string v2, "AuthenticationResult is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 423333
    iget-object v0, p0, LX/2a4;->d:LX/1BA;

    const v1, 0x990009

    invoke-virtual {v0, v1}, LX/1BA;->a(I)V

    .line 423334
    iget-object v0, p0, LX/2a4;->b:LX/1IW;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1IW;->a$redex0(LX/1IW;Z)V

    .line 423335
    :goto_0
    return-void

    .line 423336
    :cond_0
    iget-object v0, p0, LX/2a4;->b:LX/1IW;

    invoke-static {v0, p1}, LX/1IW;->a$redex0(LX/1IW;Lcom/facebook/auth/component/AuthenticationResult;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 423329
    iget-object v0, p0, LX/2a4;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/2a4;->g:Ljava/lang/String;

    .line 423330
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 423322
    iget-object v0, p0, LX/2a4;->d:LX/1BA;

    const v1, 0x990002

    invoke-virtual {v0, v1}, LX/1BA;->a(I)V

    .line 423323
    iget-object v0, p0, LX/2a4;->b:LX/1IW;

    .line 423324
    iget-object v1, p0, LX/2a4;->g:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 423325
    const/4 v1, 0x0

    .line 423326
    :goto_0
    move v1, v1

    .line 423327
    invoke-static {v0, v1}, LX/1IW;->a$redex0(LX/1IW;Z)V

    .line 423328
    return-void

    :cond_0
    iget-object v1, p0, LX/2a4;->f:LX/0fW;

    iget-object v2, p0, LX/2a4;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0fW;->b(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method
