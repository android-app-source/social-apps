.class public final LX/366;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/360;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/365;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x9

    .line 498028
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 498029
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "attachmentProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "collageItem"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "imageRequest"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "draweeController"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "actualImageFocusPoint"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "itemNumber"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "visiblePhotoCount"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "invisiblePhotoCount"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/366;->b:[Ljava/lang/String;

    .line 498030
    iput v3, p0, LX/366;->c:I

    .line 498031
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/366;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/366;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/366;LX/1De;IILX/365;)V
    .locals 1

    .prologue
    .line 498024
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 498025
    iput-object p4, p0, LX/366;->a:LX/365;

    .line 498026
    iget-object v0, p0, LX/366;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 498027
    return-void
.end method


# virtual methods
.method public final a(LX/1Pm;)LX/366;
    .locals 2

    .prologue
    .line 498021
    iget-object v0, p0, LX/366;->a:LX/365;

    iput-object p1, v0, LX/365;->i:LX/1Pm;

    .line 498022
    iget-object v0, p0, LX/366;->d:Ljava/util/BitSet;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 498023
    return-object p0
.end method

.method public final a(LX/1aZ;)LX/366;
    .locals 2

    .prologue
    .line 498018
    iget-object v0, p0, LX/366;->a:LX/365;

    iput-object p1, v0, LX/365;->d:LX/1aZ;

    .line 498019
    iget-object v0, p0, LX/366;->d:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 498020
    return-object p0
.end method

.method public final a(LX/1bf;)LX/366;
    .locals 2

    .prologue
    .line 498015
    iget-object v0, p0, LX/366;->a:LX/365;

    iput-object p1, v0, LX/365;->c:LX/1bf;

    .line 498016
    iget-object v0, p0, LX/366;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 498017
    return-object p0
.end method

.method public final a(LX/26M;)LX/366;
    .locals 2

    .prologue
    .line 498012
    iget-object v0, p0, LX/366;->a:LX/365;

    iput-object p1, v0, LX/365;->b:LX/26M;

    .line 498013
    iget-object v0, p0, LX/366;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 498014
    return-object p0
.end method

.method public final a(Landroid/graphics/PointF;)LX/366;
    .locals 2

    .prologue
    .line 497981
    iget-object v0, p0, LX/366;->a:LX/365;

    iput-object p1, v0, LX/365;->e:Landroid/graphics/PointF;

    .line 497982
    iget-object v0, p0, LX/366;->d:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 497983
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/366;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/366;"
        }
    .end annotation

    .prologue
    .line 498009
    iget-object v0, p0, LX/366;->a:LX/365;

    iput-object p1, v0, LX/365;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498010
    iget-object v0, p0, LX/366;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 498011
    return-object p0
.end method

.method public final a(Z)LX/366;
    .locals 1

    .prologue
    .line 498007
    iget-object v0, p0, LX/366;->a:LX/365;

    iput-boolean p1, v0, LX/365;->k:Z

    .line 498008
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 498003
    invoke-super {p0}, LX/1X5;->a()V

    .line 498004
    const/4 v0, 0x0

    iput-object v0, p0, LX/366;->a:LX/365;

    .line 498005
    sget-object v0, LX/360;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 498006
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/360;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 497993
    iget-object v1, p0, LX/366;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/366;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/366;->c:I

    if-ge v1, v2, :cond_2

    .line 497994
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 497995
    :goto_0
    iget v2, p0, LX/366;->c:I

    if-ge v0, v2, :cond_1

    .line 497996
    iget-object v2, p0, LX/366;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 497997
    iget-object v2, p0, LX/366;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 497998
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 497999
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 498000
    :cond_2
    iget-object v0, p0, LX/366;->a:LX/365;

    .line 498001
    invoke-virtual {p0}, LX/366;->a()V

    .line 498002
    return-object v0
.end method

.method public final h(I)LX/366;
    .locals 2

    .prologue
    .line 497990
    iget-object v0, p0, LX/366;->a:LX/365;

    iput p1, v0, LX/365;->f:I

    .line 497991
    iget-object v0, p0, LX/366;->d:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 497992
    return-object p0
.end method

.method public final i(I)LX/366;
    .locals 2

    .prologue
    .line 497987
    iget-object v0, p0, LX/366;->a:LX/365;

    iput p1, v0, LX/365;->g:I

    .line 497988
    iget-object v0, p0, LX/366;->d:Ljava/util/BitSet;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 497989
    return-object p0
.end method

.method public final j(I)LX/366;
    .locals 2

    .prologue
    .line 497984
    iget-object v0, p0, LX/366;->a:LX/365;

    iput p1, v0, LX/365;->h:I

    .line 497985
    iget-object v0, p0, LX/366;->d:Ljava/util/BitSet;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 497986
    return-object p0
.end method
