.class public LX/22i;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/0pe;

.field private final b:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;LX/0pe;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 361130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361131
    iput-object p1, p0, LX/22i;->b:LX/0SG;

    .line 361132
    iput-object p2, p0, LX/22i;->a:LX/0pe;

    .line 361133
    return-void
.end method

.method public static a(LX/0QB;)LX/22i;
    .locals 3

    .prologue
    .line 361134
    new-instance v2, LX/22i;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0pe;->a(LX/0QB;)LX/0pe;

    move-result-object v1

    check-cast v1, LX/0pe;

    invoke-direct {v2, v0, v1}, LX/22i;-><init>(LX/0SG;LX/0pe;)V

    .line 361135
    move-object v0, v2

    .line 361136
    return-object v0
.end method

.method private static a(LX/22i;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;J)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 361108
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_0

    move v0, v1

    .line 361109
    :goto_0
    return v0

    .line 361110
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 361111
    if-eqz v0, :cond_1

    iget-object v2, p0, LX/22i;->a:LX/0pe;

    .line 361112
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 361113
    invoke-virtual {v2, v0}, LX/0pe;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v2, LX/0pe;->a:LX/0pf;

    invoke-virtual {v4, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v4

    .line 361114
    iget-wide v6, v4, LX/1g0;->e:J

    move-wide v4, v6

    .line 361115
    cmp-long v4, v4, p2

    if-gez v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    move v0, v4

    .line 361116
    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedResult;LX/0fz;J)Landroid/util/Pair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feed/FetchFeedResult;",
            "LX/0fz;",
            "J)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 361117
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_0

    .line 361118
    new-instance v0, Landroid/util/Pair;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 361119
    :goto_0
    return-object v0

    .line 361120
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v4

    move v1, v4

    move v2, v4

    :goto_1
    if-ge v3, v6, :cond_2

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 361121
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_UNREAD:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->n()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/graphql/enums/GraphQLBumpReason;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 361122
    add-int/lit8 v2, v2, 0x1

    .line 361123
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0fz;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 361124
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 361125
    add-int/lit8 v0, v1, 0x1

    move v1, v2

    .line 361126
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 361127
    :cond_1
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-static {p0, v0, p3, p4}, LX/22i;->a(LX/22i;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;J)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 361128
    add-int/lit8 v0, v1, 0x1

    move v1, v2

    goto :goto_2

    .line 361129
    :cond_2
    new-instance v0, Landroid/util/Pair;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v0, v1

    move v1, v2

    goto :goto_2
.end method
