.class public final LX/3Aa;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/3AZ;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/8yo;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 525590
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 525591
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "linkableTextWithEntities"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/3Aa;->b:[Ljava/lang/String;

    .line 525592
    iput v3, p0, LX/3Aa;->c:I

    .line 525593
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/3Aa;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/3Aa;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/3Aa;LX/1De;IILX/8yo;)V
    .locals 1

    .prologue
    .line 525586
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 525587
    iput-object p4, p0, LX/3Aa;->a:LX/8yo;

    .line 525588
    iget-object v0, p0, LX/3Aa;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 525589
    return-void
.end method


# virtual methods
.method public final a(LX/3Ab;)LX/3Aa;
    .locals 2

    .prologue
    .line 525583
    iget-object v0, p0, LX/3Aa;->a:LX/8yo;

    iput-object p1, v0, LX/8yo;->a:LX/3Ab;

    .line 525584
    iget-object v0, p0, LX/3Aa;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 525585
    return-object p0
.end method

.method public final a(Landroid/text/Layout$Alignment;)LX/3Aa;
    .locals 1

    .prologue
    .line 525581
    iget-object v0, p0, LX/3Aa;->a:LX/8yo;

    iput-object p1, v0, LX/8yo;->e:Landroid/text/Layout$Alignment;

    .line 525582
    return-object p0
.end method

.method public final a(Landroid/text/TextUtils$TruncateAt;)LX/3Aa;
    .locals 1

    .prologue
    .line 525579
    iget-object v0, p0, LX/3Aa;->a:LX/8yo;

    iput-object p1, v0, LX/8yo;->c:Landroid/text/TextUtils$TruncateAt;

    .line 525580
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 525575
    invoke-super {p0}, LX/1X5;->a()V

    .line 525576
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Aa;->a:LX/8yo;

    .line 525577
    sget-object v0, LX/3AZ;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 525578
    return-void
.end method

.method public final b(Z)LX/3Aa;
    .locals 1

    .prologue
    .line 525594
    iget-object v0, p0, LX/3Aa;->a:LX/8yo;

    iput-boolean p1, v0, LX/8yo;->h:Z

    .line 525595
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/3AZ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 525565
    iget-object v1, p0, LX/3Aa;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/3Aa;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/3Aa;->c:I

    if-ge v1, v2, :cond_2

    .line 525566
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 525567
    :goto_0
    iget v2, p0, LX/3Aa;->c:I

    if-ge v0, v2, :cond_1

    .line 525568
    iget-object v2, p0, LX/3Aa;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 525569
    iget-object v2, p0, LX/3Aa;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 525570
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 525571
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 525572
    :cond_2
    iget-object v0, p0, LX/3Aa;->a:LX/8yo;

    .line 525573
    invoke-virtual {p0}, LX/3Aa;->a()V

    .line 525574
    return-object v0
.end method

.method public final h(I)LX/3Aa;
    .locals 1

    .prologue
    .line 525563
    iget-object v0, p0, LX/3Aa;->a:LX/8yo;

    iput p1, v0, LX/8yo;->g:I

    .line 525564
    return-object p0
.end method

.method public final j(I)LX/3Aa;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 525561
    iget-object v0, p0, LX/3Aa;->a:LX/8yo;

    invoke-virtual {p0, p1}, LX/1Dp;->d(I)I

    move-result v1

    iput v1, v0, LX/8yo;->j:I

    .line 525562
    return-object p0
.end method

.method public final l(I)LX/3Aa;
    .locals 1

    .prologue
    .line 525559
    iget-object v0, p0, LX/3Aa;->a:LX/8yo;

    iput p1, v0, LX/8yo;->l:I

    .line 525560
    return-object p0
.end method

.method public final m(I)LX/3Aa;
    .locals 1

    .prologue
    .line 525557
    iget-object v0, p0, LX/3Aa;->a:LX/8yo;

    iput p1, v0, LX/8yo;->m:I

    .line 525558
    return-object p0
.end method

.method public final n(I)LX/3Aa;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 525555
    iget-object v0, p0, LX/3Aa;->a:LX/8yo;

    invoke-virtual {p0, p1}, LX/1Dp;->e(I)I

    move-result v1

    iput v1, v0, LX/8yo;->n:I

    .line 525556
    return-object p0
.end method
