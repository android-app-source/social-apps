.class public LX/2PM;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 406371
    const-class v0, LX/2PM;

    sput-object v0, LX/2PM;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406373
    iput-object p1, p0, LX/2PM;->b:Landroid/content/Context;

    .line 406374
    iput-object p2, p0, LX/2PM;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 406375
    return-void
.end method

.method public static a(LX/0QB;)LX/2PM;
    .locals 1

    .prologue
    .line 406376
    invoke-static {p0}, LX/2PM;->b(LX/0QB;)LX/2PM;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2PM;
    .locals 3

    .prologue
    .line 406377
    new-instance v2, LX/2PM;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v2, v0, v1}, LX/2PM;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 406378
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 406379
    invoke-static {p1, p2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 406380
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2PM;->a(Ljava/lang/String;)V

    .line 406381
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 406382
    iget-object v0, p0, LX/2PM;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Dp9;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406383
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/facebook/messaging/tincan/TincanDebugReporter$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/messaging/tincan/TincanDebugReporter$1;-><init>(LX/2PM;Ljava/lang/String;)V

    const v2, -0x5f6024ae

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 406384
    :cond_0
    return-void
.end method
