.class public LX/2j5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2j5;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 452616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 452617
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2j5;->a:Ljava/util/Map;

    .line 452618
    return-void
.end method

.method public static a(LX/0QB;)LX/2j5;
    .locals 3

    .prologue
    .line 452619
    sget-object v0, LX/2j5;->b:LX/2j5;

    if-nez v0, :cond_1

    .line 452620
    const-class v1, LX/2j5;

    monitor-enter v1

    .line 452621
    :try_start_0
    sget-object v0, LX/2j5;->b:LX/2j5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 452622
    if-eqz v2, :cond_0

    .line 452623
    :try_start_1
    new-instance v0, LX/2j5;

    invoke-direct {v0}, LX/2j5;-><init>()V

    .line 452624
    move-object v0, v0

    .line 452625
    sput-object v0, LX/2j5;->b:LX/2j5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 452626
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 452627
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 452628
    :cond_1
    sget-object v0, LX/2j5;->b:LX/2j5;

    return-object v0

    .line 452629
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 452630
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
