.class public final enum LX/2oF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2oF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2oF;

.field public static final enum MIRROR_HORIZONTALLY:LX/2oF;

.field public static final enum NONE:LX/2oF;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 465794
    new-instance v0, LX/2oF;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/2oF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2oF;->NONE:LX/2oF;

    .line 465795
    new-instance v0, LX/2oF;

    const-string v1, "MIRROR_HORIZONTALLY"

    invoke-direct {v0, v1, v3}, LX/2oF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2oF;->MIRROR_HORIZONTALLY:LX/2oF;

    .line 465796
    const/4 v0, 0x2

    new-array v0, v0, [LX/2oF;

    sget-object v1, LX/2oF;->NONE:LX/2oF;

    aput-object v1, v0, v2

    sget-object v1, LX/2oF;->MIRROR_HORIZONTALLY:LX/2oF;

    aput-object v1, v0, v3

    sput-object v0, LX/2oF;->$VALUES:[LX/2oF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 465797
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2oF;
    .locals 1

    .prologue
    .line 465798
    const-class v0, LX/2oF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2oF;

    return-object v0
.end method

.method public static values()[LX/2oF;
    .locals 1

    .prologue
    .line 465799
    sget-object v0, LX/2oF;->$VALUES:[LX/2oF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2oF;

    return-object v0
.end method
