.class public final LX/3Ma;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "Ljava/lang/Void;",
        "LX/3MZ;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/divebar/contacts/DivebarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V
    .locals 0

    .prologue
    .line 555144
    iput-object p1, p0, LX/3Ma;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 555143
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 555114
    check-cast p2, LX/3MZ;

    .line 555115
    iget-object v0, p0, LX/3Ma;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    const/4 v2, 0x1

    .line 555116
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_1

    .line 555117
    :cond_0
    :goto_0
    return-void

    .line 555118
    :cond_1
    if-eqz p2, :cond_0

    .line 555119
    iget v1, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->O:I

    if-eqz v1, :cond_2

    iget-wide v3, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->N:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-lez v1, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iget-wide v5, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->N:J

    sub-long/2addr v3, v5

    const-wide/32 v5, 0x1d4c0

    cmp-long v1, v3, v5

    if-ltz v1, :cond_0

    .line 555120
    :cond_2
    const/4 v1, 0x0

    .line 555121
    iget-boolean v3, p2, LX/3MZ;->A:Z

    move v3, v3

    .line 555122
    iput-boolean v3, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->P:Z

    .line 555123
    iget-object v3, p2, LX/3MZ;->h:LX/0Px;

    move-object v3, v3

    .line 555124
    if-eqz v3, :cond_3

    .line 555125
    iget-object v1, p2, LX/3MZ;->h:LX/0Px;

    move-object v1, v1

    .line 555126
    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->K:Ljava/util/List;

    move v1, v2

    .line 555127
    :cond_3
    iget-object v3, p2, LX/3MZ;->d:LX/0Px;

    move-object v3, v3

    .line 555128
    if-eqz v3, :cond_4

    .line 555129
    iget-object v1, p2, LX/3MZ;->d:LX/0Px;

    move-object v1, v1

    .line 555130
    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->L:Ljava/util/List;

    move v1, v2

    .line 555131
    :cond_4
    iget-object v3, p2, LX/3MZ;->b:LX/0Px;

    move-object v3, v3

    .line 555132
    if-eqz v3, :cond_6

    .line 555133
    iget-object v1, p2, LX/3MZ;->b:LX/0Px;

    move-object v1, v1

    .line 555134
    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->J:Ljava/util/List;

    .line 555135
    :goto_1
    if-nez v2, :cond_5

    .line 555136
    iget-boolean v1, p2, LX/3MZ;->A:Z

    move v1, v1

    .line 555137
    if-nez v1, :cond_0

    :cond_5
    iget-object v1, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    if-eqz v1, :cond_0

    .line 555138
    invoke-static {v0}, Lcom/facebook/divebar/contacts/DivebarFragment;->r(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    .line 555139
    iget-object v1, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x5f0001

    const/16 v3, 0x1f

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 555140
    iget-object v1, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    new-instance v2, LX/3OW;

    invoke-direct {v2, v0}, LX/3OW;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    .line 555141
    iput-object v2, v1, LX/3Na;->h:LX/3OW;

    .line 555142
    goto :goto_0

    :cond_6
    move v2, v1

    goto :goto_1
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 555111
    return-void
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 555112
    iget-object v0, p0, LX/3Ma;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    iget-object v0, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x5f0001

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 555113
    return-void
.end method
