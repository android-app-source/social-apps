.class public LX/2gS;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/2bA;

.field public static final e:LX/2bA;

.field public static final f:LX/2bA;

.field private static final g:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 448331
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "znotif/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 448332
    sput-object v0, LX/2gS;->g:LX/0Tn;

    const-string v1, "last_active_device_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2gS;->a:LX/0Tn;

    .line 448333
    sget-object v0, LX/2gS;->g:LX/0Tn;

    const-string v1, "last_web_sent_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2gS;->b:LX/0Tn;

    .line 448334
    sget-object v0, LX/2gS;->g:LX/0Tn;

    const-string v1, "oldest_zp_wakeup"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2gS;->c:LX/0Tn;

    .line 448335
    new-instance v0, LX/2bA;

    const-string v1, "/znotif/"

    invoke-direct {v0, v1}, LX/2bA;-><init>(Ljava/lang/String;)V

    .line 448336
    sput-object v0, LX/2gS;->d:LX/2bA;

    const-string v1, "msg_time/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    .line 448337
    sput-object v0, LX/2gS;->e:LX/2bA;

    const-string v1, "def"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    sput-object v0, LX/2gS;->f:LX/2bA;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 448338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/service/model/NewMessageResult;)J
    .locals 2

    .prologue
    .line 448339
    iget-object v0, p0, Lcom/facebook/messaging/service/model/NewMessageResult;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v0, v0

    .line 448340
    if-nez v0, :cond_0

    .line 448341
    const-wide/16 v0, -0x1

    .line 448342
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->m:J

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 448343
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-eqz v0, :cond_0

    .line 448344
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b()Ljava/lang/String;

    move-result-object v0

    .line 448345
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 448346
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
