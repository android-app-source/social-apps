.class public LX/2Sc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final h:Ljava/lang/String;

.field private static volatile i:LX/2Sc;


# instance fields
.field public final a:LX/0Sh;

.field private final b:LX/03V;

.field public final c:LX/0kL;

.field private final d:LX/2Sd;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/Throwable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 412937
    const-class v0, LX/2Sc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Sc;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/03V;LX/0kL;LX/2Sd;LX/0Or;LX/0Or;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/search/common/errors/ShouldToastGraphSearchErrors;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0kL;",
            "LX/2Sd;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 412929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412930
    iput-object p1, p0, LX/2Sc;->a:LX/0Sh;

    .line 412931
    iput-object p2, p0, LX/2Sc;->b:LX/03V;

    .line 412932
    iput-object p3, p0, LX/2Sc;->c:LX/0kL;

    .line 412933
    iput-object p4, p0, LX/2Sc;->d:LX/2Sd;

    .line 412934
    iput-object p5, p0, LX/2Sc;->e:LX/0Or;

    .line 412935
    iput-object p6, p0, LX/2Sc;->f:LX/0Or;

    .line 412936
    return-void
.end method

.method public static a(LX/0QB;)LX/2Sc;
    .locals 10

    .prologue
    .line 412916
    sget-object v0, LX/2Sc;->i:LX/2Sc;

    if-nez v0, :cond_1

    .line 412917
    const-class v1, LX/2Sc;

    monitor-enter v1

    .line 412918
    :try_start_0
    sget-object v0, LX/2Sc;->i:LX/2Sc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 412919
    if-eqz v2, :cond_0

    .line 412920
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 412921
    new-instance v3, LX/2Sc;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static {v0}, LX/2Sd;->a(LX/0QB;)LX/2Sd;

    move-result-object v7

    check-cast v7, LX/2Sd;

    const/16 v8, 0x2fd

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x156e

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/2Sc;-><init>(LX/0Sh;LX/03V;LX/0kL;LX/2Sd;LX/0Or;LX/0Or;)V

    .line 412922
    move-object v0, v3

    .line 412923
    sput-object v0, LX/2Sc;->i:LX/2Sc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412924
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 412925
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 412926
    :cond_1
    sget-object v0, LX/2Sc;->i:LX/2Sc;

    return-object v0

    .line 412927
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 412928
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 412914
    iget-object v0, p0, LX/2Sc;->d:LX/2Sd;

    sget-object v1, LX/2Sc;->h:Ljava/lang/String;

    sget-object v2, LX/7CQ;->SOFT_ERROR:LX/7CQ;

    invoke-virtual {v0, v1, v2, p1}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;Ljava/lang/String;)V

    .line 412915
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 412907
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 412908
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412909
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412910
    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412911
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 412912
    invoke-direct {p0, v0}, LX/2Sc;->a(Ljava/lang/String;)V

    .line 412913
    return-void
.end method

.method private c(LX/3Ql;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 412861
    sget-object v0, LX/03R;->YES:LX/03R;

    iget-object v1, p0, LX/2Sc;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Sc;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412862
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[FB Only] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/3Ql;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " => "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 412863
    new-instance v1, LX/27k;

    invoke-direct {v1, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    .line 412864
    iget-object v0, p0, LX/2Sc;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 412865
    iget-object v0, p0, LX/2Sc;->c:LX/0kL;

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 412866
    :cond_0
    :goto_0
    return-void

    .line 412867
    :cond_1
    iget-object v0, p0, LX/2Sc;->a:LX/0Sh;

    new-instance p1, Lcom/facebook/search/common/errors/GraphSearchErrorReporter$1;

    invoke-direct {p1, p0, v1}, Lcom/facebook/search/common/errors/GraphSearchErrorReporter$1;-><init>(LX/2Sc;LX/27k;)V

    invoke-virtual {v0, p1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static d(LX/3Ql;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 412902
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 412903
    invoke-virtual {p0}, LX/3Ql;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412904
    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412905
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412906
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/3Ql;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 412896
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0, p2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/2Sc;->g:Ljava/lang/Throwable;

    .line 412897
    invoke-static {p1, p2}, LX/2Sc;->d(LX/3Ql;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 412898
    iget-object v1, p0, LX/2Sc;->b:LX/03V;

    invoke-virtual {v1, v0, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 412899
    invoke-direct {p0, v0}, LX/2Sc;->a(Ljava/lang/String;)V

    .line 412900
    invoke-direct {p0, p1, p2}, LX/2Sc;->c(LX/3Ql;Ljava/lang/String;)V

    .line 412901
    return-void
.end method

.method public final a(LX/3Ql;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 412884
    iput-object p3, p0, LX/2Sc;->g:Ljava/lang/Throwable;

    .line 412885
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 412886
    invoke-virtual {p1}, LX/3Ql;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412887
    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412888
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412889
    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412890
    invoke-virtual {p3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412891
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 412892
    iget-object v1, p0, LX/2Sc;->b:LX/03V;

    invoke-virtual {v1, v0, p2, p3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 412893
    invoke-direct {p0, v0, p3}, LX/2Sc;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 412894
    invoke-virtual {p3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/2Sc;->c(LX/3Ql;Ljava/lang/String;)V

    .line 412895
    return-void
.end method

.method public final a(LX/3Ql;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 412878
    iput-object p2, p0, LX/2Sc;->g:Ljava/lang/Throwable;

    .line 412879
    invoke-virtual {p2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/2Sc;->d(LX/3Ql;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 412880
    iget-object v1, p0, LX/2Sc;->b:LX/03V;

    invoke-virtual {v1, v0, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 412881
    invoke-direct {p0, v0, p2}, LX/2Sc;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 412882
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/2Sc;->c(LX/3Ql;Ljava/lang/String;)V

    .line 412883
    return-void
.end method

.method public final a(LX/7C4;)V
    .locals 1

    .prologue
    .line 412874
    iput-object p1, p0, LX/2Sc;->g:Ljava/lang/Throwable;

    .line 412875
    iget-object v0, p1, LX/7C4;->mError:LX/3Ql;

    move-object v0, v0

    .line 412876
    invoke-virtual {p0, v0, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 412877
    return-void
.end method

.method public final b(LX/3Ql;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 412868
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0, p2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/2Sc;->g:Ljava/lang/Throwable;

    .line 412869
    invoke-static {p1, p2}, LX/2Sc;->d(LX/3Ql;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 412870
    iget-object v1, p0, LX/2Sc;->b:LX/03V;

    invoke-virtual {v1, v0, p2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 412871
    invoke-direct {p0, v0}, LX/2Sc;->a(Ljava/lang/String;)V

    .line 412872
    invoke-direct {p0, p1, p2}, LX/2Sc;->c(LX/3Ql;Ljava/lang/String;)V

    .line 412873
    return-void
.end method
