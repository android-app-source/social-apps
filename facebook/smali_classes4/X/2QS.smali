.class public LX/2QS;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 408359
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "platform_webdialogs/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 408360
    sput-object v0, LX/2QS;->a:LX/0Tn;

    const-string v1, "internal_settings/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 408361
    sput-object v0, LX/2QS;->b:LX/0Tn;

    const-string v1, "reset_per_action"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2QS;->c:LX/0Tn;

    .line 408362
    sget-object v0, LX/2QS;->b:LX/0Tn;

    const-string v1, "disable_refresh"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2QS;->d:LX/0Tn;

    .line 408363
    sget-object v0, LX/2QS;->a:LX/0Tn;

    const-string v1, "cache_settings/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 408364
    sput-object v0, LX/2QS;->e:LX/0Tn;

    const-string v1, "read_write_failure_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2QS;->f:LX/0Tn;

    .line 408365
    sget-object v0, LX/2QS;->e:LX/0Tn;

    const-string v1, "url_map_file_name"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2QS;->g:LX/0Tn;

    .line 408366
    sget-object v0, LX/2QS;->a:LX/0Tn;

    const-string v1, "manifest/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 408367
    sput-object v0, LX/2QS;->h:LX/0Tn;

    const-string v1, "file_name"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2QS;->i:LX/0Tn;

    .line 408368
    sget-object v0, LX/2QS;->h:LX/0Tn;

    const-string v1, "last_update_timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2QS;->j:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 408369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
