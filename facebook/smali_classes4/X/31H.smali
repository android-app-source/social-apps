.class public LX/31H;
.super LX/2oy;
.source ""


# static fields
.field public static final e:Ljava/lang/String;


# instance fields
.field public a:LX/3Hu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3HR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final f:LX/3Hv;

.field public final n:LX/3GF;

.field public o:I

.field private p:I

.field public q:I

.field private r:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:LX/D6v;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:LX/0lF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 486840
    const-class v0, LX/31H;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/31H;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 486838
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/31H;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 486839
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 486769
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/31H;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 486770
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 486830
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 486831
    new-instance v0, LX/3Ht;

    invoke-direct {v0, p0}, LX/3Ht;-><init>(LX/31H;)V

    iput-object v0, p0, LX/31H;->n:LX/3GF;

    .line 486832
    const-class v0, LX/31H;

    invoke-static {v0, p0}, LX/31H;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 486833
    new-instance v0, LX/3Hv;

    invoke-direct {v0, p0}, LX/3Hv;-><init>(LX/31H;)V

    iput-object v0, p0, LX/31H;->f:LX/3Hv;

    .line 486834
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Hw;

    invoke-direct {v1, p0}, LX/3Hw;-><init>(LX/31H;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 486835
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Hx;

    invoke-direct {v1, p0}, LX/3Hx;-><init>(LX/31H;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 486836
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Hy;

    invoke-direct {v1, p0}, LX/3Hy;-><init>(LX/31H;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 486837
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/31H;

    invoke-static {p0}, LX/3Hu;->a(LX/0QB;)LX/3Hu;

    move-result-object v1

    check-cast v1, LX/3Hu;

    invoke-static {p0}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-result-object v2

    check-cast v2, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-static {p0}, LX/3HR;->b(LX/0QB;)LX/3HR;

    move-result-object v3

    check-cast v3, LX/3HR;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p0

    check-cast p0, LX/03V;

    iput-object v1, p1, LX/31H;->a:LX/3Hu;

    iput-object v2, p1, LX/31H;->b:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iput-object v3, p1, LX/31H;->c:LX/3HR;

    iput-object p0, p1, LX/31H;->d:LX/03V;

    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 486827
    iget-object v0, p0, LX/31H;->t:LX/D6v;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/31H;->t:LX/D6v;

    iget-object v0, v0, LX/D6v;->a:LX/2oN;

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    if-eq v0, v1, :cond_0

    .line 486828
    iget-object v0, p0, LX/31H;->t:LX/D6v;

    iget-object v1, p0, LX/31H;->v:LX/0lF;

    invoke-virtual {v0, v1}, LX/D6v;->a(LX/0lF;)V

    .line 486829
    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 486824
    iget-object v0, p0, LX/31H;->t:LX/D6v;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/31H;->t:LX/D6v;

    iget-object v0, v0, LX/D6v;->a:LX/2oN;

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    if-eq v0, v1, :cond_0

    .line 486825
    iget-object v0, p0, LX/31H;->t:LX/D6v;

    iget-object v1, p0, LX/31H;->v:LX/0lF;

    invoke-virtual {v0, v1}, LX/D6v;->b(LX/0lF;)V

    .line 486826
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 4

    .prologue
    .line 486795
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 486796
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->s()LX/04D;

    move-result-object v0

    sget-object v1, LX/04D;->FEED:LX/04D;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/31H;->a:LX/3Hu;

    iget-boolean v0, v0, LX/3Hu;->d:Z

    if-nez v0, :cond_1

    .line 486797
    :cond_0
    :goto_0
    return-void

    .line 486798
    :cond_1
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486799
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 486800
    instance-of v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_2

    .line 486801
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, LX/31H;->r:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 486802
    :cond_2
    iget-object v0, p0, LX/31H;->r:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 486803
    if-eqz v0, :cond_0

    .line 486804
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 486805
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 486806
    if-eqz v0, :cond_0

    .line 486807
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aj()LX/0Px;

    move-result-object v0

    .line 486808
    if-eqz v0, :cond_0

    .line 486809
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    .line 486810
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->l()Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->POST_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    if-ne v2, v3, :cond_3

    .line 486811
    iput-object v0, p0, LX/31H;->u:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    .line 486812
    :cond_4
    iget-object v0, p0, LX/31H;->u:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    if-eqz v0, :cond_0

    .line 486813
    invoke-virtual {p0}, LX/31H;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, LX/31H;->q:I

    .line 486814
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 486815
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 486816
    sget-object v1, LX/04G;->CHANNEL_PLAYER:LX/04G;

    if-ne v0, v1, :cond_5

    iget v0, p0, LX/31H;->q:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 486817
    :cond_5
    iget-object v0, p0, LX/31H;->a:LX/3Hu;

    iget-boolean v0, v0, LX/3Hu;->b:Z

    if-eqz v0, :cond_0

    .line 486818
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, LX/31H;->s:Ljava/lang/String;

    .line 486819
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iput-object v0, p0, LX/31H;->v:LX/0lF;

    .line 486820
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    iput v0, p0, LX/31H;->p:I

    .line 486821
    iget v0, p0, LX/31H;->p:I

    iget-object v1, p0, LX/31H;->a:LX/3Hu;

    iget v1, v1, LX/3Hu;->c:I

    sub-int/2addr v0, v1

    iput v0, p0, LX/31H;->o:I

    .line 486822
    if-eqz p2, :cond_0

    .line 486823
    iget-object v0, p0, LX/31H;->f:LX/3Hv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3Hv;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 486788
    iput-object v1, p0, LX/31H;->r:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 486789
    iput-object v1, p0, LX/31H;->s:Ljava/lang/String;

    .line 486790
    iget-object v0, p0, LX/31H;->f:LX/3Hv;

    invoke-virtual {v0, v1}, LX/3Hv;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 486791
    iput-object v1, p0, LX/31H;->u:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    .line 486792
    iput-object v1, p0, LX/31H;->t:LX/D6v;

    .line 486793
    iput-object v1, p0, LX/31H;->v:LX/0lF;

    .line 486794
    return-void
.end method

.method public final dH_()V
    .locals 0

    .prologue
    .line 486786
    invoke-direct {p0}, LX/31H;->h()V

    .line 486787
    return-void
.end method

.method public final dI_()V
    .locals 2

    .prologue
    .line 486781
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 486782
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 486783
    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-eq v0, v1, :cond_0

    .line 486784
    invoke-direct {p0}, LX/31H;->h()V

    .line 486785
    :cond_0
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 486778
    iget-object v0, p0, LX/31H;->t:LX/D6v;

    if-eqz v0, :cond_0

    .line 486779
    iget-object v0, p0, LX/31H;->t:LX/D6v;

    invoke-virtual {v0}, LX/D6v;->f()V

    .line 486780
    :cond_0
    return-void
.end method

.method public final s()V
    .locals 0

    .prologue
    .line 486776
    invoke-direct {p0}, LX/31H;->i()V

    .line 486777
    return-void
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 486771
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 486772
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 486773
    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-eq v0, v1, :cond_0

    .line 486774
    invoke-direct {p0}, LX/31H;->i()V

    .line 486775
    :cond_0
    return-void
.end method
