.class public LX/3MC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private cache:LX/3MD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3MD",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/regex/Pattern;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 553597
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 553598
    new-instance v0, LX/3MD;

    invoke-direct {v0, p1}, LX/3MD;-><init>(I)V

    iput-object v0, p0, LX/3MC;->cache:LX/3MD;

    .line 553599
    return-void
.end method


# virtual methods
.method public getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 2

    .prologue
    .line 553600
    iget-object v0, p0, LX/3MC;->cache:LX/3MD;

    invoke-virtual {v0, p1}, LX/3MD;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/regex/Pattern;

    .line 553601
    if-nez v0, :cond_0

    .line 553602
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 553603
    iget-object v1, p0, LX/3MC;->cache:LX/3MD;

    invoke-virtual {v1, p1, v0}, LX/3MD;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 553604
    :cond_0
    return-object v0
.end method
