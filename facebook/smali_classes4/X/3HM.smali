.class public LX/3HM;
.super LX/3HN;
.source ""


# instance fields
.field public final h:LX/0sa;

.field public final i:LX/0SG;

.field public final j:LX/0jU;

.field public final k:LX/3HO;

.field public l:LX/0sf;

.field private final m:LX/0tG;

.field private final n:LX/0tI;

.field public o:LX/3HE;

.field private p:LX/0sU;

.field private final q:LX/0wp;

.field private final r:LX/0tQ;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0sa;LX/0rq;LX/0se;LX/0sO;LX/0SG;LX/0jU;LX/3HO;LX/0sf;LX/0tE;LX/0tG;LX/0tI;LX/0ad;LX/3HE;LX/0sU;LX/0wo;LX/0wp;LX/0sX;LX/0tQ;)V
    .locals 14
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 543313
    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p2

    move-object/from16 v5, p5

    move-object/from16 v6, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p10

    move-object/from16 v9, p11

    move-object/from16 v10, p12

    move-object/from16 v11, p16

    move-object/from16 v12, p13

    move-object/from16 v13, p18

    invoke-direct/range {v1 .. v13}, LX/3HN;-><init>(Landroid/content/res/Resources;LX/0rq;LX/0sa;LX/0sO;LX/0se;LX/0SG;LX/0tE;LX/0tG;LX/0tI;LX/0wo;LX/0ad;LX/0sX;)V

    .line 543314
    move-object/from16 v0, p2

    iput-object v0, p0, LX/3HM;->h:LX/0sa;

    .line 543315
    move-object/from16 v0, p6

    iput-object v0, p0, LX/3HM;->i:LX/0SG;

    .line 543316
    move-object/from16 v0, p7

    iput-object v0, p0, LX/3HM;->j:LX/0jU;

    .line 543317
    move-object/from16 v0, p8

    iput-object v0, p0, LX/3HM;->k:LX/3HO;

    .line 543318
    move-object/from16 v0, p9

    iput-object v0, p0, LX/3HM;->l:LX/0sf;

    .line 543319
    move-object/from16 v0, p11

    iput-object v0, p0, LX/3HM;->m:LX/0tG;

    .line 543320
    move-object/from16 v0, p12

    iput-object v0, p0, LX/3HM;->n:LX/0tI;

    .line 543321
    move-object/from16 v0, p14

    iput-object v0, p0, LX/3HM;->o:LX/3HE;

    .line 543322
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3HM;->p:LX/0sU;

    .line 543323
    move-object/from16 v0, p17

    iput-object v0, p0, LX/3HM;->q:LX/0wp;

    .line 543324
    move-object/from16 v0, p19

    iput-object v0, p0, LX/3HM;->r:LX/0tQ;

    .line 543325
    return-void
.end method

.method public static a(LX/0QB;)LX/3HM;
    .locals 1

    .prologue
    .line 543326
    invoke-static {p0}, LX/3HM;->b(LX/0QB;)LX/3HM;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/0gW;)V
    .locals 2

    .prologue
    .line 543307
    const/4 v0, 0x1

    move v0, v0

    .line 543308
    if-eqz v0, :cond_0

    .line 543309
    const-string v0, "scrubbing"

    const-string v1, "MPEG_DASH"

    invoke-virtual {p0, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543310
    :cond_0
    return-void
.end method

.method private b(Lcom/facebook/api/story/FetchSingleStoryParams;)LX/0gW;
    .locals 3

    .prologue
    .line 543283
    sget-object v0, LX/80h;->a:[I

    iget-object v1, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->d:LX/5Go;

    invoke-virtual {v1}, LX/5Go;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 543284
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "UNSUPPORTED"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 543285
    :pswitch_0
    new-instance v0, LX/80e;

    invoke-direct {v0}, LX/80e;-><init>()V

    move-object v0, v0

    .line 543286
    const-string v1, "enable_download"

    iget-object v2, p0, LX/3HM;->r:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->c()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543287
    invoke-static {v0}, LX/3HM;->a(LX/0gW;)V

    .line 543288
    :goto_0
    const/4 v1, 0x1

    .line 543289
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 543290
    invoke-virtual {p0, p1, v0}, LX/3HN;->a(Lcom/facebook/api/story/FetchSingleStoryParams;LX/0gW;)LX/0gW;

    .line 543291
    return-object v0

    .line 543292
    :pswitch_1
    invoke-static {}, LX/80g;->c()LX/80d;

    move-result-object v0

    goto :goto_0

    .line 543293
    :pswitch_2
    invoke-static {}, LX/80g;->c()LX/80d;

    move-result-object v0

    .line 543294
    const-string v1, "enable_download"

    iget-object v2, p0, LX/3HM;->r:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->c()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543295
    invoke-static {v0}, LX/3HM;->a(LX/0gW;)V

    goto :goto_0

    .line 543296
    :pswitch_3
    new-instance v0, LX/80f;

    invoke-direct {v0}, LX/80f;-><init>()V

    move-object v0, v0

    .line 543297
    goto :goto_0

    .line 543298
    :pswitch_4
    new-instance v0, LX/7fh;

    invoke-direct {v0}, LX/7fh;-><init>()V

    move-object v0, v0

    .line 543299
    const-string v1, "enable_download"

    iget-object v2, p0, LX/3HM;->r:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->c()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543300
    const-string v1, "enable_facepile_blingbar"

    iget-boolean v2, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543301
    const-string v1, "rich_text_posts_enabled"

    iget-boolean v2, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 543302
    invoke-static {v0}, LX/3HM;->a(LX/0gW;)V

    goto :goto_0

    .line 543303
    :pswitch_5
    new-instance v0, LX/7ff;

    invoke-direct {v0}, LX/7ff;-><init>()V

    move-object v0, v0

    .line 543304
    goto :goto_0

    .line 543305
    :pswitch_6
    new-instance v0, LX/7fe;

    invoke-direct {v0}, LX/7fe;-><init>()V

    move-object v0, v0

    .line 543306
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/3HM;
    .locals 20

    .prologue
    .line 543311
    new-instance v0, LX/3HM;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v2

    check-cast v2, LX/0sa;

    invoke-static/range {p0 .. p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v3

    check-cast v3, LX/0rq;

    invoke-static/range {p0 .. p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v4

    check-cast v4, LX/0se;

    invoke-static/range {p0 .. p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v5

    check-cast v5, LX/0sO;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0jU;->a(LX/0QB;)LX/0jU;

    move-result-object v7

    check-cast v7, LX/0jU;

    invoke-static/range {p0 .. p0}, LX/3HO;->a(LX/0QB;)LX/3HO;

    move-result-object v8

    check-cast v8, LX/3HO;

    invoke-static/range {p0 .. p0}, LX/0sf;->a(LX/0QB;)LX/0sf;

    move-result-object v9

    check-cast v9, LX/0sf;

    invoke-static/range {p0 .. p0}, LX/0tE;->a(LX/0QB;)LX/0tE;

    move-result-object v10

    check-cast v10, LX/0tE;

    invoke-static/range {p0 .. p0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v11

    check-cast v11, LX/0tG;

    invoke-static/range {p0 .. p0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v12

    check-cast v12, LX/0tI;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/3HE;->a(LX/0QB;)LX/3HE;

    move-result-object v14

    check-cast v14, LX/3HE;

    invoke-static/range {p0 .. p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v15

    check-cast v15, LX/0sU;

    invoke-static/range {p0 .. p0}, LX/0wo;->a(LX/0QB;)LX/0wo;

    move-result-object v16

    check-cast v16, LX/0wo;

    invoke-static/range {p0 .. p0}, LX/0wp;->a(LX/0QB;)LX/0wp;

    move-result-object v17

    check-cast v17, LX/0wp;

    invoke-static/range {p0 .. p0}, LX/0sX;->a(LX/0QB;)LX/0sX;

    move-result-object v18

    check-cast v18, LX/0sX;

    invoke-static/range {p0 .. p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v19

    check-cast v19, LX/0tQ;

    invoke-direct/range {v0 .. v19}, LX/3HM;-><init>(Landroid/content/res/Resources;LX/0sa;LX/0rq;LX/0se;LX/0sO;LX/0SG;LX/0jU;LX/3HO;LX/0sf;LX/0tE;LX/0tG;LX/0tI;LX/0ad;LX/3HE;LX/0sU;LX/0wo;LX/0wp;LX/0sX;LX/0tQ;)V

    .line 543312
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/story/FetchSingleStoryParams;)LX/0zO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/story/FetchSingleStoryParams;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 543254
    invoke-virtual {p0, p1, v0, v0}, LX/3HM;->a(Lcom/facebook/api/story/FetchSingleStoryParams;Ljava/lang/String;LX/3HP;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/api/story/FetchSingleStoryParams;Ljava/lang/String;LX/3HP;)LX/0zO;
    .locals 10
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/3HP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/story/FetchSingleStoryParams;",
            "Ljava/lang/String;",
            "Lcom/facebook/api/ufiservices/common/FeedbackCacheProvider;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 543255
    iget-object v0, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->d:LX/5Go;

    sget-object v1, LX/5Go;->GRAPHQL_PHOTO_CREATION_STORY:LX/5Go;

    if-ne v0, v1, :cond_0

    .line 543256
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-static {v0}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v0

    .line 543257
    :goto_0
    invoke-direct {p0, p1}, LX/3HM;->b(Lcom/facebook/api/story/FetchSingleStoryParams;)LX/0gW;

    move-result-object v1

    invoke-static {v1, v0}, LX/0zO;->a(LX/0gW;LX/0w5;)LX/0zO;

    move-result-object v0

    .line 543258
    iget-object v1, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->c:LX/0rS;

    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne v1, v2, :cond_3

    .line 543259
    iget-object v1, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->d:LX/5Go;

    sget-object v2, LX/5Go;->NOTIFICATION_FEEDBACK_DETAILS:LX/5Go;

    if-ne v1, v2, :cond_2

    .line 543260
    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 543261
    :goto_1
    const-wide/32 v2, 0x3f480

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    .line 543262
    const/4 v1, 0x1

    .line 543263
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 543264
    new-instance v4, LX/80j;

    iget-object v5, p0, LX/3HM;->l:LX/0sf;

    invoke-virtual {v5, v0}, LX/0sf;->a(LX/0zO;)LX/1ks;

    move-result-object v7

    move-object v5, p0

    move-object v6, p1

    move-object v8, p2

    move-object v9, p3

    invoke-direct/range {v4 .. v9}, LX/80j;-><init>(LX/3HM;Lcom/facebook/api/story/FetchSingleStoryParams;LX/1ks;Ljava/lang/String;LX/3HP;)V

    .line 543265
    move-object v1, v4

    .line 543266
    iput-object v1, v0, LX/0zO;->g:LX/1kt;

    .line 543267
    return-object v0

    .line 543268
    :cond_0
    iget-object v0, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->d:LX/5Go;

    sget-object v1, LX/5Go;->GRAPHQL_VIDEO_CREATION_STORY:LX/5Go;

    if-ne v0, v1, :cond_1

    .line 543269
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-static {v0}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v0

    goto :goto_0

    .line 543270
    :cond_1
    const-class v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v0

    goto :goto_0

    .line 543271
    :cond_2
    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    goto :goto_1

    .line 543272
    :cond_3
    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    goto :goto_1
.end method

.method public final b(Lcom/facebook/api/story/FetchSingleStoryParams;LX/0gW;)V
    .locals 3

    .prologue
    .line 543273
    iget-object v0, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->d:LX/5Go;

    sget-object v1, LX/5Go;->NOTIFICATION_FEEDBACK_DETAILS:LX/5Go;

    if-ne v0, v1, :cond_1

    .line 543274
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v0

    .line 543275
    if-nez v0, :cond_0

    .line 543276
    sget-object v0, LX/0wB;->a:LX/0wC;

    .line 543277
    :cond_0
    const-string v1, "image_preview_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "icon_scale"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 543278
    :cond_1
    iget-object v0, p0, LX/3HM;->m:LX/0tG;

    invoke-virtual {v0, p2}, LX/0tG;->a(LX/0gW;)V

    .line 543279
    iget-object v0, p0, LX/3HM;->n:LX/0tI;

    invoke-virtual {v0, p2}, LX/0tI;->a(LX/0gW;)V

    .line 543280
    iget-object v0, p0, LX/3HM;->p:LX/0sU;

    invoke-virtual {v0, p2}, LX/0sU;->a(LX/0gW;)V

    .line 543281
    return-void
.end method

.method public final synthetic f(Ljava/lang/Object;)LX/0gW;
    .locals 1

    .prologue
    .line 543282
    check-cast p1, Lcom/facebook/api/story/FetchSingleStoryParams;

    invoke-direct {p0, p1}, LX/3HM;->b(Lcom/facebook/api/story/FetchSingleStoryParams;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
