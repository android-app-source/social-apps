.class public LX/2uh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 476107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 476059
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_7

    .line 476060
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 476061
    :goto_0
    return v0

    .line 476062
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 476063
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 476064
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 476065
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 476066
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 476067
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 476068
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 476069
    :cond_2
    const-string v6, "object"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 476070
    invoke-static {p0, p1}, LX/2bR;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 476071
    :cond_3
    const-string v6, "taggable_activity"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 476072
    invoke-static {p0, p1}, LX/3Ao;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 476073
    :cond_4
    const-string v6, "taggable_activity_icon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 476074
    invoke-static {p0, p1}, LX/3Ar;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 476075
    :cond_5
    const-string v6, "url"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 476076
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 476077
    :cond_6
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 476078
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 476079
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 476080
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 476081
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 476082
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 476083
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_7
    move v1, v0

    move v2, v0

    move v3, v0

    move v4, v0

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 476084
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 476085
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476086
    if-eqz v0, :cond_0

    .line 476087
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476088
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476089
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476090
    if-eqz v0, :cond_1

    .line 476091
    const-string v1, "object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476092
    invoke-static {p0, v0, p2, p3}, LX/2bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 476093
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476094
    if-eqz v0, :cond_2

    .line 476095
    const-string v1, "taggable_activity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476096
    invoke-static {p0, v0, p2, p3}, LX/3Ao;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 476097
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476098
    if-eqz v0, :cond_3

    .line 476099
    const-string v1, "taggable_activity_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476100
    invoke-static {p0, v0, p2, p3}, LX/3Ar;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 476101
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476102
    if-eqz v0, :cond_4

    .line 476103
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476104
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476105
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 476106
    return-void
.end method
