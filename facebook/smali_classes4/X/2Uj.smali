.class public LX/2Uj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "LX/FIO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416500
    const/16 v0, 0xfa

    .line 416501
    new-instance v1, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v1, v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    move-object v0, v1

    .line 416502
    iput-object v0, p0, LX/2Uj;->a:Ljava/util/concurrent/BlockingQueue;

    .line 416503
    return-void
.end method

.method public static a(LX/0QB;)LX/2Uj;
    .locals 1

    .prologue
    .line 416504
    new-instance v0, LX/2Uj;

    invoke-direct {v0}, LX/2Uj;-><init>()V

    .line 416505
    move-object v0, v0

    .line 416506
    return-object v0
.end method


# virtual methods
.method public final a(LX/FIO;)V
    .locals 1

    .prologue
    .line 416507
    iget-object v0, p0, LX/2Uj;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 416508
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/FIO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 416509
    iget-object v0, p0, LX/2Uj;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->drainTo(Ljava/util/Collection;)I

    .line 416510
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 416511
    iget-object v0, p0, LX/2Uj;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    return v0
.end method
