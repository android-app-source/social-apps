.class public LX/2AJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2AJ;


# instance fields
.field public a:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 377168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2AJ;
    .locals 5

    .prologue
    .line 377169
    sget-object v0, LX/2AJ;->c:LX/2AJ;

    if-nez v0, :cond_1

    .line 377170
    const-class v1, LX/2AJ;

    monitor-enter v1

    .line 377171
    :try_start_0
    sget-object v0, LX/2AJ;->c:LX/2AJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 377172
    if-eqz v2, :cond_0

    .line 377173
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 377174
    new-instance v4, LX/2AJ;

    invoke-direct {v4}, LX/2AJ;-><init>()V

    .line 377175
    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 377176
    iput-object v3, v4, LX/2AJ;->a:LX/0SG;

    iput-object p0, v4, LX/2AJ;->b:LX/0Or;

    .line 377177
    move-object v0, v4

    .line 377178
    sput-object v0, LX/2AJ;->c:LX/2AJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377179
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 377180
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 377181
    :cond_1
    sget-object v0, LX/2AJ;->c:LX/2AJ;

    return-object v0

    .line 377182
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 377183
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)LX/2nq;
    .locals 2

    .prologue
    .line 377184
    new-instance v0, LX/BBE;

    invoke-direct {v0}, LX/BBE;-><init>()V

    invoke-virtual {v0, p1}, LX/BBE;->b(Ljava/lang/String;)LX/BBE;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/BBE;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/BBE;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BBE;->a(LX/0Px;)LX/BBE;

    move-result-object v0

    invoke-virtual {v0}, LX/BBE;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2AJ;LX/BAS;Z)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 377185
    if-eqz p2, :cond_0

    .line 377186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 377187
    iget-object v1, p1, LX/BAS;->a:LX/BAT;

    move-object v1, v1

    .line 377188
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_unique"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 377189
    :goto_0
    iget-object v1, p0, LX/2AJ;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 377190
    new-instance v1, LX/23u;

    invoke-direct {v1}, LX/23u;-><init>()V

    const/4 v4, 0x1

    .line 377191
    iput-boolean v4, v1, LX/23u;->o:Z

    .line 377192
    move-object v1, v1

    .line 377193
    const/4 v4, 0x0

    .line 377194
    iput-boolean v4, v1, LX/23u;->p:Z

    .line 377195
    move-object v1, v1

    .line 377196
    iput-wide v2, v1, LX/23u;->v:J

    .line 377197
    move-object v1, v1

    .line 377198
    iput-wide v2, v1, LX/23u;->G:J

    .line 377199
    move-object v1, v1

    .line 377200
    const/4 v4, 0x0

    .line 377201
    iget-object v2, p0, LX/2AJ;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 377202
    invoke-virtual {v2}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v4, v4}, LX/16z;->a(Ljava/lang/String;II)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 377203
    new-instance v4, LX/3dL;

    invoke-direct {v4}, LX/3dL;-><init>()V

    .line 377204
    iget-object v5, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v5

    .line 377205
    iput-object v5, v4, LX/3dL;->E:Ljava/lang/String;

    .line 377206
    move-object v4, v4

    .line 377207
    invoke-virtual {v2}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v2

    .line 377208
    iput-object v2, v4, LX/3dL;->ag:Ljava/lang/String;

    .line 377209
    move-object v2, v4

    .line 377210
    iput-object v3, v2, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 377211
    move-object v2, v2

    .line 377212
    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v4, 0x285feb

    invoke-direct {v3, v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 377213
    iput-object v3, v2, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 377214
    move-object v2, v2

    .line 377215
    invoke-virtual {v2}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    move-object v2, v2

    .line 377216
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 377217
    iput-object v2, v1, LX/23u;->d:LX/0Px;

    .line 377218
    move-object v1, v1

    .line 377219
    new-instance v2, LX/4YL;

    invoke-direct {v2}, LX/4YL;-><init>()V

    const-string v3, "only_me"

    .line 377220
    iput-object v3, v2, LX/4YL;->m:Ljava/lang/String;

    .line 377221
    move-object v2, v2

    .line 377222
    const-string v3, ""

    .line 377223
    iput-object v3, v2, LX/4YL;->h:Ljava/lang/String;

    .line 377224
    move-object v2, v2

    .line 377225
    invoke-virtual {v2}, LX/4YL;->a()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    .line 377226
    iput-object v2, v1, LX/23u;->al:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 377227
    move-object v1, v1

    .line 377228
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 377229
    iput-object v2, v1, LX/23u;->as:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 377230
    move-object v1, v1

    .line 377231
    new-instance v2, LX/4ZV;

    invoke-direct {v2}, LX/4ZV;-><init>()V

    .line 377232
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 377233
    iput-object v3, v2, LX/4ZV;->b:LX/0Px;

    .line 377234
    move-object v2, v2

    .line 377235
    invoke-virtual {v2}, LX/4ZV;->a()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    .line 377236
    iput-object v2, v1, LX/23u;->aT:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 377237
    move-object v1, v1

    .line 377238
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 377239
    iput-object v2, v1, LX/23u;->m:Ljava/lang/String;

    .line 377240
    move-object v1, v1

    .line 377241
    iput-object v0, v1, LX/23u;->N:Ljava/lang/String;

    .line 377242
    move-object v0, v1

    .line 377243
    iget-object v1, p1, LX/BAS;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-object v1, v1

    .line 377244
    iput-object v1, v0, LX/23u;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 377245
    move-object v0, v0

    .line 377246
    iget-object v1, p1, LX/BAS;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-object v1, v1

    .line 377247
    iput-object v1, v0, LX/23u;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 377248
    move-object v0, v0

    .line 377249
    iget-object v1, p1, LX/BAS;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-object v1, v1

    .line 377250
    iput-object v1, v0, LX/23u;->ae:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 377251
    move-object v0, v0

    .line 377252
    iget-object v1, p1, LX/BAS;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-object v1, v1

    .line 377253
    iput-object v1, v0, LX/23u;->aI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 377254
    move-object v0, v0

    .line 377255
    iget-object v1, p1, LX/BAS;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-object v1, v1

    .line 377256
    iput-object v1, v0, LX/23u;->aJ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 377257
    move-object v0, v0

    .line 377258
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0

    .line 377259
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 377260
    iget-object v1, p1, LX/BAS;->a:LX/BAT;

    move-object v1, v1

    .line 377261
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
