.class public final LX/2ZO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/2ZN;

.field private final b:Z


# direct methods
.method public constructor <init>(LX/2ZN;Z)V
    .locals 0

    .prologue
    .line 422485
    iput-object p1, p0, LX/2ZO;->a:LX/2ZN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422486
    iput-boolean p2, p0, LX/2ZO;->b:Z

    .line 422487
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 422469
    iget-boolean v0, p0, LX/2ZO;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2ZO;->a:LX/2ZN;

    iget-boolean v0, v0, LX/2ZN;->f:Z

    if-eqz v0, :cond_1

    .line 422470
    iget-object v0, p0, LX/2ZO;->a:LX/2ZN;

    iget-object v0, v0, LX/2ZN;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/117;

    invoke-virtual {v0}, LX/117;->b()Ljava/util/Collection;

    move-result-object v0

    .line 422471
    :goto_0
    new-instance v1, Lcom/facebook/interstitial/api/FetchInterstitialsParams;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/interstitial/api/FetchInterstitialsParams;-><init>(LX/0Px;)V

    .line 422472
    iget-object v0, p0, LX/2ZO;->a:LX/2ZN;

    iget-object v0, v0, LX/2ZN;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetch_interstititals"

    .line 422473
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 422474
    move-object v0, v0

    .line 422475
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    .line 422476
    iget-object v0, p0, LX/2ZO;->a:LX/2ZN;

    iget-object v0, v0, LX/2ZN;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11A;

    .line 422477
    iget-object v2, v0, LX/11A;->a:LX/0Zb;

    const-string v3, "interstitials_configuration_fetch_start"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 422478
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 422479
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 422480
    :cond_0
    iget-object v0, p0, LX/2ZO;->a:LX/2ZN;

    iget-object v0, v0, LX/2ZN;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11A;

    .line 422481
    iget-object v2, v0, LX/11A;->b:LX/0if;

    sget-object v3, LX/0ig;->e:LX/0ih;

    const-string v4, "nux_eligibility_start"

    invoke-virtual {v2, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 422482
    iget-object v2, v0, LX/11A;->b:LX/0if;

    sget-object v3, LX/0ig;->f:LX/0ih;

    const-string v4, "nux_eligibility_start"

    invoke-virtual {v2, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 422483
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 422484
    :cond_1
    iget-object v0, p0, LX/2ZO;->a:LX/2ZN;

    iget-object v0, v0, LX/2ZN;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/117;

    invoke-virtual {v0}, LX/117;->a()Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 422462
    const-string v0, "fetch_interstititals"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 422463
    if-eqz v0, :cond_0

    .line 422464
    iget-object v1, p0, LX/2ZO;->a:LX/2ZN;

    iget-object v1, v1, LX/2ZN;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iA;

    invoke-virtual {v1, v0}, LX/0iA;->a(Ljava/util/List;)V

    .line 422465
    iget-object v0, p0, LX/2ZO;->a:LX/2ZN;

    iget-object v0, v0, LX/2ZN;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11A;

    .line 422466
    iget-object v1, v0, LX/11A;->b:LX/0if;

    sget-object p0, LX/0ig;->e:LX/0ih;

    const-string p1, "nux_eligibility_finish"

    invoke-virtual {v1, p0, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 422467
    iget-object v1, v0, LX/11A;->b:LX/0if;

    sget-object p0, LX/0ig;->f:LX/0ih;

    const-string p1, "nux_eligibility_finish"

    invoke-virtual {v1, p0, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 422468
    :cond_0
    return-void
.end method
