.class public LX/349;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/33w;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/ComponentCallbacks2;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 494506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494507
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/349;->a:Ljava/util/Set;

    .line 494508
    new-instance v0, LX/9ml;

    invoke-direct {v0, p0}, LX/9ml;-><init>(LX/349;)V

    iput-object v0, p0, LX/349;->b:Landroid/content/ComponentCallbacks2;

    .line 494509
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/349;->b:Landroid/content/ComponentCallbacks2;

    invoke-virtual {v0, v1}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 494510
    return-void
.end method

.method private a(LX/5pR;)V
    .locals 4

    .prologue
    .line 494490
    iget-object v0, p0, LX/349;->a:Ljava/util/Set;

    iget-object v1, p0, LX/349;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [LX/33w;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/33w;

    .line 494491
    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 494492
    invoke-interface {v3, p1}, LX/33w;->a(LX/5pR;)V

    .line 494493
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 494494
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/349;I)V
    .locals 1

    .prologue
    .line 494499
    const/16 v0, 0x50

    if-lt p1, v0, :cond_1

    .line 494500
    sget-object v0, LX/5pR;->CRITICAL:LX/5pR;

    invoke-direct {p0, v0}, LX/349;->a(LX/5pR;)V

    .line 494501
    :cond_0
    :goto_0
    return-void

    .line 494502
    :cond_1
    const/16 v0, 0x28

    if-ge p1, v0, :cond_2

    const/16 v0, 0xf

    if-ne p1, v0, :cond_3

    .line 494503
    :cond_2
    sget-object v0, LX/5pR;->MODERATE:LX/5pR;

    invoke-direct {p0, v0}, LX/349;->a(LX/5pR;)V

    goto :goto_0

    .line 494504
    :cond_3
    const/16 v0, 0x14

    if-ne p1, v0, :cond_0

    .line 494505
    sget-object v0, LX/5pR;->UI_HIDDEN:LX/5pR;

    invoke-direct {p0, v0}, LX/349;->a(LX/5pR;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/33w;)V
    .locals 1

    .prologue
    .line 494511
    iget-object v0, p0, LX/349;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 494512
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 494497
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/349;->b:Landroid/content/ComponentCallbacks2;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 494498
    return-void
.end method

.method public final b(LX/33w;)V
    .locals 1

    .prologue
    .line 494495
    iget-object v0, p0, LX/349;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 494496
    return-void
.end method
