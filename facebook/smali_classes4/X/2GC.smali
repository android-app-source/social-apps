.class public LX/2GC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/2GC;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0So;

.field private final c:Landroid/content/Context;

.field private final d:LX/0W3;

.field private final e:LX/2GD;

.field private final f:Z


# direct methods
.method public constructor <init>(LX/0SG;LX/0So;Landroid/content/Context;LX/0W3;LX/2GD;)V
    .locals 1
    .param p2    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387935
    iput-object p1, p0, LX/2GC;->a:LX/0SG;

    .line 387936
    iput-object p2, p0, LX/2GC;->b:LX/0So;

    .line 387937
    iput-object p3, p0, LX/2GC;->c:Landroid/content/Context;

    .line 387938
    iput-object p4, p0, LX/2GC;->d:LX/0W3;

    .line 387939
    iput-object p5, p0, LX/2GC;->e:LX/2GD;

    .line 387940
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 387941
    invoke-virtual {v0, p3}, LX/1od;->a(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/2GC;->f:Z

    .line 387942
    return-void

    .line 387943
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2GC;
    .locals 9

    .prologue
    .line 387944
    sget-object v0, LX/2GC;->g:LX/2GC;

    if-nez v0, :cond_1

    .line 387945
    const-class v1, LX/2GC;

    monitor-enter v1

    .line 387946
    :try_start_0
    sget-object v0, LX/2GC;->g:LX/2GC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387947
    if-eqz v2, :cond_0

    .line 387948
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 387949
    new-instance v3, LX/2GC;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v7

    check-cast v7, LX/0W3;

    invoke-static {v0}, LX/2GD;->a(LX/0QB;)LX/2GD;

    move-result-object v8

    check-cast v8, LX/2GD;

    invoke-direct/range {v3 .. v8}, LX/2GC;-><init>(LX/0SG;LX/0So;Landroid/content/Context;LX/0W3;LX/2GD;)V

    .line 387950
    move-object v0, v3

    .line 387951
    sput-object v0, LX/2GC;->g:LX/2GC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387952
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387953
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387954
    :cond_1
    sget-object v0, LX/2GC;->g:LX/2GC;

    return-object v0

    .line 387955
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387956
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/wifiscan/WifiScanResult;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 387957
    iget-boolean v1, p0, LX/2GC;->f:Z

    if-nez v1, :cond_1

    .line 387958
    :cond_0
    :goto_0
    return-object v0

    .line 387959
    :cond_1
    iget-object v1, p0, LX/2GC;->d:LX/0W3;

    sget-wide v2, LX/0X5;->ar:J

    invoke-interface {v1, v2, v3}, LX/0W4;->a(J)Z

    move-result v1

    .line 387960
    if-eqz v1, :cond_0

    iget-object v0, p0, LX/2GC;->c:Landroid/content/Context;

    iget-object v1, p0, LX/2GC;->a:LX/0SG;

    invoke-static {v0, v1}, LX/2TU;->a(Landroid/content/Context;LX/0SG;)Lcom/facebook/wifiscan/WifiScanResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/wifiscan/WifiScanResult;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 387961
    iget-boolean v1, p0, LX/2GC;->f:Z

    if-nez v1, :cond_1

    .line 387962
    :cond_0
    :goto_0
    return-object v0

    .line 387963
    :cond_1
    iget-object v1, p0, LX/2GC;->d:LX/0W3;

    sget-wide v2, LX/0X5;->aq:J

    invoke-interface {v1, v2, v3}, LX/0W4;->a(J)Z

    move-result v1

    .line 387964
    if-eqz v1, :cond_0

    .line 387965
    iget-object v0, p0, LX/2GC;->d:LX/0W3;

    sget-wide v2, LX/0X5;->ap:J

    invoke-interface {v0, v2, v3}, LX/0W4;->c(J)J

    move-result-wide v0

    .line 387966
    iget-object v2, p0, LX/2GC;->e:LX/2GD;

    invoke-virtual {v2, v0, v1}, LX/2GD;->a(J)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, LX/2GC;->a:LX/0SG;

    iget-object v2, p0, LX/2GC;->b:LX/0So;

    invoke-static {v0, v1, v2}, Lcom/facebook/wifiscan/WifiScanResult;->a(Ljava/util/List;LX/0SG;LX/0So;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
