.class public final LX/37z;
.super LX/37u;
.source ""


# static fields
.field private static final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/media/AudioManager;

.field private final c:LX/67S;

.field public d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 502752
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 502753
    const-string v1, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 502754
    const-string v1, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 502755
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 502756
    sput-object v1, LX/37z;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502757
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 502761
    invoke-direct {p0, p1}, LX/37u;-><init>(Landroid/content/Context;)V

    .line 502762
    const/4 v0, -0x1

    iput v0, p0, LX/37z;->d:I

    .line 502763
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, LX/37z;->b:Landroid/media/AudioManager;

    .line 502764
    new-instance v0, LX/67S;

    invoke-direct {v0, p0}, LX/67S;-><init>(LX/37z;)V

    iput-object v0, p0, LX/37z;->c:LX/67S;

    .line 502765
    iget-object v0, p0, LX/37z;->c:LX/67S;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.media.VOLUME_CHANGED_ACTION"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 502766
    invoke-static {p0}, LX/37z;->f(LX/37z;)V

    .line 502767
    return-void
.end method

.method public static f(LX/37z;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 502768
    iget-object v0, p0, LX/37v;->a:Landroid/content/Context;

    move-object v0, v0

    .line 502769
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 502770
    iget-object v1, p0, LX/37z;->b:Landroid/media/AudioManager;

    invoke-virtual {v1, v5}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    .line 502771
    iget-object v2, p0, LX/37z;->b:Landroid/media/AudioManager;

    invoke-virtual {v2, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    iput v2, p0, LX/37z;->d:I

    .line 502772
    new-instance v2, LX/38H;

    const-string v3, "DEFAULT_ROUTE"

    const v4, 0x7f081a18

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, LX/38H;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, LX/37z;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, LX/38H;->a(Ljava/util/Collection;)LX/38H;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/38H;->b(I)LX/38H;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/38H;->a(I)LX/38H;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/38H;->e(I)LX/38H;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/38H;->d(I)LX/38H;

    move-result-object v0

    iget v1, p0, LX/37z;->d:I

    invoke-virtual {v0, v1}, LX/38H;->c(I)LX/38H;

    move-result-object v0

    invoke-virtual {v0}, LX/38H;->a()LX/383;

    move-result-object v0

    .line 502773
    new-instance v1, LX/38N;

    invoke-direct {v1}, LX/38N;-><init>()V

    invoke-virtual {v1, v0}, LX/38N;->a(LX/383;)LX/38N;

    move-result-object v0

    invoke-virtual {v0}, LX/38N;->a()LX/382;

    move-result-object v0

    .line 502774
    invoke-virtual {p0, v0}, LX/37v;->a(LX/382;)V

    .line 502775
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/389;
    .locals 1

    .prologue
    .line 502758
    const-string v0, "DEFAULT_ROUTE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 502759
    new-instance v0, LX/67R;

    invoke-direct {v0, p0}, LX/67R;-><init>(LX/37z;)V

    .line 502760
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
