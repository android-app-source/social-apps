.class public LX/2Ro;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/platform/server/handler/ParcelableString;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 410151
    const-class v0, LX/2Ro;

    sput-object v0, LX/2Ro;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410153
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 410154
    check-cast p1, Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;

    .line 410155
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 410156
    iget-object v0, p1, Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;->a:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 410157
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 410158
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "ids"

    const-string v3, ","

    iget-object v4, p1, Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;->a:Ljava/util/ArrayList;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410159
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "fields"

    const-string v3, "canonical_id"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410160
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "getCanonicalProfileId"

    .line 410161
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 410162
    move-object v1, v1

    .line 410163
    const-string v2, "GET"

    .line 410164
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 410165
    move-object v1, v1

    .line 410166
    const-string v2, ""

    .line 410167
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 410168
    move-object v1, v1

    .line 410169
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 410170
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 410171
    move-object v1, v1

    .line 410172
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 410173
    move-object v0, v1

    .line 410174
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 410175
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 410176
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 410177
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v3

    .line 410178
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 410179
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 410180
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    const-string v4, "canonical_id"

    invoke-virtual {v1, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 410181
    if-eqz v1, :cond_0

    .line 410182
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    new-instance v4, Lcom/facebook/platform/server/handler/ParcelableString;

    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/facebook/platform/server/handler/ParcelableString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 410183
    :cond_1
    return-object v2
.end method
