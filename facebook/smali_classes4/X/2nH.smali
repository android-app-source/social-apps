.class public LX/2nH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1nG;


# direct methods
.method public constructor <init>(LX/1nG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463680
    iput-object p1, p0, LX/2nH;->a:LX/1nG;

    .line 463681
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 8
    .param p0    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 463669
    if-nez p0, :cond_0

    move v0, v1

    .line 463670
    :goto_0
    return v0

    .line 463671
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 463672
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 463673
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    const/4 v2, 0x0

    .line 463674
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v3

    if-nez v3, :cond_3

    .line 463675
    :cond_1
    :goto_1
    move v0, v2

    .line 463676
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 463677
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v3

    int-to-double v4, v3

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v4, v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v3

    int-to-double v6, v3

    div-double/2addr v4, v6

    .line 463678
    const-wide v6, 0x3fee666666666666L    # 0.95

    cmpl-double v3, v4, v6

    if-lez v3, :cond_1

    const/4 v2, 0x1

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/2nH;
    .locals 2

    .prologue
    .line 463660
    new-instance v1, LX/2nH;

    invoke-static {p0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v0

    check-cast v0, LX/1nG;

    invoke-direct {v1, v0}, LX/2nH;-><init>(LX/1nG;)V

    .line 463661
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 463662
    if-nez p1, :cond_1

    .line 463663
    :cond_0
    :goto_0
    return-object v0

    .line 463664
    :cond_1
    const v1, -0x1e53800c

    invoke-static {p1, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 463665
    if-eqz v1, :cond_0

    .line 463666
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    .line 463667
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 463668
    iget-object v0, p0, LX/2nH;->a:LX/1nG;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {v1}, LX/2yc;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1nG;->a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
