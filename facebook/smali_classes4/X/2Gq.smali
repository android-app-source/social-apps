.class public LX/2Gq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "LX/2Ge;",
            "LX/2H0;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/2Gq;


# instance fields
.field private final b:LX/2zi;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 389490
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    sput-object v0, LX/2Gq;->a:LX/01J;

    return-void
.end method

.method public constructor <init>(LX/2zi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389488
    iput-object p1, p0, LX/2Gq;->b:LX/2zi;

    .line 389489
    return-void
.end method

.method public static a(LX/0QB;)LX/2Gq;
    .locals 4

    .prologue
    .line 389467
    sget-object v0, LX/2Gq;->c:LX/2Gq;

    if-nez v0, :cond_1

    .line 389468
    const-class v1, LX/2Gq;

    monitor-enter v1

    .line 389469
    :try_start_0
    sget-object v0, LX/2Gq;->c:LX/2Gq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 389470
    if-eqz v2, :cond_0

    .line 389471
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 389472
    new-instance p0, LX/2Gq;

    const-class v3, LX/2zi;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/2zi;

    invoke-direct {p0, v3}, LX/2Gq;-><init>(LX/2zi;)V

    .line 389473
    move-object v0, p0

    .line 389474
    sput-object v0, LX/2Gq;->c:LX/2Gq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389475
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 389476
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 389477
    :cond_1
    sget-object v0, LX/2Gq;->c:LX/2Gq;

    return-object v0

    .line 389478
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 389479
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/2Ge;)LX/2H0;
    .locals 10

    .prologue
    .line 389480
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/2Gq;->a:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 389481
    sget-object v0, LX/2Gq;->a:LX/01J;

    iget-object v1, p0, LX/2Gq;->b:LX/2zi;

    .line 389482
    new-instance v2, LX/2H0;

    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v3, 0x15e7

    invoke-static {v1, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v1}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v6

    check-cast v6, LX/0dC;

    invoke-static {v1}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v7

    check-cast v7, LX/0WV;

    invoke-static {v1}, LX/2Gr;->a(LX/0QB;)LX/2Gr;

    move-result-object v8

    check-cast v8, LX/2Gr;

    invoke-static {v1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    move-object v3, p1

    invoke-direct/range {v2 .. v9}, LX/2H0;-><init>(LX/2Ge;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0dC;LX/0WV;LX/2Gr;LX/0SG;)V

    .line 389483
    move-object v1, v2

    .line 389484
    invoke-virtual {v0, p1, v1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389485
    :cond_0
    sget-object v0, LX/2Gq;->a:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2H0;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 389486
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
