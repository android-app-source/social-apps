.class public LX/3N3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile e:LX/3N3;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 558109
    const v0, 0x133782a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x1337827

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x13354a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/3N3;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/platform/IsMessengerPlatform20141218Enabled;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/platform/IsMessengerPlatform20150311Enabled;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/platform/IsMessengerPlatform20150314Enabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 558111
    iput-object p1, p0, LX/3N3;->b:LX/0Or;

    .line 558112
    iput-object p2, p0, LX/3N3;->c:LX/0Or;

    .line 558113
    iput-object p3, p0, LX/3N3;->d:LX/0Or;

    .line 558114
    return-void
.end method

.method public static a(LX/0QB;)LX/3N3;
    .locals 6

    .prologue
    .line 558115
    sget-object v0, LX/3N3;->e:LX/3N3;

    if-nez v0, :cond_1

    .line 558116
    const-class v1, LX/3N3;

    monitor-enter v1

    .line 558117
    :try_start_0
    sget-object v0, LX/3N3;->e:LX/3N3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 558118
    if-eqz v2, :cond_0

    .line 558119
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 558120
    new-instance v3, LX/3N3;

    const/16 v4, 0x152b

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x152c

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x152d

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/3N3;-><init>(LX/0Or;LX/0Or;LX/0Or;)V

    .line 558121
    move-object v0, v3

    .line 558122
    sput-object v0, LX/3N3;->e:LX/3N3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 558123
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 558124
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 558125
    :cond_1
    sget-object v0, LX/3N3;->e:LX/3N3;

    return-object v0

    .line 558126
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 558127
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
