.class public LX/36F;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AnQ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/20p;

.field private final c:LX/03V;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/20n;

.field private final f:LX/0bH;

.field private final g:LX/217;

.field private final h:LX/1WR;


# direct methods
.method public constructor <init>(LX/03V;LX/0Ot;LX/20p;LX/20n;LX/217;LX/0Ot;LX/0bH;LX/1WR;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/AnQ;",
            ">;",
            "LX/20p;",
            "LX/20n;",
            "LX/217;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0bH;",
            "LX/1WR;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 498511
    iput-object p1, p0, LX/36F;->c:LX/03V;

    .line 498512
    iput-object p2, p0, LX/36F;->a:LX/0Ot;

    .line 498513
    iput-object p3, p0, LX/36F;->b:LX/20p;

    .line 498514
    iput-object p4, p0, LX/36F;->e:LX/20n;

    .line 498515
    iput-object p5, p0, LX/36F;->g:LX/217;

    .line 498516
    iput-object p7, p0, LX/36F;->f:LX/0bH;

    .line 498517
    iput-object p8, p0, LX/36F;->h:LX/1WR;

    .line 498518
    iput-object p6, p0, LX/36F;->d:LX/0Ot;

    .line 498519
    return-void
.end method

.method public static a(LX/0QB;)LX/36F;
    .locals 12

    .prologue
    .line 498520
    const-class v1, LX/36F;

    monitor-enter v1

    .line 498521
    :try_start_0
    sget-object v0, LX/36F;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 498522
    sput-object v2, LX/36F;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498523
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498524
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 498525
    new-instance v3, LX/36F;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    const/16 v5, 0x1d6c

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/20p;->a(LX/0QB;)LX/20p;

    move-result-object v6

    check-cast v6, LX/20p;

    invoke-static {v0}, LX/20n;->a(LX/0QB;)LX/20n;

    move-result-object v7

    check-cast v7, LX/20n;

    invoke-static {v0}, LX/217;->a(LX/0QB;)LX/217;

    move-result-object v8

    check-cast v8, LX/217;

    const/16 v9, 0x97

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v10

    check-cast v10, LX/0bH;

    invoke-static {v0}, LX/1WR;->a(LX/0QB;)LX/1WR;

    move-result-object v11

    check-cast v11, LX/1WR;

    invoke-direct/range {v3 .. v11}, LX/36F;-><init>(LX/03V;LX/0Ot;LX/20p;LX/20n;LX/217;LX/0Ot;LX/0bH;LX/1WR;)V

    .line 498526
    move-object v0, v3

    .line 498527
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 498528
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/36F;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498529
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 498530
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/20X;ZLX/1PT;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            "LX/20X;",
            "Z",
            "LX/1PT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 498531
    sget-object v0, LX/Any;->a:[I

    invoke-virtual {p3}, LX/20X;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 498532
    :goto_0
    return-void

    .line 498533
    :pswitch_0
    iget-object v1, p0, LX/36F;->g:LX/217;

    iget-object v0, p0, LX/36F;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AnQ;

    iget-object v2, p0, LX/36F;->h:LX/1WR;

    invoke-static {p1, v1, p5, v0, v2}, LX/212;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/217;LX/1PT;LX/AnQ;LX/1WR;)V

    .line 498534
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 498535
    :pswitch_1
    iget-object v3, p0, LX/36F;->e:LX/20n;

    iget-object v5, p0, LX/36F;->h:LX/1WR;

    move-object v0, p2

    move-object v1, p1

    move-object v2, p5

    move v4, p4

    invoke-static/range {v0 .. v5}, LX/212;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;LX/20n;ZLX/1WR;)V

    goto :goto_0

    .line 498536
    :pswitch_2
    iget-object v0, p0, LX/36F;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gh;

    iget-object v2, p0, LX/36F;->b:LX/20p;

    iget-object v5, p0, LX/36F;->f:LX/0bH;

    iget-object v6, p0, LX/36F;->h:LX/1WR;

    move-object v0, p2

    move-object v3, p1

    move-object v4, p5

    invoke-static/range {v0 .. v6}, LX/212;->a(Landroid/view/View;LX/0gh;LX/20p;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;LX/0bH;LX/1WR;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
