.class public final LX/39x;
.super LX/0ur;
.source ""


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAttachmentProperty;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Z

.field public j:Lcom/facebook/graphql/model/GraphQLMedia;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 524158
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 524159
    const/4 v0, 0x0

    iput-object v0, p0, LX/39x;->x:LX/0x2;

    .line 524160
    instance-of v0, p0, LX/39x;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 524161
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;
    .locals 2

    .prologue
    .line 524131
    new-instance v1, LX/39x;

    invoke-direct {v1}, LX/39x;-><init>()V

    .line 524132
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 524133
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/39x;->b:LX/0Px;

    .line 524134
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->k()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v0

    iput-object v0, v1, LX/39x;->c:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 524135
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->l()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/39x;->d:LX/0Px;

    .line 524136
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/39x;->e:Ljava/lang/String;

    .line 524137
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/39x;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 524138
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->o()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    iput-object v0, v1, LX/39x;->g:Lcom/facebook/graphql/model/GraphQLNode;

    .line 524139
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->p()Z

    move-result v0

    iput-boolean v0, v1, LX/39x;->h:Z

    .line 524140
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q()Z

    move-result v0

    iput-boolean v0, v1, LX/39x;->i:Z

    .line 524141
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    iput-object v0, v1, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 524142
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/39x;->k:Ljava/lang/String;

    .line 524143
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/39x;->l:Ljava/lang/String;

    .line 524144
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->E()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/39x;->m:Ljava/lang/String;

    .line 524145
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/39x;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 524146
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/39x;->o:LX/0Px;

    .line 524147
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/39x;->p:LX/0Px;

    .line 524148
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/39x;->q:LX/0Px;

    .line 524149
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/39x;->r:Ljava/lang/String;

    .line 524150
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    iput-object v0, v1, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 524151
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/39x;->t:Ljava/lang/String;

    .line 524152
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/39x;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 524153
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/39x;->v:Ljava/lang/String;

    .line 524154
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/39x;->w:Ljava/lang/String;

    .line 524155
    invoke-static {v1, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 524156
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->L_()LX/0x2;

    move-result-object v0

    invoke-virtual {v0}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    iput-object v0, v1, LX/39x;->x:LX/0x2;

    .line 524157
    return-object v1
.end method


# virtual methods
.method public final a(LX/0Px;)LX/39x;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;)",
            "LX/39x;"
        }
    .end annotation

    .prologue
    .line 524119
    iput-object p1, p0, LX/39x;->b:LX/0Px;

    .line 524120
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/39x;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLMedia;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 524127
    iput-object p1, p0, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 524128
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/39x;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 524129
    iput-object p1, p0, LX/39x;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 524130
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 2

    .prologue
    .line 524125
    new-instance v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;-><init>(LX/39x;)V

    .line 524126
    return-object v0
.end method

.method public final e(Ljava/lang/String;)LX/39x;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 524123
    iput-object p1, p0, LX/39x;->t:Ljava/lang/String;

    .line 524124
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/39x;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 524121
    iput-object p1, p0, LX/39x;->w:Ljava/lang/String;

    .line 524122
    return-object p0
.end method
