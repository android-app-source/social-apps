.class public LX/2bB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1fT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/2bB;


# instance fields
.field private final b:LX/0lC;

.field public final c:LX/1tN;

.field public final d:LX/2bC;

.field public final e:Landroid/content/Context;

.field public final f:LX/0SG;

.field public final g:LX/2bD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 428511
    const-class v0, LX/2bB;

    sput-object v0, LX/2bB;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0lC;LX/1tN;LX/2bC;Landroid/content/Context;LX/0SG;LX/2bD;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 428438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 428439
    iput-object p1, p0, LX/2bB;->b:LX/0lC;

    .line 428440
    iput-object p2, p0, LX/2bB;->c:LX/1tN;

    .line 428441
    iput-object p3, p0, LX/2bB;->d:LX/2bC;

    .line 428442
    iput-object p4, p0, LX/2bB;->e:Landroid/content/Context;

    .line 428443
    iput-object p5, p0, LX/2bB;->f:LX/0SG;

    .line 428444
    iput-object p6, p0, LX/2bB;->g:LX/2bD;

    .line 428445
    return-void
.end method

.method public static a(LX/0QB;)LX/2bB;
    .locals 10

    .prologue
    .line 428446
    sget-object v0, LX/2bB;->h:LX/2bB;

    if-nez v0, :cond_1

    .line 428447
    const-class v1, LX/2bB;

    monitor-enter v1

    .line 428448
    :try_start_0
    sget-object v0, LX/2bB;->h:LX/2bB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 428449
    if-eqz v2, :cond_0

    .line 428450
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 428451
    new-instance v3, LX/2bB;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/1tN;->a(LX/0QB;)LX/1tN;

    move-result-object v5

    check-cast v5, LX/1tN;

    invoke-static {v0}, LX/2bC;->a(LX/0QB;)LX/2bC;

    move-result-object v6

    check-cast v6, LX/2bC;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, LX/2bD;->a(LX/0QB;)LX/2bD;

    move-result-object v9

    check-cast v9, LX/2bD;

    invoke-direct/range {v3 .. v9}, LX/2bB;-><init>(LX/0lC;LX/1tN;LX/2bC;Landroid/content/Context;LX/0SG;LX/2bD;)V

    .line 428452
    move-object v0, v3

    .line 428453
    sput-object v0, LX/2bB;->h:LX/2bB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 428454
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 428455
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 428456
    :cond_1
    sget-object v0, LX/2bB;->h:LX/2bB;

    return-object v0

    .line 428457
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 428458
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/2bB;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 9

    .prologue
    .line 428459
    if-nez p3, :cond_0

    const/4 v0, 0x0

    .line 428460
    :goto_0
    iget-object v1, p0, LX/2bB;->d:LX/2bC;

    const/4 v4, 0x0

    .line 428461
    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "source"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "MQTT_PUSH"

    aput-object v5, v2, v3

    const/4 v3, 0x2

    const-string v5, "push_id"

    aput-object v5, v2, v3

    const/4 v3, 0x3

    aput-object p1, v2, v3

    const/4 v3, 0x4

    const-string v5, "type"

    aput-object v5, v2, v3

    const/4 v3, 0x5

    aput-object p2, v2, v3

    const/4 v3, 0x6

    const-string v5, "notif_time"

    aput-object v5, v2, v3

    const/4 v3, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-static {v2}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    .line 428462
    const-string v3, "mqtt_fbns_notification_arrived_at_mqtt_service"

    move-object v2, v1

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    invoke-virtual/range {v2 .. v8}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 428463
    return-void

    .line 428464
    :cond_0
    invoke-virtual {p3}, Ljava/lang/Long;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method private a([B)V
    .locals 13

    .prologue
    .line 428465
    new-instance v0, LX/1sp;

    invoke-direct {v0}, LX/1sp;-><init>()V

    .line 428466
    new-instance v1, LX/1sr;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    const/4 v3, 0x0

    array-length v4, p1

    invoke-direct {v2, p1, v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    invoke-direct {v1, v2}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v0, v1}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    .line 428467
    :try_start_0
    const/4 v1, 0x0

    .line 428468
    invoke-virtual {v0}, LX/1su;->r()LX/1sv;

    .line 428469
    :goto_0
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 428470
    iget-byte v3, v2, LX/1sw;->b:B

    if-eqz v3, :cond_1

    .line 428471
    iget-short v3, v2, LX/1sw;->c:S

    packed-switch v3, :pswitch_data_0

    .line 428472
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 428473
    :pswitch_0
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0xc

    if-ne v3, v4, :cond_0

    .line 428474
    invoke-static {v0}, LX/6mK;->b(LX/1su;)LX/6mK;

    move-result-object v1

    goto :goto_0

    .line 428475
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 428476
    :cond_1
    invoke-virtual {v0}, LX/1su;->e()V

    .line 428477
    new-instance v2, LX/6ma;

    invoke-direct {v2, v1}, LX/6ma;-><init>(LX/6mK;)V

    .line 428478
    move-object v0, v2
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    .line 428479
    iget-object v2, v0, LX/6ma;->fbpushdata:LX/6mK;

    .line 428480
    iget-object v0, p0, LX/2bB;->b:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->e()LX/0m9;

    move-result-object v0

    const-string v1, "type"

    iget-object v3, v2, LX/6mK;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    const-string v1, "time"

    iget-object v3, v2, LX/6mK;->time:Ljava/lang/Long;

    invoke-virtual {v0, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Long;)LX/0m9;

    move-result-object v0

    const-string v1, "message"

    iget-object v3, v2, LX/6mK;->message:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    const-string v1, "unread_count"

    iget-object v3, v2, LX/6mK;->unread_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Integer;)LX/0m9;

    move-result-object v0

    const-string v1, "target_uid"

    iget-object v3, v2, LX/6mK;->target_uid:Ljava/lang/Long;

    invoke-virtual {v0, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Long;)LX/0m9;

    move-result-object v0

    const-string v1, "is_logged_out_push"

    iget-object v3, v2, LX/6mK;->is_logged_out_push:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0m9;

    move-result-object v3

    .line 428481
    iget-object v0, v2, LX/6mK;->href:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 428482
    const-string v0, "href"

    iget-object v1, v2, LX/6mK;->href:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 428483
    :cond_2
    iget-object v0, p0, LX/2bB;->b:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->e()LX/0m9;

    move-result-object v4

    .line 428484
    iget-object v0, v2, LX/6mK;->params:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 428485
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_1

    .line 428486
    :catch_0
    move-exception v0

    .line 428487
    const/4 v1, 0x0

    const/4 v10, 0x0

    .line 428488
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v9

    .line 428489
    const-string v6, "exception"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v9, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428490
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 428491
    const-string v6, "fbpushnotif"

    invoke-interface {v9, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428492
    :cond_3
    iget-object v6, p0, LX/2bB;->d:LX/2bC;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "messaging_push_notif_"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v8, LX/3B4;->MQTT_PUSH:LX/3B4;

    invoke-virtual {v8}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "MqttParseException"

    move-object v11, v10

    move-object v12, v10

    invoke-virtual/range {v6 .. v12}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 428493
    :goto_2
    return-void

    .line 428494
    :cond_4
    const-string v0, "params"

    invoke-virtual {v3, v0, v4}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 428495
    if-eqz v4, :cond_5

    .line 428496
    const-string v6, "PushNotifID"

    invoke-virtual {v4, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 428497
    if-eqz v6, :cond_5

    .line 428498
    invoke-virtual {v6}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v6

    .line 428499
    :goto_3
    move-object v0, v6

    .line 428500
    iget-object v1, v2, LX/6mK;->type:Ljava/lang/String;

    iget-object v2, v2, LX/6mK;->time:Ljava/lang/Long;

    invoke-static {p0, v0, v1, v2}, LX/2bB;->a(LX/2bB;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 428501
    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    .line 428502
    iget-object v6, p0, LX/2bB;->g:LX/2bD;

    iget-object v7, p0, LX/2bB;->e:Landroid/content/Context;

    sget-object v9, LX/3B4;->MQTT_PUSH:LX/3B4;

    iget-object v8, p0, LX/2bB;->e:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    move-object v8, v1

    move-object v11, v0

    invoke-virtual/range {v6 .. v11}, LX/2bD;->b(Landroid/content/Context;Ljava/lang/String;LX/3B4;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 428503
    iget-object v7, p0, LX/2bB;->c:LX/1tN;

    .line 428504
    invoke-virtual {v7}, LX/05I;->e()LX/0BE;

    move-result-object v8

    invoke-interface {v8, v0, v6}, LX/0BE;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 428505
    invoke-static {v7, v6}, LX/1tN;->b(LX/1tN;Landroid/content/Intent;)V

    .line 428506
    const-string v8, "FbnsNotificationDeliveryHelper"

    const-string v9, "deliverFbnsNotification %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    invoke-static {v8, v9, v10}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 428507
    goto :goto_2

    :cond_5
    iget-object v6, p0, LX/2bB;->f:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final onMessage(Ljava/lang/String;[BJ)V
    .locals 1

    .prologue
    .line 428508
    const-string v0, "/t_push"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 428509
    invoke-direct {p0, p2}, LX/2bB;->a([B)V

    .line 428510
    :cond_0
    return-void
.end method
