.class public final LX/2yq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/net/Uri;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 482744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482745
    iput-object p1, p0, LX/2yq;->a:Landroid/net/Uri;

    .line 482746
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2yq;->b:Ljava/lang/String;

    .line 482747
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2yq;->c:Ljava/lang/String;

    .line 482748
    return-void
.end method

.method public static a(Landroid/net/Uri;)LX/2yq;
    .locals 3
    .param p0    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 482749
    const/4 v0, 0x0

    .line 482750
    if-nez p0, :cond_2

    .line 482751
    :cond_0
    :goto_0
    move v0, v0

    .line 482752
    if-nez v0, :cond_1

    .line 482753
    const/4 v0, 0x0

    .line 482754
    :goto_1
    return-object v0

    :cond_1
    new-instance v0, LX/2yq;

    invoke-direct {v0, p0}, LX/2yq;-><init>(Landroid/net/Uri;)V

    goto :goto_1

    .line 482755
    :cond_2
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 482756
    if-eqz v1, :cond_0

    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
