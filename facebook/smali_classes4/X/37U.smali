.class public final LX/37U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/1yT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 501135
    iput-object p1, p0, LX/37U;->a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501136
    iput-object p2, p0, LX/37U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 501137
    iput-boolean p3, p0, LX/37U;->c:Z

    .line 501138
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 501139
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, p3}, LX/1zJ;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/37U;->d:Ljava/lang/String;

    .line 501140
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 501109
    :try_start_0
    iget-object v0, p0, LX/37U;->a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nA;

    iget-object v1, p0, LX/37U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-boolean v2, p0, LX/37U;->c:Z

    invoke-virtual {v0, v1, v2}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 501110
    :goto_0
    iget-object v0, p0, LX/37U;->a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->h:LX/1EV;

    .line 501111
    iget-object v2, v0, LX/1EV;->a:LX/0Uh;

    const/16 v3, 0x309

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v0, v2

    .line 501112
    if-eqz v0, :cond_1

    .line 501113
    iget-object v0, p0, LX/37U;->a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    iget-object v2, v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->i:LX/34w;

    iget-object v0, p0, LX/37U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 501114
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 501115
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 501116
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 501117
    iget-object v4, v2, LX/34w;->e:LX/34x;

    invoke-virtual {v4}, LX/34x;->a()Ljava/util/regex/Pattern;

    move-result-object v4

    if-nez v4, :cond_2

    .line 501118
    :cond_0
    :goto_1
    move-object v0, v3

    .line 501119
    :goto_2
    new-instance v1, LX/1yT;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    return-object v1

    .line 501120
    :catch_0
    iget-object v0, p0, LX/37U;->a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->f:LX/03V;

    sget-object v1, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Corrupt data. Can\'t linkify description"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 501121
    iget-object v0, p0, LX/37U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 501122
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 501123
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 501124
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 501125
    :cond_2
    iget-object v4, v2, LX/34w;->e:LX/34x;

    invoke-virtual {v4}, LX/34x;->a()Ljava/util/regex/Pattern;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 501126
    :goto_3
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 501127
    :try_start_1
    iget-object v5, v2, LX/34w;->e:LX/34x;

    invoke-virtual {v5}, LX/34x;->b()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/util/regex/Matcher;->start(I)I

    move-result v5

    .line 501128
    iget-object v6, v2, LX/34w;->e:LX/34x;

    invoke-virtual {v6}, LX/34x;->b()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/util/regex/Matcher;->end(I)I
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    .line 501129
    invoke-interface {v1, v5, v6}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 501130
    invoke-static {v4}, Landroid/util/Patterns;->digitsAndPlusOnly(Ljava/util/regex/Matcher;)Ljava/lang/String;

    move-result-object v8

    .line 501131
    new-instance p0, LX/DDp;

    invoke-direct {p0, v2, v8, v0, v7}, LX/DDp;-><init>(LX/34w;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    const/4 v7, 0x0

    invoke-virtual {v3, p0, v5, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_3

    .line 501132
    :catch_1
    move-exception v4

    .line 501133
    iget-object v5, v2, LX/34w;->d:LX/03V;

    sget-object v6, LX/34w;->a:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 501134
    iget-object v0, p0, LX/37U;->d:Ljava/lang/String;

    return-object v0
.end method
