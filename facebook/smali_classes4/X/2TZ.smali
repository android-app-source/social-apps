.class public abstract LX/2TZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Ta;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/2Ta",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final a:Landroid/database/Cursor;

.field private b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 414452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414453
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iput-object v0, p0, LX/2TZ;->a:Landroid/database/Cursor;

    .line 414454
    const/4 v0, 0x0

    iput-object v0, p0, LX/2TZ;->b:Ljava/lang/Object;

    .line 414455
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2TZ;->c:Z

    .line 414456
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 414457
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2TZ;->c:Z

    .line 414458
    iget-object v0, p0, LX/2TZ;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2TZ;->a:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, LX/2TZ;->a(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/2TZ;->b:Ljava/lang/Object;

    .line 414459
    return-void

    .line 414460
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/database/Cursor;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")TE;"
        }
    .end annotation
.end method

.method public close()V
    .locals 1

    .prologue
    .line 414461
    iget-object v0, p0, LX/2TZ;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 414462
    return-void
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 414463
    iget-boolean v0, p0, LX/2TZ;->c:Z

    if-eqz v0, :cond_0

    .line 414464
    invoke-direct {p0}, LX/2TZ;->a()V

    .line 414465
    :cond_0
    iget-object v0, p0, LX/2TZ;->b:Ljava/lang/Object;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 414466
    iget-boolean v0, p0, LX/2TZ;->c:Z

    if-eqz v0, :cond_0

    .line 414467
    invoke-direct {p0}, LX/2TZ;->a()V

    .line 414468
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2TZ;->c:Z

    .line 414469
    iget-object v0, p0, LX/2TZ;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final remove()V
    .locals 3

    .prologue
    .line 414470
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not support remove()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
