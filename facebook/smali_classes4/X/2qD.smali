.class public final enum LX/2qD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2qD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2qD;

.field public static final enum STATE_ERROR:LX/2qD;

.field public static final enum STATE_IDLE:LX/2qD;

.field public static final enum STATE_PAUSED:LX/2qD;

.field public static final enum STATE_PLAYBACK_COMPLETED:LX/2qD;

.field public static final enum STATE_PLAYING:LX/2qD;

.field public static final enum STATE_PREPARED:LX/2qD;

.field public static final enum STATE_PREPARING:LX/2qD;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 470746
    new-instance v0, LX/2qD;

    const-string v1, "STATE_ERROR"

    const-string v2, "state_error"

    invoke-direct {v0, v1, v4, v2}, LX/2qD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qD;->STATE_ERROR:LX/2qD;

    .line 470747
    new-instance v0, LX/2qD;

    const-string v1, "STATE_IDLE"

    const-string v2, "state_idle"

    invoke-direct {v0, v1, v5, v2}, LX/2qD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    .line 470748
    new-instance v0, LX/2qD;

    const-string v1, "STATE_PREPARING"

    const-string v2, "state_preparing"

    invoke-direct {v0, v1, v6, v2}, LX/2qD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qD;->STATE_PREPARING:LX/2qD;

    .line 470749
    new-instance v0, LX/2qD;

    const-string v1, "STATE_PREPARED"

    const-string v2, "state_prepared"

    invoke-direct {v0, v1, v7, v2}, LX/2qD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qD;->STATE_PREPARED:LX/2qD;

    .line 470750
    new-instance v0, LX/2qD;

    const-string v1, "STATE_PLAYING"

    const-string v2, "state_playing"

    invoke-direct {v0, v1, v8, v2}, LX/2qD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qD;->STATE_PLAYING:LX/2qD;

    .line 470751
    new-instance v0, LX/2qD;

    const-string v1, "STATE_PAUSED"

    const/4 v2, 0x5

    const-string v3, "state_paused"

    invoke-direct {v0, v1, v2, v3}, LX/2qD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qD;->STATE_PAUSED:LX/2qD;

    .line 470752
    new-instance v0, LX/2qD;

    const-string v1, "STATE_PLAYBACK_COMPLETED"

    const/4 v2, 0x6

    const-string v3, "state_playback_completed"

    invoke-direct {v0, v1, v2, v3}, LX/2qD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qD;->STATE_PLAYBACK_COMPLETED:LX/2qD;

    .line 470753
    const/4 v0, 0x7

    new-array v0, v0, [LX/2qD;

    sget-object v1, LX/2qD;->STATE_ERROR:LX/2qD;

    aput-object v1, v0, v4

    sget-object v1, LX/2qD;->STATE_IDLE:LX/2qD;

    aput-object v1, v0, v5

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    aput-object v1, v0, v6

    sget-object v1, LX/2qD;->STATE_PREPARED:LX/2qD;

    aput-object v1, v0, v7

    sget-object v1, LX/2qD;->STATE_PLAYING:LX/2qD;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/2qD;->STATE_PAUSED:LX/2qD;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2qD;->STATE_PLAYBACK_COMPLETED:LX/2qD;

    aput-object v2, v0, v1

    sput-object v0, LX/2qD;->$VALUES:[LX/2qD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 470740
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 470741
    iput-object p3, p0, LX/2qD;->value:Ljava/lang/String;

    .line 470742
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2qD;
    .locals 1

    .prologue
    .line 470745
    const-class v0, LX/2qD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2qD;

    return-object v0
.end method

.method public static values()[LX/2qD;
    .locals 1

    .prologue
    .line 470744
    sget-object v0, LX/2qD;->$VALUES:[LX/2qD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2qD;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 470743
    iget-object v0, p0, LX/2qD;->value:Ljava/lang/String;

    return-object v0
.end method
