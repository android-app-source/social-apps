.class public final LX/2pA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field public final synthetic a:LX/2p8;


# direct methods
.method public constructor <init>(LX/2p8;)V
    .locals 0

    .prologue
    .line 467782
    iput-object p1, p0, LX/2pA;->a:LX/2p8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 467783
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 467784
    iget-object v0, p0, LX/2pA;->a:LX/2p8;

    invoke-virtual {v0, p1}, LX/2p8;->a(Landroid/graphics/SurfaceTexture;)V

    .line 467785
    return-void
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 4

    .prologue
    .line 467786
    if-nez p1, :cond_0

    .line 467787
    iget-object v0, p0, LX/2pA;->a:LX/2p8;

    const-string v1, "onSurfaceTextureDestroyed"

    const-string v2, "onSurfaceTextureDestroyed with null SurfaceTexture"

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 467788
    const/4 v0, 0x1

    .line 467789
    :goto_0
    return v0

    .line 467790
    :cond_0
    iget-object v0, p0, LX/2pA;->a:LX/2p8;

    invoke-virtual {v0, p1}, LX/2p8;->b(Landroid/graphics/SurfaceTexture;)V

    .line 467791
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 467792
    iget-object v0, p0, LX/2pA;->a:LX/2p8;

    iget-object v0, v0, LX/2p8;->j:LX/2qG;

    if-eqz v0, :cond_0

    .line 467793
    iget-object v0, p0, LX/2pA;->a:LX/2p8;

    iget-object v0, v0, LX/2p8;->j:LX/2qG;

    invoke-interface {v0, p2, p3}, LX/2qG;->a(II)V

    .line 467794
    :cond_0
    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    .prologue
    .line 467795
    iget-object v0, p0, LX/2pA;->a:LX/2p8;

    const/4 v1, 0x1

    .line 467796
    iput-boolean v1, v0, LX/2p8;->l:Z

    .line 467797
    iget-object v0, p0, LX/2pA;->a:LX/2p8;

    iget-object v0, v0, LX/2p8;->j:LX/2qG;

    if-eqz v0, :cond_0

    .line 467798
    iget-object v0, p0, LX/2pA;->a:LX/2p8;

    iget-object v0, v0, LX/2p8;->j:LX/2qG;

    invoke-interface {v0}, LX/2qG;->a()V

    .line 467799
    :cond_0
    return-void
.end method
