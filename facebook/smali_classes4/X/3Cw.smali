.class public final enum LX/3Cw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3Cw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3Cw;

.field public static final enum NOTIFICATION:LX/3Cw;

.field public static final enum POST_FEEDBACK:LX/3Cw;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 531002
    new-instance v0, LX/3Cw;

    const-string v1, "NOTIFICATION"

    invoke-direct {v0, v1, v2}, LX/3Cw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Cw;->NOTIFICATION:LX/3Cw;

    .line 531003
    new-instance v0, LX/3Cw;

    const-string v1, "POST_FEEDBACK"

    invoke-direct {v0, v1, v3}, LX/3Cw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Cw;->POST_FEEDBACK:LX/3Cw;

    .line 531004
    const/4 v0, 0x2

    new-array v0, v0, [LX/3Cw;

    sget-object v1, LX/3Cw;->NOTIFICATION:LX/3Cw;

    aput-object v1, v0, v2

    sget-object v1, LX/3Cw;->POST_FEEDBACK:LX/3Cw;

    aput-object v1, v0, v3

    sput-object v0, LX/3Cw;->$VALUES:[LX/3Cw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 531005
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3Cw;
    .locals 1

    .prologue
    .line 531006
    const-class v0, LX/3Cw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3Cw;

    return-object v0
.end method

.method public static values()[LX/3Cw;
    .locals 1

    .prologue
    .line 531007
    sget-object v0, LX/3Cw;->$VALUES:[LX/3Cw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3Cw;

    return-object v0
.end method
