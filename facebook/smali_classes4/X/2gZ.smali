.class public LX/2gZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2gZ;


# instance fields
.field private final a:LX/0aG;

.field private final b:LX/01T;


# direct methods
.method public constructor <init>(LX/01T;LX/0aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 448617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448618
    iput-object p1, p0, LX/2gZ;->b:LX/01T;

    .line 448619
    iput-object p2, p0, LX/2gZ;->a:LX/0aG;

    .line 448620
    return-void
.end method

.method public static a(LX/0QB;)LX/2gZ;
    .locals 5

    .prologue
    .line 448621
    sget-object v0, LX/2gZ;->c:LX/2gZ;

    if-nez v0, :cond_1

    .line 448622
    const-class v1, LX/2gZ;

    monitor-enter v1

    .line 448623
    :try_start_0
    sget-object v0, LX/2gZ;->c:LX/2gZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 448624
    if-eqz v2, :cond_0

    .line 448625
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 448626
    new-instance p0, LX/2gZ;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-direct {p0, v3, v4}, LX/2gZ;-><init>(LX/01T;LX/0aG;)V

    .line 448627
    move-object v0, p0

    .line 448628
    sput-object v0, LX/2gZ;->c:LX/2gZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 448629
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 448630
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 448631
    :cond_1
    sget-object v0, LX/2gZ;->c:LX/2gZ;

    return-object v0

    .line 448632
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 448633
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 448634
    iget-object v0, p0, LX/2gZ;->b:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-eq v0, v1, :cond_1

    .line 448635
    :cond_0
    :goto_0
    return-void

    .line 448636
    :cond_1
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 448637
    new-instance v0, Lcom/facebook/contacts/server/UpdateContactIsMessengerUserParams;

    invoke-direct {v0, p1, p2}, Lcom/facebook/contacts/server/UpdateContactIsMessengerUserParams;-><init>(Ljava/lang/String;Z)V

    .line 448638
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 448639
    const-string v2, "updateIsMessengerUserParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 448640
    iget-object v0, p0, LX/2gZ;->a:LX/0aG;

    const-string v2, "update_contact_is_messenger_user"

    const v3, 0x209acf43

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    .line 448641
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    .line 448642
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    goto :goto_0
.end method
