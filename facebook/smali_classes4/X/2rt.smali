.class public final enum LX/2rt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2rt;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2rt;

.field public static final enum GOODWILL_CAMPAIGN:LX/2rt;

.field public static final enum GREETING_CARD:LX/2rt;

.field public static final enum LIFE_EVENT:LX/2rt;

.field public static final enum RECOMMENDATION:LX/2rt;

.field public static final enum SELL:LX/2rt;

.field public static final enum SHARE:LX/2rt;

.field public static final enum STATUS:LX/2rt;


# instance fields
.field public final analyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 472142
    new-instance v0, LX/2rt;

    const-string v1, "STATUS"

    const-string v2, "status"

    invoke-direct {v0, v1, v4, v2}, LX/2rt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rt;->STATUS:LX/2rt;

    .line 472143
    new-instance v0, LX/2rt;

    const-string v1, "SHARE"

    const-string v2, "share"

    invoke-direct {v0, v1, v5, v2}, LX/2rt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rt;->SHARE:LX/2rt;

    .line 472144
    new-instance v0, LX/2rt;

    const-string v1, "RECOMMENDATION"

    const-string v2, "recommendation"

    invoke-direct {v0, v1, v6, v2}, LX/2rt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rt;->RECOMMENDATION:LX/2rt;

    .line 472145
    new-instance v0, LX/2rt;

    const-string v1, "SELL"

    const-string v2, "sell"

    invoke-direct {v0, v1, v7, v2}, LX/2rt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rt;->SELL:LX/2rt;

    .line 472146
    new-instance v0, LX/2rt;

    const-string v1, "LIFE_EVENT"

    const-string v2, "life_event"

    invoke-direct {v0, v1, v8, v2}, LX/2rt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rt;->LIFE_EVENT:LX/2rt;

    .line 472147
    new-instance v0, LX/2rt;

    const-string v1, "GREETING_CARD"

    const/4 v2, 0x5

    const-string v3, "greeting_card"

    invoke-direct {v0, v1, v2, v3}, LX/2rt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rt;->GREETING_CARD:LX/2rt;

    .line 472148
    new-instance v0, LX/2rt;

    const-string v1, "GOODWILL_CAMPAIGN"

    const/4 v2, 0x6

    const-string v3, "goodwill_campaign"

    invoke-direct {v0, v1, v2, v3}, LX/2rt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rt;->GOODWILL_CAMPAIGN:LX/2rt;

    .line 472149
    const/4 v0, 0x7

    new-array v0, v0, [LX/2rt;

    sget-object v1, LX/2rt;->STATUS:LX/2rt;

    aput-object v1, v0, v4

    sget-object v1, LX/2rt;->SHARE:LX/2rt;

    aput-object v1, v0, v5

    sget-object v1, LX/2rt;->RECOMMENDATION:LX/2rt;

    aput-object v1, v0, v6

    sget-object v1, LX/2rt;->SELL:LX/2rt;

    aput-object v1, v0, v7

    sget-object v1, LX/2rt;->LIFE_EVENT:LX/2rt;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/2rt;->GREETING_CARD:LX/2rt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2rt;->GOODWILL_CAMPAIGN:LX/2rt;

    aput-object v2, v0, v1

    sput-object v0, LX/2rt;->$VALUES:[LX/2rt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 472150
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 472151
    iput-object p3, p0, LX/2rt;->analyticsName:Ljava/lang/String;

    .line 472152
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2rt;
    .locals 1

    .prologue
    .line 472153
    const-class v0, LX/2rt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2rt;

    return-object v0
.end method

.method public static values()[LX/2rt;
    .locals 1

    .prologue
    .line 472154
    sget-object v0, LX/2rt;->$VALUES:[LX/2rt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2rt;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472155
    iget-object v0, p0, LX/2rt;->analyticsName:Ljava/lang/String;

    return-object v0
.end method
