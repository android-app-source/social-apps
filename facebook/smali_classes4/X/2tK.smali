.class public LX/2tK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/io/File;

.field private final b:Z

.field private c:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/io/RandomAccessFile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:J


# direct methods
.method public constructor <init>(Ljava/io/File;Z)V
    .locals 2

    .prologue
    .line 474917
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 474918
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2tK;->e:J

    .line 474919
    iput-object p1, p0, LX/2tK;->a:Ljava/io/File;

    .line 474920
    iput-boolean p2, p0, LX/2tK;->b:Z

    .line 474921
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)LX/3Am;
    .locals 6

    .prologue
    .line 474911
    iget-object v0, p0, LX/2tK;->d:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 474912
    iget-object v0, p0, LX/2tK;->d:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    iget-wide v2, p0, LX/2tK;->e:J

    iget-boolean v1, p0, LX/2tK;->b:Z

    invoke-static {v0, v2, v3, p1, v1}, LX/31S;->a(Ljava/nio/channels/FileChannel;JLjava/nio/ByteBuffer;Z)I

    move-result v0

    .line 474913
    new-instance v1, LX/3Am;

    iget-wide v2, p0, LX/2tK;->e:J

    invoke-direct {v1, v2, v3, v0}, LX/3Am;-><init>(JI)V

    .line 474914
    iget-wide v2, p0, LX/2tK;->e:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/2tK;->e:J

    .line 474915
    return-object v1

    .line 474916
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/io/File;
    .locals 2

    .prologue
    .line 474896
    iget-object v0, p0, LX/2tK;->c:Ljava/io/File;

    if-nez v0, :cond_0

    .line 474897
    iget-object v0, p0, LX/2tK;->a:Ljava/io/File;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/3Al;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 474898
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v1

    .line 474899
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 474900
    iput-object v0, p0, LX/2tK;->c:Ljava/io/File;

    .line 474901
    :cond_0
    iget-object v0, p0, LX/2tK;->c:Ljava/io/File;

    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 474907
    iget-object v0, p0, LX/2tK;->d:Ljava/io/RandomAccessFile;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 474908
    new-instance v0, Ljava/io/RandomAccessFile;

    invoke-virtual {p0}, LX/2tK;->a()Ljava/io/File;

    move-result-object v1

    const-string v2, "rw"

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LX/2tK;->d:Ljava/io/RandomAccessFile;

    .line 474909
    return-void

    .line 474910
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 474902
    iget-object v0, p0, LX/2tK;->d:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 474903
    iget-object v0, p0, LX/2tK;->d:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 474904
    const/4 v0, 0x0

    iput-object v0, p0, LX/2tK;->d:Ljava/io/RandomAccessFile;

    .line 474905
    return-void

    .line 474906
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
