.class public LX/2qB;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/2qC;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/2qC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 470704
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2qC;
    .locals 5

    .prologue
    .line 470705
    sget-object v0, LX/2qB;->a:LX/2qC;

    if-nez v0, :cond_1

    .line 470706
    const-class v1, LX/2qB;

    monitor-enter v1

    .line 470707
    :try_start_0
    sget-object v0, LX/2qB;->a:LX/2qC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 470708
    if-eqz v2, :cond_0

    .line 470709
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 470710
    invoke-static {v0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v3

    check-cast v3, LX/19j;

    invoke-static {v0}, LX/09G;->a(LX/0QB;)LX/09G;

    move-result-object v4

    check-cast v4, LX/09G;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object p0

    check-cast p0, LX/0lC;

    invoke-static {v3, v4, p0}, LX/2oQ;->a(LX/19j;LX/09G;LX/0lC;)LX/2qC;

    move-result-object v3

    move-object v0, v3

    .line 470711
    sput-object v0, LX/2qB;->a:LX/2qC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 470712
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 470713
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 470714
    :cond_1
    sget-object v0, LX/2qB;->a:LX/2qC;

    return-object v0

    .line 470715
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 470716
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 470717
    invoke-static {p0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v0

    check-cast v0, LX/19j;

    invoke-static {p0}, LX/09G;->a(LX/0QB;)LX/09G;

    move-result-object v1

    check-cast v1, LX/09G;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v2

    check-cast v2, LX/0lC;

    invoke-static {v0, v1, v2}, LX/2oQ;->a(LX/19j;LX/09G;LX/0lC;)LX/2qC;

    move-result-object v0

    return-object v0
.end method
