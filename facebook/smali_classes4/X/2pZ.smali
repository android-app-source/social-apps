.class public final LX/2pZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/video/engine/VideoPlayerParams;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field public e:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 468419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 468420
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2pZ;->b:Ljava/util/Map;

    .line 468421
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2pZ;->c:Ljava/util/Set;

    .line 468422
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2pZ;->e:D

    return-void
.end method

.method public static a(LX/2pa;)LX/2pZ;
    .locals 1

    .prologue
    .line 468415
    new-instance v0, LX/2pZ;

    invoke-direct {v0}, LX/2pZ;-><init>()V

    .line 468416
    if-eqz p0, :cond_0

    .line 468417
    invoke-virtual {v0, p0}, LX/2pZ;->b(LX/2pa;)LX/2pZ;

    .line 468418
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()LX/2pZ;
    .locals 1

    .prologue
    .line 468413
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2pZ;->d:Z

    .line 468414
    return-object p0
.end method

.method public final a(D)LX/2pZ;
    .locals 1

    .prologue
    .line 468411
    iput-wide p1, p0, LX/2pZ;->e:D

    .line 468412
    return-object p0
.end method

.method public final a(LX/0P1;)LX/2pZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "LX/2pZ;"
        }
    .end annotation

    .prologue
    .line 468408
    iget-object v0, p0, LX/2pZ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 468409
    iget-object v0, p0, LX/2pZ;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 468410
    return-object p0
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;)LX/2pZ;
    .locals 0

    .prologue
    .line 468389
    iput-object p1, p0, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 468390
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;
    .locals 1

    .prologue
    .line 468405
    iget-object v0, p0, LX/2pZ;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468406
    iget-object v0, p0, LX/2pZ;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 468407
    return-object p0
.end method

.method public final b(LX/2pa;)LX/2pZ;
    .locals 4

    .prologue
    .line 468396
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    .line 468397
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iput-object v0, p0, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 468398
    :cond_0
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_1

    .line 468399
    iget-object v0, p0, LX/2pZ;->b:Ljava/util/Map;

    iget-object v1, p1, LX/2pa;->b:LX/0P1;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 468400
    :cond_1
    iget-object v0, p1, LX/2pa;->c:LX/0Rf;

    if-eqz v0, :cond_2

    .line 468401
    iget-object v0, p0, LX/2pZ;->c:Ljava/util/Set;

    iget-object v1, p1, LX/2pa;->c:LX/0Rf;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 468402
    :cond_2
    iget-wide v0, p1, LX/2pa;->d:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_3

    .line 468403
    iget-wide v0, p1, LX/2pa;->d:D

    iput-wide v0, p0, LX/2pZ;->e:D

    .line 468404
    :cond_3
    return-object p0
.end method

.method public final b()LX/2pa;
    .locals 7

    .prologue
    .line 468391
    iget-boolean v0, p0, LX/2pZ;->d:Z

    if-eqz v0, :cond_1

    .line 468392
    iget-object v0, p0, LX/2pZ;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 468393
    iget-object v2, p0, LX/2pZ;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 468394
    :cond_0
    iget-object v0, p0, LX/2pZ;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 468395
    :cond_1
    new-instance v0, LX/2pa;

    iget-object v1, p0, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, p0, LX/2pZ;->b:Ljava/util/Map;

    invoke-static {v2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v2

    iget-object v3, p0, LX/2pZ;->c:Ljava/util/Set;

    invoke-static {v3}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v3

    iget-wide v4, p0, LX/2pZ;->e:D

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LX/2pa;-><init>(Lcom/facebook/video/engine/VideoPlayerParams;LX/0P1;LX/0Rf;DB)V

    return-object v0
.end method
