.class public final LX/3K5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;


# direct methods
.method public constructor <init>(Lcom/facebook/spherical/ui/SphericalNuxAnimationController;)V
    .locals 0

    .prologue
    .line 548340
    iput-object p1, p0, LX/3K5;->a:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 548343
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    .line 548344
    iget-object v0, p0, LX/3K5;->a:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    iget-object v0, v0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->b:LX/7Nl;

    if-eqz v0, :cond_0

    .line 548345
    iget-object v0, p0, LX/3K5;->a:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    iget-object v0, v0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->b:LX/7Nl;

    .line 548346
    iget-object v1, v0, LX/7Nl;->a:Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    .line 548347
    iget-object v2, v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->e:LX/7DE;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->e:LX/7DE;

    invoke-interface {v2}, LX/7DE;->a()LX/03z;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->e:LX/7DE;

    invoke-interface {v2}, LX/7DE;->a()LX/03z;

    move-result-object v2

    iget-boolean v2, v2, LX/03z;->isSpatial:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 548348
    if-eqz v2, :cond_0

    .line 548349
    iget-object v2, v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->a:LX/15W;

    invoke-virtual {v1}, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance p0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object p1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SPATIAL_AUDIO_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {p0, p1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class p1, LX/3lL;

    iget-object v0, v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->c:Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;

    invoke-virtual {v2, v3, p0, p1, v0}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 548350
    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 548342
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 548341
    return-void
.end method
