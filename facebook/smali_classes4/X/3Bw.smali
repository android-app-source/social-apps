.class public LX/3Bw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1ND;


# instance fields
.field public volatile a:LX/3Bq;

.field public volatile b:Z

.field public c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3Bq;)V
    .locals 1

    .prologue
    .line 529281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529282
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Bw;->b:Z

    .line 529283
    iput-object p1, p0, LX/3Bw;->a:LX/3Bq;

    .line 529284
    iget-object v0, p0, LX/3Bw;->a:LX/3Bq;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Bw;->a:LX/3Bq;

    invoke-interface {v0}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 529285
    iget-object v0, p0, LX/3Bw;->a:LX/3Bq;

    invoke-interface {v0}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/3Bw;->c:LX/0Rf;

    .line 529286
    :goto_0
    return-void

    .line 529287
    :cond_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 529288
    iput-object v0, p0, LX/3Bw;->c:LX/0Rf;

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 529280
    iget-object v0, p0, LX/3Bw;->c:LX/0Rf;

    return-object v0
.end method

.method public final a(Ljava/util/Set;)LX/1ND;
    .locals 2

    .prologue
    .line 529277
    if-nez p1, :cond_0

    .line 529278
    :goto_0
    return-object p0

    .line 529279
    :cond_0
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    iget-object v1, p0, LX/3Bw;->c:LX/0Rf;

    invoke-virtual {v0, v1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/3Bw;->c:LX/0Rf;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 3

    .prologue
    .line 529289
    const-string v0, "GraphQLWriteMutex.updateStale"

    const v1, 0x2d905039

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 529290
    :try_start_0
    invoke-virtual {p0}, LX/3Bw;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 529291
    sget-object v0, LX/0tc;->a:Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 529292
    const v1, 0xc048af3

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 529293
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/3Bw;->a:LX/3Bq;

    .line 529294
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 529295
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/3Bq;->a(Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    .line 529296
    invoke-static {p1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v1

    .line 529297
    iput-object v0, v1, LX/1lO;->k:Ljava/lang/Object;

    .line 529298
    move-object v1, v1

    .line 529299
    iget-object v2, p0, LX/3Bw;->a:LX/3Bq;

    instance-of v2, v2, LX/3Bp;

    if-nez v2, :cond_1

    .line 529300
    invoke-static {v0}, LX/11F;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1lO;->a(Ljava/util/Collection;)LX/1lO;

    .line 529301
    :cond_1
    invoke-virtual {v1}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 529302
    const v1, -0x29939a85

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x39b0b9f5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(LX/1NB;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 529271
    iget-boolean v1, p0, LX/3Bw;->b:Z

    if-eqz v1, :cond_1

    .line 529272
    const/4 v0, 0x1

    .line 529273
    :cond_0
    :goto_0
    return v0

    .line 529274
    :cond_1
    iget-object v1, p1, LX/1NB;->f:LX/1ND;

    instance-of v1, v1, LX/1NC;

    if-eqz v1, :cond_2

    .line 529275
    invoke-virtual {p0}, LX/3Bw;->b()Z

    move-result v0

    goto :goto_0

    .line 529276
    :cond_2
    iget-object v1, p1, LX/1NB;->f:LX/1ND;

    instance-of v1, v1, LX/3Bw;

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 1

    .prologue
    .line 529269
    sget-object v0, LX/0tc;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 529270
    iget-object v0, p0, LX/3Bw;->a:LX/3Bq;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
