.class public LX/22f;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 360183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 360177
    :try_start_0
    invoke-virtual {p0, p2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 360178
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 360179
    :goto_0
    :try_start_1
    move-object v1, v1

    .line 360180
    if-eqz v1, :cond_0

    .line 360181
    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 360182
    :cond_0
    :goto_1
    return-object v0

    :catch_0
    goto :goto_1

    :catch_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 360239
    invoke-static {p0}, LX/22f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 360240
    instance-of v1, v0, LX/0X9;

    if-eqz v1, :cond_0

    .line 360241
    check-cast v0, LX/0X9;

    invoke-interface {v0}, LX/0X9;->b()Ljava/lang/String;

    move-result-object v0

    .line 360242
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(C)Z
    .locals 1

    .prologue
    .line 360243
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 360220
    invoke-static {p0}, LX/22f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 360221
    instance-of v1, v0, LX/0X9;

    if-eqz v1, :cond_0

    .line 360222
    check-cast v0, LX/0X9;

    invoke-interface {v0}, LX/0X9;->a()Ljava/lang/String;

    move-result-object v0

    .line 360223
    :goto_0
    return-object v0

    .line 360224
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 360225
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 360226
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 360227
    const/4 v3, 0x3

    if-ge v2, v3, :cond_2

    .line 360228
    :cond_1
    :goto_1
    move v2, v2

    .line 360229
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 360230
    :cond_2
    add-int/lit8 v4, v2, -0x1

    .line 360231
    add-int/lit8 v3, v4, -0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 360232
    invoke-static {v4}, LX/22f;->a(C)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 360233
    :goto_2
    if-ltz v3, :cond_1

    .line 360234
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 360235
    const/16 p0, 0x24

    if-ne v4, p0, :cond_3

    move v2, v3

    .line 360236
    goto :goto_1

    .line 360237
    :cond_3
    invoke-static {v4}, LX/22f;->a(C)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 360238
    add-int/lit8 v3, v3, -0x1

    goto :goto_2
.end method

.method private static c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 360184
    const/4 v0, 0x4

    move v1, v0

    move-object v0, p0

    .line 360185
    :goto_0
    add-int/lit8 v2, v1, -0x1

    if-lez v1, :cond_6

    .line 360186
    instance-of v1, v0, LX/0X9;

    if-eqz v1, :cond_0

    .line 360187
    :goto_1
    return-object v0

    .line 360188
    :cond_0
    instance-of v1, v0, Ljava/util/concurrent/FutureTask;

    if-eqz v1, :cond_4

    .line 360189
    const/4 v1, 0x0

    .line 360190
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p0, 0x10

    if-gt v3, p0, :cond_1

    .line 360191
    const-class v3, Ljava/util/concurrent/FutureTask;

    const-string p0, "sync"

    invoke-static {v3, v0, p0}, LX/22f;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 360192
    if-eqz v3, :cond_1

    .line 360193
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string p0, "callable"

    invoke-static {v1, v3, p0}, LX/22f;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 360194
    :cond_1
    if-nez v1, :cond_2

    .line 360195
    const-class v1, Ljava/util/concurrent/FutureTask;

    const-string v3, "callable"

    invoke-static {v1, v0, v3}, LX/22f;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 360196
    :cond_2
    if-eqz v1, :cond_3

    .line 360197
    instance-of v3, v1, LX/0X9;

    if-eqz v3, :cond_8

    move-object v0, v1

    .line 360198
    :cond_3
    :goto_2
    move-object v0, v0

    .line 360199
    move v1, v2

    goto :goto_0

    .line 360200
    :cond_4
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getEnclosingClass()Ljava/lang/Class;

    move-result-object v1

    const-class v3, LX/0Vg;

    if-ne v1, v3, :cond_6

    .line 360201
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 360202
    const-string v1, "function"

    invoke-static {v3, v0, v1}, LX/22f;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 360203
    if-eqz v1, :cond_a

    .line 360204
    :cond_5
    :goto_3
    move-object p0, v1
    :try_end_0
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_0 .. :try_end_0} :catch_0

    .line 360205
    if-eqz p0, :cond_6

    move v1, v2

    move-object v0, p0

    .line 360206
    goto :goto_0

    .line 360207
    :catch_0
    :cond_6
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "this$0"

    invoke-static {v1, v0, v2}, LX/22f;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 360208
    if-eqz v1, :cond_7

    move-object v0, v1

    .line 360209
    :cond_7
    move-object v0, v0

    .line 360210
    goto :goto_1

    .line 360211
    :cond_8
    instance-of v3, v1, Ljava/util/concurrent/Callable;

    if-eqz v3, :cond_9

    .line 360212
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string p0, "task"

    invoke-static {v3, v1, p0}, LX/22f;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 360213
    if-nez v0, :cond_3

    :cond_9
    move-object v0, v1

    .line 360214
    goto :goto_2

    .line 360215
    :cond_a
    const-string v1, "val$function"

    invoke-static {v3, v0, v1}, LX/22f;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 360216
    if-nez v1, :cond_5

    .line 360217
    const-string v1, "val$callback"

    invoke-static {v3, v0, v1}, LX/22f;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 360218
    if-nez v1, :cond_5

    .line 360219
    const/4 v1, 0x0

    goto :goto_3
.end method
