.class public LX/28W;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/28W;


# instance fields
.field private final a:LX/18W;


# direct methods
.method public constructor <init>(LX/18W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374216
    iput-object p1, p0, LX/28W;->a:LX/18W;

    .line 374217
    return-void
.end method

.method public static a(LX/0QB;)LX/28W;
    .locals 4

    .prologue
    .line 374181
    sget-object v0, LX/28W;->b:LX/28W;

    if-nez v0, :cond_1

    .line 374182
    const-class v1, LX/28W;

    monitor-enter v1

    .line 374183
    :try_start_0
    sget-object v0, LX/28W;->b:LX/28W;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 374184
    if-eqz v2, :cond_0

    .line 374185
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 374186
    new-instance p0, LX/28W;

    invoke-static {v0}, LX/18W;->a(LX/0QB;)LX/18W;

    move-result-object v3

    check-cast v3, LX/18W;

    invoke-direct {p0, v3}, LX/28W;-><init>(LX/18W;)V

    .line 374187
    move-object v0, p0

    .line 374188
    sput-object v0, LX/28W;->b:LX/28W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 374189
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 374190
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 374191
    :cond_1
    sget-object v0, LX/28W;->b:LX/28W;

    return-object v0

    .line 374192
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 374193
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/List;LX/14U;)V
    .locals 7
    .param p4    # LX/14U;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/util/List",
            "<",
            "LX/2ZE;",
            ">;",
            "LX/14U;",
            ")V"
        }
    .end annotation

    .prologue
    .line 374194
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 374195
    iget-object v0, p0, LX/28W;->a:LX/18W;

    invoke-virtual {v0}, LX/18W;->a()LX/2VK;

    move-result-object v2

    .line 374196
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v3

    .line 374197
    const/4 v0, 0x0

    .line 374198
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ZE;

    .line 374199
    invoke-interface {v0}, LX/2ZE;->a()Ljava/lang/Iterable;

    move-result-object v5

    .line 374200
    invoke-interface {v3, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374201
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Vj;

    .line 374202
    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 374203
    add-int/lit8 v1, v1, 0x1

    .line 374204
    goto :goto_0

    .line 374205
    :cond_1
    if-lez v1, :cond_2

    .line 374206
    invoke-interface {v2, p1, p2, p4}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;LX/14U;)V

    .line 374207
    :cond_2
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 374208
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2ZE;

    .line 374209
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v4

    .line 374210
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Vj;

    .line 374211
    iget-object v6, v0, LX/2Vj;->c:Ljava/lang/String;

    move-object v0, v6

    .line 374212
    invoke-interface {v2, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v4, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 374213
    :cond_3
    invoke-interface {v1, v4}, LX/2ZE;->a(Ljava/util/Map;)V

    goto :goto_1

    .line 374214
    :cond_4
    return-void
.end method
