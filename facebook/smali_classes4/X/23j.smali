.class public LX/23j;
.super LX/151;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/23j;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;LX/03V;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 365080
    invoke-direct {p0, p1, p2, p3}, LX/151;-><init>(Landroid/content/ContentResolver;LX/03V;LX/0So;)V

    .line 365081
    return-void
.end method

.method public static a(LX/0QB;)LX/23j;
    .locals 6

    .prologue
    .line 365082
    sget-object v0, LX/23j;->b:LX/23j;

    if-nez v0, :cond_1

    .line 365083
    const-class v1, LX/23j;

    monitor-enter v1

    .line 365084
    :try_start_0
    sget-object v0, LX/23j;->b:LX/23j;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 365085
    if-eqz v2, :cond_0

    .line 365086
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 365087
    new-instance p0, LX/23j;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-direct {p0, v3, v4, v5}, LX/23j;-><init>(Landroid/content/ContentResolver;LX/03V;LX/0So;)V

    .line 365088
    move-object v0, p0

    .line 365089
    sput-object v0, LX/23j;->b:LX/23j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365090
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 365091
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 365092
    :cond_1
    sget-object v0, LX/23j;->b:LX/23j;

    return-object v0

    .line 365093
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 365094
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 365095
    const-string v0, "content://com.facebook.mlite.sso.MessengerLoggedInUserProvider/logged_in_user"

    return-object v0
.end method
