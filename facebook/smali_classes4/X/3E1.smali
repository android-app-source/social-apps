.class public LX/3E1;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/Boolean;

.field private final c:Ljava/lang/Boolean;

.field public final d:LX/0lB;

.field public final e:LX/03V;

.field private final f:LX/0bH;

.field private final g:LX/0SG;

.field public final h:LX/3E4;

.field private final i:LX/1EQ;

.field public final j:LX/0Uh;

.field private final k:LX/3Bx;

.field private final l:LX/31I;

.field private final m:LX/0y5;

.field public final n:LX/0ad;

.field public final o:Ljava/util/Random;

.field private final p:LX/1jZ;

.field private q:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 536835
    const-class v0, LX/3E1;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3E1;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0lB;LX/03V;LX/0bH;Ljava/lang/Boolean;Ljava/lang/Boolean;LX/3E4;LX/0Uh;LX/1EQ;LX/3Bx;LX/31I;LX/0y5;LX/0ad;Ljava/util/Random;LX/1jZ;)V
    .locals 0
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTabletLandscape;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .param p14    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 536818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 536819
    iput-object p3, p0, LX/3E1;->e:LX/03V;

    .line 536820
    iput-object p2, p0, LX/3E1;->d:LX/0lB;

    .line 536821
    iput-object p4, p0, LX/3E1;->f:LX/0bH;

    .line 536822
    iput-object p1, p0, LX/3E1;->g:LX/0SG;

    .line 536823
    iput-object p5, p0, LX/3E1;->b:Ljava/lang/Boolean;

    .line 536824
    iput-object p6, p0, LX/3E1;->c:Ljava/lang/Boolean;

    .line 536825
    iput-object p7, p0, LX/3E1;->h:LX/3E4;

    .line 536826
    iput-object p8, p0, LX/3E1;->j:LX/0Uh;

    .line 536827
    iput-object p9, p0, LX/3E1;->i:LX/1EQ;

    .line 536828
    iput-object p10, p0, LX/3E1;->k:LX/3Bx;

    .line 536829
    iput-object p11, p0, LX/3E1;->l:LX/31I;

    .line 536830
    iput-object p12, p0, LX/3E1;->m:LX/0y5;

    .line 536831
    iput-object p13, p0, LX/3E1;->n:LX/0ad;

    .line 536832
    iput-object p14, p0, LX/3E1;->o:Ljava/util/Random;

    .line 536833
    iput-object p15, p0, LX/3E1;->p:LX/1jZ;

    .line 536834
    return-void
.end method

.method public static a(LX/0QB;)LX/3E1;
    .locals 1

    .prologue
    .line 536817
    invoke-static {p0}, LX/3E1;->b(LX/0QB;)LX/3E1;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0lF;JLjava/lang/String;Lcom/facebook/graphql/model/FeedUnit;ILjava/lang/String;LX/0P1;)V
    .locals 10
    .param p1    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            "J",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "I",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 536815
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v9, p8

    invoke-virtual/range {v0 .. v9}, LX/3E1;->a(LX/0lF;JLjava/lang/String;Lcom/facebook/graphql/model/FeedUnit;ILjava/lang/String;ILX/0P1;)V

    .line 536816
    return-void
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/0P1;)V
    .locals 0
    .param p1    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 536812
    if-nez p1, :cond_0

    .line 536813
    :goto_0
    return-void

    .line 536814
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method private a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 3

    .prologue
    .line 536795
    iget-object v0, p0, LX/3E1;->j:LX/0Uh;

    const/16 v1, 0x3af

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 536796
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 536797
    :cond_0
    :goto_0
    return-void

    .line 536798
    :cond_1
    invoke-interface {p2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    .line 536799
    iget-object v1, p0, LX/3E1;->m:LX/0y5;

    invoke-virtual {v1, v0}, LX/0y5;->a(Ljava/lang/String;)LX/14s;

    move-result-object v0

    .line 536800
    iget-object v1, p0, LX/3E1;->m:LX/0y5;

    .line 536801
    iget-object v2, v1, LX/0y5;->a:LX/0y6;

    .line 536802
    iget-object v1, v2, LX/0y6;->b:LX/0y8;

    move-object v2, v1

    .line 536803
    move-object v1, v2

    .line 536804
    if-eqz v0, :cond_2

    .line 536805
    :try_start_0
    iget-object v2, p0, LX/3E1;->d:LX/0lB;

    invoke-virtual {v2, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 536806
    const-string v2, "client_feature"

    invoke-virtual {p1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 536807
    :cond_2
    if-eqz v1, :cond_0

    .line 536808
    iget-object v0, p0, LX/3E1;->d:LX/0lB;

    invoke-virtual {v0, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 536809
    const-string v1, "global_feature"

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 536810
    :catch_0
    move-exception v0

    .line 536811
    sget-object v1, LX/3E1;->a:Ljava/lang/String;

    const-string v2, "Error creating client feature json string"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(LX/0lF;)Z
    .locals 2
    .param p0    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 536794
    if-eqz p0, :cond_2

    invoke-virtual {p0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->STRING:LX/0nH;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, LX/0lF;->q()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    invoke-virtual {p0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->STRING:LX/0nH;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, LX/0lF;->e()I

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/3E1;
    .locals 16

    .prologue
    .line 536792
    new-instance v0, LX/3E1;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v2

    check-cast v2, LX/0lB;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v4

    check-cast v4, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/3E3;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-static/range {p0 .. p0}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-static/range {p0 .. p0}, LX/3E4;->a(LX/0QB;)LX/3E4;

    move-result-object v7

    check-cast v7, LX/3E4;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/1EQ;->a(LX/0QB;)LX/1EQ;

    move-result-object v9

    check-cast v9, LX/1EQ;

    invoke-static/range {p0 .. p0}, LX/3Bx;->a(LX/0QB;)LX/3Bx;

    move-result-object v10

    check-cast v10, LX/3Bx;

    invoke-static/range {p0 .. p0}, LX/3E5;->a(LX/0QB;)LX/31I;

    move-result-object v11

    check-cast v11, LX/31I;

    invoke-static/range {p0 .. p0}, LX/0y5;->a(LX/0QB;)LX/0y5;

    move-result-object v12

    check-cast v12, LX/0y5;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v14

    check-cast v14, Ljava/util/Random;

    invoke-static/range {p0 .. p0}, LX/1jZ;->a(LX/0QB;)LX/1jZ;

    move-result-object v15

    check-cast v15, LX/1jZ;

    invoke-direct/range {v0 .. v15}, LX/3E1;-><init>(LX/0SG;LX/0lB;LX/03V;LX/0bH;Ljava/lang/Boolean;Ljava/lang/Boolean;LX/3E4;LX/0Uh;LX/1EQ;LX/3Bx;LX/31I;LX/0y5;LX/0ad;Ljava/util/Random;LX/1jZ;)V

    .line 536793
    return-object v0
.end method

.method private b(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 3
    .param p2    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 536758
    if-eqz p2, :cond_1

    invoke-interface {p2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 536759
    iget-object v1, p0, LX/3E1;->n:LX/0ad;

    sget v2, LX/0fe;->N:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v1

    .line 536760
    if-lez v1, :cond_0

    iget-object v2, p0, LX/3E1;->o:Ljava/util/Random;

    invoke-virtual {v2, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 536761
    if-nez v0, :cond_2

    .line 536762
    :cond_1
    :goto_0
    return-void

    .line 536763
    :cond_2
    iget-object v0, p0, LX/3E1;->p:LX/1jZ;

    invoke-interface {p2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    .line 536764
    invoke-virtual {v0}, LX/1jZ;->a()Z

    move-result v2

    if-nez v2, :cond_3

    .line 536765
    const/4 v2, 0x0

    .line 536766
    :goto_1
    move-object v0, v2

    .line 536767
    if-eqz v0, :cond_1

    .line 536768
    const-string v1, "feed_unit_trace_info"

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    :cond_3
    invoke-static {v0, v1}, LX/1jZ;->b(LX/1jZ;Ljava/lang/String;)LX/2xl;

    move-result-object v2

    .line 536769
    if-nez v2, :cond_4

    .line 536770
    const/4 p0, 0x0

    .line 536771
    :goto_2
    move-object v2, p0

    .line 536772
    goto :goto_1

    .line 536773
    :cond_4
    new-instance p0, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {p0, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 536774
    const-string v0, "state"

    invoke-virtual {v2}, LX/2xl;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 536775
    invoke-virtual {v2}, LX/2xl;->b()I

    move-result v0

    if-ltz v0, :cond_5

    .line 536776
    const-string v0, "load_type"

    invoke-virtual {v2}, LX/2xl;->b()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 536777
    :cond_5
    invoke-virtual {v2}, LX/2xl;->c()I

    move-result v0

    if-ltz v0, :cond_6

    .line 536778
    const-string v0, "server_pos"

    invoke-virtual {v2}, LX/2xl;->c()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 536779
    :cond_6
    invoke-virtual {v2}, LX/2xl;->d()I

    move-result v0

    if-ltz v0, :cond_7

    .line 536780
    const-string v0, "organic_pos"

    invoke-virtual {v2}, LX/2xl;->d()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 536781
    :cond_7
    invoke-virtual {v2}, LX/2xl;->e()I

    move-result v0

    if-ltz v0, :cond_8

    .line 536782
    const-string v0, "ad_server_dist"

    invoke-virtual {v2}, LX/2xl;->e()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 536783
    :cond_8
    invoke-virtual {v2}, LX/2xl;->f()I

    move-result v0

    if-ltz v0, :cond_9

    .line 536784
    const-string v0, "consume_type"

    invoke-virtual {v2}, LX/2xl;->f()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 536785
    :cond_9
    invoke-virtual {v2}, LX/2xl;->g()I

    move-result v0

    if-ltz v0, :cond_a

    .line 536786
    const-string v0, "ad_dist"

    invoke-virtual {v2}, LX/2xl;->g()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 536787
    :cond_a
    invoke-virtual {v2}, LX/2xl;->i()I

    move-result v0

    if-ltz v0, :cond_b

    .line 536788
    const-string v0, "ad_orig_dist"

    invoke-virtual {v2}, LX/2xl;->i()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 536789
    :cond_b
    invoke-virtual {v2}, LX/2xl;->h()I

    move-result v0

    if-ltz v0, :cond_c

    .line 536790
    const-string v0, "ad_orig_pos"

    invoke-virtual {v2}, LX/2xl;->h()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 536791
    :cond_c
    invoke-virtual {p0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_2
.end method

.method private static c(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    .line 536836
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 536837
    const-string v0, "vpv_seen_state"

    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->ordinal()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 536838
    :cond_0
    return-void
.end method

.method private d(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 1

    .prologue
    .line 536754
    instance-of v0, p2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    move v0, v0

    .line 536755
    if-eqz v0, :cond_0

    .line 536756
    iget-object v0, p0, LX/3E1;->k:LX/3Bx;

    invoke-virtual {v0, p1, p2}, LX/3Bx;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 536757
    :cond_0
    return-void
.end method

.method public static d(LX/3E1;)Z
    .locals 3

    .prologue
    .line 536753
    iget-object v0, p0, LX/3E1;->j:LX/0Uh;

    const/16 v1, 0x469

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/0lF;JLjava/lang/String;Lcom/facebook/graphql/model/FeedUnit;ILjava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 8
    .param p1    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v4, 0x0

    .line 536707
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "vpv_duration"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "tracking"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "vpvd_time_delta"

    invoke-virtual {v0, v1, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 536708
    const-string v0, "native_timeline"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536709
    invoke-static {v1, p5}, LX/3E1;->c(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 536710
    :cond_0
    if-eqz p5, :cond_1

    invoke-interface {p5}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v2

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 536711
    iget-object v0, p0, LX/3E1;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {p5}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v4

    sub-long v4, v2, v4

    .line 536712
    :cond_1
    iget-object v0, p0, LX/3E1;->i:LX/1EQ;

    move v2, p6

    move-object v3, p7

    move/from16 v6, p8

    invoke-virtual/range {v0 .. v6}, LX/1EQ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;ILjava/lang/String;JI)V

    .line 536713
    const-string v0, "video_home"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 536714
    invoke-direct {p0, v1, p5}, LX/3E1;->d(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 536715
    :cond_2
    invoke-direct {p0, v1, p5}, LX/3E1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 536716
    invoke-direct {p0, v1, p5}, LX/3E1;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 536717
    return-object v1
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 536751
    iget-object v0, p0, LX/3E1;->f:LX/0bH;

    new-instance v1, LX/1Zh;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, LX/1Zh;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 536752
    return-void
.end method

.method public final a(LX/0lF;JLjava/lang/String;)V
    .locals 10
    .param p1    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 536749
    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v7, v5

    move-object v8, v5

    invoke-direct/range {v0 .. v8}, LX/3E1;->a(LX/0lF;JLjava/lang/String;Lcom/facebook/graphql/model/FeedUnit;ILjava/lang/String;LX/0P1;)V

    .line 536750
    return-void
.end method

.method public final a(LX/0lF;JLjava/lang/String;Lcom/facebook/graphql/model/FeedUnit;ILjava/lang/String;ILX/0P1;)V
    .locals 2
    .param p1    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            "J",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "I",
            "Ljava/lang/String;",
            "I",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 536743
    invoke-static {p1}, LX/3E1;->a(LX/0lF;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 536744
    :goto_0
    return-void

    .line 536745
    :cond_0
    invoke-virtual/range {p0 .. p8}, LX/3E1;->a(LX/0lF;JLjava/lang/String;Lcom/facebook/graphql/model/FeedUnit;ILjava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 536746
    invoke-static {v0, p9}, LX/3E1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/0P1;)V

    .line 536747
    iget-object v1, p0, LX/3E1;->l:LX/31I;

    invoke-interface {v1, p5, p2, p3}, LX/31I;->a(Lcom/facebook/graphql/model/FeedUnit;J)V

    .line 536748
    invoke-virtual {p0, v0}, LX/3E1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_0
.end method

.method public final a(LX/0lF;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/graphql/model/FeedUnit;LX/0P1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 536728
    invoke-static {p1}, LX/3E1;->a(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 536729
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "viewport_visible"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "tracking"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "vpv_seq_id"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 536730
    iput-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 536731
    move-object v1, v0

    .line 536732
    const-string v0, "native_timeline"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536733
    invoke-static {v1, p6}, LX/3E1;->c(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 536734
    :cond_0
    iget-object v0, p0, LX/3E1;->i:LX/1EQ;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    move v2, p4

    move-object v3, p5

    invoke-virtual/range {v0 .. v6}, LX/1EQ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;ILjava/lang/String;JI)V

    .line 536735
    iget-object v0, p0, LX/3E1;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 536736
    const-string v2, "impression_type"

    iget-object v0, p0, LX/3E1;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "landscape"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 536737
    :cond_1
    invoke-static {v1, p7}, LX/3E1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/0P1;)V

    .line 536738
    invoke-static {p0}, LX/3E1;->d(LX/3E1;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 536739
    iget-object v0, p0, LX/3E1;->h:LX/3E4;

    invoke-virtual {v0, v1}, LX/3E4;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 536740
    :goto_1
    return-void

    .line 536741
    :cond_2
    const-string v0, "portrait"

    goto :goto_0

    .line 536742
    :cond_3
    iget-object v0, p0, LX/3E1;->h:LX/3E4;

    invoke-virtual {v0, v1}, LX/3E4;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_1
.end method

.method public final a(LX/162;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 536724
    iget-object v0, p0, LX/3E1;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3E1;->q:J

    sub-long/2addr v0, v2

    .line 536725
    if-nez p1, :cond_0

    .line 536726
    :goto_0
    return-void

    .line 536727
    :cond_0
    invoke-virtual {p0, p1, v0, v1, p2}, LX/3E1;->a(LX/0lF;JLjava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 536720
    invoke-static {p0}, LX/3E1;->d(LX/3E1;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536721
    iget-object v0, p0, LX/3E1;->h:LX/3E4;

    invoke-virtual {v0, p1}, LX/3E4;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 536722
    :goto_0
    return-void

    .line 536723
    :cond_0
    iget-object v0, p0, LX/3E1;->h:LX/3E4;

    invoke-virtual {v0, p1}, LX/3E4;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 536718
    iget-object v0, p0, LX/3E1;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3E1;->q:J

    .line 536719
    return-void
.end method
