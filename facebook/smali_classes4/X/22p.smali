.class public LX/22p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 361301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361302
    iput-boolean v0, p0, LX/22p;->a:Z

    .line 361303
    iput-boolean v0, p0, LX/22p;->b:Z

    .line 361304
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/22p;->c:Z

    .line 361305
    return-void
.end method

.method public static a(LX/0QB;)LX/22p;
    .locals 1

    .prologue
    .line 361306
    new-instance v0, LX/22p;

    invoke-direct {v0}, LX/22p;-><init>()V

    .line 361307
    move-object v0, v0

    .line 361308
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 361309
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/22p;->c:Z

    .line 361310
    return-void
.end method

.method public final a(LX/23E;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 361311
    iget-boolean v0, p0, LX/22p;->c:Z

    if-nez v0, :cond_1

    .line 361312
    :cond_0
    :goto_0
    return-void

    .line 361313
    :cond_1
    sget-object v0, LX/23E;->BEFORE:LX/23E;

    if-ne p1, v0, :cond_2

    iget-boolean v0, p0, LX/22p;->a:Z

    if-nez v0, :cond_0

    :cond_2
    sget-object v0, LX/23E;->AFTER:LX/23E;

    if-ne p1, v0, :cond_3

    iget-boolean v0, p0, LX/22p;->b:Z

    if-nez v0, :cond_0

    .line 361314
    :cond_3
    sget-object v0, LX/23E;->BEFORE:LX/23E;

    if-ne p1, v0, :cond_4

    .line 361315
    iput-boolean v1, p0, LX/22p;->a:Z

    .line 361316
    :cond_4
    sget-object v0, LX/23E;->AFTER:LX/23E;

    if-ne p1, v0, :cond_0

    .line 361317
    iput-boolean v1, p0, LX/22p;->b:Z

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 361318
    iget-boolean v0, p0, LX/22p;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/22p;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
