.class public LX/2Q5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Landroid/content/pm/Signature;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/2Q6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Q6",
            "<",
            "Landroid/content/pm/Signature;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(LX/0Xu;Landroid/content/Context;Landroid/content/pm/PackageManager;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<",
            "Landroid/content/pm/Signature;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/Context;",
            "Landroid/content/pm/PackageManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 407575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407576
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 407577
    invoke-static {}, LX/2Q6;->z()LX/2Q7;

    move-result-object v2

    .line 407578
    invoke-interface {p1}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/Signature;

    .line 407579
    const-string v4, "*|all_packages|*"

    invoke-interface {p1, v0, v4}, LX/0Xu;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 407580
    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 407581
    :cond_0
    invoke-interface {p1, v0}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, LX/2Q7;->a(Ljava/lang/Object;Ljava/lang/Iterable;)LX/2Q7;

    goto :goto_0

    .line 407582
    :cond_1
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/2Q5;->a:LX/0Rf;

    .line 407583
    invoke-virtual {v2}, LX/2Q7;->a()LX/2Q6;

    move-result-object v0

    iput-object v0, p0, LX/2Q5;->b:LX/2Q6;

    .line 407584
    iput-object p2, p0, LX/2Q5;->c:Landroid/content/Context;

    .line 407585
    iput-object p3, p0, LX/2Q5;->d:Landroid/content/pm/PackageManager;

    .line 407586
    return-void
.end method

.method private a(I)LX/0Rf;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407605
    iget-object v0, p0, LX/2Q5;->d:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 407606
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 407607
    :cond_0
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No packages associated with uid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407608
    :cond_1
    invoke-static {v0}, LX/0Rf;->copyOf([Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Set;)Landroid/content/pm/Signature;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/content/pm/Signature;"
        }
    .end annotation

    .prologue
    .line 407609
    const/4 v0, 0x0

    .line 407610
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 407611
    :try_start_0
    iget-object v3, p0, LX/2Q5;->d:Landroid/content/pm/PackageManager;

    const/16 v4, 0x40

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 407612
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 407613
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Package name mismatch: expected="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", was="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 407614
    :catch_0
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Name not found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 407615
    :cond_1
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v4, :cond_2

    iget-object v4, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v4, v4

    if-nez v4, :cond_3

    .line 407616
    :cond_2
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Signatures are missing: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 407617
    :cond_3
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v4, v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_4

    .line 407618
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Multiple signatures not supported: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 407619
    :cond_4
    iget-object v0, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    .line 407620
    if-nez v1, :cond_5

    move-object v1, v0

    .line 407621
    goto/16 :goto_0

    .line 407622
    :cond_5
    invoke-virtual {v1, v0}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 407623
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Uid "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has inconsistent signatures across packages."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407624
    :cond_6
    if-nez v1, :cond_7

    .line 407625
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "No uid signature."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407626
    :cond_7
    return-object v1
.end method

.method private static b(LX/2Q5;)LX/Jut;
    .locals 4

    .prologue
    .line 407591
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 407592
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This method should be called on behalf of an IPC transaction from binder thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407593
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 407594
    invoke-direct {p0, v0}, LX/2Q5;->a(I)LX/0Rf;

    move-result-object v0

    .line 407595
    invoke-direct {p0, v0}, LX/2Q5;->a(Ljava/util/Set;)Landroid/content/pm/Signature;

    move-result-object v1

    .line 407596
    iget-object v2, p0, LX/2Q5;->a:LX/0Rf;

    invoke-virtual {v2, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 407597
    invoke-static {v1, v0}, LX/Jut;->a(Landroid/content/pm/Signature;Ljava/util/Set;)LX/Jut;

    move-result-object v0

    .line 407598
    :goto_0
    return-object v0

    .line 407599
    :cond_1
    iget-object v2, p0, LX/2Q5;->b:LX/2Q6;

    invoke-virtual {v2, v1}, LX/2Q6;->e(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    .line 407600
    invoke-static {v0, v2}, LX/0RA;->b(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v2

    .line 407601
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 407602
    invoke-static {v1, v2}, LX/Jut;->a(Landroid/content/pm/Signature;Ljava/util/Set;)LX/Jut;

    move-result-object v0

    goto :goto_0

    .line 407603
    :cond_2
    new-instance v2, LX/Jut;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v1, v0}, LX/Jut;-><init>(ZLandroid/content/pm/Signature;Ljava/util/Set;)V

    move-object v0, v2

    .line 407604
    goto :goto_0
.end method


# virtual methods
.method public final a()LX/Jut;
    .locals 2

    .prologue
    .line 407587
    invoke-static {p0}, LX/2Q5;->b(LX/2Q5;)LX/Jut;

    move-result-object v0

    .line 407588
    iget-boolean v1, v0, LX/Jut;->a:Z

    if-nez v1, :cond_0

    .line 407589
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407590
    :cond_0
    return-object v0
.end method
