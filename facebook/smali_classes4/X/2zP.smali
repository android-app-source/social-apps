.class public final enum LX/2zP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2zP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2zP;

.field public static final enum CELLULAR:LX/2zP;

.field public static final enum NOT_CONNECTED:LX/2zP;

.field public static final enum WIFI:LX/2zP;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 483388
    new-instance v0, LX/2zP;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v2}, LX/2zP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2zP;->WIFI:LX/2zP;

    .line 483389
    new-instance v0, LX/2zP;

    const-string v1, "CELLULAR"

    invoke-direct {v0, v1, v3}, LX/2zP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2zP;->CELLULAR:LX/2zP;

    .line 483390
    new-instance v0, LX/2zP;

    const-string v1, "NOT_CONNECTED"

    invoke-direct {v0, v1, v4}, LX/2zP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2zP;->NOT_CONNECTED:LX/2zP;

    .line 483391
    const/4 v0, 0x3

    new-array v0, v0, [LX/2zP;

    sget-object v1, LX/2zP;->WIFI:LX/2zP;

    aput-object v1, v0, v2

    sget-object v1, LX/2zP;->CELLULAR:LX/2zP;

    aput-object v1, v0, v3

    sget-object v1, LX/2zP;->NOT_CONNECTED:LX/2zP;

    aput-object v1, v0, v4

    sput-object v0, LX/2zP;->$VALUES:[LX/2zP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 483387
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2zP;
    .locals 1

    .prologue
    .line 483386
    const-class v0, LX/2zP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2zP;

    return-object v0
.end method

.method public static values()[LX/2zP;
    .locals 1

    .prologue
    .line 483385
    sget-object v0, LX/2zP;->$VALUES:[LX/2zP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2zP;

    return-object v0
.end method
