.class public final LX/2NR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 0

    .prologue
    .line 399823
    iput-object p1, p0, LX/2NR;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 399824
    iget-object v0, p0, LX/2NR;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    .line 399825
    iput-object p1, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aM:Ljava/lang/String;

    .line 399826
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 399827
    const-string v1, "get_sso_user_params"

    iget-object v2, p0, LX/2NR;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aM:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 399828
    iget-object v1, p0, LX/2NR;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v1, v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->w:LX/0aG;

    const-string v2, "get_sso_user"

    const v3, 0x76943e1f

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    .line 399829
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 399830
    iget-object v1, p0, LX/2NR;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v1, v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bU:LX/0TF;

    iget-object v2, p0, LX/2NR;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->B:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 399831
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 399832
    sget-object v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->p:Ljava/lang/Class;

    const-string v1, "Could not get auth token from Kindle"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 399833
    iget-object v0, p0, LX/2NR;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aO:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$2$1;

    invoke-direct {v1, p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$2$1;-><init>(LX/2NR;)V

    const-wide/16 v2, 0x3e8

    const v4, -0x3855a15a

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 399834
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 399835
    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, LX/2NR;->a(Ljava/lang/String;)V

    return-void
.end method
