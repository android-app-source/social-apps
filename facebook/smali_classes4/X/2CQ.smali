.class public LX/2CQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/app/Activity;

.field private b:LX/2DR;

.field private c:Lcom/facebook/content/SecureContextHelper;

.field private d:LX/0hv;

.field public e:LX/10M;

.field public f:Z

.field public g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Xl;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final k:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/app/Activity;LX/2DR;Lcom/facebook/content/SecureContextHelper;LX/0hv;LX/10M;Ljava/lang/Boolean;LX/0Or;LX/0Xl;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;)V
    .locals 1
    .param p6    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAWorkUser;
        .end annotation
    .end param
    .param p8    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p11    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LX/2DR;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0hv;",
            "LX/10M;",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 382750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 382751
    iput-object p1, p0, LX/2CQ;->a:Landroid/app/Activity;

    .line 382752
    iput-object p2, p0, LX/2CQ;->b:LX/2DR;

    .line 382753
    iput-object p3, p0, LX/2CQ;->c:Lcom/facebook/content/SecureContextHelper;

    .line 382754
    iput-object p4, p0, LX/2CQ;->d:LX/0hv;

    .line 382755
    iput-object p5, p0, LX/2CQ;->e:LX/10M;

    .line 382756
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/2CQ;->f:Z

    .line 382757
    iput-object p7, p0, LX/2CQ;->g:LX/0Or;

    .line 382758
    iput-object p8, p0, LX/2CQ;->h:LX/0Xl;

    .line 382759
    iput-object p9, p0, LX/2CQ;->i:LX/0Or;

    .line 382760
    iput-object p10, p0, LX/2CQ;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 382761
    iput-object p11, p0, LX/2CQ;->k:LX/0Uh;

    .line 382762
    return-void
.end method

.method public static a(LX/0QB;)LX/2CQ;
    .locals 1

    .prologue
    .line 382749
    invoke-static {p0}, LX/2CQ;->b(LX/0QB;)LX/2CQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/auth/credentials/LoginCredentials;Ljava/lang/String;Lcom/facebook/katana/service/AppSession;LX/278;LX/0TF;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedClass"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/credentials/LoginCredentials;",
            "Ljava/lang/String;",
            "Lcom/facebook/katana/service/AppSession;",
            "LX/278;",
            "LX/0TF",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 382732
    invoke-virtual {p2}, Lcom/facebook/katana/service/AppSession;->a()V

    .line 382733
    if-eqz p3, :cond_0

    .line 382734
    invoke-virtual {p2, p3}, Lcom/facebook/katana/service/AppSession;->a(LX/278;)V

    .line 382735
    :cond_0
    const/4 v1, 0x0

    .line 382736
    iget-object v0, p2, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    move-object v0, v0

    .line 382737
    sget-object v2, LX/2A1;->STATUS_LOGGED_OUT:LX/2A1;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 382738
    iget-object v0, p2, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/katana/service/AppSession;->e(Landroid/content/Context;)V

    .line 382739
    iput-object p4, p2, Lcom/facebook/katana/service/AppSession;->i:LX/0TF;

    .line 382740
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 382741
    const-string v2, "passwordCredentials"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 382742
    const-string v2, "error_detail_type_param"

    const-string v3, "button_with_disabled"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 382743
    iget-object v2, p2, Lcom/facebook/katana/service/AppSession;->o:LX/0aG;

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const p3, 0x73e06230

    invoke-static {v2, p1, v0, v3, p3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 382744
    new-instance v2, LX/2Dw;

    invoke-direct {v2, p2}, LX/2Dw;-><init>(Lcom/facebook/katana/service/AppSession;)V

    iput-object v2, p2, Lcom/facebook/katana/service/AppSession;->f:LX/0Ve;

    .line 382745
    iget-object v1, p2, Lcom/facebook/katana/service/AppSession;->f:LX/0Ve;

    iget-object v2, p2, Lcom/facebook/katana/service/AppSession;->p:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 382746
    sget-object v0, LX/2A1;->STATUS_LOGGING_IN:LX/2A1;

    invoke-static {p2, v0}, Lcom/facebook/katana/service/AppSession;->a$redex0(Lcom/facebook/katana/service/AppSession;LX/2A1;)V

    .line 382747
    return-void

    :cond_1
    move v0, v1

    .line 382748
    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2CQ;
    .locals 12

    .prologue
    .line 382683
    new-instance v0, LX/2CQ;

    invoke-static {p0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-static {p0}, LX/2DR;->b(LX/0QB;)LX/2DR;

    move-result-object v2

    check-cast v2, LX/2DR;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0hv;->a(LX/0QB;)LX/0hv;

    move-result-object v4

    check-cast v4, LX/0hv;

    invoke-static {p0}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v5

    check-cast v5, LX/10M;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    const/16 v7, 0x2fc

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {p0}, LX/0aQ;->a(LX/0QB;)LX/0aQ;

    move-result-object v8

    check-cast v8, LX/0Xl;

    const/16 v9, 0x12cb

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-direct/range {v0 .. v11}, LX/2CQ;-><init>(Landroid/app/Activity;LX/2DR;Lcom/facebook/content/SecureContextHelper;LX/0hv;LX/10M;Ljava/lang/Boolean;LX/0Or;LX/0Xl;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;)V

    .line 382684
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 382685
    iget-boolean v0, p0, LX/2CQ;->f:Z

    if-nez v0, :cond_0

    .line 382686
    iget-object v0, p0, LX/2CQ;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 382687
    if-eqz v0, :cond_0

    .line 382688
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 382689
    if-eqz v1, :cond_0

    .line 382690
    iget-object v1, p0, LX/2CQ;->e:LX/10M;

    .line 382691
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 382692
    iget-object v3, v1, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    .line 382693
    invoke-static {v0}, LX/2ss;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v4

    iget-object v5, v1, LX/10M;->i:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 382694
    invoke-interface {v3}, LX/0hN;->commit()V

    .line 382695
    :cond_0
    const/high16 v2, 0x100000

    const/4 v0, 0x0

    .line 382696
    const-string v1, "finish_immediately"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "flags"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_9

    .line 382697
    :cond_1
    :goto_0
    move v0, v0

    .line 382698
    if-eqz v0, :cond_2

    .line 382699
    :goto_1
    return-void

    .line 382700
    :cond_2
    iget-object v0, p0, LX/2CQ;->b:LX/2DR;

    invoke-virtual {v0}, LX/2DR;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 382701
    iget-object v0, p0, LX/2CQ;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    .line 382702
    :cond_3
    const-string v0, "login_redirect"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 382703
    iget-object v0, p0, LX/2CQ;->a:Landroid/app/Activity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 382704
    iget-object v0, p0, LX/2CQ;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    .line 382705
    :cond_4
    if-eqz p2, :cond_5

    .line 382706
    iget-object v0, p0, LX/2CQ;->c:Lcom/facebook/content/SecureContextHelper;

    const/4 v1, 0x1

    iget-object v2, p0, LX/2CQ;->a:Landroid/app/Activity;

    invoke-interface {v0, p2, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_1

    .line 382707
    :cond_5
    iget-object v0, p0, LX/2CQ;->b:LX/2DR;

    .line 382708
    const/4 v1, 0x0

    .line 382709
    iget-object v2, v0, LX/2DR;->c:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "calling_intent"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 382710
    new-instance v2, Landroid/content/Intent;

    iget-object v1, v0, LX/2DR;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "calling_intent"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 382711
    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const v3, -0x10000001

    and-int/2addr v1, v3

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object v1, v2

    .line 382712
    :cond_6
    invoke-static {v0, v1}, LX/2DR;->a(LX/2DR;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    move-object v0, v1

    .line 382713
    if-nez v0, :cond_7

    .line 382714
    iget-object v0, p0, LX/2CQ;->b:LX/2DR;

    invoke-virtual {v0}, LX/2DR;->d()Landroid/content/Intent;

    move-result-object v0

    .line 382715
    iget-object v1, p0, LX/2CQ;->d:LX/0hv;

    invoke-virtual {v1}, LX/0hv;->a()V

    .line 382716
    const/4 v4, 0x0

    .line 382717
    iget-object v1, p0, LX/2CQ;->i:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 382718
    iget-object v2, p0, LX/2CQ;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1CA;->y:LX/0Tn;

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-nez v2, :cond_7

    if-eqz v1, :cond_7

    iget-object v2, p0, LX/2CQ;->k:LX/0Uh;

    const/16 v3, 0xf

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_a

    .line 382719
    :cond_7
    :goto_2
    iget-boolean v1, p0, LX/2CQ;->f:Z

    if-nez v1, :cond_8

    .line 382720
    iget-object v1, p0, LX/2CQ;->e:LX/10M;

    invoke-virtual {v1}, LX/10M;->m()V

    .line 382721
    :cond_8
    iget-object v1, p0, LX/2CQ;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/2CQ;->a:Landroid/app/Activity;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 382722
    iget-object v0, p0, LX/2CQ;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_1

    .line 382723
    :cond_9
    iget-object v0, p0, LX/2CQ;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 382724
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 382725
    :cond_a
    iget-object v2, p0, LX/2CQ;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/1CA;->y:LX/0Tn;

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 382726
    new-instance v2, Landroid/content/Intent;

    sget-object v3, LX/2b2;->C:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 382727
    const-string v3, "user_id"

    .line 382728
    iget-object v4, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v4

    .line 382729
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 382730
    const-string v3, "user_display_name"

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 382731
    iget-object v1, p0, LX/2CQ;->h:LX/0Xl;

    invoke-interface {v1, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    goto :goto_2
.end method
