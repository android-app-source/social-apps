.class public LX/30I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 484244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484245
    iput-object p1, p0, LX/30I;->a:LX/0Or;

    .line 484246
    iput-object p2, p0, LX/30I;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 484247
    return-void
.end method

.method public static a(LX/0QB;)LX/30I;
    .locals 1

    .prologue
    .line 484243
    invoke-static {p0}, LX/30I;->b(LX/0QB;)LX/30I;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/30I;
    .locals 3

    .prologue
    .line 484234
    new-instance v1, LX/30I;

    const/16 v0, 0x15e7

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v2, v0}, LX/30I;-><init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 484235
    return-object v1
.end method

.method public static c(LX/30I;)LX/0Tn;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 484248
    iget-object v0, p0, LX/30I;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 484249
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 484250
    const/4 v0, 0x0

    .line 484251
    :goto_0
    return-object v0

    .line 484252
    :cond_0
    sget-object v1, LX/32V;->b:LX/0Tn;

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    move-object v0, v1

    .line 484253
    goto :goto_0
.end method

.method public static declared-synchronized d(LX/30I;)V
    .locals 4

    .prologue
    .line 484236
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/30I;->c(LX/30I;)LX/0Tn;

    move-result-object v0

    .line 484237
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/30I;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/30I;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/32V;->a:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 484238
    iget-object v1, p0, LX/30I;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/32V;->a:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v1

    .line 484239
    invoke-virtual {v1}, LX/03R;->isSet()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 484240
    iget-object v2, p0, LX/30I;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/03R;->asBoolean(Z)Z

    move-result v1

    invoke-interface {v2, v0, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484241
    :cond_0
    monitor-exit p0

    return-void

    .line 484242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 484229
    invoke-static {p0}, LX/30I;->d(LX/30I;)V

    .line 484230
    invoke-static {p0}, LX/30I;->c(LX/30I;)LX/0Tn;

    move-result-object v0

    .line 484231
    if-nez v0, :cond_0

    .line 484232
    :goto_0
    return-void

    .line 484233
    :cond_0
    iget-object v1, p0, LX/30I;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, v0, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 484225
    invoke-static {p0}, LX/30I;->d(LX/30I;)V

    .line 484226
    invoke-static {p0}, LX/30I;->c(LX/30I;)LX/0Tn;

    move-result-object v1

    .line 484227
    if-nez v1, :cond_0

    .line 484228
    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, LX/30I;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2, v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    goto :goto_0
.end method
