.class public LX/2rP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16E;
.implements LX/0i1;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0fO;

.field public c:Landroid/view/View;

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0fO;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 471899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 471900
    iput-object p1, p0, LX/2rP;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 471901
    iput-object p2, p0, LX/2rP;->b:LX/0fO;

    .line 471902
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 471903
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 3

    .prologue
    .line 471904
    iget-object v0, p0, LX/2rP;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/AwE;->e:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 471905
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 471906
    iget-object v0, p0, LX/2rP;->c:Landroid/view/View;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 471907
    new-instance v1, LX/0hs;

    const/4 v0, 0x2

    invoke-direct {v1, p1, v0}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 471908
    iget-object v0, p0, LX/2rP;->b:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0827a2

    :goto_0
    invoke-virtual {v1, v0}, LX/0hs;->b(I)V

    .line 471909
    const/4 v0, -0x1

    .line 471910
    iput v0, v1, LX/0hs;->t:I

    .line 471911
    sget-object v0, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v1, v0}, LX/0ht;->a(LX/3AV;)V

    .line 471912
    iget-object v0, p0, LX/2rP;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, LX/0ht;->a(Landroid/view/View;)V

    .line 471913
    iget-object v0, p0, LX/2rP;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/AwE;->e:LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 471914
    return-void

    .line 471915
    :cond_0
    const v0, 0x7f0827a3

    goto :goto_0
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 471916
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 471917
    const-string v0, "4487"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 471918
    iget-object v0, p0, LX/2rP;->d:LX/0Px;

    if-nez v0, :cond_0

    .line 471919
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_CAPTURE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/2rP;->d:LX/0Px;

    .line 471920
    :cond_0
    iget-object v0, p0, LX/2rP;->d:LX/0Px;

    return-object v0
.end method
