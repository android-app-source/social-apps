.class public LX/36H;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498541
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 498542
    return-void
.end method


# virtual methods
.method public final a(LX/1Wi;)Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ":",
            "LX/1Pg;",
            ":",
            "LX/1Pn;",
            ":",
            "LX/1Po;",
            ":",
            "LX/1Pq;",
            ":",
            "LX/1Pv;",
            ":",
            "LX/1Pr;",
            ":",
            "LX/1Ps;",
            ">(",
            "LX/1Wi;",
            ")",
            "Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 498543
    new-instance v0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/1We;->a(LX/0QB;)LX/1We;

    move-result-object v2

    check-cast v2, LX/1We;

    invoke-static {p0}, LX/1Wm;->a(LX/0QB;)LX/1Wm;

    move-result-object v3

    check-cast v3, LX/1Wm;

    invoke-static {p0}, LX/1Wn;->a(LX/0QB;)LX/1Wn;

    move-result-object v4

    check-cast v4, LX/1Wn;

    invoke-static {p0}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;-><init>(Landroid/content/Context;LX/1We;LX/1Wm;LX/1Wn;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;LX/0ad;LX/1Wi;)V

    .line 498544
    return-object v0
.end method
