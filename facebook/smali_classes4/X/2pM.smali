.class public LX/2pM;
.super LX/2oy;
.source ""


# instance fields
.field public a:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/19m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/19w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/16U;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final o:Landroid/os/Handler;

.field private final p:LX/2pN;

.field private final q:Lcom/facebook/widget/text/BetterTextView;

.field private final r:Landroid/view/View;

.field private final s:Z

.field public t:LX/2pO;

.field public u:LX/1A0;

.field public v:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public w:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 468020
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/2pM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 468021
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 468022
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/2pM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 468023
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 468024
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 468025
    new-instance v0, LX/2pN;

    invoke-direct {v0, p0}, LX/2pN;-><init>(LX/2pM;)V

    iput-object v0, p0, LX/2pM;->p:LX/2pN;

    .line 468026
    sget-object v0, LX/2pO;->DEFAULT:LX/2pO;

    iput-object v0, p0, LX/2pM;->t:LX/2pO;

    .line 468027
    sget-object v0, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    iput-object v0, p0, LX/2pM;->u:LX/1A0;

    .line 468028
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, LX/2pM;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v4

    check-cast v4, LX/0yc;

    invoke-static {v0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object v5

    check-cast v5, LX/19m;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v6

    check-cast v6, LX/0tQ;

    invoke-static {v0}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object p1

    check-cast p1, LX/19w;

    invoke-static {v0}, LX/16U;->a(LX/0QB;)LX/16U;

    move-result-object p2

    check-cast p2, LX/16U;

    const/16 p3, 0x122d

    invoke-static {v0, p3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p3

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v4, v3, LX/2pM;->a:LX/0yc;

    iput-object v5, v3, LX/2pM;->b:LX/19m;

    iput-object v6, v3, LX/2pM;->c:LX/0tQ;

    iput-object p1, v3, LX/2pM;->d:LX/19w;

    iput-object p2, v3, LX/2pM;->e:LX/16U;

    iput-object p3, v3, LX/2pM;->f:LX/0Or;

    iput-object v0, v3, LX/2pM;->n:LX/0Uh;

    .line 468029
    const v0, 0x7f031340

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 468030
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/2pP;

    invoke-direct {v1, p0}, LX/2pP;-><init>(LX/2pM;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468031
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/2pQ;

    invoke-direct {v1, p0}, LX/2pQ;-><init>(LX/2pM;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468032
    const v0, 0x7f0d1779

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/2pM;->r:Landroid/view/View;

    .line 468033
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/2pM;->o:Landroid/os/Handler;

    .line 468034
    const v0, 0x7f0d2c9d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/2pM;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 468035
    iget-object v0, p0, LX/2pM;->c:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468036
    iget-object v0, p0, LX/2pM;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 468037
    sget-object v1, LX/2qX;->b:[I

    iget-object v3, p0, LX/2pM;->c:LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->m()LX/2qY;

    move-result-object v3

    invoke-virtual {v3}, LX/2qY;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 468038
    const v1, 0x7f080dab

    :goto_0
    move v1, v1

    .line 468039
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 468040
    iget-object v0, p0, LX/2pM;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "1288475134498479"

    .line 468041
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 468042
    move-object v0, v0

    .line 468043
    invoke-virtual {v0}, LX/0gt;->b()V

    .line 468044
    :cond_0
    iget-object v0, p0, LX/2pM;->n:LX/0Uh;

    const/16 v1, 0xe7

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2pM;->s:Z

    .line 468045
    return-void

    .line 468046
    :pswitch_0
    const v1, 0x7f080dad

    goto :goto_0

    .line 468047
    :pswitch_1
    const v1, 0x7f080dac

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getInvisibleConst()I
    .locals 1

    .prologue
    .line 468048
    iget-boolean v0, p0, LX/2pM;->s:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    .line 468049
    if-eqz p2, :cond_0

    .line 468050
    sget-object v0, LX/2pO;->DEFAULT:LX/2pO;

    iput-object v0, p0, LX/2pM;->t:LX/2pO;

    .line 468051
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, LX/2pM;->c:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 468052
    iget-object v0, p0, LX/2pM;->e:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p0, LX/2pM;->p:LX/2pN;

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 468053
    :cond_1
    if-eqz p1, :cond_4

    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_4

    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_4

    .line 468054
    const v0, 0x7f020d52

    .line 468055
    :goto_0
    if-eqz p1, :cond_3

    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/2pM;->c:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 468056
    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, LX/2pM;->v:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 468057
    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v1, p0, LX/2pM;->v:Ljava/lang/String;

    .line 468058
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2pM;->w:Z

    .line 468059
    :cond_2
    iget-object v1, p0, LX/2pM;->d:LX/19w;

    iget-object v2, p0, LX/2pM;->v:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v1

    iget-object v1, v1, LX/2fs;->c:LX/1A0;

    iput-object v1, p0, LX/2pM;->u:LX/1A0;

    .line 468060
    :cond_3
    invoke-virtual {p0}, LX/2pM;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 468061
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_5

    .line 468062
    iget-object v1, p0, LX/2pM;->r:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 468063
    :goto_1
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 468064
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 468065
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 468066
    invoke-virtual {p0, v0}, LX/2pM;->a(LX/2qV;)V

    .line 468067
    return-void

    .line 468068
    :cond_4
    const v0, 0x7f0214b3

    goto :goto_0

    .line 468069
    :cond_5
    iget-object v1, p0, LX/2pM;->r:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public final a(LX/2qV;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 468070
    iget-object v1, p0, LX/2pM;->a:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 468071
    invoke-virtual {p0}, LX/2pM;->g()V

    .line 468072
    :goto_0
    return-void

    .line 468073
    :cond_0
    sget-object v1, LX/2qX;->a:[I

    iget-object v2, p0, LX/2pM;->t:LX/2pO;

    invoke-virtual {v2}, LX/2pO;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 468074
    :pswitch_0
    invoke-virtual {p1}, LX/2qV;->isPlayingState()Z

    move-result v1

    if-nez v1, :cond_2

    .line 468075
    iget-object v1, p0, LX/2pM;->r:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 468076
    iget-object v1, p0, LX/2pM;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 468077
    iget-object v2, p0, LX/2pM;->c:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->f()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/2pM;->d:LX/19w;

    iget-object p1, p0, LX/2pM;->v:Ljava/lang/String;

    invoke-virtual {v2, p1}, LX/19w;->f(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/2pM;->u:LX/1A0;

    sget-object p1, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne v2, p1, :cond_3

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 468078
    if-eqz v2, :cond_1

    :goto_2
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, LX/2pM;->getInvisibleConst()I

    move-result v0

    goto :goto_2

    .line 468079
    :cond_2
    :pswitch_1
    iget-object v0, p0, LX/2pM;->r:Landroid/view/View;

    invoke-direct {p0}, LX/2pM;->getInvisibleConst()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 468080
    iget-object v0, p0, LX/2pM;->q:Lcom/facebook/widget/text/BetterTextView;

    invoke-direct {p0}, LX/2pM;->getInvisibleConst()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 468081
    iget-object v0, p0, LX/2pM;->c:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468082
    iget-object v0, p0, LX/2pM;->e:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p0, LX/2pM;->p:LX/2pN;

    invoke-virtual {v0, v1, v2}, LX/16V;->b(Ljava/lang/Class;LX/16Y;)V

    .line 468083
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 468084
    iget-object v0, p0, LX/2pM;->r:Landroid/view/View;

    invoke-direct {p0}, LX/2pM;->getInvisibleConst()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 468085
    iget-object v0, p0, LX/2pM;->q:Lcom/facebook/widget/text/BetterTextView;

    invoke-direct {p0}, LX/2pM;->getInvisibleConst()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 468086
    return-void
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 468087
    iget-boolean v0, p0, LX/2pM;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2pM;->c:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468088
    iget-object v0, p0, LX/2pM;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "1288475134498479"

    .line 468089
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 468090
    move-object v0, v0

    .line 468091
    iget-object v1, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->b(Landroid/content/Context;)V

    .line 468092
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2pM;->w:Z

    .line 468093
    :cond_0
    return-void
.end method
