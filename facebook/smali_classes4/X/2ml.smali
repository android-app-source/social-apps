.class public LX/2ml;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/2ml;


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:I

.field public final d:J

.field public final e:J

.field public final f:J

.field public final g:I

.field public final h:I

.field public final i:J

.field public final j:Z


# direct methods
.method public constructor <init>(LX/0ad;LX/0Uh;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 461941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 461942
    sget-short v0, LX/2mm;->b:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2ml;->a:Z

    .line 461943
    sget v0, LX/2mm;->a:I

    const/16 v1, 0xc8

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/2ml;->b:I

    .line 461944
    sget v0, LX/2mm;->g:I

    const/16 v1, 0xa

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/2ml;->c:I

    .line 461945
    sget v0, LX/2mm;->d:I

    const/16 v1, 0xbb8

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/2ml;->d:J

    .line 461946
    sget v0, LX/2mm;->e:I

    const/16 v1, 0x1388

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/2ml;->e:J

    .line 461947
    sget v0, LX/2mm;->f:I

    const/16 v1, 0x1f40

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/2ml;->f:J

    .line 461948
    sget v0, LX/2mm;->h:I

    const/16 v1, 0x3e80

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/2ml;->g:I

    .line 461949
    sget v0, LX/2mm;->i:I

    const/16 v1, 0x2710

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/2ml;->h:I

    .line 461950
    sget v0, LX/2mm;->c:I

    const v1, 0x1d4c0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/2ml;->i:J

    .line 461951
    const/16 v0, 0x316

    invoke-virtual {p2, v0, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2ml;->j:Z

    .line 461952
    return-void
.end method

.method public static a(LX/0QB;)LX/2ml;
    .locals 5

    .prologue
    .line 461953
    sget-object v0, LX/2ml;->k:LX/2ml;

    if-nez v0, :cond_1

    .line 461954
    const-class v1, LX/2ml;

    monitor-enter v1

    .line 461955
    :try_start_0
    sget-object v0, LX/2ml;->k:LX/2ml;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 461956
    if-eqz v2, :cond_0

    .line 461957
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 461958
    new-instance p0, LX/2ml;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/2ml;-><init>(LX/0ad;LX/0Uh;)V

    .line 461959
    move-object v0, p0

    .line 461960
    sput-object v0, LX/2ml;->k:LX/2ml;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 461961
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 461962
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 461963
    :cond_1
    sget-object v0, LX/2ml;->k:LX/2ml;

    return-object v0

    .line 461964
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 461965
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 461966
    iget-boolean v0, p0, LX/2ml;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "commercial_break/"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "video_broadcast/commercial_break_"

    goto :goto_0
.end method
