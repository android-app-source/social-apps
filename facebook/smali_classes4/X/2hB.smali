.class public final enum LX/2hB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2hB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2hB;

.field public static final enum ENTITY_CARDS:LX/2hB;

.field public static final enum FRIENDING_RADAR:LX/2hB;

.field public static final enum FRIENDS_CENTER_FRIENDS:LX/2hB;

.field public static final enum FRIENDS_CENTER_REQUESTS:LX/2hB;

.field public static final enum FRIENDS_CENTER_REQUESTS_PYMK:LX/2hB;

.field public static final enum FRIENDS_CENTER_SEARCH:LX/2hB;

.field public static final enum FRIENDS_CENTER_SUGGESTIONS:LX/2hB;

.field public static final enum FRIENDS_TAB:LX/2hB;

.field public static final enum NEARBY_FRIENDS:LX/2hB;

.field public static final enum PROFILE_BROWSER:LX/2hB;

.field public static final enum PROFILE_BUTTON:LX/2hB;

.field public static final enum PYMK_FEED:LX/2hB;

.field public static final enum QR_CODE:LX/2hB;

.field public static final enum TIMELINE_FRIENDS_COLLECTION:LX/2hB;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 449861
    new-instance v0, LX/2hB;

    const-string v1, "ENTITY_CARDS"

    const-string v2, "entity_cards"

    invoke-direct {v0, v1, v4, v2}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->ENTITY_CARDS:LX/2hB;

    .line 449862
    new-instance v0, LX/2hB;

    const-string v1, "FRIENDS_TAB"

    const-string v2, "bd_friends_tab"

    invoke-direct {v0, v1, v5, v2}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->FRIENDS_TAB:LX/2hB;

    .line 449863
    new-instance v0, LX/2hB;

    const-string v1, "FRIENDING_RADAR"

    const-string v2, "friending_radar"

    invoke-direct {v0, v1, v6, v2}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->FRIENDING_RADAR:LX/2hB;

    .line 449864
    new-instance v0, LX/2hB;

    const-string v1, "FRIENDS_CENTER_FRIENDS"

    const-string v2, "bd_fc_friends"

    invoke-direct {v0, v1, v7, v2}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->FRIENDS_CENTER_FRIENDS:LX/2hB;

    .line 449865
    new-instance v0, LX/2hB;

    const-string v1, "FRIENDS_CENTER_REQUESTS"

    const-string v2, "button_dropdown"

    invoke-direct {v0, v1, v8, v2}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->FRIENDS_CENTER_REQUESTS:LX/2hB;

    .line 449866
    new-instance v0, LX/2hB;

    const-string v1, "FRIENDS_CENTER_REQUESTS_PYMK"

    const/4 v2, 0x5

    const-string v3, "bd_fc_requests"

    invoke-direct {v0, v1, v2, v3}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->FRIENDS_CENTER_REQUESTS_PYMK:LX/2hB;

    .line 449867
    new-instance v0, LX/2hB;

    const-string v1, "FRIENDS_CENTER_SEARCH"

    const/4 v2, 0x6

    const-string v3, "bd_fc_search"

    invoke-direct {v0, v1, v2, v3}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->FRIENDS_CENTER_SEARCH:LX/2hB;

    .line 449868
    new-instance v0, LX/2hB;

    const-string v1, "FRIENDS_CENTER_SUGGESTIONS"

    const/4 v2, 0x7

    const-string v3, "bd_fc_suggestions"

    invoke-direct {v0, v1, v2, v3}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->FRIENDS_CENTER_SUGGESTIONS:LX/2hB;

    .line 449869
    new-instance v0, LX/2hB;

    const-string v1, "NEARBY_FRIENDS"

    const/16 v2, 0x8

    const-string v3, "nearby_friends"

    invoke-direct {v0, v1, v2, v3}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->NEARBY_FRIENDS:LX/2hB;

    .line 449870
    new-instance v0, LX/2hB;

    const-string v1, "PROFILE_BUTTON"

    const/16 v2, 0x9

    const-string v3, "bd_profile_button"

    invoke-direct {v0, v1, v2, v3}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->PROFILE_BUTTON:LX/2hB;

    .line 449871
    new-instance v0, LX/2hB;

    const-string v1, "PROFILE_BROWSER"

    const/16 v2, 0xa

    const-string v3, "bd_profile_browser"

    invoke-direct {v0, v1, v2, v3}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->PROFILE_BROWSER:LX/2hB;

    .line 449872
    new-instance v0, LX/2hB;

    const-string v1, "PYMK_FEED"

    const/16 v2, 0xb

    const-string v3, "pymk_feed"

    invoke-direct {v0, v1, v2, v3}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->PYMK_FEED:LX/2hB;

    .line 449873
    new-instance v0, LX/2hB;

    const-string v1, "QR_CODE"

    const/16 v2, 0xc

    const-string v3, "qr_code"

    invoke-direct {v0, v1, v2, v3}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->QR_CODE:LX/2hB;

    .line 449874
    new-instance v0, LX/2hB;

    const-string v1, "TIMELINE_FRIENDS_COLLECTION"

    const/16 v2, 0xd

    const-string v3, "timeline_friends_collection"

    invoke-direct {v0, v1, v2, v3}, LX/2hB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hB;->TIMELINE_FRIENDS_COLLECTION:LX/2hB;

    .line 449875
    const/16 v0, 0xe

    new-array v0, v0, [LX/2hB;

    sget-object v1, LX/2hB;->ENTITY_CARDS:LX/2hB;

    aput-object v1, v0, v4

    sget-object v1, LX/2hB;->FRIENDS_TAB:LX/2hB;

    aput-object v1, v0, v5

    sget-object v1, LX/2hB;->FRIENDING_RADAR:LX/2hB;

    aput-object v1, v0, v6

    sget-object v1, LX/2hB;->FRIENDS_CENTER_FRIENDS:LX/2hB;

    aput-object v1, v0, v7

    sget-object v1, LX/2hB;->FRIENDS_CENTER_REQUESTS:LX/2hB;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/2hB;->FRIENDS_CENTER_REQUESTS_PYMK:LX/2hB;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2hB;->FRIENDS_CENTER_SEARCH:LX/2hB;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2hB;->FRIENDS_CENTER_SUGGESTIONS:LX/2hB;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2hB;->NEARBY_FRIENDS:LX/2hB;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2hB;->PROFILE_BUTTON:LX/2hB;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/2hB;->PROFILE_BROWSER:LX/2hB;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/2hB;->PYMK_FEED:LX/2hB;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/2hB;->QR_CODE:LX/2hB;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/2hB;->TIMELINE_FRIENDS_COLLECTION:LX/2hB;

    aput-object v2, v0, v1

    sput-object v0, LX/2hB;->$VALUES:[LX/2hB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 449876
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 449877
    iput-object p3, p0, LX/2hB;->value:Ljava/lang/String;

    .line 449878
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2hB;
    .locals 1

    .prologue
    .line 449879
    const-class v0, LX/2hB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2hB;

    return-object v0
.end method

.method public static values()[LX/2hB;
    .locals 1

    .prologue
    .line 449880
    sget-object v0, LX/2hB;->$VALUES:[LX/2hB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2hB;

    return-object v0
.end method
