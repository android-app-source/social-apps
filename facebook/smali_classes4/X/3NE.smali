.class public LX/3NE;
.super LX/3Ml;
.source ""


# instance fields
.field private final c:LX/3NF;

.field private final d:LX/2Og;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/3Mq;

.field private g:I


# direct methods
.method public constructor <init>(LX/0Zr;LX/3NF;LX/2Og;LX/0Or;LX/3Mq;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zr;",
            "LX/3NF;",
            "LX/2Og;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/3Mq;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558607
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 558608
    const/4 v0, -0x1

    iput v0, p0, LX/3NE;->g:I

    .line 558609
    iput-object p2, p0, LX/3NE;->c:LX/3NF;

    .line 558610
    iput-object p3, p0, LX/3NE;->d:LX/2Og;

    .line 558611
    iput-object p4, p0, LX/3NE;->e:LX/0Or;

    .line 558612
    iput-object p5, p0, LX/3NE;->f:LX/3Mq;

    .line 558613
    return-void
.end method

.method public static a(LX/0QB;)LX/3NE;
    .locals 1

    .prologue
    .line 558614
    invoke-static {p0}, LX/3NE;->b(LX/0QB;)LX/3NE;

    move-result-object v0

    return-object v0
.end method

.method private b(LX/0Px;)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 558615
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 558616
    :cond_0
    :goto_0
    return-object p1

    .line 558617
    :cond_1
    iget-object v0, p0, LX/3NE;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 558618
    if-eqz v0, :cond_0

    .line 558619
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v4

    .line 558620
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v5

    .line 558621
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_3

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 558622
    iget-object v7, v1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v7, v7

    .line 558623
    invoke-virtual {v7}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 558624
    iget-object v7, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v7, v7

    .line 558625
    invoke-virtual {v7}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v7

    .line 558626
    iget-object v8, p0, LX/3NE;->d:LX/2Og;

    invoke-virtual {v8, v7}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v8

    if-nez v8, :cond_2

    .line 558627
    invoke-interface {v5, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 558628
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 558629
    :cond_2
    iget-object v7, v1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v1, v7

    .line 558630
    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 558631
    :cond_3
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 558632
    iget-object v0, p0, LX/3NE;->f:LX/3Mq;

    invoke-virtual {v0, v5}, LX/3Mq;->a(Ljava/util/Set;)LX/6dO;

    move-result-object v1

    .line 558633
    :goto_3
    :try_start_0
    invoke-virtual {v1}, LX/6dO;->b()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 558634
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v6, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 558635
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/6dO;->close()V

    throw v0

    :cond_4
    invoke-virtual {v1}, LX/6dO;->close()V

    .line 558636
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 558637
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 558638
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 558639
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_4
    if-ge v1, v6, :cond_6

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 558640
    iget-object v2, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 558641
    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 558642
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 558643
    :goto_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 558644
    :cond_5
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 558645
    :cond_6
    invoke-interface {v3, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 558646
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p1

    goto/16 :goto_0
.end method

.method public static b(LX/0QB;)LX/3NE;
    .locals 6

    .prologue
    .line 558647
    new-instance v0, LX/3NE;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v1

    check-cast v1, LX/0Zr;

    invoke-static {p0}, LX/3NF;->b(LX/0QB;)LX/3NF;

    move-result-object v2

    check-cast v2, LX/3NF;

    invoke-static {p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v3

    check-cast v3, LX/2Og;

    const/16 v4, 0x12cb

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/3Mq;->a(LX/0QB;)LX/3Mq;

    move-result-object v5

    check-cast v5, LX/3Mq;

    invoke-direct/range {v0 .. v5}, LX/3NE;-><init>(LX/0Zr;LX/3NF;LX/2Og;LX/0Or;LX/3Mq;)V

    .line 558648
    return-object v0
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 11
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 558649
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 558650
    :goto_0
    new-instance v2, LX/39y;

    invoke-direct {v2}, LX/39y;-><init>()V

    .line 558651
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 558652
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    .line 558653
    iput v4, v2, LX/39y;->b:I

    move-object v0, v2

    .line 558654
    :goto_1
    return-object v0

    .line 558655
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 558656
    :cond_1
    iget v1, p0, LX/3NE;->g:I

    if-eq v1, v4, :cond_4

    iget v1, p0, LX/3NE;->g:I

    .line 558657
    :goto_2
    :try_start_0
    iget-object v4, p0, LX/3NE;->c:LX/3NF;

    .line 558658
    invoke-static {v4, v0, v1}, LX/3NF;->c(LX/3NF;Ljava/lang/String;I)LX/0Px;

    move-result-object v7

    .line 558659
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 558660
    if-eqz v7, :cond_2

    .line 558661
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v9

    const/4 v5, 0x0

    move v6, v5

    :goto_3
    if-ge v6, v9, :cond_2

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel$SearchResultsModel$EdgesModel;

    .line 558662
    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel$SearchResultsModel$EdgesModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    move-result-object v5

    invoke-static {v5}, LX/3NG;->a(LX/5ZD;)Lcom/facebook/user/model/User;

    move-result-object v5

    invoke-virtual {v8, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 558663
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_3

    .line 558664
    :cond_2
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v0, v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 558665
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 558666
    invoke-virtual {v0}, LX/0Px;->size()I

    .line 558667
    invoke-direct {p0, v0}, LX/3NE;->b(LX/0Px;)LX/0Px;

    move-result-object v5

    .line 558668
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v3

    :goto_4
    if-ge v1, v6, :cond_5

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 558669
    iget-object v3, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v3, v0}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v0

    .line 558670
    if-eqz v0, :cond_3

    .line 558671
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 558672
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 558673
    :cond_4
    const/4 v1, 0x6

    goto :goto_2

    .line 558674
    :catch_0
    move-exception v0

    .line 558675
    const-string v1, "ContactPickerServerCommercePageFilter"

    const-string v4, "exception with filtering commerce pages"

    invoke-static {v1, v4, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 558676
    iput v3, v2, LX/39y;->b:I

    .line 558677
    invoke-static {p1}, LX/3Og;->b(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v2

    .line 558678
    goto :goto_1

    .line 558679
    :cond_5
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 558680
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    iput v1, v2, LX/39y;->b:I

    .line 558681
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v2

    .line 558682
    goto/16 :goto_1
.end method
