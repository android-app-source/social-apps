.class public abstract LX/35n;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/35o;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final c:Landroid/widget/TextView;

.field public d:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:Landroid/widget/TextView;

.field public final f:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

.field private final g:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 497661
    const-class v0, LX/35n;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/35n;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 497647
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 497648
    invoke-virtual {p0, p4}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 497649
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/35n;->setOrientation(I)V

    .line 497650
    const-class v0, LX/35n;

    invoke-static {v0, p0}, LX/35n;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 497651
    const v0, 0x7f0d052b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/35n;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 497652
    const v0, 0x7f0d052c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/35n;->c:Landroid/widget/TextView;

    .line 497653
    const v0, 0x7f0d052d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/35n;->e:Landroid/widget/TextView;

    .line 497654
    const v0, 0x7f0d0530

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, LX/35n;->g:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 497655
    const v0, 0x7f0d052e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    iput-object v0, p0, LX/35n;->f:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    .line 497656
    const-string v1, "newsfeed_angora_attachment_view"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/Class;)V

    .line 497657
    sget-object v0, LX/1vY;->ATTACHMENT:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 497658
    iget-object v0, p0, LX/35n;->c:Landroid/widget/TextView;

    sget-object v1, LX/1vY;->TITLE:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 497659
    iget-object v0, p0, LX/35n;->e:Landroid/widget/TextView;

    sget-object v1, LX/1vY;->SOCIAL_CONTEXT:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 497660
    return-void
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 497643
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 497644
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 497645
    return-void

    .line 497646
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/35n;

    invoke-static {p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object p0

    check-cast p0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p0, p1, LX/35n;->d:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 497641
    iget-object v0, p0, LX/35n;->f:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    .line 497642
    return-void
.end method

.method public getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;
    .locals 1

    .prologue
    .line 497640
    iget-object v0, p0, LX/35n;->f:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    return-object v0
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 497638
    iget-object v0, p0, LX/35n;->f:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    invoke-virtual {v0, p1}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 497639
    return-void
.end method

.method public setContextText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 497636
    iget-object v0, p0, LX/35n;->e:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/35n;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 497637
    return-void
.end method

.method public setSideImageController(LX/1aZ;)V
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 497631
    iget-object v0, p0, LX/35n;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 497632
    if-eqz p1, :cond_0

    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v0, p0}, Lcom/facebook/drawee/view/DraweeView;->setVisibility(I)V

    .line 497633
    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 497634
    return-void

    .line 497635
    :cond_0
    const/16 p0, 0x8

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 497629
    iget-object v0, p0, LX/35n;->c:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/35n;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 497630
    return-void
.end method
