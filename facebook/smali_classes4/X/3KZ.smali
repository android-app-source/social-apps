.class public final LX/3KZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/27U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/27U",
        "<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4v0;

.field public final synthetic b:Z

.field public final synthetic c:LX/2wX;

.field public final synthetic d:LX/2wW;


# direct methods
.method public constructor <init>(LX/2wW;LX/4v0;ZLX/2wX;)V
    .locals 0

    iput-object p1, p0, LX/3KZ;->d:LX/2wW;

    iput-object p2, p0, LX/3KZ;->a:LX/4v0;

    iput-boolean p3, p0, LX/3KZ;->b:Z

    iput-object p4, p0, LX/3KZ;->c:LX/2wX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2NW;)V
    .locals 1
    .param p1    # LX/2NW;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lcom/google/android/gms/common/api/Status;

    iget-object v0, p0, LX/3KZ;->d:LX/2wW;

    iget-object v0, v0, LX/2wW;->n:Landroid/content/Context;

    invoke-static {v0}, LX/4sO;->a(Landroid/content/Context;)LX/4sO;

    move-result-object v0

    invoke-virtual {v0}, LX/4sO;->c()V

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3KZ;->d:LX/2wW;

    invoke-virtual {v0}, LX/2wX;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3KZ;->d:LX/2wW;

    invoke-virtual {v0}, LX/2wX;->g()V

    invoke-virtual {v0}, LX/2wX;->e()V

    :cond_0
    iget-object v0, p0, LX/3KZ;->a:LX/4v0;

    invoke-virtual {v0, p1}, LX/2wf;->a(LX/2NW;)V

    iget-boolean v0, p0, LX/3KZ;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3KZ;->c:LX/2wX;

    invoke-virtual {v0}, LX/2wX;->g()V

    :cond_1
    return-void
.end method
