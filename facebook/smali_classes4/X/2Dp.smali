.class public LX/2Dp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2Dp;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384698
    iput-object p1, p0, LX/2Dp;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 384699
    return-void
.end method

.method public static a(LX/0QB;)LX/2Dp;
    .locals 4

    .prologue
    .line 384684
    sget-object v0, LX/2Dp;->b:LX/2Dp;

    if-nez v0, :cond_1

    .line 384685
    const-class v1, LX/2Dp;

    monitor-enter v1

    .line 384686
    :try_start_0
    sget-object v0, LX/2Dp;->b:LX/2Dp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 384687
    if-eqz v2, :cond_0

    .line 384688
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 384689
    new-instance p0, LX/2Dp;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/2Dp;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 384690
    move-object v0, p0

    .line 384691
    sput-object v0, LX/2Dp;->b:LX/2Dp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384692
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 384693
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 384694
    :cond_1
    sget-object v0, LX/2Dp;->b:LX/2Dp;

    return-object v0

    .line 384695
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 384696
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 384682
    iget-object v0, p0, LX/2Dp;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/4oz;->d:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/4oz;->e:LX/0Tn;

    invoke-interface {v0, v1, p2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/4oz;->f:LX/0Tn;

    invoke-interface {v0, v1, p3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 384683
    return-void
.end method
