.class public final LX/2ig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private final a:LX/0fx;

.field private final b:LX/2iI;


# direct methods
.method public constructor <init>(LX/0fx;LX/2iI;)V
    .locals 0

    .prologue
    .line 451942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 451943
    iput-object p1, p0, LX/2ig;->a:LX/0fx;

    .line 451944
    iput-object p2, p0, LX/2ig;->b:LX/2iI;

    .line 451945
    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 451940
    iget-object v0, p0, LX/2ig;->a:LX/0fx;

    iget-object v1, p0, LX/2ig;->b:LX/2iI;

    invoke-interface {v0, v1, p2, p3, p4}, LX/0fx;->a(LX/0g8;III)V

    .line 451941
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 451938
    iget-object v0, p0, LX/2ig;->a:LX/0fx;

    iget-object v1, p0, LX/2ig;->b:LX/2iI;

    invoke-interface {v0, v1, p2}, LX/0fx;->a(LX/0g8;I)V

    .line 451939
    return-void
.end method
