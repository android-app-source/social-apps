.class public final LX/2K8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:LX/15D;

.field public final synthetic c:LX/1hj;


# direct methods
.method public constructor <init>(LX/1hj;Lcom/google/common/util/concurrent/SettableFuture;LX/15D;)V
    .locals 0

    .prologue
    .line 393471
    iput-object p1, p0, LX/2K8;->c:LX/1hj;

    iput-object p2, p0, LX/2K8;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p3, p0, LX/2K8;->b:LX/15D;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 393472
    sget-object v0, LX/1hj;->a:Ljava/lang/Class;

    const-string v1, "Making fallback request for \'%s\'"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/2K8;->b:LX/15D;

    .line 393473
    iget-object v5, v4, LX/15D;->c:Ljava/lang/String;

    move-object v4, v5

    .line 393474
    aput-object v4, v2, v3

    invoke-static {v0, p1, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 393475
    iget-object v0, p0, LX/2K8;->c:LX/1hj;

    iget-object v0, v0, LX/1hj;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4bO;

    iget-object v1, p0, LX/2K8;->b:LX/15D;

    invoke-virtual {v0, v1}, LX/4bO;->a(LX/15D;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 393476
    new-instance v1, LX/4be;

    invoke-direct {v1, p0}, LX/4be;-><init>(LX/2K8;)V

    .line 393477
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 393478
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 393479
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 393480
    iget-object v0, p0, LX/2K8;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x6217fbf3

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 393481
    return-void
.end method
