.class public final LX/2wB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Collection;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Collection",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/118;


# direct methods
.method public constructor <init>(LX/118;)V
    .locals 0

    .prologue
    .line 478692
    iput-object p1, p0, LX/2wB;->a:LX/118;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 478655
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 478691
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 478689
    iget-object v0, p0, LX/2wB;->a:LX/118;

    invoke-virtual {v0}, LX/118;->c()V

    .line 478690
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 478688
    iget-object v0, p0, LX/2wB;->a:LX/118;

    invoke-virtual {v0, p1}, LX/118;->b(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 478683
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 478684
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 478685
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/2wB;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 478686
    const/4 v0, 0x0

    .line 478687
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 478682
    iget-object v0, p0, LX/2wB;->a:LX/118;

    invoke-virtual {v0}, LX/118;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 478693
    new-instance v0, LX/2wC;

    iget-object v1, p0, LX/2wB;->a:LX/118;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/2wC;-><init>(LX/118;I)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 478677
    iget-object v0, p0, LX/2wB;->a:LX/118;

    invoke-virtual {v0, p1}, LX/118;->b(Ljava/lang/Object;)I

    move-result v0

    .line 478678
    if-ltz v0, :cond_0

    .line 478679
    iget-object v1, p0, LX/2wB;->a:LX/118;

    invoke-virtual {v1, v0}, LX/118;->a(I)V

    .line 478680
    const/4 v0, 0x1

    .line 478681
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 478668
    iget-object v1, p0, LX/2wB;->a:LX/118;

    invoke-virtual {v1}, LX/118;->a()I

    move-result v3

    move v1, v0

    .line 478669
    :goto_0
    if-ge v0, v3, :cond_1

    .line 478670
    iget-object v4, p0, LX/2wB;->a:LX/118;

    invoke-virtual {v4, v0, v2}, LX/118;->a(II)Ljava/lang/Object;

    move-result-object v4

    .line 478671
    invoke-interface {p1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 478672
    iget-object v1, p0, LX/2wB;->a:LX/118;

    invoke-virtual {v1, v0}, LX/118;->a(I)V

    .line 478673
    add-int/lit8 v0, v0, -0x1

    .line 478674
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    move v1, v2

    .line 478675
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 478676
    :cond_1
    return v1
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 478659
    iget-object v1, p0, LX/2wB;->a:LX/118;

    invoke-virtual {v1}, LX/118;->a()I

    move-result v3

    move v1, v0

    .line 478660
    :goto_0
    if-ge v0, v3, :cond_1

    .line 478661
    iget-object v4, p0, LX/2wB;->a:LX/118;

    invoke-virtual {v4, v0, v2}, LX/118;->a(II)Ljava/lang/Object;

    move-result-object v4

    .line 478662
    invoke-interface {p1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 478663
    iget-object v1, p0, LX/2wB;->a:LX/118;

    invoke-virtual {v1, v0}, LX/118;->a(I)V

    .line 478664
    add-int/lit8 v0, v0, -0x1

    .line 478665
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    move v1, v2

    .line 478666
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 478667
    :cond_1
    return v1
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 478658
    iget-object v0, p0, LX/2wB;->a:LX/118;

    invoke-virtual {v0}, LX/118;->a()I

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 478657
    iget-object v0, p0, LX/2wB;->a:LX/118;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/118;->b(I)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 478656
    iget-object v0, p0, LX/2wB;->a:LX/118;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LX/118;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
