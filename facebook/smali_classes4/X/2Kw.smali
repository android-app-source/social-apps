.class public LX/2Kw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static volatile j:LX/2Kw;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:Ljava/util/concurrent/ScheduledExecutorService;

.field public final e:LX/2Kx;

.field private f:Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;

.field public g:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public final h:LX/0So;

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 394088
    sget-object v0, LX/2Kx;->a:LX/0Tn;

    const-string v1, "next/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Kw;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Kx;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 394098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394099
    iput-object p3, p0, LX/2Kw;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 394100
    iput-object p4, p0, LX/2Kw;->h:LX/0So;

    .line 394101
    iput-object p5, p0, LX/2Kw;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 394102
    iput-object p1, p0, LX/2Kw;->b:Landroid/content/Context;

    .line 394103
    iput-object p2, p0, LX/2Kw;->c:Lcom/facebook/content/SecureContextHelper;

    .line 394104
    iput-object p6, p0, LX/2Kw;->e:LX/2Kx;

    .line 394105
    return-void
.end method

.method public static a(LX/0QB;)LX/2Kw;
    .locals 10

    .prologue
    .line 394106
    sget-object v0, LX/2Kw;->j:LX/2Kw;

    if-nez v0, :cond_1

    .line 394107
    const-class v1, LX/2Kw;

    monitor-enter v1

    .line 394108
    :try_start_0
    sget-object v0, LX/2Kw;->j:LX/2Kw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 394109
    if-eqz v2, :cond_0

    .line 394110
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 394111
    new-instance v3, LX/2Kw;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Kx;->a(LX/0QB;)LX/2Kx;

    move-result-object v9

    check-cast v9, LX/2Kx;

    invoke-direct/range {v3 .. v9}, LX/2Kw;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Kx;)V

    .line 394112
    move-object v0, v3

    .line 394113
    sput-object v0, LX/2Kw;->j:LX/2Kw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394114
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 394115
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 394116
    :cond_1
    sget-object v0, LX/2Kw;->j:LX/2Kw;

    return-object v0

    .line 394117
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 394118
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 7

    .prologue
    .line 394089
    :try_start_0
    iget-object v0, p0, LX/2Kw;->e:LX/2Kx;

    invoke-virtual {v0}, LX/2Kx;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394090
    new-instance v0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;-><init>(LX/2Kw;)V

    iput-object v0, p0, LX/2Kw;->f:Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;

    .line 394091
    iget-object v0, p0, LX/2Kw;->h:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 394092
    iget-object v2, p0, LX/2Kw;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2Kw;->a:LX/0Tn;

    invoke-interface {v2, v3, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 394093
    const-wide/32 v4, 0xea60

    sub-long v0, v2, v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 394094
    iget-object v0, p0, LX/2Kw;->d:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/2Kw;->f:Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;

    const-wide/32 v4, 0x5265c00

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/2Kw;->g:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 394095
    :cond_0
    return-void

    .line 394096
    :catch_0
    move-exception v0

    .line 394097
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
