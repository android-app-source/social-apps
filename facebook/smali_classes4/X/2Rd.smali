.class public LX/2Rd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/text/Collator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/common/locale/ForPrimaryCanonicalDecomposition;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/text/Collator;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409945
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409946
    const-string v0, "NameNormalizer::_construct"

    const v1, -0x6971aa0d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 409947
    :try_start_0
    iput-object p1, p0, LX/2Rd;->a:LX/0Or;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 409948
    const v0, 0x136065a

    invoke-static {v0}, LX/02m;->a(I)V

    .line 409949
    return-void

    .line 409950
    :catchall_0
    move-exception v0

    const v1, 0xad169a1

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static b(LX/0QB;)LX/2Rd;
    .locals 2

    .prologue
    .line 409951
    new-instance v0, LX/2Rd;

    const/16 v1, 0x1614

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2Rd;-><init>(LX/0Or;)V

    .line 409952
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 409953
    invoke-static {p1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 409954
    iget-object v0, p0, LX/2Rd;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/Collator;

    const/4 v3, 0x0

    .line 409955
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    new-array v5, v2, [I

    .line 409956
    invoke-static {v1}, LX/3hP;->a(Ljava/lang/String;)LX/3hP;

    move-result-object p0

    move v2, v3

    .line 409957
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/3hP;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 409958
    invoke-virtual {p0}, LX/3hP;->b()I

    move-result p1

    .line 409959
    invoke-static {p1}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 409960
    add-int/lit8 v4, v2, 0x1

    aput p1, v5, v2

    move v2, v4

    goto :goto_0

    .line 409961
    :cond_1
    array-length v4, v5

    if-eq v2, v4, :cond_2

    .line 409962
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v5, v3, v2}, Ljava/lang/String;-><init>([III)V

    .line 409963
    :cond_2
    move-object v1, v1

    .line 409964
    invoke-virtual {v0, v1}, Ljava/text/Collator;->getCollationKey(Ljava/lang/String;)Ljava/text/CollationKey;

    move-result-object v0

    .line 409965
    invoke-virtual {v0}, Ljava/text/CollationKey;->toByteArray()[B

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/1u4;->a([BZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
