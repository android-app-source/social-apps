.class public final LX/3M7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3M3;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 553493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get(CC)C
    .locals 7

    .prologue
    const/16 v2, 0x2f

    const/16 v4, 0x2e

    const/16 v3, 0x20

    const v6, 0xd800

    const/16 v1, 0x2d

    .line 553494
    sget-object v0, LX/3Lz;->ALPHA_MAPPINGS:LX/3M3;

    invoke-interface {v0, p1, v6}, LX/3M3;->get(CC)C

    move-result v0

    .line 553495
    if-eq v0, v6, :cond_0

    move p2, v0

    .line 553496
    :goto_0
    return p2

    .line 553497
    :cond_0
    sget-object v0, LX/3Lz;->ALPHA_MAPPINGS:LX/3M3;

    invoke-static {p1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v5

    invoke-interface {v0, v5, v6}, LX/3M3;->get(CC)C

    move-result v0

    .line 553498
    if-eq v0, v6, :cond_1

    move p2, v0

    .line 553499
    goto :goto_0

    .line 553500
    :cond_1
    sget-object v0, LX/3Lz;->ASCII_DIGIT_MAPPINGS:LX/3M3;

    invoke-interface {v0, p1, v6}, LX/3M3;->get(CC)C

    move-result v0

    .line 553501
    if-eq v0, v6, :cond_2

    move p2, v0

    .line 553502
    goto :goto_0

    .line 553503
    :cond_2
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    move p2, v3

    .line 553504
    goto :goto_0

    :sswitch_1
    move p2, v1

    .line 553505
    goto :goto_0

    :sswitch_2
    move p2, v1

    .line 553506
    goto :goto_0

    :sswitch_3
    move p2, v1

    .line 553507
    goto :goto_0

    :sswitch_4
    move p2, v1

    .line 553508
    goto :goto_0

    :sswitch_5
    move p2, v1

    .line 553509
    goto :goto_0

    :sswitch_6
    move p2, v1

    .line 553510
    goto :goto_0

    :sswitch_7
    move p2, v1

    .line 553511
    goto :goto_0

    :sswitch_8
    move p2, v1

    .line 553512
    goto :goto_0

    :sswitch_9
    move p2, v1

    .line 553513
    goto :goto_0

    :sswitch_a
    move p2, v2

    .line 553514
    goto :goto_0

    :sswitch_b
    move p2, v2

    .line 553515
    goto :goto_0

    :sswitch_c
    move p2, v3

    .line 553516
    goto :goto_0

    :sswitch_d
    move p2, v3

    .line 553517
    goto :goto_0

    :sswitch_e
    move p2, v4

    .line 553518
    goto :goto_0

    :sswitch_f
    move p2, v4

    .line 553519
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x2d -> :sswitch_1
        0x2e -> :sswitch_e
        0x2f -> :sswitch_a
        0x2010 -> :sswitch_3
        0x2011 -> :sswitch_4
        0x2012 -> :sswitch_5
        0x2013 -> :sswitch_6
        0x2014 -> :sswitch_7
        0x2015 -> :sswitch_8
        0x2060 -> :sswitch_d
        0x2212 -> :sswitch_9
        0x3000 -> :sswitch_c
        0xff0d -> :sswitch_2
        0xff0e -> :sswitch_f
        0xff0f -> :sswitch_b
    .end sparse-switch
.end method
