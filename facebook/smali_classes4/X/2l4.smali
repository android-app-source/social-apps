.class public final LX/2l4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2l5;


# direct methods
.method public constructor <init>(LX/2l5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 456767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456768
    iput-object p1, p0, LX/2l4;->a:LX/2l5;

    .line 456769
    return-void
.end method

.method public static c(Lcom/facebook/bookmark/model/Bookmark;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 456734
    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 456735
    iget-object v1, p0, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-static {v1}, LX/1fg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/EhD;->lookup(Ljava/lang/String;)LX/EhD;

    move-result-object v1

    .line 456736
    sget-object v2, LX/EhF;->b:[I

    invoke-virtual {v1}, LX/EhD;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 456737
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 456738
    :pswitch_0
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 456739
    if-eqz v0, :cond_0

    .line 456740
    const-string v0, "app_fb"

    goto :goto_0

    .line 456741
    :cond_0
    const-string v0, "app_noncanvas_3rdparty"

    goto :goto_0

    .line 456742
    :pswitch_1
    const-string v0, "newsfeed"

    goto :goto_0

    .line 456743
    :pswitch_2
    const-string v0, "self_timeline"

    goto :goto_0

    .line 456744
    :pswitch_3
    const-string v0, "group_user"

    goto :goto_0

    .line 456745
    :pswitch_4
    const-string v0, "friend_list"

    goto :goto_0

    .line 456746
    :pswitch_5
    const-string v0, "interest_list"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/bookmark/model/Bookmark;)Lcom/facebook/bookmark/model/BookmarksGroup;
    .locals 3

    .prologue
    .line 456763
    iget-object v0, p0, LX/2l4;->a:LX/2l5;

    invoke-interface {v0}, LX/2l5;->b()Ljava/util/List;

    move-result-object v0

    .line 456764
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 456765
    invoke-virtual {v0, p1}, Lcom/facebook/bookmark/model/BookmarksGroup;->b(Lcom/facebook/bookmark/model/Bookmark;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 456766
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/bookmark/model/Bookmark;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 456747
    invoke-virtual {p0, p1}, LX/2l4;->a(Lcom/facebook/bookmark/model/Bookmark;)Lcom/facebook/bookmark/model/BookmarksGroup;

    move-result-object v0

    .line 456748
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-static {v0}, LX/1fg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 456749
    :goto_0
    invoke-static {v0}, LX/EhC;->lookup(Ljava/lang/String;)LX/EhC;

    move-result-object v0

    .line 456750
    sget-object v1, LX/EhF;->a:[I

    invoke-virtual {v0}, LX/EhC;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 456751
    const-string v0, "unknown"

    :goto_1
    return-object v0

    .line 456752
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 456753
    :pswitch_0
    const-string v0, "self_timeline"

    goto :goto_1

    .line 456754
    :pswitch_1
    const-string v0, "user_favorite"

    goto :goto_1

    .line 456755
    :pswitch_2
    const-string v0, "ads_section"

    goto :goto_1

    .line 456756
    :pswitch_3
    const-string v0, "apps_section"

    goto :goto_1

    .line 456757
    :pswitch_4
    const-string v0, "groups_section"

    goto :goto_1

    .line 456758
    :pswitch_5
    const-string v0, "pages_section"

    goto :goto_1

    .line 456759
    :pswitch_6
    const-string v0, "developer_section"

    goto :goto_1

    .line 456760
    :pswitch_7
    const-string v0, "settings_section"

    goto :goto_1

    .line 456761
    :pswitch_8
    const-string v0, "interests_section"

    goto :goto_1

    .line 456762
    :pswitch_9
    const-string v0, "friends_section"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
