.class public LX/34a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static i:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/3AZ;

.field public final d:LX/03V;

.field public final e:LX/2g9;

.field public final f:LX/0if;

.field public final g:Lcom/facebook/content/SecureContextHelper;

.field public final h:LX/17Y;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 495317
    const-class v0, LX/34a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/34a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/3AZ;LX/03V;LX/2g9;LX/0if;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/3AZ;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2g9;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 495318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 495319
    iput-object p1, p0, LX/34a;->b:LX/0Or;

    .line 495320
    iput-object p2, p0, LX/34a;->c:LX/3AZ;

    .line 495321
    iput-object p3, p0, LX/34a;->d:LX/03V;

    .line 495322
    iput-object p4, p0, LX/34a;->e:LX/2g9;

    .line 495323
    iput-object p5, p0, LX/34a;->f:LX/0if;

    .line 495324
    iput-object p6, p0, LX/34a;->g:Lcom/facebook/content/SecureContextHelper;

    .line 495325
    iput-object p7, p0, LX/34a;->h:LX/17Y;

    .line 495326
    return-void
.end method

.method public static a(LX/0QB;)LX/34a;
    .locals 11

    .prologue
    .line 495327
    const-class v1, LX/34a;

    monitor-enter v1

    .line 495328
    :try_start_0
    sget-object v0, LX/34a;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 495329
    sput-object v2, LX/34a;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 495330
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495331
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 495332
    new-instance v3, LX/34a;

    const/16 v4, 0x1399

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/3AZ;->a(LX/0QB;)LX/3AZ;

    move-result-object v5

    check-cast v5, LX/3AZ;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v7

    check-cast v7, LX/2g9;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v8

    check-cast v8, LX/0if;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v10

    check-cast v10, LX/17Y;

    invoke-direct/range {v3 .. v10}, LX/34a;-><init>(LX/0Or;LX/3AZ;LX/03V;LX/2g9;LX/0if;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    .line 495333
    move-object v0, v3

    .line 495334
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 495335
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/34a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 495336
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 495337
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
