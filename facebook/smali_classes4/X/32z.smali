.class public final LX/32z;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/22P;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/32y;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 491485
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 491486
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "draweeController"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "actualImageFocusPoint"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "contentId"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/32z;->b:[Ljava/lang/String;

    .line 491487
    iput v3, p0, LX/32z;->c:I

    .line 491488
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/32z;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/32z;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/32z;LX/1De;IILX/32y;)V
    .locals 1

    .prologue
    .line 491489
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 491490
    iput-object p4, p0, LX/32z;->a:LX/32y;

    .line 491491
    iget-object v0, p0, LX/32z;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 491492
    return-void
.end method


# virtual methods
.method public final a(LX/1aZ;)LX/32z;
    .locals 2

    .prologue
    .line 491493
    iget-object v0, p0, LX/32z;->a:LX/32y;

    iput-object p1, v0, LX/32y;->a:LX/1aZ;

    .line 491494
    iget-object v0, p0, LX/32z;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 491495
    return-object p0
.end method

.method public final a(Landroid/graphics/PointF;)LX/32z;
    .locals 2

    .prologue
    .line 491496
    iget-object v0, p0, LX/32z;->a:LX/32y;

    iput-object p1, v0, LX/32y;->b:Landroid/graphics/PointF;

    .line 491497
    iget-object v0, p0, LX/32z;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 491498
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 491499
    invoke-super {p0}, LX/1X5;->a()V

    .line 491500
    const/4 v0, 0x0

    iput-object v0, p0, LX/32z;->a:LX/32y;

    .line 491501
    sget-object v0, LX/22P;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 491502
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/22P;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 491503
    iget-object v1, p0, LX/32z;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/32z;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/32z;->c:I

    if-ge v1, v2, :cond_2

    .line 491504
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 491505
    :goto_0
    iget v2, p0, LX/32z;->c:I

    if-ge v0, v2, :cond_1

    .line 491506
    iget-object v2, p0, LX/32z;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 491507
    iget-object v2, p0, LX/32z;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 491508
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 491509
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 491510
    :cond_2
    iget-object v0, p0, LX/32z;->a:LX/32y;

    .line 491511
    invoke-virtual {p0}, LX/32z;->a()V

    .line 491512
    return-object v0
.end method

.method public final h(I)LX/32z;
    .locals 2

    .prologue
    .line 491513
    iget-object v0, p0, LX/32z;->a:LX/32y;

    iput p1, v0, LX/32y;->c:I

    .line 491514
    iget-object v0, p0, LX/32z;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 491515
    return-object p0
.end method
