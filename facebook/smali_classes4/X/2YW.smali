.class public LX/2YW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0aG;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GQW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0aG;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0Or;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0aG;",
            "LX/0Ot",
            "<",
            "LX/GQW;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 421019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421020
    iput-object p1, p0, LX/2YW;->a:Landroid/content/Context;

    .line 421021
    iput-object p2, p0, LX/2YW;->b:LX/0aG;

    .line 421022
    iput-object p3, p0, LX/2YW;->e:LX/0Ot;

    .line 421023
    iput-object p4, p0, LX/2YW;->c:Ljava/util/concurrent/ExecutorService;

    .line 421024
    iput-object p5, p0, LX/2YW;->d:LX/0Or;

    .line 421025
    return-void
.end method

.method public static a(LX/2YW;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/aldrin/status/AldrinUserStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 421026
    iget-object v0, p0, LX/2YW;->b:LX/0aG;

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const v5, 0x3f941e8

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 421027
    new-instance v1, LX/2YX;

    invoke-direct {v1, p0}, LX/2YX;-><init>(LX/2YW;)V

    iget-object v2, p0, LX/2YW;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2YW;
    .locals 6

    .prologue
    .line 421028
    new-instance v0, LX/2YW;

    const-class v1, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v2

    check-cast v2, LX/0aG;

    const/16 v3, 0x1740

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    const/16 v5, 0x12cb

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/2YW;-><init>(Landroid/content/Context;LX/0aG;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0Or;)V

    .line 421029
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/aldrin/status/AldrinUserStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 421030
    iget-object v0, p0, LX/2YW;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 421031
    const-string v0, "fetch_aldrin_logged_out_status"

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {p0, v0, v1, p1}, LX/2YW;->a(LX/2YW;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 421032
    :goto_0
    return-object v0

    .line 421033
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 421034
    iget-object v0, p0, LX/2YW;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 421035
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 421036
    const-string v2, "user_id"

    .line 421037
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v3

    .line 421038
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 421039
    const-string v0, "fetch_region_tos_status"

    invoke-static {p0, v0, v1, p1}, LX/2YW;->a(LX/2YW;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 421040
    new-instance v1, LX/GQP;

    invoke-direct {v1, p0, p1}, LX/GQP;-><init>(LX/2YW;Lcom/facebook/common/callercontext/CallerContext;)V

    iget-object v2, p0, LX/2YW;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 421041
    move-object v0, v0

    .line 421042
    goto :goto_0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;LX/GQU;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;",
            "LX/GQU;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/aldrin/status/AldrinUserStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 421043
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 421044
    const-string v0, "region_code"

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 421045
    const-string v0, "response_action"

    invoke-virtual {p3}, LX/GQU;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 421046
    const-string v0, "response_version"

    invoke-virtual {v2, v0, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 421047
    iget-object v0, p0, LX/2YW;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 421048
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 421049
    const-string v1, "user_id"

    .line 421050
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v3

    .line 421051
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 421052
    iget-object v0, p0, LX/2YW;->b:LX/0aG;

    const-string v1, "respond_to_region_tos"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const v5, 0x4c8dc7a4

    move-object v4, p1

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 421053
    new-instance v1, LX/GQQ;

    invoke-direct {v1, p0}, LX/GQQ;-><init>(LX/2YW;)V

    iget-object v2, p0, LX/2YW;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
