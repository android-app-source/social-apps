.class public final LX/2QH;
.super LX/2QI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2QI",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1pz;


# direct methods
.method public constructor <init>(LX/1pz;)V
    .locals 0

    .prologue
    .line 407804
    iput-object p1, p0, LX/2QH;->a:LX/1pz;

    invoke-direct {p0}, LX/2QI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 407805
    iget-object v0, p0, LX/2QH;->a:LX/1pz;

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 407806
    iget-object v0, p0, LX/2QH;->a:LX/1pz;

    iget-object v0, v0, LX/1pz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, LX/0PN;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation

    .prologue
    .line 407807
    new-instance v0, LX/2QJ;

    iget-object v1, p0, LX/2QH;->a:LX/1pz;

    invoke-direct {v0, v1}, LX/2QJ;-><init>(LX/1pz;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 407808
    invoke-virtual {p0, p1}, LX/2QH;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 407809
    const/4 v0, 0x0

    .line 407810
    :goto_0
    return v0

    .line 407811
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 407812
    iget-object v0, p0, LX/2QH;->a:LX/1pz;

    iget-object v0, v0, LX/1pz;->b:LX/0Xs;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 407813
    iget-object p0, v0, LX/0Xs;->a:Ljava/util/Map;

    invoke-static {p0, v1}, LX/0PM;->c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Collection;

    .line 407814
    if-eqz p0, :cond_1

    .line 407815
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result p1

    .line 407816
    invoke-interface {p0}, Ljava/util/Collection;->clear()V

    .line 407817
    iget p0, v0, LX/0Xs;->b:I

    sub-int/2addr p0, p1

    iput p0, v0, LX/0Xs;->b:I

    .line 407818
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
