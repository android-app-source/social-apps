.class public LX/2U4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2U4;


# instance fields
.field public a:LX/0sa;

.field public b:LX/0ad;

.field private c:LX/0rq;


# direct methods
.method public constructor <init>(LX/0sa;LX/0ad;LX/0rq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415085
    iput-object p1, p0, LX/2U4;->a:LX/0sa;

    .line 415086
    iput-object p2, p0, LX/2U4;->b:LX/0ad;

    .line 415087
    iput-object p3, p0, LX/2U4;->c:LX/0rq;

    .line 415088
    return-void
.end method

.method public static a(LX/0QB;)LX/2U4;
    .locals 6

    .prologue
    .line 415089
    sget-object v0, LX/2U4;->d:LX/2U4;

    if-nez v0, :cond_1

    .line 415090
    const-class v1, LX/2U4;

    monitor-enter v1

    .line 415091
    :try_start_0
    sget-object v0, LX/2U4;->d:LX/2U4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 415092
    if-eqz v2, :cond_0

    .line 415093
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 415094
    new-instance p0, LX/2U4;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v3

    check-cast v3, LX/0sa;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v5

    check-cast v5, LX/0rq;

    invoke-direct {p0, v3, v4, v5}, LX/2U4;-><init>(LX/0sa;LX/0ad;LX/0rq;)V

    .line 415095
    move-object v0, p0

    .line 415096
    sput-object v0, LX/2U4;->d:LX/2U4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415097
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 415098
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 415099
    :cond_1
    sget-object v0, LX/2U4;->d:LX/2U4;

    return-object v0

    .line 415100
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 415101
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/8wL;Z)LX/0zO;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/8wL;",
            "Z)",
            "LX/0zO",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedPostComponentModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415102
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 415103
    new-instance v0, LX/ABo;

    invoke-direct {v0}, LX/ABo;-><init>()V

    move-object v3, v0

    .line 415104
    const-string v4, "fetch_event_info"

    sget-object v0, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne p2, v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v3, "story_id"

    invoke-virtual {v0, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v3, "image_large_aspect_height"

    iget-object v4, p0, LX/2U4;->a:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "image_large_aspect_width"

    iget-object v4, p0, LX/2U4;->a:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "fetch_slideshow_info"

    sget-object v0, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne p2, v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v3

    const-string v4, "is_boosted_post"

    sget-object v0, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne p2, v0, :cond_4

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v3, "force_saved_settings"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v3, "fetch_flexible_spec"

    iget-object v4, p0, LX/2U4;->b:LX/0ad;

    sget-short v5, LX/GDK;->d:S

    invoke-interface {v4, v5, v2}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v3, "audience_count"

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "include_call_to_action_types"

    sget-object v4, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq p2, v4, :cond_0

    sget-object v4, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    if-ne p2, v4, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    .line 415105
    move-object v0, v0

    .line 415106
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v2

    .line 415107
    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/8wL;)LX/0zO;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/8wL;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 415108
    new-instance v0, LX/ABL;

    invoke-direct {v0}, LX/ABL;-><init>()V

    move-object v3, v0

    .line 415109
    if-eqz p2, :cond_0

    .line 415110
    const-string v0, "story_id"

    invoke-virtual {v3, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 415111
    :cond_0
    const-string v0, "page_id"

    invoke-virtual {v3, v0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 415112
    const-string v0, "max_budgets_count"

    sget-object v4, LX/GE2;->b:Ljava/lang/Integer;

    invoke-virtual {v3, v0, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 415113
    const-string v4, "is_local_awareness"

    sget-object v0, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    if-ne v0, p3, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 415114
    const-string v4, "is_promote_website"

    sget-object v0, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    if-ne v0, p3, :cond_4

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 415115
    const-string v4, "is_cta"

    sget-object v0, LX/8wL;->PROMOTE_CTA:LX/8wL;

    if-ne p3, v0, :cond_5

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 415116
    const-string v0, "fetch_saved_audiences"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 415117
    const-string v0, "num_of_saved_audiences_to_fetch"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 415118
    const-string v4, "is_page_like"

    sget-object v0, LX/8wL;->PAGE_LIKE:LX/8wL;

    if-ne v0, p3, :cond_6

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 415119
    const-string v0, "use_default_settings"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 415120
    const-string v0, "component_app"

    invoke-virtual {p3}, LX/8wL;->getComponentAppEnum()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 415121
    const-string v0, "remove_audience_targeting_sentences"

    iget-object v4, p0, LX/2U4;->b:LX/0ad;

    sget-short v5, LX/GDK;->y:S

    invoke-interface {v4, v5, v2}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 415122
    const-string v4, "is_local_targeting"

    sget-object v0, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-eq v0, p3, :cond_1

    sget-object v0, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne v0, p3, :cond_7

    :cond_1
    move v0, v1

    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 415123
    const-string v0, "media_type"

    iget-object v4, p0, LX/2U4;->c:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->b()LX/0wF;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 415124
    const-string v4, "include_call_to_action_types"

    sget-object v0, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq p3, v0, :cond_2

    sget-object v0, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    if-ne p3, v0, :cond_8

    :cond_2
    move v0, v1

    :goto_5
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 415125
    const-string v0, "is_boosted_post"

    sget-object v4, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne p3, v4, :cond_9

    :goto_6
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 415126
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v2

    .line 415127
    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 415128
    goto/16 :goto_1

    :cond_5
    move v0, v2

    .line 415129
    goto/16 :goto_2

    :cond_6
    move v0, v2

    .line 415130
    goto :goto_3

    :cond_7
    move v0, v2

    .line 415131
    goto :goto_4

    :cond_8
    move v0, v2

    .line 415132
    goto :goto_5

    :cond_9
    move v1, v2

    .line 415133
    goto :goto_6
.end method
