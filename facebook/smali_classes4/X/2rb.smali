.class public LX/2rb;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/text/SimpleDateFormat;

.field public static final b:Ljava/text/SimpleDateFormat;


# instance fields
.field private final c:LX/2Ib;

.field public final d:Landroid/content/ContentResolver;

.field private final e:LX/0en;

.field private final f:LX/75F;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 472001
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy:MM:dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/2rb;->a:Ljava/text/SimpleDateFormat;

    .line 472002
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy:MM:dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 472003
    sput-object v0, LX/2rb;->b:Ljava/text/SimpleDateFormat;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 472004
    return-void
.end method

.method public constructor <init>(LX/2Ib;LX/0en;Landroid/content/ContentResolver;LX/75F;)V
    .locals 0
    .param p3    # Landroid/content/ContentResolver;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 472027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472028
    iput-object p1, p0, LX/2rb;->c:LX/2Ib;

    .line 472029
    iput-object p2, p0, LX/2rb;->e:LX/0en;

    .line 472030
    iput-object p3, p0, LX/2rb;->d:Landroid/content/ContentResolver;

    .line 472031
    iput-object p4, p0, LX/2rb;->f:LX/75F;

    .line 472032
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/media/MediaItem;)J
    .locals 11

    .prologue
    const-wide/16 v2, -0x1

    .line 472005
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0en;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 472006
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v1

    sget-object v4, LX/4gF;->PHOTO:LX/4gF;

    if-ne v1, v4, :cond_2

    .line 472007
    const-wide/16 v5, -0x1

    .line 472008
    :try_start_0
    new-instance v7, Landroid/media/ExifInterface;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 472009
    const-string v8, "GPSDateStamp"

    invoke-virtual {v7, v8}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 472010
    const-string v9, "GPSTimeStamp"

    invoke-virtual {v7, v9}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 472011
    if-eqz v8, :cond_5

    if-eqz v9, :cond_5

    .line 472012
    sget-object v7, LX/2rb;->b:Ljava/text/SimpleDateFormat;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v10, 0x20

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v7

    .line 472013
    :goto_0
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 472014
    :cond_0
    :goto_1
    move-wide v0, v5

    .line 472015
    :goto_2
    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 472016
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->j()J

    move-result-wide v0

    .line 472017
    :cond_1
    return-wide v0

    .line 472018
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v1

    sget-object v4, LX/4gF;->VIDEO:LX/4gF;

    if-ne v1, v4, :cond_4

    .line 472019
    iget-object v5, p0, LX/2rb;->d:Landroid/content/ContentResolver;

    invoke-static {v0, v5}, LX/2Ib;->b(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v5

    .line 472020
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    .line 472021
    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-nez v7, :cond_3

    const-wide/16 v5, -0x1

    :cond_3
    move-wide v0, v5

    .line 472022
    goto :goto_2

    :cond_4
    move-wide v0, v2

    goto :goto_2

    .line 472023
    :cond_5
    :try_start_1
    const-string v8, "DateTime"

    invoke-virtual {v7, v8}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 472024
    if-nez v7, :cond_6

    const/4 v7, 0x0

    goto :goto_0

    :cond_6
    sget-object v8, LX/2rb;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v8, v7}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v7

    goto :goto_0

    .line 472025
    :catch_0
    goto :goto_1

    .line 472026
    :catch_1
    goto :goto_1
.end method
