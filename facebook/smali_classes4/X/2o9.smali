.class public LX/2o9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile e:LX/2o9;


# instance fields
.field public final b:LX/2oA;

.field public final c:LX/0yH;

.field public d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 465678
    const-class v0, LX/0kS;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2o9;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2oA;LX/0yH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 465679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465680
    iput-object p1, p0, LX/2o9;->b:LX/2oA;

    .line 465681
    iput-object p2, p0, LX/2o9;->c:LX/0yH;

    .line 465682
    return-void
.end method

.method public static a(LX/0QB;)LX/2o9;
    .locals 5

    .prologue
    .line 465683
    sget-object v0, LX/2o9;->e:LX/2o9;

    if-nez v0, :cond_1

    .line 465684
    const-class v1, LX/2o9;

    monitor-enter v1

    .line 465685
    :try_start_0
    sget-object v0, LX/2o9;->e:LX/2o9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 465686
    if-eqz v2, :cond_0

    .line 465687
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 465688
    new-instance p0, LX/2o9;

    invoke-static {v0}, LX/2oA;->b(LX/0QB;)LX/2oA;

    move-result-object v3

    check-cast v3, LX/2oA;

    invoke-static {v0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v4

    check-cast v4, LX/0yH;

    invoke-direct {p0, v3, v4}, LX/2o9;-><init>(LX/2oA;LX/0yH;)V

    .line 465689
    move-object v0, p0

    .line 465690
    sput-object v0, LX/2o9;->e:LX/2o9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 465691
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 465692
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 465693
    :cond_1
    sget-object v0, LX/2o9;->e:LX/2o9;

    return-object v0

    .line 465694
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 465695
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;)V
    .locals 1

    .prologue
    .line 465696
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2o9;->d:Ljava/lang/ref/WeakReference;

    .line 465697
    return-void
.end method
