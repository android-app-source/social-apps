.class public LX/3GO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3GO;


# instance fields
.field public final a:LX/0ka;

.field public final b:LX/0W3;

.field public final c:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(LX/0ka;LX/0W3;Landroid/telephony/TelephonyManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 540983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 540984
    iput-object p1, p0, LX/3GO;->a:LX/0ka;

    .line 540985
    iput-object p2, p0, LX/3GO;->b:LX/0W3;

    .line 540986
    iput-object p3, p0, LX/3GO;->c:Landroid/telephony/TelephonyManager;

    .line 540987
    return-void
.end method

.method public static a(LX/0QB;)LX/3GO;
    .locals 6

    .prologue
    .line 540988
    sget-object v0, LX/3GO;->d:LX/3GO;

    if-nez v0, :cond_1

    .line 540989
    const-class v1, LX/3GO;

    monitor-enter v1

    .line 540990
    :try_start_0
    sget-object v0, LX/3GO;->d:LX/3GO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 540991
    if-eqz v2, :cond_0

    .line 540992
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 540993
    new-instance p0, LX/3GO;

    invoke-static {v0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v3

    check-cast v3, LX/0ka;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-static {v0}, LX/0e7;->b(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    invoke-direct {p0, v3, v4, v5}, LX/3GO;-><init>(LX/0ka;LX/0W3;Landroid/telephony/TelephonyManager;)V

    .line 540994
    move-object v0, p0

    .line 540995
    sput-object v0, LX/3GO;->d:LX/3GO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 540996
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 540997
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 540998
    :cond_1
    sget-object v0, LX/3GO;->d:LX/3GO;

    return-object v0

    .line 540999
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 541000
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
