.class public LX/3LE;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 550122
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 550123
    return-void
.end method

.method public static a(LX/3L7;LX/3LA;LX/0Or;LX/3LB;)LX/3LG;
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/ForDivebarList;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/ForDivebarList;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3L7;",
            "LX/3LA;",
            "LX/0Or",
            "<",
            "LX/3Mi;",
            ">;",
            "LX/3LB;",
            ")",
            "LX/3LG;"
        }
    .end annotation

    .prologue
    .line 550121
    new-instance v0, LX/3LF;

    invoke-direct {v0, p0, p1, p2, p3}, LX/3LF;-><init>(LX/3L7;LX/3LA;LX/0Or;LX/3LB;)V

    return-object v0
.end method

.method public static a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;Landroid/content/res/Resources;LX/3Mk;Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;LX/3N9;LX/3NA;LX/3NE;LX/3NH;LX/3NI;LX/3NJ;LX/3NL;LX/3Km;LX/3NM;LX/3Lw;LX/0ad;)LX/3Mi;
    .locals 16
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/ForDivebarList;
    .end annotation

    .prologue
    .line 550116
    invoke-static {}, LX/3NN;->b()Ljava/util/ArrayList;

    move-result-object v15

    .line 550117
    sget-short v1, LX/3NR;->c:S

    const/4 v2, 0x0

    move-object/from16 v0, p16

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 550118
    invoke-static {}, LX/3NN;->a()Ljava/util/ArrayList;

    move-result-object v15

    .line 550119
    :cond_0
    invoke-virtual/range {p13 .. p13}, LX/3Km;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v9, 0x0

    :goto_0
    invoke-virtual/range {p13 .. p13}, LX/3Km;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object/from16 v10, p9

    :goto_1
    invoke-virtual/range {p13 .. p13}, LX/3Km;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v11, p10

    :goto_2
    invoke-virtual/range {p14 .. p14}, LX/3NM;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    move-object/from16 v12, p11

    :goto_3
    invoke-virtual/range {p15 .. p15}, LX/3Lw;->a()Z

    move-result v14

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v13, p12

    invoke-static/range {v1 .. v15}, LX/3NN;->a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;Landroid/content/res/Resources;LX/3Mk;Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;LX/3N9;LX/3NA;LX/3NE;LX/3NH;LX/3NI;LX/3NJ;LX/3NL;ZLjava/util/ArrayList;)LX/3Mi;

    move-result-object v1

    return-object v1

    :cond_1
    move-object/from16 v9, p8

    goto :goto_0

    :cond_2
    const/4 v10, 0x0

    goto :goto_1

    :cond_3
    const/4 v11, 0x0

    goto :goto_2

    :cond_4
    const/4 v12, 0x0

    goto :goto_3
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 550120
    return-void
.end method
