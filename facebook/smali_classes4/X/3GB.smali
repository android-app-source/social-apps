.class public final LX/3GB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/15D;

.field public final synthetic b:LX/3Di;

.field public final synthetic c:LX/3DV;


# direct methods
.method public constructor <init>(LX/3DV;LX/15D;LX/3Di;)V
    .locals 0

    .prologue
    .line 540693
    iput-object p1, p0, LX/3GB;->c:LX/3DV;

    iput-object p2, p0, LX/3GB;->a:LX/15D;

    iput-object p3, p0, LX/3GB;->b:LX/3Di;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 540694
    iget-object v0, p0, LX/3GB;->c:LX/3DV;

    iget-object v1, p0, LX/3GB;->a:LX/15D;

    invoke-static {v0, v1}, LX/3DV;->b(LX/3DV;LX/15D;)V

    .line 540695
    instance-of v0, p1, LX/7P3;

    if-nez v0, :cond_0

    .line 540696
    iget-object v1, p0, LX/3GB;->b:LX/3Di;

    instance-of v0, p1, LX/7P1;

    if-eqz v0, :cond_1

    check-cast p1, LX/7P1;

    :goto_0
    invoke-interface {v1, p1}, LX/3Di;->a(Ljava/io/IOException;)V

    .line 540697
    :cond_0
    return-void

    .line 540698
    :cond_1
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_2

    new-instance v0, LX/7P1;

    const-string v2, "Network Error"

    invoke-direct {v0, v2, p1}, LX/7P1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object p1, v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Request wasn\'t executed"

    invoke-direct {v0, v2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 540699
    iget-object v0, p0, LX/3GB;->c:LX/3DV;

    iget-object v1, p0, LX/3GB;->a:LX/15D;

    invoke-static {v0, v1}, LX/3DV;->b(LX/3DV;LX/15D;)V

    .line 540700
    return-void
.end method
