.class public LX/2Lq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/02u;


# instance fields
.field public final a:Ljava/util/concurrent/ExecutorService;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/2Lr;

.field public final d:LX/11H;

.field public final e:LX/2Ls;

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mInFlightTraces"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Lr;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/11H;LX/2Ls;)V
    .locals 2
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395437
    iput-object p3, p0, LX/2Lq;->b:Ljava/util/concurrent/ExecutorService;

    .line 395438
    iput-object p1, p0, LX/2Lq;->c:LX/2Lr;

    .line 395439
    iput-object p2, p0, LX/2Lq;->a:Ljava/util/concurrent/ExecutorService;

    .line 395440
    iput-object p4, p0, LX/2Lq;->d:LX/11H;

    .line 395441
    iput-object p5, p0, LX/2Lq;->e:LX/2Ls;

    .line 395442
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, LX/2Lq;->f:Ljava/util/Set;

    .line 395443
    return-void
.end method

.method private declared-synchronized a(Ljava/util/List;LX/02p;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;",
            "LX/02p;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 395426
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 395427
    iget-object v2, p0, LX/2Lq;->f:Ljava/util/Set;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 395428
    :try_start_1
    iget-object v3, p0, LX/2Lq;->f:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 395429
    monitor-exit v2

    goto :goto_0

    .line 395430
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 395431
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 395432
    :cond_1
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 395433
    :try_start_4
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 395434
    iget-object v2, p0, LX/2Lq;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;

    invoke-direct {v3, p0, v0, p2, p3}, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$1;-><init>(LX/2Lq;Ljava/io/File;LX/02p;Z)V

    const v0, 0xc971bf

    invoke-static {v2, v3, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 395435
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public static b(LX/2Lq;Ljava/io/File;LX/02p;)V
    .locals 3
    .param p1    # Ljava/io/File;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 395423
    if-eqz p2, :cond_0

    .line 395424
    iget-object v0, p0, LX/2Lq;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/facebook/loom/upload/BackgroundUploadServiceImpl$3;-><init>(LX/2Lq;LX/02p;Ljava/io/File;)V

    const v2, 0x5e5aeb0c

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 395425
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/loom/config/SystemControlConfiguration;)V
    .locals 5

    .prologue
    .line 395410
    iget-object v0, p0, LX/2Lq;->c:LX/2Lr;

    const-wide/high16 v3, -0x8000000000000000L

    .line 395411
    iput-object p1, v0, LX/2Lr;->d:Lcom/facebook/loom/config/SystemControlConfiguration;

    .line 395412
    invoke-static {v0, v3, v4}, LX/2Lr;->d(LX/2Lr;J)J

    move-result-wide v1

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 395413
    :goto_0
    if-eqz v1, :cond_0

    .line 395414
    invoke-static {v0}, LX/2Lr;->d(LX/2Lr;)J

    move-result-wide v1

    iget-object v3, v0, LX/2Lr;->d:Lcom/facebook/loom/config/SystemControlConfiguration;

    invoke-virtual {v3}, Lcom/facebook/loom/config/SystemControlConfiguration;->c()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x1

    sub-long/2addr v1, v3

    invoke-static {v0, v1, v2}, LX/2Lr;->e(LX/2Lr;J)V

    .line 395415
    :cond_0
    invoke-static {v0}, LX/2Lr;->c(LX/2Lr;)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, LX/2Lr;->c(LX/2Lr;J)V

    .line 395416
    invoke-static {v0}, LX/2Lr;->b(LX/2Lr;)V

    .line 395417
    return-void

    .line 395418
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;LX/02p;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;",
            "LX/02p;",
            ")V"
        }
    .end annotation

    .prologue
    .line 395421
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/2Lq;->a(Ljava/util/List;LX/02p;Z)V

    .line 395422
    return-void
.end method

.method public final b(Ljava/util/List;LX/02p;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;",
            "LX/02p;",
            ")V"
        }
    .end annotation

    .prologue
    .line 395419
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/2Lq;->a(Ljava/util/List;LX/02p;Z)V

    .line 395420
    return-void
.end method
