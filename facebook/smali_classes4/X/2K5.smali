.class public LX/2K5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/2K3;",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/2G2;

.field private final d:LX/0Zm;

.field private final e:LX/0UW;

.field private final f:LX/0UW;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2G2;LX/0Zm;LX/0UW;LX/0UW;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/gk/internal/DeviceIdForGKs;
        .end annotation
    .end param
    .param p6    # LX/0UW;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2G2;",
            "Lcom/facebook/analytics/logger/AnalyticsConfig;",
            "LX/0UW;",
            "LX/0UW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 393370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393371
    iput-object p1, p0, LX/2K5;->a:LX/0Or;

    .line 393372
    iput-object p2, p0, LX/2K5;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 393373
    iput-object p3, p0, LX/2K5;->c:LX/2G2;

    .line 393374
    iput-object p4, p0, LX/2K5;->d:LX/0Zm;

    .line 393375
    iput-object p5, p0, LX/2K5;->e:LX/0UW;

    .line 393376
    iput-object p6, p0, LX/2K5;->f:LX/0UW;

    .line 393377
    return-void
.end method

.method private a(Z)LX/0UW;
    .locals 1

    .prologue
    .line 393467
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/2K5;->f:LX/0UW;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2K5;->e:LX/0UW;

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 393457
    :try_start_0
    const-string v0, "SHA-1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 393458
    const-string v1, "UTF-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 393459
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 393460
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-byte v4, v1, v0

    .line 393461
    const-string v5, "%02X"

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393462
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 393463
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 393464
    :goto_1
    return-object v0

    .line 393465
    :catch_0
    const-string v0, ""

    goto :goto_1

    .line 393466
    :catch_1
    const-string v0, ""

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/16 v4, 0x1

    .line 393449
    iget-object v0, p0, LX/2K5;->d:LX/0Zm;

    invoke-virtual {v0}, LX/0Zm;->a()LX/17O;

    move-result-object v0

    sget-object v1, LX/17O;->CORE_AND_SAMPLED:LX/17O;

    if-eq v0, v1, :cond_0

    .line 393450
    :goto_0
    return-void

    .line 393451
    :cond_0
    iget-object v0, p0, LX/2K5;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2ai;->d:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 393452
    invoke-static {}, LX/1l6;->a()LX/1l6;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    move-result-object v1

    invoke-virtual {v1}, LX/1l6;->hashCode()I

    move-result v1

    .line 393453
    iget-object v2, p0, LX/2K5;->c:LX/2G2;

    const-string v3, "gatekeepes_fetches"

    invoke-virtual {v2, v3, v4, v5}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 393454
    if-ne v0, v1, :cond_1

    .line 393455
    iget-object v0, p0, LX/2K5;->c:LX/2G2;

    const-string v1, "gatekeepes_unchanged"

    invoke-virtual {v0, v1, v4, v5}, LX/0Vx;->a(Ljava/lang/String;J)V

    goto :goto_0

    .line 393456
    :cond_1
    iget-object v0, p0, LX/2K5;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/2ai;->d:LX/0Tn;

    invoke-interface {v0, v2, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method private a(ZLjava/lang/String;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 393443
    iget-object v0, p0, LX/2K5;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 393444
    if-eqz p1, :cond_0

    .line 393445
    sget-object v1, LX/2Jx;->c:LX/0Tn;

    invoke-interface {v0, v1, p2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 393446
    :goto_0
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 393447
    return-void

    .line 393448
    :cond_0
    sget-object v1, LX/2ai;->f:LX/0Tn;

    invoke-interface {v0, v1, p2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 393403
    check-cast p1, LX/2K3;

    const/4 v2, 0x1

    .line 393404
    iget-object v0, p1, LX/2K3;->b:LX/2K4;

    sget-object v1, LX/2K4;->IS_SESSIONLESS:LX/2K4;

    if-ne v0, v1, :cond_2

    move v1, v2

    .line 393405
    :goto_0
    invoke-direct {p0, v1}, LX/2K5;->a(Z)LX/0UW;

    move-result-object v0

    invoke-interface {v0}, LX/0UW;->c()Ljava/lang/String;

    move-result-object v3

    .line 393406
    iput-object v3, p0, LX/2K5;->g:Ljava/lang/String;

    .line 393407
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 393408
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "format"

    const-string v6, "json"

    invoke-direct {v0, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393409
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "query_hash"

    invoke-direct {v0, v5, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393410
    if-eqz v1, :cond_3

    .line 393411
    iget-object v0, p0, LX/2K5;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/2Jx;->c:LX/0Tn;

    const-string v6, ""

    invoke-interface {v0, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 393412
    :goto_1
    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 393413
    const/16 v0, 0x2c

    invoke-static {v0}, LX/0PO;->on(C)LX/0PO;

    move-result-object v0

    invoke-direct {p0, v1}, LX/2K5;->a(Z)LX/0UW;

    move-result-object v3

    invoke-interface {v3}, LX/0UW;->b()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 393414
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "query"

    invoke-direct {v3, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393415
    :cond_0
    if-eqz v1, :cond_1

    .line 393416
    iget-object v0, p0, LX/2K5;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 393417
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 393418
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "hash_id"

    invoke-direct {v3, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393419
    :cond_1
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    .line 393420
    if-eqz v1, :cond_4

    .line 393421
    sget-object v1, LX/11I;->SESSIONLESS_GK:LX/11I;

    iget-object v1, v1, LX/11I;->requestNameString:Ljava/lang/String;

    .line 393422
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 393423
    iput-boolean v2, v0, LX/14O;->t:Z

    .line 393424
    :goto_2
    const-string v1, "POST"

    .line 393425
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 393426
    move-object v1, v0

    .line 393427
    const-string v2, "method/mobile.gatekeepers"

    .line 393428
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 393429
    move-object v1, v1

    .line 393430
    iput-object v4, v1, LX/14O;->g:Ljava/util/List;

    .line 393431
    move-object v1, v1

    .line 393432
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 393433
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 393434
    move-object v1, v1

    .line 393435
    sget-object v2, LX/14Q;->FALLBACK_REQUIRED:LX/14Q;

    .line 393436
    iput-object v2, v1, LX/14O;->v:LX/14Q;

    .line 393437
    new-instance v1, LX/14N;

    invoke-direct {v1, v0}, LX/14N;-><init>(LX/14O;)V

    return-object v1

    .line 393438
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_0

    .line 393439
    :cond_3
    iget-object v0, p0, LX/2K5;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/2ai;->f:LX/0Tn;

    const-string v6, ""

    invoke-interface {v0, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 393440
    :cond_4
    sget-object v1, LX/11I;->GK_INFO:LX/11I;

    iget-object v1, v1, LX/11I;->requestNameString:Ljava/lang/String;

    .line 393441
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 393442
    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 393378
    check-cast p1, LX/2K3;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 393379
    iget-object v0, p1, LX/2K3;->b:LX/2K4;

    sget-object v3, LX/2K4;->IS_SESSIONLESS:LX/2K4;

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 393380
    :goto_0
    :try_start_0
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 393381
    iget-object v4, p0, LX/2K5;->g:Ljava/lang/String;

    invoke-direct {p0, v0, v4}, LX/2K5;->a(ZLjava/lang/String;)V

    .line 393382
    const-string v4, "result"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v5

    .line 393383
    const-string v4, "hash"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-virtual {v3}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v3

    .line 393384
    invoke-static {v5}, LX/2K5;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 393385
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 393386
    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 393387
    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    .line 393388
    array-length v3, v8

    invoke-direct {p0, v0}, LX/2K5;->a(Z)LX/0UW;

    move-result-object v4

    invoke-interface {v4}, LX/0UW;->a()I

    move-result v4

    if-ne v3, v4, :cond_1

    move v3, v1

    :goto_1
    invoke-static {v3}, LX/0Tp;->b(Z)V

    .line 393389
    array-length v3, v8

    new-array v9, v3, [Z

    move v3, v2

    .line 393390
    :goto_2
    array-length v4, v8

    if-ge v3, v4, :cond_3

    .line 393391
    aget-char v4, v8, v3

    const/16 v10, 0x31

    if-ne v4, v10, :cond_2

    move v4, v1

    :goto_3
    aput-boolean v4, v9, v3

    .line 393392
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_0
    move v0, v2

    .line 393393
    goto :goto_0

    .line 393394
    :catch_0
    move-exception v1

    .line 393395
    const-string v2, ""

    invoke-direct {p0, v0, v2}, LX/2K5;->a(ZLjava/lang/String;)V

    .line 393396
    throw v1

    :cond_1
    move v3, v2

    .line 393397
    goto :goto_1

    :cond_2
    move v4, v2

    .line 393398
    goto :goto_3

    .line 393399
    :cond_3
    const-string v1, "gatekeepers"

    invoke-virtual {v7, v1, v9}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 393400
    if-nez v0, :cond_4

    .line 393401
    invoke-direct {p0, v6, v5}, LX/2K5;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 393402
    :cond_4
    return-object v7
.end method
