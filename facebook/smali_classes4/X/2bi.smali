.class public LX/2bi;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 439545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 439546
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 439547
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 439548
    :goto_0
    return v1

    .line 439549
    :cond_0
    const-string v8, "eligible_for_education"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 439550
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v5, v3

    move v3, v2

    .line 439551
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 439552
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 439553
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 439554
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 439555
    const-string v8, "education_content"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 439556
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 439557
    :cond_2
    const-string v8, "show_active_education"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 439558
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 439559
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 439560
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 439561
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 439562
    if-eqz v3, :cond_5

    .line 439563
    invoke-virtual {p1, v2, v5}, LX/186;->a(IZ)V

    .line 439564
    :cond_5
    if-eqz v0, :cond_6

    .line 439565
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 439566
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 439567
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 439568
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 439569
    if-eqz v0, :cond_0

    .line 439570
    const-string v1, "education_content"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439571
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 439572
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 439573
    if-eqz v0, :cond_1

    .line 439574
    const-string v1, "eligible_for_education"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439575
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 439576
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 439577
    if-eqz v0, :cond_2

    .line 439578
    const-string v1, "show_active_education"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439579
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 439580
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 439581
    return-void
.end method
