.class public LX/3BN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/1Kf;

.field private final c:LX/0SF;

.field public final d:LX/0ad;

.field public final e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Kf;LX/0SF;LX/0ad;Ljava/lang/Boolean;)V
    .locals 0
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 528096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 528097
    iput-object p1, p0, LX/3BN;->a:Landroid/content/Context;

    .line 528098
    iput-object p2, p0, LX/3BN;->b:LX/1Kf;

    .line 528099
    iput-object p3, p0, LX/3BN;->c:LX/0SF;

    .line 528100
    iput-object p4, p0, LX/3BN;->d:LX/0ad;

    .line 528101
    iput-object p5, p0, LX/3BN;->e:Ljava/lang/Boolean;

    .line 528102
    return-void
.end method

.method public static a(LX/0QB;)LX/3BN;
    .locals 1

    .prologue
    .line 528095
    invoke-static {p0}, LX/3BN;->b(LX/0QB;)LX/3BN;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/3BN;LX/1PT;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 528094
    new-instance v0, LX/BbS;

    invoke-direct {v0, p0, p1, p2}, LX/BbS;-><init>(LX/3BN;LX/1PT;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3BN;
    .locals 6

    .prologue
    .line 528103
    new-instance v0, LX/3BN;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v2

    check-cast v2, LX/1Kf;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SF;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-direct/range {v0 .. v5}, LX/3BN;-><init>(Landroid/content/Context;LX/1Kf;LX/0SF;LX/0ad;Ljava/lang/Boolean;)V

    .line 528104
    return-object v0
.end method

.method public static b(LX/3BN;Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 4

    .prologue
    .line 528092
    iget-object v0, p0, LX/3BN;->c:LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 528093
    const-wide/32 v2, 0x93a80

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 528091
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->O()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CITY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, p1}, LX/3BN;->b(LX/3BN;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/3BN;->d:LX/0ad;

    sget-short v2, LX/5HH;->m:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
