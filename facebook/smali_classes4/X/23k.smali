.class public LX/23k;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/23k;


# instance fields
.field public final a:LX/14z;

.field private final b:LX/0s6;


# direct methods
.method public constructor <init>(LX/14z;LX/0s6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 365096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365097
    iput-object p1, p0, LX/23k;->a:LX/14z;

    .line 365098
    iput-object p2, p0, LX/23k;->b:LX/0s6;

    .line 365099
    return-void
.end method

.method public static a(LX/0QB;)LX/23k;
    .locals 5

    .prologue
    .line 365100
    sget-object v0, LX/23k;->c:LX/23k;

    if-nez v0, :cond_1

    .line 365101
    const-class v1, LX/23k;

    monitor-enter v1

    .line 365102
    :try_start_0
    sget-object v0, LX/23k;->c:LX/23k;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 365103
    if-eqz v2, :cond_0

    .line 365104
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 365105
    new-instance p0, LX/23k;

    invoke-static {v0}, LX/14y;->a(LX/0QB;)LX/14z;

    move-result-object v3

    check-cast v3, LX/14z;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v4

    check-cast v4, LX/0s6;

    invoke-direct {p0, v3, v4}, LX/23k;-><init>(LX/14z;LX/0s6;)V

    .line 365106
    move-object v0, p0

    .line 365107
    sput-object v0, LX/23k;->c:LX/23k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365108
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 365109
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 365110
    :cond_1
    sget-object v0, LX/23k;->c:LX/23k;

    return-object v0

    .line 365111
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 365112
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Landroid/content/pm/PackageInfo;
    .locals 3

    .prologue
    .line 365113
    iget-object v0, p0, LX/23k;->b:LX/0s6;

    .line 365114
    const-string v1, "com.facebook.mlite"

    move-object v1, v1

    .line 365115
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/01H;->d(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    return-object v0
.end method
