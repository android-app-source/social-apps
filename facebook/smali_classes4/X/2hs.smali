.class public LX/2hs;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final e:LX/1mR;

.field public final f:LX/2dl;

.field public final g:Z

.field public final h:LX/1Ck;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 450872
    const-string v0, "REQUESTS_TAB_REQUESTS_QUERY_TAG"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/2hs;->a:LX/0Rf;

    .line 450873
    const-string v0, "REQUESTS_TAB_MEMORIAL_QUERY_TAG"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/2hs;->b:LX/0Rf;

    .line 450874
    const-string v0, "REQUESTS_TAB_PYMK_QUERY_TAG"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/2hs;->c:LX/0Rf;

    .line 450875
    const-string v0, "REQUESTS_TAB_REQUESTS_QUERY_TAG"

    const-string v1, "REQUESTS_TAB_PYMI_QUERY_TAG"

    const-string v2, "REQUESTS_TAB_PYMK_QUERY_TAG"

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/2hs;->d:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/2dl;LX/1mR;LX/0xW;LX/0ad;LX/1Ck;)V
    .locals 2
    .param p5    # LX/1Ck;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 450886
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450887
    iput-object p1, p0, LX/2hs;->f:LX/2dl;

    .line 450888
    iput-object p2, p0, LX/2hs;->e:LX/1mR;

    .line 450889
    iput-object p5, p0, LX/2hs;->h:LX/1Ck;

    .line 450890
    sget-short v1, LX/2ez;->m:S

    invoke-interface {p4, v1, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 450891
    iget-object v1, p3, LX/0xW;->a:LX/0ad;

    sget-short p1, LX/0xc;->y:S

    const/4 p2, 0x1

    invoke-interface {v1, p1, p2}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 450892
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, LX/2hs;->g:Z

    .line 450893
    return-void
.end method


# virtual methods
.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 450882
    iget-boolean v0, p0, LX/2hs;->g:Z

    if-eqz v0, :cond_0

    .line 450883
    iget-object v0, p0, LX/2hs;->f:LX/2dl;

    const-string v1, "REQUESTS_TAB_REQUESTS_QUERY_TAG"

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2dl;->a(Ljava/util/Collection;)V

    .line 450884
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 450885
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2hs;->e:LX/1mR;

    sget-object v1, LX/2hs;->a:LX/0Rf;

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(LX/0TF;)V
    .locals 4

    .prologue
    .line 450894
    iget-object v0, p0, LX/2hs;->h:LX/1Ck;

    const-string v1, "CLEAR_FRIEND_REQUEST_CACHE_TASK"

    new-instance v2, LX/83e;

    invoke-direct {v2, p0}, LX/83e;-><init>(LX/2hs;)V

    new-instance v3, LX/83f;

    invoke-direct {v3, p0, p1}, LX/83f;-><init>(LX/2hs;LX/0TF;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 450895
    return-void
.end method

.method public final d()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 450878
    iget-boolean v0, p0, LX/2hs;->g:Z

    if-eqz v0, :cond_0

    .line 450879
    iget-object v0, p0, LX/2hs;->f:LX/2dl;

    sget-object v1, LX/5P3;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/2dl;->a(Ljava/util/Collection;)V

    .line 450880
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 450881
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2hs;->e:LX/1mR;

    sget-object v1, LX/2hs;->d:LX/0Rf;

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(LX/0TF;)V
    .locals 4

    .prologue
    .line 450876
    iget-object v0, p0, LX/2hs;->h:LX/1Ck;

    const-string v1, "CLEAR_GRAPH_QL_CACHE_TASK"

    new-instance v2, LX/83i;

    invoke-direct {v2, p0}, LX/83i;-><init>(LX/2hs;)V

    new-instance v3, LX/83j;

    invoke-direct {v3, p0, p1}, LX/83j;-><init>(LX/2hs;LX/0TF;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 450877
    return-void
.end method
