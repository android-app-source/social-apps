.class public LX/32S;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 490059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490060
    return-void
.end method


# virtual methods
.method public final a(ILX/03R;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.util.Log.w",
            "SharedPreferencesUse"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 490061
    invoke-static {}, LX/00y;->a()Landroid/app/ActivityThread;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplication()Landroid/app/Application;

    move-result-object v1

    move-object v1, v1

    .line 490062
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    .line 490063
    :goto_0
    if-nez v1, :cond_2

    .line 490064
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Context not available"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 490065
    :cond_0
    :goto_1
    return-void

    .line 490066
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 490067
    :cond_2
    const-string v2, "crash_notification_flags"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 490068
    const/16 v2, 0x4a7

    if-ne p1, v2, :cond_0

    .line 490069
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "fbandroid_show_notification_when_crash"

    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne p2, v3, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1
.end method
