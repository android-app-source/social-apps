.class public final enum LX/2kx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2kx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2kx;

.field public static final enum FAILED:LX/2kx;

.field public static final enum FINISHED:LX/2kx;

.field public static final enum LOADING:LX/2kx;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 456669
    new-instance v0, LX/2kx;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v2}, LX/2kx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2kx;->LOADING:LX/2kx;

    .line 456670
    new-instance v0, LX/2kx;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v3}, LX/2kx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2kx;->FAILED:LX/2kx;

    .line 456671
    new-instance v0, LX/2kx;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v4}, LX/2kx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2kx;->FINISHED:LX/2kx;

    .line 456672
    const/4 v0, 0x3

    new-array v0, v0, [LX/2kx;

    sget-object v1, LX/2kx;->LOADING:LX/2kx;

    aput-object v1, v0, v2

    sget-object v1, LX/2kx;->FAILED:LX/2kx;

    aput-object v1, v0, v3

    sget-object v1, LX/2kx;->FINISHED:LX/2kx;

    aput-object v1, v0, v4

    sput-object v0, LX/2kx;->$VALUES:[LX/2kx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 456668
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2kx;
    .locals 1

    .prologue
    .line 456666
    const-class v0, LX/2kx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2kx;

    return-object v0
.end method

.method public static values()[LX/2kx;
    .locals 1

    .prologue
    .line 456667
    sget-object v0, LX/2kx;->$VALUES:[LX/2kx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2kx;

    return-object v0
.end method
