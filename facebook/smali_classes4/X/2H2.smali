.class public LX/2H2;
.super LX/2H3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Tn;

.field private static volatile b:LX/2H2;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 389743
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "fbns_lite/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2H2;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389742
    invoke-direct {p0}, LX/2H3;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2H2;
    .locals 3

    .prologue
    .line 389729
    sget-object v0, LX/2H2;->b:LX/2H2;

    if-nez v0, :cond_1

    .line 389730
    const-class v1, LX/2H2;

    monitor-enter v1

    .line 389731
    :try_start_0
    sget-object v0, LX/2H2;->b:LX/2H2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 389732
    if-eqz v2, :cond_0

    .line 389733
    :try_start_1
    new-instance v0, LX/2H2;

    invoke-direct {v0}, LX/2H2;-><init>()V

    .line 389734
    move-object v0, v0

    .line 389735
    sput-object v0, LX/2H2;->b:LX/2H2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389736
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 389737
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 389738
    :cond_1
    sget-object v0, LX/2H2;->b:LX/2H2;

    return-object v0

    .line 389739
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 389740
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Tn;
    .locals 1

    .prologue
    .line 389741
    sget-object v0, LX/2H2;->a:LX/0Tn;

    return-object v0
.end method
