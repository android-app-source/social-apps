.class public LX/3El;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3El;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 537990
    const-string v0, "notifications"

    const/16 v1, 0x21

    new-instance v2, LX/3Em;

    invoke-direct {v2}, LX/3Em;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 537991
    return-void
.end method

.method public static a(LX/0QB;)LX/3El;
    .locals 3

    .prologue
    .line 537992
    sget-object v0, LX/3El;->a:LX/3El;

    if-nez v0, :cond_1

    .line 537993
    const-class v1, LX/3El;

    monitor-enter v1

    .line 537994
    :try_start_0
    sget-object v0, LX/3El;->a:LX/3El;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 537995
    if-eqz v2, :cond_0

    .line 537996
    :try_start_1
    new-instance v0, LX/3El;

    invoke-direct {v0}, LX/3El;-><init>()V

    .line 537997
    move-object v0, v0

    .line 537998
    sput-object v0, LX/3El;->a:LX/3El;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 537999
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 538000
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 538001
    :cond_1
    sget-object v0, LX/3El;->a:LX/3El;

    return-object v0

    .line 538002
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 538003
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
