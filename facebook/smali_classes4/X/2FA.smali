.class public LX/2FA;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/compactdisk/FileUtilsHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/compactdisk/FileUtilsHolder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 386598
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/compactdisk/FileUtilsHolder;
    .locals 3

    .prologue
    .line 386599
    sget-object v0, LX/2FA;->a:Lcom/facebook/compactdisk/FileUtilsHolder;

    if-nez v0, :cond_1

    .line 386600
    const-class v1, LX/2FA;

    monitor-enter v1

    .line 386601
    :try_start_0
    sget-object v0, LX/2FA;->a:Lcom/facebook/compactdisk/FileUtilsHolder;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 386602
    if-eqz v2, :cond_0

    .line 386603
    :try_start_1
    invoke-static {}, LX/2FB;->a()Lcom/facebook/compactdisk/FileUtilsHolder;

    move-result-object v0

    sput-object v0, LX/2FA;->a:Lcom/facebook/compactdisk/FileUtilsHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 386604
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 386605
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 386606
    :cond_1
    sget-object v0, LX/2FA;->a:Lcom/facebook/compactdisk/FileUtilsHolder;

    return-object v0

    .line 386607
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 386608
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 386609
    invoke-static {}, LX/2FB;->a()Lcom/facebook/compactdisk/FileUtilsHolder;

    move-result-object v0

    return-object v0
.end method
