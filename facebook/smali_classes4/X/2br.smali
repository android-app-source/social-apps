.class public final LX/2br;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field private g:I

.field private h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field private i:Z

.field public j:Z


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/model/PrivacyOptionsResult;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 439830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 439831
    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    iput-object v1, p0, LX/2br;->a:LX/0Px;

    .line 439832
    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    iput-object v1, p0, LX/2br;->b:LX/0Px;

    .line 439833
    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    iput-object v1, p0, LX/2br;->c:LX/0Px;

    .line 439834
    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    iput-object v1, p0, LX/2br;->d:LX/0Px;

    .line 439835
    iget v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOptionIndex:I

    iput v1, p0, LX/2br;->e:I

    .line 439836
    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v1, p0, LX/2br;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 439837
    iget v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOptionIndex:I

    iput v1, p0, LX/2br;->g:I

    .line 439838
    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v1, p0, LX/2br;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 439839
    iget-boolean v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isResultFromServer:Z

    iput-boolean v1, p0, LX/2br;->j:Z

    .line 439840
    iget-boolean v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isSelectedOptionExternal:Z

    if-eqz v1, :cond_2

    .line 439841
    iput-boolean v0, p0, LX/2br;->j:Z

    .line 439842
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 439843
    iget-object v1, p0, LX/2br;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, LX/2br;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 439844
    iget-object v4, p0, LX/2br;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0, v4}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 439845
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 439846
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 439847
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2br;->i:Z

    goto :goto_1

    .line 439848
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/2br;->a:LX/0Px;

    .line 439849
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/2br;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 439850
    invoke-static {p1}, LX/3R3;->a(LX/1oU;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 439851
    iput-boolean v1, p0, LX/2br;->j:Z

    .line 439852
    iget-object v0, p0, LX/2br;->a:LX/0Px;

    invoke-static {v0, p1}, LX/2cA;->a(Ljava/util/Collection;LX/1oS;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2br;->b:LX/0Px;

    invoke-static {v0, p1}, LX/2cA;->a(Ljava/util/Collection;LX/1oS;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 439853
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2br;->i:Z

    .line 439854
    :goto_0
    iput-object p1, p0, LX/2br;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 439855
    return-object p0

    .line 439856
    :cond_0
    iput-boolean v1, p0, LX/2br;->i:Z

    goto :goto_0
.end method

.method public final b()Lcom/facebook/privacy/model/PrivacyOptionsResult;
    .locals 11

    .prologue
    .line 439857
    iget-object v1, p0, LX/2br;->a:LX/0Px;

    .line 439858
    iget-boolean v0, p0, LX/2br;->i:Z

    if-eqz v0, :cond_0

    .line 439859
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v1, p0, LX/2br;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    iget-object v1, p0, LX/2br;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 439860
    :cond_0
    new-instance v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v2, p0, LX/2br;->b:LX/0Px;

    iget-object v3, p0, LX/2br;->c:LX/0Px;

    iget-object v4, p0, LX/2br;->d:LX/0Px;

    iget v5, p0, LX/2br;->e:I

    iget-object v6, p0, LX/2br;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget v7, p0, LX/2br;->g:I

    iget-object v8, p0, LX/2br;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-boolean v9, p0, LX/2br;->i:Z

    iget-boolean v10, p0, LX/2br;->j:Z

    invoke-direct/range {v0 .. v10}, Lcom/facebook/privacy/model/PrivacyOptionsResult;-><init>(LX/0Px;LX/0Px;LX/0Px;LX/0Px;ILcom/facebook/graphql/model/GraphQLPrivacyOption;ILcom/facebook/graphql/model/GraphQLPrivacyOption;ZZ)V

    return-object v0
.end method
