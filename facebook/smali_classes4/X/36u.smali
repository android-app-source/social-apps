.class public final LX/36u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Lv;


# direct methods
.method public constructor <init>(LX/1Lv;)V
    .locals 0

    .prologue
    .line 500534
    iput-object p1, p0, LX/36u;->a:LX/1Lv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 500535
    iget-object v0, p0, LX/36u;->a:LX/1Lv;

    iget-object v0, v0, LX/1Lv;->h:LX/03V;

    sget-object v1, LX/1Lv;->a:Ljava/lang/String;

    const-string v2, "Unexpected error in prefetching task"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 500536
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 500537
    iget-object v0, p0, LX/36u;->a:LX/1Lv;

    invoke-static {v0}, LX/1Lv;->j(LX/1Lv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500538
    iget-object v0, p0, LX/36u;->a:LX/1Lv;

    iget-object v0, v0, LX/1Lv;->m:Landroid/os/Handler;

    iget-object v1, p0, LX/36u;->a:LX/1Lv;

    iget-object v1, v1, LX/1Lv;->I:Ljava/lang/Runnable;

    const v2, 0x2c1110e6

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 500539
    :cond_0
    return-void
.end method
