.class public LX/2K1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2K2;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:Lcom/facebook/gk/store/GatekeeperWriter;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/gk/store/GatekeeperWriter;)V
    .locals 0
    .param p2    # Lcom/facebook/gk/store/GatekeeperWriter;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 393348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393349
    iput-object p1, p0, LX/2K1;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 393350
    iput-object p2, p0, LX/2K1;->b:Lcom/facebook/gk/store/GatekeeperWriter;

    .line 393351
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 393352
    const-string v0, "gatekeepers"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v0

    .line 393353
    iget-object v1, p0, LX/2K1;->b:Lcom/facebook/gk/store/GatekeeperWriter;

    invoke-interface {v1}, Lcom/facebook/gk/store/GatekeeperWriter;->e()LX/2LD;

    move-result-object v1

    invoke-interface {v1, v0}, LX/2LD;->a([Z)LX/2LD;

    move-result-object v0

    invoke-interface {v0, v4}, LX/2LD;->a(Z)V

    .line 393354
    iget-object v0, p0, LX/2K1;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 393355
    sget-object v1, LX/2Jx;->b:LX/0Tn;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 393356
    iget-object v1, p0, LX/2K1;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2Jx;->e:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    if-eq v1, v4, :cond_0

    .line 393357
    sget-object v1, LX/2Jx;->e:LX/0Tn;

    invoke-interface {v0, v1, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 393358
    :cond_0
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 393359
    return-void
.end method
