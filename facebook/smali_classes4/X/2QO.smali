.class public LX/2QO;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field private final h:Landroid/content/Context;

.field public final i:LX/0TD;

.field public final j:LX/0s6;

.field public final k:LX/2F7;

.field public final l:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 407934
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "device_free_space"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "device_available_space"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "device_total_space"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "cache_size"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "app_data_size"

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/2QO;->g:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0TD;LX/0s6;LX/2F7;LX/0Zb;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 407935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407936
    const-string v0, "build_number"

    iput-object v0, p0, LX/2QO;->a:Ljava/lang/String;

    .line 407937
    const-string v0, "install_unixtime"

    iput-object v0, p0, LX/2QO;->b:Ljava/lang/String;

    .line 407938
    const-string v0, "installer_package_name"

    iput-object v0, p0, LX/2QO;->c:Ljava/lang/String;

    .line 407939
    const-string v0, "package_name"

    iput-object v0, p0, LX/2QO;->d:Ljava/lang/String;

    .line 407940
    const-string v0, "update_unixtime"

    iput-object v0, p0, LX/2QO;->e:Ljava/lang/String;

    .line 407941
    const-string v0, "version_number"

    iput-object v0, p0, LX/2QO;->f:Ljava/lang/String;

    .line 407942
    iput-object p1, p0, LX/2QO;->h:Landroid/content/Context;

    .line 407943
    iput-object p2, p0, LX/2QO;->i:LX/0TD;

    .line 407944
    iput-object p3, p0, LX/2QO;->j:LX/0s6;

    .line 407945
    iput-object p4, p0, LX/2QO;->k:LX/2F7;

    .line 407946
    iput-object p5, p0, LX/2QO;->l:LX/0Zb;

    .line 407947
    return-void
.end method

.method public static b(LX/0QB;)LX/2QO;
    .locals 6

    .prologue
    .line 407948
    new-instance v0, LX/2QO;

    const-class v1, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static {p0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v3

    check-cast v3, LX/0s6;

    invoke-static {p0}, LX/2F7;->b(LX/0QB;)LX/2F7;

    move-result-object v4

    check-cast v4, LX/2F7;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-direct/range {v0 .. v5}, LX/2QO;-><init>(Landroid/content/Context;LX/0TD;LX/0s6;LX/2F7;LX/0Zb;)V

    .line 407949
    return-object v0
.end method
