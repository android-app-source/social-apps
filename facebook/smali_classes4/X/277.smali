.class public final LX/277;
.super LX/278;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/katana/LogoutActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/LogoutActivity;)V
    .locals 0

    .prologue
    .line 372834
    iput-object p1, p0, LX/277;->a:Lcom/facebook/katana/LogoutActivity;

    invoke-direct {p0}, LX/278;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const v5, 0x230012

    const/4 v4, 0x1

    .line 372835
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 372836
    iget-object v1, p0, LX/277;->a:Lcom/facebook/katana/LogoutActivity;

    invoke-static {v1, v0}, Lcom/facebook/katana/LogoutActivity;->d(Lcom/facebook/katana/LogoutActivity;Landroid/os/Bundle;)V

    .line 372837
    iget-object v1, p0, LX/277;->a:Lcom/facebook/katana/LogoutActivity;

    iget-object v1, v1, Lcom/facebook/katana/LogoutActivity;->t:LX/10N;

    invoke-virtual {v1}, LX/10N;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 372838
    const-string v1, "logout_snackbar_enabled"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 372839
    :cond_0
    iget-object v1, p0, LX/277;->a:Lcom/facebook/katana/LogoutActivity;

    iget-object v1, v1, Lcom/facebook/katana/LogoutActivity;->u:LX/276;

    iget-object v2, p0, LX/277;->a:Lcom/facebook/katana/LogoutActivity;

    new-instance v3, Lcom/facebook/katana/LogoutActivity$1$1;

    invoke-direct {v3, p0}, Lcom/facebook/katana/LogoutActivity$1$1;-><init>(LX/277;)V

    invoke-virtual {v1, v2, v3, v4}, LX/276;->a(Landroid/support/v4/app/FragmentActivity;Ljava/lang/Runnable;I)V

    .line 372840
    iget-object v1, p0, LX/277;->a:Lcom/facebook/katana/LogoutActivity;

    iget-boolean v1, v1, Lcom/facebook/katana/LogoutActivity;->x:Z

    if-eqz v1, :cond_1

    .line 372841
    iget-object v1, p0, LX/277;->a:Lcom/facebook/katana/LogoutActivity;

    iget-object v1, v1, Lcom/facebook/katana/LogoutActivity;->q:LX/275;

    iget-object v2, p0, LX/277;->a:Lcom/facebook/katana/LogoutActivity;

    invoke-virtual {v1, v2, v0}, LX/275;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 372842
    :goto_0
    if-nez p1, :cond_2

    .line 372843
    iget-object v0, p0, LX/277;->a:Lcom/facebook/katana/LogoutActivity;

    iget-object v0, v0, Lcom/facebook/katana/LogoutActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    invoke-interface {v0, v5, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 372844
    :goto_1
    iget-object v0, p0, LX/277;->a:Lcom/facebook/katana/LogoutActivity;

    iget-object v0, v0, Lcom/facebook/katana/LogoutActivity;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    invoke-virtual {v0}, LX/1HI;->a()V

    .line 372845
    return-void

    .line 372846
    :cond_1
    iget-object v0, p0, LX/277;->a:Lcom/facebook/katana/LogoutActivity;

    invoke-virtual {v0}, Lcom/facebook/katana/LogoutActivity;->finish()V

    goto :goto_0

    .line 372847
    :cond_2
    iget-object v0, p0, LX/277;->a:Lcom/facebook/katana/LogoutActivity;

    iget-object v0, v0, Lcom/facebook/katana/LogoutActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x3

    invoke-interface {v0, v5, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 372848
    iget-object v0, p0, LX/277;->a:Lcom/facebook/katana/LogoutActivity;

    iget-object v0, v0, Lcom/facebook/katana/LogoutActivity;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "LogoutActivity"

    const-string v2, "Logout failure"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
