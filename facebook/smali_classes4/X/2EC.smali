.class public LX/2EC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2EC;


# instance fields
.field private a:LX/0Uh;

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ADY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/ADY;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 385071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385072
    iput-object p1, p0, LX/2EC;->a:LX/0Uh;

    .line 385073
    iput-object p2, p0, LX/2EC;->b:LX/0Ot;

    .line 385074
    return-void
.end method

.method public static a(LX/0QB;)LX/2EC;
    .locals 5

    .prologue
    .line 385054
    sget-object v0, LX/2EC;->c:LX/2EC;

    if-nez v0, :cond_1

    .line 385055
    const-class v1, LX/2EC;

    monitor-enter v1

    .line 385056
    :try_start_0
    sget-object v0, LX/2EC;->c:LX/2EC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 385057
    if-eqz v2, :cond_0

    .line 385058
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 385059
    new-instance v4, LX/2EC;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    const/16 p0, 0x1745

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/2EC;-><init>(LX/0Uh;LX/0Ot;)V

    .line 385060
    move-object v0, v4

    .line 385061
    sput-object v0, LX/2EC;->c:LX/2EC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 385062
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 385063
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 385064
    :cond_1
    sget-object v0, LX/2EC;->c:LX/2EC;

    return-object v0

    .line 385065
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 385066
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/2EC;)Z
    .locals 3

    .prologue
    .line 385070
    iget-object v0, p0, LX/2EC;->a:LX/0Uh;

    const/16 v1, 0x23

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final init()V
    .locals 1

    .prologue
    .line 385067
    invoke-static {p0}, LX/2EC;->b(LX/2EC;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385068
    iget-object v0, p0, LX/2EC;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Bw;

    invoke-static {v0}, LX/0Bx;->a(LX/0Bw;)V

    .line 385069
    :cond_0
    return-void
.end method
