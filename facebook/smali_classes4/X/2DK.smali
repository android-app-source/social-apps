.class public LX/2DK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2DK;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ZZ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/2DL;


# direct methods
.method public constructor <init>(LX/0Ot;LX/2DL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0ZZ;",
            ">;",
            "LX/2DL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384031
    iput-object p1, p0, LX/2DK;->a:LX/0Ot;

    .line 384032
    iput-object p2, p0, LX/2DK;->b:LX/2DL;

    .line 384033
    return-void
.end method

.method public static a(LX/0QB;)LX/2DK;
    .locals 5

    .prologue
    .line 384034
    sget-object v0, LX/2DK;->c:LX/2DK;

    if-nez v0, :cond_1

    .line 384035
    const-class v1, LX/2DK;

    monitor-enter v1

    .line 384036
    :try_start_0
    sget-object v0, LX/2DK;->c:LX/2DK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 384037
    if-eqz v2, :cond_0

    .line 384038
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 384039
    new-instance v4, LX/2DK;

    const/16 v3, 0x9c

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/2DL;->a(LX/0QB;)LX/2DL;

    move-result-object v3

    check-cast v3, LX/2DL;

    invoke-direct {v4, p0, v3}, LX/2DK;-><init>(LX/0Ot;LX/2DL;)V

    .line 384040
    move-object v0, v4

    .line 384041
    sput-object v0, LX/2DK;->c:LX/2DK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384042
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 384043
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 384044
    :cond_1
    sget-object v0, LX/2DK;->c:LX/2DK;

    return-object v0

    .line 384045
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 384046
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 384047
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2DK;->b:LX/2DL;

    invoke-virtual {v0}, LX/2DL;->a()Ljava/util/List;

    move-result-object v0

    .line 384048
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 384049
    iget-object v1, p0, LX/2DK;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ZZ;

    .line 384050
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/analytics/HoneyAnalyticsEvent;

    .line 384051
    invoke-virtual {v1, v2}, LX/0ZZ;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384052
    :cond_0
    monitor-exit p0

    return-void

    .line 384053
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
