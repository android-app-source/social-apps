.class public LX/2Y1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2Y1;


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/49e;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0dC;

.field private c:LX/0SG;

.field private d:Ljava/util/Random;

.field private final e:J


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/0dC;Ljava/util/Random;LX/0SG;)V
    .locals 2
    .param p3    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/49e;",
            ">;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "Ljava/util/Random;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 420553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420554
    const-wide v0, 0x125eb7a0848L

    iput-wide v0, p0, LX/2Y1;->e:J

    .line 420555
    iput-object p1, p0, LX/2Y1;->a:Ljava/util/Set;

    .line 420556
    iput-object p2, p0, LX/2Y1;->b:LX/0dC;

    .line 420557
    iput-object p4, p0, LX/2Y1;->c:LX/0SG;

    .line 420558
    iput-object p3, p0, LX/2Y1;->d:Ljava/util/Random;

    .line 420559
    return-void
.end method

.method public static a(LX/0QB;)LX/2Y1;
    .locals 7

    .prologue
    .line 420560
    sget-object v0, LX/2Y1;->f:LX/2Y1;

    if-nez v0, :cond_1

    .line 420561
    const-class v1, LX/2Y1;

    monitor-enter v1

    .line 420562
    :try_start_0
    sget-object v0, LX/2Y1;->f:LX/2Y1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 420563
    if-eqz v2, :cond_0

    .line 420564
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 420565
    new-instance v6, LX/2Y1;

    invoke-static {v0}, LX/27u;->a(LX/0QB;)Ljava/util/Set;

    move-result-object p0

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v3

    check-cast v3, LX/0dC;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v4

    check-cast v4, Ljava/util/Random;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {v6, p0, v3, v4, v5}, LX/2Y1;-><init>(Ljava/util/Set;LX/0dC;Ljava/util/Random;LX/0SG;)V

    .line 420566
    move-object v0, v6

    .line 420567
    sput-object v0, LX/2Y1;->f:LX/2Y1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 420568
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 420569
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 420570
    :cond_1
    sget-object v0, LX/2Y1;->f:LX/2Y1;

    return-object v0

    .line 420571
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 420572
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    .line 420573
    iget-object v0, p0, LX/2Y1;->b:LX/0dC;

    invoke-virtual {v0}, LX/0dC;->b()LX/0dI;

    move-result-object v1

    .line 420574
    iget-object v0, p0, LX/2Y1;->c:LX/0SG;

    invoke-static {v0}, LX/0dC;->a(LX/0SG;)LX/0dI;

    move-result-object v2

    .line 420575
    const-wide v4, 0x125eb7a0848L

    iget-object v0, p0, LX/2Y1;->d:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v6

    const-wide/32 v8, 0x5265c00

    rem-long/2addr v6, v8

    add-long/2addr v4, v6

    .line 420576
    new-instance v0, LX/0dI;

    .line 420577
    iget-object v3, v2, LX/0dI;->a:Ljava/lang/String;

    move-object v3, v3

    .line 420578
    invoke-direct {v0, v3, v4, v5}, LX/0dI;-><init>(Ljava/lang/String;J)V

    .line 420579
    iget-object v3, p0, LX/2Y1;->b:LX/0dC;

    invoke-virtual {v3, v0}, LX/0dC;->a(LX/0dI;)V

    .line 420580
    iget-object v0, p0, LX/2Y1;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/49e;

    .line 420581
    sget-object v4, LX/49d;->REGENERATE:LX/49d;

    const/4 v5, 0x0

    invoke-interface {v0, v1, v2, v4, v5}, LX/49e;->a(LX/0dI;LX/0dI;LX/49d;Ljava/lang/String;)V

    goto :goto_0

    .line 420582
    :cond_0
    return-void
.end method
