.class public LX/2QR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile q:LX/2QR;


# instance fields
.field private final a:LX/0Sh;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:LX/03V;

.field private final d:LX/0en;

.field public final e:Landroid/content/Context;

.field private final f:LX/0lC;

.field public final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final h:LX/0SF;

.field public final i:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/lang/Object;

.field public l:Ljava/io/File;

.field public m:Ljava/io/File;

.field public n:Z

.field public o:Z

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Sh;Ljava/util/concurrent/Executor;LX/03V;LX/0en;LX/0lC;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SF;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 408339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 408340
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/2QR;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 408341
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/2QR;->k:Ljava/lang/Object;

    .line 408342
    iput-object p1, p0, LX/2QR;->e:Landroid/content/Context;

    .line 408343
    iput-object p2, p0, LX/2QR;->a:LX/0Sh;

    .line 408344
    iput-object p3, p0, LX/2QR;->b:Ljava/util/concurrent/Executor;

    .line 408345
    iput-object p4, p0, LX/2QR;->c:LX/03V;

    .line 408346
    iput-object p5, p0, LX/2QR;->d:LX/0en;

    .line 408347
    iput-object p6, p0, LX/2QR;->f:LX/0lC;

    .line 408348
    iput-object p7, p0, LX/2QR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 408349
    iput-object p8, p0, LX/2QR;->h:LX/0SF;

    .line 408350
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2QR;->j:Ljava/util/Map;

    .line 408351
    iget-object v0, p0, LX/2QR;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 408352
    return-void
.end method

.method public static a(LX/0QB;)LX/2QR;
    .locals 12

    .prologue
    .line 408326
    sget-object v0, LX/2QR;->q:LX/2QR;

    if-nez v0, :cond_1

    .line 408327
    const-class v1, LX/2QR;

    monitor-enter v1

    .line 408328
    :try_start_0
    sget-object v0, LX/2QR;->q:LX/2QR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 408329
    if-eqz v2, :cond_0

    .line 408330
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 408331
    new-instance v3, LX/2QR;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v8

    check-cast v8, LX/0en;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v9

    check-cast v9, LX/0lC;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SF;

    invoke-direct/range {v3 .. v11}, LX/2QR;-><init>(Landroid/content/Context;LX/0Sh;Ljava/util/concurrent/Executor;LX/03V;LX/0en;LX/0lC;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SF;)V

    .line 408332
    move-object v0, v3

    .line 408333
    sput-object v0, LX/2QR;->q:LX/2QR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408334
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 408335
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 408336
    :cond_1
    sget-object v0, LX/2QR;->q:LX/2QR;

    return-object v0

    .line 408337
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 408338
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/io/File;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 408322
    if-nez p1, :cond_0

    .line 408323
    :goto_0
    return-object p0

    .line 408324
    :cond_0
    :try_start_0
    const-string v0, "%s. File size: %d"

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_0

    .line 408325
    :catch_0
    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 408319
    iget-object v0, p0, LX/2QR;->l:Ljava/io/File;

    if-nez v0, :cond_0

    .line 408320
    :goto_0
    return-void

    .line 408321
    :cond_0
    iget-object v0, p0, LX/2QR;->d:LX/0en;

    iget-object v1, p0, LX/2QR;->l:Ljava/io/File;

    new-instance v2, LX/2SH;

    invoke-direct {v2, p0, p1, p2}, LX/2SH;-><init>(LX/2QR;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0en;->a(Ljava/io/File;LX/0Rl;)Z

    goto :goto_0
.end method

.method public static b(LX/2QR;LX/0Tn;Ljava/lang/String;Ljava/lang/Class;)Landroid/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Tn;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 408298
    iget-object v2, p0, LX/2QR;->l:Ljava/io/File;

    if-nez v2, :cond_0

    .line 408299
    :goto_0
    return-object v0

    .line 408300
    :cond_0
    iget-object v2, p0, LX/2QR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2, p1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 408301
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, LX/2QR;->l:Ljava/io/File;

    invoke-direct {v3, v4, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 408302
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 408303
    const/4 v2, 0x0

    .line 408304
    :try_start_0
    iget-object v4, p0, LX/2QR;->f:LX/0lC;

    invoke-virtual {v4, v3, p3}, LX/0lC;->a(Ljava/io/File;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 408305
    :cond_1
    :goto_1
    new-instance v2, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v2

    goto :goto_0

    .line 408306
    :catch_0
    move-exception v1

    .line 408307
    :try_start_1
    invoke-virtual {p0}, LX/2QR;->c()V

    .line 408308
    iget-object v4, p0, LX/2QR;->c:LX/03V;

    const-string v5, "PlatformWebDialogsCache"

    const-string v6, "Unable to find file %s"

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408309
    iget-object v1, p0, LX/2QR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, p1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 408310
    invoke-direct {p0, p2, v0}, LX/2QR;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    goto :goto_1

    .line 408311
    :catch_1
    move-exception v1

    .line 408312
    :try_start_2
    invoke-virtual {p0}, LX/2QR;->c()V

    .line 408313
    iget-object v4, p0, LX/2QR;->c:LX/03V;

    const-string v5, "PlatformWebDialogsCache"

    const-string v6, "Error while reading file"

    invoke-static {v6, v3}, LX/2QR;->a(Ljava/lang/String;Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 408314
    iget-object v1, p0, LX/2QR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, p1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 408315
    invoke-direct {p0, p2, v0}, LX/2QR;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    goto :goto_1

    .line 408316
    :catchall_0
    move-exception v1

    .line 408317
    iget-object v2, p0, LX/2QR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    invoke-interface {v2, p1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 408318
    invoke-direct {p0, p2, v0}, LX/2QR;->a(Ljava/lang/String;Ljava/lang/String;)V

    throw v1
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 408287
    const/16 v0, 0x23

    invoke-static {v0}, LX/2Cb;->on(C)LX/2Cb;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v1

    .line 408288
    invoke-static {v1, v2}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 408289
    invoke-static {v1, v2}, LX/0Ph;->c(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 408290
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 408291
    :cond_0
    :goto_0
    return-object v0

    .line 408292
    :cond_1
    const-string v3, "%1$s=\\d+"

    const-string v4, "platformurlversion"

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    .line 408293
    invoke-virtual {v3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    move-object v1, v2

    .line 408294
    :goto_1
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 408295
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 408296
    :cond_2
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 408297
    const-string v2, "%1$s#%2$s"

    invoke-static {v2, v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static g(LX/2QR;)Z
    .locals 8

    .prologue
    .line 408240
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 408241
    iget-boolean v0, p0, LX/2QR;->o:Z

    move v0, v0

    .line 408242
    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/2QR;->n:Z

    if-eqz v0, :cond_1

    .line 408243
    :cond_0
    :goto_0
    iget-boolean v0, p0, LX/2QR;->n:Z

    move v0, v0

    .line 408244
    return v0

    .line 408245
    :cond_1
    invoke-virtual {p0}, LX/2QR;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408246
    iget-object v0, p0, LX/2QR;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v3, p0, LX/2QR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/2QS;->f:LX/0Tn;

    invoke-interface {v3, v4, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 408247
    invoke-static {p0}, LX/2QR;->i(LX/2QR;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 408248
    iget-object v0, p0, LX/2QR;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 408249
    if-eqz v0, :cond_0

    .line 408250
    new-instance v3, Ljava/io/File;

    const-string v4, "%1$s%2$s"

    const-string v5, "PlatformWebDialogs_V"

    const-string v6, "1.1"

    invoke-static {v4, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v3, p0, LX/2QR;->l:Ljava/io/File;

    .line 408251
    iget-object v0, p0, LX/2QR;->l:Ljava/io/File;

    if-nez v0, :cond_7

    .line 408252
    :cond_2
    :goto_1
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, LX/2QR;->l:Ljava/io/File;

    const-string v4, "CachedResponses"

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LX/2QR;->m:Ljava/io/File;

    .line 408253
    iget-object v0, p0, LX/2QR;->l:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 408254
    :goto_2
    :try_start_0
    iget-object v3, p0, LX/2QR;->m:Ljava/io/File;

    invoke-static {v3}, LX/04M;->a(Ljava/io/File;)V
    :try_end_0
    .catch LX/0Fu; {:try_start_0 .. :try_end_0} :catch_0

    .line 408255
    if-eqz v0, :cond_5

    .line 408256
    iget-object v0, p0, LX/2QR;->l:Ljava/io/File;

    if-nez v0, :cond_8

    .line 408257
    :cond_3
    :goto_3
    iput-boolean v1, p0, LX/2QR;->n:Z

    goto :goto_0

    :cond_4
    move v0, v2

    .line 408258
    goto :goto_2

    .line 408259
    :catch_0
    move-exception v0

    .line 408260
    iput-object v7, p0, LX/2QR;->l:Ljava/io/File;

    .line 408261
    iput-object v7, p0, LX/2QR;->m:Ljava/io/File;

    .line 408262
    iget-object v1, p0, LX/2QR;->c:LX/03V;

    const-string v2, "PlatformWebDialogsCache"

    const-string v3, "Unable to create the directory for cached responses"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 408263
    :cond_5
    sget-object v0, LX/2QS;->g:LX/0Tn;

    const-string v3, "PlatformWebDialogsCache.urlMap"

    const-class v4, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$CacheWrapper;

    invoke-static {p0, v0, v3, v4}, LX/2QR;->b(LX/2QR;LX/0Tn;Ljava/lang/String;Ljava/lang/Class;)Landroid/util/Pair;

    move-result-object v3

    .line 408264
    if-eqz v3, :cond_3

    .line 408265
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 408266
    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$CacheWrapper;

    .line 408267
    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    .line 408268
    invoke-virtual {v0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$CacheWrapper;->a()Ljava/util/Map;

    move-result-object v0

    .line 408269
    if-eqz v0, :cond_6

    .line 408270
    iget-object v2, p0, LX/2QR;->j:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_3

    .line 408271
    :cond_6
    invoke-virtual {p0}, LX/2QR;->c()V

    .line 408272
    iget-object v0, p0, LX/2QR;->c:LX/03V;

    const-string v1, "PlatformWebDialogsCache"

    const-string v3, "Deserialized cache had a NULL cache-map"

    invoke-virtual {v0, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 408273
    goto :goto_3

    .line 408274
    :cond_7
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, LX/2QR;->e:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "%1$s%2$s"

    const-string v5, "PlatformWebDialogs_V"

    const-string v6, "1.1"

    invoke-static {v4, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 408275
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 408276
    iget-object v3, p0, LX/2QR;->l:Ljava/io/File;

    invoke-virtual {v0, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 408277
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto/16 :goto_1

    .line 408278
    :cond_8
    iget-object v0, p0, LX/2QR;->l:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 408279
    iget-object v2, p0, LX/2QR;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    .line 408280
    if-eqz v2, :cond_3

    .line 408281
    new-instance v3, LX/2Rx;

    invoke-direct {v3, p0, v0}, LX/2Rx;-><init>(LX/2QR;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v2

    .line 408282
    if-eqz v2, :cond_3

    .line 408283
    array-length v3, v2

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 408284
    invoke-static {v4}, LX/2W9;->a(Ljava/io/File;)Z

    .line 408285
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 408286
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method

.method public static i(LX/2QR;)Z
    .locals 2

    .prologue
    .line 408239
    iget-object v0, p0, LX/2QR;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/2QR;->o:Z

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Tn;Ljava/lang/String;Ljava/lang/Class;)Landroid/util/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Tn;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "TT;>;"
        }
    .end annotation

    .prologue
    .line 408353
    iget-object v0, p0, LX/2QR;->a:LX/0Sh;

    const-string v1, "This method will perform disk I/O and should not be called on the UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 408354
    iget-object v1, p0, LX/2QR;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 408355
    :try_start_0
    invoke-static {p0}, LX/2QR;->g(LX/2QR;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 408356
    const/4 v0, 0x0

    monitor-exit v1

    .line 408357
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1, p2, p3}, LX/2QR;->b(LX/2QR;LX/0Tn;Ljava/lang/String;Ljava/lang/Class;)Landroid/util/Pair;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 408358
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 408134
    iget-object v0, p0, LX/2QR;->a:LX/0Sh;

    const-string v2, "This method will perform disk I/O and should not be called on the UI thread"

    invoke-virtual {v0, v2}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 408135
    iget-object v2, p0, LX/2QR;->k:Ljava/lang/Object;

    monitor-enter v2

    .line 408136
    :try_start_0
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/2QR;->g(LX/2QR;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 408137
    :cond_0
    monitor-exit v2

    move-object v0, v1

    .line 408138
    :goto_0
    return-object v0

    .line 408139
    :cond_1
    invoke-static {p1}, LX/2QR;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 408140
    if-nez v3, :cond_2

    .line 408141
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 408142
    :cond_2
    iget-object v0, p0, LX/2QR;->j:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 408143
    if-nez v0, :cond_3

    .line 408144
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 408145
    :cond_3
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, LX/2QR;->m:Ljava/io/File;

    invoke-direct {v4, v5, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408146
    :try_start_1
    invoke-static {v4}, LX/0en;->a(Ljava/io/File;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 408147
    :goto_1
    :try_start_2
    monitor-exit v2

    goto :goto_0

    .line 408148
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 408149
    :catch_0
    move-exception v0

    .line 408150
    :try_start_3
    invoke-virtual {p0}, LX/2QR;->c()V

    .line 408151
    iget-object v4, p0, LX/2QR;->c:LX/03V;

    const-string v5, "PlatformWebDialogsCache"

    const-string v6, "Could not find the cached file for %s"

    invoke-static {v6, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 408152
    invoke-virtual {p0, p1}, LX/2QR;->b(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v0, v1

    goto :goto_1
.end method

.method public final a(LX/0Tn;Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Tn;",
            "Ljava/lang/String;",
            "TT;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 408223
    iget-object v1, p0, LX/2QR;->a:LX/0Sh;

    const-string v2, "This method will perform disk I/O and should not be called on the UI thread"

    invoke-virtual {v1, v2}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 408224
    iget-object v2, p0, LX/2QR;->k:Ljava/lang/Object;

    monitor-enter v2

    .line 408225
    :try_start_0
    invoke-static {p0}, LX/2QR;->g(LX/2QR;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 408226
    monitor-exit v2

    .line 408227
    :goto_0
    return v0

    .line 408228
    :cond_0
    const-string v1, "%s_%d"

    iget-object v3, p0, LX/2QR;->h:LX/0SF;

    invoke-virtual {v3}, LX/0SF;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v1, p2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 408229
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, LX/2QR;->l:Ljava/io/File;

    invoke-direct {v3, v4, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408230
    :try_start_1
    iget-object v4, p0, LX/2QR;->f:LX/0lC;

    invoke-virtual {v4, v3, p3}, LX/0lC;->a(Ljava/io/File;Ljava/lang/Object;)V

    .line 408231
    iget-object v4, p0, LX/2QR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    invoke-interface {v4, p1, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 408232
    invoke-direct {p0, p2, v1}, LX/2QR;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408233
    const/4 v0, 0x1

    :try_start_2
    monitor-exit v2

    goto :goto_0

    .line 408234
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 408235
    :catch_0
    move-exception v1

    .line 408236
    :try_start_3
    invoke-virtual {p0}, LX/2QR;->c()V

    .line 408237
    iget-object v4, p0, LX/2QR;->c:LX/03V;

    const-string v5, "PlatformWebDialogsCache"

    const-string v6, "Error while writing to file"

    invoke-static {v6, v3}, LX/2QR;->a(Ljava/lang/String;Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 408238
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;Lorg/apache/http/HttpResponse;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 408198
    iget-object v1, p0, LX/2QR;->a:LX/0Sh;

    const-string v2, "This method will perform disk I/O and should not be called on the UI thread"

    invoke-virtual {v1, v2}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 408199
    iget-object v2, p0, LX/2QR;->k:Ljava/lang/Object;

    monitor-enter v2

    .line 408200
    :try_start_0
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 408201
    if-eqz v1, :cond_0

    invoke-static {p0}, LX/2QR;->g(LX/2QR;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 408202
    :cond_0
    monitor-exit v2

    .line 408203
    :goto_0
    return v0

    .line 408204
    :cond_1
    const/4 v5, 0x0

    .line 408205
    invoke-virtual {p1}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->c()Ljava/lang/String;

    move-result-object v3

    .line 408206
    invoke-static {v3}, LX/2QR;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 408207
    const/16 v3, 0xa

    move-object v4, v5

    .line 408208
    :goto_1
    if-nez v4, :cond_3

    add-int/lit8 v3, v3, -0x1

    if-ltz v3, :cond_3

    .line 408209
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 408210
    iget-object p2, p0, LX/2QR;->j:Ljava/util/Map;

    invoke-interface {p2, v4}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    move-object v4, v5

    .line 408211
    goto :goto_1

    .line 408212
    :cond_2
    iget-object p2, p0, LX/2QR;->j:Ljava/util/Map;

    invoke-interface {p2, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 408213
    :cond_3
    move-object v3, v4

    .line 408214
    if-nez v3, :cond_4

    .line 408215
    monitor-exit v2

    goto :goto_0

    .line 408216
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 408217
    :cond_4
    :try_start_1
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, LX/2QR;->m:Ljava/io/File;

    invoke-direct {v4, v5, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408218
    :try_start_2
    iget-object v3, p0, LX/2QR;->d:LX/0en;

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v3, v1, v4}, LX/0en;->a(Ljava/io/InputStream;Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 408219
    const/4 v0, 0x1

    .line 408220
    :goto_2
    :try_start_3
    monitor-exit v2

    goto :goto_0

    .line 408221
    :catch_0
    move-exception v1

    .line 408222
    iget-object v3, p0, LX/2QR;->c:LX/03V;

    const-string v4, "PlatformWebDialogsCache"

    const-string v5, "Exception caching http response"

    invoke-virtual {v3, v4, v5, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 408183
    iget-object v0, p0, LX/2QR;->a:LX/0Sh;

    const-string v2, "This method will perform disk I/O and should not be called on the UI thread"

    invoke-virtual {v0, v2}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 408184
    iget-object v2, p0, LX/2QR;->k:Ljava/lang/Object;

    monitor-enter v2

    .line 408185
    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {p0}, LX/2QR;->g(LX/2QR;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 408186
    :cond_0
    monitor-exit v2

    move v0, v1

    .line 408187
    :goto_0
    return v0

    .line 408188
    :cond_1
    invoke-static {p1}, LX/2QR;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 408189
    if-nez v0, :cond_2

    .line 408190
    monitor-exit v2

    move v0, v1

    goto :goto_0

    .line 408191
    :cond_2
    iget-object v3, p0, LX/2QR;->j:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 408192
    if-nez v0, :cond_3

    .line 408193
    monitor-exit v2

    move v0, v1

    goto :goto_0

    .line 408194
    :cond_3
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, LX/2QR;->m:Ljava/io/File;

    invoke-direct {v3, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 408195
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    monitor-exit v2

    goto :goto_0

    .line 408196
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    move v0, v1

    .line 408197
    goto :goto_1
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 408181
    iget-object v0, p0, LX/2QR;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$1;

    invoke-direct {v1, p0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$1;-><init>(LX/2QR;)V

    const v2, -0x4832fc21

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 408182
    return-void
.end method

.method public final d()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 408172
    iget-object v2, p0, LX/2QR;->a:LX/0Sh;

    const-string v3, "This method will read sharedprefs and should not be called on the UI thread"

    invoke-virtual {v2, v3}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 408173
    iget-boolean v2, p0, LX/2QR;->p:Z

    if-eqz v2, :cond_0

    .line 408174
    :goto_0
    return v0

    .line 408175
    :cond_0
    iget-object v2, p0, LX/2QR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 408176
    goto :goto_0

    .line 408177
    :cond_1
    iget-object v2, p0, LX/2QR;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v3, p0, LX/2QR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/2QS;->f:LX/0Tn;

    invoke-interface {v3, v4, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 408178
    invoke-static {p0}, LX/2QR;->i(LX/2QR;)Z

    .line 408179
    iput-boolean v0, p0, LX/2QR;->p:Z

    .line 408180
    iget-boolean v0, p0, LX/2QR;->p:Z

    goto :goto_0
.end method

.method public final e()V
    .locals 7

    .prologue
    .line 408157
    iget-object v0, p0, LX/2QR;->a:LX/0Sh;

    const-string v1, "This method will perform disk I/O and should not be called on the UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 408158
    iget-object v1, p0, LX/2QR;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 408159
    :try_start_0
    invoke-static {p0}, LX/2QR;->g(LX/2QR;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 408160
    monitor-exit v1

    .line 408161
    :goto_0
    return-void

    .line 408162
    :cond_0
    iget-object v0, p0, LX/2QR;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    .line 408163
    iget-object v0, p0, LX/2QR;->m:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 408164
    if-nez v3, :cond_1

    .line 408165
    monitor-exit v1

    goto :goto_0

    .line 408166
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 408167
    :cond_1
    :try_start_1
    array-length v4, v3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_3

    aget-object v5, v3, v0

    .line 408168
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 408169
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 408170
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 408171
    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final f()V
    .locals 5

    .prologue
    .line 408153
    iget-object v0, p0, LX/2QR;->a:LX/0Sh;

    const-string v1, "This method will perform disk I/O and should not be called on the UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 408154
    iget-object v1, p0, LX/2QR;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 408155
    :try_start_0
    sget-object v0, LX/2QS;->g:LX/0Tn;

    const-string v2, "PlatformWebDialogsCache.urlMap"

    new-instance v3, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$CacheWrapper;

    iget-object v4, p0, LX/2QR;->j:Ljava/util/Map;

    invoke-direct {v3, v4}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$CacheWrapper;-><init>(Ljava/util/Map;)V

    invoke-virtual {p0, v0, v2, v3}, LX/2QR;->a(LX/0Tn;Ljava/lang/String;Ljava/lang/Object;)Z

    .line 408156
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
