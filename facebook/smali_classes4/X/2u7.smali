.class public LX/2u7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0Up;


# instance fields
.field private final a:LX/0qX;

.field private final b:Landroid/content/Context;

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0qX;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 475512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 475513
    iput-object p1, p0, LX/2u7;->b:Landroid/content/Context;

    .line 475514
    iput-object p2, p0, LX/2u7;->a:LX/0qX;

    .line 475515
    iput-object p3, p0, LX/2u7;->c:LX/0Uh;

    .line 475516
    return-void
.end method

.method public static b(LX/0QB;)LX/2u7;
    .locals 4

    .prologue
    .line 475523
    new-instance v3, LX/2u7;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0qX;->a(LX/0QB;)LX/0qX;

    move-result-object v1

    check-cast v1, LX/0qX;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-direct {v3, v0, v1, v2}, LX/2u7;-><init>(Landroid/content/Context;LX/0qX;LX/0Uh;)V

    .line 475524
    return-object v3
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 475521
    iget-object v0, p0, LX/2u7;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->a(Landroid/content/Context;)V

    .line 475522
    return-void
.end method

.method public final init()V
    .locals 4

    .prologue
    .line 475517
    iget-object v0, p0, LX/2u7;->c:LX/0Uh;

    const/16 v1, 0x594

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475518
    :goto_0
    return-void

    .line 475519
    :cond_0
    iget-object v0, p0, LX/2u7;->a:LX/0qX;

    const/16 v1, 0x3c

    invoke-virtual {v0, v1}, LX/0qX;->c(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3c

    mul-long/2addr v0, v2

    .line 475520
    iget-object v2, p0, LX/2u7;->b:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->a(Landroid/content/Context;J)V

    goto :goto_0
.end method
