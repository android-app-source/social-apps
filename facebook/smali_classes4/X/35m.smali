.class public LX/35m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/Ap5;

.field public final b:LX/C0D;

.field public final c:LX/1qb;

.field private final d:LX/1qa;

.field public final e:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/Ap5;LX/C0D;LX/1qb;LX/1qa;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 497610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497611
    iput-object p1, p0, LX/35m;->a:LX/Ap5;

    .line 497612
    iput-object p2, p0, LX/35m;->b:LX/C0D;

    .line 497613
    iput-object p3, p0, LX/35m;->c:LX/1qb;

    .line 497614
    iput-object p4, p0, LX/35m;->d:LX/1qa;

    .line 497615
    iput-object p5, p0, LX/35m;->e:Landroid/content/res/Resources;

    .line 497616
    return-void
.end method

.method public static a(LX/0QB;)LX/35m;
    .locals 9

    .prologue
    .line 497617
    const-class v1, LX/35m;

    monitor-enter v1

    .line 497618
    :try_start_0
    sget-object v0, LX/35m;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 497619
    sput-object v2, LX/35m;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 497620
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497621
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 497622
    new-instance v3, LX/35m;

    invoke-static {v0}, LX/Ap5;->a(LX/0QB;)LX/Ap5;

    move-result-object v4

    check-cast v4, LX/Ap5;

    invoke-static {v0}, LX/C0D;->b(LX/0QB;)LX/C0D;

    move-result-object v5

    check-cast v5, LX/C0D;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v6

    check-cast v6, LX/1qb;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v7

    check-cast v7, LX/1qa;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v8}, LX/35m;-><init>(LX/Ap5;LX/C0D;LX/1qb;LX/1qa;Landroid/content/res/Resources;)V

    .line 497623
    move-object v0, v3

    .line 497624
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 497625
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/35m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 497626
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 497627
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1V4;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1V4;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 497628
    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/1V4;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
