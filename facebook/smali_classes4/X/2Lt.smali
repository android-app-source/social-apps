.class public LX/2Lt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;
.implements LX/0dN;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile j:LX/2Lt;


# instance fields
.field public final b:Landroid/app/NotificationManager;

.field private c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:Landroid/content/Context;

.field private e:Landroid/app/PendingIntent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field public g:Z

.field private h:Z

.field public i:Landroid/app/Notification;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 395526
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "com.facebook.loom.CONTROL_TOGGLE."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Lt;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/NotificationManager;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 395527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395528
    iput-object p1, p0, LX/2Lt;->d:Landroid/content/Context;

    .line 395529
    iput-object p2, p0, LX/2Lt;->b:Landroid/app/NotificationManager;

    .line 395530
    iput-object p3, p0, LX/2Lt;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 395531
    iput-boolean v0, p0, LX/2Lt;->f:Z

    .line 395532
    iput-boolean v0, p0, LX/2Lt;->g:Z

    .line 395533
    iput-boolean v0, p0, LX/2Lt;->h:Z

    .line 395534
    return-void
.end method

.method public static a(LX/0QB;)LX/2Lt;
    .locals 6

    .prologue
    .line 395535
    sget-object v0, LX/2Lt;->j:LX/2Lt;

    if-nez v0, :cond_1

    .line 395536
    const-class v1, LX/2Lt;

    monitor-enter v1

    .line 395537
    :try_start_0
    sget-object v0, LX/2Lt;->j:LX/2Lt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 395538
    if-eqz v2, :cond_0

    .line 395539
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 395540
    new-instance p0, LX/2Lt;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5}, LX/2Lt;-><init>(Landroid/content/Context;Landroid/app/NotificationManager;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 395541
    move-object v0, p0

    .line 395542
    sput-object v0, LX/2Lt;->j:LX/2Lt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 395543
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 395544
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 395545
    :cond_1
    sget-object v0, LX/2Lt;->j:LX/2Lt;

    return-object v0

    .line 395546
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 395547
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(ILandroid/app/Notification;)V
    .locals 2
    .param p2    # Landroid/app/Notification;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 395548
    if-nez p2, :cond_0

    .line 395549
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "notification can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 395550
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/2Lt;->b:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 395551
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method private declared-synchronized b(Z)V
    .locals 2

    .prologue
    .line 395552
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 395553
    :try_start_0
    iget-boolean v0, p0, LX/2Lt;->g:Z

    invoke-static {p0, v0}, LX/2Lt;->c(LX/2Lt;Z)V

    .line 395554
    :goto_0
    iput-boolean p1, p0, LX/2Lt;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395555
    monitor-exit p0

    return-void

    .line 395556
    :cond_0
    :try_start_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Lt;->i:Landroid/app/Notification;

    .line 395557
    const/16 v0, 0xc8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 395558
    :try_start_2
    iget-object v1, p0, LX/2Lt;->b:Landroid/app/NotificationManager;

    invoke-virtual {v1, v0}, Landroid/app/NotificationManager;->cancel(I)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 395559
    :goto_1
    goto :goto_0

    .line 395560
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    goto :goto_1
.end method

.method private static c(LX/2Lt;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 395561
    sget-object v0, LX/02r;->b:LX/02r;

    move-object v0, v0

    .line 395562
    if-nez v0, :cond_0

    .line 395563
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "TraceControl is null and we\'re showing a notification"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 395564
    :cond_0
    invoke-virtual {v0}, LX/02r;->d()Ljava/lang/String;

    move-result-object v0

    .line 395565
    if-eqz p1, :cond_1

    .line 395566
    const-string v2, "Loom is weaving"

    .line 395567
    const-string v1, "Tap to stop and upload trace"

    .line 395568
    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v4, "%s\n\nTrace ID: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    aput-object v0, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 395569
    :goto_0
    new-instance v3, LX/2HB;

    iget-object v4, p0, LX/2Lt;->d:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 395570
    const-string v4, "Loom controls"

    invoke-virtual {v3, v4}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v4

    .line 395571
    iput v7, v4, LX/2HB;->j:I

    .line 395572
    move-object v4, v4

    .line 395573
    const v5, 0x1080059

    invoke-virtual {v4, v5}, LX/2HB;->a(I)LX/2HB;

    move-result-object v4

    iget-object v5, p0, LX/2Lt;->e:Landroid/app/PendingIntent;

    .line 395574
    iput-object v5, v4, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 395575
    move-object v4, v4

    .line 395576
    invoke-virtual {v4, v7}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    new-instance v2, LX/3pe;

    invoke-direct {v2, v3}, LX/3pe;-><init>(LX/2HB;)V

    invoke-virtual {v2, v0}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2HB;->a(LX/3pc;)LX/2HB;

    .line 395577
    invoke-virtual {v3}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, LX/2Lt;->i:Landroid/app/Notification;

    .line 395578
    const/16 v0, 0xc8

    iget-object v1, p0, LX/2Lt;->i:Landroid/app/Notification;

    invoke-direct {p0, v0, v1}, LX/2Lt;->a(ILandroid/app/Notification;)V

    .line 395579
    return-void

    .line 395580
    :cond_1
    const-string v1, "Loom is primed and ready"

    .line 395581
    const-string v0, "Tap to start tracing"

    move-object v2, v1

    move-object v1, v0

    .line 395582
    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(LX/GzN;)V
    .locals 5

    .prologue
    .line 395583
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2Lt;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 395584
    :goto_0
    monitor-exit p0

    return-void

    .line 395585
    :cond_0
    :try_start_1
    sget-object v0, LX/GzM;->a:[I

    invoke-virtual {p1}, LX/GzN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 395586
    const-string v0, "Trace upload status unknown"

    move-object v1, v0

    .line 395587
    :goto_1
    new-instance v2, LX/2HB;

    iget-object v3, p0, LX/2Lt;->d:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 395588
    const/4 v3, -0x1

    .line 395589
    iput v3, v2, LX/2HB;->j:I

    .line 395590
    move-object v3, v2

    .line 395591
    const v4, 0x1080055

    invoke-virtual {v3, v4}, LX/2HB;->a(I)LX/2HB;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    .line 395592
    invoke-virtual {v2}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    .line 395593
    const/16 v1, 0xc9

    invoke-direct {p0, v1, v0}, LX/2Lt;->a(ILandroid/app/Notification;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395594
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 395595
    :pswitch_0
    :try_start_2
    const-string v1, "Uploading Loom trace"

    .line 395596
    const-string v0, "Uploading trace"

    goto :goto_1

    .line 395597
    :pswitch_1
    const-string v1, "Trace upload was successful"

    .line 395598
    const-string v0, "Upload successful"

    goto :goto_1

    .line 395599
    :pswitch_2
    const-string v1, "Trace upload failed"

    .line 395600
    const-string v0, "Upload failed"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    .locals 1

    .prologue
    .line 395601
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/09i;->a:LX/0Tn;

    invoke-virtual {p2, v0}, LX/0Tn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395602
    const/4 v0, 0x0

    invoke-interface {p1, p2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 395603
    invoke-direct {p0, v0}, LX/2Lt;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395604
    :cond_0
    monitor-exit p0

    return-void

    .line 395605
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 395606
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2Lt;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 395607
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 395608
    :cond_1
    :try_start_1
    iget-boolean v0, p0, LX/2Lt;->g:Z

    if-eq v0, p1, :cond_0

    .line 395609
    invoke-static {p0, p1}, LX/2Lt;->c(LX/2Lt;Z)V

    .line 395610
    iput-boolean p1, p0, LX/2Lt;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395611
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 395612
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2Lt;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized init()V
    .locals 4

    .prologue
    .line 395613
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/content/IntentFilter;

    sget-object v1, LX/2Lt;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 395614
    new-instance v1, LX/0Yd;

    sget-object v2, LX/2Lt;->a:Ljava/lang/String;

    new-instance v3, LX/2Lu;

    invoke-direct {v3, p0}, LX/2Lu;-><init>(LX/2Lt;)V

    invoke-direct {v1, v2, v3}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    .line 395615
    iget-object v2, p0, LX/2Lt;->d:Landroid/content/Context;

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 395616
    iget-object v0, p0, LX/2Lt;->d:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    sget-object v3, LX/2Lt;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, LX/2Lt;->e:Landroid/app/PendingIntent;

    .line 395617
    iget-object v0, p0, LX/2Lt;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/09i;->a:LX/0Tn;

    invoke-interface {v0, v1, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 395618
    iget-object v0, p0, LX/2Lt;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/09i;->a:LX/0Tn;

    invoke-virtual {p0, v0, v1}, LX/2Lt;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395619
    monitor-exit p0

    return-void

    .line 395620
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
