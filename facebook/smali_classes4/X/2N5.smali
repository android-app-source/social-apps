.class public LX/2N5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static volatile k:LX/2N5;


# instance fields
.field public final b:LX/2N6;

.field public final c:LX/2N7;

.field public final d:LX/2NA;

.field public final e:LX/2ND;

.field public final f:LX/2NE;

.field public final g:LX/2NF;

.field public final h:LX/2NG;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2NI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 398484
    const/16 v0, 0x37

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "thread_key"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "legacy_thread_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "action_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "refetch_action_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "last_visible_action_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "sequence_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "senders"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "snippet"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "snippet_sender"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "admin_snippet"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "timestamp_ms"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "last_read_timestamp_ms"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "approx_total_message_count"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "unread_message_count"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "pic_hash"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "can_reply_to"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "cannot_reply_reason"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "pic"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "is_subscribed"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "folder"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "draft"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "last_fetch_time_ms"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "missed_call_status"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "mute_until"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "me_bubble_color"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "other_bubble_color"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "wallpaper_color"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "last_fetch_action_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "initial_fetch_complete"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "custom_like_emoji"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "outgoing_message_lifetime"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "custom_nicknames"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "invite_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "is_last_message_sponsored"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "group_chat_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "game_data"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "is_joinable"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "requires_approval"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "rtc_call_info"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "last_message_commerce_message_type"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "is_thread_queue_enabled"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "group_description"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "media_preview"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "booking_requests"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "last_call_ms"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "is_discoverable"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "last_sponsored_message_call_to_action"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "montage_thread_key"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "montage_preview_message_id"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "montage_preview_attachments"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "montage_preview_text"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "montage_preview_sticker_id"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "montage_latest_message_timestamp_ms"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "montage_thread_read_watermark_timestamp_ms"

    aput-object v2, v0, v1

    sput-object v0, LX/2N5;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2N6;LX/2N7;LX/2NA;LX/2ND;LX/2NE;LX/2NF;LX/2NG;LX/0Or;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2N6;",
            "LX/2N7;",
            "LX/2NA;",
            "LX/2ND;",
            "LX/2NE;",
            "LX/2NF;",
            "LX/2NG;",
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2NI;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 398473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398474
    iput-object p1, p0, LX/2N5;->b:LX/2N6;

    .line 398475
    iput-object p2, p0, LX/2N5;->c:LX/2N7;

    .line 398476
    iput-object p3, p0, LX/2N5;->d:LX/2NA;

    .line 398477
    iput-object p4, p0, LX/2N5;->e:LX/2ND;

    .line 398478
    iput-object p5, p0, LX/2N5;->f:LX/2NE;

    .line 398479
    iput-object p6, p0, LX/2N5;->g:LX/2NF;

    .line 398480
    iput-object p7, p0, LX/2N5;->h:LX/2NG;

    .line 398481
    iput-object p8, p0, LX/2N5;->i:LX/0Or;

    .line 398482
    iput-object p9, p0, LX/2N5;->j:LX/0Ot;

    .line 398483
    return-void
.end method

.method public static a(LX/0QB;)LX/2N5;
    .locals 13

    .prologue
    .line 398458
    sget-object v0, LX/2N5;->k:LX/2N5;

    if-nez v0, :cond_1

    .line 398459
    const-class v1, LX/2N5;

    monitor-enter v1

    .line 398460
    :try_start_0
    sget-object v0, LX/2N5;->k:LX/2N5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 398461
    if-eqz v2, :cond_0

    .line 398462
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 398463
    new-instance v3, LX/2N5;

    invoke-static {v0}, LX/2N6;->a(LX/0QB;)LX/2N6;

    move-result-object v4

    check-cast v4, LX/2N6;

    invoke-static {v0}, LX/2N7;->b(LX/0QB;)LX/2N7;

    move-result-object v5

    check-cast v5, LX/2N7;

    invoke-static {v0}, LX/2NA;->b(LX/0QB;)LX/2NA;

    move-result-object v6

    check-cast v6, LX/2NA;

    invoke-static {v0}, LX/2ND;->b(LX/0QB;)LX/2ND;

    move-result-object v7

    check-cast v7, LX/2ND;

    invoke-static {v0}, LX/2NE;->b(LX/0QB;)LX/2NE;

    move-result-object v8

    check-cast v8, LX/2NE;

    invoke-static {v0}, LX/2NF;->a(LX/0QB;)LX/2NF;

    move-result-object v9

    check-cast v9, LX/2NF;

    invoke-static {v0}, LX/2NG;->b(LX/0QB;)LX/2NG;

    move-result-object v10

    check-cast v10, LX/2NG;

    const/16 v11, 0x274b

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0xd1b

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, LX/2N5;-><init>(LX/2N6;LX/2N7;LX/2NA;LX/2ND;LX/2NE;LX/2NF;LX/2NG;LX/0Or;LX/0Ot;)V

    .line 398464
    move-object v0, v3

    .line 398465
    sput-object v0, LX/2N5;->k:LX/2N5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398466
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 398467
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 398468
    :cond_1
    sget-object v0, LX/2N5;->k:LX/2N5;

    return-object v0

    .line 398469
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 398470
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)LX/6dO;
    .locals 1

    .prologue
    .line 398472
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/2N5;->a(Landroid/database/Cursor;Z)LX/6dO;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/database/Cursor;Z)LX/6dO;
    .locals 1

    .prologue
    .line 398471
    new-instance v0, LX/6dO;

    invoke-direct {v0, p0, p1, p2}, LX/6dO;-><init>(LX/2N5;Landroid/database/Cursor;Z)V

    return-object v0
.end method
