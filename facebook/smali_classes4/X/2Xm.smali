.class public LX/2Xm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0lC;

.field private final b:LX/03V;


# direct methods
.method public constructor <init>(LX/0lC;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 420191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420192
    iput-object p1, p0, LX/2Xm;->a:LX/0lC;

    .line 420193
    iput-object p2, p0, LX/2Xm;->b:LX/03V;

    .line 420194
    return-void
.end method

.method public static b(LX/0QB;)LX/2Xm;
    .locals 3

    .prologue
    .line 420195
    new-instance v2, LX/2Xm;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v2, v0, v1}, LX/2Xm;-><init>(LX/0lC;LX/03V;)V

    .line 420196
    return-object v2
.end method


# virtual methods
.method public final a(LX/0lF;Ljava/lang/String;ZLjava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;
    .locals 14

    .prologue
    .line 420197
    const-string v1, "uid"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 420198
    const-string v1, "access_token"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 420199
    const-string v1, "user_storage_key"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const-string v4, ""

    invoke-static {v1, v4}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 420200
    const-string v1, "user_storage_key_prev"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const-string v4, ""

    invoke-static {v1, v4}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 420201
    const-string v1, "machine_id"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v10

    .line 420202
    const-string v1, "secret"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 420203
    const-string v1, "session_cookies"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    check-cast v1, LX/162;

    .line 420204
    const-string v4, "session_key"

    invoke-virtual {p1, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 420205
    const-string v4, "confirmed"

    invoke-virtual {p1, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->g(LX/0lF;)Z

    move-result v4

    invoke-static {v4}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v11

    .line 420206
    const/4 v4, 0x0

    .line 420207
    if-eqz p3, :cond_0

    .line 420208
    if-eqz v1, :cond_1

    .line 420209
    :try_start_0
    iget-object v7, p0, LX/2Xm;->a:LX/0lC;

    invoke-virtual {v7, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 420210
    :cond_0
    :goto_0
    new-instance v1, Lcom/facebook/auth/credentials/FacebookCredentials;

    move-object/from16 v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/facebook/auth/credentials/FacebookCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 420211
    new-instance v3, Lcom/facebook/auth/protocol/AuthenticationResultImpl;

    move-object v4, v2

    move-object v5, v1

    move-object v6, v10

    move-object v7, v11

    invoke-direct/range {v3 .. v9}, Lcom/facebook/auth/protocol/AuthenticationResultImpl;-><init>(Ljava/lang/String;Lcom/facebook/auth/credentials/FacebookCredentials;Ljava/lang/String;LX/03R;Ljava/lang/String;Ljava/lang/String;)V

    return-object v3

    .line 420212
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/2Xm;->b:LX/03V;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p4

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v12, ":cookiesArrayMissing"

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v12, "server did not return session cookie when asked."

    invoke-virtual {v1, v7, v12}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 420213
    :catch_0
    move-exception v1

    .line 420214
    iget-object v7, p0, LX/2Xm;->b:LX/03V;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p4

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ":IOException"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, "Unable to serialize session cookie."

    invoke-virtual {v7, v12, v13, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
