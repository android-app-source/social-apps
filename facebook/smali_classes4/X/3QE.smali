.class public LX/3QE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3QE;


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 563406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563407
    iput-object p1, p0, LX/3QE;->a:LX/0ad;

    .line 563408
    return-void
.end method

.method public static a(LX/0QB;)LX/3QE;
    .locals 4

    .prologue
    .line 563410
    sget-object v0, LX/3QE;->b:LX/3QE;

    if-nez v0, :cond_1

    .line 563411
    const-class v1, LX/3QE;

    monitor-enter v1

    .line 563412
    :try_start_0
    sget-object v0, LX/3QE;->b:LX/3QE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 563413
    if-eqz v2, :cond_0

    .line 563414
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 563415
    new-instance p0, LX/3QE;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/3QE;-><init>(LX/0ad;)V

    .line 563416
    move-object v0, p0

    .line 563417
    sput-object v0, LX/3QE;->b:LX/3QE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 563418
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 563419
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 563420
    :cond_1
    sget-object v0, LX/3QE;->b:LX/3QE;

    return-object v0

    .line 563421
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 563422
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 563409
    iget-object v0, p0, LX/3QE;->a:LX/0ad;

    sget-short v1, LX/15r;->E:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
