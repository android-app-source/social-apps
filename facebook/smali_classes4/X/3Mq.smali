.class public LX/3Mq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final e:[Ljava/lang/String;

.field public static final f:[Ljava/lang/String;

.field private static volatile g:LX/3Mq;


# instance fields
.field public final b:Landroid/content/ContentResolver;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dB;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/2N5;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 556360
    const-class v0, LX/3Mq;

    sput-object v0, LX/3Mq;->a:Ljava/lang/Class;

    .line 556361
    sget-object v0, LX/2N5;->a:[Ljava/lang/String;

    const-string v1, "timestamp_in_folder_ms"

    invoke-static {v0, v1}, LX/0P8;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sput-object v0, LX/3Mq;->e:[Ljava/lang/String;

    .line 556362
    sget-object v0, LX/2N5;->a:[Ljava/lang/String;

    const-string v1, "pinned_threads_display_order"

    invoke-static {v0, v1}, LX/0P8;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sput-object v0, LX/3Mq;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/0Or;LX/2N5;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "LX/0Or",
            "<",
            "LX/6dB;",
            ">;",
            "LX/2N5;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 556363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 556364
    iput-object p1, p0, LX/3Mq;->b:Landroid/content/ContentResolver;

    .line 556365
    iput-object p2, p0, LX/3Mq;->c:LX/0Or;

    .line 556366
    iput-object p3, p0, LX/3Mq;->d:LX/2N5;

    .line 556367
    return-void
.end method

.method public static a(LX/5e9;LX/6ek;)LX/0uw;
    .locals 4

    .prologue
    .line 556368
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v0

    .line 556369
    const-string v1, "folder"

    iget-object v2, p1, LX/6ek;->dbName:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 556370
    const-string v1, "thread_key"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/5e9;->dbValue:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 556371
    return-object v0
.end method

.method public static a(LX/0QB;)LX/3Mq;
    .locals 6

    .prologue
    .line 556372
    sget-object v0, LX/3Mq;->g:LX/3Mq;

    if-nez v0, :cond_1

    .line 556373
    const-class v1, LX/3Mq;

    monitor-enter v1

    .line 556374
    :try_start_0
    sget-object v0, LX/3Mq;->g:LX/3Mq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 556375
    if-eqz v2, :cond_0

    .line 556376
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 556377
    new-instance v5, LX/3Mq;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    const/16 v4, 0x2747

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2N5;->a(LX/0QB;)LX/2N5;

    move-result-object v4

    check-cast v4, LX/2N5;

    invoke-direct {v5, v3, p0, v4}, LX/3Mq;-><init>(Landroid/content/ContentResolver;LX/0Or;LX/2N5;)V

    .line 556378
    move-object v0, v5

    .line 556379
    sput-object v0, LX/3Mq;->g:LX/3Mq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 556380
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 556381
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 556382
    :cond_1
    sget-object v0, LX/3Mq;->g:LX/3Mq;

    return-object v0

    .line 556383
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 556384
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3Mq;LX/0ux;Ljava/lang/String;I)LX/6dO;
    .locals 6

    .prologue
    .line 556385
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 556386
    if-lez p3, :cond_0

    .line 556387
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIMIT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 556388
    :cond_0
    iget-object v0, p0, LX/3Mq;->b:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/3Mq;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6dB;

    iget-object v1, v1, LX/6dB;->c:LX/6dA;

    invoke-virtual {v1}, LX/6dA;->a()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/2N5;->a:[Ljava/lang/String;

    invoke-virtual {p1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 556389
    iget-object v1, p0, LX/3Mq;->d:LX/2N5;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/2N5;->a(Landroid/database/Cursor;Z)LX/6dO;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/Collection;)LX/6dO;
    .locals 6
    .param p2    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)",
            "LX/6dO;"
        }
    .end annotation

    .prologue
    .line 556390
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 556391
    const-string v0, "folder"

    sget-object v1, LX/6ek;->INBOX:LX/6ek;

    iget-object v1, v1, LX/6ek;->dbName:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 556392
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 556393
    const-string v0, "thread_key"

    invoke-static {v0, p2}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 556394
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 556395
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "name LIKE "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0uu;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 556396
    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    .line 556397
    iget-object v0, p0, LX/3Mq;->b:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/3Mq;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6dB;

    iget-object v1, v1, LX/6dB;->c:LX/6dA;

    invoke-virtual {v1}, LX/6dA;->a()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/3Mq;->e:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const-string v5, "timestamp_in_folder_ms DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 556398
    iget-object v1, p0, LX/3Mq;->d:LX/2N5;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/2N5;->a(Landroid/database/Cursor;Z)LX/6dO;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Set;)LX/6dO;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)",
            "LX/6dO;"
        }
    .end annotation

    .prologue
    .line 556399
    const-string v0, "thread_key"

    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v4

    .line 556400
    iget-object v0, p0, LX/3Mq;->b:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/3Mq;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6dB;

    iget-object v1, v1, LX/6dB;->c:LX/6dA;

    invoke-virtual {v1}, LX/6dA;->a()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/2N5;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 556401
    iget-object v1, p0, LX/3Mq;->d:LX/2N5;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/2N5;->a(Landroid/database/Cursor;Z)LX/6dO;

    move-result-object v0

    return-object v0
.end method
