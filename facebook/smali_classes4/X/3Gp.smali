.class public LX/3Gp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile d:LX/3Gp;


# instance fields
.field private final b:LX/0Sh;

.field private final c:LX/0tX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 541850
    const-class v0, LX/3Gp;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Gp;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 541851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 541852
    iput-object p1, p0, LX/3Gp;->b:LX/0Sh;

    .line 541853
    iput-object p2, p0, LX/3Gp;->c:LX/0tX;

    .line 541854
    return-void
.end method

.method private static a(Ljava/lang/String;Z)LX/0jT;
    .locals 9

    .prologue
    .line 541855
    new-instance v0, LX/7Im;

    invoke-direct {v0}, LX/7Im;-><init>()V

    .line 541856
    iput-object p0, v0, LX/7Im;->b:Ljava/lang/String;

    .line 541857
    move-object v0, v0

    .line 541858
    iput-boolean p1, v0, LX/7Im;->c:Z

    .line 541859
    move-object v0, v0

    .line 541860
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 541861
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 541862
    iget-object v3, v0, LX/7Im;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 541863
    iget-object v5, v0, LX/7Im;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 541864
    const/4 v7, 0x3

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 541865
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 541866
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 541867
    const/4 v3, 0x2

    iget-boolean v5, v0, LX/7Im;->c:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 541868
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 541869
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 541870
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 541871
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 541872
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 541873
    new-instance v3, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    invoke-direct {v3, v2}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;-><init>(LX/15i;)V

    .line 541874
    move-object v0, v3

    .line 541875
    new-instance v1, LX/7Il;

    invoke-direct {v1}, LX/7Il;-><init>()V

    .line 541876
    iput-object v0, v1, LX/7Il;->a:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    .line 541877
    move-object v0, v1

    .line 541878
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 541879
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 541880
    iget-object v3, v0, LX/7Il;->a:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 541881
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 541882
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 541883
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 541884
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 541885
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 541886
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 541887
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 541888
    new-instance v3, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;

    invoke-direct {v3, v2}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;-><init>(LX/15i;)V

    .line 541889
    move-object v0, v3

    .line 541890
    return-object v0
.end method

.method public static a(LX/0QB;)LX/3Gp;
    .locals 5

    .prologue
    .line 541891
    sget-object v0, LX/3Gp;->d:LX/3Gp;

    if-nez v0, :cond_1

    .line 541892
    const-class v1, LX/3Gp;

    monitor-enter v1

    .line 541893
    :try_start_0
    sget-object v0, LX/3Gp;->d:LX/3Gp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 541894
    if-eqz v2, :cond_0

    .line 541895
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 541896
    new-instance p0, LX/3Gp;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-direct {p0, v3, v4}, LX/3Gp;-><init>(LX/0Sh;LX/0tX;)V

    .line 541897
    move-object v0, p0

    .line 541898
    sput-object v0, LX/3Gp;->d:LX/3Gp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 541899
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 541900
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 541901
    :cond_1
    sget-object v0, LX/3Gp;->d:LX/3Gp;

    return-object v0

    .line 541902
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 541903
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(Ljava/lang/String;Z)LX/0jT;
    .locals 10

    .prologue
    .line 541904
    new-instance v0, LX/7Ij;

    invoke-direct {v0}, LX/7Ij;-><init>()V

    .line 541905
    iput-object p0, v0, LX/7Ij;->b:Ljava/lang/String;

    .line 541906
    move-object v0, v0

    .line 541907
    iput-boolean p1, v0, LX/7Ij;->d:Z

    .line 541908
    move-object v0, v0

    .line 541909
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 541910
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 541911
    iget-object v3, v0, LX/7Ij;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 541912
    iget-object v5, v0, LX/7Ij;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 541913
    iget-object v7, v0, LX/7Ij;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v2, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 541914
    const/4 v8, 0x4

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 541915
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 541916
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 541917
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 541918
    const/4 v3, 0x3

    iget-boolean v5, v0, LX/7Ij;->d:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 541919
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 541920
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 541921
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 541922
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 541923
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 541924
    new-instance v3, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    invoke-direct {v3, v2}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;-><init>(LX/15i;)V

    .line 541925
    move-object v0, v3

    .line 541926
    new-instance v1, LX/7Ii;

    invoke-direct {v1}, LX/7Ii;-><init>()V

    .line 541927
    iput-object v0, v1, LX/7Ii;->a:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    .line 541928
    move-object v0, v1

    .line 541929
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 541930
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 541931
    iget-object v3, v0, LX/7Ii;->a:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 541932
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 541933
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 541934
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 541935
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 541936
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 541937
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 541938
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 541939
    new-instance v3, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;

    invoke-direct {v3, v2}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;-><init>(LX/15i;)V

    .line 541940
    move-object v0, v3

    .line 541941
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelSubscriptionSurfaces;
        .end annotation
    .end param

    .prologue
    .line 541942
    new-instance v0, LX/4KI;

    invoke-direct {v0}, LX/4KI;-><init>()V

    .line 541943
    const-string v1, "video_channel_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 541944
    move-object v0, v0

    .line 541945
    const-string v1, "surface"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 541946
    move-object v0, v0

    .line 541947
    new-instance v1, LX/7Ia;

    invoke-direct {v1}, LX/7Ia;-><init>()V

    move-object v1, v1

    .line 541948
    const-string v2, "video_channel_subscribe_data"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 541949
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p1, v1}, LX/3Gp;->a(Ljava/lang/String;Z)LX/0jT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 541950
    iget-object v1, p0, LX/3Gp;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 541951
    iget-object v1, p0, LX/3Gp;->b:LX/0Sh;

    new-instance v2, LX/7Ie;

    invoke-direct {v2, p0}, LX/7Ie;-><init>(LX/3Gp;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 541952
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelFollowSurfaces;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 541953
    new-instance v0, LX/4KG;

    invoke-direct {v0}, LX/4KG;-><init>()V

    .line 541954
    const-string v1, "video_channel_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 541955
    move-object v0, v0

    .line 541956
    const-string v1, "surface"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 541957
    move-object v0, v0

    .line 541958
    if-eqz p3, :cond_0

    .line 541959
    const-string v1, "video_id"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 541960
    :cond_0
    new-instance v1, LX/7IZ;

    invoke-direct {v1}, LX/7IZ;-><init>()V

    move-object v1, v1

    .line 541961
    const-string v2, "video_channel_follow_data"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 541962
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p1, v1}, LX/3Gp;->b(Ljava/lang/String;Z)LX/0jT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 541963
    iget-object v1, p0, LX/3Gp;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 541964
    iget-object v1, p0, LX/3Gp;->b:LX/0Sh;

    new-instance v2, LX/7Ig;

    invoke-direct {v2, p0}, LX/7Ig;-><init>(LX/3Gp;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 541965
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelSubscriptionSurfaces;
        .end annotation
    .end param

    .prologue
    .line 541966
    new-instance v0, LX/4KK;

    invoke-direct {v0}, LX/4KK;-><init>()V

    .line 541967
    const-string v1, "video_channel_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 541968
    move-object v0, v0

    .line 541969
    const-string v1, "surface"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 541970
    move-object v0, v0

    .line 541971
    new-instance v1, LX/7Ic;

    invoke-direct {v1}, LX/7Ic;-><init>()V

    move-object v1, v1

    .line 541972
    const-string v2, "video_channel_unsubscribe_data"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 541973
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v1}, LX/3Gp;->a(Ljava/lang/String;Z)LX/0jT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 541974
    iget-object v1, p0, LX/3Gp;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 541975
    iget-object v1, p0, LX/3Gp;->b:LX/0Sh;

    new-instance v2, LX/7If;

    invoke-direct {v2, p0}, LX/7If;-><init>(LX/3Gp;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 541976
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelFollowSurfaces;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 541977
    new-instance v0, LX/4KJ;

    invoke-direct {v0}, LX/4KJ;-><init>()V

    .line 541978
    const-string v1, "video_channel_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 541979
    move-object v0, v0

    .line 541980
    const-string v1, "surface"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 541981
    move-object v0, v0

    .line 541982
    if-eqz p3, :cond_0

    .line 541983
    const-string v1, "video_id"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 541984
    :cond_0
    new-instance v1, LX/7Ib;

    invoke-direct {v1}, LX/7Ib;-><init>()V

    move-object v1, v1

    .line 541985
    const-string v2, "video_channel_unfollow_data"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 541986
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v1}, LX/3Gp;->b(Ljava/lang/String;Z)LX/0jT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 541987
    iget-object v1, p0, LX/3Gp;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 541988
    iget-object v1, p0, LX/3Gp;->b:LX/0Sh;

    new-instance v2, LX/7Ih;

    invoke-direct {v2, p0}, LX/7Ih;-><init>(LX/3Gp;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 541989
    return-void
.end method
