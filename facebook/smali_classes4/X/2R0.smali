.class public LX/2R0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/platform/opengraph/server/GetRobotextPreviewMethod$Params;",
        "Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0lp;

.field private final b:LX/0lC;


# direct methods
.method public constructor <init>(LX/0lp;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409357
    iput-object p1, p0, LX/2R0;->a:LX/0lp;

    .line 409358
    iput-object p2, p0, LX/2R0;->b:LX/0lC;

    .line 409359
    return-void
.end method

.method public static b(LX/0QB;)LX/2R0;
    .locals 3

    .prologue
    .line 409360
    new-instance v2, LX/2R0;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v0

    check-cast v0, LX/0lp;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-direct {v2, v0, v1}, LX/2R0;-><init>(LX/0lp;LX/0lC;)V

    .line 409361
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 409362
    check-cast p1, Lcom/facebook/platform/opengraph/server/GetRobotextPreviewMethod$Params;

    .line 409363
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 409364
    iget-object v0, p0, LX/2R0;->a:LX/0lp;

    .line 409365
    iget-object v1, p1, Lcom/facebook/platform/opengraph/server/GetRobotextPreviewMethod$Params;->a:Ljava/lang/String;

    move-object v1, v1

    .line 409366
    invoke-virtual {v0, v1}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v0

    invoke-virtual {v0}, LX/15w;->J()LX/0lG;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 409367
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v2

    .line 409368
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 409369
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 409370
    const-string v1, "image"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 409371
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 409372
    invoke-virtual {v1}, LX/0lF;->o()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    .line 409373
    :goto_1
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v3, v0, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 409374
    :cond_1
    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 409375
    :cond_2
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "preview"

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409376
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "proxied_app_id"

    .line 409377
    iget-object v2, p1, Lcom/facebook/platform/opengraph/server/GetRobotextPreviewMethod$Params;->c:Ljava/lang/String;

    move-object v2, v2

    .line 409378
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409379
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "android_key_hash"

    .line 409380
    iget-object v2, p1, Lcom/facebook/platform/opengraph/server/GetRobotextPreviewMethod$Params;->d:Ljava/lang/String;

    move-object v2, v2

    .line 409381
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409382
    iget-object v0, p1, Lcom/facebook/platform/opengraph/server/GetRobotextPreviewMethod$Params;->e:Ljava/lang/String;

    move-object v0, v0

    .line 409383
    if-eqz v0, :cond_3

    .line 409384
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "fb:channel"

    .line 409385
    iget-object v2, p1, Lcom/facebook/platform/opengraph/server/GetRobotextPreviewMethod$Params;->e:Ljava/lang/String;

    move-object v2, v2

    .line 409386
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409387
    :cond_3
    new-instance v0, LX/14N;

    const-string v1, "get_robotext_preview_method"

    const-string v2, "POST"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "me/"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 409388
    iget-object v5, p1, Lcom/facebook/platform/opengraph/server/GetRobotextPreviewMethod$Params;->b:Ljava/lang/String;

    move-object v5, v5

    .line 409389
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 409390
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 409391
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 409392
    if-eqz v0, :cond_0

    const-string v1, "data"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 409393
    const-string v1, "data"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    .line 409394
    :cond_0
    invoke-virtual {v0}, LX/0lF;->c()LX/15w;

    move-result-object v0

    .line 409395
    iget-object v1, p0, LX/2R0;->b:LX/0lC;

    invoke-virtual {v0, v1}, LX/15w;->a(LX/0lD;)V

    .line 409396
    const-class v1, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotextComposer;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

    return-object v0
.end method
