.class public LX/37Z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/0Tn;

.field private static volatile k:LX/37Z;


# instance fields
.field public c:LX/37d;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/37e;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/37f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/37a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final g:LX/37b;

.field private final h:Ljava/lang/String;

.field public final i:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/facebook/video/chromecast/CastApplicationManager$CastApplicationListener;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/2wX;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 501666
    const-class v0, LX/37Z;

    sput-object v0, LX/37Z;->a:Ljava/lang/Class;

    .line 501667
    sget-object v0, LX/37a;->a:LX/0Tn;

    const-string v1, "session-id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/37Z;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0ad;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 501574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501575
    new-instance v0, LX/37b;

    sget-object v1, LX/37c;->DISCONNECTED:LX/37c;

    invoke-direct {v0, p0, v1}, LX/37b;-><init>(LX/37Z;LX/37c;)V

    iput-object v0, p0, LX/37Z;->g:LX/37b;

    .line 501576
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LX/37Z;->i:Ljava/util/Vector;

    .line 501577
    sget-char v0, LX/0ws;->Y:C

    const-string v1, "urn:x-cast:com.facebook.fb"

    invoke-interface {p1, v0, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/37Z;->h:Ljava/lang/String;

    .line 501578
    return-void
.end method

.method public static a(LX/0QB;)LX/37Z;
    .locals 7

    .prologue
    .line 501651
    sget-object v0, LX/37Z;->k:LX/37Z;

    if-nez v0, :cond_1

    .line 501652
    const-class v1, LX/37Z;

    monitor-enter v1

    .line 501653
    :try_start_0
    sget-object v0, LX/37Z;->k:LX/37Z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 501654
    if-eqz v2, :cond_0

    .line 501655
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 501656
    new-instance p0, LX/37Z;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/37Z;-><init>(LX/0ad;)V

    .line 501657
    invoke-static {v0}, LX/37d;->a(LX/0QB;)LX/37d;

    move-result-object v3

    check-cast v3, LX/37d;

    invoke-static {v0}, LX/37e;->a(LX/0QB;)LX/37e;

    move-result-object v4

    check-cast v4, LX/37e;

    invoke-static {v0}, LX/37f;->a(LX/0QB;)LX/37f;

    move-result-object v5

    check-cast v5, LX/37f;

    invoke-static {v0}, LX/37a;->a(LX/0QB;)LX/37a;

    move-result-object v6

    check-cast v6, LX/37a;

    .line 501658
    iput-object v3, p0, LX/37Z;->c:LX/37d;

    iput-object v4, p0, LX/37Z;->d:LX/37e;

    iput-object v5, p0, LX/37Z;->e:LX/37f;

    iput-object v6, p0, LX/37Z;->f:LX/37a;

    .line 501659
    move-object v0, p0

    .line 501660
    sput-object v0, LX/37Z;->k:LX/37Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 501661
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 501662
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 501663
    :cond_1
    sget-object v0, LX/37Z;->k:LX/37Z;

    return-object v0

    .line 501664
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 501665
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/37Z;LX/2wX;LX/7Yd;)V
    .locals 11

    .prologue
    .line 501600
    iget-object v0, p0, LX/37Z;->f:LX/37a;

    sget-object v1, LX/37Z;->b:LX/0Tn;

    invoke-interface {p2}, LX/7Yd;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/37a;->a(LX/0Tn;Ljava/lang/String;)V

    .line 501601
    iget-object v0, p0, LX/37Z;->g:LX/37b;

    sget-object v1, LX/37c;->CONNECTED:LX/37c;

    invoke-static {v0, v1}, LX/37b;->a$redex0(LX/37b;LX/37c;)V

    .line 501602
    const/4 v0, 0x0

    .line 501603
    invoke-interface {p2}, LX/7Yd;->b()Lcom/google/android/gms/cast/ApplicationMetadata;

    move-result-object v1

    .line 501604
    if-eqz v1, :cond_4

    .line 501605
    iget-object v2, p0, LX/37Z;->h:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/android/gms/cast/ApplicationMetadata;->d:Ljava/util/List;

    if-eqz v3, :cond_5

    iget-object v3, v1, Lcom/google/android/gms/cast/ApplicationMetadata;->d:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    :goto_0
    move v1, v3

    .line 501606
    if-eqz v1, :cond_4

    .line 501607
    iget-object v0, p0, LX/37Z;->h:Ljava/lang/String;

    move-object v1, v0

    .line 501608
    :goto_1
    iget-object v0, p0, LX/37Z;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v3, :cond_3

    iget-object v0, p0, LX/37Z;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38m;

    .line 501609
    const/4 v4, 0x0

    .line 501610
    iget-object v5, v0, LX/38m;->a:LX/37Y;

    iget-boolean v5, v5, LX/37Y;->j:Z

    if-eqz v5, :cond_6

    .line 501611
    iget-object v5, v0, LX/38m;->a:LX/37Y;

    const/4 v6, 0x0

    .line 501612
    iget-object v7, v5, LX/37Y;->g:LX/37a;

    sget-object v8, LX/7JP;->b:LX/0Tn;

    invoke-virtual {v7, v8}, LX/37a;->b(LX/0Tn;)Ljava/lang/String;

    move-result-object v7

    .line 501613
    iget-object v8, v5, LX/37Y;->g:LX/37a;

    sget-object v9, LX/7JP;->c:LX/0Tn;

    invoke-virtual {v8, v9}, LX/37a;->b(LX/0Tn;)Ljava/lang/String;

    move-result-object v8

    .line 501614
    iget-object v9, v5, LX/37Y;->g:LX/37a;

    sget-object v10, LX/7JP;->a:LX/0Tn;

    invoke-virtual {v9, v10}, LX/37a;->b(LX/0Tn;)Ljava/lang/String;

    move-result-object v9

    .line 501615
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 501616
    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 501617
    invoke-static {v9}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v6

    .line 501618
    :cond_0
    invoke-static {}, LX/7JM;->newBuilder()LX/7JM;

    move-result-object v9

    invoke-virtual {v9, v7}, LX/7JM;->b(Ljava/lang/String;)LX/7JM;

    move-result-object v7

    invoke-virtual {v7, v8}, LX/7JM;->a(Ljava/lang/String;)LX/7JM;

    move-result-object v7

    .line 501619
    iput-object v6, v7, LX/7JM;->k:LX/1bf;

    .line 501620
    move-object v6, v7

    .line 501621
    invoke-virtual {v6}, LX/7JM;->a()LX/7JN;

    move-result-object v6

    .line 501622
    :cond_1
    iput-object v6, v5, LX/37Y;->r:LX/7JN;

    .line 501623
    iget-object v5, v0, LX/38m;->a:LX/37Y;

    iget-object v5, v5, LX/37Y;->f:LX/38o;

    iget-object v6, v0, LX/38m;->a:LX/37Y;

    iget-object v6, v6, LX/37Y;->r:LX/7JN;

    invoke-virtual {v5, v6}, LX/38o;->b(LX/7JN;)V

    .line 501624
    :goto_3
    if-eqz v1, :cond_7

    .line 501625
    iget-object v5, v0, LX/38m;->a:LX/37Y;

    iget-object v5, v5, LX/37Y;->p:LX/38g;

    new-instance v6, LX/7JL;

    iget-object v7, v0, LX/38m;->a:LX/37Y;

    invoke-direct {v6, v7}, LX/7JL;-><init>(LX/37Y;)V

    iget-object v4, v0, LX/38m;->a:LX/37Y;

    iget-boolean v4, v4, LX/37Y;->j:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v7, 0x1

    .line 501626
    invoke-virtual {v5}, LX/38g;->a()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 501627
    :goto_4
    move v4, v7

    .line 501628
    :goto_5
    if-nez v4, :cond_2

    .line 501629
    iget-object v4, v0, LX/38m;->a:LX/37Y;

    iget-object v4, v4, LX/37Y;->l:LX/37g;

    invoke-virtual {v4}, LX/37g;->b()V

    .line 501630
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_2

    .line 501631
    :cond_3
    return-void

    :cond_4
    move-object v1, v0

    goto/16 :goto_1

    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 501632
    :cond_6
    iget-object v5, v0, LX/38m;->a:LX/37Y;

    iget-object v5, v5, LX/37Y;->f:LX/38o;

    iget-object v6, v0, LX/38m;->a:LX/37Y;

    iget-object v6, v6, LX/37Y;->s:LX/7JN;

    invoke-virtual {v5, v6}, LX/38o;->a(LX/7JN;)V

    goto :goto_3

    .line 501633
    :cond_7
    iget-object v5, v0, LX/38m;->a:LX/37Y;

    iget-object v5, v5, LX/37Y;->c:LX/37f;

    const/16 v6, 0xd

    const-string v7, "VideoCastManager.setupApplicationListener: onApplicationConnected: No Namespace"

    invoke-virtual {v5, v6, v7}, LX/37f;->a(ILjava/lang/String;)V

    goto :goto_5

    .line 501634
    :cond_8
    iput-object p1, v5, LX/38g;->g:LX/2wX;

    .line 501635
    iput-object v6, v5, LX/38g;->h:LX/38i;

    .line 501636
    iput-object v4, v5, LX/38g;->i:Ljava/lang/Object;

    .line 501637
    new-instance v8, LX/7JI;

    iget-object v9, v5, LX/38g;->e:LX/0SF;

    iget-object v10, v5, LX/38g;->g:LX/2wX;

    invoke-direct {v8, v5, v9, v10, v1}, LX/7JI;-><init>(LX/38g;LX/0SF;LX/2wX;Ljava/lang/String;)V

    iput-object v8, v5, LX/38g;->f:LX/7JI;

    .line 501638
    :try_start_0
    sget-object v8, LX/7Yq;->c:LX/7Yl;

    iget-object v9, v5, LX/38g;->g:LX/2wX;

    iget-object v10, v5, LX/38g;->f:LX/7JI;

    .line 501639
    iget-object p2, v10, LX/7JI;->d:LX/7JH;

    move-object v10, p2

    .line 501640
    invoke-interface {v8, v9, v1, v10}, LX/7Yl;->a(LX/2wX;Ljava/lang/String;LX/7JG;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 501641
    iget-object v8, v5, LX/38g;->f:LX/7JI;

    .line 501642
    iget v9, v8, LX/7JI;->g:I

    if-eqz v9, :cond_9

    .line 501643
    iget-object v9, v8, LX/7JI;->a:LX/38g;

    iget-object v9, v9, LX/38g;->c:LX/37e;

    sget-object v10, LX/7JJ;->FbAppPlayer_Start:LX/7JJ;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v5, "Incorrect state: "

    invoke-direct {p2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v8, LX/7JI;->g:I

    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v9, v10, p2}, LX/37e;->a(LX/7JJ;Ljava/lang/String;)V

    .line 501644
    :cond_9
    const/4 v9, 0x1

    invoke-static {v8, v9}, LX/7JI;->a(LX/7JI;I)V

    .line 501645
    const-string v9, "version_request"

    const-string v10, "version"

    const-string p2, "{\"version\":\"1\"}"

    new-instance v5, LX/7J9;

    invoke-direct {v5, v8}, LX/7J9;-><init>(LX/7JI;)V

    invoke-static {v8, v9, v10, p2, v5}, LX/7JI;->a(LX/7JI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/27U;)V

    .line 501646
    goto :goto_4

    .line 501647
    :catch_0
    move-exception v7

    .line 501648
    :goto_6
    iget-object v8, v5, LX/38g;->d:LX/37f;

    const/16 v9, 0xd

    new-instance v10, Ljava/lang/StringBuilder;

    const-string p2, "CastPlayer.attach:setMessageReceivedCallbacks: "

    invoke-direct {v10, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v9, v7}, LX/37f;->a(ILjava/lang/String;)V

    .line 501649
    const/4 v7, 0x0

    goto/16 :goto_4

    .line 501650
    :catch_1
    move-exception v7

    goto :goto_6
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 501599
    iget-object v0, p0, LX/37Z;->f:LX/37a;

    sget-object v1, LX/37Z;->b:LX/0Tn;

    invoke-virtual {v0, v1}, LX/37a;->b(LX/0Tn;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 7

    .prologue
    .line 501590
    invoke-virtual {p0}, LX/37Z;->b()V

    .line 501591
    iget-object v0, p0, LX/37Z;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/37Z;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38m;

    .line 501592
    const/4 p2, 0x0

    .line 501593
    iget-object v3, v0, LX/38m;->a:LX/37Y;

    iget-boolean v3, v3, LX/37Y;->j:Z

    if-eqz v3, :cond_0

    .line 501594
    iget-object v3, v0, LX/38m;->a:LX/37Y;

    iget-object v3, v3, LX/37Y;->g:LX/37a;

    const/4 v4, 0x3

    new-array v4, v4, [LX/0Tn;

    sget-object v5, LX/7JP;->a:LX/0Tn;

    aput-object v5, v4, p2

    const/4 v5, 0x1

    sget-object v6, LX/7JP;->b:LX/0Tn;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    sget-object v6, LX/7JP;->c:LX/0Tn;

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, LX/37a;->a([LX/0Tn;)V

    .line 501595
    :cond_0
    iget-object v3, v0, LX/38m;->a:LX/37Y;

    iget-object v3, v3, LX/37Y;->l:LX/37g;

    invoke-virtual {v3}, LX/37g;->b()V

    .line 501596
    iget-object v3, v0, LX/38m;->a:LX/37Y;

    iget-object v3, v3, LX/37Y;->f:LX/38o;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, LX/38m;->a:LX/37Y;

    iget-object v5, v5, LX/37Y;->r:LX/7JN;

    iget-object v6, v0, LX/38m;->a:LX/37Y;

    iget-object v6, v6, LX/37Y;->n:LX/38j;

    invoke-virtual {v6}, LX/38j;->a()Z

    move-result v6

    invoke-virtual {v3, v4, v5, v6, p2}, LX/38o;->a(Ljava/lang/String;LX/7JN;ZI)V

    .line 501597
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 501598
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 501579
    iget-object v0, p0, LX/37Z;->g:LX/37b;

    .line 501580
    iget-object v1, v0, LX/37b;->b:LX/37c;

    sget-object v2, LX/37c;->DISCONNECTED:LX/37c;

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 501581
    if-eqz v0, :cond_1

    .line 501582
    :cond_0
    :goto_1
    return-void

    .line 501583
    :cond_1
    iget-object v0, p0, LX/37Z;->g:LX/37b;

    sget-object v1, LX/37c;->DISCONNECTED:LX/37c;

    invoke-static {v0, v1}, LX/37b;->a$redex0(LX/37b;LX/37c;)V

    .line 501584
    invoke-virtual {p0}, LX/37Z;->a()Ljava/lang/String;

    move-result-object v0

    .line 501585
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 501586
    :try_start_0
    sget-object v1, LX/7Yq;->c:LX/7Yl;

    iget-object v2, p0, LX/37Z;->j:LX/2wX;

    invoke-interface {v1, v2, v0}, LX/7Yl;->a(LX/2wX;Ljava/lang/String;)LX/2wg;

    move-result-object v0

    new-instance v1, LX/7J6;

    invoke-direct {v1, p0}, LX/7J6;-><init>(LX/37Z;)V

    invoke-virtual {v0, v1}, LX/2wg;->a(LX/27U;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 501587
    :goto_2
    iget-object v0, p0, LX/37Z;->f:LX/37a;

    sget-object v1, LX/37Z;->b:LX/0Tn;

    invoke-virtual {v0, v1}, LX/37a;->a(LX/0Tn;)V

    goto :goto_1

    .line 501588
    :catch_0
    move-exception v0

    .line 501589
    iget-object v1, p0, LX/37Z;->d:LX/37e;

    sget-object v2, LX/7JJ;->CastApplicationManager_StopApplication:LX/7JJ;

    invoke-virtual {v1, v2, v0}, LX/37e;->a(LX/7JJ;Ljava/lang/Exception;)V

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
