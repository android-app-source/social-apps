.class public interface abstract LX/2l5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/2l6;


# virtual methods
.method public abstract a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract a()V
.end method

.method public abstract a(JI)V
.end method

.method public abstract a(LX/0nz;)V
.end method

.method public abstract b()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(LX/0nz;)V
.end method

.method public abstract c()Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract d()LX/0ta;
.end method

.method public abstract e()Lcom/facebook/bookmark/FetchBookmarksResult;
.end method

.method public abstract f()V
.end method
