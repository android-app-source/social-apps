.class public final LX/2NQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 0

    .prologue
    .line 399806
    iput-object p1, p0, LX/2NQ;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 2

    .prologue
    .line 399807
    iget-object v0, p0, LX/2NQ;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 399808
    iget-object v0, p0, LX/2NQ;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 399809
    iget-object v1, p0, LX/2NQ;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v1, v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    sub-int v1, v0, v1

    .line 399810
    div-int/lit8 v0, v0, 0x3

    if-le v1, v0, :cond_1

    .line 399811
    iget-object v0, p0, LX/2NQ;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-boolean v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    if-nez v0, :cond_0

    .line 399812
    iget-object v0, p0, LX/2NQ;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    const/4 v1, 0x1

    .line 399813
    iput-boolean v1, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    .line 399814
    iget-object v0, p0, LX/2NQ;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    .line 399815
    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->G$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    .line 399816
    :cond_0
    :goto_0
    return-void

    .line 399817
    :cond_1
    iget-object v0, p0, LX/2NQ;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-boolean v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    if-eqz v0, :cond_0

    .line 399818
    iget-object v0, p0, LX/2NQ;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    const/4 v1, 0x0

    .line 399819
    iput-boolean v1, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    .line 399820
    iget-object v0, p0, LX/2NQ;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    .line 399821
    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->G$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    .line 399822
    goto :goto_0
.end method
