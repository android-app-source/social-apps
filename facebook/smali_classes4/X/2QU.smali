.class public LX/2QU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2QU;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Qf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Yb;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Qf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 408377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 408378
    iput-object p2, p0, LX/2QU;->a:LX/0Ot;

    .line 408379
    iput-object p1, p0, LX/2QU;->b:LX/0Ot;

    .line 408380
    return-void
.end method

.method public static a(LX/0QB;)LX/2QU;
    .locals 5

    .prologue
    .line 408381
    sget-object v0, LX/2QU;->d:LX/2QU;

    if-nez v0, :cond_1

    .line 408382
    const-class v1, LX/2QU;

    monitor-enter v1

    .line 408383
    :try_start_0
    sget-object v0, LX/2QU;->d:LX/2QU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 408384
    if-eqz v2, :cond_0

    .line 408385
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 408386
    new-instance v3, LX/2QU;

    const/16 v4, 0x1c5

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0xf92

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/2QU;-><init>(LX/0Ot;LX/0Ot;)V

    .line 408387
    move-object v0, v3

    .line 408388
    sput-object v0, LX/2QU;->d:LX/2QU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408389
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 408390
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 408391
    :cond_1
    sget-object v0, LX/2QU;->d:LX/2QU;

    return-object v0

    .line 408392
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 408393
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 408394
    iget-object v0, p0, LX/2QU;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Qf;

    .line 408395
    new-instance v1, LX/2Qj;

    invoke-direct {v1, v0}, LX/2Qj;-><init>(LX/2Qf;)V

    iput-object v1, v0, LX/2Qf;->e:LX/0YZ;

    move-object v1, v1

    .line 408396
    iget-object v0, p0, LX/2QU;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    sget-object v2, LX/1iu;->a:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    sget-object v2, LX/1iu;->b:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2QU;->c:LX/0Yb;

    .line 408397
    iget-object v0, p0, LX/2QU;->c:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 408398
    return-void
.end method
