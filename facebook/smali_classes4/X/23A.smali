.class public LX/23A;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/23A;


# instance fields
.field public final b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

.field public final c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 361950
    new-instance v0, LX/23A;

    const/4 v1, 0x0

    const-wide/16 v2, -0x1

    invoke-direct {v0, v1, v2, v3}, LX/23A;-><init>(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;J)V

    sput-object v0, LX/23A;->a:LX/23A;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;J)V
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 361951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361952
    iput-object p1, p0, LX/23A;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    .line 361953
    iput-wide p2, p0, LX/23A;->c:J

    .line 361954
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    .line 361955
    iget-object v0, p0, LX/23A;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v0, v0

    .line 361956
    if-eqz v0, :cond_0

    .line 361957
    iget-wide v4, p0, LX/23A;->c:J

    move-wide v0, v4

    .line 361958
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
