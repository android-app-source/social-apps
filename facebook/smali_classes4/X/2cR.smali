.class public LX/2cR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2cF;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/2cR;


# instance fields
.field private final b:LX/0Uh;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/omnistore/Collection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 440861
    const-class v0, LX/2cR;

    sput-object v0, LX/2cR;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 440862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440863
    iput-object p1, p0, LX/2cR;->b:LX/0Uh;

    .line 440864
    iput-object p2, p0, LX/2cR;->c:LX/0Or;

    .line 440865
    return-void
.end method

.method public static a(LX/0QB;)LX/2cR;
    .locals 5

    .prologue
    .line 440866
    sget-object v0, LX/2cR;->e:LX/2cR;

    if-nez v0, :cond_1

    .line 440867
    const-class v1, LX/2cR;

    monitor-enter v1

    .line 440868
    :try_start_0
    sget-object v0, LX/2cR;->e:LX/2cR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 440869
    if-eqz v2, :cond_0

    .line 440870
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 440871
    new-instance v4, LX/2cR;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    const/16 p0, 0x15e8

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/2cR;-><init>(LX/0Uh;LX/0Or;)V

    .line 440872
    move-object v0, v4

    .line 440873
    sput-object v0, LX/2cR;->e:LX/2cR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 440874
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 440875
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 440876
    :cond_1
    sget-object v0, LX/2cR;->e:LX/2cR;

    return-object v0

    .line 440877
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 440878
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final indexObject(Ljava/lang/String;Ljava/lang/String;Ljava/nio/ByteBuffer;)Lcom/facebook/omnistore/IndexedFields;
    .locals 1

    .prologue
    .line 440879
    new-instance v0, Lcom/facebook/omnistore/IndexedFields;

    invoke-direct {v0}, Lcom/facebook/omnistore/IndexedFields;-><init>()V

    return-object v0
.end method

.method public final onCollectionAvailable(Lcom/facebook/omnistore/Collection;)V
    .locals 0

    .prologue
    .line 440880
    iput-object p1, p0, LX/2cR;->d:Lcom/facebook/omnistore/Collection;

    .line 440881
    return-void
.end method

.method public final onCollectionInvalidated()V
    .locals 1

    .prologue
    .line 440882
    const/4 v0, 0x0

    iput-object v0, p0, LX/2cR;->d:Lcom/facebook/omnistore/Collection;

    .line 440883
    return-void
.end method

.method public final onDeltasReceived(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/omnistore/Delta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 440884
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 440885
    return-void
.end method

.method public final provideSubscriptionInfo(Lcom/facebook/omnistore/Omnistore;)LX/2cJ;
    .locals 3

    .prologue
    .line 440886
    iget-object v0, p0, LX/2cR;->b:LX/0Uh;

    const/16 v1, 0x1f3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 440887
    sget-object v0, LX/2cJ;->IGNORED_INFO:LX/2cJ;

    move-object v0, v0

    .line 440888
    :goto_0
    return-object v0

    .line 440889
    :cond_0
    const-string v0, "messenger_user_prefs"

    invoke-virtual {p1, v0}, Lcom/facebook/omnistore/Omnistore;->createCollectionNameBuilder(Ljava/lang/String;)Lcom/facebook/omnistore/CollectionName$Builder;

    move-result-object v1

    iget-object v0, p0, LX/2cR;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/omnistore/CollectionName$Builder;->addSegment(Ljava/lang/String;)Lcom/facebook/omnistore/CollectionName$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/omnistore/CollectionName$Builder;->build()Lcom/facebook/omnistore/CollectionName;

    move-result-object v0

    .line 440890
    invoke-static {v0}, LX/2cJ;->forOpenSubscription(Lcom/facebook/omnistore/CollectionName;)LX/2cJ;

    move-result-object v0

    goto :goto_0
.end method
