.class public LX/2uk;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 476152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 50

    .prologue
    .line 476153
    const/16 v45, 0x0

    .line 476154
    const/16 v44, 0x0

    .line 476155
    const/16 v43, 0x0

    .line 476156
    const/16 v42, 0x0

    .line 476157
    const/16 v41, 0x0

    .line 476158
    const/16 v40, 0x0

    .line 476159
    const-wide/16 v38, 0x0

    .line 476160
    const/16 v37, 0x0

    .line 476161
    const/16 v36, 0x0

    .line 476162
    const/16 v35, 0x0

    .line 476163
    const/16 v34, 0x0

    .line 476164
    const/16 v33, 0x0

    .line 476165
    const/16 v32, 0x0

    .line 476166
    const/16 v31, 0x0

    .line 476167
    const/16 v30, 0x0

    .line 476168
    const/16 v29, 0x0

    .line 476169
    const/16 v28, 0x0

    .line 476170
    const/16 v27, 0x0

    .line 476171
    const/16 v26, 0x0

    .line 476172
    const/16 v25, 0x0

    .line 476173
    const/16 v24, 0x0

    .line 476174
    const/16 v23, 0x0

    .line 476175
    const/16 v22, 0x0

    .line 476176
    const/16 v21, 0x0

    .line 476177
    const/16 v20, 0x0

    .line 476178
    const/16 v19, 0x0

    .line 476179
    const/16 v18, 0x0

    .line 476180
    const/16 v17, 0x0

    .line 476181
    const/16 v16, 0x0

    .line 476182
    const/4 v15, 0x0

    .line 476183
    const/4 v14, 0x0

    .line 476184
    const/4 v13, 0x0

    .line 476185
    const/4 v12, 0x0

    .line 476186
    const/4 v11, 0x0

    .line 476187
    const/4 v10, 0x0

    .line 476188
    const/4 v9, 0x0

    .line 476189
    const/4 v8, 0x0

    .line 476190
    const/4 v7, 0x0

    .line 476191
    const/4 v6, 0x0

    .line 476192
    const/4 v5, 0x0

    .line 476193
    const/4 v4, 0x0

    .line 476194
    const/4 v3, 0x0

    .line 476195
    const/4 v2, 0x0

    .line 476196
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v46

    sget-object v47, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v46

    move-object/from16 v1, v47

    if-eq v0, v1, :cond_2d

    .line 476197
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 476198
    const/4 v2, 0x0

    .line 476199
    :goto_0
    return v2

    .line 476200
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_28

    .line 476201
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 476202
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 476203
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v48, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v48

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 476204
    const-string v6, "android_app_config"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 476205
    invoke-static/range {p0 .. p1}, LX/2ul;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v47, v2

    goto :goto_1

    .line 476206
    :cond_1
    const-string v6, "android_store_url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 476207
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v46, v2

    goto :goto_1

    .line 476208
    :cond_2
    const-string v6, "android_urls"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 476209
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v45, v2

    goto :goto_1

    .line 476210
    :cond_3
    const-string v6, "app_center_categories"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 476211
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v44, v2

    goto :goto_1

    .line 476212
    :cond_4
    const-string v6, "app_center_cover_image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 476213
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v43, v2

    goto :goto_1

    .line 476214
    :cond_5
    const-string v6, "application_requests_social_context"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 476215
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 476216
    :cond_6
    const-string v6, "average_star_rating"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 476217
    const/4 v2, 0x1

    .line 476218
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 476219
    :cond_7
    const-string v6, "canvas_url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 476220
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v42, v2

    goto/16 :goto_1

    .line 476221
    :cond_8
    const-string v6, "feedAwesomizerProfilePicture"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 476222
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v41, v2

    goto/16 :goto_1

    .line 476223
    :cond_9
    const-string v6, "global_usage_summary_sentence"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 476224
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v40, v2

    goto/16 :goto_1

    .line 476225
    :cond_a
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 476226
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v39, v2

    goto/16 :goto_1

    .line 476227
    :cond_b
    const-string v6, "imageHighOrig"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 476228
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v38, v2

    goto/16 :goto_1

    .line 476229
    :cond_c
    const-string v6, "instant_game_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 476230
    invoke-static/range {p0 .. p1}, LX/4NG;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v37, v2

    goto/16 :goto_1

    .line 476231
    :cond_d
    const-string v6, "is_game"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 476232
    const/4 v2, 0x1

    .line 476233
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v10, v2

    move/from16 v36, v6

    goto/16 :goto_1

    .line 476234
    :cond_e
    const-string v6, "name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 476235
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 476236
    :cond_f
    const-string v6, "name_search_tokens"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 476237
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v34, v2

    goto/16 :goto_1

    .line 476238
    :cond_10
    const-string v6, "native_store_object"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 476239
    invoke-static/range {p0 .. p1}, LX/4Pf;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v33, v2

    goto/16 :goto_1

    .line 476240
    :cond_11
    const-string v6, "overall_star_rating"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 476241
    invoke-static/range {p0 .. p1}, LX/2sB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v32, v2

    goto/16 :goto_1

    .line 476242
    :cond_12
    const-string v6, "privacy_url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 476243
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 476244
    :cond_13
    const-string v6, "profileImageLarge"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 476245
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 476246
    :cond_14
    const-string v6, "profileImageSmall"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 476247
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 476248
    :cond_15
    const-string v6, "profilePicture50"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 476249
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 476250
    :cond_16
    const-string v6, "profilePictureHighRes"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 476251
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 476252
    :cond_17
    const-string v6, "profilePictureLarge"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 476253
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 476254
    :cond_18
    const-string v6, "profile_photo"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 476255
    invoke-static/range {p0 .. p1}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 476256
    :cond_19
    const-string v6, "profile_picture"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 476257
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 476258
    :cond_1a
    const-string v6, "profile_picture_is_silhouette"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 476259
    const/4 v2, 0x1

    .line 476260
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move/from16 v23, v6

    goto/16 :goto_1

    .line 476261
    :cond_1b
    const-string v6, "social_usage_summary_sentence"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 476262
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 476263
    :cond_1c
    const-string v6, "square_logo"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 476264
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 476265
    :cond_1d
    const-string v6, "streaming_profile_picture"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 476266
    invoke-static/range {p0 .. p1}, LX/4TN;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 476267
    :cond_1e
    const-string v6, "taggable_object_profile_picture"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1f

    .line 476268
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 476269
    :cond_1f
    const-string v6, "terms_of_service_url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_20

    .line 476270
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 476271
    :cond_20
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_21

    .line 476272
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 476273
    :cond_21
    const-string v6, "username"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_22

    .line 476274
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 476275
    :cond_22
    const-string v6, "native_url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_23

    .line 476276
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 476277
    :cond_23
    const-string v6, "instant_experiences_settings"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_24

    .line 476278
    invoke-static/range {p0 .. p1}, LX/4Oi;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 476279
    :cond_24
    const-string v6, "profilePicture180"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_25

    .line 476280
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 476281
    :cond_25
    const-string v6, "publisher_profile_image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_26

    .line 476282
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 476283
    :cond_26
    const-string v6, "highlight_style"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 476284
    const/4 v2, 0x1

    .line 476285
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    move-result-object v6

    move v8, v2

    move-object v11, v6

    goto/16 :goto_1

    .line 476286
    :cond_27
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 476287
    :cond_28
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 476288
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476289
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476290
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476291
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476292
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476293
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 476294
    if-eqz v3, :cond_29

    .line 476295
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 476296
    :cond_29
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476297
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476298
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476299
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476300
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476301
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476302
    if-eqz v10, :cond_2a

    .line 476303
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 476304
    :cond_2a
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476305
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476306
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476307
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476308
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476309
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476310
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476311
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476312
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476313
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476314
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476315
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476316
    if-eqz v9, :cond_2b

    .line 476317
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 476318
    :cond_2b
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476319
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476320
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476321
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476322
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476323
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476324
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 476325
    const/16 v2, 0x24

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 476326
    const/16 v2, 0x26

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 476327
    const/16 v2, 0x27

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 476328
    const/16 v2, 0x28

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 476329
    if-eqz v8, :cond_2c

    .line 476330
    const/16 v2, 0x29

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->a(ILjava/lang/Enum;)V

    .line 476331
    :cond_2c
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_2d
    move/from16 v46, v44

    move/from16 v47, v45

    move/from16 v44, v42

    move/from16 v45, v43

    move/from16 v42, v37

    move/from16 v43, v41

    move/from16 v37, v32

    move/from16 v41, v36

    move/from16 v32, v27

    move/from16 v36, v31

    move/from16 v27, v22

    move/from16 v31, v26

    move/from16 v22, v17

    move/from16 v26, v21

    move/from16 v21, v16

    move/from16 v17, v12

    move/from16 v16, v11

    move v12, v7

    move-object v11, v6

    move/from16 v7, v40

    move/from16 v40, v35

    move/from16 v35, v30

    move/from16 v30, v25

    move/from16 v25, v20

    move/from16 v20, v15

    move v15, v10

    move v10, v4

    move/from16 v49, v29

    move/from16 v29, v24

    move/from16 v24, v19

    move/from16 v19, v14

    move v14, v9

    move v9, v3

    move v3, v5

    move-wide/from16 v4, v38

    move/from16 v39, v34

    move/from16 v38, v33

    move/from16 v33, v28

    move/from16 v34, v49

    move/from16 v28, v23

    move/from16 v23, v18

    move/from16 v18, v13

    move v13, v8

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const/16 v7, 0x29

    const/16 v6, 0x10

    const/4 v5, 0x4

    const/4 v4, 0x3

    const-wide/16 v2, 0x0

    .line 476332
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 476333
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476334
    if-eqz v0, :cond_0

    .line 476335
    const-string v1, "android_app_config"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476336
    invoke-static {p0, v0, p2}, LX/2ul;->a(LX/15i;ILX/0nX;)V

    .line 476337
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476338
    if-eqz v0, :cond_1

    .line 476339
    const-string v1, "android_store_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476340
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476341
    :cond_1
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 476342
    if-eqz v0, :cond_2

    .line 476343
    const-string v0, "android_urls"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476344
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 476345
    :cond_2
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 476346
    if-eqz v0, :cond_3

    .line 476347
    const-string v0, "app_center_categories"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476348
    invoke-virtual {p0, p1, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 476349
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476350
    if-eqz v0, :cond_4

    .line 476351
    const-string v1, "app_center_cover_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476352
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476353
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476354
    if-eqz v0, :cond_5

    .line 476355
    const-string v1, "application_requests_social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476356
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 476357
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 476358
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_6

    .line 476359
    const-string v2, "average_star_rating"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476360
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 476361
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476362
    if-eqz v0, :cond_7

    .line 476363
    const-string v1, "canvas_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476364
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476365
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476366
    if-eqz v0, :cond_8

    .line 476367
    const-string v1, "feedAwesomizerProfilePicture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476368
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476369
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476370
    if-eqz v0, :cond_9

    .line 476371
    const-string v1, "global_usage_summary_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476372
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 476373
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476374
    if-eqz v0, :cond_a

    .line 476375
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476376
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476377
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476378
    if-eqz v0, :cond_b

    .line 476379
    const-string v1, "imageHighOrig"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476380
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476381
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476382
    if-eqz v0, :cond_c

    .line 476383
    const-string v1, "instant_game_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476384
    invoke-static {p0, v0, p2}, LX/4NG;->a(LX/15i;ILX/0nX;)V

    .line 476385
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 476386
    if-eqz v0, :cond_d

    .line 476387
    const-string v1, "is_game"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476388
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 476389
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476390
    if-eqz v0, :cond_e

    .line 476391
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476392
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476393
    :cond_e
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 476394
    if-eqz v0, :cond_f

    .line 476395
    const-string v0, "name_search_tokens"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476396
    invoke-virtual {p0, p1, v6}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 476397
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476398
    if-eqz v0, :cond_10

    .line 476399
    const-string v1, "native_store_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476400
    invoke-static {p0, v0, p2}, LX/4Pf;->a(LX/15i;ILX/0nX;)V

    .line 476401
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476402
    if-eqz v0, :cond_11

    .line 476403
    const-string v1, "overall_star_rating"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476404
    invoke-static {p0, v0, p2}, LX/2sB;->a(LX/15i;ILX/0nX;)V

    .line 476405
    :cond_11
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476406
    if-eqz v0, :cond_12

    .line 476407
    const-string v1, "privacy_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476408
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476409
    :cond_12
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476410
    if-eqz v0, :cond_13

    .line 476411
    const-string v1, "profileImageLarge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476412
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476413
    :cond_13
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476414
    if-eqz v0, :cond_14

    .line 476415
    const-string v1, "profileImageSmall"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476416
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476417
    :cond_14
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476418
    if-eqz v0, :cond_15

    .line 476419
    const-string v1, "profilePicture50"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476420
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476421
    :cond_15
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476422
    if-eqz v0, :cond_16

    .line 476423
    const-string v1, "profilePictureHighRes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476424
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476425
    :cond_16
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476426
    if-eqz v0, :cond_17

    .line 476427
    const-string v1, "profilePictureLarge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476428
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476429
    :cond_17
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476430
    if-eqz v0, :cond_18

    .line 476431
    const-string v1, "profile_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476432
    invoke-static {p0, v0, p2, p3}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 476433
    :cond_18
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476434
    if-eqz v0, :cond_19

    .line 476435
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476436
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476437
    :cond_19
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 476438
    if-eqz v0, :cond_1a

    .line 476439
    const-string v1, "profile_picture_is_silhouette"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476440
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 476441
    :cond_1a
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476442
    if-eqz v0, :cond_1b

    .line 476443
    const-string v1, "social_usage_summary_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476444
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 476445
    :cond_1b
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476446
    if-eqz v0, :cond_1c

    .line 476447
    const-string v1, "square_logo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476448
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476449
    :cond_1c
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476450
    if-eqz v0, :cond_1d

    .line 476451
    const-string v1, "streaming_profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476452
    invoke-static {p0, v0, p2}, LX/4TN;->a(LX/15i;ILX/0nX;)V

    .line 476453
    :cond_1d
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476454
    if-eqz v0, :cond_1e

    .line 476455
    const-string v1, "taggable_object_profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476456
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476457
    :cond_1e
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476458
    if-eqz v0, :cond_1f

    .line 476459
    const-string v1, "terms_of_service_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476460
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476461
    :cond_1f
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476462
    if-eqz v0, :cond_20

    .line 476463
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476464
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476465
    :cond_20
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476466
    if-eqz v0, :cond_21

    .line 476467
    const-string v1, "username"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476468
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476469
    :cond_21
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476470
    if-eqz v0, :cond_22

    .line 476471
    const-string v1, "native_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476472
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476473
    :cond_22
    const/16 v0, 0x26

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476474
    if-eqz v0, :cond_23

    .line 476475
    const-string v1, "instant_experiences_settings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476476
    invoke-static {p0, v0, p2, p3}, LX/4Oi;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 476477
    :cond_23
    const/16 v0, 0x27

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476478
    if-eqz v0, :cond_24

    .line 476479
    const-string v1, "profilePicture180"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476480
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476481
    :cond_24
    const/16 v0, 0x28

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476482
    if-eqz v0, :cond_25

    .line 476483
    const-string v1, "publisher_profile_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476484
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476485
    :cond_25
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v7, v0}, LX/15i;->a(IIS)S

    move-result v0

    .line 476486
    if-eqz v0, :cond_26

    .line 476487
    const-string v0, "highlight_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476488
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    invoke-virtual {p0, p1, v7, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476489
    :cond_26
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 476490
    return-void
.end method
