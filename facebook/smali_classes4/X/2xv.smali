.class public final LX/2xv;
.super LX/1n6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1n6",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/2xu;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 479619
    invoke-direct {p0}, LX/1n6;-><init>()V

    .line 479620
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "resourceId"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "color"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/2xv;->b:[Ljava/lang/String;

    .line 479621
    iput v3, p0, LX/2xv;->c:I

    .line 479622
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/2xv;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/2xv;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/2xv;LX/1De;LX/2xu;)V
    .locals 1

    .prologue
    .line 479615
    invoke-super {p0, p1}, LX/1n6;->a(LX/1De;)V

    .line 479616
    iput-object p2, p0, LX/2xv;->a:LX/2xu;

    .line 479617
    iget-object v0, p0, LX/2xv;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 479618
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 479611
    invoke-super {p0}, LX/1n6;->a()V

    .line 479612
    const/4 v0, 0x0

    iput-object v0, p0, LX/2xv;->a:LX/2xu;

    .line 479613
    sget-object v0, LX/1vg;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 479614
    return-void
.end method

.method public final b()LX/1dc;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 479623
    iget-object v1, p0, LX/2xv;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/2xv;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/2xv;->c:I

    if-ge v1, v2, :cond_2

    .line 479624
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 479625
    :goto_0
    iget v2, p0, LX/2xv;->c:I

    if-ge v0, v2, :cond_1

    .line 479626
    iget-object v2, p0, LX/2xv;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 479627
    iget-object v2, p0, LX/2xv;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 479628
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 479629
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 479630
    :cond_2
    iget-object v0, p0, LX/2xv;->a:LX/2xu;

    .line 479631
    invoke-virtual {p0}, LX/2xv;->a()V

    .line 479632
    return-object v0
.end method

.method public final h(I)LX/2xv;
    .locals 2

    .prologue
    .line 479608
    iget-object v0, p0, LX/2xv;->a:LX/2xu;

    iput p1, v0, LX/2xu;->a:I

    .line 479609
    iget-object v0, p0, LX/2xv;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 479610
    return-object p0
.end method

.method public final i(I)LX/2xv;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 479605
    iget-object v0, p0, LX/2xv;->a:LX/2xu;

    iput p1, v0, LX/2xu;->b:I

    .line 479606
    iget-object v0, p0, LX/2xv;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 479607
    return-object p0
.end method

.method public final j(I)LX/2xv;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 479602
    iget-object v0, p0, LX/2xv;->a:LX/2xu;

    invoke-virtual {p0, p1}, LX/1Dp;->d(I)I

    move-result v1

    iput v1, v0, LX/2xu;->b:I

    .line 479603
    iget-object v0, p0, LX/2xv;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 479604
    return-object p0
.end method
