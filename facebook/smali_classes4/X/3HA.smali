.class public final LX/3HA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0sZ;

.field public final b:LX/0tG;

.field public final c:LX/0tI;

.field public final d:LX/0se;


# direct methods
.method public constructor <init>(LX/0sZ;LX/0tG;LX/0tI;LX/0se;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 542985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 542986
    iput-object p1, p0, LX/3HA;->a:LX/0sZ;

    .line 542987
    iput-object p2, p0, LX/3HA;->b:LX/0tG;

    .line 542988
    iput-object p3, p0, LX/3HA;->c:LX/0tI;

    .line 542989
    iput-object p4, p0, LX/3HA;->d:LX/0se;

    .line 542990
    return-void
.end method

.method public static a(LX/0QB;)LX/3HA;
    .locals 1

    .prologue
    .line 543038
    invoke-static {p0}, LX/3HA;->b(LX/0QB;)LX/3HA;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3HA;
    .locals 5

    .prologue
    .line 543036
    new-instance v4, LX/3HA;

    invoke-static {p0}, LX/0sZ;->b(LX/0QB;)LX/0sZ;

    move-result-object v0

    check-cast v0, LX/0sZ;

    invoke-static {p0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v1

    check-cast v1, LX/0tG;

    invoke-static {p0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v2

    check-cast v2, LX/0tI;

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v3

    check-cast v3, LX/0se;

    invoke-direct {v4, v0, v1, v2, v3}, LX/3HA;-><init>(LX/0sZ;LX/0tG;LX/0tI;LX/0se;)V

    .line 543037
    return-object v4
.end method


# virtual methods
.method public final a(LX/0gW;Lcom/facebook/api/ufiservices/common/FetchReactionsParams;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 3
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gW;",
            "Lcom/facebook/api/ufiservices/common/FetchReactionsParams;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542991
    iget-object v0, p0, LX/3HA;->b:LX/0tG;

    invoke-virtual {v0, p1}, LX/0tG;->b(LX/0gW;)V

    .line 542992
    iget-object v0, p0, LX/3HA;->c:LX/0tI;

    invoke-virtual {v0, p1}, LX/0tI;->a(LX/0gW;)V

    .line 542993
    if-eqz p2, :cond_4

    .line 542994
    const-string v0, "feedback_id"

    .line 542995
    iget-object v1, p2, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 542996
    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "max_reactors"

    .line 542997
    iget v2, p2, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->b:I

    move v2, v2

    .line 542998
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 542999
    iget v0, p2, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->a:I

    move v0, v0

    .line 543000
    sget-object v1, LX/1zt;->c:LX/1zt;

    .line 543001
    iget v2, v1, LX/1zt;->e:I

    move v1, v2

    .line 543002
    if-eq v0, v1, :cond_0

    .line 543003
    const-string v0, "reaction_type"

    .line 543004
    iget v1, p2, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->a:I

    move v1, v1

    .line 543005
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543006
    :cond_0
    iget-object v0, p2, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 543007
    if-eqz v0, :cond_1

    .line 543008
    const-string v0, "before_reactors"

    .line 543009
    iget-object v1, p2, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->c:Ljava/lang/String;

    move-object v1, v1

    .line 543010
    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543011
    :cond_1
    iget-object v0, p2, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 543012
    if-eqz v0, :cond_2

    .line 543013
    const-string v0, "after_reactors"

    .line 543014
    iget-object v1, p2, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 543015
    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543016
    :cond_2
    iget-boolean v0, p2, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->c:Z

    move v0, v0

    .line 543017
    if-eqz v0, :cond_3

    .line 543018
    const-string v0, "is_show_unread_count"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 543019
    :cond_3
    const-string v0, "fetch_profile_discovery_bucket"

    .line 543020
    iget-boolean v1, p2, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->b:Z

    move v1, v1

    .line 543021
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 543022
    :cond_4
    iget-object v0, p0, LX/3HA;->d:LX/0se;

    invoke-virtual {v0, p1}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 543023
    move-object v0, p1

    .line 543024
    const-class v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v1}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v1

    move-object v1, v1

    .line 543025
    invoke-static {v0, v1}, LX/0zO;->a(LX/0gW;LX/0w5;)LX/0zO;

    move-result-object v0

    .line 543026
    iput-object p3, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 543027
    move-object v0, v0

    .line 543028
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 543029
    iget-object v1, p2, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->e:LX/0rS;

    move-object v1, v1

    .line 543030
    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne v1, v2, :cond_5

    .line 543031
    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 543032
    :goto_0
    iget-object v1, p0, LX/3HA;->a:LX/0sZ;

    invoke-virtual {v1, p2, v0}, LX/0sZ;->a(Lcom/facebook/api/ufiservices/common/FetchNodeListParams;LX/0zO;)LX/1kt;

    move-result-object v1

    .line 543033
    iput-object v1, v0, LX/0zO;->g:LX/1kt;

    .line 543034
    return-object v0

    .line 543035
    :cond_5
    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    goto :goto_0
.end method
