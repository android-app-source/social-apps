.class public LX/2E3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/auth/component/ReauthResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384937
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384938
    return-void
.end method

.method public static a(LX/0QB;)LX/2E3;
    .locals 1

    .prologue
    .line 384934
    new-instance v0, LX/2E3;

    invoke-direct {v0}, LX/2E3;-><init>()V

    .line 384935
    move-object v0, v0

    .line 384936
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 384927
    check-cast p1, Ljava/lang/String;

    .line 384928
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 384929
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "password"

    invoke-direct {v0, v1, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384930
    new-instance v0, LX/14N;

    const-string v1, "reauth"

    const-string v2, "POST"

    const-string v3, "/auth/reauth"

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 384931
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 384932
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v4

    .line 384933
    new-instance v0, Lcom/facebook/auth/component/ReauthResult;

    const-string v1, "token"

    invoke-virtual {v4, v1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    const-string v2, "creation_time"

    invoke-virtual {v4, v2}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->D()J

    move-result-wide v2

    const-string v5, "expiration_time"

    invoke-virtual {v4, v5}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->D()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/auth/component/ReauthResult;-><init>(Ljava/lang/String;JJ)V

    return-object v0
.end method
