.class public abstract LX/2kg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        "TUserInfo:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1vq",
        "<TTEdge;TTUserInfo;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 456496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456497
    return-void
.end method


# virtual methods
.method public abstract a()LX/2ke;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2ke",
            "<TTEdge;>;"
        }
    .end annotation
.end method

.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<TTEdge;>;",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 456498
    invoke-virtual {p0}, LX/2kg;->a()LX/2ke;

    move-result-object v0

    invoke-interface {v0, p4}, LX/2ke;->a(LX/2kM;)V

    .line 456499
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3CY;

    .line 456500
    sget-object v3, LX/3Cd;->a:[I

    iget-object v4, v0, LX/3CY;->a:LX/3Ca;

    invoke-virtual {v4}, LX/3Ca;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 456501
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 456502
    :pswitch_0
    invoke-virtual {p0}, LX/2kg;->a()LX/2ke;

    move-result-object v3

    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v4

    iget v0, v0, LX/3CY;->b:I

    invoke-interface {v3, v4, v0}, LX/2ke;->c(II)V

    goto :goto_1

    .line 456503
    :pswitch_1
    invoke-virtual {p0}, LX/2kg;->a()LX/2ke;

    move-result-object v3

    invoke-virtual {v0}, LX/3CY;->a()I

    move-result v4

    iget v0, v0, LX/3CY;->b:I

    invoke-interface {v3, v4, v0}, LX/2ke;->d(II)V

    goto :goto_1

    .line 456504
    :pswitch_2
    invoke-virtual {p0}, LX/2kg;->a()LX/2ke;

    move-result-object v3

    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v4

    iget v0, v0, LX/3CY;->b:I

    invoke-interface {v3, v4, v0}, LX/2ke;->a(II)V

    goto :goto_1

    .line 456505
    :cond_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 456506
    return-void
.end method
