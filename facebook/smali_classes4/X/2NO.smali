.class public LX/2NO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Dj;

.field private final b:LX/2Dk;


# direct methods
.method public constructor <init>(LX/2Dj;LX/2Dk;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 399792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399793
    iput-object p1, p0, LX/2NO;->a:LX/2Dj;

    .line 399794
    iput-object p2, p0, LX/2NO;->b:LX/2Dk;

    .line 399795
    return-void
.end method

.method public static a(LX/0QB;)LX/2NO;
    .locals 1

    .prologue
    .line 399762
    invoke-static {p0}, LX/2NO;->b(LX/0QB;)LX/2NO;

    move-result-object v0

    return-object v0
.end method

.method private b(LX/27f;)I
    .locals 10

    .prologue
    .line 399763
    iget-object v0, p0, LX/2NO;->a:LX/2Dj;

    .line 399764
    new-instance p0, LX/8DQ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Dl;->a(LX/0QB;)LX/2Dl;

    move-result-object v3

    check-cast v3, LX/2Dl;

    invoke-direct {p0, v1, v2, v3, p1}, LX/8DQ;-><init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Dl;LX/27f;)V

    .line 399765
    move-object v0, p0

    .line 399766
    const/4 v1, -0x1

    .line 399767
    const/4 v4, 0x0

    .line 399768
    iget v5, v0, LX/8DQ;->f:I

    if-nez v5, :cond_3

    .line 399769
    :cond_0
    :goto_0
    move v2, v4

    .line 399770
    if-nez v2, :cond_1

    .line 399771
    :goto_1
    move v0, v1

    .line 399772
    return v0

    .line 399773
    :cond_1
    iget-object v2, v0, LX/8DQ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v3, v0, LX/8DQ;->e:LX/0Tn;

    invoke-interface {v2, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 399774
    if-gez v1, :cond_2

    .line 399775
    iget-object v4, v0, LX/8DQ;->c:LX/2Dl;

    invoke-virtual {v4}, LX/2Dl;->a()Ljava/lang/String;

    move-result-object v4

    .line 399776
    iget-object v5, v0, LX/8DQ;->d:Ljava/lang/String;

    move-object v5, v5

    .line 399777
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/8DT;->a(Ljava/lang/String;)J

    move-result-wide v7

    .line 399778
    invoke-static {v7, v8}, LX/8DT;->a(J)I

    move-result v7

    move v4, v7

    .line 399779
    iget-object v5, v0, LX/8DQ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    iget-object v6, v0, LX/8DQ;->e:LX/0Tn;

    invoke-interface {v5, v6, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 399780
    move v1, v4

    .line 399781
    :cond_2
    iget v2, v0, LX/8DQ;->f:I

    iget v3, v0, LX/8DQ;->g:I

    .line 399782
    div-int v4, v1, v2

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 399783
    if-ge v4, v3, :cond_7

    .line 399784
    :goto_2
    move v1, v4

    .line 399785
    goto :goto_1

    .line 399786
    :cond_3
    iget-object v5, v0, LX/8DQ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 399787
    new-instance v6, Ljava/util/Date;

    iget-object v7, v0, LX/8DQ;->a:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 399788
    iget-object v7, v0, LX/8DQ;->h:Ljava/util/Date;

    invoke-virtual {v6, v7}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v7

    if-nez v7, :cond_4

    iget-object v7, v0, LX/8DQ;->i:Ljava/util/Date;

    invoke-virtual {v6, v7}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v6

    if-eqz v6, :cond_6

    :cond_4
    const/4 v6, 0x1

    :goto_3
    move v5, v6

    .line 399789
    if-nez v5, :cond_0

    .line 399790
    iget-object v5, v0, LX/8DQ;->j:LX/8DP;

    if-eqz v5, :cond_5

    iget-object v5, v0, LX/8DQ;->j:LX/8DP;

    invoke-interface {v5}, LX/8DP;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 399791
    :cond_5
    const/4 v4, 0x1

    goto/16 :goto_0

    :cond_6
    const/4 v6, 0x0

    goto :goto_3

    :cond_7
    const/4 v4, -0x1

    goto :goto_2
.end method

.method public static b(LX/0QB;)LX/2NO;
    .locals 5

    .prologue
    .line 399754
    new-instance v2, LX/2NO;

    const-class v0, LX/2Dj;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/2Dj;

    .line 399755
    new-instance v4, LX/2Dk;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/2Dl;->a(LX/0QB;)LX/2Dl;

    move-result-object v3

    check-cast v3, LX/2Dl;

    invoke-direct {v4, v1, v3}, LX/2Dk;-><init>(LX/0Zb;LX/2Dl;)V

    .line 399756
    move-object v1, v4

    .line 399757
    check-cast v1, LX/2Dk;

    invoke-direct {v2, v0, v1}, LX/2NO;-><init>(LX/2Dj;LX/2Dk;)V

    .line 399758
    return-object v2
.end method


# virtual methods
.method public final a(LX/27f;)I
    .locals 2

    .prologue
    .line 399759
    invoke-direct {p0, p1}, LX/2NO;->b(LX/27f;)I

    move-result v0

    .line 399760
    iget-object v1, p0, LX/2NO;->b:LX/2Dk;

    invoke-virtual {v1, p1, v0}, LX/2Dk;->a(LX/27f;I)V

    .line 399761
    return v0
.end method
