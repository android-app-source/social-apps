.class public LX/2bX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 436857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 436858
    const/4 v10, 0x0

    .line 436859
    const/4 v9, 0x0

    .line 436860
    const/4 v8, 0x0

    .line 436861
    const/4 v7, 0x0

    .line 436862
    const/4 v6, 0x0

    .line 436863
    const/4 v5, 0x0

    .line 436864
    const/4 v4, 0x0

    .line 436865
    const/4 v3, 0x0

    .line 436866
    const/4 v2, 0x0

    .line 436867
    const/4 v1, 0x0

    .line 436868
    const/4 v0, 0x0

    .line 436869
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 436870
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 436871
    const/4 v0, 0x0

    .line 436872
    :goto_0
    return v0

    .line 436873
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 436874
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_7

    .line 436875
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 436876
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 436877
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 436878
    const-string v12, "story_save_nux_max_consume_duration"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 436879
    const/4 v4, 0x1

    .line 436880
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v10

    goto :goto_1

    .line 436881
    :cond_2
    const-string v12, "story_save_nux_min_consume_duration"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 436882
    const/4 v3, 0x1

    .line 436883
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v9

    goto :goto_1

    .line 436884
    :cond_3
    const-string v12, "story_save_nux_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 436885
    const/4 v2, 0x1

    .line 436886
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    move-result-object v8

    goto :goto_1

    .line 436887
    :cond_4
    const-string v12, "story_save_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 436888
    const/4 v1, 0x1

    .line 436889
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    move-result-object v7

    goto :goto_1

    .line 436890
    :cond_5
    const-string v12, "viewer_save_state"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 436891
    const/4 v0, 0x1

    .line 436892
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLSavedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v6

    goto :goto_1

    .line 436893
    :cond_6
    const-string v12, "savable"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 436894
    invoke-static {p0, p1}, LX/4Sc;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 436895
    :cond_7
    const/4 v11, 0x6

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 436896
    if-eqz v4, :cond_8

    .line 436897
    const/4 v4, 0x0

    const/4 v11, 0x0

    invoke-virtual {p1, v4, v10, v11}, LX/186;->a(III)V

    .line 436898
    :cond_8
    if-eqz v3, :cond_9

    .line 436899
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v9, v4}, LX/186;->a(III)V

    .line 436900
    :cond_9
    if-eqz v2, :cond_a

    .line 436901
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v8}, LX/186;->a(ILjava/lang/Enum;)V

    .line 436902
    :cond_a
    if-eqz v1, :cond_b

    .line 436903
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->a(ILjava/lang/Enum;)V

    .line 436904
    :cond_b
    if-eqz v0, :cond_c

    .line 436905
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->a(ILjava/lang/Enum;)V

    .line 436906
    :cond_c
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 436907
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 436908
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 436909
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 436910
    if-eqz v0, :cond_0

    .line 436911
    const-string v1, "story_save_nux_max_consume_duration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 436912
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 436913
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 436914
    if-eqz v0, :cond_1

    .line 436915
    const-string v1, "story_save_nux_min_consume_duration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 436916
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 436917
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 436918
    if-eqz v0, :cond_2

    .line 436919
    const-string v0, "story_save_nux_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 436920
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 436921
    :cond_2
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 436922
    if-eqz v0, :cond_3

    .line 436923
    const-string v0, "story_save_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 436924
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 436925
    :cond_3
    invoke-virtual {p0, p1, v5, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 436926
    if-eqz v0, :cond_4

    .line 436927
    const-string v0, "viewer_save_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 436928
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 436929
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 436930
    if-eqz v0, :cond_5

    .line 436931
    const-string v1, "savable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 436932
    invoke-static {p0, v0, p2}, LX/4Sc;->a(LX/15i;ILX/0nX;)V

    .line 436933
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 436934
    return-void
.end method
