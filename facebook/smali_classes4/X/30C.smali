.class public LX/30C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:Lcom/facebook/compactdisk/ExperimentManager;


# direct methods
.method public constructor <init>(Lcom/facebook/compactdisk/ExperimentManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 484107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484108
    iput-object p1, p0, LX/30C;->a:Lcom/facebook/compactdisk/ExperimentManager;

    .line 484109
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 2

    .prologue
    .line 484110
    iget-object v0, p0, LX/30C;->a:Lcom/facebook/compactdisk/ExperimentManager;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/facebook/compactdisk/Experiment;

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/ExperimentManager;->onExperimentsUpdated([Lcom/facebook/compactdisk/Experiment;)V

    .line 484111
    return-void
.end method
