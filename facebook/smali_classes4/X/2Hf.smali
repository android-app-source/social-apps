.class public LX/2Hf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2Hf;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:Ljava/lang/String;

.field public final c:LX/2G1;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0VT;LX/2G1;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390792
    iput-object p1, p0, LX/2Hf;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 390793
    invoke-virtual {p2}, LX/0VT;->a()LX/00G;

    move-result-object v0

    invoke-virtual {v0}, LX/00G;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2Hf;->b:Ljava/lang/String;

    .line 390794
    iput-object p3, p0, LX/2Hf;->c:LX/2G1;

    .line 390795
    return-void
.end method

.method public static a(LX/0QB;)LX/2Hf;
    .locals 6

    .prologue
    .line 390796
    sget-object v0, LX/2Hf;->d:LX/2Hf;

    if-nez v0, :cond_1

    .line 390797
    const-class v1, LX/2Hf;

    monitor-enter v1

    .line 390798
    :try_start_0
    sget-object v0, LX/2Hf;->d:LX/2Hf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390799
    if-eqz v2, :cond_0

    .line 390800
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 390801
    new-instance p0, LX/2Hf;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v4

    check-cast v4, LX/0VT;

    invoke-static {v0}, LX/2G1;->a(LX/0QB;)LX/2G1;

    move-result-object v5

    check-cast v5, LX/2G1;

    invoke-direct {p0, v3, v4, v5}, LX/2Hf;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0VT;LX/2G1;)V

    .line 390802
    move-object v0, p0

    .line 390803
    sput-object v0, LX/2Hf;->d:LX/2Hf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390804
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390805
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390806
    :cond_1
    sget-object v0, LX/2Hf;->d:LX/2Hf;

    return-object v0

    .line 390807
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390808
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 6

    .prologue
    .line 390809
    const-wide/32 v2, 0x36ee80

    .line 390810
    iget-object v4, p0, LX/2Hf;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v4

    if-nez v4, :cond_0

    .line 390811
    :goto_0
    move-wide v0, v2

    .line 390812
    return-wide v0

    :cond_0
    iget-object v4, p0, LX/2Hf;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0uQ;->b:LX/0Tn;

    invoke-interface {v4, v5, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    goto :goto_0
.end method

.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 12

    .prologue
    .line 390813
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "process_status"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 390814
    iget-object v2, p0, LX/2Hf;->c:LX/2G1;

    .line 390815
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    .line 390816
    invoke-virtual {v6}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v8

    .line 390817
    invoke-virtual {v6}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v10

    invoke-virtual {v6}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v6

    sub-long v6, v10, v6

    .line 390818
    sub-long v6, v8, v6

    .line 390819
    const-string v10, "free_mem"

    invoke-virtual {v1, v10, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390820
    const-string v6, "total_mem"

    invoke-virtual {v1, v6, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390821
    const-string v6, "core_count"

    iget-object v7, v2, LX/2G1;->i:LX/0TL;

    invoke-virtual {v7}, LX/0TL;->c()I

    move-result v7

    invoke-virtual {v1, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390822
    const-string v6, "reliable_core_count"

    iget-object v7, v2, LX/2G1;->i:LX/0TL;

    invoke-virtual {v7}, LX/0TL;->b()I

    move-result v7

    invoke-virtual {v1, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390823
    const-string v4, "memory_info"

    invoke-static {v2}, LX/2G1;->a(LX/2G1;)LX/0m9;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390824
    iget-object v4, v2, LX/2G1;->j:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2IR;

    invoke-virtual {v4, v1}, LX/0Vx;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 390825
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 390826
    const-string v3, "open_fd_count"

    invoke-static {}, LX/04V;->getOpenFDCount()I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 390827
    invoke-static {}, LX/04V;->getOpenFDLimits()LX/04W;

    move-result-object v3

    .line 390828
    if-eqz v3, :cond_0

    .line 390829
    const-string v4, "open_fd_soft_limit"

    iget-object v5, v3, LX/04W;->softLimit:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 390830
    const-string v4, "open_fd_hard_limit"

    iget-object v3, v3, LX/04W;->hardLimit:Ljava/lang/String;

    invoke-virtual {v2, v4, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 390831
    :cond_0
    const-string v3, "fd_info"

    invoke-virtual {v1, v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390832
    iput-wide p1, v1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 390833
    const-string v2, "process"

    .line 390834
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 390835
    iget-object v2, p0, LX/2Hf;->b:Ljava/lang/String;

    .line 390836
    iput-object v2, v1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->i:Ljava/lang/String;

    .line 390837
    move-object v0, v1

    .line 390838
    return-object v0
.end method
