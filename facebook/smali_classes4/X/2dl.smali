.class public LX/2dl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0tX;

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2io;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Or;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Or",
            "<",
            "LX/2io;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444282
    iput-object p1, p0, LX/2dl;->a:LX/0tX;

    .line 444283
    iput-object p2, p0, LX/2dl;->b:Ljava/util/concurrent/ExecutorService;

    .line 444284
    iput-object p3, p0, LX/2dl;->c:LX/0Or;

    .line 444285
    return-void
.end method

.method public static a(LX/0QB;)LX/2dl;
    .locals 6

    .prologue
    .line 444252
    const-class v1, LX/2dl;

    monitor-enter v1

    .line 444253
    :try_start_0
    sget-object v0, LX/2dl;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444254
    sput-object v2, LX/2dl;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444255
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444256
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 444257
    new-instance v5, LX/2dl;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    const/16 p0, 0xa74

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/2dl;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Or;)V

    .line 444258
    move-object v0, v5

    .line 444259
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444260
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2dl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444261
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444262
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0zO;LX/1My;LX/0TF;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;",
            "LX/1My;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 444273
    const/4 v0, 0x1

    .line 444274
    iput-boolean v0, p1, LX/0zO;->p:Z

    .line 444275
    invoke-static {p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2dl;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2io;

    invoke-virtual {v0, p5}, LX/2io;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444276
    sget-object v0, LX/0zS;->d:LX/0zS;

    invoke-virtual {p1, v0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 444277
    :cond_0
    new-instance v0, LX/845;

    invoke-direct {v0, p0, p5, p3}, LX/845;-><init>(LX/2dl;Ljava/lang/String;LX/0TF;)V

    .line 444278
    invoke-virtual {p2, p1, v0, p4}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 444279
    iget-object v0, p0, LX/2dl;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2io;

    invoke-virtual {v0, p5}, LX/2io;->b(Ljava/lang/String;)LX/0TF;

    move-result-object v0

    iget-object v2, p0, LX/2dl;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 444280
    return-object v1
.end method

.method public final a(LX/0zO;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 444266
    const/4 v0, 0x1

    .line 444267
    iput-boolean v0, p1, LX/0zO;->p:Z

    .line 444268
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2dl;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2io;

    invoke-virtual {v0, p2}, LX/2io;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444269
    sget-object v0, LX/0zS;->d:LX/0zS;

    invoke-virtual {p1, v0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 444270
    :cond_0
    iget-object v0, p0, LX/2dl;->a:LX/0tX;

    invoke-virtual {v0, p1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 444271
    iget-object v0, p0, LX/2dl;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2io;

    invoke-virtual {v0, p2}, LX/2io;->b(Ljava/lang/String;)LX/0TF;

    move-result-object v0

    iget-object v2, p0, LX/2dl;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 444272
    return-object v1
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 444263
    iget-object v0, p0, LX/2dl;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2io;

    .line 444264
    iget-object p0, v0, LX/2io;->a:Ljava/util/Set;

    invoke-interface {p0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 444265
    return-void
.end method
