.class public LX/3Ky;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private final a:LX/2Of;

.field private final b:LX/2Ou;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 549705
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3Ky;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2Of;LX/2Ou;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 549701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549702
    iput-object p1, p0, LX/3Ky;->a:LX/2Of;

    .line 549703
    iput-object p2, p0, LX/3Ky;->b:LX/2Ou;

    .line 549704
    return-void
.end method

.method public static a(LX/0QB;)LX/3Ky;
    .locals 8

    .prologue
    .line 549658
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 549659
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 549660
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 549661
    if-nez v1, :cond_0

    .line 549662
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 549663
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 549664
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 549665
    sget-object v1, LX/3Ky;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 549666
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 549667
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 549668
    :cond_1
    if-nez v1, :cond_4

    .line 549669
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 549670
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 549671
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 549672
    new-instance p0, LX/3Ky;

    invoke-static {v0}, LX/2Of;->a(LX/0QB;)LX/2Of;

    move-result-object v1

    check-cast v1, LX/2Of;

    invoke-static {v0}, LX/2Ou;->a(LX/0QB;)LX/2Ou;

    move-result-object v7

    check-cast v7, LX/2Ou;

    invoke-direct {p0, v1, v7}, LX/3Ky;-><init>(LX/2Of;LX/2Ou;)V

    .line 549673
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 549674
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 549675
    if-nez v1, :cond_2

    .line 549676
    sget-object v0, LX/3Ky;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ky;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 549677
    :goto_1
    if-eqz v0, :cond_3

    .line 549678
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 549679
    :goto_3
    check-cast v0, LX/3Ky;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 549680
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 549681
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 549682
    :catchall_1
    move-exception v0

    .line 549683
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 549684
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 549685
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 549686
    :cond_2
    :try_start_8
    sget-object v0, LX/3Ky;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ky;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/FOO;
    .locals 8
    .param p1    # Lcom/facebook/messaging/model/threads/ThreadSummary;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 549687
    const-string v0, "MessengerThreadNameViewDataFactory.getThreadNameViewData"

    const v2, 0x2335442a

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 549688
    if-nez p1, :cond_0

    .line 549689
    const v0, -0x66a8d36f

    invoke-static {v0}, LX/02m;->a(I)V

    :goto_0
    return-object v1

    .line 549690
    :cond_0
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 549691
    iget-object v0, p0, LX/3Ky;->b:LX/2Ou;

    .line 549692
    sget-object v2, LX/5e9;->TINCAN:LX/5e9;

    invoke-static {v0, p1, v2}, LX/2Ou;->a(LX/2Ou;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/5e9;)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v2

    move-object v0, v2

    .line 549693
    :goto_1
    if-eqz v0, :cond_2

    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 549694
    :goto_2
    if-eqz v0, :cond_3

    iget-wide v6, v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b:J

    .line 549695
    :goto_3
    new-instance v1, LX/FOO;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a()Z

    move-result v2

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->g:Ljava/lang/String;

    iget-object v0, p0, LX/3Ky;->a:LX/2Of;

    invoke-virtual {v0, p1}, LX/2Of;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-direct/range {v1 .. v7}, LX/FOO;-><init>(ZLjava/lang/String;LX/0Px;Lcom/facebook/messaging/model/messages/ParticipantInfo;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549696
    const v0, 0x1b7dfb45

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 549697
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/3Ky;->b:LX/2Ou;

    invoke-virtual {v0, p1}, LX/2Ou;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadParticipant;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v5, v1

    .line 549698
    goto :goto_2

    .line 549699
    :cond_3
    const-wide/16 v6, -0x1

    goto :goto_3

    .line 549700
    :catchall_0
    move-exception v0

    const v1, -0x6b7017ed

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
