.class public LX/35R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# instance fields
.field private final a:LX/0SG;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 497144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497145
    iput-object p1, p0, LX/35R;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 497146
    iput-object p2, p0, LX/35R;->a:LX/0SG;

    .line 497147
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 497148
    const-wide/32 v0, 0xf731400

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 497139
    const-string v0, "3193"

    invoke-static {v0}, LX/11b;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 497140
    iget-object v1, p0, LX/35R;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 497141
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/35R;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/32 v4, 0xf731400

    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    .line 497142
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 497143
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 497149
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 497138
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 497136
    const-string v0, "3193"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 497137
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEE_FIRST_INDICATOR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
