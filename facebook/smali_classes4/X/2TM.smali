.class public LX/2TM;
.super LX/0hx;
.source ""


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field public static g:Ljava/lang/String;

.field public static h:Ljava/lang/String;

.field public static i:Ljava/lang/String;

.field public static j:Ljava/lang/String;

.field public static k:Ljava/lang/String;

.field public static l:Ljava/lang/String;

.field public static m:Ljava/lang/String;

.field public static n:Ljava/lang/String;

.field public static o:Ljava/lang/String;

.field public static p:Ljava/lang/String;

.field public static q:Ljava/lang/String;

.field public static r:Ljava/lang/String;

.field public static s:Ljava/lang/String;

.field public static t:Ljava/lang/String;


# instance fields
.field private final u:Landroid/content/Context;

.field private v:LX/2TL;

.field public w:LX/2TN;

.field public final x:LX/03V;

.field public final y:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 414137
    const-string v0, "nux_shown"

    sput-object v0, LX/2TM;->a:Ljava/lang/String;

    .line 414138
    const-string v0, "nux_accept"

    sput-object v0, LX/2TM;->b:Ljava/lang/String;

    .line 414139
    const-string v0, "nux_reject"

    sput-object v0, LX/2TM;->c:Ljava/lang/String;

    .line 414140
    const-string v0, "sync_start"

    sput-object v0, LX/2TM;->d:Ljava/lang/String;

    .line 414141
    const-string v0, "sync_photo_start"

    sput-object v0, LX/2TM;->e:Ljava/lang/String;

    .line 414142
    const-string v0, "sync_photo_success"

    sput-object v0, LX/2TM;->f:Ljava/lang/String;

    .line 414143
    const-string v0, "sync_photo_failure"

    sput-object v0, LX/2TM;->g:Ljava/lang/String;

    .line 414144
    const-string v0, "sync_snap_to_first_sync_time"

    sput-object v0, LX/2TM;->h:Ljava/lang/String;

    .line 414145
    const-string v0, "sync_snap_to_success"

    sput-object v0, LX/2TM;->i:Ljava/lang/String;

    .line 414146
    const-string v0, "sync_hit_max_sync_failure"

    sput-object v0, LX/2TM;->j:Ljava/lang/String;

    .line 414147
    const-string v0, "enable_sync"

    sput-object v0, LX/2TM;->k:Ljava/lang/String;

    .line 414148
    const-string v0, "disable_sync"

    sput-object v0, LX/2TM;->l:Ljava/lang/String;

    .line 414149
    const-string v0, "connectivity"

    sput-object v0, LX/2TM;->m:Ljava/lang/String;

    .line 414150
    const-string v0, "image_observer"

    sput-object v0, LX/2TM;->n:Ljava/lang/String;

    .line 414151
    const-string v0, "observer_startup"

    sput-object v0, LX/2TM;->o:Ljava/lang/String;

    .line 414152
    const-string v0, "retry"

    sput-object v0, LX/2TM;->p:Ljava/lang/String;

    .line 414153
    const-string v0, "setup"

    sput-object v0, LX/2TM;->q:Ljava/lang/String;

    .line 414154
    const-string v0, "mobile_highres_pref"

    sput-object v0, LX/2TM;->r:Ljava/lang/String;

    .line 414155
    const-string v0, "processor_idle"

    sput-object v0, LX/2TM;->s:Ljava/lang/String;

    .line 414156
    const-string v0, "photo_status"

    sput-object v0, LX/2TM;->t:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Zb;LX/0Vw;LX/2TL;LX/2TN;LX/03V;LX/0gh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 414092
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, p2, p7, p3, v0}, LX/0hx;-><init>(LX/0Zb;LX/0gh;LX/0Vw;Landroid/content/res/Resources;)V

    .line 414093
    iput-object p1, p0, LX/2TM;->u:Landroid/content/Context;

    .line 414094
    iput-object p2, p0, LX/2TM;->y:LX/0Zb;

    .line 414095
    iput-object p4, p0, LX/2TM;->v:LX/2TL;

    .line 414096
    iput-object p5, p0, LX/2TM;->w:LX/2TN;

    .line 414097
    iput-object p6, p0, LX/2TM;->x:LX/03V;

    .line 414098
    return-void
.end method

.method public static a(LX/2TM;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 3

    .prologue
    .line 414129
    iget-object v0, p0, LX/2TM;->u:Landroid/content/Context;

    invoke-static {v0}, LX/ERe;->b(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 414130
    if-eqz v0, :cond_0

    .line 414131
    const-string v1, "connectivity"

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414132
    const-string v1, "connectivity_subtype"

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414133
    :goto_0
    iget-object v0, p0, LX/2TM;->y:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 414134
    return-void

    .line 414135
    :cond_0
    const-string v0, "connectivity"

    const-string v1, "NONE"

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414136
    const-string v0, "connectivity_subtype"

    const-string v1, "NONE"

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public static c(LX/0QB;)LX/2TM;
    .locals 8

    .prologue
    .line 414127
    new-instance v0, LX/2TM;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0}, LX/0Vw;->a(LX/0QB;)LX/0Vw;

    move-result-object v3

    check-cast v3, LX/0Vw;

    invoke-static {p0}, LX/2TL;->a(LX/0QB;)LX/2TL;

    move-result-object v4

    check-cast v4, LX/2TL;

    invoke-static {p0}, LX/2TN;->c(LX/0QB;)LX/2TN;

    move-result-object v5

    check-cast v5, LX/2TN;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {p0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v7

    check-cast v7, LX/0gh;

    invoke-direct/range {v0 .. v7}, LX/2TM;-><init>(Landroid/content/Context;LX/0Zb;LX/0Vw;LX/2TL;LX/2TN;LX/03V;LX/0gh;)V

    .line 414128
    return-object v0
.end method

.method public static e(LX/2TM;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 414157
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 414158
    const-string v1, "vault"

    .line 414159
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 414160
    iget-object v1, p0, LX/2TM;->v:LX/2TL;

    invoke-virtual {v1}, LX/2TL;->a()J

    move-result-wide v2

    .line 414161
    const-string v1, "vault_device_oid"

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414162
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/vault/provider/VaultImageProviderRow;JJZLjava/lang/String;)V
    .locals 6

    .prologue
    .line 414114
    sget-object v0, LX/2TM;->f:Ljava/lang/String;

    invoke-static {p0, v0}, LX/2TM;->e(LX/2TM;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 414115
    const-string v0, "img_hash"

    iget-object v2, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414116
    const-string v2, "resolution"

    iget v0, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    if-nez v0, :cond_0

    const-string v0, "low"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414117
    const-string v0, "file_size"

    invoke-virtual {v1, v0, p4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414118
    const-string v0, "upload_time"

    invoke-virtual {v1, v0, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414119
    const-string v0, "time_to_success"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->c:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414120
    const-string v0, "is_upgrade"

    invoke-virtual {v1, v0, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414121
    const-string v0, "failures"

    iget v2, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->e:I

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414122
    const-string v0, "file_path"

    invoke-virtual {v1, v0, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414123
    invoke-static {p0, v1}, LX/2TM;->a(LX/2TM;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 414124
    iget-object v0, p0, LX/2TM;->y:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 414125
    return-void

    .line 414126
    :cond_0
    const-string v0, "high"

    goto :goto_0
.end method

.method public final a(Lcom/facebook/vault/provider/VaultImageProviderRow;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 3

    .prologue
    .line 414105
    sget-object v0, LX/2TM;->g:Ljava/lang/String;

    invoke-static {p0, v0}, LX/2TM;->e(LX/2TM;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 414106
    const-string v1, "img_hash"

    iget-object v2, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414107
    const-string v1, "error_msg"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414108
    const-string v1, "upload_time"

    invoke-virtual {v0, v1, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414109
    const-string v1, "file_size"

    invoke-virtual {v0, v1, p5, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414110
    const-string v1, "file_path"

    invoke-virtual {v0, v1, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414111
    invoke-static {p0, v0}, LX/2TM;->a(LX/2TM;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 414112
    iget-object v1, p0, LX/2TM;->y:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 414113
    return-void
.end method

.method public final b(Lcom/facebook/vault/provider/VaultImageProviderRow;J)V
    .locals 4

    .prologue
    .line 414099
    sget-object v0, LX/2TM;->j:Ljava/lang/String;

    invoke-static {p0, v0}, LX/2TM;->e(LX/2TM;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 414100
    const-string v1, "img_hash"

    iget-object v2, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414101
    const-string v1, "file_size"

    invoke-virtual {v0, v1, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 414102
    invoke-static {p0, v0}, LX/2TM;->a(LX/2TM;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 414103
    iget-object v1, p0, LX/2TM;->y:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 414104
    return-void
.end method
