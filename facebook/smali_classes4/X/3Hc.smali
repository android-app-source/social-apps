.class public LX/3Hc;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field public final c:LX/03V;

.field private final d:LX/1Ck;

.field public final e:LX/0tX;

.field private final f:Landroid/os/Handler;

.field private final g:LX/19j;

.field private final h:LX/3Hd;

.field private final i:LX/0SG;

.field public final j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Zc;

.field public l:LX/3Ha;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:I

.field public o:I

.field public p:Z

.field private q:Z

.field public r:Z

.field public s:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public t:Ljava/lang/String;

.field private u:J

.field private v:J

.field public w:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 544217
    const-class v0, LX/3Hc;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Hc;->a:Ljava/lang/String;

    .line 544218
    const-class v0, LX/3Hc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Hc;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/1Ck;LX/0tX;LX/19j;LX/3Hd;LX/0SG;LX/0Zc;LX/1b4;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 544201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 544202
    iput v0, p0, LX/3Hc;->n:I

    .line 544203
    iput v0, p0, LX/3Hc;->o:I

    .line 544204
    iput-object p1, p0, LX/3Hc;->c:LX/03V;

    .line 544205
    iput-object p2, p0, LX/3Hc;->d:LX/1Ck;

    .line 544206
    iput-object p3, p0, LX/3Hc;->e:LX/0tX;

    .line 544207
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/3Hc;->f:Landroid/os/Handler;

    .line 544208
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, LX/3Hc;->s:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 544209
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/3Hc;->j:Ljava/util/HashMap;

    .line 544210
    iput-object p4, p0, LX/3Hc;->g:LX/19j;

    .line 544211
    iput-object p5, p0, LX/3Hc;->h:LX/3Hd;

    .line 544212
    iput-object p6, p0, LX/3Hc;->i:LX/0SG;

    .line 544213
    iput-object p7, p0, LX/3Hc;->k:LX/0Zc;

    .line 544214
    iget-object v0, p8, LX/1b4;->b:LX/0Uh;

    const/16 v1, 0xea

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 544215
    iput-boolean v0, p0, LX/3Hc;->w:Z

    .line 544216
    return-void
.end method

.method public static a(LX/0QB;)LX/3Hc;
    .locals 1

    .prologue
    .line 544200
    invoke-static {p0}, LX/3Hc;->b(LX/0QB;)LX/3Hc;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 544194
    iput-object p1, p0, LX/3Hc;->m:Ljava/lang/String;

    .line 544195
    const/4 v0, -0x1

    iput v0, p0, LX/3Hc;->n:I

    .line 544196
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, LX/3Hc;->s:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 544197
    iput-boolean p2, p0, LX/3Hc;->q:Z

    .line 544198
    invoke-static {p0}, LX/3Hc;->e$redex0(LX/3Hc;)V

    .line 544199
    return-void
.end method

.method public static a$redex0(LX/3Hc;I)V
    .locals 2

    .prologue
    .line 544188
    iget v0, p0, LX/3Hc;->n:I

    if-eq v0, p1, :cond_0

    .line 544189
    iput p1, p0, LX/3Hc;->n:I

    .line 544190
    iget v0, p0, LX/3Hc;->n:I

    iget v1, p0, LX/3Hc;->o:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/3Hc;->o:I

    .line 544191
    iget-object v0, p0, LX/3Hc;->l:LX/3Ha;

    if-eqz v0, :cond_0

    .line 544192
    iget-object v0, p0, LX/3Hc;->l:LX/3Ha;

    invoke-interface {v0, p1}, LX/3Ha;->s_(I)V

    .line 544193
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/3Hc;
    .locals 9

    .prologue
    .line 544186
    new-instance v0, LX/3Hc;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v4

    check-cast v4, LX/19j;

    invoke-static {p0}, LX/3Hd;->a(LX/0QB;)LX/3Hd;

    move-result-object v5

    check-cast v5, LX/3Hd;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {p0}, LX/0Zc;->a(LX/0QB;)LX/0Zc;

    move-result-object v7

    check-cast v7, LX/0Zc;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v8

    check-cast v8, LX/1b4;

    invoke-direct/range {v0 .. v8}, LX/3Hc;-><init>(LX/03V;LX/1Ck;LX/0tX;LX/19j;LX/3Hd;LX/0SG;LX/0Zc;LX/1b4;)V

    .line 544187
    return-object v0
.end method

.method private d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 544127
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "liveStatusPoller"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/3Hc;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e$redex0(LX/3Hc;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 544165
    invoke-static {p0}, LX/3Hc;->g(LX/3Hc;)V

    .line 544166
    const-string v0, "polling"

    iput-object v0, p0, LX/3Hc;->t:Ljava/lang/String;

    .line 544167
    iget-object v0, p0, LX/3Hc;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/3Hc;->u:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/3Hc;->v:J

    .line 544168
    iget-object v0, p0, LX/3Hc;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, LX/3Hc;->u:J

    .line 544169
    invoke-static {p0}, LX/3Hc;->h(LX/3Hc;)V

    .line 544170
    iget-object v0, p0, LX/3Hc;->k:LX/0Zc;

    iget-object v2, p0, LX/3Hc;->m:Ljava/lang/String;

    sget-object v3, LX/7IM;->LIVE_POLLER_START:LX/7IM;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Live status polling started lobby: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, LX/3Hc;->q:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3, v4, v5}, LX/0Zc;->a(Ljava/lang/String;LX/7IM;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 544171
    iget-boolean v0, p0, LX/3Hc;->q:Z

    if-eqz v0, :cond_0

    .line 544172
    new-instance v0, LX/6TR;

    invoke-direct {v0}, LX/6TR;-><init>()V

    move-object v0, v0

    .line 544173
    const-string v2, "video_id"

    iget-object v3, p0, LX/3Hc;->m:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 544174
    const-string v2, "enable_read_only_viewer_count"

    iget-boolean v3, p0, LX/3Hc;->p:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 544175
    iget-object v2, p0, LX/3Hc;->e:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 544176
    iget-object v2, p0, LX/3Hc;->d:LX/1Ck;

    invoke-direct {p0}, LX/3Hc;->d()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/AgA;

    invoke-direct {v4, p0}, LX/AgA;-><init>(LX/3Hc;)V

    invoke-virtual {v2, v3, v0, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 544177
    :goto_0
    return-void

    .line 544178
    :cond_0
    iget-boolean v0, p0, LX/3Hc;->w:Z

    if-eqz v0, :cond_3

    .line 544179
    new-instance v0, LX/6TS;

    invoke-direct {v0}, LX/6TS;-><init>()V

    move-object v2, v0

    .line 544180
    const-string v0, "video_id"

    iget-object v3, p0, LX/3Hc;->m:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 544181
    const-string v3, "enable_read_only_viewer_count"

    iget-boolean v0, p0, LX/3Hc;->p:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/3Hc;->r:Z

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 544182
    iget-object v0, p0, LX/3Hc;->e:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 544183
    iget-object v2, p0, LX/3Hc;->d:LX/1Ck;

    invoke-direct {p0}, LX/3Hc;->d()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/AgB;

    invoke-direct {v4, p0}, LX/AgB;-><init>(LX/3Hc;)V

    invoke-virtual {v2, v3, v0, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 544184
    goto :goto_1

    .line 544185
    :cond_3
    iget-object v0, p0, LX/3Hc;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/3Hc;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static g(LX/3Hc;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0xbb8

    .line 544160
    iget-boolean v0, p0, LX/3Hc;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Hc;->s:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v0, v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 544161
    if-eqz v0, :cond_1

    .line 544162
    iget-object v0, p0, LX/3Hc;->f:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/livepoller/LiveStatusPoller$1;

    invoke-direct {v1, p0}, Lcom/facebook/facecastdisplay/livepoller/LiveStatusPoller$1;-><init>(LX/3Hc;)V

    const v2, 0x342b80cf

    invoke-static {v0, v1, v4, v5, v2}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 544163
    :goto_1
    return-void

    .line 544164
    :cond_1
    iget-object v0, p0, LX/3Hc;->f:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/livepoller/LiveStatusPoller$2;

    invoke-direct {v1, p0}, Lcom/facebook/facecastdisplay/livepoller/LiveStatusPoller$2;-><init>(LX/3Hc;)V

    const v2, -0x6659cc99

    invoke-static {v0, v1, v4, v5, v2}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/3Hc;)V
    .locals 4

    .prologue
    .line 544150
    iget-object v0, p0, LX/3Hc;->h:LX/3Hd;

    invoke-virtual {v0}, LX/3Hd;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 544151
    :goto_0
    return-void

    .line 544152
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 544153
    iget v1, p0, LX/3Hc;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " viewers"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 544154
    const-string v1, "\nreadOnly: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LX/3Hc;->p:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 544155
    const-string v1, "\nlobbyPolling: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LX/3Hc;->q:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 544156
    const-string v1, "\nusingShortPoller: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LX/3Hc;->w:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 544157
    const-string v1, "\ndurationBetweenLastPolls: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, LX/3Hc;->v:J

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sec"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 544158
    const-string v1, "\nstatus: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/3Hc;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 544159
    iget-object v1, p0, LX/3Hc;->h:LX/3Hd;

    sget-object v2, LX/3Hc;->b:Ljava/lang/String;

    iget-object v3, p0, LX/3Hc;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, LX/3Hd;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 544144
    iget-object v0, p0, LX/3Hc;->d:LX/1Ck;

    invoke-direct {p0}, LX/3Hc;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 544145
    iget-object v0, p0, LX/3Hc;->f:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 544146
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, LX/3Hc;->s:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 544147
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3Hc;->p:Z

    .line 544148
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Hc;->r:Z

    .line 544149
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 544142
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3Hc;->a(Ljava/lang/String;Z)V

    .line 544143
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 544139
    iput-boolean p1, p0, LX/3Hc;->p:Z

    .line 544140
    invoke-static {p0}, LX/3Hc;->h(LX/3Hc;)V

    .line 544141
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 544137
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/3Hc;->a(Ljava/lang/String;Z)V

    .line 544138
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 544128
    new-instance v0, LX/6TT;

    invoke-direct {v0}, LX/6TT;-><init>()V

    move-object v2, v0

    .line 544129
    const-string v0, "video_id"

    invoke-virtual {v2, v0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 544130
    const-string v3, "enable_read_only_viewer_count"

    iget-boolean v0, p0, LX/3Hc;->p:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/3Hc;->r:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 544131
    iget-object v0, p0, LX/3Hc;->g:LX/19j;

    iget-boolean v0, v0, LX/19j;->S:Z

    if-eqz v0, :cond_1

    .line 544132
    const-string v0, "scrubbing"

    const-string v3, "MPEG_DASH"

    invoke-virtual {v2, v0, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 544133
    :cond_1
    iget-object v0, p0, LX/3Hc;->e:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 544134
    iget-object v2, p0, LX/3Hc;->d:LX/1Ck;

    invoke-direct {p0}, LX/3Hc;->d()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/Ag9;

    invoke-direct {v4, p0}, LX/Ag9;-><init>(LX/3Hc;)V

    invoke-virtual {v2, v3, v0, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 544135
    return-void

    :cond_2
    move v0, v1

    .line 544136
    goto :goto_0
.end method
