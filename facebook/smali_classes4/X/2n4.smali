.class public LX/2n4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static volatile t:LX/2n4;


# instance fields
.field public e:Ljava/util/regex/Pattern;

.field public f:Ljava/util/regex/Pattern;

.field public g:Ljava/util/regex/Pattern;

.field public h:Ljava/util/regex/Pattern;

.field public i:Ljava/util/regex/Pattern;

.field public j:Ljava/util/regex/Pattern;

.field public k:Ljava/util/regex/Pattern;

.field public l:Ljava/util/regex/Pattern;

.field public m:Ljava/util/regex/Pattern;

.field public n:Ljava/util/regex/Pattern;

.field public o:Ljava/util/regex/Pattern;

.field public p:Ljava/util/regex/Pattern;

.field public q:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/0ad;

.field public s:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v2, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 463194
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "."

    aput-object v1, v0, v3

    const-string v1, ","

    aput-object v1, v0, v4

    const-string v1, "?"

    aput-object v1, v0, v5

    const-string v1, ":"

    aput-object v1, v0, v6

    const-string v1, "!"

    aput-object v1, v0, v2

    sput-object v0, LX/2n4;->a:[Ljava/lang/String;

    .line 463195
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "<small"

    aput-object v1, v0, v3

    const-string v1, "<script"

    aput-object v1, v0, v4

    sput-object v0, LX/2n4;->b:[Ljava/lang/String;

    .line 463196
    const/16 v0, 0x55

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "PLEASE CONTACT US"

    aput-object v1, v0, v3

    const-string v1, "PLEASE RATE THIS"

    aput-object v1, v0, v4

    const-string v1, "WHAT YOU THINK"

    aput-object v1, v0, v5

    const-string v1, "YOUR COMMENT"

    aput-object v1, v0, v6

    const-string v1, "A COMMENT"

    aput-object v1, v0, v2

    const/4 v1, 0x5

    const-string v2, "COMMENTER"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "READER VIEWS"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "READER POST"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "READER COMMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "THANK YOU FOR"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "ALL COMMENTS"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SIGN UP"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SIGNED UP"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "YOUR COMMENT"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "PRIVACY POLICY"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "TERM OF USE"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "EMAIL ADDRESS"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "E-MAIL ADDRESS"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "COPYRIGHT"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "TOP STORIES"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "ADVERTISEMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "OUTDATED BROWSER"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "YOUR BROWSER"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "UPGRADE YOUR BROWSER"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "RELATED:"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "WEEKLY UPDATES"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "YOU ARE SUBSCRIBED"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "SUBSCRIPTION"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "SUBSCRIBE TO"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "FAQ"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "ALL RIGHTS RESERVED"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "LIKE US"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "ENTERPRISES LLC"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "IMPROVE USER EXPERIENCE"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "TOPICS:"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "UPDATED"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "PUBLISHED:"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "MODIFIED:"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "POSTED"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "PHOTO BY"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "VIDEO BY"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "THIS CONTENT"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "THE CONTENT"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "THE VIEWS"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "FACEBOOK FEED"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "SHARE ON FACEBOOK"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "JAVASCRIPT"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "CSS"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "ON THIS PAGE"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "PLEASE CLICK"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "FIXING THIS ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "A BETTER SITE"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "REFRESH"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "CREATE AN ACCOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "NEW PASSWORD"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "TWEET"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "FOLLOW US"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "LATEST UPDATES"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "TRY AGAIN"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "FOR MORE DETAILS"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "LOG OUT"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "COOKIES"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "ALL CONTENT"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "YOUR FEEDBACK"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "NEWS FEED"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "THE SITE"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "THIS REPORT"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "SIGN IN"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "YOUR ACCOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "FIND OUT MORE ABOUT"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "INTERNET EXPLORER"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "THIS POST"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "A POST"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "CALL US"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "SEND UP TO"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "DELIVERS BUSINESS"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "MICROSOFT ACCOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "TO SUBSCRIBE"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "FACEBOOK MESSAGE"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "CLICK ON THE BUTTON"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "AD-FREE"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "BECOMING A MEMBER"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "SINGLE DONATION"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "NEXT COMMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "FACEBOOK MESSENGER"

    aput-object v2, v0, v1

    sput-object v0, LX/2n4;->c:[Ljava/lang/String;

    .line 463197
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "COMMENTS"

    aput-object v1, v0, v3

    const-string v1, "PLEASE HELP US"

    aput-object v1, v0, v4

    const-string v1, "CHAT WITH US"

    aput-object v1, v0, v5

    sput-object v0, LX/2n4;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463179
    iput-object p1, p0, LX/2n4;->r:LX/0ad;

    .line 463180
    const-string v0, "(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:\'\".,<>?]))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/2n4;->e:Ljava/util/regex/Pattern;

    .line 463181
    const-string v0, "\\d{2,4}/\\d{1,2}/\\d{2,4}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/2n4;->f:Ljava/util/regex/Pattern;

    .line 463182
    const-string v0, "(www.)*(.*?)\\.com"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/2n4;->g:Ljava/util/regex/Pattern;

    .line 463183
    const-string v0, "(<p>|<p[^>]+>)([\\s\\S]*?)</p>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/2n4;->h:Ljava/util/regex/Pattern;

    .line 463184
    const-string v0, "<title[^>]*>([\\s\\S]*?)</title>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/2n4;->i:Ljava/util/regex/Pattern;

    .line 463185
    const-string v0, "(data-lang|lang|xml:lang)\\s*(=|:)\\s*\"\\s*(en|en-US|en-CA|en-GB|en-IN)\\s*\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/2n4;->j:Ljava/util/regex/Pattern;

    .line 463186
    const-string v0, "<meta\\s+http-equiv\\s*=\\s*\"\\s*[c|C]ontent-[l|L]anguage\\s*\"\\s+content\\s*=\\s*\"\\s*(en|en-US|en-CA|en-GB|en-IN)\\s*\"\\s*/?>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/2n4;->k:Ljava/util/regex/Pattern;

    .line 463187
    const-string v0, "<meta\\s+property\\s*=\\s*\"\\s*og:locale\\s*\"\\s+content\\s*=\\s*\"\\s*(en_US|en_PI|en_GB|en_IN|en_UD)\\s*\"[^>]*?>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/2n4;->l:Ljava/util/regex/Pattern;

    .line 463188
    const-string v0, "<meta\\s+property\\s*=\\s*\"\\s*og:site_name\\s*\"\\s+content\\s*=\\s*\"([^>]*?)\"[^>]*?>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/2n4;->m:Ljava/util/regex/Pattern;

    .line 463189
    const-string v0, "<link[^>]+?rel\\s*=\\s*\"\\s*(apple-touch-icon|apple-touch-icon-precomposed)\\s*\"[^>]+?href\\s*=\\s*\"([^>]*?)\"[^>]*?>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/2n4;->n:Ljava/util/regex/Pattern;

    .line 463190
    const-string v0, "<link[^>]+?(href)\\s*=\\s*\"([^>]*?)\"[^>]+?rel\\s*=\\s*\"\\s*(apple-touch-icon|apple-touch-icon-precomposed)\\s*\"[^>]*?>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/2n4;->o:Ljava/util/regex/Pattern;

    .line 463191
    const-string v0, "[^>]*?\\.([^.]*?)\\.(edu|com|net|org|biz|info|tv|cc)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/2n4;->p:Ljava/util/regex/Pattern;

    .line 463192
    new-instance v0, Landroid/util/LruCache;

    const/16 p1, 0xa

    invoke-direct {v0, p1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, LX/2n4;->q:Landroid/util/LruCache;

    .line 463193
    return-void
.end method

.method public static a(LX/0QB;)LX/2n4;
    .locals 4

    .prologue
    .line 463165
    sget-object v0, LX/2n4;->t:LX/2n4;

    if-nez v0, :cond_1

    .line 463166
    const-class v1, LX/2n4;

    monitor-enter v1

    .line 463167
    :try_start_0
    sget-object v0, LX/2n4;->t:LX/2n4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 463168
    if-eqz v2, :cond_0

    .line 463169
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 463170
    new-instance p0, LX/2n4;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/2n4;-><init>(LX/0ad;)V

    .line 463171
    move-object v0, p0

    .line 463172
    sput-object v0, LX/2n4;->t:LX/2n4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463173
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 463174
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 463175
    :cond_1
    sget-object v0, LX/2n4;->t:LX/2n4;

    return-object v0

    .line 463176
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 463177
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2n4;Ljava/lang/String;Ljava/util/regex/Pattern;I)Ljava/lang/String;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 463108
    if-nez p1, :cond_0

    .line 463109
    const/4 v1, 0x0

    .line 463110
    :goto_0
    return-object v1

    .line 463111
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 463112
    move-object/from16 v0, p2

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 463113
    const/4 v4, 0x0

    .line 463114
    const/4 v3, 0x0

    .line 463115
    const/4 v2, 0x0

    .line 463116
    const/4 v1, 0x0

    move v5, v4

    move v4, v3

    .line 463117
    :cond_1
    :goto_1
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_5

    move/from16 v0, p3

    if-ge v5, v0, :cond_5

    .line 463118
    const/4 v3, 0x2

    invoke-virtual {v7, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 463119
    add-int/lit8 v1, v1, 0x1

    .line 463120
    if-eqz v3, :cond_1

    .line 463121
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 463122
    invoke-static {v3}, LX/2n4;->f(Ljava/lang/String;)Z

    move-result v8

    .line 463123
    const-string v9, "<p>"

    invoke-virtual {v3, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 463124
    const-string v9, "<p>"

    invoke-virtual {v3, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    .line 463125
    add-int/lit8 v9, v9, 0x3

    invoke-virtual {v3, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 463126
    :cond_2
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    .line 463127
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v10

    iget-object v11, p0, LX/2n4;->r:LX/0ad;

    sget v12, LX/1Bm;->j:I

    const/16 v13, 0x23

    invoke-interface {v11, v12, v13}, LX/0ad;->a(II)I

    move-result v11

    if-lt v10, v11, :cond_1

    .line 463128
    invoke-static {v3}, LX/2n4;->g(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 463129
    invoke-static {v3}, LX/2n4;->h(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 463130
    const-string v10, "&(#\\d+|#[\\w\\d]+|\\w+);"

    invoke-static {v3, v10}, LX/2n4;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 463131
    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v10}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, LX/2n4;->i(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 463132
    const-string v10, "<a[^>]*>([\\s\\S]*?)</a>"

    const/4 v11, 0x1

    invoke-static {v3, v10, v11}, LX/2n4;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 463133
    const-string v10, "<cite[^>]*>([\\s\\S]*?)</cite>"

    const/4 v11, 0x1

    invoke-static {v3, v10, v11}, LX/2n4;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 463134
    const-string v10, "<em[^>]*>([\\s\\S]*?)</em>"

    const/4 v11, 0x1

    invoke-static {v3, v10, v11}, LX/2n4;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 463135
    const-string v10, "<span[\\s\\S]+?>([\\s\\S]*?)</span>"

    const/4 v11, 0x1

    invoke-static {v3, v10, v11}, LX/2n4;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 463136
    const-string v10, "<i[^>]*>([\\s\\S]*?)</i>"

    const/4 v11, 0x1

    invoke-static {v3, v10, v11}, LX/2n4;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 463137
    const-string v10, "<b[^>]*>([\\s\\S]*?)</b>"

    const/4 v11, 0x1

    invoke-static {v3, v10, v11}, LX/2n4;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 463138
    const-string v10, "<time[^>]*>([\\s\\S]*?)</time>"

    const/4 v11, 0x1

    invoke-static {v3, v10, v11}, LX/2n4;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 463139
    const-string v10, "<strong[^>]*>([\\s\\S]*?)</strong>"

    const/4 v11, 0x1

    invoke-static {v3, v10, v11}, LX/2n4;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 463140
    const-string v10, "<br\\s*/>"

    invoke-static {v3, v10}, LX/2n4;->f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 463141
    const-string v10, "<([\\s\\S]*?)/?>"

    invoke-static {v3, v10}, LX/2n4;->f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 463142
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v10

    .line 463143
    iget-object v11, p0, LX/2n4;->r:LX/0ad;

    sget v12, LX/1Bm;->g:I

    const/4 v13, 0x6

    invoke-interface {v11, v12, v13}, LX/0ad;->a(II)I

    move-result v11

    invoke-static {v3, v11}, LX/2n4;->a(Ljava/lang/String;I)Z

    move-result v11

    if-nez v11, :cond_1

    .line 463144
    invoke-static {p0, v10, v9, v8}, LX/2n4;->a(LX/2n4;IIZ)Z

    move-result v8

    if-nez v8, :cond_1

    .line 463145
    invoke-static {p0, v3}, LX/2n4;->j(LX/2n4;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 463146
    invoke-static {p0, v3}, LX/2n4;->k(LX/2n4;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 463147
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 463148
    const/16 v8, 0xa

    const/16 v9, 0x20

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    .line 463149
    const-string v8, "\\"

    const-string v9, ""

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 463150
    const-string v8, "\\s{2,}"

    const-string v9, " "

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 463151
    invoke-static {p0, v2, v3}, LX/2n4;->d(LX/2n4;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-static {v2, v3}, LX/2n4;->e(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 463152
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 463153
    add-int/lit8 v5, v5, 0x1

    .line 463154
    add-int/lit8 v2, p3, -0x1

    if-ne v5, v2, :cond_3

    .line 463155
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, LX/2n4;->s:I

    .line 463156
    :cond_3
    invoke-static {v3}, LX/2n4;->e(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v4, v2

    .line 463157
    move/from16 v0, p3

    if-ge v5, v0, :cond_4

    .line 463158
    const-string v2, "\n\n"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    move v2, v4

    move v4, v5

    move v5, v4

    move v4, v2

    move-object v2, v3

    .line 463159
    goto/16 :goto_1

    .line 463160
    :cond_5
    iget-object v1, p0, LX/2n4;->r:LX/0ad;

    sget v2, LX/1Bm;->l:I

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    if-gt v5, v1, :cond_6

    iget-object v1, p0, LX/2n4;->r:LX/0ad;

    sget v2, LX/1Bm;->n:I

    const/16 v3, 0x32

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    if-gt v4, v1, :cond_6

    .line 463161
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 463162
    :cond_6
    iget-object v1, p0, LX/2n4;->r:LX/0ad;

    sget v2, LX/1Bm;->m:I

    const/16 v3, 0x32

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    if-gt v4, v1, :cond_7

    .line 463163
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 463164
    :cond_7
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 463093
    if-nez p0, :cond_0

    .line 463094
    const/4 v0, 0x0

    .line 463095
    :goto_0
    return-object v0

    .line 463096
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 463097
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 463098
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 463099
    const/4 v0, 0x0

    .line 463100
    :goto_1
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 463101
    invoke-virtual {v2, p2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 463102
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    .line 463103
    invoke-virtual {p0, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 463104
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 463105
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    goto :goto_1

    .line 463106
    :cond_1
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 463107
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/2n4;IIZ)Z
    .locals 4

    .prologue
    .line 463090
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    .line 463091
    iget-object v1, p0, LX/2n4;->r:LX/0ad;

    sget v2, LX/1Bm;->k:F

    const v3, 0x3ecccccd    # 0.4f

    invoke-interface {v1, v2, v3}, LX/0ad;->a(FF)F

    move-result v1

    .line 463092
    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;I)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 463082
    move v0, v1

    move v2, v1

    .line 463083
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 463084
    invoke-virtual {p0, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    const/16 v4, 0xa

    if-ne v3, v4, :cond_0

    .line 463085
    add-int/lit8 v2, v2, 0x1

    .line 463086
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 463087
    :cond_1
    if-le v2, p1, :cond_2

    .line 463088
    const/4 v1, 0x1

    .line 463089
    :cond_2
    return v1
.end method

.method private static c(LX/2n4;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 463066
    iget-object v0, p0, LX/2n4;->i:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 463067
    :cond_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 463068
    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 463069
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 463070
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 463071
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 463072
    const-string v0, "&(#\\d+|#[\\w\\d]+|\\w+);"

    invoke-static {v1, v0}, LX/2n4;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 463073
    const-string v1, "<([\\s\\S]*?)/?>"

    invoke-static {v0, v1}, LX/2n4;->f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 463074
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 463075
    const/16 v1, 0xa

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 463076
    const-string v1, "\\"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 463077
    const-string v1, "\\s{2,}"

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 463078
    const-string v1, "\\|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 463079
    if-eqz v1, :cond_1

    array-length v2, v1

    if-le v2, v3, :cond_1

    .line 463080
    const/4 v0, 0x0

    aget-object v0, v1, v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 463081
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/2n4;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 463060
    invoke-static {p1}, LX/2n4;->e(Ljava/lang/String;)I

    move-result v1

    .line 463061
    invoke-static {p2}, LX/2n4;->e(Ljava/lang/String;)I

    move-result v2

    .line 463062
    iget-object v3, p0, LX/2n4;->r:LX/0ad;

    sget v4, LX/1Bm;->o:I

    const/16 v5, 0x14

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    if-gt v2, v3, :cond_0

    .line 463063
    iget-object v2, p0, LX/2n4;->r:LX/0ad;

    sget v3, LX/1Bm;->p:I

    const/16 v4, 0x1e

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 463064
    :cond_0
    :goto_0
    return v0

    .line 463065
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 463055
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463056
    const/4 v0, 0x0

    .line 463057
    :goto_0
    return v0

    .line 463058
    :cond_0
    const-string v0, "\\s+"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 463059
    array-length v0, v0

    goto :goto_0
.end method

.method private static e(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 463198
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 463199
    :cond_0
    const/4 v0, 0x0

    .line 463200
    :goto_0
    return v0

    .line 463201
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 463202
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 463203
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 462870
    if-nez p0, :cond_0

    .line 462871
    const/4 v0, 0x0

    .line 462872
    :goto_0
    return-object v0

    .line 462873
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 462874
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 462875
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 462876
    const/4 v0, 0x0

    .line 462877
    :goto_1
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 462878
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->start()I

    move-result v3

    .line 462879
    invoke-virtual {p0, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462880
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    goto :goto_1

    .line 462881
    :cond_1
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462882
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static f(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 462883
    const-string v0, "img"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "<a"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "<time"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 462884
    :cond_0
    const/4 v0, 0x1

    .line 462885
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 462886
    if-nez p0, :cond_0

    .line 462887
    const/4 v0, 0x0

    .line 462888
    :goto_0
    return-object v0

    .line 462889
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 462890
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 462891
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 462892
    const/4 v0, 0x0

    .line 462893
    :cond_1
    :goto_1
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 462894
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    .line 462895
    if-eqz v3, :cond_1

    .line 462896
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    .line 462897
    invoke-virtual {p0, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462898
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    .line 462899
    invoke-static {v3}, LX/2n4;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462900
    const-string v4, "&amp;"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "&#38;"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 462901
    :cond_2
    invoke-virtual {p0, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    const/16 v4, 0x23

    if-ne v3, v4, :cond_4

    .line 462902
    const/16 v3, 0x3b

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 462903
    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    add-int/lit8 v4, v0, 0x5

    if-lt v4, v3, :cond_3

    .line 462904
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "&"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {p0, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2n4;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462905
    add-int/lit8 v0, v3, 0x1

    goto :goto_1

    .line 462906
    :cond_3
    const-string v3, "&"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 462907
    :cond_4
    const-string v3, "&"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 462908
    :cond_5
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462909
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private static g(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 462910
    sget-object v2, LX/2n4;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 462911
    invoke-virtual {p0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 462912
    const/4 v0, 0x1

    .line 462913
    :cond_0
    return v0

    .line 462914
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static h(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 462915
    sget-object v2, LX/2n4;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 462916
    invoke-virtual {p0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 462917
    const/4 v0, 0x1

    .line 462918
    :cond_0
    return v0

    .line 462919
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static i(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 462920
    sget-object v3, LX/2n4;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 462921
    invoke-virtual {p0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 462922
    :cond_0
    :goto_1
    return v0

    .line 462923
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 462924
    :cond_2
    sget-object v3, LX/2n4;->d:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 462925
    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 462926
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    .line 462927
    goto :goto_1
.end method

.method private static j(LX/2n4;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 462928
    const-string v1, "<a"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 462929
    iget-object v1, p0, LX/2n4;->e:Ljava/util/regex/Pattern;

    if-eqz v1, :cond_1

    .line 462930
    iget-object v1, p0, LX/2n4;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 462931
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 462932
    :cond_0
    :goto_0
    return v0

    .line 462933
    :cond_1
    iget-object v1, p0, LX/2n4;->g:Ljava/util/regex/Pattern;

    if-eqz v1, :cond_2

    .line 462934
    iget-object v1, p0, LX/2n4;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 462935
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-nez v1, :cond_0

    .line 462936
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static k(LX/2n4;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 462937
    iget-object v0, p0, LX/2n4;->f:Ljava/util/regex/Pattern;

    if-eqz v0, :cond_0

    .line 462938
    iget-object v0, p0, LX/2n4;->f:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 462939
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462940
    const/4 v0, 0x1

    .line 462941
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static l(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 462942
    if-nez p0, :cond_0

    .line 462943
    const/4 v0, 0x0

    .line 462944
    :goto_0
    return-object v0

    .line 462945
    :cond_0
    const-string v0, "&rsquo;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "&#8217;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "&#8216;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "&#39;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "&#039;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "&#x27;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "&lsquo;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 462946
    :cond_1
    const-string v0, "\'"

    goto :goto_0

    .line 462947
    :cond_2
    const-string v0, "&ndash;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "&mdash;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "&#8212;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "&#8211;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 462948
    :cond_3
    const-string v0, "-"

    goto :goto_0

    .line 462949
    :cond_4
    const-string v0, "&#8230;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "&hellip;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 462950
    :cond_5
    const-string v0, "..."

    goto :goto_0

    .line 462951
    :cond_6
    const-string v0, "&ldquo;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "&rdquo;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "&#8220;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "&#8221;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "&quot;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 462952
    :cond_7
    const-string v0, "\""

    goto/16 :goto_0

    .line 462953
    :cond_8
    const-string v0, "&nbsp;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "&#32;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 462954
    :cond_9
    const-string v0, " "

    goto/16 :goto_0

    .line 462955
    :cond_a
    const-string v0, ""

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/7ht;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 462956
    new-instance v0, LX/7ht;

    invoke-direct {v0}, LX/7ht;-><init>()V

    .line 462957
    iget-object v1, p0, LX/2n4;->r:LX/0ad;

    sget-short v2, LX/1Bm;->q:S

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 462958
    if-nez v1, :cond_1

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 462959
    if-nez p2, :cond_5

    .line 462960
    :cond_0
    :goto_0
    move v1, v1

    .line 462961
    if-nez v1, :cond_1

    .line 462962
    iput-object v3, v0, LX/7ht;->a:LX/7hu;

    .line 462963
    const-string v1, "non_english"

    iput-object v1, v0, LX/7ht;->b:Ljava/lang/String;

    .line 462964
    :goto_1
    return-object v0

    .line 462965
    :cond_1
    invoke-static {p0, p2}, LX/2n4;->c(LX/2n4;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 462966
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 462967
    iput-object v3, v0, LX/7ht;->a:LX/7hu;

    .line 462968
    const-string v1, "empty_title"

    iput-object v1, v0, LX/7ht;->b:Ljava/lang/String;

    goto :goto_1

    .line 462969
    :cond_2
    if-nez p2, :cond_8

    .line 462970
    const/4 v1, 0x0

    .line 462971
    :goto_2
    move-object v1, v1

    .line 462972
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 462973
    iput-object v3, v0, LX/7ht;->a:LX/7hu;

    .line 462974
    const-string v1, "content_too_short"

    iput-object v1, v0, LX/7ht;->b:Ljava/lang/String;

    goto :goto_1

    .line 462975
    :cond_3
    new-instance v2, LX/7hu;

    invoke-direct {v2}, LX/7hu;-><init>()V

    .line 462976
    invoke-static {p0, p2}, LX/2n4;->c(LX/2n4;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/7hu;->a:Ljava/lang/String;

    .line 462977
    iput-object v1, v2, LX/7hu;->c:Ljava/lang/String;

    .line 462978
    invoke-virtual {p0, p1, p2}, LX/2n4;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, LX/7hu;->b:Ljava/lang/String;

    .line 462979
    const/4 v9, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 462980
    if-nez p1, :cond_9

    move-object v1, v3

    .line 462981
    :cond_4
    :goto_3
    move-object v1, v1

    .line 462982
    iput-object v1, v2, LX/7hu;->d:Ljava/lang/String;

    .line 462983
    iget v1, p0, LX/2n4;->s:I

    move v1, v1

    .line 462984
    iput v1, v2, LX/7hu;->e:I

    .line 462985
    iput-object v2, v0, LX/7ht;->a:LX/7hu;

    .line 462986
    const-string v1, "Extracted"

    iput-object v1, v0, LX/7ht;->b:Ljava/lang/String;

    goto :goto_1

    .line 462987
    :cond_5
    iget-object v4, p0, LX/2n4;->j:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 462988
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_6

    move v1, v2

    .line 462989
    goto :goto_0

    .line 462990
    :cond_6
    iget-object v4, p0, LX/2n4;->k:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 462991
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_7

    move v1, v2

    .line 462992
    goto :goto_0

    .line 462993
    :cond_7
    iget-object v4, p0, LX/2n4;->l:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 462994
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    .line 462995
    goto :goto_0

    .line 462996
    :cond_8
    iget-object v1, p0, LX/2n4;->r:LX/0ad;

    sget v2, LX/1Bm;->r:I

    const/4 v4, 0x3

    invoke-interface {v1, v2, v4}, LX/0ad;->a(II)I

    move-result v1

    .line 462997
    iget-object v2, p0, LX/2n4;->h:Ljava/util/regex/Pattern;

    invoke-static {p0, p2, v2, v1}, LX/2n4;->a(LX/2n4;Ljava/lang/String;Ljava/util/regex/Pattern;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 462998
    :cond_9
    invoke-static {p1}, LX/047;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 462999
    iget-object v1, p0, LX/2n4;->q:Landroid/util/LruCache;

    invoke-virtual {v1, v6}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 463000
    if-nez v1, :cond_4

    .line 463001
    iget-object v1, p0, LX/2n4;->n:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 463002
    const/4 v4, 0x0

    move-object v1, v3

    .line 463003
    :cond_a
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v8

    if-eqz v8, :cond_b

    .line 463004
    invoke-virtual {v7, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 463005
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 463006
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 463007
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_a

    move v4, v5

    .line 463008
    :cond_b
    if-nez v4, :cond_d

    .line 463009
    iget-object v4, p0, LX/2n4;->o:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 463010
    :cond_c
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_d

    .line 463011
    invoke-virtual {v4, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 463012
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_c

    .line 463013
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 463014
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_c

    .line 463015
    :cond_d
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_e

    move-object v1, v3

    .line 463016
    goto/16 :goto_3

    .line 463017
    :cond_e
    invoke-static {v1}, LX/047;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 463018
    if-nez v3, :cond_4

    .line 463019
    const-string v3, "//"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 463020
    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 463021
    :cond_f
    const-string v3, "http:"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 463022
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "http://"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 463023
    :goto_4
    iget-object v3, p0, LX/2n4;->q:Landroid/util/LruCache;

    invoke-virtual {v3, v6, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 463024
    :cond_10
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x0

    const/16 v4, 0x2e

    .line 463025
    if-nez p1, :cond_1

    .line 463026
    :cond_0
    :goto_0
    return-object v1

    .line 463027
    :cond_1
    const/4 v0, 0x0

    .line 463028
    iget-object v2, p0, LX/2n4;->m:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 463029
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 463030
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 463031
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 463032
    :cond_2
    :goto_1
    move-object v0, v0

    .line 463033
    if-eqz v0, :cond_3

    move-object v1, v0

    .line 463034
    goto :goto_0

    .line 463035
    :cond_3
    invoke-static {p1}, LX/047;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 463036
    iget-object v2, p0, LX/2n4;->p:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 463037
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 463038
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 463039
    if-eqz v0, :cond_7

    .line 463040
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 463041
    :goto_2
    if-eqz v0, :cond_0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 463042
    :cond_4
    const-string v2, "www."

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "m."

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "3g."

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 463043
    :cond_5
    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 463044
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v0, v4, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 463045
    if-eq v3, v5, :cond_7

    .line 463046
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 463047
    :cond_6
    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 463048
    if-eq v2, v5, :cond_7

    .line 463049
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_2

    .line 463050
    :cond_8
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 463051
    const-string v3, "&(#\\d+|#[\\w\\d]+|\\w+);"

    invoke-static {v2, v3}, LX/2n4;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 463052
    const-string v3, "<([\\s\\S]*?)/?>"

    invoke-static {v2, v3}, LX/2n4;->f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 463053
    if-eqz v2, :cond_2

    .line 463054
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
