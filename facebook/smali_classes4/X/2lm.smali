.class public LX/2lm;
.super LX/1jx;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/18L;

.field public c:Z


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 458375
    invoke-direct {p0}, LX/1jx;-><init>()V

    .line 458376
    new-instance v0, LX/18L;

    invoke-direct {v0}, LX/18L;-><init>()V

    iput-object v0, p0, LX/2lm;->b:LX/18L;

    .line 458377
    iput-object p1, p0, LX/2lm;->a:Ljava/util/Map;

    .line 458378
    return-void
.end method

.method private static a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 458379
    instance-of v0, p0, Ljava/lang/Enum;

    if-eqz v0, :cond_0

    const-string v0, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    check-cast p0, Ljava/lang/Enum;

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 458380
    sget-object v1, LX/16f;->a:Ljava/lang/Object;

    if-ne p0, v1, :cond_1

    .line 458381
    :cond_0
    :goto_0
    return v0

    .line 458382
    :cond_1
    invoke-static {p0}, LX/2lm;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, LX/2lm;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 458383
    invoke-static {p0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0jT;)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 458384
    instance-of v0, p1, LX/16i;

    if-eqz v0, :cond_2

    instance-of v0, p1, LX/16f;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 458385
    check-cast v0, LX/16f;

    .line 458386
    check-cast p1, LX/16i;

    invoke-interface {p1}, LX/16i;->a()Ljava/lang/String;

    move-result-object v1

    .line 458387
    iget-object v3, p0, LX/2lm;->a:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 458388
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 458389
    iget-object v3, p0, LX/2lm;->b:LX/18L;

    const/4 v7, 0x0

    .line 458390
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v6, v7

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 458391
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 458392
    invoke-interface {v0, v5, v3}, LX/16f;->a(Ljava/lang/String;LX/18L;)V

    .line 458393
    iget-object p1, v3, LX/18L;->a:Ljava/lang/Object;

    .line 458394
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    .line 458395
    invoke-static {p1, v4}, LX/2lm;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 458396
    invoke-interface {v0, v5, v4, v7}, LX/16f;->a(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 458397
    const/4 v4, 0x1

    :goto_1
    move v6, v4

    .line 458398
    goto :goto_0

    .line 458399
    :cond_0
    move v0, v6

    .line 458400
    iget-boolean v1, p0, LX/2lm;->c:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    :goto_2
    iput-boolean v0, p0, LX/2lm;->c:Z

    .line 458401
    :cond_2
    return v2

    .line 458402
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    move v4, v6

    goto :goto_1
.end method
