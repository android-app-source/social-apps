.class public LX/2o0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final h:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final i:Ljava/lang/Object;


# instance fields
.field public final b:LX/0lC;

.field public final c:LX/0lp;

.field public final d:LX/0sO;

.field public final e:LX/0ti;

.field public final f:LX/1fb;

.field public final g:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 465527
    const-class v0, LX/2o0;

    sput-object v0, LX/2o0;->a:Ljava/lang/Class;

    .line 465528
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/2o0;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 465529
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2o0;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0lC;LX/0lp;LX/0sO;LX/0ti;LX/1fb;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 465530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465531
    iput-object p1, p0, LX/2o0;->b:LX/0lC;

    .line 465532
    iput-object p2, p0, LX/2o0;->c:LX/0lp;

    .line 465533
    iput-object p3, p0, LX/2o0;->d:LX/0sO;

    .line 465534
    iput-object p4, p0, LX/2o0;->e:LX/0ti;

    .line 465535
    iput-object p5, p0, LX/2o0;->f:LX/1fb;

    .line 465536
    iput-object p6, p0, LX/2o0;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 465537
    return-void
.end method

.method public static a(LX/0QB;)LX/2o0;
    .locals 14

    .prologue
    .line 465538
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 465539
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 465540
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 465541
    if-nez v1, :cond_0

    .line 465542
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 465543
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 465544
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 465545
    sget-object v1, LX/2o0;->i:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 465546
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 465547
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 465548
    :cond_1
    if-nez v1, :cond_4

    .line 465549
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 465550
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 465551
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 465552
    new-instance v7, LX/2o0;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v8

    check-cast v8, LX/0lC;

    invoke-static {v0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v9

    check-cast v9, LX/0lp;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v10

    check-cast v10, LX/0sO;

    invoke-static {v0}, LX/0ti;->b(LX/0QB;)LX/0ti;

    move-result-object v11

    check-cast v11, LX/0ti;

    invoke-static {v0}, LX/1fb;->b(LX/0QB;)LX/1fb;

    move-result-object v12

    check-cast v12, LX/1fb;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v13

    check-cast v13, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct/range {v7 .. v13}, LX/2o0;-><init>(LX/0lC;LX/0lp;LX/0sO;LX/0ti;LX/1fb;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 465553
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 465554
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 465555
    if-nez v1, :cond_2

    .line 465556
    sget-object v0, LX/2o0;->i:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2o0;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 465557
    :goto_1
    if-eqz v0, :cond_3

    .line 465558
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 465559
    :goto_3
    check-cast v0, LX/2o0;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 465560
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 465561
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 465562
    :catchall_1
    move-exception v0

    .line 465563
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 465564
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 465565
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 465566
    :cond_2
    :try_start_8
    sget-object v0, LX/2o0;->i:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2o0;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method
