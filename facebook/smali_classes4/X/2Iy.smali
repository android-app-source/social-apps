.class public final LX/2Iy;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    .line 392443
    sget-object v0, LX/2Iz;->a:LX/0U1;

    sget-object v1, LX/2Iz;->b:LX/0U1;

    sget-object v2, LX/2Iz;->c:LX/0U1;

    sget-object v3, LX/2Iz;->d:LX/0U1;

    sget-object v4, LX/2Iz;->e:LX/0U1;

    sget-object v5, LX/2Iz;->f:LX/0U1;

    sget-object v6, LX/2Iz;->g:LX/0U1;

    sget-object v7, LX/2Iz;->h:LX/0U1;

    sget-object v8, LX/2Iz;->i:LX/0U1;

    sget-object v9, LX/2Iz;->j:LX/0U1;

    sget-object v10, LX/2Iz;->k:LX/0U1;

    sget-object v11, LX/2Iz;->l:LX/0U1;

    const/16 v12, 0x12

    new-array v12, v12, [LX/0U1;

    const/4 v13, 0x0

    sget-object v14, LX/2Iz;->m:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    sget-object v14, LX/2Iz;->n:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x2

    sget-object v14, LX/2Iz;->o:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x3

    sget-object v14, LX/2Iz;->p:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x4

    sget-object v14, LX/2Iz;->q:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x5

    sget-object v14, LX/2Iz;->r:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x6

    sget-object v14, LX/2Iz;->s:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x7

    sget-object v14, LX/2Iz;->t:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x8

    sget-object v14, LX/2Iz;->u:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x9

    sget-object v14, LX/2Iz;->v:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xa

    sget-object v14, LX/2Iz;->w:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xb

    sget-object v14, LX/2Iz;->x:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xc

    sget-object v14, LX/2Iz;->y:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xd

    sget-object v14, LX/2Iz;->z:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xe

    sget-object v14, LX/2Iz;->A:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xf

    sget-object v14, LX/2Iz;->B:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x10

    sget-object v14, LX/2Iz;->C:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x11

    sget-object v14, LX/2Iz;->D:LX/0U1;

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2Iy;->a:LX/0Px;

    .line 392444
    const-string v0, "contacts"

    const-string v1, "contact_index_by_fbid"

    sget-object v2, LX/2Iz;->c:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Iy;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 392445
    const-string v0, "contacts"

    sget-object v1, LX/2Iy;->a:LX/0Px;

    invoke-direct {p0, v0, v1}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 392446
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 392447
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 392448
    sget-object v0, LX/2Iy;->b:Ljava/lang/String;

    const v1, 0x5c5b768d

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x527ce3eb

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392449
    return-void
.end method
