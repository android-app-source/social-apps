.class public LX/3C2;
.super LX/0Tr;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3C2;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/3C3;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 529609
    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    const-string v1, "location_signal_package_db"

    invoke-direct {p0, p1, p2, v0, v1}, LX/0Tr;-><init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V

    .line 529610
    return-void
.end method

.method public static a(LX/0QB;)LX/3C2;
    .locals 6

    .prologue
    .line 529611
    sget-object v0, LX/3C2;->a:LX/3C2;

    if-nez v0, :cond_1

    .line 529612
    const-class v1, LX/3C2;

    monitor-enter v1

    .line 529613
    :try_start_0
    sget-object v0, LX/3C2;->a:LX/3C2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 529614
    if-eqz v2, :cond_0

    .line 529615
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 529616
    new-instance p0, LX/3C2;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/3C3;->a(LX/0QB;)LX/3C3;

    move-result-object v5

    check-cast v5, LX/3C3;

    invoke-direct {p0, v3, v4, v5}, LX/3C2;-><init>(Landroid/content/Context;LX/0Tt;LX/3C3;)V

    .line 529617
    move-object v0, p0

    .line 529618
    sput-object v0, LX/3C2;->a:LX/3C2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529619
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 529620
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 529621
    :cond_1
    sget-object v0, LX/3C2;->a:LX/3C2;

    return-object v0

    .line 529622
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 529623
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final j()Landroid/database/DatabaseErrorHandler;
    .locals 2

    .prologue
    .line 529606
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    .line 529607
    new-instance v0, LX/255;

    invoke-direct {v0}, LX/255;-><init>()V

    .line 529608
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, LX/0Tr;->j()Landroid/database/DatabaseErrorHandler;

    move-result-object v0

    goto :goto_0
.end method
