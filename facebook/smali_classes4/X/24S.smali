.class public LX/24S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24L;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aQ;",
        ">",
        "Ljava/lang/Object;",
        "LX/24L",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public a:LX/1Qa;

.field public b:LX/24B;

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/5oY;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1k4;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:LX/1RN;


# direct methods
.method public constructor <init>(LX/0Or;LX/24B;LX/0Or;LX/1Qa;)V
    .locals 0
    .param p4    # LX/1Qa;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1k4;",
            ">;",
            "LX/24B;",
            "LX/0Or",
            "<",
            "LX/5oY;",
            ">;",
            "LX/1Qa;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 367194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367195
    iput-object p4, p0, LX/24S;->a:LX/1Qa;

    .line 367196
    iput-object p3, p0, LX/24S;->c:LX/0Or;

    .line 367197
    iput-object p2, p0, LX/24S;->b:LX/24B;

    .line 367198
    iput-object p1, p0, LX/24S;->d:LX/0Or;

    .line 367199
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1RN;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "LX/1RN;",
            ")V"
        }
    .end annotation

    .prologue
    .line 367206
    iput-object p1, p0, LX/24S;->e:Landroid/view/View;

    .line 367207
    check-cast p1, LX/1aQ;

    invoke-interface {p1}, LX/1aQ;->getFlyoutXoutButton()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/24S;->f:Landroid/view/View;

    .line 367208
    iput-object p2, p0, LX/24S;->g:LX/1RN;

    .line 367209
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x2

    const v0, 0x58ff813f

    invoke-static {v6, v7, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 367200
    iget-object v0, p0, LX/24S;->e:Landroid/view/View;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 367201
    iget-object v0, p0, LX/24S;->f:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 367202
    iget-object v0, p0, LX/24S;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oY;

    iget-object v2, p0, LX/24S;->g:LX/1RN;

    iget-object v2, v2, LX/1RN;->a:LX/1kK;

    invoke-interface {v2}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/5oY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 367203
    iget-object v0, p0, LX/24S;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1k4;

    new-instance v3, LX/1k8;

    iget-object v4, p0, LX/24S;->g:LX/1RN;

    iget-object v5, p0, LX/24S;->g:LX/1RN;

    invoke-static {v5, v2}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(LX/1RN;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v2

    const/4 v5, 0x0

    invoke-direct {v3, v4, v2, v5, v7}, LX/1k8;-><init>(LX/1RN;Lcom/facebook/productionprompts/logging/PromptAnalytics;ZZ)V

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b7;)V

    .line 367204
    iget-object v0, p0, LX/24S;->b:LX/24B;

    iget-object v2, p0, LX/24S;->g:LX/1RN;

    iget-object v3, p0, LX/24S;->a:LX/1Qa;

    sget-object v4, LX/5S9;->XOUT:LX/5S9;

    invoke-virtual {v0, v2, v3, v4}, LX/24B;->a(LX/1RN;LX/1Qa;LX/5S9;)V

    .line 367205
    const v0, -0x746606ec

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
