.class public final enum LX/2Aa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Aa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Aa;

.field public static final enum API_EC_DOMAIN:LX/2Aa;

.field public static final enum GRAPHQL_KERROR_DOMAIN:LX/2Aa;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 377419
    new-instance v0, LX/2Aa;

    const-string v1, "API_EC_DOMAIN"

    invoke-direct {v0, v1, v2}, LX/2Aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Aa;->API_EC_DOMAIN:LX/2Aa;

    .line 377420
    new-instance v0, LX/2Aa;

    const-string v1, "GRAPHQL_KERROR_DOMAIN"

    invoke-direct {v0, v1, v3}, LX/2Aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Aa;->GRAPHQL_KERROR_DOMAIN:LX/2Aa;

    .line 377421
    const/4 v0, 0x2

    new-array v0, v0, [LX/2Aa;

    sget-object v1, LX/2Aa;->API_EC_DOMAIN:LX/2Aa;

    aput-object v1, v0, v2

    sget-object v1, LX/2Aa;->GRAPHQL_KERROR_DOMAIN:LX/2Aa;

    aput-object v1, v0, v3

    sput-object v0, LX/2Aa;->$VALUES:[LX/2Aa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 377418
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Aa;
    .locals 1

    .prologue
    .line 377417
    const-class v0, LX/2Aa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Aa;

    return-object v0
.end method

.method public static values()[LX/2Aa;
    .locals 1

    .prologue
    .line 377416
    sget-object v0, LX/2Aa;->$VALUES:[LX/2Aa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Aa;

    return-object v0
.end method
