.class public final LX/2so;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/348;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/346;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/5pU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Landroid/app/Application;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:LX/341;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/344;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/0o1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/342;

.field public l:Landroid/app/Activity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/98l;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/345;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z

.field public p:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 473918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 473919
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2so;->a:Ljava/util/List;

    .line 473920
    sget-object v0, LX/342;->a:LX/342;

    iput-object v0, p0, LX/2so;->k:LX/342;

    .line 473921
    return-void
.end method


# virtual methods
.method public final a(LX/346;)LX/2so;
    .locals 1

    .prologue
    .line 473922
    iput-object p1, p0, LX/2so;->c:LX/346;

    .line 473923
    const/4 v0, 0x0

    iput-object v0, p0, LX/2so;->b:Ljava/lang/String;

    .line 473924
    return-object p0
.end method

.method public final a(LX/348;)LX/2so;
    .locals 1

    .prologue
    .line 473925
    iget-object v0, p0, LX/2so;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 473926
    return-object p0
.end method

.method public final a()LX/33y;
    .locals 17

    .prologue
    .line 473927
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2so;->f:Landroid/app/Application;

    const-string v2, "Application property has not been set with this builder"

    invoke-static {v1, v2}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 473928
    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/2so;->g:Z

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, LX/2so;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, LX/2so;->c:LX/346;

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_0
    const-string v2, "JS Bundle File or Asset URL has to be provided when dev support is disabled"

    invoke-static {v1, v2}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 473929
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2so;->d:Ljava/lang/String;

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, LX/2so;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, LX/2so;->c:LX/346;

    if-eqz v1, :cond_4

    :cond_1
    const/4 v1, 0x1

    :goto_1
    const-string v2, "Either MainModuleName or JS Bundle File needs to be provided"

    invoke-static {v1, v2}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 473930
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2so;->i:LX/344;

    if-nez v1, :cond_2

    .line 473931
    new-instance v1, LX/344;

    invoke-direct {v1}, LX/344;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, LX/2so;->i:LX/344;

    .line 473932
    :cond_2
    new-instance v1, LX/9mu;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2so;->f:Landroid/app/Application;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2so;->l:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2so;->m:LX/98l;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2so;->c:LX/346;

    if-nez v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2so;->b:Ljava/lang/String;

    if-eqz v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2so;->f:Landroid/app/Application;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/2so;->b:Ljava/lang/String;

    invoke-static {v5, v6}, LX/346;->a(Landroid/content/Context;Ljava/lang/String;)LX/346;

    move-result-object v5

    :goto_2
    move-object/from16 v0, p0

    iget-object v6, v0, LX/2so;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2so;->a:Ljava/util/List;

    move-object/from16 v0, p0

    iget-boolean v8, v0, LX/2so;->g:Z

    move-object/from16 v0, p0

    iget-object v9, v0, LX/2so;->e:LX/5pU;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2so;->h:LX/341;

    const-string v11, "Initial lifecycle state was not set"

    invoke-static {v10, v11}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/341;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2so;->i:LX/344;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2so;->j:LX/0o1;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2so;->k:LX/342;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/2so;->n:LX/345;

    move-object/from16 v0, p0

    iget-boolean v15, v0, LX/2so;->o:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/2so;->p:Z

    move/from16 v16, v0

    invoke-direct/range {v1 .. v16}, LX/9mu;-><init>(Landroid/content/Context;Landroid/app/Activity;LX/98l;LX/346;Ljava/lang/String;Ljava/util/List;ZLX/5pU;LX/341;LX/344;LX/0o1;LX/342;LX/345;ZZ)V

    return-object v1

    .line 473933
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 473934
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 473935
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, LX/2so;->c:LX/346;

    goto :goto_2
.end method
