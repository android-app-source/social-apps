.class public LX/2jl;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/95n;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 454228
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 454229
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;JLX/2js;LX/0QK;I)LX/95n;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "J",
            "LX/2js",
            "<TTEdge;>;",
            "LX/0QK",
            "<TTEdge;",
            "Ljava/lang/String;",
            ">;I)",
            "LX/95n",
            "<TTEdge;>;"
        }
    .end annotation

    .prologue
    .line 454230
    new-instance v2, LX/95n;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v9

    check-cast v9, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/Executor;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/Executor;

    invoke-static/range {p0 .. p0}, LX/2jz;->a(LX/0QB;)LX/2jz;

    move-result-object v12

    check-cast v12, LX/2jz;

    invoke-static/range {p0 .. p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v13

    check-cast v13, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {p0 .. p0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v14

    check-cast v14, LX/1BA;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v15

    check-cast v15, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v16

    check-cast v16, LX/03V;

    const-class v3, LX/0ox;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/0ox;

    move-object/from16 v3, p1

    move-wide/from16 v4, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    invoke-direct/range {v2 .. v17}, LX/95n;-><init>(Ljava/lang/String;JLX/2js;LX/0QK;ILX/0Sh;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/2jz;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1BA;LX/0SG;LX/03V;LX/0ox;)V

    .line 454231
    return-object v2
.end method
