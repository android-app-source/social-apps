.class public LX/2t4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field public static final b:LX/0Tn;

.field private static volatile g:LX/2t4;


# instance fields
.field public final c:LX/0Zb;

.field public final d:LX/0SG;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:LX/0xB;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 474569
    const-class v0, LX/2t4;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2t4;->a:Ljava/lang/String;

    .line 474570
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "last_marketplace_tab_impression_logging_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2t4;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0xB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 474571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 474572
    iput-object p1, p0, LX/2t4;->c:LX/0Zb;

    .line 474573
    iput-object p2, p0, LX/2t4;->d:LX/0SG;

    .line 474574
    iput-object p3, p0, LX/2t4;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 474575
    iput-object p4, p0, LX/2t4;->f:LX/0xB;

    .line 474576
    return-void
.end method

.method public static a(LX/0QB;)LX/2t4;
    .locals 7

    .prologue
    .line 474577
    sget-object v0, LX/2t4;->g:LX/2t4;

    if-nez v0, :cond_1

    .line 474578
    const-class v1, LX/2t4;

    monitor-enter v1

    .line 474579
    :try_start_0
    sget-object v0, LX/2t4;->g:LX/2t4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 474580
    if-eqz v2, :cond_0

    .line 474581
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 474582
    new-instance p0, LX/2t4;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v6

    check-cast v6, LX/0xB;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2t4;-><init>(LX/0Zb;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0xB;)V

    .line 474583
    move-object v0, p0

    .line 474584
    sput-object v0, LX/2t4;->g:LX/2t4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 474585
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 474586
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 474587
    :cond_1
    sget-object v0, LX/2t4;->g:LX/2t4;

    return-object v0

    .line 474588
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 474589
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
