.class public LX/3Ll;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3Ll;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 551985
    const-string v0, "voicemail_summary"

    const/4 v1, 0x1

    new-instance v2, LX/3Lm;

    invoke-direct {v2}, LX/3Lm;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 551986
    return-void
.end method

.method public static a(LX/0QB;)LX/3Ll;
    .locals 3

    .prologue
    .line 551987
    sget-object v0, LX/3Ll;->a:LX/3Ll;

    if-nez v0, :cond_1

    .line 551988
    const-class v1, LX/3Ll;

    monitor-enter v1

    .line 551989
    :try_start_0
    sget-object v0, LX/3Ll;->a:LX/3Ll;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 551990
    if-eqz v2, :cond_0

    .line 551991
    :try_start_1
    new-instance v0, LX/3Ll;

    invoke-direct {v0}, LX/3Ll;-><init>()V

    .line 551992
    move-object v0, v0

    .line 551993
    sput-object v0, LX/3Ll;->a:LX/3Ll;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551994
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 551995
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551996
    :cond_1
    sget-object v0, LX/3Ll;->a:LX/3Ll;

    return-object v0

    .line 551997
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 551998
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 551999
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 552000
    return-void
.end method
