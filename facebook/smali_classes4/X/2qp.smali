.class public final LX/2qp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel$AllPhonesModel;",
        "Lcom/facebook/user/model/UserPhoneNumber;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 471457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 471458
    check-cast p1, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel$AllPhonesModel;

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 471459
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel$AllPhonesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 471460
    invoke-virtual {p1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel$AllPhonesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 471461
    invoke-virtual {p1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel$AllPhonesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v6, v0, LX/1vs;->b:I

    .line 471462
    invoke-virtual {p1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel$AllPhonesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v7, v0, LX/1vs;->a:LX/15i;

    iget v8, v0, LX/1vs;->b:I

    .line 471463
    new-instance v0, Lcom/facebook/user/model/UserPhoneNumber;

    invoke-virtual {v2, v4, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v6, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v8, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPhoneType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhoneType;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLPhoneType;->ordinal()I

    move-result v4

    invoke-virtual {p1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel$AllPhonesModel;->a()Z

    move-result v5

    invoke-static {v5}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/03R;)V

    .line 471464
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 471465
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 471466
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
