.class public LX/2KE;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/03R;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/03R;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 393556
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/03R;
    .locals 3

    .prologue
    .line 393558
    sget-object v0, LX/2KE;->a:LX/03R;

    if-nez v0, :cond_1

    .line 393559
    const-class v1, LX/2KE;

    monitor-enter v1

    .line 393560
    :try_start_0
    sget-object v0, LX/2KE;->a:LX/03R;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 393561
    if-eqz v2, :cond_0

    .line 393562
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 393563
    const-class p0, LX/2Cl;

    invoke-interface {v0, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p0

    check-cast p0, LX/2Cl;

    invoke-static {p0}, LX/27G;->d(LX/2Cl;)LX/03R;

    move-result-object p0

    move-object v0, p0

    .line 393564
    sput-object v0, LX/2KE;->a:LX/03R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 393565
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 393566
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 393567
    :cond_1
    sget-object v0, LX/2KE;->a:LX/03R;

    return-object v0

    .line 393568
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 393569
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 393557
    const-class v0, LX/2Cl;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/2Cl;

    invoke-static {v0}, LX/27G;->d(LX/2Cl;)LX/03R;

    move-result-object v0

    return-object v0
.end method
