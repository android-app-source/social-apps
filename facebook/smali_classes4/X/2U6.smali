.class public LX/2U6;
.super LX/1Eg;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:J

.field private static volatile h:LX/2U6;


# instance fields
.field private final b:LX/2U1;

.field private final c:LX/0SG;

.field private final d:LX/0W3;

.field public final e:LX/2Pd;

.field public final f:LX/2Pi;

.field private volatile g:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 415138
    sget-wide v0, LX/0X5;->l:J

    sput-wide v0, LX/2U6;->a:J

    return-void
.end method

.method public constructor <init>(LX/2U1;LX/0SG;LX/0W3;LX/2Pd;LX/2Pi;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415139
    const-string v0, "OFFLINE_LWI_MUTATION_CACHE_LOADING_TASK"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 415140
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/2U6;->g:J

    .line 415141
    iput-object p1, p0, LX/2U6;->b:LX/2U1;

    .line 415142
    iput-object p2, p0, LX/2U6;->c:LX/0SG;

    .line 415143
    iput-object p3, p0, LX/2U6;->d:LX/0W3;

    .line 415144
    iput-object p4, p0, LX/2U6;->e:LX/2Pd;

    .line 415145
    iput-object p5, p0, LX/2U6;->f:LX/2Pi;

    .line 415146
    return-void
.end method

.method public static a(LX/0QB;)LX/2U6;
    .locals 9

    .prologue
    .line 415147
    sget-object v0, LX/2U6;->h:LX/2U6;

    if-nez v0, :cond_1

    .line 415148
    const-class v1, LX/2U6;

    monitor-enter v1

    .line 415149
    :try_start_0
    sget-object v0, LX/2U6;->h:LX/2U6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 415150
    if-eqz v2, :cond_0

    .line 415151
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 415152
    new-instance v3, LX/2U6;

    invoke-static {v0}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v4

    check-cast v4, LX/2U1;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v6

    check-cast v6, LX/0W3;

    invoke-static {v0}, LX/2Pd;->a(LX/0QB;)LX/2Pd;

    move-result-object v7

    check-cast v7, LX/2Pd;

    invoke-static {v0}, LX/2Pi;->a(LX/0QB;)LX/2Pi;

    move-result-object v8

    check-cast v8, LX/2Pi;

    invoke-direct/range {v3 .. v8}, LX/2U6;-><init>(LX/2U1;LX/0SG;LX/0W3;LX/2Pd;LX/2Pi;)V

    .line 415153
    move-object v0, v3

    .line 415154
    sput-object v0, LX/2U6;->h:LX/2U6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415155
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 415156
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 415157
    :cond_1
    sget-object v0, LX/2U6;->h:LX/2U6;

    return-object v0

    .line 415158
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 415159
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private l()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 415160
    iget-object v1, p0, LX/2U6;->b:LX/2U1;

    invoke-virtual {v1}, LX/2U1;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2U6;->d:LX/0W3;

    sget-wide v2, LX/2U6;->a:J

    invoke-interface {v1, v2, v3, v0}, LX/0W4;->a(JZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final c()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415161
    const-string v0, "ADMINED_PAGES_PREFETCH_TASK"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final f()J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 415162
    invoke-direct {p0}, LX/2U6;->l()Z

    move-result v2

    if-nez v2, :cond_1

    .line 415163
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-wide v2, p0, LX/2U6;->g:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    iget-object v0, p0, LX/2U6;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415164
    sget-object v0, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 415165
    invoke-direct {p0}, LX/2U6;->l()Z

    move-result v1

    if-nez v1, :cond_1

    .line 415166
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p0, LX/2U6;->g:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415167
    iget-object v0, p0, LX/2U6;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/2U6;->g:J

    .line 415168
    iget-object v0, p0, LX/2U6;->e:LX/2Pd;

    .line 415169
    iget-object v1, v0, LX/2Pd;->b:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->b()V

    .line 415170
    sget-object v1, LX/2Ph;->c:LX/0U1;

    const-string v2, "0"

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 415171
    iget-object v2, v0, LX/2Pd;->a:LX/2Pe;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "offline_lwi_table"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 415172
    iget-object v0, p0, LX/2U6;->f:LX/2Pi;

    iget-object v1, p0, LX/2U6;->e:LX/2Pd;

    invoke-virtual {v1}, LX/2Pd;->b()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Pi;->a(Ljava/util/Set;)V

    .line 415173
    const/4 v0, 0x0

    return-object v0
.end method
