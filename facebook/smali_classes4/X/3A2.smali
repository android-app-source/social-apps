.class public LX/3A2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3A2;


# instance fields
.field private final a:LX/0re;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0re",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "LX/79S;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0rd;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 524657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524658
    const/16 v0, 0xc8

    const-string v1, "rtc_presence"

    invoke-virtual {p1, v0, v1}, LX/0rd;->a(ILjava/lang/String;)LX/0re;

    move-result-object v0

    iput-object v0, p0, LX/3A2;->a:LX/0re;

    .line 524659
    return-void
.end method

.method public static a(LX/0QB;)LX/3A2;
    .locals 4

    .prologue
    .line 524660
    sget-object v0, LX/3A2;->b:LX/3A2;

    if-nez v0, :cond_1

    .line 524661
    const-class v1, LX/3A2;

    monitor-enter v1

    .line 524662
    :try_start_0
    sget-object v0, LX/3A2;->b:LX/3A2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 524663
    if-eqz v2, :cond_0

    .line 524664
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 524665
    new-instance p0, LX/3A2;

    invoke-static {v0}, LX/0rZ;->a(LX/0QB;)LX/0rd;

    move-result-object v3

    check-cast v3, LX/0rd;

    invoke-direct {p0, v3}, LX/3A2;-><init>(LX/0rd;)V

    .line 524666
    move-object v0, p0

    .line 524667
    sput-object v0, LX/3A2;->b:LX/3A2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 524668
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 524669
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 524670
    :cond_1
    sget-object v0, LX/3A2;->b:LX/3A2;

    return-object v0

    .line 524671
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 524672
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/user/model/UserKey;)LX/79S;
    .locals 1

    .prologue
    .line 524650
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 524651
    :try_start_0
    iget-object v0, p0, LX/3A2;->a:LX/0re;

    invoke-virtual {v0, p1}, LX/0re;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79S;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 524652
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 524653
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/user/model/UserKey;LX/79S;)V
    .locals 1

    .prologue
    .line 524654
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3A2;->a:LX/0re;

    invoke-virtual {v0, p1, p2}, LX/0re;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 524655
    monitor-exit p0

    return-void

    .line 524656
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
