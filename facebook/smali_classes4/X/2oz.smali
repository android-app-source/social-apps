.class public LX/2oz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2oz;


# instance fields
.field private final a:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "LX/7LO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 467564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 467565
    new-instance v0, LX/0aq;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/2oz;->a:LX/0aq;

    .line 467566
    return-void
.end method

.method private static a()LX/2oz;
    .locals 1

    .prologue
    .line 467562
    new-instance v0, LX/2oz;

    invoke-direct {v0}, LX/2oz;-><init>()V

    .line 467563
    return-object v0
.end method

.method public static a(LX/0QB;)LX/2oz;
    .locals 3

    .prologue
    .line 467552
    sget-object v0, LX/2oz;->b:LX/2oz;

    if-nez v0, :cond_1

    .line 467553
    const-class v1, LX/2oz;

    monitor-enter v1

    .line 467554
    :try_start_0
    sget-object v0, LX/2oz;->b:LX/2oz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 467555
    if-eqz v2, :cond_0

    .line 467556
    :try_start_1
    invoke-static {}, LX/2oz;->a()LX/2oz;

    move-result-object v0

    sput-object v0, LX/2oz;->b:LX/2oz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 467557
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 467558
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 467559
    :cond_1
    sget-object v0, LX/2oz;->b:LX/2oz;

    return-object v0

    .line 467560
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 467561
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 467546
    if-nez p1, :cond_0

    move-object v0, v1

    .line 467547
    :goto_0
    return-object v0

    .line 467548
    :cond_0
    iget-object v0, p0, LX/2oz;->a:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7LO;

    .line 467549
    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, v0, LX/7LO;->a:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ILandroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 467550
    iget-object v0, p0, LX/2oz;->a:LX/0aq;

    new-instance v1, LX/7LO;

    invoke-direct {v1, p0, p3, p2}, LX/7LO;-><init>(LX/2oz;Landroid/graphics/Bitmap;I)V

    invoke-virtual {v0, p1, v1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467551
    return-void
.end method
