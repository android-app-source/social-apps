.class public final LX/2Ae;
.super Ljava/io/Writer;
.source ""


# instance fields
.field public final a:LX/15x;


# direct methods
.method public constructor <init>(LX/12B;)V
    .locals 1

    .prologue
    .line 377452
    invoke-direct {p0}, Ljava/io/Writer;-><init>()V

    .line 377453
    new-instance v0, LX/15x;

    invoke-direct {v0, p1}, LX/15x;-><init>(LX/12B;)V

    iput-object v0, p0, LX/2Ae;->a:LX/15x;

    .line 377454
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 377449
    iget-object v0, p0, LX/2Ae;->a:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    .line 377450
    iget-object v1, p0, LX/2Ae;->a:LX/15x;

    invoke-virtual {v1}, LX/15x;->a()V

    .line 377451
    return-object v0
.end method

.method public final append(C)Ljava/io/Writer;
    .locals 0

    .prologue
    .line 377447
    invoke-virtual {p0, p1}, LX/2Ae;->write(I)V

    .line 377448
    return-object p0
.end method

.method public final append(Ljava/lang/CharSequence;)Ljava/io/Writer;
    .locals 4

    .prologue
    .line 377444
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 377445
    iget-object v1, p0, LX/2Ae;->a:LX/15x;

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, LX/15x;->a(Ljava/lang/String;II)V

    .line 377446
    return-object p0
.end method

.method public final append(Ljava/lang/CharSequence;II)Ljava/io/Writer;
    .locals 4

    .prologue
    .line 377441
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 377442
    iget-object v1, p0, LX/2Ae;->a:LX/15x;

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, LX/15x;->a(Ljava/lang/String;II)V

    .line 377443
    return-object p0
.end method

.method public final bridge synthetic append(C)Ljava/lang/Appendable;
    .locals 1

    .prologue
    .line 377440
    invoke-virtual {p0, p1}, LX/2Ae;->append(C)Ljava/io/Writer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
    .locals 1

    .prologue
    .line 377439
    invoke-virtual {p0, p1}, LX/2Ae;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
    .locals 1

    .prologue
    .line 377455
    invoke-virtual {p0, p1, p2, p3}, LX/2Ae;->append(Ljava/lang/CharSequence;II)Ljava/io/Writer;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 0

    .prologue
    .line 377438
    return-void
.end method

.method public final flush()V
    .locals 0

    .prologue
    .line 377428
    return-void
.end method

.method public final write(I)V
    .locals 2

    .prologue
    .line 377436
    iget-object v0, p0, LX/2Ae;->a:LX/15x;

    int-to-char v1, p1

    invoke-virtual {v0, v1}, LX/15x;->a(C)V

    .line 377437
    return-void
.end method

.method public final write(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 377435
    iget-object v0, p0, LX/2Ae;->a:LX/15x;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, LX/15x;->a(Ljava/lang/String;II)V

    return-void
.end method

.method public final write(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 377433
    iget-object v0, p0, LX/2Ae;->a:LX/15x;

    invoke-virtual {v0, p1, p2, p3}, LX/15x;->a(Ljava/lang/String;II)V

    .line 377434
    return-void
.end method

.method public final write([C)V
    .locals 3

    .prologue
    .line 377431
    iget-object v0, p0, LX/2Ae;->a:LX/15x;

    const/4 v1, 0x0

    array-length v2, p1

    invoke-virtual {v0, p1, v1, v2}, LX/15x;->c([CII)V

    .line 377432
    return-void
.end method

.method public final write([CII)V
    .locals 1

    .prologue
    .line 377429
    iget-object v0, p0, LX/2Ae;->a:LX/15x;

    invoke-virtual {v0, p1, p2, p3}, LX/15x;->c([CII)V

    .line 377430
    return-void
.end method
