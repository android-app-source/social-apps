.class public final LX/2Ak;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public c:LX/0lJ;

.field public d:Z


# direct methods
.method public constructor <init>(LX/0lJ;Z)V
    .locals 1

    .prologue
    .line 377624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 377625
    iput-object p1, p0, LX/2Ak;->c:LX/0lJ;

    .line 377626
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Ak;->b:Ljava/lang/Class;

    .line 377627
    iput-boolean p2, p0, LX/2Ak;->d:Z

    .line 377628
    invoke-static {p1, p2}, LX/2Ak;->a(LX/0lJ;Z)I

    move-result v0

    iput v0, p0, LX/2Ak;->a:I

    .line 377629
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 377618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 377619
    iput-object p1, p0, LX/2Ak;->b:Ljava/lang/Class;

    .line 377620
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Ak;->c:LX/0lJ;

    .line 377621
    iput-boolean p2, p0, LX/2Ak;->d:Z

    .line 377622
    invoke-static {p1, p2}, LX/2Ak;->a(Ljava/lang/Class;Z)I

    move-result v0

    iput v0, p0, LX/2Ak;->a:I

    .line 377623
    return-void
.end method

.method private static final a(LX/0lJ;Z)I
    .locals 1

    .prologue
    .line 377614
    invoke-virtual {p0}, LX/0lJ;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 377615
    if-eqz p1, :cond_0

    .line 377616
    add-int/lit8 v0, v0, -0x1

    .line 377617
    :cond_0
    return v0
.end method

.method private static final a(Ljava/lang/Class;Z)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;Z)I"
        }
    .end annotation

    .prologue
    .line 377610
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 377611
    if-eqz p1, :cond_0

    .line 377612
    add-int/lit8 v0, v0, 0x1

    .line 377613
    :cond_0
    return v0
.end method


# virtual methods
.method public final a(LX/0lJ;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 377582
    iput-object p1, p0, LX/2Ak;->c:LX/0lJ;

    .line 377583
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Ak;->b:Ljava/lang/Class;

    .line 377584
    iput-boolean v1, p0, LX/2Ak;->d:Z

    .line 377585
    invoke-static {p1, v1}, LX/2Ak;->a(LX/0lJ;Z)I

    move-result v0

    iput v0, p0, LX/2Ak;->a:I

    .line 377586
    return-void
.end method

.method public final a(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 377630
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Ak;->c:LX/0lJ;

    .line 377631
    iput-object p1, p0, LX/2Ak;->b:Ljava/lang/Class;

    .line 377632
    iput-boolean v1, p0, LX/2Ak;->d:Z

    .line 377633
    invoke-static {p1, v1}, LX/2Ak;->a(Ljava/lang/Class;Z)I

    move-result v0

    iput v0, p0, LX/2Ak;->a:I

    .line 377634
    return-void
.end method

.method public final b(LX/0lJ;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 377605
    iput-object p1, p0, LX/2Ak;->c:LX/0lJ;

    .line 377606
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Ak;->b:Ljava/lang/Class;

    .line 377607
    iput-boolean v1, p0, LX/2Ak;->d:Z

    .line 377608
    invoke-static {p1, v1}, LX/2Ak;->a(LX/0lJ;Z)I

    move-result v0

    iput v0, p0, LX/2Ak;->a:I

    .line 377609
    return-void
.end method

.method public final b(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 377600
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Ak;->c:LX/0lJ;

    .line 377601
    iput-object p1, p0, LX/2Ak;->b:Ljava/lang/Class;

    .line 377602
    iput-boolean v1, p0, LX/2Ak;->d:Z

    .line 377603
    invoke-static {p1, v1}, LX/2Ak;->a(Ljava/lang/Class;Z)I

    move-result v0

    iput v0, p0, LX/2Ak;->a:I

    .line 377604
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 377591
    if-nez p1, :cond_1

    .line 377592
    :cond_0
    :goto_0
    return v0

    .line 377593
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    goto :goto_0

    .line 377594
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 377595
    check-cast p1, LX/2Ak;

    .line 377596
    iget-boolean v2, p1, LX/2Ak;->d:Z

    iget-boolean v3, p0, LX/2Ak;->d:Z

    if-ne v2, v3, :cond_0

    .line 377597
    iget-object v2, p0, LX/2Ak;->b:Ljava/lang/Class;

    if-eqz v2, :cond_3

    .line 377598
    iget-object v2, p1, LX/2Ak;->b:Ljava/lang/Class;

    iget-object v3, p0, LX/2Ak;->b:Ljava/lang/Class;

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 377599
    :cond_3
    iget-object v0, p0, LX/2Ak;->c:LX/0lJ;

    iget-object v1, p1, LX/2Ak;->c:LX/0lJ;

    invoke-virtual {v0, v1}, LX/0lJ;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 377590
    iget v0, p0, LX/2Ak;->a:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 377587
    iget-object v0, p0, LX/2Ak;->b:Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 377588
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{class: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2Ak;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", typed? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/2Ak;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 377589
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2Ak;->c:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", typed? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/2Ak;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
