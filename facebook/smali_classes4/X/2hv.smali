.class public LX/2hv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final a:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Landroid/widget/AbsListView$OnScrollListener;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/widget/AbsListView$OnScrollListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 450919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450920
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/2hv;->a:LX/01J;

    .line 450921
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2hv;->c:Z

    .line 450922
    return-void
.end method

.method public static a(LX/0QB;)LX/2hv;
    .locals 1

    .prologue
    .line 450898
    new-instance v0, LX/2hv;

    invoke-direct {v0}, LX/2hv;-><init>()V

    .line 450899
    move-object v0, v0

    .line 450900
    return-object v0
.end method


# virtual methods
.method public final b(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 2

    .prologue
    .line 450917
    iget-object v0, p0, LX/2hv;->a:LX/01J;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1, v1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450918
    return-void
.end method

.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 3

    .prologue
    .line 450908
    iget-boolean v0, p0, LX/2hv;->c:Z

    if-nez v0, :cond_0

    .line 450909
    :goto_0
    return-void

    .line 450910
    :cond_0
    const-string v0, "BetterViewOnScrollListener.onScroll"

    const v1, 0x3e96cac3

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 450911
    :try_start_0
    iget-object v0, p0, LX/2hv;->b:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_1

    .line 450912
    iget-object v0, p0, LX/2hv;->b:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 450913
    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, LX/2hv;->a:LX/01J;

    invoke-virtual {v1}, LX/01J;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 450914
    iget-object v0, p0, LX/2hv;->a:LX/01J;

    invoke-virtual {v0, v1}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 450915
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 450916
    :cond_2
    const v0, 0xc2e7c40

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x73f555d0

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3

    .prologue
    .line 450901
    iget-boolean v0, p0, LX/2hv;->c:Z

    if-nez v0, :cond_1

    .line 450902
    :cond_0
    return-void

    .line 450903
    :cond_1
    iget-object v0, p0, LX/2hv;->b:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_2

    .line 450904
    iget-object v0, p0, LX/2hv;->b:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 450905
    :cond_2
    const/4 v0, 0x0

    iget-object v1, p0, LX/2hv;->a:LX/01J;

    invoke-virtual {v1}, LX/01J;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 450906
    iget-object v0, p0, LX/2hv;->a:LX/01J;

    invoke-virtual {v0, v1}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 450907
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
