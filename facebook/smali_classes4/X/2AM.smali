.class public final LX/2AM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/2CH;


# direct methods
.method public constructor <init>(LX/2CH;)V
    .locals 0

    .prologue
    .line 377280
    iput-object p1, p0, LX/2AM;->a:LX/2CH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x588342eb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 377281
    const-string v0, "com.facebook.orca.contacts.CONTACTS_UPLOAD_STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377282
    const-string v0, "state"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/upload/ContactsUploadState;

    .line 377283
    iget-object v2, v0, Lcom/facebook/contacts/upload/ContactsUploadState;->a:LX/94z;

    move-object v0, v2

    .line 377284
    sget-object v2, LX/94z;->RUNNING:LX/94z;

    if-ne v0, v2, :cond_0

    .line 377285
    iget-object v0, p0, LX/2AM;->a:LX/2CH;

    invoke-static {v0}, LX/2CH;->q(LX/2CH;)V

    .line 377286
    :cond_0
    const/16 v0, 0x27

    const v2, -0x3c839c85

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
