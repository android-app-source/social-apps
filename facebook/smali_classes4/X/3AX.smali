.class public LX/3AX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3AY;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile l:LX/3AX;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hp;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xX;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/ETB;

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3AW;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2xj;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0ho;

.field public k:LX/EUc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 525526
    const-class v0, LX/3AX;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3AX;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/ETB;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/video/videohome/prefetching/IsVideoHomeForcePrefetchEnabled;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/ETB;",
            ")V"
        }
    .end annotation

    .prologue
    .line 525507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 525508
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 525509
    iput-object v0, p0, LX/3AX;->c:LX/0Ot;

    .line 525510
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 525511
    iput-object v0, p0, LX/3AX;->d:LX/0Ot;

    .line 525512
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 525513
    iput-object v0, p0, LX/3AX;->e:LX/0Ot;

    .line 525514
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 525515
    iput-object v0, p0, LX/3AX;->f:LX/0Ot;

    .line 525516
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 525517
    iput-object v0, p0, LX/3AX;->h:LX/0Ot;

    .line 525518
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 525519
    iput-object v0, p0, LX/3AX;->i:LX/0Ot;

    .line 525520
    new-instance v0, LX/EVU;

    invoke-direct {v0, p0}, LX/EVU;-><init>(LX/3AX;)V

    iput-object v0, p0, LX/3AX;->j:LX/0ho;

    .line 525521
    iput-object p1, p0, LX/3AX;->b:LX/0Or;

    .line 525522
    iput-object p2, p0, LX/3AX;->g:LX/ETB;

    .line 525523
    iget-object v0, p0, LX/3AX;->g:LX/ETB;

    .line 525524
    iget-object p1, v0, LX/ETB;->w:LX/ETH;

    invoke-virtual {p1, p0}, LX/ETH;->a(LX/3AY;)V

    .line 525525
    return-void
.end method

.method public static a(LX/0QB;)LX/3AX;
    .locals 9

    .prologue
    .line 525492
    sget-object v0, LX/3AX;->l:LX/3AX;

    if-nez v0, :cond_1

    .line 525493
    const-class v1, LX/3AX;

    monitor-enter v1

    .line 525494
    :try_start_0
    sget-object v0, LX/3AX;->l:LX/3AX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 525495
    if-eqz v2, :cond_0

    .line 525496
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 525497
    new-instance v3, LX/3AX;

    const/16 v4, 0x159b

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/ETB;->a(LX/0QB;)LX/ETB;

    move-result-object v4

    check-cast v4, LX/ETB;

    invoke-direct {v3, v5, v4}, LX/3AX;-><init>(LX/0Or;LX/ETB;)V

    .line 525498
    const/16 v4, 0x245

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2db

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1365

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1364

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1369

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 p0, 0x1387

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 525499
    iput-object v4, v3, LX/3AX;->c:LX/0Ot;

    iput-object v5, v3, LX/3AX;->d:LX/0Ot;

    iput-object v6, v3, LX/3AX;->e:LX/0Ot;

    iput-object v7, v3, LX/3AX;->f:LX/0Ot;

    iput-object v8, v3, LX/3AX;->h:LX/0Ot;

    iput-object p0, v3, LX/3AX;->i:LX/0Ot;

    .line 525500
    move-object v0, v3

    .line 525501
    sput-object v0, LX/3AX;->l:LX/3AX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 525502
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 525503
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 525504
    :cond_1
    sget-object v0, LX/3AX;->l:LX/3AX;

    return-object v0

    .line 525505
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 525506
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3AX;LX/0JU;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 525456
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 525457
    iget-object v0, p0, LX/3AX;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 525458
    if-eqz v0, :cond_4

    .line 525459
    invoke-static {p0}, LX/3AX;->f(LX/3AX;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, LX/3AX;->g(LX/3AX;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v3

    .line 525460
    :goto_0
    move v0, v0

    .line 525461
    if-nez v0, :cond_1

    .line 525462
    :cond_0
    :goto_1
    return v1

    .line 525463
    :cond_1
    iget-object v0, p0, LX/3AX;->k:LX/EUc;

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 525464
    invoke-virtual {p0}, LX/3AX;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 525465
    iget-object v0, p0, LX/3AX;->g:LX/ETB;

    iget-object v3, p0, LX/3AX;->j:LX/0ho;

    sget-object v4, LX/2rJ;->PREFETCH:LX/2rJ;

    iget-object v5, p0, LX/3AX;->k:LX/EUc;

    .line 525466
    iget v6, v5, LX/EUc;->c:I

    move v5, v6

    .line 525467
    invoke-virtual {v0, v3, v4, v5}, LX/ETB;->a(LX/0ho;LX/2rJ;I)Z

    move-result v0

    .line 525468
    if-eqz v0, :cond_0

    .line 525469
    iget-object v0, p0, LX/3AX;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AW;

    iget-object v1, p0, LX/3AX;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    .line 525470
    iput-wide v4, v0, LX/3AW;->k:J

    .line 525471
    iget-object v0, p0, LX/3AX;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AW;

    sget-object v1, LX/04D;->VIDEO_HOME:LX/04D;

    iget-object v3, p0, LX/3AX;->k:LX/EUc;

    .line 525472
    iget-wide v6, v3, LX/EUc;->e:J

    move-wide v4, v6

    .line 525473
    invoke-virtual {v0, p1, v1, v4, v5}, LX/3AW;->a(LX/0JU;LX/04D;J)V

    move v1, v2

    .line 525474
    goto :goto_1

    :cond_2
    move v0, v1

    .line 525475
    goto :goto_2

    :cond_3
    move v0, v4

    .line 525476
    goto :goto_0

    .line 525477
    :cond_4
    iget-object v0, p0, LX/3AX;->k:LX/EUc;

    if-eqz v0, :cond_5

    invoke-static {p0}, LX/3AX;->f(LX/3AX;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {p0}, LX/3AX;->g(LX/3AX;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    move v0, v4

    .line 525478
    goto :goto_0

    .line 525479
    :cond_6
    iget-object v0, p0, LX/3AX;->g:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->d()Z

    move-result v0

    .line 525480
    iget-object v5, p0, LX/3AX;->g:LX/ETB;

    invoke-virtual {v5}, LX/ETB;->e()Z

    move-result v5

    .line 525481
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 525482
    if-eqz v0, :cond_7

    if-eqz v5, :cond_8

    :cond_7
    move v0, v3

    goto :goto_0

    :cond_8
    move v0, v4

    goto :goto_0
.end method

.method public static f(LX/3AX;)Z
    .locals 1

    .prologue
    .line 525491
    iget-object v0, p0, LX/3AX;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2xj;

    invoke-virtual {v0}, LX/2xj;->i()Z

    move-result v0

    return v0
.end method

.method public static g(LX/3AX;)Z
    .locals 1

    .prologue
    .line 525490
    iget-object v0, p0, LX/3AX;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 525488
    sget-object v0, LX/0JU;->DATA_STALE_IN_APP:LX/0JU;

    invoke-static {p0, v0}, LX/3AX;->a(LX/3AX;LX/0JU;)Z

    .line 525489
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 525486
    sget-object v0, LX/0JU;->DATA_STALE_FOREGROUNDING:LX/0JU;

    invoke-static {p0, v0}, LX/3AX;->a(LX/3AX;LX/0JU;)Z

    .line 525487
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 525483
    iget-object v0, p0, LX/3AX;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    sget-object v1, LX/1vy;->PREFETCHING:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3AX;->k:LX/EUc;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3AX;->k:LX/EUc;

    .line 525484
    iget v1, v0, LX/EUc;->c:I

    move v0, v1

    .line 525485
    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
