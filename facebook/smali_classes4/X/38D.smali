.class public LX/38D;
.super Landroid/media/MediaRouter$Callback;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/37w;",
        ">",
        "Landroid/media/MediaRouter$Callback;"
    }
.end annotation


# instance fields
.field public final a:LX/37w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/37w;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 503030
    invoke-direct {p0}, Landroid/media/MediaRouter$Callback;-><init>()V

    .line 503031
    iput-object p1, p0, LX/38D;->a:LX/37w;

    .line 503032
    return-void
.end method


# virtual methods
.method public final onRouteAdded(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 503044
    iget-object v0, p0, LX/38D;->a:LX/37w;

    invoke-interface {v0, p2}, LX/37w;->b(Ljava/lang/Object;)V

    .line 503045
    return-void
.end method

.method public final onRouteChanged(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 503042
    iget-object v0, p0, LX/38D;->a:LX/37w;

    invoke-interface {v0, p2}, LX/37w;->d(Ljava/lang/Object;)V

    .line 503043
    return-void
.end method

.method public final onRouteGrouped(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;I)V
    .locals 0

    .prologue
    .line 503041
    return-void
.end method

.method public final onRouteRemoved(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 503039
    iget-object v0, p0, LX/38D;->a:LX/37w;

    invoke-interface {v0, p2}, LX/37w;->c(Ljava/lang/Object;)V

    .line 503040
    return-void
.end method

.method public final onRouteSelected(Landroid/media/MediaRouter;ILandroid/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 503037
    iget-object v0, p0, LX/38D;->a:LX/37w;

    invoke-interface {v0, p3}, LX/37w;->a(Ljava/lang/Object;)V

    .line 503038
    return-void
.end method

.method public final onRouteUngrouped(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;)V
    .locals 0

    .prologue
    .line 503036
    return-void
.end method

.method public final onRouteUnselected(Landroid/media/MediaRouter;ILandroid/media/MediaRouter$RouteInfo;)V
    .locals 0

    .prologue
    .line 503035
    return-void
.end method

.method public final onRouteVolumeChanged(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 503033
    iget-object v0, p0, LX/38D;->a:LX/37w;

    invoke-interface {v0, p2}, LX/37w;->e(Ljava/lang/Object;)V

    .line 503034
    return-void
.end method
