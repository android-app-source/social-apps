.class public LX/2Ky;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2Ky;


# instance fields
.field public final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 394213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394214
    iput-object p1, p0, LX/2Ky;->a:LX/0Uh;

    .line 394215
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ky;
    .locals 4

    .prologue
    .line 394216
    sget-object v0, LX/2Ky;->b:LX/2Ky;

    if-nez v0, :cond_1

    .line 394217
    const-class v1, LX/2Ky;

    monitor-enter v1

    .line 394218
    :try_start_0
    sget-object v0, LX/2Ky;->b:LX/2Ky;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 394219
    if-eqz v2, :cond_0

    .line 394220
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 394221
    new-instance p0, LX/2Ky;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/2Ky;-><init>(LX/0Uh;)V

    .line 394222
    move-object v0, p0

    .line 394223
    sput-object v0, LX/2Ky;->b:LX/2Ky;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394224
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 394225
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 394226
    :cond_1
    sget-object v0, LX/2Ky;->b:LX/2Ky;

    return-object v0

    .line 394227
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 394228
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
