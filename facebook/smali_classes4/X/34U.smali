.class public final LX/34U;
.super LX/1Ah;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Ah",
        "<",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2nQ;

.field private final b:LX/34S;

.field private final c:Z

.field private final d:I


# direct methods
.method public constructor <init>(LX/2nQ;LX/34S;ZI)V
    .locals 0

    .prologue
    .line 495173
    iput-object p1, p0, LX/34U;->a:LX/2nQ;

    invoke-direct {p0}, LX/1Ah;-><init>()V

    .line 495174
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 495175
    iput-object p2, p0, LX/34U;->b:LX/34S;

    .line 495176
    iput-boolean p3, p0, LX/34U;->c:Z

    .line 495177
    iput p4, p0, LX/34U;->d:I

    .line 495178
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 5

    .prologue
    .line 495179
    check-cast p2, LX/1ln;

    const/4 v4, 0x0

    .line 495180
    iget-boolean v0, p0, LX/34U;->c:Z

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, LX/34U;->b:LX/34S;

    .line 495181
    iget-object v1, v0, LX/34S;->a:LX/1aX;

    move-object v0, v1

    .line 495182
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 495183
    iget-object v0, p0, LX/34U;->b:LX/34S;

    .line 495184
    iget-object v1, v0, LX/34S;->a:LX/1aX;

    move-object v0, v1

    .line 495185
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 495186
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 495187
    iget v2, p0, LX/34U;->d:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 495188
    iget v2, p0, LX/34U;->d:I

    int-to-float v2, v2

    invoke-virtual {p2}, LX/1ln;->h()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {p2}, LX/1ln;->g()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    .line 495189
    float-to-int v2, v2

    .line 495190
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-ne v3, v2, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget v3, p0, LX/34U;->d:I

    if-eq v1, v3, :cond_1

    .line 495191
    :cond_0
    iget v1, p0, LX/34U;->d:I

    invoke-virtual {v0, v4, v4, v2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 495192
    :cond_1
    :goto_0
    return-void

    .line 495193
    :cond_2
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p2}, LX/1ln;->g()I

    move-result v3

    if-ne v2, v3, :cond_3

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p2}, LX/1ln;->h()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 495194
    :cond_3
    invoke-virtual {p2}, LX/1ln;->g()I

    move-result v1

    invoke-virtual {p2}, LX/1ln;->h()I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0
.end method
