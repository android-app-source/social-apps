.class public LX/3H3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0w9;

.field private final b:LX/0tX;


# direct methods
.method public constructor <init>(LX/0w9;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 542645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 542646
    iput-object p1, p0, LX/3H3;->a:LX/0w9;

    .line 542647
    iput-object p2, p0, LX/3H3;->b:LX/0tX;

    .line 542648
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;IIFLjava/lang/String;ILX/04D;)LX/1Zp;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIF",
            "Ljava/lang/String;",
            "I",
            "LX/04D;",
            ")",
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542649
    new-instance v0, LX/D7K;

    invoke-direct {v0}, LX/D7K;-><init>()V

    move-object v0, v0

    .line 542650
    const-string v1, "host_video_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "duration"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "min_aspect_ratio"

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "instream_video_ad_type"

    invoke-virtual {v1, v2, p5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "instream_video_ad_break_index"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "player_origin"

    iget-object v3, p7, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 542651
    if-lez p3, :cond_0

    if-ge p3, p2, :cond_0

    .line 542652
    const-string v1, "min_duration"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 542653
    :cond_0
    invoke-static {v0}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 542654
    iget-object v1, p0, LX/3H3;->a:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 542655
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 542656
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 542657
    move-object v0, v0

    .line 542658
    iget-object v1, p0, LX/3H3;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
