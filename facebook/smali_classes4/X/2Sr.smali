.class public LX/2Sr;
.super LX/2SP;
.source ""

# interfaces
.implements LX/2SZ;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile o:LX/2Sr;


# instance fields
.field private final a:LX/2Sa;

.field private final b:LX/2Se;

.field private final c:LX/2Sj;

.field public final d:LX/2Sc;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EQ0;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/concurrent/ExecutorService;

.field private final h:LX/0SG;

.field public i:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Cw5;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private j:LX/Cw5;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private k:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private l:Z

.field private m:LX/2SR;

.field private final n:LX/0ad;


# direct methods
.method public constructor <init>(LX/2Sa;LX/2Se;LX/2Sg;LX/2Sj;LX/2Sc;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0SG;LX/0ad;)V
    .locals 1
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Sa;",
            "LX/2Se;",
            "LX/2Sg;",
            "LX/2Sj;",
            "LX/2Sc;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EQ0;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0SG;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 413438
    invoke-direct {p0, p3}, LX/2SP;-><init>(LX/2Sg;)V

    .line 413439
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Sr;->l:Z

    .line 413440
    iput-object p1, p0, LX/2Sr;->a:LX/2Sa;

    .line 413441
    iput-object p2, p0, LX/2Sr;->b:LX/2Se;

    .line 413442
    iput-object p4, p0, LX/2Sr;->c:LX/2Sj;

    .line 413443
    iput-object p5, p0, LX/2Sr;->d:LX/2Sc;

    .line 413444
    iput-object p6, p0, LX/2Sr;->e:LX/0Ot;

    .line 413445
    iput-object p7, p0, LX/2Sr;->f:LX/0Ot;

    .line 413446
    iput-object p8, p0, LX/2Sr;->g:Ljava/util/concurrent/ExecutorService;

    .line 413447
    iput-object p9, p0, LX/2Sr;->h:LX/0SG;

    .line 413448
    iput-object p10, p0, LX/2Sr;->n:LX/0ad;

    .line 413449
    return-void
.end method

.method public static a(LX/0QB;)LX/2Sr;
    .locals 14

    .prologue
    .line 413450
    sget-object v0, LX/2Sr;->o:LX/2Sr;

    if-nez v0, :cond_1

    .line 413451
    const-class v1, LX/2Sr;

    monitor-enter v1

    .line 413452
    :try_start_0
    sget-object v0, LX/2Sr;->o:LX/2Sr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413453
    if-eqz v2, :cond_0

    .line 413454
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 413455
    new-instance v3, LX/2Sr;

    invoke-static {v0}, LX/2Sa;->a(LX/0QB;)LX/2Sa;

    move-result-object v4

    check-cast v4, LX/2Sa;

    invoke-static {v0}, LX/2Se;->a(LX/0QB;)LX/2Se;

    move-result-object v5

    check-cast v5, LX/2Se;

    invoke-static {v0}, LX/2Sg;->a(LX/0QB;)LX/2Sg;

    move-result-object v6

    check-cast v6, LX/2Sg;

    invoke-static {v0}, LX/2Sj;->a(LX/0QB;)LX/2Sj;

    move-result-object v7

    check-cast v7, LX/2Sj;

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v8

    check-cast v8, LX/2Sc;

    const/16 v9, 0x1646

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x34d3

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-direct/range {v3 .. v13}, LX/2Sr;-><init>(LX/2Sa;LX/2Se;LX/2Sg;LX/2Sj;LX/2Sc;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0SG;LX/0ad;)V

    .line 413456
    move-object v0, v3

    .line 413457
    sput-object v0, LX/2Sr;->o:LX/2Sr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413458
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413459
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413460
    :cond_1
    sget-object v0, LX/2Sr;->o:LX/2Sr;

    return-object v0

    .line 413461
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413462
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(Lcom/facebook/common/callercontext/CallerContext;LX/0zS;J)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0zS;",
            "J)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Cw5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413463
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Sr;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 413464
    iget-object v0, p0, LX/2Sr;->i:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413465
    :goto_0
    monitor-exit p0

    return-object v0

    .line 413466
    :cond_0
    :try_start_1
    invoke-virtual {p0}, LX/2SP;->kF_()V

    .line 413467
    iget-object v0, p0, LX/2Sr;->a:LX/2Sa;

    const-string v1, "recent_video_search_cache_tag"

    const-string v2, "video_search"

    const/16 v3, 0xf

    move-object v4, p1

    move-object v5, p2

    move-wide v6, p3

    invoke-virtual/range {v0 .. v7}, LX/2Sa;->a(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;LX/0zS;J)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/2Sr;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 413468
    iget-object v0, p0, LX/2Sr;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/search/suggestions/nullstate/recent/RecentVideoSearchesNullStateSupplier$1;

    invoke-direct {v1, p0}, Lcom/facebook/search/suggestions/nullstate/recent/RecentVideoSearchesNullStateSupplier$1;-><init>(LX/2Sr;)V

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/2Sr;->k:Ljava/util/concurrent/ScheduledFuture;

    .line 413469
    new-instance v0, LX/EQB;

    invoke-direct {v0, p0}, LX/EQB;-><init>(LX/2Sr;)V

    .line 413470
    iget-object v1, p0, LX/2Sr;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v2, p0, LX/2Sr;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 413471
    iget-object v0, p0, LX/2Sr;->i:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 413472
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/2Sr;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 413473
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/Cw5;

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/2Sr;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, LX/Cw5;-><init>(LX/0Px;J)V

    iput-object v0, p0, LX/2Sr;->j:LX/Cw5;

    .line 413474
    iget-object v0, p0, LX/2Sr;->m:LX/2SR;

    if-eqz v0, :cond_0

    .line 413475
    iget-object v0, p0, LX/2Sr;->m:LX/2SR;

    sget-object v1, LX/7BE;->READY:LX/7BE;

    invoke-interface {v0, v1}, LX/2SR;->a(LX/7BE;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413476
    :cond_0
    monitor-exit p0

    return-void

    .line 413477
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V
    .locals 4

    .prologue
    .line 413478
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Sr;->b:LX/2Se;

    const-string v1, "recent_video_search_cache_tag"

    const/4 v2, 0x0

    iget-object v3, p0, LX/2Sr;->j:LX/Cw5;

    invoke-virtual {v0, v1, p1, v2, v3}, LX/2Se;->a(Ljava/lang/String;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;Ljava/lang/String;LX/Cw5;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, LX/2Sr;->b(LX/2Sr;LX/0Px;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413479
    monitor-exit p0

    return-void

    .line 413480
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static l(LX/2Sr;)V
    .locals 2

    .prologue
    .line 413430
    iget-object v0, p0, LX/2Sr;->k:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 413431
    iget-object v0, p0, LX/2Sr;->k:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 413432
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Sr;->k:Ljava/util/concurrent/ScheduledFuture;

    .line 413433
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/7BE;
    .locals 8

    .prologue
    .line 413481
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Sr;->j:LX/Cw5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Sr;->c:LX/2Sj;

    const-wide/16 v2, 0xe10

    iget-object v1, p0, LX/2Sr;->j:LX/Cw5;

    .line 413482
    iget-wide v6, v1, LX/Cw5;->b:J

    move-wide v4, v6

    .line 413483
    invoke-virtual {v0, v2, v3, v4, v5}, LX/2Sj;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413484
    sget-object v0, LX/7BE;->READY:LX/7BE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413485
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, LX/7BE;->NOT_READY:LX/7BE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 413486
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/2SR;LX/2Sp;)V
    .locals 0

    .prologue
    .line 413487
    iput-object p1, p0, LX/2Sr;->m:LX/2SR;

    .line 413488
    return-void
.end method

.method public final a(LX/Cwb;)V
    .locals 1

    .prologue
    .line 413489
    sget-object v0, LX/Cwb;->RECENT_VIDEOS:LX/Cwb;

    if-ne p1, v0, :cond_0

    .line 413490
    invoke-virtual {p0}, LX/2Sr;->i()V

    .line 413491
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 4
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 413492
    invoke-static {p2}, LX/2SP;->a(LX/EPu;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/0zS;->d:LX/0zS;

    :goto_0
    const-wide/32 v2, 0x3f480

    invoke-direct {p0, p1, v0, v2, v3}, LX/2Sr;->a(Lcom/facebook/common/callercontext/CallerContext;LX/0zS;J)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 413493
    return-void

    .line 413494
    :cond_0
    sget-object v0, LX/0zS;->a:LX/0zS;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 0

    .prologue
    .line 413495
    return-void
.end method

.method public final a(Lcom/facebook/search/model/EntityTypeaheadUnit;)V
    .locals 1

    .prologue
    .line 413434
    invoke-static {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a(Lcom/facebook/search/model/EntityTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2Sr;->b(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V

    .line 413435
    return-void
.end method

.method public final a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)V
    .locals 1

    .prologue
    .line 413436
    invoke-static {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2Sr;->b(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V

    .line 413437
    return-void
.end method

.method public final a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V
    .locals 1

    .prologue
    .line 413408
    invoke-static {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2Sr;->b(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V

    .line 413409
    return-void
.end method

.method public final a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)V
    .locals 1

    .prologue
    .line 413396
    iget-object v0, p1, Lcom/facebook/search/model/SeeMoreResultPageUnit;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-object v0, v0

    .line 413397
    invoke-static {v0}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a(Lcom/facebook/search/model/EntityTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    .line 413398
    invoke-direct {p0, v0}, LX/2Sr;->b(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V

    .line 413399
    return-void
.end method

.method public final a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)V
    .locals 1

    .prologue
    .line 413400
    invoke-static {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2Sr;->b(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V

    .line 413401
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 413402
    const/4 v0, 0x1

    return v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 413403
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/2Sr;->j:LX/Cw5;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413404
    monitor-exit p0

    return-void

    .line 413405
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 413406
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Sr;->l:Z

    .line 413407
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 413410
    const-string v0, "recent_video_searches_network"

    return-object v0
.end method

.method public final f()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413411
    invoke-virtual {p0}, LX/2SP;->c()V

    .line 413412
    iget-object v0, p0, LX/2Sr;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EQ0;

    const-string v1, "recent_video_search_cache_tag"

    invoke-virtual {v0, v1}, LX/EQ0;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 413413
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Sr;->j:LX/Cw5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Sr;->j:LX/Cw5;

    invoke-virtual {v0}, LX/Cw5;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 413414
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413415
    :goto_0
    monitor-exit p0

    return-object v0

    .line 413416
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2Sr;->j:LX/Cw5;

    .line 413417
    iget-object v1, v0, LX/Cw5;->a:LX/0Px;

    move-object v0, v1

    .line 413418
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 413419
    iget-boolean v1, p0, LX/2Sr;->l:Z

    if-eqz v1, :cond_2

    .line 413420
    :goto_1
    iget-object v1, p0, LX/2Sr;->b:LX/2Se;

    sget-object v2, LX/Cwb;->RECENT_VIDEOS:LX/Cwb;

    iget-object v3, p0, LX/2Sr;->j:LX/Cw5;

    iget-boolean v4, p0, LX/2Sr;->l:Z

    invoke-virtual {v1, v2, v3, v4, v0}, LX/2Se;->a(LX/Cwb;LX/Cw5;ZI)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 413421
    :cond_2
    const/4 v1, 0x3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_1

    .line 413422
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 413423
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2Sr;->l:Z

    .line 413424
    return-void
.end method

.method public final declared-synchronized j()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 413425
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/2Sr;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Sr;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 413426
    iget-object v1, p0, LX/2Sr;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 413427
    const/4 v1, 0x0

    iput-object v1, p0, LX/2Sr;->i:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413428
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 413429
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
