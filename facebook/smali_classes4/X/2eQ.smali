.class public final LX/2eQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2eR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2eR",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1RC;

.field public final synthetic b:LX/2eK;


# direct methods
.method public constructor <init>(LX/2eK;LX/1RC;)V
    .locals 0

    .prologue
    .line 445269
    iput-object p1, p0, LX/2eQ;->b:LX/2eK;

    iput-object p2, p0, LX/2eQ;->a:LX/1RC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 445270
    iget-object v0, p0, LX/2eQ;->a:LX/1RC;

    invoke-interface {v0}, LX/1RC;->a()LX/1Cz;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1Cz;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/Class;
    .locals 3

    .prologue
    .line 445271
    iget-object v0, p0, LX/2eQ;->a:LX/1RC;

    invoke-interface {v0}, LX/1RC;->a()LX/1Cz;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getEnclosingClass()Ljava/lang/Class;

    move-result-object v0

    .line 445272
    if-eqz v0, :cond_0

    const-class v1, LX/1Cz;

    if-ne v0, v1, :cond_0

    .line 445273
    iget-object v0, p0, LX/2eQ;->b:LX/2eK;

    iget-object v0, v0, LX/2eK;->d:LX/03V;

    const-string v1, "Pager Binder Delegate"

    const-string v2, "Using ViewType.FromLayout() with an hscroll is known to cause issues.  If you need to inflate a layout, implement ViewType.LayoutBasedViewType directly in your part definition"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 445274
    iget-object v0, p0, LX/2eQ;->a:LX/1RC;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 445275
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2eQ;->a:LX/1RC;

    invoke-interface {v0}, LX/1RC;->a()LX/1Cz;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method
