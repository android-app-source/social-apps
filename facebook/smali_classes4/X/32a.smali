.class public final LX/32a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/VisibleForTesting;
.end annotation


# instance fields
.field public final a:Ljava/io/File;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final synthetic b:LX/1Hv;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1Hv;Ljava/lang/String;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 490349
    iput-object p1, p0, LX/32a;->b:LX/1Hv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490350
    iput-object p2, p0, LX/32a;->c:Ljava/lang/String;

    .line 490351
    iput-object p3, p0, LX/32a;->a:Ljava/io/File;

    .line 490352
    return-void
.end method


# virtual methods
.method public final a()LX/1gI;
    .locals 5

    .prologue
    .line 490353
    iget-object v0, p0, LX/32a;->b:LX/1Hv;

    iget-object v1, p0, LX/32a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Hv;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 490354
    :try_start_0
    iget-object v1, p0, LX/32a;->a:Ljava/io/File;

    invoke-static {v1, v0}, LX/04M;->a(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch LX/0Fx; {:try_start_0 .. :try_end_0} :catch_0

    .line 490355
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 490356
    iget-object v1, p0, LX/32a;->b:LX/1Hv;

    iget-object v1, v1, LX/1Hv;->g:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setLastModified(J)Z

    .line 490357
    :cond_0
    invoke-static {v0}, LX/1gH;->a(Ljava/io/File;)LX/1gH;

    move-result-object v0

    return-object v0

    .line 490358
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 490359
    invoke-virtual {v1}, LX/0Fx;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 490360
    if-eqz v0, :cond_2

    .line 490361
    instance-of v2, v0, LX/0Fw;

    if-eqz v2, :cond_1

    .line 490362
    sget-object v0, LX/43W;->WRITE_RENAME_FILE_TEMPFILE_PARENT_NOT_FOUND:LX/43W;

    .line 490363
    :goto_0
    iget-object v2, p0, LX/32a;->b:LX/1Hv;

    iget-object v2, v2, LX/1Hv;->f:LX/1GQ;

    sget-object v3, LX/1Hv;->b:Ljava/lang/Class;

    const-string v4, "commit"

    invoke-interface {v2, v0, v3, v4, v1}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 490364
    throw v1

    .line 490365
    :cond_1
    instance-of v0, v0, Ljava/io/FileNotFoundException;

    if-eqz v0, :cond_2

    .line 490366
    sget-object v0, LX/43W;->WRITE_RENAME_FILE_TEMPFILE_NOT_FOUND:LX/43W;

    goto :goto_0

    .line 490367
    :cond_2
    sget-object v0, LX/43W;->WRITE_RENAME_FILE_OTHER:LX/43W;

    goto :goto_0
.end method

.method public final a(LX/2uu;)V
    .locals 8

    .prologue
    .line 490368
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v0, p0, LX/32a;->a:Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 490369
    :try_start_1
    new-instance v0, LX/32b;

    invoke-direct {v0, v1}, LX/32b;-><init>(Ljava/io/OutputStream;)V

    .line 490370
    invoke-interface {p1, v0}, LX/2uu;->a(Ljava/io/OutputStream;)V

    .line 490371
    invoke-virtual {v0}, LX/32b;->flush()V

    .line 490372
    iget-wide v6, v0, LX/32b;->a:J

    move-wide v2, v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 490373
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 490374
    iget-object v0, p0, LX/32a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 490375
    new-instance v0, LX/43b;

    iget-object v1, p0, LX/32a;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, LX/43b;-><init>(JJ)V

    throw v0

    .line 490376
    :catch_0
    move-exception v0

    .line 490377
    iget-object v1, p0, LX/32a;->b:LX/1Hv;

    iget-object v1, v1, LX/1Hv;->f:LX/1GQ;

    sget-object v2, LX/43W;->WRITE_UPDATE_FILE_NOT_FOUND:LX/43W;

    sget-object v3, LX/1Hv;->b:Ljava/lang/Class;

    const-string v4, "updateResource"

    invoke-interface {v1, v2, v3, v4, v0}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 490378
    throw v0

    .line 490379
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0

    .line 490380
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 490381
    iget-object v0, p0, LX/32a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/32a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
