.class public LX/3Hu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/3Hu;


# instance fields
.field public final a:LX/0ad;

.field public final b:Z

.field public final c:I

.field public final d:Z


# direct methods
.method public constructor <init>(LX/0ad;LX/0Uh;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 545200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 545201
    iput-object p1, p0, LX/3Hu;->a:LX/0ad;

    .line 545202
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-object v1, LX/0c1;->Off:LX/0c1;

    sget-short v2, LX/2mm;->m:S

    invoke-interface {p1, v0, v1, v2, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3Hu;->b:Z

    .line 545203
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-object v1, LX/0c1;->Off:LX/0c1;

    sget v2, LX/2mm;->n:I

    const/16 v3, 0x2710

    invoke-interface {p1, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    iput v0, p0, LX/3Hu;->c:I

    .line 545204
    const/16 v0, 0x410

    invoke-virtual {p2, v0, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3Hu;->d:Z

    .line 545205
    return-void
.end method

.method public static a(LX/0QB;)LX/3Hu;
    .locals 5

    .prologue
    .line 545206
    sget-object v0, LX/3Hu;->e:LX/3Hu;

    if-nez v0, :cond_1

    .line 545207
    const-class v1, LX/3Hu;

    monitor-enter v1

    .line 545208
    :try_start_0
    sget-object v0, LX/3Hu;->e:LX/3Hu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 545209
    if-eqz v2, :cond_0

    .line 545210
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 545211
    new-instance p0, LX/3Hu;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/3Hu;-><init>(LX/0ad;LX/0Uh;)V

    .line 545212
    move-object v0, p0

    .line 545213
    sput-object v0, LX/3Hu;->e:LX/3Hu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 545214
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 545215
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 545216
    :cond_1
    sget-object v0, LX/3Hu;->e:LX/3Hu;

    return-object v0

    .line 545217
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 545218
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
