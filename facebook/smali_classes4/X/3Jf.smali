.class public LX/3Jf;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/3Jf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/3JT;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3Jc;",
            ">;"
        }
    .end annotation
.end field

.field public final d:[[[F

.field public final e:[F
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final f:LX/3Jj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 547799
    new-instance v0, LX/3Jg;

    invoke-direct {v0}, LX/3Jg;-><init>()V

    sput-object v0, LX/3Jf;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/3JT;Ljava/util/List;[[[F[F)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3JT;",
            "Ljava/util/List",
            "<",
            "LX/3Jc;",
            ">;[[[F[F)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 547800
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547801
    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "property"

    invoke-static {p1, v0, v3}, LX/3Je;->a(Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3JT;

    iput-object v0, p0, LX/3Jf;->b:LX/3JT;

    .line 547802
    invoke-static {p2}, LX/3Jh;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    if-eqz p2, :cond_3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_1
    const-string v4, "key_values"

    invoke-static {v3, v0, v4}, LX/3Je;->a(Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/3Jf;->c:Ljava/util/List;

    .line 547803
    iget-object v0, p0, LX/3Jf;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p3, v0}, LX/3Je;->a([[[FI)Z

    move-result v0

    const-string v3, "timing_curves"

    invoke-static {p3, v0, v3}, LX/3Je;->a(Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[[F

    iput-object v0, p0, LX/3Jf;->d:[[[F

    .line 547804
    if-eqz p4, :cond_0

    array-length v0, p4

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    const-string v0, "anchor"

    invoke-static {p4, v2, v0}, LX/3Je;->a(Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, LX/3Jf;->e:[F

    .line 547805
    iget-object v0, p0, LX/3Jf;->b:LX/3JT;

    invoke-virtual {v0}, LX/3JT;->isMatrixBased()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 547806
    invoke-static {p0}, LX/3Ji;->a(LX/3Jf;)LX/3Ji;

    move-result-object v0

    iput-object v0, p0, LX/3Jf;->f:LX/3Jj;

    .line 547807
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 547808
    goto :goto_0

    :cond_3
    move v0, v2

    .line 547809
    goto :goto_1

    .line 547810
    :cond_4
    iget-object v0, p0, LX/3Jf;->b:LX/3JT;

    sget-object v1, LX/3JT;->STROKE_WIDTH:LX/3JT;

    if-ne v0, v1, :cond_5

    .line 547811
    invoke-static {p0}, LX/3kO;->a(LX/3Jf;)LX/3kO;

    move-result-object v0

    iput-object v0, p0, LX/3Jf;->f:LX/3Jj;

    goto :goto_2

    .line 547812
    :cond_5
    iget-object v0, p0, LX/3Jf;->b:LX/3JT;

    sget-object v1, LX/3JT;->ANCHOR_POINT:LX/3JT;

    if-ne v0, v1, :cond_6

    .line 547813
    new-instance v0, LX/9Ul;

    .line 547814
    iget-object v1, p0, LX/3Jf;->c:Ljava/util/List;

    move-object v1, v1

    .line 547815
    iget-object v2, p0, LX/3Jf;->d:[[[F

    move-object v2, v2

    .line 547816
    invoke-direct {v0, v1, v2}, LX/9Ul;-><init>(Ljava/util/List;[[[F)V

    move-object v0, v0

    .line 547817
    iput-object v0, p0, LX/3Jf;->f:LX/3Jj;

    goto :goto_2

    .line 547818
    :cond_6
    iget-object v0, p0, LX/3Jf;->b:LX/3JT;

    sget-object v1, LX/3JT;->OPACITY:LX/3JT;

    if-ne v0, v1, :cond_7

    .line 547819
    invoke-static {p0}, LX/9Up;->a(LX/3Jf;)LX/9Up;

    move-result-object v0

    iput-object v0, p0, LX/3Jf;->f:LX/3Jj;

    goto :goto_2

    .line 547820
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown property type for animation post processing: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/3Jf;->b:LX/3JT;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
