.class public LX/2Tl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/Throwable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/Throwable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    .locals 0
    .param p1    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 414580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414581
    iput-object p1, p0, LX/2Tl;->a:Ljava/lang/Throwable;

    .line 414582
    iput-object p2, p0, LX/2Tl;->b:Ljava/lang/Throwable;

    .line 414583
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 414584
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "location_manager_throwable"

    iget-object v2, p0, LX/2Tl;->a:Ljava/lang/Throwable;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "wifi_scan_throwable"

    iget-object v2, p0, LX/2Tl;->b:Ljava/lang/Throwable;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
