.class public LX/2n7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2n7;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/Object;

.field private final c:LX/2n8;

.field private final d:LX/03V;


# direct methods
.method public constructor <init>(LX/2n8;LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463222
    const-class v0, LX/2n7;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2n7;->a:Ljava/lang/String;

    .line 463223
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/2n7;->b:Ljava/lang/Object;

    .line 463224
    iput-object p1, p0, LX/2n7;->c:LX/2n8;

    .line 463225
    iput-object p2, p0, LX/2n7;->d:LX/03V;

    .line 463226
    return-void
.end method

.method public static a(LX/0QB;)LX/2n7;
    .locals 5

    .prologue
    .line 463227
    sget-object v0, LX/2n7;->e:LX/2n7;

    if-nez v0, :cond_1

    .line 463228
    const-class v1, LX/2n7;

    monitor-enter v1

    .line 463229
    :try_start_0
    sget-object v0, LX/2n7;->e:LX/2n7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 463230
    if-eqz v2, :cond_0

    .line 463231
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 463232
    new-instance p0, LX/2n7;

    invoke-static {v0}, LX/2n8;->a(LX/0QB;)LX/2n8;

    move-result-object v3

    check-cast v3, LX/2n8;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/2n7;-><init>(LX/2n8;LX/03V;)V

    .line 463233
    move-object v0, p0

    .line 463234
    sput-object v0, LX/2n7;->e:LX/2n7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463235
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 463236
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 463237
    :cond_1
    sget-object v0, LX/2n7;->e:LX/2n7;

    return-object v0

    .line 463238
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 463239
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/7i4;",
            ">;"
        }
    .end annotation

    .prologue
    .line 463240
    iget-object v2, p0, LX/2n7;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 463241
    :try_start_0
    iget-object v0, p0, LX/2n7;->c:LX/2n8;

    invoke-virtual {v0}, LX/2n8;->a()Ljava/util/List;

    move-result-object v0

    .line 463242
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 463243
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7i3;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463244
    :try_start_1
    new-instance v1, LX/7i4;

    .line 463245
    iget-object v5, v0, LX/7i3;->a:Ljava/lang/String;

    move-object v5, v5

    .line 463246
    iget-object v6, v0, LX/7i3;->b:Ljava/lang/String;

    move-object v6, v6

    .line 463247
    iget-object v7, v0, LX/7i3;->c:Ljava/lang/String;

    move-object v7, v7

    .line 463248
    invoke-static {v7}, LX/7i0;->a(Ljava/lang/String;)LX/7i0;

    move-result-object v7

    .line 463249
    iget v8, v0, LX/7i3;->d:I

    move v8, v8

    .line 463250
    invoke-direct {v1, v5, v6, v7, v8}, LX/7i4;-><init>(Ljava/lang/String;Ljava/lang/String;LX/7i0;I)V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 463251
    :catch_0
    move-exception v1

    .line 463252
    :try_start_2
    iget-object v5, p0, LX/2n7;->d:LX/03V;

    iget-object v6, p0, LX/2n7;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unable to read data from DB: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 463253
    iget-object v8, v0, LX/7i3;->c:Ljava/lang/String;

    move-object v0, v8

    .line 463254
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 463255
    iput-object v1, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 463256
    move-object v0, v0

    .line 463257
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0

    .line 463258
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 463259
    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v3
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 463260
    iget-object v1, p0, LX/2n7;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 463261
    :try_start_0
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 463262
    iget-object v0, p0, LX/2n7;->c:LX/2n8;

    invoke-virtual {v0, p2}, LX/2n8;->a(Ljava/lang/String;)V

    .line 463263
    :goto_0
    monitor-exit v1

    return-void

    .line 463264
    :cond_0
    iget-object v0, p0, LX/2n7;->c:LX/2n8;

    invoke-virtual {v0, p1}, LX/2n8;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 463265
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/7i0;I)V
    .locals 9

    .prologue
    .line 463266
    iget-object v1, p0, LX/2n7;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 463267
    :try_start_0
    iget-object v0, p0, LX/2n7;->c:LX/2n8;

    .line 463268
    if-nez p3, :cond_0

    .line 463269
    const-string v3, ""

    .line 463270
    :goto_0
    move-object v2, v3

    .line 463271
    invoke-virtual {v0, p1, p2, v2, p4}, LX/2n8;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 463272
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 463273
    :cond_0
    iget-object v3, p3, LX/7i0;->a:Ljava/lang/String;

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "_EMPTY_PLACEHOLDER_"

    .line 463274
    :goto_1
    iget-object v4, p3, LX/7i0;->b:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "_EMPTY_PLACEHOLDER_"

    .line 463275
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v7, p3, LX/7i0;->c:J

    invoke-virtual {v5, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_<<<>>>_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "_<<<>>>_"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_<<<>>>_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 463276
    :cond_1
    iget-object v3, p3, LX/7i0;->a:Ljava/lang/String;

    goto :goto_1

    .line 463277
    :cond_2
    iget-object v4, p3, LX/7i0;->b:Ljava/lang/String;

    goto :goto_2
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 463278
    iget-object v1, p0, LX/2n7;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 463279
    :try_start_0
    iget-object v0, p0, LX/2n7;->c:LX/2n8;

    const/4 p0, 0x0

    .line 463280
    iget-object v2, v0, LX/2n8;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3m2;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "browser_prefetch_cache"

    invoke-virtual {v2, v3, p0, p0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 463281
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
