.class public LX/2iU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2dj;

.field private final b:LX/2do;

.field public final c:LX/2hZ;

.field public final d:LX/2hs;

.field private final e:Landroid/content/Context;

.field public final f:LX/2h7;

.field public final g:LX/2iM;

.field public final h:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2dj;LX/2do;LX/2hZ;LX/2hS;Landroid/content/Context;LX/2h7;LX/2iM;LX/1Ck;)V
    .locals 1
    .param p5    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/2h7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/2iM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/1Ck;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 451747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 451748
    iput-object p1, p0, LX/2iU;->a:LX/2dj;

    .line 451749
    iput-object p2, p0, LX/2iU;->b:LX/2do;

    .line 451750
    iput-object p3, p0, LX/2iU;->c:LX/2hZ;

    .line 451751
    invoke-virtual {p4, p8}, LX/2hS;->a(LX/1Ck;)LX/2hs;

    move-result-object v0

    iput-object v0, p0, LX/2iU;->d:LX/2hs;

    .line 451752
    iput-object p5, p0, LX/2iU;->e:Landroid/content/Context;

    .line 451753
    iput-object p6, p0, LX/2iU;->f:LX/2h7;

    .line 451754
    iput-object p7, p0, LX/2iU;->g:LX/2iM;

    .line 451755
    iput-object p8, p0, LX/2iU;->h:LX/1Ck;

    .line 451756
    return-void
.end method

.method public static a(LX/2iU;)V
    .locals 2

    .prologue
    .line 451773
    iget-object v0, p0, LX/2iU;->e:Landroid/content/Context;

    instance-of v0, v0, LX/8DG;

    if-eqz v0, :cond_0

    .line 451774
    iget-object v0, p0, LX/2iU;->e:Landroid/content/Context;

    check-cast v0, LX/8DG;

    const-string v1, "people_you_may_know"

    invoke-interface {v0, v1}, LX/8DG;->c(Ljava/lang/String;)V

    .line 451775
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/2iU;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V
    .locals 3

    .prologue
    .line 451771
    iget-object v0, p0, LX/2iU;->b:LX/2do;

    new-instance v1, LX/2f2;

    invoke-direct {v1, p1, p2, p3, p4}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 451772
    return-void
.end method

.method public static a$redex0(LX/2iU;Lcom/facebook/friends/model/FriendRequest;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Ljava/lang/Throwable;)V
    .locals 4
    .param p1    # Lcom/facebook/friends/model/FriendRequest;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 451757
    iget-object v0, p0, LX/2iU;->c:LX/2hZ;

    .line 451758
    new-instance v1, LX/Eyd;

    invoke-direct {v1, p0, p1}, LX/Eyd;-><init>(LX/2iU;Lcom/facebook/friends/model/FriendRequest;)V

    move-object v1, v1

    .line 451759
    invoke-virtual {v0, p3, v1}, LX/2hZ;->a(Ljava/lang/Throwable;Landroid/content/DialogInterface$OnClickListener;)V

    .line 451760
    instance-of v0, p3, LX/4Ua;

    move v0, v0

    .line 451761
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/friends/model/FriendRequest;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 451762
    iget-object v0, p0, LX/2iU;->g:LX/2iM;

    .line 451763
    iget-object v1, p1, Lcom/facebook/friends/model/FriendRequest;->b:Ljava/lang/String;

    move-object v1, v1

    .line 451764
    invoke-interface {v0, v1}, LX/2iM;->a(Ljava/lang/String;)V

    .line 451765
    :goto_0
    if-eqz p2, :cond_0

    .line 451766
    invoke-virtual {p1}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, p2, v2}, LX/2iU;->a$redex0(LX/2iU;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 451767
    :cond_0
    return-void

    .line 451768
    :cond_1
    iget-object v0, p0, LX/2iU;->g:LX/2iM;

    .line 451769
    iget-object v1, p1, Lcom/facebook/friends/model/FriendRequest;->b:Ljava/lang/String;

    move-object v1, v1

    .line 451770
    sget-object v2, LX/2lu;->NEEDS_RESPONSE:LX/2lu;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, LX/2iM;->a(Ljava/lang/String;LX/2lu;Z)V

    goto :goto_0
.end method
