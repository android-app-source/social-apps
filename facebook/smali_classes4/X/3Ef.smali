.class public final LX/3Ef;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic c:LX/1rk;


# direct methods
.method public constructor <init>(LX/1rk;ZLcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 537919
    iput-object p1, p0, LX/3Ef;->c:LX/1rk;

    iput-boolean p2, p0, LX/3Ef;->a:Z

    iput-object p3, p0, LX/3Ef;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3

    .prologue
    .line 537920
    check-cast p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    .line 537921
    iget-boolean v0, p0, LX/3Ef;->a:Z

    if-eqz v0, :cond_0

    .line 537922
    iget-object v0, p0, LX/3Ef;->c:LX/1rk;

    iget-object v0, v0, LX/1rk;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Ef;->c:LX/1rk;

    iget-object v0, v0, LX/1rk;->q:Ljava/lang/String;

    .line 537923
    iget-object v1, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 537924
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 537925
    iget-object v0, p0, LX/3Ef;->c:LX/1rk;

    iget-object v0, v0, LX/1rk;->m:LX/03V;

    const-string v1, "NotificationsSyncHelper_syncNotifications"

    const-string v2, "recursive fetch with the same cursor"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 537926
    :cond_0
    iget-object v0, p0, LX/3Ef;->c:LX/1rk;

    .line 537927
    iget-object v1, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 537928
    iput-object v1, v0, LX/1rk;->q:Ljava/lang/String;

    .line 537929
    iget-object v0, p0, LX/3Ef;->c:LX/1rk;

    iget-object v1, p0, LX/3Ef;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, LX/1rk;->a(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
