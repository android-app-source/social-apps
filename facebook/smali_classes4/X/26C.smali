.class public LX/26C;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/26C;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:I

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 371611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 371612
    iput-object p1, p0, LX/26C;->a:Landroid/content/res/Resources;

    .line 371613
    iget-object v0, p0, LX/26C;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0648

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/26C;->b:I

    .line 371614
    iget-object v0, p0, LX/26C;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0646

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/26C;->c:I

    .line 371615
    iget-object v0, p0, LX/26C;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0647

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/26C;->d:I

    .line 371616
    return-void
.end method

.method public static a(LX/0QB;)LX/26C;
    .locals 4

    .prologue
    .line 371617
    sget-object v0, LX/26C;->e:LX/26C;

    if-nez v0, :cond_1

    .line 371618
    const-class v1, LX/26C;

    monitor-enter v1

    .line 371619
    :try_start_0
    sget-object v0, LX/26C;->e:LX/26C;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 371620
    if-eqz v2, :cond_0

    .line 371621
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 371622
    new-instance p0, LX/26C;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/26C;-><init>(Landroid/content/res/Resources;)V

    .line 371623
    move-object v0, p0

    .line 371624
    sput-object v0, LX/26C;->e:LX/26C;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 371625
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 371626
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 371627
    :cond_1
    sget-object v0, LX/26C;->e:LX/26C;

    return-object v0

    .line 371628
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 371629
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 371630
    iget-object v0, p0, LX/26C;->a:Landroid/content/res/Resources;

    const v1, 0x7f020caa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 371631
    iget v1, p2, Landroid/graphics/Rect;->right:I

    iget v2, p0, LX/26C;->d:I

    sub-int/2addr v1, v2

    iget v2, p0, LX/26C;->b:I

    sub-int/2addr v1, v2

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, LX/26C;->c:I

    sub-int/2addr v2, v3

    iget v3, p0, LX/26C;->b:I

    sub-int/2addr v2, v3

    iget v3, p2, Landroid/graphics/Rect;->right:I

    iget v4, p0, LX/26C;->d:I

    sub-int/2addr v3, v4

    iget v4, p2, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, LX/26C;->c:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 371632
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 371633
    return-void
.end method
