.class public LX/3Q4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3Q4;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3H7;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/3Q5;


# direct methods
.method public constructor <init>(LX/3Q5;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 563045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563046
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 563047
    iput-object v0, p0, LX/3Q4;->a:LX/0Ot;

    .line 563048
    iput-object p1, p0, LX/3Q4;->b:LX/3Q5;

    .line 563049
    iget-object v0, p0, LX/3Q4;->b:LX/3Q5;

    invoke-virtual {v0}, LX/2AE;->g()V

    .line 563050
    return-void
.end method

.method public static a(LX/0QB;)LX/3Q4;
    .locals 7

    .prologue
    .line 563027
    sget-object v0, LX/3Q4;->c:LX/3Q4;

    if-nez v0, :cond_1

    .line 563028
    const-class v1, LX/3Q4;

    monitor-enter v1

    .line 563029
    :try_start_0
    sget-object v0, LX/3Q4;->c:LX/3Q4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 563030
    if-eqz v2, :cond_0

    .line 563031
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 563032
    new-instance v4, LX/3Q4;

    .line 563033
    new-instance p0, LX/3Q5;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v5

    check-cast v5, LX/0rb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {p0, v3, v5, v6}, LX/3Q5;-><init>(LX/0Sh;LX/0rb;LX/03V;)V

    .line 563034
    move-object v3, p0

    .line 563035
    check-cast v3, LX/3Q5;

    invoke-direct {v4, v3}, LX/3Q4;-><init>(LX/3Q5;)V

    .line 563036
    const/16 v3, 0x1296

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 563037
    iput-object v3, v4, LX/3Q4;->a:LX/0Ot;

    .line 563038
    move-object v0, v4

    .line 563039
    sput-object v0, LX/3Q4;->c:LX/3Q4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 563040
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 563041
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 563042
    :cond_1
    sget-object v0, LX/3Q4;->c:LX/3Q4;

    return-object v0

    .line 563043
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 563044
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final c(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 563022
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563023
    const/4 v0, 0x0

    .line 563024
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3Q4;->b:LX/3Q5;

    invoke-virtual {v0, p1}, LX/2AG;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 563025
    iget-object v0, p0, LX/3Q4;->b:LX/3Q5;

    invoke-virtual {v0}, LX/2AG;->d()V

    .line 563026
    return-void
.end method
