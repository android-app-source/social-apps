.class public LX/2Xk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/katana/server/protocol/GetSsoUserMethod$Result;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 420149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420150
    return-void
.end method

.method public static a(LX/0QB;)LX/2Xk;
    .locals 1

    .prologue
    .line 420146
    new-instance v0, LX/2Xk;

    invoke-direct {v0}, LX/2Xk;-><init>()V

    .line 420147
    move-object v0, v0

    .line 420148
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 420143
    check-cast p1, Ljava/lang/String;

    .line 420144
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "query"

    const-string v2, "SELECT username, name FROM user WHERE uid=me()"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "access_token"

    invoke-direct {v1, v2, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 420145
    new-instance v0, LX/14N;

    const-string v1, "getSsoUserMethod"

    const-string v2, "POST"

    const-string v3, "method/fql.query"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 420138
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 420139
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    .line 420140
    const-string v1, "username"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    .line 420141
    const-string v2, "name"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    .line 420142
    new-instance v2, Lcom/facebook/katana/server/protocol/GetSsoUserMethod$Result;

    invoke-direct {v2, v1, v0}, Lcom/facebook/katana/server/protocol/GetSsoUserMethod$Result;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method
