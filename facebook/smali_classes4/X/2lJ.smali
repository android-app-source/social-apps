.class public LX/2lJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile u:LX/2lJ;


# instance fields
.field public final a:Lcom/facebook/bookmark/model/Bookmark;

.field public final b:Lcom/facebook/bookmark/model/Bookmark;

.field public final c:Lcom/facebook/bookmark/model/Bookmark;

.field public final d:Lcom/facebook/bookmark/model/Bookmark;

.field public final e:Lcom/facebook/bookmark/model/Bookmark;

.field public final f:Lcom/facebook/bookmark/model/Bookmark;

.field public final g:Lcom/facebook/bookmark/model/Bookmark;

.field public final h:Lcom/facebook/bookmark/model/Bookmark;

.field public final i:Lcom/facebook/bookmark/model/Bookmark;

.field public final j:Lcom/facebook/bookmark/model/Bookmark;

.field public final k:Lcom/facebook/bookmark/model/Bookmark;

.field public final l:Lcom/facebook/bookmark/model/Bookmark;

.field public final m:Lcom/facebook/bookmark/model/Bookmark;

.field public final n:Lcom/facebook/bookmark/model/Bookmark;

.field public final o:Lcom/facebook/bookmark/model/Bookmark;

.field public final p:Lcom/facebook/bookmark/model/Bookmark;

.field public final q:Lcom/facebook/bookmark/model/Bookmark;

.field public final r:Lcom/facebook/bookmark/model/Bookmark;

.field public final s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;Ljava/lang/Boolean;)V
    .locals 3
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 457542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457543
    iput-object p2, p0, LX/2lJ;->s:LX/0Or;

    .line 457544
    iput-object p1, p0, LX/2lJ;->t:Landroid/content/Context;

    .line 457545
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f0831f4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->fj:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->a:Lcom/facebook/bookmark/model/Bookmark;

    .line 457546
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f0831f3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->cI:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->b:Lcom/facebook/bookmark/model/Bookmark;

    .line 457547
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f0831ff

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->cO:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->c:Lcom/facebook/bookmark/model/Bookmark;

    .line 457548
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f083200

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->gF:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->d:Lcom/facebook/bookmark/model/Bookmark;

    .line 457549
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f0831f2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->ee:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->g:Lcom/facebook/bookmark/model/Bookmark;

    .line 457550
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f081e0a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->ef:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->h:Lcom/facebook/bookmark/model/Bookmark;

    .line 457551
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f0831fd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/0ax;->dy:Ljava/lang/String;

    :goto_0
    invoke-static {v1, v0}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->i:Lcom/facebook/bookmark/model/Bookmark;

    .line 457552
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f0831dc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->dl:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->j:Lcom/facebook/bookmark/model/Bookmark;

    .line 457553
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f083202

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->dz:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->k:Lcom/facebook/bookmark/model/Bookmark;

    .line 457554
    iget-object v1, p0, LX/2lJ;->t:Landroid/content/Context;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f083204

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/0ax;->dO:Ljava/lang/String;

    :goto_2
    invoke-static {v1, v0}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->l:Lcom/facebook/bookmark/model/Bookmark;

    .line 457555
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f0800cb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->ec:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->m:Lcom/facebook/bookmark/model/Bookmark;

    .line 457556
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f0831f1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->ed:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->n:Lcom/facebook/bookmark/model/Bookmark;

    .line 457557
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f083201

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->eO:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->e:Lcom/facebook/bookmark/model/Bookmark;

    .line 457558
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f082eeb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->eT:Ljava/lang/String;

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->f:Lcom/facebook/bookmark/model/Bookmark;

    .line 457559
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f080100

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->gD:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->o:Lcom/facebook/bookmark/model/Bookmark;

    .line 457560
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f080101

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->gB:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->p:Lcom/facebook/bookmark/model/Bookmark;

    .line 457561
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f0831f5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->q:Lcom/facebook/bookmark/model/Bookmark;

    .line 457562
    iget-object v0, p0, LX/2lJ;->t:Landroid/content/Context;

    const v1, 0x7f0831fe

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    iput-object v0, p0, LX/2lJ;->r:Lcom/facebook/bookmark/model/Bookmark;

    .line 457563
    return-void

    .line 457564
    :cond_0
    sget-object v0, LX/0ax;->dx:Ljava/lang/String;

    goto/16 :goto_0

    .line 457565
    :cond_1
    const v0, 0x7f083203

    goto/16 :goto_1

    :cond_2
    sget-object v0, LX/0ax;->dN:Ljava/lang/String;

    goto/16 :goto_2
.end method

.method public static a(LX/0QB;)LX/2lJ;
    .locals 6

    .prologue
    .line 457566
    sget-object v0, LX/2lJ;->u:LX/2lJ;

    if-nez v0, :cond_1

    .line 457567
    const-class v1, LX/2lJ;

    monitor-enter v1

    .line 457568
    :try_start_0
    sget-object v0, LX/2lJ;->u:LX/2lJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 457569
    if-eqz v2, :cond_0

    .line 457570
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 457571
    new-instance v5, LX/2lJ;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0x12cb

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-direct {v5, v3, p0, v4}, LX/2lJ;-><init>(Landroid/content/Context;LX/0Or;Ljava/lang/Boolean;)V

    .line 457572
    move-object v0, v5

    .line 457573
    sput-object v0, LX/2lJ;->u:LX/2lJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457574
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 457575
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 457576
    :cond_1
    sget-object v0, LX/2lJ;->u:LX/2lJ;

    return-object v0

    .line 457577
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 457578
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;
    .locals 1

    .prologue
    .line 457579
    const/4 v0, -0x1

    invoke-static {p0, p1, v0}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;I)Lcom/facebook/bookmark/model/Bookmark;
    .locals 7

    .prologue
    .line 457580
    new-instance v1, LX/2lK;

    int-to-long v2, p2

    const/4 v6, 0x0

    move-object v4, p0

    move-object v5, p1

    invoke-direct/range {v1 .. v6}, LX/2lK;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    const-string v0, "app"

    .line 457581
    iput-object v0, v1, LX/2lK;->g:Ljava/lang/String;

    .line 457582
    move-object v0, v1

    .line 457583
    invoke-virtual {v0}, LX/2lK;->a()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/bookmark/model/Bookmark;
    .locals 3

    .prologue
    .line 457584
    iget-object v0, p0, LX/2lJ;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 457585
    if-nez v0, :cond_0

    .line 457586
    const/4 v0, 0x0

    .line 457587
    :goto_0
    return-object v0

    .line 457588
    :cond_0
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 457589
    sget-object v1, LX/0ax;->bL:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 457590
    iget-object v1, p0, LX/2lJ;->t:Landroid/content/Context;

    const v2, 0x7f0831f6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LX/2lJ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 457591
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 457592
    new-instance v1, Lcom/facebook/bookmark/model/BookmarksGroup;

    const-string v2, "profile"

    iget-object v3, p0, LX/2lJ;->t:Landroid/content/Context;

    const v4, 0x7f0831ef

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Lcom/facebook/bookmark/model/Bookmark;

    const/4 v5, 0x0

    .line 457593
    iget-object v8, p0, LX/2lJ;->t:Landroid/content/Context;

    const v9, 0x7f0831ef

    invoke-virtual {v8, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 457594
    const/4 v10, 0x0

    .line 457595
    iget-object v9, p0, LX/2lJ;->s:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/user/model/User;

    .line 457596
    if-eqz v9, :cond_0

    .line 457597
    invoke-virtual {v9}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v12

    .line 457598
    invoke-virtual {v9}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v8

    .line 457599
    :goto_0
    new-instance v9, LX/2lK;

    const-wide/16 v10, -0x1

    sget-object v13, LX/0ax;->dg:Ljava/lang/String;

    const/4 v14, 0x0

    invoke-direct/range {v9 .. v14}, LX/2lK;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    .line 457600
    iput-object v8, v9, LX/2lK;->f:Ljava/lang/String;

    .line 457601
    move-object v8, v9

    .line 457602
    const-string v9, "profile"

    .line 457603
    iput-object v9, v8, LX/2lK;->g:Ljava/lang/String;

    .line 457604
    move-object v8, v8

    .line 457605
    invoke-virtual {v8}, LX/2lK;->a()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v8

    move-object v6, v8

    .line 457606
    aput-object v6, v4, v5

    invoke-static {v4}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v1, v2, v3, v7, v4}, Lcom/facebook/bookmark/model/BookmarksGroup;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 457607
    return-object v0

    :cond_0
    move-object v12, v8

    move-object v8, v10

    goto :goto_0
.end method
