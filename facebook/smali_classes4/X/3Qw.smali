.class public LX/3Qw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/VideoChannelEntryPoint;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:I

.field public final f:I

.field public final g:LX/04D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/04g;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:LX/D6I;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Z

.field public final k:LX/D4s;

.field public final l:LX/0JG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:I

.field public final o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:LX/19o;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:LX/03z;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final s:Z


# direct methods
.method private constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/List;Ljava/util/List;Ljava/lang/String;IILX/04D;LX/04g;LX/D6I;ZLX/D4s;LX/0JG;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/19o;LX/03z;Z)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelEntryPoint;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "II",
            "LX/04D;",
            "LX/04g;",
            "LX/D6I;",
            "Z",
            "LX/D4s;",
            "LX/0JG;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/19o;",
            "LX/03z;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 566643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566644
    iput-object p1, p0, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 566645
    iput-object p2, p0, LX/3Qw;->b:Ljava/util/List;

    .line 566646
    iput-object p3, p0, LX/3Qw;->c:Ljava/util/List;

    .line 566647
    iput-object p4, p0, LX/3Qw;->d:Ljava/lang/String;

    .line 566648
    iput p5, p0, LX/3Qw;->e:I

    .line 566649
    iput p6, p0, LX/3Qw;->f:I

    .line 566650
    iput-object p7, p0, LX/3Qw;->g:LX/04D;

    .line 566651
    iput-object p8, p0, LX/3Qw;->h:LX/04g;

    .line 566652
    iput-object p9, p0, LX/3Qw;->i:LX/D6I;

    .line 566653
    iput-boolean p10, p0, LX/3Qw;->j:Z

    .line 566654
    iput-object p11, p0, LX/3Qw;->k:LX/D4s;

    .line 566655
    iput-object p12, p0, LX/3Qw;->l:LX/0JG;

    .line 566656
    iput-object p13, p0, LX/3Qw;->m:Ljava/lang/String;

    .line 566657
    iput p14, p0, LX/3Qw;->n:I

    .line 566658
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3Qw;->o:Ljava/lang/String;

    .line 566659
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3Qw;->p:Ljava/lang/String;

    .line 566660
    move-object/from16 v0, p17

    iput-object v0, p0, LX/3Qw;->q:LX/19o;

    .line 566661
    move-object/from16 v0, p18

    iput-object v0, p0, LX/3Qw;->r:LX/03z;

    .line 566662
    move/from16 v0, p19

    iput-boolean v0, p0, LX/3Qw;->s:Z

    .line 566663
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/List;Ljava/util/List;Ljava/lang/String;IILX/04D;LX/04g;LX/D6I;ZLX/D4s;LX/0JG;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/19o;LX/03z;ZB)V
    .locals 0

    .prologue
    .line 566664
    invoke-direct/range {p0 .. p19}, LX/3Qw;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/List;Ljava/util/List;Ljava/lang/String;IILX/04D;LX/04g;LX/D6I;ZLX/D4s;LX/0JG;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/19o;LX/03z;Z)V

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 566665
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "ChannelFeedParams{story="

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 566666
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 566667
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", prefilledStories="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3Qw;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", videoChannelIds=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3Qw;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", entryPoint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3Qw;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", seekPositionMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/3Qw;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastStartPositionMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/3Qw;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playerOrigin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3Qw;->g:LX/04D;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", originalPlayReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3Qw;->h:LX/04g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", transitionManager="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3Qw;->i:LX/D6I;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", disableCache="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/3Qw;->j:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", headerParams="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3Qw;->k:LX/D4s;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", externalLogType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3Qw;->l:LX/0JG;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", externalLogId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3Qw;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", selectedProductItemIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/3Qw;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reactionComponentTrackingData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3Qw;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", overlayTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3Qw;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", projectionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3Qw;->q:LX/19o;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", audioChannelLayout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3Qw;->r:LX/03z;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isSphericalFallback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/3Qw;->s:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
