.class public LX/331;
.super Lcom/facebook/drawee/view/GenericDraweeView;
.source ""


# instance fields
.field public a:LX/1L1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1er;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/1LD;

.field public d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 491559
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/331;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 491560
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 491557
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/331;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 491558
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 491553
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/view/GenericDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 491554
    const/4 v0, -0x1

    iput v0, p0, LX/331;->d:I

    .line 491555
    invoke-direct {p0, p1}, LX/331;->a(Landroid/content/Context;)V

    .line 491556
    return-void
.end method

.method private static a(LX/331;LX/1L1;LX/1er;)V
    .locals 0

    .prologue
    .line 491552
    iput-object p1, p0, LX/331;->a:LX/1L1;

    iput-object p2, p0, LX/331;->b:LX/1er;

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 491529
    const-class v0, LX/331;

    invoke-static {v0, p0}, LX/331;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 491530
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 491531
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0a045d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 491532
    new-instance v2, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    const v3, 0x7f021af6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/16 v4, 0x3e8

    invoke-direct {v2, v3, v4}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 491533
    new-instance v3, LX/1Uo;

    invoke-direct {v3, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 491534
    iput-object v1, v3, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 491535
    move-object v1, v3

    .line 491536
    const v3, 0x7f020aa6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 491537
    iput-object v0, v1, LX/1Uo;->h:Landroid/graphics/drawable/Drawable;

    .line 491538
    move-object v0, v1

    .line 491539
    iput-object v2, v0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 491540
    move-object v0, v0

    .line 491541
    sget-object v1, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 491542
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 491543
    new-instance v0, LX/332;

    invoke-direct {v0, p0}, LX/332;-><init>(LX/331;)V

    iput-object v0, p0, LX/331;->c:LX/1LD;

    .line 491544
    iget-object v0, p0, LX/331;->a:LX/1L1;

    iget-object v1, p0, LX/331;->c:LX/1LD;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 491545
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/331;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, LX/331;

    invoke-static {v1}, LX/1L1;->a(LX/0QB;)LX/1L1;

    move-result-object v0

    check-cast v0, LX/1L1;

    invoke-static {v1}, LX/1er;->a(LX/0QB;)LX/1er;

    move-result-object v1

    check-cast v1, LX/1er;

    invoke-static {p0, v0, v1}, LX/331;->a(LX/331;LX/1L1;LX/1er;)V

    return-void
.end method


# virtual methods
.method public setContentId(I)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 491546
    iput p1, p0, LX/331;->d:I

    .line 491547
    iget v0, p0, LX/331;->d:I

    if-ne v0, v1, :cond_0

    .line 491548
    :goto_0
    return-void

    .line 491549
    :cond_0
    iget-object v0, p0, LX/331;->b:LX/1er;

    .line 491550
    iget p1, v0, LX/1er;->c:I

    move v0, p1

    .line 491551
    if-eq v0, v1, :cond_1

    iget v1, p0, LX/331;->d:I

    if-eq v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/331;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    goto :goto_1
.end method
