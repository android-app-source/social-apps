.class public LX/2P0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static volatile j:LX/2P0;


# instance fields
.field public final c:LX/2NB;

.field private final d:LX/2P3;

.field private final e:LX/0SF;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/2Oy;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Dom;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 404862
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, LX/2P1;->a:LX/0U1;

    .line 404863
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404864
    aput-object v1, v0, v3

    sget-object v1, LX/2P1;->b:LX/0U1;

    .line 404865
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404866
    aput-object v1, v0, v4

    sget-object v1, LX/2P1;->i:LX/0U1;

    .line 404867
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404868
    aput-object v1, v0, v5

    const/4 v1, 0x3

    sget-object v2, LX/2P1;->c:LX/0U1;

    .line 404869
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 404870
    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/2P1;->e:LX/0U1;

    .line 404871
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 404872
    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/2P1;->f:LX/0U1;

    .line 404873
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 404874
    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2P1;->g:LX/0U1;

    .line 404875
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 404876
    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2P1;->j:LX/0U1;

    .line 404877
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 404878
    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2P1;->m:LX/0U1;

    .line 404879
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 404880
    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2P1;->n:LX/0U1;

    .line 404881
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 404882
    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/2P1;->o:LX/0U1;

    .line 404883
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 404884
    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/2P1;->k:LX/0U1;

    .line 404885
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 404886
    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/2P1;->l:LX/0U1;

    .line 404887
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 404888
    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/2P1;->p:LX/0U1;

    .line 404889
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 404890
    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/2P2;->d:LX/0U1;

    .line 404891
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 404892
    aput-object v2, v0, v1

    sput-object v0, LX/2P0;->a:[Ljava/lang/String;

    .line 404893
    new-array v0, v5, [Ljava/lang/String;

    sget-object v1, LX/2P1;->b:LX/0U1;

    .line 404894
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404895
    aput-object v1, v0, v3

    sget-object v1, LX/2P1;->k:LX/0U1;

    .line 404896
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404897
    aput-object v1, v0, v4

    sput-object v0, LX/2P0;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2NB;LX/2P3;LX/0SF;LX/0Or;LX/0Or;LX/2Oy;LX/0Or;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2NB;",
            "LX/2P3;",
            "LX/0SF;",
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/2Oy;",
            "LX/0Or",
            "<",
            "LX/Dom;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 404736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 404737
    iput-object p1, p0, LX/2P0;->c:LX/2NB;

    .line 404738
    iput-object p2, p0, LX/2P0;->d:LX/2P3;

    .line 404739
    iput-object p3, p0, LX/2P0;->e:LX/0SF;

    .line 404740
    iput-object p4, p0, LX/2P0;->f:LX/0Or;

    .line 404741
    iput-object p5, p0, LX/2P0;->g:LX/0Or;

    .line 404742
    iput-object p6, p0, LX/2P0;->h:LX/2Oy;

    .line 404743
    iput-object p7, p0, LX/2P0;->i:LX/0Or;

    .line 404744
    return-void
.end method

.method public static a(LX/2P0;LX/0ux;)LX/0Rf;
    .locals 13
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ux;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 404745
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 404746
    invoke-virtual {v4, p1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404747
    invoke-static {}, LX/2P0;->d()LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404748
    const-string v0, "%s LEFT OUTER JOIN %s ON (%s.%s = %s.%s)"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "messages"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "thread_participants"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "messages"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, LX/2P1;->e:LX/0U1;

    .line 404749
    iget-object v5, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v5

    .line 404750
    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "thread_participants"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, LX/2P2;->a:LX/0U1;

    .line 404751
    iget-object v5, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v5

    .line 404752
    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 404753
    new-instance v10, LX/0cA;

    invoke-direct {v10}, LX/0cA;-><init>()V

    .line 404754
    new-instance v11, Ljava/util/LinkedList;

    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    .line 404755
    iget-object v0, p0, LX/2P0;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 404756
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 404757
    sget-object v2, LX/2P0;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 404758
    :goto_0
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 404759
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 404760
    invoke-static {v2, v0}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 404761
    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_0

    .line 404762
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 404763
    :catchall_0
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    :goto_1
    if-eqz v2, :cond_0

    if-eqz v1, :cond_5

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_0
    :goto_2
    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 404764
    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 404765
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_3
    if-eqz v8, :cond_1

    if-eqz v9, :cond_6

    :try_start_6
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    :cond_1
    :goto_4
    throw v0

    .line 404766
    :cond_2
    if-eqz v2, :cond_3

    :try_start_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 404767
    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, LX/2gJ;->close()V

    .line 404768
    :cond_4
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 404769
    invoke-direct {p0, v0}, LX/2P0;->a(Landroid/content/ContentValues;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 404770
    invoke-virtual {v10, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_5

    .line 404771
    :catch_2
    move-exception v2

    :try_start_8
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 404772
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 404773
    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 404774
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_4

    .line 404775
    :cond_7
    invoke-virtual {v10}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0

    .line 404776
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/2P0;
    .locals 11

    .prologue
    .line 404777
    sget-object v0, LX/2P0;->j:LX/2P0;

    if-nez v0, :cond_1

    .line 404778
    const-class v1, LX/2P0;

    monitor-enter v1

    .line 404779
    :try_start_0
    sget-object v0, LX/2P0;->j:LX/2P0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 404780
    if-eqz v2, :cond_0

    .line 404781
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 404782
    new-instance v3, LX/2P0;

    invoke-static {v0}, LX/2NB;->b(LX/0QB;)LX/2NB;

    move-result-object v4

    check-cast v4, LX/2NB;

    invoke-static {v0}, LX/2P3;->a(LX/0QB;)LX/2P3;

    move-result-object v5

    check-cast v5, LX/2P3;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SF;

    const/16 v7, 0xdc6

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x12cb

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/2Oy;->a(LX/0QB;)LX/2Oy;

    move-result-object v9

    check-cast v9, LX/2Oy;

    const/16 v10, 0x2a17

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/2P0;-><init>(LX/2NB;LX/2P3;LX/0SF;LX/0Or;LX/0Or;LX/2Oy;LX/0Or;)V

    .line 404783
    move-object v0, v3

    .line 404784
    sput-object v0, LX/2P0;->j:LX/2P0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404785
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 404786
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 404787
    :cond_1
    sget-object v0, LX/2P0;->j:LX/2P0;

    return-object v0

    .line 404788
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 404789
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;LX/0ux;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 404790
    sget-object v0, LX/2P1;->i:LX/0U1;

    .line 404791
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 404792
    sget-object v1, LX/2uW;->PENDING_SEND:LX/2uW;

    iget v1, v1, LX/2uW;->dbKeyValue:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 404793
    const/4 v1, 0x2

    new-array v1, v1, [LX/0ux;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    aput-object p1, v1, v0

    invoke-static {v1}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    .line 404794
    const-string v1, "messages"

    sget-object v2, LX/2P0;->b:[Ljava/lang/String;

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/database/Cursor;)Landroid/util/Pair;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 404795
    sget-object v0, LX/2P1;->b:LX/0U1;

    invoke-virtual {v0, p0}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 404796
    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 404797
    sget-object v1, LX/2P1;->k:LX/0U1;

    invoke-virtual {v1, p0}, LX/0U1;->f(Landroid/database/Cursor;)[B

    move-result-object v1

    .line 404798
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/ContentValues;)Lcom/facebook/messaging/model/messages/Message;
    .locals 20

    .prologue
    .line 404799
    sget-object v2, LX/2P1;->a:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 404800
    sget-object v2, LX/2P1;->b:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 404801
    sget-object v2, LX/2P1;->i:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    .line 404802
    const/4 v2, 0x0

    .line 404803
    if-eqz v3, :cond_9

    .line 404804
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v3, v2

    .line 404805
    :goto_0
    sget-object v2, LX/2P1;->c:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v13

    .line 404806
    sget-object v2, LX/2P1;->f:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    .line 404807
    const-wide/16 v4, 0x0

    .line 404808
    if-eqz v2, :cond_0

    .line 404809
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 404810
    :cond_0
    sget-object v2, LX/2P1;->g:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    .line 404811
    const-wide/16 v6, 0x0

    .line 404812
    if-eqz v2, :cond_1

    .line 404813
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 404814
    :cond_1
    sget-object v2, LX/2P1;->j:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 404815
    sget-object v2, LX/2P1;->m:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 404816
    sget-object v2, LX/2P1;->l:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    .line 404817
    const-wide/16 v8, 0x0

    .line 404818
    if-eqz v2, :cond_2

    .line 404819
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 404820
    :cond_2
    sget-object v2, LX/2P1;->e:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 404821
    sget-object v2, LX/2P2;->d:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 404822
    invoke-static {v10}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 404823
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2P0;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 404824
    invoke-virtual {v2}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 404825
    invoke-virtual {v2}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v2

    .line 404826
    :goto_1
    invoke-static {v12}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v10

    .line 404827
    invoke-static {v3}, LX/2uW;->fromDbKeyValue(I)LX/2uW;

    move-result-object v12

    .line 404828
    new-instance v17, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-static/range {v16 .. v16}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v3, v2}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 404829
    sget-object v2, LX/2uW;->REGULAR:LX/2uW;

    if-ne v12, v2, :cond_3

    .line 404830
    invoke-static {}, LX/Doq;->b()LX/Doq;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, LX/Doq;->a(J)V

    .line 404831
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2P0;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Dom;

    invoke-virtual {v2, v10}, LX/Dom;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)[B

    move-result-object v16

    .line 404832
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v13}, LX/2P0;->a([B[B)[B

    move-result-object v13

    .line 404833
    sget-object v2, Lcom/facebook/messaging/model/send/SendError;->a:Lcom/facebook/messaging/model/send/SendError;

    .line 404834
    invoke-static {v15}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 404835
    const-wide/16 v2, 0x0

    .line 404836
    sget-object v18, LX/2P1;->o:LX/0U1;

    invoke-virtual/range {v18 .. v18}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v18

    .line 404837
    if-eqz v18, :cond_4

    .line 404838
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 404839
    :cond_4
    sget-object v18, LX/2P1;->n:LX/0U1;

    invoke-virtual/range {v18 .. v18}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 404840
    new-instance v19, LX/6fO;

    invoke-direct/range {v19 .. v19}, LX/6fO;-><init>()V

    invoke-static {v15}, LX/6fP;->fromSerializedString(Ljava/lang/String;)LX/6fP;

    move-result-object v15

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, LX/6fO;->a(LX/6fP;)LX/6fO;

    move-result-object v15

    invoke-virtual {v15, v2, v3}, LX/6fO;->a(J)LX/6fO;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, LX/6fO;->a(Ljava/lang/String;)LX/6fO;

    move-result-object v2

    invoke-virtual {v2}, LX/6fO;->g()Lcom/facebook/messaging/model/send/SendError;

    move-result-object v2

    .line 404841
    :cond_5
    sget-object v3, LX/2P1;->k:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v3

    .line 404842
    move-object/from16 v0, p0

    iget-object v15, v0, LX/2P0;->h:LX/2Oy;

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v3}, LX/2Oy;->c([B[B)Ljava/lang/String;

    move-result-object v3

    .line 404843
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 404844
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 404845
    :goto_2
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v15

    .line 404846
    invoke-virtual {v15, v11}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    .line 404847
    invoke-virtual {v15, v10}, LX/6f7;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/6f7;

    .line 404848
    invoke-virtual {v15, v12}, LX/6f7;->a(LX/2uW;)LX/6f7;

    .line 404849
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6f7;

    .line 404850
    invoke-virtual {v15, v4, v5}, LX/6f7;->a(J)LX/6f7;

    .line 404851
    invoke-virtual {v15, v6, v7}, LX/6f7;->b(J)LX/6f7;

    .line 404852
    invoke-virtual {v15, v14}, LX/6f7;->d(Ljava/lang/String;)LX/6f7;

    .line 404853
    invoke-virtual {v15, v2}, LX/6f7;->a(Lcom/facebook/messaging/model/send/SendError;)LX/6f7;

    .line 404854
    invoke-virtual {v15, v3}, LX/6f7;->d(Ljava/util/List;)LX/6f7;

    .line 404855
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v15, v2}, LX/6f7;->a(Ljava/lang/Long;)LX/6f7;

    .line 404856
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v11, v13}, LX/2P0;->a(LX/6f7;Ljava/lang/String;[B)V

    .line 404857
    sget-object v2, LX/2P1;->p:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    .line 404858
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    .line 404859
    const-string v2, "TincanDbMessagesFetcher"

    const-string v3, "Returning an expired message, this should never happen!"

    invoke-static {v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 404860
    :cond_6
    invoke-virtual {v15}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    return-object v2

    .line 404861
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, LX/2P0;->c:LX/2NB;

    invoke-virtual {v15, v3}, LX/2NB;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    goto :goto_2

    :cond_8
    move-object v2, v10

    goto/16 :goto_1

    :cond_9
    move v3, v2

    goto/16 :goto_0
.end method

.method private a(LX/6f7;Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 404898
    if-eqz p3, :cond_0

    array-length v0, p3

    if-nez v0, :cond_1

    .line 404899
    :cond_0
    :goto_0
    return-void

    .line 404900
    :cond_1
    invoke-static {p3}, LX/Dpn;->b([B)LX/DpY;

    move-result-object v0

    .line 404901
    :try_start_0
    iget-object v1, p0, LX/2P0;->d:LX/2P3;

    invoke-virtual {v1, p1, p2, v0}, LX/2P3;->a(LX/6f7;Ljava/lang/String;LX/DpY;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 404902
    :catch_0
    move-exception v0

    .line 404903
    const-string v1, "TincanDbMessagesFetcher"

    const-string v2, "Retrieved Salamander decoded with invalid body"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a([B[B)[B
    .locals 3

    .prologue
    .line 404904
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 404905
    :cond_0
    const/4 v0, 0x0

    .line 404906
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    iget-object v0, p0, LX/2P0;->h:LX/2Oy;

    invoke-virtual {v0, p1, p2}, LX/2Oy;->b([B[B)[B
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 404907
    :catch_0
    move-exception v0

    .line 404908
    :goto_1
    const-string v1, "TincanDbMessagesFetcher"

    const-string v2, "Failed to decrypt message content"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 404909
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 404910
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method private static b(Ljava/util/Set;)Lcom/facebook/messaging/model/messages/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)",
            "Lcom/facebook/messaging/model/messages/Message;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 404911
    invoke-interface {p0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404912
    const/4 v0, 0x0

    .line 404913
    :goto_0
    return-object v0

    .line 404914
    :cond_0
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 404915
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    goto :goto_0

    .line 404916
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Should never return more than 1 message from DB."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static c()Ljava/lang/String;
    .locals 5

    .prologue
    .line 404728
    const-string v0, "%s LEFT OUTER JOIN %s ON (%s.%s = %s.%s)"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "messages"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "thread_participants"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "messages"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, LX/2P1;->e:LX/0U1;

    .line 404729
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 404730
    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "thread_participants"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, LX/2P2;->a:LX/0U1;

    .line 404731
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 404732
    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static d()LX/0ux;
    .locals 3

    .prologue
    .line 404733
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/2P1;->p:LX/0U1;

    .line 404734
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404735
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IS NOT 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0uu;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/2P0;Ljava/lang/String;)Landroid/util/Pair;
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "[B>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 404498
    sget-object v0, LX/2P1;->j:LX/0U1;

    .line 404499
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 404500
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 404501
    iget-object v0, p0, LX/2P0;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 404502
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 404503
    const-string v1, "messages"

    sget-object v2, LX/2P0;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 404504
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 404505
    invoke-static {v2}, LX/2P0;->a(Landroid/database/Cursor;)Landroid/util/Pair;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v0

    .line 404506
    if-eqz v2, :cond_0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 404507
    :cond_0
    if-eqz v8, :cond_1

    invoke-virtual {v8}, LX/2gJ;->close()V

    .line 404508
    :cond_1
    :goto_0
    return-object v0

    .line 404509
    :cond_2
    if-eqz v2, :cond_3

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 404510
    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_4
    move-object v0, v9

    .line 404511
    goto :goto_0

    .line 404512
    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 404513
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_1
    if-eqz v2, :cond_5

    if-eqz v1, :cond_7

    :try_start_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_5
    :goto_2
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 404514
    :catch_1
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 404515
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_3
    if-eqz v8, :cond_6

    if-eqz v9, :cond_8

    :try_start_8
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    :cond_6
    :goto_4
    throw v0

    .line 404516
    :catch_2
    move-exception v2

    :try_start_9
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 404517
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 404518
    :cond_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_2

    .line 404519
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_8
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_4

    .line 404520
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method

.method public static f(LX/2P0;Ljava/lang/String;)Landroid/util/Pair;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "[B>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 404521
    iget-object v0, p0, LX/2P0;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;

    .line 404522
    :try_start_0
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 404523
    sget-object v3, LX/2P1;->j:LX/0U1;

    .line 404524
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 404525
    invoke-static {v3, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v3

    .line 404526
    invoke-static {v1, v3}, LX/2P0;->a(Landroid/database/sqlite/SQLiteDatabase;LX/0ux;)Landroid/database/Cursor;

    move-result-object v3

    move-object v4, v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 404527
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 404528
    invoke-static {v4}, LX/2P0;->a(Landroid/database/Cursor;)Landroid/util/Pair;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v1

    .line 404529
    if-eqz v4, :cond_0

    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 404530
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/2gJ;->close()V

    :cond_1
    move-object v0, v1

    :goto_0
    return-object v0

    .line 404531
    :cond_2
    if-eqz v4, :cond_3

    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 404532
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, LX/2gJ;->close()V

    :cond_4
    move-object v0, v2

    goto :goto_0

    .line 404533
    :catch_0
    move-exception v1

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 404534
    :catchall_0
    move-exception v3

    move-object v5, v3

    move-object v3, v1

    move-object v1, v5

    :goto_1
    if-eqz v4, :cond_5

    if-eqz v3, :cond_7

    :try_start_5
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_5
    :goto_2
    :try_start_6
    throw v1
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 404535
    :catch_1
    move-exception v1

    :try_start_7
    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 404536
    :catchall_1
    move-exception v2

    move-object v5, v2

    move-object v2, v1

    move-object v1, v5

    :goto_3
    if-eqz v0, :cond_6

    if-eqz v2, :cond_8

    :try_start_8
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    :cond_6
    :goto_4
    throw v1

    .line 404537
    :catch_2
    move-exception v4

    :try_start_9
    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 404538
    :catchall_2
    move-exception v1

    goto :goto_3

    .line 404539
    :cond_7
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_2

    .line 404540
    :catch_3
    move-exception v0

    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_8
    invoke-virtual {v0}, LX/2gJ;->close()V

    goto :goto_4

    .line 404541
    :catchall_3
    move-exception v1

    move-object v3, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)LX/0Px;
    .locals 17
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 404542
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v13

    .line 404543
    sget-object v2, LX/2P1;->b:LX/0U1;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v6

    .line 404544
    sget-object v2, LX/2P1;->f:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v2

    .line 404545
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " LIMIT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 404546
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2P0;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Dom;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/Dom;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)[B

    move-result-object v14

    .line 404547
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2P0;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, LX/2gJ;

    const/4 v12, 0x0

    .line 404548
    :try_start_0
    invoke-virtual {v10}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 404549
    const-string v3, "messages"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v7, LX/2P1;->e:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x1

    sget-object v7, LX/2P1;->f:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x2

    sget-object v7, LX/2P1;->c:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x3

    sget-object v7, LX/2P1;->d:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-virtual {v6}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v15

    const/4 v11, 0x0

    .line 404550
    :goto_0
    :try_start_1
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 404551
    sget-object v2, LX/2P1;->e:LX/0U1;

    invoke-virtual {v2, v15}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v4

    .line 404552
    sget-object v2, LX/2P1;->f:LX/0U1;

    invoke-virtual {v2, v15}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v6

    .line 404553
    sget-object v2, LX/2P1;->c:LX/0U1;

    invoke-virtual {v2, v15}, LX/0U1;->f(Landroid/database/Cursor;)[B

    move-result-object v2

    .line 404554
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v2}, LX/2P0;->a([B[B)[B

    move-result-object v8

    .line 404555
    sget-object v2, LX/2P1;->d:LX/0U1;

    invoke-virtual {v2, v15}, LX/0U1;->f(Landroid/database/Cursor;)[B

    move-result-object v9

    .line 404556
    new-instance v3, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/messaging/tincan/database/RawTincanMessageContent;-><init>(JJ[B[B)V

    .line 404557
    invoke-virtual {v13, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_0

    .line 404558
    :catch_0
    move-exception v2

    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 404559
    :catchall_0
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    :goto_1
    if-eqz v15, :cond_0

    if-eqz v3, :cond_5

    :try_start_3
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_0
    :goto_2
    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 404560
    :catch_1
    move-exception v2

    :try_start_5
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 404561
    :catchall_1
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    :goto_3
    if-eqz v10, :cond_1

    if-eqz v3, :cond_6

    :try_start_6
    invoke-virtual {v10}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    :cond_1
    :goto_4
    throw v2

    .line 404562
    :cond_2
    if-eqz v15, :cond_3

    :try_start_7
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 404563
    :cond_3
    if-eqz v10, :cond_4

    invoke-virtual {v10}, LX/2gJ;->close()V

    .line 404564
    :cond_4
    invoke-virtual {v13}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    return-object v2

    .line 404565
    :catch_2
    move-exception v4

    :try_start_8
    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 404566
    :catchall_2
    move-exception v2

    move-object v3, v12

    goto :goto_3

    .line 404567
    :cond_5
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 404568
    :catch_3
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v10}, LX/2gJ;->close()V

    goto :goto_4

    .line 404569
    :catchall_3
    move-exception v2

    move-object v3, v11

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JLjava/lang/String;)LX/0Rf;
    .locals 4
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "J",
            "Ljava/lang/String;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 404570
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v0

    .line 404571
    sget-object v1, LX/2P1;->b:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404572
    sget-object v1, LX/2P1;->l:LX/0U1;

    const-string v2, "0"

    invoke-virtual {v1, v2}, LX/0U1;->d(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404573
    sget-object v1, LX/2P1;->l:LX/0U1;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404574
    if-eqz p4, :cond_0

    .line 404575
    sget-object v1, LX/2P1;->e:LX/0U1;

    .line 404576
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404577
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p4, v2, v3

    invoke-static {v1, v2}, LX/0uu;->b(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404578
    :cond_0
    invoke-static {p0, v0}, LX/2P0;->a(LX/2P0;LX/0ux;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    .locals 13
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 404698
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 404699
    sget-object v0, LX/2P1;->b:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404700
    sget-object v0, LX/2P1;->i:LX/0U1;

    sget-object v1, LX/2uW;->PENDING_SEND:LX/2uW;

    iget v1, v1, LX/2uW;->dbKeyValue:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404701
    invoke-static {}, LX/2P0;->d()LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404702
    invoke-static {}, LX/2P0;->c()Ljava/lang/String;

    move-result-object v1

    .line 404703
    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V

    .line 404704
    new-instance v11, LX/0Pz;

    invoke-direct {v11}, LX/0Pz;-><init>()V

    .line 404705
    iget-object v0, p0, LX/2P0;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 404706
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 404707
    sget-object v2, LX/2P0;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 404708
    :goto_0
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 404709
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 404710
    invoke-static {v2, v0}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 404711
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_0

    .line 404712
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 404713
    :catchall_0
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    :goto_1
    if-eqz v2, :cond_0

    if-eqz v1, :cond_5

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_0
    :goto_2
    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 404714
    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 404715
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_3
    if-eqz v8, :cond_1

    if-eqz v9, :cond_6

    :try_start_6
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    :cond_1
    :goto_4
    throw v0

    .line 404716
    :cond_2
    if-eqz v2, :cond_3

    :try_start_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 404717
    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, LX/2gJ;->close()V

    .line 404718
    :cond_4
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 404719
    invoke-direct {p0, v0}, LX/2P0;->a(Landroid/content/ContentValues;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 404720
    invoke-virtual {v11, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 404721
    :catch_2
    move-exception v2

    :try_start_8
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 404722
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 404723
    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 404724
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_4

    .line 404725
    :cond_7
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 404726
    new-instance v1, Lcom/facebook/messaging/model/messages/MessagesCollection;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v0, v2}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    return-object v1

    .line 404727
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JI)Lcom/facebook/messaging/model/messages/MessagesCollection;
    .locals 16
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 404579
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v6

    .line 404580
    sget-object v2, LX/2P1;->b:LX/0U1;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404581
    invoke-static {}, LX/2P0;->d()LX/0ux;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404582
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-lez v2, :cond_0

    .line 404583
    sget-object v2, LX/2P1;->f:LX/0U1;

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0U1;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404584
    :cond_0
    sget-object v2, LX/2P1;->f:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v9

    .line 404585
    if-lez p4, :cond_1

    .line 404586
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " LIMIT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 404587
    :cond_1
    invoke-static {}, LX/2P0;->c()Ljava/lang/String;

    move-result-object v3

    .line 404588
    new-instance v12, Ljava/util/LinkedList;

    invoke-direct {v12}, Ljava/util/LinkedList;-><init>()V

    .line 404589
    new-instance v13, LX/0Pz;

    invoke-direct {v13}, LX/0Pz;-><init>()V

    .line 404590
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2P0;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, LX/2gJ;

    const/4 v11, 0x0

    .line 404591
    :try_start_0
    invoke-virtual {v10}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 404592
    sget-object v4, LX/2P0;->a:[Ljava/lang/String;

    invoke-virtual {v6}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v4

    const/4 v3, 0x0

    .line 404593
    :goto_0
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 404594
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 404595
    invoke-static {v4, v2}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 404596
    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_0

    .line 404597
    :catch_0
    move-exception v2

    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 404598
    :catchall_0
    move-exception v3

    move-object v14, v3

    move-object v3, v2

    move-object v2, v14

    :goto_1
    if-eqz v4, :cond_2

    if-eqz v3, :cond_7

    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_2
    :goto_2
    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 404599
    :catch_1
    move-exception v2

    :try_start_5
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 404600
    :catchall_1
    move-exception v3

    move-object v14, v3

    move-object v3, v2

    move-object v2, v14

    :goto_3
    if-eqz v10, :cond_3

    if-eqz v3, :cond_8

    :try_start_6
    invoke-virtual {v10}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    :cond_3
    :goto_4
    throw v2

    .line 404601
    :cond_4
    if-eqz v4, :cond_5

    :try_start_7
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 404602
    :cond_5
    if-eqz v10, :cond_6

    invoke-virtual {v10}, LX/2gJ;->close()V

    .line 404603
    :cond_6
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    .line 404604
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/2P0;->a(Landroid/content/ContentValues;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 404605
    invoke-virtual {v13, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 404606
    :catch_2
    move-exception v4

    :try_start_8
    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 404607
    :catchall_2
    move-exception v2

    move-object v3, v11

    goto :goto_3

    .line 404608
    :cond_7
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 404609
    :catch_3
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_8
    invoke-virtual {v10}, LX/2gJ;->close()V

    goto :goto_4

    .line 404610
    :cond_9
    invoke-virtual {v13}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 404611
    if-lez p4, :cond_a

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v2

    move/from16 v0, p4

    if-ge v2, v0, :cond_b

    :cond_a
    const/4 v2, 0x1

    .line 404612
    :goto_6
    new-instance v4, Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v3, v2}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    return-object v4

    .line 404613
    :cond_b
    const/4 v2, 0x0

    goto :goto_6

    .line 404614
    :catchall_3
    move-exception v2

    goto :goto_1
.end method

.method public final a(LX/0ux;LX/0QK;)V
    .locals 12
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ux;",
            "LX/0QK",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 404615
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 404616
    invoke-virtual {v4, p1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404617
    invoke-static {}, LX/2P0;->d()LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 404618
    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V

    .line 404619
    invoke-static {}, LX/2P0;->c()Ljava/lang/String;

    move-result-object v1

    .line 404620
    iget-object v0, p0, LX/2P0;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 404621
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 404622
    sget-object v2, LX/2P0;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 404623
    :goto_0
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 404624
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 404625
    invoke-static {v2, v0}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 404626
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_0

    .line 404627
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 404628
    :catchall_0
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_1
    if-eqz v2, :cond_0

    if-eqz v1, :cond_5

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_0
    :goto_2
    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 404629
    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 404630
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_3
    if-eqz v8, :cond_1

    if-eqz v9, :cond_6

    :try_start_6
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    :cond_1
    :goto_4
    throw v0

    .line 404631
    :cond_2
    if-eqz v2, :cond_3

    :try_start_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 404632
    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, LX/2gJ;->close()V

    .line 404633
    :cond_4
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 404634
    invoke-direct {p0, v0}, LX/2P0;->a(Landroid/content/ContentValues;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 404635
    invoke-interface {p2, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 404636
    :catch_2
    move-exception v2

    :try_start_8
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 404637
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 404638
    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 404639
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_4

    .line 404640
    :cond_7
    return-void

    .line 404641
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method

.method public final a()Z
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 404642
    sget-object v0, LX/2P1;->p:LX/0U1;

    const-string v1, "1"

    invoke-virtual {v0, v1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 404643
    iget-object v0, p0, LX/2P0;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 404644
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 404645
    const-string v1, "messages"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v5, LX/2P1;->a:LX/0U1;

    .line 404646
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 404647
    aput-object v5, v2, v3

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 404648
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result v0

    .line 404649
    if-eqz v2, :cond_0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 404650
    :cond_0
    if-eqz v8, :cond_1

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_1
    return v0

    .line 404651
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 404652
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v2, :cond_2

    if-eqz v1, :cond_4

    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_2
    :goto_1
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 404653
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 404654
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_2
    if-eqz v8, :cond_3

    if-eqz v9, :cond_5

    :try_start_7
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :cond_3
    :goto_3
    throw v0

    .line 404655
    :catch_2
    move-exception v2

    :try_start_8
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 404656
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 404657
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_1

    .line 404658
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_3

    .line 404659
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 404660
    sget-object v0, LX/2P1;->j:LX/0U1;

    .line 404661
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 404662
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 404663
    invoke-static {p0, v0}, LX/2P0;->a(LX/2P0;LX/0ux;)LX/0Rf;

    move-result-object v0

    .line 404664
    invoke-static {v0}, LX/2P0;->b(Ljava/util/Set;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 404665
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 404666
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 404667
    iget-object v0, p0, LX/2P0;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;

    .line 404668
    :try_start_0
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 404669
    sget-object v5, LX/2P1;->k:LX/0U1;

    .line 404670
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 404671
    invoke-static {v5}, LX/0uu;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v5

    .line 404672
    invoke-static {v2, v5}, LX/2P0;->a(Landroid/database/sqlite/SQLiteDatabase;LX/0ux;)Landroid/database/Cursor;

    move-result-object v5

    move-object v5, v5
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 404673
    :goto_0
    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 404674
    invoke-static {v5}, LX/2P0;->a(Landroid/database/Cursor;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_0

    .line 404675
    :catch_0
    move-exception v1

    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 404676
    :catchall_0
    move-exception v2

    move-object v6, v2

    move-object v2, v1

    move-object v1, v6

    :goto_1
    if-eqz v5, :cond_0

    if-eqz v2, :cond_6

    :try_start_3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_0
    :goto_2
    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 404677
    :catch_1
    move-exception v1

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 404678
    :catchall_1
    move-exception v2

    move-object v3, v1

    move-object v1, v2

    :goto_3
    if-eqz v0, :cond_1

    if-eqz v3, :cond_7

    :try_start_6
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    :cond_1
    :goto_4
    throw v1

    .line 404679
    :cond_2
    if-eqz v5, :cond_3

    :try_start_7
    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 404680
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, LX/2gJ;->close()V

    .line 404681
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 404682
    iget-object v1, p0, LX/2P0;->i:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dom;

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v2}, LX/Dom;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)[B

    move-result-object v1

    .line 404683
    iget-object v2, p0, LX/2P0;->h:LX/2Oy;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [B

    invoke-virtual {v2, v1, v0}, LX/2Oy;->c([B[B)Ljava/lang/String;

    move-result-object v0

    .line 404684
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 404685
    iget-object v1, p0, LX/2P0;->c:LX/2NB;

    invoke-virtual {v1, v0}, LX/2NB;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    .line 404686
    :catch_2
    move-exception v4

    :try_start_8
    invoke-static {v2, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 404687
    :catchall_2
    move-exception v1

    goto :goto_3

    .line 404688
    :cond_6
    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 404689
    :catch_3
    move-exception v0

    invoke-static {v3, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_7
    invoke-virtual {v0}, LX/2gJ;->close()V

    goto :goto_4

    .line 404690
    :cond_8
    return-object v4

    .line 404691
    :catchall_3
    move-exception v1

    move-object v2, v3

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 404692
    invoke-static {p1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    .line 404693
    sget-object v1, LX/2P1;->a:LX/0U1;

    .line 404694
    iget-object p1, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, p1

    .line 404695
    invoke-static {v1, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v1

    .line 404696
    invoke-static {p0, v1}, LX/2P0;->a(LX/2P0;LX/0ux;)LX/0Rf;

    move-result-object v1

    move-object v0, v1

    .line 404697
    invoke-static {v0}, LX/2P0;->b(Ljava/util/Set;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    return-object v0
.end method
