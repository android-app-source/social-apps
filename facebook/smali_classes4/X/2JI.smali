.class public LX/2JI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/30V;


# instance fields
.field private final b:LX/0sT;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/4VO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0sT;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sT;",
            "LX/0Or",
            "<",
            "LX/4VO;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392759
    iput-object p1, p0, LX/2JI;->b:LX/0sT;

    .line 392760
    iput-object p2, p0, LX/2JI;->c:LX/0Or;

    .line 392761
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 392766
    const/4 v0, 0x1

    return v0
.end method

.method public final b()LX/2Jm;
    .locals 1

    .prologue
    .line 392765
    sget-object v0, LX/2Jm;->STATE_CHANGE:LX/2Jm;

    return-object v0
.end method

.method public final c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<+",
            "LX/2Jw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392764
    iget-object v0, p0, LX/2JI;->c:LX/0Or;

    return-object v0
.end method

.method public final d()LX/2Jl;
    .locals 2

    .prologue
    .line 392763
    new-instance v0, LX/2Jk;

    invoke-direct {v0}, LX/2Jk;-><init>()V

    sget-object v1, LX/2Jh;->BACKGROUND:LX/2Jh;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jh;)LX/2Jk;

    move-result-object v0

    invoke-virtual {v0}, LX/2Jk;->a()LX/2Jl;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 392762
    const-wide/32 v0, 0x2932e00

    return-wide v0
.end method
