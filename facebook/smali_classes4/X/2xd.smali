.class public LX/2xd;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/2xi;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 478829
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    .line 478830
    new-instance v0, LX/2xe;

    invoke-direct {v0}, LX/2xe;-><init>()V

    sput-object v0, LX/2xd;->a:LX/2xi;

    .line 478831
    :goto_0
    return-void

    .line 478832
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    .line 478833
    new-instance v0, LX/2xf;

    invoke-direct {v0}, LX/2xf;-><init>()V

    sput-object v0, LX/2xd;->a:LX/2xi;

    goto :goto_0

    .line 478834
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_2

    .line 478835
    new-instance v0, LX/2xg;

    invoke-direct {v0}, LX/2xg;-><init>()V

    sput-object v0, LX/2xd;->a:LX/2xi;

    goto :goto_0

    .line 478836
    :cond_2
    new-instance v0, LX/2xh;

    invoke-direct {v0}, LX/2xh;-><init>()V

    sput-object v0, LX/2xd;->a:LX/2xi;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 478837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478838
    return-void
.end method

.method public static a(Landroid/view/MotionEvent;)I
    .locals 1

    .prologue
    .line 478840
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public static a(Landroid/view/MotionEvent;I)I
    .locals 1

    .prologue
    .line 478839
    sget-object v0, LX/2xd;->a:LX/2xi;

    invoke-interface {v0, p0, p1}, LX/2xi;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/MotionEvent;)I
    .locals 2

    .prologue
    .line 478827
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v1, 0xff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    return v0
.end method

.method public static b(Landroid/view/MotionEvent;I)I
    .locals 1

    .prologue
    .line 478828
    sget-object v0, LX/2xd;->a:LX/2xi;

    invoke-interface {v0, p0, p1}, LX/2xi;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    return v0
.end method

.method public static c(Landroid/view/MotionEvent;I)F
    .locals 1

    .prologue
    .line 478826
    sget-object v0, LX/2xd;->a:LX/2xi;

    invoke-interface {v0, p0, p1}, LX/2xi;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    return v0
.end method

.method public static c(Landroid/view/MotionEvent;)I
    .locals 1

    .prologue
    .line 478825
    sget-object v0, LX/2xd;->a:LX/2xi;

    invoke-interface {v0, p0}, LX/2xi;->a(Landroid/view/MotionEvent;)I

    move-result v0

    return v0
.end method

.method public static d(Landroid/view/MotionEvent;I)F
    .locals 1

    .prologue
    .line 478824
    sget-object v0, LX/2xd;->a:LX/2xi;

    invoke-interface {v0, p0, p1}, LX/2xi;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    return v0
.end method

.method public static d(Landroid/view/MotionEvent;)I
    .locals 1

    .prologue
    .line 478823
    sget-object v0, LX/2xd;->a:LX/2xi;

    invoke-interface {v0, p0}, LX/2xi;->b(Landroid/view/MotionEvent;)I

    move-result v0

    return v0
.end method

.method public static e(Landroid/view/MotionEvent;I)F
    .locals 1

    .prologue
    .line 478822
    sget-object v0, LX/2xd;->a:LX/2xi;

    invoke-interface {v0, p0, p1}, LX/2xi;->e(Landroid/view/MotionEvent;I)F

    move-result v0

    return v0
.end method
