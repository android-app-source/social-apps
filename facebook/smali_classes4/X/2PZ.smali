.class public LX/2PZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;
.implements LX/2Pa;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:J

.field private static volatile l:LX/2PZ;


# instance fields
.field private final b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field private final c:LX/0W3;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/offlinemode/boostedcomponent/CanHandleOfflineLwiMutation;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:LX/2Pb;

.field private final f:LX/03V;

.field private final g:LX/0lB;

.field public final h:LX/13Q;

.field private final i:LX/2L0;

.field public final j:LX/2Pc;

.field private final k:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 406724
    sget-wide v0, LX/0X5;->l:J

    sput-wide v0, LX/2PZ;->a:J

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0W3;LX/2Pb;LX/03V;LX/0lB;LX/13Q;LX/2L0;LX/2Pc;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406726
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/2PZ;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 406727
    iput-object p1, p0, LX/2PZ;->b:Ljava/util/concurrent/ExecutorService;

    .line 406728
    iput-object p2, p0, LX/2PZ;->c:LX/0W3;

    .line 406729
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2PZ;->d:Ljava/util/List;

    .line 406730
    iput-object p3, p0, LX/2PZ;->e:LX/2Pb;

    .line 406731
    iput-object p4, p0, LX/2PZ;->f:LX/03V;

    .line 406732
    iput-object p5, p0, LX/2PZ;->g:LX/0lB;

    .line 406733
    iput-object p6, p0, LX/2PZ;->h:LX/13Q;

    .line 406734
    iput-object p7, p0, LX/2PZ;->i:LX/2L0;

    .line 406735
    iput-object p8, p0, LX/2PZ;->j:LX/2Pc;

    .line 406736
    return-void
.end method

.method public static a(LX/0QB;)LX/2PZ;
    .locals 12

    .prologue
    .line 406744
    sget-object v0, LX/2PZ;->l:LX/2PZ;

    if-nez v0, :cond_1

    .line 406745
    const-class v1, LX/2PZ;

    monitor-enter v1

    .line 406746
    :try_start_0
    sget-object v0, LX/2PZ;->l:LX/2PZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406747
    if-eqz v2, :cond_0

    .line 406748
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 406749
    new-instance v3, LX/2PZ;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-static {v0}, LX/2Pb;->a(LX/0QB;)LX/2Pb;

    move-result-object v6

    check-cast v6, LX/2Pb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v8

    check-cast v8, LX/0lB;

    invoke-static {v0}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v9

    check-cast v9, LX/13Q;

    invoke-static {v0}, LX/2L0;->a(LX/0QB;)LX/2L0;

    move-result-object v10

    check-cast v10, LX/2L0;

    invoke-static {v0}, LX/2Pc;->a(LX/0QB;)LX/2Pc;

    move-result-object v11

    check-cast v11, LX/2Pc;

    invoke-direct/range {v3 .. v11}, LX/2PZ;-><init>(Ljava/util/concurrent/ExecutorService;LX/0W3;LX/2Pb;LX/03V;LX/0lB;LX/13Q;LX/2L0;LX/2Pc;)V

    .line 406750
    move-object v0, v3

    .line 406751
    sput-object v0, LX/2PZ;->l:LX/2PZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406752
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406753
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406754
    :cond_1
    sget-object v0, LX/2PZ;->l:LX/2PZ;

    return-object v0

    .line 406755
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406756
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2PZ;[BLX/8wW;)V
    .locals 10

    .prologue
    .line 406737
    :try_start_0
    iget-object v0, p0, LX/2PZ;->g:LX/0lB;

    const-class v1, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;

    invoke-virtual {v0, p1, v1}, LX/0lC;->a([BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 406738
    iget-object v1, v0, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->objective:LX/8wL;

    sget-object v2, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v1, v2, :cond_0

    .line 406739
    :goto_0
    return-void

    .line 406740
    :cond_0
    iget-object v1, p0, LX/2PZ;->e:LX/2Pb;

    sget-object v2, LX/8wV;->BOOSTED_POST_MOBILE_MODULE:LX/8wV;

    iget-object v3, p0, LX/2PZ;->f:LX/03V;

    .line 406741
    const/4 v8, 0x0

    move-object v4, v1

    move-object v5, v2

    move-object v6, p2

    move-object v7, v0

    move-object v9, v3

    invoke-static/range {v4 .. v9}, LX/2Pb;->a(LX/2Pb;LX/8wV;LX/8wW;Lcom/facebook/adinterfaces/external/AdInterfacesEventData;Ljava/util/Map;LX/03V;)V

    .line 406742
    goto :goto_0

    .line 406743
    :catch_0
    goto :goto_0
.end method

.method public static declared-synchronized b$redex0(LX/2PZ;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 406699
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/2PZ;->c(LX/2PZ;Ljava/lang/String;)LX/CSG;

    move-result-object v2

    .line 406700
    iget-object v0, p0, LX/2PZ;->j:LX/2Pc;

    invoke-virtual {v0, p1}, LX/2Pc;->a(Ljava/lang/String;)V

    .line 406701
    if-nez v2, :cond_1

    .line 406702
    iget-object v0, p0, LX/2PZ;->h:LX/13Q;

    const-string v1, "spotty_ads_offline_mutation_failure_no_key_record"

    invoke-virtual {v0, v1}, LX/13Q;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 406703
    :cond_0
    monitor-exit p0

    return-void

    .line 406704
    :cond_1
    :try_start_1
    iget-object v0, v2, LX/CSG;->d:[B

    sget-object v1, LX/8wW;->SUBMIT_FLOW_ERROR:LX/8wW;

    invoke-static {p0, v0, v1}, LX/2PZ;->a$redex0(LX/2PZ;[BLX/8wW;)V

    .line 406705
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 406706
    iget-object v1, p0, LX/2PZ;->d:Ljava/util/List;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 406707
    :try_start_2
    iget-object v0, p0, LX/2PZ;->d:Ljava/util/List;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 406708
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 406709
    :try_start_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 406710
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bqu;

    .line 406711
    if-eqz v0, :cond_2

    .line 406712
    const/4 p1, 0x0

    invoke-static {v0, v2, p1}, LX/Bqu;->a(LX/Bqu;LX/CSG;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 406713
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 406714
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 406715
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static c(LX/2PZ;Ljava/lang/String;)LX/CSG;
    .locals 4

    .prologue
    .line 406716
    iget-object v0, p0, LX/2PZ;->j:LX/2Pc;

    invoke-virtual {v0, p1}, LX/2Pc;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 406717
    if-nez v1, :cond_0

    .line 406718
    const/4 v0, 0x0

    .line 406719
    :goto_0
    return-object v0

    .line 406720
    :cond_0
    iget-object v0, p0, LX/2PZ;->j:LX/2Pc;

    .line 406721
    iget-object v2, v0, LX/2Pc;->c:LX/2Pi;

    invoke-virtual {v2, p1}, LX/2Pi;->e(Ljava/lang/String;)[B

    move-result-object v2

    move-object v2, v2

    .line 406722
    iget-object v0, p0, LX/2PZ;->j:LX/2Pc;

    invoke-virtual {v0, p1}, LX/2Pc;->e(Ljava/lang/String;)Z

    move-result v3

    .line 406723
    new-instance v0, LX/CSG;

    invoke-direct {v0, p1, v1, v3, v2}, LX/CSG;-><init>(Ljava/lang/String;Ljava/lang/String;Z[B)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 406698
    iget-object v0, p0, LX/2PZ;->b:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public final declared-synchronized a(LX/Bqu;)V
    .locals 6

    .prologue
    .line 406681
    monitor-enter p0

    if-nez p1, :cond_1

    .line 406682
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 406683
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/2PZ;->d:Ljava/util/List;

    const/4 v1, 0x0

    .line 406684
    if-eqz v0, :cond_4

    move v2, v1

    move v3, v1

    .line 406685
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 406686
    iget-object v1, p0, LX/2PZ;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 406687
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 406688
    iget-object v5, p0, LX/2PZ;->d:Ljava/util/List;

    add-int/lit8 v4, v3, 0x1

    invoke-interface {v5, v3, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v3, v4

    .line 406689
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 406690
    :cond_3
    iget-object v1, p0, LX/2PZ;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_2
    if-lt v1, v3, :cond_4

    .line 406691
    iget-object v2, p0, LX/2PZ;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 406692
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 406693
    :cond_4
    const/4 v0, 0x0

    iget-object v1, p0, LX/2PZ;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_5

    .line 406694
    iget-object v0, p0, LX/2PZ;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 406695
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 406696
    :cond_5
    iget-object v0, p0, LX/2PZ;->d:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 406697
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 406665
    invoke-static {p0, p1}, LX/2PZ;->c(LX/2PZ;Ljava/lang/String;)LX/CSG;

    move-result-object v2

    .line 406666
    iget-object v0, p0, LX/2PZ;->j:LX/2Pc;

    invoke-virtual {v0, p1}, LX/2Pc;->a(Ljava/lang/String;)V

    .line 406667
    if-nez v2, :cond_1

    .line 406668
    iget-object v0, p0, LX/2PZ;->h:LX/13Q;

    const-string v1, "spotty_ads_online_mutation_success_no_key_record"

    invoke-virtual {v0, v1}, LX/13Q;->a(Ljava/lang/String;)V

    .line 406669
    :cond_0
    return-void

    .line 406670
    :cond_1
    iget-object v0, v2, LX/CSG;->d:[B

    sget-object v1, LX/8wW;->SUBMIT_FLOW:LX/8wW;

    invoke-static {p0, v0, v1}, LX/2PZ;->a$redex0(LX/2PZ;[BLX/8wW;)V

    .line 406671
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 406672
    iget-object v1, p0, LX/2PZ;->d:Ljava/util/List;

    monitor-enter v1

    .line 406673
    :try_start_0
    iget-object v0, p0, LX/2PZ;->d:Ljava/util/List;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 406674
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406675
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 406676
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bqu;

    .line 406677
    if-eqz v0, :cond_2

    .line 406678
    const/4 p0, 0x1

    invoke-static {v0, v2, p0}, LX/Bqu;->a(LX/Bqu;LX/CSG;Z)V

    .line 406679
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 406680
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/3G4;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 406664
    iget-object v1, p1, LX/3G4;->h:Ljava/lang/Class;

    const-class v2, LX/AAf;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/2PZ;->c:LX/0W3;

    sget-wide v2, LX/2PZ;->a:J

    invoke-interface {v1, v2, v3, v0}, LX/0W4;->a(JZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b(LX/3G4;)LX/0TF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3G4;",
            ")",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 406662
    iget-object v0, p1, LX/3G3;->b:Ljava/lang/String;

    .line 406663
    new-instance v1, LX/CSI;

    invoke-direct {v1, p0, v0}, LX/CSI;-><init>(LX/2PZ;Ljava/lang/String;)V

    return-object v1
.end method

.method public final declared-synchronized b(LX/Bqu;)V
    .locals 3

    .prologue
    .line 406655
    monitor-enter p0

    if-nez p1, :cond_1

    .line 406656
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 406657
    :cond_1
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, LX/2PZ;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    .line 406658
    iget-object v0, p0, LX/2PZ;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_2

    .line 406659
    iget-object v0, p0, LX/2PZ;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 406660
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 406661
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 406652
    iget-object v0, p0, LX/2PZ;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406653
    iget-object v0, p0, LX/2PZ;->i:LX/2L0;

    new-instance v1, LX/2Pj;

    invoke-direct {v1, p0}, LX/2Pj;-><init>(LX/2PZ;)V

    invoke-virtual {v0, v1}, LX/2L0;->a(LX/2LB;)V

    .line 406654
    :cond_0
    return-void
.end method
