.class public LX/2xP;
.super LX/2wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/os/IInterface;",
        ">",
        "LX/2wH",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final d:LX/3KQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3KQ",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;ILX/1qf;LX/1qg;LX/2wA;LX/3KQ;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Looper;",
            "I",
            "LX/1qf;",
            "LX/1qg;",
            "LX/2wA;",
            "LX/3KQ",
            "<TT;>;)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p6

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/2wH;-><init>(Landroid/content/Context;Landroid/os/Looper;ILX/2wA;LX/1qf;LX/1qg;)V

    iput-object p7, p0, LX/2xP;->d:LX/3KQ;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            ")TT;"
        }
    .end annotation

    iget-object v0, p0, LX/2xP;->d:LX/3KQ;

    invoke-interface {v0}, LX/3KQ;->c()Landroid/os/IInterface;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LX/2xP;->d:LX/3KQ;

    invoke-interface {v0}, LX/3KQ;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LX/2xP;->d:LX/3KQ;

    invoke-interface {v0}, LX/3KQ;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
