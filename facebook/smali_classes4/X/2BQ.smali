.class public LX/2BQ;
.super LX/0Tr;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2BQ;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/2BT;LX/2BU;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p2    # LX/0Tt;
        .annotation runtime Lcom/facebook/database/threadchecker/AllowAnyThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 379093
    invoke-static {p3, p4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    const-string v1, "legacy_key_value_db"

    invoke-direct {p0, p1, p2, v0, v1}, LX/0Tr;-><init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V

    .line 379094
    return-void
.end method

.method public static a(LX/0QB;)LX/2BQ;
    .locals 7

    .prologue
    .line 379095
    sget-object v0, LX/2BQ;->a:LX/2BQ;

    if-nez v0, :cond_1

    .line 379096
    const-class v1, LX/2BQ;

    monitor-enter v1

    .line 379097
    :try_start_0
    sget-object v0, LX/2BQ;->a:LX/2BQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 379098
    if-eqz v2, :cond_0

    .line 379099
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 379100
    new-instance p0, LX/2BQ;

    const-class v3, Landroid/content/Context;

    const-class v4, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, v4}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0sm;->a(LX/0QB;)LX/0Tt;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/2BT;->a(LX/0QB;)LX/2BT;

    move-result-object v5

    check-cast v5, LX/2BT;

    invoke-static {v0}, LX/2BU;->a(LX/0QB;)LX/2BU;

    move-result-object v6

    check-cast v6, LX/2BU;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2BQ;-><init>(Landroid/content/Context;LX/0Tt;LX/2BT;LX/2BU;)V

    .line 379101
    move-object v0, p0

    .line 379102
    sput-object v0, LX/2BQ;->a:LX/2BQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 379103
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 379104
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 379105
    :cond_1
    sget-object v0, LX/2BQ;->a:LX/2BQ;

    return-object v0

    .line 379106
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 379107
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
