.class public final LX/2s7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 0

    .prologue
    .line 472341
    iput-object p1, p0, LX/2s7;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x3158ae40

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 472342
    const-wide/16 v2, 0x3e8

    iget-object v0, p0, LX/2s7;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->J:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->l()J

    move-result-wide v4

    mul-long/2addr v2, v4

    .line 472343
    iget-object v0, p0, LX/2s7;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ar:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2s7;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->n:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    iget-object v0, p0, LX/2s7;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ar:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    cmp-long v0, v4, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 472344
    :goto_0
    if-eqz v0, :cond_0

    .line 472345
    iget-object v0, p0, LX/2s7;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    iget-object v2, p0, LX/2s7;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v2, v2, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aq:Ljava/util/List;

    iget-object v3, p0, LX/2s7;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static {v3}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->K(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/3Te;->a(Ljava/util/List;Z)V

    .line 472346
    :cond_0
    iget-object v0, p0, LX/2s7;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    .line 472347
    iput-object v2, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ar:LX/0am;

    .line 472348
    const v0, -0x7895903d

    invoke-static {v0, v1}, LX/02F;->e(II)V

    return-void

    .line 472349
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
