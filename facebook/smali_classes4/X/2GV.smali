.class public LX/2GV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2GW;

.field public b:LX/2GX;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private c:LX/2GW;

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1iq;",
            ">;"
        }
    .end annotation
.end field

.field private f:I


# direct methods
.method public constructor <init>(LX/13S;LX/0W3;Ljava/util/Random;)V
    .locals 1
    .param p3    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param

    .prologue
    .line 388737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388738
    new-instance v0, LX/2GW;

    invoke-direct {v0}, LX/2GW;-><init>()V

    iput-object v0, p0, LX/2GV;->a:LX/2GW;

    .line 388739
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2GV;->e:Ljava/util/Set;

    .line 388740
    const/4 v0, -0x1

    iput v0, p0, LX/2GV;->f:I

    .line 388741
    new-instance v0, LX/2GX;

    invoke-direct {v0, p2, p3}, LX/2GX;-><init>(LX/0W3;Ljava/util/Random;)V

    iput-object v0, p0, LX/2GV;->b:LX/2GX;

    .line 388742
    const-string v0, "localstats_histogram_container"

    const-string p2, "histogram_container_data"

    new-instance p3, LX/2GY;

    invoke-direct {p3, p0}, LX/2GY;-><init>(LX/2GV;)V

    invoke-virtual {p1, v0, p2, p3}, LX/13S;->a(Ljava/lang/String;Ljava/lang/String;LX/13W;)LX/13S;

    .line 388743
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 388744
    iget-object v1, p0, LX/2GV;->e:Ljava/util/Set;

    monitor-enter v1

    .line 388745
    :try_start_0
    iget-object v0, p0, LX/2GV;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iq;

    .line 388746
    invoke-interface {v0}, LX/1iq;->a()V

    goto :goto_0

    .line 388747
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;JJ)V
    .locals 9

    .prologue
    .line 388748
    invoke-virtual {p0, p1}, LX/2GV;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388749
    iget-object v1, p0, LX/2GV;->a:LX/2GW;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-virtual/range {v1 .. v7}, LX/2GW;->b(Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 388750
    :cond_0
    return-void
.end method

.method public final a(LX/1iq;)V
    .locals 2

    .prologue
    .line 388751
    iget-object v1, p0, LX/2GV;->e:Ljava/util/Set;

    monitor-enter v1

    .line 388752
    :try_start_0
    iget-object v0, p0, LX/2GV;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 388753
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(ILjava/lang/String;)V
    .locals 8

    .prologue
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 388734
    invoke-virtual {p0, p1}, LX/2GV;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388735
    iget-object v1, p0, LX/2GV;->a:LX/2GW;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    move-object v3, p2

    move-wide v6, v4

    invoke-virtual/range {v1 .. v7}, LX/2GW;->a(Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 388736
    :cond_0
    return-void
.end method

.method public final b(LX/1iq;)V
    .locals 2

    .prologue
    .line 388720
    iget-object v1, p0, LX/2GV;->e:Ljava/util/Set;

    monitor-enter v1

    .line 388721
    :try_start_0
    iget-object v0, p0, LX/2GV;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 388722
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 388733
    iget-object v0, p0, LX/2GV;->b:LX/2GX;

    invoke-virtual {v0, p1}, LX/2GX;->a(I)Z

    move-result v0

    return v0
.end method

.method public final c(I)LX/0lF;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 388729
    invoke-virtual {p0, p1}, LX/2GV;->d(I)V

    .line 388730
    iget-object v0, p0, LX/2GV;->c:LX/2GW;

    .line 388731
    iget-object v1, v0, LX/2GW;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    move v0, v1

    .line 388732
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2GV;->c:LX/2GW;

    iget-object v1, p0, LX/2GV;->d:Ljava/util/Map;

    invoke-virtual {v0, v1}, LX/2GW;->a(Ljava/util/Map;)LX/0lF;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 388723
    iget v0, p0, LX/2GV;->f:I

    if-eq v0, p1, :cond_0

    .line 388724
    iput p1, p0, LX/2GV;->f:I

    .line 388725
    invoke-direct {p0}, LX/2GV;->a()V

    .line 388726
    iget-object v0, p0, LX/2GV;->a:LX/2GW;

    invoke-virtual {v0}, LX/2GW;->b()LX/2GW;

    move-result-object v0

    iput-object v0, p0, LX/2GV;->c:LX/2GW;

    .line 388727
    iget-object v0, p0, LX/2GV;->b:LX/2GX;

    invoke-virtual {v0}, LX/2GX;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/2GV;->d:Ljava/util/Map;

    .line 388728
    :cond_0
    return-void
.end method
