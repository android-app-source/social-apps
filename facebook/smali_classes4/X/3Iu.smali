.class public final LX/3Iu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Iv;


# instance fields
.field public final synthetic a:LX/2oL;

.field public final synthetic b:LX/1Pe;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;LX/2oL;LX/1Pe;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 547262
    iput-object p1, p0, LX/3Iu;->d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    iput-object p2, p0, LX/3Iu;->a:LX/2oL;

    iput-object p3, p0, LX/3Iu;->b:LX/1Pe;

    iput-object p4, p0, LX/3Iu;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2oN;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 547263
    iget-object v0, p0, LX/3Iu;->d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    iget-object v0, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->h:LX/2ml;

    iget-boolean v0, v0, LX/2ml;->a:Z

    if-nez v0, :cond_1

    .line 547264
    :cond_0
    :goto_0
    return-void

    .line 547265
    :cond_1
    iget-object v0, p0, LX/3Iu;->a:LX/2oL;

    .line 547266
    iget-object v3, v0, LX/2oL;->h:LX/2oN;

    move-object v3, v3

    .line 547267
    if-eq v3, p1, :cond_0

    .line 547268
    iget-object v0, p0, LX/3Iu;->a:LX/2oL;

    .line 547269
    iput-object p1, v0, LX/2oL;->h:LX/2oN;

    .line 547270
    sget-object v0, LX/2oN;->TRANSITION:LX/2oN;

    if-ne p1, v0, :cond_3

    sget-object v0, LX/2oN;->NONE:LX/2oN;

    if-ne v3, v0, :cond_3

    move v0, v1

    .line 547271
    :goto_1
    sget-object v4, LX/2oN;->NONE:LX/2oN;

    if-ne p1, v4, :cond_4

    sget-object v4, LX/2oN;->NONE:LX/2oN;

    if-eq v3, v4, :cond_4

    move v3, v1

    .line 547272
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_0

    .line 547273
    :cond_2
    iget-object v0, p0, LX/3Iu;->b:LX/1Pe;

    check-cast v0, LX/1Pq;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, LX/3Iu;->c:Lcom/facebook/graphql/model/GraphQLStory;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 547274
    goto :goto_1

    :cond_4
    move v3, v2

    .line 547275
    goto :goto_2
.end method
