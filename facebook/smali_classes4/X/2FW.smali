.class public LX/2FW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 386999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387000
    iput-object p1, p0, LX/2FW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 387001
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/GRu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 387002
    iget-object v0, p0, LX/2FW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2FY;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v0

    .line 387003
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 387004
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 387005
    sget-object v3, LX/2FY;->b:LX/0Tn;

    invoke-virtual {v0, v3}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object v0

    .line 387006
    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    aget-object v0, v0, v3

    .line 387007
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 387008
    :cond_0
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v2

    .line 387009
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 387010
    invoke-virtual {p0, v0}, LX/2FW;->b(Ljava/lang/String;)LX/GRu;

    move-result-object v0

    .line 387011
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 387012
    :cond_1
    return-object v2
.end method

.method public final b(Ljava/lang/String;)LX/GRu;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 387013
    sget-object v0, LX/2FY;->b:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 387014
    iget-object v3, p0, LX/2FW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-string v1, "invite_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    invoke-interface {v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v2

    .line 387015
    :goto_0
    return-object v0

    .line 387016
    :cond_0
    new-instance v3, LX/GRu;

    iget-object v4, p0, LX/2FW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-string v1, "invite_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    invoke-interface {v4, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2FW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-string v4, "timestamp"

    invoke-virtual {v0, v4}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v0, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-direct {v3, v1, p1, v4, v5}, LX/GRu;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    move-object v0, v3

    .line 387017
    goto :goto_0
.end method
