.class public LX/3Lx;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field public final b:LX/3Lz;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 552761
    const-string v0, "^[0-9+\\(\\)#,;\\.\\s\\*\\-]+$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/3Lx;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/3Lz;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/common/hardware/StrictPhoneIsoCountryCode;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Lz;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 552762
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 552763
    iput-object p1, p0, LX/3Lx;->c:LX/0Or;

    .line 552764
    iput-object p2, p0, LX/3Lx;->b:LX/3Lz;

    .line 552765
    return-void
.end method

.method public static b(LX/0QB;)LX/3Lx;
    .locals 3

    .prologue
    .line 552766
    new-instance v1, LX/3Lx;

    const/16 v0, 0x15ed

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v0

    check-cast v0, LX/3Lz;

    invoke-direct {v1, v2, v0}, LX/3Lx;-><init>(LX/0Or;LX/3Lz;)V

    .line 552767
    return-object v1
.end method

.method public static c(Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 552768
    invoke-static {p0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x3f

    if-gt v0, v1, :cond_0

    sget-object v0, LX/3Lx;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 552769
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 552770
    const-string v0, ""

    .line 552771
    :goto_0
    return-object v0

    .line 552772
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 552773
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-char v4, v2, v0

    .line 552774
    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 552775
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 552776
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 552777
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static g(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 552778
    if-eqz p0, :cond_1

    const-string v0, "*"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/4hT;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 552779
    iget-object v0, p0, LX/3Lx;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 552780
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 552781
    const-string v0, "FbPhoneNumberUtils"

    const-string v2, "Country code not available"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 552782
    :goto_0
    return-object v0

    .line 552783
    :cond_0
    :try_start_0
    iget-object v2, p0, LX/3Lx;->b:LX/3Lz;

    invoke-virtual {v2, p1, v0}, LX/3Lz;->parseAndKeepRawInput(Ljava/lang/String;Ljava/lang/String;)LX/4hT;
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 552784
    :catch_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final b(LX/4hT;)Ljava/lang/String;
    .locals 2
    .param p1    # LX/4hT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 552785
    if-nez p1, :cond_0

    .line 552786
    const/4 v0, 0x0

    .line 552787
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3Lx;->b:LX/3Lz;

    sget-object v1, LX/4hG;->E164:LX/4hG;

    invoke-virtual {v0, p1, v1}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 552788
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 552789
    const/4 p1, 0x0

    .line 552790
    :cond_0
    :goto_0
    return-object p1

    .line 552791
    :cond_1
    invoke-static {p1}, LX/3Lx;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 552792
    invoke-static {p1}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 552793
    :cond_2
    invoke-virtual {p0, p1}, LX/3Lx;->a(Ljava/lang/String;)LX/4hT;

    move-result-object v0

    .line 552794
    if-eqz v0, :cond_0

    .line 552795
    iget-object v1, p0, LX/3Lx;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 552796
    iget-object v2, p0, LX/3Lx;->b:LX/3Lz;

    .line 552797
    iget p1, v0, LX/4hT;->countryCode_:I

    move p1, p1

    .line 552798
    invoke-virtual {v2, p1}, LX/3Lz;->getRegionCodeForCountryCode(I)Ljava/lang/String;

    move-result-object v2

    .line 552799
    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 552800
    if-eqz v1, :cond_3

    .line 552801
    iget-object v1, p0, LX/3Lx;->b:LX/3Lz;

    sget-object v2, LX/4hG;->INTERNATIONAL:LX/4hG;

    invoke-virtual {v1, v0, v2}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 552802
    :cond_3
    iget-object v1, p0, LX/3Lx;->b:LX/3Lz;

    sget-object v2, LX/4hG;->NATIONAL:LX/4hG;

    invoke-virtual {v1, v0, v2}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 552803
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 552804
    const-string v0, ""

    .line 552805
    :cond_0
    :goto_0
    return-object v0

    .line 552806
    :cond_1
    invoke-static {p1}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 552807
    invoke-static {v0}, LX/3Lx;->g(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x7

    if-lt v1, v2, :cond_0

    .line 552808
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 552809
    const/4 v0, 0x0

    .line 552810
    :goto_1
    move-object v0, v0

    .line 552811
    if-nez v0, :cond_0

    .line 552812
    const-string v0, "FbPhoneNumberUtils"

    const-string v1, "Parsing failed, fallback to normalize only."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 552813
    invoke-static {p1}, LX/3Lz;->normalizeDigitsOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, LX/3Lx;->a(Ljava/lang/String;)LX/4hT;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3Lx;->b(LX/4hT;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
