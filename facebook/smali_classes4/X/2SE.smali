.class public final LX/2SE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:LX/0Uh;

.field private final b:Landroid/content/pm/PackageManager;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Uh;Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/common/android/PackageName;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 411940
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 411941
    iput-object p1, p0, LX/2SE;->a:LX/0Uh;

    .line 411942
    iput-object p2, p0, LX/2SE;->b:Landroid/content/pm/PackageManager;

    .line 411943
    iput-object p3, p0, LX/2SE;->c:Ljava/lang/String;

    .line 411944
    return-void
.end method

.method public static b(LX/0QB;)LX/2SE;
    .locals 4

    .prologue
    .line 411945
    new-instance v3, LX/2SE;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageManager;

    invoke-static {p0}, LX/0dF;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v3, v0, v1, v2}, LX/2SE;-><init>(LX/0Uh;Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 411946
    return-object v3
.end method


# virtual methods
.method public final init()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 411947
    iget-object v0, p0, LX/2SE;->b:Landroid/content/pm/PackageManager;

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, LX/2SE;->c:Ljava/lang/String;

    const-string v3, "com.facebook.saved.intentfilter.ExternalSaveActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 411948
    iget-object v0, p0, LX/2SE;->a:LX/0Uh;

    const/16 v1, 0x4a4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411949
    iget-object v0, p0, LX/2SE;->b:Landroid/content/pm/PackageManager;

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, LX/2SE;->c:Ljava/lang/String;

    const-string v3, "com.facebook.saved.intentfilter.ExternalSaveActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v4, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 411950
    :cond_0
    return-void
.end method
