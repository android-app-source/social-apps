.class public LX/2G0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile p:LX/2G0;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/0ka;

.field public final e:LX/14G;

.field public final f:LX/0Zm;

.field public final g:LX/0u7;

.field public final h:LX/2G1;

.field public final i:LX/13u;

.field private final j:LX/1sz;

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Uh;

.field public final m:LX/0So;

.field public final n:LX/00G;

.field public final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 387632
    const-class v0, LX/2G0;

    sput-object v0, LX/2G0;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ka;LX/14G;LX/0u7;LX/0Zm;LX/2G1;LX/13u;LX/1sz;LX/0Or;LX/0Uh;LX/0So;LX/0Ot;LX/00G;)V
    .locals 0
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/analytics/annotations/DeviceStatusReporterInterval;
        .end annotation
    .end param
    .param p12    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .param p13    # LX/0Ot;
        .annotation runtime Lcom/facebook/device/annotations/BootId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0ka;",
            "LX/14G;",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            "Lcom/facebook/analytics/logger/AnalyticsConfig;",
            "LX/2G1;",
            "LX/13u;",
            "LX/1sz;",
            "LX/0Or",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0So;",
            "LX/0Ot",
            "<",
            "Ljava/util/UUID;",
            ">;",
            "LX/00G;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387617
    iput-object p1, p0, LX/2G0;->b:Landroid/content/Context;

    .line 387618
    iput-object p2, p0, LX/2G0;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 387619
    iput-object p3, p0, LX/2G0;->d:LX/0ka;

    .line 387620
    iput-object p4, p0, LX/2G0;->e:LX/14G;

    .line 387621
    iput-object p6, p0, LX/2G0;->f:LX/0Zm;

    .line 387622
    iput-object p5, p0, LX/2G0;->g:LX/0u7;

    .line 387623
    iput-object p7, p0, LX/2G0;->h:LX/2G1;

    .line 387624
    iput-object p8, p0, LX/2G0;->i:LX/13u;

    .line 387625
    iput-object p9, p0, LX/2G0;->j:LX/1sz;

    .line 387626
    iput-object p10, p0, LX/2G0;->k:LX/0Or;

    .line 387627
    iput-object p11, p0, LX/2G0;->l:LX/0Uh;

    .line 387628
    iput-object p12, p0, LX/2G0;->m:LX/0So;

    .line 387629
    iput-object p13, p0, LX/2G0;->o:LX/0Ot;

    .line 387630
    iput-object p14, p0, LX/2G0;->n:LX/00G;

    .line 387631
    return-void
.end method

.method public static a(LX/0QB;)LX/2G0;
    .locals 3

    .prologue
    .line 387516
    sget-object v0, LX/2G0;->p:LX/2G0;

    if-nez v0, :cond_1

    .line 387517
    const-class v1, LX/2G0;

    monitor-enter v1

    .line 387518
    :try_start_0
    sget-object v0, LX/2G0;->p:LX/2G0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387519
    if-eqz v2, :cond_0

    .line 387520
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2G0;->b(LX/0QB;)LX/2G0;

    move-result-object v0

    sput-object v0, LX/2G0;->p:LX/2G0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387521
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387522
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387523
    :cond_1
    sget-object v0, LX/2G0;->p:LX/2G0;

    return-object v0

    .line 387524
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387525
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2G0;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 14

    .prologue
    const-wide/32 v6, 0x100000

    .line 387602
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 387603
    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    .line 387604
    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v0

    sub-long v0, v4, v0

    .line 387605
    sub-long v0, v2, v0

    .line 387606
    const-string v4, "free_mem"

    div-long/2addr v0, v6

    invoke-virtual {p1, v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387607
    const-string v0, "total_mem"

    div-long/2addr v2, v6

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387608
    const-string v0, "total_mem_device"

    iget-object v1, p0, LX/2G0;->j:LX/1sz;

    .line 387609
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, LX/1sz;->a(Landroid/app/ActivityManager$MemoryInfo;)J

    move-result-wide v8

    .line 387610
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-ltz v10, :cond_0

    .line 387611
    :goto_0
    move-wide v2, v8

    .line 387612
    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387613
    return-void

    :cond_0
    invoke-virtual {v1}, LX/1sz;->a()LX/2GO;

    move-result-object v8

    .line 387614
    iget-wide v12, v8, LX/2GO;->b:J

    move-wide v8, v12

    .line 387615
    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/2G0;
    .locals 15

    .prologue
    .line 387600
    new-instance v0, LX/2G0;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v3

    check-cast v3, LX/0ka;

    invoke-static {p0}, LX/14G;->a(LX/0QB;)LX/14G;

    move-result-object v4

    check-cast v4, LX/14G;

    invoke-static {p0}, LX/0u7;->a(LX/0QB;)LX/0u7;

    move-result-object v5

    check-cast v5, LX/0u7;

    invoke-static {p0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v6

    check-cast v6, LX/0Zm;

    invoke-static {p0}, LX/2G1;->a(LX/0QB;)LX/2G1;

    move-result-object v7

    check-cast v7, LX/2G1;

    invoke-static {p0}, LX/13u;->a(LX/0QB;)LX/13u;

    move-result-object v8

    check-cast v8, LX/13u;

    invoke-static {p0}, LX/1sI;->a(LX/0QB;)LX/1sz;

    move-result-object v9

    check-cast v9, LX/1sz;

    const/16 v10, 0x15db

    invoke-static {p0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {p0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v12

    check-cast v12, LX/0So;

    const/16 v13, 0x161b

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static {p0}, LX/0VX;->b(LX/0QB;)LX/00G;

    move-result-object v14

    check-cast v14, LX/00G;

    invoke-direct/range {v0 .. v14}, LX/2G0;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ka;LX/14G;LX/0u7;LX/0Zm;LX/2G1;LX/13u;LX/1sz;LX/0Or;LX/0Uh;LX/0So;LX/0Ot;LX/00G;)V

    .line 387601
    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 387596
    iget-object v2, p0, LX/2G0;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 387597
    const-wide/32 v2, 0x36ee80

    .line 387598
    :goto_0
    move-wide v0, v2

    .line 387599
    return-wide v0

    :cond_0
    iget-object v2, p0, LX/2G0;->k:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_0
.end method

.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 12

    .prologue
    .line 387526
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "device_status"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 387527
    iput-wide p1, v1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 387528
    iget-object v4, p0, LX/2G0;->o:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/UUID;

    .line 387529
    if-eqz v4, :cond_0

    .line 387530
    const-string v5, "boot_id"

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387531
    :cond_0
    const-string v4, "battery"

    iget-object v5, p0, LX/2G0;->g:LX/0u7;

    invoke-virtual {v5}, LX/0u7;->a()F

    move-result v5

    float-to-double v6, v5

    invoke-virtual {v1, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387532
    const-string v4, "charge_state"

    iget-object v5, p0, LX/2G0;->g:LX/0u7;

    invoke-virtual {v5}, LX/0u7;->b()LX/0y1;

    move-result-object v5

    invoke-virtual {v5}, LX/0y1;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387533
    const-string v4, "battery_health"

    iget-object v5, p0, LX/2G0;->g:LX/0u7;

    .line 387534
    invoke-static {v5}, LX/0u7;->e(LX/0u7;)Landroid/content/Intent;

    move-result-object v6

    .line 387535
    if-nez v6, :cond_6

    .line 387536
    sget-object v6, LX/35a;->UNKNOWN:LX/35a;

    .line 387537
    :goto_0
    move-object v5, v6

    .line 387538
    invoke-virtual {v5}, LX/35a;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387539
    const-string v4, "wifi_enabled"

    iget-object v5, p0, LX/2G0;->d:LX/0ka;

    invoke-virtual {v5}, LX/0ka;->a()Z

    move-result v5

    invoke-virtual {v1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387540
    const-string v4, "wifi_connected"

    iget-object v5, p0, LX/2G0;->d:LX/0ka;

    invoke-virtual {v5}, LX/0ka;->b()Z

    move-result v5

    invoke-virtual {v1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387541
    iget-object v4, p0, LX/2G0;->e:LX/14G;

    invoke-virtual {v4}, LX/14G;->b()LX/0am;

    move-result-object v4

    .line 387542
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 387543
    const-string v5, "mobile_data_enabled"

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387544
    :cond_1
    iget-object v4, p0, LX/2G0;->e:LX/14G;

    invoke-virtual {v4}, LX/14G;->a()LX/0am;

    move-result-object v4

    .line 387545
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 387546
    const-string v5, "airplane_mode_on"

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387547
    :cond_2
    iget-object v4, p0, LX/2G0;->m:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    .line 387548
    const-string v6, "time_since_boot_ms"

    invoke-virtual {v1, v6, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387549
    :try_start_0
    iget-object v8, p0, LX/2G0;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "screen_brightness_mode"

    invoke-static {v8, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v8

    .line 387550
    if-nez v8, :cond_7

    .line 387551
    iget-object v8, p0, LX/2G0;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "screen_brightness"

    invoke-static {v8, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v8

    .line 387552
    const-string v9, "screen_brightness_raw_value"

    invoke-virtual {v1, v9, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 387553
    :goto_1
    iget-object v4, p0, LX/2G0;->i:LX/13u;

    invoke-virtual {v4, v1}, LX/13u;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 387554
    invoke-static {p0, v1}, LX/2G0;->a(LX/2G0;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 387555
    iget-object v4, p0, LX/2G0;->h:LX/2G1;

    .line 387556
    iget-object v5, v4, LX/2G1;->f:LX/2G3;

    invoke-virtual {v5, v1}, LX/0Vx;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 387557
    iget-object v4, p0, LX/2G0;->l:LX/0Uh;

    const/16 v5, 0x645

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-nez v4, :cond_3

    .line 387558
    iget-object v4, p0, LX/2G0;->i:LX/13u;

    invoke-virtual {v4, v1}, LX/13u;->c(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 387559
    iget-object v4, p0, LX/2G0;->h:LX/2G1;

    .line 387560
    iget-object v5, v4, LX/2G1;->e:LX/1ia;

    invoke-virtual {v5, v1}, LX/0Vx;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 387561
    :cond_3
    iget-object v4, p0, LX/2G0;->f:LX/0Zm;

    invoke-virtual {v4}, LX/0Zm;->a()LX/17O;

    move-result-object v4

    sget-object v5, LX/17O;->CORE_AND_SAMPLED:LX/17O;

    if-ne v4, v5, :cond_5

    .line 387562
    iget-object v4, p0, LX/2G0;->h:LX/2G1;

    .line 387563
    iget-object v5, v4, LX/2G1;->b:LX/2G2;

    invoke-virtual {v5, v1}, LX/0Vx;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 387564
    iget-object v4, p0, LX/2G0;->h:LX/2G1;

    .line 387565
    iget-object v5, v4, LX/2G1;->c:LX/0pj;

    invoke-virtual {v5, v1}, LX/0Vx;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 387566
    iget-object v4, p0, LX/2G0;->h:LX/2G1;

    .line 387567
    iget-object v5, v4, LX/2G1;->d:LX/1Gm;

    invoke-virtual {v5, v1}, LX/0Vx;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 387568
    iget-object v4, p0, LX/2G0;->h:LX/2G1;

    .line 387569
    iget-object v5, v4, LX/2G1;->g:LX/2G5;

    if-eqz v5, :cond_5

    .line 387570
    iget-object v5, v4, LX/2G1;->g:LX/2G5;

    invoke-virtual {v5}, LX/2G5;->a()LX/0P1;

    move-result-object v5

    invoke-virtual {v5}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v5

    invoke-virtual {v5}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 387571
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 387572
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0P1;

    invoke-virtual {v5}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v5

    invoke-virtual {v5}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 387573
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, "_"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v10, LX/3zL;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v10, v5}, LX/3zL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_2

    .line 387574
    :cond_5
    const-string v2, "process"

    iget-object v3, p0, LX/2G0;->n:LX/00G;

    invoke-virtual {v3}, LX/00G;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387575
    const-string v2, "device"

    .line 387576
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 387577
    move-object v0, v1

    .line 387578
    return-object v0

    .line 387579
    :cond_6
    const-string v7, "health"

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 387580
    packed-switch v6, :pswitch_data_0

    .line 387581
    sget-object v6, LX/35a;->UNKNOWN:LX/35a;

    goto/16 :goto_0

    .line 387582
    :pswitch_0
    sget-object v6, LX/35a;->COLD:LX/35a;

    goto/16 :goto_0

    .line 387583
    :pswitch_1
    sget-object v6, LX/35a;->DEAD:LX/35a;

    goto/16 :goto_0

    .line 387584
    :pswitch_2
    sget-object v6, LX/35a;->GOOD:LX/35a;

    goto/16 :goto_0

    .line 387585
    :pswitch_3
    sget-object v6, LX/35a;->OVERHEAT:LX/35a;

    goto/16 :goto_0

    .line 387586
    :pswitch_4
    sget-object v6, LX/35a;->OVER_VOLTAGE:LX/35a;

    goto/16 :goto_0

    .line 387587
    :pswitch_5
    sget-object v6, LX/35a;->UNSPECIFIED_FAILURE:LX/35a;

    goto/16 :goto_0

    .line 387588
    :cond_7
    :try_start_1
    const/4 v9, 0x1

    if-ne v8, v9, :cond_8

    .line 387589
    const-string v8, "screen_brightness_raw_value"

    const-wide/high16 v10, -0x4010000000000000L    # -1.0

    invoke-virtual {v1, v8, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 387590
    :catch_0
    move-exception v8

    .line 387591
    const-string v9, "Failed to get system brightness setting"

    .line 387592
    sget-object v10, LX/2G0;->a:Ljava/lang/Class;

    invoke-static {v10, v9, v8}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 387593
    goto/16 :goto_1

    .line 387594
    :cond_8
    :try_start_2
    sget-object v8, LX/2G0;->a:Ljava/lang/Class;

    const-string v9, "system brightness mode is unknown"

    invoke-static {v8, v9}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 387595
    const-string v8, "screen_brightness_raw_value"

    const-wide/high16 v10, -0x4000000000000000L    # -2.0

    invoke-virtual {v1, v8, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method
