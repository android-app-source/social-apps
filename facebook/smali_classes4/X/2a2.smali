.class public LX/2a2;
.super Ljava/util/concurrent/AbstractExecutorService;
.source ""

# interfaces
.implements LX/0Tf;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Landroid/content/Context;

.field public final d:LX/0So;

.field private final e:Landroid/app/AlarmManager;

.field private final f:Landroid/app/PendingIntent;

.field public final g:LX/0Sj;

.field private final h:Landroid/os/Handler;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "LX/30A",
            "<*>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 423240
    const-class v0, LX/2a2;

    sput-object v0, LX/2a2;->b:Ljava/lang/Class;

    .line 423241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/2a2;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ACTION_ALARM."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2a2;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0So;Landroid/app/AlarmManager;LX/0VT;LX/0Sj;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 5
    .param p2    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 423242
    invoke-direct {p0}, Ljava/util/concurrent/AbstractExecutorService;-><init>()V

    .line 423243
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, LX/2a2;->i:Ljava/util/PriorityQueue;

    .line 423244
    iput-object p1, p0, LX/2a2;->c:Landroid/content/Context;

    .line 423245
    iput-object p2, p0, LX/2a2;->d:LX/0So;

    .line 423246
    iput-object p3, p0, LX/2a2;->e:Landroid/app/AlarmManager;

    .line 423247
    iput-object p5, p0, LX/2a2;->g:LX/0Sj;

    .line 423248
    iput-object p7, p0, LX/2a2;->h:Landroid/os/Handler;

    .line 423249
    if-nez p6, :cond_0

    .line 423250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/2a2;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p4}, LX/0VT;->a()LX/00G;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 423251
    :goto_0
    move-object v0, v0

    .line 423252
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 423253
    iget-object v2, p0, LX/2a2;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 423254
    iget-object v2, p0, LX/2a2;->c:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, p0, LX/2a2;->f:Landroid/app/PendingIntent;

    .line 423255
    iget-object v1, p0, LX/2a2;->c:Landroid/content/Context;

    new-instance v2, LX/28r;

    invoke-direct {v2, p0, v0}, LX/28r;-><init>(LX/2a2;Ljava/lang/String;)V

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iget-object v4, p0, LX/2a2;->h:Landroid/os/Handler;

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 423256
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/2a2;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p4}, LX/0VT;->a()LX/00G;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture",
            "<*>;J)V"
        }
    .end annotation

    .prologue
    .line 423257
    iget-object v0, p0, LX/2a2;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    .line 423258
    monitor-enter p0

    .line 423259
    :try_start_0
    iget-object v0, p0, LX/2a2;->i:Ljava/util/PriorityQueue;

    new-instance v1, LX/30A;

    invoke-direct {v1, p1, p2, p3}, LX/30A;-><init>(Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;J)V

    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 423260
    invoke-static {p0}, LX/2a2;->b(LX/2a2;)V

    .line 423261
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a$redex0(LX/2a2;)V
    .locals 7

    .prologue
    .line 423262
    monitor-enter p0

    .line 423263
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 423264
    :goto_0
    iget-object v3, p0, LX/2a2;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v3}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, LX/2a2;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v3}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/30A;

    iget-wide v3, v3, LX/30A;->b:J

    iget-object v5, p0, LX/2a2;->d:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-lez v3, :cond_3

    :cond_0
    const/4 v3, 0x1

    :goto_1
    move v1, v3

    .line 423265
    if-nez v1, :cond_1

    .line 423266
    iget-object v1, p0, LX/2a2;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/30A;

    .line 423267
    iget-object v1, v1, LX/30A;->a:Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 423268
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 423269
    invoke-static {p0}, LX/2a2;->b(LX/2a2;)V

    .line 423270
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 423271
    invoke-virtual {v0}, LX/0Px;->size()I

    .line 423272
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v3, :cond_2

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    .line 423273
    invoke-virtual {v1}, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;->run()V

    .line 423274
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 423275
    :cond_2
    return-void

    .line 423276
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static a$redex0(LX/2a2;Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;)V
    .locals 4

    .prologue
    .line 423277
    const/4 v1, 0x0

    .line 423278
    monitor-enter p0

    .line 423279
    :try_start_0
    iget-object v0, p0, LX/2a2;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30A;

    .line 423280
    iget-object v3, v0, LX/30A;->a:Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    if-ne v3, p1, :cond_0

    .line 423281
    :goto_0
    if-eqz v0, :cond_1

    .line 423282
    iget-object v1, p0, LX/2a2;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    .line 423283
    invoke-static {p0}, LX/2a2;->b(LX/2a2;)V

    .line 423284
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private b(Ljava/lang/Runnable;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 423285
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/2a2;->b(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 423286
    invoke-direct {p0, p1}, LX/2a2;->c(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/2a2;->c(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    .line 423287
    iget-object v1, p0, LX/2a2;->d:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-direct {p0, v0, v2, v3}, LX/2a2;->a(Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;J)V

    .line 423288
    return-object v0
.end method

.method private b(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 423289
    invoke-direct {p0, p1}, LX/2a2;->c(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/2a2;->c(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    .line 423290
    iget-object v1, p0, LX/2a2;->d:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, LX/2a2;->a(Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;J)V

    .line 423291
    return-object v0
.end method

.method private b(Ljava/util/concurrent/Callable;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 423309
    const-wide/16 v0, 0x0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {p0, p1, v0, v1, v2}, LX/2a2;->b(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 423292
    iget-object v0, p0, LX/2a2;->g:LX/0Sj;

    const-string v1, "WakingExecutorService"

    invoke-static {p1, v0, v1}, LX/1A8;->a(Ljava/util/concurrent/Callable;LX/0Sj;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    move-object v0, v0

    .line 423293
    new-instance v1, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    invoke-direct {v1, p0, v0}, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;-><init>(LX/2a2;Ljava/util/concurrent/Callable;)V

    move-object v0, v1

    .line 423294
    iget-object v1, p0, LX/2a2;->d:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-direct {p0, v0, v2, v3}, LX/2a2;->a(Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;J)V

    .line 423295
    return-object v0
.end method

.method private static b(LX/2a2;)V
    .locals 5
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    const/4 v4, 0x2

    .line 423296
    iget-object v0, p0, LX/2a2;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423297
    iget-object v0, p0, LX/2a2;->e:Landroid/app/AlarmManager;

    iget-object v1, p0, LX/2a2;->f:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 423298
    :goto_0
    return-void

    .line 423299
    :cond_0
    iget-object v0, p0, LX/2a2;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30A;

    iget-wide v0, v0, LX/30A;->b:J

    .line 423300
    iget-object v2, p0, LX/2a2;->d:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    .line 423301
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-ge v2, v3, :cond_1

    .line 423302
    iget-object v2, p0, LX/2a2;->e:Landroid/app/AlarmManager;

    iget-object v3, p0, LX/2a2;->f:Landroid/app/PendingIntent;

    invoke-virtual {v2, v4, v0, v1, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0

    .line 423303
    :cond_1
    iget-object v2, p0, LX/2a2;->e:Landroid/app/AlarmManager;

    iget-object v3, p0, LX/2a2;->f:Landroid/app/PendingIntent;

    invoke-static {v2, v4, v0, v1, v3}, LX/30B;->a(Landroid/app/AlarmManager;IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private c(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 423304
    new-instance v0, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;-><init>(LX/2a2;Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method private c(Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 2

    .prologue
    .line 423305
    iget-object v0, p0, LX/2a2;->g:LX/0Sj;

    const-string v1, "WakingExecutorService"

    invoke-static {p1, v0, v1}, Lcom/facebook/common/executors/LoggingRunnable;->a(Ljava/lang/Runnable;LX/0Sj;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "JJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 423306
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 2

    .prologue
    .line 423307
    invoke-direct {p0, p1, p2, p3, p4}, LX/2a2;->b(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 2

    .prologue
    .line 423308
    invoke-direct {p0, p1, p2, p3, p4}, LX/2a2;->b(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 423238
    invoke-direct {p0, p1}, LX/2a2;->b(Ljava/lang/Runnable;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 423239
    invoke-direct {p0, p1, p2}, LX/2a2;->b(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 423222
    invoke-direct {p0, p1}, LX/2a2;->b(Ljava/util/concurrent/Callable;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method public final awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    .prologue
    .line 423220
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "JJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 423221
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final execute(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 423236
    invoke-direct {p0, p1}, LX/2a2;->b(Ljava/lang/Runnable;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    .line 423237
    return-void
.end method

.method public final isShutdown()Z
    .locals 1

    .prologue
    .line 423223
    const/4 v0, 0x0

    return v0
.end method

.method public final isTerminated()Z
    .locals 1

    .prologue
    .line 423224
    const/4 v0, 0x0

    return v0
.end method

.method public final newTaskFor(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/RunnableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Ljava/util/concurrent/RunnableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 423225
    new-instance v0, Lcom/facebook/common/executors/WakingExecutorService$ListenableScheduledRunnableFuture;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/common/executors/WakingExecutorService$ListenableScheduledRunnableFuture;-><init>(LX/2a2;Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final newTaskFor(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/RunnableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Ljava/util/concurrent/RunnableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 423226
    new-instance v0, Lcom/facebook/common/executors/WakingExecutorService$ListenableScheduledRunnableFuture;

    invoke-direct {v0, p0, p1}, Lcom/facebook/common/executors/WakingExecutorService$ListenableScheduledRunnableFuture;-><init>(LX/2a2;Ljava/util/concurrent/Callable;)V

    return-object v0
.end method

.method public final synthetic schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 423227
    invoke-direct {p0, p1, p2, p3, p4}, LX/2a2;->b(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 423228
    invoke-direct {p0, p1, p2, p3, p4}, LX/2a2;->b(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 423229
    invoke-virtual/range {p0 .. p6}, LX/2a2;->a(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 423230
    invoke-virtual/range {p0 .. p6}, LX/2a2;->b(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final shutdown()V
    .locals 1

    .prologue
    .line 423231
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final shutdownNow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 423232
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 423233
    invoke-direct {p0, p1}, LX/2a2;->b(Ljava/lang/Runnable;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 423234
    invoke-direct {p0, p1, p2}, LX/2a2;->b(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 423235
    invoke-direct {p0, p1}, LX/2a2;->b(Ljava/util/concurrent/Callable;)Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method
