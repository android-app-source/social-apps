.class public LX/2Ou;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field public final a:LX/0lB;

.field public final b:Lcom/facebook/user/model/UserKey;

.field private final c:LX/2Og;

.field private final d:LX/2Oi;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 403397
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2Ou;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0lB;Lcom/facebook/user/model/UserKey;LX/2Og;LX/2Oi;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/customthreads/annotations/CanViewCustomNicknames;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/user/model/UserKey;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserKey;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0lB;",
            "Lcom/facebook/user/model/UserKey;",
            "LX/2Og;",
            "LX/2Oi;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 403398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403399
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 403400
    iput-object v0, p0, LX/2Ou;->f:LX/0Ot;

    .line 403401
    iput-object p1, p0, LX/2Ou;->e:LX/0Or;

    .line 403402
    iput-object p2, p0, LX/2Ou;->a:LX/0lB;

    .line 403403
    iput-object p3, p0, LX/2Ou;->b:Lcom/facebook/user/model/UserKey;

    .line 403404
    iput-object p4, p0, LX/2Ou;->c:LX/2Og;

    .line 403405
    iput-object p5, p0, LX/2Ou;->d:LX/2Oi;

    .line 403406
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ou;
    .locals 13

    .prologue
    .line 403407
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 403408
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 403409
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 403410
    if-nez v1, :cond_0

    .line 403411
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 403412
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 403413
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 403414
    sget-object v1, LX/2Ou;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 403415
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 403416
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 403417
    :cond_1
    if-nez v1, :cond_4

    .line 403418
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 403419
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 403420
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 403421
    new-instance v7, LX/2Ou;

    const/16 v8, 0x150f

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v9

    check-cast v9, LX/0lB;

    invoke-static {v0}, LX/2Ot;->b(LX/0QB;)Lcom/facebook/user/model/UserKey;

    move-result-object v10

    check-cast v10, Lcom/facebook/user/model/UserKey;

    invoke-static {v0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v11

    check-cast v11, LX/2Og;

    invoke-static {v0}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v12

    check-cast v12, LX/2Oi;

    invoke-direct/range {v7 .. v12}, LX/2Ou;-><init>(LX/0Or;LX/0lB;Lcom/facebook/user/model/UserKey;LX/2Og;LX/2Oi;)V

    .line 403422
    const/16 v8, 0x259

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 403423
    iput-object v8, v7, LX/2Ou;->f:LX/0Ot;

    .line 403424
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 403425
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 403426
    if-nez v1, :cond_2

    .line 403427
    sget-object v0, LX/2Ou;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ou;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 403428
    :goto_1
    if-eqz v0, :cond_3

    .line 403429
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 403430
    :goto_3
    check-cast v0, LX/2Ou;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 403431
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 403432
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 403433
    :catchall_1
    move-exception v0

    .line 403434
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 403435
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 403436
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 403437
    :cond_2
    :try_start_8
    sget-object v0, LX/2Ou;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ou;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(Ljava/util/List;Lcom/facebook/user/model/UserKey;)Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;",
            "Lcom/facebook/user/model/UserKey;",
            ")",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 403438
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 403439
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v2

    invoke-static {p1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 403440
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 403441
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 403442
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 403443
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 403444
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 403445
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/2Ou;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/5e9;)Lcom/facebook/messaging/model/threads/ThreadParticipant;
    .locals 5
    .param p0    # LX/2Ou;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 403446
    if-eqz p1, :cond_2

    .line 403447
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 403448
    iget-object v0, p0, LX/2Ou;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "ThreadParticipantUtils.EMPTY_PARTICIPANTS"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to process participants in Canonical Thread for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 403449
    :goto_0
    return-object v0

    .line 403450
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    if-eq v0, p2, :cond_1

    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 403451
    :cond_1
    invoke-virtual {p0, p1}, LX/2Ou;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 403452
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadParticipant;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 403453
    iget-object v0, p0, LX/2Ou;->b:Lcom/facebook/user/model/UserKey;

    if-eqz v0, :cond_1

    .line 403454
    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 403455
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v5

    iget-object v6, p0, LX/2Ou;->b:Lcom/facebook/user/model/UserKey;

    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 403456
    :goto_1
    return-object v0

    .line 403457
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 403458
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    goto :goto_1
.end method

.method public final b(Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadParticipant;
    .locals 1
    .param p1    # Lcom/facebook/messaging/model/threads/ThreadSummary;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 403459
    sget-object v0, LX/5e9;->ONE_TO_ONE:LX/5e9;

    invoke-static {p0, p1, v0}, LX/2Ou;->a(LX/2Ou;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/5e9;)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    return-object v0
.end method
