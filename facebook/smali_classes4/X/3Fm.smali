.class public final LX/3Fm;
.super LX/37v;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# static fields
.field public static final a:Z


# instance fields
.field public final b:Landroid/content/ComponentName;

.field public final c:LX/3Fn;

.field public final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/67O;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field private f:Z

.field public g:LX/67N;

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 539972
    const-string v0, "MediaRouteProviderProxy"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, LX/3Fm;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 539967
    new-instance v0, LX/381;

    invoke-direct {v0, p2}, LX/381;-><init>(Landroid/content/ComponentName;)V

    invoke-direct {p0, p1, v0}, LX/37v;-><init>(Landroid/content/Context;LX/381;)V

    .line 539968
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3Fm;->d:Ljava/util/ArrayList;

    .line 539969
    iput-object p2, p0, LX/3Fm;->b:Landroid/content/ComponentName;

    .line 539970
    new-instance v0, LX/3Fn;

    invoke-direct {v0, p0}, LX/3Fn;-><init>(LX/3Fm;)V

    iput-object v0, p0, LX/3Fm;->c:LX/3Fn;

    .line 539971
    return-void
.end method

.method public static a$redex0(LX/3Fm;LX/67N;LX/382;)V
    .locals 2

    .prologue
    .line 539962
    iget-object v0, p0, LX/3Fm;->g:LX/67N;

    if-ne v0, p1, :cond_1

    .line 539963
    sget-boolean v0, LX/3Fm;->a:Z

    if-eqz v0, :cond_0

    .line 539964
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Descriptor changed, descriptor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 539965
    :cond_0
    invoke-virtual {p0, p2}, LX/37v;->a(LX/382;)V

    .line 539966
    :cond_1
    return-void
.end method

.method public static j(LX/3Fm;)V
    .locals 1

    .prologue
    .line 539958
    invoke-static {p0}, LX/3Fm;->k(LX/3Fm;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539959
    invoke-static {p0}, LX/3Fm;->l(LX/3Fm;)V

    .line 539960
    :goto_0
    return-void

    .line 539961
    :cond_0
    invoke-static {p0}, LX/3Fm;->m(LX/3Fm;)V

    goto :goto_0
.end method

.method public static k(LX/3Fm;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 539952
    iget-boolean v1, p0, LX/3Fm;->e:Z

    if-eqz v1, :cond_2

    .line 539953
    iget-object v1, p0, LX/37v;->e:LX/38A;

    move-object v1, v1

    .line 539954
    if-eqz v1, :cond_1

    .line 539955
    :cond_0
    :goto_0
    return v0

    .line 539956
    :cond_1
    iget-object v1, p0, LX/3Fm;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 539957
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(LX/3Fm;)V
    .locals 4

    .prologue
    .line 539878
    iget-boolean v0, p0, LX/3Fm;->f:Z

    if-nez v0, :cond_1

    .line 539879
    sget-boolean v0, LX/3Fm;->a:Z

    if-eqz v0, :cond_0

    .line 539880
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Binding"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 539881
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.MediaRouteProviderService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 539882
    iget-object v1, p0, LX/3Fm;->b:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 539883
    :try_start_0
    iget-object v1, p0, LX/37v;->a:Landroid/content/Context;

    move-object v1, v1

    .line 539884
    const/4 v2, 0x1

    const v3, 0xf31d42d

    invoke-static {v1, v0, p0, v2, v3}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    move-result v0

    iput-boolean v0, p0, LX/3Fm;->f:Z

    .line 539885
    iget-boolean v0, p0, LX/3Fm;->f:Z

    if-nez v0, :cond_1

    sget-boolean v0, LX/3Fm;->a:Z

    if-eqz v0, :cond_1

    .line 539886
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Bind failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 539887
    :cond_1
    :goto_0
    return-void

    .line 539888
    :catch_0
    sget-boolean v0, LX/3Fm;->a:Z

    if-eqz v0, :cond_1

    .line 539889
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Bind failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static m(LX/3Fm;)V
    .locals 2

    .prologue
    .line 539944
    iget-boolean v0, p0, LX/3Fm;->f:Z

    if-eqz v0, :cond_1

    .line 539945
    sget-boolean v0, LX/3Fm;->a:Z

    if-eqz v0, :cond_0

    .line 539946
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Unbinding"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 539947
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Fm;->f:Z

    .line 539948
    invoke-static {p0}, LX/3Fm;->n(LX/3Fm;)V

    .line 539949
    iget-object v0, p0, LX/37v;->a:Landroid/content/Context;

    move-object v0, v0

    .line 539950
    const v1, 0x29a4a452

    invoke-static {v0, p0, v1}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    .line 539951
    :cond_1
    return-void
.end method

.method public static n(LX/3Fm;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 539973
    iget-object v0, p0, LX/3Fm;->g:LX/67N;

    if-eqz v0, :cond_1

    .line 539974
    invoke-virtual {p0, v1}, LX/37v;->a(LX/382;)V

    .line 539975
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Fm;->h:Z

    .line 539976
    iget-object v0, p0, LX/3Fm;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 539977
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 539978
    iget-object v0, p0, LX/3Fm;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67O;

    invoke-virtual {v0}, LX/67O;->d()V

    .line 539979
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 539980
    :cond_0
    iget-object v0, p0, LX/3Fm;->g:LX/67N;

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 539981
    const/4 v3, 0x2

    move-object v2, v0

    move v5, v4

    move-object v7, v6

    invoke-static/range {v2 .. v7}, LX/67N;->a(LX/67N;IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 539982
    iget-object v2, v0, LX/67N;->c:LX/67P;

    .line 539983
    iget-object v3, v2, LX/67P;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->clear()V

    .line 539984
    iget-object v2, v0, LX/67N;->b:Landroid/os/Messenger;

    invoke-virtual {v2}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v2, v0, v4}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 539985
    iget-object v2, v0, LX/67N;->a:LX/3Fm;

    iget-object v2, v2, LX/3Fm;->c:LX/3Fn;

    new-instance v3, Landroid/support/v7/media/RegisteredMediaRouteProvider$Connection$1;

    invoke-direct {v3, v0}, Landroid/support/v7/media/RegisteredMediaRouteProvider$Connection$1;-><init>(LX/67N;)V

    const v4, -0x340042e3    # -3.3520186E7f

    invoke-static {v2, v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 539986
    iput-object v1, p0, LX/3Fm;->g:LX/67N;

    .line 539987
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/389;
    .locals 4

    .prologue
    .line 539929
    iget-object v0, p0, LX/37v;->g:LX/382;

    move-object v0, v0

    .line 539930
    if-eqz v0, :cond_2

    .line 539931
    invoke-virtual {v0}, LX/382;->a()Ljava/util/List;

    move-result-object v2

    .line 539932
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 539933
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 539934
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/383;

    .line 539935
    invoke-virtual {v0}, LX/383;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 539936
    new-instance v0, LX/67O;

    invoke-direct {v0, p0, p1}, LX/67O;-><init>(LX/3Fm;Ljava/lang/String;)V

    .line 539937
    iget-object v1, p0, LX/3Fm;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 539938
    iget-boolean v1, p0, LX/3Fm;->h:Z

    if-eqz v1, :cond_0

    .line 539939
    iget-object v1, p0, LX/3Fm;->g:LX/67N;

    invoke-virtual {v0, v1}, LX/67O;->a(LX/67N;)V

    .line 539940
    :cond_0
    invoke-static {p0}, LX/3Fm;->j(LX/3Fm;)V

    .line 539941
    :goto_1
    return-object v0

    .line 539942
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 539943
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(LX/38A;)V
    .locals 1

    .prologue
    .line 539925
    iget-boolean v0, p0, LX/3Fm;->h:Z

    if-eqz v0, :cond_0

    .line 539926
    iget-object v0, p0, LX/3Fm;->g:LX/67N;

    invoke-virtual {v0, p1}, LX/67N;->a(LX/38A;)V

    .line 539927
    :cond_0
    invoke-static {p0}, LX/3Fm;->j(LX/3Fm;)V

    .line 539928
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 539919
    iget-boolean v0, p0, LX/3Fm;->e:Z

    if-nez v0, :cond_1

    .line 539920
    sget-boolean v0, LX/3Fm;->a:Z

    if-eqz v0, :cond_0

    .line 539921
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Starting"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 539922
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3Fm;->e:Z

    .line 539923
    invoke-static {p0}, LX/3Fm;->j(LX/3Fm;)V

    .line 539924
    :cond_1
    return-void
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 10

    .prologue
    .line 539895
    sget-boolean v0, LX/3Fm;->a:Z

    if-eqz v0, :cond_0

    .line 539896
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Connected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 539897
    :cond_0
    iget-boolean v0, p0, LX/3Fm;->f:Z

    if-eqz v0, :cond_2

    .line 539898
    invoke-static {p0}, LX/3Fm;->n(LX/3Fm;)V

    .line 539899
    if-eqz p2, :cond_3

    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    .line 539900
    :goto_0
    const/4 v1, 0x0

    .line 539901
    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    .line 539902
    :cond_1
    :goto_1
    move v1, v1

    .line 539903
    if-eqz v1, :cond_5

    .line 539904
    new-instance v1, LX/67N;

    invoke-direct {v1, p0, v0}, LX/67N;-><init>(LX/3Fm;Landroid/os/Messenger;)V

    .line 539905
    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v9, 0x0

    .line 539906
    iget v3, v1, LX/67N;->e:I

    add-int/lit8 v5, v3, 0x1

    iput v5, v1, LX/67N;->e:I

    iput v3, v1, LX/67N;->h:I

    .line 539907
    iget v5, v1, LX/67N;->h:I

    move-object v3, v1

    move v6, v4

    move-object v8, v7

    invoke-static/range {v3 .. v8}, LX/67N;->a(LX/67N;IIILjava/lang/Object;Landroid/os/Bundle;)Z

    move-result v3

    if-nez v3, :cond_6

    move v4, v9

    .line 539908
    :goto_2
    move v0, v4

    .line 539909
    if-eqz v0, :cond_4

    .line 539910
    iput-object v1, p0, LX/3Fm;->g:LX/67N;

    .line 539911
    :cond_2
    :goto_3
    return-void

    .line 539912
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 539913
    :cond_4
    sget-boolean v0, LX/3Fm;->a:Z

    if-eqz v0, :cond_2

    .line 539914
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Registration failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 539915
    :cond_5
    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Service returned invalid messenger binder"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catch_0
    goto :goto_1

    .line 539916
    :cond_6
    :try_start_1
    iget-object v3, v1, LX/67N;->b:Landroid/os/Messenger;

    invoke-virtual {v3}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v3, v1, v5}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 539917
    :catch_1
    invoke-virtual {v1}, LX/67N;->binderDied()V

    move v4, v9

    .line 539918
    goto :goto_2
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 539891
    sget-boolean v0, LX/3Fm;->a:Z

    if-eqz v0, :cond_0

    .line 539892
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Service disconnected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 539893
    :cond_0
    invoke-static {p0}, LX/3Fm;->n(LX/3Fm;)V

    .line 539894
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 539890
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Service connection "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/3Fm;->b:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
