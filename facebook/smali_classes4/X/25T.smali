.class public LX/25T;
.super LX/1P1;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:[I

.field public final c:LX/25U;

.field public final d:LX/25V;

.field private final e:Landroid/content/Context;

.field public f:I

.field public g:F

.field private h:LX/25W;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/25U;LX/25V;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 369504
    invoke-direct {p0, p1}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 369505
    const/4 v0, 0x0

    iput v0, p0, LX/25T;->f:I

    .line 369506
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, LX/25T;->g:F

    .line 369507
    iput-object p1, p0, LX/25T;->e:Landroid/content/Context;

    .line 369508
    iput-object p2, p0, LX/25T;->c:LX/25U;

    .line 369509
    iput-object p3, p0, LX/25T;->d:LX/25V;

    .line 369510
    new-instance v0, LX/25W;

    iget-object v1, p0, LX/25T;->e:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, LX/25W;-><init>(LX/25T;Landroid/content/Context;)V

    iput-object v0, p0, LX/25T;->h:LX/25W;

    .line 369511
    return-void
.end method

.method public static a(LX/0QB;)LX/25T;
    .locals 4

    .prologue
    .line 369512
    new-instance v3, LX/25T;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/25U;->a(LX/0QB;)LX/25U;

    move-result-object v1

    check-cast v1, LX/25U;

    invoke-static {p0}, LX/25V;->a(LX/0QB;)LX/25V;

    move-result-object v2

    check-cast v2, LX/25V;

    invoke-direct {v3, v0, v1, v2}, LX/25T;-><init>(Landroid/content/Context;LX/25U;LX/25V;)V

    .line 369513
    return-object v3
.end method


# virtual methods
.method public final a(D)V
    .locals 5

    .prologue
    .line 369439
    const-wide/high16 v0, 0x4049000000000000L    # 50.0

    const-wide/16 v2, 0x0

    cmpg-double v2, p1, v2

    if-gtz v2, :cond_0

    const-wide/high16 p1, 0x3ff0000000000000L    # 1.0

    :cond_0
    div-double/2addr v0, p1

    double-to-float v0, v0

    iput v0, p0, LX/25T;->g:F

    .line 369440
    new-instance v0, LX/25W;

    iget-object v1, p0, LX/25T;->e:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, LX/25W;-><init>(LX/25T;Landroid/content/Context;)V

    iput-object v0, p0, LX/25T;->h:LX/25W;

    .line 369441
    return-void
.end method

.method public a(LX/1Od;LX/1Ok;II)V
    .locals 11

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 369464
    :try_start_0
    const-string v1, "HScrollLinearLayoutManager.onMeasure"

    const v3, 0x4a5521f3    # 3491964.8f

    invoke-static {v1, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 369465
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 369466
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 369467
    if-ne v3, v5, :cond_0

    .line 369468
    iget v1, p0, LX/1P1;->j:I

    move v1, v1

    .line 369469
    if-eq v1, v0, :cond_1

    :cond_0
    if-ne v4, v5, :cond_2

    .line 369470
    iget v1, p0, LX/1P1;->j:I

    move v1, v1

    .line 369471
    if-nez v1, :cond_2

    .line 369472
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, LX/1P1;->a(LX/1Od;LX/1Ok;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369473
    const v0, -0x514a2661

    invoke-static {v0}, LX/02m;->a(I)V

    .line 369474
    :goto_0
    return-void

    .line 369475
    :cond_2
    :try_start_1
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 369476
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 369477
    iget-object v1, p0, LX/25T;->d:LX/25V;

    iget-object v7, p0, LX/25T;->a:Ljava/lang/String;

    invoke-virtual {v1, v7}, LX/25V;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 369478
    iget-object v0, p0, LX/25T;->d:LX/25V;

    iget-object v1, p0, LX/25T;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/25V;->a(Ljava/lang/String;)[I

    move-result-object v0

    .line 369479
    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 369480
    :goto_2
    packed-switch v4, :pswitch_data_1

    .line 369481
    :goto_3
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v0, v0, v2

    invoke-virtual {p0, v1, v0}, LX/1OR;->e(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 369482
    const v0, -0x2c77082b

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 369483
    :cond_3
    const/4 v1, 0x2

    :try_start_2
    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 369484
    invoke-virtual {p0}, LX/1OR;->D()I

    move-result v7

    if-lez v7, :cond_8

    .line 369485
    invoke-virtual {p0}, LX/25T;->c()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {p0}, LX/1OR;->D()I

    move-result v0

    .line 369486
    :cond_4
    :goto_4
    if-ge v2, v0, :cond_7

    .line 369487
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-static {p1, v2, v8, v9}, LX/25U;->a(LX/1Od;III)[I

    move-result-object v7

    iput-object v7, p0, LX/25T;->b:[I

    .line 369488
    iget v7, p0, LX/1P1;->j:I

    move v7, v7

    .line 369489
    if-nez v7, :cond_6

    .line 369490
    const/4 v7, 0x0

    aget v8, v1, v7

    iget-object v9, p0, LX/25T;->b:[I

    const/4 v10, 0x0

    aget v9, v9, v10

    add-int/2addr v8, v9

    aput v8, v1, v7

    .line 369491
    iget-object v7, p0, LX/25T;->b:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    invoke-virtual {p0}, LX/1OR;->z()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, LX/1OR;->B()I

    move-result v8

    add-int/2addr v7, v8

    .line 369492
    const/4 v8, 0x1

    aget v8, v1, v8

    if-le v7, v8, :cond_5

    .line 369493
    const/4 v8, 0x1

    aput v7, v1, v8

    .line 369494
    :cond_5
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 369495
    :cond_6
    const/4 v7, 0x1

    aget v8, v1, v7

    iget-object v9, p0, LX/25T;->b:[I

    const/4 v10, 0x1

    aget v9, v9, v10

    add-int/2addr v8, v9

    aput v8, v1, v7

    .line 369496
    iget-object v7, p0, LX/25T;->b:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    invoke-virtual {p0}, LX/1OR;->y()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, LX/1OR;->A()I

    move-result v8

    add-int/2addr v7, v8

    .line 369497
    const/4 v8, 0x0

    aget v8, v1, v8

    if-le v7, v8, :cond_5

    .line 369498
    const/4 v8, 0x0

    aput v7, v1, v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    .line 369499
    :catchall_0
    move-exception v0

    const v1, -0x60a10a65

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 369500
    :cond_7
    :try_start_3
    iget-object v0, p0, LX/25T;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 369501
    iget-object v0, p0, LX/25T;->d:LX/25V;

    iget-object v2, p0, LX/25T;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/25V;->a(Ljava/lang/String;[I)V

    :cond_8
    move-object v0, v1

    goto/16 :goto_1

    .line 369502
    :pswitch_0
    const/4 v1, 0x0

    aput v5, v0, v1

    goto/16 :goto_2

    .line 369503
    :pswitch_1
    const/4 v1, 0x1

    aput v6, v0, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :pswitch_data_0
    .packed-switch 0x40000000
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x40000000
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;LX/1Ok;I)V
    .locals 1

    .prologue
    .line 369460
    iget-object v0, p0, LX/25T;->h:LX/25W;

    .line 369461
    iput p3, v0, LX/25Y;->a:I

    .line 369462
    iget-object v0, p0, LX/25T;->h:LX/25W;

    invoke-virtual {p0, v0}, LX/1OR;->a(LX/25Y;)V

    .line 369463
    return-void
.end method

.method public final a(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 369514
    const-string v0, "HScrollLinearLayoutManager.measureChildWithMargins"

    const v1, 0x34f82dde

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 369515
    :try_start_0
    invoke-super {p0, p1, p2, p3}, LX/1P1;->a(Landroid/view/View;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369516
    const v0, 0x71ea8acf

    invoke-static {v0}, LX/02m;->a(I)V

    .line 369517
    return-void

    .line 369518
    :catchall_0
    move-exception v0

    const v1, -0x6cf291e3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Landroid/view/View;IIII)V
    .locals 2

    .prologue
    .line 369455
    const-string v0, "HScrollLinearLayoutManager.layoutDecorated"

    const v1, 0x14d5cdbf

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 369456
    :try_start_0
    invoke-super/range {p0 .. p5}, LX/1P1;->a(Landroid/view/View;IIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369457
    const v0, 0x2985645f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 369458
    return-void

    .line 369459
    :catchall_0
    move-exception v0

    const v1, -0x29505369

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 369450
    const-string v0, "HScrollLinearLayoutManager.addView"

    const v1, 0x7641b158

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 369451
    :try_start_0
    invoke-super {p0, p1}, LX/1P1;->b(Landroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369452
    const v0, -0x23c9e9fb

    invoke-static {v0}, LX/02m;->a(I)V

    .line 369453
    return-void

    .line 369454
    :catchall_0
    move-exception v0

    const v1, 0x12603efd

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final c(LX/1Od;LX/1Ok;)V
    .locals 2

    .prologue
    .line 369445
    const-string v0, "HScrollLinearLayoutManager.onLayoutChildren"

    const v1, 0x483ec689

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 369446
    :try_start_0
    invoke-super {p0, p1, p2}, LX/1P1;->c(LX/1Od;LX/1Ok;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369447
    const v0, 0x7178302

    invoke-static {v0}, LX/02m;->a(I)V

    .line 369448
    return-void

    .line 369449
    :catchall_0
    move-exception v0

    const v1, -0x5409ee11

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 369444
    const/4 v0, 0x0

    return v0
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 369442
    iget v0, p0, LX/25T;->f:I

    invoke-super {p0, p1, v0}, LX/1P1;->d(II)V

    .line 369443
    return-void
.end method
