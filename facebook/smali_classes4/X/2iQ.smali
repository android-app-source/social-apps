.class public final enum LX/2iQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2iQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2iQ;

.field public static final enum FEED:LX/2iQ;

.field public static final enum FRIENDS_CENTER:LX/2iQ;

.field public static final enum JEWEL:LX/2iQ;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 451566
    new-instance v0, LX/2iQ;

    const-string v1, "FEED"

    const-string v2, "mobile_feed"

    invoke-direct {v0, v1, v3, v2}, LX/2iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2iQ;->FEED:LX/2iQ;

    .line 451567
    new-instance v0, LX/2iQ;

    const-string v1, "JEWEL"

    const-string v2, "mobile_jewel"

    invoke-direct {v0, v1, v4, v2}, LX/2iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2iQ;->JEWEL:LX/2iQ;

    .line 451568
    new-instance v0, LX/2iQ;

    const-string v1, "FRIENDS_CENTER"

    const-string v2, "mobile_friend_center"

    invoke-direct {v0, v1, v5, v2}, LX/2iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2iQ;->FRIENDS_CENTER:LX/2iQ;

    .line 451569
    const/4 v0, 0x3

    new-array v0, v0, [LX/2iQ;

    sget-object v1, LX/2iQ;->FEED:LX/2iQ;

    aput-object v1, v0, v3

    sget-object v1, LX/2iQ;->JEWEL:LX/2iQ;

    aput-object v1, v0, v4

    sget-object v1, LX/2iQ;->FRIENDS_CENTER:LX/2iQ;

    aput-object v1, v0, v5

    sput-object v0, LX/2iQ;->$VALUES:[LX/2iQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 451570
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 451571
    iput-object p3, p0, LX/2iQ;->value:Ljava/lang/String;

    .line 451572
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2iQ;
    .locals 1

    .prologue
    .line 451565
    const-class v0, LX/2iQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2iQ;

    return-object v0
.end method

.method public static values()[LX/2iQ;
    .locals 1

    .prologue
    .line 451564
    sget-object v0, LX/2iQ;->$VALUES:[LX/2iQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2iQ;

    return-object v0
.end method
