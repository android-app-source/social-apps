.class public LX/2TR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 414402
    sget-object v0, LX/0Tm;->e:LX/0Tn;

    const-string v1, "vault/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 414403
    sput-object v0, LX/2TR;->a:LX/0Tn;

    const-string v1, "vault_table_cutoff_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2TR;->b:LX/0Tn;

    .line 414404
    sget-object v0, LX/2TR;->a:LX/0Tn;

    const-string v1, "upload_state"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2TR;->c:LX/0Tn;

    .line 414405
    sget-object v0, LX/2TR;->a:LX/0Tn;

    const-string v1, "sync_old_photo"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2TR;->d:LX/0Tn;

    .line 414406
    sget-object v0, LX/2TR;->a:LX/0Tn;

    const-string v1, "retry_millis"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2TR;->e:LX/0Tn;

    .line 414407
    sget-object v0, LX/2TR;->a:LX/0Tn;

    const-string v1, "sync_mode"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2TR;->f:LX/0Tn;

    .line 414408
    sget-object v0, LX/2TR;->a:LX/0Tn;

    const-string v1, "device_created_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2TR;->g:LX/0Tn;

    .line 414409
    sget-object v0, LX/2TR;->a:LX/0Tn;

    const-string v1, "last_synced_date"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2TR;->h:LX/0Tn;

    .line 414410
    sget-object v0, LX/2TR;->a:LX/0Tn;

    const-string v1, "device_fbid"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2TR;->i:LX/0Tn;

    .line 414411
    sget-object v0, LX/2TR;->a:LX/0Tn;

    const-string v1, "vault_blacklisted_sync_paths"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2TR;->j:LX/0Tn;

    .line 414412
    sget-object v0, LX/2TR;->a:LX/0Tn;

    const-string v1, "last_blacklist_synced_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2TR;->k:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 414413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 414414
    sget-object v0, LX/2TR;->a:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
