.class public LX/2NZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 399881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/0aG;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 399882
    sget-object v0, LX/1CA;->f:LX/0Tn;

    invoke-virtual {v0, p3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 399883
    const-string v1, ""

    invoke-interface {p0, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 399884
    :goto_0
    return-void

    .line 399885
    :cond_0
    if-eqz p4, :cond_1

    .line 399886
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 399887
    new-instance v3, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-direct {v3, p3, v2, v4}, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 399888
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 399889
    const-string v4, "checkCodeParams"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 399890
    const-string v3, "fetch_code"

    const v4, 0x4c5729b

    invoke-static {p2, v3, v2, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    .line 399891
    new-instance v3, LX/FAk;

    invoke-direct {v3, p3, p0}, LX/FAk;-><init>(Ljava/lang/String;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    invoke-virtual {p1, v2, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 399892
    goto :goto_0

    .line 399893
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 399894
    sget-object v3, LX/26p;->f:LX/0Tn;

    const-string v4, ""

    invoke-interface {p0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 399895
    new-instance v4, Lcom/facebook/katana/activity/codegenerator/data/LegacyFetchCodeParams;

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, p3, v3, v2}, Lcom/facebook/katana/activity/codegenerator/data/LegacyFetchCodeParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 399896
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 399897
    const-string v3, "checkCodeParams"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 399898
    const-string v3, "legacy_fetch_code"

    const v4, -0x7dceda03

    invoke-static {p2, v3, v2, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    .line 399899
    new-instance v3, LX/2gz;

    invoke-direct {v3, p3, p0}, LX/2gz;-><init>(Ljava/lang/String;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    invoke-virtual {p1, v2, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 399900
    goto :goto_0
.end method
