.class public LX/2Fa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Fa;


# instance fields
.field private final a:LX/00H;

.field private final b:LX/0W9;


# direct methods
.method public constructor <init>(LX/00H;LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387101
    iput-object p1, p0, LX/2Fa;->a:LX/00H;

    .line 387102
    iput-object p2, p0, LX/2Fa;->b:LX/0W9;

    .line 387103
    return-void
.end method

.method public static a(LX/0QB;)LX/2Fa;
    .locals 5

    .prologue
    .line 387087
    sget-object v0, LX/2Fa;->c:LX/2Fa;

    if-nez v0, :cond_1

    .line 387088
    const-class v1, LX/2Fa;

    monitor-enter v1

    .line 387089
    :try_start_0
    sget-object v0, LX/2Fa;->c:LX/2Fa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387090
    if-eqz v2, :cond_0

    .line 387091
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 387092
    new-instance p0, LX/2Fa;

    const-class v3, LX/00H;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/00H;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v4

    check-cast v4, LX/0W9;

    invoke-direct {p0, v3, v4}, LX/2Fa;-><init>(LX/00H;LX/0W9;)V

    .line 387093
    move-object v0, p0

    .line 387094
    sput-object v0, LX/2Fa;->c:LX/2Fa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387095
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387096
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387097
    :cond_1
    sget-object v0, LX/2Fa;->c:LX/2Fa;

    return-object v0

    .line 387098
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387099
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 387083
    const-string v0, "http://h.fb.com/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 387084
    const-string v1, "cid"

    iget-object v2, p0, LX/2Fa;->a:LX/00H;

    invoke-virtual {v2}, LX/00H;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 387085
    const-string v1, "locale"

    iget-object v2, p0, LX/2Fa;->b:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 387086
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
