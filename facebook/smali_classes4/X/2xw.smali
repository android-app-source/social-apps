.class public LX/2xw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/ReferenceSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2xw;


# instance fields
.field public final a:LX/0wM;


# direct methods
.method public constructor <init>(LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 479633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 479634
    iput-object p1, p0, LX/2xw;->a:LX/0wM;

    .line 479635
    return-void
.end method

.method public static a(LX/0QB;)LX/2xw;
    .locals 4

    .prologue
    .line 479636
    sget-object v0, LX/2xw;->b:LX/2xw;

    if-nez v0, :cond_1

    .line 479637
    const-class v1, LX/2xw;

    monitor-enter v1

    .line 479638
    :try_start_0
    sget-object v0, LX/2xw;->b:LX/2xw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 479639
    if-eqz v2, :cond_0

    .line 479640
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 479641
    new-instance p0, LX/2xw;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-direct {p0, v3}, LX/2xw;-><init>(LX/0wM;)V

    .line 479642
    move-object v0, p0

    .line 479643
    sput-object v0, LX/2xw;->b:LX/2xw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 479644
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 479645
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 479646
    :cond_1
    sget-object v0, LX/2xw;->b:LX/2xw;

    return-object v0

    .line 479647
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 479648
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
