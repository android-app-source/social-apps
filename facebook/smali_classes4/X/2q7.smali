.class public interface abstract LX/2q7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2q8;


# virtual methods
.method public abstract a(F)V
.end method

.method public abstract a(ILX/04g;)V
.end method

.method public abstract a(LX/04D;)V
.end method

.method public abstract a(LX/04G;)V
.end method

.method public abstract a(LX/04H;)V
.end method

.method public abstract a(LX/04g;)V
.end method

.method public abstract a(LX/04g;LX/7K4;)V
.end method

.method public abstract a(LX/2oi;LX/04g;Ljava/lang/String;)V
.end method

.method public abstract a(LX/2p8;)V
.end method

.method public abstract a(LX/2qG;)V
.end method

.method public abstract a(LX/7K7;Ljava/lang/String;LX/04g;)V
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract a(LX/7QP;)V
.end method

.method public abstract a(Landroid/graphics/RectF;)V
.end method

.method public abstract a(Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;)V
.end method

.method public abstract a(Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;)V
.end method

.method public abstract a(Lcom/facebook/video/engine/VideoPlayerParams;LX/2qi;Z)V
    .param p2    # LX/2qi;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract a(ZLX/04g;)V
.end method

.method public abstract b(LX/04g;)V
.end method

.method public abstract c()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract c(LX/04g;)V
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract d(LX/04g;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract e()LX/2oi;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract e(LX/04g;)V
.end method

.method public abstract f()V
.end method

.method public abstract g()LX/04D;
.end method

.method public abstract h()Z
.end method

.method public abstract i()Z
.end method

.method public abstract j()V
.end method

.method public abstract k()Landroid/view/View;
.end method

.method public abstract l()I
.end method

.method public abstract m()I
.end method

.method public abstract n()V
.end method

.method public abstract o()LX/7QP;
.end method

.method public abstract p()LX/7IE;
.end method

.method public abstract q()LX/16V;
.end method

.method public abstract r()Ljava/lang/String;
.end method

.method public abstract s()I
.end method

.method public abstract t()I
.end method

.method public abstract u()Z
.end method

.method public abstract v()J
.end method

.method public abstract w()Lcom/facebook/video/engine/VideoPlayerParams;
.end method
