.class public LX/2qA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-java.lang.String.length"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile j:LX/2qA;


# instance fields
.field private final b:LX/0ad;

.field private c:LX/0wq;

.field private d:LX/0ka;

.field private e:LX/0wp;

.field private final f:LX/19j;

.field private final g:LX/03V;

.field private final h:LX/19w;

.field private final i:LX/19v;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 470690
    const-class v0, LX/2qA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2qA;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0wq;LX/0ka;LX/0wp;LX/19j;LX/03V;LX/19w;LX/19v;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 470635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 470636
    iput-object p1, p0, LX/2qA;->b:LX/0ad;

    .line 470637
    iput-object p2, p0, LX/2qA;->c:LX/0wq;

    .line 470638
    iput-object p3, p0, LX/2qA;->d:LX/0ka;

    .line 470639
    iput-object p4, p0, LX/2qA;->e:LX/0wp;

    .line 470640
    iput-object p5, p0, LX/2qA;->f:LX/19j;

    .line 470641
    iput-object p6, p0, LX/2qA;->g:LX/03V;

    .line 470642
    iput-object p7, p0, LX/2qA;->h:LX/19w;

    .line 470643
    iput-object p8, p0, LX/2qA;->i:LX/19v;

    .line 470644
    return-void
.end method

.method private a(Landroid/net/Uri;Landroid/net/Uri;)LX/0H5;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 470673
    if-nez p1, :cond_0

    .line 470674
    sget-object v0, LX/0H5;->UNKNOWN:LX/0H5;

    .line 470675
    :goto_0
    return-object v0

    .line 470676
    :cond_0
    if-eqz p2, :cond_1

    move v2, v0

    .line 470677
    :goto_1
    if-eqz v2, :cond_2

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, ".mpd"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 470678
    :goto_2
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, ".m3u8"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 470679
    if-eqz v0, :cond_3

    .line 470680
    sget-object v0, LX/0H5;->DASH_LIVE:LX/0H5;

    goto :goto_0

    :cond_1
    move v2, v1

    .line 470681
    goto :goto_1

    :cond_2
    move v0, v1

    .line 470682
    goto :goto_2

    .line 470683
    :cond_3
    if-nez v2, :cond_4

    .line 470684
    iget-object v0, p0, LX/2qA;->g:LX/03V;

    sget-object v1, LX/2qA;->a:Ljava/lang/String;

    const-string v2, "preferredVideoUri is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 470685
    :goto_3
    sget-object v0, LX/0H5;->HLS_LIVE:LX/0H5;

    goto :goto_0

    .line 470686
    :cond_4
    iget-object v0, p0, LX/2qA;->g:LX/03V;

    sget-object v1, LX/2qA;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "preferredVideoUri path is"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 470687
    :cond_5
    if-eqz v0, :cond_6

    .line 470688
    sget-object v0, LX/0H5;->DASH_VOD:LX/0H5;

    goto :goto_0

    .line 470689
    :cond_6
    sget-object v0, LX/0H5;->PROGRESSIVE:LX/0H5;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2qA;
    .locals 12

    .prologue
    .line 470691
    sget-object v0, LX/2qA;->j:LX/2qA;

    if-nez v0, :cond_1

    .line 470692
    const-class v1, LX/2qA;

    monitor-enter v1

    .line 470693
    :try_start_0
    sget-object v0, LX/2qA;->j:LX/2qA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 470694
    if-eqz v2, :cond_0

    .line 470695
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 470696
    new-instance v3, LX/2qA;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0wq;->b(LX/0QB;)LX/0wq;

    move-result-object v5

    check-cast v5, LX/0wq;

    invoke-static {v0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v6

    check-cast v6, LX/0ka;

    invoke-static {v0}, LX/0wp;->a(LX/0QB;)LX/0wp;

    move-result-object v7

    check-cast v7, LX/0wp;

    invoke-static {v0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v8

    check-cast v8, LX/19j;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object v10

    check-cast v10, LX/19w;

    invoke-static {v0}, LX/19v;->a(LX/0QB;)LX/19v;

    move-result-object v11

    check-cast v11, LX/19v;

    invoke-direct/range {v3 .. v11}, LX/2qA;-><init>(LX/0ad;LX/0wq;LX/0ka;LX/0wp;LX/19j;LX/03V;LX/19w;LX/19v;)V

    .line 470697
    move-object v0, v3

    .line 470698
    sput-object v0, LX/2qA;->j:LX/2qA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 470699
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 470700
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 470701
    :cond_1
    sget-object v0, LX/2qA;->j:LX/2qA;

    return-object v0

    .line 470702
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 470703
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;ZZ)Lcom/facebook/exoplayer/ipc/VideoPlayRequest;
    .locals 17

    .prologue
    .line 470646
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2qA;->c:LX/0wq;

    iget-boolean v3, v3, LX/0wq;->H:Z

    if-eqz v3, :cond_1

    invoke-static/range {p4 .. p4}, LX/1m0;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v5

    .line 470647
    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2}, LX/2qA;->a(Landroid/net/Uri;Landroid/net/Uri;)LX/0H5;

    move-result-object v12

    .line 470648
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    .line 470649
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2qA;->h:LX/19w;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, LX/19w;->a(Ljava/lang/String;)Z

    move-result v3

    .line 470650
    if-eqz v3, :cond_5

    .line 470651
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2qA;->h:LX/19w;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, LX/19w;->f(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 470652
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2qA;->i:LX/19v;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, LX/19v;->a(Ljava/lang/String;)V

    .line 470653
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2qA;->h:LX/19w;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, LX/19w;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 470654
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2qA;->h:LX/19w;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v15}, LX/19w;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 470655
    invoke-interface {v15}, Ljava/util/Map;->size()I

    .line 470656
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, LX/2qA;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 470657
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2qA;->c:LX/0wq;

    iget v4, v4, LX/0wq;->o:I

    if-le v3, v4, :cond_3

    .line 470658
    const/4 v4, 0x0

    .line 470659
    :try_start_0
    new-instance v16, Landroid/os/MemoryFile;

    const-string v3, "ExoService_AbrManifest"

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v6

    move-object/from16 v0, v16

    invoke-direct {v0, v3, v6}, Landroid/os/MemoryFile;-><init>(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 470660
    :try_start_1
    const-string v3, "UTF-8"

    invoke-virtual {v10, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v7

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4, v6, v7}, Landroid/os/MemoryFile;->writeBytes([BIII)V

    .line 470661
    invoke-static/range {v16 .. v16}, LX/0Bz;->fdOf(Landroid/os/MemoryFile;)Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-static {v3}, Landroid/os/ParcelFileDescriptor;->dup(Ljava/io/FileDescriptor;)Landroid/os/ParcelFileDescriptor;

    move-result-object v11

    .line 470662
    new-instance v3, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;

    const/4 v10, 0x0

    move-object/from16 v4, p4

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p5

    move/from16 v13, p7

    move/from16 v14, p8

    invoke-direct/range {v3 .. v15}, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;-><init>(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;LX/0H5;ZZLjava/util/Map;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 470663
    :goto_2
    return-object v3

    .line 470664
    :cond_1
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 470665
    :catch_0
    move-exception v3

    .line 470666
    :goto_3
    sget-object v6, LX/2qA;->a:Ljava/lang/String;

    const-string v7, "Failed to create Dash manifest memory file"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v3, v7, v8}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470667
    if-eqz v4, :cond_2

    .line 470668
    invoke-virtual {v4}, Landroid/os/MemoryFile;->close()V

    .line 470669
    :cond_2
    new-instance v3, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;

    sget-object v4, LX/0H5;->DASH_LIVE:LX/0H5;

    if-ne v12, v4, :cond_4

    move-object/from16 v9, p5

    :goto_4
    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p4

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move/from16 v13, p7

    move/from16 v14, p8

    invoke-direct/range {v3 .. v15}, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;-><init>(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;LX/0H5;ZZLjava/util/Map;)V

    goto :goto_2

    .line 470670
    :cond_3
    new-instance v3, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;

    const/4 v11, 0x0

    move-object/from16 v4, p4

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p5

    move/from16 v13, p7

    move/from16 v14, p8

    invoke-direct/range {v3 .. v15}, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;-><init>(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;LX/0H5;ZZLjava/util/Map;)V

    goto :goto_2

    .line 470671
    :cond_4
    const/4 v9, 0x0

    goto :goto_4

    .line 470672
    :catch_1
    move-exception v3

    move-object/from16 v4, v16

    goto :goto_3

    :cond_5
    move-object/from16 v10, p6

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 470645
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2qA;->c:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qA;->e:LX/0wp;

    iget-object v1, p0, LX/2qA;->d:LX/0ka;

    invoke-virtual {v0, v1}, LX/0wp;->a(LX/0ka;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
