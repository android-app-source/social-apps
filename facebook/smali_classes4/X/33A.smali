.class public LX/33A;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/33A;


# instance fields
.field public final a:LX/03V;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0lC;

.field public d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;


# direct methods
.method public constructor <init>(LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 491725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491726
    iput-object p1, p0, LX/33A;->a:LX/03V;

    .line 491727
    iput-object p2, p0, LX/33A;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 491728
    iput-object p3, p0, LX/33A;->c:LX/0lC;

    .line 491729
    return-void
.end method

.method public static a(LX/0QB;)LX/33A;
    .locals 6

    .prologue
    .line 491712
    sget-object v0, LX/33A;->e:LX/33A;

    if-nez v0, :cond_1

    .line 491713
    const-class v1, LX/33A;

    monitor-enter v1

    .line 491714
    :try_start_0
    sget-object v0, LX/33A;->e:LX/33A;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 491715
    if-eqz v2, :cond_0

    .line 491716
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 491717
    new-instance p0, LX/33A;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lC;

    invoke-direct {p0, v3, v4, v5}, LX/33A;-><init>(LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;)V

    .line 491718
    move-object v0, p0

    .line 491719
    sput-object v0, LX/33A;->e:LX/33A;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 491720
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 491721
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 491722
    :cond_1
    sget-object v0, LX/33A;->e:LX/33A;

    return-object v0

    .line 491723
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 491724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/33A;Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;)V
    .locals 5

    .prologue
    .line 491671
    iput-object p1, p0, LX/33A;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    .line 491672
    const/4 v1, 0x0

    .line 491673
    :try_start_0
    iget-object v0, p0, LX/33A;->c:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 491674
    :goto_0
    if-eqz v0, :cond_0

    .line 491675
    iget-object v1, p0, LX/33A;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2bs;->o:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 491676
    :cond_0
    return-void

    .line 491677
    :catch_0
    move-exception v0

    .line 491678
    iget-object v2, p0, LX/33A;->a:LX/03V;

    const-string v3, "inline_privacy_survey_serialization"

    const-string v4, "Can\'t serialize InlinePrivacySurveyConfig"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public static d(LX/33A;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 491700
    new-instance v0, LX/3lY;

    invoke-direct {v0}, LX/3lY;-><init>()V

    const/4 v1, 0x0

    .line 491701
    iput-boolean v1, v0, LX/3lY;->a:Z

    .line 491702
    move-object v0, v0

    .line 491703
    iput-object v2, v0, LX/3lY;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 491704
    move-object v0, v0

    .line 491705
    iput-object v2, v0, LX/3lY;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 491706
    move-object v0, v0

    .line 491707
    iput-object v2, v0, LX/3lY;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 491708
    move-object v0, v0

    .line 491709
    invoke-virtual {v0}, LX/3lY;->a()Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    move-result-object v0

    .line 491710
    invoke-static {p0, v0}, LX/33A;->a(LX/33A;Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;)V

    .line 491711
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;
    .locals 4

    .prologue
    .line 491691
    iget-object v0, p0, LX/33A;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    if-nez v0, :cond_0

    .line 491692
    iget-object v0, p0, LX/33A;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2bs;->o:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 491693
    if-nez v0, :cond_1

    .line 491694
    invoke-static {p0}, LX/33A;->d(LX/33A;)V

    .line 491695
    :cond_0
    :goto_0
    iget-object v0, p0, LX/33A;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    return-object v0

    .line 491696
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/33A;->c:LX/0lC;

    const-class v2, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    iput-object v0, p0, LX/33A;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 491697
    :catch_0
    move-exception v0

    .line 491698
    iget-object v1, p0, LX/33A;->a:LX/03V;

    const-string v2, "inline_privacy_survey_deserialization"

    const-string v3, "Can\'t deserialize InlinePrivacySurveyConfig"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 491699
    invoke-static {p0}, LX/33A;->d(LX/33A;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 491679
    new-instance v0, LX/3lY;

    invoke-direct {v0}, LX/3lY;-><init>()V

    const/4 v1, 0x0

    .line 491680
    iput-boolean v1, v0, LX/3lY;->a:Z

    .line 491681
    move-object v0, v0

    .line 491682
    iput-object v2, v0, LX/3lY;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 491683
    move-object v0, v0

    .line 491684
    iput-object v2, v0, LX/3lY;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 491685
    move-object v0, v0

    .line 491686
    iput-object v2, v0, LX/3lY;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 491687
    move-object v0, v0

    .line 491688
    invoke-virtual {v0}, LX/3lY;->a()Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    move-result-object v0

    .line 491689
    invoke-static {p0, v0}, LX/33A;->a(LX/33A;Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;)V

    .line 491690
    return-void
.end method
