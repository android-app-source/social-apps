.class public interface abstract LX/2vL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ITERATOR:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a(Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;)I"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ")",
            "Lcom/facebook/flatbuffers/MutableFlattenable;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/Collection;)Ljava/lang/Object;
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)TITERATOR;"
        }
    .end annotation
.end method

.method public abstract a([J)Ljava/lang/Object;
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J)TITERATOR;"
        }
    .end annotation
.end method

.method public abstract a()V
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;I)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;",
            "Ljava/lang/String;",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;Ljava/util/Set;)Z
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/Object;Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ")",
            "Lcom/facebook/flatbuffers/MutableFlattenable;"
        }
    .end annotation
.end method

.method public abstract b()V
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation
.end method

.method public abstract b(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;)V"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;",
            "Ljava/lang/String;",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ")V"
        }
    .end annotation
.end method

.method public abstract c(Ljava/lang/Object;)Ljava/lang/String;
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public abstract c()V
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation
.end method

.method public abstract d(Ljava/lang/Object;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;)",
            "Lcom/facebook/flatbuffers/MutableFlattenable;"
        }
    .end annotation
.end method

.method public abstract d()Ljava/lang/String;
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation
.end method

.method public abstract e(Ljava/lang/Object;)Z
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;)Z"
        }
    .end annotation
.end method

.method public abstract f(Ljava/lang/Object;)V
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;)V"
        }
    .end annotation
.end method

.method public abstract g(Ljava/lang/Object;)V
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;)V"
        }
    .end annotation
.end method

.method public abstract h(Ljava/lang/Object;)V
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TITERATOR;)V"
        }
    .end annotation
.end method
