.class public LX/301;
.super LX/300;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/300",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;LX/0Rl;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;TV;>;",
            "LX/0Rl",
            "<-",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 484009
    invoke-direct {p0, p1, p2}, LX/300;-><init>(Ljava/util/Map;LX/0Rl;)V

    .line 484010
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, LX/300;->b:LX/0Rl;

    invoke-static {v0, v1}, LX/0RA;->a(Ljava/util/Set;LX/0Rl;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/301;->c:Ljava/util/Set;

    .line 484011
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 484012
    new-instance v0, LX/304;

    invoke-direct {v0, p0}, LX/304;-><init>(LX/301;)V

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 484013
    new-instance v0, LX/2IC;

    invoke-direct {v0, p0}, LX/2IC;-><init>(LX/301;)V

    return-object v0
.end method
