.class public LX/38h;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/38h;


# instance fields
.field public final b:LX/0tX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 521232
    const-class v0, LX/38h;

    sput-object v0, LX/38h;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521234
    iput-object p1, p0, LX/38h;->b:LX/0tX;

    .line 521235
    return-void
.end method

.method public static a(LX/0QB;)LX/38h;
    .locals 4

    .prologue
    .line 521236
    sget-object v0, LX/38h;->c:LX/38h;

    if-nez v0, :cond_1

    .line 521237
    const-class v1, LX/38h;

    monitor-enter v1

    .line 521238
    :try_start_0
    sget-object v0, LX/38h;->c:LX/38h;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 521239
    if-eqz v2, :cond_0

    .line 521240
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 521241
    new-instance p0, LX/38h;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {p0, v3}, LX/38h;-><init>(LX/0tX;)V

    .line 521242
    move-object v0, p0

    .line 521243
    sput-object v0, LX/38h;->c:LX/38h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521244
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 521245
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 521246
    :cond_1
    sget-object v0, LX/38h;->c:LX/38h;

    return-object v0

    .line 521247
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 521248
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/1Zp;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 521249
    new-instance v0, LX/7JQ;

    invoke-direct {v0}, LX/7JQ;-><init>()V

    move-object v0, v0

    .line 521250
    const-string v1, "targetID"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 521251
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 521252
    iget-object v1, p0, LX/38h;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
