.class public LX/2nQ;
.super Landroid/text/SpannableStringBuilder;
.source ""

# interfaces
.implements LX/2nR;
.implements LX/1oG;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/34S;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/2nS;

.field public c:Landroid/view/View;

.field public d:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 464589
    invoke-direct {p0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 464590
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2nQ;->a:Ljava/util/Set;

    .line 464591
    new-instance v0, LX/2nS;

    invoke-direct {v0, p0}, LX/2nS;-><init>(LX/2nQ;)V

    iput-object v0, p0, LX/2nQ;->b:LX/2nS;

    .line 464592
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 464585
    invoke-direct {p0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 464586
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2nQ;->a:Ljava/util/Set;

    .line 464587
    new-instance v0, LX/2nS;

    invoke-direct {v0, p0}, LX/2nS;-><init>(LX/2nQ;)V

    iput-object v0, p0, LX/2nQ;->b:LX/2nS;

    .line 464588
    return-void
.end method

.method private a()V
    .locals 2
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 464581
    iget-object v0, p0, LX/2nQ;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/34S;

    .line 464582
    iget-object p0, v0, LX/34S;->a:LX/1aX;

    invoke-virtual {p0}, LX/1aX;->d()V

    .line 464583
    goto :goto_0

    .line 464584
    :cond_0
    return-void
.end method

.method private b()V
    .locals 2
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 464552
    iget-object v0, p0, LX/2nQ;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/34S;

    .line 464553
    iget-object p0, v0, LX/34S;->a:LX/1aX;

    invoke-virtual {p0}, LX/1aX;->f()V

    .line 464554
    goto :goto_0

    .line 464555
    :cond_0
    return-void
.end method

.method public static c(LX/2nQ;)V
    .locals 1

    .prologue
    .line 464576
    iget-object v0, p0, LX/2nQ;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 464577
    iget-object v0, p0, LX/2nQ;->c:Landroid/view/View;

    invoke-direct {p0, v0}, LX/2nQ;->d(Landroid/view/View;)V

    .line 464578
    :cond_0
    iget-object v0, p0, LX/2nQ;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 464579
    iget-object v0, p0, LX/2nQ;->d:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, LX/2nQ;->d(Landroid/graphics/drawable/Drawable;)V

    .line 464580
    :cond_1
    return-void
.end method

.method private d(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 464573
    iget-object v0, p0, LX/2nQ;->d:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    .line 464574
    :goto_0
    return-void

    .line 464575
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/2nQ;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private d(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 464570
    iget-object v0, p0, LX/2nQ;->c:Landroid/view/View;

    if-eq p1, v0, :cond_0

    .line 464571
    :goto_0
    return-void

    .line 464572
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/2nQ;->c:Landroid/view/View;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 464566
    invoke-static {p0}, LX/2nQ;->c(LX/2nQ;)V

    .line 464567
    iput-object p1, p0, LX/2nQ;->d:Landroid/graphics/drawable/Drawable;

    .line 464568
    invoke-direct {p0}, LX/2nQ;->a()V

    .line 464569
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 464562
    invoke-static {p0}, LX/2nQ;->c(LX/2nQ;)V

    .line 464563
    iput-object p1, p0, LX/2nQ;->c:Landroid/view/View;

    .line 464564
    invoke-direct {p0}, LX/2nQ;->a()V

    .line 464565
    return-void
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 464559
    invoke-direct {p0, p1}, LX/2nQ;->d(Landroid/graphics/drawable/Drawable;)V

    .line 464560
    invoke-direct {p0}, LX/2nQ;->b()V

    .line 464561
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 464556
    invoke-direct {p0, p1}, LX/2nQ;->d(Landroid/view/View;)V

    .line 464557
    invoke-direct {p0}, LX/2nQ;->b()V

    .line 464558
    return-void
.end method
