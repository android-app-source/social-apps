.class public final LX/2Kv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:LX/0ad;

.field private final b:Landroid/content/Context;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Axt;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Axr;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AyE;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AyG;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Sh;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Axs;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0bH;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Axq;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BOo;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ml;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0ad;Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Sh;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;)V
    .locals 0
    .param p2    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/Axt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Axr;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AyE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AyG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Ot",
            "<",
            "LX/Axs;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0bH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Axq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BOo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ml;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 394035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394036
    iput-object p1, p0, LX/2Kv;->a:LX/0ad;

    .line 394037
    iput-object p2, p0, LX/2Kv;->b:Landroid/content/Context;

    .line 394038
    iput-object p3, p0, LX/2Kv;->c:LX/0Ot;

    .line 394039
    iput-object p4, p0, LX/2Kv;->d:LX/0Ot;

    .line 394040
    iput-object p5, p0, LX/2Kv;->e:LX/0Ot;

    .line 394041
    iput-object p6, p0, LX/2Kv;->f:LX/0Ot;

    .line 394042
    iput-object p7, p0, LX/2Kv;->g:LX/0Ot;

    .line 394043
    iput-object p8, p0, LX/2Kv;->h:LX/0Sh;

    .line 394044
    iput-object p9, p0, LX/2Kv;->i:LX/0Ot;

    .line 394045
    iput-object p10, p0, LX/2Kv;->j:LX/0Ot;

    .line 394046
    iput-object p11, p0, LX/2Kv;->k:LX/0Ot;

    .line 394047
    iput-object p12, p0, LX/2Kv;->l:LX/0Ot;

    .line 394048
    iput-object p13, p0, LX/2Kv;->m:LX/0Ot;

    .line 394049
    iput-object p14, p0, LX/2Kv;->n:LX/0Uh;

    .line 394050
    return-void
.end method

.method public static b(LX/0QB;)LX/2Kv;
    .locals 15

    .prologue
    .line 394086
    new-instance v0, LX/2Kv;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    const-class v2, Landroid/content/Context;

    const-class v3, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v2, v3}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const/16 v3, 0x22c0

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x22be

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x22c5

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x22c7

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2e3

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    const/16 v9, 0x22bf

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x787

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x22bd

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x35b2

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x1127

    invoke-static {p0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v14

    check-cast v14, LX/0Uh;

    invoke-direct/range {v0 .. v14}, LX/2Kv;-><init>(LX/0ad;Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Sh;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;)V

    .line 394087
    return-object v0
.end method


# virtual methods
.method public final init()V
    .locals 13

    .prologue
    const/4 v3, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 394051
    iget-object v0, p0, LX/2Kv;->h:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 394052
    iget-object v0, p0, LX/2Kv;->n:LX/0Uh;

    const/16 v1, 0x32f

    invoke-virtual {v0, v1, v7}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 394053
    :cond_0
    :goto_0
    return-void

    .line 394054
    :cond_1
    iget-object v0, p0, LX/2Kv;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ml;

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394055
    iget-object v0, p0, LX/2Kv;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AyE;

    .line 394056
    iget-object v1, p0, LX/2Kv;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AyG;

    .line 394057
    invoke-virtual {v0}, LX/AyE;->c()I

    move-result v2

    if-eq v2, v3, :cond_2

    .line 394058
    invoke-virtual {v0}, LX/AyE;->c()I

    .line 394059
    invoke-virtual {v1}, LX/AyG;->b()I

    .line 394060
    invoke-virtual {v0}, LX/AyE;->b()I

    .line 394061
    iget-object v1, v0, LX/AyE;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/AyE;->a:LX/0Tn;

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 394062
    :goto_1
    iget-object v0, p0, LX/2Kv;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axt;

    iget-object v1, v0, LX/Axt;->p:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v0, p0, LX/2Kv;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axt;

    invoke-virtual {v0}, LX/Axt;->a()LX/0Px;

    move-result-object v0

    const v2, 0x4bab157a    # 2.2424308E7f

    invoke-static {v1, v0, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 394063
    iget-object v0, p0, LX/2Kv;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axq;

    .line 394064
    iget-object v1, p0, LX/2Kv;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0bH;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 394065
    iget-object v1, p0, LX/2Kv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, LX/Axt;->b:Landroid/net/Uri;

    iget-object v1, p0, LX/2Kv;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v6, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 394066
    iget-object v1, p0, LX/2Kv;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Axs;

    invoke-virtual {v1}, LX/Axs;->startWatching()V

    .line 394067
    iput-boolean v7, v0, LX/Axq;->a:Z

    .line 394068
    goto/16 :goto_0

    .line 394069
    :cond_2
    iget-object v2, p0, LX/2Kv;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    iget-object v2, p0, LX/2Kv;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BOo;

    invoke-virtual {v2}, LX/BOo;->b()J

    move-result-wide v2

    sub-long v2, v4, v2

    .line 394070
    sget-object v4, LX/AyI;->b:LX/0U1;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0U1;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 394071
    iget-object v5, v0, LX/AyE;->b:LX/AyH;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v8, "models"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v8, v9, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 394072
    iget-object v8, v1, LX/AyG;->b:LX/0Sh;

    invoke-virtual {v8}, LX/0Sh;->b()V

    .line 394073
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-ltz v8, :cond_3

    const/4 v8, 0x1

    :goto_2
    const-string v9, "Please give us a valid UNIX timestamp"

    invoke-static {v8, v9}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 394074
    sget-object v8, LX/AyK;->b:LX/0U1;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/0U1;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v8

    .line 394075
    iget-object v9, v1, LX/AyG;->a:LX/AyH;

    invoke-virtual {v9}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    const-string v10, "prompted_models"

    invoke-virtual {v8}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v10, v11, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 394076
    :try_start_0
    iget-object v2, p0, LX/2Kv;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Axt;

    invoke-virtual {v0}, LX/AyE;->a()LX/0Rf;

    move-result-object v3

    .line 394077
    iget-object v4, v2, LX/Axt;->i:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 394078
    invoke-virtual {v3}, LX/0Rf;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 394079
    :goto_3
    iget-object v2, p0, LX/2Kv;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Axt;

    iget-object v2, v2, LX/Axt;->i:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 394080
    :catch_0
    move-exception v2

    .line 394081
    sget-object v3, LX/Axt;->a:Ljava/lang/String;

    const-string v4, "Error Reading from DB. You may have to update the model version"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 394082
    invoke-virtual {v0}, LX/AyE;->b()I

    .line 394083
    invoke-virtual {v1}, LX/AyG;->b()I

    goto/16 :goto_1

    .line 394084
    :cond_3
    const/4 v8, 0x0

    goto :goto_2

    .line 394085
    :cond_4
    iget-object v4, v2, LX/Axt;->i:Ljava/util/Map;

    new-instance v5, LX/Az9;

    invoke-virtual {v3}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v8

    invoke-direct {v5, v8}, LX/Az9;-><init>(LX/0Rc;)V

    new-instance v8, LX/Axn;

    invoke-direct {v8, v2}, LX/Axn;-><init>(LX/Axt;)V

    invoke-static {v5, v8}, LX/0PM;->a(Ljava/util/Iterator;LX/0QK;)LX/0P1;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_3
.end method
