.class public LX/3Px;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Pw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            "LX/0Tn;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile y:LX/3Px;


# instance fields
.field public final c:LX/0Zb;

.field private final d:LX/0Sh;

.field private final e:Landroid/content/Context;

.field public final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final g:LX/2do;

.field private final h:LX/0ti;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0xB;

.field private final k:LX/3Py;

.field public final l:LX/2PY;

.field public final m:LX/1rx;

.field private final n:LX/2Yg;

.field public final o:LX/3Q0;

.field private final p:LX/3Q2;

.field private final q:LX/2bC;

.field public final r:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

.field public final s:LX/2c4;

.field private final t:LX/3QD;

.field public final u:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final v:LX/3QE;

.field private final w:LX/17d;

.field private x:LX/0xA;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 562498
    const-class v0, LX/3Px;

    sput-object v0, LX/3Px;->a:Ljava/lang/Class;

    .line 562499
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->WALL:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->q:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->SHARE_WALL_CREATE:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->q:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->FRIEND:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->t:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->FRIEND_CONFIRMED:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->u:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->PHOTO_TAG:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->v:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->PHOTO_TAG_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->v:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->PHOTO_TAGGED_BY_NON_OWNER:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->v:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->EVENT_INVITE:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->w:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->PLAN_USER_INVITED:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->w:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->ITEM_COMMENT:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->s:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->ITEM_REPLY:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->s:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->COMMENT_MENTION:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->s:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->APP_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->x:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->GROUP_ACTIVITY:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->y:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->PLACE_FEED_NEARBY:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, LX/0hM;->z:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/3Px;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0Sh;Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2do;LX/0ti;LX/0Or;LX/0xB;LX/3Py;LX/1rx;LX/2PY;LX/2Yg;LX/3Q0;LX/3Q2;LX/2bC;Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;LX/2c4;LX/3QD;LX/0Or;LX/3QE;LX/17d;)V
    .locals 1
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/notification/settings/IsGlobalNotificationPreferenceEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Landroid/content/Context;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2do;",
            "LX/0ti;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0xB;",
            "LX/3Py;",
            "LX/1rx;",
            "LX/2PY;",
            "LX/2Yg;",
            "Lcom/facebook/notifications/lockscreen/util/PushNotificationIntentHelper;",
            "LX/3Q2;",
            "LX/2bC;",
            "Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;",
            "LX/2c4;",
            "LX/3QD;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/3QE;",
            "LX/17d;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562566
    iput-object p1, p0, LX/3Px;->c:LX/0Zb;

    .line 562567
    iput-object p2, p0, LX/3Px;->d:LX/0Sh;

    .line 562568
    iput-object p3, p0, LX/3Px;->e:Landroid/content/Context;

    .line 562569
    iput-object p4, p0, LX/3Px;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 562570
    iput-object p5, p0, LX/3Px;->g:LX/2do;

    .line 562571
    iput-object p6, p0, LX/3Px;->h:LX/0ti;

    .line 562572
    iput-object p7, p0, LX/3Px;->i:LX/0Or;

    .line 562573
    iput-object p8, p0, LX/3Px;->j:LX/0xB;

    .line 562574
    iput-object p9, p0, LX/3Px;->k:LX/3Py;

    .line 562575
    iput-object p11, p0, LX/3Px;->l:LX/2PY;

    .line 562576
    iput-object p10, p0, LX/3Px;->m:LX/1rx;

    .line 562577
    iput-object p12, p0, LX/3Px;->n:LX/2Yg;

    .line 562578
    iput-object p13, p0, LX/3Px;->o:LX/3Q0;

    .line 562579
    iput-object p14, p0, LX/3Px;->p:LX/3Q2;

    .line 562580
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3Px;->q:LX/2bC;

    .line 562581
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3Px;->r:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    .line 562582
    move-object/from16 v0, p17

    iput-object v0, p0, LX/3Px;->s:LX/2c4;

    .line 562583
    move-object/from16 v0, p18

    iput-object v0, p0, LX/3Px;->t:LX/3QD;

    .line 562584
    move-object/from16 v0, p19

    iput-object v0, p0, LX/3Px;->u:LX/0Or;

    .line 562585
    move-object/from16 v0, p20

    iput-object v0, p0, LX/3Px;->v:LX/3QE;

    .line 562586
    move-object/from16 v0, p21

    iput-object v0, p0, LX/3Px;->w:LX/17d;

    .line 562587
    invoke-direct {p0}, LX/3Px;->b()V

    .line 562588
    return-void
.end method

.method public static a(LX/0QB;)LX/3Px;
    .locals 3

    .prologue
    .line 562555
    sget-object v0, LX/3Px;->y:LX/3Px;

    if-nez v0, :cond_1

    .line 562556
    const-class v1, LX/3Px;

    monitor-enter v1

    .line 562557
    :try_start_0
    sget-object v0, LX/3Px;->y:LX/3Px;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 562558
    if-eqz v2, :cond_0

    .line 562559
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/3Px;->b(LX/0QB;)LX/3Px;

    move-result-object v0

    sput-object v0, LX/3Px;->y:LX/3Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 562560
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 562561
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 562562
    :cond_1
    sget-object v0, LX/3Px;->y:LX/3Px;

    return-object v0

    .line 562563
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 562564
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/3Px;Ljava/lang/String;Lcom/facebook/push/PushProperty;)V
    .locals 7

    .prologue
    .line 562553
    iget-object v0, p0, LX/3Px;->q:LX/2bC;

    iget-object v1, p2, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v1}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, ""

    const-string v6, ""

    move-object v3, p1

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 562554
    return-void
.end method

.method private a(Lcom/facebook/notifications/model/SystemTrayNotification;Lcom/facebook/push/PushProperty;)Z
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 562500
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v0

    .line 562501
    sget-object v3, Lcom/facebook/notifications/constants/NotificationType;->NEKO_INSTALL_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

    if-ne v3, v0, :cond_1

    .line 562502
    iget-object v3, p0, LX/3Px;->w:LX/17d;

    iget-object v4, p0, LX/3Px;->e:Landroid/content/Context;

    iget-object v5, p1, Lcom/facebook/notifications/model/SystemTrayNotification;->mHref:Ljava/lang/String;

    const/4 v8, 0x1

    .line 562503
    invoke-static {v5}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 562504
    invoke-virtual {v3, v6}, LX/17d;->b(Landroid/net/Uri;)Ljava/util/Collection;

    move-result-object v7

    .line 562505
    const/4 v6, 0x0

    .line 562506
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v7, v6

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 562507
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 562508
    const/4 v11, 0x1

    :try_start_0
    invoke-virtual {v10, v6, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v7, v8

    .line 562509
    goto :goto_0

    .line 562510
    :catch_0
    goto :goto_0

    .line 562511
    :cond_0
    move v3, v7

    .line 562512
    if-nez v3, :cond_1

    move v0, v1

    .line 562513
    :goto_1
    return v0

    .line 562514
    :cond_1
    iget-object v3, p0, LX/3Px;->e:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/facebook/katana/service/AppSession;->b(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v3

    .line 562515
    sget-object v4, Lcom/facebook/notifications/constants/NotificationType;->PRE_REG_PUSH:Lcom/facebook/notifications/constants/NotificationType;

    if-ne v4, v0, :cond_3

    .line 562516
    if-nez v3, :cond_2

    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1

    .line 562517
    :cond_3
    if-nez v3, :cond_4

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->p()Z

    move-result v0

    if-nez v0, :cond_4

    .line 562518
    const-string v0, "logged_out_user"

    invoke-static {p0, v0, p2}, LX/3Px;->a(LX/3Px;Ljava/lang/String;Lcom/facebook/push/PushProperty;)V

    move v0, v1

    .line 562519
    goto :goto_1

    .line 562520
    :cond_4
    if-eqz v3, :cond_5

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->p()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 562521
    const-string v0, "logged_in_user"

    invoke-static {p0, v0, p2}, LX/3Px;->a(LX/3Px;Ljava/lang/String;Lcom/facebook/push/PushProperty;)V

    move v0, v1

    .line 562522
    goto :goto_1

    .line 562523
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->D()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->C()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/3Px;->p:LX/3Q2;

    invoke-virtual {v3}, LX/3Q2;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 562524
    const-string v0, "eaten_wrong_user"

    invoke-static {p0, v0, p2}, LX/3Px;->a(LX/3Px;Ljava/lang/String;Lcom/facebook/push/PushProperty;)V

    move v0, v1

    .line 562525
    goto :goto_1

    .line 562526
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->a(Lcom/facebook/notifications/constants/NotificationType;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 562527
    iget-object v0, p0, LX/3Px;->n:LX/2Yg;

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v2

    sget-object v3, LX/3B2;->DROPPED_BY_WHITELIST_TYPES:LX/3B2;

    invoke-virtual {v0, v2, v3}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/3B2;)V

    move v0, v1

    .line 562528
    goto :goto_1

    .line 562529
    :cond_7
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 562530
    iget-object v4, p0, LX/3Px;->l:LX/2PY;

    .line 562531
    iget-object v5, v4, LX/2PY;->a:LX/0ad;

    sget-short v6, LX/15r;->w:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    move v4, v5

    .line 562532
    if-nez v4, :cond_d

    move v4, v3

    .line 562533
    :goto_2
    if-eqz v4, :cond_e

    .line 562534
    :cond_8
    :goto_3
    move v0, v0

    .line 562535
    if-eqz v0, :cond_9

    move v0, v2

    .line 562536
    goto :goto_1

    .line 562537
    :cond_9
    iget-object v0, p0, LX/3Px;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_a

    .line 562538
    iget-object v0, p0, LX/3Px;->n:LX/2Yg;

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v3

    sget-object v4, LX/3B2;->DROPPED_BY_OVERALL_SETTING:LX/3B2;

    invoke-virtual {v0, v3, v4}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/3B2;)V

    .line 562539
    iget-object v0, p0, LX/3Px;->k:LX/3Py;

    invoke-virtual {v0, v2}, LX/3Py;->a(Z)V

    move v0, v1

    .line 562540
    goto/16 :goto_1

    .line 562541
    :cond_a
    sget-object v0, LX/3Px;->b:LX/0P1;

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 562542
    iget-object v3, p0, LX/3Px;->v:LX/3QE;

    invoke-virtual {v3}, LX/3QE;->a()Z

    move-result v3

    if-eqz v3, :cond_b

    move v0, v2

    .line 562543
    goto/16 :goto_1

    .line 562544
    :cond_b
    if-eqz v0, :cond_c

    iget-object v3, p0, LX/3Px;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_c

    .line 562545
    iget-object v0, p0, LX/3Px;->n:LX/2Yg;

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v2

    sget-object v3, LX/3B2;->DROPPED_BY_INDIVIDUAL_SETTING:LX/3B2;

    invoke-virtual {v0, v2, v3}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/3B2;)V

    move v0, v1

    .line 562546
    goto/16 :goto_1

    :cond_c
    move v0, v2

    .line 562547
    goto/16 :goto_1

    :cond_d
    move v4, v0

    .line 562548
    goto :goto_2

    .line 562549
    :cond_e
    iget-object v4, p0, LX/3Px;->l:LX/2PY;

    .line 562550
    iget-object v5, v4, LX/2PY;->a:LX/0ad;

    sget-short v6, LX/15r;->t:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    move v5, v5

    .line 562551
    if-nez v5, :cond_f

    invoke-virtual {v4}, LX/2PY;->a()Z

    move-result v5

    if-eqz v5, :cond_10

    :cond_f
    const/4 v5, 0x1

    :goto_4
    move v4, v5

    .line 562552
    if-eqz v4, :cond_8

    iget-object v4, p0, LX/3Px;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0hM;->L:LX/0Tn;

    invoke-interface {v4, v5, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    if-eqz v4, :cond_8

    move v0, v3

    goto :goto_3

    :cond_10
    const/4 v5, 0x0

    goto :goto_4
.end method

.method private static b(LX/0QB;)LX/3Px;
    .locals 24

    .prologue
    .line 562589
    new-instance v2, LX/3Px;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    const-class v5, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v7

    check-cast v7, LX/2do;

    invoke-static/range {p0 .. p0}, LX/0ti;->a(LX/0QB;)LX/0ti;

    move-result-object v8

    check-cast v8, LX/0ti;

    const/16 v9, 0x153a

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v10

    check-cast v10, LX/0xB;

    invoke-static/range {p0 .. p0}, LX/3Py;->a(LX/0QB;)LX/3Py;

    move-result-object v11

    check-cast v11, LX/3Py;

    invoke-static/range {p0 .. p0}, LX/1rx;->a(LX/0QB;)LX/1rx;

    move-result-object v12

    check-cast v12, LX/1rx;

    invoke-static/range {p0 .. p0}, LX/2PY;->a(LX/0QB;)LX/2PY;

    move-result-object v13

    check-cast v13, LX/2PY;

    invoke-static/range {p0 .. p0}, LX/2Yg;->a(LX/0QB;)LX/2Yg;

    move-result-object v14

    check-cast v14, LX/2Yg;

    invoke-static/range {p0 .. p0}, LX/3Q0;->a(LX/0QB;)LX/3Q0;

    move-result-object v15

    check-cast v15, LX/3Q0;

    invoke-static/range {p0 .. p0}, LX/3Q2;->a(LX/0QB;)LX/3Q2;

    move-result-object v16

    check-cast v16, LX/3Q2;

    invoke-static/range {p0 .. p0}, LX/2bC;->a(LX/0QB;)LX/2bC;

    move-result-object v17

    check-cast v17, LX/2bC;

    invoke-static/range {p0 .. p0}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a(LX/0QB;)Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    move-result-object v18

    check-cast v18, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    invoke-static/range {p0 .. p0}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v19

    check-cast v19, LX/2c4;

    invoke-static/range {p0 .. p0}, LX/3QD;->a(LX/0QB;)LX/3QD;

    move-result-object v20

    check-cast v20, LX/3QD;

    const/16 v21, 0x19e

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v21

    invoke-static/range {p0 .. p0}, LX/3QE;->a(LX/0QB;)LX/3QE;

    move-result-object v22

    check-cast v22, LX/3QE;

    invoke-static/range {p0 .. p0}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v23

    check-cast v23, LX/17d;

    invoke-direct/range {v2 .. v23}, LX/3Px;-><init>(LX/0Zb;LX/0Sh;Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2do;LX/0ti;LX/0Or;LX/0xB;LX/3Py;LX/1rx;LX/2PY;LX/2Yg;LX/3Q0;LX/3Q2;LX/2bC;Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;LX/2c4;LX/3QD;LX/0Or;LX/3QE;LX/17d;)V

    .line 562590
    return-object v2
.end method

.method private b()V
    .locals 2

    .prologue
    .line 562386
    new-instance v0, LX/2qv;

    invoke-direct {v0, p0}, LX/2qv;-><init>(LX/3Px;)V

    iput-object v0, p0, LX/3Px;->x:LX/0xA;

    .line 562387
    iget-object v0, p0, LX/3Px;->j:LX/0xB;

    iget-object v1, p0, LX/3Px;->x:LX/0xA;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/0xA;)V

    .line 562388
    return-void
.end method

.method public static c(LX/3Px;Lcom/facebook/notifications/model/SystemTrayNotification;)V
    .locals 14

    .prologue
    .line 562389
    if-nez p1, :cond_1

    .line 562390
    :cond_0
    :goto_0
    return-void

    .line 562391
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->f()LX/0am;

    move-result-object v2

    .line 562392
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562393
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 562394
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->FRIEND:Lcom/facebook/notifications/constants/NotificationType;

    if-ne v0, v1, :cond_2

    .line 562395
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v1, v0

    .line 562396
    :goto_1
    :try_start_0
    iget-object v3, p0, LX/3Px;->h:LX/0ti;

    new-instance v6, LX/Jbk;

    invoke-direct {v6}, LX/Jbk;-><init>()V

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 562397
    iput-object v0, v6, LX/Jbk;->b:Ljava/lang/String;

    .line 562398
    move-object v0, v6

    .line 562399
    iput-object v1, v0, LX/Jbk;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 562400
    move-object v0, v0

    .line 562401
    const/4 v11, 0x1

    const/4 v13, 0x0

    const/4 v9, 0x0

    .line 562402
    new-instance v7, LX/186;

    const/16 v8, 0x80

    invoke-direct {v7, v8}, LX/186;-><init>(I)V

    .line 562403
    iget-object v8, v0, LX/Jbk;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v7, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 562404
    iget-object v10, v0, LX/Jbk;->b:Ljava/lang/String;

    invoke-virtual {v7, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 562405
    const/4 v12, 0x2

    invoke-virtual {v7, v12}, LX/186;->c(I)V

    .line 562406
    invoke-virtual {v7, v13, v8}, LX/186;->b(II)V

    .line 562407
    invoke-virtual {v7, v11, v10}, LX/186;->b(II)V

    .line 562408
    invoke-virtual {v7}, LX/186;->d()I

    move-result v8

    .line 562409
    invoke-virtual {v7, v8}, LX/186;->d(I)V

    .line 562410
    invoke-virtual {v7}, LX/186;->e()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 562411
    invoke-virtual {v8, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 562412
    new-instance v7, LX/15i;

    move-object v10, v9

    move-object v12, v9

    invoke-direct/range {v7 .. v12}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 562413
    new-instance v8, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;

    invoke-direct {v8, v7}, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandlerGraphQLModels$PostToFriendingEventBusGraphQLModel;-><init>(LX/15i;)V

    .line 562414
    move-object v0, v8

    .line 562415
    invoke-virtual {v3, v0}, LX/0ti;->a(LX/0jT;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 562416
    :goto_2
    iget-object v0, p0, LX/3Px;->d:LX/0Sh;

    new-instance v2, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandler$2;

    invoke-direct {v2, p0, v4, v5, v1}, Lcom/facebook/katana/push/fbpushdata/FbandroidFbPushDataHandler$2;-><init>(LX/3Px;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    invoke-virtual {v0, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 562417
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->FRIEND_CONFIRMED:Lcom/facebook/notifications/constants/NotificationType;

    if-ne v0, v1, :cond_0

    .line 562418
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v1, v0

    goto :goto_1

    :catch_0
    goto :goto_2
.end method


# virtual methods
.method public final a()LX/2QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 562419
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0lF;Lcom/facebook/push/PushProperty;)V
    .locals 9

    .prologue
    .line 562420
    :try_start_0
    iget-object v0, p0, LX/3Px;->t:LX/3QD;

    invoke-virtual {v0, p1}, LX/3QD;->a(LX/0lF;)Lcom/facebook/notifications/model/SystemTrayNotification;

    move-result-object v0

    iget-object v1, p2, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/model/SystemTrayNotification;->a(Ljava/lang/String;)Lcom/facebook/notifications/model/SystemTrayNotification;

    move-result-object v0

    iget-object v1, p2, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v1}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/model/SystemTrayNotification;->b(Ljava/lang/String;)Lcom/facebook/notifications/model/SystemTrayNotification;

    move-result-object v0

    iget-wide v2, p2, Lcom/facebook/push/PushProperty;->c:J

    invoke-virtual {v0, v2, v3}, Lcom/facebook/notifications/model/SystemTrayNotification;->a(J)Lcom/facebook/notifications/model/SystemTrayNotification;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 562421
    sget-object v1, LX/3QZ;->a:LX/2QP;

    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2QP;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 562422
    iget-object v1, p0, LX/3Px;->n:LX/2Yg;

    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v2

    sget-object v3, LX/3B2;->PUSH_NOTIFICATION_RECEIVED:LX/3B2;

    invoke-virtual {v1, v2, v3}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/3B2;)V

    .line 562423
    :cond_0
    invoke-direct {p0, v0, p2}, LX/3Px;->a(Lcom/facebook/notifications/model/SystemTrayNotification;Lcom/facebook/push/PushProperty;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 562424
    :goto_0
    return-void

    .line 562425
    :catch_0
    move-exception v0

    .line 562426
    sget-object v1, LX/3Px;->a:Ljava/lang/Class;

    const-string v2, "IOException"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 562427
    iget-object v1, p0, LX/3Px;->q:LX/2bC;

    iget-object v2, p2, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v2}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p2, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 562428
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v1

    .line 562429
    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->FRIEND:Lcom/facebook/notifications/constants/NotificationType;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->FRIEND_CONFIRMED:Lcom/facebook/notifications/constants/NotificationType;

    if-ne v1, v2, :cond_3

    .line 562430
    :cond_2
    invoke-static {p0, v0}, LX/3Px;->c(LX/3Px;Lcom/facebook/notifications/model/SystemTrayNotification;)V

    .line 562431
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v2

    .line 562432
    sget-object v3, LX/Jbi;->a:[I

    invoke-virtual {v2}, Lcom/facebook/notifications/constants/NotificationType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sparse-switch v3, :sswitch_data_0

    .line 562433
    const v3, 0x7f0218e4

    :goto_1
    move v4, v3

    .line 562434
    iget-object v1, p0, LX/3Px;->o:LX/3Q0;

    .line 562435
    invoke-static {v1, v0}, LX/3Q0;->d(LX/3Q0;Lcom/facebook/notifications/model/SystemTrayNotification;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 562436
    const/4 v5, 0x0

    .line 562437
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 562438
    if-eqz v3, :cond_b

    .line 562439
    iget-object v6, v1, LX/3Q0;->d:LX/17X;

    iget-object v7, v1, LX/3Q0;->a:Landroid/content/Context;

    sget-object v8, LX/0ax;->eY:Ljava/lang/String;

    invoke-static {v8, v3, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v7, v3}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 562440
    :goto_2
    move-object v3, v3

    .line 562441
    :goto_3
    if-eqz v3, :cond_4

    const-string v5, "target_tab_name"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 562442
    const-string v5, "target_tab_name"

    sget-object v6, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    invoke-virtual {v6}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 562443
    :cond_4
    move-object v5, v3

    .line 562444
    iget-object v1, p0, LX/3Px;->u:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 562445
    if-eqz v6, :cond_7

    .line 562446
    iget-object v1, p0, LX/3Px;->r:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    invoke-virtual {v1, v0}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->b(Lcom/facebook/notifications/model/SystemTrayNotification;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 562447
    iget-object v1, p0, LX/3Px;->r:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    invoke-virtual {v2}, Lcom/facebook/notifications/constants/NotificationType;->ordinal()I

    move-result v2

    .line 562448
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-nez v3, :cond_11

    .line 562449
    :cond_5
    :goto_4
    goto/16 :goto_0

    .line 562450
    :cond_6
    iget-object v1, p0, LX/3Px;->r:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    invoke-virtual {v2}, Lcom/facebook/notifications/constants/NotificationType;->ordinal()I

    move-result v3

    move-object v2, v0

    .line 562451
    iget-object v7, v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->n:LX/1rj;

    sget-object v8, LX/2ub;->PUSH:LX/2ub;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->c(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;Lcom/facebook/notifications/model/SystemTrayNotification;IILandroid/content/Intent;)LX/Drn;

    move-result-object p1

    invoke-interface {v7, v6, v8, p1}, LX/1rj;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/Drn;)V

    .line 562452
    iget-object v7, v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->n:LX/1rj;

    invoke-interface {v7, v6}, LX/1rj;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 562453
    goto :goto_4

    .line 562454
    :cond_7
    if-eqz v5, :cond_5

    .line 562455
    iget-object v1, p0, LX/3Px;->r:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    invoke-virtual {v2}, Lcom/facebook/notifications/constants/NotificationType;->ordinal()I

    move-result v2

    invoke-virtual {v1, v0, v2, v4, v5}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->b(Lcom/facebook/notifications/model/SystemTrayNotification;IILandroid/content/Intent;)V

    goto :goto_4

    .line 562456
    :sswitch_0
    const v3, 0x7f0218e5

    goto/16 :goto_1

    .line 562457
    :sswitch_1
    const v3, 0x7f0218e6

    goto/16 :goto_1

    .line 562458
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v3

    const/4 v5, 0x0

    .line 562459
    iget-object v6, v1, LX/3Q0;->g:LX/0ad;

    sget-short v7, LX/0fe;->aI:S

    invoke-interface {v6, v7, v5}, LX/0ad;->a(SZ)Z

    move-result v6

    if-nez v6, :cond_c

    .line 562460
    :cond_9
    :goto_5
    move v3, v5

    .line 562461
    if-eqz v3, :cond_a

    .line 562462
    const/4 v6, 0x0

    const/4 p1, 0x1

    .line 562463
    const-string v3, "o"

    invoke-virtual {v0, v3}, Lcom/facebook/notifications/model/SystemTrayNotification;->c(Ljava/lang/String;)LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_e

    const-string v3, "o"

    invoke-virtual {v0, v3}, Lcom/facebook/notifications/model/SystemTrayNotification;->c(Ljava/lang/String;)LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 562464
    :goto_6
    if-eqz v3, :cond_10

    sget-object v5, Lcom/facebook/notifications/constants/NotificationType;->GROUP_ACTIVITY:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v5}, Lcom/facebook/notifications/constants/NotificationType;->name()Ljava/lang/String;

    move-result-object v5

    iget-object v7, v0, Lcom/facebook/notifications/model/SystemTrayNotification;->mType:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_10

    const-string v5, "p"

    invoke-virtual {v0, v5}, Lcom/facebook/notifications/model/SystemTrayNotification;->c(Ljava/lang/String;)LX/0am;

    move-result-object v5

    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_10

    .line 562465
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "_"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v3, "p"

    invoke-virtual {v0, v3}, Lcom/facebook/notifications/model/SystemTrayNotification;->c(Ljava/lang/String;)LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v3

    .line 562466
    :goto_7
    const-string v3, "uid"

    invoke-virtual {v0, v3}, Lcom/facebook/notifications/model/SystemTrayNotification;->c(Ljava/lang/String;)LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_f

    const-string v3, "uid"

    invoke-virtual {v0, v3}, Lcom/facebook/notifications/model/SystemTrayNotification;->c(Ljava/lang/String;)LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 562467
    :goto_8
    iget-object v6, v1, LX/3Q0;->d:LX/17X;

    iget-object v7, v1, LX/3Q0;->a:Landroid/content/Context;

    sget-object v8, LX/0ax;->cL:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 562468
    const-string v7, "is_from_push_notification"

    invoke-virtual {v6, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 562469
    const-string v7, "push_notification_objid"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 562470
    const-string v5, "push_notification_href"

    iget-object v7, v0, Lcom/facebook/notifications/model/SystemTrayNotification;->mHref:Ljava/lang/String;

    invoke-virtual {v6, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 562471
    const-string v5, "push_notification_uid"

    invoke-virtual {v6, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 562472
    const-string v3, "push_notification_type"

    iget-object v5, v0, Lcom/facebook/notifications/model/SystemTrayNotification;->mType:Ljava/lang/String;

    invoke-virtual {v6, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 562473
    const-string v3, "jump_to_top"

    invoke-virtual {v6, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 562474
    move-object v3, v6

    .line 562475
    goto/16 :goto_3

    .line 562476
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v3

    invoke-virtual {v1, v0}, LX/3Q0;->b(Lcom/facebook/notifications/model/SystemTrayNotification;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v3, v5}, LX/3Q0;->a(Lcom/facebook/notifications/constants/NotificationType;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    goto/16 :goto_3

    :cond_b
    move-object v3, v5

    goto/16 :goto_2

    .line 562477
    :cond_c
    iget-object v6, v1, LX/3Q0;->g:LX/0ad;

    sget-char v7, LX/0fe;->aJ:C

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 562478
    if-eqz v6, :cond_9

    .line 562479
    invoke-virtual {v3}, Lcom/facebook/notifications/constants/NotificationType;->toString()Ljava/lang/String;

    move-result-object v7

    .line 562480
    const-string v8, ","

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 562481
    array-length p1, v8

    move v6, v5

    :goto_9
    if-ge v6, p1, :cond_9

    aget-object p2, v8, v6

    .line 562482
    invoke-virtual {v7, p2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p2

    if-nez p2, :cond_d

    .line 562483
    const/4 v5, 0x1

    goto/16 :goto_5

    .line 562484
    :cond_d
    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    :cond_e
    move-object v3, v6

    .line 562485
    goto/16 :goto_6

    :cond_f
    move-object v3, v6

    .line 562486
    goto :goto_8

    :cond_10
    move-object v5, v3

    goto/16 :goto_7

    .line 562487
    :cond_11
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 562488
    iget-object v6, v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->s:LX/0xX;

    sget-object v7, LX/1vy;->PUSH_NOTIF_CACHE:LX/1vy;

    invoke-virtual {v6, v7}, LX/0xX;->a(LX/1vy;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 562489
    iget-object v6, v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->t:LX/3Q4;

    .line 562490
    iget-object v7, v6, LX/3Q4;->a:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/3H7;

    sget-object v8, LX/5Go;->NOTIFICATION_FEEDBACK_DETAILS:LX/5Go;

    sget-object p1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-virtual {v7, v3, v8, p1}, LX/3H7;->a(Ljava/lang/String;LX/5Go;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v3, v7

    .line 562491
    new-instance v6, LX/Drm;

    invoke-direct {v6, v1}, LX/Drm;-><init>(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;)V

    iget-object v7, v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->v:Ljava/util/concurrent/Executor;

    invoke-static {v3, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 562492
    :goto_a
    iget-object v6, v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->y:LX/0TF;

    if-eqz v6, :cond_12

    .line 562493
    iget-object v6, v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->y:LX/0TF;

    iget-object v7, v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->v:Ljava/util/concurrent/Executor;

    invoke-static {v3, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 562494
    :cond_12
    invoke-static {v1, v0, v2, v4, v5}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->c(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;Lcom/facebook/notifications/model/SystemTrayNotification;IILandroid/content/Intent;)LX/Drn;

    move-result-object v6

    iget-object v7, v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->v:Ljava/util/concurrent/Executor;

    invoke-static {v3, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_4

    .line 562495
    :cond_13
    iget-object v6, v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->e:LX/3Q3;

    .line 562496
    iget-object v7, v6, LX/3Q3;->a:LX/3H7;

    sget-object v8, LX/5Go;->NOTIFICATION_FEEDBACK_DETAILS:LX/5Go;

    sget-object p1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    invoke-virtual {v7, v3, v8, p1}, LX/3H7;->a(Ljava/lang/String;LX/5Go;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v3, v7

    .line 562497
    goto :goto_a

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x10 -> :sswitch_1
        0x11 -> :sswitch_1
    .end sparse-switch
.end method
