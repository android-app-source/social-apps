.class public LX/2lA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2lA;


# instance fields
.field public final a:LX/0ad;

.field private final b:LX/0s6;


# direct methods
.method public constructor <init>(LX/0s6;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 457362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457363
    iput-object p1, p0, LX/2lA;->b:LX/0s6;

    .line 457364
    iput-object p2, p0, LX/2lA;->a:LX/0ad;

    .line 457365
    return-void
.end method

.method public static a(LX/0QB;)LX/2lA;
    .locals 5

    .prologue
    .line 457366
    sget-object v0, LX/2lA;->c:LX/2lA;

    if-nez v0, :cond_1

    .line 457367
    const-class v1, LX/2lA;

    monitor-enter v1

    .line 457368
    :try_start_0
    sget-object v0, LX/2lA;->c:LX/2lA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 457369
    if-eqz v2, :cond_0

    .line 457370
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 457371
    new-instance p0, LX/2lA;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v3

    check-cast v3, LX/0s6;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/2lA;-><init>(LX/0s6;LX/0ad;)V

    .line 457372
    move-object v0, p0

    .line 457373
    sput-object v0, LX/2lA;->c:LX/2lA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457374
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 457375
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 457376
    :cond_1
    sget-object v0, LX/2lA;->c:LX/2lA;

    return-object v0

    .line 457377
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 457378
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 457379
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 457380
    const/high16 v1, 0x18000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 457381
    return-object v0
.end method

.method public static g(LX/2lA;)Z
    .locals 2

    .prologue
    .line 457382
    iget-object v0, p0, LX/2lA;->b:LX/0s6;

    const-string v1, "com.facebook.adsmanager"

    invoke-virtual {v0, v1}, LX/01H;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final d()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 457383
    invoke-static {p0}, LX/2lA;->g(LX/2lA;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 457384
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/2lA;->a:LX/0ad;

    sget-short v2, LX/ADP;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method
