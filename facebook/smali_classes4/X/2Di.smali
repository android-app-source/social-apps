.class public LX/2Di;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/0dC;


# direct methods
.method public constructor <init>(LX/0tX;LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384589
    iput-object p1, p0, LX/2Di;->a:LX/0tX;

    .line 384590
    iput-object p2, p0, LX/2Di;->b:LX/0dC;

    .line 384591
    return-void
.end method

.method public static a(LX/0QB;)LX/2Di;
    .locals 1

    .prologue
    .line 384592
    invoke-static {p0}, LX/2Di;->b(LX/0QB;)LX/2Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2Di;ZZZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 384593
    new-instance v0, LX/4E3;

    invoke-direct {v0}, LX/4E3;-><init>()V

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 384594
    const-string v2, "is_active"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 384595
    move-object v0, v0

    .line 384596
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 384597
    const-string v2, "persist"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 384598
    move-object v0, v0

    .line 384599
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 384600
    const-string v2, "opt_in"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 384601
    move-object v0, v0

    .line 384602
    iget-object v1, p0, LX/2Di;->b:LX/0dC;

    invoke-virtual {v1}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v1

    .line 384603
    const-string v2, "device_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 384604
    move-object v0, v0

    .line 384605
    new-instance v1, LX/GbJ;

    invoke-direct {v1}, LX/GbJ;-><init>()V

    move-object v1, v1

    .line 384606
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 384607
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 384608
    iget-object v1, p0, LX/2Di;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2Di;
    .locals 3

    .prologue
    .line 384609
    new-instance v2, LX/2Di;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v1

    check-cast v1, LX/0dC;

    invoke-direct {v2, v0, v1}, LX/2Di;-><init>(LX/0tX;LX/0dC;)V

    .line 384610
    return-object v2
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 384611
    const/4 v0, 0x1

    invoke-static {p0, v1, v0, v1}, LX/2Di;->a(LX/2Di;ZZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 384612
    return-void
.end method
