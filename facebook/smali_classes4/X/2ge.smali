.class public LX/2ge;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1fT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2ge;


# instance fields
.field private final a:LX/2bV;


# direct methods
.method public constructor <init>(LX/2bV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 448742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448743
    iput-object p1, p0, LX/2ge;->a:LX/2bV;

    .line 448744
    return-void
.end method

.method public static a(LX/0QB;)LX/2ge;
    .locals 4

    .prologue
    .line 448745
    sget-object v0, LX/2ge;->b:LX/2ge;

    if-nez v0, :cond_1

    .line 448746
    const-class v1, LX/2ge;

    monitor-enter v1

    .line 448747
    :try_start_0
    sget-object v0, LX/2ge;->b:LX/2ge;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 448748
    if-eqz v2, :cond_0

    .line 448749
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 448750
    new-instance p0, LX/2ge;

    invoke-static {v0}, LX/2bV;->a(LX/0QB;)LX/2bV;

    move-result-object v3

    check-cast v3, LX/2bV;

    invoke-direct {p0, v3}, LX/2ge;-><init>(LX/2bV;)V

    .line 448751
    move-object v0, p0

    .line 448752
    sput-object v0, LX/2ge;->b:LX/2ge;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 448753
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 448754
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 448755
    :cond_1
    sget-object v0, LX/2ge;->b:LX/2ge;

    return-object v0

    .line 448756
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 448757
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onMessage(Ljava/lang/String;[BJ)V
    .locals 2

    .prologue
    .line 448758
    iget-object v0, p0, LX/2ge;->a:LX/2bV;

    invoke-static {p2}, LX/0YN;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/2bV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 448759
    return-void
.end method
