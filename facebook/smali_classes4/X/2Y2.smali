.class public LX/2Y2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zh;

.field private final b:LX/2Y3;

.field private final c:LX/2Y4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Zh;Ljava/lang/String;Lcom/facebook/flexiblesampling/SamplingPolicyConfig;)V
    .locals 1
    .param p4    # Lcom/facebook/flexiblesampling/SamplingPolicyConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 420605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420606
    iput-object p2, p0, LX/2Y2;->a:LX/0Zh;

    .line 420607
    new-instance v0, LX/2Y3;

    invoke-direct {v0, p1, p3}, LX/2Y3;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, LX/2Y2;->b:LX/2Y3;

    .line 420608
    if-eqz p4, :cond_0

    new-instance v0, LX/2Y4;

    invoke-direct {v0, p4}, LX/2Y4;-><init>(Lcom/facebook/flexiblesampling/SamplingPolicyConfig;)V

    :goto_0
    iput-object v0, p0, LX/2Y2;->c:LX/2Y4;

    .line 420609
    return-void

    .line 420610
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/io/Writer;)V
    .locals 2

    .prologue
    .line 420583
    iget-object v0, p0, LX/2Y2;->a:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v1

    .line 420584
    :try_start_0
    iget-object v0, p0, LX/2Y2;->b:LX/2Y3;

    invoke-virtual {v0, v1}, LX/2Y3;->a(LX/0n9;)V

    .line 420585
    iget-object v0, p0, LX/2Y2;->c:LX/2Y4;

    if-eqz v0, :cond_0

    .line 420586
    iget-object v0, p0, LX/2Y2;->c:LX/2Y4;

    invoke-virtual {v0, v1}, LX/2Y4;->a(LX/0n9;)V

    .line 420587
    :cond_0
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, LX/0nC;->b(Ljava/io/Writer;LX/0nA;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420588
    invoke-virtual {v1}, LX/0nA;->a()V

    .line 420589
    return-void

    .line 420590
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/0nA;->a()V

    throw v0
.end method

.method public final b(Ljava/io/Writer;)V
    .locals 4

    .prologue
    .line 420591
    iget-object v0, p0, LX/2Y2;->a:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v1

    .line 420592
    :try_start_0
    iget-object v0, p0, LX/2Y2;->b:LX/2Y3;

    const-string v2, "request_info"

    invoke-virtual {v1, v2}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2Y3;->a(LX/0n9;)V

    .line 420593
    iget-object v0, p0, LX/2Y2;->c:LX/2Y4;

    if-eqz v0, :cond_0

    .line 420594
    const-string v0, "config"

    invoke-virtual {v1, v0}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    move-result-object v0

    .line 420595
    iget-object v2, p0, LX/2Y2;->c:LX/2Y4;

    invoke-virtual {v2, v0}, LX/2Y4;->a(LX/0n9;)V

    .line 420596
    iget-object v2, p0, LX/2Y2;->c:LX/2Y4;

    .line 420597
    const-string v3, "uid"

    iget-object p0, v2, LX/2Y4;->a:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    invoke-interface {p0}, Lcom/facebook/flexiblesampling/SamplingPolicyConfig;->c()Ljava/lang/String;

    move-result-object p0

    .line 420598
    invoke-static {v0, v3, p0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 420599
    const-string v3, "app_ver"

    iget-object p0, v2, LX/2Y4;->a:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    invoke-interface {p0}, Lcom/facebook/flexiblesampling/SamplingPolicyConfig;->d()Ljava/lang/String;

    move-result-object p0

    .line 420600
    invoke-static {v0, v3, p0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 420601
    :cond_0
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, LX/0nC;->b(Ljava/io/Writer;LX/0nA;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420602
    invoke-virtual {v1}, LX/0nA;->a()V

    .line 420603
    return-void

    .line 420604
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/0nA;->a()V

    throw v0
.end method
