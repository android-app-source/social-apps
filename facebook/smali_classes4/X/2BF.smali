.class public LX/2BF;
.super Ljava/io/IOException;
.source ""


# direct methods
.method private constructor <init>(Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 378946
    invoke-direct {p0}, Ljava/io/IOException;-><init>()V

    .line 378947
    invoke-virtual {p0, p1}, LX/2BF;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 378948
    return-void
.end method

.method public static a(Ljava/lang/Exception;)Ljava/io/IOException;
    .locals 1

    .prologue
    .line 378949
    const-class v0, Ljava/io/IOException;

    invoke-static {p0, v0}, LX/1Bz;->propagateIfInstanceOf(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 378950
    new-instance v0, LX/2BF;

    invoke-direct {v0, p0}, LX/2BF;-><init>(Ljava/lang/Exception;)V

    throw v0
.end method

.method public static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 378951
    instance-of v0, p0, LX/2BF;

    if-eqz v0, :cond_0

    .line 378952
    check-cast p0, LX/2BF;

    .line 378953
    invoke-virtual {p0}, LX/2BF;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    move-object p0, v0

    .line 378954
    :cond_0
    return-object p0
.end method
