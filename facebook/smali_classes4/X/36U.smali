.class public LX/36U;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 498916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 498917
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 4

    .prologue
    .line 498888
    invoke-static {p0}, LX/36U;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 498889
    if-eqz v0, :cond_1

    .line 498890
    :cond_0
    :goto_0
    return-object v0

    .line 498891
    :cond_1
    const/4 v0, 0x0

    .line 498892
    invoke-static {p0}, LX/1VS;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 498893
    invoke-static {v1}, LX/36V;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 498894
    :cond_2
    :goto_1
    move-object v0, v0

    .line 498895
    if-nez v0, :cond_0

    .line 498896
    invoke-static {p0}, LX/1VS;->d(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 498897
    invoke-static {v1}, LX/36V;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, -0x22a42d2a    # -9.8999738E17f

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    if-ne v0, v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_7

    :cond_3
    move-object v0, v1

    .line 498898
    :cond_4
    :goto_2
    move-object v0, v0

    .line 498899
    goto :goto_0

    .line 498900
    :cond_5
    const v2, -0x22a42d2a    # -9.8999738E17f

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    if-ne v2, v3, :cond_6

    move-object v0, v1

    .line 498901
    goto :goto_1

    .line 498902
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->o()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 498903
    goto :goto_1

    .line 498904
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/36U;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 498905
    invoke-static {v0}, LX/36V;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-static {v0}, LX/36V;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 498906
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    const/4 v2, 0x0

    .line 498907
    invoke-static {v0}, LX/1VS;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    .line 498908
    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    if-nez p0, :cond_b

    .line 498909
    :cond_9
    :goto_3
    move v0, v2

    .line 498910
    if-eqz v0, :cond_a

    .line 498911
    const/4 v0, 0x0

    goto :goto_2

    :cond_a
    move-object v0, v1

    .line 498912
    goto :goto_2

    .line 498913
    :cond_b
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    .line 498914
    const p0, -0x12bcfc94

    if-ne p0, v3, :cond_9

    .line 498915
    const/4 v2, 0x1

    goto :goto_3
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 498881
    invoke-static {p0}, LX/1VS;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 498882
    invoke-static {v1}, LX/36V;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 498883
    :cond_0
    :goto_0
    return-object v0

    .line 498884
    :cond_1
    invoke-static {v1}, LX/36V;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 498885
    const v2, -0x12bcfc94

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p0

    if-ne v2, p0, :cond_3

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 498886
    if-eqz v2, :cond_0

    :cond_2
    move-object v0, v1

    .line 498887
    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method
