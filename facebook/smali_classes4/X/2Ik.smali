.class public LX/2Ik;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/30S;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile b:LX/2Ik;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 392244
    new-instance v0, LX/2Il;

    invoke-direct {v0}, LX/2Il;-><init>()V

    sput-object v0, LX/2Ik;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 392231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2Ik;
    .locals 3

    .prologue
    .line 392232
    sget-object v0, LX/2Ik;->b:LX/2Ik;

    if-nez v0, :cond_1

    .line 392233
    const-class v1, LX/2Ik;

    monitor-enter v1

    .line 392234
    :try_start_0
    sget-object v0, LX/2Ik;->b:LX/2Ik;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 392235
    if-eqz v2, :cond_0

    .line 392236
    :try_start_1
    new-instance v0, LX/2Ik;

    invoke-direct {v0}, LX/2Ik;-><init>()V

    .line 392237
    move-object v0, v0

    .line 392238
    sput-object v0, LX/2Ik;->b:LX/2Ik;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392239
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 392240
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 392241
    :cond_1
    sget-object v0, LX/2Ik;->b:LX/2Ik;

    return-object v0

    .line 392242
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 392243
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/30S;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392197
    new-instance v1, Ljava/io/File;

    const-string v0, "/proc"

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 392198
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 392199
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 392200
    if-nez v2, :cond_1

    .line 392201
    :cond_0
    return-object v0

    .line 392202
    :cond_1
    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 392203
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    .line 392204
    move v6, v7

    :goto_1
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v6, v8, :cond_5

    .line 392205
    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 392206
    const/16 v9, 0x30

    if-lt v8, v9, :cond_2

    const/16 v9, 0x39

    if-le v8, v9, :cond_4

    .line 392207
    :cond_2
    :goto_2
    move v5, v7

    .line 392208
    if-eqz v5, :cond_3

    .line 392209
    const/4 v6, 0x0

    .line 392210
    :try_start_0
    const-string v5, "cmdline"

    invoke-static {v4, v5}, LX/2Ik;->a(Ljava/io/File;Ljava/lang/String;)Ljava/util/Scanner;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 392211
    :try_start_1
    invoke-virtual {v5}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v8

    .line 392212
    invoke-virtual {v5}, Ljava/util/Scanner;->close()V

    .line 392213
    const-string v7, "statm"

    invoke-static {v4, v7}, LX/2Ik;->a(Ljava/io/File;Ljava/lang/String;)Ljava/util/Scanner;

    move-result-object v5

    .line 392214
    invoke-virtual {v5}, Ljava/util/Scanner;->nextInt()I

    .line 392215
    invoke-virtual {v5}, Ljava/util/Scanner;->nextInt()I

    move-result v7

    mul-int/lit8 v9, v7, 0x4

    .line 392216
    invoke-virtual {v5}, Ljava/util/Scanner;->close()V

    .line 392217
    const-string v7, "oom_score_adj"

    invoke-static {v4, v7}, LX/2Ik;->a(Ljava/io/File;Ljava/lang/String;)Ljava/util/Scanner;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v7

    .line 392218
    :try_start_2
    invoke-virtual {v7}, Ljava/util/Scanner;->nextInt()I

    move-result v10

    .line 392219
    invoke-virtual {v7}, Ljava/util/Scanner;->close()V

    .line 392220
    new-instance v5, LX/30S;

    invoke-direct {v5, v8, v9, v10}, LX/30S;-><init>(Ljava/lang/String;II)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 392221
    :goto_3
    move-object v4, v5

    .line 392222
    if-eqz v4, :cond_3

    .line 392223
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 392224
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 392225
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 392226
    :cond_5
    const/4 v7, 0x1

    goto :goto_2

    .line 392227
    :catch_0
    move-object v5, v6

    :goto_4
    if-eqz v5, :cond_6

    .line 392228
    invoke-virtual {v5}, Ljava/util/Scanner;->close()V

    :cond_6
    move-object v5, v6

    .line 392229
    goto :goto_3

    .line 392230
    :catch_1
    move-exception v7

    goto :goto_4

    :catch_2
    move-object v5, v7

    goto :goto_4
.end method

.method public static a(Ljava/io/File;Ljava/lang/String;)Ljava/util/Scanner;
    .locals 3

    .prologue
    .line 392193
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 392194
    new-instance v1, Ljava/util/Scanner;

    invoke-direct {v1, v0}, Ljava/util/Scanner;-><init>(Ljava/io/File;)V

    .line 392195
    const-string v0, "[^A-Za-z0-9.:]"

    invoke-virtual {v1, v0}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    .line 392196
    return-object v1
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 34
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 392135
    invoke-static {}, LX/2Ik;->a()Ljava/util/List;

    move-result-object v19

    .line 392136
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 392137
    const/4 v2, 0x0

    .line 392138
    :cond_0
    :goto_0
    return-object v2

    .line 392139
    :cond_1
    invoke-static {}, LX/007;->p()Ljava/lang/String;

    move-result-object v24

    .line 392140
    invoke-static {}, LX/007;->n()Ljava/lang/String;

    move-result-object v25

    .line 392141
    sget-object v2, LX/2Ik;->a:Ljava/util/Comparator;

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 392142
    const-wide/16 v22, 0x0

    .line 392143
    const-wide/16 v20, 0x0

    .line 392144
    const/16 v18, 0x0

    .line 392145
    const-wide/16 v16, 0x0

    .line 392146
    const/4 v5, 0x0

    .line 392147
    const-wide/16 v14, 0x0

    .line 392148
    const-wide/16 v12, 0x0

    .line 392149
    const-wide/16 v10, 0x0

    .line 392150
    const/high16 v2, -0x80000000

    .line 392151
    const-wide/16 v8, 0x0

    .line 392152
    const-wide/16 v6, 0x0

    .line 392153
    const/4 v4, 0x0

    .line 392154
    new-instance v26, Ljava/util/HashMap;

    invoke-direct/range {v26 .. v26}, Ljava/util/HashMap;-><init>()V

    .line 392155
    new-instance v27, Ljava/util/HashMap;

    invoke-direct/range {v27 .. v27}, Ljava/util/HashMap;-><init>()V

    .line 392156
    const/4 v3, 0x0

    .line 392157
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    move-wide/from16 v32, v10

    move-wide v10, v14

    move v15, v2

    move/from16 v14, v18

    move-wide/from16 v18, v12

    move-wide/from16 v12, v16

    move-wide/from16 v16, v32

    :goto_1
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/30S;

    .line 392158
    iget v0, v2, LX/30S;->c:I

    move/from16 v29, v0

    move/from16 v0, v29

    if-eq v0, v15, :cond_3

    .line 392159
    if-ltz v15, :cond_2

    .line 392160
    new-instance v15, Ljava/lang/StringBuilder;

    const-string v29, "total_rss_at_priority_"

    move-object/from16 v0, v29

    invoke-direct {v15, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v2, LX/30S;->c:I

    move/from16 v29, v0

    move/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-interface {v0, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392161
    :cond_2
    iget v15, v2, LX/30S;->c:I

    .line 392162
    const-wide/16 v8, 0x0

    .line 392163
    :goto_2
    iget-object v0, v2, LX/30S;->a:Ljava/lang/String;

    move-object/from16 v29, v0

    const-string v30, "com.facebook"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v29

    if-eqz v29, :cond_5

    .line 392164
    iget v0, v2, LX/30S;->b:I

    move/from16 v29, v0

    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v22, v22, v30

    .line 392165
    iget-object v0, v2, LX/30S;->a:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v29

    if-eqz v29, :cond_4

    .line 392166
    iget v0, v2, LX/30S;->b:I

    move/from16 v29, v0

    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v18, v18, v30

    .line 392167
    :goto_3
    if-nez v3, :cond_7

    .line 392168
    iget-object v0, v2, LX/30S;->a:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_6

    .line 392169
    const/4 v3, 0x1

    move-object v4, v2

    .line 392170
    :goto_4
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v2, LX/30S;->a:Ljava/lang/String;

    move-object/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "."

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    iget v0, v2, LX/30S;->c:I

    move/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    iget v2, v2, LX/30S;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 392171
    :cond_3
    iget v0, v2, LX/30S;->b:I

    move/from16 v29, v0

    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v8, v8, v30

    goto :goto_2

    .line 392172
    :cond_4
    iget v0, v2, LX/30S;->b:I

    move/from16 v29, v0

    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v16, v16, v30

    goto :goto_3

    .line 392173
    :cond_5
    iget v0, v2, LX/30S;->b:I

    move/from16 v29, v0

    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    goto :goto_3

    .line 392174
    :cond_6
    add-int/lit8 v14, v14, 0x1

    .line 392175
    iget v6, v2, LX/30S;->b:I

    int-to-long v6, v6

    add-long/2addr v6, v12

    move-wide v12, v6

    move-wide v6, v8

    .line 392176
    goto :goto_4

    .line 392177
    :cond_7
    add-int/lit8 v5, v5, 0x1

    .line 392178
    iget v0, v2, LX/30S;->b:I

    move/from16 v29, v0

    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v10, v10, v30

    goto :goto_4

    .line 392179
    :cond_8
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "other_app_memory_usage"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 392180
    const-string v3, "earlier_process_count"

    invoke-virtual {v2, v3, v14}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 392181
    const-string v3, "earlier_total_rss_kb"

    invoke-virtual {v2, v3, v12, v13}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 392182
    const-string v3, "later_process_count"

    invoke-virtual {v2, v3, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 392183
    const-string v3, "later_total_rss_kb"

    invoke-virtual {v2, v3, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 392184
    const-string v3, "facebook_apps_total_rss_kb"

    move-wide/from16 v0, v22

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 392185
    const-string v3, "non_facebook_total_rss_kb"

    move-wide/from16 v0, v20

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 392186
    const-string v3, "total_rss_kb_before_fb4a_at_same_priority"

    invoke-virtual {v2, v3, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 392187
    const-string v3, "fb4a_total_rss_kb"

    move-wide/from16 v0, v18

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 392188
    const-string v3, "non_fb4a_total_rss_kb"

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 392189
    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 392190
    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 392191
    if-eqz v4, :cond_0

    .line 392192
    const-string v3, "fb4a_priority"

    iget v4, v4, LX/30S;->c:I

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_0
.end method
