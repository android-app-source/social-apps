.class public final LX/3Ps;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/3Pw;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/3Pw;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 562257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562258
    iput-object p1, p0, LX/3Ps;->a:LX/0QB;

    .line 562259
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 562260
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/3Ps;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 562261
    packed-switch p2, :pswitch_data_0

    .line 562262
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 562263
    :pswitch_0
    invoke-static {p1}, LX/3Pv;->a(LX/0QB;)LX/3Pv;

    move-result-object v0

    .line 562264
    :goto_0
    return-object v0

    .line 562265
    :pswitch_1
    invoke-static {p1}, LX/3Px;->a(LX/0QB;)LX/3Px;

    move-result-object v0

    goto :goto_0

    .line 562266
    :pswitch_2
    invoke-static {p1}, LX/3QF;->a(LX/0QB;)LX/3QF;

    move-result-object v0

    goto :goto_0

    .line 562267
    :pswitch_3
    invoke-static {p1}, LX/3QG;->a(LX/0QB;)LX/3QG;

    move-result-object v0

    goto :goto_0

    .line 562268
    :pswitch_4
    invoke-static {p1}, LX/3QH;->a(LX/0QB;)LX/3QH;

    move-result-object v0

    goto :goto_0

    .line 562269
    :pswitch_5
    invoke-static {p1}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(LX/0QB;)Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;

    move-result-object v0

    goto :goto_0

    .line 562270
    :pswitch_6
    invoke-static {p1}, LX/3QZ;->a(LX/0QB;)LX/3QZ;

    move-result-object v0

    goto :goto_0

    .line 562271
    :pswitch_7
    invoke-static {p1}, LX/3Qa;->a(LX/0QB;)LX/3Qa;

    move-result-object v0

    goto :goto_0

    .line 562272
    :pswitch_8
    invoke-static {p1}, LX/3Qb;->a(LX/0QB;)LX/3Qb;

    move-result-object v0

    goto :goto_0

    .line 562273
    :pswitch_9
    invoke-static {p1}, LX/3Qp;->a(LX/0QB;)LX/3Qp;

    move-result-object v0

    goto :goto_0

    .line 562274
    :pswitch_a
    invoke-static {p1}, LX/3Qq;->a(LX/0QB;)LX/3Qq;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 562275
    const/16 v0, 0xb

    return v0
.end method
