.class public LX/2TC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ug;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2TC;


# instance fields
.field private final b:LX/0Y9;

.field private final c:LX/0W3;

.field private final d:LX/0lB;

.field private final e:LX/0YB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 413814
    const-class v0, LX/2TC;

    sput-object v0, LX/2TC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0W3;LX/0Y9;LX/0lB;LX/0YB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 413815
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413816
    iput-object p1, p0, LX/2TC;->c:LX/0W3;

    .line 413817
    iput-object p2, p0, LX/2TC;->b:LX/0Y9;

    .line 413818
    iput-object p3, p0, LX/2TC;->d:LX/0lB;

    .line 413819
    iput-object p4, p0, LX/2TC;->e:LX/0YB;

    .line 413820
    return-void
.end method

.method public static a(LX/0QB;)LX/2TC;
    .locals 7

    .prologue
    .line 413821
    sget-object v0, LX/2TC;->f:LX/2TC;

    if-nez v0, :cond_1

    .line 413822
    const-class v1, LX/2TC;

    monitor-enter v1

    .line 413823
    :try_start_0
    sget-object v0, LX/2TC;->f:LX/2TC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413824
    if-eqz v2, :cond_0

    .line 413825
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 413826
    new-instance p0, LX/2TC;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v0}, LX/0Y9;->a(LX/0QB;)LX/0Y9;

    move-result-object v4

    check-cast v4, LX/0Y9;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lB;

    invoke-static {v0}, LX/0YB;->a(LX/0QB;)LX/0YB;

    move-result-object v6

    check-cast v6, LX/0YB;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2TC;-><init>(LX/0W3;LX/0Y9;LX/0lB;LX/0YB;)V

    .line 413827
    move-object v0, p0

    .line 413828
    sput-object v0, LX/2TC;->f:LX/2TC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413829
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413830
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413831
    :cond_1
    sget-object v0, LX/2TC;->f:LX/2TC;

    return-object v0

    .line 413832
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413833
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/util/Map;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 413834
    invoke-interface {p1, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 413835
    const/16 v0, 0xf8

    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 413836
    iget-object v0, p0, LX/2TC;->b:LX/0Y9;

    invoke-virtual {v0}, LX/0Y9;->b()V

    .line 413837
    iget-object v0, p0, LX/2TC;->e:LX/0YB;

    .line 413838
    iget-object p1, v0, LX/0YB;->a:Landroid/content/SharedPreferences;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 413839
    invoke-virtual {p0}, LX/2TC;->b()V

    .line 413840
    return-void
.end method

.method public final b()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 413841
    iget-object v0, p0, LX/2TC;->e:LX/0YB;

    .line 413842
    iget-object v1, v0, LX/0YB;->a:Landroid/content/SharedPreferences;

    const-string v2, "DebugTraceFinishedKey"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    move v0, v1

    .line 413843
    if-eqz v0, :cond_1

    .line 413844
    :cond_0
    :goto_0
    return-void

    .line 413845
    :cond_1
    iget-object v0, p0, LX/2TC;->c:LX/0W3;

    invoke-virtual {v0}, LX/0W3;->a()LX/0W4;

    move-result-object v0

    sget-wide v2, LX/0X5;->ig:J

    const-string v1, "default"

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 413846
    const-string v1, "default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 413847
    :try_start_0
    iget-object v1, p0, LX/2TC;->d:LX/0lB;

    const-class v2, Ljava/util/Map;

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 413848
    const-string v1, "perf_name"

    invoke-static {v1, v0}, LX/2TC;->a(Ljava/lang/String;Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "marker_id"

    invoke-static {v1, v0}, LX/2TC;->a(Ljava/lang/String;Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "quicklog_event"

    invoke-static {v1, v0}, LX/2TC;->a(Ljava/lang/String;Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "buffer_size_mb"

    invoke-static {v1, v0}, LX/2TC;->a(Ljava/lang/String;Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "trace_time_sec"

    invoke-static {v1, v0}, LX/2TC;->a(Ljava/lang/String;Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "sampling_interval_us"

    invoke-static {v1, v0}, LX/2TC;->a(Ljava/lang/String;Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "trace_type"

    invoke-static {v1, v0}, LX/2TC;->a(Ljava/lang/String;Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "timestamp"

    invoke-static {v1, v0}, LX/2TC;->a(Ljava/lang/String;Ljava/util/Map;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 413849
    :cond_2
    sget-object v1, LX/2TC;->a:Ljava/lang/Class;

    const-string v2, "Missing XConfig params. This should never happen due to serverside defaults"

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 413850
    const/4 v1, 0x0

    .line 413851
    :goto_1
    move v1, v1

    .line 413852
    if-eqz v1, :cond_0

    .line 413853
    const-string v1, "trace_type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 413854
    invoke-static {v9}, LX/0YJ;->a(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 413855
    sget-object v0, LX/2TC;->a:Ljava/lang/Class;

    const-string v1, "XConfig trace type is not valid. Must be 0 for sampling or 1 for method level"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 413856
    :catch_0
    move-exception v0

    .line 413857
    sget-object v1, LX/2TC;->a:Ljava/lang/Class;

    const-string v2, "Error while decoding DebugTraceXConfig JSON"

    new-array v3, v12, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 413858
    :cond_3
    if-nez v9, :cond_4

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_4

    .line 413859
    sget-object v0, LX/2TC;->a:Ljava/lang/Class;

    const-string v1, "Tried to sampling trace on a device that was running below SDK 21 (Lollipop)"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 413860
    :cond_4
    :try_start_1
    iget-object v1, p0, LX/2TC;->b:LX/0Y9;

    const-string v2, "perf_name"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "marker_id"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    const/high16 v4, -0x80000000

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const-string v4, "quicklog_event"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "buffer_size_mb"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/high16 v6, 0x100000

    mul-int/2addr v5, v6

    const-string v6, "trace_time_sec"

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-long v6, v6

    const-wide/16 v10, 0x3e8

    mul-long/2addr v6, v10

    const-string v8, "sampling_interval_us"

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual/range {v1 .. v9}, LX/0Y9;->a(Ljava/lang/String;ILjava/lang/String;IJII)Z
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 413861
    :catch_1
    move-exception v0

    .line 413862
    sget-object v1, LX/2TC;->a:Ljava/lang/Class;

    const-string v2, "Error while trying to parse XConfig params"

    new-array v3, v12, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x1

    goto/16 :goto_1
.end method
