.class public abstract LX/3Mm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/333;


# instance fields
.field private final a:LX/0Zr;

.field public b:Landroid/os/Handler;

.field public c:Landroid/os/Handler;

.field private d:LX/61p;

.field private e:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field

.field public f:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field

.field public g:LX/3Mr;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field

.field public final h:Ljava/lang/Object;


# direct methods
.method public constructor <init>(LX/0Zr;)V
    .locals 2

    .prologue
    .line 555425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 555426
    sget-object v0, LX/3Mr;->FINISHED:LX/3Mr;

    iput-object v0, p0, LX/3Mm;->g:LX/3Mr;

    .line 555427
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/3Mm;->h:Ljava/lang/Object;

    .line 555428
    iput-object p1, p0, LX/3Mm;->a:LX/0Zr;

    .line 555429
    new-instance v0, LX/3Ms;

    invoke-direct {v0, p0}, LX/3Ms;-><init>(LX/3Mm;)V

    iput-object v0, p0, LX/3Mm;->c:Landroid/os/Handler;

    .line 555430
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 555431
    const/4 v0, -0x1

    iput v0, p0, LX/3Mm;->f:I

    .line 555432
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 555433
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/3Mm;->a(Ljava/lang/CharSequence;LX/3NY;)V

    .line 555434
    return-void
.end method

.method public abstract a(Ljava/lang/CharSequence;LX/39y;)V
.end method

.method public final a(Ljava/lang/CharSequence;LX/3NY;)V
    .locals 7

    .prologue
    .line 555435
    iget-object v4, p0, LX/3Mm;->h:Ljava/lang/Object;

    monitor-enter v4

    .line 555436
    :try_start_0
    iget-object v0, p0, LX/3Mm;->b:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 555437
    iget-object v0, p0, LX/3Mm;->a:LX/0Zr;

    const-string v1, "Filter"

    sget-object v2, LX/0TP;->BACKGROUND:LX/0TP;

    invoke-virtual {v0, v1, v2}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 555438
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 555439
    new-instance v1, LX/3Oe;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, LX/3Oe;-><init>(LX/3Mm;Landroid/os/Looper;)V

    iput-object v1, p0, LX/3Mm;->b:Landroid/os/Handler;

    .line 555440
    :cond_0
    iget-object v0, p0, LX/3Mm;->d:LX/61p;

    if-nez v0, :cond_2

    const-wide/16 v0, 0x0

    move-wide v2, v0

    .line 555441
    :goto_0
    iget-object v0, p0, LX/3Mm;->b:Landroid/os/Handler;

    const v1, -0x2f2f0ff3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 555442
    new-instance v5, LX/3Of;

    invoke-direct {v5}, LX/3Of;-><init>()V

    .line 555443
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, v5, LX/3Of;->a:Ljava/lang/CharSequence;

    .line 555444
    iput-object p2, v5, LX/3Of;->b:LX/3NY;

    .line 555445
    iget v0, p0, LX/3Mm;->e:I

    add-int/lit8 v6, v0, 0x1

    iput v6, p0, LX/3Mm;->e:I

    iput v0, v5, LX/3Of;->d:I

    .line 555446
    iget v0, v5, LX/3Of;->d:I

    iput v0, p0, LX/3Mm;->f:I

    .line 555447
    iget-object v0, p0, LX/3Mm;->g:LX/3Mr;

    sget-object v6, LX/3Mr;->FILTERING:LX/3Mr;

    if-eq v0, v6, :cond_1

    .line 555448
    sget-object v0, LX/3Mr;->FILTERING:LX/3Mr;

    iput-object v0, p0, LX/3Mm;->g:LX/3Mr;

    .line 555449
    if-eqz p2, :cond_1

    .line 555450
    iget-object v0, p0, LX/3Mm;->g:LX/3Mr;

    invoke-interface {p2, v0}, LX/3NY;->a(LX/3Mr;)V

    .line 555451
    :cond_1
    iput-object v5, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 555452
    iget-object v0, p0, LX/3Mm;->b:Landroid/os/Handler;

    const v5, -0x2f2f0ff3

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 555453
    iget-object v0, p0, LX/3Mm;->b:Landroid/os/Handler;

    const v5, -0x21524111

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 555454
    iget-object v0, p0, LX/3Mm;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 555455
    monitor-exit v4

    return-void

    .line 555456
    :cond_2
    iget-object v0, p0, LX/3Mm;->d:LX/61p;

    invoke-interface {v0}, LX/61p;->a()J

    move-result-wide v0

    move-wide v2, v0

    goto :goto_0

    .line 555457
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 555458
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract b(Ljava/lang/CharSequence;)LX/39y;
.end method

.method public final b()LX/3Mr;
    .locals 1

    .prologue
    .line 555459
    iget-object v0, p0, LX/3Mm;->g:LX/3Mr;

    return-object v0
.end method
