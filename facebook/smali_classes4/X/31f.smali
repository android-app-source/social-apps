.class public LX/31f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/31f;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/31g;


# direct methods
.method public constructor <init>(LX/0Zb;LX/31g;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 487697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 487698
    iput-object p1, p0, LX/31f;->a:LX/0Zb;

    .line 487699
    iput-object p2, p0, LX/31f;->b:LX/31g;

    .line 487700
    return-void
.end method

.method public static a(LX/0QB;)LX/31f;
    .locals 5

    .prologue
    .line 487710
    sget-object v0, LX/31f;->c:LX/31f;

    if-nez v0, :cond_1

    .line 487711
    const-class v1, LX/31f;

    monitor-enter v1

    .line 487712
    :try_start_0
    sget-object v0, LX/31f;->c:LX/31f;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 487713
    if-eqz v2, :cond_0

    .line 487714
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 487715
    new-instance p0, LX/31f;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/31g;->a(LX/0QB;)LX/31g;

    move-result-object v4

    check-cast v4, LX/31g;

    invoke-direct {p0, v3, v4}, LX/31f;-><init>(LX/0Zb;LX/31g;)V

    .line 487716
    move-object v0, p0

    .line 487717
    sput-object v0, LX/31f;->c:LX/31f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 487718
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 487719
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 487720
    :cond_1
    sget-object v0, LX/31f;->c:LX/31f;

    return-object v0

    .line 487721
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 487722
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/31f;Ljava/lang/String;Ljava/lang/String;LX/0am;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;"
        }
    .end annotation

    .prologue
    .line 487706
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "crowdsourcing_session_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "crowdsourcing_edit"

    .line 487707
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 487708
    move-object v0, v0

    .line 487709
    const-string v1, "entry_point"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "page_id"

    invoke-virtual {p3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "session_id"

    iget-object v2, p0, LX/31f;->b:LX/31g;

    invoke-virtual {v2}, LX/31g;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/31f;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 487723
    iget-object v0, p1, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->a:Ljava/lang/String;

    invoke-static {p3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-static {p0, v0, p2, v1}, LX/31f;->a(LX/31f;Ljava/lang/String;Ljava/lang/String;LX/0am;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 487724
    const-string v1, "endpoint"

    new-instance v2, LX/0mD;

    iget-object v3, p1, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, LX/0mD;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 487725
    iget-object v1, p0, LX/31f;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 487726
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 487704
    const-string v0, "endpoint_impression"

    invoke-static {p0, p1, v0, p2}, LX/31f;->b(LX/31f;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;Ljava/lang/String;)V

    .line 487705
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0am;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 487701
    const-string v0, "entry_point_impression"

    invoke-static {p0, p1, v0, p2}, LX/31f;->a(LX/31f;Ljava/lang/String;Ljava/lang/String;LX/0am;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 487702
    iget-object v1, p0, LX/31f;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 487703
    return-void
.end method
