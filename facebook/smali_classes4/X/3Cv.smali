.class public LX/3Cv;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/widget/LinearLayout;

.field public b:Lcom/facebook/notifications/widget/CaspianNotificationsView;

.field public c:Lcom/facebook/notifications/widget/PostFeedbackView;

.field public d:Lcom/facebook/notifications/widget/RevealAnimationOverlayView;

.field public e:Lcom/facebook/resources/ui/FbFrameLayout;

.field public f:LX/3Cw;

.field private g:Landroid/animation/AnimatorSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 530992
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 530993
    sget-object v0, LX/3Cw;->NOTIFICATION:LX/3Cw;

    iput-object v0, p0, LX/3Cv;->f:LX/3Cw;

    .line 530994
    const v0, 0x7f03145e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 530995
    const v0, 0x7f0d2e67

    invoke-virtual {p0, v0}, LX/3Cv;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/3Cv;->a:Landroid/widget/LinearLayout;

    .line 530996
    const v0, 0x7f0d2e68

    invoke-virtual {p0, v0}, LX/3Cv;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/widget/CaspianNotificationsView;

    iput-object v0, p0, LX/3Cv;->b:Lcom/facebook/notifications/widget/CaspianNotificationsView;

    .line 530997
    const v0, 0x7f0d1dee

    invoke-virtual {p0, v0}, LX/3Cv;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/widget/PostFeedbackView;

    iput-object v0, p0, LX/3Cv;->c:Lcom/facebook/notifications/widget/PostFeedbackView;

    .line 530998
    const v0, 0x7f0d2e6a

    invoke-virtual {p0, v0}, LX/3Cv;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;

    iput-object v0, p0, LX/3Cv;->d:Lcom/facebook/notifications/widget/RevealAnimationOverlayView;

    .line 530999
    iget-object v0, p0, LX/3Cv;->d:Lcom/facebook/notifications/widget/RevealAnimationOverlayView;

    invoke-virtual {p0}, LX/3Cv;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a00d5

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->setColor(I)V

    .line 531000
    const v0, 0x7f0d2e69

    invoke-virtual {p0, v0}, LX/3Cv;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    iput-object v0, p0, LX/3Cv;->e:Lcom/facebook/resources/ui/FbFrameLayout;

    .line 531001
    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 530980
    if-nez p1, :cond_0

    .line 530981
    invoke-static {p0}, LX/3Cv;->g(LX/3Cv;)V

    .line 530982
    :goto_0
    return-void

    .line 530983
    :cond_0
    new-array v0, v4, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 530984
    new-instance v1, LX/DsX;

    invoke-direct {v1, p0}, LX/DsX;-><init>(LX/3Cv;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 530985
    new-array v1, v4, [F

    fill-array-data v1, :array_1

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 530986
    new-instance v2, LX/DsY;

    invoke-direct {v2, p0}, LX/DsY;-><init>(LX/3Cv;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 530987
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, LX/3Cv;->g:Landroid/animation/AnimatorSet;

    .line 530988
    iget-object v2, p0, LX/3Cv;->g:Landroid/animation/AnimatorSet;

    new-instance v3, LX/DsZ;

    invoke-direct {v3, p0}, LX/DsZ;-><init>(LX/3Cv;)V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 530989
    iget-object v2, p0, LX/3Cv;->g:Landroid/animation/AnimatorSet;

    new-array v3, v4, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 530990
    iget-object v0, p0, LX/3Cv;->g:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x42c80000    # 100.0f
        0x0
    .end array-data

    .line 530991
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private b(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 530968
    if-nez p1, :cond_0

    .line 530969
    invoke-static {p0}, LX/3Cv;->h(LX/3Cv;)V

    .line 530970
    :goto_0
    return-void

    .line 530971
    :cond_0
    new-array v0, v4, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 530972
    new-instance v1, LX/Dsa;

    invoke-direct {v1, p0}, LX/Dsa;-><init>(LX/3Cv;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 530973
    new-array v1, v4, [F

    fill-array-data v1, :array_1

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    const-wide/16 v2, 0x190

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 530974
    new-instance v2, LX/Dsb;

    invoke-direct {v2, p0}, LX/Dsb;-><init>(LX/3Cv;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 530975
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, LX/3Cv;->g:Landroid/animation/AnimatorSet;

    .line 530976
    iget-object v2, p0, LX/3Cv;->g:Landroid/animation/AnimatorSet;

    new-instance v3, LX/Dsc;

    invoke-direct {v3, p0}, LX/Dsc;-><init>(LX/3Cv;)V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 530977
    iget-object v2, p0, LX/3Cv;->g:Landroid/animation/AnimatorSet;

    new-array v3, v4, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 530978
    iget-object v0, p0, LX/3Cv;->g:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 530979
    :array_1
    .array-data 4
        0x0
        0x42c80000    # 100.0f
    .end array-data
.end method

.method private f()V
    .locals 1

    .prologue
    .line 530960
    iget-object v0, p0, LX/3Cv;->g:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Cv;->g:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530961
    iget-object v0, p0, LX/3Cv;->g:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 530962
    :cond_0
    return-void
.end method

.method public static g(LX/3Cv;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 530954
    sget-object v0, LX/3Cw;->POST_FEEDBACK:LX/3Cw;

    iput-object v0, p0, LX/3Cv;->f:LX/3Cw;

    .line 530955
    iget-object v0, p0, LX/3Cv;->c:Lcom/facebook/notifications/widget/PostFeedbackView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/widget/PostFeedbackView;->setVisibility(I)V

    .line 530956
    iget-object v0, p0, LX/3Cv;->c:Lcom/facebook/notifications/widget/PostFeedbackView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/widget/PostFeedbackView;->setAlpha(F)V

    .line 530957
    iget-object v0, p0, LX/3Cv;->b:Lcom/facebook/notifications/widget/CaspianNotificationsView;

    invoke-virtual {v0, v2}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setVisibility(I)V

    .line 530958
    iget-object v0, p0, LX/3Cv;->d:Lcom/facebook/notifications/widget/RevealAnimationOverlayView;

    invoke-virtual {v0, v2}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->setVisibility(I)V

    .line 530959
    return-void
.end method

.method public static h(LX/3Cv;)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 530963
    sget-object v0, LX/3Cw;->NOTIFICATION:LX/3Cw;

    iput-object v0, p0, LX/3Cv;->f:LX/3Cw;

    .line 530964
    iget-object v0, p0, LX/3Cv;->c:Lcom/facebook/notifications/widget/PostFeedbackView;

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/widget/PostFeedbackView;->setVisibility(I)V

    .line 530965
    iget-object v0, p0, LX/3Cv;->d:Lcom/facebook/notifications/widget/RevealAnimationOverlayView;

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->setVisibility(I)V

    .line 530966
    iget-object v0, p0, LX/3Cv;->b:Lcom/facebook/notifications/widget/CaspianNotificationsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setVisibility(I)V

    .line 530967
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 530951
    invoke-direct {p0}, LX/3Cv;->f()V

    .line 530952
    sget-object v0, LX/3Cw;->NOTIFICATION:LX/3Cw;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/3Cv;->a(LX/3Cw;Z)V

    .line 530953
    return-void
.end method

.method public final a(LX/3Cw;Z)V
    .locals 1

    .prologue
    .line 530949
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/3Cv;->a(LX/3Cw;ZLandroid/graphics/Point;)V

    .line 530950
    return-void
.end method

.method public final a(LX/3Cw;ZLandroid/graphics/Point;)V
    .locals 3
    .param p3    # Landroid/graphics/Point;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, -0x1

    .line 530941
    iget-object v0, p0, LX/3Cv;->f:LX/3Cw;

    if-ne v0, p1, :cond_0

    .line 530942
    :goto_0
    return-void

    .line 530943
    :cond_0
    if-nez p3, :cond_1

    .line 530944
    iget-object v0, p0, LX/3Cv;->d:Lcom/facebook/notifications/widget/RevealAnimationOverlayView;

    invoke-virtual {v0, v1, v1}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->a(II)V

    .line 530945
    :goto_1
    sget-object v0, LX/Dsd;->a:[I

    invoke-virtual {p1}, LX/3Cw;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 530946
    :pswitch_0
    invoke-direct {p0, p2}, LX/3Cv;->a(Z)V

    goto :goto_0

    .line 530947
    :cond_1
    iget-object v0, p0, LX/3Cv;->d:Lcom/facebook/notifications/widget/RevealAnimationOverlayView;

    iget v1, p3, Landroid/graphics/Point;->x:I

    iget v2, p3, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->a(II)V

    goto :goto_1

    .line 530948
    :pswitch_1
    invoke-direct {p0, p2}, LX/3Cv;->b(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getNotificationView()Lcom/facebook/notifications/widget/CaspianNotificationsView;
    .locals 1

    .prologue
    .line 530935
    iget-object v0, p0, LX/3Cv;->b:Lcom/facebook/notifications/widget/CaspianNotificationsView;

    return-object v0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x514eafee

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 530938
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 530939
    invoke-direct {p0}, LX/3Cv;->f()V

    .line 530940
    const/16 v1, 0x2d

    const v2, -0x564795e4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    .prologue
    .line 530936
    iget-object v0, p0, LX/3Cv;->b:Lcom/facebook/notifications/widget/CaspianNotificationsView;

    invoke-virtual {v0, p1}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setBackgroundResource(I)V

    .line 530937
    return-void
.end method
