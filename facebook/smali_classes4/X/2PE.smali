.class public LX/2PE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static volatile e:LX/2PE;


# instance fields
.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 406071
    new-array v0, v3, [Ljava/lang/String;

    sget-object v1, LX/2PF;->f:LX/0U1;

    .line 406072
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 406073
    aput-object v1, v0, v2

    sput-object v0, LX/2PE;->a:[Ljava/lang/String;

    .line 406074
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, LX/2PF;->b:LX/0U1;

    .line 406075
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 406076
    aput-object v1, v0, v2

    sget-object v1, LX/2PF;->c:LX/0U1;

    .line 406077
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 406078
    aput-object v1, v0, v3

    sput-object v0, LX/2PE;->b:[Ljava/lang/String;

    .line 406079
    new-array v0, v3, [Ljava/lang/String;

    sget-object v1, LX/2PF;->e:LX/0U1;

    .line 406080
    iget-object v3, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v3

    .line 406081
    aput-object v1, v0, v2

    sput-object v0, LX/2PE;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406069
    iput-object p1, p0, LX/2PE;->d:LX/0Or;

    .line 406070
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0ux;
    .locals 4

    .prologue
    .line 406061
    const/4 v0, 0x3

    new-array v0, v0, [LX/0ux;

    const/4 v1, 0x0

    sget-object v2, LX/2PF;->a:LX/0U1;

    .line 406062
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 406063
    invoke-static {v2, p0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/2PF;->b:LX/0U1;

    .line 406064
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 406065
    invoke-static {v2, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/2PF;->c:LX/0U1;

    .line 406066
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 406067
    invoke-static {v2, p2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/2PE;
    .locals 4

    .prologue
    .line 406048
    sget-object v0, LX/2PE;->e:LX/2PE;

    if-nez v0, :cond_1

    .line 406049
    const-class v1, LX/2PE;

    monitor-enter v1

    .line 406050
    :try_start_0
    sget-object v0, LX/2PE;->e:LX/2PE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406051
    if-eqz v2, :cond_0

    .line 406052
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 406053
    new-instance v3, LX/2PE;

    const/16 p0, 0xdc6

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2PE;-><init>(LX/0Or;)V

    .line 406054
    move-object v0, v3

    .line 406055
    sput-object v0, LX/2PE;->e:LX/2PE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406056
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406057
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406058
    :cond_1
    sget-object v0, LX/2PE;->e:LX/2PE;

    return-object v0

    .line 406059
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406060
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 406044
    invoke-static {p1, p2, p3}, LX/2PE;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 406045
    const-string v1, "thread_devices"

    sget-object v2, LX/2PE;->a:[Ljava/lang/String;

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 406046
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 406047
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v5

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static b(LX/Eay;)LX/0ux;
    .locals 3

    .prologue
    .line 406039
    iget-object v0, p0, LX/Eay;->b:LX/Eap;

    move-object v0, v0

    .line 406040
    iget-object v1, v0, LX/Eap;->a:Ljava/lang/String;

    move-object v0, v1

    .line 406041
    invoke-static {v0}, LX/2PE;->c(Ljava/lang/String;)LX/Dog;

    move-result-object v0

    .line 406042
    iget-object v1, p0, LX/Eay;->a:Ljava/lang/String;

    move-object v1, v1

    .line 406043
    iget-object v2, v0, LX/Dog;->a:Ljava/lang/String;

    iget-object v0, v0, LX/Dog;->b:Ljava/lang/String;

    invoke-static {v1, v2, v0}, LX/2PE;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)LX/0ux;
    .locals 2

    .prologue
    .line 406082
    sget-object v0, LX/2PF;->a:LX/0U1;

    .line 406083
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 406084
    invoke-static {v0, p0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/lang/String;)LX/Dog;
    .locals 3

    .prologue
    .line 406037
    const/16 v0, 0x5f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 406038
    const/4 v0, -0x1

    if-ne v1, v0, :cond_0

    new-instance v0, LX/Dog;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LX/Dog;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/Dog;

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LX/Dog;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "LX/DpM;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 406024
    invoke-static {p1}, LX/2PE;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 406025
    iget-object v0, p0, LX/2PE;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 406026
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 406027
    const-string v1, "thread_devices"

    sget-object v2, LX/2PE;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v1

    .line 406028
    :try_start_1
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 406029
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 406030
    new-instance v2, LX/DpM;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/DpM;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 406031
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 406032
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 406033
    :catchall_1
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_1
    if-eqz v8, :cond_0

    if-eqz v1, :cond_3

    :try_start_4
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :cond_0
    :goto_2
    throw v0

    .line 406034
    :cond_1
    :try_start_5
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 406035
    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 406036
    if-eqz v8, :cond_2

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_2
    return-object v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method

.method public final a(LX/Eay;)LX/Eb1;
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 406000
    invoke-static {p1}, LX/2PE;->b(LX/Eay;)LX/0ux;

    move-result-object v4

    .line 406001
    iget-object v0, p0, LX/2PE;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 406002
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 406003
    const v1, 0x37a0d87e

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 406004
    :try_start_1
    const-string v1, "thread_devices"

    sget-object v2, LX/2PE;->c:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 406005
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 406006
    const/4 v1, 0x0

    :try_start_3
    invoke-interface {v2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 406007
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 406008
    new-instance v1, LX/Eb1;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-direct {v1, v3}, LX/Eb1;-><init>([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 406009
    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 406010
    const v2, -0x1d0b4355

    :try_start_5
    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 406011
    if-eqz v8, :cond_0

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_0
    move-object v0, v1

    .line 406012
    :goto_0
    return-object v0

    .line 406013
    :catch_0
    move-exception v1

    .line 406014
    :try_start_6
    const-class v3, LX/2PE;

    const-string v4, "Error decoding sender key record"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 406015
    :cond_1
    :try_start_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 406016
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 406017
    const v1, -0x507e826f

    :try_start_8
    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 406018
    if-eqz v8, :cond_2

    invoke-virtual {v8}, LX/2gJ;->close()V

    .line 406019
    :cond_2
    new-instance v0, LX/Eb1;

    invoke-direct {v0}, LX/Eb1;-><init>()V

    goto :goto_0

    .line 406020
    :catchall_0
    move-exception v1

    :try_start_9
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 406021
    :catchall_1
    move-exception v1

    const v2, -0x5b8b3ee3

    :try_start_a
    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 406022
    :catch_1
    move-exception v0

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 406023
    :catchall_2
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_1
    if-eqz v8, :cond_3

    if-eqz v1, :cond_4

    :try_start_c
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_2

    :cond_3
    :goto_2
    throw v0

    :catch_2
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_2

    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method

.method public final a(LX/Eay;LX/Eb1;)V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 405962
    invoke-static {p1}, LX/2PE;->b(LX/Eay;)LX/0ux;

    move-result-object v1

    .line 405963
    iget-object v0, p0, LX/2PE;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;

    .line 405964
    :try_start_0
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 405965
    const v4, -0x4af0ed71

    invoke-static {v3, v4}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 405966
    :try_start_1
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 405967
    sget-object v5, LX/2PF;->e:LX/0U1;

    .line 405968
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 405969
    invoke-static {}, LX/Ec0;->u()LX/Ec0;

    move-result-object v8

    .line 405970
    iget-object v6, p2, LX/Eb1;->a:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Eb2;

    .line 405971
    iget-object p2, v6, LX/Eb2;->a:LX/EcH;

    move-object v6, p2

    .line 405972
    invoke-virtual {v8, v6}, LX/Ec0;->a(LX/EcH;)LX/Ec0;

    goto :goto_0

    .line 405973
    :cond_0
    invoke-virtual {v8}, LX/Ec0;->l()LX/Ec1;

    move-result-object v6

    invoke-virtual {v6}, LX/EWX;->jZ_()[B

    move-result-object v6

    move-object v6, v6

    .line 405974
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 405975
    const-string v5, "thread_devices"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v5, v4, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 405976
    if-nez v1, :cond_3

    .line 405977
    iget-object v1, p1, LX/Eay;->b:LX/Eap;

    move-object v1, v1

    .line 405978
    iget-object v5, v1, LX/Eap;->a:Ljava/lang/String;

    move-object v1, v5

    .line 405979
    invoke-static {v1}, LX/2PE;->c(Ljava/lang/String;)LX/Dog;

    move-result-object v1

    .line 405980
    sget-object v5, LX/2PF;->a:LX/0U1;

    .line 405981
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 405982
    iget-object v6, p1, LX/Eay;->a:Ljava/lang/String;

    move-object v6, v6

    .line 405983
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 405984
    sget-object v5, LX/2PF;->b:LX/0U1;

    .line 405985
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 405986
    iget-object v6, v1, LX/Dog;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 405987
    sget-object v5, LX/2PF;->c:LX/0U1;

    .line 405988
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 405989
    iget-object v1, v1, LX/Dog;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 405990
    const-string v1, "thread_devices"

    const/4 v5, 0x0

    const v6, -0x68b865dc

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {v3, v1, v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, -0x3d103724

    invoke-static {v1}, LX/03h;->a(I)V

    .line 405991
    :cond_1
    :goto_1
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 405992
    const v1, -0x43312295

    :try_start_2
    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 405993
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/2gJ;->close()V

    .line 405994
    :cond_2
    return-void

    .line 405995
    :cond_3
    if-eq v1, v7, :cond_1

    .line 405996
    :try_start_3
    const-class v4, LX/2PE;

    const-string v5, "[TC] New sender key did not cause a unique row update (%d rows updated)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v7

    invoke-static {v4, v5, v6}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 405997
    :catchall_0
    move-exception v1

    const v4, 0x64fc2cf8

    :try_start_4
    invoke-static {v3, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 405998
    :catch_0
    move-exception v1

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 405999
    :catchall_1
    move-exception v2

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    :goto_2
    if-eqz v0, :cond_4

    if-eqz v2, :cond_5

    :try_start_6
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    :cond_4
    :goto_3
    throw v1

    :catch_1
    move-exception v0

    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v0}, LX/2gJ;->close()V

    goto :goto_3

    :catchall_2
    move-exception v1

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 405924
    iget-object v0, p0, LX/2PE;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/2gJ;

    const/4 v7, 0x0

    .line 405925
    :try_start_0
    invoke-virtual {v6}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 405926
    invoke-static {v0, p1, p2, p3}, LX/2PE;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 405927
    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 405928
    const-class v0, LX/2PE;

    const-string v1, "Dropping update which is older than current entry."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 405929
    if-eqz v6, :cond_0

    invoke-virtual {v6}, LX/2gJ;->close()V

    .line 405930
    :cond_0
    :goto_0
    return-void

    .line 405931
    :cond_1
    if-nez v1, :cond_2

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 405932
    :try_start_1
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 405933
    sget-object p0, LX/2PF;->a:LX/0U1;

    .line 405934
    iget-object p1, p0, LX/0U1;->d:Ljava/lang/String;

    move-object p0, p1

    .line 405935
    invoke-virtual {v8, p0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 405936
    sget-object p0, LX/2PF;->b:LX/0U1;

    .line 405937
    iget-object p1, p0, LX/0U1;->d:Ljava/lang/String;

    move-object p0, p1

    .line 405938
    invoke-virtual {v8, p0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 405939
    sget-object p0, LX/2PF;->c:LX/0U1;

    .line 405940
    iget-object p1, p0, LX/0U1;->d:Ljava/lang/String;

    move-object p0, p1

    .line 405941
    invoke-virtual {v8, p0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 405942
    sget-object p0, LX/2PF;->d:LX/0U1;

    .line 405943
    iget-object p1, p0, LX/0U1;->d:Ljava/lang/String;

    move-object p0, p1

    .line 405944
    invoke-virtual {v8, p0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 405945
    sget-object p0, LX/2PF;->f:LX/0U1;

    .line 405946
    iget-object p1, p0, LX/0U1;->d:Ljava/lang/String;

    move-object p0, p1

    .line 405947
    invoke-virtual {v8, p0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 405948
    const-string p0, "thread_devices"

    const/4 p1, 0x0

    const p2, -0xc01fe97

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {v0, p0, p1, v8}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v8, 0x4a633210    # 3722372.0f

    invoke-static {v8}, LX/03h;->a(I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 405949
    :goto_1
    if-eqz v6, :cond_0

    invoke-virtual {v6}, LX/2gJ;->close()V

    goto :goto_0

    :cond_2
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 405950
    :try_start_2
    invoke-static {v1, v2, v3}, LX/2PE;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v8

    .line 405951
    new-instance p0, Landroid/content/ContentValues;

    invoke-direct {p0}, Landroid/content/ContentValues;-><init>()V

    .line 405952
    sget-object p1, LX/2PF;->d:LX/0U1;

    .line 405953
    iget-object p2, p1, LX/0U1;->d:Ljava/lang/String;

    move-object p1, p2

    .line 405954
    invoke-virtual {p0, p1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 405955
    sget-object p1, LX/2PF;->f:LX/0U1;

    .line 405956
    iget-object p2, p1, LX/0U1;->d:Ljava/lang/String;

    move-object p1, p2

    .line 405957
    invoke-virtual {p0, p1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 405958
    const-string p1, "thread_devices"

    invoke-virtual {v8}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v8}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, p1, p0, p2, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 405959
    goto :goto_1

    .line 405960
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 405961
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_2
    if-eqz v6, :cond_3

    if-eqz v1, :cond_4

    :try_start_4
    invoke-virtual {v6}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    :goto_3
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_4
    invoke-virtual {v6}, LX/2gJ;->close()V

    goto :goto_3

    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/DpM;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 405899
    invoke-static {p1}, LX/2PE;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 405900
    iget-object v0, p0, LX/2PE;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;

    .line 405901
    :try_start_0
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 405902
    const v4, 0x1aafa75e

    invoke-static {v3, v4}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 405903
    :try_start_1
    const-string v4, "thread_devices"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 405904
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DpM;

    .line 405905
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 405906
    sget-object v6, LX/2PF;->a:LX/0U1;

    .line 405907
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 405908
    invoke-virtual {v5, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 405909
    sget-object v6, LX/2PF;->b:LX/0U1;

    .line 405910
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 405911
    iget-object v7, v1, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 405912
    sget-object v6, LX/2PF;->c:LX/0U1;

    .line 405913
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 405914
    iget-object v1, v1, LX/DpM;->instance_id:Ljava/lang/String;

    invoke-virtual {v5, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 405915
    const-string v1, "thread_devices"

    const/4 v6, 0x0

    const v7, 0x3aa9b376

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v3, v1, v6, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, -0x126f6c8

    invoke-static {v1}, LX/03h;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 405916
    :catchall_0
    move-exception v1

    const v4, -0x3a6317d0

    :try_start_2
    invoke-static {v3, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 405917
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 405918
    :catchall_1
    move-exception v2

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    :goto_1
    if-eqz v0, :cond_0

    if-eqz v2, :cond_3

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :cond_0
    :goto_2
    throw v1

    .line 405919
    :cond_1
    :try_start_5
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 405920
    const v1, -0x1949d898

    :try_start_6
    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 405921
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/2gJ;->close()V

    .line 405922
    :cond_2
    return-void

    .line 405923
    :catch_1
    move-exception v0

    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v0}, LX/2gJ;->close()V

    goto :goto_2

    :catchall_2
    move-exception v1

    goto :goto_1
.end method
