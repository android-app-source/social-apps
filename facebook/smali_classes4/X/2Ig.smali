.class public LX/2Ig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2Ig;


# instance fields
.field private final a:Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;)V
    .locals 0
    .param p1    # Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;
        .annotation runtime Lcom/facebook/http/push/PushedImageFetchEfficiencyTracker;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392063
    iput-object p1, p0, LX/2Ig;->a:Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;

    .line 392064
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ig;
    .locals 4

    .prologue
    .line 392066
    sget-object v0, LX/2Ig;->b:LX/2Ig;

    if-nez v0, :cond_1

    .line 392067
    const-class v1, LX/2Ig;

    monitor-enter v1

    .line 392068
    :try_start_0
    sget-object v0, LX/2Ig;->b:LX/2Ig;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 392069
    if-eqz v2, :cond_0

    .line 392070
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 392071
    new-instance p0, LX/2Ig;

    invoke-static {v0}, LX/1Ir;->a(LX/0QB;)Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;

    invoke-direct {p0, v3}, LX/2Ig;-><init>(Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;)V

    .line 392072
    move-object v0, p0

    .line 392073
    sput-object v0, LX/2Ig;->b:LX/2Ig;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392074
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 392075
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 392076
    :cond_1
    sget-object v0, LX/2Ig;->b:LX/2Ig;

    return-object v0

    .line 392077
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 392078
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 392065
    iget-object v0, p0, LX/2Ig;->a:Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;

    const-string v1, "android_image_push_fetch_efficiency"

    invoke-virtual {v0, v1}, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->c(Ljava/lang/String;)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/analytics/HoneyAnalyticsEvent;

    return-object v0
.end method
