.class public LX/2PA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:I

.field private static final c:LX/2PC;

.field public static final d:LX/2PC;

.field public static final e:LX/2PC;

.field private static volatile v:LX/2PA;


# instance fields
.field public volatile f:LX/03R;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Dof;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

.field private final j:LX/2PG;

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/IuC;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DoO;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/2PO;

.field public final n:LX/2PP;

.field public final o:LX/2PQ;

.field public final p:LX/2PM;

.field public final q:LX/2PJ;

.field private final r:LX/26j;

.field public final s:LX/0TD;

.field private final t:LX/01T;

.field private final u:LX/0WJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 405882
    const-class v0, LX/2PA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2PA;->a:Ljava/lang/String;

    .line 405883
    sget v0, LX/2PB;->a:I

    sput v0, LX/2PA;->b:I

    .line 405884
    new-instance v0, LX/2PC;

    const-string v1, "registration_state"

    invoke-direct {v0, v1}, LX/2PC;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/2PA;->c:LX/2PC;

    .line 405885
    new-instance v0, LX/2PC;

    const-string v1, "registration_count"

    invoke-direct {v0, v1}, LX/2PC;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/2PA;->d:LX/2PC;

    .line 405886
    new-instance v0, LX/2PC;

    const-string v1, "is_primary_device"

    invoke-direct {v0, v1}, LX/2PC;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/2PA;->e:LX/2PC;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/2PG;LX/0Or;LX/0Or;LX/2PO;LX/2PP;LX/2PQ;LX/2PM;LX/2PJ;LX/26j;LX/0TD;LX/01T;LX/0WJ;)V
    .locals 2
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p13    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/Dof;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;",
            "LX/2PG;",
            "LX/0Or",
            "<",
            "LX/IuC;",
            ">;",
            "LX/0Or",
            "<",
            "LX/DoO;",
            ">;",
            "LX/2PO;",
            "LX/2PP;",
            "LX/2PQ;",
            "LX/2PM;",
            "LX/2PJ;",
            "LX/26j;",
            "LX/0TD;",
            "LX/01T;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 405864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 405865
    sget-object v1, LX/03R;->UNSET:LX/03R;

    iput-object v1, p0, LX/2PA;->f:LX/03R;

    .line 405866
    iput-object p1, p0, LX/2PA;->g:LX/0Or;

    .line 405867
    iput-object p2, p0, LX/2PA;->h:LX/0Or;

    .line 405868
    iput-object p3, p0, LX/2PA;->i:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    .line 405869
    iput-object p4, p0, LX/2PA;->j:LX/2PG;

    .line 405870
    iput-object p5, p0, LX/2PA;->k:LX/0Or;

    .line 405871
    iput-object p6, p0, LX/2PA;->l:LX/0Or;

    .line 405872
    iput-object p7, p0, LX/2PA;->m:LX/2PO;

    .line 405873
    iput-object p8, p0, LX/2PA;->n:LX/2PP;

    .line 405874
    iput-object p9, p0, LX/2PA;->o:LX/2PQ;

    .line 405875
    iput-object p10, p0, LX/2PA;->p:LX/2PM;

    .line 405876
    iput-object p11, p0, LX/2PA;->q:LX/2PJ;

    .line 405877
    iput-object p12, p0, LX/2PA;->r:LX/26j;

    .line 405878
    iput-object p13, p0, LX/2PA;->s:LX/0TD;

    .line 405879
    move-object/from16 v0, p14

    iput-object v0, p0, LX/2PA;->t:LX/01T;

    .line 405880
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2PA;->u:LX/0WJ;

    .line 405881
    return-void
.end method

.method public static a(LX/0QB;)LX/2PA;
    .locals 3

    .prologue
    .line 405854
    sget-object v0, LX/2PA;->v:LX/2PA;

    if-nez v0, :cond_1

    .line 405855
    const-class v1, LX/2PA;

    monitor-enter v1

    .line 405856
    :try_start_0
    sget-object v0, LX/2PA;->v:LX/2PA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 405857
    if-eqz v2, :cond_0

    .line 405858
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2PA;->b(LX/0QB;)LX/2PA;

    move-result-object v0

    sput-object v0, LX/2PA;->v:LX/2PA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 405859
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 405860
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 405861
    :cond_1
    sget-object v0, LX/2PA;->v:LX/2PA;

    return-object v0

    .line 405862
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 405863
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/2PA;LX/Ebg;LX/Ebk;)V
    .locals 10

    .prologue
    .line 405834
    iget-object v0, p0, LX/2PA;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DoO;

    invoke-interface {v0}, LX/DoO;->a()LX/Eaf;

    move-result-object v0

    .line 405835
    iget-object v1, v0, LX/Eaf;->a:LX/Eae;

    move-object v0, v1

    .line 405836
    invoke-virtual {v0}, LX/Eae;->b()[B

    move-result-object v3

    .line 405837
    invoke-virtual {p2}, LX/Ebk;->b()LX/Eau;

    move-result-object v0

    .line 405838
    iget-object v1, v0, LX/Eau;->a:LX/Eat;

    move-object v0, v1

    .line 405839
    invoke-virtual {v0}, LX/Eat;->a()[B

    move-result-object v5

    .line 405840
    invoke-virtual {p1}, LX/Ebg;->b()LX/Eau;

    move-result-object v0

    .line 405841
    iget-object v1, v0, LX/Eau;->a:LX/Eat;

    move-object v0, v1

    .line 405842
    invoke-virtual {v0}, LX/Eat;->a()[B

    move-result-object v8

    .line 405843
    iget-object v0, p0, LX/2PA;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dof;

    sget-object v1, LX/2PA;->d:LX/2PC;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/Dod;->a(LX/2PC;I)I

    move-result v0

    move v9, v0

    .line 405844
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 405845
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 405846
    iget-object v0, p0, LX/2PA;->m:LX/2PO;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const-string v2, "tincan"

    invoke-virtual {p2}, LX/Ebk;->a()I

    move-result v4

    invoke-virtual {p2}, LX/Ebk;->c()[B

    move-result-object v6

    invoke-virtual {p1}, LX/Ebg;->a()I

    move-result v7

    invoke-virtual/range {v0 .. v8}, LX/2PO;->a([BLjava/lang/String;[BI[B[BI[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405847
    sget-object v0, LX/IuG;->STARTED:LX/IuG;

    invoke-static {p0, v0}, LX/2PA;->a(LX/2PA;LX/IuG;)V

    .line 405848
    add-int/lit8 v0, v9, 0x1

    .line 405849
    iget-object v1, p0, LX/2PA;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dof;

    sget-object v2, LX/2PA;->d:LX/2PC;

    invoke-virtual {v1, v2, v0}, LX/Dod;->b(LX/2PC;I)V

    .line 405850
    iget-object v0, p0, LX/2PA;->p:LX/2PM;

    const-string v1, "Register-device succeeded"

    invoke-virtual {v0, v1}, LX/2PM;->a(Ljava/lang/String;)V

    .line 405851
    :goto_0
    return-void

    .line 405852
    :cond_0
    iget-object v0, p0, LX/2PA;->p:LX/2PM;

    const-string v1, "Register-device failed"

    invoke-virtual {v0, v1}, LX/2PM;->a(Ljava/lang/String;)V

    .line 405853
    sget-object v0, LX/IuG;->FAILED:LX/IuG;

    invoke-static {p0, v0}, LX/2PA;->a(LX/2PA;LX/IuG;)V

    goto :goto_0
.end method

.method private static a(LX/2PA;LX/IuG;)V
    .locals 3

    .prologue
    .line 405832
    iget-object v0, p0, LX/2PA;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dof;

    sget-object v1, LX/2PA;->c:LX/2PC;

    invoke-virtual {p1}, LX/IuG;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/Dod;->b(LX/2PC;I)V

    .line 405833
    return-void
.end method

.method private static b(LX/0QB;)LX/2PA;
    .locals 17

    .prologue
    .line 405805
    new-instance v1, LX/2PA;

    const/16 v2, 0x2a14

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x12cb

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    invoke-static/range {p0 .. p0}, LX/2PG;->a(LX/0QB;)LX/2PG;

    move-result-object v5

    check-cast v5, LX/2PG;

    const/16 v6, 0x2a2c

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x2a22

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/2PO;->a(LX/0QB;)LX/2PO;

    move-result-object v8

    check-cast v8, LX/2PO;

    invoke-static/range {p0 .. p0}, LX/2PP;->a(LX/0QB;)LX/2PP;

    move-result-object v9

    check-cast v9, LX/2PP;

    invoke-static/range {p0 .. p0}, LX/2PQ;->a(LX/0QB;)LX/2PQ;

    move-result-object v10

    check-cast v10, LX/2PQ;

    invoke-static/range {p0 .. p0}, LX/2PM;->a(LX/0QB;)LX/2PM;

    move-result-object v11

    check-cast v11, LX/2PM;

    invoke-static/range {p0 .. p0}, LX/2PJ;->a(LX/0QB;)LX/2PJ;

    move-result-object v12

    check-cast v12, LX/2PJ;

    invoke-static/range {p0 .. p0}, LX/26j;->a(LX/0QB;)LX/26j;

    move-result-object v13

    check-cast v13, LX/26j;

    invoke-static/range {p0 .. p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v14

    check-cast v14, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v15

    check-cast v15, LX/01T;

    invoke-static/range {p0 .. p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v16

    check-cast v16, LX/0WJ;

    invoke-direct/range {v1 .. v16}, LX/2PA;-><init>(LX/0Or;LX/0Or;Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/2PG;LX/0Or;LX/0Or;LX/2PO;LX/2PP;LX/2PQ;LX/2PM;LX/2PJ;LX/26j;LX/0TD;LX/01T;LX/0WJ;)V

    .line 405806
    return-object v1
.end method

.method public static l(LX/2PA;)V
    .locals 3

    .prologue
    .line 405827
    iget-object v0, p0, LX/2PA;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 405828
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2PA;->f:LX/03R;

    .line 405829
    :goto_0
    return-void

    .line 405830
    :cond_0
    iget-object v0, p0, LX/2PA;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dof;

    sget-object v1, LX/2PA;->e:LX/2PC;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/Dod;->a(LX/2PC;Z)Z

    move-result v0

    .line 405831
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/2PA;->f:LX/03R;

    goto :goto_0
.end method

.method public static m(LX/2PA;)V
    .locals 3

    .prologue
    .line 405825
    iget-object v0, p0, LX/2PA;->s:LX/0TD;

    new-instance v1, Lcom/facebook/messaging/tincan/messenger/TincanDeviceManager$4;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/tincan/messenger/TincanDeviceManager$4;-><init>(LX/2PA;)V

    const v2, -0x119889eb

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 405826
    return-void
.end method

.method public static declared-synchronized n(LX/2PA;)V
    .locals 3

    .prologue
    .line 405814
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PA;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2PA;->r:LX/26j;

    invoke-virtual {v0}, LX/26j;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 405815
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 405816
    :cond_1
    :try_start_1
    invoke-static {p0}, LX/2PA;->p(LX/2PA;)LX/IuG;

    move-result-object v0

    .line 405817
    sget-object v1, LX/IuG;->NOT_STARTED:LX/IuG;

    if-ne v0, v1, :cond_2

    .line 405818
    invoke-static {p0}, LX/2PA;->o(LX/2PA;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 405819
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 405820
    :cond_2
    :try_start_2
    sget-object v1, LX/IuG;->FAILED:LX/IuG;

    if-ne v0, v1, :cond_0

    .line 405821
    invoke-static {p0}, LX/2PA;->r(LX/2PA;)LX/Ebg;

    move-result-object v1

    .line 405822
    iget-object v0, p0, LX/2PA;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuC;

    invoke-virtual {v0}, LX/IuC;->a()Ljava/util/List;

    move-result-object v0

    .line 405823
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ebk;

    .line 405824
    invoke-static {p0, v1, v0}, LX/2PA;->a(LX/2PA;LX/Ebg;LX/Ebk;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static declared-synchronized o(LX/2PA;)V
    .locals 2

    .prologue
    .line 405808
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PA;->u:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 405809
    invoke-static {p0}, LX/2PA;->r(LX/2PA;)LX/Ebg;

    move-result-object v0

    .line 405810
    iget-object v1, p0, LX/2PA;->i:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    invoke-virtual {v1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->f()LX/Ebk;

    move-result-object v1

    .line 405811
    invoke-static {p0, v0, v1}, LX/2PA;->a(LX/2PA;LX/Ebg;LX/Ebk;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405812
    monitor-exit p0

    return-void

    .line 405813
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static p(LX/2PA;)LX/IuG;
    .locals 3

    .prologue
    .line 405807
    iget-object v0, p0, LX/2PA;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dof;

    sget-object v1, LX/2PA;->c:LX/2PC;

    sget-object v2, LX/IuG;->NOT_STARTED:LX/IuG;

    invoke-virtual {v2}, LX/IuG;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/Dod;->a(LX/2PC;I)I

    move-result v0

    invoke-static {v0}, LX/IuG;->from(I)LX/IuG;

    move-result-object v0

    return-object v0
.end method

.method private static r(LX/2PA;)LX/Ebg;
    .locals 3

    .prologue
    .line 405887
    :try_start_0
    iget-object v0, p0, LX/2PA;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuC;

    sget v1, LX/2PA;->b:I

    invoke-virtual {v0, v1}, LX/IuC;->a(I)LX/Ebg;
    :try_end_0
    .catch LX/Eah; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 405888
    :goto_0
    return-object v0

    .line 405889
    :catch_0
    iget-object v0, p0, LX/2PA;->j:LX/2PG;

    invoke-virtual {v0}, LX/2PG;->b()LX/Ebg;

    move-result-object v1

    .line 405890
    iget-object v0, p0, LX/2PA;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuC;

    invoke-virtual {v1}, LX/Ebg;->a()I

    move-result v2

    invoke-virtual {v0, v2, v1}, LX/IuC;->a(ILX/Ebg;)V

    move-object v0, v1

    .line 405891
    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 405760
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PA;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dof;

    sget-object v1, LX/2PA;->e:LX/2PC;

    invoke-virtual {v0, v1, p1}, LX/Dod;->b(LX/2PC;Z)V

    .line 405761
    invoke-static {p1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/2PA;->f:LX/03R;

    .line 405762
    sget-object v0, LX/IuG;->COMPLETED:LX/IuG;

    invoke-static {p0, v0}, LX/2PA;->a(LX/2PA;LX/IuG;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405763
    monitor-exit p0

    return-void

    .line 405764
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 405765
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2PA;->p(LX/2PA;)LX/IuG;

    move-result-object v0

    sget-object v1, LX/IuG;->COMPLETED:LX/IuG;

    if-ne v0, v1, :cond_0

    .line 405766
    sget-object v0, LX/2PA;->a:Ljava/lang/String;

    const-string v1, "Tincan device registration failed, but already registered!"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405767
    :goto_0
    monitor-exit p0

    return-void

    .line 405768
    :cond_0
    :try_start_1
    sget-object v0, LX/IuG;->FAILED:LX/IuG;

    invoke-static {p0, v0}, LX/2PA;->a(LX/2PA;LX/IuG;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 405769
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 405770
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/IuG;->FAILED:LX/IuG;

    invoke-static {p0, v0}, LX/2PA;->a(LX/2PA;LX/IuG;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405771
    monitor-exit p0

    return-void

    .line 405772
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 3

    .prologue
    .line 405773
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PA;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dof;

    sget-object v1, LX/2PA;->e:LX/2PC;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/Dod;->b(LX/2PC;Z)V

    .line 405774
    sget-object v0, LX/03R;->NO:LX/03R;

    iput-object v0, p0, LX/2PA;->f:LX/03R;

    .line 405775
    iget-object v0, p0, LX/2PA;->p:LX/2PM;

    const-string v1, "Set not-primary-device success"

    invoke-virtual {v0, v1}, LX/2PM;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405776
    monitor-exit p0

    return-void

    .line 405777
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 2

    .prologue
    .line 405778
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PA;->p:LX/2PM;

    const-string v1, "Set not-primary-device failure"

    invoke-virtual {v0, v1}, LX/2PM;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405779
    monitor-exit p0

    return-void

    .line 405780
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 2

    .prologue
    .line 405781
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PA;->p:LX/2PM;

    const-string v1, "Set not-primary-device bad data"

    invoke-virtual {v0, v1}, LX/2PM;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405782
    monitor-exit p0

    return-void

    .line 405783
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()V
    .locals 3

    .prologue
    .line 405784
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PA;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dof;

    sget-object v1, LX/2PA;->e:LX/2PC;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/Dod;->b(LX/2PC;Z)V

    .line 405785
    sget-object v0, LX/03R;->YES:LX/03R;

    iput-object v0, p0, LX/2PA;->f:LX/03R;

    .line 405786
    iget-object v0, p0, LX/2PA;->p:LX/2PM;

    const-string v1, "Set primary-device success"

    invoke-virtual {v0, v1}, LX/2PM;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405787
    monitor-exit p0

    return-void

    .line 405788
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()V
    .locals 2

    .prologue
    .line 405789
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PA;->p:LX/2PM;

    const-string v1, "Set primary-device failure"

    invoke-virtual {v0, v1}, LX/2PM;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405790
    monitor-exit p0

    return-void

    .line 405791
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()V
    .locals 2

    .prologue
    .line 405792
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PA;->p:LX/2PM;

    const-string v1, "Set primary-device bad data"

    invoke-virtual {v0, v1}, LX/2PM;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405793
    monitor-exit p0

    return-void

    .line 405794
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized init()V
    .locals 4

    .prologue
    .line 405795
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PA;->t:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_0

    .line 405796
    :goto_0
    monitor-exit p0

    return-void

    .line 405797
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2PA;->m:LX/2PO;

    invoke-virtual {v0, p0}, LX/2PO;->a(LX/2PA;)V

    .line 405798
    iget-object v0, p0, LX/2PA;->n:LX/2PP;

    invoke-virtual {v0, p0}, LX/2PI;->a(Ljava/lang/Object;)V

    .line 405799
    iget-object v0, p0, LX/2PA;->o:LX/2PQ;

    invoke-virtual {v0, p0}, LX/2PI;->a(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 405800
    :try_start_2
    invoke-static {p0}, LX/2PA;->l(LX/2PA;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 405801
    :catch_0
    move-exception v0

    .line 405802
    :try_start_3
    sget-object v1, LX/2PA;->a:Ljava/lang/String;

    const-string v2, "Could not reload primary-device status from DB."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 405803
    sget-object v0, LX/03R;->NO:LX/03R;

    iput-object v0, p0, LX/2PA;->f:LX/03R;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 405804
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
