.class public LX/2Tw;
.super LX/1Eg;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile l:LX/2Tw;


# instance fields
.field private b:LX/2U0;

.field public final c:LX/2U1;

.field private final d:LX/0SG;

.field public final e:LX/2U3;

.field public final f:LX/0tX;

.field public final g:LX/2U4;

.field private final h:LX/0ad;

.field public final i:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation
.end field

.field private final j:LX/2U5;

.field private volatile k:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 414906
    const-class v0, LX/2Tw;

    sput-object v0, LX/2Tw;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2U0;LX/2U1;LX/0SG;LX/2U3;LX/0tX;LX/2U4;LX/0ad;Ljava/util/concurrent/ExecutorService;LX/2U5;)V
    .locals 2
    .param p8    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 414894
    const-string v0, "ADMINED_PAGE_LWI_PREFETCH_TASK"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 414895
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/2Tw;->k:J

    .line 414896
    iput-object p1, p0, LX/2Tw;->b:LX/2U0;

    .line 414897
    iput-object p2, p0, LX/2Tw;->c:LX/2U1;

    .line 414898
    iput-object p3, p0, LX/2Tw;->d:LX/0SG;

    .line 414899
    iput-object p4, p0, LX/2Tw;->e:LX/2U3;

    .line 414900
    iput-object p5, p0, LX/2Tw;->f:LX/0tX;

    .line 414901
    iput-object p6, p0, LX/2Tw;->g:LX/2U4;

    .line 414902
    iput-object p7, p0, LX/2Tw;->h:LX/0ad;

    .line 414903
    iput-object p8, p0, LX/2Tw;->i:Ljava/util/concurrent/ExecutorService;

    .line 414904
    iput-object p9, p0, LX/2Tw;->j:LX/2U5;

    .line 414905
    return-void
.end method

.method public static a(LX/0QB;)LX/2Tw;
    .locals 13

    .prologue
    .line 414879
    sget-object v0, LX/2Tw;->l:LX/2Tw;

    if-nez v0, :cond_1

    .line 414880
    const-class v1, LX/2Tw;

    monitor-enter v1

    .line 414881
    :try_start_0
    sget-object v0, LX/2Tw;->l:LX/2Tw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 414882
    if-eqz v2, :cond_0

    .line 414883
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 414884
    new-instance v3, LX/2Tw;

    .line 414885
    invoke-static {}, LX/2Ty;->a()LX/2U0;

    move-result-object v4

    move-object v4, v4

    .line 414886
    check-cast v4, LX/2U0;

    invoke-static {v0}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v5

    check-cast v5, LX/2U1;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v7

    check-cast v7, LX/2U3;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {v0}, LX/2U4;->a(LX/0QB;)LX/2U4;

    move-result-object v9

    check-cast v9, LX/2U4;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/ExecutorService;

    const-class v12, LX/2U5;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/2U5;

    invoke-direct/range {v3 .. v12}, LX/2Tw;-><init>(LX/2U0;LX/2U1;LX/0SG;LX/2U3;LX/0tX;LX/2U4;LX/0ad;Ljava/util/concurrent/ExecutorService;LX/2U5;)V

    .line 414887
    move-object v0, v3

    .line 414888
    sput-object v0, LX/2Tw;->l:LX/2Tw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414889
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 414890
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 414891
    :cond_1
    sget-object v0, LX/2Tw;->l:LX/2Tw;

    return-object v0

    .line 414892
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 414893
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final c()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 414878
    const-string v0, "ADMINED_PAGES_PREFETCH_TASK"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final f()J
    .locals 5

    .prologue
    const-wide/16 v0, -0x1

    .line 414873
    iget-object v2, p0, LX/2Tw;->c:LX/2U1;

    invoke-virtual {v2}, LX/2U1;->e()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 414874
    :cond_0
    :goto_0
    return-wide v0

    .line 414875
    :cond_1
    iget-object v2, p0, LX/2Tw;->b:LX/2U0;

    iget-boolean v2, v2, LX/2U0;->a:Z

    if-eqz v2, :cond_0

    .line 414876
    iget-object v2, p0, LX/2Tw;->h:LX/0ad;

    sget-short v3, LX/GDK;->D:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 414877
    iget-wide v0, p0, LX/2Tw;->k:J

    iget-object v2, p0, LX/2Tw;->b:LX/2U0;

    iget-wide v2, v2, LX/2U0;->b:J

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 414855
    sget-object v0, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    sget-object v1, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 414866
    iget-object v2, p0, LX/2Tw;->c:LX/2U1;

    invoke-virtual {v2}, LX/2U1;->e()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 414867
    :cond_0
    :goto_0
    return v0

    .line 414868
    :cond_1
    iget-object v2, p0, LX/2Tw;->b:LX/2U0;

    iget-boolean v2, v2, LX/2U0;->a:Z

    if-eqz v2, :cond_0

    .line 414869
    iget-object v2, p0, LX/2Tw;->h:LX/0ad;

    sget-short v3, LX/GDK;->D:S

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 414870
    iget-wide v2, p0, LX/2Tw;->k:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    move v0, v1

    .line 414871
    goto :goto_0

    .line 414872
    :cond_2
    iget-wide v2, p0, LX/2Tw;->k:J

    iget-object v4, p0, LX/2Tw;->b:LX/2U0;

    iget-wide v4, v4, LX/2U0;->b:J

    add-long/2addr v2, v4

    iget-object v4, p0, LX/2Tw;->d:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 414856
    iget-object v0, p0, LX/2Tw;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/2Tw;->k:J

    .line 414857
    iget-object v0, p0, LX/2Tw;->c:LX/2U1;

    invoke-virtual {v0}, LX/2U1;->e()Ljava/lang/String;

    move-result-object v0

    .line 414858
    if-nez v0, :cond_0

    .line 414859
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 414860
    :cond_0
    iget-object v1, p0, LX/2Tw;->f:LX/0tX;

    const/4 v3, 0x3

    .line 414861
    new-instance v2, LX/ABw;

    invoke-direct {v2}, LX/ABw;-><init>()V

    move-object v2, v2

    .line 414862
    const-string v4, "page_id"

    invoke-virtual {v2, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v4, "count"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    move-object v2, v2

    .line 414863
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    move-object v2, v2

    .line 414864
    invoke-virtual {v1, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 414865
    new-instance v2, LX/GEV;

    invoke-direct {v2, p0, v0}, LX/GEV;-><init>(LX/2Tw;Ljava/lang/String;)V

    iget-object v0, p0, LX/2Tw;->i:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
