.class public LX/3FI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/3FI;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/engine/VideoPlayerViewProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0So;

.field private final c:LX/13m;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Yk;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0ad;

.field private final g:LX/0Uh;

.field private final h:LX/19j;

.field private final i:LX/19a;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0So;LX/13m;LX/0Or;LX/0Ot;LX/0ad;LX/0Uh;LX/19j;LX/19a;LX/0Ot;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/video/engine/IsInVideosLoopingBlackListedGateKeeper;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/engine/VideoPlayerViewProvider;",
            ">;",
            "LX/0So;",
            "LX/13m;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Yk;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/19j;",
            "LX/19a;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 538721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538722
    iput-object p1, p0, LX/3FI;->a:LX/0Ot;

    .line 538723
    iput-object p2, p0, LX/3FI;->b:LX/0So;

    .line 538724
    iput-object p3, p0, LX/3FI;->c:LX/13m;

    .line 538725
    iput-object p5, p0, LX/3FI;->d:LX/0Ot;

    .line 538726
    iput-object p4, p0, LX/3FI;->e:LX/0Or;

    .line 538727
    iput-object p6, p0, LX/3FI;->f:LX/0ad;

    .line 538728
    iput-object p7, p0, LX/3FI;->g:LX/0Uh;

    .line 538729
    iput-object p8, p0, LX/3FI;->h:LX/19j;

    .line 538730
    iput-object p9, p0, LX/3FI;->i:LX/19a;

    .line 538731
    iput-object p10, p0, LX/3FI;->j:LX/0Ot;

    .line 538732
    return-void
.end method

.method public static a(LX/0QB;)LX/3FI;
    .locals 14

    .prologue
    .line 538733
    sget-object v0, LX/3FI;->k:LX/3FI;

    if-nez v0, :cond_1

    .line 538734
    const-class v1, LX/3FI;

    monitor-enter v1

    .line 538735
    :try_start_0
    sget-object v0, LX/3FI;->k:LX/3FI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 538736
    if-eqz v2, :cond_0

    .line 538737
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 538738
    new-instance v3, LX/3FI;

    const/16 v4, 0x1348

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/13m;->a(LX/0QB;)LX/13m;

    move-result-object v6

    check-cast v6, LX/13m;

    const/16 v7, 0x384

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x1333

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {v0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v11

    check-cast v11, LX/19j;

    invoke-static {v0}, LX/19a;->a(LX/0QB;)LX/19a;

    move-result-object v12

    check-cast v12, LX/19a;

    const/16 v13, 0x259

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, LX/3FI;-><init>(LX/0Ot;LX/0So;LX/13m;LX/0Or;LX/0Ot;LX/0ad;LX/0Uh;LX/19j;LX/19a;LX/0Ot;)V

    .line 538739
    move-object v0, v3

    .line 538740
    sput-object v0, LX/3FI;->k:LX/3FI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 538741
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 538742
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 538743
    :cond_1
    sget-object v0, LX/3FI;->k:LX/3FI;

    return-object v0

    .line 538744
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 538745
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/3FI;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 538746
    iget-object v0, p0, LX/3FI;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/util/AttributeSet;ILX/2pf;LX/2qI;LX/1C2;LX/19R;LX/0Sh;LX/2pw;ZLX/2px;LX/2q3;)LX/2q7;
    .locals 30

    .prologue
    .line 538747
    const-string v1, "VideoPlayerFactory.allocateVideoPlayer"

    const v2, -0xf3584de

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 538748
    :try_start_0
    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    .line 538749
    invoke-static {v2}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ScheduledExecutorService;

    .line 538750
    invoke-virtual/range {p7 .. p7}, LX/19R;->a()LX/2q9;

    move-result-object v10

    .line 538751
    const-string v1, "VideoPlayerFactory.TextureViewVideoPlayer.init"

    const v3, -0x738f49ce

    invoke-static {v1, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 538752
    :try_start_1
    new-instance v1, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/3FI;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/19d;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/3FI;->d:LX/0Ot;

    invoke-static {v2}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v12

    check-cast v12, LX/0ka;

    invoke-static {v2}, LX/19U;->a(LX/0QB;)LX/0TD;

    move-result-object v14

    check-cast v14, LX/0TD;

    invoke-static {v2}, LX/19b;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v15

    check-cast v15, Ljava/lang/Boolean;

    invoke-static {v2}, LX/19X;->a(LX/0QB;)LX/19Z;

    move-result-object v18

    check-cast v18, LX/19Z;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/3FI;->b:LX/0So;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/3FI;->c:LX/13m;

    move-object/from16 v22, v0

    invoke-static/range {p0 .. p0}, LX/3FI;->a(LX/3FI;)Z

    move-result v23

    new-instance v24, LX/3FK;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3FI;->f:LX/0ad;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/3FI;->g:LX/0Uh;

    move-object/from16 v0, v24

    invoke-direct {v0, v2, v3}, LX/3FK;-><init>(LX/0ad;LX/0Uh;)V

    move-object/from16 v0, p0

    iget-object v0, v0, LX/3FI;->f:LX/0ad;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/3FI;->g:LX/0Uh;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/3FI;->h:LX/19j;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/3FI;->i:LX/19a;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/3FI;->j:LX/0Ot;

    move-object/from16 v29, v0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v11, p9

    move/from16 v16, p10

    move-object/from16 v17, p8

    move-object/from16 v20, p11

    move-object/from16 v21, p12

    invoke-direct/range {v1 .. v29}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILX/19d;LX/0Ot;LX/2pf;LX/2qI;LX/1C2;LX/2q9;LX/2pw;LX/0ka;Ljava/util/concurrent/ScheduledExecutorService;LX/0TD;Ljava/lang/Boolean;ZLX/0Sh;LX/19Z;LX/0So;LX/2px;LX/2q3;LX/13m;ZLX/3FK;LX/0ad;LX/0Uh;LX/19j;LX/19a;LX/0Ot;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 538753
    const v2, 0x4e318b6f    # 7.4467629E8f

    :try_start_2
    invoke-static {v2}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 538754
    const v2, -0xcac104f

    invoke-static {v2}, LX/02m;->a(I)V

    return-object v1

    .line 538755
    :catchall_0
    move-exception v1

    const v2, 0xcb3a344

    :try_start_3
    invoke-static {v2}, LX/02m;->a(I)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 538756
    :catchall_1
    move-exception v1

    const v2, -0x444ad921

    invoke-static {v2}, LX/02m;->a(I)V

    throw v1
.end method
