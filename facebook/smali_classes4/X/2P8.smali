.class public final LX/2P8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/27U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/27U",
        "<",
        "LX/G7U;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 0

    .prologue
    .line 405743
    iput-object p1, p0, LX/2P8;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/G7U;)V
    .locals 2

    .prologue
    .line 405744
    iget-object v0, p0, LX/2P8;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    const/4 v1, 0x0

    .line 405745
    iput-boolean v1, v0, LX/276;->h:Z

    .line 405746
    if-nez p1, :cond_0

    .line 405747
    :goto_0
    return-void

    .line 405748
    :cond_0
    invoke-interface {p1}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 405749
    iget-object v0, p0, LX/2P8;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    sget-object v1, LX/27W;->SMARTLOCK_SUCCESS:LX/27W;

    invoke-virtual {v1}, LX/27W;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/276;->a(Ljava/lang/String;)V

    .line 405750
    invoke-interface {p1}, LX/G7U;->b()Lcom/google/android/gms/auth/api/credentials/Credential;

    move-result-object v0

    .line 405751
    iget-object v1, p0, LX/2P8;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    .line 405752
    invoke-static {v1, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Lcom/google/android/gms/auth/api/credentials/Credential;)V

    .line 405753
    goto :goto_0

    .line 405754
    :cond_1
    iget-object v0, p0, LX/2P8;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-interface {p1}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    .line 405755
    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Lcom/google/android/gms/common/api/Status;)V

    .line 405756
    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/2NW;)V
    .locals 0

    .prologue
    .line 405757
    check-cast p1, LX/G7U;

    invoke-direct {p0, p1}, LX/2P8;->a(LX/G7U;)V

    return-void
.end method
