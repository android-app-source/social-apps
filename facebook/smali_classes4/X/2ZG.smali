.class public LX/2ZG;
.super LX/2ZH;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0WJ;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2ac;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/418;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/01T;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0WJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2ac;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/418;",
            ">;",
            "LX/01T;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 422269
    invoke-direct {p0}, LX/2ZH;-><init>()V

    .line 422270
    iput-object p1, p0, LX/2ZG;->a:LX/0Ot;

    .line 422271
    iput-object p2, p0, LX/2ZG;->b:LX/0Ot;

    .line 422272
    iput-object p3, p0, LX/2ZG;->c:LX/0Ot;

    .line 422273
    sget-object v0, LX/01T;->FB4A:LX/01T;

    if-ne p4, v0, :cond_0

    .line 422274
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 422275
    if-nez v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    iput v0, p0, LX/2ZG;->d:I

    .line 422276
    return-void

    .line 422277
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c()LX/2ZE;
    .locals 2

    .prologue
    .line 422268
    new-instance v0, LX/2ZI;

    invoke-direct {v0, p0}, LX/2ZI;-><init>(LX/2ZG;)V

    return-object v0
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 422258
    iget-object v0, p0, LX/2ZG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    .line 422259
    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 422260
    if-nez v1, :cond_1

    .line 422261
    :cond_0
    return-void

    .line 422262
    :cond_1
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v2

    .line 422263
    iget-object v1, p0, LX/2ZG;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/418;

    const-string v3, "logged_in_user_ids"

    iget v4, p0, LX/2ZG;->d:I

    invoke-virtual {v1, v3, v2, v4}, LX/418;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v3

    .line 422264
    if-eqz v3, :cond_0

    .line 422265
    const/4 v1, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    .line 422266
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0WJ;->a(Ljava/lang/String;)V

    .line 422267
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method public final dq_()Z
    .locals 1

    .prologue
    .line 422257
    iget-object v0, p0, LX/2ZG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->e()Z

    move-result v0

    return v0
.end method
