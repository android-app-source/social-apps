.class public LX/2WJ;
.super LX/15w;
.source ""


# instance fields
.field public b:LX/15w;


# direct methods
.method public constructor <init>(LX/15w;)V
    .locals 0

    .prologue
    .line 418768
    invoke-direct {p0}, LX/15w;-><init>()V

    .line 418769
    iput-object p1, p0, LX/2WJ;->b:LX/15w;

    .line 418770
    return-void
.end method


# virtual methods
.method public final A()F
    .locals 1

    .prologue
    .line 418771
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->A()F

    move-result v0

    return v0
.end method

.method public final B()D
    .locals 2

    .prologue
    .line 418772
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->B()D

    move-result-wide v0

    return-wide v0
.end method

.method public final C()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 418773
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->C()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public final D()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 418774
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final E()I
    .locals 1

    .prologue
    .line 418775
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->E()I

    move-result v0

    return v0
.end method

.method public final F()J
    .locals 2

    .prologue
    .line 418776
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->F()J

    move-result-wide v0

    return-wide v0
.end method

.method public final G()D
    .locals 2

    .prologue
    .line 418777
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->G()D

    move-result-wide v0

    return-wide v0
.end method

.method public final H()Z
    .locals 1

    .prologue
    .line 418778
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->H()Z

    move-result v0

    return v0
.end method

.method public final I()Ljava/lang/String;
    .locals 1

    .prologue
    .line 418779
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->I()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(D)D
    .locals 3

    .prologue
    .line 418780
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0, p1, p2}, LX/15w;->a(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public final a(J)J
    .locals 3

    .prologue
    .line 418781
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0, p1, p2}, LX/15w;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a()LX/0lD;
    .locals 1

    .prologue
    .line 418782
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->a()LX/0lD;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 418783
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0, p1}, LX/15w;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0lD;)V
    .locals 1

    .prologue
    .line 418784
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0, p1}, LX/15w;->a(LX/0lD;)V

    .line 418785
    return-void
.end method

.method public final a(LX/0lr;)Z
    .locals 1

    .prologue
    .line 418786
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0, p1}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    return v0
.end method

.method public final a(Z)Z
    .locals 1

    .prologue
    .line 418787
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0, p1}, LX/15w;->a(Z)Z

    move-result v0

    return v0
.end method

.method public final a(LX/0ln;)[B
    .locals 1

    .prologue
    .line 418788
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0, p1}, LX/15w;->a(LX/0ln;)[B

    move-result-object v0

    return-object v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 418754
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0, p1}, LX/15w;->b(I)I

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 418789
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public c()LX/15z;
    .locals 1

    .prologue
    .line 418790
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 418766
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->close()V

    .line 418767
    return-void
.end method

.method public final d()LX/15z;
    .locals 1

    .prologue
    .line 418791
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->d()LX/15z;

    move-result-object v0

    return-object v0
.end method

.method public f()LX/15w;
    .locals 1

    .prologue
    .line 418743
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->f()LX/15w;

    .line 418744
    return-object p0
.end method

.method public g()LX/15z;
    .locals 1

    .prologue
    .line 418753
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->g()LX/15z;

    move-result-object v0

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 418752
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->h()Z

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 418751
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/12V;
    .locals 1

    .prologue
    .line 418750
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->j()LX/12V;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/28G;
    .locals 1

    .prologue
    .line 418749
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->k()LX/28G;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/28G;
    .locals 1

    .prologue
    .line 418748
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->l()LX/28G;

    move-result-object v0

    return-object v0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 418746
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->n()V

    .line 418747
    return-void
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 418745
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final p()[C
    .locals 1

    .prologue
    .line 418742
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->p()[C

    move-result-object v0

    return-object v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 418755
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->q()I

    move-result v0

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 418756
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->r()I

    move-result v0

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 418757
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->s()Z

    move-result v0

    return v0
.end method

.method public final t()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 418758
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public final u()LX/16L;
    .locals 1

    .prologue
    .line 418759
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->u()LX/16L;

    move-result-object v0

    return-object v0
.end method

.method public final v()B
    .locals 1

    .prologue
    .line 418760
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->v()B

    move-result v0

    return v0
.end method

.method public final version()LX/0ne;
    .locals 1

    .prologue
    .line 418761
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->version()LX/0ne;

    move-result-object v0

    return-object v0
.end method

.method public final w()S
    .locals 1

    .prologue
    .line 418762
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->w()S

    move-result v0

    return v0
.end method

.method public final x()I
    .locals 1

    .prologue
    .line 418763
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->x()I

    move-result v0

    return v0
.end method

.method public final y()J
    .locals 2

    .prologue
    .line 418764
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->y()J

    move-result-wide v0

    return-wide v0
.end method

.method public final z()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 418765
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->z()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method
