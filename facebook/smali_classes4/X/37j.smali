.class public final LX/37j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/37k;
.implements LX/37l;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/37i;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/384;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/380;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/37m;

.field private final g:LX/37n;

.field private final h:LX/37p;

.field private final i:LX/37q;

.field public final j:LX/37u;

.field public final k:Z

.field public l:LX/38P;

.field public m:LX/384;

.field public n:LX/384;

.field public o:LX/389;

.field public p:LX/38A;

.field private q:LX/385;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 502064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 502065
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/37j;->b:Ljava/util/ArrayList;

    .line 502066
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/37j;->c:Ljava/util/ArrayList;

    .line 502067
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/37j;->d:Ljava/util/ArrayList;

    .line 502068
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/37j;->e:Ljava/util/ArrayList;

    .line 502069
    new-instance v0, LX/37m;

    invoke-direct {v0}, LX/37m;-><init>()V

    iput-object v0, p0, LX/37j;->f:LX/37m;

    .line 502070
    new-instance v0, LX/37n;

    invoke-direct {v0, p0}, LX/37n;-><init>(LX/37j;)V

    iput-object v0, p0, LX/37j;->g:LX/37n;

    .line 502071
    new-instance v0, LX/37p;

    invoke-direct {v0, p0}, LX/37p;-><init>(LX/37j;)V

    iput-object v0, p0, LX/37j;->h:LX/37p;

    .line 502072
    iput-object p1, p0, LX/37j;->a:Landroid/content/Context;

    .line 502073
    invoke-static {p1}, LX/37q;->a(Landroid/content/Context;)LX/37q;

    move-result-object v0

    iput-object v0, p0, LX/37j;->i:LX/37q;

    .line 502074
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 502075
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 502076
    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v1

    move v1, v1

    .line 502077
    :goto_0
    move v0, v1

    .line 502078
    iput-boolean v0, p0, LX/37j;->k:Z

    .line 502079
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    .line 502080
    new-instance v0, LX/37r;

    invoke-direct {v0, p1, p0}, LX/37r;-><init>(Landroid/content/Context;LX/37l;)V

    .line 502081
    :goto_1
    move-object v0, v0

    .line 502082
    iput-object v0, p0, LX/37j;->j:LX/37u;

    .line 502083
    iget-object v0, p0, LX/37j;->j:LX/37u;

    invoke-virtual {p0, v0}, LX/37j;->a(LX/37v;)V

    .line 502084
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 502085
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_2

    .line 502086
    new-instance v0, LX/37s;

    invoke-direct {v0, p1, p0}, LX/37s;-><init>(Landroid/content/Context;LX/37l;)V

    goto :goto_1

    .line 502087
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_3

    .line 502088
    new-instance v0, LX/37t;

    invoke-direct {v0, p1, p0}, LX/37t;-><init>(Landroid/content/Context;LX/37l;)V

    goto :goto_1

    .line 502089
    :cond_3
    new-instance v0, LX/37z;

    invoke-direct {v0, p1}, LX/37z;-><init>(Landroid/content/Context;)V

    goto :goto_1
.end method

.method private a(LX/380;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x2

    .line 502177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 502178
    iget-object v2, p1, LX/380;->c:LX/381;

    .line 502179
    iget-object p1, v2, LX/381;->a:Landroid/content/ComponentName;

    move-object v2, p1

    .line 502180
    move-object v2, v2

    .line 502181
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 502182
    invoke-static {p0, v2}, LX/37j;->b(LX/37j;Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    move-object v0, v2

    .line 502183
    :goto_0
    return-object v0

    :cond_0
    move v0, v1

    .line 502184
    :goto_1
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s_%d"

    new-array v5, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v6, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 502185
    invoke-static {p0, v3}, LX/37j;->b(LX/37j;Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_1

    move-object v0, v3

    .line 502186
    goto :goto_0

    .line 502187
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static a(LX/37j;LX/380;LX/382;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 502188
    iget-object v0, p1, LX/380;->d:LX/382;

    if-eq v0, p2, :cond_18

    .line 502189
    iput-object p2, p1, LX/380;->d:LX/382;

    .line 502190
    const/4 v0, 0x1

    .line 502191
    :goto_0
    move v0, v0

    .line 502192
    if-eqz v0, :cond_16

    .line 502193
    if-eqz p2, :cond_a

    .line 502194
    invoke-virtual {p2}, LX/382;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 502195
    invoke-virtual {p2}, LX/382;->a()Ljava/util/List;

    move-result-object v6

    .line 502196
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v5, v2

    move v3, v2

    .line 502197
    :goto_1
    if-ge v5, v7, :cond_b

    .line 502198
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/383;

    .line 502199
    invoke-virtual {v0}, LX/383;->a()Ljava/lang/String;

    move-result-object v1

    .line 502200
    invoke-virtual {p1, v1}, LX/380;->a(Ljava/lang/String;)I

    move-result v8

    .line 502201
    if-gez v8, :cond_1

    .line 502202
    invoke-direct {p0, p1, v1}, LX/37j;->a(LX/380;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 502203
    new-instance v8, LX/384;

    invoke-direct {v8, p1, v1, v4}, LX/384;-><init>(LX/380;Ljava/lang/String;Ljava/lang/String;)V

    .line 502204
    iget-object v4, p1, LX/380;->b:Ljava/util/ArrayList;

    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v4, v3, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 502205
    iget-object v3, p0, LX/37j;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502206
    invoke-virtual {v8, v0}, LX/384;->a(LX/383;)I

    .line 502207
    sget-boolean v0, LX/37i;->d:Z

    if-eqz v0, :cond_0

    .line 502208
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Route added: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502209
    :cond_0
    iget-object v0, p0, LX/37j;->h:LX/37p;

    const/16 v3, 0x101

    invoke-virtual {v0, v3, v8}, LX/37p;->a(ILjava/lang/Object;)V

    move v0, v2

    .line 502210
    :goto_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v3, v1

    move v2, v0

    goto :goto_1

    .line 502211
    :cond_1
    if-ge v8, v3, :cond_2

    .line 502212
    const-string v1, "MediaRouter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Ignoring route descriptor with duplicate id: "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    move v1, v3

    goto :goto_2

    .line 502213
    :cond_2
    iget-object v1, p1, LX/380;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/384;

    .line 502214
    iget-object v9, p1, LX/380;->b:Ljava/util/ArrayList;

    add-int/lit8 v4, v3, 0x1

    invoke-static {v9, v8, v3}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 502215
    invoke-virtual {v1, v0}, LX/384;->a(LX/383;)I

    move-result v0

    .line 502216
    if-eqz v0, :cond_17

    .line 502217
    and-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_4

    .line 502218
    sget-boolean v3, LX/37i;->d:Z

    if-eqz v3, :cond_3

    .line 502219
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "Route changed: "

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502220
    :cond_3
    iget-object v3, p0, LX/37j;->h:LX/37p;

    const/16 v8, 0x103

    invoke-virtual {v3, v8, v1}, LX/37p;->a(ILjava/lang/Object;)V

    .line 502221
    :cond_4
    and-int/lit8 v3, v0, 0x2

    if-eqz v3, :cond_6

    .line 502222
    sget-boolean v3, LX/37i;->d:Z

    if-eqz v3, :cond_5

    .line 502223
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "Route volume changed: "

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502224
    :cond_5
    iget-object v3, p0, LX/37j;->h:LX/37p;

    const/16 v8, 0x104

    invoke-virtual {v3, v8, v1}, LX/37p;->a(ILjava/lang/Object;)V

    .line 502225
    :cond_6
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    .line 502226
    sget-boolean v0, LX/37i;->d:Z

    if-eqz v0, :cond_7

    .line 502227
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Route presentation display changed: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502228
    :cond_7
    iget-object v0, p0, LX/37j;->h:LX/37p;

    const/16 v3, 0x105

    invoke-virtual {v0, v3, v1}, LX/37p;->a(ILjava/lang/Object;)V

    .line 502229
    :cond_8
    iget-object v0, p0, LX/37j;->n:LX/384;

    if-ne v1, v0, :cond_17

    .line 502230
    const/4 v0, 0x1

    move v1, v4

    goto/16 :goto_2

    .line 502231
    :cond_9
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Ignoring invalid provider descriptor: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    move v3, v2

    .line 502232
    :cond_b
    iget-object v0, p1, LX/380;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-lt v1, v3, :cond_c

    .line 502233
    iget-object v0, p1, LX/380;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/384;

    .line 502234
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, LX/384;->a(LX/383;)I

    .line 502235
    iget-object v4, p0, LX/37j;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 502236
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    .line 502237
    :cond_c
    const/4 v5, 0x0

    .line 502238
    iget-object v0, p0, LX/37j;->m:LX/384;

    if-eqz v0, :cond_d

    iget-object v0, p0, LX/37j;->m:LX/384;

    invoke-static {v0}, LX/37j;->c(LX/384;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 502239
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Clearing the default route because it is no longer selectable: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/37j;->m:LX/384;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502240
    iput-object v5, p0, LX/37j;->m:LX/384;

    .line 502241
    :cond_d
    iget-object v0, p0, LX/37j;->m:LX/384;

    if-nez v0, :cond_f

    iget-object v0, p0, LX/37j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 502242
    iget-object v0, p0, LX/37j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/384;

    .line 502243
    invoke-virtual {v0}, LX/384;->m()LX/37v;

    move-result-object v4

    iget-object v6, p0, LX/37j;->j:LX/37u;

    if-ne v4, v6, :cond_1a

    iget-object v4, v0, LX/384;->b:Ljava/lang/String;

    const-string v6, "DEFAULT_ROUTE"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    const/4 v4, 0x1

    :goto_4
    move v4, v4

    .line 502244
    if-eqz v4, :cond_e

    invoke-static {v0}, LX/37j;->c(LX/384;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 502245
    iput-object v0, p0, LX/37j;->m:LX/384;

    .line 502246
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Found default route: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/37j;->m:LX/384;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502247
    :cond_f
    iget-object v0, p0, LX/37j;->n:LX/384;

    if-eqz v0, :cond_10

    iget-object v0, p0, LX/37j;->n:LX/384;

    invoke-static {v0}, LX/37j;->c(LX/384;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 502248
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unselecting the current route because it is no longer selectable: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/37j;->n:LX/384;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502249
    invoke-static {p0, v5}, LX/37j;->e(LX/37j;LX/384;)V

    .line 502250
    :cond_10
    iget-object v0, p0, LX/37j;->n:LX/384;

    if-nez v0, :cond_19

    .line 502251
    iget-object v0, p0, LX/37j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/384;

    .line 502252
    iget-object v4, p0, LX/37j;->m:LX/384;

    if-eq v0, v4, :cond_11

    .line 502253
    invoke-virtual {v0}, LX/384;->m()LX/37v;

    move-result-object v4

    iget-object v5, p0, LX/37j;->j:LX/37u;

    if-ne v4, v5, :cond_1c

    const-string v4, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v4}, LX/384;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1c

    const-string v4, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v4}, LX/384;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1c

    const/4 v4, 0x1

    :goto_5
    move v4, v4

    .line 502254
    if-eqz v4, :cond_11

    invoke-static {v0}, LX/37j;->c(LX/384;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 502255
    :goto_6
    move-object v0, v0

    .line 502256
    invoke-static {p0, v0}, LX/37j;->e(LX/37j;LX/384;)V

    .line 502257
    :cond_12
    :goto_7
    iget-object v0, p1, LX/380;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_8
    if-lt v1, v3, :cond_14

    .line 502258
    iget-object v0, p1, LX/380;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/384;

    .line 502259
    sget-boolean v2, LX/37i;->d:Z

    if-eqz v2, :cond_13

    .line 502260
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Route removed: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502261
    :cond_13
    iget-object v2, p0, LX/37j;->h:LX/37p;

    const/16 v4, 0x102

    invoke-virtual {v2, v4, v0}, LX/37p;->a(ILjava/lang/Object;)V

    .line 502262
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_8

    .line 502263
    :cond_14
    sget-boolean v0, LX/37i;->d:Z

    if-eqz v0, :cond_15

    .line 502264
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Provider changed: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502265
    :cond_15
    iget-object v0, p0, LX/37j;->h:LX/37p;

    const/16 v1, 0x203

    invoke-virtual {v0, v1, p1}, LX/37p;->a(ILjava/lang/Object;)V

    .line 502266
    :cond_16
    return-void

    :cond_17
    move v0, v2

    move v1, v4

    goto/16 :goto_2

    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 502267
    :cond_19
    if-eqz v2, :cond_12

    .line 502268
    invoke-static {p0}, LX/37j;->e(LX/37j;)V

    goto :goto_7

    :cond_1a
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_1b
    iget-object v0, p0, LX/37j;->m:LX/384;

    goto :goto_6

    :cond_1c
    const/4 v4, 0x0

    goto :goto_5
.end method

.method private static b(LX/37j;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 502275
    iget-object v0, p0, LX/37j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 502276
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 502277
    iget-object v0, p0, LX/37j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/384;

    iget-object v0, v0, LX/384;->c:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 502278
    :goto_1
    return v0

    .line 502279
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 502280
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static c(LX/37j;LX/37v;)I
    .locals 3

    .prologue
    .line 502269
    iget-object v0, p0, LX/37j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 502270
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 502271
    iget-object v0, p0, LX/37j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/380;

    iget-object v0, v0, LX/380;->a:LX/37v;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 502272
    :goto_1
    return v0

    .line 502273
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 502274
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static c(LX/384;)Z
    .locals 1

    .prologue
    .line 502148
    iget-object v0, p0, LX/384;->q:LX/383;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/384;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/37j;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 502149
    iget-object v1, p0, LX/37j;->n:LX/384;

    if-eqz v1, :cond_3

    .line 502150
    iget-object v1, p0, LX/37j;->f:LX/37m;

    iget-object v2, p0, LX/37j;->n:LX/384;

    .line 502151
    iget v3, v2, LX/384;->l:I

    move v2, v3

    .line 502152
    iput v2, v1, LX/37m;->a:I

    .line 502153
    iget-object v1, p0, LX/37j;->f:LX/37m;

    iget-object v2, p0, LX/37j;->n:LX/384;

    .line 502154
    iget v3, v2, LX/384;->m:I

    move v2, v3

    .line 502155
    iput v2, v1, LX/37m;->b:I

    .line 502156
    iget-object v1, p0, LX/37j;->f:LX/37m;

    iget-object v2, p0, LX/37j;->n:LX/384;

    .line 502157
    iget v3, v2, LX/384;->k:I

    move v2, v3

    .line 502158
    iput v2, v1, LX/37m;->c:I

    .line 502159
    iget-object v1, p0, LX/37j;->f:LX/37m;

    iget-object v2, p0, LX/37j;->n:LX/384;

    .line 502160
    iget v3, v2, LX/384;->j:I

    move v2, v3

    .line 502161
    iput v2, v1, LX/37m;->d:I

    .line 502162
    iget-object v1, p0, LX/37j;->f:LX/37m;

    iget-object v2, p0, LX/37j;->n:LX/384;

    .line 502163
    iget v3, v2, LX/384;->i:I

    move v2, v3

    .line 502164
    iput v2, v1, LX/37m;->e:I

    .line 502165
    iget-object v1, p0, LX/37j;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    .line 502166
    :goto_0
    if-ge v1, v2, :cond_0

    .line 502167
    iget-object v3, p0, LX/37j;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 502168
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 502169
    :cond_0
    iget-object v1, p0, LX/37j;->q:LX/385;

    if-eqz v1, :cond_3

    .line 502170
    iget-object v1, p0, LX/37j;->f:LX/37m;

    iget v1, v1, LX/37m;->c:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 502171
    const/4 v0, 0x2

    .line 502172
    :cond_1
    iget-object v1, p0, LX/37j;->q:LX/385;

    iget-object v2, p0, LX/37j;->f:LX/37m;

    iget v2, v2, LX/37m;->b:I

    iget-object v3, p0, LX/37j;->f:LX/37m;

    iget v3, v3, LX/37m;->a:I

    .line 502173
    iget-object v4, v1, LX/385;->d:LX/386;

    if-eqz v4, :cond_2

    iget v4, v1, LX/385;->b:I

    if-ne v0, v4, :cond_2

    iget v4, v1, LX/385;->c:I

    if-eq v2, v4, :cond_3

    .line 502174
    :cond_2
    new-instance v4, LX/387;

    invoke-direct {v4, v1, v0, v2, v3}, LX/387;-><init>(LX/385;III)V

    iput-object v4, v1, LX/385;->d:LX/386;

    .line 502175
    iget-object p0, v1, LX/385;->d:LX/386;

    invoke-static {p0}, LX/388;->a(LX/386;)V

    .line 502176
    :cond_3
    return-void
.end method

.method public static e(LX/37j;LX/384;)V
    .locals 3

    .prologue
    .line 502129
    iget-object v0, p0, LX/37j;->n:LX/384;

    if-eq v0, p1, :cond_5

    .line 502130
    iget-object v0, p0, LX/37j;->n:LX/384;

    if-eqz v0, :cond_1

    .line 502131
    sget-boolean v0, LX/37i;->d:Z

    if-eqz v0, :cond_0

    .line 502132
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Route unselected: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/37j;->n:LX/384;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502133
    :cond_0
    iget-object v0, p0, LX/37j;->h:LX/37p;

    const/16 v1, 0x107

    iget-object v2, p0, LX/37j;->n:LX/384;

    invoke-virtual {v0, v1, v2}, LX/37p;->a(ILjava/lang/Object;)V

    .line 502134
    iget-object v0, p0, LX/37j;->o:LX/389;

    if-eqz v0, :cond_1

    .line 502135
    iget-object v0, p0, LX/37j;->o:LX/389;

    invoke-virtual {v0}, LX/389;->c()V

    .line 502136
    iget-object v0, p0, LX/37j;->o:LX/389;

    invoke-virtual {v0}, LX/389;->a()V

    .line 502137
    const/4 v0, 0x0

    iput-object v0, p0, LX/37j;->o:LX/389;

    .line 502138
    :cond_1
    iput-object p1, p0, LX/37j;->n:LX/384;

    .line 502139
    iget-object v0, p0, LX/37j;->n:LX/384;

    if-eqz v0, :cond_4

    .line 502140
    invoke-virtual {p1}, LX/384;->m()LX/37v;

    move-result-object v0

    iget-object v1, p1, LX/384;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/37v;->a(Ljava/lang/String;)LX/389;

    move-result-object v0

    iput-object v0, p0, LX/37j;->o:LX/389;

    .line 502141
    iget-object v0, p0, LX/37j;->o:LX/389;

    if-eqz v0, :cond_2

    .line 502142
    iget-object v0, p0, LX/37j;->o:LX/389;

    invoke-virtual {v0}, LX/389;->b()V

    .line 502143
    :cond_2
    sget-boolean v0, LX/37i;->d:Z

    if-eqz v0, :cond_3

    .line 502144
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Route selected: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/37j;->n:LX/384;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502145
    :cond_3
    iget-object v0, p0, LX/37j;->h:LX/37p;

    const/16 v1, 0x106

    iget-object v2, p0, LX/37j;->n:LX/384;

    invoke-virtual {v0, v1, v2}, LX/37p;->a(ILjava/lang/Object;)V

    .line 502146
    :cond_4
    invoke-static {p0}, LX/37j;->e(LX/37j;)V

    .line 502147
    :cond_5
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/384;
    .locals 2

    .prologue
    .line 502122
    iget-object v0, p0, LX/37j;->j:LX/37u;

    invoke-static {p0, v0}, LX/37j;->c(LX/37j;LX/37v;)I

    move-result v0

    .line 502123
    if-ltz v0, :cond_0

    .line 502124
    iget-object v1, p0, LX/37j;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/380;

    .line 502125
    invoke-virtual {v0, p1}, LX/380;->a(Ljava/lang/String;)I

    move-result v1

    .line 502126
    if-ltz v1, :cond_0

    .line 502127
    iget-object v0, v0, LX/380;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/384;

    .line 502128
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/37v;)V
    .locals 3

    .prologue
    .line 502110
    invoke-static {p0, p1}, LX/37j;->c(LX/37j;LX/37v;)I

    move-result v0

    .line 502111
    if-gez v0, :cond_1

    .line 502112
    new-instance v0, LX/380;

    invoke-direct {v0, p1}, LX/380;-><init>(LX/37v;)V

    .line 502113
    iget-object v1, p0, LX/37j;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502114
    sget-boolean v1, LX/37i;->d:Z

    if-eqz v1, :cond_0

    .line 502115
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Provider added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502116
    :cond_0
    iget-object v1, p0, LX/37j;->h:LX/37p;

    const/16 v2, 0x201

    invoke-virtual {v1, v2, v0}, LX/37p;->a(ILjava/lang/Object;)V

    .line 502117
    iget-object v1, p1, LX/37v;->g:LX/382;

    move-object v1, v1

    .line 502118
    invoke-static {p0, v0, v1}, LX/37j;->a(LX/37j;LX/380;LX/382;)V

    .line 502119
    iget-object v0, p0, LX/37j;->g:LX/37n;

    invoke-virtual {p1, v0}, LX/37v;->a(LX/37o;)V

    .line 502120
    iget-object v0, p0, LX/37j;->p:LX/38A;

    invoke-virtual {p1, v0}, LX/37v;->a(LX/38A;)V

    .line 502121
    :cond_1
    return-void
.end method

.method public final a(LX/384;)V
    .locals 3

    .prologue
    .line 502104
    iget-object v0, p0, LX/37j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 502105
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring attempt to select removed route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 502106
    :goto_0
    return-void

    .line 502107
    :cond_0
    iget-boolean v0, p1, LX/384;->f:Z

    if-nez v0, :cond_1

    .line 502108
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring attempt to select disabled route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 502109
    :cond_1
    invoke-static {p0, p1}, LX/37j;->e(LX/37j;LX/384;)V

    goto :goto_0
.end method

.method public final b()LX/384;
    .locals 2

    .prologue
    .line 502101
    iget-object v0, p0, LX/37j;->n:LX/384;

    if-nez v0, :cond_0

    .line 502102
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no currently selected route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502103
    :cond_0
    iget-object v0, p0, LX/37j;->n:LX/384;

    return-object v0
.end method

.method public final b(LX/37v;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 502090
    invoke-static {p0, p1}, LX/37j;->c(LX/37j;LX/37v;)I

    move-result v1

    .line 502091
    if-ltz v1, :cond_1

    .line 502092
    invoke-virtual {p1, v2}, LX/37v;->a(LX/37o;)V

    .line 502093
    invoke-virtual {p1, v2}, LX/37v;->a(LX/38A;)V

    .line 502094
    iget-object v0, p0, LX/37j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/380;

    .line 502095
    invoke-static {p0, v0, v2}, LX/37j;->a(LX/37j;LX/380;LX/382;)V

    .line 502096
    sget-boolean v2, LX/37i;->d:Z

    if-eqz v2, :cond_0

    .line 502097
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Provider removed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502098
    :cond_0
    iget-object v2, p0, LX/37j;->h:LX/37p;

    const/16 v3, 0x202

    invoke-virtual {v2, v3, v0}, LX/37p;->a(ILjava/lang/Object;)V

    .line 502099
    iget-object v0, p0, LX/37j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 502100
    :cond_1
    return-void
.end method
