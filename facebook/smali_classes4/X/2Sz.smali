.class public LX/2Sz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2Sz;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 413673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413674
    return-void
.end method

.method public static a(LX/0QB;)LX/2Sz;
    .locals 3

    .prologue
    .line 413647
    sget-object v0, LX/2Sz;->a:LX/2Sz;

    if-nez v0, :cond_1

    .line 413648
    const-class v1, LX/2Sz;

    monitor-enter v1

    .line 413649
    :try_start_0
    sget-object v0, LX/2Sz;->a:LX/2Sz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413650
    if-eqz v2, :cond_0

    .line 413651
    :try_start_1
    new-instance v0, LX/2Sz;

    invoke-direct {v0}, LX/2Sz;-><init>()V

    .line 413652
    move-object v0, v0

    .line 413653
    sput-object v0, LX/2Sz;->a:LX/2Sz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413654
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413655
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413656
    :cond_1
    sget-object v0, LX/2Sz;->a:LX/2Sz;

    return-object v0

    .line 413657
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413658
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/7G7;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 413670
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 413671
    const-string v1, "connectionFreshness"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 413672
    return-object v0
.end method

.method public static a(LX/1qK;)Ljava/io/Serializable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">(",
            "LX/1qK;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 413668
    iget-object v0, p0, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 413669
    const-string v1, "syncPayload"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/1qK;)Lcom/facebook/sync/service/SyncOperationParamsUtil$FullRefreshParams;
    .locals 3

    .prologue
    .line 413664
    iget-object v0, p0, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 413665
    const-string v0, "fullRefreshReason"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/sync/analytics/FullRefreshReason;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/sync/analytics/FullRefreshReason;

    .line 413666
    const-string v2, "syncTokenToReplace"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 413667
    new-instance v2, Lcom/facebook/sync/service/SyncOperationParamsUtil$FullRefreshParams;

    invoke-direct {v2, v0, v1}, Lcom/facebook/sync/service/SyncOperationParamsUtil$FullRefreshParams;-><init>(Lcom/facebook/sync/analytics/FullRefreshReason;Ljava/lang/String;)V

    return-object v2
.end method

.method public static d(LX/1qK;)LX/7G7;
    .locals 2

    .prologue
    .line 413659
    iget-object v0, p0, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 413660
    const-string v1, "connectionFreshness"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/7G7;

    .line 413661
    if-nez v0, :cond_0

    .line 413662
    sget-object v0, LX/7G7;->ENSURE:LX/7G7;

    .line 413663
    :cond_0
    return-object v0
.end method
