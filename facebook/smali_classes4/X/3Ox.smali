.class public LX/3Ox;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3Ox;


# instance fields
.field public final b:LX/3Oz;

.field public final c:Z

.field public final d:LX/03R;

.field private final e:Z

.field public final f:I

.field public final g:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 561011
    new-instance v0, LX/3Oy;

    invoke-direct {v0}, LX/3Oy;-><init>()V

    invoke-virtual {v0}, LX/3Oy;->g()LX/3Ox;

    move-result-object v0

    sput-object v0, LX/3Ox;->a:LX/3Ox;

    return-void
.end method

.method public constructor <init>(LX/3Oy;)V
    .locals 4

    .prologue
    .line 560997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560998
    iget-object v0, p1, LX/3Oy;->a:LX/3Oz;

    move-object v0, v0

    .line 560999
    iput-object v0, p0, LX/3Ox;->b:LX/3Oz;

    .line 561000
    iget-boolean v0, p1, LX/3Oy;->b:Z

    move v0, v0

    .line 561001
    iput-boolean v0, p0, LX/3Ox;->c:Z

    .line 561002
    iget-object v0, p1, LX/3Oy;->c:LX/03R;

    move-object v0, v0

    .line 561003
    iput-object v0, p0, LX/3Ox;->d:LX/03R;

    .line 561004
    iget-boolean v0, p1, LX/3Oy;->d:Z

    move v0, v0

    .line 561005
    iput-boolean v0, p0, LX/3Ox;->e:Z

    .line 561006
    iget v0, p1, LX/3Oy;->e:I

    move v0, v0

    .line 561007
    iput v0, p0, LX/3Ox;->f:I

    .line 561008
    iget-wide v2, p1, LX/3Oy;->f:J

    move-wide v0, v2

    .line 561009
    iput-wide v0, p0, LX/3Ox;->g:J

    .line 561010
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 560986
    if-ne p0, p1, :cond_1

    .line 560987
    :cond_0
    :goto_0
    return v0

    .line 560988
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 560989
    :cond_3
    check-cast p1, LX/3Ox;

    .line 560990
    iget-boolean v2, p0, LX/3Ox;->c:Z

    iget-boolean v3, p1, LX/3Ox;->c:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 560991
    :cond_4
    iget-boolean v2, p0, LX/3Ox;->e:Z

    iget-boolean v3, p1, LX/3Ox;->e:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    .line 560992
    :cond_5
    iget-object v2, p0, LX/3Ox;->b:LX/3Oz;

    iget-object v3, p1, LX/3Ox;->b:LX/3Oz;

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    .line 560993
    :cond_6
    iget v2, p0, LX/3Ox;->f:I

    iget v3, p1, LX/3Ox;->f:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    .line 560994
    :cond_7
    iget-object v2, p0, LX/3Ox;->d:LX/03R;

    iget-object v3, p1, LX/3Ox;->d:LX/03R;

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    .line 560995
    :cond_8
    iget-wide v2, p0, LX/3Ox;->g:J

    iget-wide v4, p1, LX/3Ox;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 560996
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/3Ox;->b:LX/3Oz;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, LX/3Ox;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, LX/3Ox;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/3Ox;->d:LX/03R;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, LX/3Ox;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
