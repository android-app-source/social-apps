.class public LX/2Ag;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0WV;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0WV;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0WV;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 377472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 377473
    iput-object p1, p0, LX/2Ag;->a:Landroid/content/Context;

    .line 377474
    iput-object p2, p0, LX/2Ag;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 377475
    iput-object p3, p0, LX/2Ag;->c:LX/0WV;

    .line 377476
    iput-object p4, p0, LX/2Ag;->d:LX/0Ot;

    .line 377477
    iput-object p5, p0, LX/2Ag;->e:LX/0Ot;

    .line 377478
    return-void
.end method
