.class public final LX/2El;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:LX/2DG;


# direct methods
.method public constructor <init>(LX/2DG;)V
    .locals 0

    .prologue
    .line 385992
    iput-object p1, p0, LX/2El;->a:LX/2DG;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, 0x675ea186

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 385993
    new-instance v0, LX/2Wi;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v0, v2}, LX/2Wi;-><init>(Landroid/os/Bundle;)V

    .line 385994
    iget v2, v0, LX/2Wi;->a:I

    iget-object v3, p0, LX/2El;->a:LX/2DG;

    iget v3, v3, LX/2DG;->a:I

    if-eq v2, v3, :cond_0

    .line 385995
    const/16 v0, 0x27

    const v2, 0x2a014d4

    invoke-static {p2, v4, v0, v2, v1}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 385996
    :goto_0
    return-void

    .line 385997
    :cond_0
    iget-object v2, v0, LX/2Wi;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 385998
    iget-object v2, p0, LX/2El;->a:LX/2DG;

    iget-object v3, v0, LX/2Wi;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/2DG;->c(LX/2DG;Ljava/lang/String;)V

    .line 385999
    :cond_1
    iget-boolean v2, v0, LX/2Wi;->c:Z

    if-nez v2, :cond_3

    .line 386000
    iget-object v2, v0, LX/2Wi;->b:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 386001
    iget-object v2, p0, LX/2El;->a:LX/2DG;

    invoke-static {v2}, LX/2DG;->h(LX/2DG;)V

    .line 386002
    :cond_2
    invoke-static {}, LX/0WQ;->a()Ljava/lang/String;

    move-result-object v2

    .line 386003
    iget-object v3, v0, LX/2Wi;->d:LX/01J;

    invoke-virtual {v3, v2}, LX/01J;->a(Ljava/lang/Object;)I

    move-result v2

    .line 386004
    if-ltz v2, :cond_3

    .line 386005
    iget-object v0, v0, LX/2Wi;->d:LX/01J;

    invoke-virtual {v0, v2}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 386006
    iget-object v2, p0, LX/2El;->a:LX/2DG;

    invoke-static {v2, v0}, LX/2DG;->b(LX/2DG;Ljava/io/File;)V

    .line 386007
    :cond_3
    const v0, -0x639ed519

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0
.end method
