.class public LX/2Hv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2Hv;


# instance fields
.field private final a:LX/0Xl;

.field private final b:LX/1fU;

.field private final c:LX/0lC;

.field private final d:LX/03V;

.field private final e:LX/0So;


# direct methods
.method public constructor <init>(LX/0Xl;LX/1fU;LX/0lC;LX/03V;LX/0So;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391306
    iput-object p1, p0, LX/2Hv;->a:LX/0Xl;

    .line 391307
    iput-object p2, p0, LX/2Hv;->b:LX/1fU;

    .line 391308
    iput-object p3, p0, LX/2Hv;->c:LX/0lC;

    .line 391309
    iput-object p4, p0, LX/2Hv;->d:LX/03V;

    .line 391310
    iput-object p5, p0, LX/2Hv;->e:LX/0So;

    .line 391311
    return-void
.end method

.method public static a(LX/0QB;)LX/2Hv;
    .locals 9

    .prologue
    .line 391312
    sget-object v0, LX/2Hv;->f:LX/2Hv;

    if-nez v0, :cond_1

    .line 391313
    const-class v1, LX/2Hv;

    monitor-enter v1

    .line 391314
    :try_start_0
    sget-object v0, LX/2Hv;->f:LX/2Hv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391315
    if-eqz v2, :cond_0

    .line 391316
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 391317
    new-instance v3, LX/2Hv;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-static {v0}, LX/1fU;->a(LX/0QB;)LX/1fU;

    move-result-object v5

    check-cast v5, LX/1fU;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v6

    check-cast v6, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v8

    check-cast v8, LX/0So;

    invoke-direct/range {v3 .. v8}, LX/2Hv;-><init>(LX/0Xl;LX/1fU;LX/0lC;LX/03V;LX/0So;)V

    .line 391318
    move-object v0, v3

    .line 391319
    sput-object v0, LX/2Hv;->f:LX/2Hv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391320
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391321
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391322
    :cond_1
    sget-object v0, LX/2Hv;->f:LX/2Hv;

    return-object v0

    .line 391323
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391324
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/6cX;)LX/76J;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "LX/6cX",
            "<TT;>;)",
            "LX/76J",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 391304
    new-instance v0, LX/76O;

    iget-object v2, p0, LX/2Hv;->a:LX/0Xl;

    iget-object v3, p0, LX/2Hv;->e:LX/0So;

    iget-object v4, p0, LX/2Hv;->b:LX/1fU;

    move-object v1, p1

    move-object v5, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, LX/76O;-><init>(Ljava/lang/String;LX/0Xl;LX/0So;LX/1fU;LX/6cX;LX/2gV;LX/03V;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/6cX;LX/2gV;LX/03V;)LX/76J;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "LX/6cX",
            "<TT;>;",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClient;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")",
            "LX/76J",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 391302
    new-instance v0, LX/76O;

    iget-object v2, p0, LX/2Hv;->a:LX/0Xl;

    iget-object v3, p0, LX/2Hv;->e:LX/0So;

    iget-object v4, p0, LX/2Hv;->b:LX/1fU;

    move-object v1, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, LX/76O;-><init>(Ljava/lang/String;LX/0Xl;LX/0So;LX/1fU;LX/6cX;LX/2gV;LX/03V;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/FGX;)LX/76J;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lcom/facebook/push/mqtt/service/response/JsonMqttResponseProcessor$Callback",
            "<TT;>;)",
            "LX/76J",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 391303
    new-instance v0, LX/76K;

    iget-object v2, p0, LX/2Hv;->a:LX/0Xl;

    iget-object v3, p0, LX/2Hv;->e:LX/0So;

    iget-object v4, p0, LX/2Hv;->b:LX/1fU;

    iget-object v5, p0, LX/2Hv;->c:LX/0lC;

    iget-object v6, p0, LX/2Hv;->d:LX/03V;

    move-object v1, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LX/76K;-><init>(Ljava/lang/String;LX/0Xl;LX/0So;LX/1fU;LX/0lC;LX/03V;LX/FGX;)V

    return-object v0
.end method
