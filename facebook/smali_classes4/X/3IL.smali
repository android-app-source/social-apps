.class public final LX/3IL;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/7Lq;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/Video360Plugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V
    .locals 0

    .prologue
    .line 546036
    iput-object p1, p0, LX/3IL;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/7Lq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 546037
    const-class v0, LX/7Lq;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 546038
    check-cast p1, LX/7Lq;

    .line 546039
    iget-object v0, p0, LX/3IL;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546040
    iget v0, p1, LX/7Lq;->b:I

    move v0, v0

    .line 546041
    packed-switch v0, :pswitch_data_0

    .line 546042
    :cond_0
    :goto_0
    return-void

    .line 546043
    :pswitch_0
    iget-object v0, p0, LX/3IL;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->g()LX/2qW;

    .line 546044
    iget-object v0, p0, LX/3IL;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->c:LX/1C2;

    iget-object v1, p0, LX/3IL;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, LX/3IL;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v2, v2, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->s()LX/04D;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1C2;->c(Ljava/lang/String;LX/04D;)LX/1C2;

    .line 546045
    iget-object v0, p0, LX/3IL;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    const/4 v1, 0x1

    .line 546046
    iput-boolean v1, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->I:Z

    .line 546047
    goto :goto_0

    .line 546048
    :pswitch_1
    iget-object v0, p0, LX/3IL;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->g()LX/2qW;

    move-result-object v0

    .line 546049
    iget v1, p1, LX/7Lq;->a:F

    move v1, v1

    .line 546050
    invoke-virtual {v0, v1}, LX/2qW;->a(F)V

    goto :goto_0

    .line 546051
    :pswitch_2
    iget-object v0, p0, LX/3IL;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->g()LX/2qW;

    move-result-object v0

    invoke-virtual {v0}, LX/2qW;->g()V

    .line 546052
    iget-object v0, p0, LX/3IL;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    const/4 v1, 0x0

    .line 546053
    iput-boolean v1, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->I:Z

    .line 546054
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
