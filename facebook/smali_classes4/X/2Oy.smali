.class public LX/2Oy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/1Hy;

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2Oy;


# instance fields
.field public final c:LX/1Hn;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Dok;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1Hb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 404457
    const-class v0, LX/2Oy;

    sput-object v0, LX/2Oy;->b:Ljava/lang/Class;

    .line 404458
    sget-object v0, LX/1Hy;->KEY_256:LX/1Hy;

    sput-object v0, LX/2Oy;->a:LX/1Hy;

    return-void
.end method

.method public constructor <init>(LX/1Hn;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Hn;",
            "LX/0Or",
            "<",
            "LX/Dok;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 404472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 404473
    iput-object p1, p0, LX/2Oy;->c:LX/1Hn;

    .line 404474
    iput-object p2, p0, LX/2Oy;->d:LX/0Or;

    .line 404475
    const-string v0, ""

    invoke-static {v0}, LX/1Hb;->a(Ljava/lang/String;)LX/1Hb;

    move-result-object v0

    iput-object v0, p0, LX/2Oy;->e:LX/1Hb;

    .line 404476
    return-void
.end method

.method public static a(LX/0QB;)LX/2Oy;
    .locals 5

    .prologue
    .line 404459
    sget-object v0, LX/2Oy;->f:LX/2Oy;

    if-nez v0, :cond_1

    .line 404460
    const-class v1, LX/2Oy;

    monitor-enter v1

    .line 404461
    :try_start_0
    sget-object v0, LX/2Oy;->f:LX/2Oy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 404462
    if-eqz v2, :cond_0

    .line 404463
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 404464
    new-instance v4, LX/2Oy;

    invoke-static {v0}, LX/1Hn;->a(LX/0QB;)LX/1Hn;

    move-result-object v3

    check-cast v3, LX/1Hn;

    const/16 p0, 0x2a16

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/2Oy;-><init>(LX/1Hn;LX/0Or;)V

    .line 404465
    move-object v0, v4

    .line 404466
    sput-object v0, LX/2Oy;->f:LX/2Oy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404467
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 404468
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 404469
    :cond_1
    sget-object v0, LX/2Oy;->f:LX/2Oy;

    return-object v0

    .line 404470
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 404471
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a([B)[B
    .locals 2

    .prologue
    .line 404452
    iget-object v1, p0, LX/2Oy;->c:LX/1Hn;

    iget-object v0, p0, LX/2Oy;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crypto/keychain/KeyChain;

    invoke-virtual {v1, v0}, LX/1Ho;->a(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;

    move-result-object v0

    .line 404453
    iget-object v1, p0, LX/2Oy;->e:LX/1Hb;

    invoke-virtual {v0, p1, v1}, LX/1Hz;->a([BLX/1Hb;)[B

    move-result-object v0

    return-object v0
.end method

.method public final a([B[B)[B
    .locals 2

    .prologue
    .line 404454
    new-instance v0, LX/DoV;

    invoke-direct {v0, p0, p1}, LX/DoV;-><init>(LX/2Oy;[B)V

    .line 404455
    iget-object v1, p0, LX/2Oy;->c:LX/1Hn;

    invoke-virtual {v1, v0}, LX/1Ho;->a(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;

    move-result-object v0

    .line 404456
    iget-object v1, p0, LX/2Oy;->e:LX/1Hb;

    invoke-virtual {v0, p2, v1}, LX/1Hz;->a([BLX/1Hb;)[B

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 404450
    iget-object v0, p0, LX/2Oy;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dok;

    invoke-virtual {v0}, LX/Dok;->a()[B

    .line 404451
    return-void
.end method

.method public final b([B)[B
    .locals 2

    .prologue
    .line 404448
    iget-object v1, p0, LX/2Oy;->c:LX/1Hn;

    iget-object v0, p0, LX/2Oy;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crypto/keychain/KeyChain;

    invoke-virtual {v1, v0}, LX/1Ho;->a(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;

    move-result-object v0

    .line 404449
    iget-object v1, p0, LX/2Oy;->e:LX/1Hb;

    invoke-virtual {v0, p1, v1}, LX/1Hz;->b([BLX/1Hb;)[B

    move-result-object v0

    return-object v0
.end method

.method public final b([B[B)[B
    .locals 2

    .prologue
    .line 404445
    new-instance v0, LX/DoV;

    invoke-direct {v0, p0, p1}, LX/DoV;-><init>(LX/2Oy;[B)V

    .line 404446
    iget-object v1, p0, LX/2Oy;->c:LX/1Hn;

    invoke-virtual {v1, v0}, LX/1Ho;->a(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;

    move-result-object v0

    .line 404447
    iget-object v1, p0, LX/2Oy;->e:LX/1Hb;

    invoke-virtual {v0, p2, v1}, LX/1Hz;->b([BLX/1Hb;)[B

    move-result-object v0

    return-object v0
.end method

.method public final c([B[B)Ljava/lang/String;
    .locals 3
    .param p1    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 404438
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 404439
    :cond_0
    const/4 v0, 0x0

    .line 404440
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, LX/2Oy;->b([B[B)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 404441
    :catch_0
    move-exception v0

    .line 404442
    :goto_1
    sget-object v1, LX/2Oy;->b:Ljava/lang/Class;

    const-string v2, "Failed to decrypt blob content"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 404443
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 404444
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method
