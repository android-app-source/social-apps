.class public final LX/34Y;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1S2;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/2tX;

.field public b:Landroid/content/Context;

.field public final synthetic c:LX/1S2;


# direct methods
.method public constructor <init>(LX/1S2;)V
    .locals 1

    .prologue
    .line 495291
    iput-object p1, p0, LX/34Y;->c:LX/1S2;

    .line 495292
    move-object v0, p1

    .line 495293
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 495294
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 495276
    const-string v0, "CrowdsourcingTofuComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 495277
    if-ne p0, p1, :cond_1

    .line 495278
    :cond_0
    :goto_0
    return v0

    .line 495279
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 495280
    goto :goto_0

    .line 495281
    :cond_3
    check-cast p1, LX/34Y;

    .line 495282
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 495283
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 495284
    if-eq v2, v3, :cond_0

    .line 495285
    iget-object v2, p0, LX/34Y;->a:LX/2tX;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/34Y;->a:LX/2tX;

    iget-object v3, p1, LX/34Y;->a:LX/2tX;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 495286
    goto :goto_0

    .line 495287
    :cond_5
    iget-object v2, p1, LX/34Y;->a:LX/2tX;

    if-nez v2, :cond_4

    .line 495288
    :cond_6
    iget-object v2, p0, LX/34Y;->b:Landroid/content/Context;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/34Y;->b:Landroid/content/Context;

    iget-object v3, p1, LX/34Y;->b:Landroid/content/Context;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 495289
    goto :goto_0

    .line 495290
    :cond_7
    iget-object v2, p1, LX/34Y;->b:Landroid/content/Context;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
