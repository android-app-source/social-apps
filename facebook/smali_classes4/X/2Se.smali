.class public LX/2Se;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public a:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final b:Landroid/content/res/Resources;

.field private final c:LX/2Sf;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EQ0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/2Sf;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/2Sf;",
            "LX/0Ot",
            "<",
            "LX/EQ0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 413051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413052
    iput-object p1, p0, LX/2Se;->b:Landroid/content/res/Resources;

    .line 413053
    iput-object p2, p0, LX/2Se;->c:LX/2Sf;

    .line 413054
    iput-object p3, p0, LX/2Se;->d:LX/0Ot;

    .line 413055
    return-void
.end method

.method public static a(LX/0QB;)LX/2Se;
    .locals 6

    .prologue
    .line 413038
    const-class v1, LX/2Se;

    monitor-enter v1

    .line 413039
    :try_start_0
    sget-object v0, LX/2Se;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 413040
    sput-object v2, LX/2Se;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 413041
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413042
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 413043
    new-instance v5, LX/2Se;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/2Sf;->a(LX/0QB;)LX/2Sf;

    move-result-object v4

    check-cast v4, LX/2Sf;

    const/16 p0, 0x34d3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/2Se;-><init>(Landroid/content/res/Resources;LX/2Sf;LX/0Ot;)V

    .line 413044
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    .line 413045
    iput-object v3, v5, LX/2Se;->a:LX/0ad;

    .line 413046
    move-object v0, v5

    .line 413047
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 413048
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2Se;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413049
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 413050
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;)Z
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 413032
    instance-of v1, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    if-nez v1, :cond_1

    .line 413033
    :cond_0
    :goto_0
    return v0

    .line 413034
    :cond_1
    check-cast p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    .line 413035
    invoke-virtual {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->z()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 413036
    iget-object v0, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v0, v0

    .line 413037
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Cwb;LX/Cw5;ZI)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Cwb;",
            "LX/Cw5;",
            "ZI)",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 412985
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 412986
    iget-object v1, p2, LX/Cw5;->a:LX/0Px;

    move-object v1, v1

    .line 412987
    invoke-virtual {v1, v3, p4}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 412988
    if-nez p3, :cond_1

    .line 412989
    iget-object v1, p2, LX/Cw5;->a:LX/0Px;

    move-object v1, v1

    .line 412990
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, p4, :cond_1

    .line 412991
    iget-object v1, p0, LX/2Se;->a:LX/0ad;

    sget-short v2, LX/100;->cf:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 412992
    new-instance v1, Lcom/facebook/search/model/GapTypeaheadUnit;

    sget-object v2, LX/Cw9;->DEFAULT:LX/Cw9;

    invoke-direct {v1, v2}, Lcom/facebook/search/model/GapTypeaheadUnit;-><init>(LX/Cw9;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 412993
    :cond_0
    new-instance v1, Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;

    invoke-direct {v1, p1}, Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;-><init>(LX/Cwb;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 412994
    :cond_1
    new-instance v1, LX/Cwa;

    invoke-direct {v1}, LX/Cwa;-><init>()V

    .line 412995
    iput-object p1, v1, LX/Cwa;->a:LX/Cwb;

    .line 412996
    move-object v1, v1

    .line 412997
    iget-object v2, p0, LX/2Se;->b:Landroid/content/res/Resources;

    const v3, 0x7f0820bd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 412998
    iput-object v2, v1, LX/Cwa;->c:Ljava/lang/String;

    .line 412999
    move-object v1, v1

    .line 413000
    iget-object v2, p0, LX/2Se;->b:Landroid/content/res/Resources;

    const v3, 0x7f08001f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 413001
    iput-object v2, v1, LX/Cwa;->d:Ljava/lang/String;

    .line 413002
    move-object v1, v1

    .line 413003
    const/4 v2, 0x1

    .line 413004
    iput-boolean v2, v1, LX/Cwa;->f:Z

    .line 413005
    move-object v1, v1

    .line 413006
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 413007
    iput-object v0, v1, LX/Cwa;->b:LX/0Px;

    .line 413008
    move-object v0, v1

    .line 413009
    invoke-virtual {v0}, LX/Cwa;->a()LX/Cwc;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/2Sf;->b(Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;Ljava/lang/String;LX/Cw5;)LX/0Px;
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;",
            "Ljava/lang/String;",
            "LX/Cw5;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v7, 0xf

    .line 413010
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 413011
    invoke-virtual {v3, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 413012
    if-eqz p4, :cond_0

    .line 413013
    iget-object v0, p4, LX/Cw5;->a:LX/0Px;

    move-object v0, v0

    .line 413014
    if-eqz v0, :cond_0

    .line 413015
    const/4 v1, 0x1

    .line 413016
    iget-object v0, p4, LX/Cw5;->a:LX/0Px;

    move-object v4, v0

    .line 413017
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 413018
    if-eq v1, v7, :cond_0

    .line 413019
    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-static {p3, v0}, LX/2Se;->a(Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 413020
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 413021
    add-int/lit8 v0, v1, 0x1

    .line 413022
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 413023
    :cond_0
    iget-object v0, p0, LX/2Se;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EQ0;

    .line 413024
    iget-object v1, v0, LX/EQ0;->g:LX/0Uh;

    const/16 v2, 0x409

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 413025
    iget-object v1, v0, LX/EQ0;->f:LX/0ti;

    new-instance v2, LX/EPy;

    invoke-static {p2}, LX/2Sb;->a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;

    move-result-object v4

    invoke-direct {v2, p1, v4, v7}, LX/EPy;-><init>(Ljava/lang/String;Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;I)V

    invoke-virtual {v1, v2}, LX/0ti;->a(LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 413026
    :goto_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 413027
    :cond_2
    iget-object v1, v0, LX/EQ0;->a:LX/EPx;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 413028
    new-instance v7, LX/EPw;

    invoke-static {v1}, LX/2Sb;->a(LX/0QB;)LX/2Sb;

    move-result-object v4

    check-cast v4, LX/2Sb;

    invoke-direct {v7, p1, p2, v2, v4}, LX/EPw;-><init>(Ljava/lang/String;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;Ljava/lang/Integer;LX/2Sb;)V

    .line 413029
    move-object v1, v7

    .line 413030
    iget-object v2, v0, LX/EQ0;->e:LX/0tc;

    invoke-virtual {v2, v1}, LX/0tc;->a(LX/3Bq;)LX/37X;

    move-result-object v1

    .line 413031
    iget-object v2, v0, LX/EQ0;->b:LX/0TD;

    new-instance v4, LX/EPz;

    invoke-direct {v4, v0, v1}, LX/EPz;-><init>(LX/EQ0;LX/37X;)V

    invoke-interface {v2, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_2
.end method
