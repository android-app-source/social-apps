.class public final enum LX/2uW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2uW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2uW;

.field public static final enum ACTIVITY_REPLY:LX/2uW;

.field public static final enum ADD_MEMBERS:LX/2uW;

.field public static final enum ADMIN:LX/2uW;

.field public static final enum BEFORE_FIRST_SENTINEL:LX/2uW;

.field public static final enum CALL_LOG:LX/2uW;

.field public static final enum COMMERCE_LINK:LX/2uW;

.field public static final enum COMMERCE_UNLINK:LX/2uW;

.field private static final DB_KEY_VALUE_TO_MESSAGE_TYPE:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "LX/2uW;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FAILED_SEND:LX/2uW;

.field public static final enum GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:LX/2uW;

.field public static final enum INCOMING_CALL:LX/2uW;

.field public static final enum MISSED_CALL:LX/2uW;

.field public static final enum MISSED_VIDEO_CALL:LX/2uW;

.field public static final enum OUTGOING_CALL:LX/2uW;

.field public static final enum P2P_PAYMENT:LX/2uW;

.field public static final enum P2P_PAYMENT_CANCELED:LX/2uW;

.field public static final enum P2P_PAYMENT_GROUP:LX/2uW;

.field public static final enum PENDING_SEND:LX/2uW;

.field public static final enum REGULAR:LX/2uW;

.field public static final enum REMOVED_IMAGE:LX/2uW;

.field public static final enum REMOVE_MEMBERS:LX/2uW;

.field public static final enum SET_IMAGE:LX/2uW;

.field public static final enum SET_NAME:LX/2uW;

.field public static final enum SMS_LOG:LX/2uW;

.field public static final enum TELEPHONE_CALL_LOG:LX/2uW;

.field public static final enum UNKNOWN:LX/2uW;

.field public static final enum VIDEO_CALL:LX/2uW;


# instance fields
.field public final dbKeyValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 475719
    new-instance v1, LX/2uW;

    const-string v2, "BEFORE_FIRST_SENTINEL"

    const/4 v3, -0x1

    invoke-direct {v1, v2, v0, v3}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->BEFORE_FIRST_SENTINEL:LX/2uW;

    .line 475720
    new-instance v1, LX/2uW;

    const-string v2, "REGULAR"

    invoke-direct {v1, v2, v5, v0}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->REGULAR:LX/2uW;

    .line 475721
    new-instance v1, LX/2uW;

    const-string v2, "ADD_MEMBERS"

    invoke-direct {v1, v2, v6, v5}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->ADD_MEMBERS:LX/2uW;

    .line 475722
    new-instance v1, LX/2uW;

    const-string v2, "REMOVE_MEMBERS"

    invoke-direct {v1, v2, v7, v6}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->REMOVE_MEMBERS:LX/2uW;

    .line 475723
    new-instance v1, LX/2uW;

    const-string v2, "SET_NAME"

    invoke-direct {v1, v2, v8, v7}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->SET_NAME:LX/2uW;

    .line 475724
    new-instance v1, LX/2uW;

    const-string v2, "SET_IMAGE"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3, v8}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->SET_IMAGE:LX/2uW;

    .line 475725
    new-instance v1, LX/2uW;

    const-string v2, "VIDEO_CALL"

    const/4 v3, 0x6

    const/4 v4, 0x5

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->VIDEO_CALL:LX/2uW;

    .line 475726
    new-instance v1, LX/2uW;

    const-string v2, "MISSED_VIDEO_CALL"

    const/4 v3, 0x7

    const/4 v4, 0x6

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->MISSED_VIDEO_CALL:LX/2uW;

    .line 475727
    new-instance v1, LX/2uW;

    const-string v2, "REMOVED_IMAGE"

    const/16 v3, 0x8

    const/4 v4, 0x7

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->REMOVED_IMAGE:LX/2uW;

    .line 475728
    new-instance v1, LX/2uW;

    const-string v2, "ADMIN"

    const/16 v3, 0x9

    const/16 v4, 0x8

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->ADMIN:LX/2uW;

    .line 475729
    new-instance v1, LX/2uW;

    const-string v2, "CALL_LOG"

    const/16 v3, 0xa

    const/16 v4, 0x9

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->CALL_LOG:LX/2uW;

    .line 475730
    new-instance v1, LX/2uW;

    const-string v2, "GLOBALLY_DELETED_MESSAGE_PLACEHOLDER"

    const/16 v3, 0xb

    const/16 v4, 0xa

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:LX/2uW;

    .line 475731
    new-instance v1, LX/2uW;

    const-string v2, "P2P_PAYMENT"

    const/16 v3, 0xc

    const/16 v4, 0x32

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->P2P_PAYMENT:LX/2uW;

    .line 475732
    new-instance v1, LX/2uW;

    const-string v2, "P2P_PAYMENT_CANCELED"

    const/16 v3, 0xd

    const/16 v4, 0x33

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->P2P_PAYMENT_CANCELED:LX/2uW;

    .line 475733
    new-instance v1, LX/2uW;

    const-string v2, "P2P_PAYMENT_GROUP"

    const/16 v3, 0xe

    const/16 v4, 0x34

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->P2P_PAYMENT_GROUP:LX/2uW;

    .line 475734
    new-instance v1, LX/2uW;

    const-string v2, "INCOMING_CALL"

    const/16 v3, 0xf

    const/16 v4, 0x64

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->INCOMING_CALL:LX/2uW;

    .line 475735
    new-instance v1, LX/2uW;

    const-string v2, "MISSED_CALL"

    const/16 v3, 0x10

    const/16 v4, 0x65

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->MISSED_CALL:LX/2uW;

    .line 475736
    new-instance v1, LX/2uW;

    const-string v2, "OUTGOING_CALL"

    const/16 v3, 0x11

    const/16 v4, 0x66

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->OUTGOING_CALL:LX/2uW;

    .line 475737
    new-instance v1, LX/2uW;

    const-string v2, "COMMERCE_LINK"

    const/16 v3, 0x12

    const/16 v4, 0x96

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->COMMERCE_LINK:LX/2uW;

    .line 475738
    new-instance v1, LX/2uW;

    const-string v2, "COMMERCE_UNLINK"

    const/16 v3, 0x13

    const/16 v4, 0x97

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->COMMERCE_UNLINK:LX/2uW;

    .line 475739
    new-instance v1, LX/2uW;

    const-string v2, "ACTIVITY_REPLY"

    const/16 v3, 0x14

    const/16 v4, 0x98

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->ACTIVITY_REPLY:LX/2uW;

    .line 475740
    new-instance v1, LX/2uW;

    const-string v2, "TELEPHONE_CALL_LOG"

    const/16 v3, 0x15

    const/16 v4, 0xc8

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->TELEPHONE_CALL_LOG:LX/2uW;

    .line 475741
    new-instance v1, LX/2uW;

    const-string v2, "SMS_LOG"

    const/16 v3, 0x16

    const/16 v4, 0xc9

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->SMS_LOG:LX/2uW;

    .line 475742
    new-instance v1, LX/2uW;

    const-string v2, "PENDING_SEND"

    const/16 v3, 0x17

    const/16 v4, 0x384

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->PENDING_SEND:LX/2uW;

    .line 475743
    new-instance v1, LX/2uW;

    const-string v2, "FAILED_SEND"

    const/16 v3, 0x18

    const/16 v4, 0x385

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->FAILED_SEND:LX/2uW;

    .line 475744
    new-instance v1, LX/2uW;

    const-string v2, "UNKNOWN"

    const/16 v3, 0x19

    const/16 v4, 0x3e8

    invoke-direct {v1, v2, v3, v4}, LX/2uW;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2uW;->UNKNOWN:LX/2uW;

    .line 475745
    const/16 v1, 0x1a

    new-array v1, v1, [LX/2uW;

    sget-object v2, LX/2uW;->BEFORE_FIRST_SENTINEL:LX/2uW;

    aput-object v2, v1, v0

    sget-object v2, LX/2uW;->REGULAR:LX/2uW;

    aput-object v2, v1, v5

    sget-object v2, LX/2uW;->ADD_MEMBERS:LX/2uW;

    aput-object v2, v1, v6

    sget-object v2, LX/2uW;->REMOVE_MEMBERS:LX/2uW;

    aput-object v2, v1, v7

    sget-object v2, LX/2uW;->SET_NAME:LX/2uW;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, LX/2uW;->SET_IMAGE:LX/2uW;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, LX/2uW;->VIDEO_CALL:LX/2uW;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, LX/2uW;->MISSED_VIDEO_CALL:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, LX/2uW;->REMOVED_IMAGE:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, LX/2uW;->ADMIN:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, LX/2uW;->CALL_LOG:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, LX/2uW;->GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    sget-object v3, LX/2uW;->P2P_PAYMENT:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    sget-object v3, LX/2uW;->P2P_PAYMENT_CANCELED:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    sget-object v3, LX/2uW;->P2P_PAYMENT_GROUP:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    sget-object v3, LX/2uW;->INCOMING_CALL:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    sget-object v3, LX/2uW;->MISSED_CALL:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    sget-object v3, LX/2uW;->OUTGOING_CALL:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0x12

    sget-object v3, LX/2uW;->COMMERCE_LINK:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0x13

    sget-object v3, LX/2uW;->COMMERCE_UNLINK:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    sget-object v3, LX/2uW;->ACTIVITY_REPLY:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0x15

    sget-object v3, LX/2uW;->TELEPHONE_CALL_LOG:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0x16

    sget-object v3, LX/2uW;->SMS_LOG:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0x17

    sget-object v3, LX/2uW;->PENDING_SEND:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0x18

    sget-object v3, LX/2uW;->FAILED_SEND:LX/2uW;

    aput-object v3, v1, v2

    const/16 v2, 0x19

    sget-object v3, LX/2uW;->UNKNOWN:LX/2uW;

    aput-object v3, v1, v2

    sput-object v1, LX/2uW;->$VALUES:[LX/2uW;

    .line 475746
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 475747
    invoke-static {}, LX/2uW;->values()[LX/2uW;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 475748
    iget v5, v4, LX/2uW;->dbKeyValue:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 475749
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 475750
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/2uW;->DB_KEY_VALUE_TO_MESSAGE_TYPE:LX/0P1;

    .line 475751
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 475752
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 475753
    iput p3, p0, LX/2uW;->dbKeyValue:I

    .line 475754
    return-void
.end method

.method public static fromDbKeyValue(I)LX/2uW;
    .locals 2

    .prologue
    .line 475755
    sget-object v0, LX/2uW;->DB_KEY_VALUE_TO_MESSAGE_TYPE:LX/0P1;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2uW;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/2uW;
    .locals 1

    .prologue
    .line 475756
    const-class v0, LX/2uW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2uW;

    return-object v0
.end method

.method public static values()[LX/2uW;
    .locals 1

    .prologue
    .line 475757
    sget-object v0, LX/2uW;->$VALUES:[LX/2uW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2uW;

    return-object v0
.end method
