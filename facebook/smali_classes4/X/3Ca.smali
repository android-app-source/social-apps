.class public final enum LX/3Ca;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3Ca;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3Ca;

.field public static final enum DELETE:LX/3Ca;

.field public static final enum INSERT:LX/3Ca;

.field public static final enum REPLACE:LX/3Ca;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 530250
    new-instance v0, LX/3Ca;

    const-string v1, "INSERT"

    invoke-direct {v0, v1, v2}, LX/3Ca;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Ca;->INSERT:LX/3Ca;

    .line 530251
    new-instance v0, LX/3Ca;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v3}, LX/3Ca;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Ca;->DELETE:LX/3Ca;

    .line 530252
    new-instance v0, LX/3Ca;

    const-string v1, "REPLACE"

    invoke-direct {v0, v1, v4}, LX/3Ca;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Ca;->REPLACE:LX/3Ca;

    .line 530253
    const/4 v0, 0x3

    new-array v0, v0, [LX/3Ca;

    sget-object v1, LX/3Ca;->INSERT:LX/3Ca;

    aput-object v1, v0, v2

    sget-object v1, LX/3Ca;->DELETE:LX/3Ca;

    aput-object v1, v0, v3

    sget-object v1, LX/3Ca;->REPLACE:LX/3Ca;

    aput-object v1, v0, v4

    sput-object v0, LX/3Ca;->$VALUES:[LX/3Ca;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 530254
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3Ca;
    .locals 1

    .prologue
    .line 530255
    const-class v0, LX/3Ca;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3Ca;

    return-object v0
.end method

.method public static values()[LX/3Ca;
    .locals 1

    .prologue
    .line 530256
    sget-object v0, LX/3Ca;->$VALUES:[LX/3Ca;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3Ca;

    return-object v0
.end method
