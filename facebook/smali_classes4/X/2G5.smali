.class public LX/2G5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0Wt;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Wt;)V
    .locals 0

    .prologue
    .line 387797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387798
    iput-object p1, p0, LX/2G5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 387799
    iput-object p2, p0, LX/2G5;->b:LX/0Wt;

    .line 387800
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/0P1;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 387801
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v5

    .line 387802
    iget-object v0, p0, LX/2G5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    .line 387803
    iget-object v0, p0, LX/2G5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0Wt;->a:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v3, v2

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 387804
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 387805
    sget-object v4, LX/0Wt;->a:LX/0Tn;

    invoke-virtual {v1, v4}, LX/0To;->a(LX/0To;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v4

    sget-object v8, LX/0Wt;->b:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 387806
    if-eqz v4, :cond_2

    .line 387807
    invoke-static {v1}, LX/0Wt;->a(LX/0Tn;)[Ljava/lang/String;

    move-result-object v8

    .line 387808
    array-length v4, v8

    const/4 v9, 0x2

    if-le v4, v9, :cond_2

    .line 387809
    const/4 v4, 0x0

    aget-object v4, v8, v4

    .line 387810
    const/4 v9, 0x1

    aget-object v8, v8, v9

    .line 387811
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 387812
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 387813
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 387814
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    move-object v2, v4

    .line 387815
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v8, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 387816
    invoke-interface {v6, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    :cond_2
    move-object v0, v2

    move-object v1, v3

    move-object v2, v0

    move-object v3, v1

    .line 387817
    goto :goto_0

    .line 387818
    :cond_3
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    .line 387819
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    invoke-virtual {v5, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 387820
    :cond_4
    invoke-interface {v6}, LX/0hN;->commit()V

    .line 387821
    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 387822
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    const/4 v4, 0x0

    goto :goto_1
.end method
