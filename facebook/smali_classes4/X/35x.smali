.class public final LX/35x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1ex;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1ex",
        "<",
        "Ljava/util/List",
        "<",
        "LX/3BC;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1cd;

.field public final synthetic b:LX/1cW;

.field public final synthetic c:LX/1ny;

.field public final synthetic d:LX/1bf;

.field public final synthetic e:LX/1o9;

.field public final synthetic f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic g:LX/1cJ;


# direct methods
.method public constructor <init>(LX/1cJ;LX/1cd;LX/1cW;LX/1ny;LX/1bf;LX/1o9;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    .prologue
    .line 497768
    iput-object p1, p0, LX/35x;->g:LX/1cJ;

    iput-object p2, p0, LX/35x;->a:LX/1cd;

    iput-object p3, p0, LX/35x;->b:LX/1cW;

    iput-object p4, p0, LX/35x;->c:LX/1ny;

    iput-object p5, p0, LX/35x;->d:LX/1bf;

    iput-object p6, p0, LX/35x;->e:LX/1o9;

    iput-object p7, p0, LX/35x;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1eg;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1eg",
            "<",
            "Ljava/util/List",
            "<",
            "LX/3BC;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 497769
    invoke-virtual {p1}, LX/1eg;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/1eg;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 497770
    :cond_0
    :goto_0
    return-object p1

    .line 497771
    :cond_1
    :try_start_0
    invoke-virtual {p1}, LX/1eg;->d()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, LX/1eg;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 497772
    :cond_2
    iget-object v0, p0, LX/35x;->g:LX/1cJ;

    iget-object v1, p0, LX/35x;->a:LX/1cd;

    iget-object v2, p0, LX/35x;->b:LX/1cW;

    invoke-static {v0, v1, v2}, LX/1cJ;->b(LX/1cJ;LX/1cd;LX/1cW;)V

    move-object p1, v8

    .line 497773
    goto :goto_0

    .line 497774
    :cond_3
    iget-object v0, p0, LX/35x;->g:LX/1cJ;

    iget-object v1, p0, LX/35x;->a:LX/1cd;

    iget-object v2, p0, LX/35x;->b:LX/1cW;

    iget-object v3, p0, LX/35x;->c:LX/1ny;

    invoke-virtual {p1}, LX/1eg;->d()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    iget-object v5, p0, LX/35x;->d:LX/1bf;

    iget-object v6, p0, LX/35x;->e:LX/1o9;

    iget-object v7, p0, LX/35x;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 497775
    invoke-static/range {v0 .. v7}, LX/1cJ;->a$redex0(LX/1cJ;LX/1cd;LX/1cW;LX/1ny;Ljava/util/List;LX/1bf;LX/1o9;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/1eg;

    move-result-object v9

    move-object p1, v9
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 497776
    goto :goto_0

    .line 497777
    :catch_0
    move-object p1, v8

    goto :goto_0
.end method
