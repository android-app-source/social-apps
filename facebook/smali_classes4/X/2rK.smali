.class public final LX/2rK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/videohome/data/VideoHomeItem;

.field public final synthetic b:LX/ESr;


# direct methods
.method public constructor <init>(LX/ESr;Lcom/facebook/video/videohome/data/VideoHomeItem;)V
    .locals 0

    .prologue
    .line 471823
    iput-object p1, p0, LX/2rK;->b:LX/ESr;

    iput-object p2, p0, LX/2rK;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 471824
    sget-object v0, LX/ESr;->a:Ljava/lang/String;

    const-string v1, "Video Home creator live status query failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 471825
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 471826
    check-cast p1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;

    .line 471827
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->j()Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    move-result-object v0

    if-nez v0, :cond_1

    .line 471828
    :cond_0
    sget-object v0, LX/ESr;->a:Ljava/lang/String;

    const-string v1, "Video Home creator live status query succeeded but result was null."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 471829
    :goto_0
    return-void

    .line 471830
    :cond_1
    iget-object v0, p0, LX/2rK;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-static {v0}, LX/ESr;->f(Lcom/facebook/video/videohome/data/VideoHomeItem;)Ljava/lang/String;

    move-result-object v0

    .line 471831
    invoke-virtual {p1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->a()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 471832
    iget-object v1, p0, LX/2rK;->b:LX/ESr;

    iget-object v1, v1, LX/ESr;->h:LX/03V;

    sget-object v2, LX/ESr;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Inconsistency between item id = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " and subscription creator id = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->a()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0

    .line 471833
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->j()Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/ESz;->fromString(Ljava/lang/String;)LX/ESz;

    move-result-object v0

    .line 471834
    invoke-virtual {p1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->j()Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    .line 471835
    iget-object v1, p0, LX/2rK;->b:LX/ESr;

    iget-object v2, p0, LX/2rK;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-static {v1, v0, v2}, LX/ESr;->a$redex0(LX/ESr;LX/ESz;Lcom/facebook/video/videohome/data/VideoHomeItem;)V

    goto :goto_0
.end method
