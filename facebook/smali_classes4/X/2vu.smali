.class public interface abstract LX/2vu;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract a(LX/2wX;Landroid/app/PendingIntent;)LX/2wg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Landroid/app/PendingIntent;",
            ")",
            "LX/2wg",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(LX/2wX;Lcom/google/android/gms/location/LocationRequest;LX/2vi;)LX/2wg;
    .annotation build Landroid/support/annotation/RequiresPermission;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Lcom/google/android/gms/location/LocationRequest;",
            "LX/2vi;",
            ")",
            "LX/2wg",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(LX/2wX;Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)LX/2wg;
    .annotation build Landroid/support/annotation/RequiresPermission;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Lcom/google/android/gms/location/LocationRequest;",
            "Landroid/app/PendingIntent;",
            ")",
            "LX/2wg",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(LX/2wX;)Landroid/location/Location;
    .annotation build Landroid/support/annotation/RequiresPermission;
    .end annotation
.end method
