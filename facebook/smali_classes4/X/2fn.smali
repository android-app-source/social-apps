.class public LX/2fn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLNode;

.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field

.field public final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Z)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 447431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447432
    iput-object p1, p0, LX/2fn;->a:Lcom/facebook/graphql/model/GraphQLNode;

    .line 447433
    iput-object p2, p0, LX/2fn;->b:Ljava/lang/String;

    .line 447434
    iput-object p3, p0, LX/2fn;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447435
    iput-object p4, p0, LX/2fn;->d:Landroid/view/View$OnClickListener;

    .line 447436
    iput-boolean p5, p0, LX/2fn;->e:Z

    .line 447437
    return-void
.end method
