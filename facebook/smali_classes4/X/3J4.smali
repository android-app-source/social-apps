.class public final enum LX/3J4;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3J4;",
        ">;",
        "LX/0QK",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3J4;

.field public static final enum INSTANCE:LX/3J4;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 547458
    new-instance v0, LX/3J4;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, LX/3J4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3J4;->INSTANCE:LX/3J4;

    .line 547459
    const/4 v0, 0x1

    new-array v0, v0, [LX/3J4;

    sget-object v1, LX/3J4;->INSTANCE:LX/3J4;

    aput-object v1, v0, v2

    sput-object v0, LX/3J4;->$VALUES:[LX/3J4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 547460
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3J4;
    .locals 1

    .prologue
    .line 547461
    const-class v0, LX/3J4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3J4;

    return-object v0
.end method

.method public static values()[LX/3J4;
    .locals 1

    .prologue
    .line 547462
    sget-object v0, LX/3J4;->$VALUES:[LX/3J4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3J4;

    return-object v0
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 547463
    return-object p1
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 547464
    const-string v0, "Functions.identity()"

    return-object v0
.end method
