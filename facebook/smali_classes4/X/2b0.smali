.class public final LX/2b0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 427966
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 427967
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 427968
    :goto_0
    return v1

    .line 427969
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 427970
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 427971
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 427972
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 427973
    const-string v12, "eligible_for_audience_alignment_education"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 427974
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    move v10, v5

    move v5, v2

    goto :goto_1

    .line 427975
    :cond_1
    const-string v12, "eligible_for_audience_alignment_only_me_education"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 427976
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v9, v4

    move v4, v2

    goto :goto_1

    .line 427977
    :cond_2
    const-string v12, "eligible_for_newcomer_audience_selector"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 427978
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v8, v3

    move v3, v2

    goto :goto_1

    .line 427979
    :cond_3
    const-string v12, "has_default_privacy"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 427980
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v7, v0

    move v0, v2

    goto :goto_1

    .line 427981
    :cond_4
    const-string v12, "post_privacy_followup_info"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 427982
    invoke-static {p0, p1}, LX/3lQ;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 427983
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 427984
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 427985
    if-eqz v5, :cond_7

    .line 427986
    invoke-virtual {p1, v1, v10}, LX/186;->a(IZ)V

    .line 427987
    :cond_7
    if-eqz v4, :cond_8

    .line 427988
    invoke-virtual {p1, v2, v9}, LX/186;->a(IZ)V

    .line 427989
    :cond_8
    if-eqz v3, :cond_9

    .line 427990
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->a(IZ)V

    .line 427991
    :cond_9
    if-eqz v0, :cond_a

    .line 427992
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 427993
    :cond_a
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 427994
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 427995
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 427996
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 427997
    if-eqz v0, :cond_0

    .line 427998
    const-string v1, "eligible_for_audience_alignment_education"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 427999
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428000
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428001
    if-eqz v0, :cond_1

    .line 428002
    const-string v1, "eligible_for_audience_alignment_only_me_education"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428003
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428004
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428005
    if-eqz v0, :cond_2

    .line 428006
    const-string v1, "eligible_for_newcomer_audience_selector"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428007
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428008
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428009
    if-eqz v0, :cond_3

    .line 428010
    const-string v1, "has_default_privacy"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428011
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428012
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 428013
    if-eqz v0, :cond_4

    .line 428014
    const-string v1, "post_privacy_followup_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428015
    invoke-static {p0, v0, p2, p3}, LX/3lQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 428016
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 428017
    return-void
.end method
