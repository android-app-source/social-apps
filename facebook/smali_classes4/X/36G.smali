.class public LX/36G;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498537
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 498538
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1Wi;)Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/1wK;",
            ">(",
            "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/Void;",
            "LX/1Pe;",
            "TV;>;",
            "LX/1Wi;",
            ")",
            "Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 498539
    new-instance v1, Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    invoke-direct {v1, v0, p1, p2}, Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1Wi;)V

    .line 498540
    return-object v1
.end method
