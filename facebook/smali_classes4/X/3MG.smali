.class public LX/3MG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static volatile g:LX/3MG;


# instance fields
.field private final c:Landroid/content/ContentResolver;

.field private final d:Landroid/content/Context;

.field public final e:LX/3Lx;

.field private final f:LX/1Ml;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 554009
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "normalized_number"

    aput-object v1, v0, v2

    const-string v1, "type"

    aput-object v1, v0, v3

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "display_name"

    aput-object v1, v0, v5

    const-string v1, "photo_thumb_uri"

    aput-object v1, v0, v6

    sput-object v0, LX/3MG;->a:[Ljava/lang/String;

    .line 554010
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "data1"

    aput-object v1, v0, v2

    const-string v1, "data2"

    aput-object v1, v0, v3

    const-string v1, "contact_id"

    aput-object v1, v0, v4

    const-string v1, "display_name"

    aput-object v1, v0, v5

    const-string v1, "photo_thumb_uri"

    aput-object v1, v0, v6

    sput-object v0, LX/3MG;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Landroid/content/Context;LX/3Lx;LX/1Ml;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 554019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 554020
    iput-object p1, p0, LX/3MG;->c:Landroid/content/ContentResolver;

    .line 554021
    iput-object p2, p0, LX/3MG;->d:Landroid/content/Context;

    .line 554022
    iput-object p3, p0, LX/3MG;->e:LX/3Lx;

    .line 554023
    iput-object p4, p0, LX/3MG;->f:LX/1Ml;

    .line 554024
    return-void
.end method

.method public static a(LX/0QB;)LX/3MG;
    .locals 7

    .prologue
    .line 553933
    sget-object v0, LX/3MG;->g:LX/3MG;

    if-nez v0, :cond_1

    .line 553934
    const-class v1, LX/3MG;

    monitor-enter v1

    .line 553935
    :try_start_0
    sget-object v0, LX/3MG;->g:LX/3MG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 553936
    if-eqz v2, :cond_0

    .line 553937
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 553938
    new-instance p0, LX/3MG;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/3Lx;->b(LX/0QB;)LX/3Lx;

    move-result-object v5

    check-cast v5, LX/3Lx;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v6

    check-cast v6, LX/1Ml;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3MG;-><init>(Landroid/content/ContentResolver;Landroid/content/Context;LX/3Lx;LX/1Ml;)V

    .line 553939
    move-object v0, p0

    .line 553940
    sput-object v0, LX/3MG;->g:LX/3MG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 553941
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 553942
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 553943
    :cond_1
    sget-object v0, LX/3MG;->g:LX/3MG;

    return-object v0

    .line 553944
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 553945
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/user/model/User;
    .locals 3

    .prologue
    .line 554011
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 554012
    new-instance v1, Lcom/facebook/user/model/UserEmailAddress;

    const/4 v2, 0x4

    invoke-direct {v1, p0, v2}, Lcom/facebook/user/model/UserEmailAddress;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 554013
    new-instance v1, LX/0XI;

    invoke-direct {v1}, LX/0XI;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p0}, LX/0XI;->b(Ljava/lang/String;Ljava/lang/String;)LX/0XI;

    move-result-object v1

    .line 554014
    iput-object p1, v1, LX/0XI;->h:Ljava/lang/String;

    .line 554015
    move-object v1, v1

    .line 554016
    iput-object v0, v1, LX/0XI;->c:Ljava/util/List;

    .line 554017
    move-object v0, v1

    .line 554018
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/lang/String;)Lcom/facebook/user/model/User;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 553971
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, LX/3MG;->f:LX/1Ml;

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 553972
    sget-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 553973
    :try_start_0
    iget-object v0, p0, LX/3MG;->c:Landroid/content/ContentResolver;

    sget-object v2, LX/3MG;->b:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 553974
    if-nez v1, :cond_1

    .line 553975
    if-eqz v1, :cond_0

    .line 553976
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 553977
    :cond_0
    :goto_0
    return-object v6

    .line 553978
    :cond_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 553979
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 553980
    new-instance v8, Lcom/facebook/user/model/UserEmailAddress;

    const-string v9, "data1"

    invoke-static {v1, v9}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "data2"

    invoke-static {v1, v10}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v10

    invoke-direct {v8, v9, v10}, Lcom/facebook/user/model/UserEmailAddress;-><init>(Ljava/lang/String;I)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 553981
    new-instance v8, LX/0XI;

    invoke-direct {v8}, LX/0XI;-><init>()V

    const-string v9, "contact_id"

    invoke-static {v1, v9}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    const-string v10, "data1"

    invoke-static {v1, v10}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0XI;->b(Ljava/lang/String;Ljava/lang/String;)LX/0XI;

    move-result-object v8

    .line 553982
    iput-object v7, v8, LX/0XI;->c:Ljava/util/List;

    .line 553983
    move-object v7, v8

    .line 553984
    const-string v8, "display_name"

    invoke-static {v1, v8}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 553985
    iput-object v8, v7, LX/0XI;->h:Ljava/lang/String;

    .line 553986
    move-object v7, v7

    .line 553987
    const-string v8, "contact_id"

    invoke-static {v1, v8}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 553988
    iput-object v8, v7, LX/0XI;->n:Ljava/lang/String;

    .line 553989
    move-object v7, v7

    .line 553990
    invoke-virtual {v7}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v7

    move-object v0, v7
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 553991
    :goto_1
    if-eqz v1, :cond_2

    .line 553992
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_2
    move-object v6, v0

    .line 553993
    goto :goto_0

    .line 553994
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 553995
    :goto_3
    :try_start_2
    const-string v2, "SmsUserUtil"

    const-string v3, "Failed to get user by email address %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 553996
    if-eqz v1, :cond_4

    .line 553997
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_2

    .line 553998
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_4
    if-eqz v1, :cond_3

    .line 553999
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 554000
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 554001
    :catch_1
    move-exception v0

    goto :goto_3

    :cond_4
    move-object v0, v6

    goto :goto_2

    :cond_5
    move-object v0, v6

    goto :goto_1
.end method

.method private e(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 554002
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 554003
    invoke-static {p1}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 554004
    sget-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 554005
    iget-object v0, p0, LX/3MG;->c:Landroid/content/ContentResolver;

    sget-object v2, LX/3MG;->b:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 554006
    :cond_0
    :goto_0
    return-object v3

    .line 554007
    :cond_1
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 554008
    iget-object v0, p0, LX/3MG;->c:Landroid/content/ContentResolver;

    sget-object v2, LX/3MG;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_0
.end method

.method public static g(LX/3MG;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 553970
    iget-object v0, p0, LX/3MG;->e:LX/3Lx;

    invoke-virtual {v0, p1}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/user/model/User;
    .locals 5

    .prologue
    .line 553946
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 553947
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid message address"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 553948
    :cond_0
    const-string v0, "#CMAS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 553949
    iget-object v0, p0, LX/3MG;->d:Landroid/content/Context;

    const v1, 0x7f081af9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/3MG;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 553950
    :cond_1
    :goto_0
    return-object v0

    .line 553951
    :cond_2
    invoke-static {p1}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 553952
    invoke-virtual {p0, p1}, LX/3MG;->b(Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 553953
    if-nez v0, :cond_1

    .line 553954
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 553955
    iget-object v1, p0, LX/3MG;->e:LX/3Lx;

    invoke-virtual {v1, p1}, LX/3Lx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 553956
    new-instance v2, Lcom/facebook/user/model/UserPhoneNumber;

    const/4 v3, 0x2

    invoke-direct {v2, v1, p1, v3}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 553957
    new-instance v2, LX/0XI;

    invoke-direct {v2}, LX/0XI;-><init>()V

    const/4 v3, 0x0

    invoke-static {p0, p1}, LX/3MG;->g(LX/3MG;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0XI;->a(Ljava/lang/String;Ljava/lang/String;)LX/0XI;

    move-result-object v2

    .line 553958
    iput-object v1, v2, LX/0XI;->h:Ljava/lang/String;

    .line 553959
    move-object v1, v2

    .line 553960
    iput-object v0, v1, LX/0XI;->d:Ljava/util/List;

    .line 553961
    move-object v0, v1

    .line 553962
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    move-object v0, v0

    .line 553963
    goto :goto_0

    .line 553964
    :cond_3
    invoke-static {p1}, LX/2UG;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 553965
    if-eqz v1, :cond_4

    .line 553966
    invoke-direct {p0, v1}, LX/3MG;->d(Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 553967
    if-nez v0, :cond_1

    .line 553968
    invoke-static {v1, v1}, LX/3MG;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    goto :goto_0

    .line 553969
    :cond_4
    invoke-static {p1, p1}, LX/3MG;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/user/model/User;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 553896
    invoke-static {p1}, LX/3Lz;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 553897
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/3MG;->f:LX/1Ml;

    const-string v3, "android.permission.READ_CONTACTS"

    invoke-virtual {v2, v3}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 553898
    :try_start_0
    invoke-direct {p0, v1}, LX/3MG;->e(Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 553899
    if-nez v2, :cond_1

    .line 553900
    if-eqz v2, :cond_0

    .line 553901
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 553902
    :cond_0
    :goto_0
    return-object v0

    .line 553903
    :cond_1
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 553904
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 553905
    const-string v7, "normalized_number"

    invoke-static {v2, v7}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 553906
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 553907
    invoke-static {p0, p1}, LX/3MG;->g(LX/3MG;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 553908
    :cond_2
    new-instance v9, Lcom/facebook/user/model/UserPhoneNumber;

    iget-object v10, p0, LX/3MG;->e:LX/3Lx;

    invoke-virtual {v10, p1}, LX/3Lx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "type"

    invoke-static {v2, v11}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v11

    invoke-direct {v9, v10, p1, v7, v11}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 553909
    const-string v7, "_id"

    invoke-static {v2, v7}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v9

    .line 553910
    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    .line 553911
    new-instance v10, LX/0XI;

    invoke-direct {v10}, LX/0XI;-><init>()V

    const/4 v7, 0x0

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/user/model/UserPhoneNumber;

    .line 553912
    iget-object v11, v7, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    move-object v7, v11

    .line 553913
    invoke-virtual {v10, v9, v7}, LX/0XI;->a(Ljava/lang/String;Ljava/lang/String;)LX/0XI;

    move-result-object v7

    .line 553914
    iput-object v8, v7, LX/0XI;->d:Ljava/util/List;

    .line 553915
    move-object v7, v7

    .line 553916
    const-string v8, "display_name"

    invoke-static {v2, v8}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 553917
    iput-object v8, v7, LX/0XI;->h:Ljava/lang/String;

    .line 553918
    move-object v7, v7

    .line 553919
    const-string v8, "photo_thumb_uri"

    invoke-static {v2, v8}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 553920
    iput-object v8, v7, LX/0XI;->n:Ljava/lang/String;

    .line 553921
    move-object v7, v7

    .line 553922
    invoke-virtual {v7}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v7

    move-object v0, v7
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 553923
    :cond_3
    if-eqz v2, :cond_0

    .line 553924
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 553925
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 553926
    :goto_1
    :try_start_2
    const-string v3, "SmsUserUtil"

    const-string v4, "Failed to get user by phone number %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v3, v1, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 553927
    if-eqz v2, :cond_0

    .line 553928
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 553929
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    if-eqz v2, :cond_4

    .line 553930
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 553931
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 553932
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 553866
    invoke-static {p1}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result v2

    .line 553867
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 553868
    if-eqz v2, :cond_1

    move-object v0, p1

    .line 553869
    :goto_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 553870
    const/4 v1, 0x0

    .line 553871
    :try_start_0
    invoke-direct {p0, v0}, LX/3MG;->e(Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 553872
    if-eqz v0, :cond_3

    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 553873
    if-eqz v2, :cond_2

    .line 553874
    const-string v1, "display_name"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object p1

    .line 553875
    if-eqz v0, :cond_0

    .line 553876
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 553877
    :cond_0
    :goto_1
    return-object p1

    .line 553878
    :cond_1
    invoke-static {p1}, LX/3Lz;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 553879
    :cond_2
    :try_start_2
    const-string v1, "display_name"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object p1

    .line 553880
    if-eqz v0, :cond_0

    .line 553881
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 553882
    :cond_3
    if-eqz v2, :cond_4

    .line 553883
    if-eqz v0, :cond_0

    .line 553884
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 553885
    :cond_4
    :try_start_3
    iget-object v1, p0, LX/3MG;->e:LX/3Lx;

    invoke-virtual {v1, p1}, LX/3Lx;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object p1

    .line 553886
    if-eqz v0, :cond_0

    .line 553887
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 553888
    :catch_0
    move-object v0, v1

    .line 553889
    :goto_2
    if-eqz v0, :cond_0

    .line 553890
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 553891
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_5

    .line 553892
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 553893
    :cond_6
    const-string p1, ""

    goto :goto_1

    .line 553894
    :catchall_1
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_3

    .line 553895
    :catch_1
    goto :goto_2
.end method
