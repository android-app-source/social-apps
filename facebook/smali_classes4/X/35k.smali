.class public LX/35k;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/Ap5;

.field public final b:LX/ApL;

.field public final c:LX/C0D;

.field public final d:LX/1qb;

.field private final e:LX/1qa;

.field public final f:LX/2sO;

.field public final g:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/Ap5;LX/ApL;LX/C0D;LX/1qb;LX/1qa;LX/2sO;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 497567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497568
    iput-object p1, p0, LX/35k;->a:LX/Ap5;

    .line 497569
    iput-object p2, p0, LX/35k;->b:LX/ApL;

    .line 497570
    iput-object p3, p0, LX/35k;->c:LX/C0D;

    .line 497571
    iput-object p4, p0, LX/35k;->d:LX/1qb;

    .line 497572
    iput-object p5, p0, LX/35k;->e:LX/1qa;

    .line 497573
    iput-object p6, p0, LX/35k;->f:LX/2sO;

    .line 497574
    iput-object p7, p0, LX/35k;->g:Landroid/content/res/Resources;

    .line 497575
    return-void
.end method

.method public static a(LX/0QB;)LX/35k;
    .locals 11

    .prologue
    .line 497576
    const-class v1, LX/35k;

    monitor-enter v1

    .line 497577
    :try_start_0
    sget-object v0, LX/35k;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 497578
    sput-object v2, LX/35k;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 497579
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497580
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 497581
    new-instance v3, LX/35k;

    invoke-static {v0}, LX/Ap5;->a(LX/0QB;)LX/Ap5;

    move-result-object v4

    check-cast v4, LX/Ap5;

    invoke-static {v0}, LX/ApL;->a(LX/0QB;)LX/ApL;

    move-result-object v5

    check-cast v5, LX/ApL;

    invoke-static {v0}, LX/C0D;->b(LX/0QB;)LX/C0D;

    move-result-object v6

    check-cast v6, LX/C0D;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v7

    check-cast v7, LX/1qb;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v8

    check-cast v8, LX/1qa;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v9

    check-cast v9, LX/2sO;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v10

    check-cast v10, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v10}, LX/35k;-><init>(LX/Ap5;LX/ApL;LX/C0D;LX/1qb;LX/1qa;LX/2sO;Landroid/content/res/Resources;)V

    .line 497582
    move-object v0, v3

    .line 497583
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 497584
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/35k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 497585
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 497586
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1V4;)Z
    .locals 1

    .prologue
    .line 497587
    invoke-virtual {p0}, LX/1V4;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497588
    const/4 v0, 0x1

    move v0, v0

    .line 497589
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2sO;LX/1V4;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/2sO;",
            "LX/1V4;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 497590
    invoke-virtual {p2}, LX/1V4;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/facebook/feedplugins/attachments/linkshare/ButtonShareAttachmentDecoratorPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2sO;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
