.class public final LX/2qU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2q7;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/19P;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/2pu;

.field public final c:I

.field public d:LX/2q7;

.field private e:LX/04D;

.field private f:LX/04G;

.field private g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2p8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;LX/2q7;LX/2pu;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/19P;",
            ">;",
            "LX/2q7;",
            "LX/2pu;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 471000
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 471001
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 471002
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 471003
    iput-object p1, p0, LX/2qU;->a:Ljava/lang/ref/WeakReference;

    .line 471004
    iput-object p2, p0, LX/2qU;->d:LX/2q7;

    .line 471005
    iput-object p3, p0, LX/2qU;->b:LX/2pu;

    .line 471006
    iput p4, p0, LX/2qU;->c:I

    .line 471007
    iget-object v0, p0, LX/2qU;->b:LX/2pu;

    .line 471008
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, v0, LX/2pu;->c:Ljava/lang/ref/WeakReference;

    .line 471009
    return-void
.end method

.method public static B(LX/2qU;)V
    .locals 2

    .prologue
    .line 471010
    iget-object v0, p0, LX/2qU;->g:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 471011
    iget-object v0, p0, LX/2qU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19P;

    iget-object v0, v0, LX/19P;->e:Ljava/util/List;

    iget-object v1, p0, LX/2qU;->g:Ljava/lang/ref/WeakReference;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 471012
    const/4 v0, 0x0

    iput-object v0, p0, LX/2qU;->g:Ljava/lang/ref/WeakReference;

    .line 471013
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 471014
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-nez v0, :cond_0

    .line 471015
    :goto_0
    return-void

    .line 471016
    :cond_0
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(F)V

    goto :goto_0
.end method

.method public final a(ILX/04g;)V
    .locals 1

    .prologue
    .line 471017
    iget-object v0, p0, LX/2qU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19P;

    invoke-virtual {v0, p0, p2, p1}, LX/19P;->a(LX/2qU;LX/04g;I)V

    .line 471018
    return-void
.end method

.method public final a(LX/04D;)V
    .locals 2

    .prologue
    .line 471019
    iput-object p1, p0, LX/2qU;->e:LX/04D;

    .line 471020
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-eqz v0, :cond_0

    .line 471021
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    iget-object v1, p0, LX/2qU;->e:LX/04D;

    invoke-interface {v0, v1}, LX/2q7;->a(LX/04D;)V

    .line 471022
    :cond_0
    return-void
.end method

.method public final a(LX/04G;)V
    .locals 1

    .prologue
    .line 471023
    iput-object p1, p0, LX/2qU;->f:LX/04G;

    .line 471024
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-eqz v0, :cond_0

    .line 471025
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(LX/04G;)V

    .line 471026
    :cond_0
    return-void
.end method

.method public final a(LX/04H;)V
    .locals 1

    .prologue
    .line 471027
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(LX/04H;)V

    .line 471028
    return-void
.end method

.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 471029
    sget-object v0, LX/7K4;->a:LX/7K4;

    invoke-virtual {p0, p1, v0}, LX/2qU;->a(LX/04g;LX/7K4;)V

    .line 471030
    return-void
.end method

.method public final a(LX/04g;LX/7K4;)V
    .locals 1

    .prologue
    .line 471031
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 471032
    iget-object v0, p0, LX/2qU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19P;

    invoke-virtual {v0, p0, p1, p2}, LX/19P;->a(LX/2qU;LX/04g;LX/7K4;)V

    .line 471033
    return-void
.end method

.method public final a(LX/2oi;LX/04g;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 471034
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-nez v0, :cond_0

    .line 471035
    :goto_0
    return-void

    .line 471036
    :cond_0
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1, p2, p3}, LX/2q7;->a(LX/2oi;LX/04g;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(LX/2p8;)V
    .locals 2

    .prologue
    .line 471037
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 471038
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(LX/2p8;)V

    .line 471039
    invoke-static {p0}, LX/2qU;->B(LX/2qU;)V

    .line 471040
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2qU;->g:Ljava/lang/ref/WeakReference;

    .line 471041
    iget-object v0, p0, LX/2qU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19P;

    iget-object v0, v0, LX/19P;->e:Ljava/util/List;

    iget-object v1, p0, LX/2qU;->g:Ljava/lang/ref/WeakReference;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 471042
    return-void
.end method

.method public final a(LX/2qG;)V
    .locals 1

    .prologue
    .line 471067
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(LX/2qG;)V

    .line 471068
    return-void
.end method

.method public final a(LX/7K7;Ljava/lang/String;LX/04g;)V
    .locals 1

    .prologue
    .line 471043
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-nez v0, :cond_0

    .line 471044
    :goto_0
    return-void

    .line 471045
    :cond_0
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1, p2, p3}, LX/2q7;->a(LX/7K7;Ljava/lang/String;LX/04g;)V

    goto :goto_0
.end method

.method public final a(LX/7QP;)V
    .locals 1

    .prologue
    .line 471046
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(LX/7QP;)V

    .line 471047
    return-void
.end method

.method public final a(Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 471048
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(Landroid/graphics/RectF;)V

    .line 471049
    return-void
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;)V
    .locals 1

    .prologue
    .line 471050
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-nez v0, :cond_0

    .line 471051
    :goto_0
    return-void

    .line 471052
    :cond_0
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;)V
    .locals 1

    .prologue
    .line 471053
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-nez v0, :cond_0

    .line 471054
    :goto_0
    return-void

    .line 471055
    :cond_0
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;LX/2qi;Z)V
    .locals 1
    .param p2    # LX/2qi;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 471056
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1, p2, p3}, LX/2q7;->a(Lcom/facebook/video/engine/VideoPlayerParams;LX/2qi;Z)V

    .line 471057
    return-void
.end method

.method public final a(ZLX/04g;)V
    .locals 1

    .prologue
    .line 471058
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-nez v0, :cond_0

    .line 471059
    :goto_0
    return-void

    .line 471060
    :cond_0
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1, p2}, LX/2q7;->a(ZLX/04g;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 471061
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-nez v0, :cond_0

    .line 471062
    const/4 v0, 0x0

    .line 471063
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q8;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 471064
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q8;->b()I

    move-result v0

    return v0
.end method

.method public final b(LX/04g;)V
    .locals 1

    .prologue
    .line 471065
    iget-object v0, p0, LX/2qU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19P;

    invoke-virtual {v0, p0, p1}, LX/19P;->a(LX/2qU;LX/04g;)V

    .line 471066
    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 470997
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/04g;)V
    .locals 1

    .prologue
    .line 470998
    iget-object v0, p0, LX/2qU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19P;

    invoke-virtual {v0, p0, p1}, LX/19P;->b(LX/2qU;LX/04g;)V

    .line 470999
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 470947
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d(LX/04g;)V
    .locals 1

    .prologue
    .line 470977
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->d(LX/04g;)V

    .line 470978
    return-void
.end method

.method public final e()LX/2oi;
    .locals 1

    .prologue
    .line 470974
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-nez v0, :cond_0

    .line 470975
    const/4 v0, 0x0

    .line 470976
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->e()LX/2oi;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(LX/04g;)V
    .locals 1

    .prologue
    .line 470972
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->e(LX/04g;)V

    .line 470973
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 470963
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, LX/2qU;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 470964
    iget-object v0, p0, LX/2qU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 470965
    iget-object v0, p0, LX/2qU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19P;

    invoke-static {v0, p0}, LX/19P;->j(LX/19P;LX/2qU;)V

    .line 470966
    :cond_0
    iget-object v0, p0, LX/2qU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 470967
    iget-object v0, p0, LX/2qU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19P;

    invoke-virtual {v0, p0}, LX/19P;->a(LX/2qU;)V

    .line 470968
    :cond_1
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-eqz v0, :cond_2

    .line 470969
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->f()V

    .line 470970
    const/4 v0, 0x0

    iput-object v0, p0, LX/2qU;->d:LX/2q7;

    .line 470971
    :cond_2
    return-void
.end method

.method public final f(LX/04g;)V
    .locals 1

    .prologue
    .line 470960
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-nez v0, :cond_0

    .line 470961
    :goto_0
    return-void

    .line 470962
    :cond_0
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->b(LX/04g;)V

    goto :goto_0
.end method

.method public final g()LX/04D;
    .locals 1

    .prologue
    .line 470959
    iget-object v0, p0, LX/2qU;->e:LX/04D;

    return-object v0
.end method

.method public final g(LX/04g;)V
    .locals 1

    .prologue
    .line 470956
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 470957
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->c(LX/04g;)V

    .line 470958
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 470953
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-nez v0, :cond_0

    .line 470954
    const/4 v0, 0x0

    .line 470955
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->h()Z

    move-result v0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 470950
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-nez v0, :cond_0

    .line 470951
    const/4 v0, 0x0

    .line 470952
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->i()Z

    move-result v0

    goto :goto_0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 470948
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->j()V

    .line 470949
    return-void
.end method

.method public final k()Landroid/view/View;
    .locals 1

    .prologue
    .line 470945
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 470946
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->k()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 470979
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->l()I

    move-result v0

    return v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 470980
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->m()I

    move-result v0

    return v0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 470981
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-eqz v0, :cond_0

    .line 470982
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->n()V

    .line 470983
    :cond_0
    return-void
.end method

.method public final o()LX/7QP;
    .locals 1

    .prologue
    .line 470984
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-nez v0, :cond_0

    .line 470985
    const/4 v0, 0x0

    .line 470986
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->o()LX/7QP;

    move-result-object v0

    goto :goto_0
.end method

.method public final p()LX/7IE;
    .locals 1

    .prologue
    .line 470987
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    if-nez v0, :cond_0

    .line 470988
    const/4 v0, 0x0

    .line 470989
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->p()LX/7IE;

    move-result-object v0

    goto :goto_0
.end method

.method public final q()LX/16V;
    .locals 1

    .prologue
    .line 470990
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->q()LX/16V;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 470991
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 470992
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->s()I

    move-result v0

    return v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 470993
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->t()I

    move-result v0

    return v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 470994
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->u()Z

    move-result v0

    return v0
.end method

.method public final v()J
    .locals 2

    .prologue
    .line 470995
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->v()J

    move-result-wide v0

    return-wide v0
.end method

.method public final w()Lcom/facebook/video/engine/VideoPlayerParams;
    .locals 1

    .prologue
    .line 470996
    iget-object v0, p0, LX/2qU;->d:LX/2q7;

    invoke-interface {v0}, LX/2q7;->w()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    return-object v0
.end method
