.class public LX/3EG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/30V;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7mv;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0W3;

.field private d:LX/1Ar;


# direct methods
.method public constructor <init>(LX/0Or;LX/0W3;LX/1Ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/7mv;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/1Ar;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 537321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537322
    iput-object p1, p0, LX/3EG;->b:LX/0Or;

    .line 537323
    iput-object p2, p0, LX/3EG;->c:LX/0W3;

    .line 537324
    iput-object p3, p0, LX/3EG;->d:LX/1Ar;

    .line 537325
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 9

    .prologue
    .line 537326
    iget-object v0, p0, LX/3EG;->d:LX/1Ar;

    .line 537327
    iget-boolean v1, v0, LX/1Ar;->b:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/1Ar;->a:LX/0W3;

    sget-wide v3, LX/0X5;->iU:J

    invoke-interface {v1, v3, v4}, LX/0W4;->a(J)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 537328
    invoke-virtual {v0}, LX/1Ar;->b()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/1Ar;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 537329
    if-nez v1, :cond_2

    .line 537330
    iget-object v5, v0, LX/1Ar;->a:LX/0W3;

    sget-wide v7, LX/0X5;->iW:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    .line 537331
    if-eqz v5, :cond_5

    invoke-virtual {v0}, LX/1Ar;->c()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v0}, LX/1Ar;->f()Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_1
    const/4 v5, 0x1

    :goto_1
    move v1, v5

    .line 537332
    if-eqz v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 537333
    return v0

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    :cond_5
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final b()LX/2Jm;
    .locals 1

    .prologue
    .line 537334
    sget-object v0, LX/2Jm;->STATE_CHANGE:LX/2Jm;

    return-object v0
.end method

.method public final c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<+",
            "LX/2Jw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 537335
    iget-object v0, p0, LX/3EG;->b:LX/0Or;

    return-object v0
.end method

.method public final d()LX/2Jl;
    .locals 4

    .prologue
    .line 537336
    new-instance v0, LX/2Jk;

    invoke-direct {v0}, LX/2Jk;-><init>()V

    sget-object v1, LX/2Jh;->BACKGROUND:LX/2Jh;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jh;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Ji;->NOT_LOW:LX/2Ji;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Ji;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Jq;->LOGGED_IN:LX/2Jq;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jq;)LX/2Jk;

    move-result-object v0

    .line 537337
    iget-object v1, p0, LX/3EG;->c:LX/0W3;

    sget-wide v2, LX/0X5;->iV:J

    invoke-interface {v1, v2, v3}, LX/0W4;->a(J)Z

    move-result v1

    .line 537338
    if-eqz v1, :cond_0

    .line 537339
    sget-object v1, LX/2Im;->CONNECTED:LX/2Im;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Im;)LX/2Jk;

    .line 537340
    :goto_0
    invoke-virtual {v0}, LX/2Jk;->a()LX/2Jl;

    move-result-object v0

    return-object v0

    .line 537341
    :cond_0
    sget-object v1, LX/2Im;->CONNECTED_THROUGH_WIFI:LX/2Im;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Im;)LX/2Jk;

    goto :goto_0
.end method

.method public final e()J
    .locals 4

    .prologue
    .line 537342
    iget-object v0, p0, LX/3EG;->c:LX/0W3;

    sget-wide v2, LX/0X5;->iT:J

    const/16 v1, 0x4b0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    .line 537343
    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method
