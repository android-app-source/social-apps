.class public final enum LX/3Jw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3Jw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3Jw;

.field public static final enum C:LX/3Jw;

.field public static final enum L:LX/3Jw;

.field public static final enum M:LX/3Jw;

.field public static final enum Q:LX/3Jw;

.field public static final enum c:LX/3Jw;

.field public static final enum l:LX/3Jw;

.field public static final enum m:LX/3Jw;

.field public static final enum q:LX/3Jw;


# instance fields
.field public final argCount:I

.field public final argFormat:LX/3Jx;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x6

    const/4 v5, 0x4

    const/4 v4, 0x2

    .line 548169
    new-instance v0, LX/3Jw;

    const-string v1, "m"

    sget-object v2, LX/3Jx;->RELATIVE:LX/3Jx;

    invoke-direct {v0, v1, v7, v2, v4}, LX/3Jw;-><init>(Ljava/lang/String;ILX/3Jx;I)V

    sput-object v0, LX/3Jw;->m:LX/3Jw;

    .line 548170
    new-instance v0, LX/3Jw;

    const-string v1, "M"

    sget-object v2, LX/3Jx;->ABSOLUTE:LX/3Jx;

    invoke-direct {v0, v1, v8, v2, v4}, LX/3Jw;-><init>(Ljava/lang/String;ILX/3Jx;I)V

    sput-object v0, LX/3Jw;->M:LX/3Jw;

    .line 548171
    new-instance v0, LX/3Jw;

    const-string v1, "q"

    sget-object v2, LX/3Jx;->RELATIVE:LX/3Jx;

    invoke-direct {v0, v1, v4, v2, v5}, LX/3Jw;-><init>(Ljava/lang/String;ILX/3Jx;I)V

    sput-object v0, LX/3Jw;->q:LX/3Jw;

    .line 548172
    new-instance v0, LX/3Jw;

    const-string v1, "Q"

    const/4 v2, 0x3

    sget-object v3, LX/3Jx;->ABSOLUTE:LX/3Jx;

    invoke-direct {v0, v1, v2, v3, v5}, LX/3Jw;-><init>(Ljava/lang/String;ILX/3Jx;I)V

    sput-object v0, LX/3Jw;->Q:LX/3Jw;

    .line 548173
    new-instance v0, LX/3Jw;

    const-string v1, "c"

    sget-object v2, LX/3Jx;->RELATIVE:LX/3Jx;

    invoke-direct {v0, v1, v5, v2, v6}, LX/3Jw;-><init>(Ljava/lang/String;ILX/3Jx;I)V

    sput-object v0, LX/3Jw;->c:LX/3Jw;

    .line 548174
    new-instance v0, LX/3Jw;

    const-string v1, "C"

    const/4 v2, 0x5

    sget-object v3, LX/3Jx;->ABSOLUTE:LX/3Jx;

    invoke-direct {v0, v1, v2, v3, v6}, LX/3Jw;-><init>(Ljava/lang/String;ILX/3Jx;I)V

    sput-object v0, LX/3Jw;->C:LX/3Jw;

    .line 548175
    new-instance v0, LX/3Jw;

    const-string v1, "l"

    sget-object v2, LX/3Jx;->RELATIVE:LX/3Jx;

    invoke-direct {v0, v1, v6, v2, v4}, LX/3Jw;-><init>(Ljava/lang/String;ILX/3Jx;I)V

    sput-object v0, LX/3Jw;->l:LX/3Jw;

    .line 548176
    new-instance v0, LX/3Jw;

    const-string v1, "L"

    const/4 v2, 0x7

    sget-object v3, LX/3Jx;->ABSOLUTE:LX/3Jx;

    invoke-direct {v0, v1, v2, v3, v4}, LX/3Jw;-><init>(Ljava/lang/String;ILX/3Jx;I)V

    sput-object v0, LX/3Jw;->L:LX/3Jw;

    .line 548177
    const/16 v0, 0x8

    new-array v0, v0, [LX/3Jw;

    sget-object v1, LX/3Jw;->m:LX/3Jw;

    aput-object v1, v0, v7

    sget-object v1, LX/3Jw;->M:LX/3Jw;

    aput-object v1, v0, v8

    sget-object v1, LX/3Jw;->q:LX/3Jw;

    aput-object v1, v0, v4

    const/4 v1, 0x3

    sget-object v2, LX/3Jw;->Q:LX/3Jw;

    aput-object v2, v0, v1

    sget-object v1, LX/3Jw;->c:LX/3Jw;

    aput-object v1, v0, v5

    const/4 v1, 0x5

    sget-object v2, LX/3Jw;->C:LX/3Jw;

    aput-object v2, v0, v1

    sget-object v1, LX/3Jw;->l:LX/3Jw;

    aput-object v1, v0, v6

    const/4 v1, 0x7

    sget-object v2, LX/3Jw;->L:LX/3Jw;

    aput-object v2, v0, v1

    sput-object v0, LX/3Jw;->$VALUES:[LX/3Jw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/3Jx;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Jx;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 548178
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 548179
    iput-object p3, p0, LX/3Jw;->argFormat:LX/3Jx;

    .line 548180
    iput p4, p0, LX/3Jw;->argCount:I

    .line 548181
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3Jw;
    .locals 1

    .prologue
    .line 548182
    const-class v0, LX/3Jw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3Jw;

    return-object v0
.end method

.method public static values()[LX/3Jw;
    .locals 1

    .prologue
    .line 548183
    sget-object v0, LX/3Jw;->$VALUES:[LX/3Jw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3Jw;

    return-object v0
.end method
