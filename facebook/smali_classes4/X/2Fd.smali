.class public LX/2Fd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Fe;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2Fe",
        "<",
        "Lcom/facebook/apptab/state/NavigationImmersiveConfig;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2Fd;


# instance fields
.field public final a:LX/0pZ;


# direct methods
.method public constructor <init>(LX/0pZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387142
    iput-object p1, p0, LX/2Fd;->a:LX/0pZ;

    .line 387143
    return-void
.end method

.method public static a(LX/0QB;)LX/2Fd;
    .locals 4

    .prologue
    .line 387144
    sget-object v0, LX/2Fd;->b:LX/2Fd;

    if-nez v0, :cond_1

    .line 387145
    const-class v1, LX/2Fd;

    monitor-enter v1

    .line 387146
    :try_start_0
    sget-object v0, LX/2Fd;->b:LX/2Fd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387147
    if-eqz v2, :cond_0

    .line 387148
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 387149
    new-instance p0, LX/2Fd;

    invoke-static {v0}, LX/0pY;->a(LX/0QB;)LX/0pY;

    move-result-object v3

    check-cast v3, LX/0pZ;

    invoke-direct {p0, v3}, LX/2Fd;-><init>(LX/0pZ;)V

    .line 387150
    move-object v0, p0

    .line 387151
    sput-object v0, LX/2Fd;->b:LX/2Fd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387152
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387153
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387154
    :cond_1
    sget-object v0, LX/2Fd;->b:LX/2Fd;

    return-object v0

    .line 387155
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387156
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/2WT;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 387157
    new-instance v0, LX/5HE;

    invoke-direct {v0}, LX/5HE;-><init>()V

    const-string v1, "button_action"

    const-string v2, "system_back"

    invoke-virtual {p1, v1, v2}, LX/2WT;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 387158
    iput-object v1, v0, LX/5HE;->e:Ljava/lang/String;

    .line 387159
    move-object v0, v0

    .line 387160
    const-string v1, "badge_type"

    const-string v2, "none"

    invoke-virtual {p1, v1, v2}, LX/2WT;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 387161
    iput-object v1, v0, LX/5HE;->c:Ljava/lang/String;

    .line 387162
    move-object v0, v0

    .line 387163
    const-string v1, "badging_function"

    const-string v2, "all"

    invoke-virtual {p1, v1, v2}, LX/2WT;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 387164
    iput-object v1, v0, LX/5HE;->d:Ljava/lang/String;

    .line 387165
    move-object v0, v0

    .line 387166
    const-string v1, "animation_speed"

    const-string v2, "medium_speed_animation"

    invoke-virtual {p1, v1, v2}, LX/2WT;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 387167
    iput-object v1, v0, LX/5HE;->f:Ljava/lang/String;

    .line 387168
    move-object v0, v0

    .line 387169
    invoke-virtual {v0}, LX/5HE;->a()Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 387170
    const-string v0, "immersive_views_11_06"

    return-object v0
.end method
