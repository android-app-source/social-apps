.class public LX/2SO;
.super LX/2SP;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2SO;


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2SP;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/2SR;

.field public d:LX/2SR;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;)V
    .locals 3
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2SS;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2SV;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 412212
    invoke-direct {p0}, LX/2SP;-><init>()V

    .line 412213
    new-instance v0, LX/2SQ;

    invoke-direct {v0, p0}, LX/2SQ;-><init>(LX/2SO;)V

    iput-object v0, p0, LX/2SO;->c:LX/2SR;

    .line 412214
    iput-object p1, p0, LX/2SO;->b:LX/0Or;

    .line 412215
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 412216
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SS;

    .line 412217
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 412218
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 412219
    :cond_0
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SV;

    .line 412220
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 412221
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 412222
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/2SO;->a:LX/0Px;

    .line 412223
    return-void
.end method

.method public static a(LX/0QB;)LX/2SO;
    .locals 6

    .prologue
    .line 412199
    sget-object v0, LX/2SO;->e:LX/2SO;

    if-nez v0, :cond_1

    .line 412200
    const-class v1, LX/2SO;

    monitor-enter v1

    .line 412201
    :try_start_0
    sget-object v0, LX/2SO;->e:LX/2SO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 412202
    if-eqz v2, :cond_0

    .line 412203
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 412204
    new-instance v3, LX/2SO;

    const/16 v4, 0x15e7

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x11b3

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x11b2

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/2SO;-><init>(LX/0Or;LX/0Or;LX/0Or;)V

    .line 412205
    move-object v0, v3

    .line 412206
    sput-object v0, LX/2SO;->e:LX/2SO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412207
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 412208
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 412209
    :cond_1
    sget-object v0, LX/2SO;->e:LX/2SO;

    return-object v0

    .line 412210
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 412211
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/7BE;
    .locals 1

    .prologue
    .line 412198
    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-static {v0}, LX/2SP;->a(LX/0Px;)LX/7BE;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2SR;LX/2Sp;)V
    .locals 4

    .prologue
    .line 412192
    iput-object p1, p0, LX/2SO;->d:LX/2SR;

    .line 412193
    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412194
    if-eqz p1, :cond_0

    iget-object v1, p0, LX/2SO;->c:LX/2SR;

    :goto_1
    invoke-virtual {v0, v1, p2}, LX/2SP;->a(LX/2SR;LX/2Sp;)V

    .line 412195
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 412196
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 412197
    :cond_1
    return-void
.end method

.method public final a(LX/Cwb;)V
    .locals 3

    .prologue
    .line 412158
    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412159
    invoke-virtual {v0, p1}, LX/2SP;->a(LX/Cwb;)V

    .line 412160
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 412161
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 5
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 412187
    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412188
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, LX/EPu;->MEMORY:LX/EPu;

    if-ne p2, v3, :cond_0

    sget-object v3, LX/7BE;->READY:LX/7BE;

    invoke-virtual {v0}, LX/2SP;->a()LX/7BE;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/7BE;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 412189
    :cond_0
    invoke-virtual {v0, p1, p2}, LX/2SP;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V

    .line 412190
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 412191
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 3

    .prologue
    .line 412183
    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412184
    invoke-virtual {v0, p1}, LX/2SP;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 412185
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 412186
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 412182
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 412178
    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412179
    invoke-virtual {v0}, LX/2SP;->c()V

    .line 412180
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 412181
    :cond_0
    return-void
.end method

.method public final clearUserData()V
    .locals 3

    .prologue
    .line 412174
    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412175
    invoke-virtual {v0}, LX/2SP;->c()V

    .line 412176
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 412177
    :cond_0
    return-void
.end method

.method public final get()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 412165
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 412166
    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, LX/2SO;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412167
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, LX/7BE;->NOT_READY:LX/7BE;

    invoke-virtual {v0}, LX/2SP;->a()LX/7BE;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/7BE;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 412168
    :cond_0
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 412169
    invoke-virtual {v0}, LX/2SP;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 412170
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 412171
    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 412172
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 412173
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final init()V
    .locals 2

    .prologue
    .line 412162
    iget-object v0, p0, LX/2SO;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 412163
    const/4 v0, 0x0

    sget-object v1, LX/EPu;->MEMORY:LX/EPu;

    invoke-virtual {p0, v0, v1}, LX/2SP;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V

    .line 412164
    :cond_0
    return-void
.end method
