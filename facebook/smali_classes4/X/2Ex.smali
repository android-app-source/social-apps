.class public LX/2Ex;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 386173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2CZ;",
            "LX/2Ew;",
            "Ljava/lang/String;",
            "J",
            "LX/0Ot",
            "<+",
            "LX/2F5;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 386159
    move-object/from16 v0, p8

    move/from16 v1, p9

    invoke-virtual {p0, v0, v1}, LX/2CZ;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 386160
    :cond_0
    :goto_0
    return-void

    .line 386161
    :cond_1
    invoke-interface/range {p5 .. p5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, LX/2F5;

    .line 386162
    invoke-interface {v10}, LX/2F5;->a()J

    move-result-wide v4

    move-object v2, p0

    move-object/from16 v3, p6

    move-wide v6, p3

    move/from16 v8, p9

    move-object/from16 v9, p7

    .line 386163
    invoke-virtual/range {v2 .. v9}, LX/2CZ;->a(Ljava/lang/String;JJZLjava/lang/String;)J

    move-result-wide v2

    .line 386164
    cmp-long v6, p3, v2

    if-ltz v6, :cond_2

    .line 386165
    move-object/from16 v0, p7

    move/from16 v1, p9

    invoke-virtual {p0, v0, v4, v5, v1}, LX/2CZ;->a(Ljava/lang/String;JZ)J

    move-result-wide v2

    .line 386166
    invoke-virtual {p0, v10, p3, p4, p2}, LX/2CZ;->a(LX/2F1;JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    move-result-object v4

    .line 386167
    if-eqz v4, :cond_0

    .line 386168
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 386169
    invoke-virtual {p1, v4}, LX/2Ew;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 386170
    move-object/from16 v0, p6

    invoke-virtual {p1, v0, p3, p4}, LX/2Ew;->a(Ljava/lang/String;J)V

    .line 386171
    add-long/2addr v2, p3

    invoke-virtual {p1, v2, v3}, LX/2Ew;->a(J)V

    goto :goto_0

    .line 386172
    :cond_2
    invoke-virtual {p1, v2, v3}, LX/2Ew;->a(J)V

    goto :goto_0
.end method

.method public static a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2CZ;",
            "LX/2Ew;",
            "Ljava/lang/String;",
            "J",
            "LX/0Ot",
            "<+",
            "LX/2F1;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZJ)V"
        }
    .end annotation

    .prologue
    .line 386146
    move-object/from16 v0, p8

    move/from16 v1, p9

    invoke-virtual {p0, v0, v1}, LX/2CZ;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_1

    .line 386147
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v4, p0

    move-object/from16 v5, p6

    move-wide/from16 v6, p10

    move-wide/from16 v8, p3

    move/from16 v10, p9

    move-object/from16 v11, p7

    .line 386148
    invoke-virtual/range {v4 .. v11}, LX/2CZ;->a(Ljava/lang/String;JJZLjava/lang/String;)J

    move-result-wide v4

    .line 386149
    cmp-long v6, p3, v4

    if-ltz v6, :cond_2

    .line 386150
    invoke-interface/range {p5 .. p5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2F1;

    .line 386151
    move-object/from16 v0, p7

    move-wide/from16 v1, p10

    move/from16 v3, p9

    invoke-virtual {p0, v0, v1, v2, v3}, LX/2CZ;->a(Ljava/lang/String;JZ)J

    move-result-wide v6

    .line 386152
    move-wide/from16 v0, p3

    invoke-virtual {p0, v4, v0, v1, p2}, LX/2CZ;->a(LX/2F1;JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    move-result-object v4

    .line 386153
    if-eqz v4, :cond_0

    .line 386154
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 386155
    invoke-virtual {p1, v4}, LX/2Ew;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 386156
    move-object/from16 v0, p6

    move-wide/from16 v1, p3

    invoke-virtual {p1, v0, v1, v2}, LX/2Ew;->a(Ljava/lang/String;J)V

    .line 386157
    add-long v4, p3, v6

    invoke-virtual {p1, v4, v5}, LX/2Ew;->a(J)V

    goto :goto_0

    .line 386158
    :cond_2
    invoke-virtual {p1, v4, v5}, LX/2Ew;->a(J)V

    goto :goto_0
.end method
