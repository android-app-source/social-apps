.class public LX/2e1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444831
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444832
    const-string v0, "ccu_promo_card_position"

    iput-object v0, p0, LX/2e1;->b:Ljava/lang/String;

    .line 444833
    const-string v0, "ccu_promo_scrolling_position"

    iput-object v0, p0, LX/2e1;->c:Ljava/lang/String;

    .line 444834
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2e1;->d:Ljava/lang/String;

    .line 444835
    iput-object p1, p0, LX/2e1;->a:LX/0Zb;

    .line 444836
    return-void
.end method

.method public static a(LX/0QB;)LX/2e1;
    .locals 4

    .prologue
    .line 444844
    const-class v1, LX/2e1;

    monitor-enter v1

    .line 444845
    :try_start_0
    sget-object v0, LX/2e1;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444846
    sput-object v2, LX/2e1;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444847
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444848
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 444849
    new-instance p0, LX/2e1;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/2e1;-><init>(LX/0Zb;)V

    .line 444850
    move-object v0, p0

    .line 444851
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444852
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2e1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444853
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444854
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/2e1;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 444837
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "native_newsfeed"

    .line 444838
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 444839
    move-object v0, v0

    .line 444840
    iget-object v1, p0, LX/2e1;->d:Ljava/lang/String;

    .line 444841
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 444842
    move-object v0, v0

    .line 444843
    return-object v0
.end method
