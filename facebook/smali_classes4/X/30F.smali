.class public LX/30F;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Uo;

.field private final b:LX/0SG;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/2IK;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uo;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2IK;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Uo;",
            "LX/0SG;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2IK;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 484129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484130
    iput-object p1, p0, LX/30F;->a:LX/0Uo;

    .line 484131
    iput-object p2, p0, LX/30F;->b:LX/0SG;

    .line 484132
    iput-object p3, p0, LX/30F;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 484133
    iput-object p4, p0, LX/30F;->d:LX/2IK;

    .line 484134
    iput-object p5, p0, LX/30F;->e:LX/0Ot;

    .line 484135
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 484136
    iget-object v0, p0, LX/30F;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 484137
    iget-object v0, p0, LX/30F;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    new-instance v1, Lcom/facebook/selfupdate/remotepushtrigger/RemotePushTriggerReporter$1;

    invoke-direct {v1, p0}, Lcom/facebook/selfupdate/remotepushtrigger/RemotePushTriggerReporter$1;-><init>(LX/30F;)V

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/lang/Runnable;)V

    .line 484138
    :cond_0
    :goto_0
    return-void

    .line 484139
    :cond_1
    iget-object v0, p0, LX/30F;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 484140
    iget-object v0, p0, LX/30F;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/K2l;->a:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v0, v1, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 484141
    iget-object v0, p0, LX/30F;->a:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/30F;->d:LX/2IK;

    .line 484142
    iget-object v6, v0, LX/2IK;->a:LX/0ad;

    sget v7, LX/2IM;->c:I

    const/16 v8, 0xf

    invoke-interface {v6, v7, v8}, LX/0ad;->a(II)I

    move-result v6

    .line 484143
    int-to-long v6, v6

    invoke-static {v6, v7}, LX/1lQ;->p(J)J

    move-result-wide v6

    move-wide v0, v6

    .line 484144
    :goto_1
    sub-long v4, v2, v4

    cmp-long v0, v4, v0

    if-ltz v0, :cond_0

    .line 484145
    iget-object v0, p0, LX/30F;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/K2l;->a:LX/0Tn;

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 484146
    new-instance v0, LX/4Eg;

    invoke-direct {v0}, LX/4Eg;-><init>()V

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 484147
    const-string v2, "is_on_wifi"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 484148
    move-object v0, v0

    .line 484149
    new-instance v1, LX/K2o;

    invoke-direct {v1}, LX/K2o;-><init>()V

    move-object v1, v1

    .line 484150
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 484151
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 484152
    iget-object v0, p0, LX/30F;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 484153
    goto :goto_0

    .line 484154
    :cond_2
    iget-object v0, p0, LX/30F;->d:LX/2IK;

    .line 484155
    iget-object v6, v0, LX/2IK;->a:LX/0ad;

    sget v7, LX/2IM;->d:I

    const/16 v8, 0x3c

    invoke-interface {v6, v7, v8}, LX/0ad;->a(II)I

    move-result v6

    .line 484156
    int-to-long v6, v6

    invoke-static {v6, v7}, LX/1lQ;->o(J)J

    move-result-wide v6

    move-wide v0, v6

    .line 484157
    goto :goto_1
.end method
