.class public LX/3Et;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/11M;

.field private b:Lorg/apache/http/HttpResponse;


# direct methods
.method public constructor <init>(LX/11M;)V
    .locals 0

    .prologue
    .line 538127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538128
    iput-object p1, p0, LX/3Et;->a:LX/11M;

    .line 538129
    return-void
.end method


# virtual methods
.method public final a(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 538130
    iput-object p1, p0, LX/3Et;->b:Lorg/apache/http/HttpResponse;

    .line 538131
    iget-object v0, p0, LX/3Et;->a:LX/11M;

    invoke-virtual {v0, p1}, LX/11M;->a(Lorg/apache/http/HttpResponse;)V

    .line 538132
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 538133
    if-eqz v0, :cond_0

    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 538134
    invoke-virtual {p0, p1}, LX/3Et;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
