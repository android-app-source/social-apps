.class public LX/2TS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/location/LocationSignalDataPackage;

.field public final b:LX/2Tl;


# direct methods
.method public constructor <init>(Lcom/facebook/location/LocationSignalDataPackage;LX/2Tl;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 414416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414417
    iput-object p1, p0, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    .line 414418
    iput-object p2, p0, LX/2TS;->b:LX/2Tl;

    .line 414419
    iget-object v0, p0, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v0, v0, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, LX/2TS;->b:LX/2Tl;

    iget-object v3, v3, LX/2Tl;->a:Ljava/lang/Throwable;

    if-nez v3, :cond_1

    move v3, v1

    :goto_1
    xor-int/2addr v0, v3

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 414420
    iget-object v0, p0, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v0, v0, Lcom/facebook/location/LocationSignalDataPackage;->d:Ljava/util/List;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iget-object v3, p0, LX/2TS;->b:LX/2Tl;

    iget-object v3, v3, LX/2Tl;->b:Ljava/lang/Throwable;

    if-nez v3, :cond_3

    :goto_3
    xor-int/2addr v0, v1

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 414421
    return-void

    :cond_0
    move v0, v2

    .line 414422
    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v0, v2

    .line 414423
    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 414415
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "data"

    iget-object v2, p0, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "throwables"

    iget-object v2, p0, LX/2TS;->b:LX/2Tl;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
