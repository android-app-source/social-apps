.class public final LX/2cW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2cV;


# instance fields
.field public final synthetic a:LX/2cT;


# direct methods
.method public constructor <init>(LX/2cT;)V
    .locals 0

    .prologue
    .line 441011
    iput-object p1, p0, LX/2cW;->a:LX/2cT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/omnistore/Delta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 441012
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/omnistore/Delta;

    .line 441013
    :try_start_0
    iget-object v1, v0, Lcom/facebook/omnistore/Delta;->mType:Lcom/facebook/omnistore/Delta$Type;

    move-object v1, v1

    .line 441014
    sget-object v3, Lcom/facebook/omnistore/Delta$Type;->SAVE:Lcom/facebook/omnistore/Delta$Type;

    if-ne v1, v3, :cond_0

    .line 441015
    iget-object v1, p0, LX/2cW;->a:LX/2cT;

    iget-object v1, v1, LX/2cT;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;

    new-instance v3, Lcom/facebook/messaging/tincan/omnistore/TincanMessage;

    .line 441016
    iget-object v4, v0, Lcom/facebook/omnistore/Delta;->mPrimaryKey:Ljava/lang/String;

    move-object v4, v4

    .line 441017
    iget-object p1, v0, Lcom/facebook/omnistore/Delta;->mBlob:Ljava/nio/ByteBuffer;

    move-object v0, p1

    .line 441018
    invoke-direct {v3, v4, v0}, Lcom/facebook/messaging/tincan/omnistore/TincanMessage;-><init>(Ljava/lang/String;Ljava/nio/ByteBuffer;)V

    invoke-virtual {v1, v3}, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->a(Lcom/facebook/messaging/tincan/omnistore/TincanMessage;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 441019
    :catch_0
    move-exception v0

    .line 441020
    sget-object v1, LX/2cT;->a:Ljava/lang/Class;

    const-string v3, "Exception processing messaging collection delta"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 441021
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2cW;->a:LX/2cT;

    iget-object v0, v0, LX/2cT;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;

    iget-object v1, p0, LX/2cW;->a:LX/2cT;

    invoke-static {v1}, LX/2cT;->b$redex0(LX/2cT;)J

    move-result-wide v2

    .line 441022
    iget-object v1, v0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->c:LX/0TD;

    new-instance v4, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher$1;

    invoke-direct {v4, v0, v2, v3}, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher$1;-><init>(Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;J)V

    const p0, 0xbc847b0

    invoke-static {v1, v4, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 441023
    :goto_1
    return-void

    .line 441024
    :catch_1
    move-exception v0

    .line 441025
    sget-object v1, LX/2cT;->a:Ljava/lang/Class;

    const-string v2, "Failed to update tincan_msg global version id:"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
