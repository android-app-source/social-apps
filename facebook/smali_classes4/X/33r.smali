.class public LX/33r;
.super LX/2h2;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h2;",
        "LX/0e6",
        "<",
        "Lcom/facebook/zero/server/ZeroUpdateStatusParams;",
        "LX/1pN;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 494258
    const-class v0, LX/33r;

    sput-object v0, LX/33r;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 494268
    invoke-direct {p0}, LX/2h2;-><init>()V

    .line 494269
    return-void
.end method

.method private static b(Lcom/facebook/zero/server/ZeroUpdateStatusParams;)Ljava/util/List;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/server/ZeroUpdateStatusParams;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 494263
    invoke-static {p0}, LX/2h2;->a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;)Ljava/util/List;

    move-result-object v0

    .line 494264
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "status_to_update"

    .line 494265
    iget-object v3, p0, Lcom/facebook/zero/server/ZeroUpdateStatusParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 494266
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494267
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 494260
    check-cast p1, Lcom/facebook/zero/server/ZeroUpdateStatusParams;

    .line 494261
    invoke-static {p1}, LX/33r;->b(Lcom/facebook/zero/server/ZeroUpdateStatusParams;)Ljava/util/List;

    move-result-object v4

    .line 494262
    new-instance v0, LX/14N;

    const-string v1, "zeroUpdateStatus"

    const-string v2, "GET"

    const-string v3, "method/mobile.zeroUpdateStatus"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 494259
    return-object p2
.end method
