.class public LX/3NB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/3Mw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/3MV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 558538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/0QB;)LX/3NB;
    .locals 5

    .prologue
    .line 558534
    new-instance v3, LX/3NB;

    invoke-direct {v3}, LX/3NB;-><init>()V

    .line 558535
    const/16 v0, 0x12cb

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/3Mw;->b(LX/0QB;)LX/3Mw;

    move-result-object v1

    check-cast v1, LX/3Mw;

    invoke-static {p0}, LX/3MV;->a(LX/0QB;)LX/3MV;

    move-result-object v2

    check-cast v2, LX/3MV;

    .line 558536
    iput-object v4, v3, LX/3NB;->a:LX/0Or;

    iput-object v0, v3, LX/3NB;->b:LX/0tX;

    iput-object v1, v3, LX/3NB;->c:LX/3Mw;

    iput-object v2, v3, LX/3NB;->d:LX/3MV;

    .line 558537
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;)Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;
    .locals 9

    .prologue
    .line 558491
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->c:Z

    move v0, v0

    .line 558492
    if-nez v0, :cond_0

    .line 558493
    invoke-static {}, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->a()Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;

    move-result-object v0

    .line 558494
    :goto_0
    return-object v0

    .line 558495
    :cond_0
    iget-object v0, p0, LX/3NB;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 558496
    if-nez v0, :cond_1

    .line 558497
    invoke-static {}, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->a()Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;

    move-result-object v0

    goto :goto_0

    .line 558498
    :cond_1
    new-instance v4, LX/5Z7;

    invoke-direct {v4}, LX/5Z7;-><init>()V

    move-object v4, v4

    .line 558499
    const-string v5, "search_query"

    .line 558500
    iget-object v6, p1, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->b:Ljava/lang/String;

    move-object v6, v6

    .line 558501
    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "max_count"

    .line 558502
    iget v7, p1, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->a:I

    move v7, v7

    .line 558503
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 558504
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    const-wide/16 v6, 0x78

    invoke-virtual {v4, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    move-object v1, v4

    .line 558505
    iget-object v2, p0, LX/3NB;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    const v2, -0x400e530f

    invoke-static {v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 558506
    iget-object v2, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v2

    .line 558507
    check-cast v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;

    .line 558508
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->asList()LX/0Px;

    move-result-object v1

    .line 558509
    const/4 v4, 0x0

    .line 558510
    if-nez v1, :cond_2

    .line 558511
    new-instance v2, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 558512
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 558513
    invoke-direct {v2, v3, v4}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    .line 558514
    :goto_1
    move-object v0, v2

    .line 558515
    iget-object v2, p0, LX/3NB;->d:LX/3MV;

    invoke-virtual {v2, v1}, LX/3MV;->a(Ljava/util/List;)LX/0Px;

    move-result-object v1

    .line 558516
    invoke-static {}, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->newBuilder()LX/6iu;

    move-result-object v2

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    .line 558517
    iput-object v3, v2, LX/6iu;->a:LX/0ta;

    .line 558518
    move-object v2, v2

    .line 558519
    iput-object v0, v2, LX/6iu;->b:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 558520
    move-object v0, v2

    .line 558521
    iput-object v1, v0, LX/6iu;->c:Ljava/util/List;

    .line 558522
    move-object v0, v0

    .line 558523
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 558524
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    .line 558525
    iput-wide v2, v0, LX/6iu;->d:J

    .line 558526
    move-object v0, v0

    .line 558527
    invoke-virtual {v0}, LX/6iu;->f()Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;

    move-result-object v0

    goto/16 :goto_0

    .line 558528
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 558529
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v6

    move v3, v4

    :goto_2
    if-ge v3, v6, :cond_3

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    .line 558530
    iget-object v7, p0, LX/3NB;->c:LX/3Mw;

    invoke-virtual {v7, v2, v0}, LX/3Mw;->a(LX/5Vt;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v7

    .line 558531
    iget-object v8, p0, LX/3NB;->c:LX/3Mw;

    const/4 p1, 0x0

    invoke-virtual {v8, v7, v2, p1, v0}, LX/3Mw;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/5Vt;LX/0P1;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 558532
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 558533
    :cond_3
    new-instance v2, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    goto :goto_1
.end method
