.class public final LX/35e;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 497317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel;)Lcom/facebook/graphql/model/GraphQLViewer;
    .locals 7

    .prologue
    .line 497318
    if-nez p0, :cond_0

    .line 497319
    const/4 v0, 0x0

    .line 497320
    :goto_0
    return-object v0

    .line 497321
    :cond_0
    new-instance v0, LX/2sK;

    invoke-direct {v0}, LX/2sK;-><init>()V

    .line 497322
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel;->a()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel$ComposerPrivacyOptionsModel;

    move-result-object v1

    .line 497323
    if-nez v1, :cond_1

    .line 497324
    const/4 v2, 0x0

    .line 497325
    :goto_1
    move-object v1, v2

    .line 497326
    iput-object v1, v0, LX/2sK;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    .line 497327
    invoke-virtual {v0}, LX/2sK;->a()Lcom/facebook/graphql/model/GraphQLViewer;

    move-result-object v0

    goto :goto_0

    .line 497328
    :cond_1
    new-instance v4, LX/2td;

    invoke-direct {v4}, LX/2td;-><init>()V

    .line 497329
    invoke-virtual {v1}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel$ComposerPrivacyOptionsModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 497330
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 497331
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    invoke-virtual {v1}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel$ComposerPrivacyOptionsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 497332
    invoke-virtual {v1}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel$ComposerPrivacyOptionsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;

    .line 497333
    if-nez v2, :cond_4

    .line 497334
    const/4 v6, 0x0

    .line 497335
    :goto_3
    move-object v2, v6

    .line 497336
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 497337
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 497338
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 497339
    iput-object v2, v4, LX/2td;->b:LX/0Px;

    .line 497340
    :cond_3
    new-instance v2, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    invoke-direct {v2, v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;-><init>(LX/2td;)V

    .line 497341
    move-object v2, v2

    .line 497342
    goto :goto_1

    .line 497343
    :cond_4
    new-instance v6, LX/2sW;

    invoke-direct {v6}, LX/2sW;-><init>()V

    .line 497344
    invoke-virtual {v2}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->a()Z

    move-result p0

    .line 497345
    iput-boolean p0, v6, LX/2sW;->b:Z

    .line 497346
    invoke-virtual {v2}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->b()Z

    move-result p0

    .line 497347
    iput-boolean p0, v6, LX/2sW;->c:Z

    .line 497348
    invoke-virtual {v2}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->c()Z

    move-result p0

    .line 497349
    iput-boolean p0, v6, LX/2sW;->d:Z

    .line 497350
    invoke-virtual {v2}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->d()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object p0

    .line 497351
    iput-object p0, v6, LX/2sW;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 497352
    invoke-virtual {v2}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object p0

    .line 497353
    iput-object p0, v6, LX/2sW;->f:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 497354
    new-instance p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;

    invoke-direct {p0, v6}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;-><init>(LX/2sW;)V

    .line 497355
    move-object v6, p0

    .line 497356
    goto :goto_3
.end method
