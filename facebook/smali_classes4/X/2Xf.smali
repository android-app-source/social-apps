.class public final LX/2Xf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<*>;",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 420061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 420062
    check-cast p1, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 420063
    if-nez p1, :cond_0

    .line 420064
    const/4 v0, 0x0

    .line 420065
    :goto_0
    return-object v0

    .line 420066
    :cond_0
    new-instance v0, LX/2sL;

    invoke-direct {v0, p1}, LX/2sL;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 420067
    new-instance v1, LX/2u0;

    invoke-direct {v1, v0}, LX/2u0;-><init>(LX/2sL;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object p0

    invoke-static {p1, v1, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 420068
    move-object v0, v0

    .line 420069
    goto :goto_0
.end method
