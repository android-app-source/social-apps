.class public LX/2gf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;",
        "Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 448760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448761
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 448762
    check-cast p1, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;

    .line 448763
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 448764
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 448765
    iget-object v0, p1, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 448766
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 448767
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "client_time"

    iget-object v2, p1, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 448768
    iget-boolean v0, p1, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->c:Z

    if-eqz v0, :cond_0

    .line 448769
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "send_sms"

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 448770
    :cond_0
    new-instance v0, LX/14N;

    const-string v1, "graphUserTOTPKeysPost"

    const-string v2, "POST"

    const-string v3, "/%s/totpkeys"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p1, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;->a:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v3, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 448771
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 448772
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 448773
    const-string v1, "key"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 448774
    const-string v2, "time_offset"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 448775
    new-instance v2, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;

    invoke-static {v1}, LX/2Zs;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 448776
    return-object v2
.end method
