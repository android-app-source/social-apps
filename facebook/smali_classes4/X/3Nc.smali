.class public abstract LX/3Nc;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/3O5;

.field public c:LX/3O6;

.field public d:LX/3O7;

.field public e:LX/3Nd;

.field public f:LX/3LG;

.field public g:LX/3Ne;

.field public h:LX/3NY;

.field public i:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 559622
    const-class v0, LX/3Nc;

    sput-object v0, LX/3Nc;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/3LG;I)V
    .locals 1

    .prologue
    .line 559609
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 559610
    sget-object v0, LX/3Nd;->NONE:LX/3Nd;

    iput-object v0, p0, LX/3Nc;->e:LX/3Nd;

    .line 559611
    iput-object p2, p0, LX/3Nc;->f:LX/3LG;

    .line 559612
    new-instance v0, LX/3Ne;

    invoke-virtual {p0}, LX/3Nc;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1, p3}, LX/3Ne;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/3Nc;->g:LX/3Ne;

    .line 559613
    iget-object v0, p0, LX/3Nc;->g:LX/3Ne;

    iget-object p1, p0, LX/3Nc;->f:LX/3LG;

    invoke-virtual {v0, p1}, LX/3Ne;->setAdapter(LX/3LH;)V

    .line 559614
    iget-object v0, p0, LX/3Nc;->g:LX/3Ne;

    invoke-virtual {p0, v0}, LX/3Nc;->addView(Landroid/view/View;)V

    .line 559615
    iget-object v0, p0, LX/3Nc;->g:LX/3Ne;

    new-instance p1, LX/3Nu;

    invoke-direct {p1, p0}, LX/3Nu;-><init>(LX/3Nc;)V

    .line 559616
    iput-object p1, v0, LX/3Ne;->d:LX/3Nv;

    .line 559617
    const v0, 0x7f0d2059

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3Nc;->i:Landroid/view/View;

    .line 559618
    iget-object v0, p0, LX/3Nc;->i:Landroid/view/View;

    new-instance p1, LX/3Nw;

    invoke-direct {p1, p0}, LX/3Nw;-><init>(LX/3Nc;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 559619
    invoke-virtual {p0}, LX/3Nc;->d()V

    .line 559620
    new-instance v0, LX/3Nz;

    invoke-direct {v0, p0}, LX/3Nz;-><init>(LX/3Nc;)V

    iput-object v0, p0, LX/3Nc;->h:LX/3NY;

    .line 559621
    return-void
.end method

.method public static synthetic a(LX/3Nc;)V
    .locals 2

    .prologue
    .line 559598
    invoke-virtual {p0}, LX/3Nc;->getSearchBar()LX/3Ng;

    move-result-object v0

    invoke-interface {v0}, LX/3Ng;->getSearchText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 559599
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559600
    iget-object v0, p0, LX/3Nc;->g:LX/3Ne;

    invoke-virtual {v0}, LX/3Ne;->b()V

    .line 559601
    sget-object v0, LX/3Nd;->UNFILTERED:LX/3Nd;

    invoke-virtual {p0, v0}, LX/3Nc;->a(LX/3Nd;)V

    .line 559602
    :goto_0
    invoke-static {p0}, LX/3Nc;->a$redex0(LX/3Nc;)V

    .line 559603
    return-void

    .line 559604
    :cond_0
    iget-object v0, p0, LX/3Nc;->f:LX/3LG;

    invoke-virtual {v0}, LX/3LG;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 559605
    iget-object v0, p0, LX/3Nc;->g:LX/3Ne;

    sget-object v1, LX/3Nx;->NO_RESULTS:LX/3Nx;

    invoke-virtual {v0, v1}, LX/3Ne;->a(LX/3Nx;)V

    .line 559606
    sget-object v0, LX/3Nd;->FILTERED:LX/3Nd;

    invoke-virtual {p0, v0}, LX/3Nc;->a(LX/3Nd;)V

    goto :goto_0

    .line 559607
    :cond_1
    iget-object v0, p0, LX/3Nc;->g:LX/3Ne;

    invoke-virtual {v0}, LX/3Ne;->b()V

    .line 559608
    sget-object v0, LX/3Nd;->FILTERED:LX/3Nd;

    invoke-virtual {p0, v0}, LX/3Nc;->a(LX/3Nd;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/3Nc;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 559588
    invoke-virtual {p0}, LX/3Nc;->getSearchBar()LX/3Ng;

    move-result-object v0

    invoke-interface {v0}, LX/3Ng;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Nc;->e:LX/3Nd;

    sget-object v3, LX/3Nd;->FILTERED:LX/3Nd;

    if-eq v0, v3, :cond_0

    move v0, v1

    .line 559589
    :goto_0
    iget-object v3, p0, LX/3Nc;->g:LX/3Ne;

    .line 559590
    iget-object v4, v3, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    move-object v3, v4

    .line 559591
    if-eqz v0, :cond_1

    .line 559592
    invoke-virtual {v3, v2}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 559593
    iget-object v0, p0, LX/3Nc;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 559594
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 559595
    goto :goto_0

    .line 559596
    :cond_1
    invoke-virtual {v3, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 559597
    iget-object v0, p0, LX/3Nc;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public static b(LX/3Nc;I)V
    .locals 1

    .prologue
    .line 559585
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 559586
    invoke-virtual {p0}, LX/3Nc;->getSearchBar()LX/3Ng;

    move-result-object v0

    invoke-interface {v0}, LX/3Ng;->d()V

    .line 559587
    :cond_0
    return-void
.end method


# virtual methods
.method public a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 559581
    iget-object v0, p0, LX/3Nc;->g:LX/3Ne;

    invoke-virtual {v0, p1}, LX/3Ne;->a(LX/0Px;)V

    .line 559582
    iget-object v0, p0, LX/3Nc;->e:LX/3Nd;

    sget-object v1, LX/3Nd;->NONE:LX/3Nd;

    if-eq v0, v1, :cond_0

    .line 559583
    invoke-virtual {p0}, LX/3Nc;->c()V

    .line 559584
    :cond_0
    return-void
.end method

.method public a(LX/3Nd;)V
    .locals 0

    .prologue
    .line 559623
    iput-object p1, p0, LX/3Nc;->e:LX/3Nd;

    .line 559624
    return-void
.end method

.method public a(Landroid/view/View;Z)V
    .locals 5

    .prologue
    .line 559546
    if-nez p2, :cond_4

    .line 559547
    iget-object v0, p0, LX/3Nc;->e:LX/3Nd;

    sget-object v1, LX/3Nd;->NONE:LX/3Nd;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, LX/3Nc;->d:LX/3O7;

    if-eqz v0, :cond_2

    .line 559548
    iget-object v0, p0, LX/3Nc;->d:LX/3O7;

    .line 559549
    iget-object v1, v0, LX/3O7;->a:LX/3Na;

    iget-object v1, v1, LX/3Na;->g:LX/3LU;

    if-eqz v1, :cond_2

    .line 559550
    iget-object v1, v0, LX/3O7;->a:LX/3Na;

    iget-object v1, v1, LX/3Na;->g:LX/3LU;

    .line 559551
    iget-object v2, v1, LX/3LU;->d:LX/3LV;

    .line 559552
    iget-boolean v3, v2, LX/3LV;->d:Z

    move v2, v3

    .line 559553
    if-eqz v2, :cond_2

    .line 559554
    iget-object v2, v1, LX/3LU;->d:LX/3LV;

    const/4 v3, 0x0

    .line 559555
    iget-boolean v4, v2, LX/3LV;->d:Z

    if-nez v4, :cond_0

    .line 559556
    iget-object v4, v2, LX/3LV;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/03V;

    const-string p1, "MessengerSearchFunnelLoggerlogEndWithoutStarting"

    const-string p2, "Attempting to end a search session while one hasn\'t been started!"

    invoke-virtual {v4, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 559557
    :cond_0
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v4

    const-string p1, "total_search_attempts"

    iget p2, v2, LX/3LV;->e:I

    invoke-virtual {v4, p1, p2}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v4

    .line 559558
    if-eqz v3, :cond_1

    .line 559559
    const-string p1, "impression_list"

    invoke-virtual {v3}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p1, p2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 559560
    :cond_1
    iget-object p1, v2, LX/3LV;->b:LX/0if;

    sget-object p2, LX/0ig;->R:LX/0ih;

    const-string v0, "search_session_ended"

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 559561
    iget-object v4, v2, LX/3LV;->b:LX/0if;

    sget-object p1, LX/0ig;->R:LX/0ih;

    invoke-virtual {v4, p1}, LX/0if;->c(LX/0ih;)V

    .line 559562
    const/4 v4, 0x0

    iput-boolean v4, v2, LX/3LV;->d:Z

    .line 559563
    const/4 v4, -0x1

    iput v4, v2, LX/3LV;->e:I

    .line 559564
    :cond_2
    sget-object v0, LX/3Nd;->NONE:LX/3Nd;

    invoke-virtual {p0, v0}, LX/3Nc;->a(LX/3Nd;)V

    .line 559565
    iget-object v0, p0, LX/3Nc;->f:LX/3LG;

    invoke-virtual {v0}, LX/3LG;->e()V

    .line 559566
    :cond_3
    :goto_0
    invoke-static {p0}, LX/3Nc;->a$redex0(LX/3Nc;)V

    .line 559567
    return-void

    .line 559568
    :cond_4
    iget-object v0, p0, LX/3Nc;->e:LX/3Nd;

    sget-object v1, LX/3Nd;->NONE:LX/3Nd;

    if-ne v0, v1, :cond_3

    .line 559569
    sget-object v0, LX/3Nd;->UNFILTERED:LX/3Nd;

    invoke-virtual {p0, v0}, LX/3Nc;->a(LX/3Nd;)V

    .line 559570
    iget-object v0, p0, LX/3Nc;->b:LX/3O5;

    if-eqz v0, :cond_3

    .line 559571
    iget-object v0, p0, LX/3Nc;->b:LX/3O5;

    .line 559572
    iget-object v1, v0, LX/3O5;->a:LX/3Na;

    iget-object v1, v1, LX/3Na;->g:LX/3LU;

    if-eqz v1, :cond_5

    .line 559573
    iget-object v1, v0, LX/3O5;->a:LX/3Na;

    iget-object v1, v1, LX/3Na;->g:LX/3LU;

    .line 559574
    iget-object v2, v1, LX/3LU;->d:LX/3LV;

    const-string v3, "divebar"

    .line 559575
    const/4 v4, 0x1

    iput-boolean v4, v2, LX/3LV;->d:Z

    .line 559576
    const/4 v4, 0x0

    iput v4, v2, LX/3LV;->e:I

    .line 559577
    iget-object v4, v2, LX/3LV;->b:LX/0if;

    sget-object p1, LX/0ig;->R:LX/0ih;

    invoke-virtual {v4, p1}, LX/0if;->a(LX/0ih;)V

    .line 559578
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v4

    const-string p1, "surface"

    invoke-virtual {v4, p1, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v4

    const-string p1, "locale"

    iget-object p2, v2, LX/3LV;->c:LX/0W9;

    invoke-virtual {p2}, LX/0W9;->c()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p1, p2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v4

    .line 559579
    iget-object p1, v2, LX/3LV;->b:LX/0if;

    sget-object p2, LX/0ig;->R:LX/0ih;

    const-string v0, "search_session_started_by_user"

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 559580
    :cond_5
    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 559545
    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 559524
    iget-object v0, p0, LX/3Nc;->f:LX/3LG;

    invoke-virtual {v0}, LX/3LG;->d()LX/3Mi;

    move-result-object v0

    .line 559525
    invoke-virtual {p0}, LX/3Nc;->getSearchBar()LX/3Ng;

    move-result-object v1

    invoke-interface {v1}, LX/3Ng;->getSearchText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 559526
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 559527
    iget-object v2, p0, LX/3Nc;->e:LX/3Nd;

    sget-object v3, LX/3Nd;->NONE:LX/3Nd;

    if-eq v2, v3, :cond_0

    .line 559528
    sget-object v2, LX/3Nd;->UNFILTERED:LX/3Nd;

    invoke-virtual {p0, v2}, LX/3Nc;->a(LX/3Nd;)V

    .line 559529
    :cond_0
    invoke-static {p0}, LX/3Nc;->a$redex0(LX/3Nc;)V

    .line 559530
    const/4 v2, 0x0

    iget-object v3, p0, LX/3Nc;->h:LX/3NY;

    invoke-interface {v0, v2, v3}, LX/333;->a(Ljava/lang/CharSequence;LX/3NY;)V

    .line 559531
    :goto_0
    iget-object v0, p0, LX/3Nc;->c:LX/3O6;

    if-eqz v0, :cond_2

    .line 559532
    iget-object v0, p0, LX/3Nc;->c:LX/3O6;

    .line 559533
    iget-object v2, v0, LX/3O6;->a:LX/3Na;

    iget-object v2, v2, LX/3Na;->g:LX/3LU;

    if-eqz v2, :cond_2

    .line 559534
    iget-object v2, v0, LX/3O6;->a:LX/3Na;

    iget-object v2, v2, LX/3Na;->g:LX/3LU;

    .line 559535
    if-eqz v1, :cond_2

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 559536
    iget-object v3, v2, LX/3LU;->d:LX/3LV;

    .line 559537
    iget-boolean v4, v3, LX/3LV;->d:Z

    if-nez v4, :cond_1

    .line 559538
    iget-object v4, v3, LX/3LV;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/03V;

    const-string v5, "MessengerSearchFunnelLoggerlogQueryWithoutStarting"

    const-string p0, "Logging search query without being in a session!"

    invoke-virtual {v4, v5, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 559539
    :cond_1
    iget v4, v3, LX/3LV;->e:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, LX/3LV;->e:I

    .line 559540
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v4

    const-string v5, "search_terms"

    invoke-virtual {v4, v5, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v4

    .line 559541
    iget-object v5, v3, LX/3LV;->b:LX/0if;

    sget-object p0, LX/0ig;->R:LX/0ih;

    const-string v0, "search_session_query_attempt"

    const/4 v2, 0x0

    invoke-virtual {v5, p0, v0, v2, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 559542
    :cond_2
    return-void

    .line 559543
    :cond_3
    sget-object v2, LX/3Nd;->FILTERING:LX/3Nd;

    invoke-virtual {p0, v2}, LX/3Nc;->a(LX/3Nd;)V

    .line 559544
    iget-object v2, p0, LX/3Nc;->h:LX/3NY;

    invoke-interface {v0, v1, v2}, LX/333;->a(Ljava/lang/CharSequence;LX/3NY;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 559522
    iget-object v0, p0, LX/3Nc;->g:LX/3Ne;

    invoke-virtual {v0}, LX/3Ne;->a()V

    .line 559523
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 559511
    invoke-virtual {p0}, LX/3Nc;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-virtual {p0}, LX/3Nc;->getSearchBar()LX/3Ng;

    move-result-object v1

    invoke-interface {v1}, LX/3Ng;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 559512
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public abstract getSearchBar()LX/3Ng;
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    .line 559513
    const/4 v0, -0x2

    .line 559514
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 559515
    invoke-virtual {p0}, LX/3Nc;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f010350

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 559516
    invoke-virtual {p0}, LX/3Nc;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 559517
    :cond_0
    invoke-virtual {p0}, LX/3Nc;->getSearchBar()LX/3Ng;

    move-result-object v1

    invoke-interface {v1}, LX/3Ng;->getThisView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 559518
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 559519
    invoke-virtual {p0}, LX/3Nc;->getSearchBar()LX/3Ng;

    move-result-object v0

    invoke-interface {v0}, LX/3Ng;->getThisView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 559520
    invoke-virtual {p0}, LX/3Nc;->c()V

    .line 559521
    return-void
.end method
