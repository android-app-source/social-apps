.class public LX/3CS;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 530181
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 530182
    return-void
.end method

.method public static a(LX/01T;)Ljava/lang/Boolean;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/DisableMessagingBackgroundTasks;
    .end annotation

    .prologue
    .line 530192
    sget-object v0, LX/01T;->FB4A:LX/01T;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0Uh;)Ljava/lang/Boolean;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/forcemessenger/annotations/IsInAccountSwitchingDiode;
    .end annotation

    .prologue
    .line 530191
    const/16 v0, 0xfb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/23i;)Ljava/lang/Boolean;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/IsShouldRedirectToMessenger;
    .end annotation

    .prologue
    .line 530193
    sget-object v0, LX/03R;->UNSET:LX/03R;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/23i;->a(LX/03R;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;)Ljava/lang/Boolean;
    .locals 5
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/forcemessenger/annotations/IsUserInDiode;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 530184
    const/16 v2, 0x54

    invoke-virtual {p1, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 530185
    sget-object v3, LX/3CC;->c:LX/0Tn;

    invoke-interface {p0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 530186
    sget-object v3, LX/3CC;->c:LX/0Tn;

    sget-object v4, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v4}, LX/03R;->getDbValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 530187
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v3

    if-nez v2, :cond_0

    :goto_0
    invoke-virtual {v3, v0}, LX/03R;->asBoolean(Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 530188
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 530189
    goto :goto_0

    .line 530190
    :cond_1
    if-nez v2, :cond_2

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 530183
    return-void
.end method
