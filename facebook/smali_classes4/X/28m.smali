.class public final LX/28m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0pQ;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0pQ;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 374692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374693
    iput-object p1, p0, LX/28m;->a:LX/0QB;

    .line 374694
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 374695
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/28m;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 374696
    packed-switch p2, :pswitch_data_0

    .line 374697
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 374698
    :pswitch_0
    new-instance v0, LX/BZn;

    invoke-direct {v0}, LX/BZn;-><init>()V

    .line 374699
    move-object v0, v0

    .line 374700
    move-object v0, v0

    .line 374701
    :goto_0
    return-object v0

    .line 374702
    :pswitch_1
    new-instance v0, LX/1ZE;

    invoke-direct {v0}, LX/1ZE;-><init>()V

    .line 374703
    move-object v0, v0

    .line 374704
    move-object v0, v0

    .line 374705
    goto :goto_0

    .line 374706
    :pswitch_2
    new-instance v0, LX/0pP;

    invoke-direct {v0}, LX/0pP;-><init>()V

    .line 374707
    move-object v0, v0

    .line 374708
    move-object v0, v0

    .line 374709
    goto :goto_0

    .line 374710
    :pswitch_3
    invoke-static {p1}, LX/2d3;->b(LX/0QB;)LX/2d3;

    move-result-object v0

    goto :goto_0

    .line 374711
    :pswitch_4
    new-instance v0, LX/Ezc;

    invoke-direct {v0}, LX/Ezc;-><init>()V

    .line 374712
    move-object v0, v0

    .line 374713
    move-object v0, v0

    .line 374714
    goto :goto_0

    .line 374715
    :pswitch_5
    new-instance v0, LX/1Cr;

    invoke-direct {v0}, LX/1Cr;-><init>()V

    .line 374716
    move-object v0, v0

    .line 374717
    move-object v0, v0

    .line 374718
    goto :goto_0

    .line 374719
    :pswitch_6
    new-instance v0, LX/F0C;

    invoke-direct {v0}, LX/F0C;-><init>()V

    .line 374720
    move-object v0, v0

    .line 374721
    move-object v0, v0

    .line 374722
    goto :goto_0

    .line 374723
    :pswitch_7
    new-instance v0, LX/ILq;

    invoke-direct {v0}, LX/ILq;-><init>()V

    .line 374724
    move-object v0, v0

    .line 374725
    move-object v0, v0

    .line 374726
    goto :goto_0

    .line 374727
    :pswitch_8
    new-instance v0, LX/DZA;

    invoke-direct {v0}, LX/DZA;-><init>()V

    .line 374728
    move-object v0, v0

    .line 374729
    move-object v0, v0

    .line 374730
    goto :goto_0

    .line 374731
    :pswitch_9
    new-instance v0, LX/F9Z;

    invoke-direct {v0}, LX/F9Z;-><init>()V

    .line 374732
    move-object v0, v0

    .line 374733
    move-object v0, v0

    .line 374734
    goto :goto_0

    .line 374735
    :pswitch_a
    new-instance v0, LX/F9a;

    invoke-direct {v0}, LX/F9a;-><init>()V

    .line 374736
    move-object v0, v0

    .line 374737
    move-object v0, v0

    .line 374738
    goto :goto_0

    .line 374739
    :pswitch_b
    new-instance v0, LX/Gn0;

    invoke-direct {v0}, LX/Gn0;-><init>()V

    .line 374740
    move-object v0, v0

    .line 374741
    move-object v0, v0

    .line 374742
    goto :goto_0

    .line 374743
    :pswitch_c
    invoke-static {p1}, LX/119;->a(LX/0QB;)LX/119;

    move-result-object v0

    goto :goto_0

    .line 374744
    :pswitch_d
    new-instance v0, LX/Ie9;

    invoke-direct {v0}, LX/Ie9;-><init>()V

    .line 374745
    move-object v0, v0

    .line 374746
    move-object v0, v0

    .line 374747
    goto :goto_0

    .line 374748
    :pswitch_e
    invoke-static {p1}, LX/FJV;->a(LX/0QB;)LX/FJV;

    move-result-object v0

    goto :goto_0

    .line 374749
    :pswitch_f
    new-instance v0, LX/H88;

    invoke-direct {v0}, LX/H88;-><init>()V

    .line 374750
    move-object v0, v0

    .line 374751
    move-object v0, v0

    .line 374752
    goto :goto_0

    .line 374753
    :pswitch_10
    new-instance v0, LX/JwY;

    invoke-direct {v0}, LX/JwY;-><init>()V

    .line 374754
    move-object v0, v0

    .line 374755
    move-object v0, v0

    .line 374756
    goto :goto_0

    .line 374757
    :pswitch_11
    new-instance v0, LX/2d4;

    invoke-direct {v0}, LX/2d4;-><init>()V

    .line 374758
    move-object v0, v0

    .line 374759
    move-object v0, v0

    .line 374760
    goto :goto_0

    .line 374761
    :pswitch_12
    new-instance v0, LX/75V;

    invoke-direct {v0}, LX/75V;-><init>()V

    .line 374762
    move-object v0, v0

    .line 374763
    move-object v0, v0

    .line 374764
    goto/16 :goto_0

    .line 374765
    :pswitch_13
    invoke-static {p1}, LX/2bs;->a(LX/0QB;)LX/2bs;

    move-result-object v0

    goto/16 :goto_0

    .line 374766
    :pswitch_14
    new-instance v0, LX/K2l;

    invoke-direct {v0}, LX/K2l;-><init>()V

    .line 374767
    move-object v0, v0

    .line 374768
    move-object v0, v0

    .line 374769
    goto/16 :goto_0

    .line 374770
    :pswitch_15
    new-instance v0, LX/7Uj;

    invoke-direct {v0}, LX/7Uj;-><init>()V

    .line 374771
    move-object v0, v0

    .line 374772
    move-object v0, v0

    .line 374773
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 374774
    const/16 v0, 0x16

    return v0
.end method
