.class public final LX/37i;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/37j;

.field public static final d:Z


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/38W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 501956
    const-string v0, "MediaRouter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, LX/37i;->d:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 502060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 502061
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/37i;->c:Ljava/util/ArrayList;

    .line 502062
    iput-object p1, p0, LX/37i;->b:Landroid/content/Context;

    .line 502063
    return-void
.end method

.method public static a(Landroid/content/Context;)LX/37i;
    .locals 6
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 502031
    if-nez p0, :cond_0

    .line 502032
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502033
    :cond_0
    invoke-static {}, LX/37i;->b()V

    .line 502034
    sget-object v0, LX/37i;->a:LX/37j;

    if-nez v0, :cond_1

    .line 502035
    new-instance v0, LX/37j;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/37j;-><init>(Landroid/content/Context;)V

    .line 502036
    sput-object v0, LX/37i;->a:LX/37j;

    .line 502037
    new-instance v1, LX/38P;

    iget-object v2, v0, LX/37j;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, LX/38P;-><init>(Landroid/content/Context;LX/37k;)V

    iput-object v1, v0, LX/37j;->l:LX/38P;

    .line 502038
    iget-object v1, v0, LX/37j;->l:LX/38P;

    .line 502039
    iget-boolean v2, v1, LX/38P;->f:Z

    if-nez v2, :cond_1

    .line 502040
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/38P;->f:Z

    .line 502041
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 502042
    const-string v3, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 502043
    const-string v3, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 502044
    const-string v3, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 502045
    const-string v3, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 502046
    const-string v3, "android.intent.action.PACKAGE_RESTARTED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 502047
    const-string v3, "package"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 502048
    iget-object v3, v1, LX/38P;->a:Landroid/content/Context;

    iget-object v4, v1, LX/38P;->g:Landroid/content/BroadcastReceiver;

    const/4 v5, 0x0

    iget-object v0, v1, LX/38P;->c:Landroid/os/Handler;

    invoke-virtual {v3, v4, v2, v5, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 502049
    iget-object v2, v1, LX/38P;->c:Landroid/os/Handler;

    iget-object v3, v1, LX/38P;->h:Ljava/lang/Runnable;

    const v4, 0xb294993

    invoke-static {v2, v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 502050
    :cond_1
    sget-object v0, LX/37i;->a:LX/37j;

    .line 502051
    iget-object v1, v0, LX/37j;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_0
    add-int/lit8 v2, v1, -0x1

    if-ltz v2, :cond_3

    .line 502052
    iget-object v1, v0, LX/37j;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/37i;

    .line 502053
    if-nez v1, :cond_2

    .line 502054
    iget-object v1, v0, LX/37j;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v1, v2

    goto :goto_0

    .line 502055
    :cond_2
    iget-object v3, v1, LX/37i;->b:Landroid/content/Context;

    if-ne v3, p0, :cond_4

    .line 502056
    :goto_1
    move-object v0, v1

    .line 502057
    return-object v0

    .line 502058
    :cond_3
    new-instance v1, LX/37i;

    invoke-direct {v1, p0}, LX/37i;-><init>(Landroid/content/Context;)V

    .line 502059
    iget-object v2, v0, LX/37j;->b:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public static a()LX/384;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 502029
    invoke-static {}, LX/37i;->b()V

    .line 502030
    sget-object v0, LX/37i;->a:LX/37j;

    invoke-virtual {v0}, LX/37j;->b()LX/384;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/384;)V
    .locals 2
    .param p0    # LX/384;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 502022
    if-nez p0, :cond_0

    .line 502023
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "route must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502024
    :cond_0
    invoke-static {}, LX/37i;->b()V

    .line 502025
    sget-boolean v0, LX/37i;->d:Z

    if-eqz v0, :cond_1

    .line 502026
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "selectRoute: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502027
    :cond_1
    sget-object v0, LX/37i;->a:LX/37j;

    invoke-virtual {v0, p0}, LX/37j;->a(LX/384;)V

    .line 502028
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;)Z"
        }
    .end annotation

    .prologue
    .line 502021
    if-eq p0, p1, :cond_0

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 502018
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 502019
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The media router service must only be accessed on the application\'s main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502020
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/38T;LX/38V;I)V
    .locals 8
    .param p1    # LX/38T;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # LX/38V;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 501957
    if-nez p1, :cond_0

    .line 501958
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 501959
    :cond_0
    if-nez p2, :cond_1

    .line 501960
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 501961
    :cond_1
    invoke-static {}, LX/37i;->b()V

    .line 501962
    sget-boolean v0, LX/37i;->d:Z

    if-eqz v0, :cond_2

    .line 501963
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "addCallback: selector="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", callback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 501964
    :cond_2
    iget-object v0, p0, LX/37i;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 501965
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_e

    .line 501966
    iget-object v0, p0, LX/37i;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38W;

    iget-object v0, v0, LX/38W;->b:LX/38V;

    if-ne v0, p2, :cond_d

    move v0, v1

    .line 501967
    :goto_1
    move v0, v0

    .line 501968
    if-gez v0, :cond_b

    .line 501969
    new-instance v0, LX/38W;

    invoke-direct {v0, p0, p2}, LX/38W;-><init>(LX/37i;LX/38V;)V

    .line 501970
    iget-object v1, p0, LX/37i;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 501971
    :goto_2
    const/4 v1, 0x0

    .line 501972
    iget v3, v0, LX/38W;->d:I

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v3, p3

    if-eqz v3, :cond_3

    .line 501973
    iget v1, v0, LX/38W;->d:I

    or-int/2addr v1, p3

    iput v1, v0, LX/38W;->d:I

    move v1, v2

    .line 501974
    :cond_3
    iget-object v3, v0, LX/38W;->c:LX/38T;

    .line 501975
    if-eqz p1, :cond_f

    .line 501976
    invoke-static {v3}, LX/38T;->e(LX/38T;)V

    .line 501977
    invoke-static {p1}, LX/38T;->e(LX/38T;)V

    .line 501978
    iget-object p0, v3, LX/38T;->c:Ljava/util/List;

    iget-object p2, p1, LX/38T;->c:Ljava/util/List;

    invoke-interface {p0, p2}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result p0

    .line 501979
    :goto_3
    move v3, p0

    .line 501980
    if-nez v3, :cond_c

    .line 501981
    new-instance v1, LX/38R;

    iget-object v3, v0, LX/38W;->c:LX/38T;

    invoke-direct {v1, v3}, LX/38R;-><init>(LX/38T;)V

    invoke-virtual {v1, p1}, LX/38R;->a(LX/38T;)LX/38R;

    move-result-object v1

    invoke-virtual {v1}, LX/38R;->a()LX/38T;

    move-result-object v1

    iput-object v1, v0, LX/38W;->c:LX/38T;

    .line 501982
    :goto_4
    if-eqz v2, :cond_a

    .line 501983
    sget-object v0, LX/37i;->a:LX/37j;

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 501984
    new-instance p1, LX/38R;

    invoke-direct {p1}, LX/38R;-><init>()V

    .line 501985
    iget-object v1, v0, LX/37j;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v3, v6

    move v5, v6

    :goto_5
    add-int/lit8 p0, v1, -0x1

    if-ltz p0, :cond_9

    .line 501986
    iget-object v1, v0, LX/37j;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/37i;

    .line 501987
    if-nez v1, :cond_4

    .line 501988
    iget-object v1, v0, LX/37j;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v1, p0

    goto :goto_5

    .line 501989
    :cond_4
    iget-object v2, v1, LX/37i;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result p2

    move v7, v6

    .line 501990
    :goto_6
    if-ge v7, p2, :cond_8

    .line 501991
    iget-object v2, v1, LX/37i;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/38W;

    .line 501992
    iget-object p3, v2, LX/38W;->c:LX/38T;

    invoke-virtual {p1, p3}, LX/38R;->a(LX/38T;)LX/38R;

    .line 501993
    iget p3, v2, LX/38W;->d:I

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_5

    move v3, v4

    move v5, v4

    .line 501994
    :cond_5
    iget p3, v2, LX/38W;->d:I

    and-int/lit8 p3, p3, 0x4

    if-eqz p3, :cond_6

    .line 501995
    iget-boolean p3, v0, LX/37j;->k:Z

    if-nez p3, :cond_6

    move v5, v4

    .line 501996
    :cond_6
    iget v2, v2, LX/38W;->d:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_7

    move v5, v4

    .line 501997
    :cond_7
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_6

    :cond_8
    move v1, p0

    .line 501998
    goto :goto_5

    .line 501999
    :cond_9
    if-eqz v5, :cond_10

    invoke-virtual {p1}, LX/38R;->a()LX/38T;

    move-result-object v1

    .line 502000
    :goto_7
    iget-object v2, v0, LX/37j;->p:LX/38A;

    if-eqz v2, :cond_11

    iget-object v2, v0, LX/37j;->p:LX/38A;

    invoke-virtual {v2}, LX/38A;->a()LX/38T;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/38T;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    iget-object v2, v0, LX/37j;->p:LX/38A;

    invoke-virtual {v2}, LX/38A;->b()Z

    move-result v2

    if-ne v2, v3, :cond_11

    .line 502001
    :cond_a
    return-void

    .line 502002
    :cond_b
    iget-object v1, p0, LX/37i;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38W;

    goto/16 :goto_2

    :cond_c
    move v2, v1

    goto/16 :goto_4

    .line 502003
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 502004
    :cond_e
    const/4 v0, -0x1

    goto/16 :goto_1

    :cond_f
    const/4 p0, 0x0

    goto/16 :goto_3

    .line 502005
    :cond_10
    sget-object v1, LX/38T;->a:LX/38T;

    goto :goto_7

    .line 502006
    :cond_11
    invoke-static {v1}, LX/38T;->e(LX/38T;)V

    .line 502007
    iget-object v2, v1, LX/38T;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    move v2, v2

    .line 502008
    if-eqz v2, :cond_13

    if-nez v3, :cond_13

    .line 502009
    iget-object v1, v0, LX/37j;->p:LX/38A;

    if-eqz v1, :cond_a

    .line 502010
    const/4 v1, 0x0

    iput-object v1, v0, LX/37j;->p:LX/38A;

    .line 502011
    :goto_8
    sget-boolean v1, LX/37i;->d:Z

    if-eqz v1, :cond_12

    .line 502012
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updated discovery request: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, LX/37j;->p:LX/38A;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502013
    :cond_12
    iget-object v1, v0, LX/37j;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v6

    .line 502014
    :goto_9
    if-ge v2, v3, :cond_a

    .line 502015
    iget-object v1, v0, LX/37j;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/380;

    iget-object v1, v1, LX/380;->a:LX/37v;

    iget-object v4, v0, LX/37j;->p:LX/38A;

    invoke-virtual {v1, v4}, LX/37v;->a(LX/38A;)V

    .line 502016
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_9

    .line 502017
    :cond_13
    new-instance v2, LX/38A;

    invoke-direct {v2, v1, v3}, LX/38A;-><init>(LX/38T;Z)V

    iput-object v2, v0, LX/37j;->p:LX/38A;

    goto :goto_8
.end method
