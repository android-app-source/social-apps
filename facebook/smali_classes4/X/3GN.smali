.class public LX/3GN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3GN;


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 540966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 540967
    return-void
.end method

.method public static a(LX/0QB;)LX/3GN;
    .locals 3

    .prologue
    .line 540968
    sget-object v0, LX/3GN;->b:LX/3GN;

    if-nez v0, :cond_1

    .line 540969
    const-class v1, LX/3GN;

    monitor-enter v1

    .line 540970
    :try_start_0
    sget-object v0, LX/3GN;->b:LX/3GN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 540971
    if-eqz v2, :cond_0

    .line 540972
    :try_start_1
    new-instance v0, LX/3GN;

    invoke-direct {v0}, LX/3GN;-><init>()V

    .line 540973
    move-object v0, v0

    .line 540974
    sput-object v0, LX/3GN;->b:LX/3GN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 540975
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 540976
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 540977
    :cond_1
    sget-object v0, LX/3GN;->b:LX/3GN;

    return-object v0

    .line 540978
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 540979
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/EGJ;
    .locals 3

    .prologue
    .line 540980
    iget-object v0, p0, LX/3GN;->a:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3GN;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 540981
    :cond_0
    const/4 v0, 0x0

    .line 540982
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/EGJ;

    new-instance v1, LX/EGI;

    iget-object v2, p0, LX/3GN;->a:LX/0Px;

    invoke-direct {v1, v2}, LX/EGI;-><init>(Ljava/util/List;)V

    invoke-direct {v0, v1}, LX/EGJ;-><init>(LX/EGI;)V

    goto :goto_0
.end method
