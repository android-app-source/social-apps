.class public LX/2iz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile o:LX/2iz;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/2jY;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0SG;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0jo;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/2j0;

.field public final e:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field public final f:LX/2j6;

.field public final g:LX/2jN;

.field private final h:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final i:LX/2j3;

.field private final j:LX/2j8;

.field private final k:LX/2j5;

.field private final l:LX/0Yb;

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;LX/0Xl;LX/0Ot;LX/2j0;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/2j2;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/2j3;LX/2j4;LX/2j5;LX/0Ot;)V
    .locals 3
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Xl;",
            "LX/0Ot",
            "<",
            "LX/0jo;",
            ">;",
            "LX/2j0;",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            "LX/2j2;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/2j3;",
            "LX/2j4;",
            "LX/2j5;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 452401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 452402
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2iz;->a:Ljava/util/Map;

    .line 452403
    const/4 v0, 0x0

    iput-object v0, p0, LX/2iz;->n:Ljava/lang/ref/WeakReference;

    .line 452404
    iput-object p1, p0, LX/2iz;->b:LX/0SG;

    .line 452405
    iput-object p3, p0, LX/2iz;->c:LX/0Ot;

    .line 452406
    iput-object p4, p0, LX/2iz;->d:LX/2j0;

    .line 452407
    iput-object p5, p0, LX/2iz;->e:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 452408
    new-instance v2, LX/2j6;

    invoke-static {p6}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p6}, LX/2j3;->a(LX/0QB;)LX/2j3;

    move-result-object v1

    check-cast v1, LX/2j3;

    const/16 p1, 0x3095

    invoke-static {p6, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-direct {v2, p0, v0, v1, p1}, LX/2j6;-><init>(LX/2iz;LX/0SG;LX/2j3;LX/0Ot;)V

    .line 452409
    move-object v0, v2

    .line 452410
    iput-object v0, p0, LX/2iz;->f:LX/2j6;

    .line 452411
    iput-object p7, p0, LX/2iz;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 452412
    iput-object p8, p0, LX/2iz;->i:LX/2j3;

    .line 452413
    new-instance v0, LX/2j7;

    invoke-direct {v0, p0}, LX/2j7;-><init>(LX/2iz;)V

    .line 452414
    new-instance v2, LX/2j8;

    invoke-static {p9}, LX/1vi;->a(LX/0QB;)LX/1vi;

    move-result-object v1

    check-cast v1, LX/1vi;

    invoke-direct {v2, v0, v1}, LX/2j8;-><init>(LX/2j7;LX/1vi;)V

    .line 452415
    move-object v0, v2

    .line 452416
    iput-object v0, p0, LX/2iz;->j:LX/2j8;

    .line 452417
    iput-object p10, p0, LX/2iz;->k:LX/2j5;

    .line 452418
    iput-object p11, p0, LX/2iz;->m:LX/0Ot;

    .line 452419
    new-instance v0, LX/2jN;

    invoke-direct {v0, p0}, LX/2jN;-><init>(LX/2iz;)V

    iput-object v0, p0, LX/2iz;->g:LX/2jN;

    .line 452420
    invoke-interface {p2}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.STREAM_PUBLISH_START"

    iget-object v2, p0, LX/2iz;->g:LX/2jN;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    iget-object v2, p0, LX/2iz;->g:LX/2jN;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2iz;->l:LX/0Yb;

    .line 452421
    return-void
.end method

.method public static a(LX/0QB;)LX/2iz;
    .locals 15

    .prologue
    .line 452422
    sget-object v0, LX/2iz;->o:LX/2iz;

    if-nez v0, :cond_1

    .line 452423
    const-class v1, LX/2iz;

    monitor-enter v1

    .line 452424
    :try_start_0
    sget-object v0, LX/2iz;->o:LX/2iz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 452425
    if-eqz v2, :cond_0

    .line 452426
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 452427
    new-instance v3, LX/2iz;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    const/16 v6, 0x28a

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    .line 452428
    new-instance v8, LX/2j0;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-direct {v8, v7}, LX/2j0;-><init>(LX/0SG;)V

    .line 452429
    move-object v7, v8

    .line 452430
    check-cast v7, LX/2j0;

    invoke-static {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v8

    check-cast v8, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    const-class v9, LX/2j2;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/2j2;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v10

    check-cast v10, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/2j3;->a(LX/0QB;)LX/2j3;

    move-result-object v11

    check-cast v11, LX/2j3;

    const-class v12, LX/2j4;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/2j4;

    invoke-static {v0}, LX/2j5;->a(LX/0QB;)LX/2j5;

    move-result-object v13

    check-cast v13, LX/2j5;

    const/16 v14, 0x259

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v3 .. v14}, LX/2iz;-><init>(LX/0SG;LX/0Xl;LX/0Ot;LX/2j0;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/2j2;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/2j3;LX/2j4;LX/2j5;LX/0Ot;)V

    .line 452431
    move-object v0, v3

    .line 452432
    sput-object v0, LX/2iz;->o:LX/2iz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 452433
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 452434
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 452435
    :cond_1
    sget-object v0, LX/2iz;->o:LX/2iz;

    return-object v0

    .line 452436
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 452437
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2iz;)V
    .locals 2

    .prologue
    .line 452438
    iget-object v0, p0, LX/2iz;->n:Ljava/lang/ref/WeakReference;

    .line 452439
    const/4 v1, 0x0

    iput-object v1, p0, LX/2iz;->n:Ljava/lang/ref/WeakReference;

    .line 452440
    if-eqz v0, :cond_0

    .line 452441
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 452442
    :cond_0
    return-void
.end method

.method public static a(LX/2iz;LX/2jY;JLjava/lang/Long;)V
    .locals 12

    .prologue
    .line 452443
    iget-object v1, p0, LX/2iz;->i:LX/2j3;

    invoke-virtual {p1}, LX/2jY;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, LX/2jY;->v()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/2jY;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, LX/2jY;->c()LX/8Y9;

    move-result-object v5

    const-wide/16 v8, 0xfa0

    move-wide v6, p2

    move-object/from16 v10, p4

    invoke-virtual/range {v1 .. v10}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8Y9;JJLjava/lang/Long;)V

    .line 452444
    return-void
.end method

.method public static b(LX/2jY;)Landroid/content/Intent;
    .locals 10
    .param p0    # LX/2jY;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 452474
    invoke-virtual {p0}, LX/2jY;->A()Z

    move-result v0

    if-nez v0, :cond_0

    .line 452475
    iget-wide v8, p0, LX/2jY;->i:J

    move-wide v4, v8

    .line 452476
    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 452477
    :cond_0
    iget-wide v8, p0, LX/2jY;->j:J

    move-wide v4, v8

    .line 452478
    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 452479
    iget-wide v8, p0, LX/2jY;->k:J

    move-wide v4, v8

    .line 452480
    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    :goto_2
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 452481
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "reaction_session_id"

    .line 452482
    iget-object v2, p0, LX/2jY;->a:Ljava/lang/String;

    move-object v2, v2

    .line 452483
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    .line 452484
    goto :goto_0

    :cond_2
    move v0, v2

    .line 452485
    goto :goto_1

    :cond_3
    move v1, v2

    .line 452486
    goto :goto_2
.end method

.method private b(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 9
    .param p3    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 452445
    iget-object v0, p0, LX/2iz;->i:LX/2j3;

    .line 452446
    iget-object v1, p1, LX/2jY;->a:Ljava/lang/String;

    move-object v1, v1

    .line 452447
    iget-object v2, p1, LX/2jY;->b:Ljava/lang/String;

    move-object v2, v2

    .line 452448
    invoke-virtual {p1}, LX/2jY;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/2jY;->c()LX/8Y9;

    move-result-object v4

    move-object v5, p2

    move-object v6, p3

    .line 452449
    iget-object v7, v0, LX/2j3;->a:LX/0Zb;

    invoke-static/range {v1 .. v6}, LX/2j3;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8Y9;Ljava/lang/String;Ljava/lang/Long;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    invoke-interface {v7, v8}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 452450
    return-void
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 452451
    iget-object v0, p0, LX/2iz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LX/2jY;)V
    .locals 3

    .prologue
    .line 452452
    iget-object v0, p0, LX/2iz;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 452453
    iput-wide v0, p1, LX/2jY;->k:J

    .line 452454
    return-void
.end method


# virtual methods
.method public final a(LX/2jY;)V
    .locals 4

    .prologue
    .line 452455
    iget-object v0, p0, LX/2iz;->n:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 452456
    :goto_0
    if-eqz v0, :cond_2

    .line 452457
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mRemoving:Z

    move v1, v1

    .line 452458
    if-nez v1, :cond_2

    .line 452459
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v1, v1

    .line 452460
    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 452461
    if-nez v1, :cond_1

    .line 452462
    const-string v0, "NO_PARENT_FRAGMENT"

    invoke-virtual {p1}, LX/2jY;->r()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, LX/2iz;->b(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452463
    invoke-static {p0}, LX/2iz;->a(LX/2iz;)V

    .line 452464
    iget-object v0, p1, LX/2jY;->a:Ljava/lang/String;

    move-object v0, v0

    .line 452465
    invoke-virtual {p0, v0}, LX/2iz;->g(Ljava/lang/String;)LX/2jY;

    .line 452466
    :goto_2
    return-void

    .line 452467
    :cond_0
    iget-object v0, p0, LX/2iz;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    goto :goto_0

    .line 452468
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    .line 452469
    invoke-static {p1}, LX/2iz;->b(LX/2jY;)Landroid/content/Intent;

    move-result-object v3

    .line 452470
    iget-object v2, p0, LX/2iz;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jo;

    sget-object v0, LX/0cQ;->REACTION_DIALOG_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    invoke-interface {v2, v0}, LX/0jo;->a(I)LX/0jq;

    move-result-object v2

    invoke-interface {v2, v3}, LX/0jq;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 452471
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v3

    const-string v0, "chromeless:content:fragment:tag"

    invoke-virtual {v3, v2, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v2

    invoke-virtual {v2}, LX/0hH;->c()I

    .line 452472
    invoke-static {p0}, LX/2iz;->a(LX/2iz;)V

    .line 452473
    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1
    .param p3    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 452392
    invoke-direct {p0, p1, p2, p3}, LX/2iz;->b(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452393
    invoke-static {p0}, LX/2iz;->a(LX/2iz;)V

    .line 452394
    iget-object v0, p1, LX/2jY;->a:Ljava/lang/String;

    move-object v0, v0

    .line 452395
    invoke-virtual {p0, v0}, LX/2iz;->g(Ljava/lang/String;)LX/2jY;

    .line 452396
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 452397
    invoke-virtual {p0, p1}, LX/2iz;->g(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    .line 452398
    if-eqz v0, :cond_0

    .line 452399
    const-string v1, "POST_ABORTED"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, LX/2iz;->b(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452400
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/CfG;)V
    .locals 3

    .prologue
    .line 452375
    invoke-virtual {p0, p1}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    .line 452376
    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 452377
    :cond_0
    :goto_0
    return-void

    .line 452378
    :cond_1
    invoke-virtual {v0, p2}, LX/2jY;->a(LX/CfG;)V

    .line 452379
    iget-object v1, v0, LX/2jY;->m:Ljava/lang/String;

    move-object v1, v1

    .line 452380
    if-eqz v1, :cond_2

    .line 452381
    iget-object v1, v0, LX/2jY;->m:Ljava/lang/String;

    move-object v1, v1

    .line 452382
    iget-object v2, p0, LX/2iz;->e:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v2, p1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v2

    invoke-interface {p2, v1, v2}, LX/CfG;->a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V

    .line 452383
    const/4 v1, 0x0

    .line 452384
    iput-object v1, v0, LX/2jY;->m:Ljava/lang/String;

    .line 452385
    :cond_2
    invoke-virtual {v0}, LX/2jY;->A()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 452386
    iget-boolean v1, v0, LX/2jY;->n:Z

    move v1, v1

    .line 452387
    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/2jY;->y()Z

    move-result v1

    if-nez v1, :cond_0

    .line 452388
    iget-object v1, v0, LX/2jY;->A:Ljava/lang/Runnable;

    move-object v1, v1

    .line 452389
    if-eqz v1, :cond_0

    .line 452390
    iget-object v1, v0, LX/2jY;->A:Ljava/lang/Runnable;

    move-object v0, v1

    .line 452391
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/base/fragment/FbFragment;)Z
    .locals 1

    .prologue
    .line 452371
    invoke-virtual {p0, p1}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    .line 452372
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/2jY;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 452373
    :cond_0
    const/4 v0, 0x0

    .line 452374
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1, p2}, LX/2iz;->b(Ljava/lang/String;Lcom/facebook/base/fragment/FbFragment;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/base/fragment/FbFragment;Ljava/lang/Runnable;Landroid/os/Bundle;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 452360
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 452361
    invoke-virtual {p0, p1}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    .line 452362
    if-nez v0, :cond_1

    .line 452363
    :goto_1
    return v1

    :cond_0
    move v0, v1

    .line 452364
    goto :goto_0

    .line 452365
    :cond_1
    invoke-virtual {v0}, LX/2jY;->A()Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Non-on-demand surface: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 452366
    iget-object v3, v0, LX/2jY;->b:Ljava/lang/String;

    move-object v3, v3

    .line 452367
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 452368
    iput-object p4, v0, LX/2jY;->x:Landroid/os/Bundle;

    .line 452369
    iput-object p3, v0, LX/2jY;->A:Ljava/lang/Runnable;

    .line 452370
    invoke-virtual {p0, p1, p2}, LX/2iz;->b(Ljava/lang/String;Lcom/facebook/base/fragment/FbFragment;)Z

    move-result v1

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)LX/2jY;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 452357
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 452358
    const/4 v0, 0x0

    .line 452359
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2iz;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2jY;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)LX/2jY;
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 452347
    invoke-direct {p0}, LX/2iz;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 452348
    iget-object v0, p0, LX/2iz;->j:LX/2j8;

    .line 452349
    iget-object v1, v0, LX/2j8;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v1, v0, LX/2j8;->a:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0b2;

    .line 452350
    iget-object v4, v0, LX/2j8;->b:LX/1vi;

    invoke-virtual {v4, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 452351
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 452352
    :cond_0
    iget-object v0, p0, LX/2iz;->l:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 452353
    iget-object v0, p0, LX/2iz;->l:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 452354
    :cond_1
    new-instance v0, LX/2jY;

    invoke-direct {v0, p1, p2}, LX/2jY;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 452355
    iget-object v1, p0, LX/2iz;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452356
    return-object v0
.end method

.method public final b(Ljava/lang/String;Lcom/facebook/base/fragment/FbFragment;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 452323
    invoke-virtual {p0, p1}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v2

    .line 452324
    if-nez v2, :cond_0

    .line 452325
    :goto_0
    return v0

    .line 452326
    :cond_0
    invoke-virtual {v2}, LX/2jY;->A()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 452327
    iget-object v3, v2, LX/2jY;->A:Ljava/lang/Runnable;

    move-object v3, v3

    .line 452328
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No runnable for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 452329
    iget-object v5, v2, LX/2jY;->b:Ljava/lang/String;

    move-object v5, v5

    .line 452330
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452331
    :cond_1
    invoke-direct {p0, v2}, LX/2iz;->c(LX/2jY;)V

    .line 452332
    iget-boolean v3, v2, LX/2jY;->n:Z

    move v3, v3

    .line 452333
    if-nez v3, :cond_3

    invoke-virtual {v2}, LX/2jY;->y()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2}, LX/2jY;->A()Z

    move-result v3

    if-nez v3, :cond_3

    .line 452334
    iget-object v1, v2, LX/2jY;->e:Ljava/lang/String;

    move-object v1, v1

    .line 452335
    if-eqz v1, :cond_2

    .line 452336
    iget-object v1, v2, LX/2jY;->e:Ljava/lang/String;

    move-object v1, v1

    .line 452337
    invoke-virtual {v2}, LX/2jY;->G()Ljava/lang/Long;

    move-result-object v3

    invoke-direct {p0, v2, v1, v3}, LX/2iz;->b(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452338
    :cond_2
    invoke-virtual {p0, p1}, LX/2iz;->g(Ljava/lang/String;)LX/2jY;

    goto :goto_0

    .line 452339
    :cond_3
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2iz;->n:Ljava/lang/ref/WeakReference;

    .line 452340
    invoke-virtual {v2}, LX/2jY;->y()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 452341
    invoke-virtual {p0, v2}, LX/2iz;->a(LX/2jY;)V

    :goto_1
    move v0, v1

    .line 452342
    goto :goto_0

    .line 452343
    :cond_4
    invoke-virtual {v2}, LX/2jY;->A()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 452344
    invoke-virtual {p0, v2}, LX/2iz;->a(LX/2jY;)V

    goto :goto_1

    .line 452345
    :cond_5
    iput-boolean v1, v2, LX/2jY;->l:Z

    .line 452346
    goto :goto_1
.end method

.method public final c(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 452317
    invoke-virtual {p0, p1}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    .line 452318
    if-nez v0, :cond_0

    .line 452319
    :goto_0
    return-void

    .line 452320
    :cond_0
    iget-object v1, p0, LX/2iz;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 452321
    iput-wide v2, v0, LX/2jY;->j:J

    .line 452322
    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 452313
    invoke-virtual {p0, p1}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    .line 452314
    if-eqz v0, :cond_0

    .line 452315
    invoke-direct {p0, v0}, LX/2iz;->c(LX/2jY;)V

    .line 452316
    :cond_0
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 452306
    invoke-virtual {p0, p1}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    .line 452307
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/2jY;->A()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 452308
    iget-object v1, v0, LX/2jY;->A:Ljava/lang/Runnable;

    move-object v1, v1

    .line 452309
    if-eqz v1, :cond_0

    .line 452310
    iget-object v1, v0, LX/2jY;->A:Ljava/lang/Runnable;

    move-object v0, v1

    .line 452311
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 452312
    :cond_0
    return-void
.end method

.method public final g(Ljava/lang/String;)LX/2jY;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 452285
    iget-object v0, p0, LX/2iz;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1e0002

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 452286
    iget-object v0, p0, LX/2iz;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1e000a

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 452287
    iget-object v0, p0, LX/2iz;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1e0004

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 452288
    iget-object v0, p0, LX/2iz;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2jY;

    .line 452289
    iget-object v1, p0, LX/2iz;->g:LX/2jN;

    .line 452290
    iget-object v2, v1, LX/2jN;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 452291
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 452292
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 452293
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 452294
    :cond_1
    if-eqz v0, :cond_2

    .line 452295
    iget-object v1, v0, LX/2jY;->c:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v2, v0, LX/2jY;->a:Ljava/lang/String;

    const v3, -0x71731878

    invoke-static {v1, v2, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 452296
    :cond_2
    invoke-direct {p0}, LX/2iz;->b()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, LX/2iz;->l:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 452297
    iget-object v1, p0, LX/2iz;->l:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 452298
    :cond_3
    invoke-direct {p0}, LX/2iz;->b()Z

    move-result v1

    if-nez v1, :cond_5

    .line 452299
    iget-object v1, p0, LX/2iz;->j:LX/2j8;

    .line 452300
    iget-object v2, v1, LX/2j8;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_4

    iget-object v2, v1, LX/2j8;->a:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0b2;

    .line 452301
    iget-object p1, v1, LX/2j8;->b:LX/1vi;

    invoke-virtual {p1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 452302
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 452303
    :cond_4
    iget-object v1, p0, LX/2iz;->k:LX/2j5;

    .line 452304
    iget-object v2, v1, LX/2j5;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 452305
    :cond_5
    return-object v0
.end method
