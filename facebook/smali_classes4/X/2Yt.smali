.class public final LX/2Yt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/orca/notify/MessagingNotificationHandler;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/orca/notify/MessagingNotificationHandler;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 421739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421740
    iput-object p1, p0, LX/2Yt;->a:LX/0QB;

    .line 421741
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/orca/notify/MessagingNotificationHandler;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 421742
    new-instance v0, LX/2Yt;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2Yt;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 421743
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 421744
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2Yt;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 421745
    packed-switch p2, :pswitch_data_0

    .line 421746
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 421747
    :pswitch_0
    invoke-static {p1}, LX/3R8;->a(LX/0QB;)LX/3R8;

    move-result-object v0

    .line 421748
    :goto_0
    return-object v0

    .line 421749
    :pswitch_1
    invoke-static {p1}, LX/3RG;->a(LX/0QB;)LX/3RG;

    move-result-object v0

    goto :goto_0

    .line 421750
    :pswitch_2
    invoke-static {p1}, LX/3S8;->a(LX/0QB;)LX/3S8;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 421751
    const/4 v0, 0x3

    return v0
.end method
