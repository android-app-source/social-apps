.class public final LX/3Hs;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;)V
    .locals 0

    .prologue
    .line 545173
    iput-object p1, p0, LX/3Hs;->a:Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 545174
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 545175
    check-cast p1, LX/2ou;

    .line 545176
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 545177
    iget-object v0, p0, LX/3Hs;->a:Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;

    invoke-virtual {v0}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->i()V

    .line 545178
    iget-object v0, p0, LX/3Hs;->a:Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;

    iget-object v1, p0, LX/3Hs;->a:Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;

    iget-object v1, v1, LX/3Ga;->a:LX/2pa;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->b(Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;LX/2pa;Z)V

    .line 545179
    iget-object v0, p0, LX/3Hs;->a:Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;

    invoke-static {v0}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->k(Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;)V

    .line 545180
    :cond_0
    return-void
.end method
