.class public LX/3Q0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Landroid/content/pm/PackageManager;

.field private final c:LX/1b4;

.field public final d:LX/17X;

.field private final e:LX/0xW;

.field public final f:LX/3Q1;

.field public final g:LX/0ad;

.field public final h:LX/17Y;

.field private final i:LX/0Uh;

.field private final j:LX/0gK;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/1b4;LX/17X;LX/0xW;LX/3Q1;LX/0ad;LX/17Y;LX/0Uh;LX/0gK;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562681
    iput-object p1, p0, LX/3Q0;->a:Landroid/content/Context;

    .line 562682
    iput-object p2, p0, LX/3Q0;->b:Landroid/content/pm/PackageManager;

    .line 562683
    iput-object p3, p0, LX/3Q0;->c:LX/1b4;

    .line 562684
    iput-object p4, p0, LX/3Q0;->d:LX/17X;

    .line 562685
    iput-object p5, p0, LX/3Q0;->e:LX/0xW;

    .line 562686
    iput-object p6, p0, LX/3Q0;->f:LX/3Q1;

    .line 562687
    iput-object p7, p0, LX/3Q0;->g:LX/0ad;

    .line 562688
    iput-object p8, p0, LX/3Q0;->h:LX/17Y;

    .line 562689
    iput-object p9, p0, LX/3Q0;->i:LX/0Uh;

    .line 562690
    iput-object p10, p0, LX/3Q0;->j:LX/0gK;

    .line 562691
    return-void
.end method

.method public static a(LX/0QB;)LX/3Q0;
    .locals 1

    .prologue
    .line 562679
    invoke-static {p0}, LX/3Q0;->b(LX/0QB;)LX/3Q0;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3Q0;
    .locals 11

    .prologue
    .line 562677
    new-instance v0, LX/3Q0;

    const-class v1, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageManager;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v3

    check-cast v3, LX/1b4;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v4

    check-cast v4, LX/17X;

    invoke-static {p0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v5

    check-cast v5, LX/0xW;

    invoke-static {p0}, LX/3Q1;->b(LX/0QB;)LX/3Q1;

    move-result-object v6

    check-cast v6, LX/3Q1;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v8

    check-cast v8, LX/17Y;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {p0}, LX/0gK;->b(LX/0QB;)LX/0gK;

    move-result-object v10

    check-cast v10, LX/0gK;

    invoke-direct/range {v0 .. v10}, LX/3Q0;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/1b4;LX/17X;LX/0xW;LX/3Q1;LX/0ad;LX/17Y;LX/0Uh;LX/0gK;)V

    .line 562678
    return-object v0
.end method

.method public static d(LX/3Q0;Lcom/facebook/notifications/model/SystemTrayNotification;)Z
    .locals 3

    .prologue
    .line 562674
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3Q0;->c:LX/1b4;

    .line 562675
    iget-object v1, v0, LX/1b4;->a:LX/0ad;

    sget-short v2, LX/1v6;->L:S

    const/4 p1, 0x0

    invoke-interface {v1, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 562676
    if-nez v0, :cond_0

    iget-object v0, p0, LX/3Q0;->c:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/notifications/constants/NotificationType;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 562622
    sget-object v1, LX/Jbi;->a:[I

    invoke-virtual {p1}, Lcom/facebook/notifications/constants/NotificationType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 562623
    :cond_0
    :goto_0
    return-object v0

    .line 562624
    :pswitch_0
    iget-object v0, p0, LX/3Q0;->g:LX/0ad;

    sget-short v1, LX/15r;->A:S

    invoke-interface {v0, v1, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 562625
    sget-object v0, LX/0ax;->cL:Ljava/lang/String;

    .line 562626
    :goto_1
    iget-object v1, p0, LX/3Q0;->d:LX/17X;

    iget-object v2, p0, LX/3Q0;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 562627
    :cond_1
    iget-object v0, p0, LX/3Q0;->e:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/0ax;->iA:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    sget-object v0, LX/0ax;->dd:Ljava/lang/String;

    goto :goto_1

    .line 562628
    :pswitch_1
    sget-object v0, LX/0ax;->fg:Ljava/lang/String;

    sget-object v1, LX/89v;->CONTINUOUS_SYNC:LX/89v;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 562629
    iget-object v1, p0, LX/3Q0;->d:LX/17X;

    iget-object v2, p0, LX/3Q0;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 562630
    :pswitch_2
    iget-object v0, p0, LX/3Q0;->e:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LX/0ax;->cW:Ljava/lang/String;

    sget-object v1, LX/5Oz;->FRIEND_REQUEST_TRAY_NOTIFICATION:LX/5Oz;

    invoke-virtual {v1}, LX/5Oz;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/5P0;->REQUESTS:LX/5P0;

    invoke-virtual {v2}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 562631
    :goto_2
    iget-object v1, p0, LX/3Q0;->d:LX/17X;

    iget-object v2, p0, LX/3Q0;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 562632
    iget-object v1, p0, LX/3Q0;->e:LX/0xW;

    invoke-virtual {v1}, LX/0xW;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 562633
    const-string v1, "target_tab_name"

    sget-object v2, Lcom/facebook/friending/tab/FriendRequestsTab;->l:Lcom/facebook/friending/tab/FriendRequestsTab;

    invoke-virtual {v2}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 562634
    :cond_3
    sget-object v0, LX/0ax;->da:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 562635
    :pswitch_3
    iget-object v1, p0, LX/3Q0;->f:LX/3Q1;

    invoke-virtual {v1}, LX/3Q1;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 562636
    invoke-static {p2}, Lcom/facebook/auth/login/LoginApprovalNotificationData;->a(Ljava/lang/String;)Lcom/facebook/auth/login/LoginApprovalNotificationData;

    move-result-object v1

    .line 562637
    if-eqz v1, :cond_0

    .line 562638
    iget-object v0, p0, LX/3Q0;->d:LX/17X;

    iget-object v2, p0, LX/3Q0;->a:Landroid/content/Context;

    sget-object v3, LX/0ax;->dm:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 562639
    const-string v2, "extra_login_approval_notification_data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 562640
    :cond_4
    :pswitch_4
    iget-object v0, p0, LX/3Q0;->d:LX/17X;

    iget-object v1, p0, LX/3Q0;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->dl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 562641
    :pswitch_5
    const-string v0, "/checkpoint/login_approvals"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/17X;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 562642
    iget-object v1, p0, LX/3Q0;->d:LX/17X;

    iget-object v2, p0, LX/3Q0;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 562643
    :pswitch_6
    iget-object v0, p0, LX/3Q0;->i:LX/0Uh;

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 562644
    iget-object v1, p0, LX/3Q0;->d:LX/17X;

    iget-object v2, p0, LX/3Q0;->a:Landroid/content/Context;

    if-eqz v0, :cond_5

    sget-object v0, LX/0ax;->eP:Ljava/lang/String;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PLACES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_NEAR_PLACE_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    invoke-static {v0, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v2, v0}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    const-string v0, "https://m.facebook.com/saved"

    goto :goto_3

    .line 562645
    :pswitch_7
    if-eqz p2, :cond_0

    .line 562646
    :try_start_0
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 562647
    sget-object v2, LX/0ax;->ds:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 562648
    if-eqz v1, :cond_0

    .line 562649
    iget-object v2, p0, LX/3Q0;->d:LX/17X;

    iget-object v3, p0, LX/3Q0;->a:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 562650
    :catch_0
    move-exception v1

    .line 562651
    const-string v2, "Fb4aPushNotificationIntentHelper"

    const-string v3, "Object id associated with notification was not long: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p2, v4, v5

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 562652
    :pswitch_8
    if-eqz p2, :cond_0

    .line 562653
    iget-object v0, p0, LX/3Q0;->d:LX/17X;

    iget-object v1, p0, LX/3Q0;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->bE:Ljava/lang/String;

    invoke-static {v2, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 562654
    :pswitch_9
    iget-object v0, p0, LX/3Q0;->d:LX/17X;

    iget-object v1, p0, LX/3Q0;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->gl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 562655
    :pswitch_a
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    goto/16 :goto_0

    .line 562656
    :pswitch_b
    iget-object v0, p0, LX/3Q0;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 562657
    iget-object v1, p0, LX/3Q0;->b:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 562658
    :pswitch_c
    if-eqz p2, :cond_0

    .line 562659
    iget-object v0, p0, LX/3Q0;->d:LX/17X;

    iget-object v1, p0, LX/3Q0;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->gv:Ljava/lang/String;

    const-string v3, "{id}"

    invoke-virtual {v2, v3, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_b
        :pswitch_c
        :pswitch_c
    .end packed-switch
.end method

.method public final b(Lcom/facebook/notifications/model/SystemTrayNotification;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 562660
    const/4 v0, 0x0

    .line 562661
    sget-object v1, LX/Jbi;->a:[I

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/constants/NotificationType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    sparse-switch v1, :sswitch_data_0

    .line 562662
    :cond_0
    :goto_0
    return-object v0

    .line 562663
    :sswitch_0
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->g()LX/0am;

    move-result-object v1

    .line 562664
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 562665
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 562666
    :sswitch_1
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->f()LX/0am;

    move-result-object v0

    .line 562667
    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 562668
    :sswitch_2
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->A()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 562669
    :sswitch_3
    iget-object v1, p0, LX/3Q0;->j:LX/0gK;

    invoke-virtual {v1}, LX/0gK;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "dt"

    invoke-virtual {p1, v1}, Lcom/facebook/notifications/model/SystemTrayNotification;->c(Ljava/lang/String;)LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 562670
    const-string v0, "dt"

    invoke-virtual {p1, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->c(Ljava/lang/String;)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 562671
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->g()LX/0am;

    move-result-object v1

    .line 562672
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 562673
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x8 -> :sswitch_0
        0x9 -> :sswitch_1
        0xe -> :sswitch_3
        0xf -> :sswitch_3
    .end sparse-switch
.end method
