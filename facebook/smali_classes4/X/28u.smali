.class public LX/28u;
.super LX/16B;
.source ""


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374928
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 374929
    iput-object p1, p0, LX/28u;->a:Landroid/content/Context;

    .line 374930
    return-void
.end method


# virtual methods
.method public final e()V
    .locals 4

    .prologue
    .line 374931
    iget-object v0, p0, LX/28u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    .line 374932
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->m:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 374933
    iget-object v2, v0, Lcom/facebook/katana/service/AppSession;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/1CA;->p:LX/0Tn;

    .line 374934
    iget-object p0, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->g:Ljava/lang/String;

    move-object p0, p0

    .line 374935
    invoke-interface {v2, v3, p0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 374936
    iget-object v2, v0, Lcom/facebook/katana/service/AppSession;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/1CA;->q:LX/0Tn;

    .line 374937
    iget-object p0, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, p0

    .line 374938
    invoke-static {v1}, LX/03l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v3, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 374939
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 374940
    invoke-static {v1}, LX/2Wp;->b(LX/0QB;)LX/2Wp;

    move-result-object v1

    check-cast v1, LX/2Wp;

    .line 374941
    const-string v2, "AUTH_LOGOUT"

    invoke-static {v1, v2}, LX/2Wp;->a(LX/2Wp;Ljava/lang/String;)V

    .line 374942
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->a:LX/0wb;

    invoke-virtual {v1}, LX/0wb;->b()V

    .line 374943
    return-void
.end method

.method public final h()V
    .locals 4

    .prologue
    .line 374944
    iget-object v0, p0, LX/28u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    .line 374945
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, LX/2W9;->a(Ljava/io/File;)Z

    .line 374946
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    invoke-static {v1}, Lcom/facebook/katana/service/AppSession;->e(Landroid/content/Context;)V

    .line 374947
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    invoke-static {v1}, LX/BWG;->a(Landroid/content/Context;)V

    .line 374948
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 374949
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    .line 374950
    invoke-static {v1}, LX/2Bb;->c(Landroid/content/Context;)LX/2Bb;

    move-result-object v2

    .line 374951
    iget-object v3, v2, LX/2Bb;->a:LX/2BN;

    .line 374952
    iget-object p0, v3, LX/2BN;->c:LX/2BL;

    const/4 v2, 0x0

    .line 374953
    iget-object v0, p0, LX/2BL;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/2BL;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 374954
    iget-object p0, v3, LX/2BN;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p0

    iget-object v0, v3, LX/2BN;->b:LX/0Tn;

    invoke-interface {p0, v0}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object p0

    invoke-interface {p0}, LX/0hN;->commit()V

    .line 374955
    invoke-static {}, LX/2Zg;->a()V

    .line 374956
    return-void
.end method

.method public final i()V
    .locals 9

    .prologue
    .line 374957
    iget-object v0, p0, LX/28u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    .line 374958
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/1CA;->r:LX/0Tn;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v1, v2, v3, v4}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 374959
    sget-object v1, LX/2A1;->STATUS_LOGGED_OUT:LX/2A1;

    invoke-static {v0, v1}, Lcom/facebook/katana/service/AppSession;->a$redex0(Lcom/facebook/katana/service/AppSession;LX/2A1;)V

    .line 374960
    new-instance v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-direct {v1}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;-><init>()V

    const/4 v2, 0x0

    .line 374961
    iput-object v2, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->c:Ljava/lang/String;

    .line 374962
    move-object v1, v1

    .line 374963
    const/4 v2, 0x1

    .line 374964
    iput v2, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->d:I

    .line 374965
    move-object v6, v1

    .line 374966
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    .line 374967
    invoke-static {v3}, LX/27F;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Landroid/content/ComponentName;

    .line 374968
    iget-boolean v1, v0, Lcom/facebook/katana/service/AppSession;->h:Z

    if-eqz v1, :cond_0

    .line 374969
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->s:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2c4;

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->AUTHENTICATION_FAILED:Lcom/facebook/notifications/constants/NotificationType;

    invoke-static {v3}, LX/BAa;->b(LX/0QB;)LX/BAa;

    move-result-object v3

    check-cast v3, LX/BAa;

    iget-object v5, v0, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    const v7, 0x7f0800e9

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/BAa;->a(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v3

    iget-object v5, v0, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0800c7

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/BAa;->d(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v3

    iget-object v5, v0, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    const v7, 0x7f0800e9

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/BAa;->c(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v3

    const v5, 0x108008a

    invoke-virtual {v3, v5}, LX/BAa;->a(I)LX/BAa;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, LX/BAa;->a(J)LX/BAa;

    move-result-object v3

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v5, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v4

    sget-object v5, LX/8D4;->ACTIVITY:LX/8D4;

    invoke-virtual/range {v1 .. v6}, LX/2c4;->a(Lcom/facebook/notifications/constants/NotificationType;LX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 374970
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/katana/service/AppSession;->h:Z

    .line 374971
    :cond_0
    return-void
.end method
