.class public LX/26A;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/26A;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "CacheableEntityRegistry.class"
    .end annotation
.end field


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final c:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "LX/1Rj;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/26B;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/os/Handler;

.field private final f:Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 371351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 371352
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/26A;->b:Ljava/util/Map;

    .line 371353
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, LX/26A;->c:Ljava/lang/ref/ReferenceQueue;

    .line 371354
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/26A;->d:Ljava/util/Set;

    .line 371355
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/26A;->e:Landroid/os/Handler;

    .line 371356
    new-instance v0, Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;

    iget-object v1, p0, LX/26A;->c:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0, p0, v1}, Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;-><init>(LX/26A;Ljava/lang/ref/ReferenceQueue;)V

    iput-object v0, p0, LX/26A;->f:Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;

    .line 371357
    iget-object v0, p0, LX/26A;->f:Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;

    invoke-virtual {v0}, Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;->start()V

    .line 371358
    return-void
.end method

.method public static declared-synchronized a()LX/26A;
    .locals 2

    .prologue
    .line 371347
    const-class v1, LX/26A;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/26A;->a:LX/26A;

    if-nez v0, :cond_0

    .line 371348
    new-instance v0, LX/26A;

    invoke-direct {v0}, LX/26A;-><init>()V

    sput-object v0, LX/26A;->a:LX/26A;

    .line 371349
    :cond_0
    sget-object v0, LX/26A;->a:LX/26A;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 371350
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a$redex0(LX/26A;Ljava/lang/String;LX/1Pr;)V
    .locals 1

    .prologue
    .line 371359
    if-eqz p2, :cond_0

    .line 371360
    invoke-interface {p2, p1}, LX/1Pr;->b(Ljava/lang/String;)V

    .line 371361
    :cond_0
    monitor-enter p0

    .line 371362
    :try_start_0
    iget-object v0, p0, LX/26A;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 371363
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(LX/0jW;LX/1Pr;)V
    .locals 3

    .prologue
    .line 371318
    invoke-interface {p1}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v1

    .line 371319
    monitor-enter p0

    .line 371320
    :try_start_0
    iget-object v0, p0, LX/26A;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 371321
    iget-object v2, p0, LX/26A;->b:Ljava/util/Map;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371322
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371323
    iget-object v0, p0, LX/26A;->d:Ljava/util/Set;

    new-instance v1, LX/26B;

    iget-object v2, p0, LX/26A;->c:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1, p1, v2, p2}, LX/26B;-><init>(LX/0jW;Ljava/lang/ref/ReferenceQueue;LX/1Pr;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 371324
    iget-object v0, p0, LX/26A;->f:Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;

    .line 371325
    iget-boolean v1, v0, Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;->b:Z

    move v0, v1

    .line 371326
    if-nez v0, :cond_0

    .line 371327
    iget-object v0, p0, LX/26A;->f:Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;

    invoke-virtual {v0}, Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;->start()V

    .line 371328
    :cond_0
    return-void

    .line 371329
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 371330
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/26B;)V
    .locals 3

    .prologue
    .line 371331
    if-nez p1, :cond_0

    .line 371332
    :goto_0
    return-void

    .line 371333
    :cond_0
    iget-object v0, p0, LX/26A;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 371334
    monitor-enter p0

    .line 371335
    :try_start_0
    iget-object v0, p0, LX/26A;->b:Ljava/util/Map;

    iget-object v1, p1, LX/26B;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 371336
    if-nez v0, :cond_1

    .line 371337
    monitor-exit p0

    goto :goto_0

    .line 371338
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 371339
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 371340
    iget-object v0, p0, LX/26A;->e:Landroid/os/Handler;

    iget-object v1, p1, LX/26B;->a:Ljava/lang/String;

    iget-object v2, p1, LX/26B;->b:Ljava/lang/ref/WeakReference;

    .line 371341
    new-instance p1, Lcom/facebook/components/feed/CacheableEntityRegistry$1;

    invoke-direct {p1, p0, v1, v2}, Lcom/facebook/components/feed/CacheableEntityRegistry$1;-><init>(LX/26A;Ljava/lang/String;Ljava/lang/ref/WeakReference;)V

    move-object v1, p1

    .line 371342
    const v2, 0x32bc2b3d

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 371343
    :goto_1
    monitor-exit p0

    goto :goto_0

    .line 371344
    :cond_2
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 371345
    :try_start_2
    iget-object v1, p0, LX/26A;->b:Ljava/util/Map;

    iget-object v2, p1, LX/26B;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371346
    monitor-exit p0

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
