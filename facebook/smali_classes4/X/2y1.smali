.class public LX/2y1;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bsn;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 480253
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/2y1;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 480291
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 480292
    iput-object p1, p0, LX/2y1;->b:LX/0Ot;

    .line 480293
    return-void
.end method

.method public static a(LX/0QB;)LX/2y1;
    .locals 4

    .prologue
    .line 480280
    const-class v1, LX/2y1;

    monitor-enter v1

    .line 480281
    :try_start_0
    sget-object v0, LX/2y1;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 480282
    sput-object v2, LX/2y1;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 480283
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480284
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 480285
    new-instance v3, LX/2y1;

    const/16 p0, 0x1d3a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2y1;-><init>(LX/0Ot;)V

    .line 480286
    move-object v0, v3

    .line 480287
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 480288
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2y1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480289
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 480290
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 480294
    const v0, 0x79109a62

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 480268
    check-cast p2, LX/Bso;

    .line 480269
    iget-object v0, p0, LX/2y1;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;

    iget-object v1, p2, LX/Bso;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, -0x1

    const/4 p0, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x1

    .line 480270
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 480271
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 480272
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 480273
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 480274
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 480275
    iget-object v5, v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;->c:LX/1vg;

    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v5

    const v6, 0x7f0a00d5

    invoke-virtual {v5, v6}, LX/2xv;->j(I)LX/2xv;

    move-result-object v5

    const v6, 0x7f0207ed

    invoke-virtual {v5, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v5}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    .line 480276
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v7

    const v8, 0x7f0a00e9

    invoke-virtual {v7, v8}, LX/25Q;->i(I)LX/25Q;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    invoke-interface {v7, v10}, LX/1Di;->r(I)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v11}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    iget-object v8, v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;->a:LX/1nu;

    invoke-virtual {v8, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v8

    sget-object v9, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v8, v9}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v8

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v8, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/16 v8, 0x2a

    invoke-interface {v3, v8}, LX/1Di;->r(I)LX/1Di;

    move-result-object v3

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-interface {v3, v8}, LX/1Di;->b(F)LX/1Di;

    move-result-object v3

    invoke-interface {v7, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v11}, LX/1Dh;->T(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v10}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x6

    const/16 v9, 0x78

    invoke-interface {v7, v8, v9}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v10}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v7

    const/16 v8, 0x8

    invoke-interface {v7, v8, p0}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v10}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v8}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    const v8, 0x7f0b004f

    invoke-virtual {v4, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v10}, LX/1ne;->t(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->m(I)LX/1ne;

    move-result-object v4

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v4, v8}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v4

    sget-object v8, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v4, v8}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v7, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v11}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v11}, LX/1Dh;->T(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v10}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v2

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v8}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    const v8, 0x7f0b004c

    invoke-virtual {v2, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->m(I)LX/1ne;

    move-result-object v2

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v8}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    sget-object v8, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v2, v8}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v7, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, v10, v11}, LX/1Di;->h(II)LX/1Di;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 480277
    const v4, 0x79109a62

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 480278
    invoke-interface {v2, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v6, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v4, 0x7f0a00e9

    invoke-virtual {v3, v4}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v10}, LX/1Di;->r(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 480279
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 480254
    invoke-static {}, LX/1dS;->b()V

    .line 480255
    iget v0, p1, LX/1dQ;->b:I

    .line 480256
    packed-switch v0, :pswitch_data_0

    .line 480257
    :goto_0
    return-object v2

    .line 480258
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 480259
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 480260
    check-cast v1, LX/Bso;

    .line 480261
    iget-object v3, p0, LX/2y1;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;

    iget-object p1, v1, LX/Bso;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480262
    iget-object p2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 480263
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object p2

    .line 480264
    if-nez p2, :cond_0

    sget-object p2, LX/0ax;->fz:Ljava/lang/String;

    const-string p0, "shared_feed_story"

    invoke-static {p2, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 480265
    :goto_1
    iget-object p0, v3, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;->b:LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 480266
    goto :goto_0

    .line 480267
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x79109a62
        :pswitch_0
    .end packed-switch
.end method
