.class public LX/2oK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/2oL;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/graphql/model/GraphQLVideo;

.field private final d:LX/1VK;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "LX/1VK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 466175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 466176
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 466177
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 466178
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1WT;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2oK;->a:Ljava/lang/String;

    .line 466179
    iput-object p1, p0, LX/2oK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 466180
    iput-object p2, p0, LX/2oK;->c:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 466181
    iput-object p3, p0, LX/2oK;->d:LX/1VK;

    .line 466182
    return-void

    .line 466183
    :cond_0
    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 466184
    new-instance v1, LX/2oL;

    invoke-direct {v1}, LX/2oL;-><init>()V

    .line 466185
    iget-object v0, p0, LX/2oK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 466186
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 466187
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 466188
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 466189
    :goto_0
    iget-object v2, p0, LX/2oK;->d:LX/1VK;

    iget-object v3, p0, LX/2oK;->c:Lcom/facebook/graphql/model/GraphQLVideo;

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, LX/1VK;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;Ljava/lang/Integer;)LX/2oO;

    move-result-object v0

    .line 466190
    iput-object v0, v1, LX/2oL;->f:LX/2oO;

    .line 466191
    return-object v1

    .line 466192
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 466193
    iget-object v0, p0, LX/2oK;->a:Ljava/lang/String;

    return-object v0
.end method
