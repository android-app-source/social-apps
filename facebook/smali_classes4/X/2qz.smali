.class public LX/2qz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2qz;


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 471598
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 471599
    return-void
.end method

.method public static a(LX/0QB;)LX/2qz;
    .locals 4

    .prologue
    .line 471583
    sget-object v0, LX/2qz;->b:LX/2qz;

    if-nez v0, :cond_1

    .line 471584
    const-class v1, LX/2qz;

    monitor-enter v1

    .line 471585
    :try_start_0
    sget-object v0, LX/2qz;->b:LX/2qz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 471586
    if-eqz v2, :cond_0

    .line 471587
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 471588
    new-instance p0, LX/2qz;

    invoke-direct {p0}, LX/2qz;-><init>()V

    .line 471589
    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    .line 471590
    iput-object v3, p0, LX/2qz;->a:LX/0Zb;

    .line 471591
    move-object v0, p0

    .line 471592
    sput-object v0, LX/2qz;->b:LX/2qz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 471593
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 471594
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 471595
    :cond_1
    sget-object v0, LX/2qz;->b:LX/2qz;

    return-object v0

    .line 471596
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 471597
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2qz;LX/6be;Lcom/facebook/messaging/model/threadkey/ThreadKey;JLX/2r0;ZII)V
    .locals 3
    .param p1    # LX/6be;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 471573
    if-nez p2, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 471574
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "create_thread_step"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 471575
    const-string v1, "create_thread_action"

    invoke-virtual {p1}, LX/6be;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "create_thread_key"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "create_thread_offline_id"

    invoke-virtual {v1, v2, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "create_thread_source"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "create_thread_waiting_for_media_uploads"

    invoke-virtual {v1, v2, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 471576
    if-lez p7, :cond_1

    .line 471577
    const-string v1, "create_thread_participant_count"

    invoke-virtual {v0, v1, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 471578
    :cond_1
    if-lez p8, :cond_2

    .line 471579
    const-string v1, "create_thread_retry_attempt"

    invoke-virtual {v0, v1, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 471580
    :cond_2
    iget-object v1, p0, LX/2qz;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 471581
    return-void

    .line 471582
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JLX/2r0;Z)V
    .locals 10
    .param p1    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 471569
    sget-object v2, LX/6be;->FAILED:LX/6be;

    const/4 v8, -0x1

    const/4 v9, 0x0

    move-object v1, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    move v7, p5

    invoke-static/range {v1 .. v9}, LX/2qz;->a(LX/2qz;LX/6be;Lcom/facebook/messaging/model/threadkey/ThreadKey;JLX/2r0;ZII)V

    .line 471570
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JLX/2r0;ZI)V
    .locals 10
    .param p1    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 471571
    sget-object v2, LX/6be;->START:LX/6be;

    const/4 v9, 0x0

    move-object v1, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    move v7, p5

    move/from16 v8, p6

    invoke-static/range {v1 .. v9}, LX/2qz;->a(LX/2qz;LX/6be;Lcom/facebook/messaging/model/threadkey/ThreadKey;JLX/2r0;ZII)V

    .line 471572
    return-void
.end method
