.class public final LX/2JU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2JT;


# direct methods
.method public constructor <init>(LX/2JT;)V
    .locals 0

    .prologue
    .line 393005
    iput-object p1, p0, LX/2JU;->a:LX/2JT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 393006
    check-cast p1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    check-cast p2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 393007
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393008
    const/4 v0, 0x0

    .line 393009
    :goto_0
    return v0

    .line 393010
    :cond_0
    iget-object v0, p0, LX/2JU;->a:LX/2JT;

    invoke-virtual {v0, p1}, LX/2JT;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)Z

    move-result v0

    .line 393011
    iget-object v1, p0, LX/2JU;->a:LX/2JT;

    invoke-virtual {v1, p2}, LX/2JT;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)Z

    move-result v1

    .line 393012
    if-ne v0, v1, :cond_1

    .line 393013
    invoke-virtual {p2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->c()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    goto :goto_0

    .line 393014
    :cond_1
    if-eqz v0, :cond_2

    .line 393015
    const/4 v0, -0x1

    goto :goto_0

    .line 393016
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
