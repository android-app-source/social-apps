.class public final LX/3O0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/3Nb;


# direct methods
.method public constructor <init>(LX/3Nb;)V
    .locals 0

    .prologue
    .line 560080
    iput-object p1, p0, LX/3O0;->a:LX/3Nb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x4ce1d294

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 560081
    iget-object v1, p0, LX/3O0;->a:LX/3Nb;

    iget-object v1, v1, LX/3Nb;->l:LX/3O8;

    .line 560082
    iget-object v3, v1, LX/3O8;->a:LX/3Na;

    iget-object v3, v3, LX/3Na;->g:LX/3LU;

    if-eqz v3, :cond_0

    .line 560083
    iget-object v3, v1, LX/3O8;->a:LX/3Na;

    iget-object v3, v3, LX/3Na;->g:LX/3LU;

    .line 560084
    iget-object p0, v3, LX/3LU;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object p0

    const-string p1, "chat_availability_dialog"

    invoke-virtual {p0, p1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p0

    check-cast p0, Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;

    .line 560085
    if-nez p0, :cond_0

    .line 560086
    new-instance p0, Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;

    invoke-direct {p0}, Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;-><init>()V

    .line 560087
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 560088
    move-object p0, p0

    .line 560089
    iget-object p1, v3, LX/3LU;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object p1

    const-string v1, "chat_availability_dialog"

    invoke-virtual {p0, p1, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 560090
    :cond_0
    const v1, 0x7e8f82c7

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
