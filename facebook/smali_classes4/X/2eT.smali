.class public LX/2eT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2eR;

.field public final b:LX/1Ra;

.field public c:Z


# direct methods
.method public constructor <init>(LX/2eR;LX/1Ra;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "LX/2eR",
            "<TV;>;",
            "LX/1Ra",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 445276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445277
    iput-object p1, p0, LX/2eT;->a:LX/2eR;

    .line 445278
    iput-object p2, p0, LX/2eT;->b:LX/1Ra;

    .line 445279
    return-void
.end method

.method private static a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 445280
    instance-of v0, p0, LX/1Pv;

    if-eqz v0, :cond_0

    check-cast p0, LX/1Pv;

    invoke-interface {p0}, LX/1Pv;->iO_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1Rb;LX/1PW;)V
    .locals 8

    .prologue
    .line 445281
    const-string v0, "PageItem.prepare"

    const v1, 0x6ac536f8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 445282
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-static {p2}, LX/2eT;->a(LX/1PW;)Z

    move-result v7

    move-object v0, p2

    move-object v6, p1

    invoke-static/range {v0 .. v7}, LX/1Wz;->a(LX/1PW;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;LX/1Rb;Z)LX/1Q9;

    move-result-object v0

    .line 445283
    iget-object v1, p0, LX/2eT;->b:LX/1Ra;

    invoke-virtual {v1, p2}, LX/1Ra;->a(LX/1PW;)V

    .line 445284
    invoke-static {v0, p2}, LX/1Wz;->a(LX/1Q9;LX/1PW;)V

    .line 445285
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2eT;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445286
    const v0, 0x4e0d2976    # 5.9207616E8f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 445287
    return-void

    .line 445288
    :catchall_0
    move-exception v0

    const v1, -0x44c285e0

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(LX/1Rb;LX/1PW;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "LX/1Rb;",
            "LX/1PW;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 445289
    iget-boolean v0, p0, LX/2eT;->c:Z

    if-nez v0, :cond_0

    .line 445290
    invoke-virtual {p0, p1, p2}, LX/2eT;->a(LX/1Rb;LX/1PW;)V

    .line 445291
    :cond_0
    const-string v0, "PageItem.bind"

    const v1, 0x59231620

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 445292
    :try_start_0
    iget-object v0, p0, LX/2eT;->b:LX/1Ra;

    invoke-virtual {v0, p3}, LX/1Ra;->a(Landroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445293
    const v0, -0x7f4a1681

    invoke-static {v0}, LX/02m;->a(I)V

    .line 445294
    return-void

    .line 445295
    :catchall_0
    move-exception v0

    const v1, 0x3922829

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
