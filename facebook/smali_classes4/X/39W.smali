.class public final LX/39W;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1Wn;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:LX/1Wk;

.field public d:I

.field public e:I

.field public f:LX/1zt;

.field public g:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/20K;

.field public i:LX/39f;

.field public j:LX/20b;

.field public k:LX/20b;

.field public l:LX/21H;

.field public m:LX/20Z;

.field public final synthetic n:LX/1Wn;


# direct methods
.method public constructor <init>(LX/1Wn;)V
    .locals 1

    .prologue
    .line 523147
    iput-object p1, p0, LX/39W;->n:LX/1Wn;

    .line 523148
    move-object v0, p1

    .line 523149
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 523150
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 523163
    const-string v0, "ReactionsFooterButtonsComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 523164
    if-ne p0, p1, :cond_1

    .line 523165
    :cond_0
    :goto_0
    return v0

    .line 523166
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 523167
    goto :goto_0

    .line 523168
    :cond_3
    check-cast p1, LX/39W;

    .line 523169
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 523170
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 523171
    if-eq v2, v3, :cond_0

    .line 523172
    iget-object v2, p0, LX/39W;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/39W;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/39W;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 523173
    goto :goto_0

    .line 523174
    :cond_5
    iget-object v2, p1, LX/39W;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 523175
    :cond_6
    iget-object v2, p0, LX/39W;->b:LX/1Po;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/39W;->b:LX/1Po;

    iget-object v3, p1, LX/39W;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 523176
    goto :goto_0

    .line 523177
    :cond_8
    iget-object v2, p1, LX/39W;->b:LX/1Po;

    if-nez v2, :cond_7

    .line 523178
    :cond_9
    iget-object v2, p0, LX/39W;->c:LX/1Wk;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/39W;->c:LX/1Wk;

    iget-object v3, p1, LX/39W;->c:LX/1Wk;

    invoke-virtual {v2, v3}, LX/1Wk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 523179
    goto :goto_0

    .line 523180
    :cond_b
    iget-object v2, p1, LX/39W;->c:LX/1Wk;

    if-nez v2, :cond_a

    .line 523181
    :cond_c
    iget v2, p0, LX/39W;->d:I

    iget v3, p1, LX/39W;->d:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 523182
    goto :goto_0

    .line 523183
    :cond_d
    iget v2, p0, LX/39W;->e:I

    iget v3, p1, LX/39W;->e:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 523184
    goto :goto_0

    .line 523185
    :cond_e
    iget-object v2, p0, LX/39W;->f:LX/1zt;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/39W;->f:LX/1zt;

    iget-object v3, p1, LX/39W;->f:LX/1zt;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    :cond_f
    move v0, v1

    .line 523186
    goto :goto_0

    .line 523187
    :cond_10
    iget-object v2, p1, LX/39W;->f:LX/1zt;

    if-nez v2, :cond_f

    .line 523188
    :cond_11
    iget-object v2, p0, LX/39W;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_13

    iget-object v2, p0, LX/39W;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/39W;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    :cond_12
    move v0, v1

    .line 523189
    goto/16 :goto_0

    .line 523190
    :cond_13
    iget-object v2, p1, LX/39W;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_12

    .line 523191
    :cond_14
    iget-object v2, p0, LX/39W;->h:LX/20K;

    if-eqz v2, :cond_16

    iget-object v2, p0, LX/39W;->h:LX/20K;

    iget-object v3, p1, LX/39W;->h:LX/20K;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    :cond_15
    move v0, v1

    .line 523192
    goto/16 :goto_0

    .line 523193
    :cond_16
    iget-object v2, p1, LX/39W;->h:LX/20K;

    if-nez v2, :cond_15

    .line 523194
    :cond_17
    iget-object v2, p0, LX/39W;->i:LX/39f;

    if-eqz v2, :cond_19

    iget-object v2, p0, LX/39W;->i:LX/39f;

    iget-object v3, p1, LX/39W;->i:LX/39f;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    :cond_18
    move v0, v1

    .line 523195
    goto/16 :goto_0

    .line 523196
    :cond_19
    iget-object v2, p1, LX/39W;->i:LX/39f;

    if-nez v2, :cond_18

    .line 523197
    :cond_1a
    iget-object v2, p0, LX/39W;->j:LX/20b;

    if-eqz v2, :cond_1c

    iget-object v2, p0, LX/39W;->j:LX/20b;

    iget-object v3, p1, LX/39W;->j:LX/20b;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    :cond_1b
    move v0, v1

    .line 523198
    goto/16 :goto_0

    .line 523199
    :cond_1c
    iget-object v2, p1, LX/39W;->j:LX/20b;

    if-nez v2, :cond_1b

    .line 523200
    :cond_1d
    iget-object v2, p0, LX/39W;->k:LX/20b;

    if-eqz v2, :cond_1f

    iget-object v2, p0, LX/39W;->k:LX/20b;

    iget-object v3, p1, LX/39W;->k:LX/20b;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    :cond_1e
    move v0, v1

    .line 523201
    goto/16 :goto_0

    .line 523202
    :cond_1f
    iget-object v2, p1, LX/39W;->k:LX/20b;

    if-nez v2, :cond_1e

    .line 523203
    :cond_20
    iget-object v2, p0, LX/39W;->l:LX/21H;

    if-eqz v2, :cond_22

    iget-object v2, p0, LX/39W;->l:LX/21H;

    iget-object v3, p1, LX/39W;->l:LX/21H;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    :cond_21
    move v0, v1

    .line 523204
    goto/16 :goto_0

    .line 523205
    :cond_22
    iget-object v2, p1, LX/39W;->l:LX/21H;

    if-nez v2, :cond_21

    .line 523206
    :cond_23
    iget-object v2, p0, LX/39W;->m:LX/20Z;

    if-eqz v2, :cond_24

    iget-object v2, p0, LX/39W;->m:LX/20Z;

    iget-object v3, p1, LX/39W;->m:LX/20Z;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 523207
    goto/16 :goto_0

    .line 523208
    :cond_24
    iget-object v2, p1, LX/39W;->m:LX/20Z;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 3

    .prologue
    .line 523151
    const/4 v2, 0x0

    .line 523152
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/39W;

    .line 523153
    const/4 v1, 0x0

    iput v1, v0, LX/39W;->e:I

    .line 523154
    iput-object v2, v0, LX/39W;->f:LX/1zt;

    .line 523155
    iput-object v2, v0, LX/39W;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 523156
    iput-object v2, v0, LX/39W;->h:LX/20K;

    .line 523157
    iput-object v2, v0, LX/39W;->i:LX/39f;

    .line 523158
    iput-object v2, v0, LX/39W;->j:LX/20b;

    .line 523159
    iput-object v2, v0, LX/39W;->k:LX/20b;

    .line 523160
    iput-object v2, v0, LX/39W;->l:LX/21H;

    .line 523161
    iput-object v2, v0, LX/39W;->m:LX/20Z;

    .line 523162
    return-object v0
.end method
