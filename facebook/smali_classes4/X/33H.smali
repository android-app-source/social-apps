.class public LX/33H;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/nio/ByteBuffer;

.field public final b:I

.field public final c:Z


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 492224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 492225
    iput-object p1, p0, LX/33H;->a:Ljava/nio/ByteBuffer;

    .line 492226
    iget-object v0, p0, LX/33H;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 492227
    iget-object v0, p0, LX/33H;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v3, 0x8

    if-ge v0, v3, :cond_0

    .line 492228
    iput-boolean v2, p0, LX/33H;->c:Z

    .line 492229
    iput v2, p0, LX/33H;->b:I

    .line 492230
    :goto_0
    return-void

    .line 492231
    :cond_0
    iget-object v0, p0, LX/33H;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    const v3, -0x5314ff4

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 492232
    :goto_1
    iget-object v3, p0, LX/33H;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p0, LX/33H;->b:I

    .line 492233
    iget v3, p0, LX/33H;->b:I

    mul-int/lit8 v3, v3, 0x8

    add-int/lit8 v3, v3, 0x8

    .line 492234
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/33H;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 492235
    :goto_2
    iput-boolean v1, p0, LX/33H;->c:Z

    goto :goto_0

    :cond_1
    move v0, v2

    .line 492236
    goto :goto_1

    :cond_2
    move v1, v2

    .line 492237
    goto :goto_2
.end method


# virtual methods
.method public final a(I)J
    .locals 2

    .prologue
    .line 492238
    mul-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x8

    .line 492239
    iget-object v1, p0, LX/33H;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    add-int/lit8 v1, v1, -0x8

    if-le v0, v1, :cond_0

    .line 492240
    const-wide/16 v0, -0x1

    .line 492241
    :goto_0
    return-wide v0

    :cond_0
    iget-object v1, p0, LX/33H;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    goto :goto_0
.end method
