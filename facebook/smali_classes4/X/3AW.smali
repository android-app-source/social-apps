.class public LX/3AW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile o:LX/3AW;


# instance fields
.field private final b:LX/0Zb;

.field public final c:LX/1CC;

.field private final d:LX/0xB;

.field private final e:LX/1vx;

.field public final f:LX/04K;

.field public final g:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0xX;

.field public final i:LX/3Bx;

.field public final j:LX/03V;

.field public k:J

.field private l:J

.field public m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/video/videohome/logging/VideoHomeLoggingUtils$LoggingInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 525322
    const-class v0, LX/3AW;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3AW;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/1CC;LX/0xB;LX/1vx;LX/04K;LX/0xX;LX/3Bx;LX/03V;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 525440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 525441
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/3AW;->m:Ljava/util/Set;

    .line 525442
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/3AW;->n:Ljava/util/Set;

    .line 525443
    iput-object p1, p0, LX/3AW;->b:LX/0Zb;

    .line 525444
    iput-object p2, p0, LX/3AW;->c:LX/1CC;

    .line 525445
    iput-object p3, p0, LX/3AW;->d:LX/0xB;

    .line 525446
    iput-object p4, p0, LX/3AW;->e:LX/1vx;

    .line 525447
    iput-object p5, p0, LX/3AW;->f:LX/04K;

    .line 525448
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, LX/3AW;->g:Landroid/util/LruCache;

    .line 525449
    iput-object p6, p0, LX/3AW;->h:LX/0xX;

    .line 525450
    iput-object p7, p0, LX/3AW;->i:LX/3Bx;

    .line 525451
    iput-object p8, p0, LX/3AW;->j:LX/03V;

    .line 525452
    return-void
.end method

.method public static a(LX/0QB;)LX/3AW;
    .locals 12

    .prologue
    .line 525427
    sget-object v0, LX/3AW;->o:LX/3AW;

    if-nez v0, :cond_1

    .line 525428
    const-class v1, LX/3AW;

    monitor-enter v1

    .line 525429
    :try_start_0
    sget-object v0, LX/3AW;->o:LX/3AW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 525430
    if-eqz v2, :cond_0

    .line 525431
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 525432
    new-instance v3, LX/3AW;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/1CC;->a(LX/0QB;)LX/1CC;

    move-result-object v5

    check-cast v5, LX/1CC;

    invoke-static {v0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v6

    check-cast v6, LX/0xB;

    invoke-static {v0}, LX/1vx;->a(LX/0QB;)LX/1vx;

    move-result-object v7

    check-cast v7, LX/1vx;

    invoke-static {v0}, LX/04K;->a(LX/0QB;)LX/04K;

    move-result-object v8

    check-cast v8, LX/04K;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v9

    check-cast v9, LX/0xX;

    invoke-static {v0}, LX/3Bx;->a(LX/0QB;)LX/3Bx;

    move-result-object v10

    check-cast v10, LX/3Bx;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-direct/range {v3 .. v11}, LX/3AW;-><init>(LX/0Zb;LX/1CC;LX/0xB;LX/1vx;LX/04K;LX/0xX;LX/3Bx;LX/03V;)V

    .line 525433
    move-object v0, v3

    .line 525434
    sput-object v0, LX/3AW;->o:LX/3AW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 525435
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 525436
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 525437
    :cond_1
    sget-object v0, LX/3AW;->o:LX/3AW;

    return-object v0

    .line 525438
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 525439
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/04D;LX/0JD;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 525423
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/0JT;->VIDEO_HOME_CLICK:LX/0JT;

    iget-object v1, v1, LX/0JT;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 525424
    sget-object v1, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    iget-object v2, p0, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525425
    sget-object v1, LX/0JS;->CLICK_TARGET:LX/0JS;

    iget-object v1, v1, LX/0JS;->value:Ljava/lang/String;

    iget-object v2, p1, LX/0JD;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525426
    return-object v0
.end method

.method public static a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 3

    .prologue
    .line 525411
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 525412
    const-string v0, "video_home"

    .line 525413
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 525414
    iget-object v0, p0, LX/3AW;->c:LX/1CC;

    invoke-virtual {v0, p1}, LX/1CC;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 525415
    iget-object v0, p0, LX/3AW;->c:LX/1CC;

    invoke-virtual {v0}, LX/1CC;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525416
    sget-object v0, LX/0JS;->SESSION_ID:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v1, p0, LX/3AW;->c:LX/1CC;

    .line 525417
    iget-object v2, v1, LX/1CC;->d:Ljava/lang/String;

    move-object v1, v2

    .line 525418
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525419
    :cond_0
    invoke-direct {p0, p1}, LX/3AW;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 525420
    iget-object v0, p0, LX/3AW;->b:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 525421
    invoke-virtual {p1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g()Ljava/lang/String;

    .line 525422
    return-void
.end method

.method private b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 11

    .prologue
    .line 525379
    iget-object v0, p0, LX/3AW;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ET3;

    .line 525380
    iget-object v2, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v2, v2

    .line 525381
    sget-object v3, LX/0JT;->VIDEO_HOME_SESSION_END:LX/0JT;

    iget-object v3, v3, LX/0JT;->value:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 525382
    iget-object v2, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v2, v2

    .line 525383
    sget-object v3, LX/0JT;->VIDEO_HOME_BACKGROUNDED:LX/0JT;

    iget-object v3, v3, LX/0JT;->value:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 525384
    :cond_0
    iget-object v2, v0, LX/ET3;->a:LX/ETB;

    const/4 v5, 0x0

    .line 525385
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 525386
    iget-object v3, v2, LX/ETB;->t:LX/ETQ;

    move-object v3, v3

    .line 525387
    invoke-virtual {v3}, LX/ETQ;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 525388
    iget-object v6, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v6

    .line 525389
    if-eqz v6, :cond_1

    .line 525390
    invoke-virtual {v3}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v3}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v3

    invoke-virtual {v3}, LX/ETQ;->size()I

    move-result v3

    .line 525391
    :goto_2
    invoke-static {v7, v6, v3}, LX/ETB;->a(Ljava/util/Map;Ljava/lang/String;I)V

    goto :goto_1

    .line 525392
    :cond_2
    const/4 v3, 0x1

    goto :goto_2

    .line 525393
    :cond_3
    iget-object v3, v2, LX/ETB;->J:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_4
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 525394
    check-cast v3, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 525395
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v9

    .line 525396
    if-eqz v9, :cond_4

    .line 525397
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result p0

    move v4, v5

    move v6, v5

    :goto_4
    if-ge v4, p0, :cond_6

    invoke-virtual {v10, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 525398
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 525399
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/2addr v3, v6

    .line 525400
    :goto_5
    add-int/lit8 v4, v4, 0x1

    move v6, v3

    goto :goto_4

    .line 525401
    :cond_5
    add-int/lit8 v3, v6, 0x1

    goto :goto_5

    .line 525402
    :cond_6
    invoke-static {v7, v9, v6}, LX/ETB;->a(Ljava/util/Map;Ljava/lang/String;I)V

    goto :goto_3

    .line 525403
    :cond_7
    move-object v4, v7

    .line 525404
    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-virtual {v2}, LX/0mC;->b()LX/162;

    move-result-object v5

    .line 525405
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 525406
    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-virtual {v3}, LX/0mC;->c()LX/0m9;

    move-result-object v7

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v7, v3, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Integer;)LX/0m9;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_6

    .line 525407
    :cond_8
    sget-object v2, LX/0JS;->NUMBER_OF_SECTION:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525408
    sget-object v2, LX/0JS;->REACTION_UNITS_TYPES:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {p1, v2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525409
    :cond_9
    goto/16 :goto_0

    .line 525410
    :cond_a
    return-void
.end method

.method private static c(LX/3AW;)I
    .locals 1

    .prologue
    .line 525376
    iget-object v0, p0, LX/3AW;->c:LX/1CC;

    .line 525377
    iget p0, v0, LX/1CC;->h:I

    move v0, p0

    .line 525378
    return v0
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 525453
    iput-wide p1, p0, LX/3AW;->l:J

    .line 525454
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3AW;->k:J

    .line 525455
    return-void
.end method

.method public final a(JLjava/lang/String;LX/0J9;IZ)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const-wide v4, 0x408f400000000000L    # 1000.0

    .line 525353
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v0, LX/0JT;->VIDEO_HOME_SESSION_START:LX/0JT;

    iget-object v0, v0, LX/0JT;->value:Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 525354
    sget-object v0, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    sget-object v2, LX/04D;->VIDEO_HOME:LX/04D;

    iget-object v2, v2, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525355
    sget-object v0, LX/0JS;->BADGE_COUNT:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    invoke-static {p0}, LX/3AW;->c(LX/3AW;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525356
    sget-object v0, LX/0JS;->UNSEEN_NOTIFICATIONS_COUNT:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v1, v0, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525357
    sget-object v0, LX/0JS;->DATA_PREFETCH_STATUS:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v2, p4, LX/0J9;->value:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525358
    sget-object v0, LX/0JS;->PREFETCH_ENABLED:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v1, v0, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525359
    sget-object v0, LX/0JS;->VIDEO_SEARCH_EXPERIENCE:LX/0JS;

    iget-object v2, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v0, p0, LX/3AW;->e:LX/1vx;

    invoke-virtual {v0}, LX/1vx;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/0JW;->KEYWORDS:LX/0JW;

    iget-object v0, v0, LX/0JW;->value:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525360
    sget-object v0, LX/0JS;->ENTRY_POINT_TYPE:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525361
    iget-wide v2, p0, LX/3AW;->l:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_0

    .line 525362
    iget-wide v2, p0, LX/3AW;->l:J

    sub-long v2, p1, v2

    long-to-double v2, v2

    div-double/2addr v2, v4

    .line 525363
    sget-object v0, LX/0JS;->TIME_SINCE_START:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525364
    :cond_0
    iget-wide v2, p0, LX/3AW;->k:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_2

    .line 525365
    iget-wide v2, p0, LX/3AW;->k:J

    sub-long v2, p1, v2

    long-to-double v2, v2

    div-double/2addr v2, v4

    .line 525366
    sget-object v0, LX/0JS;->TIME_SINCE_PREFETCH:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525367
    :goto_1
    invoke-static {p0, v1}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 525368
    iget-object v0, p0, LX/3AW;->f:LX/04K;

    const/4 v3, 0x0

    .line 525369
    iget-object v1, v0, LX/04K;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x1d000a

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 525370
    iput-boolean v3, v0, LX/04K;->b:Z

    .line 525371
    iput-boolean v3, v0, LX/04K;->c:Z

    .line 525372
    iput-boolean v3, v0, LX/04K;->d:Z

    .line 525373
    return-void

    .line 525374
    :cond_1
    sget-object v0, LX/0JW;->DISABLED:LX/0JW;

    iget-object v0, v0, LX/0JW;->value:Ljava/lang/String;

    goto :goto_0

    .line 525375
    :cond_2
    sget-object v0, LX/0JS;->TIME_SINCE_PREFETCH:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_1
.end method

.method public final a(LX/04D;LX/0JD;LX/7Qf;)V
    .locals 1

    .prologue
    .line 525349
    invoke-static {p1, p2}, LX/3AW;->a(LX/04D;LX/0JD;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 525350
    invoke-interface {p3, v0}, LX/7Qf;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 525351
    invoke-static {p0, v0}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 525352
    return-void
.end method

.method public final a(LX/04D;LX/0JD;Ljava/lang/String;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 525343
    invoke-static {p1, p2}, LX/3AW;->a(LX/04D;LX/0JD;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 525344
    sget-object v1, LX/0JS;->EVENT_TARGET:LX/0JS;

    iget-object v1, v1, LX/0JS;->value:Ljava/lang/String;

    const-string v2, "button"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525345
    if-eqz p3, :cond_0

    .line 525346
    sget-object v1, LX/0JS;->REACTION_COMPONENT_TRACKING_FIELD:LX/0JS;

    iget-object v1, v1, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525347
    :cond_0
    invoke-static {p0, v0}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 525348
    return-void
.end method

.method public final a(LX/0JU;LX/04D;J)V
    .locals 3

    .prologue
    .line 525337
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/0JT;->VIDEO_HOME_DATA_PREFETCH:LX/0JT;

    iget-object v1, v1, LX/0JT;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 525338
    sget-object v1, LX/0JS;->FETCH_REASON:LX/0JS;

    iget-object v1, v1, LX/0JS;->value:Ljava/lang/String;

    iget-object v2, p1, LX/0JU;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525339
    sget-object v1, LX/0JS;->DATA_STALE_INTERVAL:LX/0JS;

    iget-object v1, v1, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525340
    sget-object v1, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    iget-object v2, p2, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525341
    invoke-static {p0, v0}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 525342
    return-void
.end method

.method public final a(LX/7Qf;)V
    .locals 2

    .prologue
    .line 525333
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/0JT;->VIDEO_HOME_TTI:LX/0JT;

    iget-object v1, v1, LX/0JT;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 525334
    invoke-interface {p1, v0}, LX/7Qf;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 525335
    invoke-static {p0, v0}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 525336
    return-void
.end method

.method public final b(LX/04D;LX/0JD;LX/7Qf;)V
    .locals 1

    .prologue
    .line 525329
    invoke-static {p1, p2}, LX/3AW;->a(LX/04D;LX/0JD;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 525330
    invoke-interface {p3, v0}, LX/7Qf;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 525331
    invoke-static {p0, v0}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 525332
    return-void
.end method

.method public final b(LX/095;I)V
    .locals 3

    .prologue
    .line 525323
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/0JT;->VIDEO_HOME_BADGE_FETCHED:LX/0JT;

    iget-object v1, v1, LX/0JT;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 525324
    sget-object v1, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    sget-object v2, LX/04D;->VIDEO_HOME:LX/04D;

    iget-object v2, v2, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525325
    sget-object v1, LX/0JS;->FETCH_REASON:LX/0JS;

    iget-object v1, v1, LX/0JS;->value:Ljava/lang/String;

    iget-object v2, p1, LX/095;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525326
    sget-object v1, LX/0JS;->BADGE_COUNT:LX/0JS;

    iget-object v1, v1, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 525327
    invoke-static {p0, v0}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 525328
    return-void
.end method
