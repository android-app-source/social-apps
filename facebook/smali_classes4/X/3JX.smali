.class public LX/3JX;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3JO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3JO",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/3JO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3JO",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/3JO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3JO",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final d:LX/3JO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3JO",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 547767
    new-instance v0, LX/3JY;

    invoke-direct {v0}, LX/3JY;-><init>()V

    sput-object v0, LX/3JX;->a:LX/3JO;

    .line 547768
    new-instance v0, LX/3JZ;

    invoke-direct {v0}, LX/3JZ;-><init>()V

    sput-object v0, LX/3JX;->b:LX/3JO;

    .line 547769
    new-instance v0, LX/3Ja;

    invoke-direct {v0}, LX/3Ja;-><init>()V

    sput-object v0, LX/3JX;->c:LX/3JO;

    .line 547770
    new-instance v0, LX/3Jb;

    invoke-direct {v0}, LX/3Jb;-><init>()V

    sput-object v0, LX/3JX;->d:LX/3JO;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 547766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/util/JsonReader;)[F
    .locals 1

    .prologue
    .line 547749
    sget-object v0, LX/3JX;->b:LX/3JO;

    invoke-virtual {v0, p0}, LX/3JO;->a(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/3JX;->a(Ljava/util/List;)[F

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;)[F
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)[F"
        }
    .end annotation

    .prologue
    .line 547761
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [F

    .line 547762
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 547763
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v2, v1

    .line 547764
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 547765
    :cond_0
    return-object v2
.end method

.method public static b(Landroid/util/JsonReader;)[[[F
    .locals 8

    .prologue
    .line 547750
    sget-object v0, LX/3JX;->d:LX/3JO;

    invoke-virtual {v0, p0}, LX/3JO;->a(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v0

    const/4 v3, 0x0

    .line 547751
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v5, v1, [[[F

    .line 547752
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_1

    .line 547753
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v7, v1, [[F

    .line 547754
    array-length p0, v7

    move v2, v3

    :goto_1
    if-ge v2, p0, :cond_0

    .line 547755
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-static {v1}, LX/3JX;->a(Ljava/util/List;)[F

    move-result-object v1

    aput-object v1, v7, v2

    .line 547756
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 547757
    :cond_0
    aput-object v7, v5, v4

    .line 547758
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 547759
    :cond_1
    move-object v0, v5

    .line 547760
    return-object v0
.end method
