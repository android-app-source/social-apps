.class public LX/2oj;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/2oa;",
        "LX/2ol;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<",
            "LX/2ok;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Sh;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 467254
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, LX/2ok;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, LX/2oj;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/0Sh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 467255
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 467256
    iput-object p1, p0, LX/2oj;->b:LX/0Sh;

    .line 467257
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/0b7;)V
    .locals 0

    .prologue
    .line 467258
    check-cast p1, LX/2ol;

    invoke-virtual {p0, p1}, LX/2oj;->a(LX/2ol;)V

    return-void
.end method

.method public final a(LX/2ol;)V
    .locals 2

    .prologue
    .line 467259
    iget-object v0, p0, LX/2oj;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 467260
    iget-object v0, p0, LX/2oj;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/video/player/events/RichVideoPlayerEventBus$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/player/events/RichVideoPlayerEventBus$1;-><init>(LX/2oj;LX/2ol;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 467261
    :goto_0
    return-void

    .line 467262
    :cond_0
    sget-object v0, LX/2oj;->a:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 467263
    invoke-super {p0, p1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method
