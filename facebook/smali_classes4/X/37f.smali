.class public LX/37f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/37f;


# instance fields
.field public b:LX/0AU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Z

.field private e:J

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 501807
    const-class v0, LX/37f;

    sput-object v0, LX/37f;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 501806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/37f;)J
    .locals 4

    .prologue
    .line 501805
    iget-object v0, p0, LX/37f;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/37f;->e:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public static a(LX/0QB;)LX/37f;
    .locals 5

    .prologue
    .line 501790
    sget-object v0, LX/37f;->h:LX/37f;

    if-nez v0, :cond_1

    .line 501791
    const-class v1, LX/37f;

    monitor-enter v1

    .line 501792
    :try_start_0
    sget-object v0, LX/37f;->h:LX/37f;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 501793
    if-eqz v2, :cond_0

    .line 501794
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 501795
    new-instance p0, LX/37f;

    invoke-direct {p0}, LX/37f;-><init>()V

    .line 501796
    invoke-static {v0}, LX/0AU;->a(LX/0QB;)LX/0AU;

    move-result-object v3

    check-cast v3, LX/0AU;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    .line 501797
    iput-object v3, p0, LX/37f;->b:LX/0AU;

    iput-object v4, p0, LX/37f;->c:LX/0So;

    .line 501798
    move-object v0, p0

    .line 501799
    sput-object v0, LX/37f;->h:LX/37f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 501800
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 501801
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 501802
    :cond_1
    sget-object v0, LX/37f;->h:LX/37f;

    return-object v0

    .line 501803
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 501804
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 9

    .prologue
    .line 501757
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, LX/37f;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/37f;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/37f;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 501758
    iget-object v1, p0, LX/37f;->b:LX/0AU;

    iget-boolean v4, p0, LX/37f;->d:Z

    iget-object v5, p0, LX/37f;->f:Ljava/lang/String;

    invoke-static {p0}, LX/37f;->a(LX/37f;)J

    move-result-wide v6

    iget-object v8, p0, LX/37f;->g:Ljava/lang/String;

    move v2, p1

    move-object v3, p2

    .line 501759
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 501760
    const-string p0, "error_code"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501761
    const-string p0, "error_description"

    invoke-virtual {v0, p0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501762
    const-string p0, "initial_cast"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501763
    const-string p0, "cast_session_id"

    invoke-virtual {v0, p0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501764
    const-string p0, "time_since_requested"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501765
    const-string p0, "video_id"

    invoke-virtual {v0, p0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501766
    const-string p0, "cast_failed"

    invoke-static {v1, p0, v0}, LX/0AU;->a(LX/0AU;Ljava/lang/String;Ljava/util/Map;)V

    .line 501767
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 501779
    iget-boolean v0, p0, LX/37f;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 501780
    iget-object v0, p0, LX/37f;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 501781
    iput-object p1, p0, LX/37f;->f:Ljava/lang/String;

    .line 501782
    :cond_0
    iget-object v1, p0, LX/37f;->b:LX/0AU;

    iget-boolean v2, p0, LX/37f;->d:Z

    iget-object v3, p0, LX/37f;->f:Ljava/lang/String;

    invoke-static {p0}, LX/37f;->a(LX/37f;)J

    move-result-wide v4

    iget-object v6, p0, LX/37f;->g:Ljava/lang/String;

    .line 501783
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 501784
    const-string p0, "initial_cast"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501785
    const-string p0, "cast_session_id"

    invoke-virtual {v0, p0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501786
    const-string p0, "time_since_requested"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501787
    const-string p0, "video_id"

    invoke-virtual {v0, p0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501788
    const-string p0, "cast_started"

    invoke-static {v1, p0, v0}, LX/0AU;->a(LX/0AU;Ljava/lang/String;Ljava/util/Map;)V

    .line 501789
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 501768
    iget-object v0, p0, LX/37f;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/37f;->e:J

    .line 501769
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/37f;->d:Z

    .line 501770
    iput-object p1, p0, LX/37f;->f:Ljava/lang/String;

    .line 501771
    iput-object p2, p0, LX/37f;->g:Ljava/lang/String;

    .line 501772
    iget-object v0, p0, LX/37f;->b:LX/0AU;

    iget-boolean v1, p0, LX/37f;->d:Z

    iget-object v2, p0, LX/37f;->f:Ljava/lang/String;

    iget-object v3, p0, LX/37f;->g:Ljava/lang/String;

    .line 501773
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 501774
    const-string p1, "initial_cast"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501775
    const-string p1, "cast_session_id"

    invoke-virtual {p0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501776
    const-string p1, "video_id"

    invoke-virtual {p0, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501777
    const-string p1, "cast_requested"

    invoke-static {v0, p1, p0}, LX/0AU;->a(LX/0AU;Ljava/lang/String;Ljava/util/Map;)V

    .line 501778
    return-void
.end method
