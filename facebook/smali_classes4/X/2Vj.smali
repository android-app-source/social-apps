.class public LX/2Vj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PARAMS:",
        "Ljava/lang/Object;",
        "RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/0e6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TPARAMS;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:LX/4cn;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;

.field public final h:LX/03R;


# direct methods
.method public constructor <init>(LX/2Vk;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Vk",
            "<TPARAMS;TRESU",
            "LT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 418030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418031
    iget-object v0, p1, LX/2Vk;->a:LX/0e6;

    iput-object v0, p0, LX/2Vj;->a:LX/0e6;

    .line 418032
    iget-object v0, p1, LX/2Vk;->b:Ljava/lang/Object;

    iput-object v0, p0, LX/2Vj;->b:Ljava/lang/Object;

    .line 418033
    iget-object v0, p1, LX/2Vk;->c:Ljava/lang/String;

    iput-object v0, p0, LX/2Vj;->c:Ljava/lang/String;

    .line 418034
    iget-object v0, p1, LX/2Vk;->d:Ljava/lang/String;

    iput-object v0, p0, LX/2Vj;->d:Ljava/lang/String;

    .line 418035
    iget-object v0, p1, LX/2Vk;->e:Ljava/lang/String;

    iput-object v0, p0, LX/2Vj;->e:Ljava/lang/String;

    .line 418036
    iget-object v0, p1, LX/2Vk;->f:LX/4cn;

    iput-object v0, p0, LX/2Vj;->f:LX/4cn;

    .line 418037
    iget-object v0, p1, LX/2Vk;->h:LX/03R;

    iput-object v0, p0, LX/2Vj;->h:LX/03R;

    .line 418038
    iget-object v0, p1, LX/2Vk;->g:Ljava/lang/String;

    iput-object v0, p0, LX/2Vj;->g:Ljava/lang/String;

    .line 418039
    return-void
.end method

.method public static a(LX/0e6;Ljava/lang/Object;)LX/2Vk;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;)",
            "LX/2Vk",
            "<TPARAMS;TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418059
    new-instance v0, LX/2Vk;

    invoke-direct {v0, p0, p1}, LX/2Vk;-><init>(LX/0e6;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/0e6;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418058
    iget-object v0, p0, LX/2Vj;->a:LX/0e6;

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TPARAMS;"
        }
    .end annotation

    .prologue
    .line 418057
    iget-object v0, p0, LX/2Vj;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 418041
    instance-of v1, p1, LX/2Vj;

    if-nez v1, :cond_1

    .line 418042
    :cond_0
    :goto_0
    return v0

    .line 418043
    :cond_1
    check-cast p1, LX/2Vj;

    .line 418044
    iget-object v1, p0, LX/2Vj;->c:Ljava/lang/String;

    .line 418045
    iget-object v2, p1, LX/2Vj;->c:Ljava/lang/String;

    move-object v2, v2

    .line 418046
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Vj;->a:LX/0e6;

    .line 418047
    iget-object v2, p1, LX/2Vj;->a:LX/0e6;

    move-object v2, v2

    .line 418048
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Vj;->b:Ljava/lang/Object;

    .line 418049
    iget-object v2, p1, LX/2Vj;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 418050
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Vj;->d:Ljava/lang/String;

    .line 418051
    iget-object v2, p1, LX/2Vj;->d:Ljava/lang/String;

    move-object v2, v2

    .line 418052
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Vj;->e:Ljava/lang/String;

    .line 418053
    iget-object v2, p1, LX/2Vj;->e:Ljava/lang/String;

    move-object v2, v2

    .line 418054
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Vj;->g:Ljava/lang/String;

    .line 418055
    iget-object v2, p1, LX/2Vj;->g:Ljava/lang/String;

    move-object v2, v2

    .line 418056
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Vj;->h:LX/03R;

    iget-object v2, p1, LX/2Vj;->h:LX/03R;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 418040
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/2Vj;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/2Vj;->a:LX/0e6;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/2Vj;->b:Ljava/lang/Object;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/2Vj;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/2Vj;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LX/2Vj;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, LX/2Vj;->h:LX/03R;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
