.class public final LX/380;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/37v;

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/384;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/381;

.field public d:LX/382;


# direct methods
.method public constructor <init>(LX/37v;)V
    .locals 1

    .prologue
    .line 502786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 502787
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/380;->b:Ljava/util/ArrayList;

    .line 502788
    iput-object p1, p0, LX/380;->a:LX/37v;

    .line 502789
    iget-object v0, p1, LX/37v;->b:LX/381;

    move-object v0, v0

    .line 502790
    iput-object v0, p0, LX/380;->c:LX/381;

    .line 502791
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 502776
    iget-object v0, p0, LX/380;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 502777
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 502778
    iget-object v0, p0, LX/380;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/384;

    iget-object v0, v0, LX/384;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 502779
    :goto_1
    return v0

    .line 502780
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 502781
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 502782
    iget-object v0, p0, LX/380;->c:LX/381;

    .line 502783
    iget-object p0, v0, LX/381;->a:Landroid/content/ComponentName;

    invoke-virtual {p0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    .line 502784
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 502785
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaRouter.RouteProviderInfo{ packageName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/380;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
