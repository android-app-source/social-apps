.class public LX/2Lz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2Lz;


# instance fields
.field private final a:LX/2M0;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2M0;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/video/config/IsInlineVideoEnabled;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/momentsinvite/config/IsMomentsInviteEnabled;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/payments/p2p/config/IsP2pPaymentsRequestEligible;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/video/config/IsInlineVideoForXMAsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2M0;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395943
    iput-object p1, p0, LX/2Lz;->a:LX/2M0;

    .line 395944
    iput-object p2, p0, LX/2Lz;->b:LX/0Or;

    .line 395945
    iput-object p3, p0, LX/2Lz;->c:LX/0Or;

    .line 395946
    iput-object p4, p0, LX/2Lz;->d:LX/0Or;

    .line 395947
    iput-object p5, p0, LX/2Lz;->e:LX/0Or;

    .line 395948
    return-void
.end method

.method public static a(LX/0QB;)LX/2Lz;
    .locals 9

    .prologue
    .line 395949
    sget-object v0, LX/2Lz;->f:LX/2Lz;

    if-nez v0, :cond_1

    .line 395950
    const-class v1, LX/2Lz;

    monitor-enter v1

    .line 395951
    :try_start_0
    sget-object v0, LX/2Lz;->f:LX/2Lz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 395952
    if-eqz v2, :cond_0

    .line 395953
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 395954
    new-instance v3, LX/2Lz;

    invoke-static {v0}, LX/2M0;->a(LX/0QB;)LX/2M0;

    move-result-object v4

    check-cast v4, LX/2M0;

    const/16 v5, 0x1533

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x1527

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x1545

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x1534

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/2Lz;-><init>(LX/2M0;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V

    .line 395955
    move-object v0, v3

    .line 395956
    sput-object v0, LX/2Lz;->f:LX/2Lz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 395957
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 395958
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 395959
    :cond_1
    sget-object v0, LX/2Lz;->f:LX/2Lz;

    return-object v0

    .line 395960
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 395961
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static g(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 395962
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 395963
    iget-object v0, p0, LX/2Lz;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 395964
    :goto_0
    return v0

    .line 395965
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_3

    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v3, :cond_3

    :cond_2
    move v0, v2

    .line 395966
    :goto_1
    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-nez v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    .line 395967
    goto :goto_1

    :cond_4
    move v0, v1

    .line 395968
    goto :goto_0
.end method

.method public final e(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 395969
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->a:LX/6fR;

    sget-object v3, LX/6fR;->PAYMENT:LX/6fR;

    if-ne v0, v3, :cond_1

    move v0, v2

    .line 395970
    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Lz;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 395971
    goto :goto_0

    :cond_2
    move v0, v1

    .line 395972
    goto :goto_1
.end method

.method public final f(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 395973
    iget-object v0, p0, LX/2Lz;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 395974
    :goto_0
    return v0

    .line 395975
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 395976
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/share/Share;

    .line 395977
    :goto_1
    move-object v0, v0

    .line 395978
    if-nez v0, :cond_1

    move v0, v1

    .line 395979
    goto :goto_0

    .line 395980
    :cond_1
    if-nez v0, :cond_5

    .line 395981
    const/4 p0, 0x0

    .line 395982
    :goto_2
    move-object v0, p0

    .line 395983
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 395984
    :cond_3
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-eqz v0, :cond_4

    .line 395985
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->b:Lcom/facebook/messaging/model/share/Share;

    goto :goto_1

    .line 395986
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    iget-object p0, v0, Lcom/facebook/messaging/model/share/Share;->m:Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    goto :goto_2
.end method
