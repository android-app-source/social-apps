.class public final LX/2ZI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/2ZG;


# direct methods
.method public constructor <init>(LX/2ZG;)V
    .locals 0

    .prologue
    .line 422284
    iput-object p1, p0, LX/2ZI;->a:LX/2ZG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 422285
    iget-object v0, p0, LX/2ZI;->a:LX/2ZG;

    iget-object v0, v0, LX/2ZG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "getLoggedInUser"

    .line 422286
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 422287
    move-object v0, v0

    .line 422288
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 422289
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 422290
    const-string v0, "getLoggedInUser"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLResult;

    .line 422291
    new-instance v1, LX/0XI;

    invoke-direct {v1}, LX/0XI;-><init>()V

    .line 422292
    iget-object p1, v0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLResult;->a:Lcom/facebook/user/model/User;

    move-object v0, p1

    .line 422293
    invoke-virtual {v1, v0}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    .line 422294
    iget-object v0, p0, LX/2ZI;->a:LX/2ZG;

    iget-object v0, v0, LX/2ZG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0WJ;->c(Lcom/facebook/user/model/User;)V

    .line 422295
    return-void
.end method
