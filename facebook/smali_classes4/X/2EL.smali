.class public LX/2EL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;
.implements LX/2EM;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/google/common/util/concurrent/ListenableFuture;

.field private static volatile m:LX/2EL;


# instance fields
.field private final b:Ljava/util/concurrent/ScheduledExecutorService;

.field public final c:LX/0So;

.field public final d:LX/2EN;

.field public final e:LX/0Xl;

.field public final f:LX/0kb;

.field public final g:LX/1MZ;

.field public final h:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
    .end annotation
.end field

.field private final i:LX/01T;

.field public volatile j:J

.field public volatile k:LX/2EQ;

.field public volatile l:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 385401
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    sput-object v0, LX/2EL;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0So;LX/2EN;LX/0Xl;LX/0kb;LX/1MZ;Landroid/os/Handler;LX/01T;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p7    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 385389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385390
    iput-object p1, p0, LX/2EL;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 385391
    iput-object p2, p0, LX/2EL;->c:LX/0So;

    .line 385392
    iput-object p3, p0, LX/2EL;->d:LX/2EN;

    .line 385393
    iput-object p4, p0, LX/2EL;->e:LX/0Xl;

    .line 385394
    iput-object p5, p0, LX/2EL;->f:LX/0kb;

    .line 385395
    iput-object p6, p0, LX/2EL;->g:LX/1MZ;

    .line 385396
    iput-object p7, p0, LX/2EL;->h:Landroid/os/Handler;

    .line 385397
    sget-object v0, LX/2EQ;->NOT_CHECKED:LX/2EQ;

    iput-object v0, p0, LX/2EL;->k:LX/2EQ;

    .line 385398
    sget-object v0, LX/2EL;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object v0, p0, LX/2EL;->l:Ljava/util/concurrent/Future;

    .line 385399
    iput-object p8, p0, LX/2EL;->i:LX/01T;

    .line 385400
    return-void
.end method

.method public static a(LX/0QB;)LX/2EL;
    .locals 12

    .prologue
    .line 385373
    sget-object v0, LX/2EL;->m:LX/2EL;

    if-nez v0, :cond_1

    .line 385374
    const-class v1, LX/2EL;

    monitor-enter v1

    .line 385375
    :try_start_0
    sget-object v0, LX/2EL;->m:LX/2EL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 385376
    if-eqz v2, :cond_0

    .line 385377
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 385378
    new-instance v3, LX/2EL;

    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    .line 385379
    new-instance v8, LX/2EN;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v6

    check-cast v6, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-direct {v8, v6, v7}, LX/2EN;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0Uh;)V

    .line 385380
    move-object v6, v8

    .line 385381
    check-cast v6, LX/2EN;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v8

    check-cast v8, LX/0kb;

    invoke-static {v0}, LX/1MZ;->a(LX/0QB;)LX/1MZ;

    move-result-object v9

    check-cast v9, LX/1MZ;

    invoke-static {v0}, LX/0Zw;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v10

    check-cast v10, Landroid/os/Handler;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v11

    check-cast v11, LX/01T;

    invoke-direct/range {v3 .. v11}, LX/2EL;-><init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0So;LX/2EN;LX/0Xl;LX/0kb;LX/1MZ;Landroid/os/Handler;LX/01T;)V

    .line 385382
    move-object v0, v3

    .line 385383
    sput-object v0, LX/2EL;->m:LX/2EL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 385384
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 385385
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 385386
    :cond_1
    sget-object v0, LX/2EL;->m:LX/2EL;

    return-object v0

    .line 385387
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 385388
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/2EL;LX/2EQ;)V
    .locals 4

    .prologue
    .line 385367
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2EL;->k:LX/2EQ;

    .line 385368
    iput-object p1, p0, LX/2EL;->k:LX/2EQ;

    .line 385369
    iget-object v1, p0, LX/2EL;->k:LX/2EQ;

    if-eq v1, v0, :cond_0

    .line 385370
    iget-object v0, p0, LX/2EL;->e:LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.common.netchecker.ACTION_NETCHECK_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "state"

    iget-object v3, p0, LX/2EL;->k:LX/2EQ;

    invoke-virtual {v3}, LX/2EQ;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385371
    :cond_0
    monitor-exit p0

    return-void

    .line 385372
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a$redex0(LX/2EL;LX/2EU;)V
    .locals 2

    .prologue
    .line 385357
    sget-object v0, LX/2EU;->CHANNEL_CONNECTED:LX/2EU;

    if-ne p1, v0, :cond_0

    .line 385358
    iget-object v0, p0, LX/2EL;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/2EL;->j:J

    .line 385359
    sget-object v0, LX/2EQ;->NOT_CAPTIVE_PORTAL:LX/2EQ;

    invoke-static {p0, v0}, LX/2EL;->a$redex0(LX/2EL;LX/2EQ;)V

    .line 385360
    iget-object v0, p0, LX/2EL;->l:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 385361
    sget-object v0, LX/2EL;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object v0, p0, LX/2EL;->l:Ljava/util/concurrent/Future;

    .line 385362
    :cond_0
    return-void
.end method

.method public static g(LX/2EL;)V
    .locals 2

    .prologue
    .line 385363
    iget-object v0, p0, LX/2EL;->k:LX/2EQ;

    sget-object v1, LX/2EQ;->CAPTIVE_PORTAL:LX/2EQ;

    if-eq v0, v1, :cond_1

    .line 385364
    :cond_0
    :goto_0
    return-void

    .line 385365
    :cond_1
    iget-object v0, p0, LX/2EL;->l:Ljava/util/concurrent/Future;

    sget-object v1, LX/2EL;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    if-ne v0, v1, :cond_0

    .line 385366
    const/16 v0, 0x2710

    invoke-virtual {p0, v0}, LX/2EL;->a(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 385402
    invoke-static {p0}, LX/2EL;->g(LX/2EL;)V

    .line 385403
    return-void
.end method

.method public final declared-synchronized a(I)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 385328
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2EL;->i:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_1

    .line 385329
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 385330
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2EL;->f:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    const/4 v1, 0x1

    .line 385331
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-ne v2, v1, :cond_2

    :goto_1
    move v0, v1

    .line 385332
    if-eqz v0, :cond_0

    .line 385333
    iget-object v0, p0, LX/2EL;->f:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->t()J

    move-result-wide v0

    .line 385334
    iget-object v2, p0, LX/2EL;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lcom/facebook/common/netchecker/NetChecker$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/facebook/common/netchecker/NetChecker$1;-><init>(LX/2EL;J)V

    int-to-long v0, p1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v0, v1, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/2EL;->l:Ljava/util/concurrent/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 385335
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 385327
    return-void
.end method

.method public final c()LX/2EQ;
    .locals 1

    .prologue
    .line 385326
    iget-object v0, p0, LX/2EL;->k:LX/2EQ;

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 385336
    iget-wide v0, p0, LX/2EL;->j:J

    return-wide v0
.end method

.method public final declared-synchronized e()V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 385337
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2EL;->l:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 385338
    sget-object v0, LX/2EL;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object v0, p0, LX/2EL;->l:Ljava/util/concurrent/Future;

    .line 385339
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2EL;->j:J

    .line 385340
    sget-object v0, LX/2EQ;->NOT_CHECKED:LX/2EQ;

    invoke-static {p0, v0}, LX/2EL;->a$redex0(LX/2EL;LX/2EQ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385341
    monitor-exit p0

    return-void

    .line 385342
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final init()V
    .locals 4

    .prologue
    .line 385343
    new-instance v0, LX/2ER;

    invoke-direct {v0, p0}, LX/2ER;-><init>(LX/2EL;)V

    .line 385344
    new-instance v1, LX/2ES;

    invoke-direct {v1, p0}, LX/2ES;-><init>(LX/2EL;)V

    .line 385345
    iget-object v2, p0, LX/2EL;->e:LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-interface {v2, v3, v1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 385346
    iget-object v1, p0, LX/2EL;->e:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/2EL;->h:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 385347
    iget-object v0, p0, LX/2EL;->e:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    new-instance v2, LX/2ET;

    invoke-direct {v2, p0}, LX/2ET;-><init>(LX/2EL;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 385348
    sget-object v0, LX/2EU;->UNKNOWN:LX/2EU;

    .line 385349
    sget-object v1, LX/2EV;->a:[I

    iget-object v2, p0, LX/2EL;->g:LX/1MZ;

    invoke-virtual {v2}, LX/1MZ;->b()LX/1Mb;

    move-result-object v2

    invoke-virtual {v2}, LX/1Mb;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 385350
    :goto_0
    invoke-static {p0, v0}, LX/2EL;->a$redex0(LX/2EL;LX/2EU;)V

    .line 385351
    invoke-virtual {p0}, LX/2EL;->e()V

    .line 385352
    const/16 v0, 0x2710

    invoke-virtual {p0, v0}, LX/2EL;->a(I)V

    .line 385353
    return-void

    .line 385354
    :pswitch_0
    sget-object v0, LX/2EU;->CHANNEL_CONNECTED:LX/2EU;

    goto :goto_0

    .line 385355
    :pswitch_1
    sget-object v0, LX/2EU;->CHANNEL_CONNECTING:LX/2EU;

    goto :goto_0

    .line 385356
    :pswitch_2
    sget-object v0, LX/2EU;->CHANNEL_DISCONNECTED:LX/2EU;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
