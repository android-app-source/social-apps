.class public LX/3BT;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final d:Landroid/net/Uri;


# instance fields
.field public final a:LX/31X;

.field public final b:Ljava/lang/CharSequence;

.field public final c:Ljava/lang/CharSequence;

.field public final e:Landroid/content/Context;

.field private final f:LX/31c;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 528526
    const-string v0, "https://www.facebook.com/maps/report/?"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/3BT;->d:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 528527
    const-string v2, "Open"

    const-string v3, "This map is operated by third-party providers. You will be redirected to them to provide feedback."

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, LX/3BT;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;LX/31X;LX/31c;)V

    .line 528528
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;LX/31X;LX/31c;)V
    .locals 0
    .param p4    # LX/31X;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/31c;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 528529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 528530
    iput-object p1, p0, LX/3BT;->e:Landroid/content/Context;

    .line 528531
    iput-object p2, p0, LX/3BT;->b:Ljava/lang/CharSequence;

    .line 528532
    iput-object p3, p0, LX/3BT;->c:Ljava/lang/CharSequence;

    .line 528533
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, LX/3BT;->a:LX/31X;

    .line 528534
    if-eqz p5, :cond_1

    :goto_1
    iput-object p5, p0, LX/3BT;->f:LX/31c;

    .line 528535
    return-void

    .line 528536
    :cond_0
    new-instance p1, LX/688;

    invoke-direct {p1, p0}, LX/688;-><init>(LX/3BT;)V

    move-object p4, p1

    .line 528537
    goto :goto_0

    .line 528538
    :cond_1
    new-instance p1, LX/689;

    invoke-direct {p1, p0}, LX/689;-><init>(LX/3BT;)V

    move-object p5, p1

    .line 528539
    goto :goto_1
.end method

.method public static a$redex0(LX/3BT;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 528540
    sget-object v0, LX/31U;->x:LX/31U;

    new-instance v1, LX/687;

    invoke-direct {v1, p0, p2, p1}, LX/687;-><init>(LX/3BT;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, LX/31U;->a(Ljava/util/Map;)V

    .line 528541
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/net/Uri;I)V
    .locals 3

    .prologue
    .line 528542
    iget-object v0, p0, LX/3BT;->a:LX/31X;

    iget-object v1, p0, LX/3BT;->c:Ljava/lang/CharSequence;

    invoke-interface {v0, v1}, LX/31X;->b(Ljava/lang/CharSequence;)LX/31X;

    move-result-object v0

    iget-object v1, p0, LX/3BT;->b:Ljava/lang/CharSequence;

    new-instance v2, LX/685;

    invoke-direct {v2, p0, p2}, LX/685;-><init>(LX/3BT;Landroid/net/Uri;)V

    invoke-interface {v0, v1, v2}, LX/31X;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/31X;

    move-result-object v0

    iget-object v1, p0, LX/3BT;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/684;

    invoke-direct {v2, p0, p2}, LX/684;-><init>(LX/3BT;Landroid/net/Uri;)V

    invoke-interface {v0, v1, v2}, LX/31X;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/31X;

    move-result-object v0

    invoke-interface {v0}, LX/31X;->a()Landroid/app/Dialog;

    move-result-object v0

    .line 528543
    new-instance v1, LX/686;

    invoke-direct {v1, p0, p2}, LX/686;-><init>(LX/3BT;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 528544
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 528545
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 528546
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/3BT;->d:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "static_map_url"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 528547
    iget-object v1, p0, LX/3BT;->f:LX/31c;

    invoke-interface {v1, v0}, LX/31c;->a(Landroid/content/Intent;)V

    .line 528548
    return-void
.end method
