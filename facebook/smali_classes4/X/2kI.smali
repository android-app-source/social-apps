.class public final LX/2kI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2kJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/2kJ",
        "<TTEdge;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2kJ",
            "<TTEdge;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 455936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 455937
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/2kI;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 2
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<TTEdge;>;",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 455938
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 455939
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be run on UI thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 455940
    :cond_0
    iget-object v0, p0, LX/2kI;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2kJ;

    .line 455941
    invoke-interface {v0, p1, p2, p3, p4}, LX/2kJ;->a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V

    goto :goto_0

    .line 455942
    :cond_1
    return-void
.end method

.method public final a(LX/2kJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kJ",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 455943
    if-nez p1, :cond_0

    .line 455944
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 455945
    :cond_0
    iget-object v0, p0, LX/2kI;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 455946
    return-void
.end method

.method public final a(LX/2kM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 455947
    iget-object v0, p0, LX/2kI;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2kJ;

    .line 455948
    invoke-interface {v0, p1}, LX/2kJ;->a(LX/2kM;)V

    goto :goto_0

    .line 455949
    :cond_0
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;)V
    .locals 2
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 455950
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 455951
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be run on UI thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 455952
    :cond_0
    iget-object v0, p0, LX/2kI;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2kJ;

    .line 455953
    invoke-interface {v0, p1, p2}, LX/2kJ;->a(LX/2nj;LX/3DP;)V

    goto :goto_0

    .line 455954
    :cond_1
    return-void
.end method

.method public final b(LX/2kJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kJ",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 455955
    if-nez p1, :cond_0

    .line 455956
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 455957
    :cond_0
    iget-object v0, p0, LX/2kI;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 455958
    return-void
.end method
