.class public LX/32q;
.super LX/320;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _cfgEmptyStringsAsObjects:Z

.field public _constructorArguments:[LX/4q3;

.field public _defaultCreator:LX/2Au;

.field public _delegateArguments:[LX/4q3;

.field public _delegateCreator:LX/2Au;

.field public _delegateType:LX/0lJ;

.field public _fromBooleanCreator:LX/2Au;

.field public _fromDoubleCreator:LX/2Au;

.field public _fromIntCreator:LX/2Au;

.field public _fromLongCreator:LX/2Au;

.field public _fromStringCreator:LX/2Au;

.field public _incompleteParameter:LX/2Vd;

.field public final _valueTypeDesc:Ljava/lang/String;

.field public _withArgsCreator:LX/2Au;


# direct methods
.method public constructor <init>(LX/0mu;LX/0lJ;)V
    .locals 1

    .prologue
    .line 491120
    invoke-direct {p0}, LX/320;-><init>()V

    .line 491121
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, LX/32q;->_cfgEmptyStringsAsObjects:Z

    .line 491122
    if-nez p2, :cond_1

    const-string v0, "UNKNOWN TYPE"

    :goto_1
    iput-object v0, p0, LX/32q;->_valueTypeDesc:Ljava/lang/String;

    .line 491123
    return-void

    .line 491124
    :cond_0
    sget-object v0, LX/0mv;->ACCEPT_EMPTY_STRING_AS_NULL_OBJECT:LX/0mv;

    invoke-virtual {p1, v0}, LX/0mu;->c(LX/0mv;)Z

    move-result v0

    goto :goto_0

    .line 491125
    :cond_1
    invoke-virtual {p2}, LX/0lJ;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Ljava/lang/Throwable;)LX/28E;
    .locals 4

    .prologue
    .line 491193
    move-object v0, p1

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 491194
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0

    .line 491195
    :cond_0
    instance-of v1, v0, LX/28E;

    if-eqz v1, :cond_1

    .line 491196
    check-cast v0, LX/28E;

    .line 491197
    :goto_1
    return-object v0

    :cond_1
    new-instance v1, LX/28E;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Instantiation of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/28E;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_1
.end method

.method private b(LX/0n3;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 491183
    iget-object v0, p0, LX/32q;->_fromBooleanCreator:LX/2Au;

    if-eqz v0, :cond_1

    .line 491184
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 491185
    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 491186
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/320;->a(Z)Ljava/lang/Object;

    move-result-object v0

    .line 491187
    :goto_0
    return-object v0

    .line 491188
    :cond_0
    const-string v1, "false"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 491189
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/320;->a(Z)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 491190
    :cond_1
    iget-boolean v0, p0, LX/32q;->_cfgEmptyStringsAsObjects:Z

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 491191
    const/4 v0, 0x0

    goto :goto_0

    .line 491192
    :cond_2
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not instantiate value of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from String value; no single-String constructor/factory method"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(D)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 491176
    :try_start_0
    iget-object v0, p0, LX/32q;->_fromDoubleCreator:LX/2Au;

    if-eqz v0, :cond_0

    .line 491177
    iget-object v0, p0, LX/32q;->_fromDoubleCreator:LX/2Au;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Au;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 491178
    :catch_0
    move-exception v0

    .line 491179
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 491180
    :catch_1
    move-exception v0

    .line 491181
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 491182
    :cond_0
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not instantiate value of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from Floating-point number; no one-double/Double-arg constructor/factory method"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 491166
    :try_start_0
    iget-object v0, p0, LX/32q;->_fromIntCreator:LX/2Au;

    if-eqz v0, :cond_0

    .line 491167
    iget-object v0, p0, LX/32q;->_fromIntCreator:LX/2Au;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Au;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 491168
    :goto_0
    return-object v0

    .line 491169
    :cond_0
    iget-object v0, p0, LX/32q;->_fromLongCreator:LX/2Au;

    if-eqz v0, :cond_1

    .line 491170
    iget-object v0, p0, LX/32q;->_fromLongCreator:LX/2Au;

    int-to-long v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Au;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 491171
    :catch_0
    move-exception v0

    .line 491172
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 491173
    :catch_1
    move-exception v0

    .line 491174
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 491175
    :cond_1
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not instantiate value of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from Integral number; no single-int-arg constructor/factory method"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(J)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 491159
    :try_start_0
    iget-object v0, p0, LX/32q;->_fromLongCreator:LX/2Au;

    if-eqz v0, :cond_0

    .line 491160
    iget-object v0, p0, LX/32q;->_fromLongCreator:LX/2Au;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Au;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 491161
    :catch_0
    move-exception v0

    .line 491162
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 491163
    :catch_1
    move-exception v0

    .line 491164
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 491165
    :cond_0
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not instantiate value of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from Long integral number; no single-long-arg constructor/factory method"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 491141
    iget-object v0, p0, LX/32q;->_delegateCreator:LX/2Au;

    if-nez v0, :cond_0

    .line 491142
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No delegate constructor for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 491143
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/32q;->_delegateArguments:[LX/4q3;

    if-nez v0, :cond_1

    .line 491144
    iget-object v0, p0, LX/32q;->_delegateCreator:LX/2Au;

    invoke-virtual {v0, p2}, LX/2Au;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 491145
    :goto_0
    return-object v0

    .line 491146
    :cond_1
    iget-object v0, p0, LX/32q;->_delegateArguments:[LX/4q3;

    array-length v1, v0

    .line 491147
    new-array v2, v1, [Ljava/lang/Object;

    .line 491148
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_3

    .line 491149
    iget-object v3, p0, LX/32q;->_delegateArguments:[LX/4q3;

    aget-object v3, v3, v0

    .line 491150
    if-nez v3, :cond_2

    .line 491151
    aput-object p2, v2, v0

    .line 491152
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 491153
    :cond_2
    invoke-virtual {v3}, LX/32s;->d()Ljava/lang/Object;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v3, v5}, LX/0n3;->a(Ljava/lang/Object;LX/2Ay;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0
    :try_end_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 491154
    :catch_0
    move-exception v0

    .line 491155
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 491156
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/32q;->_delegateCreator:LX/2Au;

    invoke-virtual {v0, v2}, LX/2Au;->a([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 491157
    :catch_1
    move-exception v0

    .line 491158
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public final a(LX/0n3;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 491133
    iget-object v0, p0, LX/32q;->_fromStringCreator:LX/2Au;

    if-eqz v0, :cond_0

    .line 491134
    :try_start_0
    iget-object v0, p0, LX/32q;->_fromStringCreator:LX/2Au;

    invoke-virtual {v0, p2}, LX/2Au;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 491135
    :goto_0
    return-object v0

    .line 491136
    :catch_0
    move-exception v0

    .line 491137
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 491138
    :catch_1
    move-exception v0

    .line 491139
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 491140
    :cond_0
    invoke-direct {p0, p1, p2}, LX/32q;->b(LX/0n3;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Z)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 491126
    :try_start_0
    iget-object v0, p0, LX/32q;->_fromBooleanCreator:LX/2Au;

    if-eqz v0, :cond_0

    .line 491127
    iget-object v0, p0, LX/32q;->_fromBooleanCreator:LX/2Au;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Au;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 491128
    :catch_0
    move-exception v0

    .line 491129
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 491130
    :catch_1
    move-exception v0

    .line 491131
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 491132
    :cond_0
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not instantiate value of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from Boolean value; no single-boolean/Boolean-arg constructor/factory method"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 491104
    iget-object v0, p0, LX/32q;->_withArgsCreator:LX/2Au;

    if-nez v0, :cond_0

    .line 491105
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No with-args constructor for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 491106
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/32q;->_withArgsCreator:LX/2Au;

    invoke-virtual {v0, p1}, LX/2Au;->a([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 491107
    :catch_0
    move-exception v0

    .line 491108
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 491109
    :catch_1
    move-exception v0

    .line 491110
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 491119
    iget-object v0, p0, LX/32q;->_valueTypeDesc:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/2Au;LX/2Au;LX/0lJ;[LX/4q3;LX/2Au;[LX/4q3;)V
    .locals 0

    .prologue
    .line 491112
    iput-object p1, p0, LX/32q;->_defaultCreator:LX/2Au;

    .line 491113
    iput-object p2, p0, LX/32q;->_delegateCreator:LX/2Au;

    .line 491114
    iput-object p3, p0, LX/32q;->_delegateType:LX/0lJ;

    .line 491115
    iput-object p4, p0, LX/32q;->_delegateArguments:[LX/4q3;

    .line 491116
    iput-object p5, p0, LX/32q;->_withArgsCreator:LX/2Au;

    .line 491117
    iput-object p6, p0, LX/32q;->_constructorArguments:[LX/4q3;

    .line 491118
    return-void
.end method

.method public final a(LX/0mu;)[LX/32s;
    .locals 1

    .prologue
    .line 491111
    iget-object v0, p0, LX/32q;->_constructorArguments:[LX/4q3;

    check-cast v0, [LX/32s;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 491198
    iget-object v0, p0, LX/32q;->_fromStringCreator:LX/2Au;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 491086
    iget-object v0, p0, LX/32q;->_fromIntCreator:LX/2Au;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 491087
    iget-object v0, p0, LX/32q;->_fromLongCreator:LX/2Au;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 491088
    iget-object v0, p0, LX/32q;->_fromDoubleCreator:LX/2Au;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 491089
    iget-object v0, p0, LX/32q;->_fromBooleanCreator:LX/2Au;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 491090
    iget-object v0, p0, LX/32q;->_defaultCreator:LX/2Au;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 491091
    iget-object v0, p0, LX/32q;->_delegateType:LX/0lJ;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 491092
    iget-object v0, p0, LX/32q;->_withArgsCreator:LX/2Au;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/0lJ;
    .locals 1

    .prologue
    .line 491093
    iget-object v0, p0, LX/32q;->_delegateType:LX/0lJ;

    return-object v0
.end method

.method public final l()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 491094
    iget-object v0, p0, LX/32q;->_defaultCreator:LX/2Au;

    if-nez v0, :cond_0

    .line 491095
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No default constructor for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 491096
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/32q;->_defaultCreator:LX/2Au;

    invoke-virtual {v0}, LX/2Au;->h()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 491097
    :catch_0
    move-exception v0

    .line 491098
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 491099
    :catch_1
    move-exception v0

    .line 491100
    invoke-direct {p0, v0}, LX/32q;->a(Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public final m()LX/2Au;
    .locals 1

    .prologue
    .line 491101
    iget-object v0, p0, LX/32q;->_defaultCreator:LX/2Au;

    return-object v0
.end method

.method public final n()LX/2Au;
    .locals 1

    .prologue
    .line 491102
    iget-object v0, p0, LX/32q;->_delegateCreator:LX/2Au;

    return-object v0
.end method

.method public final o()LX/2Vd;
    .locals 1

    .prologue
    .line 491103
    iget-object v0, p0, LX/32q;->_incompleteParameter:LX/2Vd;

    return-object v0
.end method
