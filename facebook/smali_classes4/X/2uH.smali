.class public final LX/2uH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AlW;


# instance fields
.field public final synthetic a:LX/1RS;


# direct methods
.method public constructor <init>(LX/1RS;)V
    .locals 0

    .prologue
    .line 475651
    iput-object p1, p0, LX/2uH;->a:LX/1RS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 475658
    iget-object v0, p0, LX/2uH;->a:LX/1RS;

    iget-object v0, v0, LX/1RS;->a:Landroid/content/res/Resources;

    const v1, 0x7f08289b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 475657
    iget-object v0, p0, LX/2uH;->a:LX/1RS;

    iget-object v0, v0, LX/1RS;->a:Landroid/content/res/Resources;

    const v1, 0x7f08289c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 475656
    iget-object v0, p0, LX/2uH;->a:LX/1RS;

    iget-object v0, v0, LX/1RS;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a00a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 475659
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 475655
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 475654
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()LX/AkM;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 475653
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Lcom/facebook/productionprompts/model/PromptDisplayReason;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 475652
    const/4 v0, 0x0

    return-object v0
.end method
