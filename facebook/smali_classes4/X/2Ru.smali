.class public LX/2Ru;
.super LX/2QZ;
.source ""


# instance fields
.field private b:LX/2QQ;


# direct methods
.method public constructor <init>(LX/2QQ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410323
    const-string v0, "platform_webdialogs_load_manifest"

    invoke-direct {p0, v0}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 410324
    iput-object p1, p0, LX/2Ru;->b:LX/2QQ;

    .line 410325
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 410326
    iget-object v0, p0, LX/2Ru;->b:LX/2QQ;

    .line 410327
    iget-object v1, v0, LX/2QQ;->t:LX/0Sh;

    const-string v2, "This method will perform disk I/O and should not be called on the UI thread"

    invoke-virtual {v1, v2}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 410328
    const/4 v1, 0x0

    .line 410329
    iget-object v2, v0, LX/2QQ;->p:LX/2QR;

    sget-object v3, LX/2QS;->i:LX/0Tn;

    const-string p0, "PlatformWebDialogsManifest"

    const-class p1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;

    invoke-virtual {v2, v3, p0, p1}, LX/2QR;->a(LX/0Tn;Ljava/lang/String;Ljava/lang/Class;)Landroid/util/Pair;

    move-result-object v2

    .line 410330
    if-eqz v2, :cond_0

    .line 410331
    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;

    .line 410332
    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->a()Ljava/lang/Iterable;

    move-result-object v2

    if-nez v2, :cond_0

    .line 410333
    iget-object v2, v0, LX/2QQ;->p:LX/2QR;

    invoke-virtual {v2}, LX/2QR;->c()V

    .line 410334
    iget-object v2, v0, LX/2QQ;->r:LX/03V;

    const-string v3, "PlatformWebDialogsManifest"

    const-string p0, "Deserialized manifest had NULL actionManifests."

    invoke-virtual {v2, v3, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 410335
    :cond_0
    move-object v0, v1

    .line 410336
    if-eqz v0, :cond_1

    .line 410337
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 410338
    :goto_0
    return-object v0

    .line 410339
    :cond_1
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 410340
    goto :goto_0
.end method
