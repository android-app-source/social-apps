.class public LX/2VH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/2VI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 417265
    const-class v0, LX/2VH;

    sput-object v0, LX/2VH;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2VI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 417266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417267
    iput-object p1, p0, LX/2VH;->b:LX/2VI;

    .line 417268
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 417269
    if-eqz p2, :cond_0

    .line 417270
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    invoke-direct {v0, p1, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417271
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 417272
    check-cast p1, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;

    .line 417273
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 417274
    const-string v1, "experiment"

    .line 417275
    iget-object v2, p1, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 417276
    invoke-static {v2}, LX/2VI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/2VH;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 417277
    const-string v1, "hash"

    .line 417278
    iget-object v2, p1, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 417279
    invoke-static {v0, v1, v2}, LX/2VH;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 417280
    const-string v1, "log_event"

    .line 417281
    iget-object v2, p1, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 417282
    invoke-static {v0, v1, v2}, LX/2VH;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 417283
    const-string v1, "log_event_name"

    .line 417284
    iget-object v2, p1, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 417285
    invoke-static {v0, v1, v2}, LX/2VH;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 417286
    const-string v1, "log_data"

    .line 417287
    iget-object v2, p1, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->f:Ljava/lang/String;

    move-object v2, v2

    .line 417288
    invoke-static {v0, v1, v2}, LX/2VH;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 417289
    const-string v1, "/me/test_experiments"

    move-object v1, v1

    .line 417290
    new-instance v2, LX/14O;

    invoke-direct {v2}, LX/14O;-><init>()V

    const-string v3, "log_to_qe"

    .line 417291
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 417292
    move-object v2, v2

    .line 417293
    iput-object v1, v2, LX/14O;->d:Ljava/lang/String;

    .line 417294
    move-object v1, v2

    .line 417295
    const-string v2, "POST"

    .line 417296
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 417297
    move-object v1, v1

    .line 417298
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 417299
    move-object v0, v1

    .line 417300
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 417301
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 417302
    move-object v0, v0

    .line 417303
    sget-object v1, LX/14Q;->FALLBACK_REQUIRED:LX/14Q;

    .line 417304
    iput-object v1, v0, LX/14O;->v:LX/14Q;

    .line 417305
    move-object v0, v0

    .line 417306
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 417307
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 417308
    const/4 v0, 0x0

    return-object v0
.end method
