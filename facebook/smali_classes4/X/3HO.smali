.class public LX/3HO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/3HO;


# instance fields
.field private b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/3As;

.field private final d:LX/0aG;

.field private final e:Z

.field public f:I

.field public g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 543393
    const-class v0, LX/3HO;

    sput-object v0, LX/3HO;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/3As;LX/0aG;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/common/build/IsInternalBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/3As;",
            "LX/0aG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 543394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 543395
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/3HO;->b:LX/0am;

    .line 543396
    iput-object p3, p0, LX/3HO;->d:LX/0aG;

    .line 543397
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/3HO;->e:Z

    .line 543398
    iput-object p2, p0, LX/3HO;->c:LX/3As;

    .line 543399
    return-void
.end method

.method public static a(LX/0QB;)LX/3HO;
    .locals 6

    .prologue
    .line 543400
    sget-object v0, LX/3HO;->h:LX/3HO;

    if-nez v0, :cond_1

    .line 543401
    const-class v1, LX/3HO;

    monitor-enter v1

    .line 543402
    :try_start_0
    sget-object v0, LX/3HO;->h:LX/3HO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 543403
    if-eqz v2, :cond_0

    .line 543404
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 543405
    new-instance v5, LX/3HO;

    const/16 v3, 0x1466

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/3As;->b(LX/0QB;)LX/3As;

    move-result-object v3

    check-cast v3, LX/3As;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-direct {v5, p0, v3, v4}, LX/3HO;-><init>(LX/0Or;LX/3As;LX/0aG;)V

    .line 543406
    move-object v0, v5

    .line 543407
    sput-object v0, LX/3HO;->h:LX/3HO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 543408
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 543409
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 543410
    :cond_1
    sget-object v0, LX/3HO;->h:LX/3HO;

    return-object v0

    .line 543411
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 543412
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/story/FetchSingleStoryResult;)Lcom/facebook/api/story/FetchSingleStoryResult;
    .locals 11

    .prologue
    .line 543413
    iget-boolean v0, p0, LX/3HO;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3HO;->c:LX/3As;

    invoke-virtual {v0}, LX/3As;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 543414
    :cond_0
    :goto_0
    return-object p1

    .line 543415
    :cond_1
    iget-object v6, p0, LX/3HO;->c:LX/3As;

    .line 543416
    iget-object v7, v6, LX/3As;->c:LX/0Px;

    move-object v8, v7

    .line 543417
    const/4 v6, 0x0

    move v7, v6

    :goto_1
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_4

    .line 543418
    iget v6, p0, LX/3HO;->g:I

    add-int/lit8 v9, v6, 0x1

    iput v9, p0, LX/3HO;->g:I

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    rem-int/2addr v6, v9

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/FeedUnit;

    .line 543419
    instance-of v9, v6, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v9, :cond_3

    .line 543420
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v6}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v6

    iget v7, p0, LX/3HO;->f:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, LX/3HO;->f:I

    int-to-long v8, v7

    .line 543421
    iput-wide v8, v6, LX/23u;->G:J

    .line 543422
    move-object v6, v6

    .line 543423
    invoke-virtual {v6}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    invoke-static {v6}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    .line 543424
    :goto_2
    move-object v0, v6

    .line 543425
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_2

    .line 543426
    sget-object v0, LX/3HO;->a:Ljava/lang/Class;

    const-string v1, "None of the permalink injection stories are GraphQLStory type, not injecting"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 543427
    :cond_2
    new-instance v1, Lcom/facebook/api/story/FetchSingleStoryResult;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 543428
    iget-object v2, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 543429
    iget-wide v6, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v6

    .line 543430
    invoke-direct {v1, v0, v2, v4, v5}, Lcom/facebook/api/story/FetchSingleStoryResult;-><init>(Lcom/facebook/graphql/model/GraphQLStory;LX/0ta;J)V

    move-object p1, v1

    goto :goto_0

    .line 543431
    :cond_3
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1

    .line 543432
    :cond_4
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v6

    goto :goto_2
.end method
