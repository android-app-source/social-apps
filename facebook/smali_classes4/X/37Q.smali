.class public LX/37Q;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 501028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501029
    return-void
.end method

.method public static a(Landroid/content/Context;LX/37O;)I
    .locals 1

    .prologue
    .line 501030
    sget-object v0, LX/37O;->AVAILABLE:LX/37O;

    if-ne p1, v0, :cond_0

    .line 501031
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0a0158

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v0, v0

    .line 501032
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, LX/37Q;->b(Landroid/content/Context;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/DDk;Lcom/facebook/widget/text/BetterTextView;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 501033
    sget-object v0, LX/DDx;->a:[I

    invoke-virtual {p0}, LX/DDk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 501034
    const v0, 0x7f021805

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setBackgroundResource(I)V

    .line 501035
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 501036
    :goto_0
    return-void

    .line 501037
    :pswitch_0
    const v0, 0x7f021804

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setBackgroundResource(I)V

    .line 501038
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;LX/37O;Landroid/text/SpannableStringBuilder;Landroid/content/Context;LX/34u;)V
    .locals 3

    .prologue
    .line 501039
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gY()Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    move-result-object v0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->NEGOTIABLE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    if-ne v0, p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p4, v1, v0}, LX/34u;->a(Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;Z)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 501040
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    .line 501041
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p4, 0x7f0a00d4

    invoke-virtual {p1, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    move p1, p1

    .line 501042
    invoke-direct {v1, p1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v0, v1, p2}, LX/37Q;->a(Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/text/SpannableStringBuilder;)V

    .line 501043
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 501044
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 501045
    :cond_0
    const/4 v0, 0x0

    .line 501046
    :goto_1
    move-object v0, v0

    .line 501047
    if-eqz v0, :cond_1

    .line 501048
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082508

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 501049
    const-string v2, " "

    new-instance p0, Landroid/text/style/ForegroundColorSpan;

    invoke-static {p3}, LX/37Q;->b(Landroid/content/Context;)I

    move-result p1

    invoke-direct {p0, p1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v2, p0, p2}, LX/37Q;->a(Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/text/SpannableStringBuilder;)V

    .line 501050
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-static {p3}, LX/37Q;->b(Landroid/content/Context;)I

    move-result p0

    invoke-direct {v2, p0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v1, v2, p2}, LX/37Q;->a(Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/text/SpannableStringBuilder;)V

    .line 501051
    const-string v1, " "

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-static {p3}, LX/37Q;->b(Landroid/content/Context;)I

    move-result p0

    invoke-direct {v2, p0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v1, v2, p2}, LX/37Q;->a(Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/text/SpannableStringBuilder;)V

    .line 501052
    const-string v1, " "

    const/4 p4, 0x0

    .line 501053
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f020967

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 501054
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result p0

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result p1

    invoke-virtual {v2, p4, p4, p0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 501055
    new-instance p0, Landroid/text/style/ImageSpan;

    const/4 p1, 0x1

    invoke-direct {p0, v2, p1}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    move-object v2, p0

    .line 501056
    invoke-static {v1, v2, p2}, LX/37Q;->a(Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/text/SpannableStringBuilder;)V

    .line 501057
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-static {p3}, LX/37Q;->b(Landroid/content/Context;)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v0, v1, p2}, LX/37Q;->a(Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/text/SpannableStringBuilder;)V

    .line 501058
    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/text/SpannableStringBuilder;)V
    .locals 3

    .prologue
    .line 501059
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 501060
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v1, v0

    .line 501061
    invoke-virtual {p2, p0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 501062
    const/16 v2, 0x21

    invoke-virtual {p2, p1, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 501063
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z
    .locals 2

    .prologue
    .line 501064
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 501065
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method
