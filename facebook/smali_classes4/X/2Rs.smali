.class public LX/2Rs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/2Rs;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/03V;

.field private final e:LX/0lC;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/03V;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0Or;LX/0Or;LX/0lC;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/UserAgentString;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0lC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410230
    iput-object p1, p0, LX/2Rs;->a:Landroid/content/Context;

    .line 410231
    iput-object p2, p0, LX/2Rs;->d:LX/03V;

    .line 410232
    iput-object p3, p0, LX/2Rs;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 410233
    iput-object p4, p0, LX/2Rs;->c:LX/0Or;

    .line 410234
    iput-object p6, p0, LX/2Rs;->e:LX/0lC;

    .line 410235
    iput-object p5, p0, LX/2Rs;->f:LX/0Or;

    .line 410236
    return-void
.end method

.method public static a(Lorg/apache/http/HttpResponse;)LX/1nY;
    .locals 3

    .prologue
    .line 410237
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    .line 410238
    if-nez v1, :cond_1

    .line 410239
    sget-object v0, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    .line 410240
    :cond_0
    :goto_0
    return-object v0

    .line 410241
    :cond_1
    sget-object v0, LX/1nY;->NO_ERROR:LX/1nY;

    .line 410242
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 410243
    const/16 v2, 0xc8

    if-eq v1, v2, :cond_0

    .line 410244
    sget-object v0, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    .line 410245
    const/16 v2, 0x190

    if-lt v1, v2, :cond_0

    const/16 v2, 0x257

    if-gt v1, v2, :cond_0

    .line 410246
    const/16 v0, 0x1f3

    if-gt v1, v0, :cond_2

    sget-object v0, LX/1nY;->HTTP_400_OTHER:LX/1nY;

    goto :goto_0

    :cond_2
    sget-object v0, LX/1nY;->HTTP_500_CLASS:LX/1nY;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2Rs;
    .locals 10

    .prologue
    .line 410247
    sget-object v0, LX/2Rs;->h:LX/2Rs;

    if-nez v0, :cond_1

    .line 410248
    const-class v1, LX/2Rs;

    monitor-enter v1

    .line 410249
    :try_start_0
    sget-object v0, LX/2Rs;->h:LX/2Rs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 410250
    if-eqz v2, :cond_0

    .line 410251
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 410252
    new-instance v3, LX/2Rs;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v6

    check-cast v6, Lcom/facebook/http/common/FbHttpRequestProcessor;

    const/16 v7, 0x19e

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x15f5

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v9

    check-cast v9, LX/0lC;

    invoke-direct/range {v3 .. v9}, LX/2Rs;-><init>(Landroid/content/Context;LX/03V;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0Or;LX/0Or;LX/0lC;)V

    .line 410253
    move-object v0, v3

    .line 410254
    sput-object v0, LX/2Rs;->h:LX/2Rs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410255
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 410256
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 410257
    :cond_1
    sget-object v0, LX/2Rs;->h:LX/2Rs;

    return-object v0

    .line 410258
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 410259
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Lorg/apache/http/client/ResponseHandler;)Lorg/apache/http/HttpResponse;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Lorg/apache/http/client/ResponseHandler",
            "<",
            "Lorg/apache/http/HttpResponse;",
            ">;)",
            "Lorg/apache/http/HttpResponse;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 410260
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 410261
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 410262
    const/16 v0, 0x23

    invoke-static {v0}, LX/2Cb;->on(C)LX/2Cb;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    .line 410263
    invoke-static {v0, v1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 410264
    iget-object v2, p2, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v4, v2

    .line 410265
    new-instance v5, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v5, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 410266
    iget-object v0, p0, LX/2Rs;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 410267
    if-eqz v0, :cond_1

    .line 410268
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v0, v2

    .line 410269
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 410270
    iget-object v2, p0, LX/2Rs;->e:LX/0lC;

    invoke-static {v2, v0}, Lcom/facebook/auth/credentials/SessionCookie;->a(LX/0lC;Ljava/lang/String;)LX/0Px;

    move-result-object v6

    .line 410271
    if-eqz v6, :cond_4

    .line 410272
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 410273
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    move v2, v3

    :goto_0
    if-ge v2, v8, :cond_0

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/SessionCookie;

    .line 410274
    invoke-virtual {v0}, Lcom/facebook/auth/credentials/SessionCookie;->toString()Ljava/lang/String;

    move-result-object v0

    .line 410275
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, ";"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410276
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 410277
    :cond_0
    const-string v0, "Cookie"

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 410278
    :cond_1
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v0, p0, LX/2Rs;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 410279
    iget-object v0, p0, LX/2Rs;->g:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 410280
    iget-object v0, p0, LX/2Rs;->a:Landroid/content/Context;

    .line 410281
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x11

    if-lt v6, v7, :cond_6

    .line 410282
    :try_start_0
    invoke-static {v0}, Landroid/webkit/WebSettings;->getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 410283
    :goto_2
    move-object v6, v6

    .line 410284
    :goto_3
    move-object v0, v6

    .line 410285
    iput-object v0, p0, LX/2Rs;->g:Ljava/lang/String;

    .line 410286
    :cond_2
    iget-object v0, p0, LX/2Rs;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 410287
    const-string v0, " "

    invoke-virtual {v2, v3, v0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 410288
    iget-object v0, p0, LX/2Rs;->g:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 410289
    :cond_3
    const-string v0, "User-Agent"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 410290
    :try_start_1
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v0

    .line 410291
    iput-object v4, v0, LX/15E;->c:Ljava/lang/String;

    .line 410292
    move-object v0, v0

    .line 410293
    iput-object p2, v0, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 410294
    move-object v0, v0

    .line 410295
    iput-object v5, v0, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 410296
    move-object v0, v0

    .line 410297
    iput-object p3, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 410298
    move-object v0, v0

    .line 410299
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    .line 410300
    iget-object v2, p0, LX/2Rs;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v2, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/HttpResponse;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 410301
    :goto_4
    return-object v0

    .line 410302
    :cond_4
    iget-object v2, p0, LX/2Rs;->d:LX/03V;

    const-string v6, "Unable to de-serialize SessionCookie list from: %s"

    invoke-static {v6, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 410303
    :cond_5
    iget-object v0, p0, LX/2Rs;->d:LX/03V;

    const-string v2, "ViewerContext does not have a cookie string"

    invoke-virtual {v0, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 410304
    :catch_0
    move-exception v0

    .line 410305
    const-string v2, "Network error when downloading manifest from url: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v3

    invoke-static {v4, v0, v2, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_4

    :cond_6
    const-string v6, "AppleWebKit/533.1"

    goto :goto_3

    :catch_1
    const-string v6, "AppleWebKit/533.1"

    goto :goto_2
.end method
