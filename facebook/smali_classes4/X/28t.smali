.class public LX/28t;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;

.field private static volatile d:LX/28t;


# instance fields
.field private final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 374913
    const-class v0, LX/28t;

    .line 374914
    sput-object v0, LX/28t;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/28t;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374911
    iput-object p1, p0, LX/28t;->c:LX/03V;

    .line 374912
    return-void
.end method

.method public static a(LX/0QB;)LX/28t;
    .locals 4

    .prologue
    .line 374915
    sget-object v0, LX/28t;->d:LX/28t;

    if-nez v0, :cond_1

    .line 374916
    const-class v1, LX/28t;

    monitor-enter v1

    .line 374917
    :try_start_0
    sget-object v0, LX/28t;->d:LX/28t;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 374918
    if-eqz v2, :cond_0

    .line 374919
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 374920
    new-instance p0, LX/28t;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/28t;-><init>(LX/03V;)V

    .line 374921
    move-object v0, p0

    .line 374922
    sput-object v0, LX/28t;->d:LX/28t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 374923
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 374924
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 374925
    :cond_1
    sget-object v0, LX/28t;->d:LX/28t;

    return-object v0

    .line 374926
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 374927
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 374897
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 374898
    if-eqz v0, :cond_1

    .line 374899
    :cond_0
    return-void

    .line 374900
    :cond_1
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 374901
    const-string v0, "com.facebook.auth.login"

    invoke-virtual {v1, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 374902
    invoke-static {p0}, LX/2Bb;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 374903
    array-length v4, v2

    .line 374904
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    .line 374905
    aget-object v5, v2, v0

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 374906
    aget-object v5, v2, v0

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 374907
    invoke-static {p0, v6}, LX/2Bb;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 374908
    :cond_2
    aget-object v5, v2, v0

    invoke-virtual {v1, v5, v6, v6}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 374909
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 374879
    monitor-enter p0

    .line 374880
    :try_start_0
    sget-boolean v1, LX/007;->j:Z

    move v1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 374881
    if-eqz v1, :cond_0

    .line 374882
    :goto_0
    monitor-exit p0

    return-object v0

    .line 374883
    :cond_0
    :try_start_1
    invoke-static {p1, p2}, Lcom/facebook/katana/platform/FacebookAuthenticationService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 374884
    if-nez v1, :cond_3

    .line 374885
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.facebook.auth.login"

    invoke-direct {v1, p2, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 374886
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 374887
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_2
    invoke-virtual {v2, v1, v3, v4}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 374888
    sget-object v1, LX/28t;->a:Ljava/lang/Class;

    const-string v2, "Unable to create account for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 374889
    :catch_0
    move-exception v1

    .line 374890
    :try_start_3
    sget-boolean v2, LX/007;->i:Z

    move v2, v2

    .line 374891
    if-eqz v2, :cond_2

    .line 374892
    sget-object v2, LX/28t;->a:Ljava/lang/Class;

    const-string v3, "Unable to create account for %s. This may be because you installed both product and beta versions of Facebook application. Before using the beta version, make sure to uninstall other Facebook installations. In addition, make sure to DISABLE the Facebook application on devices  that come with Facebook pre-installed. "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 374893
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 374894
    :cond_1
    :try_start_4
    const-string v2, "com.android.contacts"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v0, v1

    .line 374895
    :goto_1
    :try_start_5
    invoke-static {p1, p2}, LX/2Bb;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 374896
    :cond_2
    iget-object v2, p0, LX/28t;->c:LX/03V;

    sget-object v3, LX/28t;->b:Ljava/lang/String;

    const-string v4, "SecurityException trying to create an account"

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method
