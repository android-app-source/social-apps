.class public LX/2uL;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 475677
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 475678
    return-void
.end method

.method public static a(LX/1G9;Ljava/util/concurrent/ScheduledExecutorService;LX/1Ft;LX/1bw;Landroid/content/Context;)LX/4CK;
    .locals 4
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 475668
    sget-object v0, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v1, v0

    .line 475669
    new-instance v2, LX/1bt;

    invoke-interface {p2}, LX/1Ft;->c()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-direct {v2, v0}, LX/1bt;-><init>(Ljava/util/concurrent/Executor;)V

    .line 475670
    const-string v0, "activity"

    invoke-virtual {p4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 475671
    new-instance v3, LX/5NR;

    invoke-direct {v3, v2, v0, p3, v1}, LX/5NR;-><init>(LX/1bv;Landroid/app/ActivityManager;LX/1bw;LX/0So;)V

    .line 475672
    new-instance v0, LX/4CK;

    invoke-direct {v0, p0, v3, p1, v1}, LX/4CK;-><init>(LX/1G9;LX/1c2;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)V

    return-object v0
.end method

.method public static a(LX/1G9;Ljava/util/concurrent/ScheduledExecutorService;LX/1FZ;LX/1FE;LX/0W3;)LX/4CM;
    .locals 7
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 475674
    sget-object v0, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v3, v0

    .line 475675
    new-instance v6, LX/5NS;

    invoke-direct {v6, p4}, LX/5NS;-><init>(LX/0W3;)V

    .line 475676
    new-instance v0, LX/4CM;

    invoke-virtual {p3}, LX/1FE;->c()LX/1Fg;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, LX/4CM;-><init>(LX/1G9;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;LX/1FZ;LX/1Fg;LX/1Gd;)V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 475673
    return-void
.end method
