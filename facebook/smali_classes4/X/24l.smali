.class public final LX/24l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24m;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

.field private final b:LX/1bf;

.field private final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;LX/1bf;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    .line 367778
    iput-object p1, p0, LX/24l;->a:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367779
    iput-object p2, p0, LX/24l;->b:LX/1bf;

    .line 367780
    iput-object p3, p0, LX/24l;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 367781
    iget-object v0, p0, LX/24l;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, LX/24l;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 367782
    iput-object p4, p0, LX/24l;->e:LX/1Pb;

    .line 367783
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;ZI)V
    .locals 13
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedClass"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/24e;",
            ">(TV;ZI)V"
        }
    .end annotation

    .prologue
    .line 367776
    iget-object v0, p0, LX/24l;->a:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->g:LX/1eq;

    iget-object v1, p0, LX/24l;->a:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    invoke-static {v1}, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->b(Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;)LX/0ad;

    iget-object v1, p0, LX/24l;->a:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->f:LX/0bH;

    iget-object v2, p0, LX/24l;->a:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->i:LX/0yc;

    iget-object v3, p0, LX/24l;->a:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->l:LX/1CK;

    iget-object v4, p0, LX/24l;->a:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    iget-object v4, v4, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->d:LX/0Ot;

    check-cast p1, LX/24e;

    invoke-interface {p1}, LX/24e;->getPhotoAttachmentView()Landroid/view/View;

    move-result-object v5

    const/4 v7, 0x0

    iget-object v9, p0, LX/24l;->b:LX/1bf;

    iget-object v10, p0, LX/24l;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v11, p0, LX/24l;->e:LX/1Pb;

    sget-object v12, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    move/from16 v6, p3

    move v8, p2

    invoke-static/range {v0 .. v12}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(LX/1eq;LX/0bH;LX/0yc;LX/1CK;LX/0Ot;Landroid/view/View;ILX/1aZ;ZLX/1bf;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 367777
    return-void
.end method
