.class public LX/2Eo;
.super LX/2En;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public b:LX/2En;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 386017
    invoke-direct {p0}, LX/2En;-><init>()V

    .line 386018
    iput-object p1, p0, LX/2Eo;->a:Landroid/content/Context;

    .line 386019
    return-void
.end method

.method private declared-synchronized b()LX/2En;
    .locals 4
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 386020
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Eo;->b:LX/2En;

    if-nez v0, :cond_0

    .line 386021
    invoke-static {p0}, LX/2Eo;->d(LX/2Eo;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 386022
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 386023
    iget-object v1, p0, LX/2Eo;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/1od;->a(Landroid/content/Context;)I

    move-result v1

    .line 386024
    packed-switch v1, :pswitch_data_0

    .line 386025
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Falling back to non-GMS alarm scheduling due to connection result of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1od;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386026
    new-instance v0, LX/2Eq;

    iget-object v1, p0, LX/2Eo;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2Eq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/2Eo;->b:LX/2En;

    .line 386027
    :goto_0
    iget-object v0, p0, LX/2Eo;->b:LX/2En;

    move-object v0, v0

    .line 386028
    iput-object v0, p0, LX/2Eo;->b:LX/2En;

    .line 386029
    :cond_0
    iget-object v0, p0, LX/2Eo;->b:LX/2En;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 386030
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 386031
    :pswitch_0
    new-instance v0, LX/3zz;

    iget-object v1, p0, LX/2Eo;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/3zz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/2Eo;->b:LX/2En;

    goto :goto_0

    .line 386032
    :cond_1
    new-instance v0, LX/2Eq;

    iget-object v1, p0, LX/2Eo;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2Eq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/2Eo;->b:LX/2En;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static d(LX/2Eo;)Z
    .locals 3

    .prologue
    .line 386033
    iget-object v0, p0, LX/2Eo;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 386034
    iget-object v1, p0, LX/2Eo;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 386035
    const/16 v2, 0x80

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 386036
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    if-nez v1, :cond_1

    .line 386037
    :cond_0
    const/4 v0, 0x0

    .line 386038
    :goto_0
    return v0

    .line 386039
    :catch_0
    move-exception v0

    .line 386040
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 386041
    :cond_1
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    const-string v1, "com.google.android.gms.version"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 386042
    invoke-direct {p0}, LX/2Eo;->b()LX/2En;

    move-result-object v0

    invoke-virtual {v0}, LX/2En;->a()Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 386043
    invoke-direct {p0}, LX/2Eo;->b()LX/2En;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2En;->a(I)V

    .line 386044
    return-void
.end method

.method public final a(ILX/2DI;JJ)V
    .locals 9

    .prologue
    .line 386045
    invoke-direct {p0}, LX/2Eo;->b()LX/2En;

    move-result-object v1

    move v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-virtual/range {v1 .. v7}, LX/2En;->a(ILX/2DI;JJ)V

    .line 386046
    return-void
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 386047
    invoke-direct {p0}, LX/2Eo;->b()LX/2En;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2En;->b(I)J

    move-result-wide v0

    return-wide v0
.end method
