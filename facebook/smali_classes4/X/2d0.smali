.class public LX/2d0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final t:Ljava/lang/Object;


# instance fields
.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2dB;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Runnable;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dL;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CfW;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/ReactionUtil;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CIs;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/2cw;

.field private final k:LX/0bW;

.field public final l:LX/2d1;

.field private final m:LX/1vC;

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nD;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 442765
    const-class v0, LX/2d0;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2d0;->a:Ljava/lang/String;

    .line 442766
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2d0;->t:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;Ljava/util/concurrent/Executor;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2cw;LX/0bW;LX/2d1;LX/1vC;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2dL;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Ot",
            "<",
            "LX/CfW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/ReactionUtil;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CIs;",
            ">;",
            "LX/2cw;",
            "LX/0bW;",
            "LX/2d1;",
            "LX/1vC;",
            "LX/0Ot",
            "<",
            "LX/1nD;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 442745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442746
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/2d0;->b:Ljava/util/Set;

    .line 442747
    new-instance v0, Lcom/facebook/reaction/placetips/PlaceTipsReactionManager$1;

    invoke-direct {v0, p0}, Lcom/facebook/reaction/placetips/PlaceTipsReactionManager$1;-><init>(LX/2d0;)V

    iput-object v0, p0, LX/2d0;->c:Ljava/lang/Runnable;

    .line 442748
    iput-object v1, p0, LX/2d0;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 442749
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2d0;->r:LX/0am;

    .line 442750
    iput-object v1, p0, LX/2d0;->s:Ljava/lang/String;

    .line 442751
    iput-object p1, p0, LX/2d0;->d:LX/0Ot;

    .line 442752
    iput-object p2, p0, LX/2d0;->e:Ljava/util/concurrent/Executor;

    .line 442753
    iput-object p3, p0, LX/2d0;->f:LX/0Ot;

    .line 442754
    iput-object p4, p0, LX/2d0;->g:LX/0Ot;

    .line 442755
    iput-object p5, p0, LX/2d0;->h:LX/0Ot;

    .line 442756
    iput-object p6, p0, LX/2d0;->i:LX/0Ot;

    .line 442757
    iput-object p7, p0, LX/2d0;->j:LX/2cw;

    .line 442758
    iput-object p8, p0, LX/2d0;->k:LX/0bW;

    .line 442759
    iput-object p9, p0, LX/2d0;->l:LX/2d1;

    .line 442760
    iput-object p10, p0, LX/2d0;->m:LX/1vC;

    .line 442761
    iput-object p11, p0, LX/2d0;->n:LX/0Ot;

    .line 442762
    iput-object p12, p0, LX/2d0;->p:LX/0Ot;

    .line 442763
    iput-object p13, p0, LX/2d0;->o:LX/0Ot;

    .line 442764
    return-void
.end method

.method public static a(LX/0QB;)LX/2d0;
    .locals 7

    .prologue
    .line 442718
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 442719
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 442720
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 442721
    if-nez v1, :cond_0

    .line 442722
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 442723
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 442724
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 442725
    sget-object v1, LX/2d0;->t:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 442726
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 442727
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 442728
    :cond_1
    if-nez v1, :cond_4

    .line 442729
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 442730
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 442731
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/2d0;->b(LX/0QB;)LX/2d0;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 442732
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 442733
    if-nez v1, :cond_2

    .line 442734
    sget-object v0, LX/2d0;->t:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2d0;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 442735
    :goto_1
    if-eqz v0, :cond_3

    .line 442736
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 442737
    :goto_3
    check-cast v0, LX/2d0;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 442738
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 442739
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 442740
    :catchall_1
    move-exception v0

    .line 442741
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 442742
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 442743
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 442744
    :cond_2
    :try_start_8
    sget-object v0, LX/2d0;->t:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2d0;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(LX/2d0;Lcom/facebook/placetips/bootstrap/PresenceDescription;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .prologue
    .line 442705
    iget-object v0, p0, LX/2d0;->l:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->g()Z

    move-result v0

    move v0, v0

    .line 442706
    if-eqz v0, :cond_0

    .line 442707
    sget-object v0, LX/E6J;->a:[I

    invoke-virtual {p1}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->k()Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/placetips/bootstrap/PresenceSource;->a()LX/2cx;

    move-result-object v1

    invoke-virtual {v1}, LX/2cx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 442708
    const-string v0, "ANDROID_SEARCH_LOCAL_PLACE_TIPS"

    .line 442709
    :goto_0
    return-object v0

    .line 442710
    :pswitch_0
    const-string v0, "ANDROID_SEARCH_LOCAL_PLACE_TIPS_CHECKIN"

    goto :goto_0

    .line 442711
    :pswitch_1
    const-string v0, "ANDROID_SEARCH_LOCAL_PLACE_TIPS_LOCATION"

    goto :goto_0

    .line 442712
    :cond_0
    sget-object v0, LX/E6J;->a:[I

    invoke-virtual {p1}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->k()Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/placetips/bootstrap/PresenceSource;->a()LX/2cx;

    move-result-object v1

    invoke-virtual {v1}, LX/2cx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 442713
    const-string v0, "ANDROID_GPS_LOCATION_SUGGESTION"

    goto :goto_0

    .line 442714
    :pswitch_2
    const-string v0, "ANDROID_FEED_CHECKIN_SUGGESTION"

    goto :goto_0

    .line 442715
    :pswitch_3
    const-string v0, "ANDROID_GRAVITY_SUGGESTION"

    goto :goto_0

    .line 442716
    :pswitch_4
    const-string v0, "ANDROID_GPS_LOCATION_SUGGESTION"

    goto :goto_0

    .line 442717
    :pswitch_5
    const-string v0, "ANDROID_RAGE_SHAKE_PLACE_TIPS"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(LX/2d0;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # LX/2d0;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Long;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 442695
    iget-object v0, p0, LX/2d0;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2iz;

    invoke-virtual {v0, p1}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 442696
    iget-object v0, p0, LX/2d0;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2iz;

    invoke-virtual {v0, p1}, LX/2iz;->a(Ljava/lang/String;)V

    .line 442697
    :cond_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 442698
    iget-object v0, p0, LX/2d0;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CfW;

    new-instance v2, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v2}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    .line 442699
    iput-object p2, v2, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    .line 442700
    move-object v2, v2

    .line 442701
    iput-object p2, v2, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 442702
    move-object v2, v2

    .line 442703
    invoke-virtual {v0, v1, p3, v2}, LX/CfW;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    .line 442704
    return-object v1
.end method

.method private static b(LX/0QB;)LX/2d0;
    .locals 14

    .prologue
    .line 442683
    new-instance v0, LX/2d0;

    const/16 v1, 0xf5b

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    const/16 v3, 0x3096

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x1061

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1064

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x25ea

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/2cw;->a(LX/0QB;)LX/2cw;

    move-result-object v7

    check-cast v7, LX/2cw;

    invoke-static {p0}, LX/0bU;->a(LX/0QB;)LX/0bU;

    move-result-object v8

    check-cast v8, LX/0bW;

    invoke-static {p0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v9

    check-cast v9, LX/2d1;

    invoke-static {p0}, LX/1vC;->a(LX/0QB;)LX/1vC;

    move-result-object v10

    check-cast v10, LX/1vC;

    const/16 v11, 0x1140

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x259

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x455

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, LX/2d0;-><init>(LX/0Ot;Ljava/util/concurrent/Executor;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2cw;LX/0bW;LX/2d1;LX/1vC;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 442684
    return-object v0
.end method

.method public static c(LX/2d0;LX/0am;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 442691
    invoke-static {p0, p1}, LX/2d0;->d(LX/2d0;LX/0am;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442692
    iget-object v0, p0, LX/2d0;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dB;

    .line 442693
    invoke-interface {v0}, LX/2dB;->a()V

    goto :goto_0

    .line 442694
    :cond_0
    return-void
.end method

.method public static c(LX/2d0;)Z
    .locals 3

    .prologue
    .line 442685
    invoke-direct {p0}, LX/2d0;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442686
    const/4 v0, 0x0

    .line 442687
    :goto_0
    return v0

    .line 442688
    :cond_0
    iget-object v0, p0, LX/2d0;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/2dP;

    invoke-direct {v1, p0}, LX/2dP;-><init>(LX/2d0;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/2d0;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 442689
    iget-object v0, p0, LX/2d0;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/2dU;

    invoke-direct {v1, p0}, LX/2dU;-><init>(LX/2d0;)V

    iget-object v2, p0, LX/2d0;->e:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 442690
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 442767
    iget-object v0, p0, LX/2d0;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/2d0;LX/0am;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 442602
    invoke-static {p0}, LX/2d0;->c(LX/2d0;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442603
    const/4 v0, 0x0

    .line 442604
    :goto_0
    return v0

    .line 442605
    :cond_0
    iget-object v0, p0, LX/2d0;->s:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 442606
    iget-object v0, p0, LX/2d0;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2iz;

    iget-object v1, p0, LX/2d0;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2iz;->a(Ljava/lang/String;)V

    .line 442607
    const/4 v0, 0x0

    iput-object v0, p0, LX/2d0;->s:Ljava/lang/String;

    .line 442608
    :cond_1
    iput-object p1, p0, LX/2d0;->r:LX/0am;

    .line 442609
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 442610
    invoke-direct {p0}, LX/2d0;->h()V

    .line 442611
    iget-object v1, p0, LX/2d0;->j:LX/2cw;

    sget-object v2, LX/3FC;->PAGE_NEARBY_INSERTED:LX/3FC;

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->l()LX/2cx;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/2cw;->a(LX/3FC;LX/2cx;)V

    .line 442612
    iget-object v1, p0, LX/2d0;->j:LX/2cw;

    sget-object v2, LX/3FC;->PAGE_NEARBY_INSERTED_2:LX/3FC;

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->l()LX/2cx;

    move-result-object v3

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->d()Z

    move-result v0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/2cw;->a(LX/3FC;LX/2cx;Ljava/lang/String;Z)V

    .line 442613
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static e(LX/2d0;)Z
    .locals 1

    .prologue
    .line 442614
    iget-object v0, p0, LX/2d0;->r:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    return v0
.end method

.method private h()V
    .locals 13

    .prologue
    .line 442615
    iget-object v0, p0, LX/2d0;->r:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    .line 442616
    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->n()LX/9qT;

    move-result-object v2

    .line 442617
    if-eqz v2, :cond_0

    invoke-interface {v2}, LX/9qT;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 442618
    :cond_0
    iget-object v0, p0, LX/2d0;->r:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    .line 442619
    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->p()Ljava/lang/String;

    move-result-object v0

    .line 442620
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 442621
    invoke-static {p0}, LX/2d0;->j(LX/2d0;)V

    .line 442622
    :goto_0
    return-void

    .line 442623
    :cond_1
    invoke-interface {v2}, LX/9qT;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 442624
    invoke-static {p0, v0}, LX/2d0;->a(LX/2d0;Lcom/facebook/placetips/bootstrap/PresenceDescription;)Ljava/lang/String;

    move-result-object v3

    .line 442625
    iput-object v1, p0, LX/2d0;->s:Ljava/lang/String;

    .line 442626
    iget-object v1, p0, LX/2d0;->k:LX/0bW;

    const-string v4, "Initialize reaction with %d cached stories"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v2}, LX/9qT;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-interface {v1, v4, v5}, LX/0bW;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 442627
    iget-object v1, p0, LX/2d0;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2iz;

    .line 442628
    new-instance v8, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v8}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 442629
    iput-object v9, v8, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    .line 442630
    move-object v8, v8

    .line 442631
    move-object v4, v8

    .line 442632
    const-wide/16 v10, -0x1

    .line 442633
    invoke-interface {v2}, LX/9qT;->e()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 442634
    invoke-virtual {v1, v8, v3}, LX/2iz;->b(Ljava/lang/String;Ljava/lang/String;)LX/2jY;

    move-result-object v8

    .line 442635
    invoke-virtual {v8, v2}, LX/2jY;->a(LX/9qT;)V

    .line 442636
    invoke-virtual {v8}, LX/2jY;->E()V

    .line 442637
    iput-wide v10, v8, LX/2jY;->g:J

    .line 442638
    iput-wide v10, v8, LX/2jY;->i:J

    .line 442639
    iput-object v4, v8, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    .line 442640
    move-object v1, v8

    .line 442641
    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->m()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2jY;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;)V

    .line 442642
    iget-object v0, p0, LX/2d0;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {v0, v1}, Lcom/facebook/reaction/ReactionUtil;->a(LX/2jY;)Z

    goto :goto_0

    .line 442643
    :cond_2
    iput-object v0, p0, LX/2d0;->s:Ljava/lang/String;

    goto :goto_0
.end method

.method public static j(LX/2d0;)V
    .locals 2

    .prologue
    .line 442644
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2d0;->s:Ljava/lang/String;

    .line 442645
    iget-object v0, p0, LX/2d0;->k:LX/0bW;

    const-string v1, "Initialize empty reaction"

    invoke-interface {v0, v1}, LX/0bW;->a(Ljava/lang/String;)V

    .line 442646
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/placetips/bootstrap/PresenceDescription;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 442647
    iget-object v0, p0, LX/2d0;->r:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    return-object v0
.end method

.method public final a(LX/2dB;)V
    .locals 3

    .prologue
    .line 442648
    iget-object v0, p0, LX/2d0;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 442649
    invoke-direct {p0}, LX/2d0;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442650
    :goto_0
    return-void

    .line 442651
    :cond_0
    iget-object v0, p0, LX/2d0;->e:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/reaction/placetips/PlaceTipsReactionManager$6;

    invoke-direct {v1, p0}, Lcom/facebook/reaction/placetips/PlaceTipsReactionManager$6;-><init>(LX/2d0;)V

    const v2, 0x6df82481

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/base/fragment/FbFragment;LX/8Y9;)V
    .locals 14

    .prologue
    .line 442652
    invoke-static {p0}, LX/2d0;->e(LX/2d0;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 442653
    :goto_0
    return-void

    .line 442654
    :cond_0
    iget-object v8, p0, LX/2d0;->s:Ljava/lang/String;

    .line 442655
    iget-object v0, p0, LX/2d0;->r:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    .line 442656
    invoke-virtual {v9}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 442657
    invoke-static {p0, v9}, LX/2d0;->a(LX/2d0;Lcom/facebook/placetips/bootstrap/PresenceDescription;)Ljava/lang/String;

    move-result-object v2

    .line 442658
    invoke-virtual {v9}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->k()Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object v3

    .line 442659
    invoke-static {v2}, LX/2s8;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 442660
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p0, v8, v0, v2}, LX/2d0;->a(LX/2d0;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 442661
    iget-object v0, p0, LX/2d0;->m:LX/1vC;

    const v3, 0x1e0002

    invoke-virtual {v0, v3, v1, v2}, LX/1vC;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 442662
    iget-object v0, p0, LX/2d0;->m:LX/1vC;

    const v3, 0x1e000a

    invoke-virtual {v0, v3, v1, v2}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 442663
    iget-object v0, p0, LX/2d0;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CIs;

    invoke-virtual {v9}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->h()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/8ci;->y:LX/8ci;

    invoke-virtual {v5}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CIs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 442664
    iget-object v0, p0, LX/2d0;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1nD;

    invoke-virtual {v9}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->h()Ljava/lang/String;

    move-result-object v5

    sget-object v7, LX/8ci;->y:LX/8ci;

    const/4 v9, 0x0

    move-object v6, v1

    move-object v8, v2

    invoke-virtual/range {v3 .. v9}, LX/1nD;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/String;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object v1

    .line 442665
    iget-object v0, p0, LX/2d0;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x2b67

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 442666
    :cond_1
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 442667
    const-string v0, "place_id"

    invoke-virtual {v9}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 442668
    const-string v0, "place_name"

    invoke-virtual {v9}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 442669
    const-string v0, "gravity_suggestifier_id"

    invoke-virtual {v9}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 442670
    const-string v0, "entry_point"

    invoke-virtual/range {p2 .. p2}, LX/8Y9;->ordinal()I

    move-result v1

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 442671
    new-instance v1, Lcom/facebook/placetips/settings/PlaceTipsLocationData;

    invoke-direct {v1}, Lcom/facebook/placetips/settings/PlaceTipsLocationData;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/placetips/bootstrap/PresenceSource;->b()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Lcom/facebook/placetips/bootstrap/PresenceSource;->b()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->c(I)Lcom/facebook/placetips/settings/PlaceTipsLocationData;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/placetips/bootstrap/PresenceSource;->c()Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Lcom/facebook/placetips/bootstrap/PresenceSource;->c()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    :goto_2
    invoke-virtual {v4, v0, v1}, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->b(D)Lcom/facebook/placetips/settings/PlaceTipsLocationData;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/placetips/bootstrap/PresenceSource;->d()Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v3}, Lcom/facebook/placetips/bootstrap/PresenceSource;->d()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    :goto_3
    invoke-virtual {v4, v0, v1}, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->c(D)Lcom/facebook/placetips/settings/PlaceTipsLocationData;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->a(I)Lcom/facebook/placetips/settings/PlaceTipsLocationData;

    move-result-object v0

    invoke-virtual {v9}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->a()J

    move-result-wide v4

    const-wide/16 v12, 0x3e8

    div-long/2addr v4, v12

    long-to-int v1, v4

    invoke-virtual {v0, v1}, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->b(I)Lcom/facebook/placetips/settings/PlaceTipsLocationData;

    move-result-object v0

    .line 442672
    invoke-virtual {v3}, Lcom/facebook/placetips/bootstrap/PresenceSource;->e()Ljava/lang/Float;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 442673
    invoke-virtual {v3}, Lcom/facebook/placetips/bootstrap/PresenceSource;->e()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->d(D)Lcom/facebook/placetips/settings/PlaceTipsLocationData;

    move-result-object v1

    invoke-virtual {v3}, Lcom/facebook/placetips/bootstrap/PresenceSource;->e()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->a(D)Lcom/facebook/placetips/settings/PlaceTipsLocationData;

    .line 442674
    :goto_4
    const-string v1, "gravity_location_data"

    invoke-virtual {v10, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 442675
    iget-object v0, p0, LX/2d0;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2iz;

    invoke-virtual {v0, v8}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    .line 442676
    if-nez v0, :cond_6

    .line 442677
    iget-object v0, p0, LX/2d0;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2iz;

    invoke-virtual {v0, v8, v2}, LX/2iz;->b(Ljava/lang/String;Ljava/lang/String;)LX/2jY;

    move-result-object v0

    move-object v1, v0

    .line 442678
    :goto_5
    iget-object v0, p0, LX/2d0;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2iz;

    invoke-virtual {v0, v8}, LX/2iz;->c(Ljava/lang/String;)V

    .line 442679
    invoke-virtual {v1}, LX/2jY;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iget-object v1, p0, LX/2d0;->c:Ljava/lang/Runnable;

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 442680
    iget-object v0, p0, LX/2d0;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2iz;

    new-instance v4, Lcom/facebook/reaction/placetips/PlaceTipsReactionManager$7;

    move-object v5, p0

    move-object v9, v2

    invoke-direct/range {v4 .. v9}, Lcom/facebook/reaction/placetips/PlaceTipsReactionManager$7;-><init>(LX/2d0;JLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v8, p1, v4, v10}, LX/2iz;->a(Ljava/lang/String;Lcom/facebook/base/fragment/FbFragment;Ljava/lang/Runnable;Landroid/os/Bundle;)Z

    goto/16 :goto_0

    .line 442681
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_3
    const-wide/16 v0, 0x0

    goto/16 :goto_2

    :cond_4
    const-wide/16 v0, 0x0

    goto/16 :goto_3

    .line 442682
    :cond_5
    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->a(D)Lcom/facebook/placetips/settings/PlaceTipsLocationData;

    move-result-object v1

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->d(D)Lcom/facebook/placetips/settings/PlaceTipsLocationData;

    goto :goto_4

    :cond_6
    move-object v1, v0

    goto :goto_5
.end method

.method public final b(LX/2dB;)V
    .locals 1

    .prologue
    .line 442600
    iget-object v0, p0, LX/2d0;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 442601
    return-void
.end method
