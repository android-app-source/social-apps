.class public LX/2WU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26y;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/2WU;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/qe/api/manager/SyncedExperimentData;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0ej;

.field private final d:LX/26w;

.field private e:Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 418899
    const-class v0, LX/2WU;

    sput-object v0, LX/2WU;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;LX/0ej;LX/26w;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/qe/api/manager/SyncedExperimentData;",
            ">;",
            "LX/0ej;",
            "LX/26w;",
            ")V"
        }
    .end annotation

    .prologue
    .line 418900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418901
    iput-object p1, p0, LX/2WU;->b:Ljava/util/Map;

    .line 418902
    iput-object p2, p0, LX/2WU;->c:LX/0ej;

    .line 418903
    iput-object p3, p0, LX/2WU;->d:LX/26w;

    .line 418904
    return-void
.end method

.method private a(IILX/0oc;)V
    .locals 6

    .prologue
    .line 418905
    iget-object v3, p0, LX/2WU;->c:LX/0ej;

    iget-object v4, p0, LX/2WU;->d:LX/26w;

    move v0, p1

    move v1, p1

    move v2, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/2Wy;->a(IIILX/0ej;LX/26w;LX/0oc;)V

    .line 418906
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/16 v6, 0x65

    const/16 v5, 0x64

    .line 418907
    add-int/lit8 v1, p2, 0x0

    .line 418908
    add-int/lit8 v2, p2, 0x1

    .line 418909
    add-int/lit8 v3, p2, 0x2

    .line 418910
    add-int/lit8 v4, p2, 0x3

    .line 418911
    iget-object v0, p0, LX/2WU;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    iput-object v0, p0, LX/2WU;->e:Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    .line 418912
    iget-object v0, p0, LX/2WU;->e:Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    if-nez v0, :cond_0

    .line 418913
    sget-object v0, LX/0oc;->ASSIGNED:LX/0oc;

    invoke-direct {p0, v1, v6, v0}, LX/2WU;->a(IILX/0oc;)V

    .line 418914
    sget-object v0, LX/0oc;->ASSIGNED:LX/0oc;

    invoke-direct {p0, v2, v6, v0}, LX/2WU;->a(IILX/0oc;)V

    .line 418915
    sget-object v0, LX/0oc;->ASSIGNED:LX/0oc;

    invoke-direct {p0, v3, v5, v0}, LX/2WU;->a(IILX/0oc;)V

    .line 418916
    sget-object v0, LX/0oc;->ASSIGNED:LX/0oc;

    invoke-direct {p0, v4, v5, v0}, LX/2WU;->a(IILX/0oc;)V

    .line 418917
    :goto_0
    return-void

    .line 418918
    :cond_0
    iget-object v0, p0, LX/2WU;->e:Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    iget-object v1, p0, LX/2WU;->d:LX/26w;

    sget-object v2, LX/0oc;->ASSIGNED:LX/0oc;

    .line 418919
    add-int/lit8 v3, p2, 0x0

    .line 418920
    add-int/lit8 v4, p2, 0x1

    .line 418921
    add-int/lit8 v5, p2, 0x2

    .line 418922
    add-int/lit8 v6, p2, 0x3

    .line 418923
    iget-boolean p0, v0, LX/2Wx;->d:Z

    move p0, p0

    .line 418924
    invoke-virtual {v1, v2, v3, p0}, LX/26w;->a(LX/0oc;IZ)V

    .line 418925
    iget-boolean v3, v0, LX/2Wx;->c:Z

    move v3, v3

    .line 418926
    invoke-virtual {v1, v2, v4, v3}, LX/26w;->a(LX/0oc;IZ)V

    .line 418927
    invoke-virtual {v0}, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->b()Ljava/lang/String;

    move-result-object v3

    .line 418928
    invoke-virtual {v0}, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->c()Ljava/lang/String;

    move-result-object v4

    .line 418929
    if-eqz v3, :cond_1

    .line 418930
    invoke-virtual {v1, v2, v5, v3}, LX/26w;->a(LX/0oc;ILjava/lang/String;)V

    .line 418931
    :cond_1
    if-eqz v4, :cond_2

    .line 418932
    invoke-virtual {v1, v2, v6, v4}, LX/26w;->a(LX/0oc;ILjava/lang/String;)V

    .line 418933
    :cond_2
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;IIZ)V
    .locals 7

    .prologue
    .line 418934
    iget-object v3, p0, LX/2WU;->c:LX/0ej;

    iget-object v4, p0, LX/2WU;->d:LX/26w;

    sget-object v5, LX/0oc;->OVERRIDE:LX/0oc;

    move v0, p2

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, LX/2Wy;->a(IIILX/0ej;LX/26w;LX/0oc;)V

    .line 418935
    if-eqz p4, :cond_1

    .line 418936
    :cond_0
    :goto_0
    return-void

    .line 418937
    :cond_1
    iget-object v0, p0, LX/2WU;->e:Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    if-nez v0, :cond_2

    .line 418938
    sget-object v0, LX/0oc;->ASSIGNED:LX/0oc;

    invoke-direct {p0, p2, p3, v0}, LX/2WU;->a(IILX/0oc;)V

    goto :goto_0

    .line 418939
    :cond_2
    iget-object v0, p0, LX/2WU;->e:Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    invoke-virtual {v0}, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->f()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 418940
    if-eqz v6, :cond_0

    .line 418941
    iget-object v0, p0, LX/2WU;->d:LX/26w;

    sget-object v1, LX/0oc;->ASSIGNED:LX/0oc;

    iget-object v2, p0, LX/2WU;->e:Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    invoke-virtual {v2}, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->a()Ljava/lang/String;

    move-result-object v2

    move-object v3, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v6}, LX/26w;->a(LX/0oc;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_0
.end method
