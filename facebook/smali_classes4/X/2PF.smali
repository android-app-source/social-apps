.class public final LX/2PF;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 406085
    new-instance v0, LX/0U1;

    const-string v1, "thread_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2PF;->a:LX/0U1;

    .line 406086
    new-instance v0, LX/0U1;

    const-string v1, "owner_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2PF;->b:LX/0U1;

    .line 406087
    new-instance v0, LX/0U1;

    const-string v1, "other_user_device_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2PF;->c:LX/0U1;

    .line 406088
    new-instance v0, LX/0U1;

    const-string v1, "name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2PF;->d:LX/0U1;

    .line 406089
    new-instance v0, LX/0U1;

    const-string v1, "key"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2PF;->e:LX/0U1;

    .line 406090
    new-instance v0, LX/0U1;

    const-string v1, "timestamp_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2PF;->f:LX/0U1;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    .line 406091
    const-string v6, "thread_devices"

    sget-object v0, LX/2PF;->a:LX/0U1;

    sget-object v1, LX/2PF;->b:LX/0U1;

    sget-object v2, LX/2PF;->c:LX/0U1;

    sget-object v3, LX/2PF;->d:LX/0U1;

    sget-object v4, LX/2PF;->e:LX/0U1;

    sget-object v5, LX/2PF;->f:LX/0U1;

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    new-instance v1, LX/0su;

    sget-object v2, LX/2PF;->a:LX/0U1;

    sget-object v3, LX/2PF;->b:LX/0U1;

    sget-object v4, LX/2PF;->c:LX/0U1;

    invoke-static {v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0su;-><init>(LX/0Px;)V

    invoke-direct {p0, v6, v0, v1}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 406092
    return-void
.end method

.method public static b()LX/2PF;
    .locals 1

    .prologue
    .line 406093
    new-instance v0, LX/2PF;

    invoke-direct {v0}, LX/2PF;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 406094
    return-void
.end method
