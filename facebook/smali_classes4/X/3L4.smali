.class public final enum LX/3L4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3L4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3L4;

.field public static final enum USE_PARTICIPANTS_NAMES_ONLY:LX/3L4;

.field public static final enum USE_THREAD_NAME_IF_AVAILABLE:LX/3L4;


# instance fields
.field public final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 549887
    new-instance v0, LX/3L4;

    const-string v1, "USE_THREAD_NAME_IF_AVAILABLE"

    invoke-direct {v0, v1, v2, v2}, LX/3L4;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3L4;->USE_THREAD_NAME_IF_AVAILABLE:LX/3L4;

    .line 549888
    new-instance v0, LX/3L4;

    const-string v1, "USE_PARTICIPANTS_NAMES_ONLY"

    invoke-direct {v0, v1, v3, v3}, LX/3L4;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3L4;->USE_PARTICIPANTS_NAMES_ONLY:LX/3L4;

    .line 549889
    const/4 v0, 0x2

    new-array v0, v0, [LX/3L4;

    sget-object v1, LX/3L4;->USE_THREAD_NAME_IF_AVAILABLE:LX/3L4;

    aput-object v1, v0, v2

    sget-object v1, LX/3L4;->USE_PARTICIPANTS_NAMES_ONLY:LX/3L4;

    aput-object v1, v0, v3

    sput-object v0, LX/3L4;->$VALUES:[LX/3L4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 549893
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 549894
    iput p3, p0, LX/3L4;->value:I

    .line 549895
    return-void
.end method

.method public static synthetic access$000(LX/3L4;)I
    .locals 1

    .prologue
    .line 549892
    iget v0, p0, LX/3L4;->value:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/3L4;
    .locals 1

    .prologue
    .line 549891
    const-class v0, LX/3L4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3L4;

    return-object v0
.end method

.method public static values()[LX/3L4;
    .locals 1

    .prologue
    .line 549890
    sget-object v0, LX/3L4;->$VALUES:[LX/3L4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3L4;

    return-object v0
.end method
