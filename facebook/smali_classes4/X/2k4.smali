.class public final LX/2k4;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    .line 455324
    sget-object v0, LX/2k5;->a:LX/0U1;

    sget-object v1, LX/2k5;->b:LX/0U1;

    sget-object v2, LX/2k5;->c:LX/0U1;

    sget-object v3, LX/2k5;->d:LX/0U1;

    sget-object v4, LX/2k5;->e:LX/0U1;

    sget-object v5, LX/2k5;->f:LX/0U1;

    sget-object v6, LX/2k5;->g:LX/0U1;

    sget-object v7, LX/2k5;->h:LX/0U1;

    sget-object v8, LX/2k5;->i:LX/0U1;

    sget-object v9, LX/2k5;->j:LX/0U1;

    sget-object v10, LX/2k5;->k:LX/0U1;

    invoke-static/range {v0 .. v10}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2k4;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 455322
    const-string v0, "edges"

    sget-object v1, LX/2k4;->a:LX/0Px;

    invoke-direct {p0, v0, v1}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 455323
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 455303
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 455304
    const-string v0, "DROP INDEX IF EXISTS idx_sessions_sorted;"

    const v1, -0x45091764

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x2c0465e2

    invoke-static {v0}, LX/03h;->a(I)V

    .line 455305
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE INDEX idx_sessions_sorted ON edges("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/2k5;->f:LX/0U1;

    .line 455306
    iget-object p0, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, p0

    .line 455307
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/2k5;->j:LX/0U1;

    .line 455308
    iget-object p0, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, p0

    .line 455309
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, -0x2517d49f

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x2e9fadd7

    invoke-static {v0}, LX/03h;->a(I)V

    .line 455310
    const-string v0, "DROP INDEX IF EXISTS idx_user_sessions_sorted;"

    const v1, -0xe2e13a8

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7f1f16db

    invoke-static {v0}, LX/03h;->a(I)V

    .line 455311
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE INDEX idx_user_sessions_sorted ON edges("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/2k5;->f:LX/0U1;

    .line 455312
    iget-object p0, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, p0

    .line 455313
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/2k5;->g:LX/0U1;

    .line 455314
    iget-object p0, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, p0

    .line 455315
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/2k5;->j:LX/0U1;

    .line 455316
    iget-object p0, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, p0

    .line 455317
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, -0x8cad76

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x273a720e

    invoke-static {v0}, LX/03h;->a(I)V

    .line 455318
    return-void
.end method

.method public final d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 455319
    invoke-super {p0, p1}, LX/0Tz;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 455320
    const-string v0, "PRAGMA synchronous=OFF;"

    const v1, -0x1d54d95f

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x7b1a215

    invoke-static {v0}, LX/03h;->a(I)V

    .line 455321
    return-void
.end method
