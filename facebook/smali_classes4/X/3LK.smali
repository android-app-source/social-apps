.class public LX/3LK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "LX/6Lx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 550285
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3LK;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 550282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550283
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/3LK;->a:Ljava/util/Map;

    .line 550284
    return-void
.end method

.method public static a(LX/0QB;)LX/3LK;
    .locals 7

    .prologue
    .line 550253
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 550254
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 550255
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 550256
    if-nez v1, :cond_0

    .line 550257
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 550258
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 550259
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 550260
    sget-object v1, LX/3LK;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 550261
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 550262
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 550263
    :cond_1
    if-nez v1, :cond_4

    .line 550264
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 550265
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 550266
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    .line 550267
    new-instance v0, LX/3LK;

    invoke-direct {v0}, LX/3LK;-><init>()V

    .line 550268
    move-object v1, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 550269
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 550270
    if-nez v1, :cond_2

    .line 550271
    sget-object v0, LX/3LK;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LK;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 550272
    :goto_1
    if-eqz v0, :cond_3

    .line 550273
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 550274
    :goto_3
    check-cast v0, LX/3LK;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 550275
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 550276
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 550277
    :catchall_1
    move-exception v0

    .line 550278
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 550279
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 550280
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 550281
    :cond_2
    :try_start_8
    sget-object v0, LX/3LK;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LK;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a()LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "LX/6Lx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 550286
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 550287
    iget-object v0, p0, LX/3LK;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 550288
    iget-object v1, p0, LX/3LK;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Lx;

    .line 550289
    invoke-virtual {v2, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 550290
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 550291
    :cond_0
    :try_start_1
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/facebook/graphql/enums/GraphQLUserChatContextType;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLUserChatContextType;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 550244
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3LK;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 550245
    const/4 v0, 0x0

    .line 550246
    :goto_0
    monitor-exit p0

    return-object v0

    .line 550247
    :cond_0
    :try_start_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 550248
    iget-object v0, p0, LX/3LK;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 550249
    iget-object v1, p0, LX/3LK;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Lx;

    invoke-interface {v1}, LX/6Lx;->b()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->b()Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    move-result-object v1

    if-ne v1, p1, :cond_1

    .line 550250
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 550251
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 550252
    :cond_2
    :try_start_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_0
.end method

.method public final declared-synchronized a(LX/0P1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "+",
            "LX/6Lx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 550240
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3LK;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 550241
    iget-object v0, p0, LX/3LK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 550242
    monitor-exit p0

    return-void

    .line 550243
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 550237
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3LK;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 550238
    monitor-exit p0

    return-void

    .line 550239
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearUserData()V
    .locals 1

    .prologue
    .line 550234
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/3LK;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 550235
    monitor-exit p0

    return-void

    .line 550236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
