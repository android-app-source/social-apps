.class public LX/2nh;
.super LX/2ni;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/2ni",
        "<TT;>;",
        "Ljava/io/Closeable;"
    }
.end annotation


# instance fields
.field public final a:LX/2nf;

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2nm;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Sh;

.field private d:J

.field private e:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(LX/2nf;LX/0Sh;)V
    .locals 10
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 465010
    invoke-direct {p0}, LX/2ni;-><init>()V

    .line 465011
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2nh;->d:J

    .line 465012
    const/4 v0, 0x0

    iput-object v0, p0, LX/2nh;->e:Ljava/lang/Throwable;

    .line 465013
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nf;

    iput-object v0, p0, LX/2nh;->a:LX/2nf;

    .line 465014
    invoke-interface {p1}, LX/2nf;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    .line 465015
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 465016
    :goto_0
    move-object v0, v0

    .line 465017
    iput-object v0, p0, LX/2nh;->b:LX/0Px;

    .line 465018
    iput-object p2, p0, LX/2nh;->c:LX/0Sh;

    .line 465019
    invoke-virtual {p0}, LX/2nh;->m()V

    .line 465020
    return-void

    .line 465021
    :cond_0
    invoke-interface {p1}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CHUNKS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 465022
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 465023
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 465024
    goto :goto_0

    .line 465025
    :cond_2
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 465026
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    .line 465027
    new-instance v5, LX/2nl;

    iget-object v6, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    iget-object v7, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->c:Ljava/lang/String;

    sget-object v8, LX/2nk;->BEFORE:LX/2nk;

    iget-boolean v9, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->e:Z

    invoke-direct {v5, v6, v7, v8, v9}, LX/2nl;-><init>(Ljava/lang/String;Ljava/lang/String;LX/2nk;Z)V

    .line 465028
    new-instance v6, LX/2nl;

    iget-object v7, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->b:Ljava/lang/String;

    iget-object v8, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->d:Ljava/lang/String;

    sget-object v9, LX/2nk;->AFTER:LX/2nk;

    iget-boolean v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->f:Z

    invoke-direct {v6, v7, v8, v9, v0}, LX/2nl;-><init>(Ljava/lang/String;Ljava/lang/String;LX/2nk;Z)V

    .line 465029
    new-instance v0, LX/2nm;

    invoke-direct {v0, v5, v6}, LX/2nm;-><init>(LX/2nj;LX/2nj;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 465030
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 465031
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 465032
    iget-object v0, p0, LX/2nh;->c:LX/0Sh;

    const-string v1, "Attempting to access connection state on non-UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 465033
    return-void
.end method


# virtual methods
.method public declared-synchronized a(I)Ljava/lang/Object;
    .locals 6
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 465003
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/2nh;->f()V

    .line 465004
    invoke-virtual {p0}, LX/2nh;->m()V

    .line 465005
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 465006
    :try_start_1
    iget-object v0, p0, LX/2nh;->a:LX/2nf;

    invoke-interface {v0, p1}, LX/2nf;->moveToPosition(I)Z

    move-result v0

    const-string v2, "Unable to move cursor to idx %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 465007
    iget-object v0, p0, LX/2nh;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 465008
    :try_start_2
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 465009
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()I
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 465001
    invoke-virtual {p0}, LX/2nh;->m()V

    .line 465002
    iget-object v0, p0, LX/2nh;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->getCount()I

    move-result v0

    return v0
.end method

.method public close()V
    .locals 2
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 464995
    invoke-virtual {p0}, LX/2nh;->m()V

    .line 464996
    iget-object v0, p0, LX/2nh;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->close()V

    .line 464997
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 464998
    if-eqz v0, :cond_0

    .line 464999
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Cursor was closed here"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/2nh;->e:Ljava/lang/Throwable;

    .line 465000
    :cond_0
    return-void
.end method

.method public d()LX/0Px;
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2nm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 465034
    iget-object v0, p0, LX/2nh;->b:LX/0Px;

    return-object v0
.end method

.method public e()LX/2nf;
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 464992
    invoke-virtual {p0}, LX/2nh;->m()V

    .line 464993
    invoke-direct {p0}, LX/2nh;->f()V

    .line 464994
    iget-object v0, p0, LX/2nh;->a:LX/2nf;

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 464991
    iget-object v0, p0, LX/2nh;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->isClosed()Z

    move-result v0

    return v0
.end method

.method public k()I
    .locals 3

    .prologue
    .line 464990
    iget-object v0, p0, LX/2nh;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SESSION_VERSION"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public declared-synchronized l()J
    .locals 4
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const-wide/16 v0, 0x0

    .line 464979
    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, LX/2nh;->d:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_1

    .line 464980
    iget-wide v0, p0, LX/2nh;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464981
    :cond_0
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 464982
    :cond_1
    :try_start_1
    iget-object v2, p0, LX/2nh;->a:LX/2nf;

    invoke-interface {v2}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CHUNKS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 464983
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 464984
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-wide v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->h:J

    iput-wide v0, p0, LX/2nh;->d:J

    .line 464985
    iget-wide v0, p0, LX/2nh;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 464986
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 464987
    iget-object v0, p0, LX/2nh;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464988
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to access closed connection state"

    iget-object v2, p0, LX/2nh;->e:Ljava/lang/Throwable;

    invoke-direct {v0, v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 464989
    :cond_0
    return-void
.end method
