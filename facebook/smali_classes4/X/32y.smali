.class public final LX/32y;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/22P;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1aZ;

.field public b:Landroid/graphics/PointF;

.field public c:I

.field public final synthetic d:LX/22P;


# direct methods
.method public constructor <init>(LX/22P;)V
    .locals 1

    .prologue
    .line 491464
    iput-object p1, p0, LX/32y;->d:LX/22P;

    .line 491465
    move-object v0, p1

    .line 491466
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 491467
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 491484
    const-string v0, "PhotoAttachmentWithVisibilitySubscriberFrescoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 491468
    if-ne p0, p1, :cond_1

    .line 491469
    :cond_0
    :goto_0
    return v0

    .line 491470
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 491471
    goto :goto_0

    .line 491472
    :cond_3
    check-cast p1, LX/32y;

    .line 491473
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 491474
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 491475
    if-eq v2, v3, :cond_0

    .line 491476
    iget-object v2, p0, LX/32y;->a:LX/1aZ;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/32y;->a:LX/1aZ;

    iget-object v3, p1, LX/32y;->a:LX/1aZ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 491477
    goto :goto_0

    .line 491478
    :cond_5
    iget-object v2, p1, LX/32y;->a:LX/1aZ;

    if-nez v2, :cond_4

    .line 491479
    :cond_6
    iget-object v2, p0, LX/32y;->b:Landroid/graphics/PointF;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/32y;->b:Landroid/graphics/PointF;

    iget-object v3, p1, LX/32y;->b:Landroid/graphics/PointF;

    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 491480
    goto :goto_0

    .line 491481
    :cond_8
    iget-object v2, p1, LX/32y;->b:Landroid/graphics/PointF;

    if-nez v2, :cond_7

    .line 491482
    :cond_9
    iget v2, p0, LX/32y;->c:I

    iget v3, p1, LX/32y;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 491483
    goto :goto_0
.end method
