.class public final LX/2GP;
.super LX/14l;
.source ""


# instance fields
.field public final synthetic a:LX/28x;


# direct methods
.method public constructor <init>(LX/28x;Landroid/content/Context;Landroid/content/IntentFilter;)V
    .locals 0

    .prologue
    .line 388400
    iput-object p1, p0, LX/2GP;->a:LX/28x;

    invoke-direct {p0, p2, p3}, LX/14l;-><init>(Landroid/content/Context;Landroid/content/IntentFilter;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 388401
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 388402
    const-string v1, "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388403
    iget-object v0, p0, LX/2GP;->a:LX/28x;

    .line 388404
    iget-object v1, v0, LX/28x;->v:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3R9;

    .line 388405
    sget-object p1, LX/3RF;->USER_LOGGED_OUT:LX/3RF;

    invoke-virtual {v1, p1}, LX/3R9;->a(LX/3RF;)V

    goto :goto_0

    .line 388406
    :cond_0
    iget-object v1, v0, LX/28x;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object p0, LX/28x;->a:LX/0Tn;

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 388407
    :cond_1
    return-void
.end method
