.class public final LX/2k6;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    .line 455336
    sget-object v0, LX/2k7;->a:LX/0U1;

    sget-object v1, LX/2k7;->b:LX/0U1;

    sget-object v2, LX/2k7;->c:LX/0U1;

    sget-object v3, LX/2k7;->d:LX/0U1;

    sget-object v4, LX/2k7;->e:LX/0U1;

    sget-object v5, LX/2k7;->f:LX/0U1;

    sget-object v6, LX/2k7;->g:LX/0U1;

    sget-object v7, LX/2k7;->h:LX/0U1;

    sget-object v8, LX/2k7;->i:LX/0U1;

    sget-object v9, LX/2k7;->j:LX/0U1;

    invoke-static/range {v0 .. v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2k6;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 455337
    const-string v0, "chunks"

    sget-object v1, LX/2k6;->a:LX/0Px;

    invoke-direct {p0, v0, v1}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 455338
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 455339
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 455340
    const-string v0, "DROP INDEX IF EXISTS idx_chunk_sessions_sorted;"

    const v1, 0x9ca3173

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x71e6fc43

    invoke-static {v0}, LX/03h;->a(I)V

    .line 455341
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE INDEX idx_chunk_sessions_sorted ON chunks("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/2k7;->b:LX/0U1;

    .line 455342
    iget-object p0, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, p0

    .line 455343
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/2k7;->c:LX/0U1;

    .line 455344
    iget-object p0, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, p0

    .line 455345
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, -0x36c0da34    # -782940.75f

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x496a2845

    invoke-static {v0}, LX/03h;->a(I)V

    .line 455346
    return-void
.end method
