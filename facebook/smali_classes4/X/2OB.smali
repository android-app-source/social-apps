.class public LX/2OB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field public final a:LX/0Sh;

.field public final b:LX/03V;

.field private final c:LX/2Ny;

.field public final d:LX/0ad;

.field private final e:Lcom/facebook/compactdisk/StoreManagerFactory;

.field public f:Lcom/facebook/compactdisk/DiskCache;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 400596
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2OB;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/03V;LX/2Ny;LX/0ad;Lcom/facebook/compactdisk/StoreManagerFactory;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 400589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400590
    iput-object p1, p0, LX/2OB;->a:LX/0Sh;

    .line 400591
    iput-object p2, p0, LX/2OB;->b:LX/03V;

    .line 400592
    iput-object p3, p0, LX/2OB;->c:LX/2Ny;

    .line 400593
    iput-object p4, p0, LX/2OB;->d:LX/0ad;

    .line 400594
    iput-object p5, p0, LX/2OB;->e:Lcom/facebook/compactdisk/StoreManagerFactory;

    .line 400595
    return-void
.end method

.method public static a(LX/0QB;)LX/2OB;
    .locals 13

    .prologue
    .line 400560
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 400561
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 400562
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 400563
    if-nez v1, :cond_0

    .line 400564
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 400565
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 400566
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 400567
    sget-object v1, LX/2OB;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 400568
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 400569
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 400570
    :cond_1
    if-nez v1, :cond_4

    .line 400571
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 400572
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 400573
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 400574
    new-instance v7, LX/2OB;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/2Ny;->a(LX/0QB;)LX/2Ny;

    move-result-object v10

    check-cast v10, LX/2Ny;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {v0}, LX/2Nz;->a(LX/0QB;)Lcom/facebook/compactdisk/StoreManagerFactory;

    move-result-object v12

    check-cast v12, Lcom/facebook/compactdisk/StoreManagerFactory;

    invoke-direct/range {v7 .. v12}, LX/2OB;-><init>(LX/0Sh;LX/03V;LX/2Ny;LX/0ad;Lcom/facebook/compactdisk/StoreManagerFactory;)V

    .line 400575
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 400576
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 400577
    if-nez v1, :cond_2

    .line 400578
    sget-object v0, LX/2OB;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2OB;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 400579
    :goto_1
    if-eqz v0, :cond_3

    .line 400580
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 400581
    :goto_3
    check-cast v0, LX/2OB;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 400582
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 400583
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 400584
    :catchall_1
    move-exception v0

    .line 400585
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 400586
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 400587
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 400588
    :cond_2
    :try_start_8
    sget-object v0, LX/2OB;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2OB;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/2OB;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 400493
    iget-object v2, p0, LX/2OB;->d:LX/0ad;

    sget-short v3, LX/FGN;->e:S

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 400494
    :cond_0
    :goto_0
    return v0

    .line 400495
    :cond_1
    iget-object v2, p0, LX/2OB;->f:Lcom/facebook/compactdisk/DiskCache;

    if-nez v2, :cond_2

    .line 400496
    :try_start_0
    new-instance v2, Lcom/facebook/compactdisk/DiskCacheConfig;

    invoke-direct {v2}, Lcom/facebook/compactdisk/DiskCacheConfig;-><init>()V

    const-string v3, "video_upload"

    invoke-virtual {v2, v3}, Lcom/facebook/compactdisk/DiskCacheConfig;->name(Ljava/lang/String;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/compactdisk/DiskCacheConfig;->sessionScoped(Z)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v2

    sget-object v3, Lcom/facebook/compactdisk/DiskArea;->CACHES:Lcom/facebook/compactdisk/DiskArea;

    invoke-virtual {v2, v3}, Lcom/facebook/compactdisk/DiskCacheConfig;->diskArea(Lcom/facebook/compactdisk/DiskArea;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v2

    const-wide/16 v4, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/compactdisk/DiskCacheConfig;->version(Ljava/lang/Long;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v2

    new-instance v3, Lcom/facebook/compactdisk/ManagedConfig;

    invoke-direct {v3}, Lcom/facebook/compactdisk/ManagedConfig;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/compactdisk/ManagedConfig;->a(Z)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v3

    new-instance v4, Lcom/facebook/compactdisk/StalePruningConfig;

    invoke-direct {v4}, Lcom/facebook/compactdisk/StalePruningConfig;-><init>()V

    const-wide/32 v6, 0x93a80

    invoke-virtual {v4, v6, v7}, Lcom/facebook/compactdisk/StalePruningConfig;->a(J)Lcom/facebook/compactdisk/StalePruningConfig;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/compactdisk/ManagedConfig;->a(Lcom/facebook/compactdisk/StalePruningConfig;)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v3

    new-instance v4, Lcom/facebook/compactdisk/EvictionConfig;

    invoke-direct {v4}, Lcom/facebook/compactdisk/EvictionConfig;-><init>()V

    const-wide/32 v6, 0x6400000

    invoke-virtual {v4, v6, v7}, Lcom/facebook/compactdisk/EvictionConfig;->maxSize(J)Lcom/facebook/compactdisk/EvictionConfig;

    move-result-object v4

    const-wide/32 v6, 0x1400000

    invoke-virtual {v4, v6, v7}, Lcom/facebook/compactdisk/EvictionConfig;->lowSpaceMaxSize(J)Lcom/facebook/compactdisk/EvictionConfig;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/compactdisk/ManagedConfig;->a(Lcom/facebook/compactdisk/EvictionConfig;)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/compactdisk/DiskCacheConfig;->subConfig(Lcom/facebook/compactdisk/SubConfig;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v2

    .line 400497
    iget-object v3, p0, LX/2OB;->e:Lcom/facebook/compactdisk/StoreManagerFactory;

    invoke-virtual {v3, v2}, Lcom/facebook/compactdisk/StoreManagerFactory;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/StoreManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/facebook/compactdisk/StoreManager;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/DiskCache;

    move-result-object v2

    iput-object v2, p0, LX/2OB;->f:Lcom/facebook/compactdisk/DiskCache;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 400498
    :cond_2
    :goto_1
    iget-object v2, p0, LX/2OB;->f:Lcom/facebook/compactdisk/DiskCache;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 400499
    :catch_0
    move-exception v2

    .line 400500
    iget-object v3, p0, LX/2OB;->b:LX/03V;

    const-string v4, "VideoUploadCandidateStore_initialization_failure"

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 400501
    const/4 v2, 0x0

    iput-object v2, p0, LX/2OB;->f:Lcom/facebook/compactdisk/DiskCache;

    goto :goto_1
.end method

.method public static e(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 400558
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Ny;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 400559
    const-string v1, "o_%s"

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static f(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 400555
    const-string v0, "%s_%d_%d_%s_%s_%b"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/facebook/ui/media/attachments/MediaResource;->n:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 400556
    invoke-static {v0}, LX/2Ny;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 400557
    const-string v1, "r_%s"

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 400530
    invoke-static {p0}, LX/2OB;->a(LX/2OB;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 400531
    :cond_0
    :goto_0
    return-object v0

    .line 400532
    :cond_1
    iget-object v1, p0, LX/2OB;->d:LX/0ad;

    sget-short v2, LX/FGN;->f:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 400533
    invoke-static {p1}, LX/5zs;->fromOrNull(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zs;

    move-result-object v1

    .line 400534
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/5zs;->isLikelyLocal()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 400535
    :try_start_0
    invoke-static {p1}, LX/2OB;->e(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v1

    .line 400536
    iget-object v2, p0, LX/2OB;->f:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v2, v1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetchPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 400537
    if-nez v2, :cond_2

    .line 400538
    iget-object v2, p0, LX/2OB;->c:LX/2Ny;

    iget-object v3, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, LX/2Ny;->a(Landroid/net/Uri;)LX/46f;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 400539
    :try_start_1
    iget-object v3, v2, LX/46f;->a:Ljava/io/File;

    .line 400540
    if-eqz v3, :cond_3

    .line 400541
    invoke-static {v3}, LX/1t3;->b(Ljava/io/File;)[B

    move-result-object v3

    .line 400542
    iget-object v4, p0, LX/2OB;->f:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v4, v1, v3}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->store(Ljava/lang/String;[B)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 400543
    if-eqz v2, :cond_0

    .line 400544
    invoke-virtual {v2}, LX/46f;->a()V

    goto :goto_0

    :cond_2
    move-object v2, v0

    .line 400545
    :cond_3
    if-eqz v2, :cond_0

    .line 400546
    invoke-virtual {v2}, LX/46f;->a()V

    goto :goto_0

    .line 400547
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 400548
    :goto_1
    :try_start_2
    iget-object v3, p0, LX/2OB;->b:LX/03V;

    const-string v4, "VideoUploadCandidateStore_store_original_video_failed"

    invoke-virtual {v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 400549
    if-eqz v2, :cond_0

    .line 400550
    invoke-virtual {v2}, LX/46f;->a()V

    goto :goto_0

    .line 400551
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    if-eqz v2, :cond_4

    .line 400552
    invoke-virtual {v2}, LX/46f;->a()V

    :cond_4
    throw v0

    .line 400553
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 400554
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/io/File;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/io/File;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 400521
    invoke-static {p0}, LX/2OB;->a(LX/2OB;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 400522
    :cond_0
    :goto_0
    return-object v0

    .line 400523
    :cond_1
    :try_start_0
    invoke-static {p1}, LX/2OB;->f(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v1

    .line 400524
    iget-object v2, p0, LX/2OB;->f:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v2, v1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetchPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 400525
    if-nez v2, :cond_0

    if-eqz p2, :cond_0

    .line 400526
    invoke-static {p2}, LX/1t3;->b(Ljava/io/File;)[B

    move-result-object v2

    .line 400527
    iget-object v3, p0, LX/2OB;->f:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v3, v1, v2}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->store(Ljava/lang/String;[B)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 400528
    :catch_0
    move-exception v1

    .line 400529
    iget-object v2, p0, LX/2OB;->b:LX/03V;

    const-string v3, "VideoUploadCandidateStore_store_resized_video_failed"

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final c(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/io/File;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 400513
    invoke-static {p0}, LX/2OB;->a(LX/2OB;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 400514
    :cond_0
    :goto_0
    return-object v0

    .line 400515
    :cond_1
    :try_start_0
    invoke-static {p1}, LX/2OB;->f(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v1

    .line 400516
    iget-object v2, p0, LX/2OB;->f:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v2, v1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetchPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 400517
    if-eqz v2, :cond_0

    .line 400518
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 400519
    :catch_0
    move-exception v1

    .line 400520
    iget-object v2, p0, LX/2OB;->b:LX/03V;

    const-string v3, "VideoUploadCandidateStore_fetch_resized_video_failed"

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 400510
    iget-object v0, p0, LX/2OB;->f:Lcom/facebook/compactdisk/DiskCache;

    if-eqz v0, :cond_0

    .line 400511
    iget-object v0, p0, LX/2OB;->f:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v0}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->clear()V

    .line 400512
    :cond_0
    return-void
.end method

.method public final d(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/io/File;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 400502
    invoke-static {p0}, LX/2OB;->a(LX/2OB;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 400503
    :cond_0
    :goto_0
    return-object v0

    .line 400504
    :cond_1
    :try_start_0
    invoke-static {p1}, LX/2OB;->e(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v1

    .line 400505
    iget-object v2, p0, LX/2OB;->f:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v2, v1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetchPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 400506
    if-eqz v2, :cond_0

    .line 400507
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 400508
    :catch_0
    move-exception v1

    .line 400509
    iget-object v2, p0, LX/2OB;->b:LX/03V;

    const-string v3, "VideoUploadCandidateStore_fetch_original_video_failed"

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
