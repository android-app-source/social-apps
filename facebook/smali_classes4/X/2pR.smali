.class public final enum LX/2pR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2pR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2pR;

.field public static final enum DEFAULT:LX/2pR;

.field public static final enum HIDE:LX/2pR;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 468211
    new-instance v0, LX/2pR;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/2pR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2pR;->DEFAULT:LX/2pR;

    .line 468212
    new-instance v0, LX/2pR;

    const-string v1, "HIDE"

    invoke-direct {v0, v1, v3}, LX/2pR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2pR;->HIDE:LX/2pR;

    .line 468213
    const/4 v0, 0x2

    new-array v0, v0, [LX/2pR;

    sget-object v1, LX/2pR;->DEFAULT:LX/2pR;

    aput-object v1, v0, v2

    sget-object v1, LX/2pR;->HIDE:LX/2pR;

    aput-object v1, v0, v3

    sput-object v0, LX/2pR;->$VALUES:[LX/2pR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 468210
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2pR;
    .locals 1

    .prologue
    .line 468208
    const-class v0, LX/2pR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2pR;

    return-object v0
.end method

.method public static values()[LX/2pR;
    .locals 1

    .prologue
    .line 468209
    sget-object v0, LX/2pR;->$VALUES:[LX/2pR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2pR;

    return-object v0
.end method
