.class public LX/27p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 373155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373156
    iput-object p1, p0, LX/27p;->a:LX/0Zb;

    .line 373157
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 373158
    iget-object v0, p0, LX/27p;->a:LX/0Zb;

    sget-object v1, LX/FQT;->OPEN_ID_CONNECT_FAILURE:LX/FQT;

    invoke-virtual {v1}, LX/FQT;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 373159
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 373160
    const-string v1, "open_id_connect"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 373161
    const-string v1, "error_message"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 373162
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 373163
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 373164
    iget-object v0, p0, LX/27p;->a:LX/0Zb;

    const-string v1, "email_auto_confirm_failure"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 373165
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 373166
    const-string v1, "confirmation"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 373167
    const-string v1, "error_message"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 373168
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 373169
    :cond_0
    return-void
.end method
