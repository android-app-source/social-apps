.class public LX/2Q4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2Q5;

.field public final b:LX/2QK;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 407565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407566
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v0

    .line 407567
    sget-object v1, LX/1ZP;->n:Landroid/content/pm/Signature;

    sget-object v2, LX/1ZP;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 407568
    sget-object v1, LX/1ZP;->j:Landroid/content/pm/Signature;

    sget-object v2, LX/1ZP;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 407569
    move-object v0, v0

    .line 407570
    new-instance v1, LX/2Q5;

    invoke-direct {v1, v0, p1, p2}, LX/2Q5;-><init>(LX/0Xu;Landroid/content/Context;Landroid/content/pm/PackageManager;)V

    iput-object v1, p0, LX/2Q4;->a:LX/2Q5;

    .line 407571
    new-instance v1, LX/2QK;

    sget-object v2, LX/1ZP;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v0, p2}, LX/2QK;-><init>(Ljava/lang/String;LX/0Xu;Landroid/content/pm/PackageManager;)V

    iput-object v1, p0, LX/2Q4;->b:LX/2QK;

    .line 407572
    return-void
.end method

.method public static b(LX/0QB;)LX/2Q4;
    .locals 3

    .prologue
    .line 407573
    new-instance v2, LX/2Q4;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageManager;

    invoke-direct {v2, v0, v1}, LX/2Q4;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;)V

    .line 407574
    return-object v2
.end method
