.class public abstract LX/2En;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/2En;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "UploadScheduler.class"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 386016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)LX/2En;
    .locals 3

    .prologue
    .line 386010
    const-class v1, LX/2En;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/2En;->a:LX/2En;

    if-nez v0, :cond_0

    .line 386011
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_1

    .line 386012
    new-instance v0, LX/408;

    invoke-direct {v0, p0}, LX/408;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/2En;->a:LX/2En;

    .line 386013
    :cond_0
    :goto_0
    sget-object v0, LX/2En;->a:LX/2En;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 386014
    :cond_1
    :try_start_1
    new-instance v0, LX/2Eo;

    invoke-direct {v0, p0}, LX/2Eo;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/2En;->a:LX/2En;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 386015
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public abstract a()Landroid/content/ComponentName;
.end method

.method public abstract a(I)V
.end method

.method public abstract a(ILX/2DI;JJ)V
.end method

.method public abstract b(I)J
.end method
