.class public final enum LX/2g6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2g6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2g6;

.field public static final enum ALWAYS_FAIL:LX/2g6;

.field public static final enum ALWAYS_PASS:LX/2g6;

.field public static final enum DEFAULT:LX/2g6;


# instance fields
.field private mFilterState:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 447913
    new-instance v0, LX/2g6;

    const-string v1, "DEFAULT"

    const-string v2, "No Override"

    invoke-direct {v0, v1, v3, v2}, LX/2g6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2g6;->DEFAULT:LX/2g6;

    .line 447914
    new-instance v0, LX/2g6;

    const-string v1, "ALWAYS_PASS"

    const-string v2, "Always Pass"

    invoke-direct {v0, v1, v4, v2}, LX/2g6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2g6;->ALWAYS_PASS:LX/2g6;

    .line 447915
    new-instance v0, LX/2g6;

    const-string v1, "ALWAYS_FAIL"

    const-string v2, "Always Fail"

    invoke-direct {v0, v1, v5, v2}, LX/2g6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2g6;->ALWAYS_FAIL:LX/2g6;

    .line 447916
    const/4 v0, 0x3

    new-array v0, v0, [LX/2g6;

    sget-object v1, LX/2g6;->DEFAULT:LX/2g6;

    aput-object v1, v0, v3

    sget-object v1, LX/2g6;->ALWAYS_PASS:LX/2g6;

    aput-object v1, v0, v4

    sget-object v1, LX/2g6;->ALWAYS_FAIL:LX/2g6;

    aput-object v1, v0, v5

    sput-object v0, LX/2g6;->$VALUES:[LX/2g6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 447907
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 447908
    iput-object p3, p0, LX/2g6;->mFilterState:Ljava/lang/String;

    .line 447909
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2g6;
    .locals 1

    .prologue
    .line 447910
    const-class v0, LX/2g6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2g6;

    return-object v0
.end method

.method public static values()[LX/2g6;
    .locals 1

    .prologue
    .line 447911
    sget-object v0, LX/2g6;->$VALUES:[LX/2g6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2g6;

    return-object v0
.end method


# virtual methods
.method public final getFilterStateCaption()Ljava/lang/String;
    .locals 1

    .prologue
    .line 447912
    iget-object v0, p0, LX/2g6;->mFilterState:Ljava/lang/String;

    return-object v0
.end method
