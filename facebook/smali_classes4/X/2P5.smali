.class public final LX/2P5;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field public static final p:LX/0U1;

.field public static final q:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 405500
    new-instance v0, LX/0U1;

    const-string v1, "thread_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->a:LX/0U1;

    .line 405501
    new-instance v0, LX/0U1;

    const-string v1, "other_user_fbid"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->b:LX/0U1;

    .line 405502
    new-instance v0, LX/0U1;

    const-string v1, "other_user_device_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->c:LX/0U1;

    .line 405503
    new-instance v0, LX/0U1;

    const-string v1, "thread_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->d:LX/0U1;

    .line 405504
    new-instance v0, LX/0U1;

    const-string v1, "timestamp_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->e:LX/0U1;

    .line 405505
    new-instance v0, LX/0U1;

    const-string v1, "last_read_timestamp_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->f:LX/0U1;

    .line 405506
    new-instance v0, LX/0U1;

    const-string v1, "session_state"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->g:LX/0U1;

    .line 405507
    new-instance v0, LX/0U1;

    const-string v1, "draft"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->h:LX/0U1;

    .line 405508
    new-instance v0, LX/0U1;

    const-string v1, "snippet"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->i:LX/0U1;

    .line 405509
    new-instance v0, LX/0U1;

    const-string v1, "snippet_sender_fbid"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->j:LX/0U1;

    .line 405510
    new-instance v0, LX/0U1;

    const-string v1, "admin_snippet"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->k:LX/0U1;

    .line 405511
    new-instance v0, LX/0U1;

    const-string v1, "can_reply"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->l:LX/0U1;

    .line 405512
    new-instance v0, LX/0U1;

    const-string v1, "outgoing_message_lifetime_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->m:LX/0U1;

    .line 405513
    new-instance v0, LX/0U1;

    const-string v1, "last_read_receipt_time_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->n:LX/0U1;

    .line 405514
    new-instance v0, LX/0U1;

    const-string v1, "last_delivered_receipt_time_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->o:LX/0U1;

    .line 405515
    new-instance v0, LX/0U1;

    const-string v1, "encryption_key"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->p:LX/0U1;

    .line 405516
    new-instance v0, LX/0U1;

    const-string v1, "lookup_state"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P5;->q:LX/0U1;

    return-void
.end method
