.class public final enum LX/2Cm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Cm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Cm;

.field public static final enum AR_BOUNCE_FROM_MSITE:LX/2Cm;

.field public static final enum FAST_PW_ERROR_AR:LX/2Cm;

.field public static final enum FULL_WIDTH_REG_BUTTON:LX/2Cm;

.field public static final enum HIDE_REGISTER_BUTTON:LX/2Cm;

.field public static final enum LOGIN_ERROR_REG:LX/2Cm;

.field public static final enum LOGIN_ERROR_REG_EMAIL:LX/2Cm;

.field public static final enum LOGIN_ERROR_REG_PHONE:LX/2Cm;

.field public static final enum LOGIN_TO_REG_PREFILL:LX/2Cm;

.field public static final enum WHITE_LOGIN_PAGE:LX/2Cm;


# instance fields
.field public final condition:LX/F6o;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final endDate:Ljava/util/Date;

.field public final startDate:Ljava/util/Date;

.field public final threshold:I


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x6

    const/16 v12, 0x8

    const/4 v6, 0x0

    const/4 v11, 0x7

    const/16 v10, 0x7e0

    .line 383215
    new-instance v0, LX/2Cm;

    const-string v1, "AR_BOUNCE_FROM_MSITE"

    const/4 v2, 0x0

    const/16 v3, 0x1388

    new-instance v4, Ljava/util/GregorianCalendar;

    const/16 v5, 0x7df

    const/4 v7, 0x4

    const/4 v8, 0x5

    invoke-direct {v4, v5, v7, v8}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v4}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    new-instance v5, Ljava/util/GregorianCalendar;

    const/16 v7, 0x7df

    const/4 v8, 0x5

    const/16 v9, 0x12

    invoke-direct {v5, v7, v8, v9}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v5}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, LX/2Cm;-><init>(Ljava/lang/String;IILjava/util/Date;Ljava/util/Date;LX/F6o;)V

    sput-object v0, LX/2Cm;->AR_BOUNCE_FROM_MSITE:LX/2Cm;

    .line 383216
    new-instance v0, LX/2Cm;

    const-string v1, "LOGIN_ERROR_REG"

    const/4 v2, 0x1

    const/16 v3, 0x5dc

    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4, v10, v13, v11}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v4}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    new-instance v5, Ljava/util/GregorianCalendar;

    invoke-direct {v5, v10, v11, v12}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v5}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, LX/2Cm;-><init>(Ljava/lang/String;IILjava/util/Date;Ljava/util/Date;LX/F6o;)V

    sput-object v0, LX/2Cm;->LOGIN_ERROR_REG:LX/2Cm;

    .line 383217
    new-instance v0, LX/2Cm;

    const-string v1, "LOGIN_ERROR_REG_EMAIL"

    const/4 v2, 0x2

    const/16 v3, 0x85f

    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4, v10, v13, v11}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v4}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    new-instance v5, Ljava/util/GregorianCalendar;

    invoke-direct {v5, v10, v11, v12}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v5}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, LX/2Cm;-><init>(Ljava/lang/String;IILjava/util/Date;Ljava/util/Date;LX/F6o;)V

    sput-object v0, LX/2Cm;->LOGIN_ERROR_REG_EMAIL:LX/2Cm;

    .line 383218
    new-instance v0, LX/2Cm;

    const-string v1, "LOGIN_ERROR_REG_PHONE"

    const/4 v2, 0x3

    const/16 v3, 0xea6

    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4, v10, v13, v11}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v4}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    new-instance v5, Ljava/util/GregorianCalendar;

    invoke-direct {v5, v10, v11, v12}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v5}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, LX/2Cm;-><init>(Ljava/lang/String;IILjava/util/Date;Ljava/util/Date;LX/F6o;)V

    sput-object v0, LX/2Cm;->LOGIN_ERROR_REG_PHONE:LX/2Cm;

    .line 383219
    new-instance v0, LX/2Cm;

    const-string v1, "FAST_PW_ERROR_AR"

    const/4 v2, 0x4

    const/16 v3, 0x5dc

    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4, v10, v11, v12}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v4}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    new-instance v5, Ljava/util/GregorianCalendar;

    const/16 v7, 0xd

    invoke-direct {v5, v10, v12, v7}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v5}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, LX/2Cm;-><init>(Ljava/lang/String;IILjava/util/Date;Ljava/util/Date;LX/F6o;)V

    sput-object v0, LX/2Cm;->FAST_PW_ERROR_AR:LX/2Cm;

    .line 383220
    new-instance v0, LX/2Cm;

    const-string v1, "FULL_WIDTH_REG_BUTTON"

    const/4 v2, 0x5

    const/16 v3, 0x5dc

    new-instance v4, Ljava/util/GregorianCalendar;

    const/16 v5, 0x9

    const/16 v7, 0x14

    invoke-direct {v4, v10, v5, v7}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v4}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    new-instance v5, Ljava/util/GregorianCalendar;

    const/16 v7, 0xa

    const/16 v8, 0x15

    invoke-direct {v5, v10, v7, v8}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v5}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, LX/2Cm;-><init>(Ljava/lang/String;IILjava/util/Date;Ljava/util/Date;LX/F6o;)V

    sput-object v0, LX/2Cm;->FULL_WIDTH_REG_BUTTON:LX/2Cm;

    .line 383221
    new-instance v0, LX/2Cm;

    const-string v1, "LOGIN_TO_REG_PREFILL"

    const/16 v3, 0x5dc

    new-instance v2, Ljava/util/GregorianCalendar;

    const/16 v4, 0xb

    const/4 v5, 0x5

    invoke-direct {v2, v10, v4, v5}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    new-instance v2, Ljava/util/GregorianCalendar;

    const/16 v5, 0x7e1

    const/4 v7, 0x0

    invoke-direct {v2, v5, v7, v13}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move v2, v13

    invoke-direct/range {v0 .. v6}, LX/2Cm;-><init>(Ljava/lang/String;IILjava/util/Date;Ljava/util/Date;LX/F6o;)V

    sput-object v0, LX/2Cm;->LOGIN_TO_REG_PREFILL:LX/2Cm;

    .line 383222
    new-instance v0, LX/2Cm;

    const-string v1, "WHITE_LOGIN_PAGE"

    const/16 v3, 0x3e8

    new-instance v2, Ljava/util/GregorianCalendar;

    const/16 v4, 0xa

    const/16 v5, 0x1c

    invoke-direct {v2, v10, v4, v5}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    new-instance v2, Ljava/util/GregorianCalendar;

    const/16 v5, 0xb

    const/16 v7, 0x17

    invoke-direct {v2, v10, v5, v7}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move v2, v11

    invoke-direct/range {v0 .. v6}, LX/2Cm;-><init>(Ljava/lang/String;IILjava/util/Date;Ljava/util/Date;LX/F6o;)V

    sput-object v0, LX/2Cm;->WHITE_LOGIN_PAGE:LX/2Cm;

    .line 383223
    new-instance v0, LX/2Cm;

    const-string v1, "HIDE_REGISTER_BUTTON"

    const/16 v3, 0x3e8

    new-instance v2, Ljava/util/GregorianCalendar;

    const/16 v4, 0xb

    const/16 v5, 0xc

    invoke-direct {v2, v10, v4, v5}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    new-instance v2, Ljava/util/GregorianCalendar;

    const/16 v5, 0x7e1

    const/4 v7, 0x0

    const/16 v8, 0xd

    invoke-direct {v2, v5, v7, v8}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move v2, v12

    invoke-direct/range {v0 .. v6}, LX/2Cm;-><init>(Ljava/lang/String;IILjava/util/Date;Ljava/util/Date;LX/F6o;)V

    sput-object v0, LX/2Cm;->HIDE_REGISTER_BUTTON:LX/2Cm;

    .line 383224
    const/16 v0, 0x9

    new-array v0, v0, [LX/2Cm;

    const/4 v1, 0x0

    sget-object v2, LX/2Cm;->AR_BOUNCE_FROM_MSITE:LX/2Cm;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/2Cm;->LOGIN_ERROR_REG:LX/2Cm;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/2Cm;->LOGIN_ERROR_REG_EMAIL:LX/2Cm;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/2Cm;->LOGIN_ERROR_REG_PHONE:LX/2Cm;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/2Cm;->FAST_PW_ERROR_AR:LX/2Cm;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/2Cm;->FULL_WIDTH_REG_BUTTON:LX/2Cm;

    aput-object v2, v0, v1

    sget-object v1, LX/2Cm;->LOGIN_TO_REG_PREFILL:LX/2Cm;

    aput-object v1, v0, v13

    sget-object v1, LX/2Cm;->WHITE_LOGIN_PAGE:LX/2Cm;

    aput-object v1, v0, v11

    sget-object v1, LX/2Cm;->HIDE_REGISTER_BUTTON:LX/2Cm;

    aput-object v1, v0, v12

    sput-object v0, LX/2Cm;->$VALUES:[LX/2Cm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/util/Date;Ljava/util/Date;LX/F6o;)V
    .locals 2
    .param p6    # LX/F6o;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "LX/F6o;",
            ")V"
        }
    .end annotation

    .prologue
    .line 383203
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 383204
    if-ltz p3, :cond_0

    const/16 v0, 0x2710

    if-le p3, v0, :cond_1

    .line 383205
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid IALExperiment threshold specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383206
    :cond_1
    iput-object p4, p0, LX/2Cm;->startDate:Ljava/util/Date;

    .line 383207
    iput-object p5, p0, LX/2Cm;->endDate:Ljava/util/Date;

    .line 383208
    iput-object p6, p0, LX/2Cm;->condition:LX/F6o;

    .line 383209
    if-eqz p4, :cond_2

    if-nez p5, :cond_3

    .line 383210
    :cond_2
    const/4 p3, 0x0

    .line 383211
    :cond_3
    iput p3, p0, LX/2Cm;->threshold:I

    .line 383212
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Cm;
    .locals 1

    .prologue
    .line 383214
    const-class v0, LX/2Cm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Cm;

    return-object v0
.end method

.method public static values()[LX/2Cm;
    .locals 1

    .prologue
    .line 383213
    sget-object v0, LX/2Cm;->$VALUES:[LX/2Cm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Cm;

    return-object v0
.end method
