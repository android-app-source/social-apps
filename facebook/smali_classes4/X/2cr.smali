.class public LX/2cr;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/0Vf;


# instance fields
.field public final a:LX/2cw;

.field public final b:LX/2d0;

.field private final c:LX/2d2;

.field public final d:LX/2d3;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/2dV;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/2d6;

.field private final h:LX/2cs;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/2dB;

.field public final k:LX/2dD;

.field private final l:LX/2d7;

.field public m:Z

.field public n:LX/0hs;

.field private o:Z


# direct methods
.method public constructor <init>(LX/0ad;LX/2cs;LX/2cw;LX/2d0;LX/2d2;LX/2d3;LX/0Ot;LX/0Ot;LX/2d6;LX/2d8;Ljava/util/concurrent/Executor;)V
    .locals 2
    .param p11    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/2cs;",
            "LX/2cw;",
            "LX/2d0;",
            "LX/2d2;",
            "LX/2d3;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/2d6;",
            "LX/2d8;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 442245
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 442246
    new-instance v0, LX/2dA;

    invoke-direct {v0, p0}, LX/2dA;-><init>(LX/2cr;)V

    iput-object v0, p0, LX/2cr;->j:LX/2dB;

    .line 442247
    new-instance v0, LX/2dC;

    invoke-direct {v0, p0}, LX/2dC;-><init>(LX/2cr;)V

    iput-object v0, p0, LX/2cr;->k:LX/2dD;

    .line 442248
    new-instance v0, LX/2dE;

    invoke-direct {v0, p0}, LX/2dE;-><init>(LX/2cr;)V

    iput-object v0, p0, LX/2cr;->l:LX/2d7;

    .line 442249
    iput-boolean v1, p0, LX/2cr;->m:Z

    .line 442250
    const/4 v0, 0x0

    iput-object v0, p0, LX/2cr;->n:LX/0hs;

    .line 442251
    iput-boolean v1, p0, LX/2cr;->o:Z

    .line 442252
    iput-object p8, p0, LX/2cr;->i:LX/0Ot;

    .line 442253
    iput-object p3, p0, LX/2cr;->a:LX/2cw;

    .line 442254
    iput-object p4, p0, LX/2cr;->b:LX/2d0;

    .line 442255
    iput-object p5, p0, LX/2cr;->c:LX/2d2;

    .line 442256
    iput-object p6, p0, LX/2cr;->d:LX/2d3;

    .line 442257
    iput-object p7, p0, LX/2cr;->e:LX/0Ot;

    .line 442258
    iget-object v0, p0, LX/2cr;->b:LX/2d0;

    iget-object v1, p0, LX/2cr;->j:LX/2dB;

    invoke-virtual {v0, v1}, LX/2d0;->a(LX/2dB;)V

    .line 442259
    new-instance v0, LX/2dF;

    invoke-direct {v0, p0, p1}, LX/2dF;-><init>(LX/2cr;LX/0ad;)V

    invoke-static {v0}, LX/0Wf;->memoize(LX/0QR;)LX/0QR;

    move-result-object v0

    iput-object v0, p0, LX/2cr;->f:LX/0QR;

    .line 442260
    iput-object p9, p0, LX/2cr;->g:LX/2d6;

    .line 442261
    iput-object p2, p0, LX/2cr;->h:LX/2cs;

    .line 442262
    iget-object v0, p0, LX/2cr;->g:LX/2d6;

    iget-object v1, p0, LX/2cr;->l:LX/2d7;

    invoke-virtual {v0, v1}, LX/2d6;->a(LX/2d7;)Z

    .line 442263
    new-instance v0, LX/2dG;

    invoke-direct {v0, p0}, LX/2dG;-><init>(LX/2cr;)V

    invoke-static {p10, v0, p11}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 442264
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 442240
    iget-object v0, p0, LX/2cr;->n:LX/0hs;

    if-nez v0, :cond_0

    .line 442241
    :goto_0
    return-void

    .line 442242
    :cond_0
    iget-object v0, p0, LX/2cr;->n:LX/0hs;

    const/4 v1, 0x0

    .line 442243
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 442244
    iget-object v0, p0, LX/2cr;->n:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    goto :goto_0
.end method

.method public static a$redex0(LX/2cr;Z)V
    .locals 1

    .prologue
    .line 442236
    iget-boolean v0, p0, LX/2cr;->o:Z

    if-eq v0, p1, :cond_0

    .line 442237
    iput-boolean p1, p0, LX/2cr;->o:Z

    .line 442238
    const v0, -0x74bf42ca

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 442239
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 442162
    new-instance v0, LX/DEd;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DEd;-><init>(Landroid/content/Context;)V

    .line 442163
    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 442183
    check-cast p3, LX/DEd;

    .line 442184
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, LX/DEd;->setVisibility(I)V

    .line 442185
    iget-object v0, p0, LX/2cr;->b:LX/2d0;

    invoke-virtual {v0}, LX/2d0;->a()Lcom/facebook/placetips/bootstrap/PresenceDescription;

    move-result-object v0

    .line 442186
    iget-object v1, p0, LX/2cr;->h:LX/2cs;

    invoke-virtual {v1}, LX/2cs;->a()Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;

    move-result-object v1

    .line 442187
    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->l()LX/2cx;

    move-result-object v2

    sget-object v3, LX/2cx;->INJECT:LX/2cx;

    if-eq v2, v3, :cond_1

    .line 442188
    :cond_0
    iget-object v0, p0, LX/2cr;->c:LX/2d2;

    const/4 p1, 0x0

    .line 442189
    invoke-virtual {p3}, LX/DEd;->a()V

    .line 442190
    invoke-virtual {v1}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->a()Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    .line 442191
    invoke-virtual {v1}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->b()LX/175;

    move-result-object v3

    invoke-virtual {p3, v3}, LX/DEd;->setTitle(LX/175;)V

    .line 442192
    invoke-virtual {p3, p1}, LX/DEd;->setSubText(LX/175;)V

    .line 442193
    invoke-virtual {p3, p1}, LX/DEd;->setSourceText(LX/2cx;)V

    .line 442194
    invoke-virtual {v2}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->c()LX/1Fb;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 442195
    invoke-virtual {v2}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->c()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, LX/DEd;->setIconImage(Ljava/lang/String;)V

    .line 442196
    :goto_0
    iget-object v3, v0, LX/2d2;->e:LX/2cw;

    sget-object p1, LX/3FC;->EVENT_TIP_VIEW:LX/3FC;

    sget-object p2, LX/2cx;->GPS:LX/2cx;

    invoke-virtual {v2}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->b()Ljava/lang/String;

    move-result-object v2

    const/4 p4, 0x0

    invoke-virtual {v3, p1, p2, v2, p4}, LX/2cw;->a(LX/3FC;LX/2cx;Ljava/lang/String;Z)V

    .line 442197
    invoke-direct {p0}, LX/2cr;->a()V

    .line 442198
    new-instance v0, LX/DEZ;

    invoke-direct {v0, p0, v1}, LX/DEZ;-><init>(LX/2cr;Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;)V

    invoke-virtual {p3, v0}, LX/DEd;->setPlaceTipOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 442199
    :goto_1
    return-void

    .line 442200
    :cond_1
    if-nez v0, :cond_2

    .line 442201
    iget-object v0, p0, LX/2cr;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "place_tips"

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "try to bind place tips feed unit w/ null presence description"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 442202
    invoke-direct {p0}, LX/2cr;->a()V

    .line 442203
    const/16 v0, 0x8

    invoke-virtual {p3, v0}, LX/DEd;->setVisibility(I)V

    goto :goto_1

    .line 442204
    :cond_2
    iget-object v1, p0, LX/2cr;->c:LX/2d2;

    const/4 v2, 0x0

    .line 442205
    invoke-virtual {p3}, LX/DEd;->a()V

    .line 442206
    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->b()LX/175;

    move-result-object v3

    invoke-virtual {p3, v3}, LX/DEd;->setTitle(LX/175;)V

    .line 442207
    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->c()LX/175;

    move-result-object v3

    invoke-virtual {p3, v3}, LX/DEd;->setSubText(LX/175;)V

    .line 442208
    invoke-virtual {p3, v2}, LX/DEd;->setIconImage(Ljava/lang/String;)V

    .line 442209
    const/4 p1, 0x0

    .line 442210
    iget-object v3, v1, LX/2d2;->j:Ljava/lang/Boolean;

    if-nez v3, :cond_3

    .line 442211
    iget-object v3, v1, LX/2d2;->f:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, v1, LX/2d2;->g:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/03R;

    invoke-virtual {v3, p1}, LX/03R;->asBoolean(Z)Z

    move-result v3

    if-eqz v3, :cond_9

    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v1, LX/2d2;->j:Ljava/lang/Boolean;

    .line 442212
    :cond_3
    iget-object v3, v1, LX/2d2;->j:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    move v3, v3

    .line 442213
    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->l()LX/2cx;

    move-result-object v2

    :cond_4
    invoke-virtual {p3, v2}, LX/DEd;->setSourceText(LX/2cx;)V

    .line 442214
    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, v1, LX/2d2;->c:LX/0Uh;

    const/16 v3, 0x5c2

    const/4 p1, 0x0

    invoke-virtual {v2, v3, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_a

    .line 442215
    :cond_5
    :goto_3
    iget-object v2, v1, LX/2d2;->e:LX/2cw;

    sget-object v3, LX/3FC;->FEED_UNIT_VPV:LX/3FC;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->l()LX/2cx;

    move-result-object p1

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->d()Z

    move-result p4

    invoke-virtual {v2, v3, p1, p2, p4}, LX/2cw;->a(LX/3FC;LX/2cx;Ljava/lang/String;Z)V

    .line 442216
    new-instance v1, LX/DEa;

    invoke-direct {v1, p0}, LX/DEa;-><init>(LX/2cr;)V

    invoke-virtual {p3, v1}, LX/DEd;->setPlaceTipOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 442217
    invoke-virtual {p3}, LX/DEd;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082075

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 442218
    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->l()LX/2cx;

    move-result-object v2

    sget-object v3, LX/2cx;->POST_COMPOSE:LX/2cx;

    if-ne v2, v3, :cond_7

    iget-object v2, p0, LX/2cr;->d:LX/2d3;

    invoke-virtual {v2}, LX/2d3;->b()Z

    move-result v2

    if-nez v2, :cond_7

    .line 442219
    invoke-virtual {p3}, LX/DEd;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 442220
    iget-object v3, p0, LX/2cr;->n:LX/0hs;

    if-nez v3, :cond_6

    .line 442221
    new-instance v3, LX/0hs;

    const/4 p1, 0x2

    invoke-direct {v3, v2, p1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, LX/2cr;->n:LX/0hs;

    .line 442222
    iget-object v3, p0, LX/2cr;->n:LX/0hs;

    const/4 p1, -0x1

    .line 442223
    iput p1, v3, LX/0hs;->t:I

    .line 442224
    iget-object v3, p0, LX/2cr;->n:LX/0hs;

    sget-object p1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v3, p1}, LX/0ht;->a(LX/3AV;)V

    .line 442225
    iget-object v3, p0, LX/2cr;->a:LX/2cw;

    sget-object p1, LX/3FC;->POST_COMPOSE_TOOLTIP_SEEN:LX/3FC;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->l()LX/2cx;

    move-result-object p2

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->d()Z

    move-result p5

    invoke-virtual {v3, p1, p2, p4, p5}, LX/2cw;->a(LX/3FC;LX/2cx;Ljava/lang/String;Z)V

    .line 442226
    :cond_6
    iget-object v2, p0, LX/2cr;->n:LX/0hs;

    iget-object v3, p0, LX/2cr;->k:LX/2dD;

    .line 442227
    iput-object v3, v2, LX/0ht;->H:LX/2dD;

    .line 442228
    iget-object v2, p0, LX/2cr;->n:LX/0hs;

    invoke-virtual {v2, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 442229
    iget-object v2, p0, LX/2cr;->n:LX/0hs;

    invoke-virtual {v2, p3}, LX/0ht;->f(Landroid/view/View;)V

    .line 442230
    goto/16 :goto_1

    .line 442231
    :cond_7
    invoke-direct {p0}, LX/2cr;->a()V

    goto/16 :goto_1

    .line 442232
    :cond_8
    invoke-virtual {p3, p1}, LX/DEd;->setIconImage(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    move v3, p1

    .line 442233
    goto/16 :goto_2

    .line 442234
    :cond_a
    new-instance v2, LX/DEh;

    invoke-virtual {p3}, LX/DEd;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->e()LX/175;

    move-result-object p1

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->f()LX/175;

    move-result-object p2

    new-instance p4, LX/DEb;

    invoke-direct {p4, v1, v0, p3}, LX/DEb;-><init>(LX/2d2;Lcom/facebook/placetips/bootstrap/PresenceDescription;LX/DEd;)V

    invoke-direct {v2, v3, p1, p2, p4}, LX/DEh;-><init>(Landroid/content/Context;LX/175;LX/175;LX/DEb;)V

    .line 442235
    invoke-virtual {p3, v2}, LX/DEd;->setFooterView(Landroid/view/View;)V

    goto/16 :goto_3
.end method

.method public final dispose()V
    .locals 2

    .prologue
    .line 442179
    iget-object v0, p0, LX/2cr;->b:LX/2d0;

    iget-object v1, p0, LX/2cr;->j:LX/2dB;

    invoke-virtual {v0, v1}, LX/2d0;->b(LX/2dB;)V

    .line 442180
    iget-object v0, p0, LX/2cr;->g:LX/2d6;

    iget-object v1, p0, LX/2cr;->l:LX/2d7;

    invoke-virtual {v0, v1}, LX/2d6;->b(LX/2d7;)Z

    .line 442181
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2cr;->m:Z

    .line 442182
    return-void
.end method

.method public final getCount()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 442168
    iget-boolean v0, p0, LX/2cr;->o:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 442169
    :goto_0
    return v0

    .line 442170
    :cond_0
    iget-object v0, p0, LX/2cr;->f:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dV;

    .line 442171
    iget-object v2, p0, LX/2cr;->b:LX/2d0;

    iget-object v0, v0, LX/2dV;->a:LX/Cdj;

    const/4 v4, 0x0

    .line 442172
    invoke-static {v2}, LX/2d0;->e(LX/2d0;)Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v4

    .line 442173
    :goto_1
    move v0, v3

    .line 442174
    iget-object v2, p0, LX/2cr;->h:LX/2cs;

    .line 442175
    invoke-virtual {v2}, LX/2cs;->a()Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;

    move-result-object v3

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    :goto_2
    move v2, v3

    .line 442176
    if-nez v0, :cond_1

    if-eqz v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 442177
    :cond_3
    iget-object v3, v2, LX/2d0;->r:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    .line 442178
    if-eqz v0, :cond_4

    invoke-virtual {v3}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->q()LX/Cdj;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/Cdj;->isEqualOrGreaterThan(LX/Cdj;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    const/4 v3, 0x1

    goto :goto_1

    :cond_5
    move v3, v4

    goto :goto_1

    :cond_6
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 442167
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 442166
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 442165
    const/4 v0, 0x1

    return v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 442164
    iget-boolean v0, p0, LX/2cr;->m:Z

    return v0
.end method
