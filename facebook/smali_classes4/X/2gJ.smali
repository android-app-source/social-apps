.class public LX/2gJ;
.super LX/1qS;
.source ""

# interfaces
.implements Ljava/lang/AutoCloseable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi",
        "ImprovedNewApi"
    }
.end annotation

.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field private final a:LX/2Oz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 448278
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2gJ;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/3Ro;Ljava/lang/String;LX/2Oz;)V
    .locals 6
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 448275
    invoke-static {p4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "tincan_db_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 448276
    iput-object p6, p0, LX/2gJ;->a:LX/2Oz;

    .line 448277
    return-void
.end method

.method public static a(LX/0QB;)LX/2gJ;
    .locals 14

    .prologue
    .line 448246
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 448247
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 448248
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 448249
    if-nez v1, :cond_0

    .line 448250
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 448251
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 448252
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 448253
    sget-object v1, LX/2gJ;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 448254
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 448255
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 448256
    :cond_1
    if-nez v1, :cond_4

    .line 448257
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 448258
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 448259
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 448260
    new-instance v7, LX/2gJ;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v9

    check-cast v9, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v10

    check-cast v10, LX/1qU;

    invoke-static {v0}, LX/3Ro;->a(LX/0QB;)LX/3Ro;

    move-result-object v11

    check-cast v11, LX/3Ro;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-static {v0}, LX/2Oz;->b(LX/0QB;)LX/2Oz;

    move-result-object v13

    check-cast v13, LX/2Oz;

    invoke-direct/range {v7 .. v13}, LX/2gJ;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/3Ro;Ljava/lang/String;LX/2Oz;)V

    .line 448261
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 448262
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 448263
    if-nez v1, :cond_2

    .line 448264
    sget-object v0, LX/2gJ;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 448265
    :goto_1
    if-eqz v0, :cond_3

    .line 448266
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 448267
    :goto_3
    check-cast v0, LX/2gJ;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 448268
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 448269
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 448270
    :catchall_1
    move-exception v0

    .line 448271
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 448272
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 448273
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 448274
    :cond_2
    :try_start_8
    sget-object v0, LX/2gJ;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private n()V
    .locals 1

    .prologue
    .line 448244
    iget-object v0, p0, LX/2gJ;->a:LX/2Oz;

    invoke-virtual {v0}, LX/2Oz;->c()V

    .line 448245
    return-void
.end method

.method private o()V
    .locals 1

    .prologue
    .line 448230
    iget-object v0, p0, LX/2gJ;->a:LX/2Oz;

    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 448231
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 2

    .prologue
    .line 448238
    invoke-static {}, LX/2Oz;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2gJ;->a:LX/2Oz;

    .line 448239
    invoke-static {}, LX/2Oz;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/2Oz;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->getHoldCount()I

    move-result v1

    :goto_0
    move v0, v1

    .line 448240
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 448241
    invoke-super {p0}, LX/1qS;->i()V

    .line 448242
    :cond_0
    invoke-direct {p0}, LX/2gJ;->n()V

    .line 448243
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 448234
    invoke-direct {p0}, LX/2gJ;->o()V

    .line 448235
    invoke-super {p0}, LX/1qS;->i()V

    .line 448236
    invoke-direct {p0}, LX/2gJ;->n()V

    .line 448237
    return-void
.end method

.method public final m()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 448232
    invoke-direct {p0}, LX/2gJ;->o()V

    .line 448233
    invoke-super {p0}, LX/1qS;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method
