.class public final LX/2Cb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation


# instance fields
.field public final limit:I

.field public final omitEmptyStrings:Z

.field public final strategy:LX/29S;

.field public final trimmer:LX/1IA;


# direct methods
.method private constructor <init>(LX/29S;)V
    .locals 3

    .prologue
    .line 383027
    const/4 v0, 0x0

    sget-object v1, LX/1IA;->NONE:LX/1IA;

    const v2, 0x7fffffff

    invoke-direct {p0, p1, v0, v1, v2}, LX/2Cb;-><init>(LX/29S;ZLX/1IA;I)V

    .line 383028
    return-void
.end method

.method public constructor <init>(LX/29S;ZLX/1IA;I)V
    .locals 0

    .prologue
    .line 383031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383032
    iput-object p1, p0, LX/2Cb;->strategy:LX/29S;

    .line 383033
    iput-boolean p2, p0, LX/2Cb;->omitEmptyStrings:Z

    .line 383034
    iput-object p3, p0, LX/2Cb;->trimmer:LX/1IA;

    .line 383035
    iput p4, p0, LX/2Cb;->limit:I

    .line 383036
    return-void
.end method

.method public static on(C)LX/2Cb;
    .locals 1
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 383019
    invoke-static {p0}, LX/1IA;->is(C)LX/1IA;

    move-result-object v0

    invoke-static {v0}, LX/2Cb;->on(LX/1IA;)LX/2Cb;

    move-result-object v0

    return-object v0
.end method

.method public static on(LX/1IA;)LX/2Cb;
    .locals 2
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 383029
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 383030
    new-instance v0, LX/2Cb;

    new-instance v1, LX/29R;

    invoke-direct {v1, p0}, LX/29R;-><init>(LX/1IA;)V

    invoke-direct {v0, v1}, LX/2Cb;-><init>(LX/29S;)V

    return-object v0
.end method

.method public static on(Ljava/lang/String;)LX/2Cb;
    .locals 4
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 383020
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "The separator may not be the empty string."

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 383021
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 383022
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, LX/2Cb;->on(C)LX/2Cb;

    move-result-object v0

    .line 383023
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 383024
    goto :goto_0

    .line 383025
    :cond_1
    new-instance v0, LX/2Cb;

    new-instance v1, LX/4w5;

    invoke-direct {v1, p0}, LX/4w5;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, LX/2Cb;-><init>(LX/29S;)V

    goto :goto_1
.end method

.method public static splittingIterator(LX/2Cb;Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 383026
    iget-object v0, p0, LX/2Cb;->strategy:LX/29S;

    invoke-interface {v0, p0, p1}, LX/29S;->iterator(LX/2Cb;Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final limit(I)LX/2Cb;
    .locals 5
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 383006
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "must be greater than zero: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 383007
    new-instance v0, LX/2Cb;

    iget-object v1, p0, LX/2Cb;->strategy:LX/29S;

    iget-boolean v2, p0, LX/2Cb;->omitEmptyStrings:Z

    iget-object v3, p0, LX/2Cb;->trimmer:LX/1IA;

    invoke-direct {v0, v1, v2, v3, p1}, LX/2Cb;-><init>(LX/29S;ZLX/1IA;I)V

    return-object v0

    :cond_0
    move v0, v2

    .line 383008
    goto :goto_0
.end method

.method public final omitEmptyStrings()LX/2Cb;
    .locals 5
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 383003
    new-instance v0, LX/2Cb;

    iget-object v1, p0, LX/2Cb;->strategy:LX/29S;

    const/4 v2, 0x1

    iget-object v3, p0, LX/2Cb;->trimmer:LX/1IA;

    iget v4, p0, LX/2Cb;->limit:I

    invoke-direct {v0, v1, v2, v3, v4}, LX/2Cb;-><init>(LX/29S;ZLX/1IA;I)V

    return-object v0
.end method

.method public final split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 383004
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 383005
    new-instance v0, LX/29T;

    invoke-direct {v0, p0, p1}, LX/29T;-><init>(LX/2Cb;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public final splitToList(Ljava/lang/CharSequence;)Ljava/util/List;
    .locals 3
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 383009
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 383010
    invoke-static {p0, p1}, LX/2Cb;->splittingIterator(LX/2Cb;Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    .line 383011
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 383012
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 383013
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 383014
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final trimResults()LX/2Cb;
    .locals 5
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 383015
    sget-object v0, LX/1IA;->WHITESPACE:LX/1IA;

    .line 383016
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 383017
    new-instance v1, LX/2Cb;

    iget-object v2, p0, LX/2Cb;->strategy:LX/29S;

    iget-boolean v3, p0, LX/2Cb;->omitEmptyStrings:Z

    iget v4, p0, LX/2Cb;->limit:I

    invoke-direct {v1, v2, v3, v0, v4}, LX/2Cb;-><init>(LX/29S;ZLX/1IA;I)V

    move-object v0, v1

    .line 383018
    return-object v0
.end method
