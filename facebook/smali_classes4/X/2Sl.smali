.class public LX/2Sl;
.super LX/2SP;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile o:LX/2Sl;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0Uh;

.field private final c:LX/2Sj;

.field private final d:LX/2Sf;

.field private final e:LX/2Sm;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0SG;

.field public final h:LX/2Sc;

.field private final i:Ljava/util/concurrent/ExecutorService;

.field public j:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Cw5;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private k:LX/Cw5;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private l:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private m:LX/2SR;

.field public n:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0Uh;LX/2Sj;LX/2Sf;LX/2Sg;LX/2Sm;LX/0Ot;LX/0SG;LX/2Sc;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p7    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p10    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/2Sj;",
            "LX/2Sf;",
            "LX/2Sg;",
            "LX/2Sm;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;",
            "LX/0SG;",
            "LX/2Sc;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 413257
    invoke-direct {p0, p5}, LX/2SP;-><init>(LX/2Sg;)V

    .line 413258
    iput-object p1, p0, LX/2Sl;->a:Landroid/content/res/Resources;

    .line 413259
    iput-object p2, p0, LX/2Sl;->b:LX/0Uh;

    .line 413260
    iput-object p3, p0, LX/2Sl;->c:LX/2Sj;

    .line 413261
    iput-object p4, p0, LX/2Sl;->d:LX/2Sf;

    .line 413262
    iput-object p6, p0, LX/2Sl;->e:LX/2Sm;

    .line 413263
    iput-object p7, p0, LX/2Sl;->f:LX/0Ot;

    .line 413264
    iput-object p8, p0, LX/2Sl;->g:LX/0SG;

    .line 413265
    iput-object p9, p0, LX/2Sl;->h:LX/2Sc;

    .line 413266
    iput-object p10, p0, LX/2Sl;->i:Ljava/util/concurrent/ExecutorService;

    .line 413267
    return-void
.end method

.method public static a(LX/0QB;)LX/2Sl;
    .locals 14

    .prologue
    .line 413189
    sget-object v0, LX/2Sl;->o:LX/2Sl;

    if-nez v0, :cond_1

    .line 413190
    const-class v1, LX/2Sl;

    monitor-enter v1

    .line 413191
    :try_start_0
    sget-object v0, LX/2Sl;->o:LX/2Sl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413192
    if-eqz v2, :cond_0

    .line 413193
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 413194
    new-instance v3, LX/2Sl;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/2Sj;->a(LX/0QB;)LX/2Sj;

    move-result-object v6

    check-cast v6, LX/2Sj;

    invoke-static {v0}, LX/2Sf;->a(LX/0QB;)LX/2Sf;

    move-result-object v7

    check-cast v7, LX/2Sf;

    invoke-static {v0}, LX/2Sg;->a(LX/0QB;)LX/2Sg;

    move-result-object v8

    check-cast v8, LX/2Sg;

    invoke-static {v0}, LX/2Sm;->a(LX/0QB;)LX/2Sm;

    move-result-object v9

    check-cast v9, LX/2Sm;

    const/16 v10, 0x1646

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v12

    check-cast v12, LX/2Sc;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v3 .. v13}, LX/2Sl;-><init>(Landroid/content/res/Resources;LX/0Uh;LX/2Sj;LX/2Sf;LX/2Sg;LX/2Sm;LX/0Ot;LX/0SG;LX/2Sc;Ljava/util/concurrent/ExecutorService;)V

    .line 413195
    move-object v0, v3

    .line 413196
    sput-object v0, LX/2Sl;->o:LX/2Sl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413197
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413198
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413199
    :cond_1
    sget-object v0, LX/2Sl;->o:LX/2Sl;

    return-object v0

    .line 413200
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413201
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized b(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/EPu;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Cw5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413231
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Sl;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 413232
    iget-object v0, p0, LX/2Sl;->j:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413233
    :goto_0
    monitor-exit p0

    return-object v0

    .line 413234
    :cond_0
    :try_start_1
    invoke-virtual {p0}, LX/2SP;->kF_()V

    .line 413235
    iget-object v1, p0, LX/2Sl;->e:LX/2Sm;

    invoke-static {p2}, LX/2SP;->a(LX/EPu;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/0zS;->d:LX/0zS;

    :goto_1
    const/4 v10, 0x1

    .line 413236
    new-instance v5, LX/A0D;

    invoke-direct {v5}, LX/A0D;-><init>()V

    .line 413237
    new-instance v5, LX/A0C;

    invoke-direct {v5}, LX/A0C;-><init>()V

    move-object v5, v5

    .line 413238
    const-string v6, "count"

    const/16 v7, 0xf

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "profile_picture_size"

    iget-object v8, v1, LX/2Sm;->a:Landroid/content/res/Resources;

    const v9, 0x7f0b14f2

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    .line 413239
    iput-boolean v10, v6, LX/0gW;->l:Z

    .line 413240
    iget-object v6, v1, LX/2Sm;->b:LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    const-string v7, "pymk_search_cache_tag"

    invoke-static {v7}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v7

    .line 413241
    iput-object v7, v5, LX/0zO;->d:Ljava/util/Set;

    .line 413242
    move-object v5, v5

    .line 413243
    iput-boolean v10, v5, LX/0zO;->p:Z

    .line 413244
    move-object v5, v5

    .line 413245
    sget-object v7, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v5, v7}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v5

    .line 413246
    iput-object p1, v5, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 413247
    move-object v5, v5

    .line 413248
    const-wide/16 v7, 0xe10

    invoke-virtual {v5, v7, v8}, LX/0zO;->a(J)LX/0zO;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    .line 413249
    new-instance v6, LX/Fhu;

    invoke-direct {v6, v1}, LX/Fhu;-><init>(LX/2Sm;)V

    iget-object v7, v1, LX/2Sm;->e:LX/0TD;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 413250
    iput-object v0, p0, LX/2Sl;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 413251
    iget-object v0, p0, LX/2Sl;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/search/suggestions/nullstate/PYMKNullStateSupplier$1;

    invoke-direct {v1, p0}, Lcom/facebook/search/suggestions/nullstate/PYMKNullStateSupplier$1;-><init>(LX/2Sl;)V

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/2Sl;->l:Ljava/util/concurrent/ScheduledFuture;

    .line 413252
    new-instance v0, LX/Fhv;

    invoke-direct {v0, p0}, LX/Fhv;-><init>(LX/2Sl;)V

    .line 413253
    iget-object v1, p0, LX/2Sl;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v2, p0, LX/2Sl;->i:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 413254
    iget-object v0, p0, LX/2Sl;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_0

    .line 413255
    :cond_1
    sget-object v0, LX/0zS;->a:LX/0zS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 413256
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/2Sl;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 413226
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/Cw5;

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/2Sl;->g:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, LX/Cw5;-><init>(LX/0Px;J)V

    iput-object v0, p0, LX/2Sl;->k:LX/Cw5;

    .line 413227
    iget-object v0, p0, LX/2Sl;->m:LX/2SR;

    if-eqz v0, :cond_0

    .line 413228
    iget-object v0, p0, LX/2Sl;->m:LX/2SR;

    sget-object v1, LX/7BE;->READY:LX/7BE;

    invoke-interface {v0, v1}, LX/2SR;->a(LX/7BE;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413229
    :cond_0
    monitor-exit p0

    return-void

    .line 413230
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized i(LX/2Sl;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 413221
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/2Sl;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Sl;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 413222
    iget-object v1, p0, LX/2Sl;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 413223
    const/4 v1, 0x0

    iput-object v1, p0, LX/2Sl;->j:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413224
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 413225
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static k(LX/2Sl;)V
    .locals 2

    .prologue
    .line 413217
    iget-object v0, p0, LX/2Sl;->l:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 413218
    iget-object v0, p0, LX/2Sl;->l:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 413219
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Sl;->l:Ljava/util/concurrent/ScheduledFuture;

    .line 413220
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/7BE;
    .locals 8

    .prologue
    .line 413211
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Sl;->k:LX/Cw5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Sl;->k:LX/Cw5;

    invoke-virtual {v0}, LX/Cw5;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Sl;->c:LX/2Sj;

    const-wide/32 v2, 0x15180

    iget-object v1, p0, LX/2Sl;->k:LX/Cw5;

    .line 413212
    iget-wide v6, v1, LX/Cw5;->b:J

    move-wide v4, v6

    .line 413213
    invoke-virtual {v0, v2, v3, v4, v5}, LX/2Sj;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413214
    sget-object v0, LX/7BE;->READY:LX/7BE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413215
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, LX/7BE;->NOT_READY:LX/7BE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 413216
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/2SR;LX/2Sp;)V
    .locals 0

    .prologue
    .line 413209
    iput-object p1, p0, LX/2Sl;->m:LX/2SR;

    .line 413210
    return-void
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 0
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 413207
    invoke-direct {p0, p1, p2}, LX/2Sl;->b(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 413208
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 413206
    iget-object v0, p0, LX/2Sl;->b:LX/0Uh;

    sget v1, LX/2SU;->b:I

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 413203
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/2Sl;->k:LX/Cw5;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413204
    monitor-exit p0

    return-void

    .line 413205
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 413202
    const-string v0, "pymk_network"

    return-object v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 413168
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Sl;->k:LX/Cw5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Sl;->k:LX/Cw5;

    invoke-virtual {v0}, LX/Cw5;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, LX/2Sl;->n:I

    if-nez v0, :cond_1

    .line 413169
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413170
    :goto_0
    monitor-exit p0

    return-object v0

    .line 413171
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2Sl;->k:LX/Cw5;

    .line 413172
    iget-object v1, v0, LX/Cw5;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    move v0, v1

    .line 413173
    iget v1, p0, LX/2Sl;->n:I

    if-le v0, v1, :cond_2

    iget-object v0, p0, LX/2Sl;->k:LX/Cw5;

    .line 413174
    iget-object v1, v0, LX/Cw5;->a:LX/0Px;

    move-object v0, v1

    .line 413175
    const/4 v1, 0x0

    iget v2, p0, LX/2Sl;->n:I

    invoke-virtual {v0, v1, v2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    .line 413176
    :goto_1
    new-instance v1, LX/Cwa;

    invoke-direct {v1}, LX/Cwa;-><init>()V

    sget-object v2, LX/Cwb;->PYMK:LX/Cwb;

    .line 413177
    iput-object v2, v1, LX/Cwa;->a:LX/Cwb;

    .line 413178
    move-object v1, v1

    .line 413179
    iget-object v2, p0, LX/2Sl;->a:Landroid/content/res/Resources;

    const v3, 0x7f080f77

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 413180
    iput-object v2, v1, LX/Cwa;->c:Ljava/lang/String;

    .line 413181
    move-object v1, v1

    .line 413182
    iput-object v0, v1, LX/Cwa;->b:LX/0Px;

    .line 413183
    move-object v0, v1

    .line 413184
    invoke-virtual {v0}, LX/Cwa;->a()LX/Cwc;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/2Sf;->b(Ljava/util/List;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 413185
    :cond_2
    iget-object v0, p0, LX/2Sl;->k:LX/Cw5;

    .line 413186
    iget-object v1, v0, LX/Cw5;->a:LX/0Px;

    move-object v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413187
    goto :goto_1

    .line 413188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
