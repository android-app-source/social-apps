.class public LX/2gX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:[B

.field public final c:J


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 448579
    const-string v0, "topic_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "payload"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    const-string v2, "received_time_ms"

    const-wide/16 v4, 0x0

    invoke-virtual {p1, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, LX/2gX;-><init>(Ljava/lang/String;[BJ)V

    .line 448580
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BJ)V
    .locals 1

    .prologue
    .line 448574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448575
    iput-object p1, p0, LX/2gX;->a:Ljava/lang/String;

    .line 448576
    iput-object p2, p0, LX/2gX;->b:[B

    .line 448577
    iput-wide p3, p0, LX/2gX;->c:J

    .line 448578
    return-void
.end method
