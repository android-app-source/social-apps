.class public final LX/24u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1JZ;

.field private final b:Lcom/facebook/photos/prefetch/PrefetchParams;


# direct methods
.method public constructor <init>(LX/1JZ;Lcom/facebook/photos/prefetch/PrefetchParams;)V
    .locals 0

    .prologue
    .line 367833
    iput-object p1, p0, LX/24u;->a:LX/1JZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367834
    iput-object p2, p0, LX/24u;->b:Lcom/facebook/photos/prefetch/PrefetchParams;

    .line 367835
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 367836
    iget-object v0, p0, LX/24u;->a:LX/1JZ;

    iget-object v0, v0, LX/1JZ;->d:Ljava/util/HashMap;

    iget-object v1, p0, LX/24u;->b:Lcom/facebook/photos/prefetch/PrefetchParams;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 367837
    iget-object v0, p0, LX/24u;->a:LX/1JZ;

    invoke-static {v0}, LX/1JZ;->a(LX/1JZ;)V

    .line 367838
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 367839
    iget-object v0, p0, LX/24u;->a:LX/1JZ;

    iget-object v0, v0, LX/1JZ;->e:LX/0aq;

    iget-object v1, p0, LX/24u;->b:Lcom/facebook/photos/prefetch/PrefetchParams;

    invoke-virtual {v0, v1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 367840
    iget-object v0, p0, LX/24u;->a:LX/1JZ;

    iget-object v0, v0, LX/1JZ;->e:LX/0aq;

    iget-object v1, p0, LX/24u;->b:Lcom/facebook/photos/prefetch/PrefetchParams;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367841
    iget-object v0, p0, LX/24u;->a:LX/1JZ;

    iget-object v0, v0, LX/1JZ;->a:LX/1Jd;

    iget-object v1, p0, LX/24u;->b:Lcom/facebook/photos/prefetch/PrefetchParams;

    invoke-virtual {v0, v1}, LX/1Jd;->a(Lcom/facebook/photos/prefetch/PrefetchParams;)V

    .line 367842
    :cond_0
    iget-object v0, p0, LX/24u;->a:LX/1JZ;

    iget-object v0, v0, LX/1JZ;->c:Ljava/util/List;

    iget-object v1, p0, LX/24u;->b:Lcom/facebook/photos/prefetch/PrefetchParams;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367843
    iget-object v0, p0, LX/24u;->a:LX/1JZ;

    iget-object v1, p0, LX/24u;->b:Lcom/facebook/photos/prefetch/PrefetchParams;

    invoke-static {v0, v1}, LX/1JZ;->b(LX/1JZ;Lcom/facebook/photos/prefetch/PrefetchParams;)V

    .line 367844
    :cond_1
    iget-object v0, p0, LX/24u;->a:LX/1JZ;

    iget-object v0, v0, LX/1JZ;->d:Ljava/util/HashMap;

    iget-object v1, p0, LX/24u;->b:Lcom/facebook/photos/prefetch/PrefetchParams;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 367845
    iget-object v0, p0, LX/24u;->a:LX/1JZ;

    invoke-static {v0}, LX/1JZ;->a(LX/1JZ;)V

    .line 367846
    return-void
.end method
