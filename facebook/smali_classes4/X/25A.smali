.class public abstract LX/25A;
.super LX/0k9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        ">",
        "LX/0k9",
        "<TD;>;"
    }
.end annotation


# instance fields
.field public volatile a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/25A",
            "<TD;>.",
            "LoadTask;"
        }
    .end annotation
.end field

.field public volatile b:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/25A",
            "<TD;>.",
            "LoadTask;"
        }
    .end annotation
.end field

.field public c:J

.field public d:J

.field public e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 368279
    invoke-direct {p0, p1}, LX/0k9;-><init>(Landroid/content/Context;)V

    .line 368280
    const-wide/16 v0, -0x2710

    iput-wide v0, p0, LX/25A;->d:J

    .line 368281
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 368341
    invoke-super {p0}, LX/0k9;->a()V

    .line 368342
    invoke-virtual {p0}, LX/25A;->b()Z

    .line 368343
    new-instance v0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    invoke-direct {v0, p0}, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;-><init>(LX/25A;)V

    iput-object v0, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    .line 368344
    invoke-virtual {p0}, LX/25A;->c()V

    .line 368345
    return-void
.end method

.method public final a(Landroid/support/v4/content/AsyncTaskLoader$LoadTask;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/25A",
            "<TD;>.",
            "LoadTask;",
            "TD;)V"
        }
    .end annotation

    .prologue
    .line 368333
    invoke-virtual {p0, p2}, LX/25A;->a(Ljava/lang/Object;)V

    .line 368334
    iget-object v0, p0, LX/25A;->b:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    if-ne v0, p1, :cond_1

    .line 368335
    iget-boolean v0, p0, LX/0k9;->t:Z

    if-eqz v0, :cond_0

    .line 368336
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0k9;->s:Z

    .line 368337
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/25A;->d:J

    .line 368338
    const/4 v0, 0x0

    iput-object v0, p0, LX/25A;->b:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    .line 368339
    invoke-virtual {p0}, LX/25A;->c()V

    .line 368340
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .line 368332
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 368314
    invoke-super {p0, p1, p2, p3, p4}, LX/0k9;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 368315
    iget-object v0, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    if-eqz v0, :cond_0

    .line 368316
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTask="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 368317
    const-string v0, " waiting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    iget-boolean v0, v0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->b:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 368318
    :cond_0
    iget-object v0, p0, LX/25A;->b:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    if-eqz v0, :cond_1

    .line 368319
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCancellingTask="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/25A;->b:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 368320
    const-string v0, " waiting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/25A;->b:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    iget-boolean v0, v0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->b:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 368321
    :cond_1
    iget-wide v0, p0, LX/25A;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 368322
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mUpdateThrottle="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 368323
    iget-wide v0, p0, LX/25A;->c:J

    .line 368324
    const/4 v2, 0x0

    invoke-static {v0, v1, p3, v2}, LX/3rM;->a(JLjava/io/PrintWriter;I)V

    .line 368325
    const-string v0, " mLastLoadCompleteTime="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 368326
    iget-wide v0, p0, LX/25A;->d:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 368327
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_3

    .line 368328
    const-string v4, "--"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 368329
    :goto_0
    invoke-virtual {p3}, Ljava/io/PrintWriter;->println()V

    .line 368330
    :cond_2
    return-void

    .line 368331
    :cond_3
    sub-long v4, v0, v2

    const/4 v6, 0x0

    invoke-static {v4, v5, p3, v6}, LX/3rM;->a(JLjava/io/PrintWriter;I)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 368298
    iget-object v1, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    if-eqz v1, :cond_1

    .line 368299
    iget-object v1, p0, LX/25A;->b:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    if-eqz v1, :cond_2

    .line 368300
    iget-object v1, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    iget-boolean v1, v1, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->b:Z

    if-eqz v1, :cond_0

    .line 368301
    iget-object v1, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    iput-boolean v0, v1, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->b:Z

    .line 368302
    iget-object v1, p0, LX/25A;->e:Landroid/os/Handler;

    iget-object v2, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 368303
    :cond_0
    iput-object v3, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    .line 368304
    :cond_1
    :goto_0
    return v0

    .line 368305
    :cond_2
    iget-object v1, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    iget-boolean v1, v1, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->b:Z

    if-eqz v1, :cond_3

    .line 368306
    iget-object v1, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    iput-boolean v0, v1, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->b:Z

    .line 368307
    iget-object v1, p0, LX/25A;->e:Landroid/os/Handler;

    iget-object v2, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 368308
    iput-object v3, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    goto :goto_0

    .line 368309
    :cond_3
    iget-object v1, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    .line 368310
    iget-object v2, v1, LX/3cc;->g:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    move-result v2

    move v0, v2

    .line 368311
    if-eqz v0, :cond_4

    .line 368312
    iget-object v1, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    iput-object v1, p0, LX/25A;->b:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    .line 368313
    :cond_4
    iput-object v3, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    goto :goto_0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 368282
    iget-object v0, p0, LX/25A;->b:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    if-eqz v0, :cond_1

    .line 368283
    iget-object v0, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    iget-boolean v0, v0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->b:Z

    if-eqz v0, :cond_0

    .line 368284
    iget-object v0, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->b:Z

    .line 368285
    iget-object v0, p0, LX/25A;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 368286
    :cond_0
    iget-wide v0, p0, LX/25A;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 368287
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 368288
    iget-wide v2, p0, LX/25A;->d:J

    iget-wide v4, p0, LX/25A;->c:J

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 368289
    iget-object v0, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->b:Z

    .line 368290
    iget-object v0, p0, LX/25A;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    iget-wide v2, p0, LX/25A;->d:J

    iget-wide v4, p0, LX/25A;->c:J

    add-long/2addr v2, v4

    const v4, -0x38704db9

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 368291
    :cond_1
    :goto_0
    return-void

    .line 368292
    :cond_2
    iget-object v0, p0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    sget-object v1, LX/3cc;->d:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    const v3, 0x7798c428

    const/4 v5, 0x1

    .line 368293
    invoke-static {v5}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 368294
    invoke-virtual {v0, v1, v2}, LX/3cc;->a(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)LX/3cc;

    .line 368295
    :goto_1
    goto :goto_0

    .line 368296
    :cond_3
    const/16 v4, 0xe

    invoke-static {v5, v4, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 368297
    new-instance v5, LX/JG2;

    invoke-direct {v5, v0, v3, v4}, LX/JG2;-><init>(LX/3cc;II)V

    invoke-virtual {v5, v1, v2}, LX/3cc;->a(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)LX/3cc;

    goto :goto_1
.end method

.method public abstract d()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation
.end method
