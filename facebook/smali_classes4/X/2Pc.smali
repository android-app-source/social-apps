.class public LX/2Pc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2Pc;


# instance fields
.field public final a:LX/2Kz;

.field public final b:LX/2Pd;

.field public final c:LX/2Pi;

.field private final d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Kz;LX/2Pd;LX/2Pi;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 406856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406857
    iput-object p1, p0, LX/2Pc;->a:LX/2Kz;

    .line 406858
    iput-object p2, p0, LX/2Pc;->b:LX/2Pd;

    .line 406859
    iput-object p3, p0, LX/2Pc;->c:LX/2Pi;

    .line 406860
    iput-object p4, p0, LX/2Pc;->d:Ljava/util/concurrent/ExecutorService;

    .line 406861
    return-void
.end method

.method public static a(LX/0QB;)LX/2Pc;
    .locals 7

    .prologue
    .line 406843
    sget-object v0, LX/2Pc;->e:LX/2Pc;

    if-nez v0, :cond_1

    .line 406844
    const-class v1, LX/2Pc;

    monitor-enter v1

    .line 406845
    :try_start_0
    sget-object v0, LX/2Pc;->e:LX/2Pc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406846
    if-eqz v2, :cond_0

    .line 406847
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 406848
    new-instance p0, LX/2Pc;

    invoke-static {v0}, LX/2Kz;->a(LX/0QB;)LX/2Kz;

    move-result-object v3

    check-cast v3, LX/2Kz;

    invoke-static {v0}, LX/2Pd;->a(LX/0QB;)LX/2Pd;

    move-result-object v4

    check-cast v4, LX/2Pd;

    invoke-static {v0}, LX/2Pi;->a(LX/0QB;)LX/2Pi;

    move-result-object v5

    check-cast v5, LX/2Pi;

    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2Pc;-><init>(LX/2Kz;LX/2Pd;LX/2Pi;Ljava/util/concurrent/ExecutorService;)V

    .line 406849
    move-object v0, p0

    .line 406850
    sput-object v0, LX/2Pc;->e:LX/2Pc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406851
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406852
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406853
    :cond_1
    sget-object v0, LX/2Pc;->e:LX/2Pc;

    return-object v0

    .line 406854
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406855
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 406829
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Pc;->c:LX/2Pi;

    invoke-virtual {v0, p1}, LX/2Pi;->a(Ljava/lang/String;)V

    .line 406830
    iget-object v0, p0, LX/2Pc;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$2;-><init>(LX/2Pc;Ljava/lang/String;)V

    const v2, -0xe268178

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406831
    monitor-exit p0

    return-void

    .line 406832
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Z[B)V
    .locals 7

    .prologue
    .line 406839
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Pc;->c:LX/2Pi;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/2Pi;->a(Ljava/lang/String;Ljava/lang/String;Z[B)V

    .line 406840
    iget-object v6, p0, LX/2Pc;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$1;-><init>(LX/2Pc;Ljava/lang/String;Ljava/lang/String;Z[B)V

    const v1, -0x14d15453

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406841
    monitor-exit p0

    return-void

    .line 406842
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 406836
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Pc;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$3;-><init>(LX/2Pc;Ljava/lang/String;)V

    const v2, 0x717769fc

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406837
    monitor-exit p0

    return-void

    .line 406838
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 406835
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Pc;->c:LX/2Pi;

    invoke-virtual {v0, p1}, LX/2Pi;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 406834
    iget-object v0, p0, LX/2Pc;->c:LX/2Pi;

    invoke-virtual {v0, p1}, LX/2Pi;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 406833
    iget-object v0, p0, LX/2Pc;->c:LX/2Pi;

    invoke-virtual {v0, p1}, LX/2Pi;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
