.class public final LX/2KC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/0ka;


# direct methods
.method public constructor <init>(LX/0ka;)V
    .locals 0

    .prologue
    .line 393543
    iput-object p1, p0, LX/2KC;->a:LX/0ka;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, 0x17b94997

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 393544
    :try_start_0
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;
    :try_end_0
    .catch Landroid/os/BadParcelableException; {:try_start_0 .. :try_end_0} :catch_0

    .line 393545
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393546
    iget-object v0, p0, LX/2KC;->a:LX/0ka;

    sget-object v2, LX/0ku;->WIFI_UNKNOWN:LX/0ku;

    invoke-static {v0, v2}, LX/0ka;->a$redex0(LX/0ka;LX/0ku;)V

    .line 393547
    :goto_0
    const v0, -0x2feac9af

    invoke-static {v0, v1}, LX/02F;->e(II)V

    :goto_1
    return-void

    .line 393548
    :catch_0
    move-exception v0

    .line 393549
    iget-object v2, p0, LX/2KC;->a:LX/0ka;

    sget-object v3, LX/0ku;->WIFI_UNKNOWN:LX/0ku;

    invoke-static {v2, v3}, LX/0ka;->a$redex0(LX/0ka;LX/0ku;)V

    .line 393550
    iget-object v2, p0, LX/2KC;->a:LX/0ka;

    iget-object v2, v2, LX/0ka;->f:LX/03V;

    const-class v3, LX/0ka;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 393551
    const v0, 0x6582b563

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_1

    .line 393552
    :cond_0
    iget-object v0, p0, LX/2KC;->a:LX/0ka;

    sget-object v2, LX/0ku;->WIFI_OFF:LX/0ku;

    invoke-static {v0, v2}, LX/0ka;->a$redex0(LX/0ka;LX/0ku;)V

    goto :goto_0
.end method
