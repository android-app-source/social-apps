.class public LX/2yB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 480960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 480961
    return-void
.end method

.method private static a(LX/0Px;)LX/0Px;
    .locals 1
    .param p0    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLInterfaces$GetNativeAppDetailsImageGraphQL;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/47F;",
            ">;"
        }
    .end annotation

    .prologue
    .line 480957
    if-nez p0, :cond_0

    .line 480958
    const/4 v0, 0x0

    .line 480959
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/6R2;

    invoke-direct {v0}, LX/6R2;-><init>()V

    invoke-static {p0, v0}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/1yE;Ljava/lang/String;Ljava/lang/String;)LX/47G;
    .locals 1
    .param p0    # LX/1yE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 480954
    if-nez p0, :cond_0

    .line 480955
    const/4 v0, 0x0

    .line 480956
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, LX/1yE;->q()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    invoke-static {p0, v0, p1, p2}, LX/2yB;->a(LX/1yF;Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;Ljava/lang/String;Ljava/lang/String;)LX/47G;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/1yF;Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;Ljava/lang/String;Ljava/lang/String;)LX/47G;
    .locals 19

    .prologue
    .line 480962
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 480963
    :cond_0
    const/4 v0, 0x0

    .line 480964
    :goto_0
    return-object v0

    .line 480965
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->m()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel$PlatformApplicationModel;

    move-result-object v9

    .line 480966
    if-nez v9, :cond_2

    .line 480967
    const/4 v0, 0x0

    goto :goto_0

    .line 480968
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->d()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel$DescriptionModel;

    move-result-object v2

    .line 480969
    new-instance v0, LX/47G;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    invoke-interface/range {p0 .. p0}, LX/1yF;->v_()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface/range {p0 .. p0}, LX/1yF;->v_()Ljava/lang/String;

    move-result-object v1

    :goto_1
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->dO_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->k()LX/0Px;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->o()LX/0Px;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->n()Ljava/lang/String;

    move-result-object v7

    invoke-interface/range {p0 .. p0}, LX/1yF;->p()LX/6R4;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-interface/range {p0 .. p0}, LX/1yF;->p()LX/6R4;

    move-result-object v8

    invoke-interface {v8}, LX/6R4;->b()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-interface/range {p0 .. p0}, LX/1yF;->p()LX/6R4;

    move-result-object v8

    invoke-interface {v8}, LX/6R4;->b()Ljava/lang/String;

    move-result-object v8

    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->r()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->q()I

    move-result v11

    invoke-virtual {v9}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel$PlatformApplicationModel;->b()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->b()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->dP_()Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-result-object v14

    invoke-virtual {v9}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel$PlatformApplicationModel;->c()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel$PlatformApplicationModel;->d()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->e()Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    move-result-object v18

    move-object/from16 v9, p2

    move-object/from16 v17, p3

    invoke-direct/range {v0 .. v18}, LX/47G;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;)V

    .line 480970
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->p()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;

    move-result-object v1

    invoke-static {v1}, LX/2yB;->a(Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;)Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/47G;->w:Ljava/lang/Object;

    .line 480971
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->j()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;

    move-result-object v1

    invoke-static {v1}, LX/2yB;->a(Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;)Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/47G;->x:Ljava/lang/Object;

    .line 480972
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->c()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/2yB;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/47G;->u:LX/0Px;

    .line 480973
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->l()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/2yB;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/47G;->v:LX/0Px;

    goto/16 :goto_0

    .line 480974
    :cond_3
    const-string v1, ""

    goto/16 :goto_1

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_5
    const-string v8, ""

    goto :goto_3
.end method

.method public static a(Lcom/facebook/directinstall/intent/DirectInstallAppData;Ljava/util/Map;)LX/47G;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/directinstall/intent/DirectInstallAppData;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "LX/47G;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 480939
    if-nez p0, :cond_0

    .line 480940
    const/4 v1, 0x0

    .line 480941
    :goto_0
    return-object v1

    .line 480942
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/directinstall/intent/DirectInstallAppData;->e()Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    move-result-object v13

    .line 480943
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/directinstall/intent/DirectInstallAppData;->d()Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    move-result-object v17

    .line 480944
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/directinstall/intent/DirectInstallAppData;->c()Ljava/lang/String;

    move-result-object v10

    .line 480945
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/directinstall/intent/DirectInstallAppData;->a()Ljava/lang/String;

    move-result-object v18

    .line 480946
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b()Landroid/os/Bundle;

    move-result-object v20

    .line 480947
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->a:LX/6Qo;

    .line 480948
    if-eqz v13, :cond_1

    if-eqz v17, :cond_1

    if-eqz v10, :cond_1

    if-eqz v18, :cond_1

    if-nez v1, :cond_2

    .line 480949
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 480950
    :cond_2
    invoke-static {v1}, LX/2yB;->a(LX/6Qo;)Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    move-result-object v19

    .line 480951
    new-instance v1, LX/47G;

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13}, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13}, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->d()LX/0Px;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->FB_ANDROID_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    invoke-virtual {v13}, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->g()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v13}, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->b()I

    move-result v12

    invoke-virtual {v13}, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->e()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->h()Ljava/lang/String;

    move-result-object v14

    sget-object v15, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->NOT_INSTALLED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->i()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->j()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v1 .. v19}, LX/47G;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;)V

    .line 480952
    move-object/from16 v0, v20

    iput-object v0, v1, LX/47G;->r:Landroid/os/Bundle;

    .line 480953
    move-object/from16 v0, p1

    iput-object v0, v1, LX/47G;->s:Ljava/util/Map;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)LX/47G;
    .locals 9
    .param p0    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 480910
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 480911
    :cond_0
    const/4 v0, 0x0

    .line 480912
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    const/4 v5, 0x0

    .line 480913
    if-nez v0, :cond_4

    .line 480914
    :cond_2
    :goto_1
    move-object v0, v5

    .line 480915
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->k()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v1

    const/4 v5, 0x0

    .line 480916
    if-nez v1, :cond_6

    .line 480917
    :cond_3
    :goto_2
    move-object v1, v5

    .line 480918
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, p2}, LX/2yB;->a(LX/1yF;Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;Ljava/lang/String;Ljava/lang/String;)LX/47G;

    move-result-object v0

    goto :goto_0

    .line 480919
    :cond_4
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 480920
    invoke-static {v3, v0}, LX/2yC;->a(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I

    move-result v4

    .line 480921
    if-eqz v4, :cond_2

    .line 480922
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 480923
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 480924
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 480925
    new-instance v3, LX/15i;

    const/4 v7, 0x1

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 480926
    instance-of v4, v0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v4, :cond_5

    .line 480927
    const-string v4, "DirectInstallConverter.getGetNativeAppDetailsActorGraphQL"

    invoke-virtual {v3, v4, v0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 480928
    :cond_5
    new-instance v5, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsActorGraphQLModel;

    invoke-direct {v5, v3}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsActorGraphQLModel;-><init>(LX/15i;)V

    goto :goto_1

    .line 480929
    :cond_6
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 480930
    invoke-static {v3, v1}, LX/2yC;->a(LX/186;Lcom/facebook/graphql/model/GraphQLAppStoreApplication;)I

    move-result v4

    .line 480931
    if-eqz v4, :cond_3

    .line 480932
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 480933
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 480934
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 480935
    new-instance v3, LX/15i;

    const/4 v7, 0x1

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 480936
    instance-of v4, v1, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v4, :cond_7

    .line 480937
    const-string v4, "DirectInstallConverter.getGetNativeAppDetailsAppStoreApplicationGraphQL"

    invoke-virtual {v3, v4, v1}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 480938
    :cond_7
    new-instance v5, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    invoke-direct {v5, v3}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;-><init>(LX/15i;)V

    goto :goto_2
.end method

.method private static a(Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;)Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;
    .locals 8
    .param p0    # Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 480899
    if-nez p0, :cond_0

    .line 480900
    :goto_0
    return-object v1

    .line 480901
    :cond_0
    new-instance v2, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    invoke-virtual {p0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;-><init>(Ljava/lang/String;)V

    .line 480902
    invoke-virtual {p0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel;

    .line 480903
    invoke-virtual {v0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel;->a()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel$EntityModel;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel;->a()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel$EntityModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel$EntityModel;->c()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 480904
    invoke-virtual {v0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel;->a()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    .line 480905
    invoke-virtual {v0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel$EntityModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel$EntityModel;->c()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel$EntityModel;->c()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 480906
    :goto_2
    iget-object v7, v2, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;->b:Ljava/util/ArrayList;

    new-instance p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities$Entity;

    invoke-direct {p0, v6, v0}, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities$Entity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 480907
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 480908
    goto :goto_2

    :cond_3
    move-object v1, v2

    .line 480909
    goto :goto_0
.end method

.method private static a(LX/6Qo;)Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;
    .locals 2

    .prologue
    .line 480895
    sget-object v0, LX/6R3;->a:[I

    invoke-virtual {p0}, LX/6Qo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 480896
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->ANY:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    :goto_0
    return-object v0

    .line 480897
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->WIFI_FORCE:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    goto :goto_0

    .line 480898
    :pswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->WIFI_ONLY:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
