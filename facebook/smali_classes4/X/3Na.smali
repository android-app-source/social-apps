.class public LX/3Na;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public final a:LX/3Nb;

.field public final b:Landroid/view/ViewGroup;

.field public final c:LX/3O9;

.field public final d:Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;

.field public e:LX/3OB;

.field public f:LX/2lE;

.field public g:LX/3LU;

.field public h:LX/3OW;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3LG;)V
    .locals 2

    .prologue
    .line 559448
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 559449
    new-instance v0, LX/3Nb;

    const v1, 0x7f030cf4

    invoke-direct {v0, p1, p2, v1}, LX/3Nb;-><init>(Landroid/content/Context;LX/3LG;I)V

    iput-object v0, p0, LX/3Na;->a:LX/3Nb;

    .line 559450
    iget-object v0, p0, LX/3Na;->a:LX/3Nb;

    const/4 v1, 0x1

    .line 559451
    iput-boolean v1, v0, LX/3Nb;->j:Z

    .line 559452
    iget-object v0, p0, LX/3Na;->a:LX/3Nb;

    const v1, 0x7f0802db

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3Nb;->setSearchHint(Ljava/lang/String;)V

    .line 559453
    iget-object v0, p0, LX/3Na;->a:LX/3Nb;

    invoke-virtual {p0, v0}, LX/3Na;->addView(Landroid/view/View;)V

    .line 559454
    const v0, 0x7f0d2056

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;

    iput-object v0, p0, LX/3Na;->d:Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;

    .line 559455
    iget-object v0, p0, LX/3Na;->d:Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->setVisibility(I)V

    .line 559456
    iget-object v0, p0, LX/3Na;->a:LX/3Nb;

    new-instance v1, LX/3O3;

    invoke-direct {v1, p0}, LX/3O3;-><init>(LX/3Na;)V

    .line 559457
    iget-object p1, v0, LX/3Nc;->g:LX/3Ne;

    .line 559458
    iput-object v1, p1, LX/3Ne;->c:LX/3O4;

    .line 559459
    iget-object v0, p0, LX/3Na;->a:LX/3Nb;

    new-instance v1, LX/3O5;

    invoke-direct {v1, p0}, LX/3O5;-><init>(LX/3Na;)V

    .line 559460
    iput-object v1, v0, LX/3Nc;->b:LX/3O5;

    .line 559461
    iget-object v0, p0, LX/3Na;->a:LX/3Nb;

    new-instance v1, LX/3O6;

    invoke-direct {v1, p0}, LX/3O6;-><init>(LX/3Na;)V

    .line 559462
    iput-object v1, v0, LX/3Nc;->c:LX/3O6;

    .line 559463
    iget-object v0, p0, LX/3Na;->a:LX/3Nb;

    new-instance v1, LX/3O7;

    invoke-direct {v1, p0}, LX/3O7;-><init>(LX/3Na;)V

    .line 559464
    iput-object v1, v0, LX/3Nc;->d:LX/3O7;

    .line 559465
    iget-object v0, p0, LX/3Na;->a:LX/3Nb;

    new-instance v1, LX/3O8;

    invoke-direct {v1, p0}, LX/3O8;-><init>(LX/3Na;)V

    .line 559466
    iput-object v1, v0, LX/3Nb;->l:LX/3O8;

    .line 559467
    const v0, 0x7f0d2055

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/3Na;->b:Landroid/view/ViewGroup;

    .line 559468
    new-instance v0, LX/3O9;

    iget-object v1, p0, LX/3Na;->b:Landroid/view/ViewGroup;

    invoke-direct {v0, v1}, LX/3O9;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, LX/3Na;->c:LX/3O9;

    .line 559469
    const-class v0, LX/3Na;

    invoke-static {v0, p0}, LX/3Na;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 559470
    iget-object v0, p0, LX/3Na;->a:LX/3Nb;

    new-instance v1, LX/3OE;

    invoke-direct {v1, p0}, LX/3OE;-><init>(LX/3Na;)V

    .line 559471
    iput-object v1, v0, LX/3Nb;->k:Landroid/view/View$OnFocusChangeListener;

    .line 559472
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/3Na;

    new-instance v4, LX/3OB;

    invoke-static {v2}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v1

    check-cast v1, LX/0iA;

    new-instance v0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;

    const-class v3, LX/13A;

    invoke-interface {v2, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/13A;

    invoke-static {v2}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(LX/0QB;)Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    move-result-object p0

    check-cast p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-direct {v0, v3, p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;-><init>(LX/13A;Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;)V

    move-object v3, v0

    check-cast v3, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;

    invoke-direct {v4, v1, v3}, LX/3OB;-><init>(LX/0iA;Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;)V

    move-object v1, v4

    check-cast v1, LX/3OB;

    invoke-static {v2}, LX/2lE;->a(LX/0QB;)LX/2lE;

    move-result-object v2

    check-cast v2, LX/2lE;

    iput-object v1, p1, LX/3Na;->e:LX/3OB;

    iput-object v2, p1, LX/3Na;->f:LX/2lE;

    return-void
.end method

.method public static setLastNavigationTapPoint(LX/3Na;LX/3OQ;)V
    .locals 3

    .prologue
    .line 559432
    check-cast p1, LX/3OO;

    .line 559433
    iget-object v0, p1, LX/3OO;->r:LX/3OI;

    move-object v0, v0

    .line 559434
    check-cast v0, LX/3OH;

    check-cast v0, LX/3OH;

    .line 559435
    const/4 v1, 0x0

    .line 559436
    sget-object v2, LX/JI0;->a:[I

    invoke-virtual {v0}, LX/3OH;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 559437
    :goto_0
    if-eqz v0, :cond_0

    .line 559438
    invoke-virtual {p0}, LX/3Na;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/14o;->a(Landroid/content/Context;)LX/0gh;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 559439
    :cond_0
    return-void

    .line 559440
    :pswitch_0
    const-string v0, "via_chat_bar_active_chats_section"

    goto :goto_0

    .line 559441
    :pswitch_1
    const-string v0, "via_chat_bar_auto_complete_section"

    goto :goto_0

    .line 559442
    :pswitch_2
    const-string v0, "via_chat_bar_favorite_friends_section"

    goto :goto_0

    .line 559443
    :pswitch_3
    const-string v0, "via_chat_bar_nearby_friends_section"

    goto :goto_0

    .line 559444
    :pswitch_4
    const-string v0, "via_chat_bar_search_result_section"

    goto :goto_0

    .line 559445
    :pswitch_5
    const-string v0, "via_chat_bar_suggestions_section"

    goto :goto_0

    .line 559446
    :pswitch_6
    const-string v0, "via_chat_bar_top_friends_section"

    goto :goto_0

    .line 559447
    :pswitch_7
    const-string v0, "via_chat_bar_unknown_section"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 559473
    iget-object v0, p0, LX/3Na;->a:LX/3Nb;

    invoke-virtual {v0}, LX/3Nc;->d()V

    .line 559474
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 559429
    iget-object v0, p0, LX/3Na;->a:LX/3Nb;

    invoke-virtual {v0, p1}, LX/3Nc;->a(LX/0Px;)V

    .line 559430
    iget-object v0, p0, LX/3Na;->d:Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;

    invoke-virtual {v0}, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->f()V

    .line 559431
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 559422
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 559423
    iget-object v0, p0, LX/3Na;->h:LX/3OW;

    if-eqz v0, :cond_0

    .line 559424
    iget-object v0, p0, LX/3Na;->h:LX/3OW;

    .line 559425
    iget-object v1, v0, LX/3OW;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    iget-object v1, v1, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    const/4 p0, 0x0

    .line 559426
    iput-object p0, v1, LX/3Na;->h:LX/3OW;

    .line 559427
    iget-object v1, v0, LX/3OW;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    iget-object v1, v1, Lcom/facebook/divebar/contacts/DivebarFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p0, 0x5f0001

    const/4 p1, 0x2

    invoke-interface {v1, p0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 559428
    :cond_0
    return-void
.end method

.method public getContactPickerHeaderViewManager()LX/3O9;
    .locals 1

    .prologue
    .line 559421
    iget-object v0, p0, LX/3Na;->c:LX/3O9;

    return-object v0
.end method

.method public getSearchBoxText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 559420
    iget-object v0, p0, LX/3Na;->a:LX/3Nb;

    invoke-virtual {v0}, LX/3Nb;->getSearchBoxText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
