.class public LX/3HT;
.super LX/0b4;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/3Gy;",
        "LX/AdZ;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 543521
    invoke-direct {p0}, LX/0b4;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/3HT;
    .locals 3

    .prologue
    .line 543522
    const-class v1, LX/3HT;

    monitor-enter v1

    .line 543523
    :try_start_0
    sget-object v0, LX/3HT;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 543524
    sput-object v2, LX/3HT;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 543525
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543526
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 543527
    new-instance v0, LX/3HT;

    invoke-direct {v0}, LX/3HT;-><init>()V

    .line 543528
    move-object v0, v0

    .line 543529
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 543530
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3HT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 543531
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 543532
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
