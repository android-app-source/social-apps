.class public LX/3BS;
.super LX/3BT;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/31V;",
            ">;"
        }
    .end annotation
.end field

.field private static o:LX/0Xm;


# instance fields
.field private final e:LX/121;

.field private final f:Ljava/lang/String;

.field public g:Z

.field public h:LX/0gc;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 528466
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 528467
    sput-object v0, LX/3BS;->d:Landroid/util/SparseArray;

    const v1, 0x7f0d1a9e

    new-instance v2, LX/31V;

    const-string v3, "lineLabel"

    const v4, 0x7f0818cd

    const v5, 0x7f0818d3

    invoke-direct {v2, v3, v4, v5}, LX/31V;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 528468
    sget-object v0, LX/3BS;->d:Landroid/util/SparseArray;

    const v1, 0x7f0d1a9f

    new-instance v2, LX/31V;

    const-string v3, "line"

    const v4, 0x7f0818ce

    const v5, 0x7f0818d4

    invoke-direct {v2, v3, v4, v5}, LX/31V;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 528469
    sget-object v0, LX/3BS;->d:Landroid/util/SparseArray;

    const v1, 0x7f0d1aa0

    new-instance v2, LX/31V;

    const-string v3, "lineMissing"

    const v4, 0x7f0818cf

    const v5, 0x7f0818d5

    invoke-direct {v2, v3, v4, v5}, LX/31V;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 528470
    sget-object v0, LX/3BS;->d:Landroid/util/SparseArray;

    const v1, 0x7f0d1aa1

    new-instance v2, LX/31V;

    const-string v3, "polygon"

    const v4, 0x7f0818d0

    const v5, 0x7f0818d6

    invoke-direct {v2, v3, v4, v5}, LX/31V;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 528471
    sget-object v0, LX/3BS;->d:Landroid/util/SparseArray;

    const v1, 0x7f0d1aa2

    new-instance v2, LX/31V;

    const-string v3, "border"

    const v4, 0x7f0818d1

    const v5, 0x7f0818d7

    invoke-direct {v2, v3, v4, v5}, LX/31V;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 528472
    sget-object v0, LX/3BS;->d:Landroid/util/SparseArray;

    const v1, 0x7f0d1aa3

    new-instance v2, LX/31V;

    const-string v3, "other"

    const v4, 0x7f0818d2

    const v5, 0x7f0818d8

    invoke-direct {v2, v3, v4, v5}, LX/31V;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 528473
    return-void
.end method

.method public constructor <init>(LX/121;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 528474
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v4, LX/31W;

    invoke-direct {v4, p2}, LX/31W;-><init>(Landroid/content/Context;)V

    new-instance v5, LX/31b;

    invoke-direct {v5, p3, p2}, LX/31b;-><init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;)V

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, LX/3BT;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;LX/31X;LX/31c;)V

    .line 528475
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 528476
    const v1, 0x7f080e47

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/3BS;->f:Ljava/lang/String;

    .line 528477
    iput-object p1, p0, LX/3BS;->e:LX/121;

    .line 528478
    const v1, 0x7f0818c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/3BS;->i:Ljava/lang/String;

    .line 528479
    const v1, 0x7f0818c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3BS;->j:Ljava/lang/String;

    .line 528480
    return-void
.end method

.method public static a(LX/0QB;)LX/3BS;
    .locals 6

    .prologue
    .line 528481
    const-class v1, LX/3BS;

    monitor-enter v1

    .line 528482
    :try_start_0
    sget-object v0, LX/3BS;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 528483
    sput-object v2, LX/3BS;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 528484
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528485
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 528486
    new-instance p0, LX/3BS;

    invoke-static {v0}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v3

    check-cast v3, LX/121;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4, v5}, LX/3BS;-><init>(LX/121;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 528487
    move-object v0, p0

    .line 528488
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 528489
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3BS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 528490
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 528491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static synthetic a(LX/3BS;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 528492
    invoke-super {p0, p1}, LX/3BT;->a(Landroid/net/Uri;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/net/Uri;I)V
    .locals 10
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForWindowedContext;
        .end annotation
    .end param

    .prologue
    .line 528493
    and-int/lit8 v0, p3, 0x2

    if-nez v0, :cond_0

    .line 528494
    iget-object v0, p0, LX/3BT;->a:LX/31X;

    iget-object v1, p0, LX/3BT;->c:Ljava/lang/CharSequence;

    invoke-interface {v0, v1}, LX/31X;->b(Ljava/lang/CharSequence;)LX/31X;

    move-result-object v0

    iget-object v1, p0, LX/3BT;->b:Ljava/lang/CharSequence;

    new-instance v2, LX/6b1;

    invoke-direct {v2, p0, p2}, LX/6b1;-><init>(LX/3BS;Landroid/net/Uri;)V

    invoke-interface {v0, v1, v2}, LX/31X;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/31X;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/6b0;

    invoke-direct {v2, p0}, LX/6b0;-><init>(LX/3BS;)V

    invoke-interface {v0, v1, v2}, LX/31X;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/31X;

    move-result-object v0

    invoke-interface {v0}, LX/31X;->a()Landroid/app/Dialog;

    move-result-object v0

    .line 528495
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 528496
    :goto_0
    return-void

    .line 528497
    :cond_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 528498
    const-string v0, "init"

    iput-object v0, p0, LX/3BS;->k:Ljava/lang/String;

    .line 528499
    sget-object v0, LX/3BS;->d:Landroid/util/SparseArray;

    const v2, 0x7f0d1aa3

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/31V;

    iget-object v0, v0, LX/31V;->a:Ljava/lang/String;

    iput-object v0, p0, LX/3BS;->l:Ljava/lang/String;

    .line 528500
    const-string v0, ""

    iput-object v0, p0, LX/3BS;->m:Ljava/lang/String;

    .line 528501
    iput-object p2, p0, LX/3BS;->n:Landroid/net/Uri;

    .line 528502
    const v0, 0x7f030a78

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ViewFlipper;

    .line 528503
    new-instance v7, LX/6b2;

    const v0, 0x7f0e070e

    invoke-direct {v7, p0, p1, v0}, LX/6b2;-><init>(LX/3BS;Landroid/content/Context;I)V

    .line 528504
    invoke-virtual {v7}, LX/3Ag;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    .line 528505
    const v0, 0x7f0d1a9d

    invoke-virtual {v4, v0}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/LinearLayout;

    .line 528506
    const v0, 0x7f0d1aa4

    invoke-virtual {v4, v0}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 528507
    const v0, 0x7f0d1aa5

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fig/listitem/FigListItem;

    .line 528508
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a008a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionIconColor(I)V

    .line 528509
    const v0, 0x7f0d1aa6

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/fig/textinput/FigEditText;

    .line 528510
    new-instance v0, LX/6b3;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/6b3;-><init>(LX/3BS;Lcom/facebook/fig/listitem/FigListItem;Lcom/facebook/fig/textinput/FigEditText;Landroid/widget/ViewFlipper;Landroid/widget/LinearLayout;)V

    .line 528511
    const/4 v1, 0x0

    move v5, v1

    :goto_1
    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v5, v1, :cond_2

    .line 528512
    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 528513
    sget-object v1, LX/3BS;->d:Landroid/util/SparseArray;

    invoke-virtual {v8}, Landroid/view/View;->getId()I

    move-result v9

    invoke-virtual {v1, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/31V;

    .line 528514
    if-eqz v1, :cond_1

    .line 528515
    invoke-virtual {v8, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 528516
    :cond_1
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 528517
    :cond_2
    new-instance v0, LX/6b5;

    invoke-direct {v0, p0, v3, v7, p1}, LX/6b5;-><init>(LX/3BS;Lcom/facebook/fig/textinput/FigEditText;LX/3Ag;Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 528518
    invoke-virtual {v4, v6}, Landroid/widget/ViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 528519
    invoke-virtual {v7, v4}, LX/3Ag;->setContentView(Landroid/view/View;)V

    .line 528520
    invoke-virtual {v7}, LX/3Ag;->show()V

    goto/16 :goto_0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 528521
    iget-boolean v0, p0, LX/3BS;->g:Z

    if-nez v0, :cond_0

    .line 528522
    invoke-super {p0, p1}, LX/3BT;->a(Landroid/net/Uri;)V

    .line 528523
    :goto_0
    return-void

    .line 528524
    :cond_0
    iget-object v0, p0, LX/3BS;->e:LX/121;

    sget-object v1, LX/0yY;->EXTERNAL_URLS_INTERSTITIAL:LX/0yY;

    iget-object v2, p0, LX/3BS;->f:Ljava/lang/String;

    new-instance v3, LX/6b6;

    invoke-direct {v3, p0, p1}, LX/6b6;-><init>(LX/3BS;Landroid/net/Uri;)V

    invoke-virtual {v0, v1, v2, v3}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 528525
    iget-object v0, p0, LX/3BS;->e:LX/121;

    sget-object v1, LX/0yY;->EXTERNAL_URLS_INTERSTITIAL:LX/0yY;

    iget-object v2, p0, LX/3BS;->h:LX/0gc;

    invoke-virtual {v0, v1, v2}, LX/121;->a(LX/0yY;LX/0gc;)V

    goto :goto_0
.end method
