.class public final LX/3KN;
.super LX/2xM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2xM",
        "<",
        "Lcom/google/android/gms/location/LocationSettingsResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lcom/google/android/gms/location/LocationSettingsRequest;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:LX/2vx;


# direct methods
.method public constructor <init>(LX/2vx;LX/2wX;Lcom/google/android/gms/location/LocationSettingsRequest;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, LX/3KN;->f:LX/2vx;

    iput-object p3, p0, LX/3KN;->d:Lcom/google/android/gms/location/LocationSettingsRequest;

    iput-object p4, p0, LX/3KN;->e:Ljava/lang/String;

    invoke-direct {p0, p2}, LX/2xM;-><init>(LX/2wX;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)LX/2NW;
    .locals 1

    new-instance v0, Lcom/google/android/gms/location/LocationSettingsResult;

    invoke-direct {v0, p1}, Lcom/google/android/gms/location/LocationSettingsResult;-><init>(Lcom/google/android/gms/common/api/Status;)V

    return-object v0
.end method

.method public final b(LX/2wK;)V
    .locals 6

    check-cast p1, LX/2wF;

    iget-object v0, p0, LX/3KN;->d:Lcom/google/android/gms/location/LocationSettingsRequest;

    iget-object v1, p0, LX/3KN;->e:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, LX/2wI;->k()V

    if-eqz v0, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "locationSettingsRequest can\'t be null nor empty."

    invoke-static {v2, v5}, LX/1ol;->b(ZLjava/lang/Object;)V

    if-eqz p0, :cond_1

    :goto_1
    const-string v2, "listener can\'t be null."

    invoke-static {v3, v2}, LX/1ol;->b(ZLjava/lang/Object;)V

    new-instance v3, LX/7aF;

    invoke-direct {v3, p0}, LX/7aF;-><init>(LX/2wh;)V

    invoke-virtual {p1}, LX/2wI;->m()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, LX/2xF;

    invoke-interface {v2, v0, v3, v1}, LX/2xF;->a(Lcom/google/android/gms/location/LocationSettingsRequest;LX/7a8;Ljava/lang/String;)V

    return-void

    :cond_0
    move v2, v4

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1
.end method
