.class public abstract LX/2y5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 480624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/1Nt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/35p;",
            ">()",
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;"
        }
    .end annotation
.end method

.method public abstract a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)",
            "LX/AE0;"
        }
    .end annotation
.end method

.method public final b(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)",
            "LX/1X1",
            "<",
            "LX/ApI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 480625
    invoke-virtual {p0, p1, p2, p3, p4}, LX/2y5;->a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;

    move-result-object v0

    .line 480626
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 480627
    :cond_0
    iget-object p0, v0, LX/AE0;->a:LX/ApI;

    const/4 p2, 0x0

    .line 480628
    new-instance p3, LX/ApH;

    invoke-direct {p3, p0}, LX/ApH;-><init>(LX/ApI;)V

    .line 480629
    sget-object p4, LX/ApI;->a:LX/0Zi;

    invoke-virtual {p4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, LX/ApG;

    .line 480630
    if-nez p4, :cond_1

    .line 480631
    new-instance p4, LX/ApG;

    invoke-direct {p4}, LX/ApG;-><init>()V

    .line 480632
    :cond_1
    invoke-static {p4, p1, p2, p2, p3}, LX/ApG;->a$redex0(LX/ApG;LX/1De;IILX/ApH;)V

    .line 480633
    move-object p3, p4

    .line 480634
    move-object p2, p3

    .line 480635
    move-object p0, p2

    .line 480636
    iget p2, v0, LX/AE0;->d:I

    .line 480637
    iget-object p3, p0, LX/ApG;->a:LX/ApH;

    iput p2, p3, LX/ApH;->a:I

    .line 480638
    iget-object p3, p0, LX/ApG;->d:Ljava/util/BitSet;

    const/4 p4, 0x0

    invoke-virtual {p3, p4}, Ljava/util/BitSet;->set(I)V

    .line 480639
    move-object p0, p0

    .line 480640
    iget p2, v0, LX/AE0;->d:I

    const/4 p3, 0x1

    if-ne p2, p3, :cond_2

    .line 480641
    invoke-static {v0, p1}, LX/AE0;->c(LX/AE0;LX/1De;)LX/1X1;

    move-result-object p2

    .line 480642
    iget-object p3, p0, LX/ApG;->a:LX/ApH;

    iput-object p2, p3, LX/ApH;->j:LX/1X1;

    .line 480643
    :goto_1
    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    move-object v0, p0

    .line 480644
    goto :goto_0

    .line 480645
    :cond_2
    iget-object p2, v0, LX/AE0;->f:Ljava/lang/CharSequence;

    .line 480646
    iget-object p3, p0, LX/ApG;->a:LX/ApH;

    iput-object p2, p3, LX/ApH;->b:Ljava/lang/CharSequence;

    .line 480647
    move-object p2, p0

    .line 480648
    iget-object p3, v0, LX/AE0;->h:Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    .line 480649
    iget-object p4, p2, LX/ApG;->a:LX/ApH;

    iput p3, p4, LX/ApH;->c:I

    .line 480650
    move-object p2, p2

    .line 480651
    iget-object p3, v0, LX/AE0;->g:Ljava/lang/CharSequence;

    .line 480652
    iget-object p4, p2, LX/ApG;->a:LX/ApH;

    iput-object p3, p4, LX/ApH;->d:Ljava/lang/CharSequence;

    .line 480653
    move-object p2, p2

    .line 480654
    iget-object p3, v0, LX/AE0;->i:Landroid/util/SparseArray;

    .line 480655
    iget-object p4, p2, LX/ApG;->a:LX/ApH;

    iput-object p3, p4, LX/ApH;->e:Landroid/util/SparseArray;

    .line 480656
    move-object p2, p2

    .line 480657
    iget-object p3, v0, LX/AE0;->o:Landroid/view/View$OnClickListener;

    .line 480658
    iget-object p4, p2, LX/ApG;->a:LX/ApH;

    iput-object p3, p4, LX/ApH;->g:Landroid/view/View$OnClickListener;

    .line 480659
    goto :goto_1
.end method

.method public final c(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)",
            "LX/1X1",
            "<",
            "LX/Aph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 480660
    invoke-virtual {p0, p1, p2, p3, p4}, LX/2y5;->a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;

    move-result-object v0

    .line 480661
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 480662
    :cond_0
    iget-object p0, v0, LX/AE0;->b:LX/Aph;

    const/4 p2, 0x0

    .line 480663
    new-instance p3, LX/Apg;

    invoke-direct {p3, p0}, LX/Apg;-><init>(LX/Aph;)V

    .line 480664
    sget-object p4, LX/Aph;->a:LX/0Zi;

    invoke-virtual {p4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, LX/Apf;

    .line 480665
    if-nez p4, :cond_1

    .line 480666
    new-instance p4, LX/Apf;

    invoke-direct {p4}, LX/Apf;-><init>()V

    .line 480667
    :cond_1
    invoke-static {p4, p1, p2, p2, p3}, LX/Apf;->a$redex0(LX/Apf;LX/1De;IILX/Apg;)V

    .line 480668
    move-object p3, p4

    .line 480669
    move-object p2, p3

    .line 480670
    move-object p0, p2

    .line 480671
    iget p2, v0, LX/AE0;->d:I

    .line 480672
    iget-object p3, p0, LX/Apf;->a:LX/Apg;

    iput p2, p3, LX/Apg;->a:I

    .line 480673
    iget-object p3, p0, LX/Apf;->d:Ljava/util/BitSet;

    const/4 p4, 0x0

    invoke-virtual {p3, p4}, Ljava/util/BitSet;->set(I)V

    .line 480674
    move-object p0, p0

    .line 480675
    iget p2, v0, LX/AE0;->d:I

    const/4 p3, 0x1

    if-ne p2, p3, :cond_2

    .line 480676
    invoke-static {v0, p1}, LX/AE0;->c(LX/AE0;LX/1De;)LX/1X1;

    move-result-object p2

    .line 480677
    iget-object p3, p0, LX/Apf;->a:LX/Apg;

    iput-object p2, p3, LX/Apg;->j:LX/1X1;

    .line 480678
    :goto_1
    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    move-object v0, p0

    .line 480679
    goto :goto_0

    .line 480680
    :cond_2
    iget-object p2, v0, LX/AE0;->f:Ljava/lang/CharSequence;

    .line 480681
    iget-object p3, p0, LX/Apf;->a:LX/Apg;

    iput-object p2, p3, LX/Apg;->b:Ljava/lang/CharSequence;

    .line 480682
    move-object p2, p0

    .line 480683
    iget-object p3, v0, LX/AE0;->h:Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    .line 480684
    iget-object p4, p2, LX/Apf;->a:LX/Apg;

    iput p3, p4, LX/Apg;->c:I

    .line 480685
    move-object p2, p2

    .line 480686
    iget-object p3, v0, LX/AE0;->g:Ljava/lang/CharSequence;

    .line 480687
    iget-object p4, p2, LX/Apf;->a:LX/Apg;

    iput-object p3, p4, LX/Apg;->d:Ljava/lang/CharSequence;

    .line 480688
    move-object p2, p2

    .line 480689
    iget-object p3, v0, LX/AE0;->i:Landroid/util/SparseArray;

    .line 480690
    iget-object p4, p2, LX/Apf;->a:LX/Apg;

    iput-object p3, p4, LX/Apg;->e:Landroid/util/SparseArray;

    .line 480691
    move-object p2, p2

    .line 480692
    iget-object p3, v0, LX/AE0;->o:Landroid/view/View$OnClickListener;

    .line 480693
    iget-object p4, p2, LX/Apf;->a:LX/Apg;

    iput-object p3, p4, LX/Apg;->g:Landroid/view/View$OnClickListener;

    .line 480694
    goto :goto_1
.end method
