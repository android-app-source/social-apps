.class public LX/3BI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 527762
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 527763
    return-void
.end method

.method public static a(LX/0QB;)LX/3BI;
    .locals 1

    .prologue
    .line 527764
    new-instance v0, LX/3BI;

    invoke-direct {v0}, LX/3BI;-><init>()V

    .line 527765
    move-object v0, v0

    .line 527766
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;
    .locals 11

    .prologue
    .line 527767
    invoke-static {p0}, LX/3BI;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    .line 527768
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->u()LX/0Px;

    move-result-object v0

    .line 527769
    :goto_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 527770
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 527771
    new-instance v5, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v9

    invoke-direct {v5, v7, v8, v9, v10}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 527772
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 527773
    :cond_0
    move-object v0, v3

    .line 527774
    invoke-static {v0, p1}, LX/3BI;->a(Ljava/util/List;Ljava/lang/String;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    return-object v0

    .line 527775
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 527776
    goto :goto_0
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/android/maps/model/LatLng;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;"
        }
    .end annotation

    .prologue
    .line 527777
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-direct {v0, p1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    .line 527778
    const/16 v1, 0xff

    const/16 v2, 0xf5

    const/16 v3, 0x15

    const/16 v4, 0x6f

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    const/4 v3, 0x3

    const-string v4, "bezier"

    const-string v5, "3,5"

    move-object v1, p0

    const/4 v6, 0x0

    .line 527779
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 527780
    const/4 v6, 0x0

    iput-object v6, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->f:Ljava/lang/String;

    .line 527781
    :goto_0
    const/high16 v7, 0x3f000000    # 0.5f

    .line 527782
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 527783
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v1, v3

    .line 527784
    :goto_1
    move-object v1, v1

    .line 527785
    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Ljava/util/List;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 527786
    return-object v0

    .line 527787
    :cond_0
    shl-int/lit8 v7, v2, 0x8

    ushr-int/lit8 v8, v2, 0x18

    or-int/2addr v7, v8

    .line 527788
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "color:0x"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "%08X"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v11, v6

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v9}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "|weight:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 527789
    if-eqz v4, :cond_1

    .line 527790
    const-string v7, "|route:"

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 527791
    :cond_1
    if-eqz v5, :cond_2

    .line 527792
    const-string v7, "|dashed:"

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 527793
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    move v7, v6

    :goto_2
    if-ge v7, v9, :cond_3

    .line 527794
    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/android/maps/model/LatLng;

    .line 527795
    const-string v10, "|"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v6, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v6, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 527796
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_2

    .line 527797
    :cond_3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 527798
    :cond_4
    const/4 v1, 0x0

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/android/maps/model/LatLng;

    .line 527799
    const/4 v2, 0x1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/android/maps/model/LatLng;

    .line 527800
    new-instance v4, LX/68O;

    const-string v5, "images/places/map/map_pin.png"

    invoke-direct {v4, v1, v5, v7, v7}, LX/68O;-><init>(Lcom/facebook/android/maps/model/LatLng;Ljava/lang/String;FF)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 527801
    new-instance v4, LX/68O;

    const-string v5, "images/places/map/bright-pink-pin.png"

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v4, v1, v5, v7, v6}, LX/68O;-><init>(Lcom/facebook/android/maps/model/LatLng;Ljava/lang/String;FF)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 527802
    new-instance v1, LX/68O;

    const-string v4, "images/places/map/pink-place-dot.png"

    invoke-direct {v1, v2, v4, v7, v7}, LX/68O;-><init>(Lcom/facebook/android/maps/model/LatLng;Ljava/lang/String;FF)V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v3

    .line 527803
    goto/16 :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3

    .prologue
    .line 527804
    new-instance v0, LX/4Y6;

    invoke-direct {v0}, LX/4Y6;-><init>()V

    .line 527805
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    .line 527806
    iput-object v1, v0, LX/4Y6;->n:Ljava/lang/String;

    .line 527807
    move-object v1, v0

    .line 527808
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 527809
    iput-object v2, v1, LX/4Y6;->U:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 527810
    move-object v1, v1

    .line 527811
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v2

    .line 527812
    iput-object v2, v1, LX/4Y6;->r:Ljava/lang/String;

    .line 527813
    move-object v1, v1

    .line 527814
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eC()Z

    move-result v2

    .line 527815
    iput-boolean v2, v1, LX/4Y6;->o:Z

    .line 527816
    move-object v1, v1

    .line 527817
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jp()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v2

    .line 527818
    iput-object v2, v1, LX/4Y6;->O:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 527819
    move-object v1, v1

    .line 527820
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fj()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    .line 527821
    iput-object v2, v1, LX/4Y6;->p:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 527822
    move-object v1, v1

    .line 527823
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v2

    .line 527824
    iput-object v2, v1, LX/4Y6;->P:Ljava/lang/String;

    .line 527825
    move-object v1, v1

    .line 527826
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ho()Z

    move-result v2

    .line 527827
    iput-boolean v2, v1, LX/4Y6;->G:Z

    .line 527828
    move-object v1, v1

    .line 527829
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iA()Z

    move-result v2

    .line 527830
    iput-boolean v2, v1, LX/4Y6;->M:Z

    .line 527831
    move-object v1, v1

    .line 527832
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fX()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v2

    .line 527833
    iput-object v2, v1, LX/4Y6;->s:Lcom/facebook/graphql/model/GraphQLRating;

    .line 527834
    move-object v1, v1

    .line 527835
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bn()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    .line 527836
    iput-object v2, v1, LX/4Y6;->h:Lcom/facebook/graphql/model/GraphQLPage;

    .line 527837
    move-object v1, v1

    .line 527838
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v2

    .line 527839
    iput-object v2, v1, LX/4Y6;->K:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 527840
    move-object v1, v1

    .line 527841
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    .line 527842
    iput-object v2, v1, LX/4Y6;->R:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 527843
    move-object v1, v1

    .line 527844
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 527845
    iput-object v2, v1, LX/4Y6;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 527846
    move-object v1, v1

    .line 527847
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bj()LX/0Px;

    move-result-object v2

    .line 527848
    iput-object v2, v1, LX/4Y6;->f:LX/0Px;

    .line 527849
    move-object v1, v1

    .line 527850
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->x()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v2

    .line 527851
    iput-object v2, v1, LX/4Y6;->b:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 527852
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ga()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 527853
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ga()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->O()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v1

    .line 527854
    iput-object v1, v0, LX/4Y6;->q:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 527855
    move-object v1, v0

    .line 527856
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ga()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->W()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v2

    .line 527857
    iput-object v2, v1, LX/4Y6;->u:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    .line 527858
    move-object v1, v1

    .line 527859
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ga()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->aQ()Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    move-result-object v2

    .line 527860
    iput-object v2, v1, LX/4Y6;->S:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    .line 527861
    :cond_0
    invoke-virtual {v0}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 527862
    invoke-static {p0}, LX/3BI;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    .line 527863
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->u()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->u()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 527864
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 527865
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 527866
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 527867
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
