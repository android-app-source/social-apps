.class public final LX/248;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;",
        ">;",
        "LX/DAt;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/244;


# direct methods
.method public constructor <init>(LX/244;)V
    .locals 0

    .prologue
    .line 366606
    iput-object p1, p0, LX/248;->a:LX/244;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 366607
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 366608
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 366609
    check-cast v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;

    .line 366610
    const/4 v1, 0x0

    .line 366611
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->k()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->a()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->l()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 366612
    invoke-virtual {v0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->j()Ljava/lang/String;

    move-result-object v8

    .line 366613
    new-instance v9, LX/DAr;

    invoke-virtual {v2}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v1, v3, v2}, LX/DAr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 366614
    new-instance v1, LX/DAs;

    invoke-virtual {v7}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->m()I

    move-result v5

    invoke-virtual {v7}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->n()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->j()Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_1

    const-string v7, ""

    :goto_0
    invoke-direct/range {v1 .. v7}, LX/DAs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 366615
    new-instance v2, LX/DAt;

    invoke-direct {v2, v9, v8, v1}, LX/DAt;-><init>(LX/DAr;Ljava/lang/String;LX/DAs;)V

    move-object v1, v2

    .line 366616
    :cond_0
    move-object v0, v1

    .line 366617
    return-object v0

    .line 366618
    :cond_1
    invoke-virtual {v7}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->j()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method
