.class public abstract LX/30v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1J7;


# instance fields
.field private a:LX/0g8;

.field public b:Z

.field private c:LX/2i4;

.field private d:LX/2i4;

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(LX/0g8;Z)V
    .locals 2

    .prologue
    .line 486476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486477
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/30v;->b:Z

    .line 486478
    new-instance v0, LX/2i4;

    invoke-direct {v0}, LX/2i4;-><init>()V

    iput-object v0, p0, LX/30v;->c:LX/2i4;

    .line 486479
    new-instance v0, LX/2i4;

    invoke-direct {v0}, LX/2i4;-><init>()V

    iput-object v0, p0, LX/30v;->d:LX/2i4;

    .line 486480
    iput-object p1, p0, LX/30v;->a:LX/0g8;

    .line 486481
    iput-boolean p2, p0, LX/30v;->b:Z

    .line 486482
    iget-object v0, p0, LX/30v;->a:LX/0g8;

    iget-object v1, p0, LX/30v;->c:LX/2i4;

    invoke-interface {v0, v1}, LX/0g8;->a(LX/2i4;)V

    .line 486483
    iget-object v0, p0, LX/30v;->a:LX/0g8;

    iget-object v1, p0, LX/30v;->d:LX/2i4;

    invoke-interface {v0, v1}, LX/0g8;->a(LX/2i4;)V

    .line 486484
    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 486474
    iput p2, p0, LX/30v;->e:I

    .line 486475
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 486414
    iget-object v1, p0, LX/30v;->a:LX/0g8;

    iget-object v2, p0, LX/30v;->c:LX/2i4;

    invoke-interface {v1, v2}, LX/0g8;->a(LX/2i4;)V

    .line 486415
    iget v1, p0, LX/30v;->e:I

    if-nez v1, :cond_0

    .line 486416
    :goto_0
    return-void

    .line 486417
    :cond_0
    iget-object v1, p0, LX/30v;->c:LX/2i4;

    .line 486418
    iget v2, v1, LX/2i4;->a:I

    move v1, v2

    .line 486419
    iget-object v2, p0, LX/30v;->d:LX/2i4;

    .line 486420
    iget v3, v2, LX/2i4;->a:I

    move v2, v3

    .line 486421
    sub-int v2, v1, v2

    .line 486422
    iget-object v1, p0, LX/30v;->c:LX/2i4;

    .line 486423
    iget v3, v1, LX/2i4;->c:I

    move v3, v3

    .line 486424
    iget-object v1, p0, LX/30v;->c:LX/2i4;

    .line 486425
    iget v4, v1, LX/2i4;->a:I

    move v1, v4

    .line 486426
    if-nez v1, :cond_3

    if-gez v3, :cond_3

    move v3, v0

    .line 486427
    :cond_1
    :goto_1
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/4 v4, 0x2

    if-ge v1, v4, :cond_2

    .line 486428
    iget-object v1, p0, LX/30v;->d:LX/2i4;

    .line 486429
    iget v4, v1, LX/2i4;->c:I

    move v1, v4

    .line 486430
    sub-int/2addr v1, v3

    .line 486431
    if-ne v2, v5, :cond_4

    .line 486432
    iget-object v2, p0, LX/30v;->d:LX/2i4;

    .line 486433
    iget v4, v2, LX/2i4;->b:I

    move v2, v4

    .line 486434
    sub-int/2addr v1, v2

    move v2, v1

    .line 486435
    :goto_2
    if-gez v2, :cond_5

    .line 486436
    iget v1, p0, LX/30v;->f:I

    add-int/2addr v1, v2

    iput v1, p0, LX/30v;->f:I

    .line 486437
    iget v1, p0, LX/30v;->f:I

    .line 486438
    iput v0, p0, LX/30v;->g:I

    .line 486439
    :goto_3
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/16 v4, 0x64

    if-le v1, v4, :cond_2

    .line 486440
    if-gez v2, :cond_6

    iget-boolean v1, p0, LX/30v;->b:Z

    if-eqz v1, :cond_6

    .line 486441
    iput-boolean v0, p0, LX/30v;->b:Z

    .line 486442
    invoke-virtual {p0}, LX/30v;->aa_()V

    .line 486443
    :cond_2
    :goto_4
    iget-object v0, p0, LX/30v;->d:LX/2i4;

    iget-object v1, p0, LX/30v;->c:LX/2i4;

    .line 486444
    iget v2, v1, LX/2i4;->a:I

    move v1, v2

    .line 486445
    iget-object v2, p0, LX/30v;->c:LX/2i4;

    .line 486446
    iget v4, v2, LX/2i4;->b:I

    move v2, v4

    .line 486447
    iget-object v4, p0, LX/30v;->c:LX/2i4;

    .line 486448
    iget v5, v4, LX/2i4;->d:I

    move v4, v5

    .line 486449
    iget-object v5, p0, LX/30v;->c:LX/2i4;

    .line 486450
    iget p0, v5, LX/2i4;->e:I

    move v5, p0

    .line 486451
    invoke-virtual/range {v0 .. v5}, LX/2i4;->a(IIIII)V

    goto :goto_0

    .line 486452
    :cond_3
    iget-object v1, p0, LX/30v;->c:LX/2i4;

    .line 486453
    iget v4, v1, LX/2i4;->d:I

    move v1, v4

    .line 486454
    iget-object v4, p0, LX/30v;->a:LX/0g8;

    invoke-interface {v4}, LX/0g8;->s()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v1, v4, :cond_1

    iget-object v1, p0, LX/30v;->c:LX/2i4;

    .line 486455
    iget v4, v1, LX/2i4;->e:I

    move v1, v4

    .line 486456
    iget-object v4, p0, LX/30v;->a:LX/0g8;

    invoke-interface {v4}, LX/0g8;->d()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 486457
    iget-object v1, p0, LX/30v;->c:LX/2i4;

    .line 486458
    iget v3, v1, LX/2i4;->e:I

    move v1, v3

    .line 486459
    iget-object v3, p0, LX/30v;->a:LX/0g8;

    invoke-interface {v3}, LX/0g8;->d()I

    move-result v3

    sub-int/2addr v1, v3

    .line 486460
    iget-object v3, p0, LX/30v;->c:LX/2i4;

    .line 486461
    iget v4, v3, LX/2i4;->c:I

    move v3, v4

    .line 486462
    add-int/2addr v3, v1

    goto :goto_1

    .line 486463
    :cond_4
    const/4 v4, -0x1

    if-ne v2, v4, :cond_8

    .line 486464
    iget-object v2, p0, LX/30v;->c:LX/2i4;

    .line 486465
    iget v4, v2, LX/2i4;->b:I

    move v2, v4

    .line 486466
    add-int/2addr v1, v2

    move v2, v1

    goto :goto_2

    .line 486467
    :cond_5
    if-lez v2, :cond_7

    .line 486468
    iput v0, p0, LX/30v;->f:I

    .line 486469
    iget v1, p0, LX/30v;->g:I

    add-int/2addr v1, v2

    iput v1, p0, LX/30v;->g:I

    .line 486470
    iget v1, p0, LX/30v;->g:I

    goto :goto_3

    .line 486471
    :cond_6
    if-lez v2, :cond_2

    iget-boolean v0, p0, LX/30v;->b:Z

    if-nez v0, :cond_2

    .line 486472
    iput-boolean v5, p0, LX/30v;->b:Z

    .line 486473
    invoke-virtual {p0}, LX/30v;->b()V

    goto :goto_4

    :cond_7
    move v1, v0

    goto :goto_3

    :cond_8
    move v2, v1

    goto/16 :goto_2
.end method

.method public abstract aa_()V
.end method

.method public abstract b()V
.end method
