.class public LX/37d;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/37d;


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 501709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501710
    sget-char v0, LX/0ws;->X:C

    const-string v1, "D2CA5178"

    invoke-interface {p1, v0, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/37d;->a:Ljava/lang/String;

    .line 501711
    return-void
.end method

.method public static a(LX/0QB;)LX/37d;
    .locals 4

    .prologue
    .line 501712
    sget-object v0, LX/37d;->b:LX/37d;

    if-nez v0, :cond_1

    .line 501713
    const-class v1, LX/37d;

    monitor-enter v1

    .line 501714
    :try_start_0
    sget-object v0, LX/37d;->b:LX/37d;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 501715
    if-eqz v2, :cond_0

    .line 501716
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 501717
    new-instance p0, LX/37d;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/37d;-><init>(LX/0ad;)V

    .line 501718
    move-object v0, p0

    .line 501719
    sput-object v0, LX/37d;->b:LX/37d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 501720
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 501721
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 501722
    :cond_1
    sget-object v0, LX/37d;->b:LX/37d;

    return-object v0

    .line 501723
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 501724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
