.class public final enum LX/2hC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2hC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2hC;

.field public static final enum ENTITY_CARDS:LX/2hC;

.field public static final enum FEED:LX/2hC;

.field public static final enum FRIENDS_CENTER:LX/2hC;

.field public static final enum FRIEND_FINDER:LX/2hC;

.field public static final enum JEWEL:LX/2hC;

.field public static final enum NUX:LX/2hC;

.field public static final enum PROFILE_FRIEND_LIST:LX/2hC;

.field public static final enum PYMK_TIMELINE_CHAIN:LX/2hC;

.field public static final enum QUICK_PROMOTION:LX/2hC;

.field public static final enum SELF_PROFILE:LX/2hC;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 449881
    new-instance v0, LX/2hC;

    const-string v1, "ENTITY_CARDS"

    const-string v2, "entity_cards"

    invoke-direct {v0, v1, v4, v2}, LX/2hC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hC;->ENTITY_CARDS:LX/2hC;

    .line 449882
    new-instance v0, LX/2hC;

    const-string v1, "FEED"

    const-string v2, "mobile_in_feed"

    invoke-direct {v0, v1, v5, v2}, LX/2hC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hC;->FEED:LX/2hC;

    .line 449883
    new-instance v0, LX/2hC;

    const-string v1, "FRIENDS_CENTER"

    const-string v2, "friends_center"

    invoke-direct {v0, v1, v6, v2}, LX/2hC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hC;->FRIENDS_CENTER:LX/2hC;

    .line 449884
    new-instance v0, LX/2hC;

    const-string v1, "JEWEL"

    const-string v2, "mobile_jewel"

    invoke-direct {v0, v1, v7, v2}, LX/2hC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hC;->JEWEL:LX/2hC;

    .line 449885
    new-instance v0, LX/2hC;

    const-string v1, "NUX"

    const-string v2, "nux"

    invoke-direct {v0, v1, v8, v2}, LX/2hC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hC;->NUX:LX/2hC;

    .line 449886
    new-instance v0, LX/2hC;

    const-string v1, "PROFILE_FRIEND_LIST"

    const/4 v2, 0x5

    const-string v3, "profile_friend_list"

    invoke-direct {v0, v1, v2, v3}, LX/2hC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hC;->PROFILE_FRIEND_LIST:LX/2hC;

    .line 449887
    new-instance v0, LX/2hC;

    const-string v1, "PYMK_TIMELINE_CHAIN"

    const/4 v2, 0x6

    const-string v3, "pymk_timeline_chain"

    invoke-direct {v0, v1, v2, v3}, LX/2hC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hC;->PYMK_TIMELINE_CHAIN:LX/2hC;

    .line 449888
    new-instance v0, LX/2hC;

    const-string v1, "QUICK_PROMOTION"

    const/4 v2, 0x7

    const-string v3, "quick_promotion"

    invoke-direct {v0, v1, v2, v3}, LX/2hC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hC;->QUICK_PROMOTION:LX/2hC;

    .line 449889
    new-instance v0, LX/2hC;

    const-string v1, "SELF_PROFILE"

    const/16 v2, 0x8

    const-string v3, "self_profile"

    invoke-direct {v0, v1, v2, v3}, LX/2hC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hC;->SELF_PROFILE:LX/2hC;

    .line 449890
    new-instance v0, LX/2hC;

    const-string v1, "FRIEND_FINDER"

    const/16 v2, 0x9

    const-string v3, "friend_finder"

    invoke-direct {v0, v1, v2, v3}, LX/2hC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hC;->FRIEND_FINDER:LX/2hC;

    .line 449891
    const/16 v0, 0xa

    new-array v0, v0, [LX/2hC;

    sget-object v1, LX/2hC;->ENTITY_CARDS:LX/2hC;

    aput-object v1, v0, v4

    sget-object v1, LX/2hC;->FEED:LX/2hC;

    aput-object v1, v0, v5

    sget-object v1, LX/2hC;->FRIENDS_CENTER:LX/2hC;

    aput-object v1, v0, v6

    sget-object v1, LX/2hC;->JEWEL:LX/2hC;

    aput-object v1, v0, v7

    sget-object v1, LX/2hC;->NUX:LX/2hC;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/2hC;->PROFILE_FRIEND_LIST:LX/2hC;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2hC;->PYMK_TIMELINE_CHAIN:LX/2hC;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2hC;->QUICK_PROMOTION:LX/2hC;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2hC;->SELF_PROFILE:LX/2hC;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2hC;->FRIEND_FINDER:LX/2hC;

    aput-object v2, v0, v1

    sput-object v0, LX/2hC;->$VALUES:[LX/2hC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 449894
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 449895
    iput-object p3, p0, LX/2hC;->value:Ljava/lang/String;

    .line 449896
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2hC;
    .locals 1

    .prologue
    .line 449893
    const-class v0, LX/2hC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2hC;

    return-object v0
.end method

.method public static values()[LX/2hC;
    .locals 1

    .prologue
    .line 449892
    sget-object v0, LX/2hC;->$VALUES:[LX/2hC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2hC;

    return-object v0
.end method
