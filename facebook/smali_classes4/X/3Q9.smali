.class public LX/3Q9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:LX/0Sh;


# direct methods
.method public constructor <init>(LX/0Sh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 563232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563233
    iput-object p1, p0, LX/3Q9;->a:LX/0Sh;

    .line 563234
    return-void
.end method

.method public static a(LX/0QB;)LX/3Q9;
    .locals 1

    .prologue
    .line 563251
    invoke-static {p0}, LX/3Q9;->b(LX/0QB;)LX/3Q9;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3Q9;
    .locals 2

    .prologue
    .line 563249
    new-instance v1, LX/3Q9;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-direct {v1, v0}, LX/3Q9;-><init>(LX/0Sh;)V

    .line 563250
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;I)Landroid/text/Spannable;
    .locals 6

    .prologue
    .line 563235
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    .line 563236
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 563237
    :goto_0
    return-object v0

    .line 563238
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 563239
    sget-object v2, LX/16z;->i:Ljava/util/Comparator;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 563240
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 563241
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 563242
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLEntityAtRange;)LX/1yL;

    move-result-object v0

    invoke-static {v3, v0}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v0

    .line 563243
    new-instance v3, LX/5QA;

    const/4 v4, 0x1

    invoke-direct {v3, p0, v4, p2}, LX/5QA;-><init>(LX/3Q9;II)V

    .line 563244
    iget v4, v0, LX/1yN;->a:I

    move v4, v4

    .line 563245
    invoke-virtual {v0}, LX/1yN;->c()I

    move-result v0

    const/16 v5, 0x21

    invoke-virtual {v1, v3, v4, v0, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 563246
    :catch_0
    move-exception v0

    .line 563247
    const-string v3, "TextWithEntitiesUtil"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 563248
    goto :goto_0
.end method
