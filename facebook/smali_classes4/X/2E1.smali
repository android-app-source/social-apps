.class public LX/2E1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/09r;
.implements LX/09s;
.implements LX/0Up;


# instance fields
.field private final a:LX/0Xl;

.field private final b:LX/0Uo;

.field private final c:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
    .end annotation
.end field

.field private final d:LX/03V;

.field private final e:LX/0W3;

.field private final f:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final g:LX/0Uh;

.field private h:Z

.field private i:LX/0Yb;

.field private j:LX/0Yb;


# direct methods
.method public constructor <init>(LX/0Xl;LX/0Uo;Landroid/os/Handler;LX/03V;LX/0W3;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Uh;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384859
    iput-object p1, p0, LX/2E1;->a:LX/0Xl;

    .line 384860
    iput-object p2, p0, LX/2E1;->b:LX/0Uo;

    .line 384861
    iput-object p3, p0, LX/2E1;->c:Landroid/os/Handler;

    .line 384862
    iput-object p4, p0, LX/2E1;->d:LX/03V;

    .line 384863
    iput-object p5, p0, LX/2E1;->e:LX/0W3;

    .line 384864
    iput-object p6, p0, LX/2E1;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 384865
    iput-object p7, p0, LX/2E1;->g:LX/0Uh;

    .line 384866
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/2E1;)V
    .locals 2

    .prologue
    .line 384920
    monitor-enter p0

    .line 384921
    :try_start_0
    sget-object v0, LX/00L;->mANRDetector:LX/00O;

    .line 384922
    invoke-static {v0}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->getInstance(LX/00O;)Lcom/facebook/acra/ANRDetector$ANRDetectorThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->pauseDetector()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384923
    monitor-exit p0

    return-void

    .line 384924
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b$redex0(LX/2E1;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.Thread.start"
        }
    .end annotation

    .prologue
    .line 384905
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2E1;->h:Z

    if-nez v0, :cond_0

    .line 384906
    iget-object v0, p0, LX/2E1;->e:LX/0W3;

    sget-wide v2, LX/0X5;->hQ:J

    const/4 v1, 0x5

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    .line 384907
    int-to-long v0, v0

    .line 384908
    sget-object v2, LX/00L;->mANRDetector:LX/00O;

    .line 384909
    invoke-static {v2}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->getInstance(LX/00O;)Lcom/facebook/acra/ANRDetector$ANRDetectorThread;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->setDelay(J)V

    .line 384910
    invoke-static {}, LX/00L;->startANRDetector()V

    .line 384911
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2E1;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384912
    :goto_0
    monitor-exit p0

    return-void

    .line 384913
    :cond_0
    :try_start_1
    invoke-virtual {p0}, LX/2E1;->shouldANRDetectorRun()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 384914
    invoke-static {}, LX/00L;->startANRDetector()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 384915
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 384916
    :cond_1
    :try_start_2
    sget-object v0, LX/00L;->mANRDetector:LX/00O;

    .line 384917
    invoke-static {v0}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->getInstance(LX/00O;)Lcom/facebook/acra/ANRDetector$ANRDetectorThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->stopDetector()V

    .line 384918
    iget-object v0, p0, LX/2E1;->i:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 384919
    iget-object v0, p0, LX/2E1;->j:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 384882
    iget-object v0, p0, LX/2E1;->b:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 384883
    invoke-static {p0}, LX/2E1;->b$redex0(LX/2E1;)V

    .line 384884
    :cond_0
    iget-object v0, p0, LX/2E1;->a:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v2, LX/2E7;

    invoke-direct {v2, p0}, LX/2E7;-><init>(LX/2E1;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/2E1;->c:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2E1;->i:LX/0Yb;

    .line 384885
    iget-object v0, p0, LX/2E1;->i:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 384886
    iget-object v0, p0, LX/2E1;->a:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v2, LX/2E8;

    invoke-direct {v2, p0}, LX/2E8;-><init>(LX/2E1;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/2E1;->c:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2E1;->j:LX/0Yb;

    .line 384887
    iget-object v0, p0, LX/2E1;->j:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 384888
    if-nez p0, :cond_1

    .line 384889
    :goto_0
    sget-object v0, LX/00L;->mANRReport:LX/00N;

    .line 384890
    iput-object p0, v0, LX/00N;->mANRDataProvider:LX/09r;

    .line 384891
    sget-object v0, LX/00L;->mANRDetector:LX/00O;

    .line 384892
    iput-object p0, v0, LX/00O;->mANRDataProvider:LX/09r;

    .line 384893
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "ANR Reports will "

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/00O;->shouldReportANRStats(LX/00O;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ""

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " be sent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384894
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/009;->setANRDataProvider(LX/09r;)V

    .line 384895
    sget-object v0, LX/00L;->mANRDetector:LX/00O;

    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v1

    .line 384896
    invoke-static {v0}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->getInstance(LX/00O;)Lcom/facebook/acra/ANRDetector$ANRDetectorThread;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->sendCachedANRReports(LX/009;)V

    .line 384897
    sget-object v0, LX/00L;->mANRReport:LX/00N;

    .line 384898
    iput-object p0, v0, LX/00N;->mPerformanceMarker:LX/09s;

    .line 384899
    return-void

    .line 384900
    :cond_1
    sget-object v0, LX/00L;->mConfig:LX/00K;

    move-object v0, v0

    .line 384901
    iget-object v1, v0, LX/00K;->mApplicationContext:Landroid/content/Context;

    move-object v0, v1

    .line 384902
    const-string v1, "acra_flags_store"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 384903
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "anr_gk_cached"

    invoke-interface {p0}, LX/09r;->shouldANRDetectorRun()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 384904
    :cond_2
    const-string v1, "NOT"

    goto :goto_1
.end method

.method public final markerEnd(S)V
    .locals 2

    .prologue
    .line 384880
    iget-object v0, p0, LX/2E1;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7d0002

    invoke-interface {v0, v1, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 384881
    return-void
.end method

.method public final markerStart()V
    .locals 2

    .prologue
    .line 384878
    iget-object v0, p0, LX/2E1;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7d0002

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 384879
    return-void
.end method

.method public final provideStats(LX/009;)V
    .locals 3

    .prologue
    .line 384872
    invoke-static {}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a()[Ljava/lang/String;

    move-result-object v1

    .line 384873
    const-string v2, "anr_proc_stat_state_tag"

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    aget-object v0, v1, v0

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/009;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 384874
    const-string v2, "anr_proc_stat_tag"

    if-eqz v1, :cond_1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v2, v0}, LX/009;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 384875
    return-void

    .line 384876
    :cond_0
    const-string v0, "N/A"

    goto :goto_0

    .line 384877
    :cond_1
    const-string v0, "N/A"

    goto :goto_1
.end method

.method public final reportAnrState(Z)V
    .locals 0

    .prologue
    .line 384870
    invoke-static {p1}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a(Z)V

    .line 384871
    return-void
.end method

.method public final reportSoftError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 384868
    iget-object v0, p0, LX/2E1;->d:LX/03V;

    invoke-virtual {v0, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 384869
    return-void
.end method

.method public final shouldANRDetectorRun()Z
    .locals 3

    .prologue
    .line 384867
    iget-object v0, p0, LX/2E1;->g:LX/0Uh;

    const/16 v1, 0x473

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
