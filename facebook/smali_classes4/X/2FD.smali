.class public LX/2FD;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/compactdisk/AttributeStoreHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/compactdisk/AttributeStoreHolder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 386628
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/compactdisk/AttributeStoreHolder;
    .locals 3

    .prologue
    .line 386629
    sget-object v0, LX/2FD;->a:Lcom/facebook/compactdisk/AttributeStoreHolder;

    if-nez v0, :cond_1

    .line 386630
    const-class v1, LX/2FD;

    monitor-enter v1

    .line 386631
    :try_start_0
    sget-object v0, LX/2FD;->a:Lcom/facebook/compactdisk/AttributeStoreHolder;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 386632
    if-eqz v2, :cond_0

    .line 386633
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 386634
    invoke-static {v0}, LX/2FA;->a(LX/0QB;)Lcom/facebook/compactdisk/FileUtilsHolder;

    move-result-object p0

    check-cast p0, Lcom/facebook/compactdisk/FileUtilsHolder;

    invoke-static {p0}, LX/2FB;->a(Lcom/facebook/compactdisk/FileUtilsHolder;)Lcom/facebook/compactdisk/AttributeStoreHolder;

    move-result-object p0

    move-object v0, p0

    .line 386635
    sput-object v0, LX/2FD;->a:Lcom/facebook/compactdisk/AttributeStoreHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 386636
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 386637
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 386638
    :cond_1
    sget-object v0, LX/2FD;->a:Lcom/facebook/compactdisk/AttributeStoreHolder;

    return-object v0

    .line 386639
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 386640
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 386641
    invoke-static {p0}, LX/2FA;->a(LX/0QB;)Lcom/facebook/compactdisk/FileUtilsHolder;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdisk/FileUtilsHolder;

    invoke-static {v0}, LX/2FB;->a(Lcom/facebook/compactdisk/FileUtilsHolder;)Lcom/facebook/compactdisk/AttributeStoreHolder;

    move-result-object v0

    return-object v0
.end method
