.class public LX/2cT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2cF;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile n:LX/2cT;


# instance fields
.field private final b:LX/2PJ;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/inbound/InboundMessageQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/26j;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2PM;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/3R2;

.field private h:Lcom/facebook/omnistore/Omnistore;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/omnistore/Collection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/omnistore/CollectionName;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:LX/2cV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final l:LX/2cV;

.field private final m:LX/2cV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 440938
    const-class v0, LX/2cT;

    sput-object v0, LX/2cT;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2PJ;LX/0Or;LX/0Or;LX/26j;LX/0Ot;LX/3R2;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2PJ;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/inbound/InboundMessageQueue;",
            ">;",
            "LX/26j;",
            "LX/0Ot",
            "<",
            "LX/2PM;",
            ">;",
            "LX/3R2;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 440998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440999
    new-instance v0, LX/2cU;

    invoke-direct {v0, p0}, LX/2cU;-><init>(LX/2cT;)V

    iput-object v0, p0, LX/2cT;->l:LX/2cV;

    .line 441000
    new-instance v0, LX/2cW;

    invoke-direct {v0, p0}, LX/2cW;-><init>(LX/2cT;)V

    iput-object v0, p0, LX/2cT;->m:LX/2cV;

    .line 441001
    iput-object p1, p0, LX/2cT;->b:LX/2PJ;

    .line 441002
    iput-object p2, p0, LX/2cT;->c:LX/0Or;

    .line 441003
    iput-object p3, p0, LX/2cT;->d:LX/0Or;

    .line 441004
    iput-object p4, p0, LX/2cT;->e:LX/26j;

    .line 441005
    iput-object p5, p0, LX/2cT;->f:LX/0Ot;

    .line 441006
    iput-object p6, p0, LX/2cT;->g:LX/3R2;

    .line 441007
    iget-object v0, p0, LX/2cT;->g:LX/3R2;

    invoke-virtual {v0, p0}, LX/3R2;->a(LX/2cF;)V

    .line 441008
    return-void
.end method

.method public static a(LX/0QB;)LX/2cT;
    .locals 10

    .prologue
    .line 440985
    sget-object v0, LX/2cT;->n:LX/2cT;

    if-nez v0, :cond_1

    .line 440986
    const-class v1, LX/2cT;

    monitor-enter v1

    .line 440987
    :try_start_0
    sget-object v0, LX/2cT;->n:LX/2cT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 440988
    if-eqz v2, :cond_0

    .line 440989
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 440990
    new-instance v3, LX/2cT;

    invoke-static {v0}, LX/2PJ;->a(LX/0QB;)LX/2PJ;

    move-result-object v4

    check-cast v4, LX/2PJ;

    const/16 v5, 0x15e8

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x2a30

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/26j;->b(LX/0QB;)LX/26j;

    move-result-object v7

    check-cast v7, LX/26j;

    const/16 v8, 0xdc0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/3R2;->a(LX/0QB;)LX/3R2;

    move-result-object v9

    check-cast v9, LX/3R2;

    invoke-direct/range {v3 .. v9}, LX/2cT;-><init>(LX/2PJ;LX/0Or;LX/0Or;LX/26j;LX/0Ot;LX/3R2;)V

    .line 440991
    move-object v0, v3

    .line 440992
    sput-object v0, LX/2cT;->n:LX/2cT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 440993
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 440994
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 440995
    :cond_1
    sget-object v0, LX/2cT;->n:LX/2cT;

    return-object v0

    .line 440996
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 440997
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b$redex0(LX/2cT;)J
    .locals 5

    .prologue
    .line 440972
    iget-object v0, p0, LX/2cT;->h:Lcom/facebook/omnistore/Omnistore;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2cT;->j:Lcom/facebook/omnistore/CollectionName;

    if-nez v0, :cond_1

    .line 440973
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No Omnistore available"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 440974
    :cond_1
    iget-object v0, p0, LX/2cT;->h:Lcom/facebook/omnistore/Omnistore;

    .line 440975
    invoke-virtual {v0}, Lcom/facebook/omnistore/Omnistore;->getDebugInfo()Ljava/lang/String;

    move-result-object v1

    .line 440976
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 440977
    const-string v1, "subscription_info"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "subscriptions"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    move-object v1, v1

    .line 440978
    iget-object v0, p0, LX/2cT;->j:Lcom/facebook/omnistore/CollectionName;

    invoke-virtual {v0}, Lcom/facebook/omnistore/CollectionName;->toString()Ljava/lang/String;

    move-result-object v2

    .line 440979
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 440980
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 440981
    const-string v4, "collectionName"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 440982
    const-string v0, "globalVersionId"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0

    .line 440983
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 440984
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No Omnistore collection subscription found"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 440967
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2cT;->i:Lcom/facebook/omnistore/Collection;

    if-nez v0, :cond_0

    .line 440968
    iget-object v0, p0, LX/2cT;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PM;

    sget-object v1, LX/2cT;->a:Ljava/lang/Class;

    const-string v2, "No messaging collection subscribed to drain"

    invoke-virtual {v0, v1, v2}, LX/2PM;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 440969
    :goto_0
    monitor-exit p0

    return-void

    .line 440970
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2cT;->i:Lcom/facebook/omnistore/Collection;

    invoke-virtual {v0, p1}, Lcom/facebook/omnistore/Collection;->deleteObject(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 440971
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final indexObject(Ljava/lang/String;Ljava/lang/String;Ljava/nio/ByteBuffer;)Lcom/facebook/omnistore/IndexedFields;
    .locals 1

    .prologue
    .line 440966
    new-instance v0, Lcom/facebook/omnistore/IndexedFields;

    invoke-direct {v0}, Lcom/facebook/omnistore/IndexedFields;-><init>()V

    return-object v0
.end method

.method public final declared-synchronized onCollectionAvailable(Lcom/facebook/omnistore/Collection;)V
    .locals 1

    .prologue
    .line 440960
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/2cT;->i:Lcom/facebook/omnistore/Collection;

    .line 440961
    iget-object v0, p0, LX/2cT;->e:LX/26j;

    invoke-virtual {v0}, LX/26j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440962
    iget-object v0, p0, LX/2cT;->m:LX/2cV;

    iput-object v0, p0, LX/2cT;->k:LX/2cV;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 440963
    :goto_0
    monitor-exit p0

    return-void

    .line 440964
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2cT;->l:LX/2cV;

    iput-object v0, p0, LX/2cT;->k:LX/2cV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 440965
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onCollectionInvalidated()V
    .locals 1

    .prologue
    .line 440956
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/2cT;->i:Lcom/facebook/omnistore/Collection;

    .line 440957
    const/4 v0, 0x0

    iput-object v0, p0, LX/2cT;->k:LX/2cV;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 440958
    monitor-exit p0

    return-void

    .line 440959
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onDeltasReceived(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/omnistore/Delta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 440953
    iget-object v0, p0, LX/2cT;->k:LX/2cV;

    if-eqz v0, :cond_0

    .line 440954
    iget-object v0, p0, LX/2cT;->k:LX/2cV;

    invoke-interface {v0, p1}, LX/2cV;->a(Ljava/util/List;)V

    .line 440955
    :cond_0
    return-void
.end method

.method public final provideSubscriptionInfo(Lcom/facebook/omnistore/Omnistore;)LX/2cJ;
    .locals 8

    .prologue
    .line 440939
    iput-object p1, p0, LX/2cT;->h:Lcom/facebook/omnistore/Omnistore;

    .line 440940
    const-string v0, "tincan_msg"

    invoke-virtual {p1, v0}, Lcom/facebook/omnistore/Omnistore;->createCollectionNameBuilder(Ljava/lang/String;)Lcom/facebook/omnistore/CollectionName$Builder;

    move-result-object v1

    iget-object v0, p0, LX/2cT;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/omnistore/CollectionName$Builder;->addSegment(Ljava/lang/String;)Lcom/facebook/omnistore/CollectionName$Builder;

    move-result-object v0

    iget-object v1, p0, LX/2cT;->b:LX/2PJ;

    invoke-virtual {v1}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/omnistore/CollectionName$Builder;->addSegment(Ljava/lang/String;)Lcom/facebook/omnistore/CollectionName$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/omnistore/CollectionName$Builder;->build()Lcom/facebook/omnistore/CollectionName;

    move-result-object v0

    iput-object v0, p0, LX/2cT;->j:Lcom/facebook/omnistore/CollectionName;

    .line 440941
    iget-object v0, p0, LX/2cT;->e:LX/26j;

    invoke-virtual {v0}, LX/26j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440942
    new-instance v1, LX/2cX;

    invoke-direct {v1}, LX/2cX;-><init>()V

    iget-object v0, p0, LX/2cT;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;

    .line 440943
    iget-object v4, v0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->e:LX/0Sh;

    invoke-virtual {v4}, LX/0Sh;->b()V

    .line 440944
    iget-object v4, v0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->d:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Dof;

    sget-object v5, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->a:LX/2PC;

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, LX/Dod;->a(LX/2PC;J)J

    move-result-wide v4

    move-wide v2, v4

    .line 440945
    iput-wide v2, v1, LX/2cX;->mInitialGlobalVersionId:J

    .line 440946
    move-object v0, v1

    .line 440947
    const/4 v1, 0x0

    .line 440948
    iput-boolean v1, v0, LX/2cX;->mRequiresSnapshot:Z

    .line 440949
    move-object v0, v0

    .line 440950
    invoke-virtual {v0}, LX/2cX;->build()LX/2cY;

    move-result-object v0

    .line 440951
    iget-object v1, p0, LX/2cT;->j:Lcom/facebook/omnistore/CollectionName;

    invoke-static {v1, v0}, LX/2cJ;->forOpenSubscription(Lcom/facebook/omnistore/CollectionName;LX/2cY;)LX/2cJ;

    move-result-object v0

    .line 440952
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2cT;->j:Lcom/facebook/omnistore/CollectionName;

    invoke-static {v0}, LX/2cJ;->forOpenSubscription(Lcom/facebook/omnistore/CollectionName;)LX/2cJ;

    move-result-object v0

    goto :goto_0
.end method
