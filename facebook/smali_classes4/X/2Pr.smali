.class public final LX/2Pr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/2cF;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/2cF;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final mInjector:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 407275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407276
    iput-object p1, p0, LX/2Pr;->mInjector:LX/0QB;

    .line 407277
    return-void
.end method

.method public static getLazySet(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2cF;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 407278
    new-instance v0, LX/2Pr;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2Pr;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 407279
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public static getSet(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/2cF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407272
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/2Pr;

    invoke-direct {v2, p0}, LX/2Pr;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 407273
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2Pr;->mInjector:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v0, v0

    .line 407274
    return-object v0
.end method

.method public final provide(LX/0QC;I)LX/2cF;
    .locals 2

    .prologue
    .line 407262
    packed-switch p2, :pswitch_data_0

    .line 407263
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407264
    :pswitch_0
    invoke-static {p1}, LX/2cE;->a(LX/0QB;)LX/2cE;

    move-result-object v0

    .line 407265
    :goto_0
    return-object v0

    .line 407266
    :pswitch_1
    invoke-static {p1}, LX/2cN;->a(LX/0QB;)LX/2cN;

    move-result-object v0

    goto :goto_0

    .line 407267
    :pswitch_2
    invoke-static {p1}, LX/2cP;->a(LX/0QB;)LX/2cP;

    move-result-object v0

    goto :goto_0

    .line 407268
    :pswitch_3
    invoke-static {p1}, LX/2cR;->a(LX/0QB;)LX/2cR;

    move-result-object v0

    goto :goto_0

    .line 407269
    :pswitch_4
    invoke-static {p1}, LX/2cT;->a(LX/0QB;)LX/2cT;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final bridge synthetic provide(LX/0QC;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 407270
    invoke-virtual {p0, p1, p2}, LX/2Pr;->provide(LX/0QC;I)LX/2cF;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 407271
    const/4 v0, 0x5

    return v0
.end method
