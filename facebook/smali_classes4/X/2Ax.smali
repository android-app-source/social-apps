.class public LX/2Ax;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Ay;


# static fields
.field public static final a:Ljava/lang/Object;


# instance fields
.field public final b:LX/2An;

.field public final c:LX/0lQ;

.field public final d:LX/0lJ;

.field public final e:Ljava/lang/reflect/Method;

.field public final f:Ljava/lang/reflect/Field;

.field public g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0lb;

.field public final i:LX/2Vb;

.field public final j:LX/0lJ;

.field public k:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/2B6;

.field public final n:Z

.field public final o:Ljava/lang/Object;

.field public final p:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public q:LX/4qz;

.field public r:LX/0lJ;

.field public final s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 378587
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2Ax;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2Aq;LX/2An;LX/0lQ;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;LX/0lJ;ZLjava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Aq;",
            "LX/2An;",
            "LX/0lQ;",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;",
            "LX/4qz;",
            "LX/0lJ;",
            "Z",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 378561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 378562
    iput-object p2, p0, LX/2Ax;->b:LX/2An;

    .line 378563
    iput-object p3, p0, LX/2Ax;->c:LX/0lQ;

    .line 378564
    new-instance v0, LX/0lb;

    invoke-virtual {p1}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0lb;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/2Ax;->h:LX/0lb;

    .line 378565
    invoke-virtual {p1}, LX/2Aq;->b()LX/2Vb;

    move-result-object v0

    iput-object v0, p0, LX/2Ax;->i:LX/2Vb;

    .line 378566
    iput-object p4, p0, LX/2Ax;->d:LX/0lJ;

    .line 378567
    iput-object p5, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 378568
    if-nez p5, :cond_0

    .line 378569
    sget-object v0, LX/2B7;->a:LX/2B7;

    move-object v0, v0

    .line 378570
    :goto_0
    iput-object v0, p0, LX/2Ax;->m:LX/2B6;

    .line 378571
    iput-object p6, p0, LX/2Ax;->q:LX/4qz;

    .line 378572
    iput-object p7, p0, LX/2Ax;->j:LX/0lJ;

    .line 378573
    invoke-virtual {p1}, LX/2Aq;->s()Z

    move-result v0

    iput-boolean v0, p0, LX/2Ax;->s:Z

    .line 378574
    instance-of v0, p2, LX/2Am;

    if-eqz v0, :cond_1

    .line 378575
    iput-object v1, p0, LX/2Ax;->e:Ljava/lang/reflect/Method;

    .line 378576
    invoke-virtual {p2}, LX/2An;->j()Ljava/lang/reflect/Member;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Field;

    iput-object v0, p0, LX/2Ax;->f:Ljava/lang/reflect/Field;

    .line 378577
    :goto_1
    iput-boolean p8, p0, LX/2Ax;->n:Z

    .line 378578
    iput-object p9, p0, LX/2Ax;->o:Ljava/lang/Object;

    .line 378579
    invoke-virtual {p1}, LX/2Aq;->p()[Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, LX/2Ax;->p:[Ljava/lang/Class;

    .line 378580
    iput-object v1, p0, LX/2Ax;->l:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 378581
    return-void

    :cond_0
    move-object v0, v1

    .line 378582
    goto :goto_0

    .line 378583
    :cond_1
    instance-of v0, p2, LX/2At;

    if-eqz v0, :cond_2

    .line 378584
    invoke-virtual {p2}, LX/2An;->j()Ljava/lang/reflect/Member;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Method;

    iput-object v0, p0, LX/2Ax;->e:Ljava/lang/reflect/Method;

    .line 378585
    iput-object v1, p0, LX/2Ax;->f:Ljava/lang/reflect/Field;

    goto :goto_1

    .line 378586
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not pass member of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(LX/2Ax;)V
    .locals 1

    .prologue
    .line 378559
    iget-object v0, p1, LX/2Ax;->h:LX/0lb;

    invoke-direct {p0, p1, v0}, LX/2Ax;-><init>(LX/2Ax;LX/0lb;)V

    .line 378560
    return-void
.end method

.method public constructor <init>(LX/2Ax;LX/0lb;)V
    .locals 2

    .prologue
    .line 378538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 378539
    iput-object p2, p0, LX/2Ax;->h:LX/0lb;

    .line 378540
    iget-object v0, p1, LX/2Ax;->i:LX/2Vb;

    iput-object v0, p0, LX/2Ax;->i:LX/2Vb;

    .line 378541
    iget-object v0, p1, LX/2Ax;->b:LX/2An;

    iput-object v0, p0, LX/2Ax;->b:LX/2An;

    .line 378542
    iget-object v0, p1, LX/2Ax;->c:LX/0lQ;

    iput-object v0, p0, LX/2Ax;->c:LX/0lQ;

    .line 378543
    iget-object v0, p1, LX/2Ax;->d:LX/0lJ;

    iput-object v0, p0, LX/2Ax;->d:LX/0lJ;

    .line 378544
    iget-object v0, p1, LX/2Ax;->e:Ljava/lang/reflect/Method;

    iput-object v0, p0, LX/2Ax;->e:Ljava/lang/reflect/Method;

    .line 378545
    iget-object v0, p1, LX/2Ax;->f:Ljava/lang/reflect/Field;

    iput-object v0, p0, LX/2Ax;->f:Ljava/lang/reflect/Field;

    .line 378546
    iget-object v0, p1, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iput-object v0, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 378547
    iget-object v0, p1, LX/2Ax;->l:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iput-object v0, p0, LX/2Ax;->l:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 378548
    iget-object v0, p1, LX/2Ax;->g:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 378549
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p1, LX/2Ax;->g:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, LX/2Ax;->g:Ljava/util/HashMap;

    .line 378550
    :cond_0
    iget-object v0, p1, LX/2Ax;->j:LX/0lJ;

    iput-object v0, p0, LX/2Ax;->j:LX/0lJ;

    .line 378551
    iget-object v0, p1, LX/2Ax;->m:LX/2B6;

    iput-object v0, p0, LX/2Ax;->m:LX/2B6;

    .line 378552
    iget-boolean v0, p1, LX/2Ax;->n:Z

    iput-boolean v0, p0, LX/2Ax;->n:Z

    .line 378553
    iget-object v0, p1, LX/2Ax;->o:Ljava/lang/Object;

    iput-object v0, p0, LX/2Ax;->o:Ljava/lang/Object;

    .line 378554
    iget-object v0, p1, LX/2Ax;->p:[Ljava/lang/Class;

    iput-object v0, p0, LX/2Ax;->p:[Ljava/lang/Class;

    .line 378555
    iget-object v0, p1, LX/2Ax;->q:LX/4qz;

    iput-object v0, p0, LX/2Ax;->q:LX/4qz;

    .line 378556
    iget-object v0, p1, LX/2Ax;->r:LX/0lJ;

    iput-object v0, p0, LX/2Ax;->r:LX/0lJ;

    .line 378557
    iget-boolean v0, p1, LX/2Ax;->s:Z

    iput-boolean v0, p0, LX/2Ax;->s:Z

    .line 378558
    return-void
.end method

.method public static c(Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 378535
    invoke-virtual {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378536
    return-void

    .line 378537
    :cond_0
    new-instance v0, LX/28E;

    const-string v1, "Direct self-reference leading to cycle"

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()LX/0lJ;
    .locals 1

    .prologue
    .line 378534
    iget-object v0, p0, LX/2Ax;->d:LX/0lJ;

    return-object v0
.end method

.method public a(LX/4ro;)LX/2Ax;
    .locals 3

    .prologue
    .line 378531
    iget-object v0, p0, LX/2Ax;->h:LX/0lb;

    invoke-virtual {v0}, LX/0lb;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/4ro;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 378532
    iget-object v0, p0, LX/2Ax;->h:LX/0lb;

    invoke-virtual {v0}, LX/0lb;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378533
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/2Ax;

    new-instance v2, LX/0lb;

    invoke-direct {v2, v1}, LX/0lb;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, p0, v2}, LX/2Ax;-><init>(LX/2Ax;LX/0lb;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public a(LX/2B6;Ljava/lang/Class;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2B6;",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0my;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 378524
    iget-object v0, p0, LX/2Ax;->r:LX/0lJ;

    if-eqz v0, :cond_1

    .line 378525
    iget-object v0, p0, LX/2Ax;->r:LX/0lJ;

    invoke-virtual {p3, v0, p2}, LX/0mz;->a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    .line 378526
    invoke-virtual {p1, v0, p3, p0}, LX/2B6;->a(LX/0lJ;LX/0my;LX/2Ay;)LX/2Bd;

    move-result-object v0

    .line 378527
    :goto_0
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    if-eq p1, v1, :cond_0

    .line 378528
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    iput-object v1, p0, LX/2Ax;->m:LX/2B6;

    .line 378529
    :cond_0
    iget-object v0, v0, LX/2Bd;->a:Lcom/fasterxml/jackson/databind/JsonSerializer;

    return-object v0

    .line 378530
    :cond_1
    invoke-virtual {p1, p2, p3, p0}, LX/2B6;->a(Ljava/lang/Class;LX/0my;LX/2Ay;)LX/2Bd;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 378521
    iget-object v0, p0, LX/2Ax;->e:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 378522
    iget-object v0, p0, LX/2Ax;->e:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 378523
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2Ax;->f:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 378517
    iget-object v0, p0, LX/2Ax;->l:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_0

    .line 378518
    iget-object v0, p0, LX/2Ax;->l:Lcom/fasterxml/jackson/databind/JsonSerializer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 378519
    :goto_0
    return-void

    .line 378520
    :cond_0
    invoke-virtual {p1}, LX/0nX;->h()V

    goto :goto_0
.end method

.method public a(Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 378513
    iget-object v0, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eq v0, p1, :cond_0

    .line 378514
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not override serializer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 378515
    :cond_0
    iput-object p1, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 378516
    return-void
.end method

.method public a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 378490
    invoke-virtual {p0, p1}, LX/2Ax;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 378491
    if-nez v1, :cond_1

    .line 378492
    iget-object v0, p0, LX/2Ax;->l:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_0

    .line 378493
    iget-object v0, p0, LX/2Ax;->h:LX/0lb;

    invoke-virtual {p2, v0}, LX/0nX;->b(LX/0lc;)V

    .line 378494
    iget-object v0, p0, LX/2Ax;->l:Lcom/fasterxml/jackson/databind/JsonSerializer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 378495
    :cond_0
    :goto_0
    return-void

    .line 378496
    :cond_1
    iget-object v0, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 378497
    if-nez v0, :cond_2

    .line 378498
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 378499
    iget-object v3, p0, LX/2Ax;->m:LX/2B6;

    .line 378500
    invoke-virtual {v3, v2}, LX/2B6;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 378501
    if-nez v0, :cond_2

    .line 378502
    invoke-virtual {p0, v3, v2, p3}, LX/2Ax;->a(LX/2B6;Ljava/lang/Class;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 378503
    :cond_2
    iget-object v2, p0, LX/2Ax;->o:Ljava/lang/Object;

    if-eqz v2, :cond_3

    .line 378504
    sget-object v2, LX/2Ax;->a:Ljava/lang/Object;

    iget-object v3, p0, LX/2Ax;->o:Ljava/lang/Object;

    if-ne v2, v3, :cond_5

    .line 378505
    invoke-virtual {v0, v1}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 378506
    :cond_3
    if-ne v1, p1, :cond_4

    .line 378507
    invoke-static {v0}, LX/2Ax;->c(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 378508
    :cond_4
    iget-object v2, p0, LX/2Ax;->h:LX/0lb;

    invoke-virtual {p2, v2}, LX/0nX;->b(LX/0lc;)V

    .line 378509
    iget-object v2, p0, LX/2Ax;->q:LX/4qz;

    if-nez v2, :cond_6

    .line 378510
    invoke-virtual {v0, v1, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_0

    .line 378511
    :cond_5
    iget-object v2, p0, LX/2Ax;->o:Ljava/lang/Object;

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0

    .line 378512
    :cond_6
    iget-object v2, p0, LX/2Ax;->q:LX/4qz;

    invoke-virtual {v0, v1, p2, p3, v2}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V

    goto :goto_0
.end method

.method public final b()LX/2An;
    .locals 1

    .prologue
    .line 378441
    iget-object v0, p0, LX/2Ax;->b:LX/2An;

    return-object v0
.end method

.method public final b(LX/4ro;)LX/2Ax;
    .locals 1

    .prologue
    .line 378489
    new-instance v0, LX/4rW;

    invoke-direct {v0, p0, p1}, LX/4rW;-><init>(LX/2Ax;LX/4ro;)V

    return-object v0
.end method

.method public b(Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 378485
    iget-object v0, p0, LX/2Ax;->l:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Ax;->l:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eq v0, p1, :cond_0

    .line 378486
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not override null serializer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 378487
    :cond_0
    iput-object p1, p0, LX/2Ax;->l:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 378488
    return-void
.end method

.method public b(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 378461
    invoke-virtual {p0, p1}, LX/2Ax;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 378462
    if-nez v1, :cond_1

    .line 378463
    iget-object v0, p0, LX/2Ax;->l:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_0

    .line 378464
    iget-object v0, p0, LX/2Ax;->l:Lcom/fasterxml/jackson/databind/JsonSerializer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 378465
    :goto_0
    return-void

    .line 378466
    :cond_0
    invoke-virtual {p2}, LX/0nX;->h()V

    goto :goto_0

    .line 378467
    :cond_1
    iget-object v0, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 378468
    if-nez v0, :cond_2

    .line 378469
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 378470
    iget-object v3, p0, LX/2Ax;->m:LX/2B6;

    .line 378471
    invoke-virtual {v3, v2}, LX/2B6;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 378472
    if-nez v0, :cond_2

    .line 378473
    invoke-virtual {p0, v3, v2, p3}, LX/2Ax;->a(LX/2B6;Ljava/lang/Class;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 378474
    :cond_2
    iget-object v2, p0, LX/2Ax;->o:Ljava/lang/Object;

    if-eqz v2, :cond_4

    .line 378475
    sget-object v2, LX/2Ax;->a:Ljava/lang/Object;

    iget-object v3, p0, LX/2Ax;->o:Ljava/lang/Object;

    if-ne v2, v3, :cond_3

    .line 378476
    invoke-virtual {v0, v1}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 378477
    invoke-virtual {p0, p2, p3}, LX/2Ax;->a(LX/0nX;LX/0my;)V

    goto :goto_0

    .line 378478
    :cond_3
    iget-object v2, p0, LX/2Ax;->o:Ljava/lang/Object;

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 378479
    invoke-virtual {p0, p2, p3}, LX/2Ax;->a(LX/0nX;LX/0my;)V

    goto :goto_0

    .line 378480
    :cond_4
    if-ne v1, p1, :cond_5

    .line 378481
    invoke-static {v0}, LX/2Ax;->c(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 378482
    :cond_5
    iget-object v2, p0, LX/2Ax;->q:LX/4qz;

    if-nez v2, :cond_6

    .line 378483
    invoke-virtual {v0, v1, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_0

    .line 378484
    :cond_6
    iget-object v2, p0, LX/2Ax;->q:LX/4qz;

    invoke-virtual {v0, v1, p2, p3, v2}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 378460
    iget-object v0, p0, LX/2Ax;->h:LX/0lb;

    invoke-virtual {v0}, LX/0lb;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 378459
    iget-object v0, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 378458
    iget-object v0, p0, LX/2Ax;->l:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 378455
    iget-object v0, p0, LX/2Ax;->e:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 378456
    iget-object v0, p0, LX/2Ax;->e:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v0

    .line 378457
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2Ax;->f:Ljava/lang/reflect/Field;

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 378452
    iget-object v0, p0, LX/2Ax;->e:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 378453
    iget-object v0, p0, LX/2Ax;->e:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getGenericReturnType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 378454
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2Ax;->f:Ljava/lang/reflect/Field;

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 378442
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 378443
    const-string v1, "property \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/2Ax;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378444
    iget-object v1, p0, LX/2Ax;->e:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    .line 378445
    const-string v1, "via method "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2Ax;->e:Ljava/lang/reflect/Method;

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2Ax;->e:Ljava/lang/reflect/Method;

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378446
    :goto_0
    iget-object v1, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-nez v1, :cond_1

    .line 378447
    const-string v1, ", no static serializer"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378448
    :goto_1
    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 378449
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 378450
    :cond_0
    const-string v1, "field \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2Ax;->f:Ljava/lang/reflect/Field;

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2Ax;->f:Ljava/lang/reflect/Field;

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 378451
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ", static serializer of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
