.class public LX/3Lb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Lc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Lc",
        "<",
        "Ljava/lang/Void;",
        "LX/3MZ;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# static fields
.field public static final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/3MZ;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field

.field public B:LX/3Mb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Mb",
            "<",
            "Ljava/lang/Void;",
            "LX/3MZ;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field

.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3NF;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3LN;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Kk;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Ku;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "LX/3MZ;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field

.field private final g:LX/3LP;

.field private final h:LX/3Ld;

.field private final i:LX/2CH;

.field public final j:LX/2It;

.field public final k:LX/0TD;

.field public final l:Ljava/util/concurrent/Executor;

.field private final m:LX/3Le;

.field private final n:LX/3Lj;

.field private final o:LX/2UR;

.field public final p:LX/03V;

.field public final q:LX/3Lo;

.field public final r:LX/2Iv;

.field private final s:LX/3LO;

.field private final t:LX/3Lv;

.field private final u:LX/3MU;

.field private final v:LX/0Uh;

.field private w:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FDh;",
            ">;"
        }
    .end annotation
.end field

.field private x:LX/2RQ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:LX/3La;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 551297
    const-class v0, LX/3Lb;

    sput-object v0, LX/3Lb;->f:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>(LX/3LP;LX/3Ld;LX/2CH;LX/2It;LX/0TD;Ljava/util/concurrent/Executor;LX/3Le;LX/3Lj;LX/2UR;LX/03V;LX/3Lo;LX/2Iv;LX/3LO;LX/3Lv;LX/3MU;LX/0Uh;)V
    .locals 2
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 551298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 551299
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/3Lb;->w:LX/0Ot;

    .line 551300
    sget-object v1, LX/3MZ;->a:LX/3MZ;

    iput-object v1, p0, LX/3Lb;->A:LX/3MZ;

    .line 551301
    iput-object p1, p0, LX/3Lb;->g:LX/3LP;

    .line 551302
    iput-object p2, p0, LX/3Lb;->h:LX/3Ld;

    .line 551303
    iput-object p3, p0, LX/3Lb;->i:LX/2CH;

    .line 551304
    iput-object p4, p0, LX/3Lb;->j:LX/2It;

    .line 551305
    iput-object p5, p0, LX/3Lb;->k:LX/0TD;

    .line 551306
    iput-object p6, p0, LX/3Lb;->l:Ljava/util/concurrent/Executor;

    .line 551307
    iput-object p7, p0, LX/3Lb;->m:LX/3Le;

    .line 551308
    iput-object p8, p0, LX/3Lb;->n:LX/3Lj;

    .line 551309
    iput-object p9, p0, LX/3Lb;->o:LX/2UR;

    .line 551310
    iput-object p10, p0, LX/3Lb;->p:LX/03V;

    .line 551311
    iput-object p11, p0, LX/3Lb;->q:LX/3Lo;

    .line 551312
    iput-object p12, p0, LX/3Lb;->r:LX/2Iv;

    .line 551313
    iput-object p13, p0, LX/3Lb;->s:LX/3LO;

    .line 551314
    move-object/from16 v0, p14

    iput-object v0, p0, LX/3Lb;->t:LX/3Lv;

    .line 551315
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3Lb;->u:LX/3MU;

    .line 551316
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3Lb;->v:LX/0Uh;

    .line 551317
    return-void
.end method

.method private static a(LX/3Lb;LX/3Oo;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Oo;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551318
    const-string v0, "getTopPhoneContacts"

    const v1, -0x52027efd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551319
    :try_start_0
    iget-object v0, p0, LX/3Lb;->t:LX/3Lv;

    .line 551320
    const/16 v1, 0xa

    invoke-virtual {v0, v1, p1}, LX/3Lv;->a(ILX/3Oo;)LX/0Px;

    move-result-object v1

    move-object v2, v1

    .line 551321
    invoke-virtual {v2}, LX/0Px;->size()I

    .line 551322
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 551323
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 551324
    iget-object p1, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, p1

    .line 551325
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 551326
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 551327
    :cond_0
    iget-object v0, p0, LX/3Lb;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LN;

    .line 551328
    invoke-virtual {v0, v2}, LX/3LN;->a(LX/0Px;)V

    .line 551329
    sget-object v1, LX/DAk;->TOP_PHONE_CONTACT:LX/DAk;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/3LN;->a(LX/DAk;LX/0Px;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 551330
    const v0, -0x343541b8    # -2.6573968E7f

    invoke-static {v0}, LX/02m;->a(I)V

    return-object v2

    :catchall_0
    move-exception v0

    const v1, -0x2b7d6f27

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(LX/3Lb;Ljava/util/Map;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551331
    const-string v0, "getFavoriteFriends"

    const v1, 0x20e9016f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551332
    :try_start_0
    iget-object v0, p0, LX/3Lb;->h:LX/3Ld;

    invoke-virtual {v0}, LX/3Ld;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 551333
    const v1, 0x1dfaf2a5

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x6623419f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(LX/3Lb;Ljava/util/Map;I)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551334
    const-string v0, "getPHATContacts"

    const v1, -0x125aae51

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551335
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 551336
    iget-object v1, p0, LX/3Lb;->x:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    sget-object v2, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 551337
    iput-object v2, v1, LX/2RR;->b:Ljava/util/Collection;

    .line 551338
    move-object v1, v1

    .line 551339
    const/4 v2, 0x1

    .line 551340
    iput-boolean v2, v1, LX/2RR;->f:Z

    .line 551341
    move-object v1, v1

    .line 551342
    const/4 v2, 0x1

    .line 551343
    iput-boolean v2, v1, LX/2RR;->i:Z

    .line 551344
    move-object v1, v1

    .line 551345
    invoke-virtual {v1}, LX/2RR;->i()LX/2RR;

    move-result-object v1

    sget-object v2, LX/2RS;->PHAT_RANK:LX/2RS;

    .line 551346
    iput-object v2, v1, LX/2RR;->n:LX/2RS;

    .line 551347
    move-object v1, v1

    .line 551348
    const/4 v2, 0x1

    .line 551349
    iput-boolean v2, v1, LX/2RR;->o:Z

    .line 551350
    move-object v1, v1

    .line 551351
    iput p2, v1, LX/2RR;->p:I

    .line 551352
    move-object v1, v1

    .line 551353
    iget-object v2, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v2, v1}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 551354
    const-string v2, "#fetch"

    const v3, 0xdeb9da6

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 551355
    :try_start_1
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551356
    const v2, -0x236d9f20

    :try_start_2
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551357
    invoke-interface {v1}, LX/3On;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551358
    const v1, 0x6f96c0f6

    invoke-static {v1}, LX/02m;->a(I)V

    .line 551359
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 551360
    :catchall_0
    move-exception v0

    const v2, -0x3261e52d    # -3.3156976E8f

    :try_start_3
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551361
    invoke-interface {v1}, LX/3On;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551362
    :catchall_1
    move-exception v0

    const v1, 0x7d1b7dab

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(LX/3Lb;Ljava/util/Map;LX/2RR;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/2RR;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551363
    const-string v0, "getAllFriendsWithCap"

    const v1, 0xb112bac

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551364
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 551365
    iget-object v1, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v1, p2}, LX/3LP;->a(LX/2RR;)LX/3On;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v1

    .line 551366
    :try_start_1
    const-string v2, "#fetch"

    const v3, -0x7d88c350

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 551367
    :try_start_2
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 551368
    const v2, -0x79555b4

    :try_start_3
    invoke-static {v2}, LX/02m;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551369
    :try_start_4
    invoke-interface {v1}, LX/3On;->close()V

    .line 551370
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v0

    .line 551371
    const v1, 0x6739d228

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 551372
    :catchall_0
    move-exception v0

    const v2, 0x39d501c1

    :try_start_5
    invoke-static {v2}, LX/02m;->a(I)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 551373
    :catchall_1
    move-exception v0

    :try_start_6
    invoke-interface {v1}, LX/3On;->close()V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 551374
    :catchall_2
    move-exception v0

    const v1, 0x25ff7000

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(LX/3Lb;Ljava/util/Map;Z)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;Z)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551375
    if-eqz p2, :cond_1

    .line 551376
    const-string v0, "getAllContactsWithCap"

    const v1, -0x511c9199

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551377
    :goto_0
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 551378
    iget-object v1, p0, LX/3Lb;->x:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    sget-object v2, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 551379
    iput-object v2, v1, LX/2RR;->b:Ljava/util/Collection;

    .line 551380
    move-object v1, v1

    .line 551381
    const/4 v2, 0x1

    .line 551382
    iput-boolean v2, v1, LX/2RR;->f:Z

    .line 551383
    move-object v1, v1

    .line 551384
    invoke-virtual {v1}, LX/2RR;->i()LX/2RR;

    move-result-object v1

    const/4 v2, 0x1

    .line 551385
    iput-boolean v2, v1, LX/2RR;->l:Z

    .line 551386
    move-object v1, v1

    .line 551387
    sget-object v2, LX/2RS;->NAME:LX/2RS;

    .line 551388
    iput-object v2, v1, LX/2RR;->n:LX/2RS;

    .line 551389
    move-object v1, v1

    .line 551390
    if-eqz p2, :cond_0

    .line 551391
    sget v2, LX/FDj;->a:I

    .line 551392
    iput v2, v1, LX/2RR;->p:I

    .line 551393
    :cond_0
    iget-object v2, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v2, v1}, LX/3LP;->a(LX/2RR;)LX/3On;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v1

    .line 551394
    :try_start_1
    const-string v2, "#fetch"

    const v3, -0x38f3247a

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 551395
    :try_start_2
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 551396
    const v2, -0x4e8ae4e4

    :try_start_3
    invoke-static {v2}, LX/02m;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551397
    :try_start_4
    invoke-interface {v1}, LX/3On;->close()V

    .line 551398
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 551399
    if-eqz p2, :cond_3

    .line 551400
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 551401
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 551402
    iget-object p2, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, p2

    .line 551403
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 551404
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 551405
    :cond_1
    const-string v0, "getAllContacts"

    const v1, -0x4eb1237

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 551406
    :catchall_0
    move-exception v0

    const v2, -0x570886c3

    :try_start_5
    invoke-static {v2}, LX/02m;->a(I)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 551407
    :catchall_1
    move-exception v0

    :try_start_6
    invoke-interface {v1}, LX/3On;->close()V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 551408
    :catchall_2
    move-exception v0

    const v1, 0x54c51c95

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 551409
    :cond_2
    :try_start_7
    iget-object v0, p0, LX/3Lb;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LN;

    .line 551410
    invoke-virtual {v0, v2}, LX/3LN;->a(LX/0Px;)V

    .line 551411
    sget-object v1, LX/DAk;->ALL_CONTACT_CAPPED:LX/DAk;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/3LN;->a(LX/DAk;LX/0Px;)V

    .line 551412
    :cond_3
    invoke-static {p1, v2}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-object v0

    .line 551413
    const v1, 0x641eebd

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0
.end method

.method private static a(Ljava/util/Map;Ljava/util/List;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551414
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;Z)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Map;Ljava/util/List;Z)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;Z)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551570
    const/4 v0, 0x0

    .line 551571
    if-eqz p2, :cond_5

    .line 551572
    new-instance v0, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    move-object v2, v0

    .line 551573
    :goto_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 551574
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 551575
    iget-object v1, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v5, v1

    .line 551576
    invoke-interface {p0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 551577
    if-nez v1, :cond_4

    .line 551578
    invoke-interface {p0, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551579
    :goto_2
    if-eqz v2, :cond_1

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 551580
    :cond_1
    if-eqz v2, :cond_2

    .line 551581
    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 551582
    :cond_2
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 551583
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_4
    move-object v0, v1

    goto :goto_2

    :cond_5
    move-object v2, v0

    goto :goto_0
.end method

.method private static a(LX/3Lb;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;LX/2RQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Lb;",
            "LX/0Or",
            "<",
            "LX/3NF;",
            ">;",
            "LX/0Or",
            "<",
            "LX/3LN;",
            ">;",
            "LX/0Or",
            "<",
            "LX/3Kk;",
            ">;",
            "LX/0Or",
            "<",
            "LX/3Ku;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FDh;",
            ">;",
            "LX/2RQ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 551415
    iput-object p1, p0, LX/3Lb;->a:LX/0Or;

    iput-object p2, p0, LX/3Lb;->b:LX/0Or;

    iput-object p3, p0, LX/3Lb;->c:LX/0Or;

    iput-object p4, p0, LX/3Lb;->d:LX/0Or;

    iput-object p5, p0, LX/3Lb;->w:LX/0Ot;

    iput-object p6, p0, LX/3Lb;->x:LX/2RQ;

    return-void
.end method

.method private static a(LX/3Lb;LX/3MZ;)V
    .locals 3

    .prologue
    .line 551416
    iget-object v0, p0, LX/3Lb;->l:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/messaging/contacts/loader/ContactsLoader$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/messaging/contacts/loader/ContactsLoader$3;-><init>(LX/3Lb;LX/3MZ;)V

    const v2, 0x5d6b88f7

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 551417
    return-void
.end method

.method private static b(LX/3Lb;Ljava/util/Map;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551418
    const-string v0, "getTopFriends"

    const v1, -0x1e6cae1d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551419
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 551420
    iget-object v1, p0, LX/3Lb;->x:LX/2RQ;

    sget-object v2, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    sget v3, LX/FDj;->a:I

    invoke-virtual {v1, v2, v3}, LX/2RQ;->a(Ljava/util/Collection;I)LX/2RR;

    move-result-object v1

    .line 551421
    iget-object v2, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v2, v1}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 551422
    const-string v2, "#fetch"

    const v3, -0x39a3eb7d

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 551423
    :try_start_1
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551424
    const v2, 0x36ba0191

    :try_start_2
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551425
    invoke-interface {v1}, LX/3On;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551426
    const v1, -0x30bbd8fe

    invoke-static {v1}, LX/02m;->a(I)V

    .line 551427
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;

    move-result-object v2

    .line 551428
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 551429
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 551430
    iget-object p1, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, p1

    .line 551431
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 551432
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 551433
    :catchall_0
    move-exception v0

    const v2, 0x3dfd79ff

    :try_start_3
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551434
    invoke-interface {v1}, LX/3On;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551435
    :catchall_1
    move-exception v0

    const v1, 0x3887b01b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 551436
    :cond_0
    iget-object v0, p0, LX/3Lb;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LN;

    .line 551437
    invoke-virtual {v0, v2}, LX/3LN;->a(LX/0Px;)V

    .line 551438
    sget-object v1, LX/DAk;->TOP:LX/DAk;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/3LN;->a(LX/DAk;LX/0Px;)V

    .line 551439
    return-object v2
.end method

.method private static b(LX/3Lb;Ljava/util/Map;I)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551440
    const-string v0, "getOnlineFriendsSortedByCoefficient"

    const v1, -0x717a547b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551441
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 551442
    iget-object v1, p0, LX/3Lb;->i:LX/2CH;

    invoke-virtual {v1}, LX/2CH;->d()Ljava/util/Collection;

    move-result-object v1

    .line 551443
    iget-object v2, p0, LX/3Lb;->x:LX/2RQ;

    invoke-virtual {v2}, LX/2RQ;->a()LX/2RR;

    move-result-object v2

    sget-object v3, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 551444
    iput-object v3, v2, LX/2RR;->b:Ljava/util/Collection;

    .line 551445
    move-object v2, v2

    .line 551446
    iput-object v1, v2, LX/2RR;->d:Ljava/util/Collection;

    .line 551447
    move-object v1, v2

    .line 551448
    sget-object v2, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    .line 551449
    iput-object v2, v1, LX/2RR;->n:LX/2RS;

    .line 551450
    move-object v1, v1

    .line 551451
    const/4 v2, 0x1

    .line 551452
    iput-boolean v2, v1, LX/2RR;->o:Z

    .line 551453
    move-object v1, v1

    .line 551454
    iput p2, v1, LX/2RR;->p:I

    .line 551455
    move-object v1, v1

    .line 551456
    iget-object v2, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v2, v1}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 551457
    const-string v2, "#fetch"

    const v3, 0x5848da09

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 551458
    :try_start_1
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551459
    const v2, -0xe0c028b

    :try_start_2
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551460
    invoke-interface {v1}, LX/3On;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551461
    const v1, -0x2f81757a

    invoke-static {v1}, LX/02m;->a(I)V

    .line 551462
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 551463
    :catchall_0
    move-exception v0

    const v2, -0x63783b20

    :try_start_3
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551464
    invoke-interface {v1}, LX/3On;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551465
    :catchall_1
    move-exception v0

    const v1, 0x1227810b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static b(LX/3Lb;Ljava/util/Map;Z)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;Z)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551466
    const-string v0, "getSmsInviteContacts"

    const v1, 0x3c614cd8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551467
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 551468
    const/4 v1, 0x0

    .line 551469
    :try_start_1
    iget-object v0, p0, LX/3Lb;->o:LX/2UR;

    invoke-virtual {v0}, LX/2UR;->a()LX/6N9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 551470
    :cond_0
    :try_start_2
    invoke-virtual {v2}, LX/0Rr;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 551471
    invoke-virtual {v2}, LX/0Rr;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 551472
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->p()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 551473
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->p()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, v6, :cond_0

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/UserPhoneNumber;

    .line 551474
    if-eqz p2, :cond_1

    .line 551475
    iget v7, v1, Lcom/facebook/user/model/UserPhoneNumber;->d:I

    move v7, v7

    .line 551476
    const/4 v8, 0x2

    if-ne v7, v8, :cond_3

    .line 551477
    :cond_1
    new-instance v7, LX/0XI;

    invoke-direct {v7}, LX/0XI;-><init>()V

    invoke-virtual {v7, v0}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    move-result-object v7

    .line 551478
    iget-object v8, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v8, v8

    .line 551479
    iget-object v9, v1, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    move-object v9, v9

    .line 551480
    invoke-virtual {v7, v8, v9}, LX/0XI;->a(Ljava/lang/String;Ljava/lang/String;)LX/0XI;

    move-result-object v7

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 551481
    iput-object v1, v7, LX/0XI;->d:Ljava/util/List;

    .line 551482
    move-object v1, v7

    .line 551483
    iget-object v7, v0, Lcom/facebook/user/model/User;->l:Ljava/lang/String;

    move-object v7, v7

    .line 551484
    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 551485
    iget-object v7, v0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v7, v7

    .line 551486
    invoke-virtual {v7}, Lcom/facebook/user/model/Name;->b()Z

    move-result v8

    if-nez v8, :cond_7

    invoke-virtual {v7}, Lcom/facebook/user/model/Name;->d()Z

    move-result v8

    if-nez v8, :cond_7

    invoke-virtual {v7}, Lcom/facebook/user/model/Name;->h()Z

    move-result v7

    if-nez v7, :cond_7

    .line 551487
    const/4 v7, 0x0

    .line 551488
    :goto_1
    move-object v7, v7

    .line 551489
    iput-object v7, v1, LX/0XI;->s:Ljava/lang/String;

    .line 551490
    :cond_2
    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 551491
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 551492
    :cond_4
    if-eqz v2, :cond_5

    .line 551493
    :try_start_3
    invoke-virtual {v2}, LX/6N9;->c()V

    .line 551494
    :cond_5
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 551495
    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;Z)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 551496
    invoke-static {v0}, LX/0R9;->b(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v0

    .line 551497
    new-instance v1, LX/FDk;

    invoke-direct {v1, p0}, LX/FDk;-><init>(LX/3Lb;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 551498
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 551499
    const v1, 0x694b4a94

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 551500
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_6

    .line 551501
    :try_start_4
    invoke-virtual {v1}, LX/6N9;->c()V

    :cond_6
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 551502
    :catchall_1
    move-exception v0

    const v1, -0xa428b13

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 551503
    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 551504
    :cond_7
    new-instance v7, LX/3hD;

    invoke-direct {v7}, LX/3hD;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v8

    .line 551505
    iput-object v8, v7, LX/3hD;->b:Ljava/lang/String;

    .line 551506
    move-object v7, v7

    .line 551507
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v8

    .line 551508
    iput-object v8, v7, LX/3hD;->a:Ljava/lang/String;

    .line 551509
    move-object v7, v7

    .line 551510
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->h()Ljava/lang/String;

    move-result-object v8

    .line 551511
    iput-object v8, v7, LX/3hD;->c:Ljava/lang/String;

    .line 551512
    move-object v7, v7

    .line 551513
    invoke-virtual {v7}, LX/3hD;->a()LX/3hE;

    move-result-object v7

    .line 551514
    iget-object v8, p0, LX/3Lb;->q:LX/3Lo;

    iget-object v9, p0, LX/3Lb;->r:LX/2Iv;

    invoke-virtual {v9}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    invoke-virtual {v8, v9, v7}, LX/3Lo;->a(Landroid/database/sqlite/SQLiteDatabase;LX/3hE;)Ljava/lang/String;

    move-result-object v7

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/3Lb;
    .locals 18

    .prologue
    .line 551515
    new-instance v1, LX/3Lb;

    invoke-static/range {p0 .. p0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v2

    check-cast v2, LX/3LP;

    invoke-static/range {p0 .. p0}, LX/3Ld;->a(LX/0QB;)LX/3Ld;

    move-result-object v3

    check-cast v3, LX/3Ld;

    invoke-static/range {p0 .. p0}, LX/2CH;->a(LX/0QB;)LX/2CH;

    move-result-object v4

    check-cast v4, LX/2CH;

    invoke-static/range {p0 .. p0}, LX/2It;->a(LX/0QB;)LX/2It;

    move-result-object v5

    check-cast v5, LX/2It;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static/range {p0 .. p0}, LX/3Le;->a(LX/0QB;)LX/3Le;

    move-result-object v8

    check-cast v8, LX/3Le;

    invoke-static/range {p0 .. p0}, LX/3Lj;->a(LX/0QB;)LX/3Lj;

    move-result-object v9

    check-cast v9, LX/3Lj;

    invoke-static/range {p0 .. p0}, LX/2UR;->a(LX/0QB;)LX/2UR;

    move-result-object v10

    check-cast v10, LX/2UR;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static/range {p0 .. p0}, LX/3Lo;->a(LX/0QB;)LX/3Lo;

    move-result-object v12

    check-cast v12, LX/3Lo;

    invoke-static/range {p0 .. p0}, LX/2Iv;->a(LX/0QB;)LX/2Iv;

    move-result-object v13

    check-cast v13, LX/2Iv;

    invoke-static/range {p0 .. p0}, LX/3LO;->a(LX/0QB;)LX/3LO;

    move-result-object v14

    check-cast v14, LX/3LO;

    invoke-static/range {p0 .. p0}, LX/3Lv;->a(LX/0QB;)LX/3Lv;

    move-result-object v15

    check-cast v15, LX/3Lv;

    invoke-static/range {p0 .. p0}, LX/3MU;->a(LX/0QB;)LX/3MU;

    move-result-object v16

    check-cast v16, LX/3MU;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v17

    check-cast v17, LX/0Uh;

    invoke-direct/range {v1 .. v17}, LX/3Lb;-><init>(LX/3LP;LX/3Ld;LX/2CH;LX/2It;LX/0TD;Ljava/util/concurrent/Executor;LX/3Le;LX/3Lj;LX/2UR;LX/03V;LX/3Lo;LX/2Iv;LX/3LO;LX/3Lv;LX/3MU;LX/0Uh;)V

    .line 551516
    const/16 v2, 0xce0

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x434

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0xd06

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x3fa

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x2710

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static/range {p0 .. p0}, LX/2RQ;->a(LX/0QB;)LX/2RQ;

    move-result-object v7

    check-cast v7, LX/2RQ;

    invoke-static/range {v1 .. v7}, LX/3Lb;->a(LX/3Lb;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;LX/2RQ;)V

    .line 551517
    return-object v1
.end method

.method private static c(LX/3Lb;Ljava/util/Map;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551518
    const-string v0, "getTopPushableFriends"

    const v1, -0x4886a22b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551519
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 551520
    iget-object v1, p0, LX/3Lb;->x:LX/2RQ;

    sget-object v2, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    sget v3, LX/FDj;->a:I

    invoke-virtual {v1, v2, v3}, LX/2RQ;->c(Ljava/util/Collection;I)LX/2RR;

    move-result-object v1

    .line 551521
    iget-object v2, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v2, v1}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 551522
    const-string v2, "#fetch"

    const v3, -0x211a10bb

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 551523
    :try_start_1
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551524
    const v2, -0x51ecdf0f

    :try_start_2
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551525
    invoke-interface {v1}, LX/3On;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551526
    const v1, -0x2183a27d

    invoke-static {v1}, LX/02m;->a(I)V

    .line 551527
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;

    move-result-object v2

    .line 551528
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 551529
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 551530
    iget-object p1, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, p1

    .line 551531
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 551532
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 551533
    :catchall_0
    move-exception v0

    const v2, -0x5f9321ac

    :try_start_3
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551534
    invoke-interface {v1}, LX/3On;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551535
    :catchall_1
    move-exception v0

    const v1, -0x48b933df

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 551536
    :cond_0
    iget-object v0, p0, LX/3Lb;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LN;

    .line 551537
    invoke-virtual {v0, v2}, LX/3LN;->a(LX/0Px;)V

    .line 551538
    sget-object v1, LX/DAk;->TOP_PUSHABLE:LX/DAk;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/3LN;->a(LX/DAk;LX/0Px;)V

    .line 551539
    return-object v2
.end method

.method private static d(LX/3Lb;Ljava/util/Map;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551540
    const-string v0, "getTopOnMessenger"

    const v1, 0x5a97d5f2

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551541
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 551542
    iget-object v1, p0, LX/3Lb;->x:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    sget-object v2, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 551543
    iput-object v2, v1, LX/2RR;->b:Ljava/util/Collection;

    .line 551544
    move-object v1, v1

    .line 551545
    const/4 v2, 0x1

    .line 551546
    iput-boolean v2, v1, LX/2RR;->f:Z

    .line 551547
    move-object v1, v1

    .line 551548
    const/4 v2, 0x1

    .line 551549
    iput-boolean v2, v1, LX/2RR;->i:Z

    .line 551550
    move-object v1, v1

    .line 551551
    invoke-virtual {v1}, LX/2RR;->i()LX/2RR;

    move-result-object v1

    sget-object v2, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    .line 551552
    iput-object v2, v1, LX/2RR;->n:LX/2RS;

    .line 551553
    move-object v1, v1

    .line 551554
    const/4 v2, 0x1

    .line 551555
    iput-boolean v2, v1, LX/2RR;->o:Z

    .line 551556
    move-object v1, v1

    .line 551557
    const/16 v2, 0xf

    .line 551558
    iput v2, v1, LX/2RR;->p:I

    .line 551559
    move-object v1, v1

    .line 551560
    iget-object v2, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v2, v1}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 551561
    const-string v2, "#fetch"

    const v3, -0x2343e2b9

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 551562
    :try_start_1
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551563
    const v2, -0x512ce67a

    :try_start_2
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551564
    invoke-interface {v1}, LX/3On;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551565
    const v1, -0x21c8143c

    invoke-static {v1}, LX/02m;->a(I)V

    .line 551566
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 551567
    :catchall_0
    move-exception v0

    const v2, -0x84c8320

    :try_start_3
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551568
    invoke-interface {v1}, LX/3On;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551569
    :catchall_1
    move-exception v0

    const v1, -0x75168226

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static e(LX/3Lb;Ljava/util/Map;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551234
    const-string v0, "getTopContacts"

    const v1, -0xa63fc1a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551235
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 551236
    iget-object v1, p0, LX/3Lb;->x:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    sget-object v2, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 551237
    iput-object v2, v1, LX/2RR;->b:Ljava/util/Collection;

    .line 551238
    move-object v1, v1

    .line 551239
    const/4 v2, 0x1

    .line 551240
    iput-boolean v2, v1, LX/2RR;->f:Z

    .line 551241
    move-object v1, v1

    .line 551242
    invoke-virtual {v1}, LX/2RR;->i()LX/2RR;

    move-result-object v1

    sget-object v2, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    .line 551243
    iput-object v2, v1, LX/2RR;->n:LX/2RS;

    .line 551244
    move-object v1, v1

    .line 551245
    const/4 v2, 0x1

    .line 551246
    iput-boolean v2, v1, LX/2RR;->o:Z

    .line 551247
    move-object v1, v1

    .line 551248
    const/16 v2, 0xf

    .line 551249
    iput v2, v1, LX/2RR;->p:I

    .line 551250
    move-object v1, v1

    .line 551251
    iget-object v2, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v2, v1}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 551252
    const-string v2, "#fetch"

    const v3, 0x29bff279

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 551253
    :try_start_1
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551254
    const v2, -0xeabdce0

    :try_start_2
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551255
    invoke-interface {v1}, LX/3On;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551256
    const v1, -0x22bd9300

    invoke-static {v1}, LX/02m;->a(I)V

    .line 551257
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;

    move-result-object v2

    .line 551258
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 551259
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 551260
    iget-object p1, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, p1

    .line 551261
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 551262
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 551263
    :catchall_0
    move-exception v0

    const v2, -0x16d917ba

    :try_start_3
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551264
    invoke-interface {v1}, LX/3On;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551265
    :catchall_1
    move-exception v0

    const v1, 0x413e115f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 551266
    :cond_0
    iget-object v0, p0, LX/3Lb;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LN;

    .line 551267
    invoke-virtual {v0, v2}, LX/3LN;->a(LX/0Px;)V

    .line 551268
    sget-object v1, LX/DAk;->TOP_CONTACT:LX/DAk;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/3LN;->a(LX/DAk;LX/0Px;)V

    .line 551269
    return-object v2
.end method

.method private static f(LX/3Lb;Ljava/util/Map;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551270
    const-string v0, "getOnMessenger"

    const v1, -0x7374ef81

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551271
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 551272
    iget-object v1, p0, LX/3Lb;->x:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    sget-object v2, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 551273
    iput-object v2, v1, LX/2RR;->b:Ljava/util/Collection;

    .line 551274
    move-object v1, v1

    .line 551275
    const/4 v2, 0x1

    .line 551276
    iput-boolean v2, v1, LX/2RR;->f:Z

    .line 551277
    move-object v1, v1

    .line 551278
    const/4 v2, 0x1

    .line 551279
    iput-boolean v2, v1, LX/2RR;->i:Z

    .line 551280
    move-object v1, v1

    .line 551281
    invoke-virtual {v1}, LX/2RR;->i()LX/2RR;

    move-result-object v1

    const/4 v2, 0x1

    .line 551282
    iput-boolean v2, v1, LX/2RR;->l:Z

    .line 551283
    move-object v1, v1

    .line 551284
    sget-object v2, LX/2RS;->NAME:LX/2RS;

    .line 551285
    iput-object v2, v1, LX/2RR;->n:LX/2RS;

    .line 551286
    move-object v1, v1

    .line 551287
    iget-object v2, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v2, v1}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 551288
    const-string v2, "#fetch"

    const v3, -0x21fef9b

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 551289
    :try_start_1
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551290
    const v2, -0x3cd6a09e

    :try_start_2
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551291
    invoke-interface {v1}, LX/3On;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551292
    const v1, -0x53bf4a08

    invoke-static {v1}, LX/02m;->a(I)V

    .line 551293
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 551294
    :catchall_0
    move-exception v0

    const v2, -0x422dda25

    :try_start_3
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551295
    invoke-interface {v1}, LX/3On;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551296
    :catchall_1
    move-exception v0

    const v1, 0x4e3b21ff    # 7.8489184E8f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static f(LX/3Lb;)LX/3MZ;
    .locals 36

    .prologue
    .line 550872
    const/4 v3, 0x0

    .line 550873
    const/4 v4, 0x0

    .line 550874
    const/16 v18, 0x0

    .line 550875
    const/16 v19, 0x0

    .line 550876
    const/16 v32, 0x0

    .line 550877
    const/16 v31, 0x0

    .line 550878
    const/4 v7, 0x0

    .line 550879
    const/16 v33, 0x0

    .line 550880
    const/4 v9, 0x0

    .line 550881
    const/4 v10, 0x0

    .line 550882
    const/4 v11, 0x0

    .line 550883
    const/4 v12, 0x0

    .line 550884
    const/4 v13, 0x0

    .line 550885
    const/4 v14, 0x0

    .line 550886
    const/4 v15, 0x0

    .line 550887
    const/16 v30, 0x0

    .line 550888
    const/16 v21, 0x0

    .line 550889
    const/16 v22, 0x0

    .line 550890
    const/16 v16, 0x0

    .line 550891
    const/16 v17, 0x0

    .line 550892
    const/16 v23, 0x0

    .line 550893
    const/16 v24, 0x0

    .line 550894
    const/16 v25, 0x0

    .line 550895
    const/16 v26, 0x0

    .line 550896
    const/16 v27, 0x0

    .line 550897
    invoke-static/range {p0 .. p0}, LX/3Lb;->g(LX/3Lb;)Z

    move-result v34

    .line 550898
    const-string v2, "loadInBackground"

    const v5, 0x458f85b2

    invoke-static {v2, v5}, LX/02m;->a(Ljava/lang/String;I)V

    .line 550899
    :try_start_0
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v35

    .line 550900
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 550901
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/3Lb;->a(LX/3Lb;Ljava/util/Map;)LX/0Px;

    move-result-object v3

    .line 550902
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Ku;

    .line 550903
    invoke-virtual {v2, v3}, LX/3Ku;->a(LX/0Px;)V

    .line 550904
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, LX/3Kk;

    move-object/from16 v29, v0

    .line 550905
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 550906
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/3Lb;->b(LX/3Lb;Ljava/util/Map;)LX/0Px;

    move-result-object v4

    .line 550907
    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, LX/3Kk;->a(LX/0Px;)V

    .line 550908
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 550909
    invoke-static/range {p0 .. p0}, LX/3Lb;->j(LX/3Lb;)LX/0Px;

    move-result-object v18

    .line 550910
    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/3Kk;->b(LX/0Px;)V

    .line 550911
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->j()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 550912
    invoke-static/range {p0 .. p0}, LX/3Lb;->m(LX/3Lb;)LX/0Px;

    move-result-object v25

    .line 550913
    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/3Kk;->c(LX/0Px;)V

    .line 550914
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->k()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 550915
    invoke-static/range {p0 .. p0}, LX/3Lb;->k(LX/3Lb;)LX/0Px;

    move-result-object v26

    .line 550916
    move-object/from16 v0, v29

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/3Kk;->d(LX/0Px;)V

    .line 550917
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->l()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 550918
    invoke-static/range {p0 .. p0}, LX/3Lb;->l(LX/3Lb;)LX/0Px;

    move-result-object v27

    .line 550919
    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/3Kk;->e(LX/0Px;)V

    .line 550920
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->m()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 550921
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/3Lb;->c(LX/3Lb;Ljava/util/Map;)LX/0Px;

    move-result-object v19

    .line 550922
    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/3Kk;->f(LX/0Px;)V

    .line 550923
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->e()Z

    move-result v2

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->f()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 550924
    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/3Lb;->d(LX/3Lb;Ljava/util/Map;)LX/0Px;

    move-result-object v9

    .line 550925
    move-object/from16 v0, v29

    invoke-virtual {v0, v9}, LX/3Kk;->k(LX/0Px;)V

    .line 550926
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->g()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 550927
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/3Lb;->h(LX/3Lb;Ljava/util/Map;)LX/0Px;

    move-result-object v10

    .line 550928
    move-object/from16 v0, v29

    invoke-virtual {v0, v10}, LX/3Kk;->j(LX/0Px;)V

    .line 550929
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->h()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 550930
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->A()I

    move-result v2

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1, v2}, LX/3Lb;->a(LX/3Lb;Ljava/util/Map;I)LX/0Px;

    move-result-object v11

    .line 550931
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->n()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 550932
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/3Lb;->e(LX/3Lb;Ljava/util/Map;)LX/0Px;

    move-result-object v12

    .line 550933
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->c()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 550934
    const/16 v2, 0xf

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1, v2}, LX/3Lb;->b(LX/3Lb;Ljava/util/Map;I)LX/0Px;

    move-result-object v7

    .line 550935
    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, LX/3Kk;->h(LX/0Px;)V

    .line 550936
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->o()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 550937
    invoke-static/range {p0 .. p0}, LX/3Lb;->n(LX/3Lb;)LX/2RR;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1, v2}, LX/3Lb;->a(LX/3Lb;Ljava/util/Map;LX/2RR;)LX/0Px;

    move-result-object v13

    .line 550938
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->p()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 550939
    invoke-static/range {p0 .. p0}, LX/3Lb;->o(LX/3Lb;)LX/2RR;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1, v2}, LX/3Lb;->a(LX/3Lb;Ljava/util/Map;LX/2RR;)LX/0Px;

    move-result-object v14

    .line 550940
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->x()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 550941
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1, v2}, LX/3Lb;->a(LX/3Lb;Ljava/util/Map;Z)LX/0Px;

    move-result-object v21

    .line 550942
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->y()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 550943
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/3Lb;->j(LX/3Lb;Ljava/util/Map;)LX/0Px;

    move-result-object v22

    .line 550944
    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/3Kk;->o(LX/0Px;)V

    .line 550945
    :cond_10
    if-nez v21, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->v()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 550946
    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1, v2}, LX/3Lb;->a(LX/3Lb;Ljava/util/Map;Z)LX/0Px;

    move-result-object v21

    .line 550947
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->r()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 550948
    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1, v2}, LX/3Lb;->b(LX/3Lb;Ljava/util/Map;Z)LX/0Px;

    move-result-object v15

    .line 550949
    move-object/from16 v0, v29

    invoke-virtual {v0, v15}, LX/3Kk;->l(LX/0Px;)V

    .line 550950
    :cond_12
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->s()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 550951
    sget-object v2, LX/3Oo;->PEOPLE_TAB:LX/3Oo;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3Lb;->a(LX/3Lb;LX/3Oo;)LX/0Px;

    move-result-object v16

    .line 550952
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->t()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 550953
    sget-object v2, LX/3Oo;->NULL_STATE:LX/3Oo;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3Lb;->a(LX/3Lb;LX/3Oo;)LX/0Px;

    move-result-object v16

    .line 550954
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->u()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 550955
    invoke-static/range {p0 .. p0}, LX/3Lb;->q(LX/3Lb;)LX/0Px;

    move-result-object v17

    .line 550956
    move-object/from16 v0, v29

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/3Kk;->m(LX/0Px;)V

    .line 550957
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->z()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 550958
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/3Lb;->i(LX/3Lb;Ljava/util/Map;)LX/0Px;

    move-result-object v23

    .line 550959
    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/3Kk;->p(LX/0Px;)V

    .line 550960
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->w()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 550961
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/3Lb;->k(LX/3Lb;Ljava/util/Map;)LX/0Px;

    move-result-object v24

    .line 550962
    :cond_17
    invoke-static {v3}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static {v4}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static {v9}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static {v7}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static {v12}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static {v10}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static {v11}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static {v13}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static {v14}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static/range {v18 .. v18}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static/range {v19 .. v19}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static {v15}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static/range {v21 .. v21}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static/range {v23 .. v23}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static/range {v22 .. v22}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static/range {v16 .. v16}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static/range {v17 .. v17}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static/range {v25 .. v25}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static/range {v26 .. v26}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static/range {v27 .. v27}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static/range {v24 .. v24}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 550963
    :cond_18
    new-instance v2, LX/3MZ;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/16 v20, 0x0

    const/16 v28, 0x1

    invoke-direct/range {v2 .. v28}, LX/3MZ;-><init>(LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/3Lb;->A:LX/3MZ;

    .line 550964
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->A:LX/3MZ;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3Lb;->a(LX/3Lb;LX/3MZ;)V

    .line 550965
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->e()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 550966
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/3Lb;->f(LX/3Lb;Ljava/util/Map;)LX/0Px;

    move-result-object v8

    .line 550967
    move-object/from16 v0, v29

    invoke-virtual {v0, v8}, LX/3Kk;->i(LX/0Px;)V

    .line 550968
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->c()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 550969
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/3Lb;->g(LX/3Lb;Ljava/util/Map;)LX/0Px;

    move-result-object v5

    .line 550970
    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, LX/3Kk;->g(LX/0Px;)V

    .line 550971
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->d()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 550972
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->A()I

    move-result v2

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1, v2}, LX/3Lb;->b(LX/3Lb;Ljava/util/Map;I)LX/0Px;

    move-result-object v6

    .line 550973
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->x()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 550974
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1, v2}, LX/3Lb;->a(LX/3Lb;Ljava/util/Map;Z)LX/0Px;

    move-result-object v21

    .line 550975
    move-object/from16 v0, v29

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/3Kk;->n(LX/0Px;)V

    .line 550976
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->y:LX/0Px;

    if-eqz v2, :cond_1c

    .line 550977
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/3Lb;->l(LX/3Lb;Ljava/util/Map;)LX/0Px;

    move-result-object v20

    .line 550978
    :goto_4
    new-instance v2, LX/3MZ;

    move/from16 v28, v34

    invoke-direct/range {v2 .. v28}, LX/3MZ;-><init>(LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 550979
    const v3, -0x2f3c24f

    invoke-static {v3}, LX/02m;->a(I)V

    .line 550980
    sget-object v3, LX/3Lb;->f:Ljava/lang/Class;

    invoke-static {v3}, LX/0PR;->a(Ljava/lang/Class;)V

    return-object v2

    .line 550981
    :cond_1b
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->q()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 550982
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1, v2}, LX/3Lb;->b(LX/3Lb;Ljava/util/Map;Z)LX/0Px;

    move-result-object v15

    .line 550983
    move-object/from16 v0, v29

    invoke-virtual {v0, v15}, LX/3Kk;->l(LX/0Px;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 550984
    :catchall_0
    move-exception v2

    const v3, 0x1cb1f57f

    invoke-static {v3}, LX/02m;->a(I)V

    .line 550985
    sget-object v3, LX/3Lb;->f:Ljava/lang/Class;

    invoke-static {v3}, LX/0PR;->a(Ljava/lang/Class;)V

    throw v2

    :cond_1c
    move-object/from16 v20, v30

    goto :goto_4

    :cond_1d
    move-object/from16 v6, v31

    goto :goto_3

    :cond_1e
    move-object/from16 v5, v32

    goto/16 :goto_2

    :cond_1f
    move-object/from16 v8, v33

    goto/16 :goto_1
.end method

.method private static g(LX/3Lb;Ljava/util/Map;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 550854
    const-string v0, "getOnlineFriends"

    const v1, 0x3c8ce8b9

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 550855
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 550856
    iget-object v1, p0, LX/3Lb;->i:LX/2CH;

    invoke-virtual {v1}, LX/2CH;->d()Ljava/util/Collection;

    move-result-object v1

    .line 550857
    iget-object v2, p0, LX/3Lb;->x:LX/2RQ;

    invoke-virtual {v2}, LX/2RQ;->a()LX/2RR;

    move-result-object v2

    sget-object v3, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 550858
    iput-object v3, v2, LX/2RR;->b:Ljava/util/Collection;

    .line 550859
    move-object v2, v2

    .line 550860
    iput-object v1, v2, LX/2RR;->d:Ljava/util/Collection;

    .line 550861
    move-object v1, v2

    .line 550862
    sget-object v2, LX/2RS;->NAME:LX/2RS;

    .line 550863
    iput-object v2, v1, LX/2RR;->n:LX/2RS;

    .line 550864
    move-object v1, v1

    .line 550865
    iget-object v2, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v2, v1}, LX/3LP;->a(LX/2RR;)LX/3On;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 550866
    :try_start_1
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 550867
    :try_start_2
    invoke-interface {v1}, LX/3On;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 550868
    const v1, -0x4620c797

    invoke-static {v1}, LX/02m;->a(I)V

    .line 550869
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 550870
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v1}, LX/3On;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 550871
    :catchall_1
    move-exception v0

    const v1, 0x20a1ba2e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static g(LX/3Lb;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 550986
    iget-object v2, p0, LX/3Lb;->j:LX/2It;

    sget-object v3, LX/3Fg;->a:LX/3OK;

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v4, v5}, LX/2Iu;->a(LX/0To;J)J

    move-result-wide v2

    .line 550987
    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 550988
    if-eqz v1, :cond_1

    .line 550989
    :cond_0
    :goto_1
    return v0

    .line 550990
    :cond_1
    iget-object v2, p0, LX/3Lb;->z:LX/3La;

    invoke-virtual {v2}, LX/3La;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/3Lb;->j:LX/2It;

    sget-object v3, LX/3Fg;->e:LX/3OK;

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v4, v5}, LX/2Iu;->a(LX/0To;J)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    const/4 v2, 0x1

    :goto_2
    move v1, v2

    .line 550991
    if-nez v1, :cond_0

    .line 550992
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method private static h(LX/3Lb;Ljava/util/Map;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 550993
    const-string v0, "getNotOnMessengerFriends"

    const v1, -0x3a2937c9

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 550994
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 550995
    iget-object v1, p0, LX/3Lb;->x:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    sget-object v2, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 550996
    iput-object v2, v1, LX/2RR;->b:Ljava/util/Collection;

    .line 550997
    move-object v1, v1

    .line 550998
    const/4 v2, 0x1

    .line 550999
    iput-boolean v2, v1, LX/2RR;->f:Z

    .line 551000
    move-object v1, v1

    .line 551001
    const/4 v2, 0x1

    .line 551002
    iput-boolean v2, v1, LX/2RR;->h:Z

    .line 551003
    move-object v1, v1

    .line 551004
    invoke-virtual {v1}, LX/2RR;->i()LX/2RR;

    move-result-object v1

    sget-object v2, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    .line 551005
    iput-object v2, v1, LX/2RR;->n:LX/2RS;

    .line 551006
    move-object v1, v1

    .line 551007
    const/4 v2, 0x1

    .line 551008
    iput-boolean v2, v1, LX/2RR;->o:Z

    .line 551009
    move-object v1, v1

    .line 551010
    iget-object v2, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v2, v1}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 551011
    iget-object v2, p0, LX/3Lb;->x:LX/2RQ;

    invoke-virtual {v2}, LX/2RQ;->a()LX/2RR;

    move-result-object v2

    sget-object v3, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 551012
    iput-object v3, v2, LX/2RR;->b:Ljava/util/Collection;

    .line 551013
    move-object v2, v2

    .line 551014
    const/4 v3, 0x1

    .line 551015
    iput-boolean v3, v2, LX/2RR;->f:Z

    .line 551016
    move-object v2, v2

    .line 551017
    const/4 v3, 0x1

    .line 551018
    iput-boolean v3, v2, LX/2RR;->h:Z

    .line 551019
    move-object v2, v2

    .line 551020
    invoke-virtual {v2}, LX/2RR;->i()LX/2RR;

    move-result-object v2

    sget-object v3, LX/2RS;->NAME:LX/2RS;

    .line 551021
    iput-object v3, v2, LX/2RR;->n:LX/2RS;

    .line 551022
    move-object v2, v2

    .line 551023
    const/4 v3, 0x1

    .line 551024
    iput-boolean v3, v2, LX/2RR;->k:Z

    .line 551025
    move-object v2, v2

    .line 551026
    iget-object v3, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v3, v2}, LX/3LP;->a(LX/2RR;)LX/3On;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 551027
    :try_start_1
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;

    .line 551028
    invoke-virtual {v0, v2}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551029
    :try_start_2
    invoke-interface {v1}, LX/3On;->close()V

    .line 551030
    invoke-interface {v2}, LX/3On;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551031
    const v1, 0x4611b46b

    invoke-static {v1}, LX/02m;->a(I)V

    .line 551032
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 551033
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v1}, LX/3On;->close()V

    .line 551034
    invoke-interface {v2}, LX/3On;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551035
    :catchall_1
    move-exception v0

    const v1, -0x67f0e06d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static i(LX/3Lb;Ljava/util/Map;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x5

    .line 551036
    const-string v0, "getPages"

    const v1, 0x1ac1dd19

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551037
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 551038
    :try_start_0
    iget-object v0, p0, LX/3Lb;->u:LX/3MU;

    .line 551039
    iget-object v4, v0, LX/3MU;->b:LX/0Sh;

    invoke-virtual {v4}, LX/0Sh;->b()V

    .line 551040
    new-instance v4, LX/FD5;

    invoke-direct {v4}, LX/FD5;-><init>()V

    move-object v4, v4

    .line 551041
    const-string v5, "max_pages_to_fetch"

    const/16 v6, 0x1e

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 551042
    iget-object v5, v0, LX/3MU;->d:LX/3MW;

    invoke-virtual {v5, v4}, LX/3MW;->a(LX/0gW;)V

    .line 551043
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    const-wide/16 v6, 0x258

    invoke-virtual {v4, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    .line 551044
    iget-object v5, v0, LX/3MU;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    const v5, 0x37bfb395

    invoke-static {v4, v5}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 551045
    iget-object v5, v4, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v5

    .line 551046
    check-cast v4, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessUserHasMessagedQueryModel;

    .line 551047
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessUserHasMessagedQueryModel;->a()Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessUserHasMessagedQueryModel$BusinessUserHasMessagedModel;

    move-result-object v5

    if-nez v5, :cond_3

    .line 551048
    :cond_0
    const/4 v4, 0x0

    .line 551049
    :goto_0
    move-object v0, v4

    .line 551050
    if-eqz v0, :cond_2

    .line 551051
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    if-le v2, v3, :cond_1

    .line 551052
    const/4 v2, 0x0

    const/4 v3, 0x5

    invoke-virtual {v0, v2, v3}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    .line 551053
    :cond_1
    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 551054
    :cond_2
    :goto_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 551055
    invoke-static {p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 551056
    :catch_0
    const v0, 0x5676459a

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    :cond_3
    invoke-virtual {v4}, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessUserHasMessagedQueryModel;->a()Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessUserHasMessagedQueryModel$BusinessUserHasMessagedModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessUserHasMessagedQueryModel$BusinessUserHasMessagedModel;->a()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/3MU;->a(LX/0Px;)LX/0Px;

    move-result-object v4

    goto :goto_0
.end method

.method private static j(LX/3Lb;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551057
    iget-object v0, p0, LX/3Lb;->m:LX/3Le;

    .line 551058
    const/4 v1, 0x5

    invoke-static {v0, v1}, LX/3Le;->c(LX/3Le;I)LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/3Le;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 551059
    const-string v0, "getRecentPeerToPeerCalls"

    const v2, 0x7d4a4105

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551060
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 551061
    iget-object v2, p0, LX/3Lb;->x:LX/2RQ;

    invoke-virtual {v2}, LX/2RQ;->a()LX/2RR;

    move-result-object v2

    .line 551062
    iput-object v1, v2, LX/2RR;->d:Ljava/util/Collection;

    .line 551063
    move-object v2, v2

    .line 551064
    iget-object v3, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v3, v2}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v2

    .line 551065
    const-string v3, "#fetch"

    const v4, -0x6c819d67

    invoke-static {v3, v4}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 551066
    :try_start_1
    invoke-virtual {v0, v2}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551067
    const v3, -0x59b390a1

    :try_start_2
    invoke-static {v3}, LX/02m;->a(I)V

    .line 551068
    invoke-interface {v2}, LX/3On;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551069
    const v2, -0xdd8185b

    invoke-static {v2}, LX/02m;->a(I)V

    .line 551070
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 551071
    iget-object v0, p0, LX/3Lb;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LN;

    .line 551072
    invoke-virtual {v0, v2}, LX/3LN;->a(LX/0Px;)V

    .line 551073
    sget-object v3, LX/DAk;->RECENT_CALLS:LX/DAk;

    invoke-virtual {v0, v3, v1}, LX/3LN;->a(LX/DAk;LX/0Px;)V

    .line 551074
    return-object v2

    .line 551075
    :catchall_0
    move-exception v0

    const v1, 0x5c31dba2

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    .line 551076
    invoke-interface {v2}, LX/3On;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551077
    :catchall_1
    move-exception v0

    const v1, 0x7604f532

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static j(LX/3Lb;Ljava/util/Map;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 551078
    const-string v0, "getPromotionalContacts"

    const v1, -0x4cfeab2

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551079
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 551080
    :try_start_0
    iget-object v0, p0, LX/3Lb;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3NF;

    .line 551081
    invoke-virtual {v0}, LX/3NF;->a()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 551082
    if-eqz v0, :cond_0

    .line 551083
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 551084
    :cond_0
    :goto_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 551085
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 551086
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 551087
    iget-object v5, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v5

    .line 551088
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 551089
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 551090
    :catch_0
    const v0, 0x48935e07

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 551091
    :cond_1
    iget-object v0, p0, LX/3Lb;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LN;

    .line 551092
    invoke-virtual {v0, v2}, LX/3LN;->a(LX/0Px;)V

    .line 551093
    sget-object v1, LX/DAk;->PROMOTION:LX/DAk;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/3LN;->a(LX/DAk;LX/0Px;)V

    .line 551094
    invoke-static {p1, v2}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static k(LX/3Lb;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551095
    iget-object v0, p0, LX/3Lb;->m:LX/3Le;

    const/16 v1, 0x64

    const/4 v5, 0x1

    .line 551096
    new-array v2, v5, [I

    const/4 v3, 0x0

    const/4 v4, 0x3

    aput v4, v2, v3

    invoke-static {v0, v1, v2, v5}, LX/3Le;->a(LX/3Le;I[IZ)LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 551097
    iget-object v0, p0, LX/3Lb;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LN;

    .line 551098
    sget-object v2, LX/DAk;->RTC_ONGOING_GROUP_CALLS:LX/DAk;

    invoke-virtual {v0, v2, v1}, LX/3LN;->a(LX/DAk;LX/0Px;)V

    .line 551099
    return-object v1
.end method

.method private static k(LX/3Lb;Ljava/util/Map;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 551100
    iget-object v0, p0, LX/3Lb;->v:LX/0Uh;

    const/16 v1, 0x5bf

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 551101
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 551102
    :goto_0
    return-object v0

    .line 551103
    :cond_0
    const-string v0, "getPSTNCallLogContacts"

    const v1, -0x3d50b37c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551104
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 551105
    iget-object v0, p0, LX/3Lb;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 551106
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 551107
    goto :goto_0

    .line 551108
    :cond_1
    iget-object v0, p0, LX/3Lb;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDh;

    .line 551109
    iget-object v2, v0, LX/FDh;->j:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 551110
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 551111
    :goto_1
    move-object v2, v2

    .line 551112
    :try_start_0
    iget-object v0, p0, LX/3Lb;->x:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    .line 551113
    iput-object v2, v0, LX/2RR;->d:Ljava/util/Collection;

    .line 551114
    move-object v0, v0

    .line 551115
    iget-object v3, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v3, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v0

    .line 551116
    const-string v3, "#fetch"

    const v4, 0x37ec442a

    invoke-static {v3, v4}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 551117
    :try_start_1
    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551118
    const v3, -0x20eb7d37

    :try_start_2
    invoke-static {v3}, LX/02m;->a(I)V

    .line 551119
    invoke-interface {v0}, LX/3On;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551120
    const v0, 0xd72087c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 551121
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 551122
    iget-object v0, p0, LX/3Lb;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LN;

    .line 551123
    invoke-virtual {v0, v1}, LX/3LN;->a(LX/0Px;)V

    .line 551124
    sget-object v3, LX/DAk;->PHONE_CALLLOGS:LX/DAk;

    invoke-virtual {v0, v3, v2}, LX/3LN;->a(LX/DAk;LX/0Px;)V

    .line 551125
    invoke-static {p1, v1}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 551126
    :catchall_0
    move-exception v1

    const v2, -0x23cdb52e

    :try_start_3
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551127
    invoke-interface {v0}, LX/3On;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551128
    :catchall_1
    move-exception v0

    const v1, -0x6043a2a9

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 551129
    :cond_2
    iget-object v2, v0, LX/FDh;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/EGK;->j:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 551130
    if-eqz v2, :cond_3

    .line 551131
    const/4 v4, 0x0

    .line 551132
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 551133
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 551134
    :goto_2
    move-object v2, v3

    .line 551135
    goto :goto_1

    .line 551136
    :cond_3
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 551137
    goto :goto_1

    .line 551138
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 551139
    :try_start_4
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    move v3, v4

    :goto_3
    if-ge v3, v7, :cond_5

    aget-object v0, v6, v3

    .line 551140
    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->a(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_0

    .line 551141
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 551142
    :catch_0
    move-exception v3

    .line 551143
    sget-object v5, LX/FDh;->a:Ljava/lang/String;

    const-string v6, "User keys are not parsable for call log matched users. String: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v4

    invoke-static {v5, v3, v6, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 551144
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 551145
    goto :goto_2

    .line 551146
    :cond_5
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    goto :goto_2
.end method

.method private static l(LX/3Lb;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcVoicemailInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551147
    iget-object v0, p0, LX/3Lb;->n:LX/3Lj;

    .line 551148
    invoke-static {v0}, LX/3Lj;->e(LX/3Lj;)LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 551149
    iget-object v0, p0, LX/3Lb;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LN;

    .line 551150
    sget-object v2, LX/DAk;->RTC_VOICEMAILS:LX/DAk;

    invoke-virtual {v0, v2, v1}, LX/3LN;->a(LX/DAk;LX/0Px;)V

    .line 551151
    return-object v1
.end method

.method private static l(LX/3Lb;Ljava/util/Map;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551152
    const-string v0, "getSpecifiedContacts"

    const v1, 0x1920a50

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551153
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 551154
    iget-object v1, p0, LX/3Lb;->x:LX/2RQ;

    iget-object v2, p0, LX/3Lb;->y:LX/0Px;

    invoke-virtual {v1, v2}, LX/2RQ;->b(Ljava/util/Collection;)LX/2RR;

    move-result-object v1

    .line 551155
    iget-object v2, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v2, v1}, LX/3LP;->a(LX/2RR;)LX/3On;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 551156
    :try_start_1
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551157
    :try_start_2
    invoke-interface {v1}, LX/3On;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551158
    const v1, -0x2fcf3cca

    invoke-static {v1}, LX/02m;->a(I)V

    .line 551159
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/3Lb;->a(Ljava/util/Map;Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 551160
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v1}, LX/3On;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551161
    :catchall_1
    move-exception v0

    const v1, 0x11d9f1b8

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static m(LX/3Lb;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551162
    iget-object v0, p0, LX/3Lb;->m:LX/3Le;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, LX/3Le;->a(I)LX/0Px;

    move-result-object v1

    .line 551163
    const/4 v0, 0x0

    .line 551164
    if-eqz v1, :cond_0

    .line 551165
    invoke-static {v1}, LX/3Le;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 551166
    :cond_0
    const-string v2, "getCallLogs"

    const v3, -0x41d328a9

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 551167
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 551168
    iget-object v3, p0, LX/3Lb;->x:LX/2RQ;

    invoke-virtual {v3}, LX/2RQ;->a()LX/2RR;

    move-result-object v3

    .line 551169
    iput-object v0, v3, LX/2RR;->d:Ljava/util/Collection;

    .line 551170
    move-object v0, v3

    .line 551171
    iget-object v3, p0, LX/3Lb;->g:LX/3LP;

    invoke-virtual {v3, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v0

    .line 551172
    const-string v3, "#fetch"

    const v4, 0x3db14b1f

    invoke-static {v3, v4}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 551173
    :try_start_1
    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551174
    const v3, 0x27ff0422

    :try_start_2
    invoke-static {v3}, LX/02m;->a(I)V

    .line 551175
    invoke-interface {v0}, LX/3On;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551176
    const v0, -0x12307c2e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 551177
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 551178
    iget-object v0, p0, LX/3Lb;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LN;

    .line 551179
    invoke-virtual {v0, v2}, LX/3LN;->a(LX/0Px;)V

    .line 551180
    sget-object v2, LX/DAk;->RTC_CALLLOGS:LX/DAk;

    invoke-virtual {v0, v2, v1}, LX/3LN;->b(LX/DAk;LX/0Px;)V

    .line 551181
    return-object v1

    .line 551182
    :catchall_0
    move-exception v1

    const v2, 0x16e6c0ab

    :try_start_3
    invoke-static {v2}, LX/02m;->a(I)V

    .line 551183
    invoke-interface {v0}, LX/3On;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 551184
    :catchall_1
    move-exception v0

    const v1, 0xe74174d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static n(LX/3Lb;)LX/2RR;
    .locals 2

    .prologue
    .line 551185
    invoke-static {p0}, LX/3Lb;->p(LX/3Lb;)LX/2RR;

    move-result-object v0

    const/4 v1, 0x1

    .line 551186
    iput-boolean v1, v0, LX/2RR;->o:Z

    .line 551187
    move-object v0, v0

    .line 551188
    sget-object v1, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    .line 551189
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 551190
    move-object v0, v0

    .line 551191
    return-object v0
.end method

.method private static o(LX/3Lb;)LX/2RR;
    .locals 2

    .prologue
    .line 551192
    invoke-static {p0}, LX/3Lb;->p(LX/3Lb;)LX/2RR;

    move-result-object v0

    const/4 v1, 0x1

    .line 551193
    iput-boolean v1, v0, LX/2RR;->l:Z

    .line 551194
    move-object v0, v0

    .line 551195
    const/4 v1, 0x0

    .line 551196
    iput-boolean v1, v0, LX/2RR;->o:Z

    .line 551197
    move-object v0, v0

    .line 551198
    sget-object v1, LX/2RS;->NAME:LX/2RS;

    .line 551199
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 551200
    move-object v0, v0

    .line 551201
    return-object v0
.end method

.method private static p(LX/3Lb;)LX/2RR;
    .locals 3

    .prologue
    .line 551202
    iget-object v0, p0, LX/3Lb;->s:LX/3LO;

    const/16 v1, 0x64

    const/16 v2, 0x12c

    invoke-virtual {v0, v1, v2}, LX/3LO;->a(II)I

    move-result v0

    .line 551203
    iget-object v1, p0, LX/3Lb;->x:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    sget-object v2, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 551204
    iput-object v2, v1, LX/2RR;->b:Ljava/util/Collection;

    .line 551205
    move-object v1, v1

    .line 551206
    iput v0, v1, LX/2RR;->p:I

    .line 551207
    move-object v0, v1

    .line 551208
    const/4 v1, 0x1

    .line 551209
    iput-boolean v1, v0, LX/2RR;->f:Z

    .line 551210
    move-object v0, v0

    .line 551211
    invoke-virtual {v0}, LX/2RR;->i()LX/2RR;

    move-result-object v0

    sget-object v1, LX/3Oq;->FRIEND:LX/3Oq;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 551212
    iput-object v1, v0, LX/2RR;->c:Ljava/util/Collection;

    .line 551213
    move-object v0, v0

    .line 551214
    return-object v0
.end method

.method private static q(LX/3Lb;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 550846
    const-string v0, "getPhoneContacts"

    const v1, 0x4406876a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 550847
    :try_start_0
    iget-object v0, p0, LX/3Lb;->t:LX/3Lv;

    sget-object v1, LX/3Oo;->PEOPLE_TAB:LX/3Oo;

    .line 550848
    invoke-static {v0}, LX/3Lv;->a(LX/3Lv;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 550849
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 550850
    :goto_0
    move-object v0, v2

    .line 550851
    invoke-virtual {v0}, LX/0Px;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 550852
    const v1, -0x3fbeae70

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x50926ec

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 550853
    :cond_0
    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, LX/3Lv;->d:LX/0ux;

    const/16 v5, 0x7d0

    const-string v6, "contact_id"

    const/4 v7, 0x0

    move-object v2, v0

    move-object v8, v1

    invoke-static/range {v2 .. v8}, LX/3Lv;->b(LX/3Lv;Landroid/net/Uri;LX/0ux;ILjava/lang/String;ZLX/3Oo;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 551215
    iget-object v0, p0, LX/3Lb;->z:LX/3La;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 551216
    iget-object v0, p0, LX/3Lb;->A:LX/3MZ;

    if-eqz v0, :cond_0

    .line 551217
    iget-object v0, p0, LX/3Lb;->B:LX/3Mb;

    if-eqz v0, :cond_0

    .line 551218
    iget-object v0, p0, LX/3Lb;->B:LX/3Mb;

    const/4 v1, 0x0

    iget-object v2, p0, LX/3Lb;->A:LX/3MZ;

    invoke-interface {v0, v1, v2}, LX/3Mb;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 551219
    :cond_0
    iget-object v0, p0, LX/3Lb;->e:LX/1Mv;

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 551220
    if-eqz v0, :cond_2

    .line 551221
    iget-object v0, p0, LX/3Lb;->k:LX/0TD;

    new-instance v1, LX/3OX;

    invoke-direct {v1, p0}, LX/3OX;-><init>(LX/3Lb;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 551222
    iget-object v1, p0, LX/3Lb;->B:LX/3Mb;

    if-eqz v1, :cond_1

    .line 551223
    iget-object v1, p0, LX/3Lb;->B:LX/3Mb;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v0}, LX/3Mb;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 551224
    :cond_1
    new-instance v1, LX/3OY;

    invoke-direct {v1, p0}, LX/3OY;-><init>(LX/3Lb;)V

    .line 551225
    iget-object v2, p0, LX/3Lb;->l:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 551226
    invoke-static {v0, v1}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v0

    iput-object v0, p0, LX/3Lb;->e:LX/1Mv;

    .line 551227
    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 551228
    iget-object v0, p0, LX/3Lb;->e:LX/1Mv;

    if-eqz v0, :cond_0

    .line 551229
    iget-object v0, p0, LX/3Lb;->e:LX/1Mv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 551230
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Lb;->e:LX/1Mv;

    .line 551231
    :cond_0
    iget-object v0, p0, LX/3Lb;->A:LX/3MZ;

    iget-boolean v0, v0, LX/3MZ;->A:Z

    if-eqz v0, :cond_1

    .line 551232
    sget-object v0, LX/3MZ;->a:LX/3MZ;

    iput-object v0, p0, LX/3Lb;->A:LX/3MZ;

    .line 551233
    :cond_1
    return-void
.end method
