.class public final enum LX/397;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/397;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/397;

.field public static final enum ALBUM:LX/397;

.field public static final enum LINK:LX/397;

.field public static final enum PHOTO:LX/397;

.field public static final enum VIDEO:LX/397;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 522293
    new-instance v0, LX/397;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v2}, LX/397;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/397;->PHOTO:LX/397;

    .line 522294
    new-instance v0, LX/397;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/397;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/397;->VIDEO:LX/397;

    .line 522295
    new-instance v0, LX/397;

    const-string v1, "LINK"

    invoke-direct {v0, v1, v4}, LX/397;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/397;->LINK:LX/397;

    .line 522296
    new-instance v0, LX/397;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v5}, LX/397;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/397;->ALBUM:LX/397;

    .line 522297
    const/4 v0, 0x4

    new-array v0, v0, [LX/397;

    sget-object v1, LX/397;->PHOTO:LX/397;

    aput-object v1, v0, v2

    sget-object v1, LX/397;->VIDEO:LX/397;

    aput-object v1, v0, v3

    sget-object v1, LX/397;->LINK:LX/397;

    aput-object v1, v0, v4

    sget-object v1, LX/397;->ALBUM:LX/397;

    aput-object v1, v0, v5

    sput-object v0, LX/397;->$VALUES:[LX/397;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 522298
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/397;
    .locals 1

    .prologue
    .line 522299
    const-class v0, LX/397;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/397;

    return-object v0
.end method

.method public static values()[LX/397;
    .locals 1

    .prologue
    .line 522300
    sget-object v0, LX/397;->$VALUES:[LX/397;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/397;

    return-object v0
.end method
