.class public LX/2es;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 445841
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1Fa;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 445842
    invoke-static {p0}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    .line 445843
    if-nez v1, :cond_1

    .line 445844
    :cond_0
    :goto_0
    return-object v0

    .line 445845
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 445846
    if-eqz v1, :cond_0

    .line 445847
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/1Fa;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 445848
    invoke-interface {p0}, LX/1Fa;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 445849
    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(LX/1Fa;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 445850
    invoke-static {p0}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    .line 445851
    if-nez v0, :cond_0

    .line 445852
    const/4 v0, 0x0

    .line 445853
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
