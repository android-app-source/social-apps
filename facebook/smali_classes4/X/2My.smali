.class public LX/2My;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile s:LX/2My;


# instance fields
.field public a:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ae;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6eW;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/01T;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final g:LX/2Mz;

.field public final h:LX/2Mz;

.field private final i:LX/2Mz;

.field private j:LX/03R;

.field private k:LX/03R;

.field private l:LX/03R;

.field private m:LX/03R;

.field private n:LX/03R;

.field private o:LX/03R;

.field private p:LX/03R;

.field private q:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 10
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 397958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397959
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 397960
    iput-object v0, p0, LX/2My;->b:LX/0Ot;

    .line 397961
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 397962
    iput-object v0, p0, LX/2My;->c:LX/0Ot;

    .line 397963
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 397964
    iput-object v0, p0, LX/2My;->d:LX/0Ot;

    .line 397965
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 397966
    iput-object v0, p0, LX/2My;->e:LX/0Ot;

    .line 397967
    new-instance v0, LX/2Mz;

    sget-wide v2, LX/0X5;->fP:J

    sget-wide v4, LX/0X5;->fS:J

    sget-short v6, LX/2N0;->y:S

    sget-short v7, LX/2N0;->K:S

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, LX/2Mz;-><init>(LX/2My;JJSSZB)V

    iput-object v0, p0, LX/2My;->g:LX/2Mz;

    .line 397968
    new-instance v0, LX/2Mz;

    sget-wide v2, LX/0X5;->fR:J

    sget-wide v4, LX/0X5;->fU:J

    sget-short v6, LX/2N0;->x:S

    sget-short v7, LX/2N0;->J:S

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, LX/2Mz;-><init>(LX/2My;JJSSZB)V

    iput-object v0, p0, LX/2My;->h:LX/2Mz;

    .line 397969
    new-instance v0, LX/2Mz;

    sget-wide v2, LX/0X5;->fQ:J

    sget-wide v4, LX/0X5;->fT:J

    sget-short v6, LX/2N0;->w:S

    sget-short v7, LX/2N0;->I:S

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, LX/2Mz;-><init>(LX/2My;JJSSZB)V

    iput-object v0, p0, LX/2My;->i:LX/2Mz;

    .line 397970
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->j:LX/03R;

    .line 397971
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->k:LX/03R;

    .line 397972
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->l:LX/03R;

    .line 397973
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->m:LX/03R;

    .line 397974
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->n:LX/03R;

    .line 397975
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->o:LX/03R;

    .line 397976
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->p:LX/03R;

    .line 397977
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2My;->q:LX/0am;

    .line 397978
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2My;->r:LX/0am;

    .line 397979
    return-void
.end method

.method public static a(LX/0QB;)LX/2My;
    .locals 9

    .prologue
    .line 397943
    sget-object v0, LX/2My;->s:LX/2My;

    if-nez v0, :cond_1

    .line 397944
    const-class v1, LX/2My;

    monitor-enter v1

    .line 397945
    :try_start_0
    sget-object v0, LX/2My;->s:LX/2My;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 397946
    if-eqz v2, :cond_0

    .line 397947
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 397948
    new-instance v3, LX/2My;

    invoke-direct {v3}, LX/2My;-><init>()V

    .line 397949
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    const/16 v5, 0x1032

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1032

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xdf4

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x27e7

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object p0

    check-cast p0, LX/01T;

    .line 397950
    iput-object v4, v3, LX/2My;->a:LX/0Uh;

    iput-object v5, v3, LX/2My;->b:LX/0Ot;

    iput-object v6, v3, LX/2My;->c:LX/0Ot;

    iput-object v7, v3, LX/2My;->d:LX/0Ot;

    iput-object v8, v3, LX/2My;->e:LX/0Ot;

    iput-object p0, v3, LX/2My;->f:LX/01T;

    .line 397951
    move-object v0, v3

    .line 397952
    sput-object v0, LX/2My;->s:LX/2My;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397953
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 397954
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 397955
    :cond_1
    sget-object v0, LX/2My;->s:LX/2My;

    return-object v0

    .line 397956
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 397957
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 397980
    iget-object v0, p0, LX/2My;->f:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/2My;->g:LX/2Mz;

    invoke-virtual {v0}, LX/2Mz;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 397942
    iget-object v0, p0, LX/2My;->a:LX/0Uh;

    const/16 v1, 0x200

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 397929
    iget-object v0, p0, LX/2My;->g:LX/2Mz;

    invoke-virtual {v0}, LX/2Mz;->a()V

    .line 397930
    iget-object v0, p0, LX/2My;->h:LX/2Mz;

    invoke-virtual {v0}, LX/2Mz;->a()V

    .line 397931
    iget-object v0, p0, LX/2My;->i:LX/2Mz;

    invoke-virtual {v0}, LX/2Mz;->a()V

    .line 397932
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->j:LX/03R;

    .line 397933
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->k:LX/03R;

    .line 397934
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->l:LX/03R;

    .line 397935
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->m:LX/03R;

    .line 397936
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->n:LX/03R;

    .line 397937
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->o:LX/03R;

    .line 397938
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2My;->p:LX/03R;

    .line 397939
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2My;->q:LX/0am;

    .line 397940
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2My;->r:LX/0am;

    .line 397941
    return-void
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 397927
    invoke-virtual {p0}, LX/2My;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/2My;->h:LX/2Mz;

    invoke-virtual {v1}, LX/2Mz;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 397928
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2My;->a:LX/0Uh;

    const/16 v2, 0x201

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 4

    .prologue
    .line 397924
    iget-object v0, p0, LX/2My;->p:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 397925
    iget-object v0, p0, LX/2My;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->fV:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/2My;->p:LX/03R;

    .line 397926
    :cond_0
    iget-object v0, p0, LX/2My;->p:LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method
