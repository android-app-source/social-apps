.class public final LX/2pL;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2pH;


# direct methods
.method public constructor <init>(LX/2pH;)V
    .locals 0

    .prologue
    .line 468019
    iput-object p1, p0, LX/2pL;->a:LX/2pH;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 468011
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 468012
    check-cast p1, LX/2ou;

    .line 468013
    iget-object v0, p0, LX/2pL;->a:LX/2pH;

    iget-object v1, p1, LX/2ou;->b:LX/2qV;

    .line 468014
    sget-object v2, LX/2qV;->PLAYING:LX/2qV;

    if-ne v2, v1, :cond_1

    .line 468015
    iget-object v2, v0, LX/2pH;->b:LX/2pI;

    iget-object v3, v0, LX/2pH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 p0, 0x64

    invoke-static {v0}, LX/2pH;->getInvisibleConst(LX/2pH;)I

    move-result p1

    invoke-virtual {v2, v3, p0, p1}, LX/2pI;->a(Landroid/view/View;II)V

    .line 468016
    :cond_0
    :goto_0
    return-void

    .line 468017
    :cond_1
    iget-boolean v2, v0, LX/2pH;->p:Z

    if-eqz v2, :cond_0

    sget-object v2, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v2, v1, :cond_0

    .line 468018
    const/4 v2, 0x1

    invoke-static {v0, v2}, LX/2pH;->setCoverImageVisible(LX/2pH;Z)V

    goto :goto_0
.end method
