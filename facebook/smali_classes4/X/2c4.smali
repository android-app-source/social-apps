.class public LX/2c4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/2c4;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/app/NotificationManager;

.field private final c:LX/2Yg;

.field private final d:LX/2aN;

.field public final e:LX/0hy;

.field public final f:LX/17Y;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Q7;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final i:LX/1rx;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/NotificationManager;LX/2Yg;LX/2aN;LX/0hy;LX/17Y;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1rx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/app/NotificationManager;",
            "LX/2Yg;",
            "LX/2aN;",
            "LX/0hy;",
            "LX/17Y;",
            "LX/0Ot",
            "<",
            "LX/3Q7;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/1rx;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 440168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440169
    iput-object p1, p0, LX/2c4;->a:Landroid/content/Context;

    .line 440170
    iput-object p2, p0, LX/2c4;->b:Landroid/app/NotificationManager;

    .line 440171
    iput-object p3, p0, LX/2c4;->c:LX/2Yg;

    .line 440172
    iput-object p4, p0, LX/2c4;->d:LX/2aN;

    .line 440173
    iput-object p5, p0, LX/2c4;->e:LX/0hy;

    .line 440174
    iput-object p6, p0, LX/2c4;->f:LX/17Y;

    .line 440175
    iput-object p7, p0, LX/2c4;->g:LX/0Ot;

    .line 440176
    iput-object p8, p0, LX/2c4;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 440177
    iput-object p9, p0, LX/2c4;->i:LX/1rx;

    .line 440178
    return-void
.end method

.method public static a(LX/0QB;)LX/2c4;
    .locals 13

    .prologue
    .line 440155
    sget-object v0, LX/2c4;->j:LX/2c4;

    if-nez v0, :cond_1

    .line 440156
    const-class v1, LX/2c4;

    monitor-enter v1

    .line 440157
    :try_start_0
    sget-object v0, LX/2c4;->j:LX/2c4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 440158
    if-eqz v2, :cond_0

    .line 440159
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 440160
    new-instance v3, LX/2c4;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    invoke-static {v0}, LX/2Yg;->a(LX/0QB;)LX/2Yg;

    move-result-object v6

    check-cast v6, LX/2Yg;

    invoke-static {v0}, LX/2aN;->b(LX/0QB;)LX/2aN;

    move-result-object v7

    check-cast v7, LX/2aN;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v8

    check-cast v8, LX/0hy;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v9

    check-cast v9, LX/17Y;

    const/16 v10, 0xe43

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/1rx;->b(LX/0QB;)LX/1rx;

    move-result-object v12

    check-cast v12, LX/1rx;

    invoke-direct/range {v3 .. v12}, LX/2c4;-><init>(Landroid/content/Context;Landroid/app/NotificationManager;LX/2Yg;LX/2aN;LX/0hy;LX/17Y;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1rx;)V

    .line 440161
    move-object v0, v3

    .line 440162
    sput-object v0, LX/2c4;->j:LX/2c4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 440163
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 440164
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 440165
    :cond_1
    sget-object v0, LX/2c4;->j:LX/2c4;

    return-object v0

    .line 440166
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 440167
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2c4;Lcom/facebook/notifications/constants/NotificationType;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/constants/NotificationType;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 440151
    invoke-static {p1}, LX/2c4;->d(Lcom/facebook/notifications/constants/NotificationType;)LX/0Tn;

    move-result-object v0

    .line 440152
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p2}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 440153
    iget-object v2, p0, LX/2c4;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 440154
    return-void
.end method

.method private b(Lcom/facebook/notifications/constants/NotificationType;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 440147
    invoke-static {p0, p1}, LX/2c4;->c(LX/2c4;Lcom/facebook/notifications/constants/NotificationType;)Ljava/util/List;

    move-result-object v0

    .line 440148
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 440149
    invoke-static {p0, p1, v0}, LX/2c4;->a(LX/2c4;Lcom/facebook/notifications/constants/NotificationType;Ljava/util/List;)V

    .line 440150
    return-void
.end method

.method private b(Lcom/facebook/notifications/constants/NotificationType;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 440140
    invoke-static {p0, p1}, LX/2c4;->c(LX/2c4;Lcom/facebook/notifications/constants/NotificationType;)Ljava/util/List;

    move-result-object v0

    .line 440141
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 440142
    :goto_0
    return v0

    .line 440143
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 440144
    iget-object v3, p0, LX/2c4;->b:Landroid/app/NotificationManager;

    invoke-virtual {v3, v0, v1}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_1

    .line 440145
    :cond_1
    invoke-direct {p0, p1}, LX/2c4;->e(Lcom/facebook/notifications/constants/NotificationType;)V

    .line 440146
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(LX/2c4;Lcom/facebook/notifications/constants/NotificationType;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 440131
    invoke-static {p1}, LX/2c4;->d(Lcom/facebook/notifications/constants/NotificationType;)LX/0Tn;

    move-result-object v0

    .line 440132
    iget-object v1, p0, LX/2c4;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 440133
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 440134
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 440135
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 440136
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 440137
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 440138
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 440139
    :catch_0
    :cond_0
    return-object v1
.end method

.method private static d(Lcom/facebook/notifications/constants/NotificationType;)LX/0Tn;
    .locals 1

    .prologue
    .line 440064
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->FRIEND:Lcom/facebook/notifications/constants/NotificationType;

    if-ne p0, v0, :cond_0

    sget-object v0, LX/0hM;->I:LX/0Tn;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0hM;->H:LX/0Tn;

    goto :goto_0
.end method

.method private e(Lcom/facebook/notifications/constants/NotificationType;)V
    .locals 2

    .prologue
    .line 440128
    invoke-static {p1}, LX/2c4;->d(Lcom/facebook/notifications/constants/NotificationType;)LX/0Tn;

    move-result-object v0

    .line 440129
    iget-object v1, p0, LX/2c4;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 440130
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 440127
    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 440123
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->DEFAULT_PUSH_OF_JEWEL_NOTIF:Lcom/facebook/notifications/constants/NotificationType;

    invoke-direct {p0, v0}, LX/2c4;->b(Lcom/facebook/notifications/constants/NotificationType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440124
    iget-object v0, p0, LX/2c4;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Q7;

    .line 440125
    iget-object v1, v0, LX/3Q7;->k:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/notifications/lockscreen/util/LockScreenUtil$1;

    const-string v3, "LockScreenUtil"

    const-string p0, "DeletePushNotifications"

    invoke-direct {v2, v0, v3, p0}, Lcom/facebook/notifications/lockscreen/util/LockScreenUtil$1;-><init>(LX/3Q7;Ljava/lang/String;Ljava/lang/String;)V

    const v3, -0x742064b7

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 440126
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 440062
    iget-object v0, p0, LX/2c4;->b:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 440063
    return-void
.end method

.method public final a(ILX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 10

    .prologue
    .line 440065
    iget-object v0, p0, LX/2c4;->d:LX/2aN;

    const/4 v4, 0x0

    move-object v1, p5

    move-object v2, p3

    move-object v3, p4

    move v5, p1

    invoke-virtual/range {v0 .. v5}, LX/2aN;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Landroid/content/Intent;LX/8D4;LX/3B2;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 440066
    iget-object v1, p2, LX/BAa;->k:LX/2HB;

    .line 440067
    iput-object v0, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 440068
    iget-object v0, p0, LX/2c4;->d:LX/2aN;

    .line 440069
    new-instance v6, Landroid/content/Intent;

    iget-object v7, v0, LX/2aN;->b:Landroid/content/Context;

    const-class v8, Lcom/facebook/notifications/service/SystemTrayLogService;

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v7, "NOTIF_LOG"

    invoke-virtual {v6, v7, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "EVENT_TYPE"

    sget-object v8, LX/3B2;->CLEAR_FROM_TRAY:LX/3B2;

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v6

    .line 440070
    iget-object v7, v0, LX/2aN;->b:Landroid/content/Context;

    iget-object v8, v0, LX/2aN;->a:LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v8

    long-to-int v8, v8

    const/4 v9, 0x0

    invoke-static {v7, v8, v6, v9}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    move-object v0, v6

    .line 440071
    iget-object v1, p2, LX/BAa;->k:LX/2HB;

    invoke-virtual {v1, v0}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    .line 440072
    iget-object v0, p0, LX/2c4;->c:LX/2Yg;

    sget-object v1, LX/3B2;->ADD_TO_TRAY:LX/3B2;

    invoke-virtual {v0, p5, v1}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/3B2;)V

    .line 440073
    iget-boolean v0, p2, LX/BAa;->e:Z

    if-eqz v0, :cond_0

    .line 440074
    iget-object v0, p2, LX/BAa;->k:LX/2HB;

    const v1, -0xffff01

    const/16 v2, 0x1f4

    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, LX/2HB;->a(III)LX/2HB;

    .line 440075
    :cond_0
    iget-boolean v0, p2, LX/BAa;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p2, LX/BAa;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p2, LX/BAa;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 440076
    iget-object v0, p2, LX/BAa;->k:LX/2HB;

    iget-object v1, p2, LX/BAa;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x5

    .line 440077
    iget-object v3, v0, LX/2HB;->B:Landroid/app/Notification;

    iput-object v1, v3, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 440078
    iget-object v3, v0, LX/2HB;->B:Landroid/app/Notification;

    iput v2, v3, Landroid/app/Notification;->audioStreamType:I

    .line 440079
    :cond_1
    iget-boolean v0, p2, LX/BAa;->f:Z

    if-eqz v0, :cond_2

    .line 440080
    iget-object v0, p2, LX/BAa;->k:LX/2HB;

    sget-object v1, LX/BAa;->a:[J

    invoke-virtual {v0, v1}, LX/2HB;->a([J)LX/2HB;

    .line 440081
    :cond_2
    iget-object v0, p2, LX/BAa;->k:LX/2HB;

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    move-object v0, v0

    .line 440082
    invoke-virtual {p5}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->b()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v1

    .line 440083
    invoke-static {v1}, Lcom/facebook/notifications/model/SystemTrayNotification;->a(Lcom/facebook/notifications/constants/NotificationType;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 440084
    iget-object v2, p0, LX/2c4;->b:Landroid/app/NotificationManager;

    .line 440085
    iget-object v3, p5, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->f:Ljava/lang/String;

    move-object v3, v3

    .line 440086
    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 440087
    iget-object v2, p5, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->f:Ljava/lang/String;

    move-object v2, v2

    .line 440088
    invoke-static {p0, v1}, LX/2c4;->c(LX/2c4;Lcom/facebook/notifications/constants/NotificationType;)Ljava/util/List;

    move-result-object v3

    .line 440089
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 440090
    invoke-static {p0, v1, v3}, LX/2c4;->a(LX/2c4;Lcom/facebook/notifications/constants/NotificationType;Ljava/util/List;)V

    .line 440091
    :goto_0
    iget-boolean v0, p2, LX/BAa;->i:Z

    move v0, v0

    .line 440092
    if-eqz v0, :cond_3

    .line 440093
    iget-object v0, p0, LX/2c4;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Q7;

    invoke-virtual {v0}, LX/3Q7;->g()V

    .line 440094
    :cond_3
    return-void

    .line 440095
    :cond_4
    if-nez p1, :cond_5

    .line 440096
    invoke-virtual {v1}, Lcom/facebook/notifications/constants/NotificationType;->ordinal()I

    move-result p1

    .line 440097
    :cond_5
    iget-object v1, p0, LX/2c4;->b:Landroid/app/NotificationManager;

    invoke-virtual {v1, p1, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/notifications/constants/NotificationType;)V
    .locals 2

    .prologue
    .line 440098
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->FRIEND:Lcom/facebook/notifications/constants/NotificationType;

    if-ne p1, v0, :cond_0

    .line 440099
    invoke-virtual {p0}, LX/2c4;->b()V

    .line 440100
    :goto_0
    return-void

    .line 440101
    :cond_0
    invoke-static {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->a(Lcom/facebook/notifications/constants/NotificationType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 440102
    invoke-virtual {p0}, LX/2c4;->a()V

    goto :goto_0

    .line 440103
    :cond_1
    iget-object v0, p0, LX/2c4;->b:Landroid/app/NotificationManager;

    invoke-virtual {p1}, Lcom/facebook/notifications/constants/NotificationType;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/notifications/constants/NotificationType;LX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 6

    .prologue
    .line 440104
    invoke-virtual {p5}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->b()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->UNKNOWN:Lcom/facebook/notifications/constants/NotificationType;

    if-ne v0, v1, :cond_0

    .line 440105
    iput-object p1, p5, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->b:Lcom/facebook/notifications/constants/NotificationType;

    .line 440106
    :cond_0
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/2c4;->a(ILX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 440107
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 440108
    iget-object v0, p0, LX/2c4;->i:LX/1rx;

    invoke-virtual {v0}, LX/1rx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440109
    invoke-virtual {p0, p1}, LX/2c4;->b(Ljava/lang/String;)V

    .line 440110
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 440111
    iget-object v0, p0, LX/2c4;->b:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 440112
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 440113
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->FRIEND:Lcom/facebook/notifications/constants/NotificationType;

    invoke-direct {p0, v0}, LX/2c4;->b(Lcom/facebook/notifications/constants/NotificationType;)Z

    .line 440114
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 440115
    iget-object v0, p0, LX/2c4;->b:Landroid/app/NotificationManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 440116
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->FRIEND:Lcom/facebook/notifications/constants/NotificationType;

    invoke-direct {p0, v0, p1}, LX/2c4;->b(Lcom/facebook/notifications/constants/NotificationType;Ljava/lang/String;)V

    .line 440117
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->DEFAULT_PUSH_OF_JEWEL_NOTIF:Lcom/facebook/notifications/constants/NotificationType;

    invoke-direct {p0, v0, p1}, LX/2c4;->b(Lcom/facebook/notifications/constants/NotificationType;Ljava/lang/String;)V

    .line 440118
    return-void
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 440119
    iget-object v0, p0, LX/2c4;->b:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 440120
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->DEFAULT_PUSH_OF_JEWEL_NOTIF:Lcom/facebook/notifications/constants/NotificationType;

    invoke-direct {p0, v0}, LX/2c4;->e(Lcom/facebook/notifications/constants/NotificationType;)V

    .line 440121
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->FRIEND:Lcom/facebook/notifications/constants/NotificationType;

    invoke-direct {p0, v0}, LX/2c4;->e(Lcom/facebook/notifications/constants/NotificationType;)V

    .line 440122
    return-void
.end method
