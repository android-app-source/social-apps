.class public LX/3FK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:I

.field public final d:J

.field public final e:I

.field public final f:I

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:Z

.field public final k:Z

.field public final l:Z


# direct methods
.method public constructor <init>(LX/0ad;LX/0Uh;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 539431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 539432
    const/16 v0, 0x2be

    invoke-virtual {p2, v0, v6}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 539433
    const/16 v1, 0x2d2

    invoke-virtual {p2, v1, v6}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 539434
    sget-short v2, LX/0ws;->ba:S

    invoke-interface {p1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3FK;->a:Z

    .line 539435
    sget-short v0, LX/0ws;->bb:S

    invoke-interface {p1, v0, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3FK;->b:Z

    .line 539436
    sget v0, LX/0ws;->bf:I

    const/16 v2, 0x3e8

    invoke-interface {p1, v0, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/3FK;->c:I

    .line 539437
    sget-wide v2, LX/0ws;->bd:J

    const-wide/32 v4, 0x927c0

    invoke-interface {p1, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v2

    iput-wide v2, p0, LX/3FK;->d:J

    .line 539438
    sget v0, LX/0ws;->be:I

    const/4 v2, 0x5

    invoke-interface {p1, v0, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/3FK;->e:I

    .line 539439
    sget v0, LX/0ws;->bc:I

    const/high16 v2, 0x100000

    invoke-interface {p1, v0, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/3FK;->f:I

    .line 539440
    sget-short v0, LX/0ws;->bj:S

    invoke-interface {p1, v0, v7}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3FK;->g:Z

    .line 539441
    sget-short v0, LX/0ws;->bh:S

    invoke-interface {p1, v0, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3FK;->h:Z

    .line 539442
    sget-short v0, LX/0ws;->bi:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3FK;->i:Z

    .line 539443
    sget-short v0, LX/0ws;->bg:S

    invoke-interface {p1, v0, v7}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3FK;->j:Z

    .line 539444
    sget-short v0, LX/0ws;->bk:S

    invoke-interface {p1, v0, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3FK;->k:Z

    .line 539445
    sget-short v0, LX/0ws;->bl:S

    invoke-interface {p1, v0, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3FK;->l:Z

    .line 539446
    return-void
.end method
