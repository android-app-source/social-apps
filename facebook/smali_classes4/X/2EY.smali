.class public LX/2EY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private b:LX/0UY;


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0UY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 385480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385481
    iput-object p1, p0, LX/2EY;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 385482
    iput-object p2, p0, LX/2EY;->b:LX/0UY;

    .line 385483
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 9

    .prologue
    const v4, 0x700003

    .line 385484
    iget-object v0, p0, LX/2EY;->b:LX/0UY;

    if-nez v0, :cond_0

    .line 385485
    :goto_0
    return-void

    .line 385486
    :cond_0
    iget-object v0, p0, LX/2EY;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v1, 0x3e8

    invoke-interface {v0, v4, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(II)V

    .line 385487
    iget-object v0, p0, LX/2EY;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    iget-object v2, p0, LX/2EY;->b:LX/0UY;

    .line 385488
    iget-wide v5, v2, LX/0UY;->c:J

    iget-wide v7, v2, LX/0UY;->b:J

    sub-long/2addr v5, v7

    move-wide v2, v5

    .line 385489
    long-to-int v2, v2

    invoke-interface {v0, v4, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    goto :goto_0
.end method
