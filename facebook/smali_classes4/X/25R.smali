.class public LX/25R;
.super Landroid/support/v7/widget/RecyclerView;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public h:Z

.field public i:I

.field public j:I

.field public k:Landroid/view/View$OnTouchListener;

.field public l:I

.field private m:Z

.field private n:Z

.field public o:LX/1P1;

.field public p:LX/25S;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 369431
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 369432
    iput-boolean v1, p0, LX/25R;->h:Z

    .line 369433
    iput v0, p0, LX/25R;->i:I

    .line 369434
    iput v0, p0, LX/25R;->l:I

    .line 369435
    iput-boolean v1, p0, LX/25R;->m:Z

    .line 369436
    iput-boolean v0, p0, LX/25R;->n:Z

    .line 369437
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/25R;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 369438
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 369423
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 369424
    iput-boolean v1, p0, LX/25R;->h:Z

    .line 369425
    iput v0, p0, LX/25R;->i:I

    .line 369426
    iput v0, p0, LX/25R;->l:I

    .line 369427
    iput-boolean v1, p0, LX/25R;->m:Z

    .line 369428
    iput-boolean v0, p0, LX/25R;->n:Z

    .line 369429
    invoke-direct {p0, p1, p2}, LX/25R;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 369430
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 369415
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 369416
    iput-boolean v1, p0, LX/25R;->h:Z

    .line 369417
    iput v0, p0, LX/25R;->i:I

    .line 369418
    iput v0, p0, LX/25R;->l:I

    .line 369419
    iput-boolean v1, p0, LX/25R;->m:Z

    .line 369420
    iput-boolean v0, p0, LX/25R;->n:Z

    .line 369421
    invoke-direct {p0, p1, p2}, LX/25R;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 369422
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 369349
    if-eqz p2, :cond_0

    .line 369350
    sget-object v0, LX/03r;->HScrollRecyclerView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 369351
    const/16 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/25R;->h:Z

    .line 369352
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 369353
    :cond_0
    iget-boolean v0, p0, LX/25R;->h:Z

    if-eqz v0, :cond_1

    move-object v0, p0

    :goto_0
    invoke-super {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 369354
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f76

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/25R;->j:I

    .line 369355
    return-void

    .line 369356
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/25R;II)I
    .locals 2

    .prologue
    .line 369409
    add-int v0, p1, p2

    .line 369410
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v1, v1

    .line 369411
    if-nez v1, :cond_0

    .line 369412
    const/4 v1, 0x0

    .line 369413
    :goto_0
    move v1, v1

    .line 369414
    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0

    :cond_0
    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    goto :goto_0
.end method

.method public static i(II)I
    .locals 2

    .prologue
    .line 369408
    sub-int v0, p0, p1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/62w;)V
    .locals 4

    .prologue
    .line 369401
    iget v0, p0, LX/25R;->i:I

    const/4 v3, 0x1

    .line 369402
    sget-object v1, LX/62v;->a:[I

    invoke-virtual {p1}, LX/62w;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 369403
    const/4 v1, 0x0

    :goto_0
    move v0, v1

    .line 369404
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/25R;->b(IZ)V

    .line 369405
    return-void

    .line 369406
    :pswitch_0
    invoke-static {p0, v0, v3}, LX/25R;->h(LX/25R;II)I

    move-result v1

    goto :goto_0

    .line 369407
    :pswitch_1
    invoke-static {v0, v3}, LX/25R;->i(II)I

    move-result v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(IZ)V
    .locals 1

    .prologue
    .line 369394
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 369395
    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 369396
    :cond_0
    :goto_0
    return-void

    .line 369397
    :cond_1
    iput p1, p0, LX/25R;->i:I

    .line 369398
    if-eqz p2, :cond_2

    .line 369399
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    goto :goto_0

    .line 369400
    :cond_2
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 369369
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    .line 369370
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    .line 369371
    if-eq v3, v1, :cond_0

    const/4 v4, 0x6

    if-eq v3, v4, :cond_0

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    const/4 v4, 0x4

    if-ne v3, v4, :cond_4

    .line 369372
    :cond_0
    iget-boolean v3, p0, LX/25R;->n:Z

    if-eqz v3, :cond_1

    .line 369373
    const/4 v6, 0x1

    .line 369374
    iget v3, p0, LX/25R;->l:I

    sub-int/2addr v3, v2

    .line 369375
    iget-object v4, p0, LX/25R;->p:LX/25S;

    invoke-interface {v4, v3}, LX/25S;->a(I)I

    move-result v4

    .line 369376
    iget v5, p0, LX/25R;->j:I

    if-le v3, v5, :cond_8

    .line 369377
    invoke-virtual {p0}, LX/25R;->getLayoutDirection()I

    move-result v3

    if-ne v3, v6, :cond_7

    iget v3, p0, LX/25R;->i:I

    invoke-static {v3, v4}, LX/25R;->i(II)I

    move-result v3

    .line 369378
    :goto_0
    move v2, v3

    .line 369379
    invoke-virtual {p0, v2, v1}, LX/25R;->b(IZ)V

    .line 369380
    :cond_1
    iput-boolean v1, p0, LX/25R;->m:Z

    .line 369381
    iput-boolean v0, p0, LX/25R;->n:Z

    move v0, v1

    .line 369382
    :cond_2
    :goto_1
    iget-object v1, p0, LX/25R;->k:Landroid/view/View$OnTouchListener;

    if-eqz v1, :cond_3

    .line 369383
    iget-object v1, p0, LX/25R;->k:Landroid/view/View$OnTouchListener;

    invoke-interface {v1, p1, p2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 369384
    :cond_3
    return v0

    .line 369385
    :cond_4
    if-eqz v3, :cond_5

    const/4 v4, 0x5

    if-eq v3, v4, :cond_5

    iget-boolean v4, p0, LX/25R;->m:Z

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 369386
    :cond_5
    iput v2, p0, LX/25R;->l:I

    .line 369387
    iget-boolean v2, p0, LX/25R;->m:Z

    if-eqz v2, :cond_6

    .line 369388
    iput-boolean v0, p0, LX/25R;->m:Z

    .line 369389
    :cond_6
    iput-boolean v1, p0, LX/25R;->n:Z

    goto :goto_1

    .line 369390
    :cond_7
    iget v3, p0, LX/25R;->i:I

    invoke-static {p0, v3, v4}, LX/25R;->h(LX/25R;II)I

    move-result v3

    goto :goto_0

    .line 369391
    :cond_8
    iget v5, p0, LX/25R;->j:I

    neg-int v5, v5

    if-ge v3, v5, :cond_a

    .line 369392
    invoke-virtual {p0}, LX/25R;->getLayoutDirection()I

    move-result v3

    if-ne v3, v6, :cond_9

    iget v3, p0, LX/25R;->i:I

    invoke-static {p0, v3, v4}, LX/25R;->h(LX/25R;II)I

    move-result v3

    goto :goto_0

    :cond_9
    iget v3, p0, LX/25R;->i:I

    invoke-static {v3, v4}, LX/25R;->i(II)I

    move-result v3

    goto :goto_0

    .line 369393
    :cond_a
    iget v3, p0, LX/25R;->i:I

    goto :goto_0
.end method

.method public setLayoutManager(LX/1OR;)V
    .locals 2

    .prologue
    .line 369364
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/1P1;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "SnapRecyclerView only supports LinearLayoutManager"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 369365
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 369366
    check-cast p1, LX/1P1;

    iput-object p1, p0, LX/25R;->o:LX/1P1;

    .line 369367
    return-void

    .line 369368
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 369362
    iput-object p1, p0, LX/25R;->k:Landroid/view/View$OnTouchListener;

    .line 369363
    return-void
.end method

.method public setSnappingEnabled(Z)V
    .locals 1

    .prologue
    .line 369357
    iget-boolean v0, p0, LX/25R;->h:Z

    if-eq v0, p1, :cond_0

    .line 369358
    iput-boolean p1, p0, LX/25R;->h:Z

    .line 369359
    iget-boolean v0, p0, LX/25R;->h:Z

    if-eqz v0, :cond_1

    move-object v0, p0

    :goto_0
    invoke-super {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 369360
    :cond_0
    return-void

    .line 369361
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
