.class public final LX/2eH;
.super LX/2eF;
.source ""


# instance fields
.field private final b:F

.field private final c:I

.field private final d:Z

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(FIZ)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 445143
    invoke-direct {p0}, LX/2eF;-><init>()V

    .line 445144
    iput p2, p0, LX/2eH;->c:I

    .line 445145
    iput p1, p0, LX/2eH;->b:F

    .line 445146
    iput-boolean p3, p0, LX/2eH;->d:Z

    .line 445147
    iput v0, p0, LX/2eH;->e:I

    .line 445148
    iput v0, p0, LX/2eH;->f:I

    .line 445149
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 445140
    iget v0, p0, LX/2eH;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 445141
    const/high16 v0, 0x41000000    # 8.0f

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/2eH;->f:I

    .line 445142
    :cond_0
    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 445134
    iget v0, p0, LX/2eH;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 445135
    iget v0, p0, LX/2eH;->b:F

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/2eH;->e:I

    .line 445136
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 445150
    iget v0, p0, LX/2eH;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 445151
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getPageWidthPixelsForRecyclerView() must be called after applyToRecyclerView()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 445152
    :cond_0
    iget v0, p0, LX/2eH;->e:I

    return v0
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 445137
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2eH;->b(Landroid/content/Context;)V

    .line 445138
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2eH;->a(Landroid/content/Context;)V

    .line 445139
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 445128
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2eH;->b(Landroid/content/Context;)V

    .line 445129
    iget-boolean v0, p0, LX/2eH;->d:Z

    if-eqz v0, :cond_1

    .line 445130
    iget v0, p0, LX/2eH;->e:I

    invoke-static {p1, v0}, LX/2eF;->a(Landroid/view/View;I)V

    .line 445131
    :cond_0
    :goto_0
    return-void

    .line 445132
    :cond_1
    instance-of v0, p1, LX/2fC;

    if-eqz v0, :cond_0

    .line 445133
    check-cast p1, LX/2fC;

    iget v0, p0, LX/2eH;->e:I

    invoke-interface {p1, v0}, LX/2fC;->setWidth(I)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 445115
    instance-of v0, p1, Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    .line 445116
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This style requires an extra wrapping FrameLayout to work"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 445117
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2eH;->b(Landroid/content/Context;)V

    .line 445118
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2eH;->a(Landroid/content/Context;)V

    .line 445119
    check-cast p1, Landroid/widget/FrameLayout;

    .line 445120
    invoke-virtual {p1, v5}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 445121
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 445122
    iget v2, p0, LX/2eH;->e:I

    iget v3, p0, LX/2eH;->f:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    .line 445123
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, p0, LX/2eH;->f:I

    if-ne v3, v4, :cond_1

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v4, p0, LX/2eH;->f:I

    if-ne v3, v4, :cond_1

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v3, v2, :cond_2

    .line 445124
    :cond_1
    iget v3, p0, LX/2eH;->f:I

    iget v4, p0, LX/2eH;->f:I

    invoke-virtual {v0, v3, v5, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 445125
    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 445126
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 445127
    :cond_2
    return-void
.end method
