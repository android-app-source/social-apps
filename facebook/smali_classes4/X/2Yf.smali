.class public final LX/2Yf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/aldrin/status/AldrinUserStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;


# direct methods
.method public constructor <init>(Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;)V
    .locals 0

    .prologue
    .line 421200
    iput-object p1, p0, LX/2Yf;->a:Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 421195
    iget-object v0, p0, LX/2Yf;->a:Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;

    const/4 v1, 0x0

    .line 421196
    iput-object v1, v0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 421197
    iget-object v0, p0, LX/2Yf;->a:Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;

    invoke-static {v0}, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->m(Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;)J

    move-result-wide v0

    .line 421198
    iget-object v2, p0, LX/2Yf;->a:Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;

    iget-object v2, v2, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/2YT;->c:LX/0Tn;

    const-wide/16 v4, 0x2

    mul-long/2addr v0, v4

    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 421199
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 421192
    iget-object v0, p0, LX/2Yf;->a:Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;

    const/4 v1, 0x0

    .line 421193
    iput-object v1, v0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 421194
    return-void
.end method
