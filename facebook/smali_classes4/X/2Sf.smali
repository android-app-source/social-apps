.class public LX/2Sf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2Sf;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 413056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413057
    return-void
.end method

.method public static final a(Ljava/util/List;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Cwc;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413058
    sget-object v0, LX/Cw9;->DEFAULT:LX/Cw9;

    .line 413059
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 413060
    new-instance v2, Lcom/facebook/search/model/GapTypeaheadUnit;

    invoke-direct {v2, v0}, Lcom/facebook/search/model/GapTypeaheadUnit;-><init>(LX/Cw9;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 413061
    invoke-static {v1, p0}, LX/2Sf;->a(LX/0Pz;Ljava/util/List;)V

    .line 413062
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 413063
    return-object v0
.end method

.method public static a(LX/0QB;)LX/2Sf;
    .locals 3

    .prologue
    .line 413064
    sget-object v0, LX/2Sf;->a:LX/2Sf;

    if-nez v0, :cond_1

    .line 413065
    const-class v1, LX/2Sf;

    monitor-enter v1

    .line 413066
    :try_start_0
    sget-object v0, LX/2Sf;->a:LX/2Sf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413067
    if-eqz v2, :cond_0

    .line 413068
    :try_start_1
    new-instance v0, LX/2Sf;

    invoke-direct {v0}, LX/2Sf;-><init>()V

    .line 413069
    move-object v0, v0

    .line 413070
    sput-object v0, LX/2Sf;->a:LX/2Sf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413071
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413072
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413073
    :cond_1
    sget-object v0, LX/2Sf;->a:LX/2Sf;

    return-object v0

    .line 413074
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413075
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0Pz;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/Cwc;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 413076
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwc;

    .line 413077
    iget-object v2, v0, LX/Cwc;->c:Ljava/lang/String;

    move-object v2, v2

    .line 413078
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 413079
    new-instance v2, Lcom/facebook/search/model/HeaderRowTypeaheadUnit;

    invoke-direct {v2, v0}, Lcom/facebook/search/model/HeaderRowTypeaheadUnit;-><init>(LX/Cwc;)V

    invoke-virtual {p0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 413080
    :cond_0
    iget-object v2, v0, LX/Cwc;->b:LX/0Px;

    move-object v0, v2

    .line 413081
    invoke-virtual {p0, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_0

    .line 413082
    :cond_1
    return-void
.end method

.method public static b(Ljava/util/List;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Cwc;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413083
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 413084
    invoke-static {v0, p0}, LX/2Sf;->a(LX/0Pz;Ljava/util/List;)V

    .line 413085
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
