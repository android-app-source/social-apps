.class public abstract LX/2An;
.super LX/0lO;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x6633b4850c53b6dfL


# instance fields
.field public final transient b:LX/0lP;


# direct methods
.method public constructor <init>(LX/0lP;)V
    .locals 0

    .prologue
    .line 378013
    invoke-direct {p0}, LX/0lO;-><init>()V

    .line 378014
    iput-object p1, p0, LX/2An;->b:LX/0lP;

    .line 378015
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;)V
.end method

.method public final a(Ljava/lang/annotation/Annotation;)V
    .locals 1

    .prologue
    .line 378010
    iget-object v0, p0, LX/2An;->b:LX/0lP;

    .line 378011
    invoke-static {v0, p1}, LX/0lP;->c(LX/0lP;Ljava/lang/annotation/Annotation;)V

    .line 378012
    return-void
.end method

.method public abstract b(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public final b(Ljava/lang/annotation/Annotation;)V
    .locals 1

    .prologue
    .line 378008
    iget-object v0, p0, LX/2An;->b:LX/0lP;

    invoke-virtual {v0, p1}, LX/0lP;->a(Ljava/lang/annotation/Annotation;)V

    .line 378009
    return-void
.end method

.method public final e()LX/0lP;
    .locals 1

    .prologue
    .line 378007
    iget-object v0, p0, LX/2An;->b:LX/0lP;

    return-object v0
.end method

.method public abstract i()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end method

.method public abstract j()Ljava/lang/reflect/Member;
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 378005
    invoke-virtual {p0}, LX/2An;->j()Ljava/lang/reflect/Member;

    move-result-object v0

    invoke-static {v0}, LX/1Xw;->a(Ljava/lang/reflect/Member;)V

    .line 378006
    return-void
.end method
