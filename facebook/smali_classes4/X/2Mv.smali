.class public LX/2Mv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/2Mw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 397889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2Mv;
    .locals 1

    .prologue
    .line 397861
    invoke-static {p0}, LX/2Mv;->b(LX/0QB;)LX/2Mv;

    move-result-object v0

    return-object v0
.end method

.method public static a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 397886
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 397887
    const/4 v0, 0x0

    .line 397888
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 4
    .param p0    # Lcom/facebook/user/model/User;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 397881
    if-nez p0, :cond_0

    .line 397882
    const/4 v0, 0x0

    .line 397883
    :goto_0
    return-object v0

    .line 397884
    :cond_0
    iget-wide v2, p0, Lcom/facebook/user/model/User;->N:J

    move-wide v0, v2

    .line 397885
    invoke-static {v0, v1}, LX/2Mv;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2Mv;
    .locals 3

    .prologue
    .line 397877
    new-instance v1, LX/2Mv;

    invoke-direct {v1}, LX/2Mv;-><init>()V

    .line 397878
    invoke-static {p0}, LX/2Mw;->b(LX/0QB;)LX/2Mw;

    move-result-object v0

    check-cast v0, LX/2Mw;

    const/16 v2, 0x12cb

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 397879
    iput-object v0, v1, LX/2Mv;->a:LX/2Mw;

    iput-object v2, v1, LX/2Mv;->b:LX/0Or;

    .line 397880
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 10

    .prologue
    .line 397862
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->g(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 397863
    iget-object v1, p0, LX/2Mv;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    invoke-static {v1}, LX/2Mv;->a(Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 397864
    if-nez v1, :cond_1

    .line 397865
    iget-object v1, p0, LX/2Mv;->a:LX/2Mw;

    .line 397866
    invoke-static {v1}, LX/2Mw;->c(LX/2Mw;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, v1, LX/2Mw;->f:LX/2Mx;

    .line 397867
    iget-object v3, v2, LX/2Mx;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/6gE;->m:LX/0Tn;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    move v2, v3

    .line 397868
    if-eqz v2, :cond_4

    .line 397869
    :cond_0
    :goto_0
    invoke-static {}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 397870
    :cond_1
    move-object v0, v1

    .line 397871
    invoke-virtual {v0, p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 397872
    :cond_4
    new-instance v6, LX/6hE;

    invoke-direct {v6}, LX/6hE;-><init>()V

    move-object v6, v6

    .line 397873
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    const-wide/16 v8, 0x0

    invoke-virtual {v6, v8, v9}, LX/0zO;->a(J)LX/0zO;

    move-result-object v7

    .line 397874
    iget-object v6, v1, LX/2Mw;->g:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-virtual {v6, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    .line 397875
    new-instance v7, LX/6hJ;

    invoke-direct {v7, v1}, LX/6hJ;-><init>(LX/2Mw;)V

    iget-object v8, v1, LX/2Mw;->b:Ljava/util/concurrent/Executor;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v2, v6

    .line 397876
    new-instance v3, LX/6hI;

    invoke-direct {v3, v1}, LX/6hI;-><init>(LX/2Mw;)V

    iget-object v4, v1, LX/2Mw;->c:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
