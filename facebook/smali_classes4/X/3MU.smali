.class public LX/3MU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/0Sh;

.field private final c:LX/3MV;

.field public final d:LX/3MW;

.field public final e:LX/3MX;


# direct methods
.method public constructor <init>(LX/0tX;LX/0Sh;LX/3MV;LX/3MW;LX/3MX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 554677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 554678
    iput-object p1, p0, LX/3MU;->a:LX/0tX;

    .line 554679
    iput-object p2, p0, LX/3MU;->b:LX/0Sh;

    .line 554680
    iput-object p3, p0, LX/3MU;->c:LX/3MV;

    .line 554681
    iput-object p4, p0, LX/3MU;->d:LX/3MW;

    .line 554682
    iput-object p5, p0, LX/3MU;->e:LX/3MX;

    .line 554683
    return-void
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessMessagingPageModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 554660
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 554661
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessMessagingPageModel;

    .line 554662
    invoke-virtual {v0}, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessMessagingPageModel;->o()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessMessagingPageModel;->n()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessMessagingPageModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v6

    invoke-static {v4, v5, v6}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquare;

    move-result-object v4

    .line 554663
    new-instance v5, LX/0XI;

    invoke-direct {v5}, LX/0XI;-><init>()V

    sget-object v6, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessMessagingPageModel;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v5

    new-instance v6, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessMessagingPageModel;->l()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 554664
    iput-object v6, v5, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 554665
    move-object v5, v5

    .line 554666
    iput-object v4, v5, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 554667
    move-object v4, v5

    .line 554668
    invoke-virtual {v0}, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessMessagingPageModel;->k()Z

    move-result v0

    .line 554669
    iput-boolean v0, v4, LX/0XI;->z:Z

    .line 554670
    move-object v0, v4

    .line 554671
    const-string v4, "page"

    .line 554672
    iput-object v4, v0, LX/0XI;->y:Ljava/lang/String;

    .line 554673
    move-object v0, v0

    .line 554674
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 554675
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 554676
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/3MU;
    .locals 1

    .prologue
    .line 554659
    invoke-static {p0}, LX/3MU;->b(LX/0QB;)LX/3MU;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3MU;
    .locals 6

    .prologue
    .line 554657
    new-instance v0, LX/3MU;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, LX/3MV;->a(LX/0QB;)LX/3MV;

    move-result-object v3

    check-cast v3, LX/3MV;

    invoke-static {p0}, LX/3MW;->b(LX/0QB;)LX/3MW;

    move-result-object v4

    check-cast v4, LX/3MW;

    invoke-static {p0}, LX/3MX;->a(LX/0QB;)LX/3MX;

    move-result-object v5

    check-cast v5, LX/3MX;

    invoke-direct/range {v0 .. v5}, LX/3MU;-><init>(LX/0tX;LX/0Sh;LX/3MV;LX/3MW;LX/3MX;)V

    .line 554658
    return-object v0
.end method


# virtual methods
.method public final a(I)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 554643
    iget-object v0, p0, LX/3MU;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 554644
    new-instance v0, LX/FD6;

    invoke-direct {v0}, LX/FD6;-><init>()V

    move-object v0, v0

    .line 554645
    const-string v1, "max_pages_to_fetch"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 554646
    iget-object v1, p0, LX/3MU;->e:LX/3MX;

    sget-object v2, LX/3MY;->HUGE:LX/3MY;

    invoke-virtual {v1, v2}, LX/3MX;->a(LX/3MY;)I

    move-result v1

    .line 554647
    iget-object v2, p0, LX/3MU;->e:LX/3MX;

    sget-object v3, LX/3MY;->BIG:LX/3MY;

    invoke-virtual {v2, v3}, LX/3MX;->a(LX/3MY;)I

    move-result v2

    .line 554648
    iget-object v3, p0, LX/3MU;->e:LX/3MX;

    sget-object p1, LX/3MY;->SMALL:LX/3MY;

    invoke-virtual {v3, p1}, LX/3MX;->a(LX/3MY;)I

    move-result v3

    .line 554649
    const-string p1, "profile_pic_large_size"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string p1, "profile_pic_medium_size"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_pic_small_size"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 554650
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 554651
    iget-object v1, p0, LX/3MU;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    const v1, -0x7ed58678

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 554652
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 554653
    check-cast v0, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$SuggestedBotsQueryModel;

    .line 554654
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$SuggestedBotsQueryModel;->a()Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$SuggestedBotsQueryModel$MessengerBotsYouMayMessageModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 554655
    :cond_0
    const/4 v0, 0x0

    .line 554656
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$SuggestedBotsQueryModel;->a()Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$SuggestedBotsQueryModel$MessengerBotsYouMayMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$SuggestedBotsQueryModel$MessengerBotsYouMayMessageModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/3MU;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
