.class public LX/2g3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2g3;


# instance fields
.field public final a:LX/2g5;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/2g5;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 447815
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447816
    iput-object p1, p0, LX/2g3;->a:LX/2g5;

    .line 447817
    iput-object p2, p0, LX/2g3;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 447818
    return-void
.end method

.method public static a(LX/0QB;)LX/2g3;
    .locals 5

    .prologue
    .line 447819
    sget-object v0, LX/2g3;->c:LX/2g3;

    if-nez v0, :cond_1

    .line 447820
    const-class v1, LX/2g3;

    monitor-enter v1

    .line 447821
    :try_start_0
    sget-object v0, LX/2g3;->c:LX/2g3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 447822
    if-eqz v2, :cond_0

    .line 447823
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 447824
    new-instance p0, LX/2g3;

    invoke-static {v0}, LX/2g4;->a(LX/0QB;)LX/2g4;

    move-result-object v3

    check-cast v3, LX/2g5;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4}, LX/2g3;-><init>(LX/2g5;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 447825
    move-object v0, p0

    .line 447826
    sput-object v0, LX/2g3;->c:LX/2g3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447827
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 447828
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 447829
    :cond_1
    sget-object v0, LX/2g3;->c:LX/2g3;

    return-object v0

    .line 447830
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 447831
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    .locals 4
    .param p3    # Lcom/facebook/interstitial/manager/InterstitialTrigger;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 447832
    iget-object v1, p0, LX/2g3;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->a()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;

    move-result-object v2

    invoke-static {v2}, LX/2fw;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;)LX/0Tn;

    move-result-object v2

    sget-object v3, LX/2g6;->DEFAULT:LX/2g6;

    invoke-virtual {v3}, LX/2g6;->ordinal()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 447833
    invoke-static {}, LX/2g6;->values()[LX/2g6;

    move-result-object v2

    aget-object v1, v2, v1

    .line 447834
    sget-object v2, LX/2g6;->ALWAYS_PASS:LX/2g6;

    invoke-virtual {v1, v2}, LX/2g6;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 447835
    :cond_0
    :goto_0
    return v0

    .line 447836
    :cond_1
    iget-object v2, p0, LX/2g3;->a:LX/2g5;

    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->a()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;

    move-result-object v3

    invoke-interface {v2, v3}, LX/2g5;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;)LX/2g7;

    move-result-object v2

    .line 447837
    if-nez v2, :cond_2

    .line 447838
    iget-object v2, p0, LX/2g3;->a:LX/2g5;

    sget-object v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;->UNKNOWN:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;

    invoke-interface {v2, v3}, LX/2g5;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;)LX/2g7;

    move-result-object v2

    .line 447839
    :cond_2
    move-object v2, v2

    .line 447840
    if-eqz v2, :cond_0

    sget-object v3, LX/2g6;->ALWAYS_FAIL:LX/2g6;

    invoke-virtual {v1, v3}, LX/2g6;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v2, p2, p1, p3}, LX/2g7;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 447841
    :cond_3
    invoke-virtual {v2, p2, p1}, LX/2g7;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)V

    .line 447842
    const/4 v0, 0x0

    goto :goto_0
.end method
