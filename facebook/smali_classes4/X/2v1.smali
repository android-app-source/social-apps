.class public LX/2v1;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Byk;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/2v1",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Byk;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 477195
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 477196
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/2v1;->b:LX/0Zi;

    .line 477197
    iput-object p1, p0, LX/2v1;->a:LX/0Ot;

    .line 477198
    return-void
.end method

.method public static a(LX/0QB;)LX/2v1;
    .locals 4

    .prologue
    .line 477184
    const-class v1, LX/2v1;

    monitor-enter v1

    .line 477185
    :try_start_0
    sget-object v0, LX/2v1;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 477186
    sput-object v2, LX/2v1;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 477187
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477188
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 477189
    new-instance v3, LX/2v1;

    const/16 p0, 0x1e2f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2v1;-><init>(LX/0Ot;)V

    .line 477190
    move-object v0, v3

    .line 477191
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 477192
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2v1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 477193
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 477194
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 477169
    check-cast p2, LX/Byi;

    .line 477170
    iget-object v0, p0, LX/2v1;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Byk;

    iget-object v1, p2, LX/Byi;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Byi;->b:LX/1Pf;

    .line 477171
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 477172
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v4

    .line 477173
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 477174
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bd()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 477175
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 477176
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bd()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v3

    .line 477177
    :goto_0
    iget-object p0, v0, LX/Byk;->a:LX/1Vm;

    invoke-virtual {p0, p1}, LX/1Vm;->c(LX/1De;)LX/C2N;

    move-result-object p0

    new-instance p2, LX/Byj;

    invoke-direct {p2, v0, v4, v3}, LX/Byj;-><init>(LX/Byk;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, LX/C2N;->a(Landroid/view/View$OnClickListener;)LX/C2N;

    move-result-object v4

    .line 477178
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 477179
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v4, v3}, LX/C2N;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/C2N;->a(LX/1Pp;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 477180
    return-object v0

    .line 477181
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 477182
    invoke-static {}, LX/1dS;->b()V

    .line 477183
    const/4 v0, 0x0

    return-object v0
.end method
