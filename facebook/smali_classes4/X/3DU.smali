.class public LX/3DU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2vL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2vL",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/9JB;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final c:Landroid/database/sqlite/SQLiteDatabase;

.field public final d:LX/2kD;

.field public final e:LX/0SG;

.field public final f:LX/0tC;

.field private final g:Ljava/lang/String;

.field private h:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/database/sqlite/SQLiteDatabase;LX/2kD;LX/0SG;LX/0tC;Ljava/lang/String;)V
    .locals 1
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 535030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 535031
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/3DU;->a:Ljava/util/HashMap;

    .line 535032
    const/4 v0, 0x0

    iput-object v0, p0, LX/3DU;->h:[Ljava/lang/String;

    .line 535033
    iput-object p1, p0, LX/3DU;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 535034
    iput-object p2, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    .line 535035
    iput-object p3, p0, LX/3DU;->d:LX/2kD;

    .line 535036
    iput-object p4, p0, LX/3DU;->e:LX/0SG;

    .line 535037
    iput-object p5, p0, LX/3DU;->f:LX/0tC;

    .line 535038
    invoke-static {p6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p6, "_no_session_"

    :cond_0
    iput-object p6, p0, LX/3DU;->g:Ljava/lang/String;

    .line 535039
    return-void
.end method

.method public static h(LX/3DU;Landroid/database/Cursor;)J
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 534985
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 534986
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 534987
    cmp-long v5, v6, v2

    if-eqz v5, :cond_0

    move-wide v0, v2

    .line 534988
    :goto_0
    return-wide v0

    .line 534989
    :cond_0
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 534990
    :try_start_0
    iget-object v2, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    .line 534991
    const-string v3, "SELECT file, offset, mutation_data FROM models WHERE _id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v10

    invoke-virtual {v2, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    move-object v4, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 534992
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 534993
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 534994
    sget-object v2, LX/2k9;->b:LX/0U1;

    .line 534995
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 534996
    invoke-interface {v4, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 534997
    sget-object v3, LX/2k9;->c:LX/0U1;

    .line 534998
    iget-object v5, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v5

    .line 534999
    invoke-interface {v4, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 535000
    sget-object v5, LX/2k9;->d:LX/0U1;

    .line 535001
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 535002
    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 535003
    new-instance v6, Landroid/content/ContentValues;

    const/4 v7, 0x2

    invoke-direct {v6, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 535004
    sget-object v7, LX/2k9;->b:LX/0U1;

    .line 535005
    iget-object v10, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v10

    .line 535006
    invoke-interface {v4, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v7, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 535007
    sget-object v2, LX/2k9;->c:LX/0U1;

    .line 535008
    iget-object v7, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v7

    .line 535009
    invoke-interface {v4, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 535010
    sget-object v2, LX/2k9;->d:LX/0U1;

    .line 535011
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 535012
    invoke-interface {v4, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 535013
    iget-object v2, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "models"

    const/4 v5, 0x0

    const v7, 0x75c8cd37

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v2, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const v5, -0x521300ab

    invoke-static {v5}, LX/03h;->a(I)V

    .line 535014
    const-wide/16 v6, -0x1

    cmp-long v5, v2, v6

    if-eqz v5, :cond_2

    move v5, v0

    :goto_2
    invoke-static {v5}, LX/0PB;->checkState(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 535015
    invoke-static {v4}, LX/2k0;->a(Landroid/database/Cursor;)V

    .line 535016
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 535017
    sget-object v5, LX/2k5;->c:LX/0U1;

    .line 535018
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 535019
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 535020
    iget-object v5, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "edges"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, LX/2k5;->a:LX/0U1;

    .line 535021
    iget-object v11, v10, LX/0U1;->d:Ljava/lang/String;

    move-object v10, v11

    .line 535022
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " = ?"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v10, v0, [Ljava/lang/String;

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v10, v1

    invoke-virtual {v5, v6, v4, v7, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 535023
    if-ne v4, v0, :cond_3

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-wide v0, v2

    .line 535024
    goto/16 :goto_0

    :cond_1
    move v2, v1

    .line 535025
    goto/16 :goto_1

    :cond_2
    move v5, v1

    .line 535026
    goto :goto_2

    .line 535027
    :catchall_0
    move-exception v0

    move-object v1, v4

    :goto_4
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    throw v0

    :cond_3
    move v0, v1

    .line 535028
    goto :goto_3

    .line 535029
    :catchall_1
    move-exception v0

    move-object v1, v4

    goto :goto_4
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 534983
    check-cast p1, Landroid/database/Cursor;

    .line 534984
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 6

    .prologue
    .line 534977
    const/4 v2, 0x0

    .line 534978
    invoke-static {p2}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 534979
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 534980
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 534981
    const-string v2, "GraphCursorDatabaseModelStore.reflattenMutableFlatbuffer"

    invoke-virtual {v0, v2, p2}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 534982
    invoke-static {v1}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/15i;->a(ILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    return-object v0
.end method

.method public final a(Ljava/util/Collection;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 534967
    const-string v0, "tag"

    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v1

    .line 534968
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 534969
    iget-object v2, p0, LX/3DU;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 534970
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v3

    .line 534971
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 534972
    iget-object v2, p0, LX/3DU;->g:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 534973
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 534974
    iget-object v2, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SELECT edges._id AS _id, edges.confirmed_model AS confirmed_model, edges.optimistic_model AS optimistic_model, edges.tags AS tags, edges.session_id AS session_id, edges.class AS class, edges.model_type AS model_type, models.file AS file, models.offset AS offset, models.mutation_data AS mutation_data FROM edges INNER JOIN models ON edges.confirmed_model = models._id WHERE edges._id IN (  SELECT node_id FROM tags WHERE "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") AND user_id = ? "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "AND session_id != ?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 534975
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 534976
    return-object v0
.end method

.method public final a([J)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 534959
    array-length v0, p1

    new-array v1, v0, [Ljava/lang/String;

    .line 534960
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 534961
    aget-wide v2, p1, v0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 534962
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 534963
    :cond_0
    const-string v0, "edges._id"

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 534964
    iget-object v1, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SELECT edges._id AS _id, edges.confirmed_model AS confirmed_model, edges.optimistic_model AS optimistic_model, edges.tags AS tags, edges.session_id AS session_id, edges.class AS class, edges.model_type AS model_type, models.file AS file, models.offset AS offset, models.mutation_data AS mutation_data FROM edges INNER JOIN models ON edges.confirmed_model = models._id WHERE "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 534965
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 534966
    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 534957
    iget-object v0, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    const v1, -0x3f5c3c4b

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 534958
    return-void
.end method

.method public final a(Ljava/lang/Object;I)V
    .locals 1

    .prologue
    .line 534953
    check-cast p1, Landroid/database/Cursor;

    .line 534954
    invoke-interface {p1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 534955
    const/4 v0, 0x0

    iput-object v0, p0, LX/3DU;->h:[Ljava/lang/String;

    .line 534956
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .locals 12
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param

    .prologue
    .line 534914
    check-cast p1, Landroid/database/Cursor;

    .line 534915
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 534916
    const-string v0, "confirmed"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    move-wide v8, v0

    .line 534917
    :goto_0
    invoke-static {p3}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v3

    .line 534918
    invoke-interface {p3}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v1

    .line 534919
    invoke-virtual {v1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 534920
    invoke-virtual {v1}, LX/15i;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 534921
    invoke-virtual {v1}, LX/15i;->e()Ljava/nio/ByteBuffer;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 534922
    iget-object v0, p0, LX/3DU;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 534923
    iget-object v1, p0, LX/3DU;->d:LX/2kD;

    iget-object v0, p0, LX/3DU;->f:LX/0tC;

    invoke-virtual {v0}, LX/0tC;->a()Z

    move-result v6

    invoke-virtual/range {v1 .. v6}, LX/2kD;->a(Ljava/nio/ByteBuffer;LX/0w5;JZ)Ljava/lang/String;

    move-result-object v0

    .line 534924
    invoke-interface {p3}, Lcom/facebook/flatbuffers/MutableFlattenable;->o_()I

    move-result v1

    .line 534925
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 534926
    sget-object v3, LX/2k9;->b:LX/0U1;

    .line 534927
    iget-object v6, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v6

    .line 534928
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 534929
    sget-object v0, LX/2k9;->c:LX/0U1;

    .line 534930
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 534931
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 534932
    sget-object v0, LX/2k9;->d:LX/0U1;

    .line 534933
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 534934
    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 534935
    iget-object v0, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "models"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, LX/2k9;->a:LX/0U1;

    .line 534936
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 534937
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " = ?"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 534938
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 534939
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 534940
    sget-object v1, LX/2k5;->i:LX/0U1;

    .line 534941
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 534942
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 534943
    iget-object v1, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "edges"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/2k5;->a:LX/0U1;

    .line 534944
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 534945
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 534946
    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_4
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 534947
    return-void

    .line 534948
    :cond_0
    invoke-static {p0, p1}, LX/3DU;->h(LX/3DU;Landroid/database/Cursor;)J

    move-result-wide v0

    move-wide v8, v0

    goto/16 :goto_0

    .line 534949
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 534950
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 534951
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 534952
    :cond_4
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final a(Ljava/lang/Object;Ljava/util/Set;)Z
    .locals 5

    .prologue
    .line 534889
    check-cast p1, Landroid/database/Cursor;

    const/4 v0, 0x0

    .line 534890
    if-nez p2, :cond_1

    .line 534891
    :cond_0
    :goto_0
    return v0

    .line 534892
    :cond_1
    iget-object v1, p0, LX/3DU;->h:[Ljava/lang/String;

    if-nez v1, :cond_2

    .line 534893
    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 534894
    if-eqz v1, :cond_4

    .line 534895
    const-string v2, ","

    invoke-static {v1, v2}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 534896
    :goto_1
    move-object v1, v1

    .line 534897
    iput-object v1, p0, LX/3DU;->h:[Ljava/lang/String;

    .line 534898
    :cond_2
    iget-object v1, p0, LX/3DU;->h:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 534899
    iget-object v2, p0, LX/3DU;->h:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 534900
    invoke-interface {p2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 534901
    const/4 v0, 0x1

    goto :goto_0

    .line 534902
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 6

    .prologue
    .line 534907
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 534908
    invoke-interface {p2}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v2

    .line 534909
    new-instance v0, LX/15i;

    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v2}, LX/15i;->c()Ljava/nio/ByteBuffer;

    move-result-object v2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 534910
    const-string v1, "GraphCursorDatabaseModelStore.reloadFromMutableFlatbuffer"

    invoke-virtual {v0, v1, p2}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 534911
    invoke-virtual {v0}, LX/15i;->b()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 534912
    invoke-interface {p2}, Lcom/facebook/flatbuffers/MutableFlattenable;->o_()I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/15i;->a(ILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    return-object v0

    .line 534913
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 535040
    iget-object v0, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 535041
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 534903
    check-cast p1, Landroid/database/Cursor;

    .line 534904
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 534905
    const/4 v0, 0x0

    iput-object v0, p0, LX/3DU;->h:[Ljava/lang/String;

    .line 534906
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .locals 11
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param

    .prologue
    .line 534863
    check-cast p1, Landroid/database/Cursor;

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 534864
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 534865
    const-string v0, "confirmed"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 534866
    :goto_0
    invoke-interface {p3}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v2

    .line 534867
    invoke-virtual {v2}, LX/15i;->c()Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 534868
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 534869
    sget-object v8, LX/2k9;->d:LX/0U1;

    .line 534870
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 534871
    if-eqz v2, :cond_1

    invoke-static {v2}, LX/31S;->a(Ljava/nio/ByteBuffer;)[B

    move-result-object v2

    :goto_1
    invoke-virtual {v5, v8, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 534872
    iget-object v2, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v8, "models"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, LX/2k9;->a:LX/0U1;

    .line 534873
    iget-object p1, v10, LX/0U1;->d:Ljava/lang/String;

    move-object v10, p1

    .line 534874
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " = ?"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-array v10, v3, [Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v10, v4

    invoke-virtual {v2, v8, v5, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 534875
    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 534876
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 534877
    sget-object v1, LX/2k5;->i:LX/0U1;

    .line 534878
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 534879
    iget-object v2, p0, LX/3DU;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 534880
    iget-object v1, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "edges"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, LX/2k5;->a:LX/0U1;

    .line 534881
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 534882
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " = ?"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-array v8, v3, [Ljava/lang/String;

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v4

    invoke-virtual {v1, v2, v0, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 534883
    if-ne v0, v3, :cond_3

    :goto_3
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 534884
    return-void

    .line 534885
    :cond_0
    invoke-static {p0, p1}, LX/3DU;->h(LX/3DU;Landroid/database/Cursor;)J

    move-result-wide v0

    goto/16 :goto_0

    .line 534886
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    move v0, v4

    .line 534887
    goto :goto_2

    :cond_3
    move v3, v4

    .line 534888
    goto :goto_3
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 534861
    check-cast p1, Landroid/database/Cursor;

    .line 534862
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 534859
    iget-object v0, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    const v1, 0x3252930e

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 534860
    return-void
.end method

.method public final d(Ljava/lang/Object;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 13

    .prologue
    .line 534848
    check-cast p1, Landroid/database/Cursor;

    .line 534849
    const-string v0, "offset"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 534850
    const-string v0, "file"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 534851
    const-string v0, "class"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 534852
    const-string v0, "model_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 534853
    const-string v0, "mutation_data"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 534854
    iget-object v0, p0, LX/3DU;->f:LX/0tC;

    invoke-virtual {v0}, LX/0tC;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 534855
    iget-object v0, p0, LX/3DU;->d:LX/2kD;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/2kD;->a(Ljava/lang/String;)V

    .line 534856
    :cond_0
    iget-object v0, p0, LX/3DU;->d:LX/2kD;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 534857
    iget-object v7, v0, LX/2kD;->b:LX/2kE;

    move v8, v1

    move-object v9, v2

    move-object v10, v3

    move-object v11, v4

    move-object v12, v5

    invoke-virtual/range {v7 .. v12}, LX/2kE;->a(ILjava/lang/String;[BLjava/lang/String;[B)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v7

    move-object v0, v7

    .line 534858
    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 534847
    const-string v0, "GraphCursorDatabase"

    return-object v0
.end method

.method public final e(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 534843
    check-cast p1, Landroid/database/Cursor;

    const/4 v0, 0x1

    .line 534844
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 534845
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 534846
    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic f(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 534842
    return-void
.end method

.method public final g(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 534826
    check-cast p1, Landroid/database/Cursor;

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 534827
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 534828
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 534829
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 534830
    sget-object v7, LX/2k5;->c:LX/0U1;

    .line 534831
    iget-object v8, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v8

    .line 534832
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 534833
    sget-object v4, LX/2k5;->i:LX/0U1;

    .line 534834
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 534835
    iget-object v5, p0, LX/3DU;->e:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v6, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 534836
    iget-object v4, p0, LX/3DU;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "edges"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, LX/2k5;->a:LX/0U1;

    .line 534837
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 534838
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " = ?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v0, [Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v1

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 534839
    if-ne v2, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 534840
    return-void

    :cond_0
    move v0, v1

    .line 534841
    goto :goto_0
.end method

.method public final h(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 534819
    check-cast p1, Landroid/database/Cursor;

    .line 534820
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 534821
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 534822
    iget-object v1, p0, LX/3DU;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 534823
    iget-object v1, p0, LX/3DU;->a:Ljava/util/HashMap;

    new-instance v4, LX/9JB;

    invoke-direct {v4}, LX/9JB;-><init>()V

    invoke-virtual {v1, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 534824
    :cond_0
    iget-object v1, p0, LX/3DU;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JB;

    iget-object v0, v0, LX/9JB;->a:LX/1Yt;

    invoke-virtual {v0, v2, v3}, LX/1Yt;->a(J)V

    .line 534825
    return-void
.end method
