.class public LX/2Qu;
.super LX/2QZ;
.source ""


# instance fields
.field private final b:LX/2Qe;

.field private final c:LX/2Qr;

.field private final d:LX/2Qi;


# direct methods
.method public constructor <init>(LX/2Qe;LX/2Qr;LX/2Qi;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409055
    const-string v0, "platform_get_app_call_for_pending_upload"

    invoke-direct {p0, v0}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 409056
    iput-object p1, p0, LX/2Qu;->b:LX/2Qe;

    .line 409057
    iput-object p2, p0, LX/2Qu;->c:LX/2Qr;

    .line 409058
    iput-object p3, p0, LX/2Qu;->d:LX/2Qi;

    .line 409059
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    .line 409060
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 409061
    const-string v1, "platform_get_app_call_for_pending_upload_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;

    .line 409062
    iget-object v1, p0, LX/2Qu;->d:LX/2Qi;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 409063
    iget-object v2, v0, Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;->a:Ljava/lang/String;

    move-object v2, v2

    .line 409064
    iget-object v3, p0, LX/2Qu;->b:LX/2Qe;

    .line 409065
    invoke-static {v3, v2}, LX/2Qe;->c(LX/2Qe;Ljava/lang/String;)LX/0Px;

    move-result-object v4

    .line 409066
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 409067
    const/4 v4, 0x0

    .line 409068
    :goto_0
    move-object v3, v4

    .line 409069
    if-eqz v3, :cond_1

    .line 409070
    iget-object v4, p0, LX/2Qu;->c:LX/2Qr;

    .line 409071
    iget-object p1, v3, Lcom/facebook/katana/platform/PendingMediaUpload;->b:Ljava/lang/String;

    move-object v3, p1

    .line 409072
    invoke-static {v4, v3}, LX/2Qr;->c(LX/2Qr;Ljava/lang/String;)LX/0Px;

    move-result-object v5

    .line 409073
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 409074
    const/4 v5, 0x0

    .line 409075
    :goto_1
    move-object v3, v5

    .line 409076
    if-eqz v3, :cond_1

    .line 409077
    iget-boolean v4, v0, Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;->b:Z

    move v0, v4

    .line 409078
    if-eqz v0, :cond_0

    .line 409079
    const v0, 0x33e811e7

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 409080
    :try_start_0
    iget-object v0, p0, LX/2Qu;->b:LX/2Qe;

    .line 409081
    sget-object v4, LX/2Qg;->a:LX/0U1;

    .line 409082
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 409083
    invoke-static {v4, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 409084
    iget-object v5, v0, LX/2Qe;->b:LX/2Qi;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 409085
    const-string v6, "pending_media_uploads"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 409086
    iget-object v0, p0, LX/2Qu;->c:LX/2Qr;

    .line 409087
    iget-object v2, v3, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object v2, v2

    .line 409088
    sget-object v4, LX/2Qo;->a:LX/0U1;

    .line 409089
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 409090
    invoke-static {v4, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 409091
    iget-object v5, v0, LX/2Qr;->b:LX/2Qi;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 409092
    const-string v6, "pending_app_calls"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, p0, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 409093
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 409094
    const v0, -0x3e7d4b9e

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 409095
    :cond_0
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 409096
    :goto_2
    return-object v0

    .line 409097
    :catchall_0
    move-exception v0

    const v2, 0x6a3155d4

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 409098
    :cond_1
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 409099
    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/katana/platform/PendingMediaUpload;

    goto/16 :goto_0

    :cond_3
    const/4 p1, 0x0

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/platform/common/action/PlatformAppCall;

    goto :goto_1
.end method
