.class public final enum LX/2LZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2LZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2LZ;

.field public static final enum HTC:LX/2LZ;

.field public static final enum LG:LX/2LZ;

.field public static final enum OPPO:LX/2LZ;

.field public static final enum SAMSUNG:LX/2LZ;

.field public static final enum SONY:LX/2LZ;

.field public static final enum UNKNOWN:LX/2LZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 395205
    new-instance v0, LX/2LZ;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/2LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2LZ;->UNKNOWN:LX/2LZ;

    .line 395206
    new-instance v0, LX/2LZ;

    const-string v1, "HTC"

    invoke-direct {v0, v1, v4}, LX/2LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2LZ;->HTC:LX/2LZ;

    .line 395207
    new-instance v0, LX/2LZ;

    const-string v1, "SAMSUNG"

    invoke-direct {v0, v1, v5}, LX/2LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2LZ;->SAMSUNG:LX/2LZ;

    .line 395208
    new-instance v0, LX/2LZ;

    const-string v1, "SONY"

    invoke-direct {v0, v1, v6}, LX/2LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2LZ;->SONY:LX/2LZ;

    .line 395209
    new-instance v0, LX/2LZ;

    const-string v1, "LG"

    invoke-direct {v0, v1, v7}, LX/2LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2LZ;->LG:LX/2LZ;

    .line 395210
    new-instance v0, LX/2LZ;

    const-string v1, "OPPO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/2LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2LZ;->OPPO:LX/2LZ;

    .line 395211
    const/4 v0, 0x6

    new-array v0, v0, [LX/2LZ;

    sget-object v1, LX/2LZ;->UNKNOWN:LX/2LZ;

    aput-object v1, v0, v3

    sget-object v1, LX/2LZ;->HTC:LX/2LZ;

    aput-object v1, v0, v4

    sget-object v1, LX/2LZ;->SAMSUNG:LX/2LZ;

    aput-object v1, v0, v5

    sget-object v1, LX/2LZ;->SONY:LX/2LZ;

    aput-object v1, v0, v6

    sget-object v1, LX/2LZ;->LG:LX/2LZ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/2LZ;->OPPO:LX/2LZ;

    aput-object v2, v0, v1

    sput-object v0, LX/2LZ;->$VALUES:[LX/2LZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 395212
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2LZ;
    .locals 1

    .prologue
    .line 395213
    const-class v0, LX/2LZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2LZ;

    return-object v0
.end method

.method public static values()[LX/2LZ;
    .locals 1

    .prologue
    .line 395214
    sget-object v0, LX/2LZ;->$VALUES:[LX/2LZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2LZ;

    return-object v0
.end method
