.class public LX/2Of;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field private final a:LX/2Og;

.field public final b:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/DdR;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/user/model/UserKey;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 402822
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2Of;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2Og;Lcom/facebook/user/model/UserKey;)V
    .locals 7
    .param p2    # Lcom/facebook/user/model/UserKey;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserKey;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 402807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402808
    iput-object p1, p0, LX/2Of;->a:LX/2Og;

    .line 402809
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const/16 v1, 0x80

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 402810
    iget v2, v0, LX/0QN;->f:I

    const/4 v5, -0x1

    if-ne v2, v5, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "initial capacity was already set to %s"

    new-array v6, v3, [Ljava/lang/Object;

    iget p1, v0, LX/0QN;->f:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v4

    invoke-static {v2, v5, v6}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 402811
    if-ltz v1, :cond_1

    :goto_1
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 402812
    iput v1, v0, LX/0QN;->f:I

    .line 402813
    move-object v0, v0

    .line 402814
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/0QN;->b(I)LX/0QN;

    move-result-object v0

    .line 402815
    sget-object v1, LX/0QX;->SOFT:LX/0QX;

    invoke-virtual {v0, v1}, LX/0QN;->b(LX/0QX;)LX/0QN;

    move-result-object v1

    move-object v0, v1

    .line 402816
    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/2Of;->b:LX/0QI;

    .line 402817
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/2Of;->c:Ljava/util/Map;

    .line 402818
    iput-object p2, p0, LX/2Of;->d:Lcom/facebook/user/model/UserKey;

    .line 402819
    return-void

    :cond_0
    move v2, v4

    .line 402820
    goto :goto_0

    :cond_1
    move v3, v4

    .line 402821
    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/2Of;
    .locals 8

    .prologue
    .line 402703
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 402704
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 402705
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 402706
    if-nez v1, :cond_0

    .line 402707
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402708
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 402709
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 402710
    sget-object v1, LX/2Of;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 402711
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 402712
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 402713
    :cond_1
    if-nez v1, :cond_4

    .line 402714
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 402715
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 402716
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 402717
    new-instance p0, LX/2Of;

    invoke-static {v0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v1

    check-cast v1, LX/2Og;

    invoke-static {v0}, LX/2Ot;->b(LX/0QB;)Lcom/facebook/user/model/UserKey;

    move-result-object v7

    check-cast v7, Lcom/facebook/user/model/UserKey;

    invoke-direct {p0, v1, v7}, LX/2Of;-><init>(LX/2Og;Lcom/facebook/user/model/UserKey;)V

    .line 402718
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 402719
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 402720
    if-nez v1, :cond_2

    .line 402721
    sget-object v0, LX/2Of;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Of;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 402722
    :goto_1
    if-eqz v0, :cond_3

    .line 402723
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 402724
    :goto_3
    check-cast v0, LX/2Of;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 402725
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 402726
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 402727
    :catchall_1
    move-exception v0

    .line 402728
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 402729
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 402730
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 402731
    :cond_2
    :try_start_8
    sget-object v0, LX/2Of;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Of;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static c(Lcom/facebook/messaging/model/threads/ThreadSummary;)I
    .locals 1

    .prologue
    .line 402806
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method private d(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/DdR;
    .locals 11

    .prologue
    const/4 v5, 0x1

    .line 402744
    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 402745
    iget-object v0, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    .line 402746
    sget-object v2, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v0, v2, :cond_0

    .line 402747
    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_f

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 402748
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v6

    iget-object v7, p0, LX/2Of;->d:Lcom/facebook/user/model/UserKey;

    invoke-static {v6, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_e

    .line 402749
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 402750
    :goto_1
    move-object v0, v0

    .line 402751
    :goto_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 402752
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    if-ne v3, v5, :cond_8

    .line 402753
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 402754
    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 402755
    iget-object v4, p0, LX/2Of;->a:LX/2Og;

    invoke-virtual {v4, v1, v0}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/ParticipantInfo;)Ljava/lang/String;

    move-result-object v1

    .line 402756
    if-eqz v1, :cond_7

    .line 402757
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 402758
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 402759
    :goto_3
    new-instance v0, LX/DdR;

    iget-wide v4, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v4, v5, v1, v2}, LX/DdR;-><init>(JLX/0Px;LX/0Px;)V

    .line 402760
    :goto_4
    return-object v0

    .line 402761
    :cond_0
    const/4 v2, 0x0

    .line 402762
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    .line 402763
    iget-object v6, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->n:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v2

    :goto_5
    if-ge v3, v7, :cond_2

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 402764
    iget-object v8, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    iget-object v9, p0, LX/2Of;->d:Lcom/facebook/user/model/UserKey;

    invoke-static {v8, v9}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 402765
    iget-object v8, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-interface {v4, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402766
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 402767
    :cond_2
    new-instance v6, LX/0UE;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v6, v0}, LX/0UE;-><init>(Ljava/util/Collection;)V

    .line 402768
    iget-object v7, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v3, v2

    :goto_6
    if-ge v3, v8, :cond_3

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 402769
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 402770
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    .line 402771
    :cond_3
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 402772
    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    .line 402773
    :cond_4
    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    :goto_8
    if-ge v2, v6, :cond_6

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 402774
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v7

    iget-object v8, p0, LX/2Of;->d:Lcom/facebook/user/model/UserKey;

    invoke-static {v7, v8}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 402775
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v7

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-interface {v4, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402776
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 402777
    :cond_6
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 402778
    goto/16 :goto_2

    .line 402779
    :cond_7
    const-string v1, "ThreadDisplayCache"

    const-string v4, "ParticipantInfo [%s]"

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v1, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 402780
    :cond_8
    iget-object v3, p0, LX/2Of;->a:LX/2Og;

    const/4 v8, 0x0

    .line 402781
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 402782
    invoke-virtual {v3, v1}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v6

    .line 402783
    if-eqz v6, :cond_12

    .line 402784
    iget-object v4, v3, LX/2Og;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2Ou;

    .line 402785
    iget-object v7, v4, LX/2Ou;->e:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-nez v7, :cond_13

    .line 402786
    const/4 v7, 0x0

    .line 402787
    :goto_9
    move-object v4, v7

    .line 402788
    if-eqz v4, :cond_11

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_11

    move-object v7, v8

    .line 402789
    :goto_a
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_9
    :goto_b
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 402790
    if-nez v7, :cond_b

    move-object v6, v8

    .line 402791
    :goto_c
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_c

    .line 402792
    :cond_a
    :goto_d
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_9

    .line 402793
    invoke-virtual {v9, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 402794
    if-eqz v2, :cond_9

    .line 402795
    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_b

    .line 402796
    :cond_b
    iget-object v6, v4, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v6}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v7, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    goto :goto_c

    .line 402797
    :cond_c
    iget-object v6, v3, LX/2Og;->b:LX/2Oh;

    invoke-virtual {v6, v4}, LX/2Oh;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)Ljava/lang/String;

    move-result-object v6

    .line 402798
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 402799
    if-eqz v5, :cond_10

    iget-object v6, v4, Lcom/facebook/messaging/model/messages/ParticipantInfo;->d:Ljava/lang/String;

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_10

    .line 402800
    iget-object v6, v4, Lcom/facebook/messaging/model/messages/ParticipantInfo;->d:Ljava/lang/String;

    goto :goto_d

    .line 402801
    :cond_d
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v1, v4

    .line 402802
    new-instance v0, LX/DdR;

    iget-wide v4, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v4, v5, v2, v1}, LX/DdR;-><init>(JLX/0Px;LX/0Px;)V

    goto/16 :goto_4

    .line 402803
    :cond_e
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 402804
    :cond_f
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 402805
    goto/16 :goto_1

    :cond_10
    move-object v6, v8

    goto :goto_d

    :cond_11
    move-object v7, v4

    goto :goto_a

    :cond_12
    move-object v7, v8

    goto :goto_a

    :cond_13
    iget-object v7, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iget-object v7, v7, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    iget-object v10, v4, LX/2Ou;->a:LX/0lB;

    invoke-virtual {v7, v10}, Lcom/facebook/messaging/model/threads/NicknamesMap;->a(LX/0lC;)LX/0P1;

    move-result-object v7

    goto/16 :goto_9
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402739
    iget-object v0, p0, LX/2Of;->b:LX/0QI;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0, v1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdR;

    .line 402740
    if-eqz v0, :cond_0

    iget-wide v2, v0, LX/DdR;->a:J

    iget-wide v4, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 402741
    :cond_0
    invoke-direct {p0, p1}, LX/2Of;->d(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/DdR;

    move-result-object v0

    .line 402742
    iget-object v1, p0, LX/2Of;->b:LX/0QI;

    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v1, v2, v0}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 402743
    :cond_1
    iget-object v0, v0, LX/DdR;->c:LX/0Px;

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 402737
    iget-object v0, p0, LX/2Of;->b:LX/0QI;

    invoke-interface {v0}, LX/0QI;->a()V

    .line 402738
    return-void
.end method

.method public final a(Lcom/facebook/user/model/UserKey;J)V
    .locals 4

    .prologue
    .line 402732
    if-nez p1, :cond_1

    .line 402733
    :cond_0
    :goto_0
    return-void

    .line 402734
    :cond_1
    iget-object v0, p0, LX/2Of;->c:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 402735
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, p2

    if-gez v0, :cond_0

    .line 402736
    :cond_2
    iget-object v0, p0, LX/2Of;->c:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402698
    iget-object v0, p0, LX/2Of;->b:LX/0QI;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0, v1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdR;

    .line 402699
    if-eqz v0, :cond_0

    iget-wide v2, v0, LX/DdR;->a:J

    iget-wide v4, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 402700
    :cond_0
    invoke-direct {p0, p1}, LX/2Of;->d(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/DdR;

    move-result-object v0

    .line 402701
    iget-object v1, p0, LX/2Of;->b:LX/0QI;

    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v1, v2, v0}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 402702
    :cond_1
    iget-object v0, v0, LX/DdR;->b:LX/0Px;

    return-object v0
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 402696
    invoke-virtual {p0}, LX/2Of;->a()V

    .line 402697
    return-void
.end method
