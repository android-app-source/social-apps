.class public final LX/3I0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3GF;


# instance fields
.field public final synthetic a:LX/3Hz;


# direct methods
.method public constructor <init>(LX/3Hz;)V
    .locals 0

    .prologue
    .line 545394
    iput-object p1, p0, LX/3I0;->a:LX/3Hz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 545395
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 545396
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    move-result v0

    .line 545397
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/2oN;LX/2oN;LX/7M6;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 545398
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_1

    .line 545399
    :cond_0
    :goto_0
    return-void

    .line 545400
    :cond_1
    sget-object v0, LX/D7H;->a:[I

    invoke-virtual {p2}, LX/2oN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 545401
    :cond_2
    :goto_1
    sget-object v0, LX/D7H;->a:[I

    invoke-virtual {p1}, LX/2oN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 545402
    :cond_3
    :goto_2
    :pswitch_0
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2ow;

    invoke-direct {v1, p1, p2, p3}, LX/2ow;-><init>(LX/2oN;LX/2oN;LX/7M6;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0

    .line 545403
    :pswitch_1
    sget-object v0, LX/2oN;->NONE:LX/2oN;

    if-eq p1, v0, :cond_2

    .line 545404
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_COMMERCIAL_BREAK:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 545405
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 545406
    iput-boolean v4, v0, Lcom/facebook/video/player/RichVideoPlayer;->R:Z

    .line 545407
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->t:LX/3I5;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3I5;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_1

    .line 545408
    :pswitch_2
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/3I0;->a:LX/3Hz;

    iget-object v1, v1, LX/3Hz;->v:LX/2pa;

    sget-object v2, LX/3Hz;->r:LX/0Px;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2pa;LX/0Px;Z)V

    goto :goto_1

    .line 545409
    :pswitch_3
    sget-object v0, LX/2oN;->NONE:LX/2oN;

    if-eq p2, v0, :cond_3

    .line 545410
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_COMMERCIAL_BREAK:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 545411
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 545412
    iput-boolean v3, v0, Lcom/facebook/video/player/RichVideoPlayer;->R:Z

    .line 545413
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->t:LX/3I5;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v4, v2, v3}, LX/3I5;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 545414
    :pswitch_4
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v1, p3, LX/7M6;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 545415
    if-nez v1, :cond_5

    .line 545416
    :cond_4
    :goto_3
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/3I0;->a:LX/3Hz;

    iget-object v1, v1, LX/3Hz;->A:LX/2pa;

    sget-object v2, LX/3Hz;->r:LX/0Px;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2pa;LX/0Px;Z)V

    .line 545417
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->B()V

    .line 545418
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v1, p0, LX/3I0;->a:LX/3Hz;

    iget-object v1, v1, LX/3Hz;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 545419
    iput-wide v2, v0, LX/3Hz;->G:J

    .line 545420
    goto :goto_2

    .line 545421
    :cond_5
    iget-object v2, v0, LX/3Hz;->d:LX/3HR;

    invoke-virtual {v2, v1}, LX/3HR;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v2

    .line 545422
    if-eqz v2, :cond_4

    .line 545423
    iget-object v4, v0, LX/3Hz;->w:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1, v4, v2}, LX/3HR;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/video/engine/VideoPlayerParams;)LX/2pa;

    move-result-object v2

    iput-object v2, v0, LX/3Hz;->A:LX/2pa;

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 545424
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 545425
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->A()J

    move-result-wide v0

    .line 545426
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 545427
    iget-object v0, p0, LX/3I0;->a:LX/3Hz;

    const/4 v1, 0x0

    .line 545428
    iput-object v1, v0, LX/3Hz;->z:LX/D6v;

    .line 545429
    return-void
.end method

.method public final d()LX/BSW;
    .locals 1

    .prologue
    .line 545430
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 545431
    const/4 v0, 0x0

    return v0
.end method
