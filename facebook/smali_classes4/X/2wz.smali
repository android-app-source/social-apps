.class public final LX/2wz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/2wN;

.field public final b:LX/2x0;

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/content/ServiceConnection;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field public e:Z

.field public f:Landroid/os/IBinder;

.field public final g:LX/2wx;

.field public h:Landroid/content/ComponentName;


# direct methods
.method public constructor <init>(LX/2wN;LX/2wx;)V
    .locals 1

    iput-object p1, p0, LX/2wz;->a:LX/2wN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, LX/2wz;->g:LX/2wx;

    new-instance v0, LX/2x0;

    invoke-direct {v0, p0}, LX/2x0;-><init>(LX/2wz;)V

    iput-object v0, p0, LX/2wz;->b:LX/2x0;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2wz;->c:Ljava/util/Set;

    const/4 v0, 0x2

    iput v0, p0, LX/2wz;->d:I

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/ServiceConnection;)V
    .locals 8

    iget-object v0, p0, LX/2wz;->a:LX/2wN;

    iget-object v0, v0, LX/2wN;->d:LX/1ou;

    iget-object v1, p0, LX/2wz;->a:LX/2wN;

    iget-object v1, v1, LX/2wN;->b:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {p1}, LX/1ou;->a(Landroid/content/ServiceConnection;)Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x4

    move-object v2, v0

    move-object v3, v1

    move-object v6, v5

    invoke-static/range {v2 .. v7}, LX/1ou;->a(LX/1ou;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    iget-object v0, p0, LX/2wz;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Landroid/content/ServiceConnection;Ljava/lang/String;)V
    .locals 9

    iget-object v0, p0, LX/2wz;->a:LX/2wN;

    iget-object v0, v0, LX/2wN;->d:LX/1ou;

    iget-object v1, p0, LX/2wz;->a:LX/2wN;

    iget-object v1, v1, LX/2wN;->b:Landroid/content/Context;

    iget-object v2, p0, LX/2wz;->g:LX/2wx;

    invoke-virtual {v2}, LX/2wx;->a()Landroid/content/Intent;

    move-result-object v2

    invoke-static {p1}, LX/1ou;->a(Landroid/content/ServiceConnection;)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x3

    move-object v3, v0

    move-object v4, v1

    move-object v6, p2

    move-object v7, v2

    invoke-static/range {v3 .. v8}, LX/1ou;->a(LX/1ou;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    iget-object v0, p0, LX/2wz;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    const/4 v0, 0x3

    iput v0, p0, LX/2wz;->d:I

    iget-object v0, p0, LX/2wz;->a:LX/2wN;

    iget-object v0, v0, LX/2wN;->d:LX/1ou;

    iget-object v1, p0, LX/2wz;->a:LX/2wN;

    iget-object v1, v1, LX/2wN;->b:Landroid/content/Context;

    iget-object v2, p0, LX/2wz;->g:LX/2wx;

    invoke-virtual {v2}, LX/2wx;->a()Landroid/content/Intent;

    move-result-object v3

    iget-object v4, p0, LX/2wz;->b:LX/2x0;

    const/16 v5, 0x81

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, LX/1ou;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, LX/2wz;->e:Z

    iget-boolean v0, p0, LX/2wz;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x2

    iput v0, p0, LX/2wz;->d:I

    :try_start_0
    iget-object v0, p0, LX/2wz;->a:LX/2wN;

    iget-object v0, v0, LX/2wN;->d:LX/1ou;

    iget-object v1, p0, LX/2wz;->a:LX/2wN;

    iget-object v1, v1, LX/2wN;->b:Landroid/content/Context;

    iget-object v2, p0, LX/2wz;->b:LX/2x0;

    invoke-virtual {v0, v1, v2}, LX/1ou;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final b(Landroid/content/ServiceConnection;)Z
    .locals 1

    iget-object v0, p0, LX/2wz;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, LX/2wz;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    return v0
.end method
