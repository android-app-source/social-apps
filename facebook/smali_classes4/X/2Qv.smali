.class public LX/2Qv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/share/protocol/LinksPreviewParams;",
        "Lcom/facebook/share/model/LinksPreview;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0lC;


# direct methods
.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409101
    iput-object p1, p0, LX/2Qv;->a:LX/0lC;

    .line 409102
    return-void
.end method

.method public static b(LX/0QB;)LX/2Qv;
    .locals 2

    .prologue
    .line 409103
    new-instance v1, LX/2Qv;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-direct {v1, v0}, LX/2Qv;-><init>(LX/0lC;)V

    .line 409104
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 409105
    check-cast p1, Lcom/facebook/share/protocol/LinksPreviewParams;

    .line 409106
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 409107
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409108
    iget-object v0, p1, Lcom/facebook/share/protocol/LinksPreviewParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 409109
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "url"

    iget-object v2, p1, Lcom/facebook/share/protocol/LinksPreviewParams;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409110
    :cond_0
    iget-object v0, p1, Lcom/facebook/share/protocol/LinksPreviewParams;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 409111
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "id"

    iget-object v2, p1, Lcom/facebook/share/protocol/LinksPreviewParams;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409112
    :cond_1
    iget-object v0, p1, Lcom/facebook/share/protocol/LinksPreviewParams;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 409113
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "composer_session_id"

    iget-object v2, p1, Lcom/facebook/share/protocol/LinksPreviewParams;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409114
    :cond_2
    iget-object v0, p1, Lcom/facebook/share/protocol/LinksPreviewParams;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 409115
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "requested_sizes"

    iget-object v2, p0, LX/2Qv;->a:LX/0lC;

    iget-object v3, p1, Lcom/facebook/share/protocol/LinksPreviewParams;->d:LX/0Px;

    invoke-virtual {v2, v3}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409116
    :cond_3
    new-instance v0, LX/14N;

    const-string v1, "links.preview"

    const-string v2, "GET"

    const-string v3, "method/links.preview"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 409117
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 409118
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 409119
    invoke-virtual {v0}, LX/0lF;->c()LX/15w;

    move-result-object v0

    .line 409120
    iget-object v1, p0, LX/2Qv;->a:LX/0lC;

    invoke-virtual {v0, v1}, LX/15w;->a(LX/0lD;)V

    .line 409121
    const-class v1, Lcom/facebook/share/model/LinksPreview;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/LinksPreview;

    return-object v0
.end method
