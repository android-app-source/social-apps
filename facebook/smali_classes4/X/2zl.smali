.class public LX/2zl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2C2;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile sInstance__com_facebook_omnistore_mqtt_ConnectionStarter__INJECTED_BY_TemplateInjector:LX/2zl;


# instance fields
.field public mCallback:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

.field public final mChannelConnectivityTracker:LX/1MZ;

.field public mIsAppActive:Z

.field public final mIsEnabledInBackground:Z

.field public final mLocalBroadcastManager:LX/0Xl;

.field public mPendingConnect:Z

.field public final mUiThreadExecutor:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1MZ;LX/0Xl;LX/0Uh;LX/0Ot;)V
    .locals 2
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1MZ;",
            "LX/0Xl;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 483884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483885
    iput-boolean v0, p0, LX/2zl;->mIsAppActive:Z

    .line 483886
    iput-boolean v0, p0, LX/2zl;->mPendingConnect:Z

    .line 483887
    iput-object p1, p0, LX/2zl;->mChannelConnectivityTracker:LX/1MZ;

    .line 483888
    iput-object p2, p0, LX/2zl;->mLocalBroadcastManager:LX/0Xl;

    .line 483889
    const/16 v1, 0x5b

    invoke-virtual {p3, v1, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, LX/2zl;->mIsEnabledInBackground:Z

    .line 483890
    iput-object p4, p0, LX/2zl;->mUiThreadExecutor:LX/0Ot;

    .line 483891
    return-void
.end method

.method public static getInstance__com_facebook_omnistore_mqtt_ConnectionStarter__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2zl;
    .locals 7

    .prologue
    .line 483892
    sget-object v0, LX/2zl;->sInstance__com_facebook_omnistore_mqtt_ConnectionStarter__INJECTED_BY_TemplateInjector:LX/2zl;

    if-nez v0, :cond_1

    .line 483893
    const-class v1, LX/2zl;

    monitor-enter v1

    .line 483894
    :try_start_0
    sget-object v0, LX/2zl;->sInstance__com_facebook_omnistore_mqtt_ConnectionStarter__INJECTED_BY_TemplateInjector:LX/2zl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 483895
    if-eqz v2, :cond_0

    .line 483896
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 483897
    new-instance v6, LX/2zl;

    invoke-static {v0}, LX/1MZ;->a(LX/0QB;)LX/1MZ;

    move-result-object v3

    check-cast v3, LX/1MZ;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    const/16 p0, 0x1430

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/2zl;-><init>(LX/1MZ;LX/0Xl;LX/0Uh;LX/0Ot;)V

    .line 483898
    move-object v0, v6

    .line 483899
    sput-object v0, LX/2zl;->sInstance__com_facebook_omnistore_mqtt_ConnectionStarter__INJECTED_BY_TemplateInjector:LX/2zl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483900
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 483901
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 483902
    :cond_1
    sget-object v0, LX/2zl;->sInstance__com_facebook_omnistore_mqtt_ConnectionStarter__INJECTED_BY_TemplateInjector:LX/2zl;

    return-object v0

    .line 483903
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 483904
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public onAppActive()V
    .locals 3

    .prologue
    .line 483905
    iget-boolean v0, p0, LX/2zl;->mIsEnabledInBackground:Z

    if-nez v0, :cond_0

    .line 483906
    iget-object v0, p0, LX/2zl;->mUiThreadExecutor:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/omnistore/mqtt/ConnectionStarter$3;

    invoke-direct {v1, p0}, Lcom/facebook/omnistore/mqtt/ConnectionStarter$3;-><init>(LX/2zl;)V

    const v2, 0x40345b3d

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 483907
    :cond_0
    return-void
.end method

.method public onAppStopped()V
    .locals 3

    .prologue
    .line 483908
    iget-boolean v0, p0, LX/2zl;->mIsEnabledInBackground:Z

    if-nez v0, :cond_0

    .line 483909
    iget-object v0, p0, LX/2zl;->mUiThreadExecutor:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/omnistore/mqtt/ConnectionStarter$4;

    invoke-direct {v1, p0}, Lcom/facebook/omnistore/mqtt/ConnectionStarter$4;-><init>(LX/2zl;)V

    const v2, -0xfd18351

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 483910
    :cond_0
    return-void
.end method

.method public onDeviceActive()V
    .locals 0

    .prologue
    .line 483911
    return-void
.end method

.method public onDeviceStopped()V
    .locals 0

    .prologue
    .line 483912
    return-void
.end method
