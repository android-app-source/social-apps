.class public LX/2Eq;
.super LX/2En;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/ComponentName;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 386054
    invoke-direct {p0}, LX/2En;-><init>()V

    .line 386055
    iput-object p1, p0, LX/2Eq;->a:Landroid/content/Context;

    .line 386056
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/facebook/analytics2/logger/AlarmBasedUploadService;

    invoke-direct {v0, p1, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, LX/2Eq;->b:Landroid/content/ComponentName;

    .line 386057
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 386051
    iget-object v0, p0, LX/2Eq;->b:Landroid/content/ComponentName;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 386052
    iget-object v0, p0, LX/2Eq;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/facebook/analytics2/logger/AlarmBasedUploadService;->a(Landroid/content/Context;I)V

    .line 386053
    return-void
.end method

.method public final a(ILX/2DI;JJ)V
    .locals 9

    .prologue
    .line 386048
    iget-object v1, p0, LX/2Eq;->a:Landroid/content/Context;

    move v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-static/range {v1 .. v7}, Lcom/facebook/analytics2/logger/AlarmBasedUploadService;->a(Landroid/content/Context;ILX/2DI;JJ)V

    .line 386049
    return-void
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 386050
    const-wide v0, 0x7fffffffffffffffL

    return-wide v0
.end method
