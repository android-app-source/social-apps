.class public final LX/3Au;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Dw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/3Au;


# instance fields
.field private final b:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 526500
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "max_silence_frame_count"

    sget v2, LX/3Dx;->dI:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "enable_silence_restart"

    sget v2, LX/3Dx;->dH:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/3Au;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 526501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 526502
    iput-object p1, p0, LX/3Au;->b:LX/0ad;

    .line 526503
    return-void
.end method

.method public static a(LX/0QB;)LX/3Au;
    .locals 4

    .prologue
    .line 526504
    sget-object v0, LX/3Au;->c:LX/3Au;

    if-nez v0, :cond_1

    .line 526505
    const-class v1, LX/3Au;

    monitor-enter v1

    .line 526506
    :try_start_0
    sget-object v0, LX/3Au;->c:LX/3Au;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 526507
    if-eqz v2, :cond_0

    .line 526508
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 526509
    new-instance p0, LX/3Au;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/3Au;-><init>(LX/0ad;)V

    .line 526510
    move-object v0, p0

    .line 526511
    sput-object v0, LX/3Au;->c:LX/3Au;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 526512
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 526513
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 526514
    :cond_1
    sget-object v0, LX/3Au;->c:LX/3Au;

    return-object v0

    .line 526515
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 526516
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 526517
    sget-object v0, LX/3Au;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 526518
    if-nez v0, :cond_0

    .line 526519
    const/4 v0, 0x0

    .line 526520
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/3Au;->b:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v2, v3, v0, p2}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 526521
    const-string v0, "audio_silence_restart"

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 526522
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 526523
    iget-object v0, p0, LX/3Au;->b:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget v2, LX/3Dx;->dH:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(LX/0c0;I)V

    .line 526524
    return-void
.end method
