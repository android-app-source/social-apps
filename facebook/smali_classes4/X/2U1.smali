.class public LX/2U1;
.super LX/2U2;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2U2",
        "<",
        "Ljava/lang/String;",
        "LX/8Dk;",
        ">;",
        "LX/0c5;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2U1;


# instance fields
.field private c:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private d:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0rd;LX/0SG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415017
    const/16 v0, 0x80

    invoke-direct {p0, p1, p2, v0}, LX/2U2;-><init>(LX/0rd;LX/0SG;I)V

    .line 415018
    return-void
.end method

.method public static a(LX/0QB;)LX/2U1;
    .locals 5

    .prologue
    .line 415004
    sget-object v0, LX/2U1;->f:LX/2U1;

    if-nez v0, :cond_1

    .line 415005
    const-class v1, LX/2U1;

    monitor-enter v1

    .line 415006
    :try_start_0
    sget-object v0, LX/2U1;->f:LX/2U1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 415007
    if-eqz v2, :cond_0

    .line 415008
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 415009
    new-instance p0, LX/2U1;

    invoke-static {v0}, LX/0rZ;->a(LX/0QB;)LX/0rd;

    move-result-object v3

    check-cast v3, LX/0rd;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/2U1;-><init>(LX/0rd;LX/0SG;)V

    .line 415010
    move-object v0, p0

    .line 415011
    sput-object v0, LX/2U1;->f:LX/2U1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415012
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 415013
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 415014
    :cond_1
    sget-object v0, LX/2U1;->f:LX/2U1;

    return-object v0

    .line 415015
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 415016
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 415001
    monitor-enter p0

    :try_start_0
    iput p1, p0, LX/2U1;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415002
    monitor-exit p0

    return-void

    .line 415003
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)V
    .locals 1

    .prologue
    .line 414998
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, LX/2U1;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414999
    monitor-exit p0

    return-void

    .line 415000
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 414995
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/2U1;->e:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414996
    monitor-exit p0

    return-void

    .line 414997
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/Boolean;LX/0am;)V
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<+",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 414941
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 414942
    if-eqz v0, :cond_4

    .line 414943
    iget-object v1, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v1, v1

    .line 414944
    check-cast v1, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    invoke-static {v1}, LX/8De;->a(Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;)LX/8De;

    move-result-object v1

    move-object v2, v1

    .line 414945
    :goto_0
    iput-object p1, v2, LX/8De;->c:Ljava/lang/String;

    .line 414946
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 414947
    iput-object p2, v2, LX/8De;->d:Ljava/lang/String;

    .line 414948
    :cond_0
    if-eqz p3, :cond_1

    .line 414949
    invoke-static {p3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 414950
    iput-object v1, v2, LX/8De;->g:LX/0Px;

    .line 414951
    :cond_1
    if-eqz p5, :cond_2

    .line 414952
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    new-instance v3, LX/186;

    const/16 v4, 0x400

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v1}, LX/186;->a(IZ)V

    const v1, -0x171aa0d7

    invoke-static {v3, v1}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 414953
    invoke-virtual {v2, v3, v1}, LX/8De;->a(LX/15i;I)LX/8De;

    .line 414954
    :cond_2
    invoke-virtual {p6}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 414955
    invoke-virtual {p6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v3, LX/186;

    const/16 v4, 0x400

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    invoke-virtual {v3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v1}, LX/186;->b(II)V

    const v1, -0x4e30aecf

    invoke-static {v3, v1}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 414956
    invoke-virtual {v2, v3, v1}, LX/8De;->b(LX/15i;I)LX/8De;

    .line 414957
    :cond_3
    const/4 v1, 0x0

    .line 414958
    if-eqz p4, :cond_5

    .line 414959
    :goto_1
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 414960
    new-instance v0, LX/8Dk;

    invoke-virtual {v2}, LX/8De;->a()Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8Dk;-><init>(Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;)V

    .line 414961
    :goto_2
    invoke-virtual {p0, p1, v0}, LX/2U2;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414962
    monitor-exit p0

    return-void

    .line 414963
    :cond_4
    :try_start_1
    new-instance v1, LX/8De;

    invoke-direct {v1}, LX/8De;-><init>()V

    move-object v2, v1

    goto :goto_0

    .line 414964
    :cond_5
    if-eqz v0, :cond_7

    .line 414965
    iget-object v3, v0, LX/8Dk;->b:LX/0am;

    move-object v3, v3

    .line 414966
    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 414967
    iget-object v1, v0, LX/8Dk;->b:LX/0am;

    move-object v0, v1

    .line 414968
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object p4, v0

    goto :goto_1

    .line 414969
    :cond_6
    new-instance v0, LX/8Dk;

    invoke-virtual {v2}, LX/8De;->a()Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-result-object v1

    invoke-direct {v0, v1, p4}, LX/8Dk;-><init>(Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 414970
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    move-object p4, v1

    goto :goto_1
.end method

.method public final declared-synchronized a(Ljava/util/Iterator;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+",
            "LX/8Dk;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 414986
    monitor-enter p0

    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 414987
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 414988
    if-eqz v0, :cond_0

    .line 414989
    iget-object v1, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v1, v1

    .line 414990
    invoke-virtual {v1}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 414991
    iget-object v1, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v1, v1

    .line 414992
    invoke-virtual {v1}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0, p2, p3}, LX/2U2;->a(Ljava/lang/Object;Ljava/lang/Object;J)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 414993
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 414994
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 414983
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/2U2;->c(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414984
    monitor-exit p0

    return-void

    .line 414985
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/Boolean;LX/0am;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<+",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 414979
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/2U1;->a(Ljava/lang/String;)V

    .line 414980
    invoke-virtual/range {p0 .. p6}, LX/2U1;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/Boolean;LX/0am;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414981
    monitor-exit p0

    return-void

    .line 414982
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()I
    .locals 1

    .prologue
    .line 414978
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/2U1;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)LX/8Dk;
    .locals 2

    .prologue
    .line 414977
    monitor-enter p0

    const-wide/32 v0, 0x5265c00

    :try_start_0
    invoke-virtual {p0, p1, v0, v1}, LX/2U2;->a(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearUserData()V
    .locals 1

    .prologue
    .line 414973
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, LX/2U1;->c:I

    .line 414974
    invoke-virtual {p0}, LX/2U2;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414975
    monitor-exit p0

    return-void

    .line 414976
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()J
    .locals 2

    .prologue
    .line 414972
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/2U1;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 414971
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2U1;->e:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
