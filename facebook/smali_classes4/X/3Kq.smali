.class public LX/3Kq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3Kq;


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/0ae;


# direct methods
.method public constructor <init>(LX/0ad;LX/0ae;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 549241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549242
    iput-object p1, p0, LX/3Kq;->a:LX/0ad;

    .line 549243
    iput-object p2, p0, LX/3Kq;->b:LX/0ae;

    .line 549244
    return-void
.end method

.method public static a(LX/0QB;)LX/3Kq;
    .locals 5

    .prologue
    .line 549245
    sget-object v0, LX/3Kq;->c:LX/3Kq;

    if-nez v0, :cond_1

    .line 549246
    const-class v1, LX/3Kq;

    monitor-enter v1

    .line 549247
    :try_start_0
    sget-object v0, LX/3Kq;->c:LX/3Kq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 549248
    if-eqz v2, :cond_0

    .line 549249
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 549250
    new-instance p0, LX/3Kq;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ae;

    invoke-direct {p0, v3, v4}, LX/3Kq;-><init>(LX/0ad;LX/0ae;)V

    .line 549251
    move-object v0, p0

    .line 549252
    sput-object v0, LX/3Kq;->c:LX/3Kq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 549253
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 549254
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 549255
    :cond_1
    sget-object v0, LX/3Kq;->c:LX/3Kq;

    return-object v0

    .line 549256
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 549257
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
