.class public LX/3Ie;
.super LX/2oy;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/19m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/resources/ui/FbButton;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public e:LX/3Ee;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public f:Landroid/view/View$OnClickListener;

.field public n:LX/04G;

.field public o:LX/3If;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 546918
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3Ie;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 546919
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 546920
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3Ie;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546921
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 546922
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546923
    sget-object v0, LX/04G;->OTHERS:LX/04G;

    iput-object v0, p0, LX/3Ie;->n:LX/04G;

    .line 546924
    const-class v0, LX/3Ie;

    invoke-static {v0, p0}, LX/3Ie;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 546925
    const v0, 0x7f0315f8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 546926
    const v0, 0x7f0d3173

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/3Ie;->d:Lcom/facebook/resources/ui/FbButton;

    .line 546927
    new-instance v0, LX/3If;

    invoke-direct {v0, p0}, LX/3If;-><init>(LX/3Ie;)V

    iput-object v0, p0, LX/3Ie;->o:LX/3If;

    .line 546928
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    iget-object p1, p0, LX/3Ie;->o:LX/3If;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546929
    new-instance v0, LX/3Ee;

    invoke-direct {v0, p0}, LX/3Ee;-><init>(LX/3Ie;)V

    iput-object v0, p0, LX/3Ie;->e:LX/3Ee;

    .line 546930
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    iget-object p1, p0, LX/3Ie;->e:LX/3Ee;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546931
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3Ie;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v2

    check-cast v2, LX/1C2;

    invoke-static {p0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object p0

    check-cast p0, LX/19m;

    iput-object v1, p1, LX/3Ie;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object v2, p1, LX/3Ie;->b:LX/1C2;

    iput-object p0, p1, LX/3Ie;->c:LX/19m;

    return-void
.end method

.method public static b(LX/3Ie;LX/2pa;LX/04G;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 546932
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3Ie;->c:LX/19m;

    .line 546933
    iget-boolean v3, v0, LX/19m;->w:Z

    move v0, v3

    .line 546934
    if-nez v0, :cond_2

    move v0, v1

    .line 546935
    :goto_0
    invoke-virtual {p0}, LX/3Ie;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 546936
    sget-object v4, LX/7Ne;->b:Ljava/util/List;

    if-nez v4, :cond_0

    .line 546937
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    sput-object v4, LX/7Ne;->b:Ljava/util/List;

    .line 546938
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 546939
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.oculus.cinema.action.CAST"

    invoke-virtual {v5, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    const-string v3, "video/vr"

    invoke-virtual {v5, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 546940
    const/high16 v3, 0x10000

    invoke-virtual {v4, v5, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v5

    .line 546941
    if-eqz v5, :cond_5

    const/4 v5, 0x1

    :goto_1
    move v4, v5

    .line 546942
    if-eqz v4, :cond_0

    .line 546943
    sget-object v4, LX/7Ne;->b:Ljava/util/List;

    sget-object v5, LX/7D1;->GEAR_VR:LX/7D1;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546944
    :cond_0
    sget-object v4, LX/7Ne;->b:Ljava/util/List;

    move-object v3, v4

    .line 546945
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v0, :cond_3

    .line 546946
    :cond_1
    iget-object v0, p0, LX/3Ie;->d:Lcom/facebook/resources/ui/FbButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 546947
    invoke-virtual {p0}, LX/2oy;->im_()V

    move v1, v2

    .line 546948
    :goto_2
    return v1

    :cond_2
    move v0, v2

    .line 546949
    goto :goto_0

    .line 546950
    :cond_3
    iget-object v0, p0, LX/3Ie;->f:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_4

    .line 546951
    new-instance v0, LX/7ON;

    invoke-direct {v0, p0, p1}, LX/7ON;-><init>(LX/3Ie;LX/2pa;)V

    iput-object v0, p0, LX/3Ie;->f:Landroid/view/View$OnClickListener;

    .line 546952
    :cond_4
    iget-object v0, p0, LX/3Ie;->d:Lcom/facebook/resources/ui/FbButton;

    iget-object v2, p0, LX/3Ie;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 546953
    iput-object p2, p0, LX/3Ie;->n:LX/04G;

    goto :goto_2

    :cond_5
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 546954
    iget-object v0, p0, LX/3Ie;->f:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 546955
    iget-object v0, p0, LX/3Ie;->d:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, LX/3Ie;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 546956
    :cond_0
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    invoke-static {p0, p1, v0}, LX/3Ie;->b(LX/3Ie;LX/2pa;LX/04G;)Z

    .line 546957
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 546958
    invoke-super {p0}, LX/2oy;->d()V

    .line 546959
    iget-object v0, p0, LX/3Ie;->d:Lcom/facebook/resources/ui/FbButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 546960
    return-void
.end method
