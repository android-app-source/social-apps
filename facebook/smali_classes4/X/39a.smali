.class public LX/39a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/1dr;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 523300
    sget-object v0, LX/1vY;->FEEDBACK_SECTION:LX/1vY;

    invoke-static {v0}, LX/1vZ;->a(LX/1vY;)Landroid/util/SparseArray;

    move-result-object v0

    sput-object v0, LX/39a;->a:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(LX/1dr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 523301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 523302
    iput-object p1, p0, LX/39a;->b:LX/1dr;

    .line 523303
    return-void
.end method

.method public static a(LX/0QB;)LX/39a;
    .locals 4

    .prologue
    .line 523304
    const-class v1, LX/39a;

    monitor-enter v1

    .line 523305
    :try_start_0
    sget-object v0, LX/39a;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 523306
    sput-object v2, LX/39a;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 523307
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523308
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 523309
    new-instance p0, LX/39a;

    invoke-static {v0}, LX/1dr;->a(LX/0QB;)LX/1dr;

    move-result-object v3

    check-cast v3, LX/1dr;

    invoke-direct {p0, v3}, LX/39a;-><init>(LX/1dr;)V

    .line 523310
    move-object v0, p0

    .line 523311
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 523312
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/39a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 523313
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 523314
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
