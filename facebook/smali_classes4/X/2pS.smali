.class public final LX/2pS;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V
    .locals 1

    .prologue
    .line 468344
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 468345
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2pS;->a:Ljava/lang/ref/WeakReference;

    .line 468346
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 468347
    iget-object v0, p0, LX/2pS;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    .line 468348
    if-eqz v0, :cond_0

    iget-object v1, v0, LX/2oy;->j:LX/2pb;

    if-eqz v1, :cond_0

    .line 468349
    iget-object v1, v0, LX/2oy;->j:LX/2pb;

    .line 468350
    iget-object v2, v1, LX/2pb;->y:LX/2qV;

    move-object v1, v2

    .line 468351
    sget-object v2, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v0, v1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->a$redex0(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;Z)V

    .line 468352
    :cond_0
    return-void

    .line 468353
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
