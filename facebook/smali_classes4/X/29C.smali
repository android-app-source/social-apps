.class public LX/29C;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0Tf;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0Tf;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 375915
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0Tf;
    .locals 4

    .prologue
    .line 375916
    sget-object v0, LX/29C;->a:LX/0Tf;

    if-nez v0, :cond_1

    .line 375917
    const-class v1, LX/29C;

    monitor-enter v1

    .line 375918
    :try_start_0
    sget-object v0, LX/29C;->a:LX/0Tf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 375919
    if-eqz v2, :cond_0

    .line 375920
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 375921
    const-class v3, LX/28q;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/28q;

    invoke-static {v0}, LX/1rR;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object p0

    check-cast p0, Landroid/os/Handler;

    invoke-static {v3, p0}, LX/2C7;->a(LX/28q;Landroid/os/Handler;)LX/0Tf;

    move-result-object v3

    move-object v0, v3

    .line 375922
    sput-object v0, LX/29C;->a:LX/0Tf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375923
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 375924
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 375925
    :cond_1
    sget-object v0, LX/29C;->a:LX/0Tf;

    return-object v0

    .line 375926
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 375927
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 375928
    const-class v0, LX/28q;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/28q;

    invoke-static {p0}, LX/1rR;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    invoke-static {v0, v1}, LX/2C7;->a(LX/28q;Landroid/os/Handler;)LX/0Tf;

    move-result-object v0

    return-object v0
.end method
