.class public final LX/2t8;
.super LX/2tA;
.source ""


# instance fields
.field public final d:Landroid/view/View;

.field public final e:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

.field public final f:Z

.field public final synthetic g:LX/AlV;


# direct methods
.method public constructor <init>(LX/AlV;Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 474739
    iput-object p1, p0, LX/2t8;->g:LX/AlV;

    .line 474740
    invoke-direct {p0, p1, p3, p4}, LX/2tA;-><init>(LX/AlU;Landroid/view/View;Z)V

    .line 474741
    iput-object p3, p0, LX/2t8;->d:Landroid/view/View;

    .line 474742
    iput-object p2, p0, LX/2t8;->e:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    .line 474743
    iput-boolean p4, p0, LX/2t8;->f:Z

    .line 474744
    return-void
.end method


# virtual methods
.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    .line 474745
    iget-boolean v0, p0, LX/2t8;->f:Z

    if-eqz v0, :cond_0

    .line 474746
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 474747
    iget-object v1, p0, LX/2t8;->g:LX/AlV;

    iget-object v1, v1, LX/AlV;->a:LX/Alf;

    iget-object v2, p0, LX/2t8;->e:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    .line 474748
    iget-object v3, v2, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->h:LX/1lR;

    move-object v2, v3

    .line 474749
    invoke-virtual {v2}, LX/1lR;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/Alf;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 474750
    iget-object v1, p0, LX/2t8;->g:LX/AlV;

    iget-object v1, v1, LX/AlV;->b:LX/1kG;

    invoke-virtual {v1}, LX/1kG;->c()V

    .line 474751
    iget-object v1, p0, LX/2t8;->e:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->setSessionID(Ljava/lang/String;)V

    .line 474752
    iget-object v0, p0, LX/2t8;->e:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->setVisibility(I)V

    .line 474753
    :cond_0
    invoke-super {p0, p1}, LX/2tA;->onAnimationStart(Landroid/animation/Animator;)V

    .line 474754
    return-void
.end method
