.class public LX/2zR;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2zR;


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 483408
    invoke-direct {p0}, LX/398;-><init>()V

    .line 483409
    const-string v0, "appcenter/mobile_canvas/f?href={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mobile_page"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 483410
    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/398;->a(Ljava/lang/String;LX/0Or;Landroid/os/Bundle;)V

    .line 483411
    return-void
.end method

.method public static a(LX/0QB;)LX/2zR;
    .locals 4

    .prologue
    .line 483412
    sget-object v0, LX/2zR;->a:LX/2zR;

    if-nez v0, :cond_1

    .line 483413
    const-class v1, LX/2zR;

    monitor-enter v1

    .line 483414
    :try_start_0
    sget-object v0, LX/2zR;->a:LX/2zR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 483415
    if-eqz v2, :cond_0

    .line 483416
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 483417
    new-instance v3, LX/2zR;

    const/16 p0, 0xc

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2zR;-><init>(LX/0Or;)V

    .line 483418
    move-object v0, v3

    .line 483419
    sput-object v0, LX/2zR;->a:LX/2zR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483420
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 483421
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 483422
    :cond_1
    sget-object v0, LX/2zR;->a:LX/2zR;

    return-object v0

    .line 483423
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 483424
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
