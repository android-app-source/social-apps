.class public LX/2JX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/30V;


# instance fields
.field public b:LX/10M;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0WJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Uh;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutBadgePollConditionalWorker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 393040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393041
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 393039
    iget-object v1, p0, LX/2JX;->c:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->b()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/2JX;->b:LX/10M;

    invoke-virtual {v1}, LX/10M;->d()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, LX/2JX;->d:LX/0Uh;

    const/16 v2, 0x8

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b()LX/2Jm;
    .locals 1

    .prologue
    .line 393038
    sget-object v0, LX/2Jm;->INTERVAL:LX/2Jm;

    return-object v0
.end method

.method public final c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<+",
            "LX/2Jw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 393042
    iget-object v0, p0, LX/2JX;->e:LX/0Or;

    return-object v0
.end method

.method public final d()LX/2Jl;
    .locals 2

    .prologue
    .line 393037
    new-instance v0, LX/2Jk;

    invoke-direct {v0}, LX/2Jk;-><init>()V

    sget-object v1, LX/2Im;->CONNECTED:LX/2Im;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Im;)LX/2Jk;

    move-result-object v0

    invoke-virtual {v0}, LX/2Jk;->a()LX/2Jl;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 393036
    const-wide/32 v0, 0x6ddd00

    return-wide v0
.end method
