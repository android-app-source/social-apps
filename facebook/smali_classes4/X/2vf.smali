.class public LX/2vf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field private static final f:LX/0Tn;

.field private static final g:LX/0Tn;

.field private static final h:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 478381
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "contacts_ccu_upload/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2vf;->f:LX/0Tn;

    .line 478382
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "contacts_ccu_persist/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2vf;->g:LX/0Tn;

    .line 478383
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "contacts_persist/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2vf;->h:LX/0Tn;

    .line 478384
    sget-object v0, LX/2vf;->f:LX/0Tn;

    const-string v1, "last_upload_success_timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2vf;->a:LX/0Tn;

    .line 478385
    sget-object v0, LX/2vf;->f:LX/0Tn;

    const-string v1, "last_upload_client_root_hash"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2vf;->b:LX/0Tn;

    .line 478386
    sget-object v0, LX/2vf;->g:LX/0Tn;

    const-string v1, "scoped_continuous_upload_status"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2vf;->c:LX/0Tn;

    .line 478387
    sget-object v0, LX/2vf;->h:LX/0Tn;

    const-string v1, "continuous_import"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2vf;->d:LX/0Tn;

    .line 478388
    sget-object v0, LX/2vf;->f:LX/0Tn;

    const-string v1, "synced_ccu_setting_with_server"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2vf;->e:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 478389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478390
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 478391
    sget-object v0, LX/2vf;->f:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
