.class public LX/2zU;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2zU;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 483464
    invoke-direct {p0}, LX/398;-><init>()V

    .line 483465
    sget-object v0, LX/0ax;->gE:Ljava/lang/String;

    const-class v1, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 483466
    sget-object v0, LX/0ax;->gF:Ljava/lang/String;

    const-class v1, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 483467
    const-string v0, "data_savings_mode_settings/?source=bar"

    const-class v1, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 483468
    return-void
.end method

.method public static a(LX/0QB;)LX/2zU;
    .locals 3

    .prologue
    .line 483469
    sget-object v0, LX/2zU;->a:LX/2zU;

    if-nez v0, :cond_1

    .line 483470
    const-class v1, LX/2zU;

    monitor-enter v1

    .line 483471
    :try_start_0
    sget-object v0, LX/2zU;->a:LX/2zU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 483472
    if-eqz v2, :cond_0

    .line 483473
    :try_start_1
    new-instance v0, LX/2zU;

    invoke-direct {v0}, LX/2zU;-><init>()V

    .line 483474
    move-object v0, v0

    .line 483475
    sput-object v0, LX/2zU;->a:LX/2zU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483476
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 483477
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 483478
    :cond_1
    sget-object v0, LX/2zU;->a:LX/2zU;

    return-object v0

    .line 483479
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 483480
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
