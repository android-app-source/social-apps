.class public final LX/3DN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 534242
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 534243
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 534244
    :goto_0
    return v1

    .line 534245
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 534246
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 534247
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 534248
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 534249
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 534250
    const-string v7, "friendship_status"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 534251
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 534252
    :cond_2
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 534253
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 534254
    :cond_3
    const-string v7, "name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 534255
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 534256
    :cond_4
    const-string v7, "profile_picture"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 534257
    const/4 v6, 0x0

    .line 534258
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v7, :cond_b

    .line 534259
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 534260
    :goto_2
    move v2, v6

    .line 534261
    goto :goto_1

    .line 534262
    :cond_5
    const-string v7, "social_context"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 534263
    const/4 v6, 0x0

    .line 534264
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v7, :cond_f

    .line 534265
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 534266
    :goto_3
    move v0, v6

    .line 534267
    goto :goto_1

    .line 534268
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 534269
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 534270
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 534271
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 534272
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 534273
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 534274
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 534275
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 534276
    :cond_9
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 534277
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 534278
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 534279
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_9

    if-eqz v7, :cond_9

    .line 534280
    const-string v8, "uri"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 534281
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_4

    .line 534282
    :cond_a
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 534283
    invoke-virtual {p1, v6, v2}, LX/186;->b(II)V

    .line 534284
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_2

    :cond_b
    move v2, v6

    goto :goto_4

    .line 534285
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 534286
    :cond_d
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_e

    .line 534287
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 534288
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 534289
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_d

    if-eqz v7, :cond_d

    .line 534290
    const-string v8, "text"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 534291
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 534292
    :cond_e
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 534293
    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 534294
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_f
    move v0, v6

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 534295
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 534296
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 534297
    if-eqz v0, :cond_0

    .line 534298
    const-string v0, "friendship_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 534299
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 534300
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 534301
    if-eqz v0, :cond_1

    .line 534302
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 534303
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 534304
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 534305
    if-eqz v0, :cond_2

    .line 534306
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 534307
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 534308
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 534309
    if-eqz v0, :cond_4

    .line 534310
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 534311
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 534312
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 534313
    if-eqz v1, :cond_3

    .line 534314
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 534315
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 534316
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 534317
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 534318
    if-eqz v0, :cond_6

    .line 534319
    const-string v1, "social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 534320
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 534321
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 534322
    if-eqz v1, :cond_5

    .line 534323
    const-string p1, "text"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 534324
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 534325
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 534326
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 534327
    return-void
.end method
