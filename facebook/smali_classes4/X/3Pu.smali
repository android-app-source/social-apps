.class public LX/3Pu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3Pu;


# instance fields
.field public final a:Ljava/lang/Boolean;

.field public final b:LX/0So;

.field public final c:LX/0ZL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0ZL",
            "<",
            "LX/0ki;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;LX/0So;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsInternalBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562316
    iput-object p1, p0, LX/3Pu;->a:Ljava/lang/Boolean;

    .line 562317
    iput-object p2, p0, LX/3Pu;->b:LX/0So;

    .line 562318
    new-instance v0, LX/0ZL;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, LX/0ZL;-><init>(I)V

    iput-object v0, p0, LX/3Pu;->c:LX/0ZL;

    .line 562319
    return-void
.end method

.method public static a(LX/0QB;)LX/3Pu;
    .locals 5

    .prologue
    .line 562320
    sget-object v0, LX/3Pu;->d:LX/3Pu;

    if-nez v0, :cond_1

    .line 562321
    const-class v1, LX/3Pu;

    monitor-enter v1

    .line 562322
    :try_start_0
    sget-object v0, LX/3Pu;->d:LX/3Pu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 562323
    if-eqz v2, :cond_0

    .line 562324
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 562325
    new-instance p0, LX/3Pu;

    invoke-static {v0}, LX/0XR;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/3Pu;-><init>(Ljava/lang/Boolean;LX/0So;)V

    .line 562326
    move-object v0, p0

    .line 562327
    sput-object v0, LX/3Pu;->d:LX/3Pu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 562328
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 562329
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 562330
    :cond_1
    sget-object v0, LX/3Pu;->d:LX/3Pu;

    return-object v0

    .line 562331
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 562332
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
