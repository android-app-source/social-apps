.class public LX/2Rv;
.super LX/2QZ;
.source ""


# instance fields
.field private b:LX/2QQ;


# direct methods
.method public constructor <init>(LX/2QQ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410341
    const-string v0, "platform_webdialogs_save_manifest"

    invoke-direct {p0, v0}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 410342
    iput-object p1, p0, LX/2Rv;->b:LX/2QQ;

    .line 410343
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 410344
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 410345
    const-string v1, "platform_webdialogs_save_parcel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 410346
    iget-object v1, p0, LX/2Rv;->b:LX/2QQ;

    .line 410347
    iget-object v2, v1, LX/2QQ;->t:LX/0Sh;

    const-string v3, "This method will perform disk I/O and should not be called on the UI thread"

    invoke-virtual {v2, v3}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 410348
    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;

    if-nez v2, :cond_1

    .line 410349
    :cond_0
    :goto_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 410350
    return-object v0

    .line 410351
    :cond_1
    iget-object v2, v1, LX/2QQ;->p:LX/2QR;

    sget-object v3, LX/2QS;->i:LX/0Tn;

    const-string v4, "PlatformWebDialogsManifest"

    invoke-virtual {v2, v3, v4, v0}, LX/2QR;->a(LX/0Tn;Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v2

    .line 410352
    if-eqz v2, :cond_2

    .line 410353
    iget-object v2, v1, LX/2QQ;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/2QS;->j:LX/0Tn;

    iget-wide v4, v1, LX/2QQ;->d:J

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 410354
    :cond_2
    iget-object v2, v1, LX/2QQ;->p:LX/2QR;

    invoke-virtual {v2}, LX/2QR;->f()V

    goto :goto_0
.end method
