.class public LX/3Ep;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field public a:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "LX/1mx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 538013
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538014
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/3Ep;->d:LX/01J;

    .line 538015
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/3Ep;->a:LX/01J;

    .line 538016
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/3Ep;->b:LX/01J;

    .line 538017
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/3Ep;->c:LX/01J;

    .line 538018
    return-void
.end method

.method public static b(LX/3Ep;LX/1my;)V
    .locals 5

    .prologue
    .line 538019
    const/4 v0, 0x0

    iget-object v1, p0, LX/3Ep;->a:LX/01J;

    invoke-virtual {v1}, LX/01J;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 538020
    iget-object v0, p0, LX/3Ep;->a:LX/01J;

    invoke-virtual {v0, v1}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 538021
    iget-object v3, p1, LX/1my;->a:LX/01J;

    invoke-virtual {v3, v0}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1mx;

    move-object v3, v3

    .line 538022
    if-nez v3, :cond_0

    .line 538023
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Transition not defined for transitionKey: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 538024
    :cond_0
    iget-object v4, p0, LX/3Ep;->d:LX/01J;

    invoke-virtual {v4, v0, v3}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538025
    new-instance v4, LX/48K;

    invoke-direct {v4, p0, v3, v0}, LX/48K;-><init>(LX/3Ep;LX/1mx;Ljava/lang/String;)V

    .line 538026
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 538027
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 538028
    iget-object v0, p0, LX/3Ep;->d:LX/01J;

    invoke-virtual {v0}, LX/01J;->size()I

    .line 538029
    iget-object v0, p0, LX/3Ep;->c:LX/01J;

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 538030
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The KeyToViewsAfterMount map should have been cleared after processing the previous mount state."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 538031
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 538032
    iget-object v0, p0, LX/3Ep;->c:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 538033
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Duplicate transition key detected: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 538034
    :cond_0
    iget-object v0, p0, LX/3Ep;->c:LX/01J;

    invoke-virtual {v0, p1, p2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538035
    return-void
.end method
