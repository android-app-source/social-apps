.class public LX/3CY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/3Ca;

.field public final b:I

.field public final c:I

.field public final d:I


# direct methods
.method public constructor <init>(LX/3Ca;III)V
    .locals 0

    .prologue
    .line 530240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530241
    iput-object p1, p0, LX/3CY;->a:LX/3Ca;

    .line 530242
    iput p4, p0, LX/3CY;->b:I

    .line 530243
    iput p2, p0, LX/3CY;->c:I

    .line 530244
    iput p3, p0, LX/3CY;->d:I

    .line 530245
    return-void
.end method

.method public static a(II)LX/3CY;
    .locals 1

    .prologue
    .line 530239
    new-instance v0, LX/3CZ;

    invoke-direct {v0, p0, p1}, LX/3CZ;-><init>(II)V

    return-object v0
.end method

.method public static a(III)LX/3CY;
    .locals 1

    .prologue
    .line 530238
    new-instance v0, LX/5Mf;

    invoke-direct {v0, p0, p1, p2}, LX/5Mf;-><init>(III)V

    return-object v0
.end method

.method public static b(II)LX/3CY;
    .locals 1

    .prologue
    .line 530246
    new-instance v0, LX/5Mc;

    invoke-direct {v0, p0, p1}, LX/5Mc;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 530237
    iget v0, p0, LX/3CY;->c:I

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 530236
    iget v0, p0, LX/3CY;->d:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 530231
    if-ne p0, p1, :cond_1

    .line 530232
    :cond_0
    :goto_0
    return v0

    .line 530233
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 530234
    :cond_3
    check-cast p1, LX/3CY;

    .line 530235
    iget v2, p0, LX/3CY;->c:I

    iget v3, p1, LX/3CY;->c:I

    if-ne v2, v3, :cond_4

    iget v2, p0, LX/3CY;->d:I

    iget v3, p1, LX/3CY;->d:I

    if-ne v2, v3, :cond_4

    iget v2, p0, LX/3CY;->b:I

    iget v3, p1, LX/3CY;->b:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, LX/3CY;->a:LX/3Ca;

    iget-object v3, p1, LX/3CY;->a:LX/3Ca;

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 530230
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/3CY;->a:LX/3Ca;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, LX/3CY;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, LX/3CY;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, LX/3CY;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 530229
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "changeType"

    iget-object v2, p0, LX/3CY;->a:LX/3Ca;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mPreviousStateIndex"

    iget v2, p0, LX/3CY;->c:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "mNewStateIndex"

    iget v2, p0, LX/3CY;->d:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "itemCount"

    iget v2, p0, LX/3CY;->b:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
