.class public final LX/3Du;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/20Z;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Z

.field public final synthetic c:LX/1PT;

.field public final synthetic d:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;ZLX/1PT;)V
    .locals 0

    .prologue
    .line 536252
    iput-object p1, p0, LX/3Du;->d:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    iput-object p2, p0, LX/3Du;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-boolean p3, p0, LX/3Du;->b:Z

    iput-object p4, p0, LX/3Du;->c:LX/1PT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/20X;)V
    .locals 6

    .prologue
    .line 536253
    iget-object v0, p0, LX/3Du;->d:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/And;

    iget-object v1, p0, LX/3Du;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 536254
    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    .line 536255
    instance-of v2, v3, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    if-nez v2, :cond_0

    instance-of v2, v3, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;

    if-nez v2, :cond_0

    instance-of v2, v3, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    if-eqz v2, :cond_1

    .line 536256
    :cond_0
    invoke-static {v3}, LX/And;->a(Lcom/facebook/flatbuffers/Flattenable;)Ljava/lang/String;

    move-result-object v2

    .line 536257
    if-eqz v2, :cond_1

    .line 536258
    invoke-static {v3}, LX/And;->a(Lcom/facebook/flatbuffers/Flattenable;)Ljava/lang/String;

    move-result-object v4

    .line 536259
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 536260
    check-cast v2, LX/16h;

    check-cast v3, LX/16h;

    invoke-static {v2, v3}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v2

    invoke-static {v4, v2}, LX/17Q;->d(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 536261
    iget-object v3, v0, LX/And;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 536262
    :cond_1
    iget-object v0, p0, LX/3Du;->d:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->c:LX/36F;

    iget-object v1, p0, LX/3Du;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-boolean v4, p0, LX/3Du;->b:Z

    iget-object v5, p0, LX/3Du;->c:LX/1PT;

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LX/36F;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/20X;ZLX/1PT;)V

    .line 536263
    return-void
.end method
