.class public LX/3Fd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3Fd;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 539776
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 539777
    return-void
.end method

.method public static a(LX/0QB;)LX/3Fd;
    .locals 4

    .prologue
    .line 539778
    sget-object v0, LX/3Fd;->b:LX/3Fd;

    if-nez v0, :cond_1

    .line 539779
    const-class v1, LX/3Fd;

    monitor-enter v1

    .line 539780
    :try_start_0
    sget-object v0, LX/3Fd;->b:LX/3Fd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 539781
    if-eqz v2, :cond_0

    .line 539782
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 539783
    new-instance p0, LX/3Fd;

    invoke-direct {p0}, LX/3Fd;-><init>()V

    .line 539784
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 539785
    iput-object v3, p0, LX/3Fd;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 539786
    move-object v0, p0

    .line 539787
    sput-object v0, LX/3Fd;->b:LX/3Fd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 539788
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 539789
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 539790
    :cond_1
    sget-object v0, LX/3Fd;->b:LX/3Fd;

    return-object v0

    .line 539791
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 539792
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 539793
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 539794
    :cond_0
    :goto_0
    return v0

    .line 539795
    :cond_1
    iget-object v1, p0, LX/3Fd;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Kr;->ab:LX/0Tn;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 539796
    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 539797
    const/4 v0, 0x1

    goto :goto_0
.end method
