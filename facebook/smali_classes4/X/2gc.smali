.class public LX/2gc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0xB;

.field public final b:LX/2gd;

.field public final c:LX/2PR;


# direct methods
.method public constructor <init>(LX/0xB;LX/2gd;LX/2PR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 448725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448726
    iput-object p1, p0, LX/2gc;->a:LX/0xB;

    .line 448727
    iput-object p2, p0, LX/2gc;->b:LX/2gd;

    .line 448728
    iput-object p3, p0, LX/2gc;->c:LX/2PR;

    .line 448729
    return-void
.end method

.method public static b(LX/0QB;)LX/2gc;
    .locals 4

    .prologue
    .line 448730
    new-instance v3, LX/2gc;

    invoke-static {p0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v0

    check-cast v0, LX/0xB;

    invoke-static {p0}, LX/2gd;->b(LX/0QB;)LX/2gd;

    move-result-object v1

    check-cast v1, LX/2gd;

    invoke-static {p0}, LX/2PR;->a(LX/0QB;)LX/2PR;

    move-result-object v2

    check-cast v2, LX/2PR;

    invoke-direct {v3, v0, v1, v2}, LX/2gc;-><init>(LX/0xB;LX/2gd;LX/2PR;)V

    .line 448731
    return-object v3
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 448732
    iget-object v0, p0, LX/2gc;->a:LX/0xB;

    sget-object v1, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v0, v1, p1}, LX/0xB;->a(LX/12j;I)V

    .line 448733
    return-void
.end method
