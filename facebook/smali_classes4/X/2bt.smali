.class public LX/2bt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 439931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 439932
    return-void
.end method

.method public static a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 439919
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v0

    if-nez v0, :cond_0

    .line 439920
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 439921
    invoke-virtual {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/186;)I

    move-result v1

    .line 439922
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 439923
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    .line 439924
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 439925
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 439926
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 439927
    const-string v1, "SerializerHelpers.getOrCreateMutableFlatBuffer"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    .line 439928
    invoke-virtual {v0}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {v1}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 439929
    new-instance v1, LX/2bu;

    invoke-direct {v1, v0, v2}, LX/2bu;-><init>(LX/15i;I)V

    move-object v0, v1

    .line 439930
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/2bu;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/2bu;-><init>(LX/15i;I)V

    goto :goto_0
.end method

.method public static a(LX/15i;IILX/0nX;)V
    .locals 5

    .prologue
    .line 439913
    invoke-virtual {p0, p1, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 439914
    invoke-static {v0}, LX/38I;->a(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 439915
    invoke-static {v0, p3}, LX/2bt;->a(Ljava/lang/String;LX/0nX;)V

    .line 439916
    :goto_0
    return-void

    .line 439917
    :cond_0
    const-string v1, "SerializerHelpers"

    const-string v2, "Unrecognized graphql object type: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 439918
    invoke-virtual {p3}, LX/0nX;->h()V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;LX/0nX;)V
    .locals 1

    .prologue
    .line 439933
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 439934
    const-string v0, "name"

    invoke-virtual {p1, v0, p0}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 439935
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 439936
    return-void
.end method

.method public static a(Ljava/util/Iterator;LX/0nX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0nX;",
            ")V"
        }
    .end annotation

    .prologue
    .line 439908
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 439909
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439910
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 439911
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 439912
    return-void
.end method

.method public static b(LX/15i;IILX/0nX;)V
    .locals 5

    .prologue
    .line 439898
    const-class v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {p0, p1, p2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 439899
    if-nez v0, :cond_0

    .line 439900
    const-string v0, "SerializerHelpers"

    const-string v1, "Missing graphql object type"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 439901
    invoke-virtual {p3}, LX/0nX;->h()V

    .line 439902
    :goto_0
    return-void

    .line 439903
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    .line 439904
    invoke-static {v0}, LX/38I;->a(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    .line 439905
    invoke-static {v0, p3}, LX/2bt;->a(Ljava/lang/String;LX/0nX;)V

    goto :goto_0

    .line 439906
    :cond_1
    const-string v1, "SerializerHelpers"

    const-string v2, "Unrecognized graphql object type: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 439907
    invoke-virtual {p3}, LX/0nX;->h()V

    goto :goto_0
.end method

.method public static b(Ljava/util/Iterator;LX/0nX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(",
            "Ljava/util/Iterator",
            "<TT;>;",
            "LX/0nX;",
            ")V"
        }
    .end annotation

    .prologue
    .line 439893
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 439894
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439895
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 439896
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 439897
    return-void
.end method

.method public static c(Ljava/util/Iterator;LX/0nX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0nX;",
            ")V"
        }
    .end annotation

    .prologue
    .line 439888
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 439889
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439890
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/0nX;->b(I)V

    goto :goto_0

    .line 439891
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 439892
    return-void
.end method
