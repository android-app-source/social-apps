.class public LX/2dZ;
.super LX/2da;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/0wO;",
        ">",
        "LX/2da;"
    }
.end annotation


# instance fields
.field private b:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 443337
    invoke-direct {p0, p1}, LX/2da;-><init>(Landroid/view/View;)V

    .line 443338
    iput-object p1, p0, LX/2dZ;->b:Landroid/view/View;

    .line 443339
    return-void
.end method


# virtual methods
.method public final c(II)[Landroid/text/style/ClickableSpan;
    .locals 2

    .prologue
    .line 443340
    iget-object v0, p0, LX/2dZ;->b:Landroid/view/View;

    check-cast v0, LX/0wO;

    invoke-interface {v0}, LX/0wO;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-nez v0, :cond_0

    .line 443341
    const/4 v0, 0x0

    .line 443342
    :goto_0
    return-object v0

    .line 443343
    :cond_0
    iget-object v0, p0, LX/2dZ;->b:Landroid/view/View;

    check-cast v0, LX/0wO;

    invoke-interface {v0}, LX/0wO;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 443344
    const-class v1, LX/1zQ;

    invoke-interface {v0, p1, p2, v1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ClickableSpan;

    goto :goto_0
.end method
