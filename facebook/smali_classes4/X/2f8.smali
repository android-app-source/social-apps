.class public final LX/2f8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1bf;

.field public b:F

.field public c:Lcom/facebook/common/callercontext/CallerContext;

.field public d:Ljava/lang/CharSequence;

.field private e:I

.field private f:I

.field public g:Z

.field public h:Landroid/graphics/PointF;

.field public i:LX/1Up;

.field public j:Landroid/graphics/drawable/Drawable;

.field public k:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 446172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)LX/2f8;
    .locals 0

    .prologue
    .line 446173
    iput p2, p0, LX/2f8;->e:I

    .line 446174
    iput p1, p0, LX/2f8;->f:I

    .line 446175
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/2f8;
    .locals 1

    .prologue
    .line 446176
    invoke-static {p1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    iput-object v0, p0, LX/2f8;->a:LX/1bf;

    .line 446177
    return-object p0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/2f8;
    .locals 0

    .prologue
    .line 446178
    iput-object p1, p0, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 446179
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/2f8;
    .locals 1

    .prologue
    .line 446180
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2f8;->a(Landroid/net/Uri;)LX/2f8;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/2f9;
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 446181
    iget-object v0, p0, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "You must set a CallerContext"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 446182
    new-instance v0, LX/2f9;

    iget-object v1, p0, LX/2f8;->a:LX/1bf;

    iget v2, p0, LX/2f8;->b:F

    iget-object v3, p0, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v4, p0, LX/2f8;->d:Ljava/lang/CharSequence;

    iget v5, p0, LX/2f8;->e:I

    iget v6, p0, LX/2f8;->f:I

    iget-boolean v7, p0, LX/2f8;->g:Z

    iget-object v8, p0, LX/2f8;->h:Landroid/graphics/PointF;

    iget-object v9, p0, LX/2f8;->i:LX/1Up;

    iget-object v10, p0, LX/2f8;->j:Landroid/graphics/drawable/Drawable;

    iget v11, p0, LX/2f8;->k:I

    invoke-direct/range {v0 .. v12}, LX/2f9;-><init>(LX/1bf;FLcom/facebook/common/callercontext/CallerContext;Ljava/lang/CharSequence;IIZLandroid/graphics/PointF;LX/1Up;Landroid/graphics/drawable/Drawable;IB)V

    return-object v0

    :cond_0
    move v0, v12

    .line 446183
    goto :goto_0
.end method
