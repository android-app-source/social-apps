.class public final LX/3MZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3MZ;


# instance fields
.field public final A:Z

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final x:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final z:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcVoicemailInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 27

    .prologue
    .line 555081
    new-instance v0, LX/3MZ;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    invoke-direct/range {v0 .. v26}, LX/3MZ;-><init>(LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;Z)V

    sput-object v0, LX/3MZ;->a:LX/3MZ;

    return-void
.end method

.method public constructor <init>(LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;Z)V
    .locals 1
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p14    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p15    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p16    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p17    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p18    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p19    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p20    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p21    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p22    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p23    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p24    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p25    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcVoicemailInfo;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 555082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 555083
    iput-object p1, p0, LX/3MZ;->b:LX/0Px;

    .line 555084
    iput-object p2, p0, LX/3MZ;->c:LX/0Px;

    .line 555085
    iput-object p3, p0, LX/3MZ;->d:LX/0Px;

    .line 555086
    iput-object p4, p0, LX/3MZ;->e:LX/0Px;

    .line 555087
    iput-object p5, p0, LX/3MZ;->f:LX/0Px;

    .line 555088
    iput-object p6, p0, LX/3MZ;->g:LX/0Px;

    .line 555089
    iput-object p7, p0, LX/3MZ;->h:LX/0Px;

    .line 555090
    iput-object p8, p0, LX/3MZ;->i:LX/0Px;

    .line 555091
    iput-object p9, p0, LX/3MZ;->j:LX/0Px;

    .line 555092
    iput-object p10, p0, LX/3MZ;->k:LX/0Px;

    .line 555093
    iput-object p11, p0, LX/3MZ;->l:LX/0Px;

    .line 555094
    iput-object p12, p0, LX/3MZ;->m:LX/0Px;

    .line 555095
    iput-object p13, p0, LX/3MZ;->n:LX/0Px;

    .line 555096
    iput-object p14, p0, LX/3MZ;->o:LX/0Px;

    .line 555097
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3MZ;->p:LX/0Px;

    .line 555098
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3MZ;->q:LX/0Px;

    .line 555099
    move-object/from16 v0, p17

    iput-object v0, p0, LX/3MZ;->r:LX/0Px;

    .line 555100
    move/from16 v0, p26

    iput-boolean v0, p0, LX/3MZ;->A:Z

    .line 555101
    move-object/from16 v0, p18

    iput-object v0, p0, LX/3MZ;->s:LX/0Px;

    .line 555102
    move-object/from16 v0, p19

    iput-object v0, p0, LX/3MZ;->t:LX/0Px;

    .line 555103
    move-object/from16 v0, p20

    iput-object v0, p0, LX/3MZ;->u:LX/0Px;

    .line 555104
    move-object/from16 v0, p21

    iput-object v0, p0, LX/3MZ;->v:LX/0Px;

    .line 555105
    move-object/from16 v0, p22

    iput-object v0, p0, LX/3MZ;->w:LX/0Px;

    .line 555106
    move-object/from16 v0, p23

    iput-object v0, p0, LX/3MZ;->x:LX/0Px;

    .line 555107
    move-object/from16 v0, p24

    iput-object v0, p0, LX/3MZ;->y:LX/0Px;

    .line 555108
    move-object/from16 v0, p25

    iput-object v0, p0, LX/3MZ;->z:LX/0Px;

    .line 555109
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 555110
    const-class v0, LX/3MZ;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "favoriteFriends"

    iget-object v2, p0, LX/3MZ;->b:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "topFriends"

    iget-object v2, p0, LX/3MZ;->c:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "onlineFriends"

    iget-object v2, p0, LX/3MZ;->d:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "onlineFriendsByCoefficient"

    iget-object v2, p0, LX/3MZ;->e:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "topOnlineFriends"

    iget-object v2, p0, LX/3MZ;->f:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "onMessengerFriends"

    iget-object v2, p0, LX/3MZ;->g:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "topOnMessengerFriends"

    iget-object v2, p0, LX/3MZ;->h:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "notOnMessengerFriends"

    iget-object v2, p0, LX/3MZ;->i:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "PHATContacts"

    iget-object v2, p0, LX/3MZ;->j:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "topContacts"

    iget-object v2, p0, LX/3MZ;->k:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "allFriendsByCoefficient"

    iget-object v2, p0, LX/3MZ;->l:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "allFriendsByName"

    iget-object v2, p0, LX/3MZ;->m:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "smsInviteContacts"

    iget-object v2, p0, LX/3MZ;->n:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "phoneContacts"

    iget-object v2, p0, LX/3MZ;->p:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "recentCalls"

    iget-object v2, p0, LX/3MZ;->q:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "topPushableFriends"

    iget-object v2, p0, LX/3MZ;->r:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "specifiedContacts"

    iget-object v2, p0, LX/3MZ;->s:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "allContacts"

    iget-object v2, p0, LX/3MZ;->t:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "promotionalContacts"

    iget-object v2, p0, LX/3MZ;->u:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "hasPendingUpdates"

    iget-boolean v2, p0, LX/3MZ;->A:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "pages"

    iget-object v2, p0, LX/3MZ;->v:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "pstnFriends"

    iget-object v2, p0, LX/3MZ;->w:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "rtcCallLogs"

    iget-object v2, p0, LX/3MZ;->x:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "rtcOngoingGroupCalls"

    iget-object v2, p0, LX/3MZ;->y:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "rtcVoicemails"

    iget-object v2, p0, LX/3MZ;->z:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
