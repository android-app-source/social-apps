.class public LX/3Ig;
.super LX/2oy;
.source ""


# instance fields
.field public final a:Lcom/facebook/gif/AnimatedImagePlayButtonView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 546981
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3Ig;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 546982
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 546967
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3Ig;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546968
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 546976
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546977
    const v0, 0x7f0300e2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 546978
    const v0, 0x7f0d053a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/gif/AnimatedImagePlayButtonView;

    iput-object v0, p0, LX/3Ig;->a:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    .line 546979
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3ED;

    invoke-direct {v1, p0}, LX/3ED;-><init>(LX/3Ig;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546980
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 546973
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 546974
    iget-object v0, p0, LX/3Ig;->a:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    sget-object v1, LX/6Wv;->READY_TO_PLAY:LX/6Wv;

    invoke-virtual {v0, v1}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->setState(LX/6Wv;)V

    .line 546975
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 546971
    invoke-virtual {p0}, LX/3Ig;->e()V

    .line 546972
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 546969
    iget-object v0, p0, LX/3Ig;->a:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    sget-object v1, LX/6Wv;->HIDDEN:LX/6Wv;

    invoke-virtual {v0, v1}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->setState(LX/6Wv;)V

    .line 546970
    return-void
.end method
