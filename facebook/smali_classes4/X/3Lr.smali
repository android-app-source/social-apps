.class public LX/3Lr;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/3Lt;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/3Lt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 552204
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/3Lt;
    .locals 4

    .prologue
    .line 552205
    sget-object v0, LX/3Lr;->a:LX/3Lt;

    if-nez v0, :cond_1

    .line 552206
    const-class v1, LX/3Lr;

    monitor-enter v1

    .line 552207
    :try_start_0
    sget-object v0, LX/3Lr;->a:LX/3Lt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 552208
    if-eqz v2, :cond_0

    .line 552209
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 552210
    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object p0

    check-cast p0, Ljava/util/Locale;

    invoke-static {v3, p0}, LX/3Ls;->a(Landroid/content/res/Resources;Ljava/util/Locale;)LX/3Lt;

    move-result-object v3

    move-object v0, v3

    .line 552211
    sput-object v0, LX/3Lr;->a:LX/3Lt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 552212
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 552213
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 552214
    :cond_1
    sget-object v0, LX/3Lr;->a:LX/3Lt;

    return-object v0

    .line 552215
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 552216
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 552217
    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-static {v0, v1}, LX/3Ls;->a(Landroid/content/res/Resources;Ljava/util/Locale;)LX/3Lt;

    move-result-object v0

    return-object v0
.end method
