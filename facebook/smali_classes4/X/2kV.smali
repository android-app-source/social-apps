.class public LX/2kV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        "TUserInfo:",
        "Ljava/lang/Object;",
        "TResponseModel:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/0tX;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/3DR",
            "<TTUserInfo;>;",
            "LX/1Mv",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TTResponseModel;>;>;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private volatile d:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0tX;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 456053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456054
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, LX/2kV;->b:Ljava/util/Map;

    .line 456055
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2kV;->d:Z

    .line 456056
    iput-object p1, p0, LX/2kV;->c:Ljava/lang/String;

    .line 456057
    iput-object p2, p0, LX/2kV;->a:LX/0tX;

    .line 456058
    return-void
.end method

.method private static a(LX/2nj;LX/3DP;ILjava/lang/Object;LX/2kM;Ljava/lang/String;)LX/3DR;
    .locals 7
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TUserInfo:",
            "Ljava/lang/Object;",
            "TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2nj;",
            "LX/3DP;",
            "ITTUserInfo;",
            "LX/2kM",
            "<TTEdge;>;",
            "Ljava/lang/String;",
            ")",
            "LX/3DR",
            "<TTUserInfo;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 456167
    sget-object v1, LX/95Q;->a:[I

    .line 456168
    iget-object v2, p0, LX/2nj;->c:LX/2nk;

    move-object v2, v2

    .line 456169
    invoke-virtual {v2}, LX/2nk;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 456170
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported location type: %s in session %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 456171
    iget-object v4, p0, LX/2nj;->c:LX/2nk;

    move-object v4, v4

    .line 456172
    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p5, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move-object v4, v0

    move-object v3, v0

    .line 456173
    :goto_0
    new-instance v0, LX/3DR;

    move-object v1, p0

    move-object v2, p1

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/3DR;-><init>(LX/2nj;LX/3DP;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-object v0

    .line 456174
    :pswitch_1
    iget-object v0, p0, LX/2nj;->b:Ljava/lang/String;

    move-object v0, v0

    .line 456175
    invoke-static {p0, p4, p5}, LX/2kV;->a(LX/2nj;LX/2kM;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v3, v0

    .line 456176
    goto :goto_0

    .line 456177
    :pswitch_2
    iget-object v1, p0, LX/2nj;->b:Ljava/lang/String;

    move-object v1, v1

    .line 456178
    invoke-static {v1}, LX/3DQ;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 456179
    :goto_1
    iget-object v1, p0, LX/2nj;->b:Ljava/lang/String;

    move-object v4, v1

    .line 456180
    move-object v3, v0

    .line 456181
    goto :goto_0

    .line 456182
    :cond_0
    invoke-static {p0, p4, p5}, LX/2kV;->b(LX/2nj;LX/2kM;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(LX/2nj;LX/2kM;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2nj;",
            "LX/2kM",
            "<TTEdge;>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 456145
    iget-object v0, p0, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 456146
    sget-object v3, LX/2nk;->BEFORE:LX/2nk;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 456147
    iget-object v0, p0, LX/2nj;->b:Ljava/lang/String;

    move-object v0, v0

    .line 456148
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 456149
    invoke-interface {p1}, LX/2kM;->d()LX/0Px;

    move-result-object v5

    .line 456150
    iget-object v0, p0, LX/2nj;->b:Ljava/lang/String;

    move-object v6, v0

    .line 456151
    move v3, v2

    .line 456152
    :goto_1
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 456153
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nm;

    .line 456154
    iget-object p0, v0, LX/2nm;->a:LX/2nj;

    move-object v0, p0

    .line 456155
    iget-object p0, v0, LX/2nj;->b:Ljava/lang/String;

    move-object v0, p0

    .line 456156
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 456157
    :goto_2
    if-ne v3, v4, :cond_2

    .line 456158
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v3, "Location not found: %s in session %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v6, v4, v2

    aput-object p2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 456159
    goto :goto_0

    .line 456160
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 456161
    :cond_2
    if-nez v3, :cond_3

    .line 456162
    const/4 v0, 0x0

    .line 456163
    :goto_3
    return-object v0

    :cond_3
    add-int/lit8 v0, v3, -0x1

    invoke-virtual {v5, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nm;

    .line 456164
    iget-object v1, v0, LX/2nm;->b:LX/2nj;

    move-object v0, v1

    .line 456165
    iget-object v1, v0, LX/2nj;->b:Ljava/lang/String;

    move-object v0, v1

    .line 456166
    goto :goto_3

    :cond_4
    move v3, v4

    goto :goto_2
.end method

.method private static a(Ljava/util/Set;LX/2nj;)Z
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TUserInfo:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<",
            "LX/3DR",
            "<TTUserInfo;>;>;",
            "LX/2nj;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 456128
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v2, v0

    .line 456129
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3DR;

    .line 456130
    iget-object v4, v0, LX/3DR;->a:LX/2nj;

    move-object v4, v4

    .line 456131
    iget-object v5, v4, LX/2nj;->c:LX/2nk;

    move-object v4, v5

    .line 456132
    sget-object v5, LX/2nk;->INITIAL:LX/2nk;

    if-ne v4, v5, :cond_1

    move v0, v1

    .line 456133
    :goto_0
    return v0

    .line 456134
    :cond_1
    sget-object v4, LX/2nk;->BEFORE:LX/2nk;

    if-ne v2, v4, :cond_2

    .line 456135
    iget-object v4, p1, LX/2nj;->b:Ljava/lang/String;

    move-object v4, v4

    .line 456136
    iget-object v5, v0, LX/3DR;->c:Ljava/lang/String;

    move-object v0, v5

    .line 456137
    invoke-static {v4, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 456138
    goto :goto_0

    .line 456139
    :cond_2
    sget-object v4, LX/2nk;->AFTER:LX/2nk;

    if-ne v2, v4, :cond_0

    .line 456140
    iget-object v4, p1, LX/2nj;->b:Ljava/lang/String;

    move-object v4, v4

    .line 456141
    iget-object v5, v0, LX/3DR;->d:Ljava/lang/String;

    move-object v0, v5

    .line 456142
    invoke-static {v4, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 456143
    goto :goto_0

    .line 456144
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(LX/2nj;LX/2kM;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2nj;",
            "LX/2kM",
            "<TTEdge;>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 456106
    iget-object v0, p0, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 456107
    sget-object v3, LX/2nk;->AFTER:LX/2nk;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 456108
    iget-object v0, p0, LX/2nj;->b:Ljava/lang/String;

    move-object v0, v0

    .line 456109
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 456110
    invoke-interface {p1}, LX/2kM;->d()LX/0Px;

    move-result-object v5

    .line 456111
    iget-object v0, p0, LX/2nj;->b:Ljava/lang/String;

    move-object v6, v0

    .line 456112
    move v3, v2

    .line 456113
    :goto_1
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 456114
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nm;

    .line 456115
    iget-object p0, v0, LX/2nm;->b:LX/2nj;

    move-object v0, p0

    .line 456116
    iget-object p0, v0, LX/2nj;->b:Ljava/lang/String;

    move-object v0, p0

    .line 456117
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 456118
    :goto_2
    if-ne v3, v4, :cond_2

    .line 456119
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v3, "Location not found: %s in session %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v6, v4, v2

    aput-object p2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 456120
    goto :goto_0

    .line 456121
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 456122
    :cond_2
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-lt v0, v1, :cond_3

    .line 456123
    const/4 v0, 0x0

    .line 456124
    :goto_3
    return-object v0

    :cond_3
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {v5, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nm;

    .line 456125
    iget-object v1, v0, LX/2nm;->a:LX/2nj;

    move-object v0, v1

    .line 456126
    iget-object v1, v0, LX/2nj;->b:Ljava/lang/String;

    move-object v0, v1

    .line 456127
    goto :goto_3

    :cond_4
    move v3, v4

    goto :goto_2
.end method


# virtual methods
.method public final declared-synchronized a(LX/2nj;LX/3DP;ILjava/lang/Object;LX/2kM;)LX/3DR;
    .locals 6
    .param p4    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "ITTUserInfo;",
            "LX/2kM",
            "<TTEdge;>;)",
            "LX/3DR",
            "<TTUserInfo;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 456093
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/2kV;->d:Z

    if-nez v1, :cond_0

    .line 456094
    iget-object v1, p1, LX/2nj;->c:LX/2nk;

    move-object v1, v1

    .line 456095
    sget-object v2, LX/2nk;->INITIAL:LX/2nk;

    if-eq v1, v2, :cond_3

    .line 456096
    iget-object v1, p1, LX/2nj;->b:Ljava/lang/String;

    move-object v1, v1

    .line 456097
    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 456098
    if-nez v1, :cond_0

    iget-object v1, p0, LX/2kV;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1, p1}, LX/2kV;->a(Ljava/util/Set;LX/2nj;)Z

    move-result v1

    if-nez v1, :cond_1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456099
    :cond_0
    :goto_1
    monitor-exit p0

    return-object v0

    .line 456100
    :cond_1
    :try_start_1
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 456101
    sget-object v1, LX/2nk;->INITIAL:LX/2nk;

    if-ne v0, v1, :cond_2

    .line 456102
    invoke-virtual {p0}, LX/2kV;->b()V

    .line 456103
    :cond_2
    iget-object v5, p0, LX/2kV;->c:Ljava/lang/String;

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move-object v4, p5

    invoke-static/range {v0 .. v5}, LX/2kV;->a(LX/2nj;LX/3DP;ILjava/lang/Object;LX/2kM;Ljava/lang/String;)LX/3DR;

    move-result-object v0

    .line 456104
    iget-object v1, p0, LX/2kV;->b:Ljava/util/Map;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 456105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 456090
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/2kV;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456091
    monitor-exit p0

    return-void

    .line 456092
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/2nj;LX/3DP;)V
    .locals 1

    .prologue
    .line 456085
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p2}, LX/2kV;->b(LX/2nj;LX/3DP;)LX/3DR;

    move-result-object v0

    .line 456086
    if-eqz v0, :cond_0

    .line 456087
    invoke-virtual {p0, v0}, LX/2kV;->a(LX/3DR;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456088
    :cond_0
    monitor-exit p0

    return-void

    .line 456089
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/3DR;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3DR",
            "<TTUserInfo;>;)V"
        }
    .end annotation

    .prologue
    .line 456082
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2kV;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456083
    monitor-exit p0

    return-void

    .line 456084
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/3DR;LX/0zO;LX/0Ve;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3DR",
            "<TTUserInfo;>;",
            "LX/0zO",
            "<TTResponseModel;>;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TTResponseModel;>;>;)V"
        }
    .end annotation

    .prologue
    .line 456074
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2kV;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 456075
    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v1, "Operation in %s session not contained in ongoing fetches"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/2kV;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456076
    :goto_0
    monitor-exit p0

    return-void

    .line 456077
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2kV;->a:LX/0tX;

    invoke-virtual {v0, p2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 456078
    iget-object v1, p0, LX/2kV;->b:Ljava/util/Map;

    invoke-static {v0, p3}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 456079
    sget-object v1, LX/131;->INSTANCE:LX/131;

    move-object v1, v1

    .line 456080
    invoke-static {v0, p3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 456081
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/2nj;LX/3DP;)LX/3DR;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            ")",
            "LX/3DR",
            "<TTUserInfo;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 456067
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2kV;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3DR;

    .line 456068
    iget-object v2, v0, LX/3DR;->a:LX/2nj;

    move-object v2, v2

    .line 456069
    if-ne v2, p1, :cond_0

    .line 456070
    iget-object v2, v0, LX/3DR;->b:LX/3DP;

    move-object v2, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456071
    if-ne v2, p2, :cond_0

    .line 456072
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 456073
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 3

    .prologue
    .line 456059
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2kV;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 456060
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 456061
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Mv;

    .line 456062
    if-eqz v0, :cond_0

    .line 456063
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/1Mv;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 456064
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 456065
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2kV;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 456066
    monitor-exit p0

    return-void
.end method
