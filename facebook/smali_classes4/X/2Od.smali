.class public LX/2Od;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 402487
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 402488
    return-void
.end method

.method public static a(LX/2OL;LX/2OQ;)LX/2Oe;
    .locals 1
    .param p1    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .prologue
    .line 402489
    invoke-virtual {p0, p1}, LX/2OL;->a(LX/2OQ;)LX/2Oe;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/FDJ;LX/2OQ;)LX/FDI;
    .locals 1
    .param p1    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .prologue
    .line 402486
    invoke-virtual {p0, p1}, LX/FDJ;->a(LX/2OQ;)LX/FDI;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/FDQ;LX/2OQ;LX/FDI;LX/0Ot;)LX/FDP;
    .locals 1
    .param p1    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .param p2    # LX/FDI;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDQ;",
            "LX/2OQ;",
            "LX/FDI;",
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;)",
            "LX/FDP;"
        }
    .end annotation

    .prologue
    .line 402485
    const-string v0, "FacebookCacheServiceHandler"

    invoke-virtual {p0, v0, p1, p2, p3}, LX/FDQ;->a(Ljava/lang/String;LX/2OQ;LX/FDI;LX/0Ot;)LX/FDP;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/2OL;LX/2OQ;)LX/2Oe;
    .locals 1
    .param p1    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
    .end annotation

    .prologue
    .line 402484
    invoke-virtual {p0, p1}, LX/2OL;->a(LX/2OQ;)LX/2Oe;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/FDJ;LX/2OQ;)LX/FDI;
    .locals 1
    .param p1    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
    .end annotation

    .prologue
    .line 402490
    invoke-virtual {p0, p1}, LX/FDJ;->a(LX/2OQ;)LX/FDI;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/FDQ;LX/2OQ;LX/FDI;LX/0Ot;)LX/FDP;
    .locals 1
    .param p1    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
        .end annotation
    .end param
    .param p2    # LX/FDI;
        .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDQ;",
            "LX/2OQ;",
            "LX/FDI;",
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;)",
            "LX/FDP;"
        }
    .end annotation

    .prologue
    .line 402483
    const-string v0, "SmsCacheServiceHandler"

    invoke-virtual {p0, v0, p1, p2, p3}, LX/FDQ;->a(Ljava/lang/String;LX/2OQ;LX/FDI;LX/0Ot;)LX/FDP;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/2OL;LX/2OQ;)LX/2Oe;
    .locals 1
    .param p1    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
    .end annotation

    .prologue
    .line 402482
    invoke-virtual {p0, p1}, LX/2OL;->a(LX/2OQ;)LX/2Oe;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/FDJ;LX/2OQ;)LX/FDI;
    .locals 1
    .param p1    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
    .end annotation

    .prologue
    .line 402481
    invoke-virtual {p0, p1}, LX/FDJ;->a(LX/2OQ;)LX/FDI;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/FDQ;LX/2OQ;LX/FDI;LX/0Ot;)LX/FDP;
    .locals 1
    .param p1    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
        .end annotation
    .end param
    .param p2    # LX/FDI;
        .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDQ;",
            "LX/2OQ;",
            "LX/FDI;",
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;)",
            "LX/FDP;"
        }
    .end annotation

    .prologue
    .line 402479
    const-string v0, "TincanCacheServiceHandler"

    invoke-virtual {p0, v0, p1, p2, p3}, LX/FDQ;->a(Ljava/lang/String;LX/2OQ;LX/FDI;LX/0Ot;)LX/FDP;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 402480
    return-void
.end method
