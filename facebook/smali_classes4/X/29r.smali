.class public LX/29r;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/01T;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile v:LX/29r;


# instance fields
.field public final c:LX/29s;

.field private final d:LX/0aG;

.field public final e:Landroid/os/Handler;

.field public final f:LX/03V;

.field private final g:LX/00H;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Sy;

.field public j:I

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public final o:Ljava/lang/Runnable;

.field private p:Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;

.field public q:Lcom/facebook/appirater/api/FetchISRConfigResult;

.field public r:Lcom/facebook/appirater/api/AppRaterReport;

.field public s:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field public t:Z

.field public u:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 376729
    const-class v0, LX/29r;

    sput-object v0, LX/29r;->a:Ljava/lang/Class;

    .line 376730
    sget-object v0, LX/01T;->FB4A:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    sget-object v2, LX/01T;->MESSENGER:LX/01T;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/29r;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/29s;LX/0aG;Landroid/os/Handler;LX/03V;LX/00H;LX/0Or;LX/0Sy;)V
    .locals 3
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/appirater/api/annotation/IsAppiraterIsrAllowed;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/29s;",
            "LX/0aG;",
            "Landroid/os/Handler;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/00H;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/common/userinteraction/UserInteractionController;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 376643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376644
    new-instance v0, Lcom/facebook/appirater/InternalStarRatingController$1;

    invoke-direct {v0, p0}, Lcom/facebook/appirater/InternalStarRatingController$1;-><init>(LX/29r;)V

    iput-object v0, p0, LX/29r;->o:Ljava/lang/Runnable;

    .line 376645
    iput-object v1, p0, LX/29r;->p:Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;

    .line 376646
    iput-object v1, p0, LX/29r;->q:Lcom/facebook/appirater/api/FetchISRConfigResult;

    .line 376647
    iput-object v1, p0, LX/29r;->r:Lcom/facebook/appirater/api/AppRaterReport;

    .line 376648
    iput-object v1, p0, LX/29r;->s:Ljava/lang/ref/WeakReference;

    .line 376649
    iput-boolean v2, p0, LX/29r;->t:Z

    .line 376650
    iput-boolean v2, p0, LX/29r;->u:Z

    .line 376651
    iput-object p1, p0, LX/29r;->c:LX/29s;

    .line 376652
    iput-object p2, p0, LX/29r;->d:LX/0aG;

    .line 376653
    iput-object p3, p0, LX/29r;->e:Landroid/os/Handler;

    .line 376654
    iput-object p4, p0, LX/29r;->f:LX/03V;

    .line 376655
    iput-object p5, p0, LX/29r;->g:LX/00H;

    .line 376656
    iput-object p6, p0, LX/29r;->h:LX/0Or;

    .line 376657
    iput-object p7, p0, LX/29r;->i:LX/0Sy;

    .line 376658
    iput v2, p0, LX/29r;->j:I

    .line 376659
    return-void
.end method

.method public static a(LX/0QB;)LX/29r;
    .locals 11

    .prologue
    .line 376713
    sget-object v0, LX/29r;->v:LX/29r;

    if-nez v0, :cond_1

    .line 376714
    const-class v1, LX/29r;

    monitor-enter v1

    .line 376715
    :try_start_0
    sget-object v0, LX/29r;->v:LX/29r;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 376716
    if-eqz v2, :cond_0

    .line 376717
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 376718
    new-instance v3, LX/29r;

    .line 376719
    new-instance v6, LX/29s;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lC;

    invoke-direct {v6, v4, v5}, LX/29s;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;)V

    .line 376720
    move-object v4, v6

    .line 376721
    check-cast v4, LX/29s;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v6

    check-cast v6, Landroid/os/Handler;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    const-class v8, LX/00H;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/00H;

    const/16 v9, 0x2fa

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v10

    check-cast v10, LX/0Sy;

    invoke-direct/range {v3 .. v10}, LX/29r;-><init>(LX/29s;LX/0aG;Landroid/os/Handler;LX/03V;LX/00H;LX/0Or;LX/0Sy;)V

    .line 376722
    move-object v0, v3

    .line 376723
    sput-object v0, LX/29r;->v:LX/29r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 376724
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 376725
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 376726
    :cond_1
    sget-object v0, LX/29r;->v:LX/29r;

    return-object v0

    .line 376727
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 376728
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static g(LX/29r;)Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;
    .locals 3

    .prologue
    .line 376708
    iget-object v0, p0, LX/29r;->p:Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;

    if-nez v0, :cond_0

    .line 376709
    iget-object v0, p0, LX/29r;->c:LX/29s;

    .line 376710
    sget-object v1, LX/BZn;->f:LX/0Tn;

    const-class v2, Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;

    invoke-static {v0, v1, v2}, LX/29s;->a(LX/29s;LX/0Tn;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;

    move-object v0, v1

    .line 376711
    iput-object v0, p0, LX/29r;->p:Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;

    .line 376712
    :cond_0
    iget-object v0, p0, LX/29r;->p:Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;

    return-object v0
.end method

.method public static i(LX/29r;)Landroid/app/Activity;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 376707
    iget-object v0, p0, LX/29r;->s:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/29r;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    goto :goto_0
.end method

.method private j()LX/0gc;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 376703
    invoke-static {p0}, LX/29r;->i(LX/29r;)Landroid/app/Activity;

    move-result-object v0

    .line 376704
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/support/v4/app/FragmentActivity;

    if-nez v1, :cond_1

    .line 376705
    :cond_0
    const/4 v0, 0x0

    .line 376706
    :goto_0
    return-object v0

    :cond_1
    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    goto :goto_0
.end method

.method public static k(LX/29r;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 376731
    invoke-direct {p0}, LX/29r;->j()LX/0gc;

    move-result-object v1

    .line 376732
    if-eqz v1, :cond_0

    .line 376733
    const-string v2, "appirater_isr_dialog_fragment"

    invoke-virtual {v1, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 376734
    :cond_0
    return v0
.end method

.method public static l(LX/29r;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 376685
    iput-boolean v0, p0, LX/29r;->u:Z

    .line 376686
    iput v0, p0, LX/29r;->j:I

    .line 376687
    invoke-direct {p0}, LX/29r;->j()LX/0gc;

    move-result-object v0

    .line 376688
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "appirater_isr_dialog_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 376689
    :cond_0
    :goto_0
    return-void

    .line 376690
    :cond_1
    invoke-static {p0}, LX/29r;->g(LX/29r;)Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;

    move-result-object v1

    .line 376691
    new-instance v2, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    invoke-direct {v2}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;-><init>()V

    .line 376692
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 376693
    if-eqz v1, :cond_2

    .line 376694
    const-string v4, "rating"

    iget v5, v1, Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;->rating:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 376695
    const-string v4, "rating_comment"

    iget-object v5, v1, Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;->ratingComment:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 376696
    const-string v4, "current_screen"

    iget-object v5, v1, Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;->lastScreen:Ljava/lang/String;

    invoke-static {v5}, LX/BZw;->valueOf(Ljava/lang/String;)LX/BZw;

    move-result-object v5

    invoke-virtual {v5}, LX/BZw;->toInt()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 376697
    :cond_2
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 376698
    move-object v1, v2

    .line 376699
    const-string v2, "appirater_isr_dialog_fragment"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 376700
    iget-object v0, p0, LX/29r;->c:LX/29s;

    const/4 v1, 0x1

    .line 376701
    iget-object v2, v0, LX/29s;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/BZn;->d:LX/0Tn;

    invoke-interface {v2, v3, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 376702
    goto :goto_0
.end method

.method public static m(LX/29r;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 376674
    iget-boolean v0, p0, LX/29r;->t:Z

    if-eqz v0, :cond_1

    .line 376675
    :cond_0
    :goto_0
    return-void

    .line 376676
    :cond_1
    iget-object v0, p0, LX/29r;->r:Lcom/facebook/appirater/api/AppRaterReport;

    if-nez v0, :cond_2

    .line 376677
    iget-object v0, p0, LX/29r;->c:LX/29s;

    invoke-virtual {v0}, LX/29s;->b()Lcom/facebook/appirater/api/AppRaterReport;

    move-result-object v0

    iput-object v0, p0, LX/29r;->r:Lcom/facebook/appirater/api/AppRaterReport;

    .line 376678
    :cond_2
    iget-object v0, p0, LX/29r;->r:Lcom/facebook/appirater/api/AppRaterReport;

    move-object v0, v0

    .line 376679
    if-eqz v0, :cond_0

    .line 376680
    iput-boolean v2, p0, LX/29r;->t:Z

    .line 376681
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    .line 376682
    const-string v2, "app_rater_report"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 376683
    iget-object v0, p0, LX/29r;->d:LX/0aG;

    const-string v2, "appirater_create_report"

    const v3, 0x50b5db1

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    .line 376684
    new-instance v1, LX/BZk;

    invoke-direct {v1, p0}, LX/BZk;-><init>(LX/29r;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;)V
    .locals 1

    .prologue
    .line 376670
    iput-object p1, p0, LX/29r;->p:Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;

    .line 376671
    iget-object v0, p0, LX/29r;->c:LX/29s;

    .line 376672
    sget-object p0, LX/BZn;->f:LX/0Tn;

    invoke-static {v0, p0, p1}, LX/29s;->a(LX/29s;LX/0Tn;Ljava/lang/Object;)V

    .line 376673
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 376667
    sget-object v0, LX/29r;->b:LX/0Px;

    iget-object v2, p0, LX/29r;->g:LX/00H;

    .line 376668
    iget-object v3, v2, LX/00H;->j:LX/01T;

    move-object v2, v3

    .line 376669
    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/29r;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final d()Lcom/facebook/appirater/api/FetchISRConfigResult;
    .locals 3

    .prologue
    .line 376660
    iget-object v0, p0, LX/29r;->q:Lcom/facebook/appirater/api/FetchISRConfigResult;

    if-nez v0, :cond_0

    .line 376661
    iget-object v0, p0, LX/29r;->c:LX/29s;

    .line 376662
    sget-object v1, LX/BZn;->b:LX/0Tn;

    const-class v2, Lcom/facebook/appirater/api/FetchISRConfigResult;

    invoke-static {v0, v1, v2}, LX/29s;->a(LX/29s;LX/0Tn;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/appirater/api/FetchISRConfigResult;

    .line 376663
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/appirater/api/FetchISRConfigResult;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 376664
    :goto_0
    move-object v0, v1

    .line 376665
    iput-object v0, p0, LX/29r;->q:Lcom/facebook/appirater/api/FetchISRConfigResult;

    .line 376666
    :cond_0
    iget-object v0, p0, LX/29r;->q:Lcom/facebook/appirater/api/FetchISRConfigResult;

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
