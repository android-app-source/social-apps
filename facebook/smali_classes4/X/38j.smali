.class public LX/38j;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 521262
    const-class v0, LX/38j;

    sput-object v0, LX/38j;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 521263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521264
    iput p1, p0, LX/38j;->b:I

    .line 521265
    return-void
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 521266
    iget v0, p0, LX/38j;->b:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 521267
    iget v0, p0, LX/38j;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 521268
    iget v0, p0, LX/38j;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 521269
    iget v1, p0, LX/38j;->b:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 521270
    invoke-virtual {p0}, LX/38j;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, LX/38j;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/38j;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/38j;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 521271
    instance-of v1, p1, LX/38j;

    if-eqz v1, :cond_0

    .line 521272
    check-cast p1, LX/38j;

    .line 521273
    iget v1, p0, LX/38j;->b:I

    iget v2, p1, LX/38j;->b:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 521274
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 521275
    iget v0, p0, LX/38j;->b:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 521276
    invoke-virtual {p0}, LX/38j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Playing"

    .line 521277
    :goto_0
    return-object v0

    .line 521278
    :cond_0
    invoke-direct {p0}, LX/38j;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Buffering"

    goto :goto_0

    .line 521279
    :cond_1
    invoke-virtual {p0}, LX/38j;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Paused"

    goto :goto_0

    .line 521280
    :cond_2
    invoke-virtual {p0}, LX/38j;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Casting"

    goto :goto_0

    .line 521281
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PlayerState: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/38j;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
