.class public LX/25q;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/25q;


# instance fields
.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/25p;",
            ">;"
        }
    .end annotation
.end field

.field private final c:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 369974
    new-instance v0, LX/25r;

    invoke-direct {v0, v1, v1}, LX/25r;-><init>(Landroid/util/SparseArray;[B)V

    sput-object v0, LX/25q;->a:LX/25q;

    return-void
.end method

.method public constructor <init>(Landroid/util/SparseArray;[B)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "LX/25p;",
            ">;[B)V"
        }
    .end annotation

    .prologue
    .line 369975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369976
    iput-object p1, p0, LX/25q;->b:Landroid/util/SparseArray;

    .line 369977
    iput-object p2, p0, LX/25q;->c:[B

    .line 369978
    return-void
.end method


# virtual methods
.method public a(ILX/0XJ;)LX/12t;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 369979
    iget-object v0, p0, LX/25q;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/25p;

    .line 369980
    if-nez v0, :cond_0

    .line 369981
    const/4 v0, 0x0

    .line 369982
    :goto_0
    return-object v0

    .line 369983
    :cond_0
    iget-object v2, v0, LX/25p;->a:Landroid/util/SparseArray;

    invoke-virtual {p2}, LX/0XJ;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v2

    if-gez v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 369984
    if-eqz v2, :cond_1

    .line 369985
    sget-object p2, LX/0XJ;->UNKNOWN:LX/0XJ;

    .line 369986
    :cond_1
    iget-object v0, v0, LX/25p;->a:Landroid/util/SparseArray;

    invoke-virtual {p2}, LX/0XJ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/25o;

    .line 369987
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    move v2, v1

    .line 369988
    :goto_2
    iget-object v4, v0, LX/25o;->b:[I

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 369989
    iget-object v4, p0, LX/25q;->c:[B

    iget v5, v0, LX/25o;->a:I

    add-int/2addr v5, v2

    iget-object v6, v0, LX/25o;->c:[I

    aget v6, v6, v1

    invoke-static {v4, v5, v6}, LX/25l;->a([BII)Ljava/lang/String;

    move-result-object v4

    .line 369990
    iget-object v5, v0, LX/25o;->c:[I

    aget v5, v5, v1

    add-int/2addr v2, v5

    .line 369991
    iget-object v5, v0, LX/25o;->b:[I

    aget v5, v5, v1

    invoke-static {v5}, LX/0W6;->of(I)LX/0W6;

    move-result-object v5

    invoke-virtual {v3, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 369992
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 369993
    :cond_2
    new-instance v0, LX/12t;

    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-direct {v0, v1}, LX/12t;-><init>(LX/0P1;)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method
