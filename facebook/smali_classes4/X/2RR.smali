.class public LX/2RR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/2RT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LX/2RU;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

.field public n:LX/2RS;

.field public o:Z

.field public p:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409828
    sget-object v0, LX/2RS;->NO_SORT_ORDER:LX/2RS;

    iput-object v0, p0, LX/2RR;->n:LX/2RS;

    .line 409829
    const/4 v0, -0x1

    iput v0, p0, LX/2RR;->p:I

    .line 409830
    return-void
.end method


# virtual methods
.method public final i()LX/2RR;
    .locals 4

    .prologue
    .line 409831
    iget-object v0, p0, LX/2RR;->a:LX/2RT;

    .line 409832
    iget-object v1, v0, LX/2RT;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v2, 0x122

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 409833
    if-eqz v0, :cond_0

    .line 409834
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CONNECTED:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 409835
    iput-object v0, p0, LX/2RR;->m:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 409836
    :goto_0
    return-object p0

    .line 409837
    :cond_0
    const/4 v0, 0x1

    .line 409838
    iput-boolean v0, p0, LX/2RR;->j:Z

    .line 409839
    goto :goto_0
.end method
