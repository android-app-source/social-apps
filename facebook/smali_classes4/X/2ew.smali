.class public LX/2ew;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/0bH;

.field public final b:LX/2do;

.field public final c:LX/2dj;

.field private final d:LX/0Zb;

.field private final e:LX/17Q;


# direct methods
.method public constructor <init>(LX/0bH;LX/2do;LX/2dj;LX/0Zb;LX/17Q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 446043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 446044
    iput-object p1, p0, LX/2ew;->a:LX/0bH;

    .line 446045
    iput-object p2, p0, LX/2ew;->b:LX/2do;

    .line 446046
    iput-object p3, p0, LX/2ew;->c:LX/2dj;

    .line 446047
    iput-object p4, p0, LX/2ew;->d:LX/0Zb;

    .line 446048
    iput-object p5, p0, LX/2ew;->e:LX/17Q;

    .line 446049
    return-void
.end method

.method public static a(LX/0QB;)LX/2ew;
    .locals 9

    .prologue
    .line 446050
    const-class v1, LX/2ew;

    monitor-enter v1

    .line 446051
    :try_start_0
    sget-object v0, LX/2ew;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 446052
    sput-object v2, LX/2ew;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 446053
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446054
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 446055
    new-instance v3, LX/2ew;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v4

    check-cast v4, LX/0bH;

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v5

    check-cast v5, LX/2do;

    invoke-static {v0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v6

    check-cast v6, LX/2dj;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v8

    check-cast v8, LX/17Q;

    invoke-direct/range {v3 .. v8}, LX/2ew;-><init>(LX/0bH;LX/2do;LX/2dj;LX/0Zb;LX/17Q;)V

    .line 446056
    move-object v0, v3

    .line 446057
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 446058
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2ew;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446059
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 446060
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/1Pq;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            "LX/1Fa;",
            "TE;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 446061
    invoke-static {p2, p1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v0

    .line 446062
    invoke-static {v0}, LX/17Q;->F(LX/0lF;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 446063
    const/4 v1, 0x0

    .line 446064
    :goto_0
    move-object v0, v1

    .line 446065
    iget-object v1, p0, LX/2ew;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 446066
    new-instance v1, LX/2e7;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    new-instance v2, LX/2e8;

    sget-object v3, LX/3ka;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-direct {v2, v4, v3}, LX/2e8;-><init>(ZLcom/facebook/interstitial/manager/InterstitialTrigger;)V

    invoke-direct {v1, v0, v2}, LX/2e7;-><init>(Ljava/lang/String;LX/2e8;)V

    move-object v0, p3

    .line 446067
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v1, p1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-object v0, p3

    .line 446068
    check-cast v0, LX/1Pr;

    new-instance v2, LX/2e8;

    sget-object v3, LX/3ka;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-direct {v2, v4, v3}, LX/2e8;-><init>(ZLcom/facebook/interstitial/manager/InterstitialTrigger;)V

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 446069
    new-array v0, v4, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-interface {p3, v0}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 446070
    invoke-static {p2}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    .line 446071
    iget-object v5, p0, LX/2ew;->c:LX/2dj;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    sget-object v6, LX/2hC;->FEED:LX/2hC;

    invoke-virtual {v5, v7, v8, v6}, LX/2dj;->a(JLX/2hC;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 446072
    iget-object v5, p0, LX/2ew;->b:LX/2do;

    new-instance v6, LX/2iB;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-direct {v6, v7, v8}, LX/2iB;-><init>(J)V

    invoke-virtual {v5, v6}, LX/0b4;->a(LX/0b7;)V

    .line 446073
    iget-object v5, p0, LX/2ew;->a:LX/0bH;

    new-instance v6, LX/1Ng;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->g()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v0}, LX/1Ng;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, LX/0b4;->a(LX/0b7;)V

    .line 446074
    return-void

    .line 446075
    :cond_0
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "pymk_xout"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "tracking"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "native_newsfeed"

    .line 446076
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 446077
    move-object v1, v1

    .line 446078
    goto :goto_0
.end method
