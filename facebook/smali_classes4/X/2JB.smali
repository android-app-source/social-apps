.class public LX/2JB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/2JB;


# instance fields
.field public final b:LX/2It;

.field private final c:LX/0W9;

.field private final d:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 392595
    const-class v0, LX/2JB;

    sput-object v0, LX/2JB;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2It;LX/0W9;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392597
    iput-object p1, p0, LX/2JB;->b:LX/2It;

    .line 392598
    iput-object p2, p0, LX/2JB;->c:LX/0W9;

    .line 392599
    iput-object p3, p0, LX/2JB;->d:LX/0SG;

    .line 392600
    return-void
.end method

.method public static a(LX/0QB;)LX/2JB;
    .locals 6

    .prologue
    .line 392601
    sget-object v0, LX/2JB;->e:LX/2JB;

    if-nez v0, :cond_1

    .line 392602
    const-class v1, LX/2JB;

    monitor-enter v1

    .line 392603
    :try_start_0
    sget-object v0, LX/2JB;->e:LX/2JB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 392604
    if-eqz v2, :cond_0

    .line 392605
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 392606
    new-instance p0, LX/2JB;

    invoke-static {v0}, LX/2It;->b(LX/0QB;)LX/2It;

    move-result-object v3

    check-cast v3, LX/2It;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v4

    check-cast v4, LX/0W9;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {p0, v3, v4, v5}, LX/2JB;-><init>(LX/2It;LX/0W9;LX/0SG;)V

    .line 392607
    move-object v0, p0

    .line 392608
    sput-object v0, LX/2JB;->e:LX/2JB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392609
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 392610
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 392611
    :cond_1
    sget-object v0, LX/2JB;->e:LX/2JB;

    return-object v0

    .line 392612
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 392613
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v0, 0x1

    .line 392614
    iget-object v1, p0, LX/2JB;->b:LX/2It;

    sget-object v2, LX/3Fg;->b:LX/3OK;

    invoke-virtual {v1, v2, v6, v7}, LX/2Iu;->a(LX/0To;J)J

    move-result-wide v2

    .line 392615
    cmp-long v1, v2, v6

    if-nez v1, :cond_1

    .line 392616
    :cond_0
    :goto_0
    return v0

    .line 392617
    :cond_1
    iget-object v1, p0, LX/2JB;->b:LX/2It;

    sget-object v4, LX/3Fg;->a:LX/3OK;

    invoke-virtual {v1, v4, v6, v7}, LX/2Iu;->a(LX/0To;J)J

    move-result-wide v4

    .line 392618
    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    .line 392619
    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 392620
    iget-object v1, p0, LX/2JB;->b:LX/2It;

    sget-object v4, LX/3Fg;->c:LX/3OK;

    iget-object v5, p0, LX/2JB;->c:LX/0W9;

    invoke-virtual {v5}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/2Iu;->a(LX/0To;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 392621
    iget-object v4, p0, LX/2JB;->c:LX/0W9;

    invoke-virtual {v4}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 392622
    iget-object v1, p0, LX/2JB;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 392623
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const-wide/32 v4, 0x48190800

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 392624
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 392625
    iget-object v0, p0, LX/2JB;->b:LX/2It;

    sget-object v1, LX/3Fg;->g:LX/3OK;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1, v2}, LX/2Iu;->b(LX/0To;I)V

    .line 392626
    return-void
.end method
