.class public final LX/2pv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2pw;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/19P;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2qU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/19P;)V
    .locals 2

    .prologue
    .line 469123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 469124
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2pv;->a:Ljava/lang/ref/WeakReference;

    .line 469125
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2pv;->b:Ljava/lang/ref/WeakReference;

    .line 469126
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 469127
    iget-object v0, p0, LX/2pv;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2pv;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 469128
    iget-object v0, p0, LX/2pv;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19P;

    iget-object v1, p0, LX/2pv;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2qU;

    invoke-virtual {v0, v1}, LX/19P;->d(LX/2qU;)I

    move-result v0

    .line 469129
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/2qU;)V
    .locals 1

    .prologue
    .line 469130
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2pv;->b:Ljava/lang/ref/WeakReference;

    .line 469131
    return-void
.end method
