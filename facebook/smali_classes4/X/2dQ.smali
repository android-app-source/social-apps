.class public LX/2dQ;
.super LX/1SG;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1SG",
        "<",
        "Lcom/facebook/prefs/shared/objects/FbSharedObjectPreferences;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2dQ;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/prefs/shared/objects/FbSharedObjectPreferences;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2dH;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/objects/FbSharedObjectPreferences;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 443049
    invoke-direct {p0}, LX/1SG;-><init>()V

    .line 443050
    new-instance v0, LX/2dR;

    invoke-direct {v0, p0, p2}, LX/2dR;-><init>(LX/2dQ;LX/0Ot;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/1SK;->a(LX/0Ot;LX/0QK;Ljava/util/concurrent/Executor;)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/2dQ;->a:LX/0Ot;

    .line 443051
    return-void
.end method

.method public static a(LX/0QB;)LX/2dQ;
    .locals 5

    .prologue
    .line 443052
    sget-object v0, LX/2dQ;->b:LX/2dQ;

    if-nez v0, :cond_1

    .line 443053
    const-class v1, LX/2dQ;

    monitor-enter v1

    .line 443054
    :try_start_0
    sget-object v0, LX/2dQ;->b:LX/2dQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 443055
    if-eqz v2, :cond_0

    .line 443056
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 443057
    new-instance v3, LX/2dQ;

    const/16 v4, 0xf99

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0xfa2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/2dQ;-><init>(LX/0Ot;LX/0Ot;)V

    .line 443058
    move-object v0, v3

    .line 443059
    sput-object v0, LX/2dQ;->b:LX/2dQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 443060
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 443061
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 443062
    :cond_1
    sget-object v0, LX/2dQ;->b:LX/2dQ;

    return-object v0

    .line 443063
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 443064
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 443065
    invoke-virtual {p0}, LX/2dQ;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/prefs/shared/objects/FbSharedObjectPreferences;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443066
    iget-object v0, p0, LX/2dQ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 443067
    invoke-virtual {p0}, LX/2dQ;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
