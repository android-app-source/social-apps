.class public LX/2QN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0s6;

.field private final d:LX/2QO;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 407890
    const-class v0, LX/2QN;

    sput-object v0, LX/2QN;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0s6;LX/2QO;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 407891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407892
    iput-object p1, p0, LX/2QN;->b:Landroid/content/Context;

    .line 407893
    iput-object p2, p0, LX/2QN;->c:LX/0s6;

    .line 407894
    iput-object p3, p0, LX/2QN;->d:LX/2QO;

    .line 407895
    return-void
.end method

.method private static a(Ljava/io/File;J)V
    .locals 3

    .prologue
    .line 407896
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 407897
    invoke-virtual {p0, p1, p2}, Ljava/io/File;->setLastModified(J)Z

    .line 407898
    :cond_0
    :goto_0
    return-void

    .line 407899
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 407900
    :catch_0
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 407901
    if-eqz v0, :cond_0

    .line 407902
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 12

    .prologue
    const-wide/32 v10, 0x493e0

    const/4 v0, 0x0

    .line 407903
    iget-object v1, p0, LX/2QN;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 407904
    iget-object v2, p0, LX/2QN;->c:LX/0s6;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/01H;->b(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 407905
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, LX/2QN;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v4, ".sentinel"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 407906
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, LX/2QN;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    const-string v5, ".sentinel"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 407907
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    .line 407908
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    .line 407909
    sget-object v6, LX/0SF;->a:LX/0SF;

    move-object v6, v6

    .line 407910
    invoke-virtual {v6}, LX/0SF;->a()J

    move-result-wide v6

    .line 407911
    if-eqz v5, :cond_1

    if-eqz v4, :cond_1

    .line 407912
    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    iget-wide v8, v1, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    cmp-long v1, v4, v8

    if-gtz v1, :cond_0

    .line 407913
    const-string v0, "app_upgraded"

    .line 407914
    :cond_0
    :goto_0
    invoke-static {v3, v6, v7}, LX/2QN;->a(Ljava/io/File;J)V

    .line 407915
    invoke-static {v2, v6, v7}, LX/2QN;->a(Ljava/io/File;J)V

    .line 407916
    if-nez v0, :cond_5

    .line 407917
    :goto_1
    return-void

    .line 407918
    :cond_1
    if-eqz v5, :cond_2

    if-nez v4, :cond_2

    .line 407919
    const-string v0, "app_cache_cleared"

    goto :goto_0

    .line 407920
    :cond_2
    if-nez v5, :cond_3

    if-eqz v4, :cond_3

    .line 407921
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 407922
    if-eqz v1, :cond_0

    .line 407923
    sget-object v1, LX/2QN;->a:Ljava/lang/Class;

    const-string v4, "Unexpected package state"

    invoke-static {v1, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 407924
    :cond_3
    iget-wide v4, v1, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    iget-wide v8, v1, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    cmp-long v4, v4, v8

    if-nez v4, :cond_4

    iget-wide v4, v1, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    sub-long v8, v6, v10

    cmp-long v4, v4, v8

    if-ltz v4, :cond_4

    .line 407925
    const-string v0, "app_installed"

    goto :goto_0

    .line 407926
    :cond_4
    iget-wide v4, v1, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    sub-long v8, v6, v10

    cmp-long v1, v4, v8

    if-gez v1, :cond_0

    .line 407927
    const-string v0, "app_data_cleared"

    goto :goto_0

    .line 407928
    :cond_5
    iget-object v1, p0, LX/2QN;->d:LX/2QO;

    iget-object v2, p0, LX/2QN;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 407929
    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 407930
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    const/4 v3, 0x1

    :goto_2
    const-string v4, "eventName must not be empty"

    invoke-static {v3, v4}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 407931
    iget-object v3, v1, LX/2QO;->i:LX/0TD;

    new-instance v4, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;

    invoke-direct {v4, v1, v0, v2}, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;-><init>(LX/2QO;Ljava/lang/String;Ljava/lang/String;)V

    const v5, -0x79fd7593

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 407932
    goto :goto_1

    .line 407933
    :cond_6
    const/4 v3, 0x0

    goto :goto_2
.end method
