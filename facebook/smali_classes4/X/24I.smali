.class public LX/24I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aQ;",
        ">",
        "Ljava/lang/Object;",
        "LX/24J",
        "<",
        "LX/24P;",
        "TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/1Rg;


# direct methods
.method public constructor <init>(LX/1Rg;)V
    .locals 0

    .prologue
    .line 367070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367071
    iput-object p1, p0, LX/24I;->a:LX/1Rg;

    .line 367072
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)Ljava/lang/Runnable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 367073
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/util/List;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)V
    .locals 11

    .prologue
    .line 367074
    check-cast p2, LX/24P;

    check-cast p3, LX/24P;

    .line 367075
    if-ne p2, p3, :cond_1

    .line 367076
    :cond_0
    :goto_0
    return-void

    .line 367077
    :cond_1
    sget-object v0, LX/24P;->MAXIMIZED:LX/24P;

    if-ne p2, v0, :cond_2

    .line 367078
    iget-object v0, p0, LX/24I;->a:LX/1Rg;

    const-wide/16 v2, 0xc8

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, LX/1Rg;->a(JLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367079
    iget-object v4, p0, LX/24I;->a:LX/1Rg;

    const-wide/16 v6, 0xc8

    move-object v5, p4

    check-cast v5, LX/1aQ;

    invoke-interface {v5}, LX/1aQ;->getCollapsedHeight()I

    move-result v8

    move-object v5, p4

    check-cast v5, LX/1aQ;

    invoke-interface {v5}, LX/1aQ;->getExpandedFlyoutHeight()I

    move-result v5

    add-int/2addr v8, v5

    move-object v5, p4

    check-cast v5, LX/1aQ;

    invoke-interface {v5}, LX/1aQ;->getCollapsedHeight()I

    move-result v9

    const/4 v10, 0x0

    move-object v5, p4

    invoke-virtual/range {v4 .. v10}, LX/1Rg;->a(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v4

    move-object v0, v4

    .line 367080
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367081
    :cond_2
    sget-object v0, LX/24P;->MAXIMIZED:LX/24P;

    if-ne p3, v0, :cond_0

    .line 367082
    move-object v4, p4

    check-cast v4, LX/1aQ;

    invoke-interface {v4}, LX/1aQ;->getCollapsedHeight()I

    move-result v8

    move-object v4, p4

    .line 367083
    check-cast v4, LX/1aQ;

    invoke-interface {v4}, LX/1aQ;->getExpandedFlyoutHeight()I

    move-result v5

    .line 367084
    iget-object v4, p0, LX/24I;->a:LX/1Rg;

    const-wide/16 v6, 0xc8

    add-int v9, v8, v5

    const/4 v10, 0x0

    move-object v5, p4

    invoke-virtual/range {v4 .. v10}, LX/1Rg;->a(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v4

    move-object v0, v4

    .line 367085
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
