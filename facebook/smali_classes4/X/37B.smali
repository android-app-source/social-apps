.class public final LX/37B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/19Q;


# instance fields
.field public final synthetic a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

.field public final synthetic b:LX/19s;


# direct methods
.method public constructor <init>(LX/19s;Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)V
    .locals 0

    .prologue
    .line 500845
    iput-object p1, p0, LX/37B;->b:LX/19s;

    iput-object p2, p0, LX/37B;->a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 500846
    return-void
.end method

.method public final a(LX/04j;)V
    .locals 3

    .prologue
    .line 500847
    :try_start_0
    iget-object v0, p0, LX/37B;->a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    invoke-interface {p1, v0}, LX/04j;->a(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 500848
    :goto_0
    return-void

    .line 500849
    :catch_0
    move-exception v0

    .line 500850
    sget-object v1, LX/19s;->a:Ljava/lang/String;

    const-string v2, "Exception prefetching in exo service"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
