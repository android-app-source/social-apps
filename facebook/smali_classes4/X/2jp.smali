.class public LX/2jp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0W3;


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 454240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 454241
    iput-object p1, p0, LX/2jp;->a:LX/0W3;

    .line 454242
    return-void
.end method

.method public static a(LX/0QB;)LX/2jp;
    .locals 4

    .prologue
    .line 454243
    const-class v1, LX/2jp;

    monitor-enter v1

    .line 454244
    :try_start_0
    sget-object v0, LX/2jp;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 454245
    sput-object v2, LX/2jp;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 454246
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454247
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 454248
    new-instance p0, LX/2jp;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/2jp;-><init>(LX/0W3;)V

    .line 454249
    move-object v0, p0

    .line 454250
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 454251
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2jp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 454252
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 454253
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2jw;
    .locals 4

    .prologue
    .line 454254
    iget-object v0, p0, LX/2jp;->a:LX/0W3;

    sget-wide v2, LX/0X5;->br:J

    const-string v1, "disk"

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 454255
    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 454256
    sget-object v0, LX/2jw;->DISK:LX/2jw;

    :goto_1
    return-object v0

    .line 454257
    :sswitch_0
    const-string v2, "disk_v2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "disk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 454258
    :pswitch_0
    sget-object v0, LX/2jw;->DISK_V2:LX/2jw;

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x2f0d9d -> :sswitch_1
        0x63a2a45e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
