.class public interface abstract LX/2k7;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 455347
    new-instance v0, LX/0U1;

    const-string v1, "_id"

    const-string v2, "INTEGER PRIMARY KEY"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k7;->a:LX/0U1;

    .line 455348
    new-instance v0, LX/0U1;

    const-string v1, "session_id"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k7;->b:LX/0U1;

    .line 455349
    new-instance v0, LX/0U1;

    const-string v1, "sort_key"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k7;->c:LX/0U1;

    .line 455350
    new-instance v0, LX/0U1;

    const-string v1, "start_cursor"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k7;->d:LX/0U1;

    .line 455351
    new-instance v0, LX/0U1;

    const-string v1, "end_cursor"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k7;->e:LX/0U1;

    .line 455352
    new-instance v0, LX/0U1;

    const-string v1, "has_previous_page"

    const-string v2, "TINYINT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k7;->f:LX/0U1;

    .line 455353
    new-instance v0, LX/0U1;

    const-string v1, "has_next_page"

    const-string v2, "TINYINT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k7;->g:LX/0U1;

    .line 455354
    new-instance v0, LX/0U1;

    const-string v1, "row_count"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k7;->h:LX/0U1;

    .line 455355
    new-instance v0, LX/0U1;

    const-string v1, "timestamp"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k7;->i:LX/0U1;

    .line 455356
    new-instance v0, LX/0U1;

    const-string v1, "expiration_time"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k7;->j:LX/0U1;

    return-void
.end method
