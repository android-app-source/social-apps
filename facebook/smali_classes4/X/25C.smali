.class public LX/25C;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 368433
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->APPROXIMATE_LOCATION:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->PULSE:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/25C;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 368441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368434
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;)LX/0Px;

    move-result-object v2

    .line 368435
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 368436
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;

    .line 368437
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 368438
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 368439
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 368440
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368402
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 368403
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->s()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    .line 368404
    sget-object v5, LX/25C;->a:LX/0Rf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 368405
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->PULSE:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-ne v5, v6, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->k()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 368406
    :cond_0
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 368407
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 368408
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368426
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;)LX/0Px;

    move-result-object v2

    .line 368427
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 368428
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;

    .line 368429
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 368430
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 368431
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 368432
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368417
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->v()LX/0Px;

    move-result-object v2

    .line 368418
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;)LX/0Px;

    move-result-object v3

    .line 368419
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 368420
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;

    .line 368421
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v6

    .line 368422
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v6, :cond_1

    if-eqz v2, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 368423
    :cond_0
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 368424
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 368425
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "LX/4ZX;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 368410
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 368411
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->n()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_1

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;

    .line 368412
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->j()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v2, v3

    :goto_1
    if-ge v2, v9, :cond_0

    invoke-virtual {v8, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 368413
    new-instance v10, LX/4ZX;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->c()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->k()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v1, v11, v12}, LX/4ZX;-><init>(Lcom/facebook/graphql/model/GraphQLPhoto;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 368414
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 368415
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 368416
    :cond_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368409
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->o()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368442
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;)LX/0Px;

    move-result-object v2

    .line 368443
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 368444
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;

    .line 368445
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 368446
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 368447
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 368448
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "LX/25E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368346
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 368347
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->x()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;

    .line 368348
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 368349
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 368350
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 368351
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPageBrowserCategoryInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 368352
    new-instance v0, LX/4Zo;

    invoke-direct {v0}, LX/4Zo;-><init>()V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 368353
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368354
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)LX/0Px;

    move-result-object v2

    .line 368355
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->x()LX/0Px;

    move-result-object v3

    .line 368356
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 368357
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    .line 368358
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v6

    .line 368359
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v6, :cond_1

    if-eqz v3, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 368360
    :cond_0
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 368361
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 368362
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368363
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->w()LX/0Px;

    move-result-object v2

    .line 368364
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)LX/0Px;

    move-result-object v3

    .line 368365
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 368366
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;

    .line 368367
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v6

    .line 368368
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v6, :cond_1

    if-eqz v2, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 368369
    :cond_0
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 368370
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 368371
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368372
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 368373
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 368374
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    .line 368375
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLUser;->ag()Z

    move-result v5

    if-nez v5, :cond_0

    .line 368376
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 368377
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 368378
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368379
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->w()LX/0Px;

    move-result-object v2

    .line 368380
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)LX/0Px;

    move-result-object v3

    .line 368381
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 368382
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    .line 368383
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v6

    .line 368384
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v6, :cond_1

    if-eqz v2, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 368385
    :cond_0
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 368386
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 368387
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368388
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 368389
    invoke-static {p0}, LX/2ch;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    .line 368390
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 368391
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 368392
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 368393
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368394
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;

    if-eqz v0, :cond_0

    .line 368395
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;

    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;)LX/0Px;

    move-result-object v0

    .line 368396
    :goto_0
    return-object v0

    .line 368397
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    if-eqz v0, :cond_1

    .line 368398
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 368399
    :cond_1
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    if-eqz v0, :cond_2

    .line 368400
    check-cast p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 368401
    :cond_2
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "This should not be called for base class object"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0
.end method
