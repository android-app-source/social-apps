.class public LX/2Fv;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/2Fw;

.field private final d:LX/03V;

.field private final e:LX/2zf;

.field private final f:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 387399
    const-class v0, LX/2Fv;

    sput-object v0, LX/2Fv;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/2Fw;LX/03V;LX/2zf;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387401
    iput-object p1, p0, LX/2Fv;->b:Landroid/content/Context;

    .line 387402
    iput-object p2, p0, LX/2Fv;->c:LX/2Fw;

    .line 387403
    iput-object p3, p0, LX/2Fv;->d:LX/03V;

    .line 387404
    iput-object p4, p0, LX/2Fv;->e:LX/2zf;

    .line 387405
    iput-object p5, p0, LX/2Fv;->f:LX/0W3;

    .line 387406
    return-void
.end method

.method private static a(I)I
    .locals 3

    .prologue
    .line 387407
    packed-switch p0, :pswitch_data_0

    .line 387408
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized value for location request priority: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387409
    :pswitch_0
    const/16 v0, 0x69

    .line 387410
    :goto_0
    return v0

    .line 387411
    :pswitch_1
    const/16 v0, 0x68

    goto :goto_0

    .line 387412
    :pswitch_2
    const/16 v0, 0x66

    goto :goto_0

    .line 387413
    :pswitch_3
    const/16 v0, 0x64

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    .line 387414
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 387415
    iget-object v1, p0, LX/2Fv;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/1od;->a(Landroid/content/Context;)I

    move-result v0

    .line 387416
    if-nez v0, :cond_2

    .line 387417
    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iget-object v1, p0, LX/2Fv;->f:LX/0W3;

    sget-wide v2, LX/0X5;->J:J

    const/4 v4, 0x2

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v1

    invoke-static {v1}, LX/2Fv;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iget-object v1, p0, LX/2Fv;->f:LX/0W3;

    sget-wide v2, LX/0X5;->E:J

    invoke-interface {v1, v2, v3}, LX/0W4;->c(J)J

    move-result-wide v2

    const-wide v11, 0x7fffffffffffffffL

    const-wide/16 v9, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long v7, v11, v5

    cmp-long v7, v2, v7

    if-lez v7, :cond_3

    iput-wide v11, v0, Lcom/google/android/gms/location/LocationRequest;->e:J

    :goto_0
    iget-wide v5, v0, Lcom/google/android/gms/location/LocationRequest;->e:J

    cmp-long v5, v5, v9

    if-gez v5, :cond_0

    iput-wide v9, v0, Lcom/google/android/gms/location/LocationRequest;->e:J

    :cond_0
    move-object v0, v0

    .line 387418
    iget-object v1, p0, LX/2Fv;->f:LX/0W3;

    sget-wide v2, LX/0X5;->G:J

    invoke-interface {v1, v2, v3}, LX/0W4;->c(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iget-object v1, p0, LX/2Fv;->f:LX/0W3;

    sget-wide v2, LX/0X5;->F:J

    invoke-interface {v1, v2, v3}, LX/0W4;->c(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->c(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iget-object v1, p0, LX/2Fv;->f:LX/0W3;

    sget-wide v2, LX/0X5;->H:J

    invoke-interface {v1, v2, v3}, LX/0W4;->c(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/location/LocationRequest;->e(J)V

    iput-wide v2, v0, Lcom/google/android/gms/location/LocationRequest;->h:J

    move-object v0, v0

    .line 387419
    iget-object v1, p0, LX/2Fv;->f:LX/0W3;

    sget-wide v2, LX/0X5;->K:J

    invoke-interface {v1, v2, v3}, LX/0W4;->g(J)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(F)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    .line 387420
    iget-object v1, p0, LX/2Fv;->f:LX/0W3;

    sget-wide v2, LX/0X5;->I:J

    const/4 v4, -0x1

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v1

    .line 387421
    if-lez v1, :cond_1

    .line 387422
    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->b(I)Lcom/google/android/gms/location/LocationRequest;

    .line 387423
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/2Fv;->e:LX/2zf;

    const/4 v2, 0x1

    .line 387424
    new-instance v3, Landroid/content/Intent;

    iget-object v4, v1, LX/2zf;->a:Landroid/content/Context;

    const-class v5, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 387425
    iget-object v4, v1, LX/2zf;->b:LX/0aU;

    const-string v5, "BACKGROUND_LOCATION_REPORTING_ACTION_LOCATION_UPDATE"

    invoke-virtual {v4, v5}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 387426
    iget-object v4, v1, LX/2zf;->a:Landroid/content/Context;

    const/high16 v5, 0x8000000

    invoke-static {v4, v2, v3, v5}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    move-object v1, v3

    .line 387427
    iget-object v2, p0, LX/2Fv;->c:LX/2Fw;

    invoke-virtual {v2, v1, v0}, LX/2Fw;->a(Landroid/app/PendingIntent;Lcom/google/android/gms/location/LocationRequest;)V
    :try_end_0
    .catch LX/6ZF; {:try_start_0 .. :try_end_0} :catch_0

    .line 387428
    :cond_2
    :goto_1
    return-void

    .line 387429
    :catch_0
    move-exception v0

    .line 387430
    iget-object v1, p0, LX/2Fv;->d:LX/03V;

    sget-object v2, LX/2Fv;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Unable to start location collection"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    add-long/2addr v5, v2

    iput-wide v5, v0, Lcom/google/android/gms/location/LocationRequest;->e:J

    goto/16 :goto_0
.end method
