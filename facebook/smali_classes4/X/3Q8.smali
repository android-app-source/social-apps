.class public LX/3Q8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Landroid/content/ComponentName;

.field public static final b:Landroid/content/ComponentName;

.field private static volatile e:LX/3Q8;


# instance fields
.field private final c:Landroid/app/ActivityManager;

.field private final d:LX/1Ml;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 563202
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.apps.maps"

    const-string v2, "com.google.android.maps.driveabout.app.NavigationActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3Q8;->a:Landroid/content/ComponentName;

    .line 563203
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.apps.maps"

    const-string v2, "com.google.android.maps.MapsActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3Q8;->b:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>(Landroid/app/ActivityManager;LX/1Ml;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 563204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563205
    iput-object p1, p0, LX/3Q8;->c:Landroid/app/ActivityManager;

    .line 563206
    iput-object p2, p0, LX/3Q8;->d:LX/1Ml;

    .line 563207
    return-void
.end method

.method public static a(LX/0QB;)LX/3Q8;
    .locals 5

    .prologue
    .line 563208
    sget-object v0, LX/3Q8;->e:LX/3Q8;

    if-nez v0, :cond_1

    .line 563209
    const-class v1, LX/3Q8;

    monitor-enter v1

    .line 563210
    :try_start_0
    sget-object v0, LX/3Q8;->e:LX/3Q8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 563211
    if-eqz v2, :cond_0

    .line 563212
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 563213
    new-instance p0, LX/3Q8;

    invoke-static {v0}, LX/0VU;->b(LX/0QB;)Landroid/app/ActivityManager;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v4

    check-cast v4, LX/1Ml;

    invoke-direct {p0, v3, v4}, LX/3Q8;-><init>(Landroid/app/ActivityManager;LX/1Ml;)V

    .line 563214
    move-object v0, p0

    .line 563215
    sput-object v0, LX/3Q8;->e:LX/3Q8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 563216
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 563217
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 563218
    :cond_1
    sget-object v0, LX/3Q8;->e:LX/3Q8;

    return-object v0

    .line 563219
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 563220
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3Q8;Landroid/content/ComponentName;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 563221
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_0

    iget-object v2, p0, LX/3Q8;->d:LX/1Ml;

    const-string v3, "android.permission.GET_TASKS"

    invoke-virtual {v2, v3}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 563222
    :cond_1
    :goto_0
    return v0

    .line 563223
    :cond_2
    iget-object v2, p0, LX/3Q8;->c:Landroid/app/ActivityManager;

    invoke-virtual {v2, v1}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 563224
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 563225
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v0, p1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/3Q8;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 563226
    iget-object v0, p0, LX/3Q8;->c:Landroid/app/ActivityManager;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v0

    .line 563227
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 563228
    iget-boolean v2, v0, Landroid/app/ActivityManager$RunningServiceInfo;->started:Z

    if-eqz v2, :cond_0

    iget-object v2, v0, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    if-eqz v2, :cond_0

    .line 563229
    iget-object v2, v0, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563230
    const/4 v0, 0x1

    .line 563231
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
