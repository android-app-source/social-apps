.class public LX/3Po;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HL",
        "<",
        "Lcom/facebook/feed/protocol/QuestionAddPollOptionModels$QuestionAddResponseMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3Pp;


# direct methods
.method public constructor <init>(LX/3Pp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562218
    iput-object p1, p0, LX/3Po;->a:LX/3Pp;

    .line 562219
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/4VT;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 562220
    check-cast p1, Lcom/facebook/feed/protocol/QuestionAddPollOptionModels$QuestionAddResponseMutationModel;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 562221
    invoke-virtual {p1}, Lcom/facebook/feed/protocol/QuestionAddPollOptionModels$QuestionAddResponseMutationModel;->a()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    move-result-object v4

    .line 562222
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->k()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->k()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 562223
    :goto_0
    return-object v0

    .line 562224
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->k()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel;->a()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel$NodeModel;

    move-result-object v5

    .line 562225
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v2

    :goto_1
    if-eqz v0, :cond_5

    move v0, v2

    :goto_2
    if-eqz v0, :cond_7

    move-object v0, v1

    .line 562226
    goto :goto_0

    .line 562227
    :cond_3
    invoke-virtual {v5}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 562228
    if-nez v0, :cond_4

    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_1

    .line 562229
    :cond_5
    invoke-virtual {v5}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 562230
    invoke-virtual {v6, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    move v0, v2

    goto :goto_2

    :cond_6
    move v0, v3

    goto :goto_2

    .line 562231
    :cond_7
    invoke-virtual {v5}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 562232
    invoke-virtual {v4}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 562233
    new-instance v1, LX/Amg;

    invoke-direct {v1, v2, v4, v0}, LX/Amg;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 562234
    move-object v0, v1

    .line 562235
    goto :goto_0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 562236
    const-class v0, Lcom/facebook/feed/protocol/QuestionAddPollOptionModels$QuestionAddResponseMutationModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/feed/protocol/QuestionAddPollOptionModels$QuestionAddResponseMutationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 562237
    const/4 v0, 0x0

    return-object v0
.end method
