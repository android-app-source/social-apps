.class public LX/31G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ib;


# instance fields
.field private final a:LX/1HY;

.field private final b:LX/1HY;

.field private final c:LX/1Ao;

.field private final d:I


# direct methods
.method public constructor <init>(LX/1HY;LX/1HY;LX/1Ao;I)V
    .locals 0

    .prologue
    .line 486763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486764
    iput-object p1, p0, LX/31G;->a:LX/1HY;

    .line 486765
    iput-object p2, p0, LX/31G;->b:LX/1HY;

    .line 486766
    iput-object p3, p0, LX/31G;->c:LX/1Ao;

    .line 486767
    iput p4, p0, LX/31G;->d:I

    .line 486768
    return-void
.end method


# virtual methods
.method public final a(LX/1bf;Ljava/lang/Object;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/1eg;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "Ljava/lang/Object;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ")",
            "LX/1eg",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 486748
    iget-object v0, p0, LX/31G;->c:LX/1Ao;

    invoke-virtual {v0, p1, p2}, LX/1Ao;->c(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v2

    .line 486749
    iget-object v0, p0, LX/31G;->b:LX/1HY;

    invoke-virtual {v0, v2}, LX/1HY;->a(LX/1bh;)Z

    move-result v0

    .line 486750
    iget-object v1, p0, LX/31G;->a:LX/1HY;

    invoke-virtual {v1, v2}, LX/1HY;->a(LX/1bh;)Z

    move-result v1

    .line 486751
    if-nez v0, :cond_0

    if-nez v1, :cond_1

    .line 486752
    :cond_0
    iget-object v1, p0, LX/31G;->b:LX/1HY;

    .line 486753
    iget-object v0, p0, LX/31G;->a:LX/1HY;

    .line 486754
    :goto_0
    invoke-virtual {v1, v2, p3}, LX/1HY;->a(LX/1bh;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/1eg;

    move-result-object v1

    new-instance v3, LX/4dt;

    invoke-direct {v3, p0, v0, v2, p3}, LX/4dt;-><init>(LX/31G;LX/1HY;LX/1bh;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-virtual {v1, v3}, LX/1eg;->b(LX/1ex;)LX/1eg;

    move-result-object v0

    return-object v0

    .line 486755
    :cond_1
    iget-object v1, p0, LX/31G;->a:LX/1HY;

    .line 486756
    iget-object v0, p0, LX/31G;->b:LX/1HY;

    goto :goto_0
.end method

.method public final a(LX/1FL;LX/1bf;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 486757
    iget-object v0, p0, LX/31G;->c:LX/1Ao;

    invoke-virtual {v0, p2, p3}, LX/1Ao;->c(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v0

    .line 486758
    invoke-virtual {p1}, LX/1FL;->i()I

    move-result v1

    .line 486759
    if-lez v1, :cond_0

    iget v2, p0, LX/31G;->d:I

    if-ge v1, v2, :cond_0

    .line 486760
    iget-object v1, p0, LX/31G;->b:LX/1HY;

    invoke-virtual {v1, v0, p1}, LX/1HY;->a(LX/1bh;LX/1FL;)V

    .line 486761
    :goto_0
    return-void

    .line 486762
    :cond_0
    iget-object v1, p0, LX/31G;->a:LX/1HY;

    invoke-virtual {v1, v0, p1}, LX/1HY;->a(LX/1bh;LX/1FL;)V

    goto :goto_0
.end method
