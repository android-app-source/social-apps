.class public final LX/2co;
.super LX/1eP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1eP",
        "<",
        "LX/1FL;",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1cH;

.field private final b:LX/1cW;

.field private c:LX/03R;


# direct methods
.method public constructor <init>(LX/1cH;LX/1cd;LX/1cW;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 441993
    iput-object p1, p0, LX/2co;->a:LX/1cH;

    .line 441994
    invoke-direct {p0, p2}, LX/1eP;-><init>(LX/1cd;)V

    .line 441995
    iput-object p3, p0, LX/2co;->b:LX/1cW;

    .line 441996
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2co;->c:LX/03R;

    .line 441997
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 441998
    check-cast p1, LX/1FL;

    .line 441999
    iget-object v0, p0, LX/2co;->c:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    .line 442000
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 442001
    invoke-virtual {p1}, LX/1FL;->b()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, LX/1la;->b(Ljava/io/InputStream;)LX/1lW;

    move-result-object v0

    .line 442002
    invoke-static {v0}, LX/1ld;->b(LX/1lW;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 442003
    sget-object v1, LX/4ek;->b:Lcom/facebook/imagepipeline/nativecode/WebpTranscoderImpl;

    move-object v1, v1

    .line 442004
    if-nez v1, :cond_4

    .line 442005
    sget-object v0, LX/03R;->NO:LX/03R;

    .line 442006
    :goto_0
    move-object v0, v0

    .line 442007
    iput-object v0, p0, LX/2co;->c:LX/03R;

    .line 442008
    :cond_0
    iget-object v0, p0, LX/2co;->c:LX/03R;

    sget-object v1, LX/03R;->NO:LX/03R;

    if-ne v0, v1, :cond_2

    .line 442009
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 442010
    invoke-virtual {v0, p1, p2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 442011
    :cond_1
    :goto_1
    return-void

    .line 442012
    :cond_2
    if-eqz p2, :cond_1

    .line 442013
    iget-object v0, p0, LX/2co;->c:LX/03R;

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_3

    if-eqz p1, :cond_3

    .line 442014
    iget-object v0, p0, LX/2co;->a:LX/1cH;

    .line 442015
    iget-object v1, p0, LX/1eP;->a:LX/1cd;

    move-object v1, v1

    .line 442016
    iget-object v2, p0, LX/2co;->b:LX/1cW;

    .line 442017
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 442018
    invoke-static {p1}, LX/1FL;->a(LX/1FL;)LX/1FL;

    move-result-object v9

    .line 442019
    new-instance v3, Lcom/facebook/imagepipeline/producers/WebpTranscodeProducer$1;

    .line 442020
    iget-object v4, v2, LX/1cW;->c:LX/1BV;

    move-object v6, v4

    .line 442021
    const-string v7, "WebpTranscodeProducer"

    .line 442022
    iget-object v4, v2, LX/1cW;->b:Ljava/lang/String;

    move-object v8, v4

    .line 442023
    move-object v4, v0

    move-object v5, v1

    invoke-direct/range {v3 .. v9}, Lcom/facebook/imagepipeline/producers/WebpTranscodeProducer$1;-><init>(LX/1cH;LX/1cd;LX/1BV;Ljava/lang/String;Ljava/lang/String;LX/1FL;)V

    .line 442024
    iget-object v4, v0, LX/1cH;->a:Ljava/util/concurrent/Executor;

    const v5, -0x6870007f

    invoke-static {v4, v3, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 442025
    goto :goto_1

    .line 442026
    :cond_3
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 442027
    invoke-virtual {v0, p1, p2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    goto :goto_1

    .line 442028
    :cond_4
    invoke-virtual {v1, v0}, Lcom/facebook/imagepipeline/nativecode/WebpTranscoderImpl;->a(LX/1lW;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 442029
    :cond_6
    sget-object v1, LX/1lW;->a:LX/1lW;

    if-ne v0, v1, :cond_7

    .line 442030
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0

    .line 442031
    :cond_7
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method
