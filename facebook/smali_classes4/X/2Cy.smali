.class public LX/2Cy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 383568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383569
    iput-object p1, p0, LX/2Cy;->a:LX/0Zb;

    .line 383570
    return-void
.end method

.method public static a(LX/0QB;)LX/2Cy;
    .locals 1

    .prologue
    .line 383567
    invoke-static {p0}, LX/2Cy;->b(LX/0QB;)LX/2Cy;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2Cy;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 383544
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "initial_app_launch_experiment_exposure"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "experiment"

    .line 383545
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 383546
    move-object v0, v0

    .line 383547
    const-string v1, "exp_name"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "exp_group"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 383548
    iget-object v1, p0, LX/2Cy;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 383549
    return-void
.end method

.method public static b(LX/0QB;)LX/2Cy;
    .locals 2

    .prologue
    .line 383565
    new-instance v1, LX/2Cy;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/2Cy;-><init>(LX/0Zb;)V

    .line 383566
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/03R;)V
    .locals 1

    .prologue
    .line 383563
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/2Cy;->a(Ljava/lang/String;LX/03R;Ljava/util/Map;)V

    .line 383564
    return-void
.end method

.method public final a(Ljava/lang/String;LX/03R;Ljava/util/Map;)V
    .locals 2
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/03R;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 383557
    sget-object v0, LX/286;->a:[I

    invoke-virtual {p2}, LX/03R;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 383558
    const-string v0, "unset"

    .line 383559
    :goto_0
    invoke-static {p0, p1, v0, p3}, LX/2Cy;->a(LX/2Cy;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 383560
    return-void

    .line 383561
    :pswitch_0
    const-string v0, "test"

    goto :goto_0

    .line 383562
    :pswitch_1
    const-string v0, "control"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 383555
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/2Cy;->a(LX/2Cy;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 383556
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 383550
    const/4 v0, 0x0

    .line 383551
    if-eqz p2, :cond_0

    const-string v1, "test"

    .line 383552
    :goto_0
    invoke-static {p0, p1, v1, v0}, LX/2Cy;->a(LX/2Cy;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 383553
    return-void

    .line 383554
    :cond_0
    const-string v1, "control"

    goto :goto_0
.end method
