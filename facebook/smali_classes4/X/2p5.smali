.class public final enum LX/2p5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2p5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2p5;

.field public static final enum BOTTOM:LX/2p5;

.field public static final enum CENTER:LX/2p5;

.field public static final enum END:LX/2p5;

.field public static final enum INNER_BOTTOM:LX/2p5;

.field public static final enum INNER_CENTER:LX/2p5;

.field public static final enum INNER_END:LX/2p5;

.field public static final enum INNER_START:LX/2p5;

.field public static final enum INNER_TOP:LX/2p5;

.field public static final enum NONE:LX/2p5;

.field public static final enum START:LX/2p5;

.field public static final enum TOP:LX/2p5;


# instance fields
.field private mIsHorizontal:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 467610
    new-instance v0, LX/2p5;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4, v3}, LX/2p5;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/2p5;->NONE:LX/2p5;

    .line 467611
    new-instance v0, LX/2p5;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3, v3}, LX/2p5;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/2p5;->TOP:LX/2p5;

    .line 467612
    new-instance v0, LX/2p5;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v5, v3}, LX/2p5;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/2p5;->BOTTOM:LX/2p5;

    .line 467613
    new-instance v0, LX/2p5;

    const-string v1, "START"

    invoke-direct {v0, v1, v6, v4}, LX/2p5;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/2p5;->START:LX/2p5;

    .line 467614
    new-instance v0, LX/2p5;

    const-string v1, "END"

    invoke-direct {v0, v1, v7, v4}, LX/2p5;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/2p5;->END:LX/2p5;

    .line 467615
    new-instance v0, LX/2p5;

    const-string v1, "INNER_TOP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/2p5;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/2p5;->INNER_TOP:LX/2p5;

    .line 467616
    new-instance v0, LX/2p5;

    const-string v1, "INNER_BOTTOM"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/2p5;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/2p5;->INNER_BOTTOM:LX/2p5;

    .line 467617
    new-instance v0, LX/2p5;

    const-string v1, "INNER_START"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v4}, LX/2p5;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/2p5;->INNER_START:LX/2p5;

    .line 467618
    new-instance v0, LX/2p5;

    const-string v1, "INNER_END"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v4}, LX/2p5;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/2p5;->INNER_END:LX/2p5;

    .line 467619
    new-instance v0, LX/2p5;

    const-string v1, "CENTER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, LX/2p5;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/2p5;->CENTER:LX/2p5;

    .line 467620
    new-instance v0, LX/2p5;

    const-string v1, "INNER_CENTER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/2p5;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/2p5;->INNER_CENTER:LX/2p5;

    .line 467621
    const/16 v0, 0xb

    new-array v0, v0, [LX/2p5;

    sget-object v1, LX/2p5;->NONE:LX/2p5;

    aput-object v1, v0, v4

    sget-object v1, LX/2p5;->TOP:LX/2p5;

    aput-object v1, v0, v3

    sget-object v1, LX/2p5;->BOTTOM:LX/2p5;

    aput-object v1, v0, v5

    sget-object v1, LX/2p5;->START:LX/2p5;

    aput-object v1, v0, v6

    sget-object v1, LX/2p5;->END:LX/2p5;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/2p5;->INNER_TOP:LX/2p5;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2p5;->INNER_BOTTOM:LX/2p5;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2p5;->INNER_START:LX/2p5;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2p5;->INNER_END:LX/2p5;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2p5;->CENTER:LX/2p5;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/2p5;->INNER_CENTER:LX/2p5;

    aput-object v2, v0, v1

    sput-object v0, LX/2p5;->$VALUES:[LX/2p5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 467622
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 467623
    iput-boolean p3, p0, LX/2p5;->mIsHorizontal:Z

    .line 467624
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2p5;
    .locals 1

    .prologue
    .line 467625
    const-class v0, LX/2p5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2p5;

    return-object v0
.end method

.method public static values()[LX/2p5;
    .locals 1

    .prologue
    .line 467626
    sget-object v0, LX/2p5;->$VALUES:[LX/2p5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2p5;

    return-object v0
.end method


# virtual methods
.method public final isHorizontal()Z
    .locals 1

    .prologue
    .line 467627
    iget-boolean v0, p0, LX/2p5;->mIsHorizontal:Z

    return v0
.end method
