.class public LX/2Ct;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:LX/2Cu;

.field public static final c:LX/2Cu;

.field private static volatile r:LX/2Ct;


# instance fields
.field private final d:LX/0Uh;

.field private final e:LX/0W3;

.field public final f:LX/2Cw;

.field private final g:LX/2D0;

.field public final h:Landroid/content/Context;

.field public final i:LX/0aU;

.field private final j:LX/2D5;

.field public final k:LX/2Cx;

.field private final l:LX/0Xl;

.field public final m:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final n:LX/0SG;

.field public final o:LX/03V;

.field public final p:LX/2D6;

.field private final q:LX/3C0;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    .line 383394
    const-class v0, LX/2Ct;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Ct;->a:Ljava/lang/String;

    .line 383395
    new-instance v0, LX/2Cu;

    sget-object v1, LX/2Cv;->BALANCED_POWER_AND_ACCURACY:LX/2Cv;

    const-wide/32 v2, 0xdbba0

    const-wide/32 v4, 0x2bf20

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LX/2Cu;-><init>(LX/2Cv;JJF)V

    sput-object v0, LX/2Ct;->b:LX/2Cu;

    .line 383396
    new-instance v0, LX/2Cu;

    sget-object v1, LX/2Ct;->b:LX/2Cu;

    iget-object v1, v1, LX/2Cu;->a:LX/2Cv;

    const-wide/32 v2, 0xea60

    const-wide/16 v4, 0x7530

    sget-object v6, LX/2Ct;->b:LX/2Cu;

    iget v6, v6, LX/2Cu;->d:F

    invoke-direct/range {v0 .. v6}, LX/2Cu;-><init>(LX/2Cv;JJF)V

    sput-object v0, LX/2Ct;->c:LX/2Cu;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/0W3;LX/2Cw;LX/2D0;Landroid/content/Context;LX/0aU;LX/2D5;LX/2Cx;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/03V;LX/2D6;LX/3C0;)V
    .locals 0
    .param p9    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 383378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383379
    iput-object p1, p0, LX/2Ct;->d:LX/0Uh;

    .line 383380
    iput-object p2, p0, LX/2Ct;->e:LX/0W3;

    .line 383381
    iput-object p3, p0, LX/2Ct;->f:LX/2Cw;

    .line 383382
    iput-object p4, p0, LX/2Ct;->g:LX/2D0;

    .line 383383
    iput-object p5, p0, LX/2Ct;->h:Landroid/content/Context;

    .line 383384
    iput-object p6, p0, LX/2Ct;->i:LX/0aU;

    .line 383385
    iput-object p7, p0, LX/2Ct;->j:LX/2D5;

    .line 383386
    iput-object p8, p0, LX/2Ct;->k:LX/2Cx;

    .line 383387
    iput-object p9, p0, LX/2Ct;->l:LX/0Xl;

    .line 383388
    iput-object p10, p0, LX/2Ct;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 383389
    iput-object p11, p0, LX/2Ct;->n:LX/0SG;

    .line 383390
    iput-object p12, p0, LX/2Ct;->o:LX/03V;

    .line 383391
    iput-object p13, p0, LX/2Ct;->p:LX/2D6;

    .line 383392
    iput-object p14, p0, LX/2Ct;->q:LX/3C0;

    .line 383393
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ct;
    .locals 3

    .prologue
    .line 383368
    sget-object v0, LX/2Ct;->r:LX/2Ct;

    if-nez v0, :cond_1

    .line 383369
    const-class v1, LX/2Ct;

    monitor-enter v1

    .line 383370
    :try_start_0
    sget-object v0, LX/2Ct;->r:LX/2Ct;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 383371
    if-eqz v2, :cond_0

    .line 383372
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2Ct;->b(LX/0QB;)LX/2Ct;

    move-result-object v0

    sput-object v0, LX/2Ct;->r:LX/2Ct;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 383373
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 383374
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 383375
    :cond_1
    sget-object v0, LX/2Ct;->r:LX/2Ct;

    return-object v0

    .line 383376
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 383377
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2Ct;LX/2Cu;)V
    .locals 9

    .prologue
    .line 383344
    invoke-direct {p0}, LX/2Ct;->d()Landroid/app/PendingIntent;

    move-result-object v0

    .line 383345
    :try_start_0
    iget-object v1, p0, LX/2Ct;->j:LX/2D5;

    invoke-interface {v1, v0, p1}, LX/2D5;->a(Landroid/app/PendingIntent;LX/2Cu;)V

    .line 383346
    iget-object v0, p0, LX/2Ct;->q:LX/3C0;

    .line 383347
    iget-object v3, v0, LX/3C0;->e:LX/0W3;

    sget-wide v5, LX/0X5;->ay:J

    invoke-interface {v3, v5, v6}, LX/0W4;->a(J)Z

    move-result v3

    if-nez v3, :cond_0

    .line 383348
    :goto_0
    iget-object v0, p0, LX/2Ct;->k:LX/2Cx;

    sget-object v1, LX/1vX;->c:LX/1vX;

    move-object v1, v1

    .line 383349
    iget-object v2, p0, LX/2Ct;->h:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/1od;->a(Landroid/content/Context;)I

    move-result v1

    .line 383350
    iget-object v2, v0, LX/2Cx;->b:LX/0Zb;

    const-string v3, "background_location_location_request_start"

    invoke-static {v3}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "google_api_availability"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catch LX/6ZF; {:try_start_0 .. :try_end_0} :catch_0

    .line 383351
    :goto_1
    return-void

    .line 383352
    :catch_0
    move-exception v0

    .line 383353
    iget-object v1, p0, LX/2Ct;->k:LX/2Cx;

    .line 383354
    iget-object v2, v1, LX/2Cx;->b:LX/0Zb;

    const-string v3, "background_location_location_request_failure"

    invoke-static {v3}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "type"

    iget-object v5, v0, LX/6ZF;->type:LX/6ZE;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "cause"

    invoke-virtual {v0}, LX/6ZF;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 383355
    goto :goto_1

    .line 383356
    :cond_0
    :try_start_1
    iget-object v3, v0, LX/3C0;->e:LX/0W3;

    sget-wide v5, LX/0X5;->az:J

    invoke-interface {v3, v5, v6}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v3

    .line 383357
    iget-object v4, v0, LX/3C0;->e:LX/0W3;

    sget-wide v5, LX/0X5;->aA:J

    invoke-interface {v4, v5, v6}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 383358
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch LX/6ZF; {:try_start_1 .. :try_end_1} :catch_0

    .line 383359
    :try_start_2
    invoke-static {v3}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch LX/6ZF; {:try_start_2 .. :try_end_2} :catch_0

    .line 383360
    :goto_2
    :try_start_3
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;
    :try_end_3
    .catch LX/6ZF; {:try_start_3 .. :try_end_3} :catch_0

    .line 383361
    :try_start_4
    invoke-static {v3}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catch LX/6ZF; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_3

    .line 383362
    :catch_1
    goto :goto_3

    .line 383363
    :cond_1
    iget-object v3, v0, LX/3C0;->b:LX/3C1;

    .line 383364
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    iget-object v6, v0, LX/3C0;->c:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    iget-object v6, v0, LX/3C0;->d:LX/0aU;

    const-string v7, "BLE_SCAN_ACTION_BLE_UPDATE"

    invoke-virtual {v6, v7}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 383365
    iget-object v6, v0, LX/3C0;->c:Landroid/content/Context;

    const/4 v7, 0x0

    const/high16 v8, 0x8000000

    invoke-static {v6, v7, v4, v8}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    move-object v4, v4

    .line 383366
    iget-object v6, v3, LX/3C1;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v7, Lcom/facebook/blescan/BleScanBackgroundListener$1;

    invoke-direct {v7, v3, v5, v4}, Lcom/facebook/blescan/BleScanBackgroundListener$1;-><init>(LX/3C1;Ljava/util/List;Landroid/app/PendingIntent;)V

    const v8, 0x15c3a2fc

    invoke-static {v6, v7, v8}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 383367
    goto/16 :goto_0

    :catch_2
    goto :goto_2
.end method

.method private static b(LX/0QB;)LX/2Ct;
    .locals 15

    .prologue
    .line 383339
    new-instance v0, LX/2Ct;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v2

    check-cast v2, LX/0W3;

    invoke-static {p0}, LX/2Cw;->a(LX/0QB;)LX/2Cw;

    move-result-object v3

    check-cast v3, LX/2Cw;

    .line 383340
    new-instance v8, LX/2D0;

    invoke-static {p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v4

    check-cast v4, LX/0y3;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-direct {v8, v4, v5, v6, v7}, LX/2D0;-><init>(LX/0y3;LX/1Ck;LX/0tX;LX/03V;)V

    .line 383341
    move-object v4, v8

    .line 383342
    check-cast v4, LX/2D0;

    const-class v5, Landroid/content/Context;

    invoke-interface {p0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {p0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v6

    check-cast v6, LX/0aU;

    invoke-static {p0}, LX/2D1;->b(LX/0QB;)LX/2D5;

    move-result-object v7

    check-cast v7, LX/2D5;

    invoke-static {p0}, LX/2Cx;->a(LX/0QB;)LX/2Cx;

    move-result-object v8

    check-cast v8, LX/2Cx;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v9

    check-cast v9, LX/0Xl;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static {p0}, LX/2D6;->a(LX/0QB;)LX/2D6;

    move-result-object v13

    check-cast v13, LX/2D6;

    invoke-static {p0}, LX/3C0;->a(LX/0QB;)LX/3C0;

    move-result-object v14

    check-cast v14, LX/3C0;

    invoke-direct/range {v0 .. v14}, LX/2Ct;-><init>(LX/0Uh;LX/0W3;LX/2Cw;LX/2D0;Landroid/content/Context;LX/0aU;LX/2D5;LX/2Cx;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/03V;LX/2D6;LX/3C0;)V

    .line 383343
    return-object v0
.end method

.method public static c(LX/2Ct;)V
    .locals 2

    .prologue
    .line 383334
    invoke-direct {p0}, LX/2Ct;->d()Landroid/app/PendingIntent;

    move-result-object v0

    .line 383335
    iget-object v1, p0, LX/2Ct;->j:LX/2D5;

    invoke-interface {v1, v0}, LX/2D5;->a(Landroid/app/PendingIntent;)V

    .line 383336
    iget-object v0, p0, LX/2Ct;->k:LX/2Cx;

    .line 383337
    iget-object v1, v0, LX/2Cx;->b:LX/0Zb;

    const-string p0, "background_location_location_request_stop"

    invoke-static {p0}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 383338
    return-void
.end method

.method private d()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 383332
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, LX/2Ct;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, LX/2Ct;->i:LX/0aU;

    const-string v2, "BACKGROUND_LOCATION_REPORTING_ACTION_LOCATION_UPDATE"

    invoke-virtual {v1, v2}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 383333
    iget-object v1, p0, LX/2Ct;->h:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/2Ct;)LX/2Cu;
    .locals 8

    .prologue
    .line 383291
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 383292
    iget-object v1, p0, LX/2Ct;->h:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/1od;->a(Landroid/content/Context;)I

    move-result v0

    .line 383293
    if-eqz v0, :cond_0

    .line 383294
    sget-object v0, LX/2Ct;->b:LX/2Cu;

    .line 383295
    :goto_0
    return-object v0

    .line 383296
    :cond_0
    iget-object v0, p0, LX/2Ct;->e:LX/0W3;

    sget-wide v2, LX/0X5;->at:J

    invoke-interface {v0, v2, v3}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v0

    .line 383297
    :try_start_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/2Cv;->valueOf(Ljava/lang/String;)LX/2Cv;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 383298
    :goto_1
    move-object v1, v1

    .line 383299
    new-instance v0, LX/2Cu;

    if-eqz v1, :cond_1

    :goto_2
    iget-object v2, p0, LX/2Ct;->e:LX/0W3;

    sget-wide v4, LX/0X5;->aj:J

    invoke-interface {v2, v4, v5}, LX/0W4;->c(J)J

    move-result-wide v2

    iget-object v4, p0, LX/2Ct;->e:LX/0W3;

    sget-wide v6, LX/0X5;->ak:J

    invoke-interface {v4, v6, v7}, LX/0W4;->c(J)J

    move-result-wide v4

    sget-object v6, LX/2Ct;->b:LX/2Cu;

    iget v6, v6, LX/2Cu;->d:F

    invoke-direct/range {v0 .. v6}, LX/2Cu;-><init>(LX/2Cv;JJF)V

    goto :goto_0

    :cond_1
    sget-object v1, LX/2Ct;->b:LX/2Cu;

    iget-object v1, v1, LX/2Cu;->a:LX/2Cv;

    goto :goto_2

    .line 383300
    :catch_0
    iget-object v1, p0, LX/2Ct;->o:LX/03V;

    sget-object v2, LX/2Ct;->a:Ljava/lang/String;

    const-string v3, "Invalid high frequency priority value"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 383301
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final b()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 383325
    iget-object v0, p0, LX/2Ct;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2Fn;->c:LX/0Tn;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 383326
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/2Ct;->n:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 383327
    iget-object v0, p0, LX/2Ct;->f:LX/2Cw;

    invoke-virtual {v0}, LX/2Cw;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383328
    invoke-static {p0}, LX/2Ct;->e(LX/2Ct;)LX/2Cu;

    move-result-object v0

    invoke-static {p0, v0}, LX/2Ct;->a(LX/2Ct;LX/2Cu;)V

    .line 383329
    :goto_0
    iget-object v0, p0, LX/2Ct;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2Fn;->c:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 383330
    :cond_0
    return-void

    .line 383331
    :cond_1
    invoke-static {p0}, LX/2Ct;->c(LX/2Ct;)V

    goto :goto_0
.end method

.method public final init()V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 383302
    iget-object v0, p0, LX/2Ct;->d:LX/0Uh;

    const/16 v1, 0x2e9

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_0

    .line 383303
    :goto_0
    return-void

    .line 383304
    :cond_0
    iget-object v0, p0, LX/2Ct;->f:LX/2Cw;

    invoke-virtual {v0}, LX/2Cw;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 383305
    iget-object v0, p0, LX/2Ct;->e:LX/0W3;

    sget-wide v2, LX/0X5;->ae:J

    invoke-interface {v0, v2, v3}, LX/0W4;->b(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383306
    iget-object v0, p0, LX/2Ct;->h:Landroid/content/Context;

    .line 383307
    const-string v4, "alarm"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/AlarmManager;

    .line 383308
    const-wide/16 v6, 0x0

    invoke-static {v0, v6, v7}, LX/Hln;->d(Landroid/content/Context;J)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 383309
    :cond_1
    iget-object v0, p0, LX/2Ct;->p:LX/2D6;

    invoke-virtual {v0}, LX/2D6;->c()V

    .line 383310
    invoke-static {p0}, LX/2Ct;->c(LX/2Ct;)V

    goto :goto_0

    .line 383311
    :cond_2
    iget-object v0, p0, LX/2Ct;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2Fn;->c:LX/0Tn;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 383312
    cmp-long v2, v0, v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/2Ct;->n:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-gez v0, :cond_4

    const/4 v0, 0x1

    .line 383313
    :goto_1
    if-eqz v0, :cond_5

    sget-object v0, LX/2Ct;->c:LX/2Cu;

    :goto_2
    invoke-static {p0, v0}, LX/2Ct;->a(LX/2Ct;LX/2Cu;)V

    .line 383314
    iget-object v0, p0, LX/2Ct;->e:LX/0W3;

    sget-wide v2, LX/0X5;->ae:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 383315
    iget-object v0, p0, LX/2Ct;->h:Landroid/content/Context;

    iget-object v1, p0, LX/2Ct;->e:LX/0W3;

    sget-wide v2, LX/0X5;->ah:J

    invoke-interface {v1, v2, v3}, LX/0W4;->c(J)J

    move-result-wide v2

    const/4 v4, 0x1

    .line 383316
    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    .line 383317
    invoke-virtual {v1, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    .line 383318
    if-nez v1, :cond_6

    .line 383319
    :cond_3
    :goto_3
    iget-object v0, p0, LX/2Ct;->p:LX/2D6;

    invoke-virtual {v0}, LX/2D6;->a()V

    .line 383320
    iget-object v0, p0, LX/2Ct;->l:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.STREAM_PUBLISH_START"

    new-instance v2, LX/HlO;

    invoke-direct {v2, p0}, LX/HlO;-><init>(LX/2Ct;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    goto/16 :goto_0

    .line 383321
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 383322
    :cond_5
    invoke-static {p0}, LX/2Ct;->e(LX/2Ct;)LX/2Cu;

    move-result-object v0

    goto :goto_2

    .line 383323
    :cond_6
    invoke-static {v0, v2, v3}, LX/Hln;->c(Landroid/content/Context;J)V

    .line 383324
    goto :goto_3
.end method
