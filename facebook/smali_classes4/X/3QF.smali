.class public LX/3QF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Pw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:LX/3QF;


# instance fields
.field private final b:LX/2LL;

.field private final c:LX/0WJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 563423
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/notifications/constants/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->LOGGED_OUT_BADGE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/3QF;->a:LX/2QP;

    return-void
.end method

.method public constructor <init>(LX/2LL;LX/0WJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 563424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563425
    iput-object p1, p0, LX/3QF;->b:LX/2LL;

    .line 563426
    iput-object p2, p0, LX/3QF;->c:LX/0WJ;

    .line 563427
    return-void
.end method

.method public static a(LX/0QB;)LX/3QF;
    .locals 5

    .prologue
    .line 563428
    sget-object v0, LX/3QF;->d:LX/3QF;

    if-nez v0, :cond_1

    .line 563429
    const-class v1, LX/3QF;

    monitor-enter v1

    .line 563430
    :try_start_0
    sget-object v0, LX/3QF;->d:LX/3QF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 563431
    if-eqz v2, :cond_0

    .line 563432
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 563433
    new-instance p0, LX/3QF;

    invoke-static {v0}, LX/2LL;->a(LX/0QB;)LX/2LL;

    move-result-object v3

    check-cast v3, LX/2LL;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    invoke-direct {p0, v3, v4}, LX/3QF;-><init>(LX/2LL;LX/0WJ;)V

    .line 563434
    move-object v0, p0

    .line 563435
    sput-object v0, LX/3QF;->d:LX/3QF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 563436
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 563437
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 563438
    :cond_1
    sget-object v0, LX/3QF;->d:LX/3QF;

    return-object v0

    .line 563439
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 563440
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 563441
    sget-object v0, LX/3QF;->a:LX/2QP;

    return-object v0
.end method

.method public final a(LX/0lF;Lcom/facebook/push/PushProperty;)V
    .locals 2

    .prologue
    .line 563442
    iget-object v0, p0, LX/3QF;->c:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 563443
    :cond_0
    :goto_0
    return-void

    .line 563444
    :cond_1
    const-string v0, "unread_count"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 563445
    if-eqz v0, :cond_0

    .line 563446
    iget-object v1, p0, LX/3QF;->b:LX/2LL;

    invoke-virtual {v0}, LX/0lF;->C()I

    move-result v0

    invoke-virtual {v1, v0}, LX/2LL;->a(I)V

    goto :goto_0
.end method
