.class public final LX/2Ey;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/reactivesocket/GatewayCallback;


# instance fields
.field public final synthetic a:Lcom/facebook/reactivesocket/GatewayCallback;

.field public final synthetic b:LX/1N0;


# direct methods
.method public constructor <init>(LX/1N0;Lcom/facebook/reactivesocket/GatewayCallback;)V
    .locals 0

    .prologue
    .line 386174
    iput-object p1, p0, LX/2Ey;->b:LX/1N0;

    iput-object p2, p0, LX/2Ey;->a:Lcom/facebook/reactivesocket/GatewayCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 386175
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 386176
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 386177
    const/4 v3, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v3, :pswitch_data_0

    move v0, v2

    .line 386178
    :pswitch_0
    move v0, v0

    .line 386179
    if-eqz v0, :cond_1

    .line 386180
    new-instance v0, LX/4l8;

    invoke-direct {v0, v1, p1}, LX/4l8;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object p1, v0

    .line 386181
    :goto_1
    iget-object v0, p0, LX/2Ey;->a:Lcom/facebook/reactivesocket/GatewayCallback;

    invoke-interface {v0, p1}, Lcom/facebook/reactivesocket/GatewayCallback;->onFailure(Ljava/lang/Throwable;)V

    .line 386182
    return-void

    .line 386183
    :cond_1
    iget-object v0, p0, LX/2Ey;->b:LX/1N0;

    iget-object v0, v0, LX/1N0;->b:LX/03V;

    const-string v1, "LithiumClient"

    const-string v2, "onFailure in subscriber"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 386184
    :sswitch_0
    const-string v4, "connection closed"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v2

    goto :goto_0

    :sswitch_1
    const-string v4, "connection error"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x610c0b12 -> :sswitch_0
        -0x240ab9ba -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final onSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 386185
    iget-object v0, p0, LX/2Ey;->a:Lcom/facebook/reactivesocket/GatewayCallback;

    invoke-interface {v0, p1, p2}, Lcom/facebook/reactivesocket/GatewayCallback;->onSuccess(Ljava/lang/String;Ljava/lang/String;)V

    .line 386186
    return-void
.end method
