.class public final LX/2nS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field public final synthetic a:LX/2nQ;


# direct methods
.method public constructor <init>(LX/2nQ;)V
    .locals 0

    .prologue
    .line 464593
    iput-object p1, p0, LX/2nS;->a:LX/2nQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 464599
    iget-object v0, p0, LX/2nS;->a:LX/2nQ;

    iget-object v0, v0, LX/2nQ;->c:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 464600
    iget-object v0, p0, LX/2nS;->a:LX/2nQ;

    iget-object v0, v0, LX/2nQ;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 464601
    :cond_0
    :goto_0
    return-void

    .line 464602
    :cond_1
    iget-object v0, p0, LX/2nS;->a:LX/2nQ;

    iget-object v0, v0, LX/2nQ;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 464603
    iget-object v0, p0, LX/2nS;->a:LX/2nQ;

    iget-object v0, v0, LX/2nQ;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    goto :goto_0
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 464604
    iget-object v0, p0, LX/2nS;->a:LX/2nQ;

    iget-object v0, v0, LX/2nQ;->c:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 464605
    iget-object v0, p0, LX/2nS;->a:LX/2nQ;

    iget-object v0, v0, LX/2nQ;->c:Landroid/view/View;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/View;->scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V

    .line 464606
    :cond_0
    :goto_0
    return-void

    .line 464607
    :cond_1
    iget-object v0, p0, LX/2nS;->a:LX/2nQ;

    iget-object v0, v0, LX/2nQ;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 464608
    iget-object v0, p0, LX/2nS;->a:LX/2nQ;

    iget-object v0, v0, LX/2nQ;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->scheduleSelf(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 464594
    iget-object v0, p0, LX/2nS;->a:LX/2nQ;

    iget-object v0, v0, LX/2nQ;->c:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 464595
    invoke-virtual {p0, p1, p2}, LX/2nS;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V

    .line 464596
    :cond_0
    :goto_0
    return-void

    .line 464597
    :cond_1
    iget-object v0, p0, LX/2nS;->a:LX/2nQ;

    iget-object v0, v0, LX/2nQ;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 464598
    iget-object v0, p0, LX/2nS;->a:LX/2nQ;

    iget-object v0, v0, LX/2nQ;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
