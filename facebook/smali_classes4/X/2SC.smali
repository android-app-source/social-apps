.class public LX/2SC;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final h:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/content/Context;

.field public final c:LX/1oy;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2MA;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 411901
    const-class v0, LX/2SC;

    sput-object v0, LX/2SC;->h:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/1oy;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/1oy;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "LX/0Ot",
            "<",
            "LX/2MA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 411902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 411903
    iput-object p1, p0, LX/2SC;->b:Landroid/content/Context;

    .line 411904
    iput-object p2, p0, LX/2SC;->a:LX/0Ot;

    .line 411905
    iput-object p3, p0, LX/2SC;->c:LX/1oy;

    .line 411906
    iput-object p4, p0, LX/2SC;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 411907
    iput-object p5, p0, LX/2SC;->e:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 411908
    iput-object p6, p0, LX/2SC;->f:LX/0Ot;

    .line 411909
    iput-object p7, p0, LX/2SC;->g:LX/0Ot;

    .line 411910
    return-void
.end method

.method public static b(LX/0QB;)LX/2SC;
    .locals 8

    .prologue
    .line 411911
    new-instance v0, LX/2SC;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0x140d

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/1ow;->c(LX/0QB;)LX/1ow;

    move-result-object v3

    check-cast v3, LX/1oy;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v5

    check-cast v5, Lcom/facebook/http/common/FbHttpRequestProcessor;

    const/16 v6, 0xcff

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2ca

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/2SC;-><init>(Landroid/content/Context;LX/0Ot;LX/1oy;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0Ot;LX/0Ot;)V

    .line 411912
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 411913
    if-nez p1, :cond_1

    .line 411914
    :cond_0
    :goto_0
    return-object v0

    .line 411915
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fb_voicemail_asset_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 411916
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, LX/2SC;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 411917
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 411918
    goto :goto_0
.end method
