.class public final LX/39s;
.super LX/1n6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1n6",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/39q;

.field private d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 523990
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "downStateType"

    aput-object v2, v0, v1

    sput-object v0, LX/39s;->b:[Ljava/lang/String;

    .line 523991
    sput v3, LX/39s;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 523992
    invoke-direct {p0}, LX/1n6;-><init>()V

    .line 523993
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/39s;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/39s;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/39s;LX/1De;LX/39q;)V
    .locals 1

    .prologue
    .line 523994
    invoke-super {p0, p1}, LX/1n6;->a(LX/1De;)V

    .line 523995
    iput-object p2, p0, LX/39s;->a:LX/39q;

    .line 523996
    iget-object v0, p0, LX/39s;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 523997
    return-void
.end method


# virtual methods
.method public final a(LX/1Wk;)LX/39s;
    .locals 2

    .prologue
    .line 523998
    iget-object v0, p0, LX/39s;->a:LX/39q;

    iput-object p1, v0, LX/39q;->a:LX/1Wk;

    .line 523999
    iget-object v0, p0, LX/39s;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 524000
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 524001
    invoke-super {p0}, LX/1n6;->a()V

    .line 524002
    const/4 v0, 0x0

    iput-object v0, p0, LX/39s;->a:LX/39q;

    .line 524003
    sget-object v0, LX/39p;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 524004
    return-void
.end method

.method public final b()LX/1dc;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 524005
    iget-object v1, p0, LX/39s;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/39s;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/39s;->c:I

    if-ge v1, v2, :cond_2

    .line 524006
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 524007
    :goto_0
    sget v2, LX/39s;->c:I

    if-ge v0, v2, :cond_1

    .line 524008
    iget-object v2, p0, LX/39s;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 524009
    sget-object v2, LX/39s;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 524010
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 524011
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 524012
    :cond_2
    iget-object v0, p0, LX/39s;->a:LX/39q;

    .line 524013
    invoke-virtual {p0}, LX/39s;->a()V

    .line 524014
    return-object v0
.end method
