.class public final LX/2IE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0UE;

.field private b:I

.field private c:Z


# direct methods
.method public constructor <init>(LX/0UE;)V
    .locals 1

    .prologue
    .line 391528
    iput-object p1, p0, LX/2IE;->a:LX/0UE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391529
    const/4 v0, -0x1

    iput v0, p0, LX/2IE;->b:I

    .line 391530
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 391531
    iget v0, p0, LX/2IE;->b:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/2IE;->a:LX/0UE;

    invoke-virtual {v1}, LX/0UE;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 391532
    invoke-virtual {p0}, LX/2IE;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 391533
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 391534
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2IE;->c:Z

    .line 391535
    iget v0, p0, LX/2IE;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/2IE;->b:I

    .line 391536
    iget-object v0, p0, LX/2IE;->a:LX/0UE;

    iget v1, p0, LX/2IE;->b:I

    invoke-virtual {v0, v1}, LX/0UE;->b(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 391537
    iget-boolean v0, p0, LX/2IE;->c:Z

    if-eqz v0, :cond_0

    .line 391538
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 391539
    :cond_0
    iget-object v0, p0, LX/2IE;->a:LX/0UE;

    iget v1, p0, LX/2IE;->b:I

    invoke-virtual {v0, v1}, LX/0UE;->a(I)Ljava/lang/Object;

    .line 391540
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2IE;->c:Z

    .line 391541
    iget v0, p0, LX/2IE;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/2IE;->b:I

    .line 391542
    return-void
.end method
