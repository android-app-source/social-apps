.class public LX/26J;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/26J;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nB;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 371975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 371976
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 371977
    iput-object v0, p0, LX/26J;->a:LX/0Ot;

    .line 371978
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 371979
    iput-object v0, p0, LX/26J;->b:LX/0Ot;

    .line 371980
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 371981
    iput-object v0, p0, LX/26J;->c:LX/0Ot;

    .line 371982
    return-void
.end method

.method public static a(II)F
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 371983
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    .line 371984
    int-to-float v0, p0

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v0, v2

    int-to-float v2, p1

    div-float/2addr v0, v2

    .line 371985
    :goto_0
    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 371986
    const v1, 0x3ecccccd    # 0.4f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 371987
    if-eqz v1, :cond_1

    .line 371988
    :cond_0
    const v0, 0x3faaaaab

    .line 371989
    :cond_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/26J;
    .locals 6

    .prologue
    .line 371990
    sget-object v0, LX/26J;->d:LX/26J;

    if-nez v0, :cond_1

    .line 371991
    const-class v1, LX/26J;

    monitor-enter v1

    .line 371992
    :try_start_0
    sget-object v0, LX/26J;->d:LX/26J;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 371993
    if-eqz v2, :cond_0

    .line 371994
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 371995
    new-instance v3, LX/26J;

    invoke-direct {v3}, LX/26J;-><init>()V

    .line 371996
    const/16 v4, 0xbc6

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x4e0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x455

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 371997
    iput-object v4, v3, LX/26J;->a:LX/0Ot;

    iput-object v5, v3, LX/26J;->b:LX/0Ot;

    iput-object p0, v3, LX/26J;->c:LX/0Ot;

    .line 371998
    move-object v0, v3

    .line 371999
    sput-object v0, LX/26J;->d:LX/26J;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 372000
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 372001
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 372002
    :cond_1
    sget-object v0, LX/26J;->d:LX/26J;

    return-object v0

    .line 372003
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 372004
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1EO;)LX/74S;
    .locals 2

    .prologue
    .line 372005
    if-nez p0, :cond_0

    .line 372006
    sget-object v0, LX/74S;->UNKNOWN:LX/74S;

    .line 372007
    :goto_0
    return-object v0

    .line 372008
    :cond_0
    sget-object v0, LX/8wo;->a:[I

    invoke-virtual {p0}, LX/1EO;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 372009
    sget-object v0, LX/74S;->OTHER:LX/74S;

    goto :goto_0

    .line 372010
    :pswitch_0
    sget-object v0, LX/74S;->NEWSFEED:LX/74S;

    goto :goto_0

    .line 372011
    :pswitch_1
    sget-object v0, LX/74S;->TIMELINE_WALL:LX/74S;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final b(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 9

    .prologue
    .line 372012
    iget-object v0, p0, LX/26J;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372013
    :goto_0
    return-void

    .line 372014
    :cond_0
    iget-object v0, p0, LX/26J;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nB;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->t()Ljava/lang/String;

    move-result-object v1

    sget-object v4, LX/74S;->PHOTO_COMMENT:LX/74S;

    .line 372015
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    new-instance v6, Landroid/content/ComponentName;

    iget-object v7, v0, LX/1nB;->a:Landroid/content/Context;

    const-string v8, "com.facebook.photos.mediagallery.ui.MediaGalleryActivity"

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v5

    .line 372016
    const-string v6, "photo_fbid"

    invoke-virtual {v5, v6, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 372017
    const-string v6, "photoset_token"

    invoke-virtual {v5, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372018
    const-string v6, "comment_photo"

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 372019
    const-string v6, "fullscreen_gallery_source"

    invoke-virtual {v4}, LX/74S;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372020
    move-object v1, v5

    .line 372021
    iget-object v0, p0, LX/26J;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
