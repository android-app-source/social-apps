.class public LX/295;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public b:LX/03R;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 375642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 375643
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/295;->b:LX/03R;

    .line 375644
    iput-object p1, p0, LX/295;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 375645
    return-void
.end method

.method public static a(LX/0QB;)LX/295;
    .locals 2

    .prologue
    .line 375633
    new-instance v1, LX/295;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v0}, LX/295;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 375634
    move-object v0, v1

    .line 375635
    return-object v0
.end method

.method public static a(LX/295;LX/03R;Z)V
    .locals 1

    .prologue
    .line 375636
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/295;->b:LX/03R;

    .line 375637
    sget-object v0, LX/03R;->YES:LX/03R;

    if-eq p1, v0, :cond_0

    .line 375638
    :goto_0
    return-void

    .line 375639
    :cond_0
    if-eqz p2, :cond_1

    .line 375640
    sget-object v0, LX/03R;->YES:LX/03R;

    iput-object v0, p0, LX/295;->b:LX/03R;

    goto :goto_0

    .line 375641
    :cond_1
    sget-object v0, LX/03R;->NO:LX/03R;

    iput-object v0, p0, LX/295;->b:LX/03R;

    goto :goto_0
.end method
