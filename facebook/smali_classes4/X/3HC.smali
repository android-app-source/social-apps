.class public final LX/3HC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0sa;

.field public final b:LX/0se;

.field public final c:LX/0ad;

.field public final d:LX/0tG;

.field public e:LX/0sU;

.field public f:LX/0sX;

.field public final g:LX/0tE;


# direct methods
.method public constructor <init>(LX/0sa;LX/0se;LX/0ad;LX/0tG;LX/0sU;LX/0sX;LX/0tE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 543084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 543085
    iput-object p1, p0, LX/3HC;->a:LX/0sa;

    .line 543086
    iput-object p2, p0, LX/3HC;->b:LX/0se;

    .line 543087
    iput-object p3, p0, LX/3HC;->c:LX/0ad;

    .line 543088
    iput-object p4, p0, LX/3HC;->d:LX/0tG;

    .line 543089
    iput-object p5, p0, LX/3HC;->e:LX/0sU;

    .line 543090
    iput-object p6, p0, LX/3HC;->f:LX/0sX;

    .line 543091
    iput-object p7, p0, LX/3HC;->g:LX/0tE;

    .line 543092
    return-void
.end method

.method public static a(LX/0QB;)LX/3HC;
    .locals 9

    .prologue
    .line 543093
    new-instance v1, LX/3HC;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v2

    check-cast v2, LX/0sa;

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v3

    check-cast v3, LX/0se;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v5

    check-cast v5, LX/0tG;

    invoke-static {p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v6

    check-cast v6, LX/0sU;

    invoke-static {p0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v7

    check-cast v7, LX/0sX;

    invoke-static {p0}, LX/0tE;->b(LX/0QB;)LX/0tE;

    move-result-object v8

    check-cast v8, LX/0tE;

    invoke-direct/range {v1 .. v8}, LX/3HC;-><init>(LX/0sa;LX/0se;LX/0ad;LX/0tG;LX/0sU;LX/0sX;LX/0tE;)V

    .line 543094
    move-object v0, v1

    .line 543095
    return-object v0
.end method
