.class public final LX/2P1;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field public static final p:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 404917
    new-instance v0, LX/0U1;

    const-string v1, "msg_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->a:LX/0U1;

    .line 404918
    new-instance v0, LX/0U1;

    const-string v1, "thread_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->b:LX/0U1;

    .line 404919
    new-instance v0, LX/0U1;

    const-string v1, "encrypted_content"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->c:LX/0U1;

    .line 404920
    new-instance v0, LX/0U1;

    const-string v1, "facebook_hmac"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->d:LX/0U1;

    .line 404921
    new-instance v0, LX/0U1;

    const-string v1, "sender_fbid"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->e:LX/0U1;

    .line 404922
    new-instance v0, LX/0U1;

    const-string v1, "timestamp_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->f:LX/0U1;

    .line 404923
    new-instance v0, LX/0U1;

    const-string v1, "timestamp_sent_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->g:LX/0U1;

    .line 404924
    new-instance v0, LX/0U1;

    const-string v1, "attachments_encrypted"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->h:LX/0U1;

    .line 404925
    new-instance v0, LX/0U1;

    const-string v1, "msg_type"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->i:LX/0U1;

    .line 404926
    new-instance v0, LX/0U1;

    const-string v1, "offline_threading_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->j:LX/0U1;

    .line 404927
    new-instance v0, LX/0U1;

    const-string v1, "pending_send_media_attachment"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->k:LX/0U1;

    .line 404928
    new-instance v0, LX/0U1;

    const-string v1, "client_expiration_time_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->l:LX/0U1;

    .line 404929
    new-instance v0, LX/0U1;

    const-string v1, "send_error"

    const-string v2, "STRING"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->m:LX/0U1;

    .line 404930
    new-instance v0, LX/0U1;

    const-string v1, "send_error_message"

    const-string v2, "STRING"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->n:LX/0U1;

    .line 404931
    new-instance v0, LX/0U1;

    const-string v1, "send_error_timestamp_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->o:LX/0U1;

    .line 404932
    new-instance v0, LX/0U1;

    const-string v1, "expired"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2P1;->p:LX/0U1;

    return-void
.end method
