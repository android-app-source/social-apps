.class public LX/2UW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2UW;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416127
    iput-object p1, p0, LX/2UW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 416128
    return-void
.end method

.method public static a(LX/0QB;)LX/2UW;
    .locals 4

    .prologue
    .line 416129
    sget-object v0, LX/2UW;->b:LX/2UW;

    if-nez v0, :cond_1

    .line 416130
    const-class v1, LX/2UW;

    monitor-enter v1

    .line 416131
    :try_start_0
    sget-object v0, LX/2UW;->b:LX/2UW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 416132
    if-eqz v2, :cond_0

    .line 416133
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 416134
    new-instance p0, LX/2UW;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/2UW;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 416135
    move-object v0, p0

    .line 416136
    sput-object v0, LX/2UW;->b:LX/2UW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416137
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 416138
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 416139
    :cond_1
    sget-object v0, LX/2UW;->b:LX/2UW;

    return-object v0

    .line 416140
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 416141
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 416142
    iget-object v0, p0, LX/2UW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2Vv;->f:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 416143
    return-void
.end method
