.class public final LX/27e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 0

    .prologue
    .line 372997
    iput-object p1, p0, LX/27e;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 372998
    iget-object v0, p0, LX/27e;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->F(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    .line 372999
    iget-object v0, p0, LX/27e;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-boolean v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aZ:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 373000
    iget-object v0, p0, LX/27e;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    const/4 v1, 0x1

    .line 373001
    iput-boolean v1, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aZ:Z

    .line 373002
    iget-object v0, p0, LX/27e;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/27e;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->au:LX/0Uh;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 373003
    iget-object v0, p0, LX/27e;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ag(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    .line 373004
    iget-object v0, p0, LX/27e;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v1, p0, LX/27e;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v1, v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    invoke-static {v0, v1}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 373005
    :cond_0
    return-void
.end method
