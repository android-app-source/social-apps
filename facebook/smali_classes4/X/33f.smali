.class public final LX/33f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:Lcom/facebook/zero/service/FbZeroRequestHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/service/FbZeroRequestHandler;LX/0TF;)V
    .locals 0

    .prologue
    .line 493933
    iput-object p1, p0, LX/33f;->b:Lcom/facebook/zero/service/FbZeroRequestHandler;

    iput-object p2, p0, LX/33f;->a:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 493931
    iget-object v0, p0, LX/33f;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 493932
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 493927
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 493928
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    .line 493929
    iget-object v1, p0, LX/33f;->a:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 493930
    return-void
.end method
