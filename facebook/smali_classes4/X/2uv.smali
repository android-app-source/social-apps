.class public final LX/2uv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2uu;


# instance fields
.field public final synthetic a:LX/2uu;

.field public final synthetic b:LX/1HZ;


# direct methods
.method public constructor <init>(LX/1HZ;LX/2uu;)V
    .locals 0

    .prologue
    .line 477075
    iput-object p1, p0, LX/2uv;->b:LX/1HZ;

    iput-object p2, p0, LX/2uv;->a:LX/2uu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/OutputStream;)V
    .locals 4

    .prologue
    const/16 v2, 0x2000

    .line 477076
    iget-object v0, p0, LX/2uv;->b:LX/1HZ;

    iget-object v0, v0, LX/1HZ;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ux;

    .line 477077
    sget-object v1, LX/2ux;->NONE:LX/2ux;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/2ux;->ENCRYPTED:LX/2ux;

    if-ne v0, v1, :cond_1

    iget-object v1, p0, LX/2uv;->b:LX/1HZ;

    iget-object v1, v1, LX/1HZ;->f:LX/1IW;

    invoke-virtual {v1}, LX/1IW;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 477078
    :cond_0
    iget-object v0, p0, LX/2uv;->a:LX/2uu;

    invoke-interface {v0, p1}, LX/2uu;->a(Ljava/io/OutputStream;)V

    .line 477079
    :goto_0
    return-void

    .line 477080
    :cond_1
    sget-object v1, LX/2ux;->FIXED_KEY:LX/2ux;

    if-ne v0, v1, :cond_2

    .line 477081
    sget-object v0, LX/1HZ;->m:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 477082
    iget-object v0, p0, LX/2uv;->b:LX/1HZ;

    iget-object v0, v0, LX/1HZ;->g:LX/1FQ;

    invoke-interface {v0, v2}, LX/1FS;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 477083
    :try_start_0
    iget-object v1, p0, LX/2uv;->b:LX/1HZ;

    iget-object v1, v1, LX/1HZ;->e:LX/1Hz;

    invoke-static {p1}, LX/45j;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v2

    sget-object v3, LX/1HZ;->a:LX/1Hb;

    invoke-virtual {v1, v2, v3, v0}, LX/1Hz;->a(Ljava/io/OutputStream;LX/1Hb;[B)Ljava/io/OutputStream;
    :try_end_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 477084
    :try_start_1
    iget-object v1, p0, LX/2uv;->a:LX/2uu;

    invoke-interface {v1, v2}, LX/2uu;->a(Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 477085
    :try_start_2
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch LX/48f; {:try_start_2 .. :try_end_2} :catch_0
    .catch LX/48g; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 477086
    iget-object v1, p0, LX/2uv;->b:LX/1HZ;

    iget-object v1, v1, LX/1HZ;->g:LX/1FQ;

    invoke-interface {v1, v0}, LX/1FS;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 477087
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    throw v1
    :try_end_3
    .catch LX/48f; {:try_start_3 .. :try_end_3} :catch_0
    .catch LX/48g; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 477088
    :catch_0
    move-exception v1

    .line 477089
    :goto_1
    :try_start_4
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Error encrypting"

    invoke-direct {v2, v3, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 477090
    :catchall_1
    move-exception v1

    iget-object v2, p0, LX/2uv;->b:LX/1HZ;

    iget-object v2, v2, LX/1HZ;->g:LX/1FQ;

    invoke-interface {v2, v0}, LX/1FS;->a(Ljava/lang/Object;)V

    throw v1

    .line 477091
    :cond_2
    sget-object v1, LX/2ux;->FIXED_KEY_128:LX/2ux;

    if-ne v0, v1, :cond_3

    .line 477092
    sget-object v0, LX/1HZ;->k:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 477093
    iget-object v0, p0, LX/2uv;->b:LX/1HZ;

    iget-object v0, v0, LX/1HZ;->g:LX/1FQ;

    invoke-interface {v0, v2}, LX/1FS;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 477094
    :try_start_5
    iget-object v1, p0, LX/2uv;->b:LX/1HZ;

    iget-object v1, v1, LX/1HZ;->d:LX/1Hz;

    invoke-static {p1}, LX/45j;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v2

    sget-object v3, LX/1HZ;->a:LX/1Hb;

    invoke-virtual {v1, v2, v3, v0}, LX/1Hz;->a(Ljava/io/OutputStream;LX/1Hb;[B)Ljava/io/OutputStream;
    :try_end_5
    .catch LX/48f; {:try_start_5 .. :try_end_5} :catch_1
    .catch LX/48g; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result-object v2

    .line 477095
    :try_start_6
    iget-object v1, p0, LX/2uv;->a:LX/2uu;

    invoke-interface {v1, v2}, LX/2uu;->a(Ljava/io/OutputStream;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 477096
    :try_start_7
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch LX/48f; {:try_start_7 .. :try_end_7} :catch_1
    .catch LX/48g; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 477097
    iget-object v1, p0, LX/2uv;->b:LX/1HZ;

    iget-object v1, v1, LX/1HZ;->g:LX/1FQ;

    invoke-interface {v1, v0}, LX/1FS;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 477098
    :catchall_2
    move-exception v1

    :try_start_8
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    throw v1
    :try_end_8
    .catch LX/48f; {:try_start_8 .. :try_end_8} :catch_1
    .catch LX/48g; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 477099
    :catch_1
    move-exception v1

    .line 477100
    :goto_2
    :try_start_9
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Error encrypting"

    invoke-direct {v2, v3, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 477101
    :catchall_3
    move-exception v1

    iget-object v2, p0, LX/2uv;->b:LX/1HZ;

    iget-object v2, v2, LX/1HZ;->g:LX/1FQ;

    invoke-interface {v2, v0}, LX/1FS;->a(Ljava/lang/Object;)V

    throw v1

    .line 477102
    :cond_3
    sget-object v0, LX/1HZ;->l:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 477103
    iget-object v0, p0, LX/2uv;->b:LX/1HZ;

    iget-object v0, v0, LX/1HZ;->g:LX/1FQ;

    invoke-interface {v0, v2}, LX/1FS;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 477104
    :try_start_a
    iget-object v1, p0, LX/2uv;->b:LX/1HZ;

    iget-object v1, v1, LX/1HZ;->f:LX/1IW;

    invoke-static {p1}, LX/45j;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v2

    sget-object v3, LX/1HZ;->a:LX/1Hb;

    invoke-virtual {v1, v2, v3, v0}, LX/1IW;->a(Ljava/io/OutputStream;LX/1Hb;[B)Ljava/io/OutputStream;
    :try_end_a
    .catch LX/48f; {:try_start_a .. :try_end_a} :catch_2
    .catch LX/48g; {:try_start_a .. :try_end_a} :catch_3
    .catch LX/48k; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    move-result-object v2

    .line 477105
    :try_start_b
    iget-object v1, p0, LX/2uv;->a:LX/2uu;

    invoke-interface {v1, v2}, LX/2uu;->a(Ljava/io/OutputStream;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    .line 477106
    :try_start_c
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_c
    .catch LX/48f; {:try_start_c .. :try_end_c} :catch_2
    .catch LX/48g; {:try_start_c .. :try_end_c} :catch_3
    .catch LX/48k; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    .line 477107
    iget-object v1, p0, LX/2uv;->b:LX/1HZ;

    iget-object v1, v1, LX/1HZ;->g:LX/1FQ;

    invoke-interface {v1, v0}, LX/1FS;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 477108
    :catchall_4
    move-exception v1

    :try_start_d
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    throw v1
    :try_end_d
    .catch LX/48f; {:try_start_d .. :try_end_d} :catch_2
    .catch LX/48g; {:try_start_d .. :try_end_d} :catch_3
    .catch LX/48k; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    .line 477109
    :catch_2
    move-exception v1

    .line 477110
    :goto_3
    :try_start_e
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Error encrypting"

    invoke-direct {v2, v3, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    .line 477111
    :catchall_5
    move-exception v1

    iget-object v2, p0, LX/2uv;->b:LX/1HZ;

    iget-object v2, v2, LX/1HZ;->g:LX/1FQ;

    invoke-interface {v2, v0}, LX/1FS;->a(Ljava/lang/Object;)V

    throw v1

    .line 477112
    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_3

    .line 477113
    :catch_5
    move-exception v1

    goto :goto_2

    .line 477114
    :catch_6
    move-exception v1

    goto/16 :goto_1
.end method
