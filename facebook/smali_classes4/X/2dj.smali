.class public LX/2dj;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final e:Ljava/lang/String;

.field private static final f:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final g:Landroid/content/Context;

.field public final h:Ljava/util/concurrent/ExecutorService;

.field private final i:LX/03V;

.field public final j:LX/0bH;

.field public final k:LX/2dk;

.field public final l:LX/2dm;

.field public final m:LX/0if;

.field private final n:LX/15W;

.field public final o:LX/0ie;

.field private final p:LX/0ad;

.field public final q:LX/0aG;

.field public final r:LX/0SI;

.field private final s:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 443941
    const-class v0, LX/2dj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2dj;->e:Ljava/lang/String;

    .line 443942
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/2dj;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0bH;LX/2dk;LX/2dm;LX/0if;LX/15W;LX/0ie;LX/0ad;LX/0aG;LX/0SI;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 443943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 443944
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    iput-object v0, p0, LX/2dj;->s:LX/0tf;

    .line 443945
    iput-object p1, p0, LX/2dj;->g:Landroid/content/Context;

    .line 443946
    iput-object p2, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    .line 443947
    iput-object p3, p0, LX/2dj;->i:LX/03V;

    .line 443948
    iput-object p4, p0, LX/2dj;->j:LX/0bH;

    .line 443949
    iput-object p5, p0, LX/2dj;->k:LX/2dk;

    .line 443950
    iput-object p6, p0, LX/2dj;->l:LX/2dm;

    .line 443951
    iput-object p7, p0, LX/2dj;->m:LX/0if;

    .line 443952
    iput-object p8, p0, LX/2dj;->n:LX/15W;

    .line 443953
    iput-object p9, p0, LX/2dj;->o:LX/0ie;

    .line 443954
    iput-object p10, p0, LX/2dj;->p:LX/0ad;

    .line 443955
    iput-object p11, p0, LX/2dj;->q:LX/0aG;

    .line 443956
    iput-object p12, p0, LX/2dj;->r:LX/0SI;

    .line 443957
    invoke-virtual {p0}, LX/2dj;->j()V

    .line 443958
    return-void
.end method

.method public static a(LX/0QB;)LX/2dj;
    .locals 1

    .prologue
    .line 443959
    invoke-static {p0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v0

    return-object v0
.end method

.method private a(JLX/2h8;LX/2hC;LX/5P2;LX/4F1;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .param p4    # LX/2hC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/2h8;",
            "LX/2hC;",
            "LX/5P2;",
            "LX/4F1;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 443960
    sget-object v0, LX/2dj;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/16 v1, 0x7fff

    if-le v0, v1, :cond_0

    .line 443961
    sget-object v0, LX/2dj;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v7}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 443962
    :cond_0
    sget-object v0, LX/2dj;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    int-to-short v0, v0

    .line 443963
    iget-object v1, p0, LX/2dj;->m:LX/0if;

    sget-object v2, LX/0ig;->u:LX/0ih;

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, LX/0if;->a(LX/0ih;J)V

    .line 443964
    iget-object v1, p0, LX/2dj;->o:LX/0ie;

    const-string v2, "friend_request"

    invoke-virtual {v1, v2}, LX/0ie;->c(Ljava/lang/String;)V

    .line 443965
    if-eqz p5, :cond_1

    .line 443966
    iget-object v1, p5, LX/5P2;->value:Ljava/lang/String;

    .line 443967
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 443968
    const-string v2, "refs"

    invoke-virtual {p6, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 443969
    :cond_1
    if-eqz p4, :cond_2

    .line 443970
    iget-object v1, p4, LX/2hC;->value:Ljava/lang/String;

    .line 443971
    const-string v2, "people_you_may_know_location"

    invoke-virtual {p6, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443972
    :cond_2
    new-instance v1, LX/868;

    invoke-direct {v1}, LX/868;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 443973
    iput-object v2, v1, LX/868;->b:Ljava/lang/String;

    .line 443974
    move-object v1, v1

    .line 443975
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 443976
    iput-object v2, v1, LX/868;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 443977
    move-object v1, v1

    .line 443978
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 443979
    iput-object v2, v1, LX/868;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 443980
    move-object v1, v1

    .line 443981
    invoke-virtual {v1}, LX/868;->a()Lcom/facebook/friends/protocol/FriendRequestsConsistencyGraphQLModels$FriendRequestsRepresentedProfileFieldsModel;

    move-result-object v3

    .line 443982
    new-instance v1, LX/85d;

    invoke-direct {v1}, LX/85d;-><init>()V

    move-object v2, v1

    .line 443983
    const-string v1, "input"

    invoke-virtual {v2, v1, p6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 443984
    sget-object v6, LX/5P3;->a:LX/0Px;

    move-object v1, p0

    move-wide v4, p1

    invoke-static/range {v1 .. v6}, LX/2dj;->a(LX/2dj;LX/0zP;LX/0jT;JLX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/83l;

    invoke-direct {v2, p0}, LX/83l;-><init>(LX/2dj;)V

    iget-object v3, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 443985
    new-instance v2, LX/83m;

    invoke-direct {v2, p0, v0, p3}, LX/83m;-><init>(LX/2dj;SLX/2h8;)V

    iget-object v0, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 443986
    iget-object v0, p0, LX/2dj;->p:LX/0ad;

    sget-short v2, LX/0fe;->bx:S

    invoke-interface {v0, v2, v7}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 443987
    new-instance v0, LX/83r;

    invoke-direct {v0, p0}, LX/83r;-><init>(LX/2dj;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 443988
    :cond_3
    iget-object v0, p0, LX/2dj;->n:LX/15W;

    iget-object v2, p0, LX/2dj;->g:Landroid/content/Context;

    new-instance v3, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v4, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FRIEND_REQUEST_SENT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v3, v4}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 443989
    const-class v4, LX/0i1;

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v4, v5}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 443990
    sget-object v0, LX/2h8;->PROFILE_BUTTON:LX/2h8;

    if-ne p3, v0, :cond_4

    .line 443991
    iget-object v0, p0, LX/2dj;->n:LX/15W;

    iget-object v2, p0, LX/2dj;->g:Landroid/content/Context;

    new-instance v3, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v4, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PROFILE_FRIEND_REQUEST_SENT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v3, v4}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 443992
    const-class v4, LX/0i1;

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v4, v5}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 443993
    :cond_4
    invoke-static {v1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 443994
    invoke-direct {p0, p1, p2, v0}, LX/2dj;->a(JLcom/google/common/util/concurrent/ListenableFuture;)V

    .line 443995
    return-object v0
.end method

.method private a(JLX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/2hA;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443996
    new-instance v0, LX/4Ex;

    invoke-direct {v0}, LX/4Ex;-><init>()V

    iget-object v1, p3, LX/2hA;->value:Ljava/lang/String;

    .line 443997
    const-string v2, "source"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443998
    move-object v0, v0

    .line 443999
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 444000
    const-string v2, "friend_requester_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 444001
    move-object v0, v0

    .line 444002
    new-instance v1, LX/868;

    invoke-direct {v1}, LX/868;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 444003
    iput-object v2, v1, LX/868;->b:Ljava/lang/String;

    .line 444004
    move-object v1, v1

    .line 444005
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 444006
    iput-object v2, v1, LX/868;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 444007
    move-object v1, v1

    .line 444008
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 444009
    iput-object v2, v1, LX/868;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 444010
    move-object v1, v1

    .line 444011
    invoke-virtual {v1}, LX/868;->a()Lcom/facebook/friends/protocol/FriendRequestsConsistencyGraphQLModels$FriendRequestsRepresentedProfileFieldsModel;

    move-result-object v3

    .line 444012
    new-instance v1, LX/85Z;

    invoke-direct {v1}, LX/85Z;-><init>()V

    move-object v2, v1

    .line 444013
    const-string v1, "input"

    invoke-virtual {v2, v1, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 444014
    iget-object v0, p0, LX/2dj;->o:LX/0ie;

    const-string v1, "friend_accept"

    invoke-virtual {v0, v1}, LX/0ie;->c(Ljava/lang/String;)V

    .line 444015
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 444016
    move-object v1, p0

    move-wide v4, p1

    invoke-static/range {v1 .. v6}, LX/2dj;->a(LX/2dj;LX/0zP;LX/0jT;JLX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/83t;

    invoke-direct {v1, p0}, LX/83t;-><init>(LX/2dj;)V

    iget-object v2, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2dj;ILjava/lang/Integer;LX/2hC;Lcom/facebook/common/callercontext/CallerContext;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p1    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Integer;",
            "LX/2hC;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0TF",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 444017
    iget-object v0, p0, LX/2dj;->k:LX/2dk;

    invoke-static {p0}, LX/2dj;->o(LX/2dj;)Ljava/lang/String;

    move-result-object v1

    new-instance v6, LX/83k;

    invoke-direct {v6, p0, p5}, LX/83k;-><init>(LX/2dj;LX/0TF;)V

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v6}, LX/2dk;->a(Ljava/lang/String;ILjava/lang/Integer;LX/2hC;Lcom/facebook/common/callercontext/CallerContext;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {p0, v0}, LX/2dj;->c(LX/2dj;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/2dj;JLX/2na;LX/2hA;ZJ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/2na;",
            "LX/2hA;",
            "ZJ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444018
    if-nez p4, :cond_0

    .line 444019
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FriendRequestResponseRef should not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 444020
    iget-object v1, p0, LX/2dj;->i:LX/03V;

    sget-object v2, LX/2dj;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 444021
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 444022
    :goto_0
    return-object v0

    .line 444023
    :cond_0
    sget-object v0, LX/83s;->a:[I

    invoke-virtual {p3}, LX/2na;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 444024
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected FriendRequestResponse: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 444025
    :pswitch_0
    if-eqz p5, :cond_1

    .line 444026
    new-instance v0, LX/4K1;

    invoke-direct {v0}, LX/4K1;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 444027
    const-string v2, "friend_requester_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 444028
    move-object v0, v0

    .line 444029
    invoke-static {p6, p7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 444030
    const-string v2, "memorialized_user_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 444031
    move-object v0, v0

    .line 444032
    new-instance v1, LX/86M;

    invoke-direct {v1}, LX/86M;-><init>()V

    move-object v1, v1

    .line 444033
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 444034
    iget-object v0, p0, LX/2dj;->o:LX/0ie;

    const-string v2, "friend_accept"

    invoke-virtual {v0, v2}, LX/0ie;->c(Ljava/lang/String;)V

    .line 444035
    iget-object v0, p0, LX/2dj;->l:LX/2dm;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2dm;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/83u;

    invoke-direct {v1, p0}, LX/83u;-><init>(LX/2dj;)V

    iget-object v2, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 444036
    :goto_1
    invoke-direct {p0, p1, p2, v0}, LX/2dj;->a(JLcom/google/common/util/concurrent/ListenableFuture;)V

    goto :goto_0

    .line 444037
    :cond_1
    invoke-direct {p0, p1, p2, p4}, LX/2dj;->a(JLX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    .line 444038
    :pswitch_1
    if-eqz p5, :cond_2

    .line 444039
    new-instance v0, LX/4Gy;

    invoke-direct {v0}, LX/4Gy;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 444040
    const-string v2, "friend_requester_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 444041
    move-object v0, v0

    .line 444042
    invoke-static {p6, p7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 444043
    const-string v2, "memorialized_user_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 444044
    move-object v0, v0

    .line 444045
    new-instance v1, LX/86L;

    invoke-direct {v1}, LX/86L;-><init>()V

    move-object v1, v1

    .line 444046
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 444047
    iget-object v0, p0, LX/2dj;->o:LX/0ie;

    const-string v2, "friend_reject"

    invoke-virtual {v0, v2}, LX/0ie;->c(Ljava/lang/String;)V

    .line 444048
    iget-object v0, p0, LX/2dj;->l:LX/2dm;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2dm;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/83w;

    invoke-direct {v1, p0}, LX/83w;-><init>(LX/2dj;)V

    iget-object v2, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 444049
    :goto_2
    invoke-direct {p0, p1, p2, v0}, LX/2dj;->a(JLcom/google/common/util/concurrent/ListenableFuture;)V

    goto/16 :goto_0

    .line 444050
    :cond_2
    invoke-direct {p0, p1, p2, p4}, LX/2dj;->b(JLX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(LX/2dj;LX/0zP;LX/0jT;JLX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zP",
            "<TT;>;",
            "LX/0jT;",
            "J",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 444051
    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p2, v0, p5}, LX/2dj;->a(LX/2dj;LX/0zP;LX/0jT;Ljava/lang/String;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/2dj;LX/0zP;LX/0jT;Ljava/lang/String;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zP",
            "<TT;>;",
            "LX/0jT;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 444052
    invoke-static {p1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 444053
    iget-object v1, p0, LX/2dj;->l:LX/2dm;

    .line 444054
    iget-object v2, v1, LX/2dm;->a:LX/2dn;

    .line 444055
    iget-boolean p0, v2, LX/2dn;->a:Z

    move v2, p0

    .line 444056
    if-nez v2, :cond_0

    .line 444057
    invoke-virtual {v1, v0, p4}, LX/2dm;->a(LX/399;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 444058
    :goto_0
    move-object v0, v2

    .line 444059
    return-object v0

    .line 444060
    :cond_0
    new-instance v2, LX/3G1;

    invoke-direct {v2}, LX/3G1;-><init>()V

    invoke-virtual {v2, v0}, LX/3G1;->a(LX/399;)LX/3G1;

    move-result-object v2

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p1, v0, LX/399;->a:LX/0zP;

    .line 444061
    iget-object p2, p1, LX/0gW;->f:Ljava/lang/String;

    move-object p1, p2

    .line 444062
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 444063
    iput-object p0, v2, LX/3G2;->a:Ljava/lang/String;

    .line 444064
    move-object v2, v2

    .line 444065
    iget-object p0, v0, LX/399;->a:LX/0zP;

    .line 444066
    instance-of p1, p0, LX/85d;

    if-nez p1, :cond_1

    instance-of p1, p0, LX/85a;

    if-eqz p1, :cond_3

    .line 444067
    :cond_1
    const-string p1, "FRIENDING_MUTATION_"

    .line 444068
    :goto_1
    move-object p1, p1

    .line 444069
    if-nez p1, :cond_2

    const/4 p1, 0x0

    :goto_2
    move-object p0, p1

    .line 444070
    iput-object p0, v2, LX/3G2;->b:Ljava/lang/String;

    .line 444071
    move-object v2, v2

    .line 444072
    invoke-virtual {v2}, LX/3G2;->a()LX/3G3;

    move-result-object v2

    check-cast v2, LX/3G4;

    .line 444073
    iget-object p0, v1, LX/2dm;->b:LX/2dl;

    sget-object p1, LX/3Fz;->c:LX/3Fz;

    .line 444074
    iget-object p2, p0, LX/2dl;->a:LX/0tX;

    invoke-virtual {p2, v2, p1}, LX/0tX;->a(LX/3G4;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 444075
    iget-object p2, p0, LX/2dl;->c:LX/0Or;

    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/2io;

    invoke-virtual {p2, p4}, LX/2io;->a(LX/0Px;)LX/0TF;

    move-result-object p2

    iget-object v0, p0, LX/2dl;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, p2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 444076
    move-object v2, v1

    .line 444077
    goto :goto_0

    :cond_2
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 444078
    :cond_3
    instance-of p1, p0, LX/85g;

    if-eqz p1, :cond_4

    .line 444079
    const-string p1, "FUTURE_FRIENDING_MUTATION_"

    goto :goto_1

    .line 444080
    :cond_4
    const/4 p1, 0x0

    goto :goto_1
.end method

.method public static a(LX/2dj;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2lp;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/FriendRequest;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 444081
    new-instance v0, LX/2ir;

    invoke-direct {v0, p0}, LX/2ir;-><init>(LX/2dj;)V

    iget-object v1, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private a(JLcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 444083
    iget-object v0, p0, LX/2dj;->s:LX/0tf;

    invoke-virtual {v0, p1, p2}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 444084
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 444085
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 444086
    :cond_0
    iget-object v0, p0, LX/2dj;->s:LX/0tf;

    invoke-virtual {v0, p1, p2, p3}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 444087
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Z
    .locals 1

    .prologue
    .line 443888
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->p_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2dj;
    .locals 13

    .prologue
    .line 444127
    new-instance v0, LX/2dj;

    const-class v1, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v4

    check-cast v4, LX/0bH;

    invoke-static {p0}, LX/2dk;->b(LX/0QB;)LX/2dk;

    move-result-object v5

    check-cast v5, LX/2dk;

    .line 444128
    new-instance v8, LX/2dm;

    invoke-static {p0}, LX/2dn;->a(LX/0QB;)LX/2dn;

    move-result-object v6

    check-cast v6, LX/2dn;

    invoke-static {p0}, LX/2dl;->a(LX/0QB;)LX/2dl;

    move-result-object v7

    check-cast v7, LX/2dl;

    invoke-direct {v8, v6, v7}, LX/2dm;-><init>(LX/2dn;LX/2dl;)V

    .line 444129
    move-object v6, v8

    .line 444130
    check-cast v6, LX/2dm;

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v7

    check-cast v7, LX/0if;

    invoke-static {p0}, LX/15W;->b(LX/0QB;)LX/15W;

    move-result-object v8

    check-cast v8, LX/15W;

    invoke-static {p0}, LX/0ie;->a(LX/0QB;)LX/0ie;

    move-result-object v9

    check-cast v9, LX/0ie;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v11

    check-cast v11, LX/0aG;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v12

    check-cast v12, LX/0SI;

    invoke-direct/range {v0 .. v12}, LX/2dj;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0bH;LX/2dk;LX/2dm;LX/0if;LX/15W;LX/0ie;LX/0ad;LX/0aG;LX/0SI;)V

    .line 444131
    return-object v0
.end method

.method private b(JLX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/2hA;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444109
    new-instance v0, LX/4F0;

    invoke-direct {v0}, LX/4F0;-><init>()V

    iget-object v1, p3, LX/2hA;->value:Ljava/lang/String;

    .line 444110
    const-string v2, "source"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 444111
    move-object v0, v0

    .line 444112
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 444113
    const-string v2, "friend_requester_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 444114
    move-object v0, v0

    .line 444115
    new-instance v1, LX/868;

    invoke-direct {v1}, LX/868;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 444116
    iput-object v2, v1, LX/868;->b:Ljava/lang/String;

    .line 444117
    move-object v1, v1

    .line 444118
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 444119
    iput-object v2, v1, LX/868;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 444120
    move-object v1, v1

    .line 444121
    invoke-virtual {v1}, LX/868;->a()Lcom/facebook/friends/protocol/FriendRequestsConsistencyGraphQLModels$FriendRequestsRepresentedProfileFieldsModel;

    move-result-object v3

    .line 444122
    new-instance v1, LX/85c;

    invoke-direct {v1}, LX/85c;-><init>()V

    move-object v2, v1

    .line 444123
    const-string v1, "input"

    invoke-virtual {v2, v1, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 444124
    iget-object v0, p0, LX/2dj;->o:LX/0ie;

    const-string v1, "friend_reject"

    invoke-virtual {v0, v1}, LX/0ie;->c(Ljava/lang/String;)V

    .line 444125
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 444126
    move-object v1, p0

    move-wide v4, p1

    invoke-static/range {v1 .. v6}, LX/2dj;->a(LX/2dj;LX/0zP;LX/0jT;JLX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/83v;

    invoke-direct {v1, p0}, LX/83v;-><init>(LX/2dj;)V

    iget-object v2, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/2dj;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/3Fw;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 444108
    new-instance v0, LX/3Bv;

    invoke-direct {v0, p0}, LX/3Bv;-><init>(LX/2dj;)V

    iget-object v1, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized l(LX/2dj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 444107
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2dj;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized m(LX/2dj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 444106
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2dj;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized n(LX/2dj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 444105
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2dj;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized o(LX/2dj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 444104
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2dj;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444102
    iget-object v0, p0, LX/2dj;->q:LX/0aG;

    const-string v1, "friending_mark_friend_requests_seen"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const v3, 0x623e5453

    invoke-static {v0, v1, v2, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 444103
    const/4 v1, 0x0

    invoke-static {v1}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v1

    iget-object v2, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/FriendRequest;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 444082
    iget-object v0, p0, LX/2dj;->k:LX/2dk;

    invoke-static {p0}, LX/2dj;->m(LX/2dj;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, LX/2dk;->a(Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {p0, v0}, LX/2dj;->a(LX/2dj;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444088
    new-instance v0, LX/4F2;

    invoke-direct {v0}, LX/4F2;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 444089
    const-string v2, "suggested_friend_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 444090
    move-object v0, v0

    .line 444091
    new-instance v1, LX/868;

    invoke-direct {v1}, LX/868;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 444092
    iput-object v2, v1, LX/868;->b:Ljava/lang/String;

    .line 444093
    move-object v1, v1

    .line 444094
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 444095
    iput-object v2, v1, LX/868;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 444096
    move-object v1, v1

    .line 444097
    invoke-virtual {v1}, LX/868;->a()Lcom/facebook/friends/protocol/FriendRequestsConsistencyGraphQLModels$FriendRequestsRepresentedProfileFieldsModel;

    move-result-object v3

    .line 444098
    new-instance v1, LX/85e;

    invoke-direct {v1}, LX/85e;-><init>()V

    move-object v2, v1

    .line 444099
    const-string v1, "input"

    invoke-virtual {v2, v1, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 444100
    sget-object v6, LX/5P3;->a:LX/0Px;

    move-object v1, p0

    move-wide v4, p1

    invoke-static/range {v1 .. v6}, LX/2dj;->a(LX/2dj;LX/0zP;LX/0jT;JLX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/83o;

    invoke-direct {v1, p0}, LX/83o;-><init>(LX/2dj;)V

    iget-object v2, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 444101
    return-object v0
.end method

.method public final a(JJ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443935
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 443936
    const-string v1, "blockUser"

    new-instance v2, Lcom/facebook/friends/methods/BlockUserMethod$Params;

    invoke-direct {v2, p1, p2, p3, p4}, Lcom/facebook/friends/methods/BlockUserMethod$Params;-><init>(JJ)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 443937
    iget-object v1, p0, LX/2dj;->q:LX/0aG;

    const-string v2, "friending_block_user"

    const v3, 0xcf119fe

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    .line 443938
    invoke-static {v0}, LX/2dm;->a(LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 443939
    const/4 v1, 0x0

    invoke-static {v1}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(JJLX/2na;LX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "LX/2na;",
            "LX/2hA;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443940
    const/4 v5, 0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p5

    move-object v4, p6

    move-wide v6, p3

    invoke-static/range {v0 .. v7}, LX/2dj;->a(LX/2dj;JLX/2na;LX/2hA;ZJ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .param p4    # LX/2hC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/2h8;",
            "LX/2hC;",
            "LX/5P2;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443693
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 443694
    new-instance v0, LX/4F1;

    invoke-direct {v0}, LX/4F1;-><init>()V

    iget-object v1, p3, LX/2h8;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4F1;->a(Ljava/lang/String;)LX/4F1;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4F1;->a(Ljava/util/List;)LX/4F1;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 443695
    const-string v2, "warn_ack_for_ids"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 443696
    move-object v7, v0

    .line 443697
    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 443698
    invoke-direct/range {v1 .. v7}, LX/2dj;->a(JLX/2h8;LX/2hC;LX/5P2;LX/4F1;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLX/2h9;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/2h9;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443870
    new-instance v0, LX/4Ey;

    invoke-direct {v0}, LX/4Ey;-><init>()V

    iget-object v1, p3, LX/2h9;->value:Ljava/lang/String;

    .line 443871
    const-string v2, "source"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443872
    move-object v0, v0

    .line 443873
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 443874
    const-string v2, "cancelled_friend_requestee_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443875
    move-object v0, v0

    .line 443876
    new-instance v1, LX/868;

    invoke-direct {v1}, LX/868;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 443877
    iput-object v2, v1, LX/868;->b:Ljava/lang/String;

    .line 443878
    move-object v1, v1

    .line 443879
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 443880
    iput-object v2, v1, LX/868;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 443881
    move-object v1, v1

    .line 443882
    invoke-virtual {v1}, LX/868;->a()Lcom/facebook/friends/protocol/FriendRequestsConsistencyGraphQLModels$FriendRequestsRepresentedProfileFieldsModel;

    move-result-object v3

    .line 443883
    new-instance v1, LX/85a;

    invoke-direct {v1}, LX/85a;-><init>()V

    move-object v2, v1

    .line 443884
    const-string v1, "input"

    invoke-virtual {v2, v1, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 443885
    sget-object v6, LX/5P3;->a:LX/0Px;

    move-object v1, p0

    move-wide v4, p1

    invoke-static/range {v1 .. v6}, LX/2dj;->a(LX/2dj;LX/0zP;LX/0jT;JLX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/83n;

    invoke-direct {v1, p0}, LX/83n;-><init>(LX/2dj;)V

    iget-object v2, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 443886
    invoke-direct {p0, p1, p2, v0}, LX/2dj;->a(JLcom/google/common/util/concurrent/ListenableFuture;)V

    .line 443887
    return-object v0
.end method

.method public final a(JLX/2hB;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/2hB;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443844
    if-nez p3, :cond_0

    .line 443845
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RemoveFriendRef should not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 443846
    iget-object v1, p0, LX/2dj;->i:LX/03V;

    sget-object v2, LX/2dj;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 443847
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 443848
    :goto_0
    return-object v0

    .line 443849
    :cond_0
    new-instance v0, LX/4Ew;

    invoke-direct {v0}, LX/4Ew;-><init>()V

    iget-object v1, p3, LX/2hB;->value:Ljava/lang/String;

    .line 443850
    const-string v2, "source"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443851
    move-object v0, v0

    .line 443852
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 443853
    const-string v2, "unfriended_user_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443854
    move-object v0, v0

    .line 443855
    new-instance v1, LX/868;

    invoke-direct {v1}, LX/868;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 443856
    iput-object v2, v1, LX/868;->b:Ljava/lang/String;

    .line 443857
    move-object v1, v1

    .line 443858
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 443859
    iput-object v2, v1, LX/868;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 443860
    move-object v1, v1

    .line 443861
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 443862
    iput-object v2, v1, LX/868;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 443863
    move-object v1, v1

    .line 443864
    invoke-virtual {v1}, LX/868;->a()Lcom/facebook/friends/protocol/FriendRequestsConsistencyGraphQLModels$FriendRequestsRepresentedProfileFieldsModel;

    move-result-object v3

    .line 443865
    new-instance v1, LX/85Y;

    invoke-direct {v1}, LX/85Y;-><init>()V

    move-object v2, v1

    .line 443866
    const-string v1, "input"

    invoke-virtual {v2, v1, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 443867
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 443868
    move-object v1, p0

    move-wide v4, p1

    invoke-static/range {v1 .. v6}, LX/2dj;->a(LX/2dj;LX/0zP;LX/0jT;JLX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/83p;

    invoke-direct {v1, p0}, LX/83p;-><init>(LX/2dj;)V

    iget-object v2, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 443869
    invoke-direct {p0, p1, p2, v0}, LX/2dj;->a(JLcom/google/common/util/concurrent/ListenableFuture;)V

    goto :goto_0
.end method

.method public final a(JLX/2hC;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/2hC;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 443813
    new-instance v0, LX/4Iy;

    invoke-direct {v0}, LX/4Iy;-><init>()V

    iget-object v1, p3, LX/2hC;->value:Ljava/lang/String;

    .line 443814
    const-string v2, "people_you_may_know_location"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443815
    move-object v0, v0

    .line 443816
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 443817
    const-string v2, "people_you_may_know_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443818
    move-object v0, v0

    .line 443819
    new-instance v1, LX/86S;

    invoke-direct {v1}, LX/86S;-><init>()V

    move-object v1, v1

    .line 443820
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 443821
    new-instance v0, LX/86U;

    invoke-direct {v0}, LX/86U;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 443822
    iput-object v2, v0, LX/86U;->a:Ljava/lang/String;

    .line 443823
    move-object v0, v0

    .line 443824
    iput-boolean v3, v0, LX/86U;->b:Z

    .line 443825
    move-object v0, v0

    .line 443826
    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v7, 0x0

    .line 443827
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 443828
    iget-object v6, v0, LX/86U;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 443829
    const/4 v8, 0x2

    invoke-virtual {v5, v8}, LX/186;->c(I)V

    .line 443830
    invoke-virtual {v5, v10, v6}, LX/186;->b(II)V

    .line 443831
    iget-boolean v6, v0, LX/86U;->b:Z

    invoke-virtual {v5, v9, v6}, LX/186;->a(IZ)V

    .line 443832
    invoke-virtual {v5}, LX/186;->d()I

    move-result v6

    .line 443833
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 443834
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 443835
    invoke-virtual {v6, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 443836
    new-instance v5, LX/15i;

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 443837
    new-instance v6, Lcom/facebook/friends/protocol/PeopleYouMayKnowMutationModels$PeopleYouMayKnowBlacklistConsistencyFieldsModel;

    invoke-direct {v6, v5}, Lcom/facebook/friends/protocol/PeopleYouMayKnowMutationModels$PeopleYouMayKnowBlacklistConsistencyFieldsModel;-><init>(LX/15i;)V

    .line 443838
    move-object v0, v6

    .line 443839
    sget-object v2, LX/5P3;->a:LX/0Px;

    .line 443840
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v4

    .line 443841
    iput-boolean v3, v4, LX/399;->d:Z

    .line 443842
    iget-object v5, p0, LX/2dj;->l:LX/2dm;

    invoke-virtual {v5, v4, v2}, LX/2dm;->a(LX/399;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 443843
    const/4 v1, 0x0

    invoke-static {v1}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v1

    iget-object v2, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLX/2na;LX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/2na;",
            "LX/2hA;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443812
    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v7}, LX/2dj;->a(LX/2dj;JLX/2na;LX/2hA;ZJ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443807
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 443808
    const-string v1, "blacklistPeopleYouMayInviteParamsKey"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 443809
    iget-object v1, p0, LX/2dj;->q:LX/0aG;

    const-string v2, "friending_blacklist_people_you_may_invite"

    const v3, 0x4b0a2c69    # 9055337.0f

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    .line 443810
    invoke-static {v0}, LX/2dm;->a(LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 443811
    new-instance v1, LX/83q;

    invoke-direct {v1, p0}, LX/83q;-><init>(LX/2dj;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/2iQ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/FutureFriendingUpdateType;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/2iQ;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443776
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 443777
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 443778
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 443779
    new-instance v0, LX/4FB;

    invoke-direct {v0}, LX/4FB;-><init>()V

    .line 443780
    const-string v1, "action"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443781
    move-object v0, v0

    .line 443782
    iget-object v1, p2, LX/2iQ;->value:Ljava/lang/String;

    .line 443783
    const-string v2, "location"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443784
    move-object v0, v0

    .line 443785
    const-string v1, "credential"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443786
    move-object v0, v0

    .line 443787
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 443788
    const-string v1, "name"

    invoke-virtual {v0, v1, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443789
    :cond_0
    invoke-static {p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 443790
    const-string v1, "contact_id"

    invoke-virtual {v0, v1, p5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443791
    :cond_1
    new-instance v1, LX/85t;

    invoke-direct {v1}, LX/85t;-><init>()V

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 443792
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 443793
    iget-object v5, v1, LX/85t;->a:Lcom/facebook/friends/protocol/FriendMutationsModels$FutureFriendingCoreMutationFieldsModel$ContactPointModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 443794
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 443795
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 443796
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 443797
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 443798
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 443799
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 443800
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 443801
    new-instance v5, Lcom/facebook/friends/protocol/FriendMutationsModels$FutureFriendingCoreMutationFieldsModel;

    invoke-direct {v5, v4}, Lcom/facebook/friends/protocol/FriendMutationsModels$FutureFriendingCoreMutationFieldsModel;-><init>(LX/15i;)V

    .line 443802
    move-object v1, v5

    .line 443803
    new-instance v2, LX/85g;

    invoke-direct {v2}, LX/85g;-><init>()V

    move-object v2, v2

    .line 443804
    const-string v3, "input"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 443805
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 443806
    invoke-static {p0, v2, v1, p5, v0}, LX/2dj;->a(LX/2dj;LX/0zP;LX/0jT;Ljava/lang/String;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v1

    iget-object v2, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/5P4;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/5P4;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443748
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 443749
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 443750
    new-instance v0, LX/4Dh;

    invoke-direct {v0}, LX/4Dh;-><init>()V

    .line 443751
    const-string v1, "credential"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443752
    move-object v0, v0

    .line 443753
    iget-object v1, p2, LX/5P4;->value:Ljava/lang/String;

    .line 443754
    const-string v2, "location"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443755
    move-object v0, v0

    .line 443756
    if-eqz p3, :cond_0

    .line 443757
    const-string v1, "name"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443758
    :cond_0
    if-eqz p4, :cond_1

    .line 443759
    const-string v1, "contact_id"

    invoke-virtual {v0, v1, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443760
    :cond_1
    new-instance v1, LX/85l;

    invoke-direct {v1}, LX/85l;-><init>()V

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 443761
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 443762
    iget-object v5, v1, LX/85l;->a:Lcom/facebook/friends/protocol/FriendMutationsModels$ContactInviteCoreMutationFieldsModel$ContactPointModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 443763
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 443764
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 443765
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 443766
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 443767
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 443768
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 443769
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 443770
    new-instance v5, Lcom/facebook/friends/protocol/FriendMutationsModels$ContactInviteCoreMutationFieldsModel;

    invoke-direct {v5, v4}, Lcom/facebook/friends/protocol/FriendMutationsModels$ContactInviteCoreMutationFieldsModel;-><init>(LX/15i;)V

    .line 443771
    move-object v1, v5

    .line 443772
    new-instance v2, LX/85X;

    invoke-direct {v2}, LX/85X;-><init>()V

    move-object v2, v2

    .line 443773
    const-string v3, "input"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 443774
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 443775
    invoke-static {p0, v2, v1, p4, v0}, LX/2dj;->a(LX/2dj;LX/0zP;LX/0jT;Ljava/lang/String;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v1

    iget-object v2, p0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/FollowLocations;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443744
    const/4 v0, 0x0

    .line 443745
    invoke-virtual {p0, p1, p2, v0}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 443746
    const/4 v2, 0x0

    invoke-static {v2}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 443747
    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ProfileSubFollowStatus;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/FollowLocations;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443717
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 443718
    new-instance v1, LX/4Im;

    invoke-direct {v1}, LX/4Im;-><init>()V

    .line 443719
    const-string v3, "profile_id"

    invoke-virtual {v1, v3, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443720
    move-object v1, v1

    .line 443721
    const-string v3, "new_status"

    invoke-virtual {v1, v3, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443722
    move-object v1, v1

    .line 443723
    const-string v3, "location"

    invoke-virtual {v1, v3, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443724
    move-object v1, v1

    .line 443725
    new-instance v3, LX/85U;

    invoke-direct {v3}, LX/85U;-><init>()V

    move-object v3, v3

    .line 443726
    const-string v4, "input"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 443727
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    new-instance v1, LX/868;

    invoke-direct {v1}, LX/868;-><init>()V

    .line 443728
    iput-object p1, v1, LX/868;->b:Ljava/lang/String;

    .line 443729
    move-object v1, v1

    .line 443730
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 443731
    iput-object v4, v1, LX/868;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 443732
    move-object v4, v1

    .line 443733
    const-string v1, "SEE_FIRST"

    if-ne p2, v1, :cond_1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 443734
    :goto_0
    iput-object v1, v4, LX/868;->c:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 443735
    move-object v1, v4

    .line 443736
    invoke-virtual {v1}, LX/868;->a()Lcom/facebook/friends/protocol/FriendRequestsConsistencyGraphQLModels$FriendRequestsRepresentedProfileFieldsModel;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v1

    .line 443737
    if-eqz v0, :cond_0

    .line 443738
    iget-object v3, p0, LX/2dj;->r:LX/0SI;

    invoke-interface {v3}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    .line 443739
    iput-object v3, v1, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 443740
    :cond_0
    iget-object v3, p0, LX/2dj;->l:LX/2dm;

    invoke-virtual {v3, v1}, LX/2dm;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 443741
    invoke-static {v2}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 443742
    return-object v0

    :cond_1
    move-object v1, v2

    .line 443743
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/FollowLocations;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSubscribeCoreMutationFieldsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 443699
    new-instance v0, LX/4CW;

    invoke-direct {v0}, LX/4CW;-><init>()V

    .line 443700
    const-string v1, "subscribee_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443701
    move-object v0, v0

    .line 443702
    const-string v1, "subscribe_location"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443703
    move-object v0, v0

    .line 443704
    new-instance v1, LX/85V;

    invoke-direct {v1}, LX/85V;-><init>()V

    move-object v1, v1

    .line 443705
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 443706
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    new-instance v1, LX/868;

    invoke-direct {v1}, LX/868;-><init>()V

    .line 443707
    iput-object p1, v1, LX/868;->b:Ljava/lang/String;

    .line 443708
    move-object v1, v1

    .line 443709
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 443710
    iput-object v2, v1, LX/868;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 443711
    move-object v1, v1

    .line 443712
    invoke-virtual {v1}, LX/868;->a()Lcom/facebook/friends/protocol/FriendRequestsConsistencyGraphQLModels$FriendRequestsRepresentedProfileFieldsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 443713
    if-eqz p3, :cond_0

    .line 443714
    iget-object v1, p0, LX/2dj;->r:LX/0SI;

    invoke-interface {v1}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 443715
    iput-object v1, v0, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 443716
    :cond_0
    iget-object v1, p0, LX/2dj;->l:LX/2dm;

    invoke-virtual {v1, v0}, LX/2dm;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(ILX/2hC;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/2hC;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 443690
    const/4 v0, 0x0

    .line 443691
    iget-object v1, p0, LX/2dj;->k:LX/2dk;

    invoke-static {p0}, LX/2dj;->o(LX/2dj;)Ljava/lang/String;

    move-result-object v2

    move v3, p1

    move-object v4, v0

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, LX/2dk;->a(Ljava/lang/String;ILjava/lang/Integer;LX/2hC;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-static {p0, v1}, LX/2dj;->c(LX/2dj;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 443692
    return-object v0
.end method

.method public final b(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .param p4    # LX/2hC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/2h8;",
            "LX/2hC;",
            "LX/5P2;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443889
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 443890
    new-instance v0, LX/4F1;

    invoke-direct {v0}, LX/4F1;-><init>()V

    iget-object v1, p3, LX/2h8;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4F1;->a(Ljava/lang/String;)LX/4F1;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4F1;->a(Ljava/util/List;)LX/4F1;

    move-result-object v7

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 443891
    invoke-direct/range {v1 .. v7}, LX/2dj;->a(JLX/2h8;LX/2hC;LX/5P2;LX/4F1;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/FollowLocations;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443892
    new-instance v1, LX/4CX;

    invoke-direct {v1}, LX/4CX;-><init>()V

    .line 443893
    const-string v2, "unsubscribee_id"

    invoke-virtual {v1, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443894
    move-object v1, v1

    .line 443895
    const-string v2, "subscribe_location"

    invoke-virtual {v1, v2, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443896
    move-object v1, v1

    .line 443897
    new-instance v2, LX/85W;

    invoke-direct {v2}, LX/85W;-><init>()V

    move-object v2, v2

    .line 443898
    const-string v3, "input"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 443899
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    new-instance v2, LX/868;

    invoke-direct {v2}, LX/868;-><init>()V

    .line 443900
    iput-object p1, v2, LX/868;->b:Ljava/lang/String;

    .line 443901
    move-object v2, v2

    .line 443902
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 443903
    iput-object v3, v2, LX/868;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 443904
    move-object v2, v2

    .line 443905
    invoke-virtual {v2}, LX/868;->a()Lcom/facebook/friends/protocol/FriendRequestsConsistencyGraphQLModels$FriendRequestsRepresentedProfileFieldsModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v1

    .line 443906
    goto :goto_0

    .line 443907
    :goto_0
    iget-object v2, p0, LX/2dj;->l:LX/2dm;

    invoke-virtual {v2, v1}, LX/2dm;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 443908
    const/4 v2, 0x0

    invoke-static {v2}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 443909
    return-object v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 443910
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2dj;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 443911
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2dj;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    .prologue
    .line 443912
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2dj;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Z
    .locals 1

    .prologue
    .line 443913
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2dj;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {v0}, LX/2dj;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    .prologue
    .line 443914
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2dj;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {v0}, LX/2dj;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Z
    .locals 1

    .prologue
    .line 443915
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2dj;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {v0}, LX/2dj;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443916
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2dj;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2dj;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 443917
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 443918
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2dj;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 443919
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443920
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2dj;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2dj;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 443921
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 443922
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2dj;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 443923
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized j()V
    .locals 4

    .prologue
    .line 443924
    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    :try_start_0
    invoke-static {v0, v1, v2, v3}, LX/16z;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    iput-object v0, p0, LX/2dj;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 443925
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, LX/16z;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    iput-object v0, p0, LX/2dj;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 443926
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, LX/16z;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    iput-object v0, p0, LX/2dj;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 443927
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, LX/16z;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    iput-object v0, p0, LX/2dj;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 443928
    iget-object v0, p0, LX/2dj;->k:LX/2dk;

    const/4 v2, 0x0

    .line 443929
    iget v1, v0, LX/2dk;->f:I

    if-gtz v1, :cond_0

    iget v1, v0, LX/2dk;->g:I

    if-lez v1, :cond_1

    .line 443930
    :cond_0
    iput v2, v0, LX/2dk;->f:I

    .line 443931
    iput v2, v0, LX/2dk;->g:I

    .line 443932
    iget-object v1, v0, LX/2dk;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1My;

    invoke-virtual {v1}, LX/1My;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 443933
    :cond_1
    monitor-exit p0

    return-void

    .line 443934
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
