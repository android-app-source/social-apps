.class public LX/261;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 370056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 370057
    return-void
.end method

.method public static a(LX/15w;LX/0lC;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0lC;",
            ")",
            "LX/0Px",
            "<",
            "LX/2Oo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 370160
    const-string v0, "error"

    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370161
    const-class v0, Lcom/facebook/graphql/error/GraphQLError;

    invoke-virtual {p1, p0, v0}, LX/0lD;->a(LX/15w;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/error/GraphQLError;

    invoke-static {v0}, LX/261;->a(Lcom/facebook/graphql/error/GraphQLError;)LX/2Oo;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 370162
    :goto_0
    return-object v0

    .line 370163
    :cond_0
    const-string v0, "errors"

    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 370164
    const/4 v0, 0x1

    new-array v0, v0, [LX/15z;

    const/4 v1, 0x0

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    aput-object v2, v0, v1

    invoke-static {p0, v0}, LX/261;->a(LX/15w;[LX/15z;)V

    .line 370165
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 370166
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 370167
    :goto_1
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v0, v2, :cond_1

    .line 370168
    const-class v0, Lcom/facebook/graphql/error/GraphQLError;

    invoke-virtual {p1, p0, v0}, LX/0lD;->a(LX/15w;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/error/GraphQLError;

    invoke-static {v0}, LX/261;->a(Lcom/facebook/graphql/error/GraphQLError;)LX/2Oo;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 370169
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    goto :goto_1

    .line 370170
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 370171
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 370172
    goto :goto_0
.end method

.method public static a(ILX/15w;LX/0lC;)LX/15w;
    .locals 3

    .prologue
    .line 370134
    if-lez p0, :cond_2

    .line 370135
    invoke-static {p1, p2}, LX/261;->b(LX/15w;LX/0lC;)Ljava/lang/String;

    .line 370136
    :goto_0
    add-int/lit8 p0, p0, -0x1

    if-lez p0, :cond_3

    .line 370137
    const/4 p2, 0x1

    const/4 v2, 0x0

    .line 370138
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_0

    .line 370139
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 370140
    :cond_0
    new-array v0, p2, [LX/15z;

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    aput-object v1, v0, v2

    invoke-static {p1, v0}, LX/261;->a(LX/15w;[LX/15z;)V

    .line 370141
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 370142
    const/4 v0, 0x2

    new-array v0, v0, [LX/15z;

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    aput-object v1, v0, v2

    sget-object v1, LX/15z;->END_OBJECT:LX/15z;

    aput-object v1, v0, p2

    invoke-static {p1, v0}, LX/261;->a(LX/15w;[LX/15z;)V

    .line 370143
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 370144
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 370145
    const-string v1, "__type__"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 370146
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 370147
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 370148
    new-array v0, p2, [LX/15z;

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    aput-object v1, v0, v2

    invoke-static {p1, v0}, LX/261;->a(LX/15w;[LX/15z;)V

    .line 370149
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 370150
    :cond_1
    :goto_1
    goto :goto_0

    .line 370151
    :cond_2
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    if-nez v0, :cond_3

    .line 370152
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 370153
    :cond_3
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    .line 370154
    const/4 v0, 0x4

    new-array v0, v0, [LX/15z;

    const/4 v1, 0x0

    sget-object v2, LX/15z;->VALUE_NULL:LX/15z;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, LX/261;->a(LX/15w;[LX/15z;)V

    .line 370155
    return-object p1

    .line 370156
    :cond_4
    const-string v1, "__typename"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 370157
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 370158
    new-array v0, p2, [LX/15z;

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    aput-object v1, v0, v2

    invoke-static {p1, v0}, LX/261;->a(LX/15w;[LX/15z;)V

    .line 370159
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    goto :goto_1
.end method

.method public static a(LX/15w;)LX/15z;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 370128
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    if-nez v0, :cond_0

    .line 370129
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 370130
    :cond_0
    new-array v0, v4, [LX/15z;

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    aput-object v1, v0, v3

    invoke-static {p0, v0}, LX/261;->a(LX/15w;[LX/15z;)V

    .line 370131
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 370132
    const/4 v1, 0x2

    new-array v1, v1, [LX/15z;

    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    aput-object v2, v1, v3

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    aput-object v2, v1, v4

    invoke-static {p0, v1}, LX/261;->a(LX/15w;[LX/15z;)V

    .line 370133
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/error/GraphQLError;)LX/2Oo;
    .locals 2

    .prologue
    .line 370110
    new-instance v0, LX/4UY;

    invoke-direct {v0}, LX/4UY;-><init>()V

    iget v1, p0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    .line 370111
    iput v1, v0, LX/4UY;->a:I

    .line 370112
    move-object v0, v0

    .line 370113
    iget-object v1, p0, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    .line 370114
    iput-object v1, v0, LX/4UY;->d:Ljava/lang/String;

    .line 370115
    move-object v0, v0

    .line 370116
    iget-object v1, p0, Lcom/facebook/graphql/error/GraphQLError;->debugInfo:Ljava/lang/String;

    .line 370117
    iput-object v1, v0, LX/4UY;->h:Ljava/lang/String;

    .line 370118
    move-object v0, v0

    .line 370119
    iget-object v1, p0, Lcom/facebook/graphql/error/GraphQLError;->severity:Ljava/lang/String;

    .line 370120
    iput-object v1, v0, LX/4UY;->j:Ljava/lang/String;

    .line 370121
    move-object v0, v0

    .line 370122
    invoke-virtual {v0}, LX/4UY;->a()Lcom/facebook/graphql/error/GraphQLError;

    move-result-object v1

    .line 370123
    iget v0, p0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    sparse-switch v0, :sswitch_data_0

    .line 370124
    new-instance v0, LX/4Ua;

    invoke-direct {v0, p0}, LX/4Ua;-><init>(Lcom/facebook/graphql/error/GraphQLError;)V

    :goto_0
    return-object v0

    .line 370125
    :sswitch_0
    new-instance v0, LX/4cy;

    invoke-direct {v0, v1}, LX/4cy;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    goto :goto_0

    .line 370126
    :sswitch_1
    new-instance v0, LX/4cz;

    invoke-direct {v0, v1}, LX/4cz;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    goto :goto_0

    .line 370127
    :sswitch_2
    new-instance v0, LX/4d0;

    invoke-direct {v0, v1}, LX/4d0;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x66 -> :sswitch_0
        0xbe -> :sswitch_0
        0x198eff -> :sswitch_1
        0x198f05 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/0w5;LX/15w;LX/0lC;Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0w5",
            "<TT;>;",
            "LX/15w;",
            "LX/0lC;",
            "Z)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 370106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 370107
    new-instance v1, LX/4Zx;

    invoke-direct {v1, p0, v0}, LX/4Zx;-><init>(LX/0w5;Ljava/util/List;)V

    .line 370108
    invoke-static {p1, p2, v1, p3}, LX/261;->a(LX/15w;LX/0lC;LX/4Zw;Z)V

    .line 370109
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/15w;LX/0lC;LX/4Zw;)V
    .locals 3

    .prologue
    .line 370088
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    if-nez v0, :cond_0

    .line 370089
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 370090
    :cond_0
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 370091
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_1

    .line 370092
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 370093
    :cond_1
    :goto_0
    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_4

    .line 370094
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 370095
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 370096
    const-string v1, "error"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 370097
    invoke-virtual {p0}, LX/15w;->J()LX/0lG;

    move-result-object v1

    .line 370098
    invoke-interface {v1, p1}, LX/0lG;->a(LX/0lD;)LX/15w;

    move-result-object v0

    const-class v2, Lcom/facebook/graphql/error/GraphQLError;

    invoke-virtual {v0, v2}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/error/GraphQLError;

    .line 370099
    if-eqz v0, :cond_2

    iget v2, v0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    if-ltz v2, :cond_2

    .line 370100
    invoke-static {v0}, LX/261;->a(Lcom/facebook/graphql/error/GraphQLError;)LX/2Oo;

    move-result-object v0

    throw v0

    .line 370101
    :cond_2
    invoke-interface {v1, p1}, LX/0lG;->a(LX/0lD;)LX/15w;

    move-result-object v0

    .line 370102
    invoke-interface {p2, v0}, LX/4Zw;->a(LX/15w;)V

    .line 370103
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto :goto_0

    .line 370104
    :cond_3
    invoke-interface {p2, p0}, LX/4Zw;->a(LX/15w;)V

    goto :goto_1

    .line 370105
    :cond_4
    return-void
.end method

.method public static a(LX/15w;LX/0lC;LX/4Zw;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 370074
    if-eqz p3, :cond_2

    .line 370075
    invoke-static {v2, p0, p1}, LX/261;->a(ILX/15w;LX/0lC;)LX/15w;

    .line 370076
    new-array v0, v2, [LX/15z;

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    aput-object v1, v0, v3

    invoke-static {p0, v0}, LX/261;->a(LX/15w;[LX/15z;)V

    .line 370077
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 370078
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    if-nez v0, :cond_0

    .line 370079
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 370080
    :cond_0
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 370081
    :goto_0
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_1

    .line 370082
    invoke-interface {p2, p0}, LX/4Zw;->a(LX/15w;)V

    .line 370083
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto :goto_0

    .line 370084
    :cond_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 370085
    const/4 v0, 0x2

    new-array v0, v0, [LX/15z;

    sget-object v1, LX/15z;->END_ARRAY:LX/15z;

    aput-object v1, v0, v3

    sget-object v1, LX/15z;->END_OBJECT:LX/15z;

    aput-object v1, v0, v2

    invoke-static {p0, v0}, LX/261;->a(LX/15w;[LX/15z;)V

    .line 370086
    :goto_1
    return-void

    .line 370087
    :cond_2
    invoke-static {p0, p1, p2}, LX/261;->a(LX/15w;LX/0lC;LX/4Zw;)V

    goto :goto_1
.end method

.method public static varargs a(LX/15w;[LX/15z;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 370066
    const/4 v0, 0x1

    .line 370067
    array-length v3, p1

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, p1, v2

    .line 370068
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    if-ne v5, v4, :cond_0

    move v0, v1

    .line 370069
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 370070
    :cond_1
    if-eqz v0, :cond_2

    .line 370071
    const/16 v0, 0x2c

    invoke-static {v0}, LX/0PO;->on(C)LX/0PO;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0PO;->join([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 370072
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected token(s) "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " but found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/facebook/common/json/FbJsonDeserializer;->getJsonParserText(LX/15w;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 370073
    :cond_2
    return-void
.end method

.method private static b(LX/15w;LX/0lC;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 370058
    invoke-static {p0}, LX/261;->a(LX/15w;)LX/15z;

    move-result-object v0

    .line 370059
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 370060
    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v2, :cond_0

    .line 370061
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 370062
    :cond_0
    invoke-static {p0, p1}, LX/261;->a(LX/15w;LX/0lC;)LX/0Px;

    move-result-object v0

    .line 370063
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 370064
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oo;

    throw v0

    .line 370065
    :cond_1
    return-object v1
.end method
