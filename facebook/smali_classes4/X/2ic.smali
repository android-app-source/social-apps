.class public final LX/2ic;
.super LX/1Cd;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 0

    .prologue
    .line 451826
    iput-object p1, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-direct {p0}, LX/1Cd;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;)V
    .locals 5

    .prologue
    .line 451827
    iget-object v0, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-boolean v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aJ:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->F:LX/2hc;

    invoke-virtual {v0}, LX/2hc;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 451828
    :cond_0
    :goto_0
    return-void

    .line 451829
    :cond_1
    invoke-interface {p1}, LX/0g8;->q()I

    move-result v0

    .line 451830
    invoke-interface {p1}, LX/0g8;->r()I

    move-result v1

    .line 451831
    :goto_1
    if-ge v0, v1, :cond_0

    .line 451832
    iget-object v2, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v2, v2, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    invoke-interface {v2, v0}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v2

    .line 451833
    iget-object v3, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v3, v3, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    invoke-interface {v3, v0}, LX/0g8;->c(I)Landroid/view/View;

    move-result-object v3

    .line 451834
    instance-of v2, v2, LX/2no;

    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 451835
    iget-object v0, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->F:LX/2hc;

    .line 451836
    new-instance v1, LX/0hs;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0hs;-><init>(Landroid/content/Context;)V

    .line 451837
    const v2, 0x7f081fe7

    invoke-virtual {v1, v2}, LX/0hs;->a(I)V

    .line 451838
    const v2, 0x7f081fe8

    invoke-virtual {v1, v2}, LX/0hs;->b(I)V

    .line 451839
    iget-object v2, v0, LX/2hc;->a:LX/0wM;

    const v4, 0x7f0208ed

    const/4 p1, -0x1

    invoke-virtual {v2, v4, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 451840
    const/16 v2, 0x1770

    .line 451841
    iput v2, v1, LX/0hs;->t:I

    .line 451842
    invoke-virtual {v1, v3}, LX/0ht;->a(Landroid/view/View;)V

    .line 451843
    iget-object v0, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->F:LX/2hc;

    .line 451844
    iget-object v1, v0, LX/2hc;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/1nR;->f:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 451845
    goto :goto_0

    .line 451846
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 451847
    instance-of v0, p1, LX/2no;

    if-eqz v0, :cond_3

    .line 451848
    check-cast p1, LX/2no;

    .line 451849
    iget-boolean v0, p1, LX/2no;->f:Z

    move v0, v0

    .line 451850
    if-eqz v0, :cond_1

    .line 451851
    :cond_0
    :goto_0
    return-void

    .line 451852
    :cond_1
    iget-object v0, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->C:LX/2hb;

    .line 451853
    iget-object v7, p1, LX/2no;->d:Ljava/lang/String;

    move-object v7, v7

    .line 451854
    if-eqz v7, :cond_2

    .line 451855
    iget-boolean v7, p1, LX/2no;->f:Z

    move v7, v7

    .line 451856
    if-eqz v7, :cond_6

    .line 451857
    :cond_2
    :goto_1
    goto :goto_0

    .line 451858
    :cond_3
    instance-of v0, p1, Lcom/facebook/friends/model/PersonYouMayKnow;

    if-eqz v0, :cond_5

    .line 451859
    check-cast p1, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 451860
    iget-boolean v0, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->i:Z

    move v0, v0

    .line 451861
    if-nez v0, :cond_0

    .line 451862
    iget-object v0, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->C:LX/2hb;

    .line 451863
    iget-object v7, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->h:Ljava/lang/String;

    move-object v7, v7

    .line 451864
    if-eqz v7, :cond_4

    .line 451865
    iget-boolean v7, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->i:Z

    move v7, v7

    .line 451866
    if-eqz v7, :cond_7

    .line 451867
    :cond_4
    :goto_2
    iget-object v0, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->L:LX/2hd;

    iget-object v0, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v2, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ay:Ljava/lang/String;

    .line 451868
    iget-object v0, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->j:Ljava/lang/String;

    move-object v3, v0

    .line 451869
    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v4

    sget-object v6, LX/2hC;->JEWEL:LX/2hC;

    invoke-virtual/range {v1 .. v6}, LX/2hd;->a(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    .line 451870
    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->k()V

    goto :goto_0

    .line 451871
    :cond_5
    instance-of v0, p1, Lcom/facebook/friends/model/FriendRequest;

    if-eqz v0, :cond_0

    .line 451872
    check-cast p1, Lcom/facebook/friends/model/FriendRequest;

    .line 451873
    iget-object v0, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->x:LX/2hW;

    invoke-virtual {v0}, LX/2hW;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451874
    iget-boolean v0, p1, Lcom/facebook/friends/model/FriendRequest;->k:Z

    move v0, v0

    .line 451875
    if-nez v0, :cond_0

    .line 451876
    iget-object v0, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->x:LX/2hW;

    invoke-virtual {p1}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v2

    sget-object v1, LX/2hA;->MOBILE_JEWEL:LX/2hA;

    iget-object v4, p0, LX/2ic;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v4, v4, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ay:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1, v4}, LX/2hW;->a(JLX/2hA;Ljava/lang/String;)V

    .line 451877
    invoke-virtual {p1}, Lcom/facebook/friends/model/FriendRequest;->m()V

    goto :goto_0

    .line 451878
    :cond_6
    const-string v7, "pymi_imp"

    .line 451879
    iget-object v8, p1, LX/2no;->d:Ljava/lang/String;

    move-object v8, v8

    .line 451880
    invoke-virtual {p1}, LX/2no;->a()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v7, v8, v9}, LX/2hb;->a(LX/2hb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 451881
    const/4 v7, 0x1

    iput-boolean v7, p1, LX/2no;->f:Z

    .line 451882
    goto :goto_1

    .line 451883
    :cond_7
    const-string v7, "pymk_imp"

    .line 451884
    iget-object v8, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->h:Ljava/lang/String;

    move-object v8, v8

    .line 451885
    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v7, v8, v9}, LX/2hb;->a(LX/2hb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 451886
    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->k()V

    goto :goto_2
.end method
