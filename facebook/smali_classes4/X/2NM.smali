.class public LX/2NM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/2Dr;

.field private final d:LX/0dC;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/2Ds;

.field public final g:LX/2Du;

.field public final h:Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

.field public final i:LX/10M;

.field public final j:LX/10O;

.field public final k:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final l:LX/10N;

.field public final m:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 399718
    const-class v0, LX/2NM;

    sput-object v0, LX/2NM;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/2Dr;LX/0dC;LX/0Or;LX/2Ds;LX/2Du;Lcom/facebook/account/recovery/common/model/AccountRecoveryData;LX/10M;LX/10O;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/10N;LX/0SG;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/2Dr;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/2Ds;",
            "LX/2Du;",
            "Lcom/facebook/account/recovery/common/model/AccountRecoveryData;",
            "LX/10M;",
            "LX/10O;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/10N;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 399719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399720
    iput-object p1, p0, LX/2NM;->b:Ljava/util/concurrent/ExecutorService;

    .line 399721
    iput-object p2, p0, LX/2NM;->c:LX/2Dr;

    .line 399722
    iput-object p3, p0, LX/2NM;->d:LX/0dC;

    .line 399723
    iput-object p4, p0, LX/2NM;->e:LX/0Or;

    .line 399724
    iput-object p5, p0, LX/2NM;->f:LX/2Ds;

    .line 399725
    iput-object p6, p0, LX/2NM;->g:LX/2Du;

    .line 399726
    iput-object p7, p0, LX/2NM;->h:Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    .line 399727
    iput-object p8, p0, LX/2NM;->i:LX/10M;

    .line 399728
    iput-object p9, p0, LX/2NM;->j:LX/10O;

    .line 399729
    iput-object p10, p0, LX/2NM;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 399730
    iput-object p11, p0, LX/2NM;->l:LX/10N;

    .line 399731
    iput-object p12, p0, LX/2NM;->m:LX/0SG;

    .line 399732
    return-void
.end method

.method public static a(LX/0QB;)LX/2NM;
    .locals 14

    .prologue
    .line 399733
    new-instance v1, LX/2NM;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/2Dr;->b(LX/0QB;)LX/2Dr;

    move-result-object v3

    check-cast v3, LX/2Dr;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v4

    check-cast v4, LX/0dC;

    const/16 v5, 0xb83

    invoke-static {p0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/2Ds;->b(LX/0QB;)LX/2Ds;

    move-result-object v6

    check-cast v6, LX/2Ds;

    invoke-static {p0}, LX/2Du;->b(LX/0QB;)LX/2Du;

    move-result-object v7

    check-cast v7, LX/2Du;

    invoke-static {p0}, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a(LX/0QB;)Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    move-result-object v8

    check-cast v8, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    invoke-static {p0}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v9

    check-cast v9, LX/10M;

    invoke-static {p0}, LX/10O;->b(LX/0QB;)LX/10O;

    move-result-object v10

    check-cast v10, LX/10O;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/10N;->b(LX/0QB;)LX/10N;

    move-result-object v12

    check-cast v12, LX/10N;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v13

    check-cast v13, LX/0SG;

    invoke-direct/range {v1 .. v13}, LX/2NM;-><init>(Ljava/util/concurrent/ExecutorService;LX/2Dr;LX/0dC;LX/0Or;LX/2Ds;LX/2Du;Lcom/facebook/account/recovery/common/model/AccountRecoveryData;LX/10M;LX/10O;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/10N;LX/0SG;)V

    .line 399734
    move-object v0, v1

    .line 399735
    return-object v0
.end method

.method public static d(LX/2NM;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 399736
    sget-object v0, LX/26p;->s:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 399737
    iget-object v1, p0, LX/2NM;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    return v0
.end method

.method public static e(LX/2NM;Ljava/lang/String;)Lcom/facebook/openidconnect/model/OpenIDCredential;
    .locals 3

    .prologue
    .line 399738
    iget-object v0, p0, LX/2NM;->h:Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a()LX/0Px;

    move-result-object v0

    .line 399739
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/openidconnect/model/OpenIDCredential;

    .line 399740
    iget-object v2, v0, Lcom/facebook/openidconnect/model/OpenIDCredential;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 399741
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
