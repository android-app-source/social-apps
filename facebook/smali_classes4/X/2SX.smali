.class public LX/2SX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Uh;

.field public final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Uh;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 412557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412558
    iput-object p1, p0, LX/2SX;->a:LX/0Uh;

    .line 412559
    iput-object p2, p0, LX/2SX;->b:LX/0ad;

    .line 412560
    return-void
.end method

.method public static a(LX/0QB;)LX/2SX;
    .locals 1

    .prologue
    .line 412561
    invoke-static {p0}, LX/2SX;->b(LX/0QB;)LX/2SX;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2SX;
    .locals 3

    .prologue
    .line 412562
    new-instance v2, LX/2SX;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/2SX;-><init>(LX/0Uh;LX/0ad;)V

    .line 412563
    return-object v2
.end method

.method public static d(LX/2SX;)Z
    .locals 2

    .prologue
    .line 412564
    iget-object v0, p0, LX/2SX;->a:LX/0Uh;

    const/16 v1, 0x4

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method
