.class public LX/3I7;
.super LX/2oy;
.source ""

# interfaces
.implements LX/3I8;


# instance fields
.field public a:LX/19m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3I9;

.field public c:LX/3IC;

.field public d:Landroid/view/GestureDetector;

.field public e:Z

.field public f:Z

.field public n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 545584
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3I7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 545585
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 545602
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3I7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545603
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 545593
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545594
    iput-boolean v0, p0, LX/3I7;->e:Z

    .line 545595
    iput-boolean v0, p0, LX/3I7;->f:Z

    .line 545596
    iput-boolean v0, p0, LX/3I7;->n:Z

    .line 545597
    const-class v0, LX/3I7;

    invoke-static {v0, p0}, LX/3I7;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 545598
    new-instance v0, LX/3I9;

    invoke-direct {v0, p0}, LX/3I9;-><init>(LX/3I7;)V

    iput-object v0, p0, LX/3I7;->b:LX/3I9;

    .line 545599
    new-instance v0, LX/3IC;

    iget-object v1, p0, LX/3I7;->b:LX/3I9;

    iget-object p2, p0, LX/3I7;->b:LX/3I9;

    invoke-direct {v0, p1, v1, p2}, LX/3IC;-><init>(Landroid/content/Context;LX/3IA;LX/3IB;)V

    iput-object v0, p0, LX/3I7;->c:LX/3IC;

    .line 545600
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, LX/3I7;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance p2, LX/3IH;

    invoke-direct {p2, p0}, LX/3IH;-><init>(LX/3I7;)V

    invoke-direct {v0, v1, p2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/3I7;->d:Landroid/view/GestureDetector;

    .line 545601
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3I7;

    invoke-static {p0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object p0

    check-cast p0, LX/19m;

    iput-object p0, p1, LX/3I7;->a:LX/19m;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)I
    .locals 1

    .prologue
    .line 545589
    iget-object v0, p0, LX/3I7;->d:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    iput-boolean v0, p0, LX/3I7;->n:Z

    .line 545590
    iget-boolean v0, p0, LX/3I7;->n:Z

    if-eqz v0, :cond_0

    .line 545591
    const/4 v0, 0x2

    .line 545592
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/3I7;->c:LX/3IC;

    invoke-virtual {v0, p1}, LX/3IC;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 545588
    iget-boolean v0, p0, LX/3I7;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/3I7;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x1ff2f5f9

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 545586
    iget-object v2, p0, LX/3I7;->d:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    iput-boolean v2, p0, LX/3I7;->n:Z

    .line 545587
    iget-boolean v2, p0, LX/3I7;->n:Z

    if-nez v2, :cond_0

    iget-object v2, p0, LX/3I7;->c:LX/3IC;

    invoke-virtual {v2, p1}, LX/3IC;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    const v2, -0x512144a6

    invoke-static {v2, v1}, LX/02F;->a(II)V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
