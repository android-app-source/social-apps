.class public final LX/3EE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final b:I

.field public final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/CDf;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/04D;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I",
            "LX/0am",
            "<",
            "LX/CDf;",
            ">;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 537116
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;LX/04D;)V

    .line 537117
    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;LX/04D;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I",
            "LX/0am",
            "<",
            "LX/CDf;",
            ">;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;",
            "LX/04D;",
            ")V"
        }
    .end annotation

    .prologue
    .line 537118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537119
    iput-object p1, p0, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 537120
    iput p2, p0, LX/3EE;->b:I

    .line 537121
    iput-object p3, p0, LX/3EE;->c:LX/0am;

    .line 537122
    iput-object p4, p0, LX/3EE;->d:Ljava/util/concurrent/atomic/AtomicReference;

    .line 537123
    iput-object p5, p0, LX/3EE;->e:LX/04D;

    .line 537124
    return-void
.end method
