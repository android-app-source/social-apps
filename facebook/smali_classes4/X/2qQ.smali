.class public final LX/2qQ;
.super LX/1AD;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1AD",
        "<",
        "Lcom/facebook/video/server/VideoServerEvents$NetworkAccessRequestedEvent$Handler;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:J

.field public final b:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 470913
    invoke-direct {p0}, LX/1AD;-><init>()V

    .line 470914
    iput-wide p1, p0, LX/2qQ;->a:J

    .line 470915
    iput-wide p3, p0, LX/2qQ;->b:J

    .line 470916
    return-void
.end method


# virtual methods
.method public final a(LX/16Y;)V
    .locals 4

    .prologue
    .line 470917
    check-cast p1, LX/2py;

    .line 470918
    iget-object v0, p1, LX/2py;->b:LX/11o;

    if-eqz v0, :cond_0

    .line 470919
    iget v0, p1, LX/2py;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, LX/2py;->e:I

    .line 470920
    iget-wide v0, p0, LX/2qQ;->a:J

    iget-wide v2, p0, LX/2qQ;->b:J

    invoke-static {v0, v1, v2, v3}, LX/2py;->a(JJ)LX/0P1;

    move-result-object v0

    .line 470921
    const-string v1, "NetworkAccess"

    invoke-static {p1, v1, v0}, LX/2py;->a(LX/2py;Ljava/lang/String;LX/0P1;)V

    .line 470922
    const-string v1, "NetworkAccessHeader"

    invoke-static {p1, v1, v0}, LX/2py;->a(LX/2py;Ljava/lang/String;LX/0P1;)V

    .line 470923
    :cond_0
    return-void
.end method
