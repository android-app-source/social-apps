.class public final LX/39Q;
.super LX/2uF;
.source ""


# instance fields
.field private final transient a:I

.field private final transient b:I

.field private final transient c:LX/3Sd;


# direct methods
.method public constructor <init>(LX/3Sd;)V
    .locals 2

    .prologue
    .line 522943
    const/4 v0, 0x0

    iget v1, p1, LX/3Sd;->b:I

    invoke-direct {p0, p1, v0, v1}, LX/39Q;-><init>(LX/3Sd;II)V

    .line 522944
    return-void
.end method

.method private constructor <init>(LX/3Sd;II)V
    .locals 0

    .prologue
    .line 522938
    invoke-direct {p0}, LX/2uF;-><init>()V

    .line 522939
    iput p2, p0, LX/39Q;->a:I

    .line 522940
    iput p3, p0, LX/39Q;->b:I

    .line 522941
    iput-object p1, p0, LX/39Q;->c:LX/3Sd;

    .line 522942
    return-void
.end method


# virtual methods
.method public final a(LX/3Sd;I)I
    .locals 3

    .prologue
    .line 522912
    iget-object v0, p0, LX/39Q;->c:LX/3Sd;

    iget v1, p0, LX/39Q;->a:I

    iget v2, p0, LX/39Q;->b:I

    invoke-static {v0, v1, p1, p2, v2}, LX/3Sd;->a(LX/3Sd;ILX/3Sd;II)V

    .line 522913
    iget v0, p0, LX/39Q;->b:I

    add-int/2addr v0, p2

    return v0
.end method

.method public final a(I)LX/1vs;
    .locals 5

    .prologue
    .line 522928
    iget v0, p0, LX/39Q;->b:I

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 522929
    iget v0, p0, LX/39Q;->a:I

    add-int/2addr v0, p1

    .line 522930
    iget-object v1, p0, LX/39Q;->c:LX/3Sd;

    .line 522931
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 522932
    :try_start_0
    invoke-virtual {v1, v0}, LX/3Sd;->b(I)LX/15i;

    move-result-object v3

    .line 522933
    invoke-virtual {v1, v0}, LX/3Sd;->c(I)I

    move-result v4

    .line 522934
    invoke-virtual {v1, v0}, LX/3Sd;->d(I)I

    move-result v0

    .line 522935
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 522936
    invoke-static {v3, v4, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    return-object v0

    .line 522937
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(II)LX/2uF;
    .locals 4

    .prologue
    .line 522927
    new-instance v0, LX/39Q;

    iget-object v1, p0, LX/39Q;->c:LX/3Sd;

    iget v2, p0, LX/39Q;->a:I

    add-int/2addr v2, p1

    sub-int v3, p2, p1

    invoke-direct {v0, v1, v2, v3}, LX/39Q;-><init>(LX/3Sd;II)V

    return-object v0
.end method

.method public final b(I)LX/3Sg;
    .locals 4

    .prologue
    .line 522916
    iget-object v0, p0, LX/39Q;->c:LX/3Sd;

    iget v1, p0, LX/39Q;->a:I

    iget v2, p0, LX/39Q;->b:I

    .line 522917
    if-ltz v2, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 522918
    add-int v3, v1, v2

    .line 522919
    iget p0, v0, LX/3Sd;->b:I

    invoke-static {v1, v3, p0}, LX/0PB;->checkPositionIndexes(III)V

    .line 522920
    invoke-static {p1, v2}, LX/0PB;->checkPositionIndex(II)I

    .line 522921
    if-nez v2, :cond_1

    .line 522922
    sget-object v3, LX/3Se;->a:LX/3Sg;

    check-cast v3, LX/3Sg;

    move-object v3, v3

    .line 522923
    :goto_1
    move-object v0, v3

    .line 522924
    return-object v0

    .line 522925
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 522926
    :cond_1
    new-instance v3, LX/3Sn;

    invoke-direct {v3, v2, p1, v1, v0}, LX/3Sn;-><init>(IIILX/3Sd;)V

    goto :goto_1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 522915
    iget v0, p0, LX/39Q;->b:I

    return v0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 522914
    iget v0, p0, LX/39Q;->b:I

    iget-object v1, p0, LX/39Q;->c:LX/3Sd;

    iget v1, v1, LX/3Sd;->b:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
