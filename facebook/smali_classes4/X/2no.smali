.class public LX/2no;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2lr;


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field private final c:J

.field public final d:Ljava/lang/String;

.field public e:Z

.field public f:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 9

    .prologue
    .line 465153
    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v7}, LX/2no;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 465154
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 465145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465146
    iput-object p1, p0, LX/2no;->a:Ljava/lang/String;

    .line 465147
    iput-object p2, p0, LX/2no;->b:Ljava/lang/String;

    .line 465148
    iput-wide p3, p0, LX/2no;->c:J

    .line 465149
    iput-object p5, p0, LX/2no;->d:Ljava/lang/String;

    .line 465150
    iput-boolean p6, p0, LX/2no;->e:Z

    .line 465151
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2no;->f:Z

    .line 465152
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 465155
    iget-wide v0, p0, LX/2no;->c:J

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 465144
    iget-object v0, p0, LX/2no;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 465130
    if-ne p0, p1, :cond_1

    .line 465131
    :cond_0
    :goto_0
    return v0

    .line 465132
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 465133
    goto :goto_0

    .line 465134
    :cond_3
    check-cast p1, LX/2no;

    .line 465135
    iget-object v2, p0, LX/2no;->a:Ljava/lang/String;

    invoke-virtual {p1}, LX/2no;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/2no;->b:Ljava/lang/String;

    .line 465136
    iget-object v3, p1, LX/2no;->b:Ljava/lang/String;

    move-object v3, v3

    .line 465137
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, p0, LX/2no;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1}, LX/2no;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/2no;->d:Ljava/lang/String;

    .line 465138
    iget-object v3, p1, LX/2no;->d:Ljava/lang/String;

    move-object v3, v3

    .line 465139
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, LX/2no;->e:Z

    .line 465140
    iget-boolean v3, p1, LX/2no;->e:Z

    move v3, v3

    .line 465141
    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, LX/2no;->f:Z

    .line 465142
    iget-boolean v3, p1, LX/2no;->f:Z

    move v3, v3

    .line 465143
    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 465129
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/2no;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/2no;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, LX/2no;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/2no;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, LX/2no;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, LX/2no;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
