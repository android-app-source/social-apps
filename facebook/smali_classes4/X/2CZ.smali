.class public LX/2CZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/2CZ;


# instance fields
.field public final a:LX/0kd;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0Uh;

.field private final d:LX/0WP;

.field private final e:LX/03V;

.field private final f:LX/0Zm;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/2DN;

.field private i:J

.field public volatile j:Z

.field public k:LX/0WS;


# direct methods
.method public constructor <init>(LX/0kd;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0WP;LX/03V;LX/0Zm;LX/0Or;LX/2DN;)V
    .locals 2
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0kd;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0WP;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/analytics/logger/AnalyticsConfig;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2DN;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 382989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 382990
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2CZ;->j:Z

    .line 382991
    iput-object p2, p0, LX/2CZ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 382992
    iput-object p3, p0, LX/2CZ;->c:LX/0Uh;

    .line 382993
    iput-object p4, p0, LX/2CZ;->d:LX/0WP;

    .line 382994
    iput-object p1, p0, LX/2CZ;->a:LX/0kd;

    .line 382995
    iput-object p5, p0, LX/2CZ;->e:LX/03V;

    .line 382996
    iput-object p6, p0, LX/2CZ;->f:LX/0Zm;

    .line 382997
    iput-object p7, p0, LX/2CZ;->g:LX/0Or;

    .line 382998
    iput-object p8, p0, LX/2CZ;->h:LX/2DN;

    .line 382999
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/2CZ;->i:J

    .line 383000
    return-void
.end method

.method public static a(LX/0QB;)LX/2CZ;
    .locals 12

    .prologue
    .line 382976
    sget-object v0, LX/2CZ;->l:LX/2CZ;

    if-nez v0, :cond_1

    .line 382977
    const-class v1, LX/2CZ;

    monitor-enter v1

    .line 382978
    :try_start_0
    sget-object v0, LX/2CZ;->l:LX/2CZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 382979
    if-eqz v2, :cond_0

    .line 382980
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 382981
    new-instance v3, LX/2CZ;

    invoke-static {v0}, LX/0kd;->a(LX/0QB;)LX/0kd;

    move-result-object v4

    check-cast v4, LX/0kd;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, LX/0WL;->a(LX/0QB;)LX/0WP;

    move-result-object v7

    check-cast v7, LX/0WP;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v9

    check-cast v9, LX/0Zm;

    const/16 v10, 0x15e7

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v0}, LX/2DM;->a(LX/0QB;)LX/2DM;

    move-result-object v11

    check-cast v11, LX/2DN;

    invoke-direct/range {v3 .. v11}, LX/2CZ;-><init>(LX/0kd;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0WP;LX/03V;LX/0Zm;LX/0Or;LX/2DN;)V

    .line 382982
    move-object v0, v3

    .line 382983
    sput-object v0, LX/2CZ;->l:LX/2CZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382984
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 382985
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 382986
    :cond_1
    sget-object v0, LX/2CZ;->l:LX/2CZ;

    return-object v0

    .line 382987
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 382988
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2CZ;JLjava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/analytics/HoneyAnalyticsEvent;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 382965
    invoke-static {p0}, LX/2CZ;->e(LX/2CZ;)J

    move-result-wide v0

    .line 382966
    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    .line 382967
    const/4 v0, 0x0

    .line 382968
    :goto_0
    return-object v0

    .line 382969
    :cond_0
    new-instance v2, LX/2Ew;

    invoke-direct {v2, p0}, LX/2Ew;-><init>(LX/2CZ;)V

    .line 382970
    :try_start_0
    iget-object v0, p0, LX/2CZ;->h:LX/2DN;

    move-object v1, p0

    move-object v3, p3

    move-wide v4, p1

    invoke-interface/range {v0 .. v5}, LX/2DN;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;J)V

    .line 382971
    iget-object v0, v2, LX/2Ew;->b:LX/0Pz;

    if-eqz v0, :cond_1

    iget-object v0, v2, LX/2Ew;->b:LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    :goto_1
    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382972
    invoke-static {v2}, LX/2Ew;->b$redex0(LX/2Ew;)V

    .line 382973
    invoke-static {v2}, LX/2Ew;->c$redex0(LX/2Ew;)J

    move-result-wide v2

    invoke-static {p0, v2, v3}, LX/2CZ;->b(LX/2CZ;J)V

    goto :goto_0

    .line 382974
    :catchall_0
    move-exception v0

    invoke-static {v2}, LX/2Ew;->b$redex0(LX/2Ew;)V

    .line 382975
    invoke-static {v2}, LX/2Ew;->c$redex0(LX/2Ew;)J

    move-result-wide v2

    invoke-static {p0, v2, v3}, LX/2CZ;->b(LX/2CZ;J)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static declared-synchronized b(LX/2CZ;J)V
    .locals 1

    .prologue
    .line 382923
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, LX/2CZ;->i:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382924
    monitor-exit p0

    return-void

    .line 382925
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized e(LX/2CZ;)J
    .locals 2

    .prologue
    .line 382964
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/2CZ;->i:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;JJZLjava/lang/String;)J
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 382959
    invoke-virtual {p0}, LX/2CZ;->b()LX/0WS;

    move-result-object v0

    invoke-virtual {v0, p1, v2, v3}, LX/0WS;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 382960
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 382961
    :goto_0
    return-wide p4

    .line 382962
    :cond_0
    invoke-virtual {p0, p7, p2, p3, p6}, LX/2CZ;->a(Ljava/lang/String;JZ)J

    move-result-wide v2

    .line 382963
    add-long p4, v0, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;JZ)J
    .locals 8

    .prologue
    .line 382954
    if-eqz p4, :cond_1

    const-wide/32 v0, 0x36ee80

    .line 382955
    :goto_0
    cmp-long v2, p2, v0

    if-gez v2, :cond_0

    .line 382956
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "Requested time interval of %d ms should be increased to at least %d ms for %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x2

    aput-object p1, v4, v0

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 382957
    :cond_0
    return-wide p2

    .line 382958
    :cond_1
    const-wide/32 v0, 0xdbba0

    goto :goto_0
.end method

.method public final a(LX/2F1;JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 382949
    :try_start_0
    invoke-interface {p1, p2, p3, p4}, LX/2F1;->a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 382950
    :goto_0
    return-object v0

    .line 382951
    :catch_0
    move-exception v0

    .line 382952
    iget-object v1, p0, LX/2CZ;->e:LX/03V;

    const-string v2, "client_side_periodic_reporter_throw"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 382953
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 382947
    const-wide/16 v0, -0x1

    invoke-static {p0, v0, v1}, LX/2CZ;->b(LX/2CZ;J)V

    .line 382948
    return-void
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 382946
    iget-object v0, p0, LX/2CZ;->f:LX/0Zm;

    invoke-virtual {v0, p1, p2}, LX/0Zm;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized b()LX/0WS;
    .locals 11
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 382928
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2CZ;->k:LX/0WS;

    if-nez v0, :cond_2

    .line 382929
    iget-object v0, p0, LX/2CZ;->d:LX/0WP;

    const-string v1, "analytics_periodic_events"

    invoke-virtual {v0, v1}, LX/0WP;->a(Ljava/lang/String;)LX/0WS;

    move-result-object v0

    iput-object v0, p0, LX/2CZ;->k:LX/0WS;

    .line 382930
    iget-object v0, p0, LX/2CZ;->k:LX/0WS;

    const-string v1, "client_periodic_lightprefs_migration"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 382931
    iget-object v0, p0, LX/2CZ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 382932
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Unexpected race with the shared preferences store!"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382933
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/2CZ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->c()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382934
    :cond_0
    :try_start_2
    iget-object v3, p0, LX/2CZ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0uQ;->j:LX/0Tn;

    invoke-interface {v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v3

    .line 382935
    iget-object v4, p0, LX/2CZ;->k:LX/0WS;

    invoke-virtual {v4}, LX/0WS;->b()LX/1gW;

    move-result-object v4

    .line 382936
    iget-object v5, p0, LX/2CZ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    .line 382937
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Tn;

    .line 382938
    sget-object v7, LX/0uQ;->j:LX/0Tn;

    invoke-virtual {v3, v7}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object v7

    .line 382939
    iget-object v8, p0, LX/2CZ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v9, 0x0

    invoke-interface {v8, v3, v9, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v9

    invoke-interface {v4, v7, v9, v10}, LX/1gW;->a(Ljava/lang/String;J)LX/1gW;

    .line 382940
    invoke-interface {v5, v3}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    goto :goto_1

    .line 382941
    :cond_1
    const-string v3, "client_periodic_lightprefs_migration"

    const/4 v6, 0x1

    invoke-interface {v4, v3, v6}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    move-result-object v3

    invoke-interface {v3}, LX/1gW;->b()Z

    .line 382942
    invoke-interface {v5}, LX/0hN;->commit()V

    .line 382943
    :cond_2
    iget-object v0, p0, LX/2CZ;->k:LX/0WS;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 382944
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 382945
    :catch_0
    goto :goto_0
.end method

.method public final init()V
    .locals 1

    .prologue
    .line 382926
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2CZ;->j:Z

    .line 382927
    return-void
.end method
