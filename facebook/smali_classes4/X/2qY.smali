.class public final enum LX/2qY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2qY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2qY;

.field public static final enum DOWNLOAD:LX/2qY;

.field public static final enum DOWNLOAD_TO_FACEBOOK:LX/2qY;

.field public static final enum SAVE_OFFLINE:LX/2qY;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 471343
    new-instance v0, LX/2qY;

    const-string v1, "DOWNLOAD_TO_FACEBOOK"

    invoke-direct {v0, v1, v2}, LX/2qY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qY;->DOWNLOAD_TO_FACEBOOK:LX/2qY;

    .line 471344
    new-instance v0, LX/2qY;

    const-string v1, "DOWNLOAD"

    invoke-direct {v0, v1, v3}, LX/2qY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qY;->DOWNLOAD:LX/2qY;

    .line 471345
    new-instance v0, LX/2qY;

    const-string v1, "SAVE_OFFLINE"

    invoke-direct {v0, v1, v4}, LX/2qY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qY;->SAVE_OFFLINE:LX/2qY;

    .line 471346
    const/4 v0, 0x3

    new-array v0, v0, [LX/2qY;

    sget-object v1, LX/2qY;->DOWNLOAD_TO_FACEBOOK:LX/2qY;

    aput-object v1, v0, v2

    sget-object v1, LX/2qY;->DOWNLOAD:LX/2qY;

    aput-object v1, v0, v3

    sget-object v1, LX/2qY;->SAVE_OFFLINE:LX/2qY;

    aput-object v1, v0, v4

    sput-object v0, LX/2qY;->$VALUES:[LX/2qY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 471340
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static of(Ljava/lang/String;)LX/2qY;
    .locals 1

    .prologue
    .line 471341
    :try_start_0
    invoke-static {p0}, LX/2qY;->valueOf(Ljava/lang/String;)LX/2qY;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 471342
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, LX/2qY;->DOWNLOAD_TO_FACEBOOK:LX/2qY;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/2qY;
    .locals 1

    .prologue
    .line 471339
    const-class v0, LX/2qY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2qY;

    return-object v0
.end method

.method public static values()[LX/2qY;
    .locals 1

    .prologue
    .line 471338
    sget-object v0, LX/2qY;->$VALUES:[LX/2qY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2qY;

    return-object v0
.end method
