.class public LX/2Dc;
.super Ljava/io/Writer;
.source ""


# instance fields
.field private a:Ljava/io/Writer;

.field private b:[C

.field private c:I


# direct methods
.method public constructor <init>(Ljava/io/Writer;[C)V
    .locals 0

    .prologue
    .line 384399
    invoke-direct {p0, p1}, Ljava/io/Writer;-><init>(Ljava/lang/Object;)V

    .line 384400
    iput-object p1, p0, LX/2Dc;->a:Ljava/io/Writer;

    .line 384401
    iput-object p2, p0, LX/2Dc;->b:[C

    .line 384402
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 384396
    invoke-direct {p0}, LX/2Dc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384397
    new-instance v0, Ljava/io/IOException;

    const-string v1, "BufferedWriter is closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384398
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Throwable;",
            ">(",
            "Ljava/lang/Throwable;",
            ")V^TT;"
        }
    .end annotation

    .prologue
    .line 384395
    throw p0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 384391
    iget v0, p0, LX/2Dc;->c:I

    if-lez v0, :cond_0

    .line 384392
    iget-object v0, p0, LX/2Dc;->a:Ljava/io/Writer;

    iget-object v1, p0, LX/2Dc;->b:[C

    iget v2, p0, LX/2Dc;->c:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/Writer;->write([CII)V

    .line 384393
    :cond_0
    iput v3, p0, LX/2Dc;->c:I

    .line 384394
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 384390
    iget-object v0, p0, LX/2Dc;->a:Ljava/io/Writer;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final close()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 384403
    iget-object v2, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 384404
    :try_start_0
    invoke-direct {p0}, LX/2Dc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384405
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384406
    :goto_0
    return-void

    .line 384407
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/2Dc;->b()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384408
    :goto_1
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, LX/2Dc;->b:[C
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 384409
    :try_start_3
    iget-object v0, p0, LX/2Dc;->a:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v0, v1

    .line 384410
    :cond_1
    :goto_2
    const/4 v1, 0x0

    :try_start_4
    iput-object v1, p0, LX/2Dc;->a:Ljava/io/Writer;

    .line 384411
    if-eqz v0, :cond_2

    .line 384412
    invoke-static {v0}, LX/2Dc;->a(Ljava/lang/Throwable;)V

    .line 384413
    :cond_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 384414
    :catch_0
    move-exception v0

    .line 384415
    if-eqz v1, :cond_1

    move-object v0, v1

    goto :goto_2

    .line 384416
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public final flush()V
    .locals 2

    .prologue
    .line 384385
    iget-object v1, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 384386
    :try_start_0
    invoke-direct {p0}, LX/2Dc;->a()V

    .line 384387
    invoke-direct {p0}, LX/2Dc;->b()V

    .line 384388
    iget-object v0, p0, LX/2Dc;->a:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    .line 384389
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final write(I)V
    .locals 5

    .prologue
    .line 384378
    iget-object v1, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 384379
    :try_start_0
    invoke-direct {p0}, LX/2Dc;->a()V

    .line 384380
    iget v0, p0, LX/2Dc;->c:I

    iget-object v2, p0, LX/2Dc;->b:[C

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 384381
    iget-object v0, p0, LX/2Dc;->a:Ljava/io/Writer;

    iget-object v2, p0, LX/2Dc;->b:[C

    const/4 v3, 0x0

    iget-object v4, p0, LX/2Dc;->b:[C

    array-length v4, v4

    invoke-virtual {v0, v2, v3, v4}, Ljava/io/Writer;->write([CII)V

    .line 384382
    const/4 v0, 0x0

    iput v0, p0, LX/2Dc;->c:I

    .line 384383
    :cond_0
    iget-object v0, p0, LX/2Dc;->b:[C

    iget v2, p0, LX/2Dc;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/2Dc;->c:I

    int-to-char v3, p1

    aput-char v3, v0, v2

    .line 384384
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final write(Ljava/lang/String;II)V
    .locals 6

    .prologue
    .line 384346
    iget-object v1, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 384347
    :try_start_0
    invoke-direct {p0}, LX/2Dc;->a()V

    .line 384348
    if-gtz p3, :cond_0

    .line 384349
    monitor-exit v1

    .line 384350
    :goto_0
    return-void

    .line 384351
    :cond_0
    if-ltz p2, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, p3

    if-le p2, v0, :cond_2

    .line 384352
    :cond_1
    invoke-static {p1, p2, p3}, LX/2Df;->a(Ljava/lang/String;II)Ljava/lang/StringIndexOutOfBoundsException;

    move-result-object v0

    throw v0

    .line 384353
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 384354
    :cond_2
    :try_start_1
    iget v0, p0, LX/2Dc;->c:I

    if-nez v0, :cond_3

    iget-object v0, p0, LX/2Dc;->b:[C

    array-length v0, v0

    if-lt p3, v0, :cond_3

    .line 384355
    new-array v0, p3, [C

    .line 384356
    add-int v2, p2, p3

    const/4 v3, 0x0

    invoke-virtual {p1, p2, v2, v0, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 384357
    iget-object v2, p0, LX/2Dc;->a:Ljava/io/Writer;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, p3}, Ljava/io/Writer;->write([CII)V

    .line 384358
    monitor-exit v1

    goto :goto_0

    .line 384359
    :cond_3
    iget-object v0, p0, LX/2Dc;->b:[C

    array-length v0, v0

    iget v2, p0, LX/2Dc;->c:I

    sub-int/2addr v0, v2

    .line 384360
    if-ge p3, v0, :cond_4

    move v0, p3

    .line 384361
    :cond_4
    if-lez v0, :cond_5

    .line 384362
    add-int v2, p2, v0

    iget-object v3, p0, LX/2Dc;->b:[C

    iget v4, p0, LX/2Dc;->c:I

    invoke-virtual {p1, p2, v2, v3, v4}, Ljava/lang/String;->getChars(II[CI)V

    .line 384363
    iget v2, p0, LX/2Dc;->c:I

    add-int/2addr v2, v0

    iput v2, p0, LX/2Dc;->c:I

    .line 384364
    :cond_5
    iget v2, p0, LX/2Dc;->c:I

    iget-object v3, p0, LX/2Dc;->b:[C

    array-length v3, v3

    if-ne v2, v3, :cond_7

    .line 384365
    iget-object v2, p0, LX/2Dc;->a:Ljava/io/Writer;

    iget-object v3, p0, LX/2Dc;->b:[C

    const/4 v4, 0x0

    iget-object v5, p0, LX/2Dc;->b:[C

    array-length v5, v5

    invoke-virtual {v2, v3, v4, v5}, Ljava/io/Writer;->write([CII)V

    .line 384366
    const/4 v2, 0x0

    iput v2, p0, LX/2Dc;->c:I

    .line 384367
    if-le p3, v0, :cond_7

    .line 384368
    add-int v2, p2, v0

    .line 384369
    sub-int v0, p3, v0

    .line 384370
    iget-object v3, p0, LX/2Dc;->b:[C

    array-length v3, v3

    if-lt v0, v3, :cond_6

    .line 384371
    new-array v3, p3, [C

    .line 384372
    add-int v4, v2, v0

    const/4 v5, 0x0

    invoke-virtual {p1, v2, v4, v3, v5}, Ljava/lang/String;->getChars(II[CI)V

    .line 384373
    iget-object v2, p0, LX/2Dc;->a:Ljava/io/Writer;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v0}, Ljava/io/Writer;->write([CII)V

    .line 384374
    monitor-exit v1

    goto :goto_0

    .line 384375
    :cond_6
    add-int v3, v2, v0

    iget-object v4, p0, LX/2Dc;->b:[C

    iget v5, p0, LX/2Dc;->c:I

    invoke-virtual {p1, v2, v3, v4, v5}, Ljava/lang/String;->getChars(II[CI)V

    .line 384376
    iget v2, p0, LX/2Dc;->c:I

    add-int/2addr v0, v2

    iput v0, p0, LX/2Dc;->c:I

    .line 384377
    :cond_7
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public final write([CII)V
    .locals 6

    .prologue
    .line 384319
    iget-object v1, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 384320
    :try_start_0
    invoke-direct {p0}, LX/2Dc;->a()V

    .line 384321
    if-nez p1, :cond_0

    .line 384322
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "buffer == null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384323
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 384324
    :cond_0
    :try_start_1
    array-length v0, p1

    invoke-static {v0, p2, p3}, LX/2Df;->a(III)V

    .line 384325
    iget v0, p0, LX/2Dc;->c:I

    if-nez v0, :cond_1

    iget-object v0, p0, LX/2Dc;->b:[C

    array-length v0, v0

    if-lt p3, v0, :cond_1

    .line 384326
    iget-object v0, p0, LX/2Dc;->a:Ljava/io/Writer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/Writer;->write([CII)V

    .line 384327
    monitor-exit v1

    .line 384328
    :goto_0
    return-void

    .line 384329
    :cond_1
    iget-object v0, p0, LX/2Dc;->b:[C

    array-length v0, v0

    iget v2, p0, LX/2Dc;->c:I

    sub-int/2addr v0, v2

    .line 384330
    if-ge p3, v0, :cond_2

    move v0, p3

    .line 384331
    :cond_2
    if-lez v0, :cond_3

    .line 384332
    iget-object v2, p0, LX/2Dc;->b:[C

    iget v3, p0, LX/2Dc;->c:I

    invoke-static {p1, p2, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 384333
    iget v2, p0, LX/2Dc;->c:I

    add-int/2addr v2, v0

    iput v2, p0, LX/2Dc;->c:I

    .line 384334
    :cond_3
    iget v2, p0, LX/2Dc;->c:I

    iget-object v3, p0, LX/2Dc;->b:[C

    array-length v3, v3

    if-ne v2, v3, :cond_5

    .line 384335
    iget-object v2, p0, LX/2Dc;->a:Ljava/io/Writer;

    iget-object v3, p0, LX/2Dc;->b:[C

    const/4 v4, 0x0

    iget-object v5, p0, LX/2Dc;->b:[C

    array-length v5, v5

    invoke-virtual {v2, v3, v4, v5}, Ljava/io/Writer;->write([CII)V

    .line 384336
    const/4 v2, 0x0

    iput v2, p0, LX/2Dc;->c:I

    .line 384337
    if-le p3, v0, :cond_5

    .line 384338
    add-int v2, p2, v0

    .line 384339
    sub-int v0, p3, v0

    .line 384340
    iget-object v3, p0, LX/2Dc;->b:[C

    array-length v3, v3

    if-lt v0, v3, :cond_4

    .line 384341
    iget-object v3, p0, LX/2Dc;->a:Ljava/io/Writer;

    invoke-virtual {v3, p1, v2, v0}, Ljava/io/Writer;->write([CII)V

    .line 384342
    monitor-exit v1

    goto :goto_0

    .line 384343
    :cond_4
    iget-object v3, p0, LX/2Dc;->b:[C

    iget v4, p0, LX/2Dc;->c:I

    invoke-static {p1, v2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 384344
    iget v2, p0, LX/2Dc;->c:I

    add-int/2addr v0, v2

    iput v0, p0, LX/2Dc;->c:I

    .line 384345
    :cond_5
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
