.class public abstract LX/2AE;
.super LX/2AF;
.source ""

# interfaces
.implements LX/0rf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/2AF",
        "<TK;TV;>;",
        "LX/0rf;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:LX/0rb;

.field private final d:LX/03V;


# direct methods
.method public constructor <init>(Ljava/util/Comparator;LX/0Sh;Ljava/lang/String;Ljava/lang/String;LX/0rb;LX/03V;)V
    .locals 4
    .param p1    # Ljava/util/Comparator;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<TV;>;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0rb;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 377051
    invoke-direct {p0, p1, p2}, LX/2AF;-><init>(Ljava/util/Comparator;LX/0Sh;)V

    .line 377052
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Can\'t have null or empty name"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 377053
    iput-object p3, p0, LX/2AE;->a:Ljava/lang/String;

    .line 377054
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Can\'t have null or empty id"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 377055
    iput-object p4, p0, LX/2AE;->b:Ljava/lang/String;

    .line 377056
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rb;

    iput-object v0, p0, LX/2AE;->c:LX/0rb;

    .line 377057
    iput-object p6, p0, LX/2AE;->d:LX/03V;

    .line 377058
    return-void

    :cond_0
    move v0, v2

    .line 377059
    goto :goto_0

    :cond_1
    move v1, v2

    .line 377060
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/32G;)V
    .locals 0

    .prologue
    .line 377061
    invoke-virtual {p0}, LX/2AG;->d()V

    .line 377062
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 377063
    invoke-super {p0}, LX/2AF;->e()V

    .line 377064
    invoke-virtual {p0}, LX/2AG;->c()I

    move-result v0

    if-lez v0, :cond_0

    .line 377065
    iget-object v0, p0, LX/2AE;->d:LX/03V;

    iget-object v1, p0, LX/2AE;->b:Ljava/lang/String;

    invoke-virtual {p0}, LX/2AG;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 377066
    :goto_0
    return-void

    .line 377067
    :cond_0
    iget-object v0, p0, LX/2AE;->d:LX/03V;

    iget-object v1, p0, LX/2AE;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/03V;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 377068
    iget-object v0, p0, LX/2AE;->c:LX/0rb;

    invoke-interface {v0, p0}, LX/0rb;->a(LX/0rf;)V

    .line 377069
    return-void
.end method
