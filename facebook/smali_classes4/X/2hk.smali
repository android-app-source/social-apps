.class public LX/2hk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2hi;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2hk;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 450540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2hk;
    .locals 3

    .prologue
    .line 450541
    sget-object v0, LX/2hk;->a:LX/2hk;

    if-nez v0, :cond_1

    .line 450542
    const-class v1, LX/2hk;

    monitor-enter v1

    .line 450543
    :try_start_0
    sget-object v0, LX/2hk;->a:LX/2hk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 450544
    if-eqz v2, :cond_0

    .line 450545
    :try_start_1
    new-instance v0, LX/2hk;

    invoke-direct {v0}, LX/2hk;-><init>()V

    .line 450546
    move-object v0, v0

    .line 450547
    sput-object v0, LX/2hk;->a:LX/2hk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 450548
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 450549
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 450550
    :cond_1
    sget-object v0, LX/2hk;->a:LX/2hk;

    return-object v0

    .line 450551
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 450552
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 450553
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->ANDROID_FOOTER:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    const-class v2, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->TOAST_FOOTER:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    const-class v2, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->ANDROID_MESSAGES_INTERSTITIAL_2_BUTTON:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    const-class v2, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->INTERSTITIAL_1_BUTTON_X:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    const-class v2, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->INTERSTITIAL_2_BUTTON:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    const-class v2, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->CARD:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    const-class v2, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->CARD_WITH_HEADER:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    const-class v2, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->FIG_DIALOG:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    const-class v2, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->MESSENGER_CARD:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    const-class v2, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->MESSENGER_CARD_NO_BADGE:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    const-class v2, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method
