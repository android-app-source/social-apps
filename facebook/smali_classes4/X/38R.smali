.class public final LX/38R;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 520978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 520979
    return-void
.end method

.method public constructor <init>(LX/38T;)V
    .locals 2
    .param p1    # LX/38T;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 520980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 520981
    if-nez p1, :cond_0

    .line 520982
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520983
    :cond_0
    invoke-static {p1}, LX/38T;->e(LX/38T;)V

    .line 520984
    iget-object v0, p1, LX/38T;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 520985
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, LX/38T;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/38R;->a:Ljava/util/ArrayList;

    .line 520986
    :cond_1
    return-void
.end method

.method private a(Ljava/util/Collection;)LX/38R;
    .locals 2
    .param p1    # Ljava/util/Collection;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/38R;"
        }
    .end annotation

    .prologue
    .line 520987
    if-nez p1, :cond_0

    .line 520988
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "categories must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520989
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 520990
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 520991
    invoke-virtual {p0, v0}, LX/38R;->a(Ljava/lang/String;)LX/38R;

    goto :goto_0

    .line 520992
    :cond_1
    return-object p0
.end method


# virtual methods
.method public final a(LX/38T;)LX/38R;
    .locals 2
    .param p1    # LX/38T;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 520993
    if-nez p1, :cond_0

    .line 520994
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520995
    :cond_0
    invoke-virtual {p1}, LX/38T;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, LX/38R;->a(Ljava/util/Collection;)LX/38R;

    .line 520996
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/38R;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 520997
    if-nez p1, :cond_0

    .line 520998
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "category must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520999
    :cond_0
    iget-object v0, p0, LX/38R;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 521000
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/38R;->a:Ljava/util/ArrayList;

    .line 521001
    :cond_1
    iget-object v0, p0, LX/38R;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 521002
    iget-object v0, p0, LX/38R;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 521003
    :cond_2
    return-object p0
.end method

.method public final a()LX/38T;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 521004
    iget-object v0, p0, LX/38R;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 521005
    sget-object v0, LX/38T;->a:LX/38T;

    .line 521006
    :goto_0
    return-object v0

    .line 521007
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 521008
    const-string v0, "controlCategories"

    iget-object v2, p0, LX/38R;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 521009
    new-instance v0, LX/38T;

    iget-object v2, p0, LX/38R;->a:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, LX/38T;-><init>(Landroid/os/Bundle;Ljava/util/List;)V

    goto :goto_0
.end method
