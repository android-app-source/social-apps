.class public LX/2eo;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 445707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1Fa;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 3

    .prologue
    .line 445708
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem;

    if-eqz v0, :cond_0

    .line 445709
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem;

    .line 445710
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    move-object v0, v0

    .line 445711
    :goto_0
    return-object v0

    .line 445712
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    if-eqz v0, :cond_1

    .line 445713
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    .line 445714
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    move-object v0, v0

    .line 445715
    goto :goto_0

    .line 445716
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Calling getFriendshipStatus with wrong type"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
