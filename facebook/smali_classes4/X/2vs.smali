.class public final LX/2vs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<O::",
        "LX/2w7;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/2vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vq",
            "<*TO;>;"
        }
    .end annotation
.end field

.field private final b:LX/3KF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3KF",
            "<*TO;>;"
        }
    .end annotation
.end field

.field private final c:LX/2vn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vn",
            "<*>;"
        }
    .end annotation
.end field

.field private final d:LX/3KP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3KP",
            "<*>;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/2vq;LX/2vn;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "LX/2wJ;",
            ">(",
            "Ljava/lang/String;",
            "LX/2vq",
            "<TC;TO;>;",
            "LX/2vn",
            "<TC;>;)V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Cannot construct an Api with a null ClientBuilder"

    invoke-static {p2, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Cannot construct an Api with a null ClientKey"

    invoke-static {p3, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, LX/2vs;->e:Ljava/lang/String;

    iput-object p2, p0, LX/2vs;->a:LX/2vq;

    iput-object v1, p0, LX/2vs;->b:LX/3KF;

    iput-object p3, p0, LX/2vs;->c:LX/2vn;

    iput-object v1, p0, LX/2vs;->d:LX/3KP;

    return-void
.end method


# virtual methods
.method public final a()LX/2vr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2vr",
            "<*TO;>;"
        }
    .end annotation

    goto :goto_1

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, LX/2vs;->a:LX/2vq;

    goto :goto_0
.end method

.method public final b()LX/2vq;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2vq",
            "<*TO;>;"
        }
    .end annotation

    iget-object v0, p0, LX/2vs;->a:LX/2vq;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder"

    invoke-static {v0, v1}, LX/1ol;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, LX/2vs;->a:LX/2vq;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/2vo;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2vo",
            "<*>;"
        }
    .end annotation

    iget-object v0, p0, LX/2vs;->c:LX/2vn;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2vs;->c:LX/2vn;

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This API was constructed with null client keys. This should not be possible."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
