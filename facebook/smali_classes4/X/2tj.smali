.class public LX/2tj;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field private final b:LX/0iA;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 475208
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEEDBACK_COMPOSER_INIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/2tj;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/0iA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 475176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 475177
    iput-object p1, p0, LX/2tj;->b:LX/0iA;

    .line 475178
    return-void
.end method

.method public static a(LX/0QB;)LX/2tj;
    .locals 2

    .prologue
    .line 475205
    new-instance v1, LX/2tj;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v0

    check-cast v0, LX/0iA;

    invoke-direct {v1, v0}, LX/2tj;-><init>(LX/0iA;)V

    .line 475206
    move-object v0, v1

    .line 475207
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 475199
    iget-object v0, p0, LX/2tj;->b:LX/0iA;

    const-string v1, "4181"

    const-class v2, LX/3kz;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kz;

    .line 475200
    if-eqz v0, :cond_0

    .line 475201
    invoke-static {p1}, Lcom/facebook/feedback/ui/CommentComposerHelper;->d(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v1

    .line 475202
    iput-boolean v1, v0, LX/3kz;->g:Z

    .line 475203
    iput-object p2, v0, LX/3kz;->h:Landroid/view/View;

    .line 475204
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 475184
    iget-object v0, p0, LX/2tj;->b:LX/0iA;

    sget-object v1, LX/2tj;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3kz;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kz;

    .line 475185
    if-eqz v0, :cond_0

    .line 475186
    iget-object v1, p0, LX/2tj;->b:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    invoke-virtual {v0}, LX/3kz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 475187
    iget-object v1, v0, LX/3kz;->f:LX/0hs;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/3kz;->e:Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    .line 475188
    :goto_0
    iget-object v1, v0, LX/3kz;->c:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->a()V

    .line 475189
    iget-object v1, v0, LX/3kz;->f:LX/0hs;

    iget-object v2, v0, LX/3kz;->h:Landroid/view/View;

    invoke-virtual {v1, v2}, LX/0ht;->f(Landroid/view/View;)V

    .line 475190
    const/4 v0, 0x1

    .line 475191
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 475192
    :cond_1
    new-instance v1, LX/0hs;

    iget-object v2, v0, LX/3kz;->b:Landroid/content/Context;

    const/4 p0, 0x2

    invoke-direct {v1, v2, p0}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v1, v0, LX/3kz;->f:LX/0hs;

    .line 475193
    iget-object v1, v0, LX/3kz;->f:LX/0hs;

    const/4 v2, -0x1

    .line 475194
    iput v2, v1, LX/0hs;->t:I

    .line 475195
    iget-object v1, v0, LX/3kz;->f:LX/0hs;

    new-instance v2, LX/9F9;

    invoke-direct {v2, v0}, LX/9F9;-><init>(LX/3kz;)V

    invoke-virtual {v1, v2}, LX/0hs;->a(LX/5Od;)V

    .line 475196
    iget-object v1, v0, LX/3kz;->f:LX/0hs;

    sget-object v2, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v1, v2}, LX/0ht;->a(LX/3AV;)V

    .line 475197
    iget-object v1, v0, LX/3kz;->f:LX/0hs;

    iget-object v2, v0, LX/3kz;->b:Landroid/content/Context;

    const p0, 0x7f081263

    invoke-virtual {v2, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 475198
    new-instance v1, Lcom/facebook/feedback/ui/VideoCommentInterstitialController$2;

    invoke-direct {v1, v0}, Lcom/facebook/feedback/ui/VideoCommentInterstitialController$2;-><init>(LX/3kz;)V

    iput-object v1, v0, LX/3kz;->e:Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 475179
    iget-object v0, p0, LX/2tj;->b:LX/0iA;

    const-string v1, "4181"

    const-class v2, LX/3kz;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kz;

    .line 475180
    if-eqz v0, :cond_0

    .line 475181
    const/4 v1, 0x0

    iput-object v1, v0, LX/3kz;->h:Landroid/view/View;

    .line 475182
    iget-object v1, v0, LX/3kz;->d:Landroid/os/Handler;

    iget-object v2, v0, LX/3kz;->e:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 475183
    :cond_0
    return-void
.end method
