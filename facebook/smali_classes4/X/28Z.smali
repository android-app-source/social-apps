.class public LX/28Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/42T;",
        "Lcom/facebook/auth/component/AuthenticationResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/2Xm;

.field private final b:LX/0hw;

.field private final c:LX/0dC;


# direct methods
.method public constructor <init>(LX/2Xm;LX/0hw;LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374249
    iput-object p1, p0, LX/28Z;->a:LX/2Xm;

    .line 374250
    iput-object p2, p0, LX/28Z;->b:LX/0hw;

    .line 374251
    iput-object p3, p0, LX/28Z;->c:LX/0dC;

    .line 374252
    return-void
.end method

.method public static a(LX/0QB;)LX/28Z;
    .locals 4

    .prologue
    .line 374253
    new-instance v3, LX/28Z;

    invoke-static {p0}, LX/2Xm;->b(LX/0QB;)LX/2Xm;

    move-result-object v0

    check-cast v0, LX/2Xm;

    invoke-static {p0}, LX/0hw;->b(LX/0QB;)LX/0hw;

    move-result-object v1

    check-cast v1, LX/0hw;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v2

    check-cast v2, LX/0dC;

    invoke-direct {v3, v0, v1, v2}, LX/28Z;-><init>(LX/2Xm;LX/0hw;LX/0dC;)V

    .line 374254
    move-object v0, v3

    .line 374255
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 374256
    check-cast p1, LX/42T;

    .line 374257
    iget-object v0, p1, LX/42T;->a:Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;

    .line 374258
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 374259
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "adid"

    iget-object v4, p0, LX/28Z;->b:LX/0hw;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/0hw;->a(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374260
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374261
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "device_id"

    iget-object v4, p0, LX/28Z;->c:LX/0dC;

    invoke-virtual {v4}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374262
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "email"

    const-string v4, "X"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374263
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "password"

    const-string v4, "X"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374264
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "credentials_type"

    const-string v4, "personal_to_work_switch"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374265
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "community_id"

    .line 374266
    iget-object v4, v0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;->b:Ljava/lang/String;

    move-object v4, v4

    .line 374267
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374268
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "access_token"

    .line 374269
    iget-object v4, v0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;->c:Ljava/lang/String;

    move-object v0, v4

    .line 374270
    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374271
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "generate_session_cookies"

    const-string v3, "1"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374272
    iget-object v0, p1, LX/42T;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 374273
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "machine_id"

    iget-object v3, p1, LX/42T;->b:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374274
    :goto_0
    iget-object v0, p1, LX/42T;->c:Landroid/location/Location;

    if-eqz v0, :cond_0

    .line 374275
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "login_latitude"

    iget-object v3, p1, LX/42T;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374276
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "login_longitude"

    iget-object v3, p1, LX/42T;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374277
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "login_location_accuracy_m"

    iget-object v3, p1, LX/42T;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374278
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "login_location_timestamp_ms"

    iget-object v3, p1, LX/42T;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374279
    :cond_0
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    sget-object v2, LX/11I;->AUTHENTICATE:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    .line 374280
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 374281
    move-object v0, v0

    .line 374282
    const-string v2, "POST"

    .line 374283
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 374284
    move-object v0, v0

    .line 374285
    const-string v2, "method/auth.login"

    .line 374286
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 374287
    move-object v0, v0

    .line 374288
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 374289
    move-object v0, v0

    .line 374290
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 374291
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 374292
    move-object v0, v0

    .line 374293
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 374294
    :cond_1
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "generate_machine_id"

    const-string v3, "1"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 374295
    check-cast p1, LX/42T;

    .line 374296
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 374297
    iget-object v0, p1, LX/42T;->a:Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;

    .line 374298
    iget-object v1, v0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;->a:Ljava/lang/String;

    move-object v0, v1

    .line 374299
    iget-object v1, p0, LX/28Z;->a:LX/2Xm;

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v2

    iget-boolean v3, p1, LX/42T;->d:Z

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, LX/2Xm;->a(LX/0lF;Ljava/lang/String;ZLjava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method
