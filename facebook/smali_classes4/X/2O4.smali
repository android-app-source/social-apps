.class public LX/2O4;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/compactdisk/PrivacyGuard;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/compactdisk/PrivacyGuard;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 400439
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/compactdisk/PrivacyGuard;
    .locals 4

    .prologue
    .line 400427
    sget-object v0, LX/2O4;->a:Lcom/facebook/compactdisk/PrivacyGuard;

    if-nez v0, :cond_1

    .line 400428
    const-class v1, LX/2O4;

    monitor-enter v1

    .line 400429
    :try_start_0
    sget-object v0, LX/2O4;->a:Lcom/facebook/compactdisk/PrivacyGuard;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 400430
    if-eqz v2, :cond_0

    .line 400431
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 400432
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2O5;->a(LX/0QB;)LX/2O6;

    move-result-object p0

    check-cast p0, LX/2O6;

    invoke-static {v3, p0}, LX/2FB;->a(Landroid/content/Context;LX/2O6;)Lcom/facebook/compactdisk/PrivacyGuard;

    move-result-object v3

    move-object v0, v3

    .line 400433
    sput-object v0, LX/2O4;->a:Lcom/facebook/compactdisk/PrivacyGuard;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400434
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 400435
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 400436
    :cond_1
    sget-object v0, LX/2O4;->a:Lcom/facebook/compactdisk/PrivacyGuard;

    return-object v0

    .line 400437
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 400438
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 400426
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/2O5;->a(LX/0QB;)LX/2O6;

    move-result-object v1

    check-cast v1, LX/2O6;

    invoke-static {v0, v1}, LX/2FB;->a(Landroid/content/Context;LX/2O6;)Lcom/facebook/compactdisk/PrivacyGuard;

    move-result-object v0

    return-object v0
.end method
