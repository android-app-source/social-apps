.class public final LX/2li;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/bookmark/client/BookmarkClient;


# direct methods
.method public constructor <init>(Lcom/facebook/bookmark/client/BookmarkClient;)V
    .locals 0

    .prologue
    .line 458237
    iput-object p1, p0, LX/2li;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 458238
    iget-object v1, p0, LX/2li;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    monitor-enter v1

    .line 458239
    :try_start_0
    iget-object v0, p0, LX/2li;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    const/4 v2, 0x0

    .line 458240
    iput-object v2, v0, Lcom/facebook/bookmark/client/BookmarkClient;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 458241
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 458242
    iget-object v0, p0, LX/2li;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    const-string v1, "syncWithDb"

    invoke-static {v0, p1, v1}, Lcom/facebook/bookmark/client/BookmarkClient;->a$redex0(Lcom/facebook/bookmark/client/BookmarkClient;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 458243
    return-void

    .line 458244
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 458245
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 458246
    iget-object v1, p0, LX/2li;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    monitor-enter v1

    .line 458247
    :try_start_0
    iget-object v0, p0, LX/2li;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    const/4 v2, 0x0

    .line 458248
    iput-object v2, v0, Lcom/facebook/bookmark/client/BookmarkClient;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 458249
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 458250
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/FetchBookmarksResult;

    .line 458251
    iget-object v1, p0, LX/2li;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    .line 458252
    iget-object v2, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 458253
    const-string v3, "bookmarks_expire_time"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 458254
    iput v2, v1, Lcom/facebook/bookmark/client/BookmarkClient;->k:I

    .line 458255
    iget-object v1, p0, LX/2li;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    iget v1, v1, Lcom/facebook/bookmark/client/BookmarkClient;->k:I

    if-gez v1, :cond_0

    .line 458256
    sget-object v1, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    const-string v2, "The expire time is not set or not correct!"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 458257
    :cond_0
    iget-object v1, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v1, v1

    .line 458258
    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    if-eq v1, v2, :cond_1

    .line 458259
    iget-object v1, p0, LX/2li;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    invoke-static {v1, v0}, Lcom/facebook/bookmark/client/BookmarkClient;->a$redex0(Lcom/facebook/bookmark/client/BookmarkClient;Lcom/facebook/bookmark/FetchBookmarksResult;)V

    .line 458260
    iget-object v1, p0, LX/2li;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    .line 458261
    iget-wide v5, v0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v2, v5

    .line 458262
    iput-wide v2, v1, Lcom/facebook/bookmark/client/BookmarkClient;->j:J

    .line 458263
    :cond_1
    return-void

    .line 458264
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
