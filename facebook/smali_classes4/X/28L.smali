.class public final LX/28L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 373916
    iput-object p1, p0, LX/28L;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iput-object p2, p0, LX/28L;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 373908
    iget-object v1, p0, LX/28L;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v1, v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bi:LX/0Rf;

    iget-object v2, p0, LX/28L;->a:Landroid/view/View;

    .line 373909
    invoke-static {v1, v2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/0Rf;Landroid/view/View;)V

    .line 373910
    iget-object v0, p0, LX/28L;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    const/4 v1, 0x1

    .line 373911
    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Z)V

    .line 373912
    iget-object v0, p0, LX/28L;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-static {v0, v3}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->e(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;I)V

    .line 373913
    iget-object v0, p0, LX/28L;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->X(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373914
    iget-object v0, p0, LX/28L;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bA:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 373915
    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 373897
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 373898
    iget-object v0, p0, LX/28L;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 373899
    iget-object v0, p0, LX/28L;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    const/4 v1, 0x1

    .line 373900
    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->c$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Z)V

    .line 373901
    iget-object v0, p0, LX/28L;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->br:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 373902
    iget-object v0, p0, LX/28L;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bE:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 373903
    iget-object v0, p0, LX/28L;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bF:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 373904
    iget-object v0, p0, LX/28L;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-static {v0, v2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->e(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;I)V

    .line 373905
    iget-object v0, p0, LX/28L;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->X(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373906
    iget-object v0, p0, LX/28L;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bA:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 373907
    :cond_0
    return-void
.end method
