.class public LX/2Kz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;

.field private static volatile s:LX/2Kz;


# instance fields
.field public final b:LX/0tA;

.field public final c:LX/0tc;

.field public final d:LX/1dx;

.field public final e:LX/03V;

.field private final f:LX/0Sh;

.field private final g:Landroid/content/Context;

.field private final h:LX/12x;

.field public final i:LX/0kb;

.field public final j:LX/0sT;

.field public final k:LX/0tk;

.field public final l:LX/2L0;

.field public final m:LX/2L1;

.field public final n:LX/0Xl;

.field public final o:LX/2L7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2L7",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public volatile p:Z

.field private volatile q:J

.field public final r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/3G4;",
            "LX/37X;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 394229
    const-class v0, LX/2Kz;

    sput-object v0, LX/2Kz;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0tA;LX/0tc;LX/1dx;LX/03V;LX/0Sh;Landroid/content/Context;LX/12x;LX/0kb;LX/0sT;LX/0tk;LX/2L0;LX/2L1;LX/0Xl;)V
    .locals 2
    .param p13    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 394230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394231
    new-instance v0, LX/2L7;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, LX/2L7;-><init>(I)V

    iput-object v0, p0, LX/2Kz;->o:LX/2L7;

    .line 394232
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Kz;->p:Z

    .line 394233
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2Kz;->q:J

    .line 394234
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2Kz;->r:Ljava/util/Map;

    .line 394235
    iput-object p1, p0, LX/2Kz;->b:LX/0tA;

    .line 394236
    iput-object p2, p0, LX/2Kz;->c:LX/0tc;

    .line 394237
    iput-object p3, p0, LX/2Kz;->d:LX/1dx;

    .line 394238
    iput-object p4, p0, LX/2Kz;->e:LX/03V;

    .line 394239
    iput-object p5, p0, LX/2Kz;->f:LX/0Sh;

    .line 394240
    iput-object p6, p0, LX/2Kz;->g:Landroid/content/Context;

    .line 394241
    iput-object p7, p0, LX/2Kz;->h:LX/12x;

    .line 394242
    iput-object p8, p0, LX/2Kz;->i:LX/0kb;

    .line 394243
    iput-object p9, p0, LX/2Kz;->j:LX/0sT;

    .line 394244
    iput-object p10, p0, LX/2Kz;->k:LX/0tk;

    .line 394245
    iput-object p11, p0, LX/2Kz;->l:LX/2L0;

    .line 394246
    iput-object p12, p0, LX/2Kz;->m:LX/2L1;

    .line 394247
    iput-object p13, p0, LX/2Kz;->n:LX/0Xl;

    .line 394248
    return-void
.end method

.method public static a(LX/0QB;)LX/2Kz;
    .locals 3

    .prologue
    .line 394249
    sget-object v0, LX/2Kz;->s:LX/2Kz;

    if-nez v0, :cond_1

    .line 394250
    const-class v1, LX/2Kz;

    monitor-enter v1

    .line 394251
    :try_start_0
    sget-object v0, LX/2Kz;->s:LX/2Kz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 394252
    if-eqz v2, :cond_0

    .line 394253
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2Kz;->b(LX/0QB;)LX/2Kz;

    move-result-object v0

    sput-object v0, LX/2Kz;->s:LX/2Kz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394254
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 394255
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 394256
    :cond_1
    sget-object v0, LX/2Kz;->s:LX/2Kz;

    return-object v0

    .line 394257
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 394258
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/2Kz;
    .locals 14

    .prologue
    .line 394259
    new-instance v0, LX/2Kz;

    invoke-static {p0}, LX/0tA;->a(LX/0QB;)LX/0tA;

    move-result-object v1

    check-cast v1, LX/0tA;

    invoke-static {p0}, LX/0tc;->a(LX/0QB;)LX/0tc;

    move-result-object v2

    check-cast v2, LX/0tc;

    invoke-static {p0}, LX/1dx;->a(LX/0QB;)LX/1dx;

    move-result-object v3

    check-cast v3, LX/1dx;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    const-class v6, Landroid/content/Context;

    invoke-interface {p0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {p0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v7

    check-cast v7, LX/12x;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v8

    check-cast v8, LX/0kb;

    invoke-static {p0}, LX/0sT;->a(LX/0QB;)LX/0sT;

    move-result-object v9

    check-cast v9, LX/0sT;

    invoke-static {p0}, LX/0tk;->b(LX/0QB;)LX/0tk;

    move-result-object v10

    check-cast v10, LX/0tk;

    invoke-static {p0}, LX/2L0;->a(LX/0QB;)LX/2L0;

    move-result-object v11

    check-cast v11, LX/2L0;

    invoke-static {p0}, LX/2L1;->a(LX/0QB;)LX/2L1;

    move-result-object v12

    check-cast v12, LX/2L1;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v13

    check-cast v13, LX/0Xl;

    invoke-direct/range {v0 .. v13}, LX/2Kz;-><init>(LX/0tA;LX/0tc;LX/1dx;LX/03V;LX/0Sh;Landroid/content/Context;LX/12x;LX/0kb;LX/0sT;LX/0tk;LX/2L0;LX/2L1;LX/0Xl;)V

    .line 394260
    return-object v0
.end method

.method public static declared-synchronized c(LX/2Kz;)V
    .locals 8

    .prologue
    .line 394261
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/2Kz;->q:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 394262
    :goto_0
    monitor-exit p0

    return-void

    .line 394263
    :cond_0
    :try_start_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.offlinemode.executor.OfflineMutationBroadcastReceiver.RETRY_MUTATIONS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 394264
    iget-object v1, p0, LX/2Kz;->g:Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 394265
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v1, p0, LX/2Kz;->j:LX/0sT;

    .line 394266
    iget-object v4, v1, LX/0sT;->a:LX/0ad;

    sget v5, LX/1NG;->d:I

    const/16 v6, 0xf

    invoke-interface {v4, v5, v6}, LX/0ad;->a(II)I

    move-result v4

    move v1, v4

    .line 394267
    int-to-long v4, v1

    const-wide/32 v6, 0xea60

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/2Kz;->q:J

    .line 394268
    iget-object v1, p0, LX/2Kz;->h:LX/12x;

    const/4 v2, 0x3

    iget-wide v4, p0, LX/2Kz;->q:J

    invoke-virtual {v1, v2, v4, v5, v0}, LX/12x;->b(IJLandroid/app/PendingIntent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 394269
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/3G4;LX/37X;)V
    .locals 2

    .prologue
    .line 394270
    iget-object v0, p0, LX/2Kz;->o:LX/2L7;

    iget-object v1, p1, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2L7;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 394271
    iget-object v0, p0, LX/2Kz;->o:LX/2L7;

    iget-object v1, p1, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2L7;->remove(Ljava/lang/Object;)Z

    .line 394272
    :cond_0
    :goto_0
    return-void

    .line 394273
    :cond_1
    invoke-virtual {p0}, LX/2Kz;->init()V

    .line 394274
    iget-object v0, p0, LX/2Kz;->r:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 394275
    iget-object v0, p0, LX/2Kz;->l:LX/2L0;

    invoke-virtual {v0, p1}, LX/2L0;->a(LX/3G3;)V

    .line 394276
    invoke-virtual {p2}, LX/37X;->a()V

    .line 394277
    iget-object v0, p0, LX/2Kz;->d:LX/1dx;

    invoke-virtual {v0, p1}, LX/1dx;->a(LX/3G3;)V

    .line 394278
    iget-object v0, p0, LX/2Kz;->j:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Kz;->i:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394279
    invoke-static {p0}, LX/2Kz;->c(LX/2Kz;)V

    goto :goto_0
.end method

.method public final init()V
    .locals 9

    .prologue
    .line 394280
    iget-boolean v0, p0, LX/2Kz;->p:Z

    if-nez v0, :cond_1

    .line 394281
    iget-object v0, p0, LX/2Kz;->f:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 394282
    monitor-enter p0

    .line 394283
    :try_start_0
    iget-boolean v0, p0, LX/2Kz;->p:Z

    if-nez v0, :cond_0

    .line 394284
    const/4 v3, 0x1

    .line 394285
    iget-object v0, p0, LX/2Kz;->d:LX/1dx;

    invoke-virtual {v0}, LX/1dx;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 394286
    iput-boolean v3, p0, LX/2Kz;->p:Z

    .line 394287
    :cond_0
    :goto_0
    monitor-exit p0

    .line 394288
    :cond_1
    :goto_1
    return-void

    .line 394289
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 394290
    :cond_2
    iget-object v0, p0, LX/2Kz;->e:LX/03V;

    const-string v1, "offline"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/2Kz;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " used on UI thread before initialized"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 394291
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/2Kz;->n:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance v2, LX/2L9;

    invoke-direct {v2, p0}, LX/2L9;-><init>(LX/2Kz;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 394292
    iget-object v0, p0, LX/2Kz;->l:LX/2L0;

    new-instance v1, LX/2LA;

    invoke-direct {v1, p0}, LX/2LA;-><init>(LX/2Kz;)V

    invoke-virtual {v0, v1}, LX/2L0;->a(LX/2LB;)V

    .line 394293
    iget-object v0, p0, LX/2Kz;->l:LX/2L0;

    invoke-virtual {v0}, LX/2L0;->a()LX/0Px;

    move-result-object v0

    .line 394294
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_5

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3G3;

    .line 394295
    instance-of v5, v1, LX/3G4;

    if-eqz v5, :cond_4

    .line 394296
    check-cast v1, LX/3G4;

    .line 394297
    iget-object v5, p0, LX/2Kz;->r:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394298
    :try_start_2
    invoke-virtual {v1}, LX/3G4;->d()LX/399;

    move-result-object v5

    .line 394299
    iget-object v6, p0, LX/2Kz;->k:LX/0tk;

    invoke-virtual {v6, v5}, LX/0tk;->a(LX/399;)LX/3Bq;

    move-result-object v5

    .line 394300
    iget-object v6, p0, LX/2Kz;->b:LX/0tA;

    iget-object v7, v1, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v5}, LX/0tA;->a(Ljava/lang/String;LX/3Bq;)V

    .line 394301
    iget-object v6, p0, LX/2Kz;->c:LX/0tc;

    invoke-virtual {v6, v5}, LX/0tc;->a(LX/3Bq;)LX/37X;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    .line 394302
    :goto_3
    move-object v5, v5

    .line 394303
    if-eqz v5, :cond_4

    .line 394304
    iget-object v6, p0, LX/2Kz;->r:Ljava/util/Map;

    invoke-interface {v6, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 394305
    invoke-virtual {v5}, LX/37X;->a()V

    .line 394306
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 394307
    :cond_5
    iget-object v1, p0, LX/2Kz;->l:LX/2L0;

    sget-object v2, LX/2LJ;->COLD_START:LX/2LJ;

    invoke-virtual {v1, v2, v0}, LX/2L0;->a(LX/2LJ;LX/0Px;)V

    .line 394308
    iput-boolean v3, p0, LX/2Kz;->p:Z

    goto/16 :goto_0

    .line 394309
    :catch_0
    move-exception v5

    .line 394310
    iget-object v6, p0, LX/2Kz;->e:LX/03V;

    const-string v7, "offline"

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 394311
    const/4 v5, 0x0

    goto :goto_3
.end method
