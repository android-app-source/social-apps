.class public final LX/3MJ;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:LX/0sv;

.field private static final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    .line 554271
    new-instance v0, LX/0U1;

    const-string v1, "address"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MJ;->a:LX/0U1;

    .line 554272
    new-instance v0, LX/0U1;

    const-string v1, "smsc"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MJ;->b:LX/0U1;

    .line 554273
    new-instance v0, LX/0U1;

    const-string v1, "delete_score"

    const-string v2, "REAL DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MJ;->c:LX/0U1;

    .line 554274
    new-instance v0, LX/0U1;

    const-string v1, "delete_score_ts"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MJ;->d:LX/0U1;

    .line 554275
    new-instance v0, LX/0U1;

    const-string v1, "spam_score"

    const-string v2, "REAL DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MJ;->e:LX/0U1;

    .line 554276
    new-instance v0, LX/0U1;

    const-string v1, "ranking_score"

    const-string v2, "REAL DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MJ;->f:LX/0U1;

    .line 554277
    new-instance v0, LX/0U1;

    const-string v1, "scores_ts"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MJ;->g:LX/0U1;

    .line 554278
    sget-object v0, LX/3MJ;->a:LX/0U1;

    sget-object v1, LX/3MJ;->b:LX/0U1;

    sget-object v2, LX/3MJ;->c:LX/0U1;

    sget-object v3, LX/3MJ;->d:LX/0U1;

    sget-object v4, LX/3MJ;->e:LX/0U1;

    sget-object v5, LX/3MJ;->f:LX/0U1;

    sget-object v6, LX/3MJ;->g:LX/0U1;

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3MJ;->h:LX/0Px;

    .line 554279
    new-instance v0, LX/0su;

    sget-object v1, LX/3MJ;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    .line 554280
    sput-object v0, LX/3MJ;->i:LX/0sv;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3MJ;->j:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 554281
    const-string v0, "address_table"

    sget-object v1, LX/3MJ;->h:LX/0Px;

    sget-object v2, LX/3MJ;->j:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 554282
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 554283
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 554284
    const-string v0, "address_table"

    const-string v1, "INDEX_ADDRESS"

    sget-object v2, LX/3MJ;->a:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7cec6a2e

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x433f8378

    invoke-static {v0}, LX/03h;->a(I)V

    .line 554285
    return-void
.end method
