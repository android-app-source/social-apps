.class public final LX/30A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/30A",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final b:J


# direct methods
.method public constructor <init>(Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture",
            "<TV;>;J)V"
        }
    .end annotation

    .prologue
    .line 484098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484099
    iput-object p1, p0, LX/30A;->a:Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;

    .line 484100
    iput-wide p2, p0, LX/30A;->b:J

    .line 484101
    return-void
.end method


# virtual methods
.method public final compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 484102
    check-cast p1, LX/30A;

    .line 484103
    iget-wide v0, p0, LX/30A;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-wide v2, p1, LX/30A;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    return v0
.end method
