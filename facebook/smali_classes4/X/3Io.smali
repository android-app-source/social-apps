.class public LX/3Io;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3Ip;

.field public final b:Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Sj;

.field public final e:LX/03V;

.field public final f:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/3Ip;Lorg/apache/http/protocol/HttpRequestHandlerRegistry;LX/0Sj;LX/03V;Ljava/util/concurrent/ExecutorService;)V
    .locals 1

    .prologue
    .line 547231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547232
    iput-object p1, p0, LX/3Io;->a:LX/3Ip;

    .line 547233
    iput-object p2, p0, LX/3Io;->b:Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    .line 547234
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/3Io;->c:Ljava/util/ArrayList;

    .line 547235
    iput-object p3, p0, LX/3Io;->d:LX/0Sj;

    .line 547236
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, LX/3Io;->e:LX/03V;

    .line 547237
    iput-object p5, p0, LX/3Io;->f:Ljava/util/concurrent/ExecutorService;

    .line 547238
    return-void
.end method
