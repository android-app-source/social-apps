.class public LX/2HE;
.super LX/2H3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Tn;

.field private static final b:LX/0Tn;

.field private static volatile c:LX/2HE;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 390194
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "messenger/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 390195
    sput-object v0, LX/2HE;->a:LX/0Tn;

    const-string v1, "c2dm/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2HE;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390196
    invoke-direct {p0}, LX/2H3;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2HE;
    .locals 3

    .prologue
    .line 390197
    sget-object v0, LX/2HE;->c:LX/2HE;

    if-nez v0, :cond_1

    .line 390198
    const-class v1, LX/2HE;

    monitor-enter v1

    .line 390199
    :try_start_0
    sget-object v0, LX/2HE;->c:LX/2HE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390200
    if-eqz v2, :cond_0

    .line 390201
    :try_start_1
    new-instance v0, LX/2HE;

    invoke-direct {v0}, LX/2HE;-><init>()V

    .line 390202
    move-object v0, v0

    .line 390203
    sput-object v0, LX/2HE;->c:LX/2HE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390204
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390205
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390206
    :cond_1
    sget-object v0, LX/2HE;->c:LX/2HE;

    return-object v0

    .line 390207
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390208
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Tn;
    .locals 1

    .prologue
    .line 390209
    sget-object v0, LX/2HE;->b:LX/0Tn;

    return-object v0
.end method
