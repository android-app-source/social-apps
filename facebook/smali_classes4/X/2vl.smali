.class public final enum LX/2vl;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2vl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2vl;

.field public static final enum ANY_LOCATION:LX/2vl;

.field public static final enum BLOCK_GRANULARITY:LX/2vl;

.field public static final enum CITY_GRANULARITY:LX/2vl;

.field public static final enum EXACT_GRANULARITY:LX/2vl;


# instance fields
.field public final perfLoggerName:Ljava/lang/String;

.field public final perfMarkerId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 478644
    new-instance v0, LX/2vl;

    const-string v1, "ANY_LOCATION"

    const v2, 0x330001

    invoke-direct {v0, v1, v3, v2}, LX/2vl;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/2vl;->ANY_LOCATION:LX/2vl;

    .line 478645
    new-instance v0, LX/2vl;

    const-string v1, "CITY_GRANULARITY"

    const v2, 0x330002

    invoke-direct {v0, v1, v4, v2}, LX/2vl;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/2vl;->CITY_GRANULARITY:LX/2vl;

    .line 478646
    new-instance v0, LX/2vl;

    const-string v1, "BLOCK_GRANULARITY"

    const v2, 0x330003

    invoke-direct {v0, v1, v5, v2}, LX/2vl;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/2vl;->BLOCK_GRANULARITY:LX/2vl;

    .line 478647
    new-instance v0, LX/2vl;

    const-string v1, "EXACT_GRANULARITY"

    const v2, 0x330004

    invoke-direct {v0, v1, v6, v2}, LX/2vl;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/2vl;->EXACT_GRANULARITY:LX/2vl;

    .line 478648
    const/4 v0, 0x4

    new-array v0, v0, [LX/2vl;

    sget-object v1, LX/2vl;->ANY_LOCATION:LX/2vl;

    aput-object v1, v0, v3

    sget-object v1, LX/2vl;->CITY_GRANULARITY:LX/2vl;

    aput-object v1, v0, v4

    sget-object v1, LX/2vl;->BLOCK_GRANULARITY:LX/2vl;

    aput-object v1, v0, v5

    sget-object v1, LX/2vl;->EXACT_GRANULARITY:LX/2vl;

    aput-object v1, v0, v6

    sput-object v0, LX/2vl;->$VALUES:[LX/2vl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 478649
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 478650
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FbLocationManager."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/2vl;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2vl;->perfLoggerName:Ljava/lang/String;

    .line 478651
    iput p3, p0, LX/2vl;->perfMarkerId:I

    .line 478652
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2vl;
    .locals 1

    .prologue
    .line 478653
    const-class v0, LX/2vl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2vl;

    return-object v0
.end method

.method public static values()[LX/2vl;
    .locals 1

    .prologue
    .line 478654
    sget-object v0, LX/2vl;->$VALUES:[LX/2vl;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2vl;

    return-object v0
.end method
