.class public final LX/2Cq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 0

    .prologue
    .line 383272
    iput-object p1, p0, LX/2Cq;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 6

    .prologue
    .line 383274
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/server/protocol/GetSsoUserMethod$Result;

    .line 383275
    iget-object v1, p0, LX/2Cq;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v2, v0, Lcom/facebook/katana/server/protocol/GetSsoUserMethod$Result;->a:Ljava/lang/String;

    .line 383276
    iput-object v2, v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aN:Ljava/lang/String;

    .line 383277
    iget-object v1, p0, LX/2Cq;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v1, v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bw:Lcom/facebook/resources/ui/FbButton;

    .line 383278
    iget-object v2, p0, LX/2Cq;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-virtual {v2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 383279
    new-instance v3, LX/47x;

    invoke-direct {v3, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 383280
    const v4, 0x7f080157

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 383281
    const-string v2, "[[name]]"

    iget-object v0, v0, Lcom/facebook/katana/server/protocol/GetSsoUserMethod$Result;->b:Ljava/lang/String;

    const/4 v4, 0x0

    const/16 v5, 0x21

    invoke-virtual {v3, v2, v0, v4, v5}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 383282
    invoke-virtual {v3}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 383283
    iget-object v0, p0, LX/2Cq;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bV:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 383284
    iget-object v0, p0, LX/2Cq;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    sget-object v1, LX/2NP;->SSO_AUTH_UI:LX/2NP;

    .line 383285
    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NP;)V

    .line 383286
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 383287
    sget-object v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->p:Ljava/lang/Class;

    const-string v1, "Could not get user data using auth token"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 383288
    iget-object v0, p0, LX/2Cq;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    sget-object v1, LX/2NP;->PASSWORD_AUTH_UI:LX/2NP;

    .line 383289
    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NP;)V

    .line 383290
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 383273
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/2Cq;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
