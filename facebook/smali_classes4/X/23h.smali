.class public LX/23h;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/11i;

.field private final b:LX/0So;

.field public c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/11o",
            "<",
            "LX/0Pq;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0Pq;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/11i;LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 364958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 364959
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->c:LX/0am;

    .line 364960
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->d:LX/0am;

    .line 364961
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->e:LX/0am;

    .line 364962
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->f:LX/0am;

    .line 364963
    iput-object p1, p0, LX/23h;->a:LX/11i;

    .line 364964
    iput-object p2, p0, LX/23h;->b:LX/0So;

    .line 364965
    return-void
.end method

.method public static a(LX/0QB;)LX/23h;
    .locals 5

    .prologue
    .line 364947
    const-class v1, LX/23h;

    monitor-enter v1

    .line 364948
    :try_start_0
    sget-object v0, LX/23h;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 364949
    sput-object v2, LX/23h;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 364950
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364951
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 364952
    new-instance p0, LX/23h;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/23h;-><init>(LX/11i;LX/0So;)V

    .line 364953
    move-object v0, p0

    .line 364954
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 364955
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/23h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364956
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 364957
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static c()Z
    .locals 4

    .prologue
    .line 364946
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x3fb99999a0000000L    # 0.10000000149011612

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 364915
    iget-object v0, p0, LX/23h;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 364916
    :goto_0
    return-void

    .line 364917
    :cond_0
    iget-object v0, p0, LX/23h;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11o;

    const-string v1, "WaitTime"

    invoke-interface {v0, v1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 364918
    iget-object v0, p0, LX/23h;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11o;

    const-string v1, "WaitTime"

    const v2, 0xc275892

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 364919
    :cond_1
    iget-object v0, p0, LX/23h;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11o;

    const-string v1, "DataFetch"

    invoke-interface {v0, v1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 364920
    iget-object v0, p0, LX/23h;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11o;

    const-string v1, "DataFetch"

    const v2, 0x622d5c91

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 364921
    :cond_2
    iget-object v0, p0, LX/23h;->a:LX/11i;

    iget-object v1, p0, LX/23h;->d:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Pq;

    iget-object v2, p0, LX/23h;->e:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, LX/23h;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, LX/11i;->b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V

    .line 364922
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->c:LX/0am;

    .line 364923
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->d:LX/0am;

    .line 364924
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->e:LX/0am;

    .line 364925
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->f:LX/0am;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/0Pq;Ljava/lang/String;J)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 364932
    iget-object v0, p0, LX/23h;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364933
    iget-object v0, p0, LX/23h;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_2

    .line 364934
    :cond_0
    :goto_0
    invoke-static {}, LX/23h;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 364935
    :goto_1
    return-void

    .line 364936
    :cond_1
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->e:LX/0am;

    .line 364937
    iget-object v0, p0, LX/23h;->a:LX/11i;

    const-string v1, "source"

    invoke-static {v1, p3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    move-object v1, p2

    move-object v2, p1

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, LX/11i;->a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->c:LX/0am;

    .line 364938
    invoke-static {p2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->d:LX/0am;

    .line 364939
    iget-object v0, p0, LX/23h;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11o;

    const-string v1, "WaitTime"

    const v6, -0x6a3aaae9

    move-object v2, v7

    move-object v3, v7

    move-wide v4, p4

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 364940
    iget-object v0, p0, LX/23h;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11o;

    const-string v1, "DataFetch"

    const v6, 0x63055810

    move-object v2, v7

    move-object v3, v7

    move-wide v4, p4

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    goto :goto_1

    .line 364941
    :cond_2
    iget-object v2, p0, LX/23h;->a:LX/11i;

    iget-object v0, p0, LX/23h;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pq;

    iget-object v1, p0, LX/23h;->e:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v0, v1}, LX/11i;->a(LX/0Pq;Ljava/lang/String;)V

    .line 364942
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->c:LX/0am;

    .line 364943
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->d:LX/0am;

    .line 364944
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->e:LX/0am;

    .line 364945
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/23h;->f:LX/0am;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 364929
    iget-object v0, p0, LX/23h;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 364930
    :goto_0
    return-void

    .line 364931
    :cond_0
    iget-object v0, p0, LX/23h;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11o;

    const v1, -0x11ba033f

    invoke-static {v0, p1, v1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 364926
    iget-object v0, p0, LX/23h;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 364927
    :goto_0
    return-void

    .line 364928
    :cond_0
    iget-object v0, p0, LX/23h;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11o;

    const v1, 0x118db46f

    invoke-static {v0, p1, v1}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    goto :goto_0
.end method
