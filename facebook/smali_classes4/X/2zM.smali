.class public final LX/2zM;
.super LX/0rP;
.source ""


# static fields
.field public static final a:LX/2zM;

.field private static final c:Ljava/math/BigDecimal;

.field private static final d:Ljava/math/BigDecimal;

.field private static final e:Ljava/math/BigDecimal;

.field private static final f:Ljava/math/BigDecimal;


# instance fields
.field public final b:Ljava/math/BigDecimal;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 483342
    new-instance v0, LX/2zM;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-direct {v0, v1}, LX/2zM;-><init>(Ljava/math/BigDecimal;)V

    sput-object v0, LX/2zM;->a:LX/2zM;

    .line 483343
    const-wide/32 v0, -0x80000000

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, LX/2zM;->c:Ljava/math/BigDecimal;

    .line 483344
    const-wide/32 v0, 0x7fffffff

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, LX/2zM;->d:Ljava/math/BigDecimal;

    .line 483345
    const-wide/high16 v0, -0x8000000000000000L

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, LX/2zM;->e:Ljava/math/BigDecimal;

    .line 483346
    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, LX/2zM;->f:Ljava/math/BigDecimal;

    return-void
.end method

.method private constructor <init>(Ljava/math/BigDecimal;)V
    .locals 0

    .prologue
    .line 483341
    invoke-direct {p0}, LX/0rP;-><init>()V

    iput-object p1, p0, LX/2zM;->b:Ljava/math/BigDecimal;

    return-void
.end method

.method public static a(Ljava/math/BigDecimal;)LX/2zM;
    .locals 1

    .prologue
    .line 483340
    new-instance v0, LX/2zM;

    invoke-direct {v0, p0}, LX/2zM;-><init>(Ljava/math/BigDecimal;)V

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 483339
    iget-object v0, p0, LX/2zM;->b:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 483338
    iget-object v0, p0, LX/2zM;->b:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/15z;
    .locals 1

    .prologue
    .line 483337
    sget-object v0, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    return-object v0
.end method

.method public final b()LX/16L;
    .locals 1

    .prologue
    .line 483347
    sget-object v0, LX/16L;->BIG_DECIMAL:LX/16L;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 483320
    if-ne p1, p0, :cond_1

    .line 483321
    :cond_0
    :goto_0
    return v0

    .line 483322
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 483323
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 483324
    goto :goto_0

    .line 483325
    :cond_3
    check-cast p1, LX/2zM;

    iget-object v2, p1, LX/2zM;->b:Ljava/math/BigDecimal;

    iget-object v3, p0, LX/2zM;->b:Ljava/math/BigDecimal;

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 483336
    iget-object v0, p0, LX/2zM;->b:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    return v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 483331
    sget-object v0, LX/0mt;->WRITE_BIGDECIMAL_AS_PLAIN:LX/0mt;

    invoke-virtual {p2, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 483332
    instance-of v0, p1, LX/0nW;

    if-nez v0, :cond_0

    .line 483333
    iget-object v0, p0, LX/2zM;->b:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->e(Ljava/lang/String;)V

    .line 483334
    :goto_0
    return-void

    .line 483335
    :cond_0
    iget-object v0, p0, LX/2zM;->b:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/math/BigDecimal;)V

    goto :goto_0
.end method

.method public final v()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 483330
    iget-object v0, p0, LX/2zM;->b:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 483329
    iget-object v0, p0, LX/2zM;->b:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    return v0
.end method

.method public final x()J
    .locals 2

    .prologue
    .line 483328
    iget-object v0, p0, LX/2zM;->b:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final y()D
    .locals 2

    .prologue
    .line 483327
    iget-object v0, p0, LX/2zM;->b:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public final z()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 483326
    iget-object v0, p0, LX/2zM;->b:Ljava/math/BigDecimal;

    return-object v0
.end method
