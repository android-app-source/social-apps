.class public final LX/2Y7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "LX/2YK;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:Ljava/io/FileFilter;


# instance fields
.field public final a:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "LX/2YH;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:LX/2YK;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 420741
    new-instance v0, LX/2Y8;

    invoke-direct {v0}, LX/2Y8;-><init>()V

    sput-object v0, LX/2Y7;->d:Ljava/io/FileFilter;

    return-void
.end method

.method public constructor <init>(LX/2Y9;)V
    .locals 5

    .prologue
    .line 420742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420743
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/2Y7;->a:Ljava/util/ArrayDeque;

    .line 420744
    invoke-virtual {p1}, LX/2YA;->a()Ljava/util/Iterator;

    move-result-object v1

    .line 420745
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 420746
    iget-object v2, p0, LX/2Y7;->a:Ljava/util/ArrayDeque;

    new-instance v3, LX/2YH;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2YB;

    invoke-direct {v3, v0}, LX/2YH;-><init>(LX/2YB;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    goto :goto_0

    .line 420747
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/2YK;
    .locals 2

    .prologue
    .line 420748
    invoke-virtual {p0}, LX/2Y7;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 420749
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 420750
    :cond_0
    iget-object v0, p0, LX/2Y7;->c:LX/2YK;

    .line 420751
    const/4 v1, 0x0

    iput-object v1, p0, LX/2Y7;->c:LX/2YK;

    .line 420752
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2Y7;->b:Z

    .line 420753
    return-object v0
.end method

.method public final hasNext()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 420754
    iget-boolean v1, p0, LX/2Y7;->b:Z

    if-nez v1, :cond_1

    .line 420755
    iput-boolean v0, p0, LX/2Y7;->b:Z

    .line 420756
    :cond_0
    iget-object v1, p0, LX/2Y7;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 420757
    iget-object v1, p0, LX/2Y7;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2YH;

    .line 420758
    iget-object v2, v1, LX/2YH;->a:LX/2YB;

    move-object v2, v2

    .line 420759
    invoke-static {v1}, LX/2YH;->e(LX/2YH;)Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    move v3, v3

    .line 420760
    if-eqz v3, :cond_3

    .line 420761
    iget v3, v1, LX/2YH;->c:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, LX/2YH;->c:I

    .line 420762
    invoke-static {v1}, LX/2YH;->e(LX/2YH;)Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2YB;

    move-object v3, v3

    .line 420763
    iget-object v4, p0, LX/2Y7;->a:Ljava/util/ArrayDeque;

    new-instance v5, LX/2YH;

    invoke-direct {v5, v3}, LX/2YH;-><init>(LX/2YB;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 420764
    const/4 v3, 0x1

    .line 420765
    iget v4, v1, LX/2YH;->c:I

    if-ne v4, v3, :cond_6

    :goto_0
    move v1, v3

    .line 420766
    if-eqz v1, :cond_0

    .line 420767
    new-instance v1, LX/2YK;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, LX/2YK;-><init>(LX/2YB;I)V

    .line 420768
    :goto_1
    move-object v1, v1

    .line 420769
    iput-object v1, p0, LX/2Y7;->c:LX/2YK;

    .line 420770
    :cond_1
    iget-object v1, p0, LX/2Y7;->c:LX/2YK;

    if-eqz v1, :cond_2

    :goto_2
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 420771
    :cond_3
    iget-object v1, p0, LX/2Y7;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->removeLast()Ljava/lang/Object;

    .line 420772
    instance-of v1, v2, LX/2YA;

    if-eqz v1, :cond_4

    .line 420773
    new-instance v1, LX/2YK;

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/2YK;-><init>(LX/2YB;I)V

    goto :goto_1

    .line 420774
    :cond_4
    new-instance v1, LX/2YK;

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, LX/2YK;-><init>(LX/2YB;I)V

    goto :goto_1

    .line 420775
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    :cond_6
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 420776
    invoke-virtual {p0}, LX/2Y7;->a()LX/2YK;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 420777
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
