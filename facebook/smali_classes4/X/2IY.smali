.class public LX/2IY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2IY;


# instance fields
.field public final a:LX/0lp;

.field private final b:LX/2IZ;

.field public final c:LX/03V;


# direct methods
.method public constructor <init>(LX/0lp;LX/2IZ;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391861
    iput-object p1, p0, LX/2IY;->a:LX/0lp;

    .line 391862
    iput-object p2, p0, LX/2IY;->b:LX/2IZ;

    .line 391863
    iput-object p3, p0, LX/2IY;->c:LX/03V;

    .line 391864
    return-void
.end method

.method public static a(LX/0QB;)LX/2IY;
    .locals 6

    .prologue
    .line 391865
    sget-object v0, LX/2IY;->d:LX/2IY;

    if-nez v0, :cond_1

    .line 391866
    const-class v1, LX/2IY;

    monitor-enter v1

    .line 391867
    :try_start_0
    sget-object v0, LX/2IY;->d:LX/2IY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391868
    if-eqz v2, :cond_0

    .line 391869
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 391870
    new-instance p0, LX/2IY;

    invoke-static {v0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v3

    check-cast v3, LX/0lp;

    invoke-static {v0}, LX/2IZ;->a(LX/0QB;)LX/2IZ;

    move-result-object v4

    check-cast v4, LX/2IZ;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/2IY;-><init>(LX/0lp;LX/2IZ;LX/03V;)V

    .line 391871
    move-object v0, p0

    .line 391872
    sput-object v0, LX/2IY;->d:LX/2IY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391873
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391874
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391875
    :cond_1
    sget-object v0, LX/2IY;->d:LX/2IY;

    return-object v0

    .line 391876
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391877
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a([B)Lcom/facebook/assetdownload/AssetDownloadConfiguration;
    .locals 7

    .prologue
    .line 391878
    :try_start_0
    iget-object v0, p0, LX/2IY;->a:LX/0lp;

    invoke-virtual {v0, p1}, LX/0lp;->a([B)LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 391879
    :goto_0
    return-object v0

    .line 391880
    :catch_0
    move-exception v0

    .line 391881
    iget-object v1, p0, LX/2IY;->c:LX/03V;

    const-string v2, "download_task_unmarshal_io_exception"

    const-string v3, "Cannot unmarshal rawBytes[%d]"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    array-length v6, p1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 391882
    const/4 v0, 0x0

    goto :goto_0
.end method
