.class public LX/2Ap;
.super LX/2Aq;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2Aq;",
        "Ljava/lang/Comparable",
        "<",
        "LX/2Ap;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Z

.field public final b:LX/0lU;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public e:LX/2Ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Ar",
            "<",
            "LX/2Am;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/2Ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Ar",
            "<",
            "LX/2Vd;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/2Ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Ar",
            "<",
            "LX/2At;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/2Ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Ar",
            "<",
            "LX/2At;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/2Ap;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 378204
    invoke-direct {p0}, LX/2Aq;-><init>()V

    .line 378205
    iget-object v0, p1, LX/2Ap;->d:Ljava/lang/String;

    iput-object v0, p0, LX/2Ap;->d:Ljava/lang/String;

    .line 378206
    iput-object p2, p0, LX/2Ap;->c:Ljava/lang/String;

    .line 378207
    iget-object v0, p1, LX/2Ap;->b:LX/0lU;

    iput-object v0, p0, LX/2Ap;->b:LX/0lU;

    .line 378208
    iget-object v0, p1, LX/2Ap;->e:LX/2Ar;

    iput-object v0, p0, LX/2Ap;->e:LX/2Ar;

    .line 378209
    iget-object v0, p1, LX/2Ap;->f:LX/2Ar;

    iput-object v0, p0, LX/2Ap;->f:LX/2Ar;

    .line 378210
    iget-object v0, p1, LX/2Ap;->g:LX/2Ar;

    iput-object v0, p0, LX/2Ap;->g:LX/2Ar;

    .line 378211
    iget-object v0, p1, LX/2Ap;->h:LX/2Ar;

    iput-object v0, p0, LX/2Ap;->h:LX/2Ar;

    .line 378212
    iget-boolean v0, p1, LX/2Ap;->a:Z

    iput-boolean v0, p0, LX/2Ap;->a:Z

    .line 378213
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0lU;Z)V
    .locals 0

    .prologue
    .line 378135
    invoke-direct {p0}, LX/2Aq;-><init>()V

    .line 378136
    iput-object p1, p0, LX/2Ap;->d:Ljava/lang/String;

    .line 378137
    iput-object p1, p0, LX/2Ap;->c:Ljava/lang/String;

    .line 378138
    iput-object p2, p0, LX/2Ap;->b:LX/0lU;

    .line 378139
    iput-boolean p3, p0, LX/2Ap;->a:Z

    .line 378140
    return-void
.end method

.method private varargs a(I[LX/2Ar;)LX/0lP;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[",
            "LX/2Ar",
            "<+",
            "LX/2An;",
            ">;)",
            "LX/0lP;"
        }
    .end annotation

    .prologue
    .line 378141
    aget-object v0, p2, p1

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2An;

    invoke-virtual {v0}, LX/2An;->e()LX/0lP;

    move-result-object v1

    .line 378142
    add-int/lit8 v0, p1, 0x1

    .line 378143
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_1

    .line 378144
    aget-object v2, p2, v0

    if-eqz v2, :cond_0

    .line 378145
    invoke-direct {p0, v0, p2}, LX/2Ap;->a(I[LX/2Ar;)LX/0lP;

    move-result-object v0

    invoke-static {v1, v0}, LX/0lP;->a(LX/0lP;LX/0lP;)LX/0lP;

    move-result-object v0

    .line 378146
    :goto_1
    return-object v0

    .line 378147
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 378148
    goto :goto_1
.end method

.method private static a(LX/2Ar;)LX/2Ar;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ar",
            "<TT;>;)",
            "LX/2Ar",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 378149
    if-nez p0, :cond_0

    .line 378150
    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p0}, LX/2Ar;->a()LX/2Ar;

    move-result-object p0

    goto :goto_0
.end method

.method private static a(LX/2Ar;LX/2Ar;)LX/2Ar;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ar",
            "<TT;>;",
            "LX/2Ar",
            "<TT;>;)",
            "LX/2Ar",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 378151
    if-nez p0, :cond_0

    .line 378152
    :goto_0
    return-object p1

    .line 378153
    :cond_0
    if-nez p1, :cond_1

    move-object p1, p0

    .line 378154
    goto :goto_0

    .line 378155
    :cond_1
    invoke-static {p0, p1}, LX/2Ar;->b(LX/2Ar;LX/2Ar;)LX/2Ar;

    move-result-object p1

    goto :goto_0
.end method

.method private a(LX/2B1;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2B1",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 378156
    const/4 v0, 0x0

    .line 378157
    iget-object v1, p0, LX/2Ap;->b:LX/0lU;

    if-eqz v1, :cond_1

    .line 378158
    iget-boolean v1, p0, LX/2Ap;->a:Z

    if-eqz v1, :cond_2

    .line 378159
    iget-object v1, p0, LX/2Ap;->g:LX/2Ar;

    if-eqz v1, :cond_0

    .line 378160
    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2An;

    invoke-interface {p1, v0}, LX/2B1;->a(LX/2An;)Ljava/lang/Object;

    move-result-object v0

    .line 378161
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    iget-object v1, p0, LX/2Ap;->e:LX/2Ar;

    if-eqz v1, :cond_1

    .line 378162
    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2An;

    invoke-interface {p1, v0}, LX/2B1;->a(LX/2An;)Ljava/lang/Object;

    move-result-object v0

    .line 378163
    :cond_1
    return-object v0

    .line 378164
    :cond_2
    iget-object v1, p0, LX/2Ap;->f:LX/2Ar;

    if-eqz v1, :cond_3

    .line 378165
    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2An;

    invoke-interface {p1, v0}, LX/2B1;->a(LX/2An;)Ljava/lang/Object;

    move-result-object v0

    .line 378166
    :cond_3
    if-nez v0, :cond_0

    iget-object v1, p0, LX/2Ap;->h:LX/2Ar;

    if-eqz v1, :cond_0

    .line 378167
    iget-object v0, p0, LX/2Ap;->h:LX/2Ar;

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2An;

    invoke-interface {p1, v0}, LX/2B1;->a(LX/2An;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private b(LX/2Ap;)I
    .locals 2

    .prologue
    .line 378168
    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    if-eqz v0, :cond_0

    .line 378169
    iget-object v0, p1, LX/2Ap;->f:LX/2Ar;

    if-nez v0, :cond_1

    .line 378170
    const/4 v0, -0x1

    .line 378171
    :goto_0
    return v0

    .line 378172
    :cond_0
    iget-object v0, p1, LX/2Ap;->f:LX/2Ar;

    if-eqz v0, :cond_1

    .line 378173
    const/4 v0, 0x1

    goto :goto_0

    .line 378174
    :cond_1
    invoke-virtual {p0}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/2Ar;)LX/2Ar;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ar",
            "<TT;>;)",
            "LX/2Ar",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 378116
    if-nez p0, :cond_0

    .line 378117
    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p0}, LX/2Ar;->b()LX/2Ar;

    move-result-object p0

    goto :goto_0
.end method

.method private b(LX/2Ar;LX/2Ar;)LX/2Ar;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ar",
            "<+",
            "LX/2An;",
            ">;",
            "LX/2Ar",
            "<+",
            "LX/2An;",
            ">;)",
            "LX/2Ar",
            "<+",
            "LX/2An;",
            ">;"
        }
    .end annotation

    .prologue
    .line 378175
    move-object v0, p2

    move-object v1, p1

    :goto_0
    if-eqz v1, :cond_2

    .line 378176
    iget-object v2, v1, LX/2Ar;->c:Ljava/lang/String;

    .line 378177
    if-eqz v2, :cond_0

    .line 378178
    iget-object v3, p0, LX/2Ap;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 378179
    if-nez v0, :cond_1

    move-object v0, v1

    .line 378180
    :cond_0
    iget-object v1, v1, LX/2Ar;->b:LX/2Ar;

    goto :goto_0

    .line 378181
    :cond_1
    iget-object v3, v0, LX/2Ar;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 378182
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Conflicting property name definitions: \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, LX/2Ar;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' (for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ") vs \'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, v1, LX/2Ar;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\' (for "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v1, LX/2Ar;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 378183
    :cond_2
    return-object v0
.end method

.method private static c(LX/2Ar;)LX/2Ar;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ar",
            "<TT;>;)",
            "LX/2Ar",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 378184
    if-nez p0, :cond_0

    .line 378185
    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p0}, LX/2Ar;->c()LX/2Ar;

    move-result-object p0

    goto :goto_0
.end method

.method private static d(LX/2Ar;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ar",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 378186
    :goto_0
    if-eqz p0, :cond_1

    .line 378187
    iget-object v0, p0, LX/2Ar;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Ar;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 378188
    const/4 v0, 0x1

    .line 378189
    :goto_1
    return v0

    .line 378190
    :cond_0
    iget-object p0, p0, LX/2Ar;->b:LX/2Ar;

    goto :goto_0

    .line 378191
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static e(LX/2Ar;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ar",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 378192
    :goto_0
    if-eqz p0, :cond_1

    .line 378193
    iget-boolean v0, p0, LX/2Ar;->d:Z

    if-eqz v0, :cond_0

    .line 378194
    const/4 v0, 0x1

    .line 378195
    :goto_1
    return v0

    .line 378196
    :cond_0
    iget-object p0, p0, LX/2Ar;->b:LX/2Ar;

    goto :goto_0

    .line 378197
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static f(LX/2Ar;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ar",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 378233
    :goto_0
    if-eqz p0, :cond_1

    .line 378234
    iget-boolean v0, p0, LX/2Ar;->e:Z

    if-eqz v0, :cond_0

    .line 378235
    const/4 v0, 0x1

    .line 378236
    :goto_1
    return v0

    .line 378237
    :cond_0
    iget-object p0, p0, LX/2Ar;->b:LX/2Ar;

    goto :goto_0

    .line 378238
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/2Ap;
    .locals 1

    .prologue
    .line 378232
    new-instance v0, LX/2Ap;

    invoke-direct {v0, p0, p1}, LX/2Ap;-><init>(LX/2Ap;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 378231
    iget-object v0, p0, LX/2Ap;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/2Am;Ljava/lang/String;ZZ)V
    .locals 6

    .prologue
    .line 378229
    new-instance v0, LX/2Ar;

    iget-object v2, p0, LX/2Ap;->e:LX/2Ar;

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/2Ar;-><init>(Ljava/lang/Object;LX/2Ar;Ljava/lang/String;ZZ)V

    iput-object v0, p0, LX/2Ap;->e:LX/2Ar;

    .line 378230
    return-void
.end method

.method public final a(LX/2Ap;)V
    .locals 2

    .prologue
    .line 378224
    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    iget-object v1, p1, LX/2Ap;->e:LX/2Ar;

    invoke-static {v0, v1}, LX/2Ap;->a(LX/2Ar;LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->e:LX/2Ar;

    .line 378225
    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    iget-object v1, p1, LX/2Ap;->f:LX/2Ar;

    invoke-static {v0, v1}, LX/2Ap;->a(LX/2Ar;LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->f:LX/2Ar;

    .line 378226
    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    iget-object v1, p1, LX/2Ap;->g:LX/2Ar;

    invoke-static {v0, v1}, LX/2Ap;->a(LX/2Ar;LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->g:LX/2Ar;

    .line 378227
    iget-object v0, p0, LX/2Ap;->h:LX/2Ar;

    iget-object v1, p1, LX/2Ap;->h:LX/2Ar;

    invoke-static {v0, v1}, LX/2Ap;->a(LX/2Ar;LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->h:LX/2Ar;

    .line 378228
    return-void
.end method

.method public final a(LX/2At;Ljava/lang/String;ZZ)V
    .locals 6

    .prologue
    .line 378222
    new-instance v0, LX/2Ar;

    iget-object v2, p0, LX/2Ap;->g:LX/2Ar;

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/2Ar;-><init>(Ljava/lang/Object;LX/2Ar;Ljava/lang/String;ZZ)V

    iput-object v0, p0, LX/2Ap;->g:LX/2Ar;

    .line 378223
    return-void
.end method

.method public final a(LX/2Vd;Ljava/lang/String;ZZ)V
    .locals 6

    .prologue
    .line 378220
    new-instance v0, LX/2Ar;

    iget-object v2, p0, LX/2Ap;->f:LX/2Ar;

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/2Ar;-><init>(Ljava/lang/Object;LX/2Ar;Ljava/lang/String;ZZ)V

    iput-object v0, p0, LX/2Ap;->f:LX/2Ar;

    .line 378221
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 378214
    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->b(LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->g:LX/2Ar;

    .line 378215
    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->b(LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->f:LX/2Ar;

    .line 378216
    if-nez p1, :cond_0

    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    if-nez v0, :cond_1

    .line 378217
    :cond_0
    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->b(LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->e:LX/2Ar;

    .line 378218
    iget-object v0, p0, LX/2Ap;->h:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->b(LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->h:LX/2Ar;

    .line 378219
    :cond_1
    return-void
.end method

.method public final b()LX/2Vb;
    .locals 1

    .prologue
    .line 378198
    invoke-virtual {p0}, LX/2Aq;->o()LX/2An;

    move-result-object v0

    .line 378199
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Ap;->b:LX/0lU;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 378200
    :cond_1
    const/4 v0, 0x0

    move-object v0, v0

    .line 378201
    goto :goto_0
.end method

.method public final b(LX/2At;Ljava/lang/String;ZZ)V
    .locals 6

    .prologue
    .line 378202
    new-instance v0, LX/2Ar;

    iget-object v2, p0, LX/2Ap;->h:LX/2Ar;

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/2Ar;-><init>(Ljava/lang/Object;LX/2Ar;Ljava/lang/String;ZZ)V

    iput-object v0, p0, LX/2Ap;->h:LX/2Ar;

    .line 378203
    return-void
.end method

.method public final b(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 378118
    if-eqz p1, :cond_2

    .line 378119
    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    if-eqz v0, :cond_1

    .line 378120
    new-array v0, v1, [LX/2Ar;

    iget-object v1, p0, LX/2Ap;->g:LX/2Ar;

    aput-object v1, v0, v2

    iget-object v1, p0, LX/2Ap;->e:LX/2Ar;

    aput-object v1, v0, v3

    iget-object v1, p0, LX/2Ap;->f:LX/2Ar;

    aput-object v1, v0, v4

    iget-object v1, p0, LX/2Ap;->h:LX/2Ar;

    aput-object v1, v0, v5

    invoke-direct {p0, v2, v0}, LX/2Ap;->a(I[LX/2Ar;)LX/0lP;

    move-result-object v1

    .line 378121
    iget-object v2, p0, LX/2Ap;->g:LX/2Ar;

    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2At;

    invoke-virtual {v0, v1}, LX/2At;->a(LX/0lP;)LX/2At;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2Ar;->a(Ljava/lang/Object;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->g:LX/2Ar;

    .line 378122
    :cond_0
    :goto_0
    return-void

    .line 378123
    :cond_1
    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    if-eqz v0, :cond_0

    .line 378124
    new-array v0, v5, [LX/2Ar;

    iget-object v1, p0, LX/2Ap;->e:LX/2Ar;

    aput-object v1, v0, v2

    iget-object v1, p0, LX/2Ap;->f:LX/2Ar;

    aput-object v1, v0, v3

    iget-object v1, p0, LX/2Ap;->h:LX/2Ar;

    aput-object v1, v0, v4

    invoke-direct {p0, v2, v0}, LX/2Ap;->a(I[LX/2Ar;)LX/0lP;

    move-result-object v1

    .line 378125
    iget-object v2, p0, LX/2Ap;->e:LX/2Ar;

    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2Am;

    invoke-virtual {v0, v1}, LX/2Am;->a(LX/0lP;)LX/2Am;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2Ar;->a(Ljava/lang/Object;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->e:LX/2Ar;

    goto :goto_0

    .line 378126
    :cond_2
    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    if-eqz v0, :cond_3

    .line 378127
    new-array v0, v1, [LX/2Ar;

    iget-object v1, p0, LX/2Ap;->f:LX/2Ar;

    aput-object v1, v0, v2

    iget-object v1, p0, LX/2Ap;->h:LX/2Ar;

    aput-object v1, v0, v3

    iget-object v1, p0, LX/2Ap;->e:LX/2Ar;

    aput-object v1, v0, v4

    iget-object v1, p0, LX/2Ap;->g:LX/2Ar;

    aput-object v1, v0, v5

    invoke-direct {p0, v2, v0}, LX/2Ap;->a(I[LX/2Ar;)LX/0lP;

    move-result-object v1

    .line 378128
    iget-object v2, p0, LX/2Ap;->f:LX/2Ar;

    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2Vd;

    invoke-virtual {v0, v1}, LX/2Vd;->a(LX/0lP;)LX/2Vd;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2Ar;->a(Ljava/lang/Object;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->f:LX/2Ar;

    goto :goto_0

    .line 378129
    :cond_3
    iget-object v0, p0, LX/2Ap;->h:LX/2Ar;

    if-eqz v0, :cond_4

    .line 378130
    new-array v0, v5, [LX/2Ar;

    iget-object v1, p0, LX/2Ap;->h:LX/2Ar;

    aput-object v1, v0, v2

    iget-object v1, p0, LX/2Ap;->e:LX/2Ar;

    aput-object v1, v0, v3

    iget-object v1, p0, LX/2Ap;->g:LX/2Ar;

    aput-object v1, v0, v4

    invoke-direct {p0, v2, v0}, LX/2Ap;->a(I[LX/2Ar;)LX/0lP;

    move-result-object v1

    .line 378131
    iget-object v2, p0, LX/2Ap;->h:LX/2Ar;

    iget-object v0, p0, LX/2Ap;->h:LX/2Ar;

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2At;

    invoke-virtual {v0, v1}, LX/2At;->a(LX/0lP;)LX/2At;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2Ar;->a(Ljava/lang/Object;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->h:LX/2Ar;

    goto :goto_0

    .line 378132
    :cond_4
    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    if-eqz v0, :cond_0

    .line 378133
    new-array v0, v4, [LX/2Ar;

    iget-object v1, p0, LX/2Ap;->e:LX/2Ar;

    aput-object v1, v0, v2

    iget-object v1, p0, LX/2Ap;->g:LX/2Ar;

    aput-object v1, v0, v3

    invoke-direct {p0, v2, v0}, LX/2Ap;->a(I[LX/2Ar;)LX/0lP;

    move-result-object v1

    .line 378134
    iget-object v2, p0, LX/2Ap;->e:LX/2Ar;

    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2Am;

    invoke-virtual {v0, v1}, LX/2Am;->a(LX/0lP;)LX/2Am;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2Ar;->a(Ljava/lang/Object;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->e:LX/2Ar;

    goto/16 :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 378085
    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->d(LX/2Ar;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->d(LX/2Ar;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Ap;->h:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->d(LX/2Ar;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->d(LX/2Ar;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 378078
    check-cast p1, LX/2Ap;

    invoke-direct {p0, p1}, LX/2Ap;->b(LX/2Ap;)I

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 378077
    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 378076
    iget-object v0, p0, LX/2Ap;->h:LX/2Ar;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 378075
    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 378074
    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()LX/2At;
    .locals 6

    .prologue
    .line 378060
    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    if-nez v0, :cond_1

    .line 378061
    const/4 v1, 0x0

    .line 378062
    :cond_0
    return-object v1

    .line 378063
    :cond_1
    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2At;

    .line 378064
    iget-object v1, p0, LX/2Ap;->g:LX/2Ar;

    iget-object v1, v1, LX/2Ar;->b:LX/2Ar;

    move-object v2, v1

    move-object v1, v0

    .line 378065
    :goto_0
    if-eqz v2, :cond_0

    .line 378066
    iget-object v0, v2, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2At;

    .line 378067
    invoke-virtual {v1}, LX/2An;->i()Ljava/lang/Class;

    move-result-object v3

    .line 378068
    invoke-virtual {v0}, LX/2An;->i()Ljava/lang/Class;

    move-result-object v4

    .line 378069
    if-eq v3, v4, :cond_3

    .line 378070
    invoke-virtual {v3, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 378071
    :goto_1
    iget-object v1, v2, LX/2Ar;->b:LX/2Ar;

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 378072
    :cond_2
    invoke-virtual {v4, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 378073
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Conflicting getter definitions for property \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, LX/2At;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " vs "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, LX/2At;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public final j()LX/2At;
    .locals 6

    .prologue
    .line 378046
    iget-object v0, p0, LX/2Ap;->h:LX/2Ar;

    if-nez v0, :cond_1

    .line 378047
    const/4 v1, 0x0

    .line 378048
    :cond_0
    return-object v1

    .line 378049
    :cond_1
    iget-object v0, p0, LX/2Ap;->h:LX/2Ar;

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2At;

    .line 378050
    iget-object v1, p0, LX/2Ap;->h:LX/2Ar;

    iget-object v1, v1, LX/2Ar;->b:LX/2Ar;

    move-object v2, v1

    move-object v1, v0

    .line 378051
    :goto_0
    if-eqz v2, :cond_0

    .line 378052
    iget-object v0, v2, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2At;

    .line 378053
    invoke-virtual {v1}, LX/2An;->i()Ljava/lang/Class;

    move-result-object v3

    .line 378054
    invoke-virtual {v0}, LX/2An;->i()Ljava/lang/Class;

    move-result-object v4

    .line 378055
    if-eq v3, v4, :cond_3

    .line 378056
    invoke-virtual {v3, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 378057
    :goto_1
    iget-object v1, v2, LX/2Ar;->b:LX/2Ar;

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 378058
    :cond_2
    invoke-virtual {v4, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 378059
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Conflicting setter definitions for property \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, LX/2At;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " vs "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, LX/2At;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public final k()LX/2Am;
    .locals 6

    .prologue
    .line 378032
    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    if-nez v0, :cond_1

    .line 378033
    const/4 v1, 0x0

    .line 378034
    :cond_0
    return-object v1

    .line 378035
    :cond_1
    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2Am;

    .line 378036
    iget-object v1, p0, LX/2Ap;->e:LX/2Ar;

    iget-object v1, v1, LX/2Ar;->b:LX/2Ar;

    move-object v2, v1

    move-object v1, v0

    .line 378037
    :goto_0
    if-eqz v2, :cond_0

    .line 378038
    iget-object v0, v2, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2Am;

    .line 378039
    invoke-virtual {v1}, LX/2An;->i()Ljava/lang/Class;

    move-result-object v3

    .line 378040
    invoke-virtual {v0}, LX/2An;->i()Ljava/lang/Class;

    move-result-object v4

    .line 378041
    if-eq v3, v4, :cond_3

    .line 378042
    invoke-virtual {v3, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 378043
    :goto_1
    iget-object v1, v2, LX/2Ar;->b:LX/2Ar;

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 378044
    :cond_2
    invoke-virtual {v4, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 378045
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Multiple fields representing property \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, LX/2Am;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " vs "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, LX/2Am;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public final l()LX/2Vd;
    .locals 3

    .prologue
    .line 378021
    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    if-nez v0, :cond_0

    .line 378022
    const/4 v0, 0x0

    .line 378023
    :goto_0
    return-object v0

    .line 378024
    :cond_0
    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    move-object v1, v0

    .line 378025
    :goto_1
    iget-object v0, v1, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2Vd;

    .line 378026
    iget-object v2, v0, LX/2Vd;->_owner:LX/2Au;

    move-object v0, v2

    .line 378027
    instance-of v0, v0, LX/2Vc;

    if-eqz v0, :cond_1

    .line 378028
    iget-object v0, v1, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2Vd;

    goto :goto_0

    .line 378029
    :cond_1
    iget-object v0, v1, LX/2Ar;->b:LX/2Ar;

    .line 378030
    if-nez v0, :cond_2

    .line 378031
    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    iget-object v0, v0, LX/2Ar;->a:Ljava/lang/Object;

    check-cast v0, LX/2Vd;

    goto :goto_0

    :cond_2
    move-object v1, v0

    goto :goto_1
.end method

.method public final m()LX/2An;
    .locals 1

    .prologue
    .line 378017
    invoke-virtual {p0}, LX/2Aq;->i()LX/2At;

    move-result-object v0

    .line 378018
    if-nez v0, :cond_0

    .line 378019
    invoke-virtual {p0}, LX/2Aq;->k()LX/2Am;

    move-result-object v0

    .line 378020
    :cond_0
    return-object v0
.end method

.method public final n()LX/2An;
    .locals 1

    .prologue
    .line 378079
    invoke-virtual {p0}, LX/2Aq;->l()LX/2Vd;

    move-result-object v0

    .line 378080
    if-nez v0, :cond_0

    .line 378081
    invoke-virtual {p0}, LX/2Aq;->j()LX/2At;

    move-result-object v0

    .line 378082
    if-nez v0, :cond_0

    .line 378083
    invoke-virtual {p0}, LX/2Aq;->k()LX/2Am;

    move-result-object v0

    .line 378084
    :cond_0
    return-object v0
.end method

.method public final o()LX/2An;
    .locals 1

    .prologue
    .line 378086
    iget-boolean v0, p0, LX/2Ap;->a:Z

    if-eqz v0, :cond_0

    .line 378087
    invoke-virtual {p0}, LX/2Aq;->m()LX/2An;

    move-result-object v0

    .line 378088
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/2Aq;->n()LX/2An;

    move-result-object v0

    goto :goto_0
.end method

.method public final p()[Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 378089
    new-instance v0, LX/2B8;

    invoke-direct {v0, p0}, LX/2B8;-><init>(LX/2Ap;)V

    invoke-direct {p0, v0}, LX/2Ap;->a(LX/2B1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Class;

    return-object v0
.end method

.method public final q()LX/4pn;
    .locals 1

    .prologue
    .line 378090
    new-instance v0, LX/2B2;

    invoke-direct {v0, p0}, LX/2B2;-><init>(LX/2Ap;)V

    invoke-direct {p0, v0}, LX/2Ap;->a(LX/2B1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4pn;

    return-object v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 378091
    new-instance v0, LX/2B0;

    invoke-direct {v0, p0}, LX/2B0;-><init>(LX/2Ap;)V

    invoke-direct {p0, v0}, LX/2Ap;->a(LX/2B1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 378092
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 378093
    new-instance v0, LX/2B4;

    invoke-direct {v0, p0}, LX/2B4;-><init>(LX/2Ap;)V

    invoke-direct {p0, v0}, LX/2Ap;->a(LX/2B1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 378094
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 378095
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 378096
    const-string v1, "[Property \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2Ap;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'; ctors: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2Ap;->f:LX/2Ar;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", field(s): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2Ap;->e:LX/2Ar;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", getter(s): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2Ap;->g:LX/2Ar;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", setter(s): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2Ap;->h:LX/2Ar;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 378097
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378098
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 378099
    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->a(LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->e:LX/2Ar;

    .line 378100
    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->a(LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->g:LX/2Ar;

    .line 378101
    iget-object v0, p0, LX/2Ap;->h:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->a(LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->h:LX/2Ar;

    .line 378102
    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->a(LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->f:LX/2Ar;

    .line 378103
    return-void
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 378104
    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->c(LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->e:LX/2Ar;

    .line 378105
    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->c(LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->g:LX/2Ar;

    .line 378106
    iget-object v0, p0, LX/2Ap;->h:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->c(LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->h:LX/2Ar;

    .line 378107
    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->c(LX/2Ar;)LX/2Ar;

    move-result-object v0

    iput-object v0, p0, LX/2Ap;->f:LX/2Ar;

    .line 378108
    return-void
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 378109
    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->e(LX/2Ar;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->e(LX/2Ar;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Ap;->h:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->e(LX/2Ar;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->e(LX/2Ar;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 378110
    iget-object v0, p0, LX/2Ap;->e:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->f(LX/2Ar;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Ap;->g:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->f(LX/2Ar;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Ap;->h:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->f(LX/2Ar;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Ap;->f:LX/2Ar;

    invoke-static {v0}, LX/2Ap;->f(LX/2Ar;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 378111
    iget-object v1, p0, LX/2Ap;->e:LX/2Ar;

    invoke-direct {p0, v1, v0}, LX/2Ap;->b(LX/2Ar;LX/2Ar;)LX/2Ar;

    move-result-object v1

    .line 378112
    iget-object v2, p0, LX/2Ap;->g:LX/2Ar;

    invoke-direct {p0, v2, v1}, LX/2Ap;->b(LX/2Ar;LX/2Ar;)LX/2Ar;

    move-result-object v1

    .line 378113
    iget-object v2, p0, LX/2Ap;->h:LX/2Ar;

    invoke-direct {p0, v2, v1}, LX/2Ap;->b(LX/2Ar;LX/2Ar;)LX/2Ar;

    move-result-object v1

    .line 378114
    iget-object v2, p0, LX/2Ap;->f:LX/2Ar;

    invoke-direct {p0, v2, v1}, LX/2Ap;->b(LX/2Ar;LX/2Ar;)LX/2Ar;

    move-result-object v1

    .line 378115
    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v1, LX/2Ar;->c:Ljava/lang/String;

    goto :goto_0
.end method
