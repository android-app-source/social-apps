.class public final LX/2jd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V
    .locals 0

    .prologue
    .line 454169
    iput-object p1, p0, LX/2jd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 454157
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 454158
    iget-object v2, p0, LX/2jd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    .line 454159
    iput p2, v2, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->M:I

    .line 454160
    iget-object v2, p0, LX/2jd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    .line 454161
    iput p3, v2, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->N:I

    .line 454162
    add-int v2, p2, p3

    add-int/lit8 v2, v2, 0x3

    if-lt v2, p4, :cond_1

    move v2, v0

    .line 454163
    :goto_0
    if-eqz v2, :cond_2

    iget-object v2, p0, LX/2jd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-boolean v2, v2, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->I:Z

    if-eqz v2, :cond_2

    .line 454164
    :goto_1
    if-eqz v0, :cond_0

    .line 454165
    iget-object v0, p0, LX/2jd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v1, p0, LX/2jd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v1, v1, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->o:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 454166
    :cond_0
    return-void

    :cond_1
    move v2, v1

    .line 454167
    goto :goto_0

    :cond_2
    move v0, v1

    .line 454168
    goto :goto_1
.end method
