.class public final enum LX/23m;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/23m;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/23m;

.field public static final enum INSTALL_NOW:LX/23m;

.field public static final enum NOT_IN_EXPERIMENT:LX/23m;

.field public static final enum REDIRECT:LX/23m;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 365126
    new-instance v0, LX/23m;

    const-string v1, "NOT_IN_EXPERIMENT"

    invoke-direct {v0, v1, v2}, LX/23m;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/23m;->NOT_IN_EXPERIMENT:LX/23m;

    .line 365127
    new-instance v0, LX/23m;

    const-string v1, "REDIRECT"

    invoke-direct {v0, v1, v3}, LX/23m;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/23m;->REDIRECT:LX/23m;

    .line 365128
    new-instance v0, LX/23m;

    const-string v1, "INSTALL_NOW"

    invoke-direct {v0, v1, v4}, LX/23m;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/23m;->INSTALL_NOW:LX/23m;

    .line 365129
    const/4 v0, 0x3

    new-array v0, v0, [LX/23m;

    sget-object v1, LX/23m;->NOT_IN_EXPERIMENT:LX/23m;

    aput-object v1, v0, v2

    sget-object v1, LX/23m;->REDIRECT:LX/23m;

    aput-object v1, v0, v3

    sget-object v1, LX/23m;->INSTALL_NOW:LX/23m;

    aput-object v1, v0, v4

    sput-object v0, LX/23m;->$VALUES:[LX/23m;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 365130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/23m;
    .locals 1

    .prologue
    .line 365131
    const-class v0, LX/23m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/23m;

    return-object v0
.end method

.method public static values()[LX/23m;
    .locals 1

    .prologue
    .line 365132
    sget-object v0, LX/23m;->$VALUES:[LX/23m;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/23m;

    return-object v0
.end method
