.class public LX/361;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CDN;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/361",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CDN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 497920
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 497921
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/361;->b:LX/0Zi;

    .line 497922
    iput-object p1, p0, LX/361;->a:LX/0Ot;

    .line 497923
    return-void
.end method

.method public static a(LX/0QB;)LX/361;
    .locals 4

    .prologue
    .line 497909
    const-class v1, LX/361;

    monitor-enter v1

    .line 497910
    :try_start_0
    sget-object v0, LX/361;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 497911
    sput-object v2, LX/361;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 497912
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497913
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 497914
    new-instance v3, LX/361;

    const/16 p0, 0x21eb

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/361;-><init>(LX/0Ot;)V

    .line 497915
    move-object v0, v3

    .line 497916
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 497917
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/361;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 497918
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 497919
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 497907
    invoke-static {}, LX/1dS;->b()V

    .line 497908
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6654cfca

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 497902
    check-cast p6, LX/CDL;

    .line 497903
    iget-object v1, p0, LX/361;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v1, p6, LX/CDL;->e:LX/3FN;

    .line 497904
    iget-object v2, v1, LX/3FN;->p:LX/3FO;

    iget v2, v2, LX/3FO;->a:I

    int-to-float v2, v2

    iget-object p0, v1, LX/3FN;->p:LX/3FO;

    iget p0, p0, LX/3FO;->b:I

    int-to-float p0, p0

    div-float/2addr v2, p0

    .line 497905
    invoke-static {p3, p4, v2, p5}, LX/1oC;->a(IIFLX/1no;)V

    .line 497906
    const/16 v1, 0x1f

    const v2, 0xdfc33c9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 497899
    iget-object v0, p0, LX/361;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 497900
    new-instance v0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    invoke-direct {v0, p1}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 497901
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 11

    .prologue
    .line 497843
    check-cast p2, LX/CDL;

    .line 497844
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 497845
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 497846
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v7

    .line 497847
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v8

    .line 497848
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v9

    .line 497849
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v10

    .line 497850
    iget-object v0, p0, LX/361;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDN;

    iget-object v1, p2, LX/CDL;->a:LX/3EE;

    iget-object v2, p2, LX/CDL;->b:LX/1Pe;

    iget-object v3, p2, LX/CDL;->c:LX/3FV;

    iget-object v4, p2, LX/CDL;->d:Landroid/view/View$OnClickListener;

    invoke-virtual/range {v0 .. v10}, LX/CDN;->a(LX/3EE;LX/1Pe;LX/3FV;Landroid/view/View$OnClickListener;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V

    .line 497851
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 497852
    check-cast v0, LX/3FN;

    iput-object v0, p2, LX/CDL;->e:LX/3FN;

    .line 497853
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 497854
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 497855
    check-cast v0, LX/3FW;

    iput-object v0, p2, LX/CDL;->f:LX/3FW;

    .line 497856
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 497857
    iget-object v0, v7, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 497858
    check-cast v0, LX/3FX;

    iput-object v0, p2, LX/CDL;->g:LX/3FX;

    .line 497859
    invoke-static {v7}, LX/1cy;->a(LX/1np;)V

    .line 497860
    iget-object v0, v8, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 497861
    check-cast v0, LX/3FY;

    iput-object v0, p2, LX/CDL;->h:LX/3FY;

    .line 497862
    invoke-static {v8}, LX/1cy;->a(LX/1np;)V

    .line 497863
    iget-object v0, v9, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 497864
    check-cast v0, LX/3FZ;

    iput-object v0, p2, LX/CDL;->i:LX/3FZ;

    .line 497865
    invoke-static {v9}, LX/1cy;->a(LX/1np;)V

    .line 497866
    iget-object v0, v10, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 497867
    check-cast v0, LX/3Fa;

    iput-object v0, p2, LX/CDL;->j:LX/3Fa;

    .line 497868
    invoke-static {v10}, LX/1cy;->a(LX/1np;)V

    .line 497869
    return-void
.end method

.method public final c(LX/1De;)LX/CDK;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/361",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 497891
    new-instance v1, LX/CDL;

    invoke-direct {v1, p0}, LX/CDL;-><init>(LX/361;)V

    .line 497892
    iget-object v2, p0, LX/361;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CDK;

    .line 497893
    if-nez v2, :cond_0

    .line 497894
    new-instance v2, LX/CDK;

    invoke-direct {v2, p0}, LX/CDK;-><init>(LX/361;)V

    .line 497895
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/CDK;->a$redex0(LX/CDK;LX/1De;IILX/CDL;)V

    .line 497896
    move-object v1, v2

    .line 497897
    move-object v0, v1

    .line 497898
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 497890
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 4

    .prologue
    .line 497887
    check-cast p3, LX/CDL;

    .line 497888
    iget-object v0, p0, LX/361;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDN;

    check-cast p2, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    iget-object v1, p3, LX/CDL;->e:LX/3FN;

    iget-object v2, p3, LX/CDL;->f:LX/3FW;

    iget-object v3, p3, LX/CDL;->a:LX/3EE;

    invoke-virtual {v0, p2, v1, v2, v3}, LX/CDN;->a(Lcom/facebook/feedplugins/video/RichVideoAttachmentView;LX/3FN;LX/3FW;LX/3EE;)V

    .line 497889
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 497886
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 3

    .prologue
    .line 497881
    check-cast p3, LX/CDL;

    .line 497882
    iget-object v0, p0, LX/361;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDN;

    check-cast p2, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    iget-object v1, p3, LX/CDL;->e:LX/3FN;

    iget-object v2, p3, LX/CDL;->a:LX/3EE;

    .line 497883
    iget-object p0, v0, LX/CDN;->a:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    invoke-virtual {p0, v2, v1, p2}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->b(LX/3EE;LX/3FN;Landroid/view/View;)V

    .line 497884
    const/4 p0, 0x0

    invoke-virtual {p2, p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 497885
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 8

    .prologue
    .line 497878
    check-cast p3, LX/CDL;

    .line 497879
    iget-object v0, p0, LX/361;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDN;

    move-object v1, p2

    check-cast v1, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    iget-object v2, p3, LX/CDL;->e:LX/3FN;

    iget-object v3, p3, LX/CDL;->g:LX/3FX;

    iget-object v4, p3, LX/CDL;->j:LX/3Fa;

    iget-object v5, p3, LX/CDL;->h:LX/3FY;

    iget-object v6, p3, LX/CDL;->i:LX/3FZ;

    iget-object v7, p3, LX/CDL;->b:LX/1Pe;

    invoke-virtual/range {v0 .. v7}, LX/CDN;->a(Lcom/facebook/feedplugins/video/RichVideoAttachmentView;LX/3FN;LX/3FX;LX/3Fa;LX/3FY;LX/3FZ;LX/1Pe;)V

    .line 497880
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 7

    .prologue
    .line 497871
    check-cast p3, LX/CDL;

    .line 497872
    iget-object v0, p0, LX/361;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDN;

    move-object v1, p2

    check-cast v1, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    iget-object v2, p3, LX/CDL;->e:LX/3FN;

    iget-object v3, p3, LX/CDL;->j:LX/3Fa;

    iget-object v4, p3, LX/CDL;->h:LX/3FY;

    iget-object v5, p3, LX/CDL;->i:LX/3FZ;

    iget-object v6, p3, LX/CDL;->b:LX/1Pe;

    .line 497873
    iget-object p0, v2, LX/3FN;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 497874
    invoke-interface {v6, p0}, LX/1Pe;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 497875
    invoke-static {v3}, LX/3Fb;->a(LX/3Fa;)V

    .line 497876
    invoke-static {v5, v4, v1}, LX/2mi;->b(LX/3FZ;LX/3FY;Landroid/view/View;)V

    .line 497877
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 497870
    const/4 v0, 0x3

    return v0
.end method
