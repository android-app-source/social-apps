.class public final LX/3FF;
.super LX/0eB;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtIncompatible;
    value = "TODO"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0eB",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/CancellationException;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 538675
    invoke-direct {p0}, LX/0eB;-><init>()V

    .line 538676
    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v1, "Immediate cancelled future."

    invoke-direct {v0, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/3FF;->a:Ljava/util/concurrent/CancellationException;

    .line 538677
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 538679
    const-string v0, "Task was cancelled."

    iget-object v1, p0, LX/3FF;->a:Ljava/util/concurrent/CancellationException;

    invoke-static {v0, v1}, LX/0SQ;->cancellationExceptionWithCause(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/util/concurrent/CancellationException;

    move-result-object v0

    throw v0
.end method

.method public final isCancelled()Z
    .locals 1

    .prologue
    .line 538678
    const/4 v0, 0x1

    return v0
.end method
