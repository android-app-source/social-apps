.class public LX/29A;
.super LX/16B;
.source ""

# interfaces
.implements LX/0Up;
.implements LX/2C2;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile C:LX/29A;

.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:Z

.field public final B:Landroid/content/ServiceConnection;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field private final e:Ljava/util/concurrent/ScheduledExecutorService;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2HQ;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2XP;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/29D;

.field public final j:LX/0Xl;

.field public final k:LX/1fU;

.field public final l:LX/2Bs;

.field public final m:LX/1MZ;

.field public final n:LX/00G;

.field public final o:LX/0ZR;

.field public final p:LX/0So;

.field private final q:LX/0ad;

.field private final r:LX/03V;

.field public final s:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
    .end annotation
.end field

.field public final t:Landroid/os/Looper;
    .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
    .end annotation
.end field

.field public u:LX/1tH;

.field public v:LX/0Yd;

.field private w:Z

.field public x:Ljava/lang/String;

.field private y:Ljava/util/concurrent/ScheduledFuture;

.field private final z:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 375831
    const-class v0, LX/29A;

    sput-object v0, LX/29A;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;Landroid/content/Context;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;LX/0Or;Ljava/util/Set;LX/29D;LX/0Xl;LX/1fU;LX/2Bs;LX/1MZ;LX/00G;LX/0ZR;LX/0So;LX/0ad;LX/03V;Landroid/os/Handler;Landroid/os/Looper;)V
    .locals 4
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/push/mqtt/DisableMqttPushService;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/push/mqtt/annotations/ForMqttThreadWakeup;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/mqttlite/persistence/HighestMqttPersistence;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p9    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p18    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
        .end annotation
    .end param
    .param p19    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0Or",
            "<",
            "LX/2HQ;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/2XP;",
            ">;",
            "LX/29D;",
            "LX/0Xl;",
            "LX/1fU;",
            "LX/2Bs;",
            "LX/1MZ;",
            "LX/00G;",
            "LX/0ZR;",
            "LX/0So;",
            "LX/0ad;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/os/Handler;",
            "Landroid/os/Looper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 375872
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 375873
    new-instance v1, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$1;

    sget-object v2, LX/29A;->a:Ljava/lang/Class;

    const-string v3, "stopService"

    invoke-direct {v1, p0, v2, v3}, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$1;-><init>(LX/29A;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v1, p0, LX/29A;->z:Ljava/lang/Runnable;

    .line 375874
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/29A;->A:Z

    .line 375875
    new-instance v1, LX/29G;

    invoke-direct {v1, p0}, LX/29G;-><init>(LX/29A;)V

    iput-object v1, p0, LX/29A;->B:Landroid/content/ServiceConnection;

    .line 375876
    iput-object p1, p0, LX/29A;->b:LX/0Or;

    .line 375877
    iput-object p2, p0, LX/29A;->c:Landroid/content/Context;

    .line 375878
    iput-object p3, p0, LX/29A;->d:Ljava/util/concurrent/ExecutorService;

    .line 375879
    iput-object p4, p0, LX/29A;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 375880
    iput-object p5, p0, LX/29A;->f:LX/0Or;

    .line 375881
    iput-object p6, p0, LX/29A;->g:LX/0Or;

    .line 375882
    iput-object p7, p0, LX/29A;->h:Ljava/util/Set;

    .line 375883
    iput-object p8, p0, LX/29A;->i:LX/29D;

    .line 375884
    iput-object p9, p0, LX/29A;->j:LX/0Xl;

    .line 375885
    iput-object p10, p0, LX/29A;->k:LX/1fU;

    .line 375886
    iput-object p11, p0, LX/29A;->l:LX/2Bs;

    .line 375887
    move-object/from16 v0, p13

    iput-object v0, p0, LX/29A;->n:LX/00G;

    .line 375888
    move-object/from16 v0, p12

    iput-object v0, p0, LX/29A;->m:LX/1MZ;

    .line 375889
    move-object/from16 v0, p14

    iput-object v0, p0, LX/29A;->o:LX/0ZR;

    .line 375890
    move-object/from16 v0, p15

    iput-object v0, p0, LX/29A;->p:LX/0So;

    .line 375891
    move-object/from16 v0, p16

    iput-object v0, p0, LX/29A;->q:LX/0ad;

    .line 375892
    move-object/from16 v0, p17

    iput-object v0, p0, LX/29A;->r:LX/03V;

    .line 375893
    move-object/from16 v0, p18

    iput-object v0, p0, LX/29A;->s:Landroid/os/Handler;

    .line 375894
    move-object/from16 v0, p19

    iput-object v0, p0, LX/29A;->t:Landroid/os/Looper;

    .line 375895
    return-void
.end method

.method private A()J
    .locals 6

    .prologue
    .line 375871
    iget-object v0, p0, LX/29A;->q:LX/0ad;

    sget-wide v2, LX/2IF;->b:J

    const-wide/16 v4, 0x1e0

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public static a(LX/0QB;)LX/29A;
    .locals 3

    .prologue
    .line 375861
    sget-object v0, LX/29A;->C:LX/29A;

    if-nez v0, :cond_1

    .line 375862
    const-class v1, LX/29A;

    monitor-enter v1

    .line 375863
    :try_start_0
    sget-object v0, LX/29A;->C:LX/29A;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 375864
    if-eqz v2, :cond_0

    .line 375865
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/29A;->b(LX/0QB;)LX/29A;

    move-result-object v0

    sput-object v0, LX/29A;->C:LX/29A;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375866
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 375867
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 375868
    :cond_1
    sget-object v0, LX/29A;->C:LX/29A;

    return-object v0

    .line 375869
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 375870
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/29A;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 375848
    invoke-direct {p0}, LX/29A;->p()V

    .line 375849
    iget-object v0, p0, LX/29A;->u:LX/1tH;

    if-nez v0, :cond_1

    .line 375850
    iget-object v0, p0, LX/29A;->l:LX/2Bs;

    iget-object v1, p0, LX/29A;->c:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v3, p0, LX/29A;->B:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, LX/2Bs;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;ILjava/lang/String;)LX/2XW;

    move-result-object v0

    .line 375851
    iget-object v1, v0, LX/2XW;->b:Landroid/os/IBinder;

    if-eqz v1, :cond_0

    .line 375852
    iget-object v0, v0, LX/2XW;->b:Landroid/os/IBinder;

    invoke-static {p0, v0}, LX/29A;->a$redex0(LX/29A;Landroid/os/IBinder;)V

    .line 375853
    :cond_0
    :goto_0
    return-void

    .line 375854
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 375855
    if-eqz p2, :cond_2

    .line 375856
    const-string v1, "Orca.PERSISTENT_KICK_SKIP_PING"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 375857
    :goto_1
    const-string v1, "caller"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 375858
    iget-object v1, p0, LX/29A;->l:LX/2Bs;

    iget-object v2, p0, LX/29A;->c:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, LX/2Bs;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 375859
    goto :goto_0

    .line 375860
    :cond_2
    const-string v1, "Orca.PERSISTENT_KICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public static a$redex0(LX/29A;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 375839
    invoke-static {p0}, LX/29A;->x(LX/29A;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375840
    const-string v0, "EXPIRED_SESSION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375841
    const-string v0, "EXPIRED_SESSION"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 375842
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "Orca.EXPIRE_CONNECTION"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "EXPIRED_SESSION"

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    .line 375843
    iget-object v3, p0, LX/29A;->l:LX/2Bs;

    iget-object p1, p0, LX/29A;->c:Landroid/content/Context;

    invoke-virtual {v3, p1, v2}, LX/2Bs;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 375844
    :goto_0
    return-void

    .line 375845
    :cond_0
    const-string v0, "EXTRA_SKIP_PING"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 375846
    const-string v1, "onWakeupBroadcast"

    invoke-static {p0, v1, v0}, LX/29A;->a(LX/29A;Ljava/lang/String;Z)V

    goto :goto_0

    .line 375847
    :cond_1
    invoke-direct {p0}, LX/29A;->o()V

    goto :goto_0
.end method

.method public static a$redex0(LX/29A;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 375836
    invoke-static {p1}, LX/1tG;->a(Landroid/os/IBinder;)LX/1tH;

    move-result-object v0

    iput-object v0, p0, LX/29A;->u:LX/1tH;

    .line 375837
    iget-object v0, p0, LX/29A;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$10;

    invoke-direct {v1, p0}, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$10;-><init>(LX/29A;)V

    const v2, -0x5db268c9

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 375838
    return-void
.end method

.method public static a$redex0(LX/29A;Z)V
    .locals 1

    .prologue
    .line 375832
    iget-object v0, p0, LX/29A;->n:LX/00G;

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375833
    invoke-static {p0, p1}, LX/29A;->b(LX/29A;Z)V

    .line 375834
    :goto_0
    return-void

    .line 375835
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/29A;
    .locals 21

    .prologue
    .line 375803
    new-instance v1, LX/29A;

    const/16 v2, 0x155b

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/29B;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/29C;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ScheduledExecutorService;

    const/16 v6, 0xe0b

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/2C9;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/29D;->a(LX/0QB;)LX/29D;

    move-result-object v9

    check-cast v9, LX/29D;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v10

    check-cast v10, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/1fU;->a(LX/0QB;)LX/1fU;

    move-result-object v11

    check-cast v11, LX/1fU;

    invoke-static/range {p0 .. p0}, LX/2Bs;->a(LX/0QB;)LX/2Bs;

    move-result-object v12

    check-cast v12, LX/2Bs;

    invoke-static/range {p0 .. p0}, LX/1MZ;->a(LX/0QB;)LX/1MZ;

    move-result-object v13

    check-cast v13, LX/1MZ;

    invoke-static/range {p0 .. p0}, LX/0VX;->a(LX/0QB;)LX/00G;

    move-result-object v14

    check-cast v14, LX/00G;

    invoke-static/range {p0 .. p0}, LX/0ZR;->a(LX/0QB;)LX/0ZR;

    move-result-object v15

    check-cast v15, LX/0ZR;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v16

    check-cast v16, LX/0So;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v17

    check-cast v17, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v18

    check-cast v18, LX/03V;

    invoke-static/range {p0 .. p0}, LX/1rR;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v19

    check-cast v19, Landroid/os/Handler;

    invoke-static/range {p0 .. p0}, LX/1rS;->a(LX/0QB;)Landroid/os/Looper;

    move-result-object v20

    check-cast v20, Landroid/os/Looper;

    invoke-direct/range {v1 .. v20}, LX/29A;-><init>(LX/0Or;Landroid/content/Context;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;LX/0Or;Ljava/util/Set;LX/29D;LX/0Xl;LX/1fU;LX/2Bs;LX/1MZ;LX/00G;LX/0ZR;LX/0So;LX/0ad;LX/03V;Landroid/os/Handler;Landroid/os/Looper;)V

    .line 375804
    return-object v1
.end method

.method public static b(LX/29A;Z)V
    .locals 2

    .prologue
    .line 375822
    iget-object v0, p0, LX/29A;->n:LX/00G;

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 375823
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 375824
    iget-boolean v0, p0, LX/29A;->w:Z

    if-eq v0, p1, :cond_0

    .line 375825
    iput-boolean p1, p0, LX/29A;->w:Z

    .line 375826
    iget-object v0, p0, LX/29A;->i:LX/29D;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/29D;->a(ZLjava/lang/String;)V

    .line 375827
    :cond_0
    invoke-static {p0}, LX/29A;->x(LX/29A;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375828
    const-string v0, "setEnabledForMainProcess"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/29A;->a(LX/29A;Ljava/lang/String;Z)V

    .line 375829
    :goto_0
    return-void

    .line 375830
    :cond_1
    invoke-static {p0}, LX/29A;->n(LX/29A;)V

    goto :goto_0
.end method

.method public static n(LX/29A;)V
    .locals 4

    .prologue
    .line 375814
    iget-object v0, p0, LX/29A;->l:LX/2Bs;

    iget-object v1, p0, LX/29A;->c:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 375815
    invoke-static {v0, v1, v2}, LX/2Bs;->c(LX/2Bs;Landroid/content/Context;Landroid/content/Intent;)V

    .line 375816
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    .line 375817
    invoke-virtual {v1, v2}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 375818
    invoke-static {p0}, LX/29A;->z(LX/29A;)V

    .line 375819
    invoke-static {p0}, LX/29A;->q(LX/29A;)V

    .line 375820
    invoke-direct {p0}, LX/29A;->p()V

    .line 375821
    return-void
.end method

.method private declared-synchronized o()V
    .locals 5

    .prologue
    .line 375805
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/29A;->y:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/29A;->y:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375806
    :cond_0
    invoke-direct {p0}, LX/29A;->A()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 375807
    :try_start_1
    iget-object v0, p0, LX/29A;->e:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/29A;->z:Ljava/lang/Runnable;

    invoke-direct {p0}, LX/29A;->A()J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/29A;->y:Ljava/util/concurrent/ScheduledFuture;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375808
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 375809
    :catch_0
    move-exception v0

    .line 375810
    :try_start_2
    sget-object v1, LX/29A;->a:Ljava/lang/Class;

    const-string v2, "Failed to schedule stopping service, trying to stop it now"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 375811
    iget-object v1, p0, LX/29A;->r:LX/03V;

    const-string v2, "MqttPushServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "stopServiceDelayed got exception "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 375812
    iget-object v0, p0, LX/29A;->d:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, LX/29A;->z:Ljava/lang/Runnable;

    const v2, -0x30e5e547

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 375813
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized p()V
    .locals 2

    .prologue
    .line 375896
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/29A;->y:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 375897
    iget-object v0, p0, LX/29A;->y:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 375898
    const/4 v0, 0x0

    iput-object v0, p0, LX/29A;->y:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 375899
    :cond_0
    monitor-exit p0

    return-void

    .line 375900
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static q(LX/29A;)V
    .locals 2

    .prologue
    .line 375731
    iget-object v0, p0, LX/29A;->l:LX/2Bs;

    iget-object v1, p0, LX/29A;->B:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, LX/2Bs;->a(Landroid/content/ServiceConnection;)V

    .line 375732
    invoke-static {p0}, LX/29A;->v(LX/29A;)V

    .line 375733
    return-void
.end method

.method public static s(LX/29A;)V
    .locals 4

    .prologue
    .line 375734
    iget-object v0, p0, LX/29A;->u:LX/1tH;

    if-eqz v0, :cond_1

    .line 375735
    :try_start_0
    iget-object v0, p0, LX/29A;->u:LX/1tH;

    if-eqz v0, :cond_0

    .line 375736
    iget-object v0, p0, LX/29A;->u:LX/1tH;

    iget-object v1, p0, LX/29A;->m:LX/1MZ;

    .line 375737
    iget-object v2, v1, LX/1MZ;->a:LX/1Mc;

    move-object v1, v2

    .line 375738
    invoke-interface {v0, v1}, LX/1tH;->a(LX/1Me;)V

    .line 375739
    iget-object v0, p0, LX/29A;->m:LX/1MZ;

    .line 375740
    iget-object v1, v0, LX/1MZ;->a:LX/1Mc;

    move-object v0, v1

    .line 375741
    iget-object v1, p0, LX/29A;->u:LX/1tH;

    invoke-interface {v1}, LX/1tH;->f()Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Md;->a(Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 375742
    :cond_0
    :goto_0
    iget-object v0, p0, LX/29A;->k:LX/1fU;

    iget-object v1, p0, LX/29A;->u:LX/1tH;

    .line 375743
    iget-object v2, v0, LX/1fU;->a:LX/1fX;

    new-instance v3, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$7;

    invoke-direct {v3, v0, v1}, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$7;-><init>(LX/1fU;LX/1tH;)V

    const p0, -0xec86cc9

    invoke-static {v2, v3, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 375744
    :cond_1
    return-void

    :catch_0
    goto :goto_0
.end method

.method public static v(LX/29A;)V
    .locals 3

    .prologue
    .line 375745
    const/4 v0, 0x0

    iput-object v0, p0, LX/29A;->u:LX/1tH;

    .line 375746
    iget-object v0, p0, LX/29A;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$12;

    invoke-direct {v1, p0}, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$12;-><init>(LX/29A;)V

    const v2, 0x68162107

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 375747
    return-void
.end method

.method public static x(LX/29A;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 375748
    iget-boolean v0, p0, LX/29A;->w:Z

    if-nez v0, :cond_0

    .line 375749
    iget-object v0, p0, LX/29A;->i:LX/29D;

    iget-boolean v1, p0, LX/29A;->w:Z

    const-string v3, "NOT_ENABLED"

    invoke-virtual {v0, v1, v3}, LX/29D;->a(ZLjava/lang/String;)V

    .line 375750
    :goto_0
    return v2

    .line 375751
    :cond_0
    iget-object v0, p0, LX/29A;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375752
    iget-object v0, p0, LX/29A;->i:LX/29D;

    iget-boolean v1, p0, LX/29A;->w:Z

    const-string v3, "FORCE_DISABLED"

    invoke-virtual {v0, v1, v3}, LX/29D;->a(ZLjava/lang/String;)V

    goto :goto_0

    .line 375753
    :cond_1
    iget-object v0, p0, LX/29A;->x:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 375754
    iget-object v0, p0, LX/29A;->x:Ljava/lang/String;

    .line 375755
    :goto_1
    move-object v0, v0

    .line 375756
    if-nez v0, :cond_2

    .line 375757
    iget-object v0, p0, LX/29A;->i:LX/29D;

    iget-boolean v1, p0, LX/29A;->w:Z

    const-string v3, "NOT_LOGGED_IN"

    invoke-virtual {v0, v1, v3}, LX/29D;->a(ZLjava/lang/String;)V

    goto :goto_0

    .line 375758
    :cond_2
    iget-object v0, p0, LX/29A;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2XP;

    .line 375759
    invoke-interface {v0}, LX/2XP;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 375760
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move v2, v1

    .line 375761
    goto :goto_0

    .line 375762
    :cond_4
    iget-object v0, p0, LX/29A;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2HQ;

    .line 375763
    sget-object v3, LX/2XS;->a:[I

    invoke-virtual {v0}, LX/2HQ;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 375764
    sget-object v3, LX/29A;->a:Ljava/lang/Class;

    const-string v4, "Invalid value from HighestMqttPersistenceProvider: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-static {v3, v4, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v2

    .line 375765
    :goto_2
    :pswitch_0
    iget-object v2, p0, LX/29A;->i:LX/29D;

    iget-boolean v3, p0, LX/29A;->w:Z

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, LX/2HQ;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/29D;->a(ZLjava/lang/String;)V

    move v2, v1

    .line 375766
    goto/16 :goto_0

    .line 375767
    :pswitch_1
    iget-boolean v1, p0, LX/29A;->A:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 375768
    iget-boolean v1, p0, LX/29A;->A:Z

    goto :goto_2

    :cond_5
    iget-object v0, p0, LX/29A;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static z(LX/29A;)V
    .locals 2

    .prologue
    .line 375769
    :try_start_0
    iget-object v0, p0, LX/29A;->u:LX/1tH;

    if-eqz v0, :cond_0

    .line 375770
    iget-object v0, p0, LX/29A;->u:LX/1tH;

    iget-object v1, p0, LX/29A;->m:LX/1MZ;

    .line 375771
    iget-object p0, v1, LX/1MZ;->a:LX/1Mc;

    move-object v1, p0

    .line 375772
    invoke-interface {v0, v1}, LX/1tH;->b(LX/1Me;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 375773
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/auth/component/AuthenticationResult;)V
    .locals 3
    .param p1    # Lcom/facebook/auth/component/AuthenticationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 375774
    iget-object v0, p0, LX/29A;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 375775
    iget-object v0, p0, LX/29A;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$6;

    invoke-direct {v1, p0, p1}, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$6;-><init>(LX/29A;Lcom/facebook/auth/component/AuthenticationResult;)V

    const v2, 0x6dd998b1

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 375776
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 375777
    iget-object v0, p0, LX/29A;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 375778
    iget-object v0, p0, LX/29A;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$7;

    invoke-direct {v1, p0}, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$7;-><init>(LX/29A;)V

    const v2, 0x262b5a92

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 375779
    :cond_0
    return-void
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 375780
    iget-object v0, p0, LX/29A;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 375781
    iget-object v0, p0, LX/29A;->n:LX/00G;

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375782
    new-instance v0, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$2;

    invoke-direct {v0, p0}, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$2;-><init>(LX/29A;)V

    .line 375783
    iget-object v1, p0, LX/29A;->d:Ljava/util/concurrent/ExecutorService;

    const v2, -0x542d4ccc

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 375784
    :cond_0
    return-void
.end method

.method public final onAppActive()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 375785
    iput-boolean v1, p0, LX/29A;->A:Z

    .line 375786
    invoke-static {p0}, LX/29A;->x(LX/29A;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375787
    const-string v0, "onAppActive"

    invoke-static {p0, v0, v1}, LX/29A;->a(LX/29A;Ljava/lang/String;Z)V

    .line 375788
    :goto_0
    return-void

    .line 375789
    :cond_0
    invoke-direct {p0}, LX/29A;->o()V

    goto :goto_0
.end method

.method public final onAppStopped()V
    .locals 2

    .prologue
    .line 375790
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/29A;->A:Z

    .line 375791
    invoke-static {p0}, LX/29A;->x(LX/29A;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375792
    const-string v0, "onAppStopped"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/29A;->a(LX/29A;Ljava/lang/String;Z)V

    .line 375793
    :goto_0
    return-void

    .line 375794
    :cond_0
    invoke-direct {p0}, LX/29A;->o()V

    goto :goto_0
.end method

.method public final onDeviceActive()V
    .locals 2

    .prologue
    .line 375795
    invoke-static {p0}, LX/29A;->x(LX/29A;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375796
    const-string v0, "onDeviceActive"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/29A;->a(LX/29A;Ljava/lang/String;Z)V

    .line 375797
    :goto_0
    return-void

    .line 375798
    :cond_0
    invoke-direct {p0}, LX/29A;->o()V

    goto :goto_0
.end method

.method public final onDeviceStopped()V
    .locals 2

    .prologue
    .line 375799
    invoke-static {p0}, LX/29A;->x(LX/29A;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375800
    const-string v0, "onDeviceStopped"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/29A;->a(LX/29A;Ljava/lang/String;Z)V

    .line 375801
    :goto_0
    return-void

    .line 375802
    :cond_0
    invoke-direct {p0}, LX/29A;->o()V

    goto :goto_0
.end method
