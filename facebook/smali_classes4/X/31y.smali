.class public abstract LX/31y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/4h9;

.field public final b:Ljava/lang/String;

.field private final c:J

.field private d:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 488223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488224
    iput-object p1, p0, LX/31y;->b:Ljava/lang/String;

    .line 488225
    sget-object v0, LX/4h9;->NO_RESPONSE:LX/4h9;

    iput-object v0, p0, LX/31y;->a:LX/4h9;

    .line 488226
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/31y;->c:J

    .line 488227
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 2

    .prologue
    .line 488219
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/31y;->d:J

    .line 488220
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 488222
    iget-object v0, p0, LX/31y;->a:LX/4h9;

    invoke-virtual {v0}, LX/4h9;->getStatus()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()I
    .locals 4

    .prologue
    .line 488221
    iget-wide v0, p0, LX/31y;->d:J

    iget-wide v2, p0, LX/31y;->c:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
