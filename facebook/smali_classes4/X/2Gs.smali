.class public LX/2Gs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/2Gs;


# instance fields
.field private final b:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 389565
    const-class v0, LX/2Gs;

    sput-object v0, LX/2Gs;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389563
    iput-object p1, p0, LX/2Gs;->b:LX/0Zb;

    .line 389564
    return-void
.end method

.method public static a(LX/0QB;)LX/2Gs;
    .locals 4

    .prologue
    .line 389549
    sget-object v0, LX/2Gs;->c:LX/2Gs;

    if-nez v0, :cond_1

    .line 389550
    const-class v1, LX/2Gs;

    monitor-enter v1

    .line 389551
    :try_start_0
    sget-object v0, LX/2Gs;->c:LX/2Gs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 389552
    if-eqz v2, :cond_0

    .line 389553
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 389554
    new-instance p0, LX/2Gs;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/2Gs;-><init>(LX/0Zb;)V

    .line 389555
    move-object v0, p0

    .line 389556
    sput-object v0, LX/2Gs;->c:LX/2Gs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389557
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 389558
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 389559
    :cond_1
    sget-object v0, LX/2Gs;->c:LX/2Gs;

    return-object v0

    .line 389560
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 389561
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2Gs;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 389519
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389520
    invoke-virtual {p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->s()Ljava/lang/String;

    .line 389521
    :cond_0
    iget-object v0, p0, LX/2Gs;->b:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 389522
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 389546
    const-string v0, "push_unreg_c2dm"

    const/4 v1, 0x0

    const-string v2, "registration_id"

    invoke-static {v0, p1, v1, v2, p2}, LX/2gN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 389547
    invoke-static {p0, v0}, LX/2Gs;->a(LX/2Gs;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 389548
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 389540
    sget-object v0, LX/2H9;->CURRENT:LX/2H9;

    invoke-virtual {v0}, LX/2H9;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389541
    :goto_0
    return-void

    .line 389542
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 389543
    const-string v1, "push_source"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389544
    const-string v1, "push_reg_initial_status"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "registration_id"

    invoke-static {v1, v2, v0, v3, p3}, LX/2gN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 389545
    invoke-static {p0, v0}, LX/2Gs;->a(LX/2Gs;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 389534
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 389535
    const-string v1, "service_type"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389536
    const-string v1, "action"

    invoke-static {p3}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389537
    const-string v1, "push_reg_status"

    invoke-static {v1, p2, v0, v3, v3}, LX/2gN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 389538
    invoke-static {p0, v0}, LX/2Gs;->a(LX/2Gs;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 389539
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 389529
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 389530
    const-string v1, "service_type"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389531
    const-string v1, "push_unreg_server"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "registration_id"

    invoke-static {v1, v2, v0, v3, p2}, LX/2gN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 389532
    invoke-static {p0, v0}, LX/2Gs;->a(LX/2Gs;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 389533
    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 389523
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 389524
    const-string v1, "detection"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389525
    const-string v1, "rm_pkg"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389526
    const-string v1, "push_messenger_fbns_unreg"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0, v3, v3}, LX/2gN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 389527
    invoke-static {p0, v0}, LX/2Gs;->a(LX/2Gs;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 389528
    return-void
.end method
