.class public LX/2Nx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field public final a:LX/0Sh;

.field public final b:LX/03V;

.field private final c:LX/2Ny;

.field public final d:LX/0ad;

.field private final e:Lcom/facebook/compactdisk/StoreManagerFactory;

.field public f:Lcom/facebook/compactdisk/DiskCache;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 400329
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2Nx;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/03V;LX/2Ny;LX/0ad;Lcom/facebook/compactdisk/StoreManagerFactory;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 400322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400323
    iput-object p1, p0, LX/2Nx;->a:LX/0Sh;

    .line 400324
    iput-object p2, p0, LX/2Nx;->b:LX/03V;

    .line 400325
    iput-object p3, p0, LX/2Nx;->c:LX/2Ny;

    .line 400326
    iput-object p4, p0, LX/2Nx;->d:LX/0ad;

    .line 400327
    iput-object p5, p0, LX/2Nx;->e:Lcom/facebook/compactdisk/StoreManagerFactory;

    .line 400328
    return-void
.end method

.method public static a(LX/0QB;)LX/2Nx;
    .locals 13

    .prologue
    .line 400293
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 400294
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 400295
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 400296
    if-nez v1, :cond_0

    .line 400297
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 400298
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 400299
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 400300
    sget-object v1, LX/2Nx;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 400301
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 400302
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 400303
    :cond_1
    if-nez v1, :cond_4

    .line 400304
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 400305
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 400306
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 400307
    new-instance v7, LX/2Nx;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/2Ny;->a(LX/0QB;)LX/2Ny;

    move-result-object v10

    check-cast v10, LX/2Ny;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {v0}, LX/2Nz;->a(LX/0QB;)Lcom/facebook/compactdisk/StoreManagerFactory;

    move-result-object v12

    check-cast v12, Lcom/facebook/compactdisk/StoreManagerFactory;

    invoke-direct/range {v7 .. v12}, LX/2Nx;-><init>(LX/0Sh;LX/03V;LX/2Ny;LX/0ad;Lcom/facebook/compactdisk/StoreManagerFactory;)V

    .line 400308
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 400309
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 400310
    if-nez v1, :cond_2

    .line 400311
    sget-object v0, LX/2Nx;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Nx;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 400312
    :goto_1
    if-eqz v0, :cond_3

    .line 400313
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 400314
    :goto_3
    check-cast v0, LX/2Nx;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 400315
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 400316
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 400317
    :catchall_1
    move-exception v0

    .line 400318
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 400319
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 400320
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 400321
    :cond_2
    :try_start_8
    sget-object v0, LX/2Nx;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Nx;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/2Nx;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 400284
    :try_start_0
    iget-object v0, p0, LX/2Nx;->f:Lcom/facebook/compactdisk/DiskCache;

    if-eqz v0, :cond_0

    .line 400285
    iget-object v0, p0, LX/2Nx;->f:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v0, p1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetchPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 400286
    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 400287
    invoke-static {p2}, LX/1t3;->b(Ljava/io/File;)[B

    move-result-object v0

    .line 400288
    iget-object v1, p0, LX/2Nx;->f:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v1, p1, v0}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->store(Ljava/lang/String;[B)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 400289
    :goto_0
    return-object v0

    .line 400290
    :catch_0
    move-exception v0

    .line 400291
    iget-object v1, p0, LX/2Nx;->b:LX/03V;

    const-string v2, "ImageUploadCandidateStore_store_%s_image_failed"

    invoke-static {v2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 400292
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 400277
    invoke-static {p0}, LX/2Nx;->a(LX/2Nx;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 400278
    :cond_0
    :goto_0
    return-object v0

    .line 400279
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/2Nx;->f:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v1, p1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetchPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 400280
    if-eqz v2, :cond_0

    .line 400281
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 400282
    :catch_0
    move-exception v1

    .line 400283
    iget-object v2, p0, LX/2Nx;->b:LX/03V;

    const-string v3, "ImageUploadCandidateStore_fetch_%s_image_failed"

    invoke-static {v3, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(LX/2Nx;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 400330
    iget-object v2, p0, LX/2Nx;->d:LX/0ad;

    sget-short v3, LX/FGN;->b:S

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 400331
    :cond_0
    :goto_0
    return v0

    .line 400332
    :cond_1
    iget-object v2, p0, LX/2Nx;->f:Lcom/facebook/compactdisk/DiskCache;

    if-nez v2, :cond_2

    .line 400333
    :try_start_0
    new-instance v2, Lcom/facebook/compactdisk/DiskCacheConfig;

    invoke-direct {v2}, Lcom/facebook/compactdisk/DiskCacheConfig;-><init>()V

    const-string v3, "image_upload"

    invoke-virtual {v2, v3}, Lcom/facebook/compactdisk/DiskCacheConfig;->name(Ljava/lang/String;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/compactdisk/DiskCacheConfig;->sessionScoped(Z)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v2

    sget-object v3, Lcom/facebook/compactdisk/DiskArea;->CACHES:Lcom/facebook/compactdisk/DiskArea;

    invoke-virtual {v2, v3}, Lcom/facebook/compactdisk/DiskCacheConfig;->diskArea(Lcom/facebook/compactdisk/DiskArea;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v2

    const-wide/16 v4, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/compactdisk/DiskCacheConfig;->version(Ljava/lang/Long;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v2

    new-instance v3, Lcom/facebook/compactdisk/ManagedConfig;

    invoke-direct {v3}, Lcom/facebook/compactdisk/ManagedConfig;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/compactdisk/ManagedConfig;->a(Z)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v3

    new-instance v4, Lcom/facebook/compactdisk/StalePruningConfig;

    invoke-direct {v4}, Lcom/facebook/compactdisk/StalePruningConfig;-><init>()V

    const-wide/32 v6, 0x93a80

    invoke-virtual {v4, v6, v7}, Lcom/facebook/compactdisk/StalePruningConfig;->a(J)Lcom/facebook/compactdisk/StalePruningConfig;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/compactdisk/ManagedConfig;->a(Lcom/facebook/compactdisk/StalePruningConfig;)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v3

    new-instance v4, Lcom/facebook/compactdisk/EvictionConfig;

    invoke-direct {v4}, Lcom/facebook/compactdisk/EvictionConfig;-><init>()V

    const-wide/32 v6, 0x6400000

    invoke-virtual {v4, v6, v7}, Lcom/facebook/compactdisk/EvictionConfig;->maxSize(J)Lcom/facebook/compactdisk/EvictionConfig;

    move-result-object v4

    const-wide/32 v6, 0x1400000

    invoke-virtual {v4, v6, v7}, Lcom/facebook/compactdisk/EvictionConfig;->lowSpaceMaxSize(J)Lcom/facebook/compactdisk/EvictionConfig;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/compactdisk/ManagedConfig;->a(Lcom/facebook/compactdisk/EvictionConfig;)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/compactdisk/DiskCacheConfig;->subConfig(Lcom/facebook/compactdisk/SubConfig;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v2

    .line 400334
    iget-object v3, p0, LX/2Nx;->e:Lcom/facebook/compactdisk/StoreManagerFactory;

    invoke-virtual {v3, v2}, Lcom/facebook/compactdisk/StoreManagerFactory;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/StoreManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/facebook/compactdisk/StoreManager;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/DiskCache;

    move-result-object v2

    iput-object v2, p0, LX/2Nx;->f:Lcom/facebook/compactdisk/DiskCache;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 400335
    :cond_2
    :goto_1
    iget-object v2, p0, LX/2Nx;->f:Lcom/facebook/compactdisk/DiskCache;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 400336
    :catch_0
    move-exception v2

    .line 400337
    iget-object v3, p0, LX/2Nx;->b:LX/03V;

    const-string v4, "ImageUploadCandidateStore_initialization_failure"

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 400338
    const/4 v2, 0x0

    iput-object v2, p0, LX/2Nx;->f:Lcom/facebook/compactdisk/DiskCache;

    goto :goto_1
.end method

.method public static b(Landroid/net/Uri;II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 400274
    const-string v0, "%s_%d_%d"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, p0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 400275
    invoke-static {v0}, LX/2Ny;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 400276
    const-string v1, "r_%s"

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 400272
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Ny;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 400273
    const-string v1, "o_%s"

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 400255
    invoke-static {p0}, LX/2Nx;->a(LX/2Nx;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 400256
    :cond_0
    :goto_0
    return-object v0

    .line 400257
    :cond_1
    iget-object v1, p0, LX/2Nx;->d:LX/0ad;

    sget-short v2, LX/FGN;->c:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 400258
    invoke-static {p1}, LX/5zs;->fromOrNull(Landroid/net/Uri;)LX/5zs;

    move-result-object v1

    .line 400259
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/5zs;->isLikelyLocal()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 400260
    :try_start_0
    iget-object v1, p0, LX/2Nx;->c:LX/2Ny;

    invoke-virtual {v1, p1}, LX/2Ny;->a(Landroid/net/Uri;)LX/46f;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 400261
    :try_start_1
    invoke-static {p1}, LX/2Nx;->d(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, v2, LX/46f;->a:Ljava/io/File;

    const-string v4, "original"

    invoke-static {p0, v1, v3, v4}, LX/2Nx;->a(LX/2Nx;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 400262
    if-eqz v2, :cond_0

    .line 400263
    invoke-virtual {v2}, LX/46f;->a()V

    goto :goto_0

    .line 400264
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 400265
    :goto_1
    :try_start_2
    iget-object v3, p0, LX/2Nx;->b:LX/03V;

    const-string v4, "ImageUploadCandidateStore_store_original_image_failed"

    invoke-virtual {v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 400266
    if-eqz v2, :cond_0

    .line 400267
    invoke-virtual {v2}, LX/46f;->a()V

    goto :goto_0

    .line 400268
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    if-eqz v2, :cond_2

    .line 400269
    invoke-virtual {v2}, LX/46f;->a()V

    :cond_2
    throw v0

    .line 400270
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 400271
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public final a(Landroid/net/Uri;II)Ljava/io/File;
    .locals 2

    .prologue
    .line 400254
    invoke-static {p1, p2, p3}, LX/2Nx;->b(Landroid/net/Uri;II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resized"

    invoke-direct {p0, v0, v1}, LX/2Nx;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/net/Uri;)Ljava/io/File;
    .locals 2

    .prologue
    .line 400253
    invoke-static {p1}, LX/2Nx;->d(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "original"

    invoke-direct {p0, v0, v1}, LX/2Nx;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 400250
    iget-object v0, p0, LX/2Nx;->f:Lcom/facebook/compactdisk/DiskCache;

    if-eqz v0, :cond_0

    .line 400251
    iget-object v0, p0, LX/2Nx;->f:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v0}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->clear()V

    .line 400252
    :cond_0
    return-void
.end method
