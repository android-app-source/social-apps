.class public LX/2sP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile k:LX/2sP;


# instance fields
.field private final b:J

.field private final c:LX/0SG;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1qN;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0lC;

.field private final f:LX/3R3;

.field private final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public volatile h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

.field public volatile i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field public volatile j:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 472577
    const-class v0, LX/2sP;

    sput-object v0, LX/2sP;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0Or;LX/0lC;LX/3R3;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/1qN;",
            ">;",
            "LX/0lC;",
            "LX/3R3;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 472578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472579
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, LX/2sP;->b:J

    .line 472580
    iput-object p1, p0, LX/2sP;->c:LX/0SG;

    .line 472581
    iput-object p2, p0, LX/2sP;->d:LX/0Or;

    .line 472582
    iput-object p3, p0, LX/2sP;->e:LX/0lC;

    .line 472583
    iput-object p4, p0, LX/2sP;->f:LX/3R3;

    .line 472584
    iput-object p5, p0, LX/2sP;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 472585
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2sP;->i:LX/0am;

    .line 472586
    return-void
.end method

.method public static a(LX/0QB;)LX/2sP;
    .locals 9

    .prologue
    .line 472587
    sget-object v0, LX/2sP;->k:LX/2sP;

    if-nez v0, :cond_1

    .line 472588
    const-class v1, LX/2sP;

    monitor-enter v1

    .line 472589
    :try_start_0
    sget-object v0, LX/2sP;->k:LX/2sP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 472590
    if-eqz v2, :cond_0

    .line 472591
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 472592
    new-instance v3, LX/2sP;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    const/16 v5, 0x3cd

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v6

    check-cast v6, LX/0lC;

    invoke-static {v0}, LX/3R3;->b(LX/0QB;)LX/3R3;

    move-result-object v7

    check-cast v7, LX/3R3;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v3 .. v8}, LX/2sP;-><init>(LX/0SG;LX/0Or;LX/0lC;LX/3R3;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 472593
    move-object v0, v3

    .line 472594
    sput-object v0, LX/2sP;->k:LX/2sP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 472595
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 472596
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 472597
    :cond_1
    sget-object v0, LX/2sP;->k:LX/2sP;

    return-object v0

    .line 472598
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 472599
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static declared-synchronized c(LX/2sP;)V
    .locals 7

    .prologue
    .line 472600
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2sP;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2bs;->b:LX/0Tn;

    iget-object v2, p0, LX/2sP;->e:LX/0lC;

    iget-object v3, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v3, v3, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v2, v3}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 472601
    iget-object v0, p0, LX/2sP;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qN;

    iget-object v1, p0, LX/2sP;->e:LX/0lC;

    iget-object v2, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-virtual {v1, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    .line 472602
    iget-object v2, v0, LX/1qN;->e:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 472603
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 472604
    iget-object v2, v0, LX/1qN;->d:LX/1qR;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "privacy_options"

    invoke-virtual {v2, v3, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 472605
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 472606
    sget-object v3, LX/1qO;->a:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 472607
    iget-object v3, v0, LX/1qN;->d:LX/1qR;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "privacy_options"

    const-string v5, ""

    const v6, -0x72e1b82b

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {v3, v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v2, -0x7cdd2b4e

    invoke-static {v2}, LX/03h;->a(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 472608
    :goto_0
    monitor-exit p0

    return-void

    .line 472609
    :catch_0
    move-exception v0

    .line 472610
    :try_start_1
    sget-object v1, LX/2sP;->a:Ljava/lang/Class;

    const-string v2, "Unable to write privacy options result to disk."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 472611
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 472612
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2sP;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472613
    iget-object v0, p0, LX/2sP;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 472614
    :goto_0
    monitor-exit p0

    return-object v0

    .line 472615
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2sP;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2bs;->b:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 472616
    if-nez v0, :cond_1

    move-object v0, v1

    .line 472617
    goto :goto_0

    .line 472618
    :cond_1
    :try_start_2
    iget-object v2, p0, LX/2sP;->e:LX/0lC;

    const-class v3, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v2, v0, v3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 472619
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    iput-object v2, p0, LX/2sP;->i:LX/0am;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 472620
    :catch_0
    move-exception v0

    .line 472621
    :try_start_3
    sget-object v2, LX/2sP;->a:Ljava/lang/Class;

    const-string v3, "Unable to read selected privacy option from prefs."

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v0, v1

    .line 472622
    goto :goto_0

    .line 472623
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)Lcom/facebook/privacy/model/PrivacyOptionsResult;
    .locals 4

    .prologue
    .line 472624
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    .line 472625
    :cond_0
    iget-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 472626
    :goto_0
    monitor-exit p0

    return-object v0

    .line 472627
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2sP;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qN;

    invoke-virtual {v0}, LX/1qN;->a()Ljava/lang/String;

    move-result-object v0

    .line 472628
    if-nez v0, :cond_2

    .line 472629
    iget-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 472630
    :cond_2
    :try_start_2
    iget-object v1, p0, LX/2sP;->e:LX/0lC;

    const-class v2, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iput-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 472631
    iget-object v0, p0, LX/2sP;->f:LX/3R3;

    iget-object v1, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-virtual {v0, v1}, LX/3R3;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)Lcom/facebook/privacy/model/PrivacyOptionsResult;

    move-result-object v0

    iput-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 472632
    iget-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2sP;->i:LX/0am;

    .line 472633
    iget-object v0, p0, LX/2sP;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/2sP;->j:J
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 472634
    :goto_1
    :try_start_3
    iget-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    goto :goto_0

    .line 472635
    :catch_0
    move-exception v0

    .line 472636
    sget-object v1, LX/2sP;->a:Ljava/lang/Class;

    const-string v2, "Unable to read privacy options result from disk."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 472637
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 1

    .prologue
    .line 472638
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_0

    .line 472639
    iget-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-static {v0}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)LX/2br;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2br;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/2br;

    move-result-object v0

    invoke-virtual {v0}, LX/2br;->b()Lcom/facebook/privacy/model/PrivacyOptionsResult;

    move-result-object v0

    iput-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 472640
    iget-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2sP;->i:LX/0am;

    .line 472641
    invoke-static {p0}, LX/2sP;->c(LX/2sP;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 472642
    :cond_0
    monitor-exit p0

    return-void

    .line 472643
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)V
    .locals 2

    .prologue
    .line 472644
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 472645
    invoke-static {p1}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)LX/2br;

    move-result-object v0

    .line 472646
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/2br;->j:Z

    .line 472647
    move-object v0, v0

    .line 472648
    invoke-virtual {v0}, LX/2br;->b()Lcom/facebook/privacy/model/PrivacyOptionsResult;

    move-result-object v0

    iput-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 472649
    iget-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2sP;->i:LX/0am;

    .line 472650
    iget-object v0, p0, LX/2sP;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/2sP;->j:J

    .line 472651
    invoke-static {p0}, LX/2sP;->c(LX/2sP;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 472652
    monitor-exit p0

    return-void

    .line 472653
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 4

    .prologue
    .line 472654
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2sP;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 472655
    iget-wide v2, p0, LX/2sP;->j:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 472656
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 472657
    const/4 v0, 0x0

    iput-object v0, p0, LX/2sP;->h:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 472658
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2sP;->i:LX/0am;

    .line 472659
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2sP;->j:J

    .line 472660
    return-void
.end method
