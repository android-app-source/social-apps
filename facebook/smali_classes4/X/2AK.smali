.class public final LX/2AK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:J

.field public c:I

.field public d:J

.field public e:LX/2AL;

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const-wide/16 v0, -0x1

    .line 377262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 377263
    iput-wide v0, p0, LX/2AK;->a:J

    .line 377264
    iput-wide v0, p0, LX/2AK;->b:J

    .line 377265
    iput v2, p0, LX/2AK;->c:I

    .line 377266
    iput-wide v0, p0, LX/2AK;->d:J

    .line 377267
    sget-object v0, LX/2AL;->TP_DISABLED:LX/2AL;

    iput-object v0, p0, LX/2AK;->e:LX/2AL;

    .line 377268
    iput v2, p0, LX/2AK;->f:I

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 377269
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "lastUpdateTimestamp"

    iget-wide v2, p0, LX/2AK;->a:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "lastFullUpdateTimestamp"

    iget-wide v2, p0, LX/2AK;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "lastFullUpdateSize"

    iget v2, p0, LX/2AK;->c:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "lastMqttDisconnect"

    iget-wide v2, p0, LX/2AK;->d:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "lastPresenceFullListDownloadState"

    iget-object v2, p0, LX/2AK;->e:LX/2AL;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "numUsersOnline"

    iget v2, p0, LX/2AK;->f:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
