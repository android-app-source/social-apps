.class public LX/3Lu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/Locale;

.field public static final b:Ljava/util/Locale;

.field public static final c:Ljava/util/Locale;

.field public static final d:Ljava/util/Locale;

.field public static final e:Ljava/util/Locale;

.field public static final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final h:Ljava/lang/String;

.field private static volatile l:LX/3Lu;


# instance fields
.field private g:LX/03R;

.field private final i:Ljava/util/Locale;

.field private final j:Ljava/lang/String;

.field public k:LX/3hH;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 552339
    const-class v0, LX/3Lu;

    sput-object v0, LX/3Lu;->f:Ljava/lang/Class;

    .line 552340
    new-instance v0, Ljava/util/Locale;

    const-string v1, "ar"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/3Lu;->a:Ljava/util/Locale;

    .line 552341
    new-instance v0, Ljava/util/Locale;

    const-string v1, "el"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/3Lu;->b:Ljava/util/Locale;

    .line 552342
    new-instance v0, Ljava/util/Locale;

    const-string v1, "he"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/3Lu;->c:Ljava/util/Locale;

    .line 552343
    new-instance v0, Ljava/util/Locale;

    const-string v1, "uk"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/3Lu;->d:Ljava/util/Locale;

    .line 552344
    new-instance v0, Ljava/util/Locale;

    const-string v1, "th"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/3Lu;->e:Ljava/util/Locale;

    .line 552345
    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Lu;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 552346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 552347
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/3Lu;->g:LX/03R;

    .line 552348
    if-nez p1, :cond_0

    .line 552349
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, LX/3Lu;->i:Ljava/util/Locale;

    .line 552350
    :goto_0
    iget-object v0, p0, LX/3Lu;->i:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3Lu;->j:Ljava/lang/String;

    .line 552351
    return-void

    .line 552352
    :cond_0
    iput-object p1, p0, LX/3Lu;->i:Ljava/util/Locale;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/3Lu;
    .locals 4

    .prologue
    .line 552353
    sget-object v0, LX/3Lu;->l:LX/3Lu;

    if-nez v0, :cond_1

    .line 552354
    const-class v1, LX/3Lu;

    monitor-enter v1

    .line 552355
    :try_start_0
    sget-object v0, LX/3Lu;->l:LX/3Lu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 552356
    if-eqz v2, :cond_0

    .line 552357
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 552358
    new-instance p0, LX/3Lu;

    invoke-static {v0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v3

    check-cast v3, Ljava/util/Locale;

    invoke-direct {p0, v3}, LX/3Lu;-><init>(Ljava/util/Locale;)V

    .line 552359
    move-object v0, p0

    .line 552360
    sput-object v0, LX/3Lu;->l:LX/3Lu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 552361
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 552362
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 552363
    :cond_1
    sget-object v0, LX/3Lu;->l:LX/3Lu;

    return-object v0

    .line 552364
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 552365
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/3Lu;)V
    .locals 4

    .prologue
    .line 552366
    iget-object v0, p0, LX/3Lu;->k:LX/3hH;

    if-nez v0, :cond_0

    .line 552367
    const/4 v1, 0x0

    .line 552368
    :try_start_0
    iget-object v0, p0, LX/3Lu;->j:Ljava/lang/String;

    sget-object v2, LX/3Lu;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 552369
    new-instance v0, LX/7Hr;

    iget-object v2, p0, LX/3Lu;->i:Ljava/util/Locale;

    invoke-direct {v0, v2}, LX/7Hr;-><init>(Ljava/util/Locale;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    .line 552370
    :goto_0
    iput-object v0, p0, LX/3Lu;->k:LX/3hH;

    .line 552371
    :cond_0
    iget-object v0, p0, LX/3Lu;->k:LX/3hH;

    if-nez v0, :cond_2

    .line 552372
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to instantiate AlphabeticIndexLocaleUtils.isAlphabeticIndexAvailable should be called first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 552373
    :cond_1
    :try_start_1
    new-instance v0, LX/3hH;

    iget-object v2, p0, LX/3Lu;->i:Ljava/util/Locale;

    invoke-direct {v0, v2}, LX/3hH;-><init>(Ljava/util/Locale;)V
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_0

    .line 552374
    :catch_0
    move-exception v0

    .line 552375
    sget-object v2, LX/3Lu;->f:Ljava/lang/Class;

    const-string v3, "ensureAlphabeticIndexLocaleUtils"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 552376
    goto :goto_0

    .line 552377
    :catch_1
    move-exception v0

    .line 552378
    sget-object v2, LX/3Lu;->f:Ljava/lang/Class;

    const-string v3, "ensureAlphabeticIndexLocaleUtils"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 552379
    goto :goto_0

    .line 552380
    :catch_2
    move-exception v0

    .line 552381
    sget-object v2, LX/3Lu;->f:Ljava/lang/Class;

    const-string v3, "ensureAlphabeticIndexLocaleUtils"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 552382
    goto :goto_0

    .line 552383
    :catch_3
    move-exception v0

    .line 552384
    sget-object v2, LX/3Lu;->f:Ljava/lang/Class;

    const-string v3, "ensureAlphabeticIndexLocaleUtils"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 552385
    goto :goto_0

    .line 552386
    :catch_4
    move-exception v0

    .line 552387
    sget-object v2, LX/3Lu;->f:Ljava/lang/Class;

    const-string v3, "ensureAlphabeticIndexLocaleUtils"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0

    .line 552388
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 552389
    iget-object v1, p0, LX/3Lu;->g:LX/03R;

    invoke-virtual {v1}, LX/03R;->isSet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 552390
    iget-object v0, p0, LX/3Lu;->g:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    .line 552391
    :goto_0
    return v0

    .line 552392
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v1, v2, :cond_1

    .line 552393
    sget-object v1, LX/03R;->NO:LX/03R;

    iput-object v1, p0, LX/3Lu;->g:LX/03R;

    goto :goto_0

    .line 552394
    :cond_1
    :try_start_0
    new-instance v1, LX/3hF;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    .line 552395
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 552396
    const/16 v4, 0x12c

    invoke-direct {v1, v2, v3, v4}, LX/3hF;-><init>(Ljava/util/Locale;Ljava/util/List;I)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 552397
    const/4 v0, 0x1

    .line 552398
    :goto_1
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/3Lu;->g:LX/03R;

    .line 552399
    iget-object v0, p0, LX/3Lu;->g:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    goto :goto_0

    :catch_0
    goto :goto_1

    .line 552400
    :catch_1
    goto :goto_1

    :catch_2
    goto :goto_1

    :catch_3
    goto :goto_1

    :catch_4
    goto :goto_1
.end method
