.class public LX/3Dm;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DH4;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/3Dm",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DH4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 535809
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 535810
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/3Dm;->b:LX/0Zi;

    .line 535811
    iput-object p1, p0, LX/3Dm;->a:LX/0Ot;

    .line 535812
    return-void
.end method

.method public static a(LX/0QB;)LX/3Dm;
    .locals 4

    .prologue
    .line 535813
    const-class v1, LX/3Dm;

    monitor-enter v1

    .line 535814
    :try_start_0
    sget-object v0, LX/3Dm;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 535815
    sput-object v2, LX/3Dm;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535816
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535817
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 535818
    new-instance v3, LX/3Dm;

    const/16 p0, 0x21c3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3Dm;-><init>(LX/0Ot;)V

    .line 535819
    move-object v0, v3

    .line 535820
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 535821
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3Dm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535822
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 535823
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 535824
    check-cast p2, LX/DH3;

    .line 535825
    iget-object v0, p0, LX/3Dm;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DH4;

    iget-object v2, p2, LX/DH3;->a:LX/DGK;

    iget-object v3, p2, LX/DH3;->b:LX/1Pd;

    iget v4, p2, LX/DH3;->c:I

    iget v5, p2, LX/DH3;->d:I

    iget v6, p2, LX/DH3;->e:I

    iget v7, p2, LX/DH3;->f:I

    move-object v1, p1

    const/4 p2, 0x1

    .line 535826
    iget-object p0, v2, LX/DGK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object p1, p0

    .line 535827
    iget-object p0, v0, LX/DH4;->d:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1VD;

    invoke-virtual {p0, v1}, LX/1VD;->c(LX/1De;)LX/1X4;

    move-result-object p0

    invoke-virtual {p0, p1}, LX/1X4;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X4;

    move-result-object p0

    check-cast v3, LX/1Pb;

    invoke-virtual {p0, v3}, LX/1X4;->a(LX/1Pb;)LX/1X4;

    move-result-object p0

    invoke-virtual {p0, p2}, LX/1X4;->f(Z)LX/1X4;

    move-result-object p0

    invoke-virtual {p0, p2}, LX/1X4;->d(Z)LX/1X4;

    move-result-object p0

    invoke-virtual {p0, p2}, LX/1X4;->g(Z)LX/1X4;

    move-result-object p0

    invoke-virtual {p0, p2}, LX/1X4;->e(Z)LX/1X4;

    move-result-object p0

    invoke-virtual {p0, p2}, LX/1X4;->c(Z)LX/1X4;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/1X4;->m(I)LX/1X4;

    move-result-object p0

    .line 535828
    iget-object p1, p0, LX/1X4;->a:LX/1X0;

    iput v5, p1, LX/1X0;->m:I

    .line 535829
    move-object p0, p0

    .line 535830
    iget-object p1, p0, LX/1X4;->a:LX/1X0;

    iput v6, p1, LX/1X0;->n:I

    .line 535831
    move-object p0, p0

    .line 535832
    invoke-virtual {p0, v7}, LX/1X4;->i(I)LX/1X4;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 535833
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 535834
    invoke-static {}, LX/1dS;->b()V

    .line 535835
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/DH2;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/3Dm",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 535836
    new-instance v1, LX/DH3;

    invoke-direct {v1, p0}, LX/DH3;-><init>(LX/3Dm;)V

    .line 535837
    iget-object v2, p0, LX/3Dm;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/DH2;

    .line 535838
    if-nez v2, :cond_0

    .line 535839
    new-instance v2, LX/DH2;

    invoke-direct {v2, p0}, LX/DH2;-><init>(LX/3Dm;)V

    .line 535840
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/DH2;->a$redex0(LX/DH2;LX/1De;IILX/DH3;)V

    .line 535841
    move-object v1, v2

    .line 535842
    move-object v0, v1

    .line 535843
    return-object v0
.end method
