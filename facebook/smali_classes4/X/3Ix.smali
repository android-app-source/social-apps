.class public final LX/3Ix;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Iy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/392;",
        ">",
        "Ljava/lang/Object;",
        "LX/3Iy;"
    }
.end annotation


# instance fields
.field public final a:LX/1Pq;

.field public final b:LX/1CK;

.field public c:LX/3FN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3FN",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3FN;LX/1Pq;LX/1CK;)V
    .locals 0

    .prologue
    .line 547368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547369
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 547370
    iput-object p1, p0, LX/3Ix;->c:LX/3FN;

    .line 547371
    iput-object p2, p0, LX/3Ix;->a:LX/1Pq;

    .line 547372
    iput-object p3, p0, LX/3Ix;->b:LX/1CK;

    .line 547373
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 547366
    iget-object v0, p0, LX/3Ix;->c:LX/3FN;

    const/4 v1, 0x0

    iput-boolean v1, v0, LX/3FN;->r:Z

    .line 547367
    return-void
.end method

.method public final a(LX/198;LX/04g;LX/7K4;)V
    .locals 2

    .prologue
    .line 547362
    iget-object v0, p0, LX/3Ix;->c:LX/3FN;

    iget-object v0, v0, LX/3FN;->l:Ljava/lang/Object;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 547363
    iget-object v0, p0, LX/3Ix;->c:LX/3FN;

    iget-object v0, v0, LX/3FN;->l:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    check-cast v0, LX/392;

    invoke-interface {v0}, LX/392;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 547364
    iget-object v1, p0, LX/3Ix;->c:LX/3FN;

    invoke-static {v0, v1, p1, p2, p3}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/3FN;LX/198;LX/04g;LX/7K4;)V

    .line 547365
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 547353
    iget-object v0, p0, LX/3Ix;->c:LX/3FN;

    iget-object v0, v0, LX/3FN;->l:Ljava/lang/Object;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 547354
    iget-object v0, p0, LX/3Ix;->c:LX/3FN;

    iget-object v0, v0, LX/3FN;->l:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    check-cast v0, LX/392;

    invoke-interface {v0}, LX/392;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 547355
    iget-object v1, p0, LX/3Ix;->c:LX/3FN;

    iget-object v1, v1, LX/3FN;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 547356
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    move-object v1, v2

    .line 547357
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 547358
    iget-object v1, p0, LX/3Ix;->c:LX/3FN;

    iget-object v1, v1, LX/3FN;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 547359
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    move-object v1, v2

    .line 547360
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setChannelEligibility(LX/04H;)V

    .line 547361
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 547351
    iget-object v0, p0, LX/3Ix;->c:LX/3FN;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-static {v0, v1}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/3FN;LX/04g;)V

    .line 547352
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 547349
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition$RichVideoAutoplayTransitionManager$1;

    invoke-direct {v1, p0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition$RichVideoAutoplayTransitionManager$1;-><init>(LX/3Ix;)V

    const v2, -0x4d783cb7

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 547350
    return-void
.end method

.method public final e()Landroid/view/View;
    .locals 1

    .prologue
    .line 547345
    iget-object v0, p0, LX/3Ix;->c:LX/3FN;

    iget-object v0, v0, LX/3FN;->l:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public final f()LX/2oO;
    .locals 1

    .prologue
    .line 547348
    iget-object v0, p0, LX/3Ix;->c:LX/3FN;

    iget-object v0, v0, LX/3FN;->i:LX/2oO;

    return-object v0
.end method

.method public final g()LX/2oL;
    .locals 1

    .prologue
    .line 547347
    iget-object v0, p0, LX/3Ix;->c:LX/3FN;

    iget-object v0, v0, LX/3FN;->j:LX/2oL;

    return-object v0
.end method

.method public final h()Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 1

    .prologue
    .line 547346
    iget-object v0, p0, LX/3Ix;->c:LX/3FN;

    iget-object v0, v0, LX/3FN;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    return-object v0
.end method
