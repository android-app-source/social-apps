.class public LX/38z;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C2c;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/38z",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C2c;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521969
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 521970
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/38z;->b:LX/0Zi;

    .line 521971
    iput-object p1, p0, LX/38z;->a:LX/0Ot;

    .line 521972
    return-void
.end method

.method public static a(LX/0QB;)LX/38z;
    .locals 4

    .prologue
    .line 521958
    const-class v1, LX/38z;

    monitor-enter v1

    .line 521959
    :try_start_0
    sget-object v0, LX/38z;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 521960
    sput-object v2, LX/38z;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 521961
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521962
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 521963
    new-instance v3, LX/38z;

    const/16 p0, 0x1eae

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/38z;-><init>(LX/0Ot;)V

    .line 521964
    move-object v0, v3

    .line 521965
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 521966
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/38z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521967
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 521968
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 521957
    const v0, 0x59a638a1

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 521932
    check-cast p2, LX/C2a;

    .line 521933
    iget-object v0, p0, LX/38z;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C2c;

    iget-object v1, p2, LX/C2a;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C2a;->b:LX/1Pd;

    .line 521934
    invoke-static {v1}, LX/C2c;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    .line 521935
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v5

    .line 521936
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 521937
    invoke-static {v4}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v4

    .line 521938
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const p0, 0x7f0810d0

    const p2, 0x7f0810d1

    invoke-static {v6, p0, p2, v4}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v4

    .line 521939
    invoke-static {p1}, LX/Ap8;->c(LX/1De;)LX/Ap6;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/Ap6;->a(Ljava/lang/CharSequence;)LX/Ap6;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/Ap6;->b(Ljava/lang/CharSequence;)LX/Ap6;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/Ap6;->c(Ljava/lang/CharSequence;)LX/Ap6;

    move-result-object v3

    .line 521940
    const v4, 0x59a638a1

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 521941
    invoke-virtual {v3, v4}, LX/Ap6;->a(LX/1dQ;)LX/Ap6;

    move-result-object v3

    iget-object v4, v0, LX/C2c;->a:LX/AEd;

    const/4 v5, 0x1

    invoke-virtual {v4, p1, v2, v1, v5}, LX/2y5;->b(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1X1;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Ap6;->a(LX/1X1;)LX/Ap6;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 521942
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 521943
    invoke-static {}, LX/1dS;->b()V

    .line 521944
    iget v0, p1, LX/1dQ;->b:I

    .line 521945
    packed-switch v0, :pswitch_data_0

    .line 521946
    :goto_0
    return-object v2

    .line 521947
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 521948
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 521949
    check-cast v1, LX/C2a;

    .line 521950
    iget-object v3, p0, LX/38z;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C2c;

    iget-object p1, v1, LX/C2a;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 521951
    invoke-static {p1}, LX/C2c;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p2

    .line 521952
    invoke-static {p2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object p2

    move-object p2, p2

    .line 521953
    new-instance p0, LX/C2b;

    invoke-direct {p0, v3}, LX/C2b;-><init>(LX/C2c;)V

    move-object p0, p0

    .line 521954
    invoke-interface {p0, p1, v0}, LX/2yN;->a(Ljava/lang/Object;Landroid/view/View;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 521955
    iget-object v1, v3, LX/C2c;->b:LX/AmS;

    invoke-virtual {v1, p2, p0, v0}, LX/AmS;->a(LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 521956
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x59a638a1
        :pswitch_0
    .end packed-switch
.end method
