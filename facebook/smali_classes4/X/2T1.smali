.class public final LX/2T1;
.super Ljava/util/AbstractList;
.source ""

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/RandomAccess;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Ljava/lang/Integer;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# instance fields
.field public final array:[I

.field public final end:I

.field public final start:I


# direct methods
.method private constructor <init>([III)V
    .locals 0

    .prologue
    .line 413728
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 413729
    iput-object p1, p0, LX/2T1;->array:[I

    .line 413730
    iput p2, p0, LX/2T1;->start:I

    .line 413731
    iput p3, p0, LX/2T1;->end:I

    .line 413732
    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 413733
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2T1;->array:[I

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, LX/2T1;->start:I

    iget v3, p0, LX/2T1;->end:I

    invoke-static {v0, v1, v2, v3}, LX/0a4;->c([IIII)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 413710
    if-ne p1, p0, :cond_1

    .line 413711
    :cond_0
    :goto_0
    return v0

    .line 413712
    :cond_1
    instance-of v2, p1, LX/2T1;

    if-eqz v2, :cond_4

    .line 413713
    check-cast p1, LX/2T1;

    .line 413714
    invoke-virtual {p0}, LX/2T1;->size()I

    move-result v3

    .line 413715
    invoke-virtual {p1}, LX/2T1;->size()I

    move-result v2

    if-eq v2, v3, :cond_2

    move v0, v1

    .line 413716
    goto :goto_0

    :cond_2
    move v2, v1

    .line 413717
    :goto_1
    if-ge v2, v3, :cond_0

    .line 413718
    iget-object v4, p0, LX/2T1;->array:[I

    iget v5, p0, LX/2T1;->start:I

    add-int/2addr v5, v2

    aget v4, v4, v5

    iget-object v5, p1, LX/2T1;->array:[I

    iget v6, p1, LX/2T1;->start:I

    add-int/2addr v6, v2

    aget v5, v5, v6

    if-eq v4, v5, :cond_3

    move v0, v1

    .line 413719
    goto :goto_0

    .line 413720
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 413721
    :cond_4
    invoke-super {p0, p1}, Ljava/util/AbstractList;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 413741
    invoke-virtual {p0}, LX/2T1;->size()I

    move-result v0

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 413742
    iget-object v0, p0, LX/2T1;->array:[I

    iget v1, p0, LX/2T1;->start:I

    add-int/2addr v1, p1

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 413734
    const/4 v1, 0x1

    .line 413735
    iget v0, p0, LX/2T1;->start:I

    :goto_0
    iget v2, p0, LX/2T1;->end:I

    if-ge v0, v2, :cond_0

    .line 413736
    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, LX/2T1;->array:[I

    aget v2, v2, v0

    .line 413737
    move v2, v2

    .line 413738
    add-int/2addr v1, v2

    .line 413739
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 413740
    :cond_0
    return v1
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 413722
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 413723
    iget-object v0, p0, LX/2T1;->array:[I

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, LX/2T1;->start:I

    iget v3, p0, LX/2T1;->end:I

    invoke-static {v0, v1, v2, v3}, LX/0a4;->c([IIII)I

    move-result v0

    .line 413724
    if-ltz v0, :cond_0

    .line 413725
    iget v1, p0, LX/2T1;->start:I

    sub-int/2addr v0, v1

    .line 413726
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 413727
    const/4 v0, 0x0

    return v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .locals 5

    .prologue
    .line 413689
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 413690
    iget-object v0, p0, LX/2T1;->array:[I

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, LX/2T1;->start:I

    iget v3, p0, LX/2T1;->end:I

    .line 413691
    add-int/lit8 v4, v3, -0x1

    :goto_0
    if-lt v4, v2, :cond_2

    .line 413692
    aget p1, v0, v4

    if-ne p1, v1, :cond_1

    .line 413693
    :goto_1
    move v0, v4

    .line 413694
    if-ltz v0, :cond_0

    .line 413695
    iget v1, p0, LX/2T1;->start:I

    sub-int/2addr v0, v1

    .line 413696
    :goto_2
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_2

    .line 413697
    :cond_1
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 413698
    :cond_2
    const/4 v4, -0x1

    goto :goto_1
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 413683
    check-cast p2, Ljava/lang/Integer;

    .line 413684
    invoke-virtual {p0}, LX/2T1;->size()I

    move-result v0

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 413685
    iget-object v0, p0, LX/2T1;->array:[I

    iget v1, p0, LX/2T1;->start:I

    add-int/2addr v1, p1

    aget v1, v0, v1

    .line 413686
    iget-object v2, p0, LX/2T1;->array:[I

    iget v0, p0, LX/2T1;->start:I

    add-int v3, v0, p1

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v3

    .line 413687
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 2

    .prologue
    .line 413688
    iget v0, p0, LX/2T1;->end:I

    iget v1, p0, LX/2T1;->start:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final subList(II)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413699
    invoke-virtual {p0}, LX/2T1;->size()I

    move-result v0

    .line 413700
    invoke-static {p1, p2, v0}, LX/0PB;->checkPositionIndexes(III)V

    .line 413701
    if-ne p1, p2, :cond_0

    .line 413702
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 413703
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/2T1;

    iget-object v1, p0, LX/2T1;->array:[I

    iget v2, p0, LX/2T1;->start:I

    add-int/2addr v2, p1

    iget v3, p0, LX/2T1;->start:I

    add-int/2addr v3, p2

    invoke-direct {v0, v1, v2, v3}, LX/2T1;-><init>([III)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 413704
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, LX/2T1;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 413705
    const/16 v0, 0x5b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/2T1;->array:[I

    iget v3, p0, LX/2T1;->start:I

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 413706
    iget v0, p0, LX/2T1;->start:I

    add-int/lit8 v0, v0, 0x1

    :goto_0
    iget v2, p0, LX/2T1;->end:I

    if-ge v0, v2, :cond_0

    .line 413707
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/2T1;->array:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 413708
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 413709
    :cond_0
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
