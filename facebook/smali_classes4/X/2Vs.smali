.class public final LX/2Vs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;

.field private final b:LX/14U;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2Vj",
            "<**>;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2Vr",
            "<**>;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;LX/14U;Ljava/util/List;Ljava/util/List;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1
    .param p5    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14U;",
            "Ljava/util/List",
            "<",
            "LX/2Vj",
            "<**>;>;",
            "Ljava/util/List",
            "<",
            "LX/2Vr",
            "<**>;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 418090
    iput-object p1, p0, LX/2Vs;->a:Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418091
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14U;

    iput-object v0, p0, LX/2Vs;->b:LX/14U;

    .line 418092
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/2Vs;->c:Ljava/util/List;

    .line 418093
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/2Vs;->d:Ljava/util/List;

    .line 418094
    iput-object p5, p0, LX/2Vs;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 418095
    return-void
.end method

.method private b(Lorg/apache/http/HttpResponse;)V
    .locals 11

    .prologue
    .line 418096
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 418097
    iget-object v2, p0, LX/2Vs;->a:Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;

    iget-object v2, v2, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v2, v2, LX/18W;->p:LX/11M;

    invoke-virtual {v2, p1}, LX/11M;->a(Lorg/apache/http/HttpResponse;)V

    .line 418098
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 418099
    iget-object v2, p0, LX/2Vs;->a:Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;

    iget-object v2, v2, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v2, v2, LX/18W;->n:LX/0lp;

    invoke-virtual {v2, v1}, LX/0lp;->a(Ljava/io/InputStream;)LX/15w;

    move-result-object v2

    .line 418100
    new-instance v3, LX/2WH;

    iget-object v1, p0, LX/2Vs;->a:Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;

    iget-object v1, v1, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v1, v1, LX/18W;->z:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-direct {v3, v2, v1}, LX/2WH;-><init>(LX/15w;Z)V

    .line 418101
    const/4 v8, 0x0

    .line 418102
    const/4 v1, 0x0

    move v9, v1

    .line 418103
    :goto_0
    :try_start_0
    invoke-virtual {v3}, LX/2WH;->L()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 418104
    iget-object v1, p0, LX/2Vs;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v9, v1, :cond_1

    iget-object v1, p0, LX/2Vs;->a:Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;

    invoke-virtual {v1}, LX/2VJ;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 418105
    iget-object v2, p0, LX/2Vs;->a:Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;

    new-instance v4, LX/4cw;

    const-class v1, LX/0lF;

    invoke-virtual {v3, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    invoke-direct {v4, v1}, LX/4cw;-><init>(LX/0lF;)V

    .line 418106
    iput-object v4, v2, LX/2VJ;->g:LX/4cw;

    .line 418107
    :cond_0
    iget-object v1, p0, LX/2Vs;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v9, v1, :cond_8

    .line 418108
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Received wrong number of batches in response"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418109
    :catchall_0
    move-exception v1

    invoke-virtual {v3}, LX/2WH;->K()V

    throw v1

    .line 418110
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/2Vs;->c:Ljava/util/List;

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, LX/2Vj;

    move-object v7, v0

    .line 418111
    iget-object v1, p0, LX/2Vs;->d:Ljava/util/List;

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Vr;

    .line 418112
    invoke-virtual {v3}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v1, v4, :cond_2

    .line 418113
    invoke-virtual {v3}, LX/15w;->c()LX/15z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418114
    :cond_2
    :try_start_2
    iget-object v1, p0, LX/2Vs;->a:Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;

    iget-object v5, p0, LX/2Vs;->b:LX/14U;

    iget-object v6, p0, LX/2Vs;->e:Lcom/facebook/common/callercontext/CallerContext;

    move-object v4, p1

    .line 418115
    invoke-static/range {v1 .. v6}, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a$redex0(Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;LX/2Vr;LX/15w;Lorg/apache/http/HttpResponse;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v10

    move-object v1, v10

    .line 418116
    iget-object v0, v7, LX/2Vj;->f:LX/4cn;

    move-object v4, v0

    .line 418117
    if-eqz v4, :cond_3

    .line 418118
    iget-object v0, v7, LX/2Vj;->f:LX/4cn;

    move-object v4, v0

    .line 418119
    invoke-interface {v4, v1}, LX/4cn;->a(Ljava/lang/Object;)V

    .line 418120
    :cond_3
    iget-object v4, p0, LX/2Vs;->a:Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;

    .line 418121
    iget-object v0, v7, LX/2Vj;->c:Ljava/lang/String;

    move-object v5, v0

    .line 418122
    iget-object v0, v4, LX/2VJ;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 418123
    if-eqz v5, :cond_4

    .line 418124
    iget-object v0, v4, LX/2VJ;->c:Ljava/util/Map;

    invoke-interface {v0, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch LX/2Oo; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 418125
    :cond_4
    move-object v1, v8

    .line 418126
    :goto_1
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    move-object v8, v1

    .line 418127
    goto/16 :goto_0

    .line 418128
    :catch_0
    move-exception v1

    move-object v4, v1

    .line 418129
    :try_start_3
    iget-object v1, v2, LX/2Vr;->a:LX/2Vj;

    .line 418130
    iget-object v0, v1, LX/2Vj;->h:LX/03R;

    move-object v1, v0

    .line 418131
    if-nez v8, :cond_a

    sget-object v2, LX/03R;->YES:LX/03R;

    invoke-virtual {v1, v2}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, LX/2Vs;->b:LX/14U;

    .line 418132
    iget-boolean v0, v2, LX/14U;->h:Z

    move v2, v0

    .line 418133
    if-eqz v2, :cond_a

    sget-object v2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v1, v2}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_5
    move-object v1, v4

    .line 418134
    :goto_2
    iget-object v0, v7, LX/2Vj;->f:LX/4cn;

    move-object v2, v0

    .line 418135
    if-eqz v2, :cond_6

    .line 418136
    iget-object v0, v7, LX/2Vj;->f:LX/4cn;

    move-object v2, v0

    .line 418137
    invoke-interface {v2, v4}, LX/4cn;->a(Ljava/lang/Exception;)V

    .line 418138
    :cond_6
    iget-object v2, p0, LX/2Vs;->a:Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;

    .line 418139
    iget-object v0, v7, LX/2Vj;->c:Ljava/lang/String;

    move-object v5, v0

    .line 418140
    if-eqz v5, :cond_7

    .line 418141
    iget-object v0, v2, LX/2VJ;->d:Ljava/util/Map;

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 418142
    :cond_7
    goto :goto_1

    .line 418143
    :cond_8
    if-eqz v8, :cond_9

    .line 418144
    throw v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 418145
    :cond_9
    invoke-virtual {v3}, LX/2WH;->K()V

    .line 418146
    return-void

    :cond_a
    move-object v1, v8

    goto :goto_2
.end method


# virtual methods
.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 418147
    :try_start_0
    invoke-direct {p0, p1}, LX/2Vs;->b(Lorg/apache/http/HttpResponse;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418148
    const/4 v0, 0x0

    return-object v0

    .line 418149
    :catch_0
    move-exception v0

    .line 418150
    :try_start_1
    invoke-static {v0}, LX/2BF;->a(Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418151
    :catchall_0
    move-exception v0

    throw v0
.end method
