.class public LX/32b;
.super Ljava/io/FilterOutputStream;
.source ""


# instance fields
.field public a:J


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 490382
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 490383
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/32b;->a:J

    .line 490384
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 490385
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 490386
    return-void
.end method

.method public final write(I)V
    .locals 4

    .prologue
    .line 490387
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 490388
    iget-wide v0, p0, LX/32b;->a:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/32b;->a:J

    .line 490389
    return-void
.end method

.method public final write([BII)V
    .locals 4

    .prologue
    .line 490390
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 490391
    iget-wide v0, p0, LX/32b;->a:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/32b;->a:J

    .line 490392
    return-void
.end method
