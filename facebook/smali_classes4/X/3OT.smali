.class public final enum LX/3OT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3OT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3OT;

.field public static final enum CHAT_CONTEXT_DISABLED:LX/3OT;

.field public static final enum LIST:LX/3OT;

.field public static final enum LOADING:LX/3OT;

.field public static final enum LOCATION_DISABLED:LX/3OT;

.field public static final enum NUX:LX/3OT;

.field public static final enum UPSELL:LX/3OT;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 560558
    new-instance v0, LX/3OT;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, LX/3OT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OT;->LOADING:LX/3OT;

    .line 560559
    new-instance v0, LX/3OT;

    const-string v1, "NUX"

    invoke-direct {v0, v1, v4}, LX/3OT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OT;->NUX:LX/3OT;

    .line 560560
    new-instance v0, LX/3OT;

    const-string v1, "UPSELL"

    invoke-direct {v0, v1, v5}, LX/3OT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OT;->UPSELL:LX/3OT;

    .line 560561
    new-instance v0, LX/3OT;

    const-string v1, "LOCATION_DISABLED"

    invoke-direct {v0, v1, v6}, LX/3OT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OT;->LOCATION_DISABLED:LX/3OT;

    .line 560562
    new-instance v0, LX/3OT;

    const-string v1, "CHAT_CONTEXT_DISABLED"

    invoke-direct {v0, v1, v7}, LX/3OT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OT;->CHAT_CONTEXT_DISABLED:LX/3OT;

    .line 560563
    new-instance v0, LX/3OT;

    const-string v1, "LIST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3OT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OT;->LIST:LX/3OT;

    .line 560564
    const/4 v0, 0x6

    new-array v0, v0, [LX/3OT;

    sget-object v1, LX/3OT;->LOADING:LX/3OT;

    aput-object v1, v0, v3

    sget-object v1, LX/3OT;->NUX:LX/3OT;

    aput-object v1, v0, v4

    sget-object v1, LX/3OT;->UPSELL:LX/3OT;

    aput-object v1, v0, v5

    sget-object v1, LX/3OT;->LOCATION_DISABLED:LX/3OT;

    aput-object v1, v0, v6

    sget-object v1, LX/3OT;->CHAT_CONTEXT_DISABLED:LX/3OT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3OT;->LIST:LX/3OT;

    aput-object v2, v0, v1

    sput-object v0, LX/3OT;->$VALUES:[LX/3OT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 560565
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3OT;
    .locals 1

    .prologue
    .line 560566
    const-class v0, LX/3OT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3OT;

    return-object v0
.end method

.method public static values()[LX/3OT;
    .locals 1

    .prologue
    .line 560567
    sget-object v0, LX/3OT;->$VALUES:[LX/3OT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3OT;

    return-object v0
.end method
