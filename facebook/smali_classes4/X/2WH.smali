.class public LX/2WH;
.super LX/2WI;
.source ""


# direct methods
.method public constructor <init>(LX/15w;Z)V
    .locals 0

    .prologue
    .line 418701
    invoke-direct {p0, p1, p2}, LX/2WI;-><init>(LX/15w;Z)V

    .line 418702
    return-void
.end method


# virtual methods
.method public final K()V
    .locals 1

    .prologue
    .line 418703
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->close()V

    .line 418704
    return-void
.end method

.method public final L()Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    .line 418692
    :try_start_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    .line 418693
    :goto_0
    if-eqz v1, :cond_5

    .line 418694
    iget v2, p0, LX/2WI;->c:I

    if-ne v2, v3, :cond_0

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-eq v1, v2, :cond_3

    :cond_0
    iget v2, p0, LX/2WI;->c:I

    if-ne v2, v0, :cond_1

    sget-object v2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v1, v2, :cond_3

    :cond_1
    iget v2, p0, LX/2WI;->c:I

    if-nez v2, :cond_2

    sget-object v2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v1, v2, :cond_3

    :cond_2
    iget v2, p0, LX/2WI;->c:I

    if-ne v2, v3, :cond_4

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-ne v1, v2, :cond_4

    .line 418695
    :cond_3
    :goto_1
    return v0

    .line 418696
    :cond_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 418697
    :catch_0
    move-exception v0

    .line 418698
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error finding next batch."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 418699
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final close()V
    .locals 0

    .prologue
    .line 418700
    return-void
.end method
