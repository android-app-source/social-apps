.class public LX/2GS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private final a:LX/0qd;

.field private final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 388454
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2GS;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0qd;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 388455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388456
    iput-object p1, p0, LX/2GS;->a:LX/0qd;

    .line 388457
    iput-object p2, p0, LX/2GS;->b:Ljava/lang/String;

    .line 388458
    return-void
.end method

.method public static a(LX/0QB;)LX/2GS;
    .locals 8

    .prologue
    .line 388459
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 388460
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 388461
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 388462
    if-nez v1, :cond_0

    .line 388463
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 388464
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 388465
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 388466
    sget-object v1, LX/2GS;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 388467
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 388468
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 388469
    :cond_1
    if-nez v1, :cond_4

    .line 388470
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 388471
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 388472
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 388473
    new-instance p0, LX/2GS;

    invoke-static {v0}, LX/0qd;->a(LX/0QB;)LX/0qd;

    move-result-object v1

    check-cast v1, LX/0qd;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct {p0, v1, v7}, LX/2GS;-><init>(LX/0qd;Ljava/lang/String;)V

    .line 388474
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 388475
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 388476
    if-nez v1, :cond_2

    .line 388477
    sget-object v0, LX/2GS;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2GS;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 388478
    :goto_1
    if-eqz v0, :cond_3

    .line 388479
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 388480
    :goto_3
    check-cast v0, LX/2GS;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 388481
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 388482
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 388483
    :catchall_1
    move-exception v0

    .line 388484
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 388485
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 388486
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 388487
    :cond_2
    :try_start_8
    sget-object v0, LX/2GS;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2GS;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final init()V
    .locals 1

    .prologue
    .line 388488
    iget-object v0, p0, LX/2GS;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 388489
    iget-object v0, p0, LX/2GS;->a:LX/0qd;

    invoke-virtual {v0}, LX/0qd;->a()V

    .line 388490
    :cond_0
    return-void
.end method
