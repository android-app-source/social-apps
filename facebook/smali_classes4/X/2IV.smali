.class public LX/2IV;
.super LX/0Tv;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2IV;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391835
    const-string v0, "assetdownload"

    const/4 v1, 0x2

    new-instance v2, LX/2IW;

    invoke-direct {v2}, LX/2IW;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 391836
    return-void
.end method

.method public static a(LX/0QB;)LX/2IV;
    .locals 3

    .prologue
    .line 391837
    sget-object v0, LX/2IV;->a:LX/2IV;

    if-nez v0, :cond_1

    .line 391838
    const-class v1, LX/2IV;

    monitor-enter v1

    .line 391839
    :try_start_0
    sget-object v0, LX/2IV;->a:LX/2IV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391840
    if-eqz v2, :cond_0

    .line 391841
    :try_start_1
    new-instance v0, LX/2IV;

    invoke-direct {v0}, LX/2IV;-><init>()V

    .line 391842
    move-object v0, v0

    .line 391843
    sput-object v0, LX/2IV;->a:LX/2IV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391844
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391845
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391846
    :cond_1
    sget-object v0, LX/2IV;->a:LX/2IV;

    return-object v0

    .line 391847
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391848
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
