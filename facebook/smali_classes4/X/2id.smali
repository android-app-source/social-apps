.class public final LX/2id;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 0

    .prologue
    .line 451887
    iput-object p1, p0, LX/2id;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 451898
    if-nez p2, :cond_0

    .line 451899
    iget-object v0, p0, LX/2id;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->l:LX/1Kt;

    invoke-virtual {v0, p1}, LX/1Kt;->b(LX/0g8;)V

    .line 451900
    :cond_0
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 2

    .prologue
    .line 451888
    add-int v0, p2, p3

    iget-object v1, p0, LX/2id;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->D:LX/2e3;

    const/4 p1, 0x3

    .line 451889
    iget-boolean p2, v1, LX/2e3;->c:Z

    if-nez p2, :cond_1

    .line 451890
    :goto_0
    move v1, p1

    .line 451891
    add-int/2addr v0, v1

    if-lt v0, p4, :cond_0

    iget-object v0, p0, LX/2id;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-boolean v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aQ:Z

    if-nez v0, :cond_0

    .line 451892
    iget-object v0, p0, LX/2id;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->r(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    .line 451893
    :cond_0
    return-void

    .line 451894
    :cond_1
    sget-object p2, LX/33c;->a:[I

    iget-object p3, v1, LX/2e3;->a:LX/0oz;

    invoke-virtual {p3}, LX/0oz;->c()LX/0p3;

    move-result-object p3

    invoke-virtual {p3}, LX/0p3;->ordinal()I

    move-result p3

    aget p2, p2, p3

    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 451895
    :pswitch_0
    iget-object p2, v1, LX/2e3;->b:LX/0ad;

    sget p3, LX/2e4;->p:I

    invoke-interface {p2, p3, p1}, LX/0ad;->a(II)I

    move-result p1

    goto :goto_0

    .line 451896
    :pswitch_1
    iget-object p2, v1, LX/2e3;->b:LX/0ad;

    sget p3, LX/2e4;->m:I

    invoke-interface {p2, p3, p1}, LX/0ad;->a(II)I

    move-result p1

    goto :goto_0

    .line 451897
    :pswitch_2
    iget-object p2, v1, LX/2e3;->b:LX/0ad;

    sget p3, LX/2e4;->j:I

    invoke-interface {p2, p3, p1}, LX/0ad;->a(II)I

    move-result p1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
