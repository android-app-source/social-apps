.class public LX/2cj;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hj;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2cj;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;Ljava/lang/Class;)V
    .locals 0
    .param p3    # Ljava/lang/Class;
        .annotation runtime Lcom/facebook/composer/shareintent/util/ShareIntentHandlerClass;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 441304
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 441305
    iput-object p1, p0, LX/2cj;->a:Landroid/content/Context;

    .line 441306
    iput-object p2, p0, LX/2cj;->b:Lcom/facebook/content/SecureContextHelper;

    .line 441307
    iput-object p3, p0, LX/2cj;->c:Ljava/lang/Class;

    .line 441308
    return-void
.end method

.method public static a(LX/0QB;)LX/2cj;
    .locals 6

    .prologue
    .line 441320
    sget-object v0, LX/2cj;->d:LX/2cj;

    if-nez v0, :cond_1

    .line 441321
    const-class v1, LX/2cj;

    monitor-enter v1

    .line 441322
    :try_start_0
    sget-object v0, LX/2cj;->d:LX/2cj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 441323
    if-eqz v2, :cond_0

    .line 441324
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 441325
    new-instance p0, LX/2cj;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    .line 441326
    const-class v5, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;

    move-object v5, v5

    .line 441327
    move-object v5, v5

    .line 441328
    check-cast v5, Ljava/lang/Class;

    invoke-direct {p0, v3, v4, v5}, LX/2cj;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;Ljava/lang/Class;)V

    .line 441329
    move-object v0, p0

    .line 441330
    sput-object v0, LX/2cj;->d:LX/2cj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 441331
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 441332
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 441333
    :cond_1
    sget-object v0, LX/2cj;->d:LX/2cj;

    return-object v0

    .line 441334
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 441335
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 441309
    const/16 v0, 0x3ec

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 441310
    :cond_0
    :goto_0
    return-void

    .line 441311
    :cond_1
    iget-object v0, p0, LX/2cj;->a:Landroid/content/Context;

    .line 441312
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_3

    .line 441313
    :cond_2
    :goto_1
    goto :goto_0

    .line 441314
    :cond_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 441315
    iget-object p1, p0, LX/2cj;->c:Ljava/lang/Class;

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 441316
    const-string p1, "android.intent.action.SEND"

    invoke-virtual {v1, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 441317
    const-string p1, "android.intent.extra.STREAM"

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 441318
    const/high16 p1, 0x10000000

    invoke-virtual {v1, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 441319
    iget-object p1, p0, LX/2cj;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p1, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1
.end method
