.class public LX/36e;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/Set;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/8G1;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/photos/creativeediting/abtest/IsEligibleForFramePrompts;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 499812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 499813
    return-void
.end method

.method public static a(LX/0QB;)LX/36e;
    .locals 1

    .prologue
    .line 499814
    invoke-static {p0}, LX/36e;->b(LX/0QB;)LX/36e;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/36e;
    .locals 4

    .prologue
    .line 499807
    new-instance v1, LX/36e;

    invoke-direct {v1}, LX/36e;-><init>()V

    .line 499808
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance v3, LX/36f;

    invoke-direct {v3, p0}, LX/36f;-><init>(LX/0QB;)V

    invoke-direct {v0, v2, v3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v2, v0

    .line 499809
    const/16 v0, 0x154a

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    .line 499810
    iput-object v2, v1, LX/36e;->a:Ljava/util/Set;

    iput-object v3, v1, LX/36e;->b:LX/0Or;

    iput-object v0, v1, LX/36e;->c:Ljava/util/concurrent/ExecutorService;

    .line 499811
    return-object v1
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 499800
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 499801
    iget-object v0, p0, LX/36e;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 499802
    iget-object v0, p0, LX/36e;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8G1;

    .line 499803
    invoke-interface {v0}, LX/8G1;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 499804
    if-eqz v0, :cond_0

    .line 499805
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 499806
    :cond_1
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/8GA;

    invoke-direct {v1, p0}, LX/8GA;-><init>(LX/36e;)V

    iget-object v2, p0, LX/36e;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
