.class public final LX/39k;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/39j;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:LX/1Wk;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/CharSequence;

.field public f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/CharSequence;

.field public h:I

.field public i:I

.field public j:F

.field public k:F

.field public l:F

.field public m:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public n:I

.field public final synthetic o:LX/39j;


# direct methods
.method public constructor <init>(LX/39j;)V
    .locals 1

    .prologue
    .line 523815
    iput-object p1, p0, LX/39k;->o:LX/39j;

    .line 523816
    move-object v0, p1

    .line 523817
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 523818
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 523814
    const-string v0, "FooterButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 523770
    if-ne p0, p1, :cond_1

    .line 523771
    :cond_0
    :goto_0
    return v0

    .line 523772
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 523773
    goto :goto_0

    .line 523774
    :cond_3
    check-cast p1, LX/39k;

    .line 523775
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 523776
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 523777
    if-eq v2, v3, :cond_0

    .line 523778
    iget-object v2, p0, LX/39k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/39k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/39k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 523779
    goto :goto_0

    .line 523780
    :cond_5
    iget-object v2, p1, LX/39k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 523781
    :cond_6
    iget-object v2, p0, LX/39k;->b:LX/1Pr;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/39k;->b:LX/1Pr;

    iget-object v3, p1, LX/39k;->b:LX/1Pr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 523782
    goto :goto_0

    .line 523783
    :cond_8
    iget-object v2, p1, LX/39k;->b:LX/1Pr;

    if-nez v2, :cond_7

    .line 523784
    :cond_9
    iget-object v2, p0, LX/39k;->c:LX/1Wk;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/39k;->c:LX/1Wk;

    iget-object v3, p1, LX/39k;->c:LX/1Wk;

    invoke-virtual {v2, v3}, LX/1Wk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 523785
    goto :goto_0

    .line 523786
    :cond_b
    iget-object v2, p1, LX/39k;->c:LX/1Wk;

    if-nez v2, :cond_a

    .line 523787
    :cond_c
    iget-object v2, p0, LX/39k;->d:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/39k;->d:Ljava/lang/String;

    iget-object v3, p1, LX/39k;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 523788
    goto :goto_0

    .line 523789
    :cond_e
    iget-object v2, p1, LX/39k;->d:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 523790
    :cond_f
    iget-object v2, p0, LX/39k;->e:Ljava/lang/CharSequence;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/39k;->e:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/39k;->e:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 523791
    goto :goto_0

    .line 523792
    :cond_11
    iget-object v2, p1, LX/39k;->e:Ljava/lang/CharSequence;

    if-nez v2, :cond_10

    .line 523793
    :cond_12
    iget-object v2, p0, LX/39k;->f:Landroid/util/SparseArray;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/39k;->f:Landroid/util/SparseArray;

    iget-object v3, p1, LX/39k;->f:Landroid/util/SparseArray;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 523794
    goto/16 :goto_0

    .line 523795
    :cond_14
    iget-object v2, p1, LX/39k;->f:Landroid/util/SparseArray;

    if-nez v2, :cond_13

    .line 523796
    :cond_15
    iget-object v2, p0, LX/39k;->g:Ljava/lang/CharSequence;

    if-eqz v2, :cond_17

    iget-object v2, p0, LX/39k;->g:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/39k;->g:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 523797
    goto/16 :goto_0

    .line 523798
    :cond_17
    iget-object v2, p1, LX/39k;->g:Ljava/lang/CharSequence;

    if-nez v2, :cond_16

    .line 523799
    :cond_18
    iget v2, p0, LX/39k;->h:I

    iget v3, p1, LX/39k;->h:I

    if-eq v2, v3, :cond_19

    move v0, v1

    .line 523800
    goto/16 :goto_0

    .line 523801
    :cond_19
    iget v2, p0, LX/39k;->i:I

    iget v3, p1, LX/39k;->i:I

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 523802
    goto/16 :goto_0

    .line 523803
    :cond_1a
    iget v2, p0, LX/39k;->j:F

    iget v3, p1, LX/39k;->j:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_1b

    move v0, v1

    .line 523804
    goto/16 :goto_0

    .line 523805
    :cond_1b
    iget v2, p0, LX/39k;->k:F

    iget v3, p1, LX/39k;->k:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_1c

    move v0, v1

    .line 523806
    goto/16 :goto_0

    .line 523807
    :cond_1c
    iget v2, p0, LX/39k;->l:F

    iget v3, p1, LX/39k;->l:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_1d

    move v0, v1

    .line 523808
    goto/16 :goto_0

    .line 523809
    :cond_1d
    iget-object v2, p0, LX/39k;->m:LX/1dc;

    if-eqz v2, :cond_1f

    iget-object v2, p0, LX/39k;->m:LX/1dc;

    iget-object v3, p1, LX/39k;->m:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    :cond_1e
    move v0, v1

    .line 523810
    goto/16 :goto_0

    .line 523811
    :cond_1f
    iget-object v2, p1, LX/39k;->m:LX/1dc;

    if-nez v2, :cond_1e

    .line 523812
    :cond_20
    iget v2, p0, LX/39k;->n:I

    iget v3, p1, LX/39k;->n:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 523813
    goto/16 :goto_0
.end method
