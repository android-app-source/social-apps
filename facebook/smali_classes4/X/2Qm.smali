.class public final LX/2Qm;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0sv;

.field public static final b:LX/0sv;

.field public static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 408889
    new-instance v0, LX/0su;

    sget-object v1, LX/2Qg;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/2Qm;->a:LX/0sv;

    .line 408890
    new-instance v0, LX/2Qn;

    sget-object v1, LX/2Qg;->b:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    const-string v2, "pending_app_calls"

    sget-object v3, LX/2Qo;->a:LX/0U1;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/2Qn;-><init>(LX/0Px;Ljava/lang/String;LX/0Px;)V

    sput-object v0, LX/2Qm;->b:LX/0sv;

    .line 408891
    sget-object v0, LX/2Qm;->a:LX/0sv;

    sget-object v1, LX/2Qm;->b:LX/0sv;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2Qm;->c:LX/0Px;

    .line 408892
    sget-object v0, LX/2Qg;->a:LX/0U1;

    sget-object v1, LX/2Qg;->b:LX/0U1;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2Qm;->d:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 408887
    const-string v0, "pending_media_uploads"

    sget-object v1, LX/2Qm;->d:LX/0Px;

    sget-object v2, LX/2Qm;->c:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 408888
    return-void
.end method
