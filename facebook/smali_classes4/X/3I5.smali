.class public final LX/3I5;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/3Hz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3Hz;)V
    .locals 1

    .prologue
    .line 545566
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 545567
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/3I5;->a:Ljava/lang/ref/WeakReference;

    .line 545568
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 14

    .prologue
    .line 545506
    iget-object v0, p0, LX/3I5;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Hz;

    .line 545507
    if-nez v0, :cond_0

    .line 545508
    :goto_0
    return-void

    .line 545509
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 545510
    :pswitch_0
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 545511
    iget-object v2, v0, LX/3Hz;->t:LX/3I5;

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v9, v4, v5}, LX/3I5;->sendEmptyMessageDelayed(IJ)Z

    .line 545512
    iget-object v2, v0, LX/3Hz;->u:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v12

    move v11, v10

    :goto_1
    if-ge v11, v12, :cond_6

    iget-object v2, v0, LX/3Hz;->u:LX/0Px;

    invoke-virtual {v2, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    .line 545513
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->l()Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->MID_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    if-ne v2, v3, :cond_c

    .line 545514
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->a()I

    move-result v13

    .line 545515
    iget-object v2, v0, LX/3Hz;->E:LX/3H0;

    sget-object v3, LX/3H0;->VOD:LX/3H0;

    if-ne v2, v3, :cond_7

    iget-object v2, v0, LX/3Hz;->a:LX/3I1;

    iget v2, v2, LX/3I1;->d:I

    sub-int v2, v13, v2

    .line 545516
    :goto_2
    iget-object v3, v0, LX/3Hz;->E:LX/3H0;

    sget-object v4, LX/3H0;->VOD:LX/3H0;

    if-ne v3, v4, :cond_1

    iget-object v3, v0, LX/3Hz;->a:LX/3I1;

    iget-boolean v3, v3, LX/3I1;->b:Z

    if-nez v3, :cond_2

    :cond_1
    iget-object v3, v0, LX/3Hz;->E:LX/3H0;

    sget-object v4, LX/3H0;->NONLIVE:LX/3H0;

    if-ne v3, v4, :cond_4

    iget-object v3, v0, LX/3Hz;->b:LX/3I2;

    iget-boolean v3, v3, LX/3I2;->b:Z

    if-eqz v3, :cond_4

    :cond_2
    iget-object v3, v0, LX/3Hz;->E:LX/3H0;

    sget-object v4, LX/3H0;->VOD:LX/3H0;

    if-ne v3, v4, :cond_8

    iget-object v3, v0, LX/3Hz;->a:LX/3I1;

    iget v3, v3, LX/3I1;->d:I

    :goto_3
    const/4 v4, 0x0

    .line 545517
    iget-object v5, v0, LX/2oy;->j:LX/2pb;

    if-eqz v5, :cond_3

    iget-object v5, v0, LX/2oy;->j:LX/2pb;

    .line 545518
    iget-object v6, v5, LX/2pb;->y:LX/2qV;

    move-object v5, v6

    .line 545519
    sget-object v6, LX/2qV;->PLAYING:LX/2qV;

    if-eq v5, v6, :cond_d

    .line 545520
    :cond_3
    :goto_4
    move v2, v4

    .line 545521
    if-eqz v2, :cond_4

    iget-object v2, v0, LX/3Hz;->c:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v3, v0, LX/3Hz;->x:Ljava/lang/String;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->m()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->d(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {v0}, LX/3Hz;->x$redex0(LX/3Hz;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 545522
    iget-object v2, v0, LX/3Hz;->E:LX/3H0;

    sget-object v3, LX/3H0;->VOD:LX/3H0;

    if-ne v2, v3, :cond_9

    const-string v6, "VOD"

    .line 545523
    :goto_5
    iget-object v2, v0, LX/3Hz;->c:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v3, v0, LX/3Hz;->x:Ljava/lang/String;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->j()I

    move-result v4

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->k()I

    move-result v5

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->m()I

    move-result v7

    invoke-virtual/range {v2 .. v7}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(Ljava/lang/String;IILjava/lang/String;I)V

    .line 545524
    iget-boolean v2, v0, LX/3Hz;->D:Z

    if-eqz v2, :cond_4

    .line 545525
    iget-object v2, v0, LX/3Hz;->c:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v3, v0, LX/3Hz;->x:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e(Ljava/lang/String;)LX/D6v;

    move-result-object v2

    iput-object v2, v0, LX/3Hz;->z:LX/D6v;

    .line 545526
    iget-object v2, v0, LX/3Hz;->z:LX/D6v;

    iget-object v3, v0, LX/3Hz;->E:LX/3H0;

    .line 545527
    iput-object v3, v2, LX/D6v;->w:LX/3H0;

    .line 545528
    iget-object v2, v0, LX/3Hz;->z:LX/D6v;

    iget-object v3, v0, LX/3Hz;->s:LX/3I0;

    invoke-virtual {v2, v3}, LX/D6v;->a(LX/3GF;)V

    .line 545529
    :cond_4
    iget-object v2, v0, LX/3Hz;->z:LX/D6v;

    if-eqz v2, :cond_a

    iget-object v2, v0, LX/3Hz;->z:LX/D6v;

    iget v2, v2, LX/D6v;->d:I

    if-ne v2, v13, :cond_a

    move v2, v9

    .line 545530
    :goto_6
    if-nez v2, :cond_c

    const/16 v2, 0x12c

    const/4 v3, 0x0

    .line 545531
    iget-object v4, v0, LX/2oy;->j:LX/2pb;

    if-eqz v4, :cond_5

    iget-object v4, v0, LX/2oy;->j:LX/2pb;

    .line 545532
    iget-object v5, v4, LX/2pb;->y:LX/2qV;

    move-object v4, v5

    .line 545533
    sget-object v5, LX/2qV;->PLAYING:LX/2qV;

    if-eq v4, v5, :cond_e

    .line 545534
    :cond_5
    :goto_7
    move v2, v3

    .line 545535
    if-eqz v2, :cond_c

    .line 545536
    iget-object v2, v0, LX/3Hz;->E:LX/3H0;

    sget-object v3, LX/3H0;->VOD:LX/3H0;

    if-ne v2, v3, :cond_b

    .line 545537
    iget-object v2, v0, LX/3Hz;->a:LX/3I1;

    invoke-virtual {v2}, LX/3I1;->b()V

    .line 545538
    :goto_8
    iget-object v2, v0, LX/3Hz;->z:LX/D6v;

    if-eqz v2, :cond_6

    .line 545539
    iget-object v2, v0, LX/3Hz;->z:LX/D6v;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->m()I

    move-result v3

    iput v3, v2, LX/D6v;->g:I

    .line 545540
    iget-object v2, v0, LX/3Hz;->z:LX/D6v;

    sget-object v3, LX/BSU;->NORMAL:LX/BSU;

    invoke-virtual {v2, v3, v13}, LX/D6v;->a(LX/BSU;I)V

    .line 545541
    :cond_6
    goto/16 :goto_0

    .line 545542
    :pswitch_1
    const/4 v4, -0x1

    .line 545543
    iget-object v2, v0, LX/3Hz;->z:LX/D6v;

    if-nez v2, :cond_f

    .line 545544
    :goto_9
    goto/16 :goto_0

    .line 545545
    :cond_7
    iget-object v2, v0, LX/3Hz;->b:LX/3I2;

    iget v2, v2, LX/3I2;->c:I

    sub-int v2, v13, v2

    goto/16 :goto_2

    .line 545546
    :cond_8
    iget-object v3, v0, LX/3Hz;->b:LX/3I2;

    iget v3, v3, LX/3I2;->c:I

    goto/16 :goto_3

    .line 545547
    :cond_9
    const-string v6, "NONLIVE"

    goto/16 :goto_5

    :cond_a
    move v2, v10

    .line 545548
    goto :goto_6

    .line 545549
    :cond_b
    iget-object v2, v0, LX/3Hz;->b:LX/3I2;

    .line 545550
    iget-object v3, v2, LX/3I2;->a:LX/0ad;

    sget-object v4, LX/0c0;->Live:LX/0c0;

    sget-short v5, LX/2mm;->j:S

    invoke-interface {v3, v4, v5}, LX/0ad;->a(LX/0c0;S)V

    .line 545551
    goto :goto_8

    .line 545552
    :cond_c
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto/16 :goto_1

    .line 545553
    :cond_d
    iget-object v5, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v5}, LX/2pb;->h()I

    move-result v5

    .line 545554
    sub-int/2addr v5, v2

    .line 545555
    if-ltz v5, :cond_3

    if-ge v5, v3, :cond_3

    const/4 v4, 0x1

    goto/16 :goto_4

    .line 545556
    :cond_e
    iget-object v4, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v4}, LX/2pb;->h()I

    move-result v4

    .line 545557
    sub-int v4, v13, v4

    .line 545558
    if-ltz v4, :cond_5

    if-ge v4, v2, :cond_5

    const/4 v3, 0x1

    goto :goto_7

    .line 545559
    :cond_f
    iget-object v2, v0, LX/2oy;->j:LX/2pb;

    if-eqz v2, :cond_10

    iget-object v2, v0, LX/2oy;->j:LX/2pb;

    .line 545560
    iget-object v3, v2, LX/2pb;->y:LX/2qV;

    move-object v2, v3

    .line 545561
    sget-object v3, LX/2qV;->PLAYING:LX/2qV;

    if-ne v2, v3, :cond_10

    iget-object v2, v0, LX/3Hz;->z:LX/D6v;

    iget v2, v2, LX/D6v;->d:I

    if-eq v2, v4, :cond_10

    .line 545562
    iget-object v2, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->h()I

    move-result v2

    iget-object v3, v0, LX/3Hz;->z:LX/D6v;

    iget v3, v3, LX/D6v;->d:I

    sub-int/2addr v2, v3

    .line 545563
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const/16 v3, 0x7d0

    if-le v2, v3, :cond_10

    .line 545564
    iget-object v2, v0, LX/3Hz;->z:LX/D6v;

    iput v4, v2, LX/D6v;->d:I

    goto :goto_9

    .line 545565
    :cond_10
    iget-object v2, v0, LX/3Hz;->t:LX/3I5;

    const/4 v3, 0x2

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v3, v4, v5}, LX/3I5;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_9

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
