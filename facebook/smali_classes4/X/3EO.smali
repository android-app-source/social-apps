.class public final LX/3EO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Dw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:LX/3EO;


# instance fields
.field private final c:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 537494
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "_v"

    sget v2, LX/3Dx;->cS:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "mono"

    sget v2, LX/3Dx;->cT:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "stereo"

    sget v2, LX/3Dx;->cU:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/3EO;->a:LX/0P1;

    .line 537495
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/3EO;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 537496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537497
    iput-object p1, p0, LX/3EO;->c:LX/0ad;

    .line 537498
    return-void
.end method

.method public static a(LX/0QB;)LX/3EO;
    .locals 4

    .prologue
    .line 537499
    sget-object v0, LX/3EO;->d:LX/3EO;

    if-nez v0, :cond_1

    .line 537500
    const-class v1, LX/3EO;

    monitor-enter v1

    .line 537501
    :try_start_0
    sget-object v0, LX/3EO;->d:LX/3EO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 537502
    if-eqz v2, :cond_0

    .line 537503
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 537504
    new-instance p0, LX/3EO;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/3EO;-><init>(LX/0ad;)V

    .line 537505
    move-object v0, p0

    .line 537506
    sput-object v0, LX/3EO;->d:LX/3EO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 537507
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 537508
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 537509
    :cond_1
    sget-object v0, LX/3EO;->d:LX/3EO;

    return-object v0

    .line 537510
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 537511
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 537512
    sget-object v0, LX/3EO;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 537513
    if-nez v0, :cond_0

    .line 537514
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 537515
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/3EO;->c:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v2, v3, v0, p2}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 537516
    const-string v0, "rtc_opus_bitrate"

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 537517
    sget-object v0, LX/3EO;->b:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    .line 537518
    if-nez v0, :cond_0

    .line 537519
    :goto_0
    return-object p2

    :cond_0
    iget-object v1, p0, LX/3EO;->c:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-interface {v1, v2, v3, v0, p2}, LX/0ad;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 537520
    iget-object v0, p0, LX/3EO;->c:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget v2, LX/3Dx;->cS:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(LX/0c0;I)V

    .line 537521
    return-void
.end method
