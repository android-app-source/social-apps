.class public LX/2Un;
.super LX/1Eg;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Landroid/net/Uri;

.field private static final c:[Ljava/lang/String;

.field private static volatile m:LX/2Un;


# instance fields
.field public d:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/2Uq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3MF;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6jK;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oq;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 416556
    const-class v0, LX/2Un;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Un;->a:Ljava/lang/String;

    .line 416557
    sget-object v0, LX/2Uo;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "simple"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/2Un;->b:Landroid/net/Uri;

    .line 416558
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "recipient_ids"

    aput-object v2, v0, v1

    sput-object v0, LX/2Un;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 416597
    const-string v0, "SMS_BUSINESS_NUMBER_BACKGROUND_FETCH"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 416598
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416599
    iput-object v0, p0, LX/2Un;->h:LX/0Ot;

    .line 416600
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416601
    iput-object v0, p0, LX/2Un;->i:LX/0Ot;

    .line 416602
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416603
    iput-object v0, p0, LX/2Un;->j:LX/0Ot;

    .line 416604
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416605
    iput-object v0, p0, LX/2Un;->k:LX/0Ot;

    .line 416606
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416607
    iput-object v0, p0, LX/2Un;->l:LX/0Ot;

    .line 416608
    return-void
.end method

.method public static a(LX/0QB;)LX/2Un;
    .locals 12

    .prologue
    .line 416582
    sget-object v0, LX/2Un;->m:LX/2Un;

    if-nez v0, :cond_1

    .line 416583
    const-class v1, LX/2Un;

    monitor-enter v1

    .line 416584
    :try_start_0
    sget-object v0, LX/2Un;->m:LX/2Un;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 416585
    if-eqz v2, :cond_0

    .line 416586
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 416587
    new-instance v3, LX/2Un;

    invoke-direct {v3}, LX/2Un;-><init>()V

    .line 416588
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Uq;->a(LX/0QB;)LX/2Uq;

    move-result-object v5

    check-cast v5, LX/2Uq;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    const/16 v8, 0xdac

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2986

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x140f

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xd9e

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 p0, 0x29b7

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 416589
    iput-object v4, v3, LX/2Un;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v5, v3, LX/2Un;->e:LX/2Uq;

    iput-object v6, v3, LX/2Un;->f:LX/0SG;

    iput-object v7, v3, LX/2Un;->g:Landroid/content/Context;

    iput-object v8, v3, LX/2Un;->h:LX/0Ot;

    iput-object v9, v3, LX/2Un;->i:LX/0Ot;

    iput-object v10, v3, LX/2Un;->j:LX/0Ot;

    iput-object v11, v3, LX/2Un;->k:LX/0Ot;

    iput-object p0, v3, LX/2Un;->l:LX/0Ot;

    .line 416590
    move-object v0, v3

    .line 416591
    sput-object v0, LX/2Un;->m:LX/2Un;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416592
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 416593
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 416594
    :cond_1
    sget-object v0, LX/2Un;->m:LX/2Un;

    return-object v0

    .line 416595
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 416596
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static m(LX/2Un;)LX/0Rf;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416569
    const-string v0, "message_count"

    const-string v1, "0"

    invoke-static {v0, v1}, LX/0uu;->e(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 416570
    iget-object v0, p0, LX/2Un;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/2Un;->b:Landroid/net/Uri;

    sget-object v2, LX/2Un;->c:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const-string v5, "date DESC LIMIT 200"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 416571
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v3

    .line 416572
    if-eqz v2, :cond_2

    .line 416573
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416574
    const-string v0, "recipient_ids"

    invoke-static {v2, v0}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 416575
    iget-object v0, p0, LX/2Un;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3MF;

    invoke-virtual {v0, v1}, LX/3MF;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 416576
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_0

    .line 416577
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 416578
    iget-object v1, p0, LX/2Un;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FNh;

    invoke-virtual {v1, v0}, LX/FNh;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 416579
    invoke-virtual {v3, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 416580
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 416581
    :cond_2
    invoke-virtual {v3}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final f()J
    .locals 4

    .prologue
    .line 416566
    iget-object v0, p0, LX/2Un;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Un;->e:LX/2Uq;

    invoke-virtual {v0}, LX/2Uq;->m()Z

    move-result v0

    if-nez v0, :cond_1

    .line 416567
    :cond_0
    const-wide/16 v0, -0x1

    .line 416568
    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, LX/2Un;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3Kr;->aa:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416565
    sget-object v0, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 416561
    iget-object v0, p0, LX/2Un;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Un;->e:LX/2Uq;

    invoke-virtual {v0}, LX/2Uq;->m()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 416562
    :goto_0
    return v0

    .line 416563
    :cond_1
    iget-object v0, p0, LX/2Un;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Kr;->aa:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 416564
    iget-object v0, p0, LX/2Un;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-gtz v0, :cond_2

    iget-object v0, p0, LX/2Un;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x240c8400

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416559
    new-instance v1, LX/FM4;

    invoke-direct {v1, p0}, LX/FM4;-><init>(LX/2Un;)V

    .line 416560
    iget-object v0, p0, LX/2Un;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TD;

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
