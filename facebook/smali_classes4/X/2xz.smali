.class public final LX/2xz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public a:LX/0Zm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0y3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 479914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/1rv;)LX/0m9;
    .locals 3

    .prologue
    .line 479915
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 479916
    const-string v1, "user_enabled"

    iget-object v2, p0, LX/1rv;->b:LX/0Rf;

    invoke-static {v2}, LX/2xz;->a(Ljava/util/Collection;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 479917
    const-string v1, "user_disabled"

    iget-object v2, p0, LX/1rv;->c:LX/0Rf;

    invoke-static {v2}, LX/2xz;->a(Ljava/util/Collection;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 479918
    return-object v0
.end method

.method private static a(Ljava/util/Collection;)LX/162;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 479919
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 479920
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 479921
    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 479922
    :cond_0
    return-object v1
.end method

.method private static a(LX/2xz;LX/0Zm;LX/0Zb;LX/0y3;)V
    .locals 0

    .prologue
    .line 479923
    iput-object p1, p0, LX/2xz;->a:LX/0Zm;

    iput-object p2, p0, LX/2xz;->b:LX/0Zb;

    iput-object p3, p0, LX/2xz;->c:LX/0y3;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, LX/2xz;

    invoke-static {v2}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v0

    check-cast v0, LX/0Zm;

    invoke-static {v2}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {v2}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v2

    check-cast v2, LX/0y3;

    invoke-static {p0, v0, v1, v2}, LX/2xz;->a(LX/2xz;LX/0Zm;LX/0Zb;LX/0y3;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, -0x6efa52ae

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 479924
    invoke-static {p0, p1}, LX/2xz;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 479925
    iget-object v1, p0, LX/2xz;->a:LX/0Zm;

    const-string v2, "android_location_settings_change"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Zm;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 479926
    const/16 v1, 0x27

    const v2, 0x66187e0f

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 479927
    :goto_0
    return-void

    .line 479928
    :cond_0
    iget-object v1, p0, LX/2xz;->c:LX/0y3;

    invoke-virtual {v1}, LX/0y3;->b()LX/1rv;

    move-result-object v1

    .line 479929
    invoke-static {v1}, LX/2xz;->a(LX/1rv;)LX/0m9;

    move-result-object v1

    .line 479930
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "android_location_settings_change"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "background_location"

    .line 479931
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 479932
    move-object v2, v2

    .line 479933
    const-string v3, "providers"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 479934
    iget-object v1, p0, LX/2xz;->b:LX/0Zb;

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 479935
    const v1, -0x3f714294

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
