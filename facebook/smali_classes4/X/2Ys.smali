.class public LX/2Ys;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "LX/2aS;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/facebook/http/tigon/Tigon4aRequestToken;

.field public c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 421738
    const-class v0, LX/2Ys;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Ys;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/http/tigon/Tigon4aRequestToken;)V
    .locals 1

    .prologue
    .line 421710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421711
    iput-object p1, p0, LX/2Ys;->b:Lcom/facebook/http/tigon/Tigon4aRequestToken;

    .line 421712
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Ys;->c:Z

    .line 421713
    return-void
.end method


# virtual methods
.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 421714
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 421715
    iget-object v0, p0, LX/2Ys;->b:Lcom/facebook/http/tigon/Tigon4aRequestToken;

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v2

    const/4 v3, 0x0

    .line 421716
    array-length v4, v2

    mul-int/lit8 v4, v4, 0x2

    new-array v5, v4, [Ljava/lang/String;

    move v4, v3

    .line 421717
    :goto_0
    array-length v6, v2

    if-ge v4, v6, :cond_0

    .line 421718
    add-int/lit8 v6, v3, 0x1

    aget-object v7, v2, v4

    invoke-interface {v7}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v3

    .line 421719
    add-int/lit8 v3, v6, 0x1

    aget-object v7, v2, v4

    invoke-interface {v7}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 421720
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 421721
    :cond_0
    move-object v2, v5

    .line 421722
    invoke-virtual {v0, v1, v2}, Lcom/facebook/http/tigon/Tigon4aRequestToken;->onResponse(I[Ljava/lang/String;)V

    .line 421723
    const/4 v0, 0x0

    .line 421724
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    .line 421725
    const/16 v3, 0x2000

    :try_start_0
    new-array v3, v3, [B

    .line 421726
    :goto_1
    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .line 421727
    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 421728
    add-int/2addr v0, v4

    .line 421729
    iget-object v5, p0, LX/2Ys;->b:Lcom/facebook/http/tigon/Tigon4aRequestToken;

    invoke-virtual {v5, v3, v4}, Lcom/facebook/http/tigon/Tigon4aRequestToken;->onBody([BI)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 421730
    :catch_0
    move-exception v0

    .line 421731
    :try_start_1
    iget-boolean v1, p0, LX/2Ys;->c:Z

    if-eqz v1, :cond_1

    .line 421732
    iget-object v1, p0, LX/2Ys;->b:Lcom/facebook/http/tigon/Tigon4aRequestToken;

    invoke-virtual {v1, v0}, Lcom/facebook/http/tigon/Tigon4aRequestToken;->b(Ljava/lang/Throwable;)V

    .line 421733
    :cond_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 421734
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v0

    .line 421735
    :cond_2
    :try_start_2
    iget-object v3, p0, LX/2Ys;->b:Lcom/facebook/http/tigon/Tigon4aRequestToken;

    invoke-virtual {v3}, Lcom/facebook/http/tigon/Tigon4aRequestToken;->onEOM()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 421736
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 421737
    new-instance v2, LX/2aS;

    invoke-direct {v2, v1, v0}, LX/2aS;-><init>(II)V

    return-object v2
.end method
