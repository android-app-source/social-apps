.class public final enum LX/2h9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2h9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2h9;

.field public static final enum CONTACT_IMPORTER:LX/2h9;

.field public static final enum ENTITY_CARDS:LX/2h9;

.field public static final enum FEED:LX/2h9;

.field public static final enum FEED_FRIENDABLE_HEADER:LX/2h9;

.field public static final enum FRIENDING_CARD:LX/2h9;

.field public static final enum FRIENDING_RADAR:LX/2h9;

.field public static final enum FRIENDS_CENTER_FRIENDS:LX/2h9;

.field public static final enum FRIENDS_CENTER_OUTGOING_REQUESTS_TAB:LX/2h9;

.field public static final enum FRIENDS_CENTER_REQUESTS:LX/2h9;

.field public static final enum FRIENDS_CENTER_SEARCH:LX/2h9;

.field public static final enum FRIENDS_CENTER_SUGGESTIONS:LX/2h9;

.field public static final enum FRIENDS_TAB:LX/2h9;

.field public static final enum FRIEND_LIST_PROFILE:LX/2h9;

.field public static final enum NEARBY_FRIENDS:LX/2h9;

.field public static final enum PROFILE:LX/2h9;

.field public static final enum PROFILE_BROWSER_EVENTS:LX/2h9;

.field public static final enum PROFILE_BROWSER_LIKES:LX/2h9;

.field public static final enum PYMK_CI:LX/2h9;

.field public static final enum PYMK_FEED:LX/2h9;

.field public static final enum PYMK_JEWEL:LX/2h9;

.field public static final enum PYMK_NUX:LX/2h9;

.field public static final enum PYMK_QUICK_PROMOTION:LX/2h9;

.field public static final enum PYMK_SIDESHOW:LX/2h9;

.field public static final enum QR_CODE:LX/2h9;

.field public static final enum SEARCH:LX/2h9;

.field public static final enum TIMELINE_FRIENDS_COLLECTION:LX/2h9;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 449799
    new-instance v0, LX/2h9;

    const-string v1, "CONTACT_IMPORTER"

    const-string v2, "friend_finder"

    invoke-direct {v0, v1, v4, v2}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->CONTACT_IMPORTER:LX/2h9;

    .line 449800
    new-instance v0, LX/2h9;

    const-string v1, "ENTITY_CARDS"

    const-string v2, "entity_cards"

    invoke-direct {v0, v1, v5, v2}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->ENTITY_CARDS:LX/2h9;

    .line 449801
    new-instance v0, LX/2h9;

    const-string v1, "FEED"

    const-string v2, "feed"

    invoke-direct {v0, v1, v6, v2}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->FEED:LX/2h9;

    .line 449802
    new-instance v0, LX/2h9;

    const-string v1, "FEED_FRIENDABLE_HEADER"

    const-string v2, "feed_friendable_header"

    invoke-direct {v0, v1, v7, v2}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->FEED_FRIENDABLE_HEADER:LX/2h9;

    .line 449803
    new-instance v0, LX/2h9;

    const-string v1, "FRIEND_LIST_PROFILE"

    const-string v2, "friend_list_profile"

    invoke-direct {v0, v1, v8, v2}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->FRIEND_LIST_PROFILE:LX/2h9;

    .line 449804
    new-instance v0, LX/2h9;

    const-string v1, "FRIENDING_CARD"

    const/4 v2, 0x5

    const-string v3, "friending_card"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->FRIENDING_CARD:LX/2h9;

    .line 449805
    new-instance v0, LX/2h9;

    const-string v1, "FRIENDING_RADAR"

    const/4 v2, 0x6

    const-string v3, "friending_radar"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->FRIENDING_RADAR:LX/2h9;

    .line 449806
    new-instance v0, LX/2h9;

    const-string v1, "FRIENDS_CENTER_FRIENDS"

    const/4 v2, 0x7

    const-string v3, "fc_friends"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->FRIENDS_CENTER_FRIENDS:LX/2h9;

    .line 449807
    new-instance v0, LX/2h9;

    const-string v1, "FRIENDS_CENTER_OUTGOING_REQUESTS_TAB"

    const/16 v2, 0x8

    const-string v3, "outgoing_requests"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->FRIENDS_CENTER_OUTGOING_REQUESTS_TAB:LX/2h9;

    .line 449808
    new-instance v0, LX/2h9;

    const-string v1, "FRIENDS_CENTER_REQUESTS"

    const/16 v2, 0x9

    const-string v3, "fc_requests"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->FRIENDS_CENTER_REQUESTS:LX/2h9;

    .line 449809
    new-instance v0, LX/2h9;

    const-string v1, "FRIENDS_CENTER_SEARCH"

    const/16 v2, 0xa

    const-string v3, "fc_search"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->FRIENDS_CENTER_SEARCH:LX/2h9;

    .line 449810
    new-instance v0, LX/2h9;

    const-string v1, "FRIENDS_CENTER_SUGGESTIONS"

    const/16 v2, 0xb

    const-string v3, "friend_browser"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->FRIENDS_CENTER_SUGGESTIONS:LX/2h9;

    .line 449811
    new-instance v0, LX/2h9;

    const-string v1, "FRIENDS_TAB"

    const/16 v2, 0xc

    const-string v3, "timeline_friends_pagelet"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->FRIENDS_TAB:LX/2h9;

    .line 449812
    new-instance v0, LX/2h9;

    const-string v1, "NEARBY_FRIENDS"

    const/16 v2, 0xd

    const-string v3, "nearby_friends"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->NEARBY_FRIENDS:LX/2h9;

    .line 449813
    new-instance v0, LX/2h9;

    const-string v1, "PROFILE"

    const/16 v2, 0xe

    const-string v3, "profile"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->PROFILE:LX/2h9;

    .line 449814
    new-instance v0, LX/2h9;

    const-string v1, "PROFILE_BROWSER_EVENTS"

    const/16 v2, 0xf

    const-string v3, "profile_browser_events"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->PROFILE_BROWSER_EVENTS:LX/2h9;

    .line 449815
    new-instance v0, LX/2h9;

    const-string v1, "PROFILE_BROWSER_LIKES"

    const/16 v2, 0x10

    const-string v3, "pb_likes"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->PROFILE_BROWSER_LIKES:LX/2h9;

    .line 449816
    new-instance v0, LX/2h9;

    const-string v1, "PYMK_SIDESHOW"

    const/16 v2, 0x11

    const-string v3, "pymk_sideshow"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->PYMK_SIDESHOW:LX/2h9;

    .line 449817
    new-instance v0, LX/2h9;

    const-string v1, "PYMK_CI"

    const/16 v2, 0x12

    const-string v3, "pymk_ci"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->PYMK_CI:LX/2h9;

    .line 449818
    new-instance v0, LX/2h9;

    const-string v1, "PYMK_FEED"

    const/16 v2, 0x13

    const-string v3, "pymk_feed"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->PYMK_FEED:LX/2h9;

    .line 449819
    new-instance v0, LX/2h9;

    const-string v1, "PYMK_JEWEL"

    const/16 v2, 0x14

    const-string v3, "pymk_jewel"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->PYMK_JEWEL:LX/2h9;

    .line 449820
    new-instance v0, LX/2h9;

    const-string v1, "PYMK_NUX"

    const/16 v2, 0x15

    const-string v3, "pymk_nux"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->PYMK_NUX:LX/2h9;

    .line 449821
    new-instance v0, LX/2h9;

    const-string v1, "PYMK_QUICK_PROMOTION"

    const/16 v2, 0x16

    const-string v3, "pymk_quick_promotion"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->PYMK_QUICK_PROMOTION:LX/2h9;

    .line 449822
    new-instance v0, LX/2h9;

    const-string v1, "QR_CODE"

    const/16 v2, 0x17

    const-string v3, "qr_code"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->QR_CODE:LX/2h9;

    .line 449823
    new-instance v0, LX/2h9;

    const-string v1, "SEARCH"

    const/16 v2, 0x18

    const-string v3, "search"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->SEARCH:LX/2h9;

    .line 449824
    new-instance v0, LX/2h9;

    const-string v1, "TIMELINE_FRIENDS_COLLECTION"

    const/16 v2, 0x19

    const-string v3, "timeline_friends_collection"

    invoke-direct {v0, v1, v2, v3}, LX/2h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h9;->TIMELINE_FRIENDS_COLLECTION:LX/2h9;

    .line 449825
    const/16 v0, 0x1a

    new-array v0, v0, [LX/2h9;

    sget-object v1, LX/2h9;->CONTACT_IMPORTER:LX/2h9;

    aput-object v1, v0, v4

    sget-object v1, LX/2h9;->ENTITY_CARDS:LX/2h9;

    aput-object v1, v0, v5

    sget-object v1, LX/2h9;->FEED:LX/2h9;

    aput-object v1, v0, v6

    sget-object v1, LX/2h9;->FEED_FRIENDABLE_HEADER:LX/2h9;

    aput-object v1, v0, v7

    sget-object v1, LX/2h9;->FRIEND_LIST_PROFILE:LX/2h9;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/2h9;->FRIENDING_CARD:LX/2h9;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2h9;->FRIENDING_RADAR:LX/2h9;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2h9;->FRIENDS_CENTER_FRIENDS:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2h9;->FRIENDS_CENTER_OUTGOING_REQUESTS_TAB:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2h9;->FRIENDS_CENTER_REQUESTS:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/2h9;->FRIENDS_CENTER_SEARCH:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/2h9;->FRIENDS_CENTER_SUGGESTIONS:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/2h9;->FRIENDS_TAB:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/2h9;->NEARBY_FRIENDS:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/2h9;->PROFILE:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/2h9;->PROFILE_BROWSER_EVENTS:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/2h9;->PROFILE_BROWSER_LIKES:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/2h9;->PYMK_SIDESHOW:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/2h9;->PYMK_CI:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/2h9;->PYMK_FEED:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/2h9;->PYMK_JEWEL:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/2h9;->PYMK_NUX:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/2h9;->PYMK_QUICK_PROMOTION:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/2h9;->QR_CODE:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/2h9;->SEARCH:LX/2h9;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/2h9;->TIMELINE_FRIENDS_COLLECTION:LX/2h9;

    aput-object v2, v0, v1

    sput-object v0, LX/2h9;->$VALUES:[LX/2h9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 449826
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 449827
    iput-object p3, p0, LX/2h9;->value:Ljava/lang/String;

    .line 449828
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2h9;
    .locals 1

    .prologue
    .line 449829
    const-class v0, LX/2h9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2h9;

    return-object v0
.end method

.method public static values()[LX/2h9;
    .locals 1

    .prologue
    .line 449830
    sget-object v0, LX/2h9;->$VALUES:[LX/2h9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2h9;

    return-object v0
.end method
