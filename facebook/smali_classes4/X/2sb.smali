.class public LX/2sb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2sb;


# instance fields
.field public a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 473724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 473725
    iput-object p1, p0, LX/2sb;->a:LX/0ad;

    .line 473726
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 473778
    invoke-static {p0}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 473779
    if-nez v0, :cond_0

    move v0, v1

    .line 473780
    :goto_0
    return v0

    .line 473781
    :cond_0
    invoke-static {v0}, LX/17E;->j(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/util/List;

    move-result-object v2

    .line 473782
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 473783
    goto :goto_0

    .line 473784
    :cond_2
    const/4 v0, 0x0

    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 473785
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 473786
    iget-object v4, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 473787
    if-ne v3, v4, :cond_3

    .line 473788
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 473789
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v1

    .line 473790
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;LX/0Px;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 473772
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 473773
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 473774
    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 473775
    :goto_1
    return v0

    .line 473776
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 473777
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/2sb;
    .locals 4

    .prologue
    .line 473759
    sget-object v0, LX/2sb;->b:LX/2sb;

    if-nez v0, :cond_1

    .line 473760
    const-class v1, LX/2sb;

    monitor-enter v1

    .line 473761
    :try_start_0
    sget-object v0, LX/2sb;->b:LX/2sb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 473762
    if-eqz v2, :cond_0

    .line 473763
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 473764
    new-instance p0, LX/2sb;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/2sb;-><init>(LX/0ad;)V

    .line 473765
    move-object v0, p0

    .line 473766
    sput-object v0, LX/2sb;->b:LX/2sb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 473767
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 473768
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 473769
    :cond_1
    sget-object v0, LX/2sb;->b:LX/2sb;

    return-object v0

    .line 473770
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 473771
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)LX/B77;
    .locals 6

    .prologue
    .line 473754
    invoke-static {}, LX/B77;->values()[LX/B77;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 473755
    iget-object v4, v0, LX/B77;->fieldInputType:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->l()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v5

    if-ne v4, v5, :cond_0

    .line 473756
    :goto_1
    return-object v0

    .line 473757
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 473758
    :cond_1
    sget-object v0, LX/B77;->UNKNOWN_ERROR:LX/B77;

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 473750
    if-nez p0, :cond_0

    .line 473751
    const/4 v0, 0x0

    .line 473752
    :goto_0
    return-object v0

    .line 473753
    :cond_0
    const v0, 0x46a1c4a4

    invoke-static {p0, v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 473791
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v0

    .line 473792
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLLeadGenPage;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 473742
    if-nez p0, :cond_0

    move v0, v1

    .line 473743
    :goto_0
    return v0

    .line 473744
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenPage;->j()LX/0Px;

    move-result-object v3

    .line 473745
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenPrivacyNode;

    .line 473746
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenPrivacyNode;->j()Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->DISCLAIMER_PAGE_TITLE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    if-ne v0, v5, :cond_1

    .line 473747
    const/4 v0, 0x1

    goto :goto_0

    .line 473748
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 473749
    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z
    .locals 1

    .prologue
    .line 473738
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v0

    .line 473739
    if-nez v0, :cond_0

    .line 473740
    const/4 v0, 0x0

    .line 473741
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->l()Z

    move-result v0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 473729
    invoke-static {p0}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 473730
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v0

    .line 473731
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->m()LX/0Px;

    move-result-object v3

    .line 473732
    if-nez v3, :cond_1

    .line 473733
    :cond_0
    return v1

    .line 473734
    :cond_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenPage;

    .line 473735
    invoke-static {v0}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLLeadGenPage;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 473736
    add-int/lit8 v0, v1, 0x1

    .line 473737
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 473728
    iget-object v0, p0, LX/2sb;->a:LX/0ad;

    sget-short v1, LX/B78;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 473727
    iget-object v0, p0, LX/2sb;->a:LX/0ad;

    sget-short v1, LX/B78;->c:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
