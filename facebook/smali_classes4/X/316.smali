.class public LX/316;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:I

.field private static volatile c:LX/316;


# instance fields
.field public final b:LX/315;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 486686
    const/16 v0, 0x4fe

    sput v0, LX/316;->a:I

    return-void
.end method

.method public constructor <init>(LX/K1n;LX/317;LX/0Uh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 486669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486670
    sget v0, LX/316;->a:I

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    iput-object p1, p0, LX/316;->b:LX/315;

    .line 486671
    return-void

    :cond_0
    move-object p1, p2

    .line 486672
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/316;
    .locals 6

    .prologue
    .line 486673
    sget-object v0, LX/316;->c:LX/316;

    if-nez v0, :cond_1

    .line 486674
    const-class v1, LX/316;

    monitor-enter v1

    .line 486675
    :try_start_0
    sget-object v0, LX/316;->c:LX/316;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 486676
    if-eqz v2, :cond_0

    .line 486677
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 486678
    new-instance p0, LX/316;

    invoke-static {v0}, LX/K1n;->a(LX/0QB;)LX/K1n;

    move-result-object v3

    check-cast v3, LX/K1n;

    invoke-static {v0}, LX/317;->a(LX/0QB;)LX/317;

    move-result-object v4

    check-cast v4, LX/317;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, LX/316;-><init>(LX/K1n;LX/317;LX/0Uh;)V

    .line 486679
    move-object v0, p0

    .line 486680
    sput-object v0, LX/316;->c:LX/316;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 486681
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 486682
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 486683
    :cond_1
    sget-object v0, LX/316;->c:LX/316;

    return-object v0

    .line 486684
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 486685
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
