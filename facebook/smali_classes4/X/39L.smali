.class public LX/39L;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 522732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 522784
    const/4 v14, 0x0

    .line 522785
    const/4 v13, 0x0

    .line 522786
    const/4 v12, 0x0

    .line 522787
    const/4 v11, 0x0

    .line 522788
    const/4 v10, 0x0

    .line 522789
    const/4 v9, 0x0

    .line 522790
    const/4 v8, 0x0

    .line 522791
    const/4 v7, 0x0

    .line 522792
    const/4 v6, 0x0

    .line 522793
    const/4 v5, 0x0

    .line 522794
    const/4 v4, 0x0

    .line 522795
    const/4 v3, 0x0

    .line 522796
    const/4 v2, 0x0

    .line 522797
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    .line 522798
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 522799
    const/4 v2, 0x0

    .line 522800
    :goto_0
    return v2

    .line 522801
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 522802
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_d

    .line 522803
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v15

    .line 522804
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 522805
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    if-eqz v15, :cond_1

    .line 522806
    const-string v16, "current_tag_expansion"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 522807
    const/4 v2, 0x1

    .line 522808
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v14

    goto :goto_1

    .line 522809
    :cond_2
    const-string v16, "excluded_members"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 522810
    invoke-static/range {p0 .. p1}, LX/2bE;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 522811
    :cond_3
    const-string v16, "explanation"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 522812
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 522813
    :cond_4
    const-string v16, "icon_image"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_5

    .line 522814
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 522815
    :cond_5
    const-string v16, "id"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 522816
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 522817
    :cond_6
    const-string v16, "included_members"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 522818
    invoke-static/range {p0 .. p1}, LX/2bE;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 522819
    :cond_7
    const-string v16, "legacy_graph_api_privacy_json"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_8

    .line 522820
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 522821
    :cond_8
    const-string v16, "name"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_9

    .line 522822
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 522823
    :cond_9
    const-string v16, "privacy_row_input"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_a

    .line 522824
    invoke-static/range {p0 .. p1}, LX/2bF;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 522825
    :cond_a
    const-string v16, "tag_expansion_options"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_b

    .line 522826
    const-class v5, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5}, LX/2gu;->a(LX/15w;LX/186;Ljava/lang/Class;)I

    move-result v5

    goto/16 :goto_1

    .line 522827
    :cond_b
    const-string v16, "type"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_c

    .line 522828
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 522829
    :cond_c
    const-string v16, "url"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 522830
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 522831
    :cond_d
    const/16 v15, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->c(I)V

    .line 522832
    if-eqz v2, :cond_e

    .line 522833
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->a(ILjava/lang/Enum;)V

    .line 522834
    :cond_e
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 522835
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 522836
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 522837
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 522838
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 522839
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 522840
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 522841
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 522842
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 522843
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 522844
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 522845
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    const/4 v1, 0x1

    .line 522733
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 522734
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IIS)S

    move-result v0

    .line 522735
    if-eqz v0, :cond_0

    .line 522736
    const-string v0, "current_tag_expansion"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522737
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522738
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 522739
    if-eqz v0, :cond_1

    .line 522740
    const-string v1, "excluded_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522741
    invoke-static {p0, v0, p2, p3}, LX/2bE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 522742
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 522743
    if-eqz v0, :cond_2

    .line 522744
    const-string v1, "explanation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522745
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522746
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 522747
    if-eqz v0, :cond_3

    .line 522748
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522749
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 522750
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 522751
    if-eqz v0, :cond_4

    .line 522752
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522753
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522754
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 522755
    if-eqz v0, :cond_5

    .line 522756
    const-string v1, "included_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522757
    invoke-static {p0, v0, p2, p3}, LX/2bE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 522758
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 522759
    if-eqz v0, :cond_6

    .line 522760
    const-string v1, "legacy_graph_api_privacy_json"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522761
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522762
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 522763
    if-eqz v0, :cond_7

    .line 522764
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522765
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522766
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 522767
    if-eqz v0, :cond_8

    .line 522768
    const-string v1, "privacy_row_input"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522769
    invoke-static {p0, v0, p2}, LX/2bF;->a(LX/15i;ILX/0nX;)V

    .line 522770
    :cond_8
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 522771
    if-eqz v0, :cond_9

    .line 522772
    const-string v0, "tag_expansion_options"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522773
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->b(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->b(Ljava/util/Iterator;LX/0nX;)V

    .line 522774
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 522775
    if-eqz v0, :cond_a

    .line 522776
    const-string v1, "type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522777
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522778
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 522779
    if-eqz v0, :cond_b

    .line 522780
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522781
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522782
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 522783
    return-void
.end method
