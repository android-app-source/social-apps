.class public LX/2MR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private final a:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "LX/6ed;",
            "LX/FGc;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/6ed;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 396738
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2MR;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396735
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/32 v2, 0x7b98a000

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    new-instance v1, LX/2MS;

    invoke-direct {v1, p0}, LX/2MS;-><init>(LX/2MR;)V

    invoke-virtual {v0, v1}, LX/0QN;->a(LX/0Qn;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/2MR;->a:LX/0QI;

    .line 396736
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2MR;->b:Ljava/util/Map;

    .line 396737
    return-void
.end method

.method public static a(LX/0QB;)LX/2MR;
    .locals 7

    .prologue
    .line 396705
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 396706
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 396707
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 396708
    if-nez v1, :cond_0

    .line 396709
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 396710
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 396711
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 396712
    sget-object v1, LX/2MR;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 396713
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 396714
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 396715
    :cond_1
    if-nez v1, :cond_4

    .line 396716
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 396717
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 396718
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    .line 396719
    new-instance v0, LX/2MR;

    invoke-direct {v0}, LX/2MR;-><init>()V

    .line 396720
    move-object v1, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 396721
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 396722
    if-nez v1, :cond_2

    .line 396723
    sget-object v0, LX/2MR;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2MR;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 396724
    :goto_1
    if-eqz v0, :cond_3

    .line 396725
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 396726
    :goto_3
    check-cast v0, LX/2MR;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 396727
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 396728
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 396729
    :catchall_1
    move-exception v0

    .line 396730
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 396731
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 396732
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 396733
    :cond_2
    :try_start_8
    sget-object v0, LX/2MR;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2MR;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static declared-synchronized a$redex0(LX/2MR;LX/FGc;)V
    .locals 2

    .prologue
    .line 396701
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, LX/FGc;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 396702
    iget-object v0, p0, LX/2MR;->b:Ljava/util/Map;

    invoke-virtual {p1}, LX/FGc;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396703
    :cond_0
    monitor-exit p0

    return-void

    .line 396704
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/6ed;)LX/FGc;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 396700
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2MR;->a:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FGc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "LX/6ed;",
            "LX/FGc;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 396739
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2MR;->a:LX/0QI;

    invoke-interface {v0}, LX/0QI;->b()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/6ed;LX/FGc;)V
    .locals 2

    .prologue
    .line 396695
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2MR;->a:LX/0QI;

    invoke-interface {v0, p1, p2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 396696
    invoke-virtual {p2}, LX/FGc;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 396697
    iget-object v0, p0, LX/2MR;->b:Ljava/util/Map;

    invoke-virtual {p2}, LX/FGc;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396698
    :cond_0
    monitor-exit p0

    return-void

    .line 396699
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 396689
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 396690
    iget-object v2, p0, LX/2MR;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ed;

    .line 396691
    if-eqz v0, :cond_0

    .line 396692
    iget-object v2, p0, LX/2MR;->a:LX/0QI;

    invoke-interface {v2, v0}, LX/0QI;->b(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 396693
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 396694
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(LX/6ed;)V
    .locals 1

    .prologue
    .line 396686
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2MR;->a:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->b(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396687
    monitor-exit p0

    return-void

    .line 396688
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/6ed;LX/FGc;)Z
    .locals 2

    .prologue
    .line 396680
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/2MR;->a(LX/6ed;)LX/FGc;

    move-result-object v0

    .line 396681
    if-eqz v0, :cond_0

    iget-object v0, v0, LX/FGc;->b:LX/FGb;

    iget-object v1, p2, LX/FGc;->b:LX/FGb;

    if-eq v0, v1, :cond_1

    .line 396682
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/2MR;->a(LX/6ed;LX/FGc;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396683
    const/4 v0, 0x1

    .line 396684
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 396685
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
