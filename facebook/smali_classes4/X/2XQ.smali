.class public LX/2XQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2XP;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2XQ;


# instance fields
.field private final a:LX/0Xl;

.field private final b:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(LX/0Xl;)V
    .locals 2
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 419893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419894
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/2XQ;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 419895
    iput-object p1, p0, LX/2XQ;->a:LX/0Xl;

    .line 419896
    return-void
.end method

.method public static a(LX/0QB;)LX/2XQ;
    .locals 4

    .prologue
    .line 419898
    sget-object v0, LX/2XQ;->c:LX/2XQ;

    if-nez v0, :cond_1

    .line 419899
    const-class v1, LX/2XQ;

    monitor-enter v1

    .line 419900
    :try_start_0
    sget-object v0, LX/2XQ;->c:LX/2XQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 419901
    if-eqz v2, :cond_0

    .line 419902
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 419903
    new-instance p0, LX/2XQ;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-direct {p0, v3}, LX/2XQ;-><init>(LX/0Xl;)V

    .line 419904
    move-object v0, p0

    .line 419905
    sput-object v0, LX/2XQ;->c:LX/2XQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 419906
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 419907
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 419908
    :cond_1
    sget-object v0, LX/2XQ;->c:LX/2XQ;

    return-object v0

    .line 419909
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 419910
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 419911
    iget-object v0, p0, LX/2XQ;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 419912
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 419913
    const-string v1, "com.facebook.rti.mqtt.intent.ACTION_WAKEUP"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 419914
    const-string v1, "EXTRA_SKIP_PING"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 419915
    iget-object v1, p0, LX/2XQ;->a:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 419916
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 419897
    iget-object v0, p0, LX/2XQ;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method
