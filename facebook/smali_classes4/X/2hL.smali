.class public final LX/2hL;
.super LX/2hM;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 0

    .prologue
    .line 449962
    iput-object p1, p0, LX/2hL;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-direct {p0}, LX/2hM;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 449963
    check-cast p1, LX/2iF;

    .line 449964
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/2hL;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    if-nez v0, :cond_1

    .line 449965
    :cond_0
    :goto_0
    return-void

    .line 449966
    :cond_1
    iget-object v0, p0, LX/2hL;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-wide v2, p1, LX/2iF;->a:J

    .line 449967
    iget-object v1, v0, LX/2iK;->k:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 449968
    :goto_1
    goto :goto_0

    .line 449969
    :cond_2
    iget-object v1, v0, LX/2iK;->k:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2no;

    .line 449970
    iget-object p0, v0, LX/2iK;->i:LX/86b;

    if-eqz p0, :cond_3

    .line 449971
    iget-object p0, v0, LX/2iK;->i:LX/86b;

    .line 449972
    iget-object p1, p0, LX/86b;->a:Ljava/util/LinkedList;

    invoke-virtual {p1, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 449973
    :cond_3
    iget-object p0, v0, LX/2iK;->o:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 449974
    iget-object v1, v0, LX/2iK;->k:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 449975
    invoke-virtual {v0}, LX/2iK;->m()V

    .line 449976
    const v1, 0x280881d5

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_1
.end method
