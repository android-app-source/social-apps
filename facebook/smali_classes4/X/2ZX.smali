.class public LX/2ZX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2ZV;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2ZV;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/urimap/fetchable/UriTemplateMapParser$HandlerBuilder;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 422537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422538
    iput-object p1, p0, LX/2ZX;->a:LX/2ZV;

    .line 422539
    iput-object p2, p0, LX/2ZX;->b:Ljava/util/Map;

    .line 422540
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0cG;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0cG",
            "<",
            "Lcom/facebook/katana/urimap/api/UriHandler;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 422541
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    new-instance v1, LX/2rL;

    invoke-direct {v1, p0}, LX/2rL;-><init>(LX/2ZX;)V

    invoke-virtual {v0, p1, v1}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v0

    .line 422542
    if-eqz v0, :cond_0

    instance-of v1, v0, Ljava/util/Map;

    if-nez v1, :cond_1

    :cond_0
    move-object v0, v4

    .line 422543
    :goto_0
    return-object v0

    .line 422544
    :cond_1
    check-cast v0, Ljava/util/Map;

    .line 422545
    new-instance v3, LX/0cG;

    invoke-direct {v3}, LX/0cG;-><init>()V

    .line 422546
    iget-object v1, p0, LX/2ZX;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 422547
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 422548
    :try_start_1
    iget-object v6, p0, LX/2ZX;->a:LX/2ZV;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6, v1}, LX/2ZV;->a(Ljava/lang/String;)LX/2sr;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch LX/47U; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 422549
    :catch_0
    move-exception v0

    .line 422550
    :try_start_2
    const-string v1, "UriTemplateMapParser"

    const-string v3, "Invalid uri template: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v1, v0, v3, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 422551
    throw v0

    .line 422552
    :catch_1
    move-object v0, v4

    goto :goto_0

    .line 422553
    :cond_2
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 422554
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 422555
    :try_start_3
    iget-object v5, p0, LX/2ZX;->a:LX/2ZV;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, LX/2ZV;->a(Ljava/lang/String;)LX/2sr;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_3
    .catch LX/47U; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 422556
    :catch_2
    move-exception v0

    .line 422557
    :try_start_4
    const-string v5, "UriTemplateMapParser"

    const-string v6, "Invalid uri template: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    invoke-static {v5, v0, v6, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :cond_3
    move-object v0, v3

    .line 422558
    goto :goto_0
.end method
