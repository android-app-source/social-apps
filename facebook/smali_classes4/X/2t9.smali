.class public LX/2t9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0y5;

.field private b:LX/0fz;


# direct methods
.method public constructor <init>(LX/0y5;LX/0fz;)V
    .locals 0
    .param p2    # LX/0fz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 474755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 474756
    iput-object p1, p0, LX/2t9;->a:LX/0y5;

    .line 474757
    iput-object p2, p0, LX/2t9;->b:LX/0fz;

    .line 474758
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 474759
    iget-object v0, p0, LX/2t9;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    .line 474760
    iget-object v1, p0, LX/2t9;->b:LX/0fz;

    invoke-virtual {v1, v0}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 474761
    if-eqz v1, :cond_1

    .line 474762
    iget-object v2, p0, LX/2t9;->a:LX/0y5;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0y5;->a(Ljava/lang/String;)LX/14s;

    move-result-object v1

    .line 474763
    if-eqz v1, :cond_1

    .line 474764
    iget v2, v1, LX/14s;->mSeenState:I

    move v1, v2

    .line 474765
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 474766
    iget-object v1, p0, LX/2t9;->b:LX/0fz;

    invoke-virtual {v1}, LX/0fz;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 474767
    :goto_1
    return v0

    .line 474768
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 474769
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 474770
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method
