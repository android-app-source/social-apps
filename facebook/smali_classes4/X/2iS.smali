.class public LX/2iS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/23P;

.field private final c:LX/2iT;

.field public final d:LX/2dj;

.field public final e:LX/2do;

.field public final f:LX/2ha;

.field public final g:LX/2hb;

.field private final h:LX/2dl;

.field private final i:LX/1mR;

.field public final j:LX/2hd;

.field private final k:LX/0ad;

.field public final l:LX/2h7;

.field public final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;>;"
        }
    .end annotation
.end field

.field public n:LX/2ia;

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/23P;LX/2iT;LX/2dj;LX/2do;LX/2ha;LX/2hb;LX/2dl;LX/1mR;LX/2hd;LX/0ad;LX/2h7;)V
    .locals 1
    .param p12    # LX/2h7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 451708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 451709
    iput-object p1, p0, LX/2iS;->a:Landroid/content/res/Resources;

    .line 451710
    iput-object p2, p0, LX/2iS;->b:LX/23P;

    .line 451711
    iput-object p3, p0, LX/2iS;->c:LX/2iT;

    .line 451712
    iput-object p4, p0, LX/2iS;->d:LX/2dj;

    .line 451713
    iput-object p5, p0, LX/2iS;->e:LX/2do;

    .line 451714
    iput-object p6, p0, LX/2iS;->f:LX/2ha;

    .line 451715
    iput-object p7, p0, LX/2iS;->g:LX/2hb;

    .line 451716
    iput-object p8, p0, LX/2iS;->h:LX/2dl;

    .line 451717
    iput-object p9, p0, LX/2iS;->i:LX/1mR;

    .line 451718
    iput-object p10, p0, LX/2iS;->j:LX/2hd;

    .line 451719
    iput-object p11, p0, LX/2iS;->k:LX/0ad;

    .line 451720
    iput-object p12, p0, LX/2iS;->l:LX/2h7;

    .line 451721
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2iS;->m:Ljava/util/Map;

    .line 451722
    return-void
.end method

.method public static a(LX/2iS;J)Lcom/facebook/fbui/widget/contentview/ContentView;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 451706
    iget-object v0, p0, LX/2iS;->m:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 451707
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 2

    .prologue
    .line 451700
    sget-object v0, LX/EzG;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 451701
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    :goto_0
    return-object v0

    .line 451702
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    .line 451703
    :pswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    .line 451704
    :pswitch_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    .line 451705
    :pswitch_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(LX/2iS;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 451699
    if-lez p1, :cond_0

    iget-object v0, p0, LX/2iS;->a:Landroid/content/res/Resources;

    const v1, 0x7f0f005c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static a$redex0(LX/2iS;Lcom/facebook/friending/common/list/FriendListItemView;Lcom/facebook/friends/model/PersonYouMayKnow;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 451680
    invoke-virtual {p2}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 451681
    sget-object v1, LX/EzG;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 451682
    invoke-virtual {p1, v4}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451683
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 451684
    :goto_0
    return-void

    .line 451685
    :pswitch_0
    sget-object v1, LX/EyR;->PRIMARY:LX/EyR;

    iget-object v2, p0, LX/2iS;->a:Landroid/content/res/Resources;

    const v3, 0x7f020b78

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/facebook/friending/common/list/FriendListItemView;->a(LX/EyR;Landroid/graphics/drawable/Drawable;)V

    .line 451686
    const v1, 0x7f080f7b

    invoke-static {p0, v1}, LX/2iS;->b(LX/2iS;I)Ljava/lang/CharSequence;

    move-result-object v1

    const v2, 0x7f080f7c

    invoke-static {p0, v2}, LX/2iS;->b(LX/2iS;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/facebook/friending/common/list/FriendListItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 451687
    iget-object v1, p0, LX/2iS;->a:Landroid/content/res/Resources;

    const v2, 0x7f080f9c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 451688
    :goto_1
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 451689
    new-instance v1, LX/EzD;

    invoke-direct {v1, p0, v0, p2}, LX/EzD;-><init>(LX/2iS;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 451690
    :pswitch_1
    sget-object v1, LX/EyR;->PRIMARY:LX/EyR;

    iget-object v2, p0, LX/2iS;->a:Landroid/content/res/Resources;

    const v3, 0x7f020b78

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/facebook/friending/common/list/FriendListItemView;->a(LX/EyR;Landroid/graphics/drawable/Drawable;)V

    .line 451691
    const v1, 0x7f080f7b

    invoke-static {p0, v1}, LX/2iS;->b(LX/2iS;I)Ljava/lang/CharSequence;

    move-result-object v1

    const v2, 0x7f080f7c

    invoke-static {p0, v2}, LX/2iS;->b(LX/2iS;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/facebook/friending/common/list/FriendListItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 451692
    iget-object v1, p0, LX/2iS;->a:Landroid/content/res/Resources;

    const v2, 0x7f080f9f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 451693
    :pswitch_2
    sget-object v1, LX/EyR;->SECONDARY:LX/EyR;

    iget-object v2, p0, LX/2iS;->a:Landroid/content/res/Resources;

    const v3, 0x7f020b7a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/facebook/friending/common/list/FriendListItemView;->a(LX/EyR;Landroid/graphics/drawable/Drawable;)V

    .line 451694
    const v1, 0x7f080017

    invoke-static {p0, v1}, LX/2iS;->b(LX/2iS;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1, v4}, Lcom/facebook/friending/common/list/FriendListItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 451695
    iget-object v1, p0, LX/2iS;->a:Landroid/content/res/Resources;

    const v2, 0x7f080f9e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 451696
    :pswitch_3
    sget-object v1, LX/EyR;->SECONDARY:LX/EyR;

    iget-object v2, p0, LX/2iS;->a:Landroid/content/res/Resources;

    const v3, 0x7f020b7e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/facebook/friending/common/list/FriendListItemView;->a(LX/EyR;Landroid/graphics/drawable/Drawable;)V

    .line 451697
    const v1, 0x7f080f76

    invoke-static {p0, v1}, LX/2iS;->b(LX/2iS;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1, v4}, Lcom/facebook/friending/common/list/FriendListItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 451698
    iget-object v1, p0, LX/2iS;->a:Landroid/content/res/Resources;

    const v2, 0x7f080f9d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a$redex0(LX/2iS;Lcom/facebook/friends/model/PersonYouMayKnow;)V
    .locals 7

    .prologue
    .line 451672
    invoke-static {p0}, LX/2iS;->c(LX/2iS;)V

    .line 451673
    iget-object v0, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v0, v0

    .line 451674
    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    .line 451675
    iput-object v5, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 451676
    invoke-static {v5}, LX/2iS;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 451677
    iget-object v1, p0, LX/2iS;->n:LX/2ia;

    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, LX/2ia;->a(J)V

    .line 451678
    iget-object v1, p0, LX/2iS;->c:LX/2iT;

    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->b()Ljava/lang/String;

    iget-object v4, p0, LX/2iS;->l:LX/2h7;

    new-instance v6, LX/EzE;

    invoke-direct {v6, p0, p1, v0, v5}, LX/EzE;-><init>(LX/2iS;Lcom/facebook/friends/model/PersonYouMayKnow;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    invoke-virtual/range {v1 .. v6}, LX/2iT;->a(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)V

    .line 451679
    return-void
.end method

.method public static b(LX/2iS;I)Ljava/lang/CharSequence;
    .locals 3
    .param p0    # LX/2iS;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 451723
    iget-object v0, p0, LX/2iS;->b:LX/23P;

    iget-object v1, p0, LX/2iS;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/2iS;Lcom/facebook/fbui/widget/contentview/ContentView;Lcom/facebook/friends/model/PersonYouMayKnow;)V
    .locals 3

    .prologue
    .line 451653
    invoke-virtual {p2}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 451654
    iget-object v1, p2, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v1, v1

    .line 451655
    sget-object v2, LX/EzG;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 451656
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/friends/model/PersonYouMayKnow;->e()I

    move-result v0

    invoke-static {p0, v0}, LX/2iS;->a(LX/2iS;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 451657
    :goto_0
    const-string v0, "%s %s"

    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSubtitleText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 451658
    return-void

    .line 451659
    :pswitch_0
    if-eqz v1, :cond_0

    .line 451660
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 451661
    const v0, 0x7f080f8b

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto :goto_0

    .line 451662
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 451663
    const v0, 0x7f080f86

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto :goto_0

    .line 451664
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 451665
    const v0, 0x7f080fa2

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto :goto_0

    .line 451666
    :cond_3
    invoke-virtual {p2}, Lcom/facebook/friends/model/PersonYouMayKnow;->e()I

    move-result v0

    invoke-static {p0, v0}, LX/2iS;->a(LX/2iS;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 451667
    :pswitch_1
    invoke-virtual {p2}, Lcom/facebook/friends/model/PersonYouMayKnow;->e()I

    move-result v0

    invoke-static {p0, v0}, LX/2iS;->a(LX/2iS;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 451668
    :pswitch_2
    const v0, 0x7f080f85

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto :goto_0

    .line 451669
    :pswitch_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 451670
    const v0, 0x7f080f8a

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto :goto_0

    .line 451671
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/friends/model/PersonYouMayKnow;->e()I

    move-result v0

    invoke-static {p0, v0}, LX/2iS;->a(LX/2iS;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(LX/2iS;J)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 451646
    invoke-static {p0, p1, p2}, LX/2iS;->a(LX/2iS;J)Lcom/facebook/fbui/widget/contentview/ContentView;

    move-result-object v0

    .line 451647
    if-nez v0, :cond_0

    move v0, v1

    .line 451648
    :goto_0
    return v0

    .line 451649
    :cond_0
    const v2, 0x7f0d01ff

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 451650
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v2

    cmp-long v0, v2, p1

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 451651
    goto :goto_0

    .line 451652
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(LX/2iS;)V
    .locals 3

    .prologue
    .line 451642
    iget-object v0, p0, LX/2iS;->k:LX/0ad;

    sget-short v1, LX/2ez;->m:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451643
    iget-object v0, p0, LX/2iS;->h:LX/2dl;

    const-string v1, "REQUESTS_TAB_PYMK_QUERY_TAG"

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2dl;->a(Ljava/util/Collection;)V

    .line 451644
    :goto_0
    return-void

    .line 451645
    :cond_0
    iget-object v0, p0, LX/2iS;->i:LX/1mR;

    const-string v1, "REQUESTS_TAB_PYMK_QUERY_TAG"

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public static c$redex0(LX/2iS;Lcom/facebook/friends/model/PersonYouMayKnow;)V
    .locals 11

    .prologue
    .line 451628
    invoke-static {p0}, LX/2iS;->c(LX/2iS;)V

    .line 451629
    iget-object v0, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v3, v0

    .line 451630
    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    .line 451631
    invoke-static {v4}, LX/2iS;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    .line 451632
    iput-object v4, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 451633
    invoke-virtual {p1, v5}, Lcom/facebook/friends/model/PersonYouMayKnow;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 451634
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v5}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v5}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 451635
    :cond_0
    iget-object v0, p0, LX/2iS;->n:LX/2ia;

    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v6

    invoke-interface {v0, v6, v7}, LX/2ia;->c(J)V

    .line 451636
    :cond_1
    :goto_0
    iget-object v9, p0, LX/2iS;->c:LX/2iT;

    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->b()Ljava/lang/String;

    iget-object v8, p0, LX/2iS;->l:LX/2h7;

    new-instance v0, LX/EzF;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LX/EzF;-><init>(LX/2iS;Lcom/facebook/friends/model/PersonYouMayKnow;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    move-object v5, v9

    move-object v9, v4

    move-object v10, v0

    invoke-virtual/range {v5 .. v10}, LX/2iT;->a(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)V

    .line 451637
    return-void

    .line 451638
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, LX/2iS;->a(LX/2iS;J)Lcom/facebook/fbui/widget/contentview/ContentView;

    move-result-object v0

    .line 451639
    instance-of v1, v0, Lcom/facebook/friending/common/list/FriendListItemView;

    if-eqz v1, :cond_1

    .line 451640
    invoke-static {p0, v0, p1}, LX/2iS;->b(LX/2iS;Lcom/facebook/fbui/widget/contentview/ContentView;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    .line 451641
    check-cast v0, Lcom/facebook/friending/common/list/FriendListItemView;

    invoke-static {p0, v0, p1}, LX/2iS;->a$redex0(LX/2iS;Lcom/facebook/friending/common/list/FriendListItemView;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 451594
    iget-object v0, p0, LX/2iS;->c:LX/2iT;

    const/4 v1, 0x0

    .line 451595
    iput-boolean v1, v0, LX/2hY;->d:Z

    .line 451596
    return-void
.end method

.method public final a(Lcom/facebook/fbui/widget/contentview/ContentView;Lcom/facebook/friends/model/PersonYouMayKnow;)V
    .locals 8

    .prologue
    .line 451601
    invoke-virtual {p2}, Lcom/facebook/friends/model/PersonYouMayKnow;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 451602
    invoke-virtual {p2}, Lcom/facebook/friends/model/PersonYouMayKnow;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 451603
    invoke-static {p0, p1, p2}, LX/2iS;->b(LX/2iS;Lcom/facebook/fbui/widget/contentview/ContentView;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    .line 451604
    instance-of v0, p1, Lcom/facebook/friending/common/list/FriendRequestItemView;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 451605
    check-cast v0, Lcom/facebook/friending/common/list/FriendRequestItemView;

    const/4 v3, 0x0

    .line 451606
    iget-object v1, p0, LX/2iS;->f:LX/2ha;

    invoke-virtual {v1}, LX/2ha;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 451607
    const v1, 0x7f080020

    invoke-static {p0, v1}, LX/2iS;->b(LX/2iS;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/facebook/friending/common/list/FriendRequestItemView;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 451608
    const v1, 0x7f080021

    invoke-static {p0, v1}, LX/2iS;->b(LX/2iS;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    .line 451609
    :goto_0
    invoke-virtual {p2}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 451610
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v2, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v2, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 451611
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setFriendRequestButtonsVisible(Z)V

    .line 451612
    invoke-virtual {v0, v3}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451613
    invoke-virtual {v0, v3}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451614
    :goto_1
    const v2, 0x7f0d01ff

    invoke-virtual {p1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 451615
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 451616
    iget-object v3, p0, LX/2iS;->m:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 451617
    :cond_0
    const v2, 0x7f0d01ff

    invoke-virtual {p1, v2, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTag(ILjava/lang/Object;)V

    .line 451618
    iget-object v2, p0, LX/2iS;->m:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451619
    return-void

    .line 451620
    :cond_1
    instance-of v0, p1, Lcom/facebook/friending/common/list/FriendListItemView;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 451621
    check-cast v0, Lcom/facebook/friending/common/list/FriendListItemView;

    invoke-static {p0, v0, p2}, LX/2iS;->a$redex0(LX/2iS;Lcom/facebook/friending/common/list/FriendListItemView;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    goto :goto_1

    .line 451622
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ContentView not an instance of FriendRequestItemView or FriendListItemView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 451623
    :cond_3
    const v1, 0x7f080f7b

    invoke-static {p0, v1}, LX/2iS;->b(LX/2iS;I)Ljava/lang/CharSequence;

    move-result-object v1

    const v2, 0x7f080f7c

    invoke-static {p0, v2}, LX/2iS;->b(LX/2iS;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/friending/common/list/FriendRequestItemView;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 451624
    const v1, 0x7f080f78

    invoke-static {p0, v1}, LX/2iS;->b(LX/2iS;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 451625
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setFriendRequestButtonsVisible(Z)V

    .line 451626
    new-instance v1, LX/EzB;

    invoke-direct {v1, p0, p2}, LX/EzB;-><init>(LX/2iS;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    invoke-virtual {v0, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451627
    new-instance v1, LX/EzC;

    invoke-direct {v1, p0, p2}, LX/EzC;-><init>(LX/2iS;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    invoke-virtual {v0, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 451597
    iget-object v0, p0, LX/2iS;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 451598
    iget-object v0, p0, LX/2iS;->c:LX/2iT;

    const/4 v1, 0x1

    .line 451599
    iput-boolean v1, v0, LX/2hY;->d:Z

    .line 451600
    return-void
.end method
