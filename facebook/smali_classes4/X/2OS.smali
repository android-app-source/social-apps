.class public LX/2OS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;

.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/regex/Pattern;

.field private static o:LX/0Xm;


# instance fields
.field private final d:LX/2Ly;

.field private final e:LX/2Lz;

.field private final f:LX/2M0;

.field public final g:LX/2OT;

.field public final h:LX/2OU;

.field public final i:LX/03V;

.field public final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6eZ;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Uh;

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1m0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 402231
    const-class v0, LX/2OS;

    sput-object v0, LX/2OS;->a:Ljava/lang/Class;

    .line 402232
    const-string v0, "_[ts]\\.jpg$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/2OS;->b:Ljava/util/regex/Pattern;

    .line 402233
    const-string v0, "\\/[ts]([^/]+\\.jpg)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/2OS;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(LX/2Ly;LX/2M0;LX/2Lz;LX/2OT;LX/2OU;LX/03V;LX/0Or;LX/0Or;LX/0Uh;)V
    .locals 1
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/webp/annotation/IsWebpEnabled;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ly;",
            "LX/2M0;",
            "LX/2Lz;",
            "LX/2OT;",
            "LX/2OU;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/6eZ;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .prologue
    .line 402217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402218
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 402219
    iput-object v0, p0, LX/2OS;->n:LX/0Ot;

    .line 402220
    iput-object p1, p0, LX/2OS;->d:LX/2Ly;

    .line 402221
    iput-object p2, p0, LX/2OS;->f:LX/2M0;

    .line 402222
    iput-object p3, p0, LX/2OS;->e:LX/2Lz;

    .line 402223
    iput-object p4, p0, LX/2OS;->g:LX/2OT;

    .line 402224
    iput-object p5, p0, LX/2OS;->h:LX/2OU;

    .line 402225
    iput-object p6, p0, LX/2OS;->i:LX/03V;

    .line 402226
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/2OS;->j:Ljava/util/Set;

    .line 402227
    iput-object p7, p0, LX/2OS;->l:LX/0Or;

    .line 402228
    iput-object p8, p0, LX/2OS;->k:LX/0Or;

    .line 402229
    iput-object p9, p0, LX/2OS;->m:LX/0Uh;

    .line 402230
    return-void
.end method

.method public static a(LX/0QB;)LX/2OS;
    .locals 13

    .prologue
    .line 402201
    const-class v1, LX/2OS;

    monitor-enter v1

    .line 402202
    :try_start_0
    sget-object v0, LX/2OS;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 402203
    sput-object v2, LX/2OS;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 402204
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402205
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 402206
    new-instance v3, LX/2OS;

    invoke-static {v0}, LX/2Ly;->a(LX/0QB;)LX/2Ly;

    move-result-object v4

    check-cast v4, LX/2Ly;

    invoke-static {v0}, LX/2M0;->a(LX/0QB;)LX/2M0;

    move-result-object v5

    check-cast v5, LX/2M0;

    invoke-static {v0}, LX/2Lz;->a(LX/0QB;)LX/2Lz;

    move-result-object v6

    check-cast v6, LX/2Lz;

    invoke-static {v0}, LX/2OT;->b(LX/0QB;)LX/2OT;

    move-result-object v7

    check-cast v7, LX/2OT;

    .line 402207
    new-instance v9, LX/2OU;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v8

    check-cast v8, LX/0W3;

    invoke-direct {v9, v8}, LX/2OU;-><init>(LX/0W3;)V

    .line 402208
    move-object v8, v9

    .line 402209
    check-cast v8, LX/2OU;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    const/16 v10, 0x2805

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x159c

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-direct/range {v3 .. v12}, LX/2OS;-><init>(LX/2Ly;LX/2M0;LX/2Lz;LX/2OT;LX/2OU;LX/03V;LX/0Or;LX/0Or;LX/0Uh;)V

    .line 402210
    const/16 v4, 0x1358

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    .line 402211
    iput-object v4, v3, LX/2OS;->n:LX/0Ot;

    .line 402212
    move-object v0, v3

    .line 402213
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 402214
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2OS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 402215
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 402216
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/2OS;Ljava/lang/String;Lcom/facebook/messaging/model/attachment/Attachment;)Landroid/net/Uri;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 402191
    iget-object v0, p0, LX/2OS;->g:LX/2OT;

    invoke-virtual {v0}, LX/2OT;->a()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 402192
    invoke-static {p1}, LX/2OT;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 402193
    const-string v2, "mid"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 402194
    if-eqz p2, :cond_1

    .line 402195
    const-string v0, "aid"

    iget-object v2, p2, Lcom/facebook/messaging/model/attachment/Attachment;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 402196
    :goto_0
    const-string v0, "format"

    const-string v2, "binary"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 402197
    iget-object v0, p0, LX/2OS;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    if-eqz v0, :cond_0

    .line 402198
    const-string v0, "ext"

    const-string v2, "webp"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 402199
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 402200
    :cond_1
    const-string v0, "aid"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/attachment/Attachment;)Lcom/facebook/messaging/attachments/AudioAttachmentData;
    .locals 7

    .prologue
    .line 402181
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {p0, v0, p2}, LX/2OS;->a(LX/2OS;Ljava/lang/String;Lcom/facebook/messaging/model/attachment/Attachment;)Landroid/net/Uri;

    move-result-object v4

    .line 402182
    iget-object v0, p2, Lcom/facebook/messaging/model/attachment/Attachment;->e:Ljava/lang/String;

    invoke-static {v0}, LX/6bj;->a(Ljava/lang/String;)J

    move-result-wide v2

    .line 402183
    const/4 v5, 0x0

    .line 402184
    const-string v6, ""

    .line 402185
    iget-object v0, p2, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    if-eqz v0, :cond_0

    .line 402186
    iget-object v0, p2, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    .line 402187
    iget-boolean v1, v0, Lcom/facebook/messaging/model/attachment/AudioData;->a:Z

    move v5, v1

    .line 402188
    iget-object v0, p2, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    .line 402189
    iget-object v1, v0, Lcom/facebook/messaging/model/attachment/AudioData;->b:Ljava/lang/String;

    move-object v6, v1

    .line 402190
    :cond_0
    new-instance v1, Lcom/facebook/messaging/attachments/AudioAttachmentData;

    invoke-direct/range {v1 .. v6}, Lcom/facebook/messaging/attachments/AudioAttachmentData;-><init>(JLandroid/net/Uri;ZLjava/lang/String;)V

    return-object v1
.end method

.method public static a(LX/2OS;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/attachment/Attachment;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/messaging/attachments/ImageAttachmentData;
    .locals 4
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/messaging/model/attachment/Attachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 402144
    new-instance v0, LX/6bp;

    invoke-direct {v0}, LX/6bp;-><init>()V

    .line 402145
    if-eqz p2, :cond_0

    iget-object v1, p2, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    if-eqz v1, :cond_0

    .line 402146
    iget-object v1, p2, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    .line 402147
    iget v2, v1, Lcom/facebook/messaging/model/attachment/ImageData;->a:I

    .line 402148
    iput v2, v0, LX/6bp;->c:I

    .line 402149
    move-object v2, v0

    .line 402150
    iget v3, v1, Lcom/facebook/messaging/model/attachment/ImageData;->b:I

    .line 402151
    iput v3, v2, LX/6bp;->d:I

    .line 402152
    move-object v2, v2

    .line 402153
    iget-object v3, p2, Lcom/facebook/messaging/model/attachment/Attachment;->c:Ljava/lang/String;

    .line 402154
    iput-object v3, v2, LX/6bp;->f:Ljava/lang/String;

    .line 402155
    move-object v2, v2

    .line 402156
    iput-object p3, v2, LX/6bp;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 402157
    move-object v2, v2

    .line 402158
    iget-boolean v3, v1, Lcom/facebook/messaging/model/attachment/ImageData;->f:Z

    .line 402159
    iput-boolean v3, v2, LX/6bp;->g:Z

    .line 402160
    move-object v2, v2

    .line 402161
    iget-object v3, v1, Lcom/facebook/messaging/model/attachment/ImageData;->g:Ljava/lang/String;

    .line 402162
    iput-object v3, v2, LX/6bp;->h:Ljava/lang/String;

    .line 402163
    move-object v2, v2

    .line 402164
    iget-object v3, p2, Lcom/facebook/messaging/model/attachment/Attachment;->d:Ljava/lang/String;

    .line 402165
    iput-object v3, v2, LX/6bp;->i:Ljava/lang/String;

    .line 402166
    iget-object v2, v1, Lcom/facebook/messaging/model/attachment/ImageData;->c:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, v2, v3}, LX/2OS;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/attachment/Attachment;Lcom/facebook/messaging/model/attachment/AttachmentImageMap;Z)Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    move-result-object v2

    .line 402167
    iput-object v2, v0, LX/6bp;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    .line 402168
    iget-object v1, v1, Lcom/facebook/messaging/model/attachment/ImageData;->d:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, v1, v2}, LX/2OS;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/attachment/Attachment;Lcom/facebook/messaging/model/attachment/AttachmentImageMap;Z)Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    move-result-object v1

    .line 402169
    iput-object v1, v0, LX/6bp;->b:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    .line 402170
    :cond_0
    iget-object v1, v0, LX/6bp;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    move-object v1, v1

    .line 402171
    if-nez v1, :cond_1

    .line 402172
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {p0, v1, p2}, LX/2OS;->a(LX/2OS;Ljava/lang/String;Lcom/facebook/messaging/model/attachment/Attachment;)Landroid/net/Uri;

    move-result-object v1

    .line 402173
    invoke-static {}, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->newBuilder()LX/6br;

    move-result-object v2

    .line 402174
    iput-object v1, v2, LX/6br;->a:Landroid/net/Uri;

    .line 402175
    move-object v2, v2

    .line 402176
    iput-object v1, v2, LX/6br;->e:Landroid/net/Uri;

    .line 402177
    move-object v1, v2

    .line 402178
    invoke-virtual {v1}, LX/6br;->f()Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    move-result-object v1

    .line 402179
    iput-object v1, v0, LX/6bp;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    .line 402180
    :cond_1
    invoke-virtual {v0}, LX/6bp;->j()Lcom/facebook/messaging/attachments/ImageAttachmentData;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/messaging/attachments/ImageAttachmentData;
    .locals 2

    .prologue
    .line 402124
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 402125
    new-instance v1, Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/attachments/ImageAttachmentUris;-><init>(Landroid/net/Uri;)V

    move-object v0, v1

    .line 402126
    new-instance v1, LX/6bp;

    invoke-direct {v1}, LX/6bp;-><init>()V

    .line 402127
    iput-object v0, v1, LX/6bp;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    .line 402128
    move-object v0, v1

    .line 402129
    iput-object p0, v0, LX/6bp;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 402130
    move-object v0, v0

    .line 402131
    iget v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    .line 402132
    iput v1, v0, LX/6bp;->c:I

    .line 402133
    move-object v0, v0

    .line 402134
    iget v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    .line 402135
    iput v1, v0, LX/6bp;->d:I

    .line 402136
    move-object v0, v0

    .line 402137
    iget-boolean v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->E:Z

    .line 402138
    iput-boolean v1, v0, LX/6bp;->g:Z

    .line 402139
    move-object v0, v0

    .line 402140
    const/4 v1, 0x0

    .line 402141
    iput-object v1, v0, LX/6bp;->h:Ljava/lang/String;

    .line 402142
    move-object v0, v0

    .line 402143
    invoke-virtual {v0}, LX/6bp;->j()Lcom/facebook/messaging/attachments/ImageAttachmentData;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/attachment/Attachment;Lcom/facebook/messaging/model/attachment/AttachmentImageMap;Z)Lcom/facebook/messaging/attachments/ImageAttachmentUris;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 402234
    if-nez p3, :cond_1

    .line 402235
    :cond_0
    :goto_0
    return-object v1

    .line 402236
    :cond_1
    if-eqz p4, :cond_5

    .line 402237
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {p0, v0, p2}, LX/2OS;->a(LX/2OS;Ljava/lang/String;Lcom/facebook/messaging/model/attachment/Attachment;)Landroid/net/Uri;

    move-result-object v0

    .line 402238
    :goto_1
    invoke-static {p3}, LX/5dU;->a(Lcom/facebook/messaging/model/attachment/AttachmentImageMap;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 402239
    invoke-static {}, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->newBuilder()LX/6br;

    move-result-object v1

    sget-object v2, LX/5dQ;->FULL_SCREEN:LX/5dQ;

    invoke-virtual {p3, v2}, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a(LX/5dQ;)Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v2

    .line 402240
    iget-object v3, v2, Lcom/facebook/messaging/model/attachment/ImageUrl;->c:Ljava/lang/String;

    move-object v2, v3

    .line 402241
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 402242
    iput-object v2, v1, LX/6br;->a:Landroid/net/Uri;

    .line 402243
    move-object v1, v1

    .line 402244
    sget-object v2, LX/5dQ;->SMALL_PREVIEW:LX/5dQ;

    invoke-virtual {p3, v2}, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a(LX/5dQ;)Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v2

    .line 402245
    iget-object v3, v2, Lcom/facebook/messaging/model/attachment/ImageUrl;->c:Ljava/lang/String;

    move-object v2, v3

    .line 402246
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 402247
    iput-object v2, v1, LX/6br;->b:Landroid/net/Uri;

    .line 402248
    move-object v1, v1

    .line 402249
    sget-object v2, LX/5dQ;->MEDIUM_PREVIEW:LX/5dQ;

    invoke-virtual {p3, v2}, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a(LX/5dQ;)Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v2

    .line 402250
    iget-object v3, v2, Lcom/facebook/messaging/model/attachment/ImageUrl;->c:Ljava/lang/String;

    move-object v2, v3

    .line 402251
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 402252
    iput-object v2, v1, LX/6br;->c:Landroid/net/Uri;

    .line 402253
    move-object v1, v1

    .line 402254
    sget-object v2, LX/5dQ;->LARGE_PREVIEW:LX/5dQ;

    invoke-virtual {p3, v2}, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a(LX/5dQ;)Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v2

    .line 402255
    iget-object v3, v2, Lcom/facebook/messaging/model/attachment/ImageUrl;->c:Ljava/lang/String;

    move-object v2, v3

    .line 402256
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 402257
    iput-object v2, v1, LX/6br;->d:Landroid/net/Uri;

    .line 402258
    move-object v1, v1

    .line 402259
    iput-object v0, v1, LX/6br;->e:Landroid/net/Uri;

    .line 402260
    move-object v0, v1

    .line 402261
    invoke-virtual {v0}, LX/6br;->f()Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    move-result-object v1

    goto :goto_0

    .line 402262
    :cond_2
    const/4 v2, 0x0

    .line 402263
    invoke-static {}, LX/5dQ;->values()[LX/5dQ;

    move-result-object v4

    array-length v5, v4

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_8

    aget-object p0, v4, v3

    .line 402264
    invoke-virtual {p3, p0}, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a(LX/5dQ;)Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object p2

    .line 402265
    sget-object p4, LX/5dQ;->FULL_SCREEN:LX/5dQ;

    if-ne p0, p4, :cond_6

    .line 402266
    invoke-static {p2}, LX/5dU;->a(Lcom/facebook/messaging/model/attachment/ImageUrl;)Z

    move-result p0

    if-nez p0, :cond_7

    .line 402267
    :cond_3
    :goto_3
    move v2, v2

    .line 402268
    if-eqz v2, :cond_4

    .line 402269
    invoke-static {}, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->newBuilder()LX/6br;

    move-result-object v0

    sget-object v1, LX/5dQ;->FULL_SCREEN:LX/5dQ;

    invoke-virtual {p3, v1}, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a(LX/5dQ;)Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v1

    .line 402270
    iget-object v2, v1, Lcom/facebook/messaging/model/attachment/ImageUrl;->c:Ljava/lang/String;

    move-object v1, v2

    .line 402271
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 402272
    iput-object v1, v0, LX/6br;->a:Landroid/net/Uri;

    .line 402273
    move-object v0, v0

    .line 402274
    invoke-virtual {v0}, LX/6br;->f()Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    move-result-object v1

    goto/16 :goto_0

    .line 402275
    :cond_4
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v2, p3}, LX/5dU;->a(Ljava/lang/String;Lcom/facebook/messaging/model/attachment/AttachmentImageMap;)Ljava/lang/String;

    move-result-object v2

    .line 402276
    sget-object v3, LX/2OS;->a:Ljava/lang/Class;

    invoke-static {v3, v2}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    .line 402277
    if-eqz v0, :cond_0

    .line 402278
    invoke-static {}, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->newBuilder()LX/6br;

    move-result-object v1

    .line 402279
    iput-object v0, v1, LX/6br;->a:Landroid/net/Uri;

    .line 402280
    move-object v1, v1

    .line 402281
    iput-object v0, v1, LX/6br;->e:Landroid/net/Uri;

    .line 402282
    move-object v0, v1

    .line 402283
    invoke-virtual {v0}, LX/6br;->f()Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    move-result-object v1

    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    goto/16 :goto_1

    .line 402284
    :cond_6
    invoke-static {p2}, LX/5dU;->a(Lcom/facebook/messaging/model/attachment/ImageUrl;)Z

    move-result p0

    if-nez p0, :cond_3

    .line 402285
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 402286
    :cond_8
    const/4 v2, 0x1

    goto :goto_3
.end method

.method public static final a(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 402111
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->SET_IMAGE:LX/2uW;

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 402112
    :goto_0
    return v0

    .line 402113
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 402114
    goto :goto_0

    .line 402115
    :cond_1
    iget-object v4, p0, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 402116
    invoke-static {v0}, LX/2M0;->b(Lcom/facebook/messaging/model/attachment/Attachment;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 402117
    goto :goto_0

    .line 402118
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 402119
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_5

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 402120
    invoke-static {v0}, LX/5zt;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 402121
    goto :goto_0

    .line 402122
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_5
    move v0, v2

    .line 402123
    goto :goto_0
.end method

.method public static final b(Lcom/facebook/messaging/model/messages/Message;)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 402100
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    .line 402101
    :cond_0
    return v1

    .line 402102
    :cond_1
    iget-object v4, p0, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    move v1, v2

    :goto_0
    if-ge v3, v5, :cond_2

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 402103
    invoke-static {v0}, LX/2M0;->b(Lcom/facebook/messaging/model/attachment/Attachment;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 402104
    add-int/lit8 v0, v1, 0x1

    .line 402105
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 402106
    :cond_2
    if-gtz v1, :cond_0

    .line 402107
    invoke-virtual {p0}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_2
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 402108
    invoke-static {v0}, LX/5zt;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 402109
    add-int/lit8 v0, v1, 0x1

    .line 402110
    :goto_3
    add-int/lit8 v2, v2, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static final c(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 402091
    iget-object v4, p0, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 402092
    invoke-static {v0}, LX/2M0;->c(Lcom/facebook/messaging/model/attachment/Attachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 402093
    :goto_1
    return v0

    .line 402094
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 402095
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_3

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 402096
    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 402097
    goto :goto_1

    .line 402098
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_3
    move v0, v2

    .line 402099
    goto :goto_1
.end method

.method public static l(LX/2OS;Lcom/facebook/messaging/model/messages/Message;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/Message;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/video/engine/VideoDataSource;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 402028
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0, v2}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 402029
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-static {v1, v2}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 402030
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 402031
    if-eqz v0, :cond_1

    .line 402032
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 402033
    iput-object v4, v3, LX/2oE;->a:Landroid/net/Uri;

    .line 402034
    move-object v3, v3

    .line 402035
    sget-object v4, LX/097;->FROM_LOCAL_STORAGE:LX/097;

    .line 402036
    iput-object v4, v3, LX/2oE;->e:LX/097;

    .line 402037
    move-object v3, v3

    .line 402038
    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    .line 402039
    if-eqz v4, :cond_0

    .line 402040
    iput-object v4, v3, LX/2oE;->f:Landroid/graphics/RectF;

    .line 402041
    :cond_0
    move-object v4, v3

    .line 402042
    iget-boolean v3, v0, Lcom/facebook/ui/media/attachments/MediaResource;->n:Z

    if-eqz v3, :cond_4

    sget-object v3, LX/2oF;->MIRROR_HORIZONTALLY:LX/2oF;

    .line 402043
    :goto_0
    iput-object v3, v4, LX/2oE;->g:LX/2oF;

    .line 402044
    move-object v3, v4

    .line 402045
    invoke-virtual {v3}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v3

    move-object v0, v3

    .line 402046
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 402047
    :cond_1
    if-eqz v1, :cond_2

    .line 402048
    iget-object v0, p0, LX/2OS;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1m0;

    const/4 v3, 0x0

    .line 402049
    iget-object v4, p0, LX/2OS;->m:LX/0Uh;

    const/16 v5, 0x138

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, v1, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    if-eqz v4, :cond_5

    .line 402050
    iget-object v4, v1, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 402051
    iget-object v5, v4, Lcom/facebook/messaging/model/attachment/VideoData;->f:Landroid/net/Uri;

    move-object v4, v5

    .line 402052
    invoke-static {v4}, LX/1H1;->i(Landroid/net/Uri;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 402053
    iget-object v4, v1, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 402054
    iget-object v5, v4, Lcom/facebook/messaging/model/attachment/VideoData;->f:Landroid/net/Uri;

    move-object v4, v5

    .line 402055
    :goto_1
    move-object v3, v4

    .line 402056
    iget-object v4, v1, Lcom/facebook/messaging/model/attachment/Attachment;->a:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-virtual {v0, v3, v4, v5}, LX/1m0;->a(Landroid/net/Uri;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v0

    .line 402057
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v3

    .line 402058
    iput-object v0, v3, LX/2oE;->a:Landroid/net/Uri;

    .line 402059
    move-object v0, v3

    .line 402060
    sget-object v3, LX/097;->FROM_STREAM:LX/097;

    .line 402061
    iput-object v3, v0, LX/2oE;->e:LX/097;

    .line 402062
    move-object v0, v0

    .line 402063
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    move-object v0, v0

    .line 402064
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 402065
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->r()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 402066
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->r()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    move-result-object v0

    .line 402067
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 402068
    iput-object v3, v1, LX/2oE;->a:Landroid/net/Uri;

    .line 402069
    move-object v1, v1

    .line 402070
    sget-object v3, LX/097;->FROM_STREAM:LX/097;

    .line 402071
    iput-object v3, v1, LX/2oE;->e:LX/097;

    .line 402072
    move-object v1, v1

    .line 402073
    invoke-virtual {v1}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v1

    move-object v0, v1

    .line 402074
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 402075
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_4
    sget-object v3, LX/2oF;->NONE:LX/2oF;

    goto/16 :goto_0

    .line 402076
    :cond_5
    iget-object v4, p0, LX/2OS;->g:LX/2OT;

    .line 402077
    iget-object v5, v4, LX/2OT;->a:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0s9;

    invoke-interface {v5}, LX/0s9;->a()Landroid/net/Uri$Builder;

    move-result-object v5

    .line 402078
    const-string v6, "method/messaging.attachmentRedirect"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 402079
    invoke-static {v4, v5}, LX/2OT;->a(LX/2OT;Landroid/net/Uri$Builder;)V

    .line 402080
    move-object v5, v5

    .line 402081
    const-string v4, "id"

    iget-object v6, v1, Lcom/facebook/messaging/model/attachment/Attachment;->c:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 402082
    const-string v6, "preview"

    if-eqz v3, :cond_7

    const-string v4, "true"

    :goto_2
    invoke-virtual {v5, v6, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 402083
    iget-object v4, v1, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    if-eqz v4, :cond_6

    .line 402084
    iget-object v4, v1, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 402085
    iget-object v6, v4, Lcom/facebook/messaging/model/attachment/VideoData;->f:Landroid/net/Uri;

    move-object v4, v6

    .line 402086
    invoke-virtual {v4}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    .line 402087
    const-string v6, "video"

    invoke-virtual {v5, v6, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 402088
    :cond_6
    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    move-object v4, v4

    .line 402089
    goto/16 :goto_1

    .line 402090
    :cond_7
    const-string v4, "false"

    goto :goto_2
.end method


# virtual methods
.method public final e(Lcom/facebook/messaging/model/messages/Message;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/Message;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/attachments/ImageAttachmentData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 401989
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v1, LX/2uW;->SET_IMAGE:LX/2uW;

    if-ne v0, v1, :cond_0

    .line 401990
    invoke-static {p0, p1, v2, v2}, LX/2OS;->a(LX/2OS;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/attachment/Attachment;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/messaging/attachments/ImageAttachmentData;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 401991
    :goto_0
    return-object v0

    .line 401992
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 401993
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 401994
    goto :goto_0

    .line 401995
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 401996
    const/4 v1, 0x0

    .line 401997
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 401998
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 401999
    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 402000
    const/4 v6, 0x0

    invoke-static {p0, p1, v0, v6}, LX/2OS;->a(LX/2OS;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/attachment/Attachment;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/messaging/attachments/ImageAttachmentData;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 402001
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 402002
    :cond_2
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    :goto_2
    if-ge v1, v4, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 402003
    invoke-static {v0}, LX/2OS;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/messaging/attachments/ImageAttachmentData;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 402004
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 402005
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 402006
    goto :goto_0

    .line 402007
    :cond_4
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v1, LX/2uW;->PENDING_SEND:LX/2uW;

    if-eq v0, v1, :cond_5

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v1, LX/2uW;->FAILED_SEND:LX/2uW;

    if-eq v0, v1, :cond_5

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 402008
    :cond_5
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 402009
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v4, :cond_7

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 402010
    invoke-static {v0}, LX/5zt;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 402011
    invoke-static {v0}, LX/2OS;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/messaging/attachments/ImageAttachmentData;

    move-result-object v0

    .line 402012
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 402013
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 402014
    :cond_7
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 402015
    :cond_8
    const/4 v3, 0x0

    .line 402016
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 402017
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v0, v1, :cond_c

    .line 402018
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    move-object v2, v0

    .line 402019
    :goto_4
    const/4 v0, 0x0

    move v4, v0

    :goto_5
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v4, v0, :cond_b

    .line 402020
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 402021
    if-eqz v2, :cond_a

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 402022
    :goto_6
    invoke-static {v0}, LX/2M0;->b(Lcom/facebook/messaging/model/attachment/Attachment;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 402023
    invoke-static {p0, p1, v0, v1}, LX/2OS;->a(LX/2OS;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/attachment/Attachment;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/messaging/attachments/ImageAttachmentData;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 402024
    :cond_9
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_5

    :cond_a
    move-object v1, v3

    .line 402025
    goto :goto_6

    .line 402026
    :cond_b
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 402027
    goto/16 :goto_0

    :cond_c
    move-object v2, v3

    goto :goto_4
.end method

.method public final f(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/attachments/AudioAttachmentData;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 401972
    iget-object v0, p0, LX/2OS;->d:LX/2Ly;

    invoke-virtual {v0, p1}, LX/2Ly;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;

    move-result-object v0

    sget-object v3, LX/6eh;->AUDIO_CLIP:LX/6eh;

    if-eq v0, v3, :cond_1

    .line 401973
    :cond_0
    :goto_0
    return-object v1

    .line 401974
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 401975
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 401976
    iget-wide v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    .line 401977
    const-string v6, ""

    .line 401978
    const-wide/16 v8, 0x0

    cmp-long v1, v4, v8

    if-nez v1, :cond_4

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 401979
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/attachment/Attachment;

    iget-object v1, v1, Lcom/facebook/messaging/model/attachment/Attachment;->e:Ljava/lang/String;

    invoke-static {v1}, LX/6bj;->a(Ljava/lang/String;)J

    move-result-wide v8

    .line 401980
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/attachment/Attachment;

    iget-object v1, v1, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    if-eqz v1, :cond_3

    .line 401981
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/attachment/Attachment;

    iget-object v1, v1, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    .line 401982
    iget-boolean v3, v1, Lcom/facebook/messaging/model/attachment/AudioData;->a:Z

    move v5, v3

    .line 401983
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/attachment/Attachment;

    iget-object v1, v1, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    .line 401984
    iget-object v2, v1, Lcom/facebook/messaging/model/attachment/AudioData;->b:Ljava/lang/String;

    move-object v6, v2

    .line 401985
    move-wide v2, v8

    .line 401986
    :goto_1
    new-instance v1, Lcom/facebook/messaging/attachments/AudioAttachmentData;

    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-direct/range {v1 .. v6}, Lcom/facebook/messaging/attachments/AudioAttachmentData;-><init>(JLandroid/net/Uri;ZLjava/lang/String;)V

    goto :goto_0

    .line 401987
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 401988
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    invoke-direct {p0, p1, v0}, LX/2OS;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/attachment/Attachment;)Lcom/facebook/messaging/attachments/AudioAttachmentData;

    move-result-object v1

    goto :goto_0

    :cond_3
    move v5, v2

    move-wide v2, v8

    goto :goto_1

    :cond_4
    move v10, v2

    move-wide v2, v4

    move v5, v10

    goto :goto_1
.end method

.method public final g(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/attachments/VideoAttachmentData;
    .locals 13
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 401827
    if-nez p1, :cond_1

    .line 401828
    :cond_0
    :goto_0
    return-object v0

    .line 401829
    :cond_1
    iget-object v1, p0, LX/2OS;->e:LX/2Lz;

    invoke-virtual {v1, p1}, LX/2Lz;->b(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 401830
    const/4 v4, 0x0

    .line 401831
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-static {v2, v4}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 401832
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v3

    invoke-static {v3, v4}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 401833
    invoke-static {p0, p1}, LX/2OS;->l(LX/2OS;Lcom/facebook/messaging/model/messages/Message;)LX/0Px;

    move-result-object v4

    .line 401834
    invoke-static {}, Lcom/facebook/messaging/attachments/VideoAttachmentData;->newBuilder()LX/6by;

    move-result-object v5

    .line 401835
    iput-object v4, v5, LX/6by;->g:Ljava/util/List;

    .line 401836
    move-object v4, v5

    .line 401837
    sget-object v5, LX/6bx;->MESSAGE_ATTACHMENT:LX/6bx;

    .line 401838
    iput-object v5, v4, LX/6by;->a:LX/6bx;

    .line 401839
    move-object v4, v4

    .line 401840
    if-eqz v3, :cond_4

    .line 401841
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 401842
    iget-object v6, v3, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v7, LX/2MK;->VIDEO:LX/2MK;

    if-ne v6, v7, :cond_8

    const/4 v6, 0x1

    :goto_1
    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 401843
    iget-object v6, p0, LX/2OS;->l:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/6eZ;

    iget-boolean v6, v6, LX/6eZ;->a:Z

    if-eqz v6, :cond_9

    .line 401844
    iget-object v6, p0, LX/2OS;->h:LX/2OU;

    iget v7, v3, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    iget v8, v3, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    iget-object v9, v3, Lcom/facebook/ui/media/attachments/MediaResource;->m:LX/47d;

    invoke-static {v9}, LX/47e;->a(LX/47d;)I

    move-result v9

    iget-object v10, v3, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    sget-object v11, LX/7Sv;->NONE:LX/7Sv;

    invoke-virtual/range {v6 .. v11}, LX/2Md;->a(IIILandroid/graphics/RectF;LX/7Sv;)LX/7Sx;

    move-result-object v6

    .line 401845
    invoke-virtual {v6}, LX/7Sx;->a()I

    move-result v7

    invoke-static {v7}, LX/47e;->a(I)LX/47d;

    move-result-object v8

    .line 401846
    iget v7, v6, LX/7Sx;->d:I

    .line 401847
    iget v6, v6, LX/7Sx;->e:I

    .line 401848
    :goto_2
    sget-object v9, LX/47d;->ROTATE_90:LX/47d;

    if-eq v8, v9, :cond_2

    sget-object v9, LX/47d;->ROTATE_270:LX/47d;

    if-ne v8, v9, :cond_b

    :cond_2
    const/4 v9, 0x1

    :goto_3
    move v8, v9

    .line 401849
    if-eqz v8, :cond_a

    .line 401850
    :goto_4
    invoke-static {v3}, LX/6eg;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5dX;

    move-result-object v8

    .line 401851
    iput v6, v4, LX/6by;->b:I

    .line 401852
    move-object v6, v4

    .line 401853
    iput v7, v6, LX/6by;->c:I

    .line 401854
    move-object v6, v6

    .line 401855
    invoke-virtual {v3}, Lcom/facebook/ui/media/attachments/MediaResource;->c()I

    move-result v7

    .line 401856
    iput v7, v6, LX/6by;->e:I

    .line 401857
    move-object v6, v6

    .line 401858
    iget-wide v10, v3, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    long-to-int v7, v10

    .line 401859
    iput v7, v6, LX/6by;->f:I

    .line 401860
    move-object v6, v6

    .line 401861
    iget-object v7, v3, Lcom/facebook/ui/media/attachments/MediaResource;->g:Landroid/net/Uri;

    .line 401862
    iput-object v7, v6, LX/6by;->h:Landroid/net/Uri;

    .line 401863
    move-object v6, v6

    .line 401864
    iput-object v8, v6, LX/6by;->i:LX/5dX;

    .line 401865
    move-object v6, v6

    .line 401866
    const-string v7, ""

    .line 401867
    iput-object v7, v6, LX/6by;->j:Ljava/lang/String;

    .line 401868
    move-object v6, v6

    .line 401869
    iput-object v3, v6, LX/6by;->k:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 401870
    iget v6, v3, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    if-lez v6, :cond_3

    .line 401871
    iget v6, v3, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    .line 401872
    iput v6, v4, LX/6by;->l:I

    .line 401873
    :cond_3
    iget v6, v3, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    if-lez v6, :cond_4

    .line 401874
    iget v6, v3, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    .line 401875
    iput v6, v4, LX/6by;->m:I

    .line 401876
    :cond_4
    if-eqz v2, :cond_5

    .line 401877
    iget-object v3, v2, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    if-eqz v3, :cond_7

    .line 401878
    iget-object v6, v2, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 401879
    iget v7, v6, Lcom/facebook/messaging/model/attachment/VideoData;->d:I

    move v6, v7

    .line 401880
    int-to-long v6, v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    long-to-int v6, v6

    .line 401881
    iget-object v7, v2, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 401882
    iget v8, v7, Lcom/facebook/messaging/model/attachment/VideoData;->a:I

    move v7, v8

    .line 401883
    iput v7, v4, LX/6by;->b:I

    .line 401884
    move-object v7, v4

    .line 401885
    iget-object v8, v2, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 401886
    iget v9, v8, Lcom/facebook/messaging/model/attachment/VideoData;->b:I

    move v8, v9

    .line 401887
    iput v8, v7, LX/6by;->c:I

    .line 401888
    move-object v7, v7

    .line 401889
    iget-object v8, v2, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 401890
    iget v9, v8, Lcom/facebook/messaging/model/attachment/VideoData;->c:I

    move v8, v9

    .line 401891
    iput v8, v7, LX/6by;->d:I

    .line 401892
    move-object v7, v7

    .line 401893
    iput v6, v7, LX/6by;->e:I

    .line 401894
    move-object v6, v7

    .line 401895
    iget v7, v2, Lcom/facebook/messaging/model/attachment/Attachment;->f:I

    .line 401896
    iput v7, v6, LX/6by;->f:I

    .line 401897
    move-object v6, v6

    .line 401898
    iget-object v7, v2, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 401899
    iget-object v8, v7, Lcom/facebook/messaging/model/attachment/VideoData;->g:Landroid/net/Uri;

    move-object v7, v8

    .line 401900
    iput-object v7, v6, LX/6by;->h:Landroid/net/Uri;

    .line 401901
    move-object v6, v6

    .line 401902
    iget-object v7, v2, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 401903
    iget-object v8, v7, Lcom/facebook/messaging/model/attachment/VideoData;->e:LX/5dX;

    move-object v7, v8

    .line 401904
    iput-object v7, v6, LX/6by;->i:LX/5dX;

    .line 401905
    move-object v6, v6

    .line 401906
    iget-object v7, v2, Lcom/facebook/messaging/model/attachment/Attachment;->c:Ljava/lang/String;

    .line 401907
    iput-object v7, v6, LX/6by;->j:Ljava/lang/String;

    .line 401908
    :cond_5
    :goto_5
    invoke-virtual {v4}, LX/6by;->n()Lcom/facebook/messaging/attachments/VideoAttachmentData;

    move-result-object v2

    move-object v0, v2

    .line 401909
    goto/16 :goto_0

    .line 401910
    :cond_6
    iget-object v1, p0, LX/2OS;->e:LX/2Lz;

    const/4 v3, 0x0

    .line 401911
    iget-object v2, v1, LX/2Lz;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_c

    move v2, v3

    .line 401912
    :goto_6
    move v1, v2

    .line 401913
    if-eqz v1, :cond_0

    .line 401914
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 401915
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 401916
    invoke-static {p0, p1}, LX/2OS;->l(LX/2OS;Lcom/facebook/messaging/model/messages/Message;)LX/0Px;

    move-result-object v1

    .line 401917
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->r()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    move-result-object v2

    .line 401918
    if-nez v2, :cond_14

    .line 401919
    const/4 v0, 0x0

    .line 401920
    :goto_7
    move-object v0, v0

    .line 401921
    goto/16 :goto_0

    .line 401922
    :cond_7
    iget-object v3, p0, LX/2OS;->j:Ljava/util/Set;

    iget-object v5, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 401923
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Attachment without videoData info:\n"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "fbid: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v2, Lcom/facebook/messaging/model/attachment/Attachment;->c:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\nfilename: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v2, Lcom/facebook/messaging/model/attachment/Attachment;->e:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\nfileSize: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, v2, Lcom/facebook/messaging/model/attachment/Attachment;->f:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\nid: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v2, Lcom/facebook/messaging/model/attachment/Attachment;->a:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\nmimeType: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, v2, Lcom/facebook/messaging/model/attachment/Attachment;->d:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\nmessage info:\nid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\nmsgType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\nchannelSource: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->q:LX/6f2;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 401924
    iget-object v3, p0, LX/2OS;->i:LX/03V;

    const-string v5, "MESSENGER_INLINE_VIDEO_ATTACHMENT_WITHOUT_VIDEO_DATA"

    invoke-virtual {v3, v5, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 401925
    iget-object v2, p0, LX/2OS;->j:Ljava/util/Set;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 401926
    :cond_8
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 401927
    :cond_9
    iget-object v8, v3, Lcom/facebook/ui/media/attachments/MediaResource;->m:LX/47d;

    .line 401928
    iget v7, v3, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    .line 401929
    iget v6, v3, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    goto/16 :goto_2

    :cond_a
    move v12, v6

    move v6, v7

    move v7, v12

    goto/16 :goto_4

    :cond_b
    const/4 v9, 0x0

    goto/16 :goto_3

    .line 401930
    :cond_c
    iget-object v2, v1, LX/2Lz;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_d

    move v2, v3

    .line 401931
    goto/16 :goto_6

    .line 401932
    :cond_d
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    if-nez v2, :cond_e

    move v2, v3

    .line 401933
    goto/16 :goto_6

    .line 401934
    :cond_e
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v2

    .line 401935
    if-nez v2, :cond_f

    move v2, v3

    .line 401936
    goto/16 :goto_6

    .line 401937
    :cond_f
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->r()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    move-result-object v2

    .line 401938
    if-nez v2, :cond_10

    move v2, v3

    .line 401939
    goto/16 :goto_6

    .line 401940
    :cond_10
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-nez v4, :cond_11

    move v2, v3

    .line 401941
    goto/16 :goto_6

    .line 401942
    :cond_11
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v5, 0x4ed245b

    if-eq v4, v5, :cond_12

    move v2, v3

    .line 401943
    goto/16 :goto_6

    .line 401944
    :cond_12
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->n()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_13

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    if-eqz v4, :cond_13

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->o()I

    move-result v4

    if-lez v4, :cond_13

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->c()I

    move-result v2

    if-lez v2, :cond_13

    const/4 v2, 0x1

    goto/16 :goto_6

    :cond_13
    move v2, v3

    goto/16 :goto_6

    .line 401945
    :cond_14
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 401946
    if-nez v0, :cond_15

    .line 401947
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 401948
    :cond_15
    invoke-static {}, Lcom/facebook/messaging/attachments/VideoAttachmentData;->newBuilder()LX/6by;

    move-result-object v3

    sget-object v4, LX/6bx;->FACEBOOK_STORY_ATTACHMENT:LX/6bx;

    .line 401949
    iput-object v4, v3, LX/6by;->a:LX/6bx;

    .line 401950
    move-object v3, v3

    .line 401951
    iput-object v1, v3, LX/6by;->g:Ljava/util/List;

    .line 401952
    move-object v1, v3

    .line 401953
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->o()I

    move-result v3

    .line 401954
    iput v3, v1, LX/6by;->b:I

    .line 401955
    move-object v1, v1

    .line 401956
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->c()I

    move-result v3

    .line 401957
    iput v3, v1, LX/6by;->c:I

    .line 401958
    move-object v1, v1

    .line 401959
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 401960
    iput-object v3, v1, LX/6by;->j:Ljava/lang/String;

    .line 401961
    move-object v1, v1

    .line 401962
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->m()I

    move-result v2

    .line 401963
    iput v2, v1, LX/6by;->e:I

    .line 401964
    move-object v1, v1

    .line 401965
    sget-object v2, LX/5dX;->VIDEO_ATTACHMENT:LX/5dX;

    .line 401966
    iput-object v2, v1, LX/6by;->i:LX/5dX;

    .line 401967
    move-object v1, v1

    .line 401968
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 401969
    iput-object v0, v1, LX/6by;->h:Landroid/net/Uri;

    .line 401970
    move-object v0, v1

    .line 401971
    invoke-virtual {v0}, LX/6by;->n()Lcom/facebook/messaging/attachments/VideoAttachmentData;

    move-result-object v0

    goto/16 :goto_7
.end method
