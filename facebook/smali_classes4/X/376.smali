.class public LX/376;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/03V;

.field public final c:LX/0So;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Z

.field public g:J

.field public h:J


# direct methods
.method public constructor <init>(LX/03V;LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 500741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 500742
    const-class v0, LX/376;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/376;->a:Ljava/lang/String;

    .line 500743
    iput-object p1, p0, LX/376;->b:LX/03V;

    .line 500744
    iput-object p2, p0, LX/376;->c:LX/0So;

    .line 500745
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 500746
    iget-object v0, p0, LX/376;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 500747
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Watch and Go session start() called on an already initialized session"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500748
    :cond_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/376;->d:Ljava/lang/String;

    .line 500749
    iget-object v2, p0, LX/376;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/376;->g:J

    .line 500750
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/376;->e:Z

    .line 500751
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 500752
    iget-boolean v0, p0, LX/376;->e:Z

    if-nez v0, :cond_0

    .line 500753
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Watch and Go session pause() called on a non-started session"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500754
    :cond_0
    iget-boolean v0, p0, LX/376;->f:Z

    if-eqz v0, :cond_1

    .line 500755
    iget-object v0, p0, LX/376;->b:LX/03V;

    iget-object v1, p0, LX/376;->a:Ljava/lang/String;

    const-string v2, "Watch and Go session pause() called on an already paused session"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 500756
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/376;->f:Z

    .line 500757
    return-void

    .line 500758
    :cond_1
    iget-wide v0, p0, LX/376;->h:J

    iget-object v2, p0, LX/376;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/376;->g:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/376;->h:J

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 500759
    iget-boolean v0, p0, LX/376;->e:Z

    if-nez v0, :cond_0

    .line 500760
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Watch and Go session resume() called on a non-started session"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500761
    :cond_0
    iget-boolean v0, p0, LX/376;->f:Z

    if-nez v0, :cond_1

    .line 500762
    iget-object v0, p0, LX/376;->b:LX/03V;

    iget-object v1, p0, LX/376;->a:Ljava/lang/String;

    const-string v2, "Watch and Go session resume() called on a non-paused session"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 500763
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/376;->f:Z

    .line 500764
    return-void

    .line 500765
    :cond_1
    iget-object v0, p0, LX/376;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/376;->g:J

    goto :goto_0
.end method

.method public final d()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 500766
    iget-boolean v0, p0, LX/376;->e:Z

    if-nez v0, :cond_0

    .line 500767
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Watch and Go session finish() called on a non-started session"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500768
    :cond_0
    iget-boolean v0, p0, LX/376;->f:Z

    if-nez v0, :cond_1

    .line 500769
    iget-wide v0, p0, LX/376;->h:J

    iget-object v2, p0, LX/376;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/376;->g:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/376;->h:J

    .line 500770
    :goto_0
    iput-boolean v6, p0, LX/376;->e:Z

    .line 500771
    return-void

    .line 500772
    :cond_1
    iput-boolean v6, p0, LX/376;->f:Z

    goto :goto_0
.end method

.method public final f()J
    .locals 6

    .prologue
    .line 500773
    iget-wide v0, p0, LX/376;->h:J

    .line 500774
    iget-boolean v2, p0, LX/376;->e:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, LX/376;->f:Z

    if-nez v2, :cond_0

    .line 500775
    iget-object v2, p0, LX/376;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/376;->g:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 500776
    :cond_0
    return-wide v0
.end method
