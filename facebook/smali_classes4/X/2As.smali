.class public final LX/2As;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "LX/2At;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "LX/2Av;",
            "LX/2At;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 378310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;[Ljava/lang/Class;)LX/2At;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/2At;"
        }
    .end annotation

    .prologue
    .line 378307
    iget-object v0, p0, LX/2As;->a:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_0

    .line 378308
    const/4 v0, 0x0

    .line 378309
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2As;->a:Ljava/util/LinkedHashMap;

    new-instance v1, LX/2Av;

    invoke-direct {v1, p1, p2}, LX/2Av;-><init>(Ljava/lang/String;[Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2At;

    goto :goto_0
.end method

.method public final a(Ljava/lang/reflect/Method;)LX/2At;
    .locals 2

    .prologue
    .line 378304
    iget-object v0, p0, LX/2As;->a:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_0

    .line 378305
    iget-object v0, p0, LX/2As;->a:Ljava/util/LinkedHashMap;

    new-instance v1, LX/2Av;

    invoke-direct {v1, p1}, LX/2Av;-><init>(Ljava/lang/reflect/Method;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2At;

    .line 378306
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/2At;)V
    .locals 3

    .prologue
    .line 378298
    iget-object v0, p0, LX/2As;->a:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_0

    .line 378299
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/2As;->a:Ljava/util/LinkedHashMap;

    .line 378300
    :cond_0
    iget-object v0, p0, LX/2As;->a:Ljava/util/LinkedHashMap;

    new-instance v1, LX/2Av;

    .line 378301
    iget-object v2, p1, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v2, v2

    .line 378302
    invoke-direct {v1, v2}, LX/2Av;-><init>(Ljava/lang/reflect/Method;)V

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378303
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 378289
    iget-object v0, p0, LX/2As;->a:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2As;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/reflect/Method;)LX/2At;
    .locals 2

    .prologue
    .line 378295
    iget-object v0, p0, LX/2As;->a:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_0

    .line 378296
    const/4 v0, 0x0

    .line 378297
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2As;->a:Ljava/util/LinkedHashMap;

    new-instance v1, LX/2Av;

    invoke-direct {v1, p1}, LX/2Av;-><init>(Ljava/lang/reflect/Method;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2At;

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/2At;",
            ">;"
        }
    .end annotation

    .prologue
    .line 378290
    iget-object v0, p0, LX/2As;->a:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_0

    .line 378291
    iget-object v0, p0, LX/2As;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 378292
    :goto_0
    return-object v0

    .line 378293
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 378294
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method
