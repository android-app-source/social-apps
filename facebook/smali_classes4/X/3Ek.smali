.class public LX/3Ek;
.super LX/0Tr;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3Ek;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/3El;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 537975
    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    const-string v1, "notifications_db"

    invoke-direct {p0, p1, p2, v0, v1}, LX/0Tr;-><init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V

    .line 537976
    return-void
.end method

.method public static a(LX/0QB;)LX/3Ek;
    .locals 6

    .prologue
    .line 537977
    sget-object v0, LX/3Ek;->a:LX/3Ek;

    if-nez v0, :cond_1

    .line 537978
    const-class v1, LX/3Ek;

    monitor-enter v1

    .line 537979
    :try_start_0
    sget-object v0, LX/3Ek;->a:LX/3Ek;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 537980
    if-eqz v2, :cond_0

    .line 537981
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 537982
    new-instance p0, LX/3Ek;

    const-class v3, Landroid/content/Context;

    const-class v4, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, v4}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/3El;->a(LX/0QB;)LX/3El;

    move-result-object v5

    check-cast v5, LX/3El;

    invoke-direct {p0, v3, v4, v5}, LX/3Ek;-><init>(Landroid/content/Context;LX/0Tt;LX/3El;)V

    .line 537983
    move-object v0, p0

    .line 537984
    sput-object v0, LX/3Ek;->a:LX/3Ek;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 537985
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 537986
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 537987
    :cond_1
    sget-object v0, LX/3Ek;->a:LX/3Ek;

    return-object v0

    .line 537988
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 537989
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
