.class public LX/2oH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/engine/VideoDataSource;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:LX/162;

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public j:I

.field public k:I

.field public l:I

.field public m:Lcom/facebook/spherical/model/SphericalVideoParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Z

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 465988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465989
    iput-boolean v2, p0, LX/2oH;->o:Z

    .line 465990
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/2oH;->a:Ljava/util/List;

    .line 465991
    const-string v0, ""

    iput-object v0, p0, LX/2oH;->b:Ljava/lang/String;

    .line 465992
    iput v2, p0, LX/2oH;->c:I

    .line 465993
    const-string v0, ""

    iput-object v0, p0, LX/2oH;->d:Ljava/lang/String;

    .line 465994
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    iput-object v0, p0, LX/2oH;->e:LX/162;

    .line 465995
    iput-boolean v2, p0, LX/2oH;->f:Z

    .line 465996
    iput-boolean v2, p0, LX/2oH;->g:Z

    .line 465997
    iput-boolean v2, p0, LX/2oH;->h:Z

    .line 465998
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, LX/2oH;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 465999
    iput v2, p0, LX/2oH;->j:I

    .line 466000
    iput v2, p0, LX/2oH;->k:I

    .line 466001
    iput v2, p0, LX/2oH;->l:I

    .line 466002
    const/4 v0, 0x0

    iput-object v0, p0, LX/2oH;->m:Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 466003
    iput-boolean v2, p0, LX/2oH;->n:Z

    .line 466004
    iput v2, p0, LX/2oH;->p:I

    .line 466005
    iput v3, p0, LX/2oH;->q:I

    .line 466006
    iput v3, p0, LX/2oH;->r:I

    .line 466007
    iput-boolean v2, p0, LX/2oH;->t:Z

    .line 466008
    iput-boolean v2, p0, LX/2oH;->u:Z

    .line 466009
    iput-boolean v2, p0, LX/2oH;->v:Z

    .line 466010
    iput-boolean v2, p0, LX/2oH;->w:Z

    .line 466011
    iput-boolean v2, p0, LX/2oH;->x:Z

    .line 466012
    iput v3, p0, LX/2oH;->s:I

    .line 466013
    iput-boolean v2, p0, LX/2oH;->y:Z

    .line 466014
    return-void
.end method


# virtual methods
.method public final a(I)LX/2oH;
    .locals 0

    .prologue
    .line 466015
    iput p1, p0, LX/2oH;->c:I

    .line 466016
    return-object p0
.end method

.method public final a(II)LX/2oH;
    .locals 0

    .prologue
    .line 465983
    iput p1, p0, LX/2oH;->j:I

    .line 465984
    iput p2, p0, LX/2oH;->k:I

    .line 465985
    return-object p0
.end method

.method public final a(LX/162;)LX/2oH;
    .locals 0

    .prologue
    .line 466017
    iput-object p1, p0, LX/2oH;->e:LX/162;

    .line 466018
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)LX/2oH;
    .locals 0

    .prologue
    .line 466019
    iput-object p1, p0, LX/2oH;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466020
    return-object p0
.end method

.method public final a(Lcom/facebook/spherical/model/SphericalVideoParams;)LX/2oH;
    .locals 0
    .param p1    # Lcom/facebook/spherical/model/SphericalVideoParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 466021
    iput-object p1, p0, LX/2oH;->m:Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 466022
    return-object p0
.end method

.method public final a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;
    .locals 1

    .prologue
    .line 466023
    iget-object v0, p0, LX/2oH;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 466024
    return-object p0
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;)LX/2oH;
    .locals 1

    .prologue
    .line 466025
    iget-object v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    iput-object v0, p0, LX/2oH;->a:Ljava/util/List;

    .line 466026
    iget-object v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, LX/2oH;->b:Ljava/lang/String;

    .line 466027
    iget v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    iput v0, p0, LX/2oH;->c:I

    .line 466028
    iget-object v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->d:Ljava/lang/String;

    iput-object v0, p0, LX/2oH;->d:Ljava/lang/String;

    .line 466029
    iget-object v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iput-object v0, p0, LX/2oH;->e:LX/162;

    .line 466030
    iget-boolean v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    iput-boolean v0, p0, LX/2oH;->f:Z

    .line 466031
    iget-boolean v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->l:Z

    iput-boolean v0, p0, LX/2oH;->g:Z

    .line 466032
    iget-boolean v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    iput-boolean v0, p0, LX/2oH;->h:Z

    .line 466033
    iget-object v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, LX/2oH;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466034
    iget v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->u:I

    iput v0, p0, LX/2oH;->j:I

    .line 466035
    iget v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->v:I

    iput v0, p0, LX/2oH;->k:I

    .line 466036
    iget v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->w:I

    iput v0, p0, LX/2oH;->l:I

    .line 466037
    iget-object v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    iput-object v0, p0, LX/2oH;->m:Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 466038
    iget-boolean v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->m:Z

    iput-boolean v0, p0, LX/2oH;->n:Z

    .line 466039
    iget-boolean v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->n:Z

    iput-boolean v0, p0, LX/2oH;->o:Z

    .line 466040
    iget v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->p:I

    iput v0, p0, LX/2oH;->p:I

    .line 466041
    iget v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->q:I

    iput v0, p0, LX/2oH;->q:I

    .line 466042
    iget v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->r:I

    iput v0, p0, LX/2oH;->r:I

    .line 466043
    iget-boolean v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->t:Z

    iput-boolean v0, p0, LX/2oH;->t:Z

    .line 466044
    invoke-virtual {p1}, Lcom/facebook/video/engine/VideoPlayerParams;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/2oH;->u:Z

    .line 466045
    iget-boolean v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->j:Z

    iput-boolean v0, p0, LX/2oH;->v:Z

    .line 466046
    iget-boolean v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->k:Z

    iput-boolean v0, p0, LX/2oH;->w:Z

    .line 466047
    iget-boolean v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->o:Z

    iput-boolean v0, p0, LX/2oH;->x:Z

    .line 466048
    invoke-virtual {p1}, Lcom/facebook/video/engine/VideoPlayerParams;->h()I

    move-result v0

    iput v0, p0, LX/2oH;->s:I

    .line 466049
    iget-boolean v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->y:Z

    iput-boolean v0, p0, LX/2oH;->y:Z

    .line 466050
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/2oH;
    .locals 0

    .prologue
    .line 466051
    iput-object p1, p0, LX/2oH;->b:Ljava/lang/String;

    .line 466052
    return-object p0
.end method

.method public final a(Z)LX/2oH;
    .locals 0

    .prologue
    .line 466053
    iput-boolean p1, p0, LX/2oH;->f:Z

    .line 466054
    return-object p0
.end method

.method public final b(I)LX/2oH;
    .locals 0

    .prologue
    .line 466055
    iput p1, p0, LX/2oH;->l:I

    .line 466056
    return-object p0
.end method

.method public final b(Z)LX/2oH;
    .locals 0

    .prologue
    .line 465986
    iput-boolean p1, p0, LX/2oH;->h:Z

    .line 465987
    return-object p0
.end method

.method public final c(Z)LX/2oH;
    .locals 0

    .prologue
    .line 465981
    iput-boolean p1, p0, LX/2oH;->g:Z

    .line 465982
    return-object p0
.end method

.method public final d(I)LX/2oH;
    .locals 0

    .prologue
    .line 465979
    iput p1, p0, LX/2oH;->r:I

    .line 465980
    return-object p0
.end method

.method public final d(Z)LX/2oH;
    .locals 0

    .prologue
    .line 465977
    iput-boolean p1, p0, LX/2oH;->n:Z

    .line 465978
    return-object p0
.end method

.method public final e(I)LX/2oH;
    .locals 0

    .prologue
    .line 465975
    iput p1, p0, LX/2oH;->s:I

    .line 465976
    return-object p0
.end method

.method public final e(Z)LX/2oH;
    .locals 0

    .prologue
    .line 465973
    iput-boolean p1, p0, LX/2oH;->o:Z

    .line 465974
    return-object p0
.end method

.method public final g(Z)LX/2oH;
    .locals 0

    .prologue
    .line 465962
    iput-boolean p1, p0, LX/2oH;->u:Z

    .line 465963
    return-object p0
.end method

.method public final h(Z)LX/2oH;
    .locals 0

    .prologue
    .line 465971
    iput-boolean p1, p0, LX/2oH;->v:Z

    .line 465972
    return-object p0
.end method

.method public final i(Z)LX/2oH;
    .locals 0

    .prologue
    .line 465969
    iput-boolean p1, p0, LX/2oH;->w:Z

    .line 465970
    return-object p0
.end method

.method public final j(Z)LX/2oH;
    .locals 0

    .prologue
    .line 465967
    iput-boolean p1, p0, LX/2oH;->x:Z

    .line 465968
    return-object p0
.end method

.method public final k(Z)LX/2oH;
    .locals 0

    .prologue
    .line 465965
    iput-boolean p1, p0, LX/2oH;->y:Z

    .line 465966
    return-object p0
.end method

.method public final n()Lcom/facebook/video/engine/VideoPlayerParams;
    .locals 1

    .prologue
    .line 465964
    new-instance v0, Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-direct {v0, p0}, Lcom/facebook/video/engine/VideoPlayerParams;-><init>(LX/2oH;)V

    return-object v0
.end method
