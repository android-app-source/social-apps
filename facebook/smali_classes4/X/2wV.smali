.class public LX/2wV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2wU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2wU",
            "<",
            "LX/2xF;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private c:Landroid/content/ContentProviderClient;

.field public d:Z

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/2vi;",
            "LX/2xT;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "LX/7aD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2wU;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2wU",
            "<",
            "LX/2xF;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, LX/2wV;->c:Landroid/content/ContentProviderClient;

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2wV;->d:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2wV;->e:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2wV;->f:Ljava/util/Map;

    iput-object p1, p0, LX/2wV;->b:Landroid/content/Context;

    iput-object p2, p0, LX/2wV;->a:LX/2wU;

    return-void
.end method

.method private static a(LX/2wV;LX/2vi;Landroid/os/Looper;)LX/2xT;
    .locals 3

    iget-object v1, p0, LX/2wV;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/2wV;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2xT;

    if-nez v0, :cond_0

    new-instance v0, LX/2xT;

    invoke-direct {v0, p1, p2}, LX/2xT;-><init>(LX/2vi;Landroid/os/Looper;)V

    :cond_0
    iget-object v2, p0, LX/2wV;->e:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()Landroid/location/Location;
    .locals 2

    iget-object v0, p0, LX/2wV;->a:LX/2wU;

    invoke-interface {v0}, LX/2wU;->a()V

    :try_start_0
    iget-object v0, p0, LX/2wV;->a:LX/2wU;

    invoke-interface {v0}, LX/2wU;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LX/2xF;

    iget-object v1, p0, LX/2wV;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/2xF;->b(Ljava/lang/String;)Landroid/location/Location;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;LX/2vi;Landroid/os/Looper;LX/2xS;)V
    .locals 11

    iget-object v0, p0, LX/2wV;->a:LX/2wU;

    invoke-interface {v0}, LX/2wU;->a()V

    invoke-static {p0, p2, p3}, LX/2wV;->a(LX/2wV;LX/2vi;Landroid/os/Looper;)LX/2xT;

    move-result-object v1

    iget-object v0, p0, LX/2wV;->a:LX/2wU;

    invoke-interface {v0}, LX/2wU;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LX/2xF;

    invoke-static {p1}, Lcom/google/android/gms/location/internal/LocationRequestInternal;->a(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/location/internal/LocationRequestInternal;

    move-result-object v2

    const/4 v4, 0x1

    const/4 v8, 0x0

    new-instance v3, Lcom/google/android/gms/location/internal/LocationRequestUpdateData;

    invoke-interface {v1}, LX/2xV;->asBinder()Landroid/os/IBinder;

    move-result-object v7

    if-eqz p4, :cond_0

    invoke-interface {p4}, LX/2xS;->asBinder()Landroid/os/IBinder;

    move-result-object v10

    :goto_0
    move v5, v4

    move-object v6, v2

    move-object v9, v8

    invoke-direct/range {v3 .. v10}, Lcom/google/android/gms/location/internal/LocationRequestUpdateData;-><init>(IILcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/os/IBinder;Landroid/app/PendingIntent;Landroid/os/IBinder;Landroid/os/IBinder;)V

    move-object v1, v3

    invoke-interface {v0, v1}, LX/2xF;->a(Lcom/google/android/gms/location/internal/LocationRequestUpdateData;)V

    return-void

    :cond_0
    move-object v10, v8

    goto :goto_0
.end method

.method public final b()V
    .locals 13

    :try_start_0
    iget-object v2, p0, LX/2wV;->e:Ljava/util/Map;

    monitor-enter v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, LX/2wV;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2xT;

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/2wV;->a:LX/2wU;

    invoke-interface {v1}, LX/2wU;->b()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, LX/2xF;

    const/4 v8, 0x0

    new-instance v5, Lcom/google/android/gms/location/internal/LocationRequestUpdateData;

    const/4 v6, 0x1

    const/4 v7, 0x2

    invoke-interface {v0}, LX/2xV;->asBinder()Landroid/os/IBinder;

    move-result-object v9

    goto :goto_4

    :goto_1
    move-object v10, v8

    move-object v11, v8

    invoke-direct/range {v5 .. v12}, Lcom/google/android/gms/location/internal/LocationRequestUpdateData;-><init>(IILcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/os/IBinder;Landroid/app/PendingIntent;Landroid/os/IBinder;Landroid/os/IBinder;)V

    move-object v0, v5

    invoke-interface {v1, v0}, LX/2xF;->a(Lcom/google/android/gms/location/internal/LocationRequestUpdateData;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    :try_start_3
    iget-object v0, p0, LX/2wV;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v2, p0, LX/2wV;->f:Ljava/util/Map;

    monitor-enter v2
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    :try_start_5
    iget-object v0, p0, LX/2wV;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7aD;

    if-eqz v0, :cond_2

    iget-object v1, p0, LX/2wV;->a:LX/2wU;

    invoke-interface {v1}, LX/2wU;->b()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, LX/2xF;

    const/4 v8, 0x0

    new-instance v5, Lcom/google/android/gms/location/internal/LocationRequestUpdateData;

    const/4 v6, 0x1

    const/4 v7, 0x2

    invoke-interface {v0}, LX/7aB;->asBinder()Landroid/os/IBinder;

    move-result-object v11

    goto :goto_5

    :goto_3
    move-object v9, v8

    move-object v10, v8

    invoke-direct/range {v5 .. v12}, Lcom/google/android/gms/location/internal/LocationRequestUpdateData;-><init>(IILcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/os/IBinder;Landroid/app/PendingIntent;Landroid/os/IBinder;Landroid/os/IBinder;)V

    move-object v0, v5

    invoke-interface {v1, v0}, LX/2xF;->a(Lcom/google/android/gms/location/internal/LocationRequestUpdateData;)V

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0

    :cond_3
    :try_start_7
    iget-object v0, p0, LX/2wV;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    return-void

    :goto_4
    :try_start_8
    move-object v12, v8

    goto :goto_1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :goto_5
    move-object v12, v8

    goto :goto_3
.end method

.method public final c()V
    .locals 2

    iget-boolean v0, p0, LX/2wV;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, LX/2wV;->a:LX/2wU;

    invoke-interface {v1}, LX/2wU;->a()V

    iget-object v1, p0, LX/2wV;->a:LX/2wU;

    invoke-interface {v1}, LX/2wU;->b()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, LX/2xF;

    invoke-interface {v1, v0}, LX/2xF;->a(Z)V

    iput-boolean v0, p0, LX/2wV;->d:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
