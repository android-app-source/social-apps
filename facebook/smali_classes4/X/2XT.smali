.class public final LX/2XT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:Landroid/content/ComponentName;

.field public final b:LX/2XU;

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/content/ServiceConnection;",
            ">;"
        }
    .end annotation
.end field

.field public final d:I

.field public e:Z

.field public f:Landroid/os/IBinder;


# direct methods
.method public constructor <init>(Landroid/content/ComponentName;LX/2XU;I)V
    .locals 1

    .prologue
    .line 419919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419920
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/2XT;->c:Ljava/util/Set;

    .line 419921
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    iput-object v0, p0, LX/2XT;->a:Landroid/content/ComponentName;

    .line 419922
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2XU;

    iput-object v0, p0, LX/2XT;->b:LX/2XU;

    .line 419923
    iput p3, p0, LX/2XT;->d:I

    .line 419924
    return-void
.end method
