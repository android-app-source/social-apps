.class public LX/2F9;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 386597
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;
    .locals 7

    .prologue
    .line 386584
    sget-object v0, LX/2F9;->a:Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    if-nez v0, :cond_1

    .line 386585
    const-class v1, LX/2F9;

    monitor-enter v1

    .line 386586
    :try_start_0
    sget-object v0, LX/2F9;->a:Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 386587
    if-eqz v2, :cond_0

    .line 386588
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 386589
    invoke-static {v0}, LX/2FA;->a(LX/0QB;)Lcom/facebook/compactdisk/FileUtilsHolder;

    move-result-object v3

    check-cast v3, Lcom/facebook/compactdisk/FileUtilsHolder;

    invoke-static {v0}, LX/2FD;->a(LX/0QB;)Lcom/facebook/compactdisk/AttributeStoreHolder;

    move-result-object v4

    check-cast v4, Lcom/facebook/compactdisk/AttributeStoreHolder;

    invoke-static {v0}, LX/2FE;->a(LX/0QB;)Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    move-result-object v5

    check-cast v5, Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object p0

    check-cast p0, LX/0So;

    invoke-static {v3, v4, v5, v6, p0}, LX/2FB;->a(Lcom/facebook/compactdisk/FileUtilsHolder;Lcom/facebook/compactdisk/AttributeStoreHolder;Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    move-result-object v3

    move-object v0, v3

    .line 386590
    sput-object v0, LX/2F9;->a:Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 386591
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 386592
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 386593
    :cond_1
    sget-object v0, LX/2F9;->a:Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    return-object v0

    .line 386594
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 386595
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 386596
    invoke-static {p0}, LX/2FA;->a(LX/0QB;)Lcom/facebook/compactdisk/FileUtilsHolder;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdisk/FileUtilsHolder;

    invoke-static {p0}, LX/2FD;->a(LX/0QB;)Lcom/facebook/compactdisk/AttributeStoreHolder;

    move-result-object v1

    check-cast v1, Lcom/facebook/compactdisk/AttributeStoreHolder;

    invoke-static {p0}, LX/2FE;->a(LX/0QB;)Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    move-result-object v2

    check-cast v2, Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    invoke-static {p0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0, v1, v2, v3, v4}, LX/2FB;->a(Lcom/facebook/compactdisk/FileUtilsHolder;Lcom/facebook/compactdisk/AttributeStoreHolder;Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    move-result-object v0

    return-object v0
.end method
