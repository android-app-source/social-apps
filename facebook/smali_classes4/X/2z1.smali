.class public LX/2z1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/2z4;",
        "Lcom/facebook/resources/impl/loading/LanguagePackInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Wn;


# direct methods
.method public constructor <init>(LX/0Wn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 482885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482886
    iput-object p1, p0, LX/2z1;->a:LX/0Wn;

    .line 482887
    return-void
.end method

.method private a(LX/162;LX/2z4;)V
    .locals 2

    .prologue
    .line 482888
    if-nez p1, :cond_0

    .line 482889
    iget-object v0, p0, LX/2z1;->a:LX/0Wn;

    invoke-virtual {p2}, LX/2z4;->c()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Wn;->c(Ljava/util/Map;)V

    .line 482890
    new-instance v0, LX/K1j;

    invoke-direct {v0}, LX/K1j;-><init>()V

    throw v0

    .line 482891
    :cond_0
    invoke-virtual {p1}, LX/0lF;->e()I

    move-result v0

    if-nez v0, :cond_1

    .line 482892
    iget-object v0, p0, LX/2z1;->a:LX/0Wn;

    invoke-virtual {p2}, LX/2z4;->c()Ljava/util/Map;

    move-result-object v1

    .line 482893
    const-string p0, "empty"

    invoke-static {v0, p0, v1}, LX/0Wn;->a(LX/0Wn;Ljava/lang/String;Ljava/util/Map;)V

    .line 482894
    new-instance v0, LX/K1k;

    invoke-direct {v0}, LX/K1k;-><init>()V

    throw v0

    .line 482895
    :cond_1
    invoke-virtual {p1}, LX/0lF;->e()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    .line 482896
    iget-object v0, p0, LX/2z1;->a:LX/0Wn;

    invoke-virtual {p2}, LX/2z4;->c()Ljava/util/Map;

    move-result-object v1

    .line 482897
    const-string p0, "too_many"

    invoke-static {v0, p0, v1}, LX/0Wn;->a(LX/0Wn;Ljava/lang/String;Ljava/util/Map;)V

    .line 482898
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 2

    .prologue
    .line 482899
    check-cast p1, LX/2z4;

    .line 482900
    new-instance v0, LX/14O;

    invoke-direct {v0}, LX/14O;-><init>()V

    const-string v1, "getLanguagePackInfo"

    .line 482901
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 482902
    move-object v0, v0

    .line 482903
    const-string v1, "language_packs"

    .line 482904
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 482905
    move-object v0, v0

    .line 482906
    const-string v1, "GET"

    .line 482907
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 482908
    move-object v0, v0

    .line 482909
    iget-object v1, p1, LX/2z4;->a:LX/0ed;

    invoke-virtual {v1}, LX/0ed;->i()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v1

    move-object v1, v1

    .line 482910
    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-static {p1}, LX/2z4;->d(LX/2z4;)Ljava/util/List;

    move-result-object v1

    .line 482911
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 482912
    move-object v0, v0

    .line 482913
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 482914
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 482915
    move-object v0, v0

    .line 482916
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 482917
    check-cast p1, LX/2z4;

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 482918
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 482919
    const-string v1, "data"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/162;

    .line 482920
    invoke-direct {p0, v0, p1}, LX/2z1;->a(LX/162;LX/2z4;)V

    .line 482921
    invoke-static {p1}, LX/2z4;->e(LX/2z4;)Ljava/lang/String;

    move-result-object v5

    .line 482922
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    .line 482923
    const-string v1, "download_url"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    .line 482924
    const-string v2, "download_checksum"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v3

    .line 482925
    const-string v2, "content_checksum"

    invoke-virtual {v0, v2}, LX/0lF;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 482926
    const-string v2, "content_checksum"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v4

    .line 482927
    :goto_0
    const-string v2, "release_number"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, LX/0lF;->b(I)I

    move-result v2

    .line 482928
    invoke-virtual {p1}, LX/2z4;->b()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 482929
    const-string v8, "delta"

    invoke-virtual {v0, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 482930
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0lF;->q()Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v8

    if-nez v8, :cond_8

    :cond_0
    const/4 v8, 0x1

    :goto_1
    move v8, v8

    .line 482931
    if-nez v8, :cond_6

    .line 482932
    const/4 v7, 0x1

    .line 482933
    const-string v8, "download_url"

    invoke-virtual {v0, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 482934
    :goto_2
    invoke-virtual {p1}, LX/2z4;->a()LX/0eh;

    move-result-object v8

    .line 482935
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    if-eqz v2, :cond_3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    sget-object v9, LX/0eh;->LANGPACK:LX/0eh;

    if-ne v8, v9, :cond_1

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    :cond_1
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    if-eqz v7, :cond_4

    .line 482936
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_9

    :cond_2
    const/4 v8, 0x1

    :goto_3
    move v8, v8

    .line 482937
    if-eqz v8, :cond_4

    .line 482938
    :cond_3
    iget-object v0, p0, LX/2z1;->a:LX/0Wn;

    invoke-virtual {p1}, LX/2z4;->c()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Wn;->c(Ljava/util/Map;)V

    .line 482939
    new-instance v0, LX/K1j;

    invoke-direct {v0}, LX/K1j;-><init>()V

    throw v0

    .line 482940
    :catch_0
    move-exception v0

    .line 482941
    iget-object v1, p0, LX/2z1;->a:LX/0Wn;

    invoke-virtual {p1}, LX/2z4;->c()Ljava/util/Map;

    move-result-object v2

    .line 482942
    const-string v3, "json"

    invoke-static {v1, v3, v2}, LX/0Wn;->a(LX/0Wn;Ljava/lang/String;Ljava/util/Map;)V

    .line 482943
    throw v0

    .line 482944
    :cond_4
    if-eqz v7, :cond_5

    .line 482945
    new-instance v6, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;

    invoke-direct {v6, v0}, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;-><init>(Ljava/lang/String;)V

    .line 482946
    :cond_5
    new-instance v0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/resources/impl/loading/LanguagePackInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;)V

    return-object v0

    :cond_6
    move-object v0, v6

    goto :goto_2

    :cond_7
    move-object v4, v6

    goto/16 :goto_0

    :cond_8
    :try_start_1
    const/4 v8, 0x0

    goto :goto_1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_9
    const/4 v8, 0x0

    goto :goto_3
.end method
