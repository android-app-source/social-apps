.class public final LX/2sp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Gd5;I)V
    .locals 1

    .prologue
    .line 473936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 473937
    iput p2, p0, LX/2sp;->a:I

    .line 473938
    iget v0, p0, LX/2sp;->a:I

    if-lez v0, :cond_0

    .line 473939
    iget v0, p0, LX/2sp;->a:I

    invoke-virtual {p1, v0}, LX/Gd5;->b(I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/2sp;->b:Ljava/io/File;

    .line 473940
    :goto_0
    return-void

    .line 473941
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/2sp;->b:Ljava/io/File;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 473942
    iget-object v0, p0, LX/2sp;->b:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2sp;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 473943
    :goto_0
    return v0

    .line 473944
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 473945
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, LX/2sp;->b:Ljava/io/File;

    invoke-direct {v3, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 473946
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 473947
    goto :goto_0

    .line 473948
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
