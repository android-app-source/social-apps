.class public LX/3AZ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/3Aa;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8yt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 525551
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/3AZ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8yt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 525528
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 525529
    iput-object p1, p0, LX/3AZ;->b:LX/0Ot;

    .line 525530
    return-void
.end method

.method public static a(LX/0QB;)LX/3AZ;
    .locals 4

    .prologue
    .line 525540
    const-class v1, LX/3AZ;

    monitor-enter v1

    .line 525541
    :try_start_0
    sget-object v0, LX/3AZ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 525542
    sput-object v2, LX/3AZ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 525543
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525544
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 525545
    new-instance v3, LX/3AZ;

    const/16 p0, 0x193e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3AZ;-><init>(LX/0Ot;)V

    .line 525546
    move-object v0, v3

    .line 525547
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 525548
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3AZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 525549
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 525550
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 24

    .prologue
    .line 525552
    check-cast p2, LX/8yo;

    .line 525553
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3AZ;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8yt;

    move-object/from16 v0, p2

    iget-object v3, v0, LX/8yo;->a:LX/3Ab;

    move-object/from16 v0, p2

    iget-object v4, v0, LX/8yo;->b:LX/HJI;

    move-object/from16 v0, p2

    iget-object v5, v0, LX/8yo;->c:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p2

    iget-boolean v6, v0, LX/8yo;->d:Z

    move-object/from16 v0, p2

    iget-object v7, v0, LX/8yo;->e:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p2

    iget v8, v0, LX/8yo;->f:I

    move-object/from16 v0, p2

    iget v9, v0, LX/8yo;->g:I

    move-object/from16 v0, p2

    iget-boolean v10, v0, LX/8yo;->h:Z

    move-object/from16 v0, p2

    iget v11, v0, LX/8yo;->i:F

    move-object/from16 v0, p2

    iget v12, v0, LX/8yo;->j:I

    move-object/from16 v0, p2

    iget v13, v0, LX/8yo;->k:I

    move-object/from16 v0, p2

    iget v14, v0, LX/8yo;->l:I

    move-object/from16 v0, p2

    iget v15, v0, LX/8yo;->m:I

    move-object/from16 v0, p2

    iget v0, v0, LX/8yo;->n:I

    move/from16 v16, v0

    move-object/from16 v0, p2

    iget v0, v0, LX/8yo;->o:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    iget-object v0, v0, LX/8yo;->p:Landroid/graphics/Typeface;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    iget-object v0, v0, LX/8yo;->q:Landroid/graphics/Typeface;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, LX/8yo;->r:I

    move/from16 v20, v0

    move-object/from16 v0, p2

    iget v0, v0, LX/8yo;->s:I

    move/from16 v21, v0

    move-object/from16 v0, p2

    iget v0, v0, LX/8yo;->t:I

    move/from16 v22, v0

    move-object/from16 v0, p2

    iget v0, v0, LX/8yo;->u:I

    move/from16 v23, v0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v23}, LX/8yt;->a(LX/1De;LX/3Ab;LX/HJI;Landroid/text/TextUtils$TruncateAt;ZLandroid/text/Layout$Alignment;IIZFIIIIIILandroid/graphics/Typeface;Landroid/graphics/Typeface;IIII)LX/1Dg;

    move-result-object v1

    .line 525554
    return-object v1
.end method

.method public final a(LX/1De;II)LX/3Aa;
    .locals 2

    .prologue
    .line 525533
    new-instance v0, LX/8yo;

    invoke-direct {v0, p0}, LX/8yo;-><init>(LX/3AZ;)V

    .line 525534
    sget-object v1, LX/3AZ;->a:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Aa;

    .line 525535
    if-nez v1, :cond_0

    .line 525536
    new-instance v1, LX/3Aa;

    invoke-direct {v1}, LX/3Aa;-><init>()V

    .line 525537
    :cond_0
    invoke-static {v1, p1, p2, p3, v0}, LX/3Aa;->a$redex0(LX/3Aa;LX/1De;IILX/8yo;)V

    .line 525538
    move-object v0, v1

    .line 525539
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 525531
    invoke-static {}, LX/1dS;->b()V

    .line 525532
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/3Aa;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 525527
    invoke-virtual {p0, p1, v0, v0}, LX/3AZ;->a(LX/1De;II)LX/3Aa;

    move-result-object v0

    return-object v0
.end method
