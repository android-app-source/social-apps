.class public LX/2PG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DoO;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Dof;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/DoO;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Dof;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406118
    iput-object p1, p0, LX/2PG;->a:LX/0Or;

    .line 406119
    iput-object p2, p0, LX/2PG;->b:LX/0Or;

    .line 406120
    return-void
.end method

.method public static a(LX/0QB;)LX/2PG;
    .locals 3

    .prologue
    .line 406114
    new-instance v0, LX/2PG;

    const/16 v1, 0x2a22

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x2a14

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/2PG;-><init>(LX/0Or;LX/0Or;)V

    .line 406115
    move-object v0, v0

    .line 406116
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/IuI;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/IuI",
            "<",
            "LX/Ebk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 406110
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PG;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dof;

    sget-object v1, LX/Doo;->b:LX/2PC;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/Dod;->a(LX/2PC;I)I

    move-result v1

    .line 406111
    iget-object v0, p0, LX/2PG;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DoO;

    invoke-interface {v0}, LX/DoO;->a()LX/Eaf;

    move-result-object v0

    invoke-static {v0, v1}, LX/Ecq;->a(LX/Eaf;I)LX/Ebk;

    move-result-object v0

    .line 406112
    new-instance v2, LX/IuI;

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v2, v1, v0}, LX/IuI;-><init>(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v2

    .line 406113
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)LX/IuI;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/IuI",
            "<",
            "Ljava/util/List",
            "<",
            "LX/Ebg;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 406095
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PG;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dof;

    sget-object v1, LX/Doo;->a:LX/2PC;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/Dod;->a(LX/2PC;I)I

    move-result v0

    .line 406096
    invoke-static {v0, p1}, LX/Ecq;->a(II)Ljava/util/List;

    move-result-object v1

    .line 406097
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ebg;

    invoke-virtual {v0}, LX/Ebg;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 406098
    new-instance v2, LX/IuI;

    invoke-direct {v2, v0, v1}, LX/IuI;-><init>(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v2

    .line 406099
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/Ebg;
    .locals 3

    .prologue
    .line 406106
    monitor-enter p0

    .line 406107
    :try_start_0
    invoke-static {}, LX/Ear;->a()LX/Eau;

    move-result-object v0

    .line 406108
    new-instance v1, LX/Ebg;

    sget v2, LX/2PB;->a:I

    invoke-direct {v1, v2, v0}, LX/Ebg;-><init>(ILX/Eau;)V

    move-object v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406109
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)V
    .locals 2

    .prologue
    .line 406103
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PG;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dof;

    sget-object v1, LX/Doo;->a:LX/2PC;

    invoke-virtual {v0, v1, p1}, LX/Dod;->b(LX/2PC;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406104
    monitor-exit p0

    return-void

    .line 406105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(I)V
    .locals 2

    .prologue
    .line 406100
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2PG;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dof;

    sget-object v1, LX/Doo;->b:LX/2PC;

    invoke-virtual {v0, v1, p1}, LX/Dod;->b(LX/2PC;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406101
    monitor-exit p0

    return-void

    .line 406102
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
