.class public LX/3G7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/0TD;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0si;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/11H;

.field private final d:LX/1kx;

.field private final e:LX/2Kz;

.field public final f:LX/1dx;

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0jV;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Sh;

.field private final i:LX/03V;

.field private final j:LX/0SI;

.field private final k:LX/0sg;

.field private final l:LX/0tA;

.field public final m:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final n:LX/0sT;

.field private final o:LX/3G8;

.field private final p:LX/0tk;

.field private final q:LX/1kv;


# direct methods
.method public constructor <init>(LX/0TD;LX/0Ot;LX/11H;LX/1kx;LX/2Kz;LX/1dx;Ljava/util/Set;LX/0Sh;LX/03V;LX/0sg;LX/0SI;LX/0tA;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;LX/3G8;LX/0tk;LX/1kv;)V
    .locals 1
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .param p11    # LX/0SI;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "LX/0Ot",
            "<",
            "LX/0si;",
            ">;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/1kx;",
            "LX/2Kz;",
            "LX/1dx;",
            "Ljava/util/Set",
            "<",
            "LX/0jV;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/graphql/executor/iface/ConsistencyCacheFactory;",
            "LX/0SI;",
            "LX/0tA;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0sT;",
            "Lcom/facebook/graphql/executor/MutationRequestFailureObserver;",
            "LX/0tk;",
            "LX/1kv;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 540599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 540600
    iput-object p1, p0, LX/3G7;->a:LX/0TD;

    .line 540601
    iput-object p2, p0, LX/3G7;->b:LX/0Ot;

    .line 540602
    iput-object p3, p0, LX/3G7;->c:LX/11H;

    .line 540603
    iput-object p4, p0, LX/3G7;->d:LX/1kx;

    .line 540604
    iput-object p5, p0, LX/3G7;->e:LX/2Kz;

    .line 540605
    iput-object p6, p0, LX/3G7;->f:LX/1dx;

    .line 540606
    iput-object p7, p0, LX/3G7;->g:Ljava/util/Set;

    .line 540607
    iput-object p8, p0, LX/3G7;->h:LX/0Sh;

    .line 540608
    iput-object p9, p0, LX/3G7;->i:LX/03V;

    .line 540609
    iput-object p10, p0, LX/3G7;->k:LX/0sg;

    .line 540610
    iput-object p11, p0, LX/3G7;->j:LX/0SI;

    .line 540611
    iput-object p12, p0, LX/3G7;->l:LX/0tA;

    .line 540612
    iput-object p13, p0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 540613
    iput-object p14, p0, LX/3G7;->n:LX/0sT;

    .line 540614
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3G7;->o:LX/3G8;

    .line 540615
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3G7;->p:LX/0tk;

    .line 540616
    move-object/from16 v0, p17

    iput-object v0, p0, LX/3G7;->q:LX/1kv;

    .line 540617
    return-void
.end method

.method private static a(LX/399;LX/4V2;)Ljava/lang/String;
    .locals 2
    .param p0    # LX/399;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/4V2;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 540592
    if-eqz p0, :cond_0

    .line 540593
    iget-object v0, p0, LX/399;->a:LX/0zP;

    .line 540594
    iget-object v1, v0, LX/0gW;->f:Ljava/lang/String;

    move-object v0, v1

    .line 540595
    :goto_0
    return-object v0

    .line 540596
    :cond_0
    if-eqz p1, :cond_1

    .line 540597
    invoke-interface {p1}, LX/4V2;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 540598
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected one to be non null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(LX/3Bq;)V
    .locals 1

    .prologue
    .line 540589
    iget-object v0, p0, LX/3G7;->n:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540590
    :goto_0
    return-void

    .line 540591
    :cond_0
    iget-object v0, p0, LX/3G7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0si;

    invoke-interface {v0, p1}, LX/0si;->a(LX/3Bq;)V

    goto :goto_0
.end method

.method private a(LX/3G6;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3G6;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 540500
    invoke-virtual/range {p1 .. p1}, LX/3G6;->a()LX/3G4;

    move-result-object v8

    .line 540501
    invoke-virtual/range {p1 .. p1}, LX/3G6;->b()LX/399;

    move-result-object v13

    .line 540502
    invoke-virtual/range {p1 .. p1}, LX/3G6;->c()LX/3Fz;

    move-result-object v9

    .line 540503
    invoke-virtual/range {p1 .. p1}, LX/3G6;->d()LX/37X;

    move-result-object v14

    .line 540504
    invoke-virtual/range {p1 .. p1}, LX/3G6;->e()Ljava/util/concurrent/locks/ReadWriteLock;

    move-result-object v15

    .line 540505
    invoke-virtual/range {p1 .. p1}, LX/3G6;->g()LX/4V2;

    move-result-object v10

    .line 540506
    const/4 v11, 0x0

    .line 540507
    invoke-virtual/range {p1 .. p1}, LX/3G6;->b()LX/399;

    move-result-object v1

    invoke-static {v1}, LX/3GA;->a(LX/399;)V

    .line 540508
    if-nez v13, :cond_0

    if-eqz v10, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    const-string v2, "A mutation or legacyMutation is required"

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 540509
    invoke-interface {v15}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v1

    if-nez v1, :cond_2

    .line 540510
    new-instance v1, Ljava/util/concurrent/CancellationException;

    const-string v2, "Unable to acquire run lock, runner is shut down"

    invoke-direct {v1, v2}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 540511
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 540512
    :cond_2
    if-eqz v13, :cond_7

    invoke-virtual {v13}, LX/399;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    move-object v12, v1

    .line 540513
    :goto_1
    if-eqz v12, :cond_3

    .line 540514
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->j:LX/0SI;

    invoke-interface {v1, v12}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;

    .line 540515
    :cond_3
    invoke-virtual/range {p1 .. p1}, LX/3G6;->f()I

    move-result v3

    .line 540516
    const/4 v7, 0x2

    .line 540517
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310029    # 4.499997E-39f

    invoke-virtual {v14, v1, v2, v3}, LX/37X;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;II)I

    move-result v6

    .line 540518
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310029    # 4.499997E-39f

    const/16 v4, 0x9d

    const-string v5, "network_blocker_count"

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v1 .. v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IISLjava/lang/String;Ljava/lang/String;)V

    .line 540519
    if-eqz v8, :cond_8

    invoke-virtual {v8}, LX/3G3;->b()Z

    move-result v1

    if-nez v1, :cond_8

    const/4 v1, 0x1

    .line 540520
    :goto_2
    if-eqz v10, :cond_9

    .line 540521
    invoke-interface {v10}, LX/4V2;->c()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    move-object v5, v1

    .line 540522
    :goto_3
    invoke-virtual {v5}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jT;

    .line 540523
    if-eqz v13, :cond_4

    if-eqz v1, :cond_4

    .line 540524
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3G7;->p:LX/0tk;

    invoke-virtual {v13}, LX/399;->b()LX/0jT;

    move-result-object v4

    invoke-virtual {v2, v4, v1}, LX/0tk;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    .line 540525
    :cond_4
    if-eqz v13, :cond_a

    invoke-virtual {v13}, LX/399;->e()Z

    move-result v2

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3G7;->n:LX/0sT;

    invoke-virtual {v2}, LX/0sT;->c()Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    move v4, v2

    .line 540526
    :goto_4
    if-eqz v13, :cond_b

    iget-object v2, v13, LX/399;->a:LX/0zP;

    invoke-virtual {v2}, LX/0gW;->t()Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    .line 540527
    :goto_5
    if-eqz v4, :cond_c

    if-eqz v2, :cond_c

    .line 540528
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3G7;->q:LX/1kv;

    check-cast v1, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-virtual {v2, v1}, LX/1kv;->a(Lcom/facebook/flatbuffers/MutableFlattenable;)LX/2lk;

    move-result-object v2

    .line 540529
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->p:LX/0tk;

    invoke-interface {v2}, LX/2lk;->d()LX/3Bq;

    move-result-object v4

    invoke-virtual {v1, v13, v5, v4}, LX/0tk;->a(LX/399;Lcom/facebook/graphql/executor/GraphQLResult;LX/3Bq;)LX/3Bq;

    move-result-object v1

    move-object v4, v2

    move-object v2, v1

    .line 540530
    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x310029    # 4.499997E-39f

    const/16 v16, 0x97

    move/from16 v0, v16

    invoke-interface {v1, v6, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 540531
    invoke-virtual {v14, v2}, LX/37X;->a(LX/3Bq;)V

    .line 540532
    const/4 v1, 0x1

    invoke-virtual {v14, v1}, LX/37X;->a(Z)V

    .line 540533
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x310029    # 4.499997E-39f

    const/16 v16, 0x14

    move/from16 v0, v16

    invoke-interface {v1, v6, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 540534
    const v1, -0x75d63eb5

    move-object/from16 v0, p2

    invoke-static {v0, v5, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 540535
    :try_start_1
    invoke-virtual {v14}, LX/1NB;->c()V

    .line 540536
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->g:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jV;

    .line 540537
    invoke-interface {v1, v2}, LX/0jV;->a(LX/3Bq;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_7

    .line 540538
    :catch_0
    move-exception v1

    .line 540539
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3G7;->i:LX/03V;

    const-string v4, "MutationRunner.updateDB"

    invoke-static {v13, v10}, LX/3G7;->a(LX/399;LX/4V2;)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x7d0

    invoke-virtual {v2, v4, v5, v1, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 540540
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310029    # 4.499997E-39f

    const-string v4, "local_db_write"

    const-string v5, "error"

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 540541
    :goto_8
    if-eqz v8, :cond_5

    invoke-virtual {v8}, LX/3G3;->b()Z

    move-result v1

    if-nez v1, :cond_5

    .line 540542
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310029    # 4.499997E-39f

    const-string v4, "offline_retry_success"

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 540543
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->f:LX/1dx;

    invoke-virtual {v1, v8}, LX/1dx;->c(LX/3G3;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 540544
    :cond_5
    invoke-virtual {v14}, LX/1NB;->e()V

    .line 540545
    if-eqz v12, :cond_6

    .line 540546
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->j:LX/0SI;

    invoke-interface {v1}, LX/0SI;->f()V

    .line 540547
    :cond_6
    invoke-interface {v15}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 540548
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310029    # 4.499997E-39f

    const/4 v4, 0x2

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 540549
    return-void

    .line 540550
    :cond_7
    const/4 v1, 0x0

    move-object v12, v1

    goto/16 :goto_1

    .line 540551
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 540552
    :cond_9
    :try_start_3
    move-object/from16 v0, p1

    invoke-static {v13, v0}, LX/0zO;->a(LX/399;LX/3G6;)LX/0zO;

    move-result-object v2

    .line 540553
    invoke-virtual {v2, v1}, LX/0zO;->c(Z)LX/0zO;

    .line 540554
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->c:LX/11H;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/3G7;->d:LX/1kx;

    const/4 v5, 0x0

    iget-object v6, v2, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2}, LX/0zO;->p()LX/0w5;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v6, v0}, LX/0zO;->a(Lcom/facebook/common/callercontext/CallerContext;LX/0w5;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    invoke-virtual {v1, v4, v2, v5, v6}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/executor/GraphQLResult;

    move-object v5, v1

    goto/16 :goto_3

    .line 540555
    :cond_a
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_4

    .line 540556
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 540557
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3G7;->k:LX/0sg;

    invoke-virtual {v2}, LX/0sg;->a()LX/2lk;

    move-result-object v2

    .line 540558
    move-object/from16 v0, p0

    iget-object v4, v0, LX/3G7;->p:LX/0tk;

    invoke-virtual {v4, v13, v1, v2}, LX/0tk;->a(LX/399;LX/0jT;LX/2lk;)LX/3Bq;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v1

    move-object v4, v2

    move-object v2, v1

    goto/16 :goto_6

    .line 540559
    :cond_d
    :try_start_4
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x310029    # 4.499997E-39f

    const/16 v6, 0x11

    invoke-interface {v1, v5, v3, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 540560
    invoke-virtual {v14}, LX/1NB;->d()V

    .line 540561
    instance-of v1, v2, LX/3Bp;

    if-nez v1, :cond_e

    .line 540562
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/3G7;->a(LX/3Bq;)V

    .line 540563
    :cond_e
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310029    # 4.499997E-39f

    const/16 v5, 0x12

    invoke-interface {v1, v2, v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 540564
    invoke-interface {v4}, LX/2lk;->a()Z

    move-result v1

    if-nez v1, :cond_f

    .line 540565
    invoke-interface {v4}, LX/2lk;->b()V

    .line 540566
    :cond_f
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310029    # 4.499997E-39f

    const/16 v4, 0x13

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 540567
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310029    # 4.499997E-39f

    const-string v4, "local_db_write"

    const-string v5, "success"

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_8

    .line 540568
    :catch_1
    move-exception v6

    .line 540569
    const/4 v2, 0x3

    .line 540570
    if-eqz v8, :cond_13

    :try_start_5
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->f:LX/1dx;

    invoke-virtual {v1, v6, v9}, LX/1dx;->a(Ljava/lang/Throwable;LX/3Fz;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result v1

    if-eqz v1, :cond_13

    .line 540571
    const/4 v4, 0x1

    .line 540572
    :try_start_6
    invoke-virtual {v14}, LX/37X;->a()V

    .line 540573
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v7, v13, LX/399;->a:LX/0zP;

    invoke-virtual {v7}, LX/0gW;->h()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v5

    .line 540574
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->e:LX/2Kz;

    invoke-virtual {v8}, LX/3G4;->e()LX/3G4;

    move-result-object v5

    invoke-virtual {v1, v5, v14}, LX/2Kz;->a(LX/3G4;LX/37X;)V

    .line 540575
    invoke-static/range {p1 .. p1}, LX/3G7;->b(LX/3G6;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 540576
    invoke-static/range {p1 .. p1}, LX/3G7;->c(LX/3G6;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    .line 540577
    const v5, -0x23357fbc

    move-object/from16 v0, p2

    invoke-static {v0, v1, v5}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 540578
    :cond_10
    :goto_9
    new-instance v1, LX/4V3;

    invoke-direct {v1, v6, v4}, LX/4V3;-><init>(Ljava/lang/Throwable;Z)V

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 540579
    :catchall_0
    move-exception v1

    :goto_a
    if-nez v4, :cond_11

    .line 540580
    invoke-virtual {v14}, LX/1NB;->e()V

    .line 540581
    :cond_11
    if-eqz v12, :cond_12

    .line 540582
    move-object/from16 v0, p0

    iget-object v4, v0, LX/3G7;->j:LX/0SI;

    invoke-interface {v4}, LX/0SI;->f()V

    .line 540583
    :cond_12
    invoke-interface {v15}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 540584
    move-object/from16 v0, p0

    iget-object v4, v0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x310029    # 4.499997E-39f

    invoke-interface {v4, v5, v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v1

    .line 540585
    :cond_13
    :try_start_7
    const-string v4, "MutationRunner.runInternal"

    invoke-static {v13, v10}, LX/3G7;->a(LX/399;LX/4V2;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, LX/3G7;->i:LX/03V;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/3G7;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v9, 0x310029    # 4.499997E-39f

    move v10, v3

    invoke-static/range {v4 .. v10}, LX/1l8;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;LX/03V;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V

    .line 540586
    const/4 v1, 0x0

    invoke-virtual {v14, v1}, LX/37X;->a(Z)V

    .line 540587
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3G7;->o:LX/3G8;

    invoke-virtual {v1, v13, v6}, LX/3G8;->a(LX/399;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move v4, v11

    goto :goto_9

    .line 540588
    :catchall_1
    move-exception v1

    move v2, v7

    move v4, v11

    goto :goto_a

    :catchall_2
    move-exception v1

    move v4, v11

    goto :goto_a
.end method

.method private a(LX/3G6;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3G6;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 540493
    instance-of v0, p3, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_1

    .line 540494
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/0SQ;->cancel(Z)Z

    .line 540495
    :cond_0
    :goto_0
    return-void

    .line 540496
    :cond_1
    iget-object v0, p1, LX/3G6;->c:LX/3Fz;

    move-object v0, v0

    .line 540497
    iget-object v1, p0, LX/3G7;->f:LX/1dx;

    invoke-virtual {v1, p3, v0}, LX/1dx;->a(Ljava/lang/Throwable;LX/3Fz;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/3Fz;->b:LX/3Fz;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 540498
    if-nez v0, :cond_0

    .line 540499
    invoke-virtual {p2, p3}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/3G7;
    .locals 19

    .prologue
    .line 540491
    new-instance v1, LX/3G7;

    invoke-static/range {p0 .. p0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    const/16 v3, 0xb0f

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v4

    check-cast v4, LX/11H;

    invoke-static/range {p0 .. p0}, LX/1kx;->a(LX/0QB;)LX/1kx;

    move-result-object v5

    check-cast v5, LX/1kx;

    invoke-static/range {p0 .. p0}, LX/2Kz;->a(LX/0QB;)LX/2Kz;

    move-result-object v6

    check-cast v6, LX/2Kz;

    invoke-static/range {p0 .. p0}, LX/1dx;->a(LX/0QB;)LX/1dx;

    move-result-object v7

    check-cast v7, LX/1dx;

    invoke-static/range {p0 .. p0}, LX/0tj;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v9

    check-cast v9, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0sg;->a(LX/0QB;)LX/0sg;

    move-result-object v11

    check-cast v11, LX/0sg;

    invoke-interface/range {p0 .. p0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v12

    invoke-static {v12}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v12

    check-cast v12, LX/0SI;

    invoke-static/range {p0 .. p0}, LX/0tA;->a(LX/0QB;)LX/0tA;

    move-result-object v13

    check-cast v13, LX/0tA;

    invoke-static/range {p0 .. p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v14

    check-cast v14, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {p0 .. p0}, LX/0sT;->a(LX/0QB;)LX/0sT;

    move-result-object v15

    check-cast v15, LX/0sT;

    invoke-static/range {p0 .. p0}, LX/3G8;->a(LX/0QB;)LX/3G8;

    move-result-object v16

    check-cast v16, LX/3G8;

    invoke-static/range {p0 .. p0}, LX/0tk;->a(LX/0QB;)LX/0tk;

    move-result-object v17

    check-cast v17, LX/0tk;

    invoke-static/range {p0 .. p0}, LX/1kv;->a(LX/0QB;)LX/1kv;

    move-result-object v18

    check-cast v18, LX/1kv;

    invoke-direct/range {v1 .. v18}, LX/3G7;-><init>(LX/0TD;LX/0Ot;LX/11H;LX/1kx;LX/2Kz;LX/1dx;Ljava/util/Set;LX/0Sh;LX/03V;LX/0sg;LX/0SI;LX/0tA;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;LX/3G8;LX/0tk;LX/1kv;)V

    .line 540492
    return-object v1
.end method

.method public static b(LX/3G7;LX/3G6;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/String;)V
    .locals 4
    .param p2    # Lcom/google/common/util/concurrent/SettableFuture;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3G6;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 540471
    iget-object v0, p1, LX/3G6;->d:LX/37X;

    move-object v2, v0

    .line 540472
    iget-object v0, p1, LX/3G6;->a:LX/3G4;

    move-object v0, v0

    .line 540473
    if-eqz v0, :cond_2

    .line 540474
    iget-object v0, v0, LX/3G3;->b:Ljava/lang/String;

    move-object v1, v0

    .line 540475
    :goto_0
    if-eqz v2, :cond_0

    .line 540476
    invoke-virtual {v2}, LX/37X;->h()LX/3Bq;

    move-result-object v0

    .line 540477
    iget-object v3, p0, LX/3G7;->l:LX/0tA;

    invoke-virtual {v3, v1, v0}, LX/0tA;->a(Ljava/lang/String;LX/3Bq;)V

    .line 540478
    :cond_0
    :try_start_0
    invoke-direct {p0, p1, p2}, LX/3G7;->a(LX/3G6;Lcom/google/common/util/concurrent/SettableFuture;)V

    .line 540479
    if-eqz v2, :cond_1

    .line 540480
    invoke-virtual {v2}, LX/37X;->h()LX/3Bq;

    move-result-object v0

    .line 540481
    iget-object v2, p0, LX/3G7;->l:LX/0tA;

    invoke-virtual {v2, v1, v0}, LX/0tA;->b(Ljava/lang/String;LX/3Bq;)V
    :try_end_0
    .catch LX/4V3; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 540482
    :cond_1
    :goto_1
    return-void

    .line 540483
    :cond_2
    invoke-static {}, LX/0tA;->a()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 540484
    :catch_0
    move-exception v0

    .line 540485
    iget-boolean v2, v0, LX/4V3;->stashedUntilOnline:Z

    if-nez v2, :cond_3

    .line 540486
    iget-object v2, p0, LX/3G7;->l:LX/0tA;

    invoke-virtual {v2, v1}, LX/0tA;->a(Ljava/lang/String;)V

    .line 540487
    :cond_3
    invoke-virtual {v0}, LX/4V3;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, LX/3G7;->a(LX/3G6;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 540488
    :catch_1
    move-exception v0

    .line 540489
    iget-object v2, p0, LX/3G7;->l:LX/0tA;

    invoke-virtual {v2, v1}, LX/0tA;->a(Ljava/lang/String;)V

    .line 540490
    invoke-direct {p0, p1, p2, v0}, LX/3G7;->a(LX/3G6;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static b(LX/3G6;)Z
    .locals 1

    .prologue
    .line 540466
    iget-object v0, p0, LX/3G6;->c:LX/3Fz;

    move-object v0, v0

    .line 540467
    instance-of v0, v0, LX/3G0;

    return v0
.end method

.method private static c(LX/3G6;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3G6;",
            ")",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 540468
    iget-object v0, p0, LX/3G6;->c:LX/3Fz;

    move-object v0, v0

    .line 540469
    instance-of v1, v0, LX/3G0;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 540470
    check-cast v0, LX/3G0;

    iget-object v0, v0, LX/3G0;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    return-object v0
.end method
