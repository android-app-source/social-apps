.class public LX/2OR;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:J


# instance fields
.field private final b:LX/2OS;

.field private final c:LX/0SG;

.field public final d:LX/2Ly;

.field private final e:LX/2OV;

.field private final f:LX/2Mv;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 401826
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/2OR;->a:J

    return-void
.end method

.method public constructor <init>(LX/2OS;LX/0SG;LX/2Ly;LX/2OV;LX/2Mv;LX/0Or;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2OS;",
            "LX/0SG;",
            "LX/2Ly;",
            "LX/2OV;",
            "LX/2Mv;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 401818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401819
    iput-object p2, p0, LX/2OR;->c:LX/0SG;

    .line 401820
    iput-object p1, p0, LX/2OR;->b:LX/2OS;

    .line 401821
    iput-object p3, p0, LX/2OR;->d:LX/2Ly;

    .line 401822
    iput-object p4, p0, LX/2OR;->e:LX/2OV;

    .line 401823
    iput-object p5, p0, LX/2OR;->f:LX/2Mv;

    .line 401824
    iput-object p6, p0, LX/2OR;->g:LX/0Or;

    .line 401825
    return-void
.end method

.method public static a(LX/2OR;)J
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 401817
    iget-object v0, p0, LX/2OR;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public static b(LX/0QB;)LX/2OR;
    .locals 7

    .prologue
    .line 401815
    new-instance v0, LX/2OR;

    invoke-static {p0}, LX/2OS;->a(LX/0QB;)LX/2OS;

    move-result-object v1

    check-cast v1, LX/2OS;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/2Ly;->a(LX/0QB;)LX/2Ly;

    move-result-object v3

    check-cast v3, LX/2Ly;

    invoke-static {p0}, LX/2OV;->a(LX/0QB;)LX/2OV;

    move-result-object v4

    check-cast v4, LX/2OV;

    invoke-static {p0}, LX/2Mv;->b(LX/0QB;)LX/2Mv;

    move-result-object v5

    check-cast v5, LX/2Mv;

    const/16 v6, 0x12cd

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/2OR;-><init>(LX/2OS;LX/0SG;LX/2Ly;LX/2OV;LX/2Mv;LX/0Or;)V

    .line 401816
    return-object v0
.end method

.method public static b(LX/2OR;Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 4
    .param p0    # LX/2OR;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 401777
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 401778
    if-nez p1, :cond_2

    .line 401779
    :cond_0
    :goto_0
    move-object v0, v0

    .line 401780
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 401781
    :cond_2
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 401782
    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    if-nez v1, :cond_0

    .line 401783
    iget-object v1, p0, LX/2OR;->d:LX/2Ly;

    invoke-virtual {v1, p1}, LX/2Ly;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;

    move-result-object v1

    .line 401784
    sget-object v2, LX/Dho;->a:[I

    invoke-virtual {v1}, LX/6eh;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 401785
    :pswitch_0
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-gt v1, v3, :cond_0

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-gt v1, v3, :cond_0

    sget-object v0, LX/DhN;->PHOTO:LX/DhN;

    goto :goto_0

    .line 401786
    :pswitch_1
    sget-object v0, LX/DhN;->TEXT:LX/DhN;

    goto :goto_0

    .line 401787
    :pswitch_2
    sget-object v0, LX/DhN;->VIDEO:LX/DhN;

    goto :goto_0

    .line 401788
    :pswitch_3
    sget-object v0, LX/DhN;->STICKER:LX/DhN;

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/Message;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 401789
    if-nez p1, :cond_0

    .line 401790
    const/4 v0, 0x0

    .line 401791
    :goto_0
    return-object v0

    .line 401792
    :cond_0
    invoke-static {p0}, LX/2OR;->a(LX/2OR;)J

    move-result-wide v1

    .line 401793
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 401794
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->g()I

    move-result v5

    if-ge v3, v5, :cond_3

    .line 401795
    invoke-virtual {p1, v3}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b(I)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v5

    .line 401796
    iget-wide v7, v5, Lcom/facebook/messaging/model/messages/Message;->c:J

    cmp-long v6, v7, v1

    if-lez v6, :cond_3

    .line 401797
    const/4 v9, 0x0

    .line 401798
    if-nez v5, :cond_5

    .line 401799
    :cond_1
    :goto_2
    move v6, v9

    .line 401800
    if-eqz v6, :cond_2

    .line 401801
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 401802
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 401803
    :cond_3
    invoke-static {v4}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 401804
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object v1, v3

    .line 401805
    move-object v0, v1

    .line 401806
    const/4 v2, 0x0

    .line 401807
    if-nez v0, :cond_6

    .line 401808
    :cond_4
    move-object v0, v2

    .line 401809
    goto :goto_0

    .line 401810
    :cond_5
    invoke-static {p0, v5}, LX/2OR;->b(LX/2OR;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 401811
    iget-wide v11, v5, Lcom/facebook/messaging/model/messages/Message;->c:J

    cmp-long v10, v11, v1

    if-lez v10, :cond_1

    const/4 v9, 0x1

    goto :goto_2

    .line 401812
    :cond_6
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/Message;

    .line 401813
    if-eqz v2, :cond_7

    iget-wide v5, v1, Lcom/facebook/messaging/model/messages/Message;->c:J

    iget-wide v7, v2, Lcom/facebook/messaging/model/messages/Message;->c:J

    cmp-long v4, v5, v7

    if-lez v4, :cond_8

    :cond_7
    :goto_4
    move-object v2, v1

    .line 401814
    goto :goto_3

    :cond_8
    move-object v1, v2

    goto :goto_4
.end method
