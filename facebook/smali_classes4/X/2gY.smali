.class public LX/2gY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1fT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/2gY;


# instance fields
.field private final b:LX/0lC;

.field private final c:LX/0WJ;

.field public final d:LX/2gZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 448581
    const-class v0, LX/2gY;

    sput-object v0, LX/2gY;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0lC;LX/0WJ;LX/2gZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 448582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448583
    iput-object p1, p0, LX/2gY;->b:LX/0lC;

    .line 448584
    iput-object p2, p0, LX/2gY;->c:LX/0WJ;

    .line 448585
    iput-object p3, p0, LX/2gY;->d:LX/2gZ;

    .line 448586
    return-void
.end method

.method public static a(LX/0QB;)LX/2gY;
    .locals 6

    .prologue
    .line 448587
    sget-object v0, LX/2gY;->e:LX/2gY;

    if-nez v0, :cond_1

    .line 448588
    const-class v1, LX/2gY;

    monitor-enter v1

    .line 448589
    :try_start_0
    sget-object v0, LX/2gY;->e:LX/2gY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 448590
    if-eqz v2, :cond_0

    .line 448591
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 448592
    new-instance p0, LX/2gY;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    invoke-static {v0}, LX/2gZ;->a(LX/0QB;)LX/2gZ;

    move-result-object v5

    check-cast v5, LX/2gZ;

    invoke-direct {p0, v3, v4, v5}, LX/2gY;-><init>(LX/0lC;LX/0WJ;LX/2gZ;)V

    .line 448593
    move-object v0, p0

    .line 448594
    sput-object v0, LX/2gY;->e:LX/2gY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 448595
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 448596
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 448597
    :cond_1
    sget-object v0, LX/2gY;->e:LX/2gY;

    return-object v0

    .line 448598
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 448599
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onMessage(Ljava/lang/String;[BJ)V
    .locals 3

    .prologue
    .line 448600
    iget-object v0, p0, LX/2gY;->c:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 448601
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Not logged in: throwing out Mqtt message. "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448602
    :cond_0
    :goto_0
    return-void

    .line 448603
    :cond_1
    :try_start_0
    const-string v0, "/messaging_events"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 448604
    iget-object v0, p0, LX/2gY;->b:LX/0lC;

    invoke-static {p2}, LX/0YN;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 448605
    const/4 v1, 0x2

    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 448606
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Publish:\n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448607
    :cond_2
    const-string v1, "event"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 448608
    const-string v2, "messenger_status"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 448609
    const-string v1, "from_fbid"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 448610
    const-string v2, "is_messenger_user"

    invoke-virtual {v0, v2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 448611
    const-string v2, "is_messenger_user"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->g(LX/0lF;)Z

    move-result v2

    .line 448612
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 448613
    iget-object p1, p0, LX/2gY;->d:LX/2gZ;

    invoke-virtual {p1, v1, v2}, LX/2gZ;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 448614
    :cond_3
    goto :goto_0

    .line 448615
    :catch_0
    move-exception v0

    .line 448616
    sget-object v1, LX/2gY;->a:Ljava/lang/Class;

    const-string v2, "IOException"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
