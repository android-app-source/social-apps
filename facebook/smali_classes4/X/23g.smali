.class public LX/23g;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final b:LX/0id;

.field public final c:LX/23h;

.field public d:LX/74S;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I

.field public g:LX/9eT;

.field public h:Z

.field public i:Z

.field public j:Z


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0id;LX/23h;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 364833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 364834
    iput-object v2, p0, LX/23g;->d:LX/74S;

    .line 364835
    iput-object v2, p0, LX/23g;->e:Ljava/lang/String;

    .line 364836
    iget-object v0, p0, LX/23g;->d:LX/74S;

    invoke-static {v0}, LX/23g;->a(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, LX/23g;->f:I

    .line 364837
    iput-object v2, p0, LX/23g;->g:LX/9eT;

    .line 364838
    iput-boolean v1, p0, LX/23g;->h:Z

    .line 364839
    iput-boolean v1, p0, LX/23g;->i:Z

    .line 364840
    iput-boolean v1, p0, LX/23g;->j:Z

    .line 364841
    iput-object p1, p0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 364842
    iput-object p2, p0, LX/23g;->b:LX/0id;

    .line 364843
    iput-object p3, p0, LX/23g;->c:LX/23h;

    .line 364844
    return-void
.end method

.method public static a(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 364911
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/23g;
    .locals 6

    .prologue
    .line 364900
    const-class v1, LX/23g;

    monitor-enter v1

    .line 364901
    :try_start_0
    sget-object v0, LX/23g;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 364902
    sput-object v2, LX/23g;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 364903
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364904
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 364905
    new-instance p0, LX/23g;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v4

    check-cast v4, LX/0id;

    invoke-static {v0}, LX/23h;->a(LX/0QB;)LX/23h;

    move-result-object v5

    check-cast v5, LX/23h;

    invoke-direct {p0, v3, v4, v5}, LX/23g;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0id;LX/23h;)V

    .line 364906
    move-object v0, p0

    .line 364907
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 364908
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/23g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364909
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 364910
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static h(LX/23g;)V
    .locals 4

    .prologue
    .line 364896
    iget-object v0, p0, LX/23g;->g:LX/9eT;

    iget v0, v0, LX/9eT;->f:I

    .line 364897
    iget-object v1, p0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, LX/23g;->f:I

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 364898
    iget-object v1, p0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, LX/23g;->f:I

    iget-object v3, p0, LX/23g;->e:Ljava/lang/String;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 364899
    return-void
.end method

.method public static j(LX/23g;)V
    .locals 3

    .prologue
    .line 364894
    iget-object v0, p0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/23g;->g:LX/9eT;

    iget v1, v1, LX/9eT;->f:I

    iget v2, p0, LX/23g;->f:I

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 364895
    return-void
.end method

.method public static k(LX/23g;)V
    .locals 3

    .prologue
    .line 364891
    iget-boolean v0, p0, LX/23g;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/23g;->i:Z

    if-eqz v0, :cond_0

    .line 364892
    iget-object v0, p0, LX/23g;->b:LX/0id;

    iget-object v1, p0, LX/23g;->g:LX/9eT;

    iget-object v1, v1, LX/9eT;->b:Ljava/lang/String;

    iget-object v2, p0, LX/23g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0id;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 364893
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 364912
    iget-boolean v0, p0, LX/23g;->h:Z

    if-nez v0, :cond_0

    .line 364913
    :goto_0
    return-void

    .line 364914
    :cond_0
    iget-object v0, p0, LX/23g;->c:LX/23h;

    const-string v1, "DataFetch"

    invoke-virtual {v0, v1}, LX/23h;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const v3, 0x14000b

    const/4 v2, 0x2

    .line 364870
    iget-boolean v0, p0, LX/23g;->h:Z

    if-nez v0, :cond_1

    .line 364871
    :cond_0
    :goto_0
    return-void

    .line 364872
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/23g;->j:Z

    .line 364873
    iget-boolean v0, p0, LX/23g;->i:Z

    if-eqz v0, :cond_4

    .line 364874
    iget-object v0, p0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/23g;->g:LX/9eT;

    iget v1, v1, LX/9eT;->e:I

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 364875
    iget-object v0, p0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/23g;->g:LX/9eT;

    iget v1, v1, LX/9eT;->f:I

    iget v4, p0, LX/23g;->f:I

    const/4 v5, 0x2

    invoke-interface {v0, v1, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 364876
    :goto_1
    iget-object v0, p0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p1}, LX/23g;->a(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v0, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 364877
    iget-object v0, p0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p1}, LX/23g;->a(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 364878
    :cond_2
    invoke-static {p0}, LX/23g;->k(LX/23g;)V

    .line 364879
    if-eqz p2, :cond_0

    .line 364880
    iget-object v0, p0, LX/23g;->c:LX/23h;

    .line 364881
    iget-object v1, v0, LX/23h;->e:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/23h;->f:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_5

    .line 364882
    :cond_3
    :goto_2
    goto :goto_0

    .line 364883
    :cond_4
    iget-object v0, p0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/23g;->g:LX/9eT;

    iget v1, v1, LX/9eT;->e:I

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 364884
    invoke-static {p0}, LX/23g;->j(LX/23g;)V

    goto :goto_1

    .line 364885
    :cond_5
    iget-object v1, v0, LX/23h;->f:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 364886
    const-string v1, "ImageFetch"

    invoke-virtual {v0, v1}, LX/23h;->d(Ljava/lang/String;)V

    .line 364887
    iget-object v1, v0, LX/23h;->c:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11o;

    const-string v2, "WaitTime"

    invoke-interface {v1, v2}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 364888
    iget-object v1, v0, LX/23h;->c:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11o;

    const-string v2, "WaitTime"

    const v3, -0x43300084

    invoke-static {v1, v2, v3}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 364889
    :cond_6
    iget-object v1, v0, LX/23h;->c:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11o;

    const-string v2, "DataFetch"

    invoke-interface {v1, v2}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 364890
    invoke-virtual {v0}, LX/23h;->a()V

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 364864
    iget-boolean v0, p0, LX/23g;->h:Z

    if-nez v0, :cond_1

    .line 364865
    :cond_0
    :goto_0
    return-void

    .line 364866
    :cond_1
    iget-object v0, p0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/23g;->g:LX/9eT;

    iget v1, v1, LX/9eT;->c:I

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 364867
    if-eqz p1, :cond_0

    .line 364868
    iget-object v0, p0, LX/23g;->c:LX/23h;

    invoke-virtual {v0}, LX/23h;->a()V

    .line 364869
    iget-object v0, p0, LX/23g;->b:LX/0id;

    iget-object v1, p0, LX/23g;->g:LX/9eT;

    iget-object v1, v1, LX/9eT;->b:Ljava/lang/String;

    iget-object v2, p0, LX/23g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0id;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 364858
    iget-boolean v0, p0, LX/23g;->h:Z

    if-nez v0, :cond_0

    .line 364859
    :goto_0
    return-void

    .line 364860
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/23g;->i:Z

    .line 364861
    iget-object v0, p0, LX/23g;->b:LX/0id;

    const/4 v2, 0x0

    .line 364862
    const v1, 0x4c000a

    invoke-static {v0, v1, v2, v2}, LX/0id;->a(LX/0id;ILjava/lang/String;Ljava/lang/String;)V

    .line 364863
    iget-object v0, p0, LX/23g;->c:LX/23h;

    const-string v1, "Animation"

    invoke-virtual {v0, v1}, LX/23h;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 364851
    iget-boolean v0, p0, LX/23g;->h:Z

    if-nez v0, :cond_0

    .line 364852
    :goto_0
    return-void

    .line 364853
    :cond_0
    iget-object v0, p0, LX/23g;->c:LX/23h;

    .line 364854
    iget-object v1, v0, LX/23h;->e:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/23h;->f:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, LX/23h;->c:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11o;

    const-string p0, "ImageFetch"

    invoke-interface {v1, p0}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 364855
    :cond_1
    :goto_1
    goto :goto_0

    .line 364856
    :cond_2
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, v0, LX/23h;->f:LX/0am;

    .line 364857
    const-string v1, "ImageFetch"

    invoke-virtual {v0, v1}, LX/23h;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 364848
    iget-boolean v0, p0, LX/23g;->h:Z

    if-nez v0, :cond_0

    .line 364849
    :goto_0
    return-void

    .line 364850
    :cond_0
    iget-object v0, p0, LX/23g;->c:LX/23h;

    const-string v1, "Inflate"

    invoke-virtual {v0, v1}, LX/23h;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 364845
    iget-boolean v0, p0, LX/23g;->h:Z

    if-nez v0, :cond_0

    .line 364846
    :goto_0
    return-void

    .line 364847
    :cond_0
    iget-object v0, p0, LX/23g;->c:LX/23h;

    const-string v1, "Inflate"

    invoke-virtual {v0, v1}, LX/23h;->d(Ljava/lang/String;)V

    goto :goto_0
.end method
