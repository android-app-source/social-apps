.class public LX/3QG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Pw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile b:LX/3QG;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 563447
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/notifications/constants/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PUSH_REACHABILITY_CHECK:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/3QG;->a:LX/2QP;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 563448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563449
    return-void
.end method

.method public static a(LX/0QB;)LX/3QG;
    .locals 3

    .prologue
    .line 563450
    sget-object v0, LX/3QG;->b:LX/3QG;

    if-nez v0, :cond_1

    .line 563451
    const-class v1, LX/3QG;

    monitor-enter v1

    .line 563452
    :try_start_0
    sget-object v0, LX/3QG;->b:LX/3QG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 563453
    if-eqz v2, :cond_0

    .line 563454
    :try_start_1
    new-instance v0, LX/3QG;

    invoke-direct {v0}, LX/3QG;-><init>()V

    .line 563455
    move-object v0, v0

    .line 563456
    sput-object v0, LX/3QG;->b:LX/3QG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 563457
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 563458
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 563459
    :cond_1
    sget-object v0, LX/3QG;->b:LX/3QG;

    return-object v0

    .line 563460
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 563461
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 563462
    sget-object v0, LX/3QG;->a:LX/2QP;

    return-object v0
.end method

.method public final a(LX/0lF;Lcom/facebook/push/PushProperty;)V
    .locals 0

    .prologue
    .line 563463
    return-void
.end method
