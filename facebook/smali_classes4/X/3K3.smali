.class public LX/3K3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3K1;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3Jl;",
            ">;"
        }
    .end annotation
.end field

.field public final e:[F

.field private final f:I


# direct methods
.method private constructor <init>(IILjava/util/List;Ljava/util/List;[FI)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LX/3K1;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/3Jl;",
            ">;[FI)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 548277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 548278
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    if-lez p1, :cond_2

    move v0, v1

    :goto_0
    const-string v4, "frame_rate"

    invoke-static {v3, v0, v4}, LX/3Je;->a(Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/3K3;->a:I

    .line 548279
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    if-lez p2, :cond_3

    move v0, v1

    :goto_1
    const-string v4, "animation_frame_count"

    invoke-static {v3, v0, v4}, LX/3Je;->a(Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/3K3;->b:I

    .line 548280
    invoke-static {p3}, LX/3Jh;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    move v0, v1

    :goto_2
    const-string v4, "features"

    invoke-static {v3, v0, v4}, LX/3Je;->a(Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/3K3;->c:Ljava/util/List;

    .line 548281
    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 548282
    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 548283
    :goto_3
    move-object v0, v0

    .line 548284
    invoke-static {v0}, LX/3Jh;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 548285
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_b

    :cond_1
    move v4, v7

    .line 548286
    :goto_4
    move v0, v4

    .line 548287
    const-string v4, "animation_groups"

    invoke-static {v3, v0, v4}, LX/3Je;->a(Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/3K3;->d:Ljava/util/List;

    .line 548288
    array-length v0, p5

    const/4 v3, 0x2

    if-ne v0, v3, :cond_5

    aget v0, p5, v2

    cmpl-float v0, v0, v5

    if-lez v0, :cond_5

    aget v0, p5, v1

    cmpl-float v0, v0, v5

    if-lez v0, :cond_5

    :goto_5
    const-string v0, "canvas_size"

    invoke-static {p5, v1, v0}, LX/3Je;->a(Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, LX/3K3;->e:[F

    .line 548289
    iput p6, p0, LX/3K3;->f:I

    .line 548290
    return-void

    :cond_2
    move v0, v2

    .line 548291
    goto :goto_0

    :cond_3
    move v0, v2

    .line 548292
    goto :goto_1

    :cond_4
    move v0, v2

    .line 548293
    goto :goto_2

    :cond_5
    move v1, v2

    .line 548294
    goto :goto_5

    .line 548295
    :cond_6
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 548296
    new-instance v6, Ljava/util/Stack;

    invoke-direct {v6}, Ljava/util/Stack;-><init>()V

    .line 548297
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 548298
    :cond_7
    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 548299
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Jl;

    .line 548300
    iget p1, v0, LX/3Jl;->b:I

    move p1, p1

    .line 548301
    if-nez p1, :cond_7

    .line 548302
    invoke-virtual {v6, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 548303
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_6

    .line 548304
    :cond_8
    invoke-virtual {v6}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 548305
    invoke-virtual {v6}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Jl;

    .line 548306
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 548307
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 548308
    :cond_9
    :goto_7
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 548309
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3Jl;

    .line 548310
    iget p2, v3, LX/3Jl;->b:I

    move p2, p2

    .line 548311
    iget p3, v0, LX/3Jl;->a:I

    move p3, p3

    .line 548312
    if-ne p2, p3, :cond_9

    .line 548313
    invoke-virtual {v6, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    :cond_a
    move-object v0, v4

    .line 548314
    goto/16 :goto_3

    .line 548315
    :cond_b
    new-instance p2, Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {p2, v4}, Ljava/util/HashSet;-><init>(I)V

    .line 548316
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p3

    move p1, v6

    :goto_8
    if-ge p1, p3, :cond_d

    .line 548317
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3Jl;

    .line 548318
    iget p4, v4, LX/3Jl;->a:I

    move v4, p4

    .line 548319
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 548320
    invoke-interface {p2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_c

    move v4, v6

    .line 548321
    goto/16 :goto_4

    .line 548322
    :cond_c
    invoke-interface {p2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 548323
    add-int/lit8 v4, p1, 0x1

    move p1, v4

    goto :goto_8

    :cond_d
    move v4, v7

    .line 548324
    goto/16 :goto_4
.end method

.method public synthetic constructor <init>(IILjava/util/List;Ljava/util/List;[FIB)V
    .locals 0

    .prologue
    .line 548325
    invoke-direct/range {p0 .. p6}, LX/3K3;-><init>(IILjava/util/List;Ljava/util/List;[FI)V

    return-void
.end method
