.class public LX/36a;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CAa;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/36a",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CAa;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 499388
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 499389
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/36a;->b:LX/0Zi;

    .line 499390
    iput-object p1, p0, LX/36a;->a:LX/0Ot;

    .line 499391
    return-void
.end method

.method public static a(LX/0QB;)LX/36a;
    .locals 4

    .prologue
    .line 499377
    const-class v1, LX/36a;

    monitor-enter v1

    .line 499378
    :try_start_0
    sget-object v0, LX/36a;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499379
    sput-object v2, LX/36a;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499380
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499381
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499382
    new-instance v3, LX/36a;

    const/16 p0, 0x20ad

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/36a;-><init>(LX/0Ot;)V

    .line 499383
    move-object v0, v3

    .line 499384
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499385
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/36a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499386
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499387
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 499392
    check-cast p2, LX/CAY;

    .line 499393
    iget-object v0, p0, LX/36a;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CAa;

    iget-object v1, p2, LX/CAY;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/CAY;->b:LX/1Pp;

    .line 499394
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 499395
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    move-object v3, v3

    .line 499396
    invoke-static {v3}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    .line 499397
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 499398
    const-string v5, "extra_action_on_fragment_create_create_profile_video"

    const/4 v6, 0x1

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 499399
    iget-object p2, v0, LX/CAa;->a:LX/0Or;

    iget-object v5, v0, LX/CAa;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/17Y;

    iget-object v6, v0, LX/CAa;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    new-instance v1, LX/CAZ;

    invoke-direct {v1, v0, v3}, LX/CAZ;-><init>(LX/CAa;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-static {p0, p2, v5, v6, v1}, LX/CAe;->a(Landroid/os/Bundle;LX/0Or;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/CAZ;)Landroid/view/View$OnClickListener;

    move-result-object v5

    move-object v5, v5

    .line 499400
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    iget-object p0, v0, LX/CAa;->e:LX/1Vm;

    invoke-virtual {p0, p1}, LX/1Vm;->c(LX/1De;)LX/C2N;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/C2N;->a(Landroid/view/View$OnClickListener;)LX/C2N;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/C2N;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/C2N;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/C2N;->a(LX/1Pp;)LX/C2N;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 499401
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-nez v5, :cond_1

    const-string v5, ""

    .line 499402
    :goto_0
    iget-object v6, v0, LX/CAa;->f:LX/CAn;

    const/4 p0, 0x0

    .line 499403
    new-instance v0, LX/CAm;

    invoke-direct {v0, v6}, LX/CAm;-><init>(LX/CAn;)V

    .line 499404
    sget-object v3, LX/CAn;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CAl;

    .line 499405
    if-nez v3, :cond_0

    .line 499406
    new-instance v3, LX/CAl;

    invoke-direct {v3}, LX/CAl;-><init>()V

    .line 499407
    :cond_0
    invoke-static {v3, p1, p0, p0, v0}, LX/CAl;->a$redex0(LX/CAl;LX/1De;IILX/CAm;)V

    .line 499408
    move-object v0, v3

    .line 499409
    move-object p0, v0

    .line 499410
    move-object v6, p0

    .line 499411
    iget-object p0, v6, LX/CAl;->a:LX/CAm;

    iput-object v5, p0, LX/CAm;->a:Ljava/lang/String;

    .line 499412
    iget-object p0, v6, LX/CAl;->d:Ljava/util/BitSet;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/util/BitSet;->set(I)V

    .line 499413
    move-object v5, v6

    .line 499414
    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v5

    move-object v3, v5

    .line 499415
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 499416
    return-object v0

    .line 499417
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 499375
    invoke-static {}, LX/1dS;->b()V

    .line 499376
    const/4 v0, 0x0

    return-object v0
.end method
