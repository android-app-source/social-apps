.class public LX/2U3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2U3;


# instance fields
.field private a:LX/03V;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415057
    iput-object p1, p0, LX/2U3;->a:LX/03V;

    .line 415058
    return-void
.end method

.method public static a(LX/0QB;)LX/2U3;
    .locals 4

    .prologue
    .line 415059
    sget-object v0, LX/2U3;->b:LX/2U3;

    if-nez v0, :cond_1

    .line 415060
    const-class v1, LX/2U3;

    monitor-enter v1

    .line 415061
    :try_start_0
    sget-object v0, LX/2U3;->b:LX/2U3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 415062
    if-eqz v2, :cond_0

    .line 415063
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 415064
    new-instance p0, LX/2U3;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/2U3;-><init>(LX/03V;)V

    .line 415065
    move-object v0, p0

    .line 415066
    sput-object v0, LX/2U3;->b:LX/2U3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415067
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 415068
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 415069
    :cond_1
    sget-object v0, LX/2U3;->b:LX/2U3;

    return-object v0

    .line 415070
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 415071
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 415072
    iget-object v0, p0, LX/2U3;->a:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ad Interfaces: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    const/16 v2, 0x64

    .line 415073
    iput v2, v1, LX/0VK;->e:I

    .line 415074
    move-object v1, v1

    .line 415075
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 415076
    return-void
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 415077
    iget-object v0, p0, LX/2U3;->a:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ad Interfaces: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    const/16 v2, 0x64

    .line 415078
    iput v2, v1, LX/0VK;->e:I

    .line 415079
    move-object v1, v1

    .line 415080
    iput-object p3, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 415081
    move-object v1, v1

    .line 415082
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 415083
    return-void
.end method
