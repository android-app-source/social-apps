.class public final LX/3ED;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Ig;


# direct methods
.method public constructor <init>(LX/3Ig;)V
    .locals 0

    .prologue
    .line 537108
    iput-object p1, p0, LX/3ED;->a:LX/3Ig;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 537109
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 537110
    check-cast p1, LX/2ou;

    .line 537111
    sget-object v0, LX/7MN;->a:[I

    iget-object v1, p1, LX/2ou;->b:LX/2qV;

    invoke-virtual {v1}, LX/2qV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 537112
    :goto_0
    return-void

    .line 537113
    :pswitch_0
    iget-object v0, p0, LX/3ED;->a:LX/3Ig;

    iget-object v1, v0, LX/3Ig;->a:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    iget-object v0, p0, LX/3ED;->a:LX/3Ig;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ED;->a:LX/3Ig;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/6Wv;->DONE_LOADING:LX/6Wv;

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->setState(LX/6Wv;)V

    goto :goto_0

    :cond_0
    sget-object v0, LX/6Wv;->LOADING:LX/6Wv;

    goto :goto_1

    .line 537114
    :pswitch_1
    iget-object v0, p0, LX/3ED;->a:LX/3Ig;

    iget-object v0, v0, LX/3Ig;->a:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    sget-object v1, LX/6Wv;->DONE_LOADING:LX/6Wv;

    invoke-virtual {v0, v1}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->setState(LX/6Wv;)V

    goto :goto_0

    .line 537115
    :pswitch_2
    iget-object v0, p0, LX/3ED;->a:LX/3Ig;

    iget-object v0, v0, LX/3Ig;->a:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    sget-object v1, LX/6Wv;->READY_TO_PLAY:LX/6Wv;

    invoke-virtual {v0, v1}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->setState(LX/6Wv;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
