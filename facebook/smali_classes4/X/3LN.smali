.class public LX/3LN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0c5;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final f:Ljava/lang/Object;


# instance fields
.field private final a:LX/2Oi;

.field private final b:LX/3LO;

.field private final c:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "LX/DAk;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "LX/DAk;",
            "LX/0Px",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 550434
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3LN;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2Oi;LX/3LO;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x4b0

    .line 550427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550428
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/3LN;->d:LX/0QI;

    .line 550429
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/3LN;->e:LX/0QI;

    .line 550430
    iput-object p1, p0, LX/3LN;->a:LX/2Oi;

    .line 550431
    iput-object p2, p0, LX/3LN;->b:LX/3LO;

    .line 550432
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    iget-object v1, p0, LX/3LN;->b:LX/3LO;

    const/16 v2, 0x3c

    const/16 v3, 0xc8

    invoke-virtual {v1, v2, v3}, LX/3LO;->a(II)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/0QN;->a(J)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/3LN;->c:LX/0QI;

    .line 550433
    return-void
.end method

.method public static a(LX/0QB;)LX/3LN;
    .locals 8

    .prologue
    .line 550398
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 550399
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 550400
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 550401
    if-nez v1, :cond_0

    .line 550402
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 550403
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 550404
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 550405
    sget-object v1, LX/3LN;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 550406
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 550407
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 550408
    :cond_1
    if-nez v1, :cond_4

    .line 550409
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 550410
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 550411
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 550412
    new-instance p0, LX/3LN;

    invoke-static {v0}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v1

    check-cast v1, LX/2Oi;

    invoke-static {v0}, LX/3LO;->a(LX/0QB;)LX/3LO;

    move-result-object v7

    check-cast v7, LX/3LO;

    invoke-direct {p0, v1, v7}, LX/3LN;-><init>(LX/2Oi;LX/3LO;)V

    .line 550413
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 550414
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 550415
    if-nez v1, :cond_2

    .line 550416
    sget-object v0, LX/3LN;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LN;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 550417
    :goto_1
    if-eqz v0, :cond_3

    .line 550418
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 550419
    :goto_3
    check-cast v0, LX/3LN;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 550420
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 550421
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 550422
    :catchall_1
    move-exception v0

    .line 550423
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 550424
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 550425
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 550426
    :cond_2
    :try_start_8
    sget-object v0, LX/3LN;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LN;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private declared-synchronized a(Lcom/facebook/user/model/User;)V
    .locals 2

    .prologue
    .line 550392
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 550393
    iget-object v0, p0, LX/3LN;->c:LX/0QI;

    .line 550394
    iget-object v1, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 550395
    invoke-interface {v0, v1, p1}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 550396
    monitor-exit p0

    return-void

    .line 550397
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/DAk;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DAk;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 550435
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3LN;->d:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 550387
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, LX/0Px;->reverse()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 550388
    invoke-direct {p0, v0}, LX/3LN;->a(Lcom/facebook/user/model/User;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 550389
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 550390
    :cond_0
    monitor-exit p0

    return-void

    .line 550391
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/DAk;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DAk;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 550384
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3LN;->d:LX/0QI;

    invoke-interface {v0, p1, p2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 550385
    monitor-exit p0

    return-void

    .line 550386
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/DAk;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DAk;",
            "LX/0Px",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 550381
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3LN;->e:LX/0QI;

    invoke-interface {v0, p1, p2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 550382
    monitor-exit p0

    return-void

    .line 550383
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearUserData()V
    .locals 1

    .prologue
    .line 550376
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3LN;->c:LX/0QI;

    invoke-interface {v0}, LX/0QI;->a()V

    .line 550377
    iget-object v0, p0, LX/3LN;->d:LX/0QI;

    invoke-interface {v0}, LX/0QI;->a()V

    .line 550378
    iget-object v0, p0, LX/3LN;->e:LX/0QI;

    invoke-interface {v0}, LX/0QI;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 550379
    monitor-exit p0

    return-void

    .line 550380
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
