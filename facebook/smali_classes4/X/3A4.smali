.class public LX/3A4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/79Q;",
        "LX/79R;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/3A5;


# direct methods
.method public constructor <init>(LX/3A5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 524704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524705
    iput-object p1, p0, LX/3A4;->a:LX/3A5;

    .line 524706
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 524707
    check-cast p1, LX/79Q;

    .line 524708
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 524709
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "ids"

    .line 524710
    iget-object v3, p1, LX/79Q;->a:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 524711
    const-string v3, ""

    .line 524712
    :goto_0
    move-object v3, v3

    .line 524713
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 524714
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "fields"

    const-string v3, "voip_info"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 524715
    new-instance v1, LX/14O;

    invoke-direct {v1}, LX/14O;-><init>()V

    const-string v2, "rtcPresenceFetch"

    .line 524716
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 524717
    move-object v1, v1

    .line 524718
    const-string v2, ""

    .line 524719
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 524720
    move-object v1, v1

    .line 524721
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 524722
    move-object v0, v1

    .line 524723
    const-string v1, "GET"

    .line 524724
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 524725
    move-object v0, v0

    .line 524726
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 524727
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 524728
    move-object v0, v0

    .line 524729
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 524730
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 524731
    iget-object v3, p1, LX/79Q;->a:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/UserKey;

    .line 524732
    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string p0, ","

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 524733
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v3, v5}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 524734
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 524735
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v2

    .line 524736
    new-instance v3, LX/79R;

    invoke-direct {v3}, LX/79R;-><init>()V

    .line 524737
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524738
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 524739
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    iget-object v4, p0, LX/3A4;->a:LX/3A5;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v4, v0}, LX/3A5;->a(LX/0lF;)LX/79S;

    move-result-object v0

    .line 524740
    iget-object v4, v3, LX/79R;->a:Ljava/util/Map;

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 524741
    goto :goto_0

    .line 524742
    :cond_0
    return-object v3
.end method
