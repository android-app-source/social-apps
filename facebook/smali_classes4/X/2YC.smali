.class public final LX/2YC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:[Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/io/FileFilter;Ljava/util/Comparator;)V
    .locals 1
    .param p2    # Ljava/io/FileFilter;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Comparator;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/io/FileFilter;",
            "Ljava/util/Comparator",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 420794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420795
    if-eqz p2, :cond_1

    .line 420796
    invoke-virtual {p1, p2}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v0

    .line 420797
    :goto_0
    if-eqz v0, :cond_2

    .line 420798
    if-eqz p3, :cond_0

    .line 420799
    invoke-static {v0, p3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 420800
    :cond_0
    :goto_1
    move-object v0, v0

    .line 420801
    iput-object v0, p0, LX/2YC;->a:[Ljava/io/File;

    .line 420802
    return-void

    .line 420803
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 420804
    :cond_2
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/io/File;

    goto :goto_1
.end method


# virtual methods
.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 420805
    new-instance v0, LX/2YD;

    iget-object v1, p0, LX/2YC;->a:[Ljava/io/File;

    invoke-direct {v0, v1}, LX/2YD;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method
