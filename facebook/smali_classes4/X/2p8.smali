.class public LX/2p8;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:Landroid/view/Surface;

.field private final c:LX/2pA;

.field private final d:LX/19e;

.field public final e:LX/19h;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/view/ViewGroup;

.field private h:Landroid/graphics/SurfaceTexture;

.field public i:Landroid/view/TextureView;

.field public j:LX/2qG;

.field private k:LX/2pB;

.field public l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 467728
    const-class v0, LX/2p8;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2p8;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/19e;LX/19h;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/19e;",
            "LX/19h;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 467643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 467644
    new-instance v0, LX/2pA;

    invoke-direct {v0, p0}, LX/2pA;-><init>(LX/2p8;)V

    iput-object v0, p0, LX/2p8;->c:LX/2pA;

    .line 467645
    sget-object v0, LX/2pB;->NOT_INITIALIZED:LX/2pB;

    iput-object v0, p0, LX/2p8;->k:LX/2pB;

    .line 467646
    iput-object p1, p0, LX/2p8;->d:LX/19e;

    .line 467647
    iput-object p2, p0, LX/2p8;->e:LX/19h;

    .line 467648
    iput-object p3, p0, LX/2p8;->f:LX/0Ot;

    .line 467649
    return-void
.end method

.method public static a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 467729
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/2p8;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 467730
    iput-object p3, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 467731
    move-object v0, v0

    .line 467732
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    .line 467733
    iget-object v0, p0, LX/2p8;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 467734
    return-void
.end method

.method public static b(LX/19h;Landroid/view/Surface;Landroid/graphics/SurfaceTexture;LX/2pB;)V
    .locals 2

    .prologue
    .line 467735
    if-eqz p1, :cond_0

    .line 467736
    invoke-virtual {p1}, Landroid/view/Surface;->release()V

    .line 467737
    :cond_0
    sget-object v0, LX/7KH;->a:[I

    invoke-virtual {p3}, LX/2pB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 467738
    :goto_0
    return-void

    .line 467739
    :pswitch_0
    const/4 v0, 0x0

    invoke-static {p0, p2, v0}, LX/19h;->a(LX/19h;Landroid/graphics/SurfaceTexture;Z)V

    .line 467740
    goto :goto_0

    .line 467741
    :pswitch_1
    invoke-virtual {p2}, Landroid/graphics/SurfaceTexture;->release()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(DD)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 467760
    iget-object v1, p0, LX/2p8;->i:Landroid/view/TextureView;

    if-nez v1, :cond_0

    .line 467761
    :goto_0
    return-object v0

    .line 467762
    :cond_0
    :try_start_0
    iget-object v1, p0, LX/2p8;->i:Landroid/view/TextureView;

    iget-object v2, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v2}, Landroid/view/TextureView;->getWidth()I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, p1

    double-to-int v2, v2

    iget-object v3, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v3}, Landroid/view/TextureView;->getHeight()I

    move-result v3

    int-to-double v4, v3

    mul-double/2addr v4, p3

    double-to-int v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/view/TextureView;->getBitmap(II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 467763
    :catch_0
    move-exception v1

    .line 467764
    const-string v2, "getCurrentFrameAsBitmapSLOW"

    const-string v3, "Error encountered in getting current frame bitmap from textureView"

    invoke-static {p0, v2, v3, v1}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 467742
    iget-object v0, p0, LX/2p8;->g:Landroid/view/ViewGroup;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 467743
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 467744
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 467745
    const-string v0, "detachFromView"

    const-string v1, "TextureView must be attached"

    invoke-static {p0, v0, v1, v3}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 467746
    :cond_0
    iget-boolean v0, p0, LX/2p8;->l:Z

    if-nez v0, :cond_1

    .line 467747
    :try_start_0
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/TextureView;->getBitmap(II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 467748
    :cond_1
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/2p8;->g:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 467749
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 467750
    const-string v0, "detachFromView"

    const-string v1, "mTextureView.getParent is not null after removeView"

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 467751
    :cond_2
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2p8;->l:Z

    .line 467752
    iput-object v3, p0, LX/2p8;->g:Landroid/view/ViewGroup;

    .line 467753
    return-void

    .line 467754
    :catch_0
    move-exception v0

    .line 467755
    const-string v1, "detachFromView"

    const-string v2, "Failed to call TextureView.getBitmap"

    invoke-static {p0, v1, v2, v0}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 467756
    :catch_1
    move-exception v0

    .line 467757
    const-string v1, "detachFromView"

    const-string v2, "removeView TextureView failed"

    invoke-static {p0, v1, v2, v0}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 467758
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v0, v3}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 467759
    iput-object v3, p0, LX/2p8;->i:Landroid/view/TextureView;

    goto :goto_1
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 1

    .prologue
    .line 467778
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    if-nez v0, :cond_0

    .line 467779
    :goto_0
    return-void

    .line 467780
    :cond_0
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v0, p1}, Landroid/view/TextureView;->setTransform(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/SurfaceTexture;)V
    .locals 3

    .prologue
    .line 467765
    iput-object p1, p0, LX/2p8;->h:Landroid/graphics/SurfaceTexture;

    .line 467766
    :try_start_0
    iget-object v0, p0, LX/2p8;->a:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 467767
    const-string v0, "acquireSurfaceTexture"

    const-string v1, "acquireSurfaceTexture was called before releaseSurfaceTexture"

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 467768
    iget-object v0, p0, LX/2p8;->a:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 467769
    const/4 v0, 0x0

    iput-object v0, p0, LX/2p8;->a:Landroid/view/Surface;

    .line 467770
    :cond_0
    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, LX/2p8;->a:Landroid/view/Surface;

    .line 467771
    iget-object v0, p0, LX/2p8;->a:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-nez v0, :cond_2

    .line 467772
    const-string v0, "acquireSurfaceTexture"

    const-string v1, "Surface is not valid"

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 .. :try_end_0} :catch_0

    .line 467773
    :cond_1
    :goto_0
    return-void

    .line 467774
    :catch_0
    move-exception v0

    .line 467775
    const-string v1, "acquireSurfaceTexture"

    const-string v2, "Error encountered in creating Surface"

    invoke-static {p0, v1, v2, v0}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 467776
    :cond_2
    iget-object v0, p0, LX/2p8;->j:LX/2qG;

    if-eqz v0, :cond_1

    .line 467777
    iget-object v0, p0, LX/2p8;->j:LX/2qG;

    iget-object v1, p0, LX/2p8;->a:Landroid/view/Surface;

    invoke-interface {v0, v1}, LX/2qG;->a(Landroid/view/Surface;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 467660
    const-string v0, "Must pass a parent as an argument"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467661
    iget-object v0, p0, LX/2p8;->h:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_1

    .line 467662
    const-string v0, "attachToView"

    const-string v2, "onSurfaceTextureDestroyed wasn\'t called"

    invoke-static {p0, v0, v2, v1}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 467663
    iget-object v0, p0, LX/2p8;->k:LX/2pB;

    sget-object v2, LX/2pB;->NEEDS_TO_RECYCLE_SURFACETEXTURE_FOR_REGULAR_VIDEO:LX/2pB;

    if-ne v0, v2, :cond_0

    .line 467664
    sget-object v0, LX/2pB;->NEEDS_TO_RELEASE_SURFACETEXTURE:LX/2pB;

    iput-object v0, p0, LX/2p8;->k:LX/2pB;

    .line 467665
    :cond_0
    iget-object v0, p0, LX/2p8;->h:Landroid/graphics/SurfaceTexture;

    invoke-virtual {p0, v0}, LX/2p8;->b(Landroid/graphics/SurfaceTexture;)V

    .line 467666
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    if-eqz v0, :cond_1

    .line 467667
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 467668
    iput-object v1, p0, LX/2p8;->i:Landroid/view/TextureView;

    .line 467669
    :cond_1
    iput-object p1, p0, LX/2p8;->g:Landroid/view/ViewGroup;

    .line 467670
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    if-nez v0, :cond_2

    .line 467671
    iget-object v0, p0, LX/2p8;->d:LX/19e;

    invoke-virtual {v0}, LX/19e;->a()Landroid/view/TextureView;

    move-result-object v0

    iput-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    .line 467672
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    iget-object v2, p0, LX/2p8;->c:LX/2pA;

    invoke-virtual {v0, v2}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 467673
    :cond_2
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->isAvailable()Z

    move-result v0

    if-nez v0, :cond_b

    .line 467674
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    instance-of v0, v0, LX/2qW;

    if-eqz v0, :cond_5

    .line 467675
    sget-object v0, LX/2pB;->USES_MANAGED_SURFACETEXTURE:LX/2pB;

    iput-object v0, p0, LX/2p8;->k:LX/2pB;

    .line 467676
    iget-object v0, p0, LX/2p8;->e:LX/19h;

    .line 467677
    const/4 v2, 0x1

    invoke-static {v0, v2}, LX/19h;->a(LX/19h;Z)Landroid/graphics/SurfaceTexture;

    move-result-object v2

    move-object v2, v2

    .line 467678
    if-eqz v2, :cond_a

    .line 467679
    :try_start_0
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    check-cast v0, LX/2qW;

    .line 467680
    new-instance v5, Lcom/facebook/video/engine/texview/VideoSurfaceTarget$1;

    invoke-direct {v5, p0, v2}, Lcom/facebook/video/engine/texview/VideoSurfaceTarget$1;-><init>(LX/2p8;Landroid/graphics/SurfaceTexture;)V

    new-instance v6, Lcom/facebook/video/engine/texview/VideoSurfaceTarget$2;

    invoke-direct {v6, p0, v2}, Lcom/facebook/video/engine/texview/VideoSurfaceTarget$2;-><init>(LX/2p8;Landroid/graphics/SurfaceTexture;)V

    .line 467681
    iget-object v7, v0, LX/2qW;->c:LX/7Cy;

    if-eqz v7, :cond_c

    const/4 v7, 0x1

    :goto_0
    invoke-static {v7}, LX/0PB;->checkState(Z)V

    .line 467682
    iget-object v7, v0, LX/2qW;->c:LX/7Cy;

    invoke-virtual {v0}, LX/2qW;->getWidth()I

    move-result v11

    invoke-virtual {v0}, LX/2qW;->getHeight()I

    move-result v12

    move-object v8, v2

    move-object v9, v5

    move-object v10, v6

    .line 467683
    invoke-static/range {v7 .. v12}, LX/7Cy;->a$redex0(LX/7Cy;Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;Ljava/lang/Runnable;II)V

    .line 467684
    invoke-virtual {v0, v2}, LX/2qW;->setSurfaceTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 467685
    move-object v0, v2

    move v2, v3

    .line 467686
    :goto_1
    iget-object v5, p0, LX/2p8;->k:LX/2pB;

    sget-object v6, LX/2pB;->NOT_INITIALIZED:LX/2pB;

    if-eq v5, v6, :cond_7

    move v5, v4

    :goto_2
    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 467687
    iget-object v5, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v5}, Landroid/view/TextureView;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-nez v5, :cond_8

    :goto_3
    const-string v5, "Must detach before re-attaching"

    invoke-static {v4, v5}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 467688
    iget-object v4, p0, LX/2p8;->g:Landroid/view/ViewGroup;

    iget-object v5, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 467689
    iput-boolean v3, p0, LX/2p8;->l:Z

    .line 467690
    iget-object v3, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v3}, Landroid/view/TextureView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-nez v3, :cond_3

    .line 467691
    const-string v3, "attachToView"

    const-string v4, "addView TextureView failed"

    invoke-static {p0, v3, v4, v1}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 467692
    :cond_3
    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    .line 467693
    invoke-virtual {p0, v0}, LX/2p8;->a(Landroid/graphics/SurfaceTexture;)V

    .line 467694
    :cond_4
    return-void

    .line 467695
    :catch_0
    move-exception v0

    .line 467696
    const-string v5, "attachToView"

    const-string v6, "setSurfaceTexture failed for 360 Video"

    invoke-static {p0, v5, v6, v0}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 467697
    invoke-virtual {v2}, Landroid/graphics/SurfaceTexture;->release()V

    move-object v0, v1

    move v2, v3

    .line 467698
    goto :goto_1

    .line 467699
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v0, v2, :cond_9

    .line 467700
    iget-object v0, p0, LX/2p8;->e:LX/19h;

    .line 467701
    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/19h;->a(LX/19h;Z)Landroid/graphics/SurfaceTexture;

    move-result-object v2

    move-object v0, v2

    .line 467702
    :goto_4
    if-eqz v0, :cond_6

    .line 467703
    :try_start_1
    iget-object v2, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v2, v0}, Landroid/view/TextureView;->setSurfaceTexture(Landroid/graphics/SurfaceTexture;)V

    .line 467704
    sget-object v2, LX/2pB;->NEEDS_TO_RECYCLE_SURFACETEXTURE_FOR_REGULAR_VIDEO:LX/2pB;

    iput-object v2, p0, LX/2p8;->k:LX/2pB;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move v2, v4

    .line 467705
    goto :goto_1

    .line 467706
    :catch_1
    move-exception v2

    .line 467707
    const-string v5, "attachToView"

    const-string v6, "setSurfaceTexture failed for Regular Video"

    invoke-static {p0, v5, v6, v2}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 467708
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    move-object v0, v1

    .line 467709
    :cond_6
    sget-object v2, LX/2pB;->NEEDS_TO_RELEASE_SURFACETEXTURE:LX/2pB;

    iput-object v2, p0, LX/2p8;->k:LX/2pB;

    move v2, v3

    goto :goto_1

    :cond_7
    move v5, v3

    .line 467710
    goto :goto_2

    :cond_8
    move v4, v3

    .line 467711
    goto :goto_3

    :cond_9
    move-object v0, v1

    goto :goto_4

    :cond_a
    move-object v0, v2

    move v2, v3

    goto :goto_1

    :cond_b
    move-object v0, v1

    move v2, v3

    goto :goto_1

    .line 467712
    :cond_c
    const/4 v7, 0x0

    goto/16 :goto_0
.end method

.method public final b(Landroid/graphics/SurfaceTexture;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 467713
    iget-object v0, p0, LX/2p8;->h:Landroid/graphics/SurfaceTexture;

    if-nez v0, :cond_0

    .line 467714
    const-string v0, "releaseSurfaceTexture"

    const-string v1, "releaseSurfaceTexture was called before acquireSurfaceTexture, or error occured"

    invoke-static {p0, v0, v1, v7}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 467715
    invoke-virtual {p1}, Landroid/graphics/SurfaceTexture;->release()V

    .line 467716
    :goto_0
    return-void

    .line 467717
    :cond_0
    iget-object v0, p0, LX/2p8;->h:Landroid/graphics/SurfaceTexture;

    if-eq v0, p1, :cond_1

    .line 467718
    const-string v0, "releaseSurfaceTexture"

    const-string v1, "Destroying a different SurfaceTexture?"

    invoke-static {p0, v0, v1, v7}, LX/2p8;->a$redex0(LX/2p8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 467719
    invoke-virtual {p1}, Landroid/graphics/SurfaceTexture;->release()V

    goto :goto_0

    .line 467720
    :cond_1
    iget-object v2, p0, LX/2p8;->e:LX/19h;

    .line 467721
    iget-object v3, p0, LX/2p8;->a:Landroid/view/Surface;

    .line 467722
    iget-object v5, p0, LX/2p8;->k:LX/2pB;

    .line 467723
    iget-object v0, p0, LX/2p8;->j:LX/2qG;

    if-eqz v0, :cond_2

    .line 467724
    iget-object v6, p0, LX/2p8;->j:LX/2qG;

    new-instance v0, LX/7KG;

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, LX/7KG;-><init>(LX/2p8;LX/19h;Landroid/view/Surface;Landroid/graphics/SurfaceTexture;LX/2pB;)V

    invoke-interface {v6, v0}, LX/2qG;->a(LX/7KG;)V

    .line 467725
    :goto_1
    iput-object v7, p0, LX/2p8;->h:Landroid/graphics/SurfaceTexture;

    .line 467726
    iput-object v7, p0, LX/2p8;->a:Landroid/view/Surface;

    goto :goto_0

    .line 467727
    :cond_2
    invoke-static {v2, v3, p1, v5}, LX/2p8;->b(LX/19h;Landroid/view/Surface;Landroid/graphics/SurfaceTexture;LX/2pB;)V

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 467659
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 467656
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    instance-of v0, v0, LX/2qW;

    if-eqz v0, :cond_0

    .line 467657
    const/4 v0, 0x1

    .line 467658
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 467655
    iget-object v0, p0, LX/2p8;->a:Landroid/view/Surface;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/2qW;
    .locals 1

    .prologue
    .line 467653
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    instance-of v0, v0, LX/2qW;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 467654
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    check-cast v0, LX/2qW;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 467652
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 467651
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 467650
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getMeasuredWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 467642
    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2p8;->i:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getMeasuredHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
