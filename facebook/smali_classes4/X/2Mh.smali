.class public LX/2Mh;
.super LX/2Md;
.source ""


# instance fields
.field private final a:LX/0W3;

.field public b:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 397075
    invoke-direct {p0}, LX/2Md;-><init>()V

    .line 397076
    iput-object p1, p0, LX/2Mh;->a:LX/0W3;

    .line 397077
    return-void
.end method

.method public static a(LX/0QB;)LX/2Mh;
    .locals 1

    .prologue
    .line 397078
    invoke-static {p0}, LX/2Mh;->b(LX/0QB;)LX/2Mh;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2Mh;
    .locals 2

    .prologue
    .line 397079
    new-instance v1, LX/2Mh;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    invoke-direct {v1, v0}, LX/2Mh;-><init>(LX/0W3;)V

    .line 397080
    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    .line 397081
    iput-object v0, v1, LX/2Mh;->b:LX/0ad;

    .line 397082
    return-object v1
.end method


# virtual methods
.method public final a()LX/2Mg;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v0, -0x1

    .line 397083
    iget-object v1, p0, LX/2Mh;->a:LX/0W3;

    sget-wide v2, LX/0X5;->fN:J

    const/16 v4, 0x280

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v1

    .line 397084
    iget-object v2, p0, LX/2Mh;->a:LX/0W3;

    sget-wide v4, LX/0X5;->fM:J

    const/16 v3, 0x1e

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JI)I

    move-result v3

    .line 397085
    iget-object v2, p0, LX/2Mh;->a:LX/0W3;

    sget-wide v4, LX/0X5;->fL:J

    const/4 v6, 0x2

    invoke-interface {v2, v4, v5, v6}, LX/0W4;->a(JI)I

    move-result v4

    .line 397086
    iget-object v2, p0, LX/2Mh;->a:LX/0W3;

    sget-wide v6, LX/0X5;->fO:J

    const/16 v5, 0x2d0

    invoke-interface {v2, v6, v7, v5}, LX/0W4;->a(JI)I

    move-result v2

    mul-int/lit16 v2, v2, 0x400

    .line 397087
    iget-object v5, p0, LX/2Mh;->b:LX/0ad;

    sget-object v6, LX/0c0;->Cached:LX/0c0;

    sget-short v7, LX/6eX;->d:S

    invoke-interface {v5, v6, v7, v9}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 397088
    iget-object v2, p0, LX/2Mh;->b:LX/0ad;

    sget-object v5, LX/0c0;->Cached:LX/0c0;

    sget-short v6, LX/6eX;->e:S

    const/4 v7, 0x0

    invoke-interface {v2, v5, v6, v7}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v2, v5, :cond_2

    .line 397089
    const/4 v2, 0x1

    .line 397090
    :goto_0
    move v6, v2

    .line 397091
    iget-object v2, p0, LX/2Mh;->b:LX/0ad;

    sget-object v5, LX/0c0;->Cached:LX/0c0;

    sget v7, LX/6eX;->c:I

    const/16 v8, 0x2d0

    invoke-interface {v2, v5, v7, v8}, LX/0ad;->a(LX/0c0;II)I

    move-result v2

    move v2, v2

    .line 397092
    mul-int/lit16 v2, v2, 0x400

    .line 397093
    :goto_1
    iget-object v5, p0, LX/2Mh;->b:LX/0ad;

    sget-object v7, LX/0c0;->Cached:LX/0c0;

    sget-short v8, LX/6eX;->b:S

    invoke-interface {v5, v7, v8, v9}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 397094
    iget-object v5, p0, LX/2Mh;->b:LX/0ad;

    sget-object v7, LX/0c0;->Cached:LX/0c0;

    sget v8, LX/6eX;->a:I

    invoke-interface {v5, v7, v8, v0}, LX/0ad;->a(LX/0c0;II)I

    move-result v5

    .line 397095
    :goto_2
    iget-object v0, p0, LX/2Mh;->b:LX/0ad;

    sget-object v7, LX/0c0;->Cached:LX/0c0;

    sget-short v8, LX/6eX;->j:S

    invoke-interface {v0, v7, v8, v9}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v7

    .line 397096
    new-instance v0, LX/2Mg;

    int-to-float v3, v3

    invoke-direct/range {v0 .. v7}, LX/2Mg;-><init>(IIFIIIZ)V

    return-object v0

    :cond_0
    move v5, v0

    goto :goto_2

    :cond_1
    move v6, v0

    goto :goto_1

    :cond_2
    const/4 v2, -0x1

    goto :goto_0
.end method
