.class public LX/2Qi;
.super LX/0Tr;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2Qi;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;Ljava/util/Set;)V
    .locals 2
    .param p3    # Ljava/util/Set;
        .annotation runtime Lcom/facebook/platform/database/annotations/PlatformDatabase;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Tt;",
            "Ljava/util/Set",
            "<",
            "LX/0Tw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 408829
    invoke-static {p3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    const-string v1, "platform_db"

    invoke-direct {p0, p1, p2, v0, v1}, LX/0Tr;-><init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V

    .line 408830
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Qi;->a:Z

    .line 408831
    return-void
.end method

.method public static a(LX/0QB;)LX/2Qi;
    .locals 8

    .prologue
    .line 408832
    sget-object v0, LX/2Qi;->b:LX/2Qi;

    if-nez v0, :cond_1

    .line 408833
    const-class v1, LX/2Qi;

    monitor-enter v1

    .line 408834
    :try_start_0
    sget-object v0, LX/2Qi;->b:LX/2Qi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 408835
    if-eqz v2, :cond_0

    .line 408836
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 408837
    new-instance v5, LX/2Qi;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    .line 408838
    new-instance v6, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    new-instance p0, LX/2Qk;

    invoke-direct {p0, v0}, LX/2Qk;-><init>(LX/0QB;)V

    invoke-direct {v6, v7, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v6

    .line 408839
    invoke-direct {v5, v3, v4, v6}, LX/2Qi;-><init>(Landroid/content/Context;LX/0Tt;Ljava/util/Set;)V

    .line 408840
    move-object v0, v5

    .line 408841
    sput-object v0, LX/2Qi;->b:LX/2Qi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408842
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 408843
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 408844
    :cond_1
    sget-object v0, LX/2Qi;->b:LX/2Qi;

    return-object v0

    .line 408845
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 408846
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Landroid/database/sqlite/SQLiteDatabase;
    .locals 3

    .prologue
    .line 408847
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 408848
    if-eqz v0, :cond_0

    iget-boolean v1, p0, LX/2Qi;->a:Z

    if-nez v1, :cond_0

    .line 408849
    const-string v1, "PRAGMA foreign_keys = ON;"

    const v2, 0x5802e5be

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x6da45927

    invoke-static {v1}, LX/03h;->a(I)V

    .line 408850
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/2Qi;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408851
    :cond_0
    monitor-exit p0

    return-object v0

    .line 408852
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 408853
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method
