.class public LX/3Eu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3Eu;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 538135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538136
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 538137
    iput-object v0, p0, LX/3Eu;->a:LX/0Ot;

    .line 538138
    return-void
.end method

.method public static a(LX/0QB;)LX/3Eu;
    .locals 5

    .prologue
    .line 538139
    sget-object v0, LX/3Eu;->c:LX/3Eu;

    if-nez v0, :cond_1

    .line 538140
    const-class v1, LX/3Eu;

    monitor-enter v1

    .line 538141
    :try_start_0
    sget-object v0, LX/3Eu;->c:LX/3Eu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 538142
    if-eqz v2, :cond_0

    .line 538143
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 538144
    new-instance v4, LX/3Eu;

    invoke-direct {v4}, LX/3Eu;-><init>()V

    .line 538145
    const/16 v3, 0xac0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 538146
    iput-object p0, v4, LX/3Eu;->a:LX/0Ot;

    iput-object v3, v4, LX/3Eu;->b:Landroid/content/Context;

    .line 538147
    move-object v0, v4

    .line 538148
    sput-object v0, LX/3Eu;->c:LX/3Eu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 538149
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 538150
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 538151
    :cond_1
    sget-object v0, LX/3Eu;->c:LX/3Eu;

    return-object v0

    .line 538152
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 538153
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 538154
    iget-object v0, p0, LX/3Eu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x2d6

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 538155
    iget-object v1, p0, LX/3Eu;->b:Landroid/content/Context;

    const-string v2, "android_watchdog_patch_store"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 538156
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "android_watchdog_patch"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 538157
    return-void
.end method
