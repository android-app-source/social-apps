.class public LX/37a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static volatile c:LX/37a;


# instance fields
.field private b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 501692
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "com/facebook/video/chromecast/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/37a;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 501693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501694
    iput-object p1, p0, LX/37a;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 501695
    return-void
.end method

.method public static a(LX/0QB;)LX/37a;
    .locals 4

    .prologue
    .line 501677
    sget-object v0, LX/37a;->c:LX/37a;

    if-nez v0, :cond_1

    .line 501678
    const-class v1, LX/37a;

    monitor-enter v1

    .line 501679
    :try_start_0
    sget-object v0, LX/37a;->c:LX/37a;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 501680
    if-eqz v2, :cond_0

    .line 501681
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 501682
    new-instance p0, LX/37a;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/37a;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 501683
    move-object v0, p0

    .line 501684
    sput-object v0, LX/37a;->c:LX/37a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 501685
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 501686
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 501687
    :cond_1
    sget-object v0, LX/37a;->c:LX/37a;

    return-object v0

    .line 501688
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 501689
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 501690
    iget-object v0, p0, LX/37a;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/37a;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/37a;->a:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;)V

    .line 501691
    return-void
.end method

.method public final a(LX/0Tn;)V
    .locals 2

    .prologue
    .line 501675
    const/4 v0, 0x1

    new-array v0, v0, [LX/0Tn;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, LX/37a;->a([LX/0Tn;)V

    .line 501676
    return-void
.end method

.method public final a(LX/0Tn;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 501673
    iget-object v0, p0, LX/37a;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 501674
    return-void
.end method

.method public final a([LX/0Tn;)V
    .locals 4

    .prologue
    .line 501669
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 501670
    iget-object v3, p0, LX/37a;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    invoke-interface {v3, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 501671
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 501672
    :cond_0
    return-void
.end method

.method public final b(LX/0Tn;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 501668
    iget-object v0, p0, LX/37a;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
