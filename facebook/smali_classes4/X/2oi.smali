.class public final enum LX/2oi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2oi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2oi;

.field public static final enum CUSTOM_DEFINITION:LX/2oi;

.field public static final enum HIGH_DEFINITION:LX/2oi;

.field public static final enum STANDARD_DEFINITION:LX/2oi;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 467247
    new-instance v0, LX/2oi;

    const-string v1, "HIGH_DEFINITION"

    invoke-direct {v0, v1, v2}, LX/2oi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    .line 467248
    new-instance v0, LX/2oi;

    const-string v1, "STANDARD_DEFINITION"

    invoke-direct {v0, v1, v3}, LX/2oi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    .line 467249
    new-instance v0, LX/2oi;

    const-string v1, "CUSTOM_DEFINITION"

    invoke-direct {v0, v1, v4}, LX/2oi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2oi;->CUSTOM_DEFINITION:LX/2oi;

    .line 467250
    const/4 v0, 0x3

    new-array v0, v0, [LX/2oi;

    sget-object v1, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    aput-object v1, v0, v2

    sget-object v1, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    aput-object v1, v0, v3

    sget-object v1, LX/2oi;->CUSTOM_DEFINITION:LX/2oi;

    aput-object v1, v0, v4

    sput-object v0, LX/2oi;->$VALUES:[LX/2oi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 467251
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2oi;
    .locals 1

    .prologue
    .line 467252
    const-class v0, LX/2oi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2oi;

    return-object v0
.end method

.method public static values()[LX/2oi;
    .locals 1

    .prologue
    .line 467253
    sget-object v0, LX/2oi;->$VALUES:[LX/2oi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2oi;

    return-object v0
.end method
