.class public LX/3G8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Uh;

.field private final b:LX/0Xl;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;LX/0Ot;LX/0Xl;)V
    .locals 0
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Xl;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 540618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 540619
    iput-object p1, p0, LX/3G8;->a:LX/0Uh;

    .line 540620
    iput-object p2, p0, LX/3G8;->c:LX/0Ot;

    .line 540621
    iput-object p3, p0, LX/3G8;->b:LX/0Xl;

    .line 540622
    return-void
.end method

.method public static a(LX/0QB;)LX/3G8;
    .locals 4

    .prologue
    .line 540623
    new-instance v2, LX/3G8;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0xbc

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-direct {v2, v0, v3, v1}, LX/3G8;-><init>(LX/0Uh;LX/0Ot;LX/0Xl;)V

    .line 540624
    move-object v0, v2

    .line 540625
    return-object v0
.end method


# virtual methods
.method public final a(LX/399;Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 540626
    instance-of v0, p2, LX/4Ua;

    if-eqz v0, :cond_3

    .line 540627
    check-cast p2, LX/4Ua;

    .line 540628
    :goto_0
    move-object v0, p2

    .line 540629
    if-nez v0, :cond_1

    .line 540630
    :cond_0
    :goto_1
    return-void

    .line 540631
    :cond_1
    iget-object v1, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, v1

    .line 540632
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->h()LX/2Aa;

    move-result-object v2

    sget-object v3, LX/2Aa;->GRAPHQL_KERROR_DOMAIN:LX/2Aa;

    if-ne v2, v3, :cond_4

    iget-object v2, v1, Lcom/facebook/graphql/error/GraphQLError;->summary:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, v1, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 540633
    if-eqz v2, :cond_2

    .line 540634
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "graphql_block_access_sentry_restriction"

    invoke-direct {v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 540635
    const-string v2, "sentry"

    .line 540636
    iput-object v2, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 540637
    const-string v2, "query_name"

    iget-object v4, p1, LX/399;->a:LX/0zP;

    .line 540638
    iget-object p2, v4, LX/0gW;->f:Ljava/lang/String;

    move-object v4, p2

    .line 540639
    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 540640
    const-string v2, "call_name"

    iget-object v4, p1, LX/399;->a:LX/0zP;

    .line 540641
    iget-object p2, v4, LX/0gW;->b:Ljava/lang/String;

    move-object v4, p2

    .line 540642
    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 540643
    const-string v2, "code"

    iget v4, v1, Lcom/facebook/graphql/error/GraphQLError;->code:I

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 540644
    const-string v2, "api_error_code"

    iget v4, v1, Lcom/facebook/graphql/error/GraphQLError;->apiErrorCode:I

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 540645
    const-string v2, "error_domain"

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->h()LX/2Aa;

    move-result-object v4

    invoke-virtual {v4}, LX/2Aa;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 540646
    iget-object v2, p0, LX/3G8;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 540647
    :cond_2
    iget-object v2, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    invoke-virtual {v2}, Lcom/facebook/graphql/error/GraphQLError;->j()I

    move-result v2

    const/16 v3, 0x170

    if-ne v2, v3, :cond_5

    const/4 v2, 0x1

    :goto_3
    move v0, v2

    .line 540648
    if-eqz v0, :cond_0

    .line 540649
    iget-object v0, p0, LX/3G8;->a:LX/0Uh;

    const/16 v2, 0x1d

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540650
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 540651
    const-string v2, "BlockAccessRestrictionSummary"

    iget-object v3, v1, Lcom/facebook/graphql/error/GraphQLError;->summary:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 540652
    const-string v2, "BlockAccessRestrictionDescription"

    iget-object v3, v1, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 540653
    const-string v2, "GraphQLOperationName"

    iget-object v3, p1, LX/399;->a:LX/0zP;

    .line 540654
    iget-object v4, v3, LX/0gW;->f:Ljava/lang/String;

    move-object v3, v4

    .line 540655
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 540656
    const-string v2, "GraphqlErrorCode"

    iget v1, v1, Lcom/facebook/graphql/error/GraphQLError;->code:I

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 540657
    iget-object v1, p0, LX/3G8;->b:LX/0Xl;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "BlockAccessRestrictionForGraphQLAction"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_3
    const/4 p2, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_5
    const/4 v2, 0x0

    goto :goto_3
.end method
