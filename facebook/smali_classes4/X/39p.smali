.class public final LX/39p;
.super LX/1n4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1n4",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/39p;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/39s;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/39r;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 523973
    const/4 v0, 0x0

    sput-object v0, LX/39p;->a:LX/39p;

    .line 523974
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/39p;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 523942
    invoke-direct {p0}, LX/1n4;-><init>()V

    .line 523943
    new-instance v0, LX/39r;

    invoke-direct {v0}, LX/39r;-><init>()V

    iput-object v0, p0, LX/39p;->c:LX/39r;

    .line 523944
    return-void
.end method

.method public static declared-synchronized a()LX/39p;
    .locals 2

    .prologue
    .line 523969
    const-class v1, LX/39p;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/39p;->a:LX/39p;

    if-nez v0, :cond_0

    .line 523970
    new-instance v0, LX/39p;

    invoke-direct {v0}, LX/39p;-><init>()V

    sput-object v0, LX/39p;->a:LX/39p;

    .line 523971
    :cond_0
    sget-object v0, LX/39p;->a:LX/39p;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 523972
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(LX/1De;)LX/39s;
    .locals 2

    .prologue
    .line 523962
    new-instance v0, LX/39q;

    invoke-direct {v0}, LX/39q;-><init>()V

    .line 523963
    sget-object v1, LX/39p;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/39s;

    .line 523964
    if-nez v1, :cond_0

    .line 523965
    new-instance v1, LX/39s;

    invoke-direct {v1}, LX/39s;-><init>()V

    .line 523966
    :cond_0
    invoke-static {v1, p0, v0}, LX/39s;->a$redex0(LX/39s;LX/1De;LX/39q;)V

    .line 523967
    move-object v0, v1

    .line 523968
    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1dc;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 523952
    check-cast p2, LX/39q;

    .line 523953
    iget-object v0, p2, LX/39q;->a:LX/1Wk;

    .line 523954
    sget-object p0, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    if-eq v0, p0, :cond_0

    sget-object p0, LX/1Wk;->NEWSFEED_FULLBLEED_SHADOW:LX/1Wk;

    if-ne v0, p0, :cond_1

    .line 523955
    :cond_0
    sget-object p0, LX/39r;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/graphics/drawable/Drawable;

    .line 523956
    :goto_0
    if-eqz p0, :cond_2

    .line 523957
    :goto_1
    move-object v0, p0

    .line 523958
    return-object v0

    .line 523959
    :cond_1
    sget-object p0, LX/39r;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 523960
    :cond_2
    new-instance p0, LX/39v;

    invoke-direct {p0, p1, v0}, LX/39v;-><init>(Landroid/content/Context;LX/1Wk;)V

    move-object p0, p0

    .line 523961
    goto :goto_1
.end method

.method public final a(LX/1De;Ljava/lang/Object;LX/1dc;)V
    .locals 1

    .prologue
    .line 523945
    check-cast p2, Landroid/graphics/drawable/Drawable;

    .line 523946
    check-cast p3, LX/39q;

    .line 523947
    iget-object v0, p3, LX/39q;->a:LX/1Wk;

    .line 523948
    sget-object p0, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    if-eq v0, p0, :cond_0

    sget-object p0, LX/1Wk;->NEWSFEED_FULLBLEED_SHADOW:LX/1Wk;

    if-ne v0, p0, :cond_1

    .line 523949
    :cond_0
    sget-object p0, LX/39r;->a:LX/0Zi;

    invoke-virtual {p0, p2}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 523950
    :goto_0
    return-void

    .line 523951
    :cond_1
    sget-object p0, LX/39r;->b:LX/0Zi;

    invoke-virtual {p0, p2}, LX/0Zj;->a(Ljava/lang/Object;)Z

    goto :goto_0
.end method
