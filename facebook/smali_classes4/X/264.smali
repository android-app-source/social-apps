.class public LX/264;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 370609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 370610
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 370611
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 370612
    :goto_0
    return v1

    .line 370613
    :cond_0
    const-string v10, "has_next_page"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 370614
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v6, v3

    move v3, v2

    .line 370615
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 370616
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 370617
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 370618
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 370619
    const-string v10, "delta_cursor"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 370620
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 370621
    :cond_2
    const-string v10, "end_cursor"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 370622
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 370623
    :cond_3
    const-string v10, "has_previous_page"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 370624
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 370625
    :cond_4
    const-string v10, "start_cursor"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 370626
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 370627
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 370628
    :cond_6
    const/4 v9, 0x6

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 370629
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 370630
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 370631
    if-eqz v3, :cond_7

    .line 370632
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 370633
    :cond_7
    if-eqz v0, :cond_8

    .line 370634
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 370635
    :cond_8
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 370636
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 370637
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 370638
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 370639
    if-eqz v0, :cond_0

    .line 370640
    const-string v1, "delta_cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 370641
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 370642
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 370643
    if-eqz v0, :cond_1

    .line 370644
    const-string v1, "end_cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 370645
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 370646
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 370647
    if-eqz v0, :cond_2

    .line 370648
    const-string v1, "has_next_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 370649
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 370650
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 370651
    if-eqz v0, :cond_3

    .line 370652
    const-string v1, "has_previous_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 370653
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 370654
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 370655
    if-eqz v0, :cond_4

    .line 370656
    const-string v1, "start_cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 370657
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 370658
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 370659
    return-void
.end method
