.class public LX/35G;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 497003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/res/Resources;LX/1Pr;)LX/35L;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pr;",
            ">(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/res/Resources;",
            "TE;)",
            "LX/35L;"
        }
    .end annotation

    .prologue
    .line 497004
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 497005
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 497006
    new-instance v1, LX/35H;

    invoke-direct {v1, v0}, LX/35H;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p2, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/35I;

    .line 497007
    const v2, 0x7f0b1072

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 497008
    new-instance v3, LX/35J;

    invoke-direct {v3, v0}, LX/35J;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 497009
    invoke-interface {p2, v3, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35K;

    .line 497010
    iget-object v3, v0, LX/35K;->b:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    move-object v0, v3

    .line 497011
    new-instance v3, LX/35L;

    invoke-direct {v3, p0, v1, v2, v0}, LX/35L;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/35I;ILcom/facebook/graphql/enums/ChainingSectionViewState;)V

    return-object v3
.end method

.method public static a(LX/21r;Landroid/view/View;LX/35I;ILcom/facebook/graphql/enums/ChainingSectionViewState;Z)V
    .locals 8

    .prologue
    .line 496985
    iget-boolean v0, p2, LX/35I;->a:Z

    move v0, v0

    .line 496986
    if-eqz v0, :cond_1

    .line 496987
    :cond_0
    :goto_0
    return-void

    .line 496988
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/ChainingSectionViewState;->START_ANIMATE:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    if-ne p4, v0, :cond_0

    .line 496989
    iget-boolean v0, p2, LX/35I;->b:Z

    move v0, v0

    .line 496990
    if-nez v0, :cond_0

    .line 496991
    if-eqz p5, :cond_2

    .line 496992
    new-instance v0, LX/C68;

    invoke-direct {v0, p1}, LX/C68;-><init>(Landroid/view/View;)V

    .line 496993
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 496994
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x190

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 496995
    invoke-virtual {p2}, LX/35I;->b()V

    .line 496996
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p2, LX/35I;->b:Z

    .line 496997
    goto :goto_0

    .line 496998
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 496999
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 497000
    :cond_3
    const/4 v0, 0x0

    .line 497001
    iget-object v4, p0, LX/21r;->b:LX/0Sh;

    new-instance v5, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineRowAnimator$1;

    invoke-direct {v5, p0, p1, p3, p2}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineRowAnimator$1;-><init>(LX/21r;Landroid/view/View;ILX/35I;)V

    int-to-long v6, v0

    invoke-virtual {v4, v5, v6, v7}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    .line 497002
    goto :goto_1
.end method
