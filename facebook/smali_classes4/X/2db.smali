.class public LX/2db;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2dX;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:LX/BtK;

.field private c:LX/3rW;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 443478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 443479
    iput-object p1, p0, LX/2db;->a:Landroid/content/Context;

    .line 443480
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 443481
    iput-object v0, p0, LX/2db;->c:LX/3rW;

    .line 443482
    iput-object v0, p0, LX/2db;->b:LX/BtK;

    .line 443483
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 443484
    iget-object v1, p0, LX/2db;->c:LX/3rW;

    if-eqz v1, :cond_0

    .line 443485
    iget-object v1, p0, LX/2db;->c:LX/3rW;

    invoke-virtual {v1, p1}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    .line 443486
    :cond_0
    iget-object v1, p0, LX/2db;->b:LX/BtK;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    iget-object v1, p0, LX/2db;->b:LX/BtK;

    .line 443487
    iget-boolean p1, v1, LX/BtK;->f:Z

    move v1, p1

    .line 443488
    if-eqz v1, :cond_1

    .line 443489
    iget-object v0, p0, LX/2db;->b:LX/BtK;

    .line 443490
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/BtK;->f:Z

    .line 443491
    const/4 v0, 0x0

    .line 443492
    :cond_1
    return v0
.end method

.method public final setCopyTextGestureListener(LX/BtK;)V
    .locals 2

    .prologue
    .line 443493
    iput-object p1, p0, LX/2db;->b:LX/BtK;

    .line 443494
    new-instance v0, LX/3rW;

    iget-object v1, p0, LX/2db;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/2db;->c:LX/3rW;

    .line 443495
    return-void
.end method
