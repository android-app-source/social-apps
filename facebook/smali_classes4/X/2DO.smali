.class public LX/2DO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:LX/0Xl;

.field private final b:Landroid/os/Handler;

.field private final c:LX/0Uo;

.field public final d:LX/2DS;

.field private final e:LX/0YZ;

.field private final f:LX/0YZ;

.field public final g:Landroid/view/accessibility/AccessibilityManager;

.field public h:Landroid/view/accessibility/AccessibilityManager$TouchExplorationStateChangeListener;


# direct methods
.method public constructor <init>(LX/0Xl;Landroid/os/Handler;LX/0Uo;LX/2DS;Landroid/view/accessibility/AccessibilityManager;)V
    .locals 2
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384131
    const/4 v0, 0x0

    iput-object v0, p0, LX/2DO;->h:Landroid/view/accessibility/AccessibilityManager$TouchExplorationStateChangeListener;

    .line 384132
    iput-object p1, p0, LX/2DO;->a:LX/0Xl;

    .line 384133
    iput-object p2, p0, LX/2DO;->b:Landroid/os/Handler;

    .line 384134
    iput-object p3, p0, LX/2DO;->c:LX/0Uo;

    .line 384135
    iput-object p4, p0, LX/2DO;->d:LX/2DS;

    .line 384136
    iput-object p5, p0, LX/2DO;->g:Landroid/view/accessibility/AccessibilityManager;

    .line 384137
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 384138
    new-instance v0, LX/2DT;

    invoke-direct {v0, p0}, LX/2DT;-><init>(LX/2DO;)V

    iput-object v0, p0, LX/2DO;->h:Landroid/view/accessibility/AccessibilityManager$TouchExplorationStateChangeListener;

    .line 384139
    :cond_0
    new-instance v0, LX/2DU;

    invoke-direct {v0, p0}, LX/2DU;-><init>(LX/2DO;)V

    iput-object v0, p0, LX/2DO;->e:LX/0YZ;

    .line 384140
    new-instance v0, LX/2DV;

    invoke-direct {v0, p0}, LX/2DV;-><init>(LX/2DO;)V

    iput-object v0, p0, LX/2DO;->f:LX/0YZ;

    .line 384141
    return-void
.end method

.method public static a(LX/2DO;)V
    .locals 2

    .prologue
    .line 384142
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    iget-object v0, p0, LX/2DO;->h:Landroid/view/accessibility/AccessibilityManager$TouchExplorationStateChangeListener;

    if-eqz v0, :cond_0

    .line 384143
    iget-object v0, p0, LX/2DO;->g:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, LX/2DO;->h:Landroid/view/accessibility/AccessibilityManager$TouchExplorationStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->addTouchExplorationStateChangeListener(Landroid/view/accessibility/AccessibilityManager$TouchExplorationStateChangeListener;)Z

    .line 384144
    :cond_0
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 384145
    iget-object v0, p0, LX/2DO;->g:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384146
    iget-object v0, p0, LX/2DO;->d:LX/2DS;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2DS;->a(ZZ)V

    .line 384147
    :cond_0
    iget-object v0, p0, LX/2DO;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 384148
    invoke-static {p0}, LX/2DO;->a(LX/2DO;)V

    .line 384149
    :cond_1
    iget-object v0, p0, LX/2DO;->a:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    iget-object v2, p0, LX/2DO;->e:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/2DO;->b:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 384150
    iget-object v0, p0, LX/2DO;->a:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    iget-object v2, p0, LX/2DO;->f:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/2DO;->b:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 384151
    return-void
.end method
