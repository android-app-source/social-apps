.class public final LX/2tN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 474941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)I
    .locals 3

    .prologue
    .line 474942
    invoke-static {p0}, LX/0vv;->F(Landroid/view/View;)F

    move-result v0

    .line 474943
    invoke-static {p1}, LX/0vv;->F(Landroid/view/View;)F

    move-result v1

    .line 474944
    cmpl-float v2, v0, v1

    if-lez v2, :cond_0

    .line 474945
    const/4 v0, -0x1

    .line 474946
    :goto_0
    return v0

    .line 474947
    :cond_0
    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 474948
    const/4 v0, 0x1

    goto :goto_0

    .line 474949
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 474950
    check-cast p1, Landroid/view/View;

    check-cast p2, Landroid/view/View;

    invoke-static {p1, p2}, LX/2tN;->a(Landroid/view/View;Landroid/view/View;)I

    move-result v0

    return v0
.end method
