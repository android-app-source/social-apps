.class public LX/2UC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/ContentResolver;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1Ml;


# direct methods
.method public constructor <init>(LX/0Ot;LX/1Ml;LX/0SG;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Landroid/content/ContentResolver;",
            ">;",
            "LX/1Ml;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415514
    iput-object p1, p0, LX/2UC;->b:LX/0Ot;

    .line 415515
    iput-object p2, p0, LX/2UC;->c:LX/1Ml;

    .line 415516
    iput-object p3, p0, LX/2UC;->a:LX/0SG;

    .line 415517
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/util/regex/Pattern;I)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 415578
    invoke-virtual {p1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 415579
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-nez v1, :cond_0

    .line 415580
    const/4 v0, 0x0

    .line 415581
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(Ljava/util/List;Ljava/util/Set;Ljava/util/regex/Pattern;I)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/EiD;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/regex/Pattern;",
            "I)",
            "Ljava/util/List",
            "<",
            "LX/EiE;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 415550
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 415551
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v6

    .line 415552
    invoke-static {p0}, LX/2UC;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v7

    .line 415553
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v3

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EiD;

    .line 415554
    iget-object v1, v0, LX/EiD;->c:Ljava/lang/String;

    invoke-interface {v6, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/EiD;->c:Ljava/lang/String;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v4, v1

    .line 415555
    :goto_1
    const/4 v1, 0x5

    if-ge v4, v1, :cond_0

    .line 415556
    iget-object v1, v0, LX/EiD;->d:Ljava/lang/String;

    invoke-static {v1, p2, p3}, LX/2UC;->a(Ljava/lang/String;Ljava/util/regex/Pattern;I)Ljava/lang/String;

    move-result-object v9

    .line 415557
    if-eqz v9, :cond_0

    .line 415558
    add-int/lit8 v1, v2, 0x1

    .line 415559
    sget-object v2, LX/EiF;->NORMAL:LX/EiF;

    .line 415560
    iget-boolean v10, v0, LX/EiD;->e:Z

    if-nez v10, :cond_1

    iget-object v10, v0, LX/EiD;->c:Ljava/lang/String;

    invoke-interface {v7, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 415561
    :cond_1
    iget-boolean v2, v0, LX/EiD;->e:Z

    if-eqz v2, :cond_4

    sget-object v2, LX/EiF;->PRIORITY_FB_TOKEN_1:LX/EiF;

    .line 415562
    :cond_2
    :goto_2
    new-instance v10, LX/EiE;

    invoke-direct {v10, v9, v2}, LX/EiE;-><init>(Ljava/lang/String;LX/EiF;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 415563
    iget-object v0, v0, LX/EiD;->c:Ljava/lang/String;

    add-int/lit8 v2, v4, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v6, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415564
    const/16 v0, 0x64

    if-gt v1, v0, :cond_6

    move v2, v1

    .line 415565
    goto :goto_0

    :cond_3
    move v4, v3

    .line 415566
    goto :goto_1

    .line 415567
    :cond_4
    sget-object v2, LX/EiF;->PRIORITY_FB_TOKEN_2:LX/EiF;

    goto :goto_2

    .line 415568
    :cond_5
    iget-object v10, v0, LX/EiD;->c:Ljava/lang/String;

    invoke-interface {p1, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 415569
    sget-object v2, LX/EiF;->PRIORITY_SENDER:LX/EiF;

    goto :goto_2

    .line 415570
    :cond_6
    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 415571
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 415572
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v2

    .line 415573
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EiE;

    .line 415574
    iget-object v4, v0, LX/EiE;->a:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 415575
    iget-object v4, v0, LX/EiE;->a:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 415576
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 415577
    :cond_8
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/util/Set;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/EiD;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415545
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    .line 415546
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EiD;

    .line 415547
    iget-boolean v3, v0, LX/EiD;->e:Z

    if-eqz v3, :cond_0

    .line 415548
    iget-object v0, v0, LX/EiD;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 415549
    :cond_1
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2UC;
    .locals 4

    .prologue
    .line 415543
    new-instance v2, LX/2UC;

    const/16 v0, 0x13

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v0

    check-cast v0, LX/1Ml;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {v2, v3, v0, v1}, LX/2UC;-><init>(LX/0Ot;LX/1Ml;LX/0SG;)V

    .line 415544
    return-object v2
.end method


# virtual methods
.method public final a(JJ)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/List",
            "<",
            "LX/EiD;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 415518
    iget-object v0, p0, LX/2UC;->c:LX/1Ml;

    const-string v1, "android.permission.READ_SMS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 415519
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 415520
    :goto_0
    return-object v0

    .line 415521
    :cond_0
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 415522
    sget-object v0, LX/2UJ;->a:Landroid/net/Uri;

    move-object v1, v0

    .line 415523
    iget-object v0, p0, LX/2UC;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    .line 415524
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "date"

    aput-object v4, v2, v6

    const/4 v4, 0x1

    const-string v5, "body"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "address"

    aput-object v5, v2, v4

    .line 415525
    const-string v5, "date DESC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 415526
    if-nez v1, :cond_3

    move v0, v6

    .line 415527
    :goto_1
    if-lez v0, :cond_4

    .line 415528
    const-wide/16 v2, 0x2710

    sub-long v2, p1, v2

    move v0, v6

    .line 415529
    :cond_1
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_4

    const/16 v4, 0x3e8

    if-ge v0, v4, :cond_4

    .line 415530
    add-int/lit8 v0, v0, 0x1

    .line 415531
    const-string v4, "date"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 415532
    cmp-long v6, v4, v2

    if-ltz v6, :cond_4

    iget-object v6, p0, LX/2UC;->a:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v8

    sub-long/2addr v8, v4

    cmp-long v6, v8, p3

    if-gtz v6, :cond_4

    .line 415533
    const-string v6, "body"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 415534
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 415535
    const-string v8, "address"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 415536
    new-instance v9, LX/EiD;

    invoke-direct {v9, v4, v5, v8, v6}, LX/EiD;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 415537
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 415538
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 415539
    :cond_3
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_1

    .line 415540
    :cond_4
    if-eqz v1, :cond_5

    .line 415541
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 415542
    :cond_5
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0
.end method
