.class public abstract LX/2q6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2q7;


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/04g;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:I

.field public B:I

.field public C:LX/7K4;

.field public D:LX/2qD;

.field public E:LX/2qD;

.field public volatile F:LX/7IE;

.field public G:Ljava/lang/String;

.field public H:Landroid/graphics/Bitmap;

.field public I:Landroid/view/Surface;

.field public J:Lcom/facebook/video/engine/VideoPlayerParams;

.field public K:J

.field public L:Z

.field public M:LX/04g;

.field public N:LX/09M;

.field public O:LX/09M;

.field public P:LX/09L;

.field public Q:I

.field public R:LX/04g;

.field public S:LX/097;

.field public T:LX/2qG;

.field public U:Z

.field public V:LX/2qE;

.field public W:LX/0AR;

.field public X:I

.field public final b:Landroid/content/Context;

.field public final c:Landroid/util/AttributeSet;

.field public final d:I

.field public final e:LX/19d;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2pf;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/1C2;

.field public final h:LX/2q9;

.field public final i:LX/2qI;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Yk;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/util/concurrent/ScheduledExecutorService;

.field public final l:LX/0Sh;

.field public final m:LX/0So;

.field public final n:LX/2q3;

.field public final o:LX/13m;

.field public final p:LX/2px;

.field public final q:LX/16V;

.field public final r:LX/0ad;

.field public final s:LX/19j;

.field public final t:Ljava/lang/Boolean;

.field public final u:Z

.field public final v:LX/0Uh;

.field public w:LX/04D;

.field public x:LX/04H;

.field public y:LX/04G;

.field public z:LX/04g;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 470468
    sget-object v0, LX/04g;->BY_ANDROID:LX/04g;

    const/4 v1, 0x1

    new-array v1, v1, [LX/04g;

    const/4 v2, 0x0

    sget-object v3, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/0RA;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/2q6;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILX/19d;LX/0Ot;LX/2pf;LX/2qI;LX/2q9;Ljava/util/concurrent/ScheduledExecutorService;LX/0Sh;Ljava/lang/Boolean;ZLX/0So;LX/2px;LX/13m;LX/2q3;LX/1C2;LX/0ad;LX/0Uh;LX/19j;)V
    .locals 4
    .param p11    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/engine/IsPausedBitmapEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/util/AttributeSet;",
            "I",
            "Lcom/facebook/video/engine/VideoPlayerViewProvider;",
            "LX/0Ot",
            "<",
            "LX/1Yk;",
            ">;",
            "LX/2pf;",
            "LX/2qI;",
            "Lcom/facebook/video/subtitles/controller/SubtitleAdapter;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/lang/Boolean;",
            "Z",
            "LX/0So;",
            "LX/2px;",
            "LX/13m;",
            "LX/2q3;",
            "LX/1C2;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/19j;",
            ")V"
        }
    .end annotation

    .prologue
    .line 470469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 470470
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, LX/2q6;->f:Ljava/util/List;

    .line 470471
    new-instance v2, LX/16V;

    invoke-direct {v2}, LX/16V;-><init>()V

    iput-object v2, p0, LX/2q6;->q:LX/16V;

    .line 470472
    sget-object v2, LX/04D;->UNKNOWN:LX/04D;

    iput-object v2, p0, LX/2q6;->w:LX/04D;

    .line 470473
    sget-object v2, LX/04H;->NO_INFO:LX/04H;

    iput-object v2, p0, LX/2q6;->x:LX/04H;

    .line 470474
    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    iput-object v2, p0, LX/2q6;->y:LX/04G;

    .line 470475
    sget-object v2, LX/04g;->BY_USER:LX/04g;

    iput-object v2, p0, LX/2q6;->z:LX/04g;

    .line 470476
    const/4 v2, -0x1

    iput v2, p0, LX/2q6;->A:I

    .line 470477
    sget-object v2, LX/2qD;->STATE_IDLE:LX/2qD;

    iput-object v2, p0, LX/2q6;->D:LX/2qD;

    .line 470478
    sget-object v2, LX/2qD;->STATE_IDLE:LX/2qD;

    iput-object v2, p0, LX/2q6;->E:LX/2qD;

    .line 470479
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/2q6;->K:J

    .line 470480
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/2q6;->L:Z

    .line 470481
    sget-object v2, LX/04g;->BY_USER:LX/04g;

    iput-object v2, p0, LX/2q6;->M:LX/04g;

    .line 470482
    sget-object v2, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    iput-object v2, p0, LX/2q6;->P:LX/09L;

    .line 470483
    const/4 v2, 0x0

    iput v2, p0, LX/2q6;->Q:I

    .line 470484
    sget-object v2, LX/04g;->BY_ANDROID:LX/04g;

    iput-object v2, p0, LX/2q6;->R:LX/04g;

    .line 470485
    sget-object v2, LX/097;->FROM_STREAM:LX/097;

    iput-object v2, p0, LX/2q6;->S:LX/097;

    .line 470486
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/2q6;->U:Z

    .line 470487
    sget-object v2, LX/2qE;->STATE_UNKNOWN:LX/2qE;

    iput-object v2, p0, LX/2q6;->V:LX/2qE;

    .line 470488
    const/4 v2, 0x0

    iput-object v2, p0, LX/2q6;->W:LX/0AR;

    .line 470489
    const/4 v2, 0x0

    iput v2, p0, LX/2q6;->X:I

    .line 470490
    iput-object p1, p0, LX/2q6;->b:Landroid/content/Context;

    .line 470491
    iput-object p2, p0, LX/2q6;->c:Landroid/util/AttributeSet;

    .line 470492
    iput p3, p0, LX/2q6;->d:I

    .line 470493
    iput-object p4, p0, LX/2q6;->e:LX/19d;

    .line 470494
    invoke-virtual {p0, p6}, LX/2q6;->a(LX/2pf;)V

    .line 470495
    move-object/from16 v0, p17

    iput-object v0, p0, LX/2q6;->g:LX/1C2;

    .line 470496
    iput-object p8, p0, LX/2q6;->h:LX/2q9;

    .line 470497
    iput-object p7, p0, LX/2q6;->i:LX/2qI;

    .line 470498
    iput-object p9, p0, LX/2q6;->k:Ljava/util/concurrent/ScheduledExecutorService;

    .line 470499
    iput-object p10, p0, LX/2q6;->l:LX/0Sh;

    .line 470500
    iput-object p11, p0, LX/2q6;->t:Ljava/lang/Boolean;

    .line 470501
    move/from16 v0, p12

    iput-boolean v0, p0, LX/2q6;->u:Z

    .line 470502
    move-object/from16 v0, p13

    iput-object v0, p0, LX/2q6;->m:LX/0So;

    .line 470503
    move-object/from16 v0, p14

    iput-object v0, p0, LX/2q6;->p:LX/2px;

    .line 470504
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2q6;->o:LX/13m;

    .line 470505
    move-object/from16 v0, p16

    iput-object v0, p0, LX/2q6;->n:LX/2q3;

    .line 470506
    iput-object p5, p0, LX/2q6;->j:LX/0Ot;

    .line 470507
    move-object/from16 v0, p18

    iput-object v0, p0, LX/2q6;->r:LX/0ad;

    .line 470508
    move-object/from16 v0, p19

    iput-object v0, p0, LX/2q6;->v:LX/0Uh;

    .line 470509
    move-object/from16 v0, p20

    iput-object v0, p0, LX/2q6;->s:LX/19j;

    .line 470510
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 4

    .prologue
    .line 470511
    const-string v0, ""

    .line 470512
    iget-object v1, p0, LX/2q6;->W:LX/0AR;

    if-eqz v1, :cond_0

    .line 470513
    const-string v0, "bitrate: %dkbps, w: %d, h: %d"

    iget-object v1, p0, LX/2q6;->W:LX/0AR;

    iget v1, v1, LX/0AR;->c:I

    div-int/lit16 v1, v1, 0x3e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->W:LX/0AR;

    iget v2, v2, LX/0AR;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, LX/2q6;->W:LX/0AR;

    iget v3, v3, LX/0AR;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 470514
    :cond_0
    const-string v1, "Current dash stream is: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, LX/2q6;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470515
    return-object v0
.end method

.method public final B()Z
    .locals 1

    .prologue
    .line 470516
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/2q6;->a(Z)Z

    move-result v0

    return v0
.end method

.method public final C()V
    .locals 15

    .prologue
    .line 470517
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_0

    .line 470518
    invoke-virtual {p0}, LX/2q6;->b()I

    move-result v6

    .line 470519
    :goto_0
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->P:LX/09L;

    iget-object v3, v3, LX/09L;->value:Ljava/lang/String;

    iget-object v4, p0, LX/2q6;->W:LX/0AR;

    iget v5, p0, LX/2q6;->X:I

    iget v7, p0, LX/2q6;->B:I

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v8, v8, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v9, p0, LX/2q6;->w:LX/04D;

    iget-object v10, p0, LX/2q6;->z:LX/04g;

    iget-object v10, v10, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, LX/2q6;->r()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, LX/2q6;->O:LX/09M;

    iget-object v13, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v14, p0, LX/2q6;->S:LX/097;

    iget-object v14, v14, LX/097;->value:Ljava/lang/String;

    invoke-virtual/range {v0 .. v14}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;IIILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 470520
    return-void

    .line 470521
    :cond_0
    iget-object v0, p0, LX/2q6;->v:LX/0Uh;

    const/16 v1, 0x2b6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 470522
    invoke-virtual {p0}, LX/2q6;->l()I

    move-result v6

    goto :goto_0

    .line 470523
    :cond_1
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v6, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    goto :goto_0
.end method

.method public final D()V
    .locals 10

    .prologue
    .line 470524
    const/4 v1, 0x0

    .line 470525
    iget-object v0, p0, LX/2q6;->V:LX/2qE;

    sget-object v2, LX/2qE;->STATE_CREATED:LX/2qE;

    invoke-virtual {v0, v2}, LX/2qE;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/2q6;->L:Z

    if-eqz v0, :cond_2

    .line 470526
    const-string v1, "never_updated"

    .line 470527
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 470528
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, LX/2q6;->w:LX/04D;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, LX/2q6;->r()Ljava/lang/String;

    move-result-object v6

    .line 470529
    sget-object v7, LX/04A;->VIDEO_NO_SURFACE_UPDATE:LX/04A;

    iget-object v8, v7, LX/04A;->value:Ljava/lang/String;

    .line 470530
    if-eqz v2, :cond_3

    iget-object v7, v2, LX/04G;->value:Ljava/lang/String;

    .line 470531
    :goto_1
    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v9, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object p0, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object p0, p0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v9, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    sget-object p0, LX/04F;->VIDEO_PLAYER_TYPE:LX/04F;

    iget-object p0, p0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v9, p0, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    sget-object v9, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v9, v9, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v7, v9, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    sget-object v9, LX/04F;->PLAYER_VERSION:LX/04F;

    iget-object v9, v9, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v7, v9, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    sget-object v9, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v9, v9, LX/04F;->value:Ljava/lang/String;

    invoke-static {v0, v5, v8}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v9, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 470532
    invoke-static {v7, v4}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/04D;)V

    .line 470533
    invoke-static {v0, v7}, LX/1C2;->d(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1C2;

    .line 470534
    :cond_1
    return-void

    .line 470535
    :cond_2
    iget-wide v2, p0, LX/2q6;->K:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2q6;->m:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/2q6;->K:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1f4

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 470536
    const-string v1, "playback_stuck"

    goto :goto_0

    .line 470537
    :cond_3
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public abstract E()V
.end method

.method public final a(LX/04D;)V
    .locals 0

    .prologue
    .line 470538
    iput-object p1, p0, LX/2q6;->w:LX/04D;

    .line 470539
    return-void
.end method

.method public a(LX/04G;)V
    .locals 1

    .prologue
    .line 470458
    iput-object p1, p0, LX/2q6;->y:LX/04G;

    .line 470459
    iget-object v0, p0, LX/2q6;->p:LX/2px;

    .line 470460
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/04G;

    iget-object p0, p0, LX/04G;->value:Ljava/lang/String;

    iput-object p0, v0, LX/2px;->g:Ljava/lang/String;

    .line 470461
    return-void
.end method

.method public final a(LX/04H;)V
    .locals 0

    .prologue
    .line 470540
    iput-object p1, p0, LX/2q6;->x:LX/04H;

    .line 470541
    return-void
.end method

.method public abstract a(LX/04g;I)V
.end method

.method public final a(LX/04g;LX/2qD;)V
    .locals 15

    .prologue
    .line 470542
    iget-object v1, p0, LX/2q6;->N:LX/09M;

    if-eqz v1, :cond_0

    .line 470543
    iget-object v1, p0, LX/2q6;->N:LX/09M;

    invoke-virtual {v1}, LX/09M;->c()V

    .line 470544
    :cond_0
    iget-object v1, p0, LX/2q6;->r:LX/0ad;

    sget-short v2, LX/0ws;->aa:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/2q6;->E:LX/2qD;

    sget-object v2, LX/2qD;->STATE_IDLE:LX/2qD;

    if-eq v1, v2, :cond_2

    .line 470545
    :cond_1
    iget-object v1, p0, LX/2q6;->g:LX/1C2;

    iget-object v2, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v3, p0, LX/2q6;->y:LX/04G;

    iget-object v4, p0, LX/2q6;->P:LX/09L;

    iget-object v4, v4, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, LX/2q6;->b()I

    move-result v6

    iget-object v7, p0, LX/2q6;->S:LX/097;

    iget-object v7, v7, LX/097;->value:Ljava/lang/String;

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v8, v8, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v9, p0, LX/2q6;->w:LX/04D;

    const/4 v10, 0x0

    iget-object v11, p0, LX/2q6;->z:LX/04g;

    iget-object v11, v11, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, LX/2q6;->r()Ljava/lang/String;

    move-result-object v12

    iget-object v13, p0, LX/2q6;->N:LX/09M;

    iget-object v14, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v1 .. v14}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;)LX/1C2;

    .line 470546
    :cond_2
    move-object/from16 v0, p1

    iput-object v0, p0, LX/2q6;->R:LX/04g;

    .line 470547
    move-object/from16 v0, p2

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 470548
    return-void
.end method

.method public final a(LX/2pf;)V
    .locals 1

    .prologue
    .line 470564
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 470565
    return-void
.end method

.method public a(LX/2qD;)V
    .locals 4

    .prologue
    .line 470549
    iput-object p1, p0, LX/2q6;->D:LX/2qD;

    .line 470550
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 470551
    iget-object v2, p0, LX/2q6;->l:LX/0Sh;

    new-instance v3, Lcom/facebook/video/engine/texview/BaseVideoPlayer$1;

    invoke-direct {v3, p0, v0, p1}, Lcom/facebook/video/engine/texview/BaseVideoPlayer$1;-><init>(LX/2q6;LX/2pf;LX/2qD;)V

    invoke-virtual {v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 470552
    :cond_0
    return-void
.end method

.method public final a(LX/2qG;)V
    .locals 0

    .prologue
    .line 470566
    iput-object p1, p0, LX/2q6;->T:LX/2qG;

    .line 470567
    return-void
.end method

.method public abstract a(LX/2qk;)V
.end method

.method public a(LX/7KG;)V
    .locals 1

    .prologue
    .line 470558
    invoke-virtual {p0}, LX/2q6;->z()V

    .line 470559
    sget-object v0, LX/2qk;->FROM_DESTROY_SURFACE:LX/2qk;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qk;)V

    .line 470560
    const/4 v0, 0x0

    iput-object v0, p0, LX/2q6;->I:Landroid/view/Surface;

    .line 470561
    sget-object v0, LX/2qE;->STATE_DESTROYED:LX/2qE;

    iput-object v0, p0, LX/2q6;->V:LX/2qE;

    .line 470562
    invoke-virtual {p1}, LX/7KG;->a()V

    .line 470563
    return-void
.end method

.method public final a(Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 470556
    invoke-virtual {p0, p1}, LX/2q6;->b(Landroid/graphics/RectF;)V

    .line 470557
    return-void
.end method

.method public a(Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;)V
    .locals 0

    .prologue
    .line 470555
    return-void
.end method

.method public a(Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;)V
    .locals 0

    .prologue
    .line 470554
    return-void
.end method

.method public varargs abstract a(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method public final a(Z)Z
    .locals 1

    .prologue
    .line 470553
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2q6;->s:LX/19j;

    iget-boolean v0, v0, LX/19j;->b:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/2qD;)V
    .locals 4

    .prologue
    .line 470462
    sget-object v0, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne p1, v0, :cond_0

    .line 470463
    iget-object v0, p0, LX/2q6;->C:LX/7K4;

    const-string v1, "Play position must be set"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 470464
    :cond_0
    iput-object p1, p0, LX/2q6;->E:LX/2qD;

    .line 470465
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 470466
    iget-object v2, p0, LX/2q6;->l:LX/0Sh;

    new-instance v3, Lcom/facebook/video/engine/texview/BaseVideoPlayer$2;

    invoke-direct {v3, p0, v0, p1}, Lcom/facebook/video/engine/texview/BaseVideoPlayer$2;-><init>(LX/2q6;LX/2pf;LX/2qD;)V

    invoke-virtual {v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 470467
    :cond_1
    return-void
.end method

.method public abstract b(Landroid/graphics/RectF;)V
.end method

.method public abstract b(Landroid/view/Surface;)V
.end method

.method public c(LX/04g;)V
    .locals 1

    .prologue
    .line 470440
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2q6;->L:Z

    .line 470441
    return-void
.end method

.method public final d(LX/04g;)V
    .locals 0

    .prologue
    .line 470438
    iput-object p1, p0, LX/2q6;->z:LX/04g;

    .line 470439
    return-void
.end method

.method public final declared-synchronized e(LX/04g;)V
    .locals 2

    .prologue
    .line 470424
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 470425
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 470426
    :cond_1
    :try_start_1
    const-string v0, "forceLiveVideoToEnd"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, LX/2q6;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470427
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2q6;->U:Z

    .line 470428
    iget-object v0, p0, LX/2q6;->N:LX/09M;

    if-eqz v0, :cond_2

    .line 470429
    iget-object v0, p0, LX/2q6;->N:LX/09M;

    invoke-virtual {v0}, LX/09M;->e()V

    .line 470430
    iget-object v0, p0, LX/2q6;->N:LX/09M;

    invoke-virtual {v0}, LX/09M;->c()V

    .line 470431
    :cond_2
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, LX/2q6;->s:LX/19j;

    iget-boolean v0, v0, LX/19j;->av:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-ne v0, v1, :cond_5

    :cond_3
    iget-object v0, p0, LX/2q6;->s:LX/19j;

    iget-boolean v0, v0, LX/19j;->av:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_IDLE:LX/2qD;

    if-eq v0, v1, :cond_5

    .line 470432
    :cond_4
    sget-object v0, LX/2qD;->STATE_PREPARED:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 470433
    invoke-virtual {p0}, LX/2q6;->b()I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/2q6;->a(LX/04g;I)V

    .line 470434
    invoke-virtual {p0}, LX/2q6;->E()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 470435
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 470436
    :cond_5
    :try_start_2
    iget-object v0, p0, LX/2q6;->s:LX/19j;

    iget-boolean v0, v0, LX/19j;->aP:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_IDLE:LX/2qD;

    if-eq v0, v1, :cond_0

    .line 470437
    invoke-virtual {p0}, LX/2q6;->E()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public f(LX/04g;)V
    .locals 1

    .prologue
    .line 470422
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, p1, v0}, LX/2q6;->a(LX/04g;LX/2qD;)V

    .line 470423
    return-void
.end method

.method public final g()LX/04D;
    .locals 1

    .prologue
    .line 470421
    iget-object v0, p0, LX/2q6;->w:LX/04D;

    return-object v0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 470420
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 470419
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 0

    .prologue
    .line 470418
    return-void
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 470417
    iget v0, p0, LX/2q6;->A:I

    return v0
.end method

.method public final m()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 470414
    iget v0, p0, LX/2q6;->A:I

    if-lez v0, :cond_0

    iget v0, p0, LX/2q6;->A:I

    .line 470415
    :goto_0
    iget v2, p0, LX/2q6;->B:I

    invoke-static {v2, v1, v0}, LX/13m;->a(III)I

    move-result v0

    return v0

    :cond_0
    move v0, v1

    .line 470416
    goto :goto_0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 470410
    iget-object v0, p0, LX/2q6;->H:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 470411
    iget-object v0, p0, LX/2q6;->H:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 470412
    const/4 v0, 0x0

    iput-object v0, p0, LX/2q6;->H:Landroid/graphics/Bitmap;

    .line 470413
    :cond_0
    return-void
.end method

.method public final o()LX/7QP;
    .locals 1

    .prologue
    .line 470442
    const-string v0, "getSubtitles"

    iput-object v0, p0, LX/2q6;->G:Ljava/lang/String;

    .line 470443
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    .line 470444
    iget-object p0, v0, LX/2q9;->b:LX/7QP;

    move-object v0, p0

    .line 470445
    return-object v0
.end method

.method public final p()LX/7IE;
    .locals 1

    .prologue
    .line 470446
    iget-object v0, p0, LX/2q6;->F:LX/7IE;

    return-object v0
.end method

.method public final q()LX/16V;
    .locals 1

    .prologue
    .line 470447
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    return-object v0
.end method

.method public s()I
    .locals 1

    .prologue
    .line 470448
    invoke-virtual {p0}, LX/2q6;->b()I

    move-result v0

    return v0
.end method

.method public t()I
    .locals 1

    .prologue
    .line 470449
    invoke-virtual {p0}, LX/2q6;->s()I

    move-result v0

    return v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 470450
    const/4 v0, 0x0

    return v0
.end method

.method public v()J
    .locals 2

    .prologue
    .line 470451
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final w()Lcom/facebook/video/engine/VideoPlayerParams;
    .locals 1

    .prologue
    .line 470452
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    return-object v0
.end method

.method public x()V
    .locals 3

    .prologue
    .line 470453
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 470454
    iget-object v2, p0, LX/2q6;->M:LX/04g;

    invoke-interface {v0, v2}, LX/2pf;->c(LX/04g;)V

    goto :goto_0

    .line 470455
    :cond_0
    return-void
.end method

.method public final y()V
    .locals 1

    .prologue
    .line 470456
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/2q6;->b(Landroid/graphics/RectF;)V

    .line 470457
    return-void
.end method

.method public abstract z()V
.end method
