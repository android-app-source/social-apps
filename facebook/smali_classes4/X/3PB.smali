.class public final LX/3PB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/util/List",
        "<",
        "LX/3OS;",
        ">;",
        "LX/3OU;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3LL;


# direct methods
.method public constructor <init>(LX/3LL;)V
    .locals 0

    .prologue
    .line 561168
    iput-object p1, p0, LX/3PB;->a:LX/3LL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 561169
    check-cast p1, Ljava/util/List;

    const/4 v1, 0x0

    .line 561170
    if-nez p1, :cond_0

    move-object v0, v1

    .line 561171
    :goto_0
    return-object v0

    .line 561172
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OS;

    .line 561173
    if-eqz v0, :cond_1

    .line 561174
    sget-object v1, LX/JHo;->a:[I

    iget-object v2, v0, LX/3OS;->a:LX/3OT;

    invoke-virtual {v2}, LX/3OT;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 561175
    :goto_1
    new-instance v1, LX/3OU;

    new-instance v2, LX/JHm;

    invoke-direct {v2, p0, v0}, LX/JHm;-><init>(LX/3PB;LX/3OS;)V

    invoke-direct {v1, v0, v2}, LX/3OU;-><init>(LX/3OS;Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    goto :goto_0

    .line 561176
    :pswitch_0
    iget-object v1, p0, LX/3PB;->a:LX/3LL;

    iget-object v1, v1, LX/3LL;->g:LX/3LQ;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/3LQ;->a(Z)V

    goto :goto_1

    .line 561177
    :pswitch_1
    iget-object v1, p0, LX/3PB;->a:LX/3LL;

    iget-object v1, v1, LX/3LL;->g:LX/3LQ;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/3LQ;->a(Z)V

    goto :goto_1

    .line 561178
    :pswitch_2
    iget-object v1, p0, LX/3PB;->a:LX/3LL;

    iget-object v1, v1, LX/3LL;->g:LX/3LQ;

    .line 561179
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "background_location_location_disabled_miniphone_displayed"

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "background_location"

    .line 561180
    iput-object p1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 561181
    move-object v2, v2

    .line 561182
    iget-object p1, v1, LX/3LQ;->a:LX/0Zb;

    invoke-interface {p1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 561183
    goto :goto_1

    .line 561184
    :pswitch_3
    iget-object v1, p0, LX/3PB;->a:LX/3LL;

    iget-object v1, v1, LX/3LL;->g:LX/3LQ;

    .line 561185
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "background_location_chat_context_disabled_miniphone_displayed"

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "background_location"

    .line 561186
    iput-object p1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 561187
    move-object v2, v2

    .line 561188
    iget-object p1, v1, LX/3LQ;->a:LX/0Zb;

    invoke-interface {p1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 561189
    :pswitch_4
    iget-object v1, p0, LX/3PB;->a:LX/3LL;

    iget-object v1, v1, LX/3LL;->g:LX/3LQ;

    .line 561190
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "background_location_miniphone_displayed"

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "background_location"

    .line 561191
    iput-object p1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 561192
    move-object v2, v2

    .line 561193
    iget-object p1, v1, LX/3LQ;->a:LX/0Zb;

    invoke-interface {p1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 561194
    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 561195
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
