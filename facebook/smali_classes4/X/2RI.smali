.class public LX/2RI;
.super LX/2QY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2QY",
        "<",
        "Lcom/facebook/platform/server/protocol/GetAppNameMethod$Params;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0Or;LX/2Qz;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/2Qz;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409752
    const-string v0, "platform_get_app_name"

    invoke-direct {p0, v0, p1, p2}, LX/2QY;-><init>(Ljava/lang/String;LX/0Or;LX/0e6;)V

    .line 409753
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 409754
    check-cast p1, Ljava/lang/String;

    .line 409755
    invoke-static {p1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 409756
    const-string v0, "app_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/server/protocol/GetAppNameMethod$Params;

    return-object v0
.end method
