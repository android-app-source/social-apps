.class public LX/3ME;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:[Ljava/lang/String;

.field private static volatile j:LX/3ME;


# instance fields
.field private c:J

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/content/Context;

.field private final h:LX/3MF;

.field private final i:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 553613
    sget-object v0, LX/2Uo;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "simple"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/3ME;->a:Landroid/net/Uri;

    .line 553614
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "recipient_ids"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "message_count"

    aput-object v2, v0, v1

    sput-object v0, LX/3ME;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0SG;LX/3MF;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 553701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 553702
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3ME;->c:J

    .line 553703
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 553704
    iput-object v0, p0, LX/3ME;->d:Ljava/util/Map;

    .line 553705
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 553706
    iput-object v0, p0, LX/3ME;->e:Ljava/util/Map;

    .line 553707
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 553708
    iput-object v0, p0, LX/3ME;->f:Ljava/util/Map;

    .line 553709
    iput-object p1, p0, LX/3ME;->g:Landroid/content/Context;

    .line 553710
    iput-object p2, p0, LX/3ME;->i:LX/0SG;

    .line 553711
    iput-object p3, p0, LX/3ME;->h:LX/3MF;

    .line 553712
    return-void
.end method

.method private static a(DJ)D
    .locals 6

    .prologue
    .line 553700
    const-wide/16 v0, 0x0

    cmpl-double v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_1

    :cond_0
    :goto_0
    return-wide p0

    :cond_1
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    long-to-double v2, p2

    const-wide v4, 0x41d2064200000000L    # 1.2096E9

    div-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    mul-double/2addr p0, v0

    goto :goto_0
.end method

.method private static a(IJ)D
    .locals 11

    .prologue
    .line 553696
    int-to-double v3, p0

    const-wide v5, 0x3fa999999999999aL    # 0.05

    mul-double/2addr v3, v5

    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    .line 553697
    neg-double v7, v3

    invoke-static {v7, v8}, Ljava/lang/Math;->exp(D)D

    move-result-wide v7

    add-double/2addr v7, v9

    div-double v7, v9, v7

    move-wide v3, v7

    .line 553698
    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    sub-double/2addr v3, v5

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    mul-double/2addr v3, v5

    move-wide v0, v3

    .line 553699
    invoke-static {v0, v1, p1, p2}, LX/3ME;->a(DJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(LX/0QB;)LX/3ME;
    .locals 6

    .prologue
    .line 553683
    sget-object v0, LX/3ME;->j:LX/3ME;

    if-nez v0, :cond_1

    .line 553684
    const-class v1, LX/3ME;

    monitor-enter v1

    .line 553685
    :try_start_0
    sget-object v0, LX/3ME;->j:LX/3ME;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 553686
    if-eqz v2, :cond_0

    .line 553687
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 553688
    new-instance p0, LX/3ME;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/3MF;->a(LX/0QB;)LX/3MF;

    move-result-object v5

    check-cast v5, LX/3MF;

    invoke-direct {p0, v3, v4, v5}, LX/3ME;-><init>(Landroid/content/Context;LX/0SG;LX/3MF;)V

    .line 553689
    move-object v0, p0

    .line 553690
    sput-object v0, LX/3ME;->j:LX/3ME;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 553691
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 553692
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 553693
    :cond_1
    sget-object v0, LX/3ME;->j:LX/3ME;

    return-object v0

    .line 553694
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 553695
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(J)V
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 553643
    const-wide v0, 0x1cf7c5800L

    sub-long v0, p1, v0

    .line 553644
    const/4 v2, 0x3

    new-array v2, v2, [LX/0ux;

    const-string v3, "message_count"

    const-string v4, "0"

    invoke-static {v3, v4}, LX/0uu;->e(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x1

    const-string v4, "date"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, LX/0uu;->f(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string v1, "recipient_ids"

    const-string v3, "% %"

    invoke-static {v1, v3}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-static {v1}, LX/0uu;->a(LX/0ux;)LX/0ux;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v2}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v4

    .line 553645
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 553646
    :try_start_0
    iget-object v0, p0, LX/3ME;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/3ME;->a:Landroid/net/Uri;

    sget-object v2, LX/3ME;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 553647
    :try_start_1
    const-string v0, "recipient_ids"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 553648
    const-string v2, "message_count"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 553649
    const-string v3, "date"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 553650
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    .line 553651
    :try_start_2
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 553652
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v4

    .line 553653
    :try_start_3
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 553654
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 553655
    sub-long v10, p1, v10

    invoke-static {v7, v10, v11}, LX/3ME;->a(IJ)D

    move-result-wide v10

    .line 553656
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-interface {v8, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 553657
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 553658
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 553659
    :cond_1
    if-eqz v1, :cond_2

    .line 553660
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 553661
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 553662
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 553663
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 553664
    iget-object v0, p0, LX/3ME;->h:LX/3MF;

    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3MF;->a(Ljava/util/Collection;)LX/0tf;

    move-result-object v7

    move v2, v6

    .line 553665
    :goto_2
    invoke-virtual {v7}, LX/0tf;->a()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 553666
    invoke-virtual {v7, v2}, LX/0tf;->b(I)J

    move-result-wide v10

    .line 553667
    invoke-virtual {v7, v10, v11}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 553668
    invoke-static {v0}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 553669
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553670
    :cond_3
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 553671
    :cond_4
    invoke-static {v0}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 553672
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    .line 553673
    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553674
    invoke-static {v0}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 553675
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 553676
    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 553677
    :cond_5
    iput-object v4, p0, LX/3ME;->d:Ljava/util/Map;

    .line 553678
    iput-object v3, p0, LX/3ME;->e:Ljava/util/Map;

    .line 553679
    iput-object v5, p0, LX/3ME;->f:Ljava/util/Map;

    .line 553680
    return-void

    .line 553681
    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_1

    .line 553682
    :catch_0
    goto/16 :goto_0
.end method

.method public static declared-synchronized a(LX/3ME;)V
    .locals 6

    .prologue
    .line 553634
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3ME;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 553635
    iget-wide v0, p0, LX/3ME;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long v0, v2, v0

    const-wide/32 v4, 0x5265c00

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    .line 553636
    :goto_0
    monitor-exit p0

    return-void

    .line 553637
    :cond_0
    :try_start_1
    invoke-direct {p0, v2, v3}, LX/3ME;->a(J)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 553638
    :try_start_2
    iput-wide v2, p0, LX/3ME;->c:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 553639
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 553640
    :catch_0
    move-exception v0

    .line 553641
    :try_start_3
    const-string v1, "SmsContactsRankingHelper"

    const-string v4, "load ranking map failed"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 553642
    iput-wide v2, p0, LX/3ME;->c:J

    goto :goto_0

    :catchall_1
    move-exception v0

    iput-wide v2, p0, LX/3ME;->c:J

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)D
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 553626
    invoke-static {p0}, LX/3ME;->a(LX/3ME;)V

    .line 553627
    iget-object v0, p0, LX/3ME;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 553628
    if-nez v0, :cond_0

    .line 553629
    invoke-static {p1}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 553630
    iget-object v1, p0, LX/3ME;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 553631
    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 553632
    iget-object v0, p0, LX/3ME;->e:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 553633
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 553618
    invoke-static {p0}, LX/3ME;->a(LX/3ME;)V

    .line 553619
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 553620
    iget-object v0, p0, LX/3ME;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 553621
    invoke-static {v0}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 553622
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    int-to-long v4, v3

    const-wide/16 v6, 0x6

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 553623
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 553624
    :cond_1
    new-instance v0, LX/6jI;

    invoke-direct {v0, p0}, LX/6jI;-><init>(LX/3ME;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 553625
    const/4 v0, 0x0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-interface {v1, v0, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)D
    .locals 2

    .prologue
    .line 553615
    invoke-static {p0}, LX/3ME;->a(LX/3ME;)V

    .line 553616
    iget-object v0, p0, LX/3ME;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 553617
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
