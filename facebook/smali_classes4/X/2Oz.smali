.class public LX/2Oz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/AutoCloseable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi",
        "ImprovedNewApi"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/concurrent/locks/ReentrantLock;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 404497
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, LX/2Oz;->a:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method public constructor <init>(LX/26j;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 404477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 404478
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, LX/2Oz;->b:Ljava/util/concurrent/locks/ReentrantLock;

    .line 404479
    sget-object v1, LX/2Oz;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 404480
    invoke-virtual {p1}, LX/26j;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p1, LX/26j;->a:LX/0Uh;

    const/16 p0, 0x1ed

    invoke-virtual {v3, p0, v0}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 404481
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 404482
    return-void

    .line 404483
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2Oz;
    .locals 2

    .prologue
    .line 404484
    new-instance v1, LX/2Oz;

    invoke-static {p0}, LX/26j;->b(LX/0QB;)LX/26j;

    move-result-object v0

    check-cast v0, LX/26j;

    invoke-direct {v1, v0}, LX/2Oz;-><init>(LX/26j;)V

    .line 404485
    return-object v1
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 404486
    sget-object v0, LX/2Oz;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/2Oz;
    .locals 0

    .prologue
    .line 404487
    invoke-virtual {p0}, LX/2Oz;->b()V

    .line 404488
    return-object p0
.end method

.method public final b()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 404489
    invoke-static {}, LX/2Oz;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404490
    iget-object v0, p0, LX/2Oz;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 404491
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 404492
    invoke-static {}, LX/2Oz;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404493
    iget-object v0, p0, LX/2Oz;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 404494
    :cond_0
    return-void
.end method

.method public final close()V
    .locals 0

    .prologue
    .line 404495
    invoke-virtual {p0}, LX/2Oz;->c()V

    .line 404496
    return-void
.end method
