.class public abstract LX/2Tu;
.super LX/1Eg;
.source ""


# static fields
.field private static a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0SG;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:LX/0aG;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Class;LX/0SG;Ljava/util/concurrent/ExecutorService;LX/0aG;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0SG;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0aG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 414831
    invoke-direct {p0, p1}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 414832
    sput-object p2, LX/2Tu;->a:Ljava/lang/Class;

    .line 414833
    iput-object p3, p0, LX/2Tu;->b:LX/0SG;

    .line 414834
    iput-object p4, p0, LX/2Tu;->c:Ljava/util/concurrent/ExecutorService;

    .line 414835
    iput-object p5, p0, LX/2Tu;->d:LX/0aG;

    .line 414836
    return-void
.end method


# virtual methods
.method public final i()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 414841
    iget-object v1, p0, LX/2Tu;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 414842
    invoke-virtual {p0}, LX/2Tu;->k()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {p0}, LX/2Tu;->m()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 414843
    :cond_0
    :goto_0
    return v0

    .line 414844
    :cond_1
    invoke-virtual {p0}, LX/2Tu;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 414845
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 414837
    iget-object v0, p0, LX/2Tu;->d:LX/0aG;

    invoke-virtual {p0}, LX/2Tu;->n()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {p0}, LX/2Tu;->o()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x41c0c230

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 414838
    new-instance v1, LX/2VG;

    invoke-direct {v1}, LX/2VG;-><init>()V

    .line 414839
    iget-object v2, p0, LX/2Tu;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 414840
    return-object v1
.end method

.method public abstract k()J
.end method

.method public abstract l()Z
.end method

.method public abstract m()J
.end method

.method public abstract n()Ljava/lang/String;
.end method

.method public abstract o()Lcom/facebook/common/callercontext/CallerContext;
.end method
