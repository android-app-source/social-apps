.class public final LX/2LC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2LD;


# instance fields
.field public final synthetic a:LX/0Uh;

.field private b:[LX/03R;

.field private c:[LX/03R;

.field private d:Z


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 1

    .prologue
    .line 394766
    iput-object p1, p0, LX/2LC;->a:LX/0Uh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394767
    iget-object v0, p1, LX/0Uh;->b:LX/0UW;

    invoke-interface {v0}, LX/0UW;->a()I

    move-result v0

    new-array v0, v0, [LX/03R;

    iput-object v0, p0, LX/2LC;->b:[LX/03R;

    .line 394768
    iget-object v0, p1, LX/0Uh;->b:LX/0UW;

    invoke-interface {v0}, LX/0UW;->a()I

    move-result v0

    new-array v0, v0, [LX/03R;

    iput-object v0, p0, LX/2LC;->c:[LX/03R;

    .line 394769
    return-void
.end method

.method private declared-synchronized a(ILX/03R;)LX/2LC;
    .locals 1

    .prologue
    .line 394763
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2LC;->b:[LX/03R;

    aput-object p2, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394764
    monitor-exit p0

    return-object p0

    .line 394765
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/2LC;ILX/03R;)LX/2LC;
    .locals 1

    .prologue
    .line 394760
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2LC;->c:[LX/03R;

    aput-object p2, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394761
    monitor-exit p0

    return-object p0

    .line 394762
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(IZ)LX/2LC;
    .locals 1

    .prologue
    .line 394759
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/2LC;->b(LX/2LC;ILX/03R;)LX/2LC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(IZ)LX/2LD;
    .locals 2

    .prologue
    .line 394770
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2LC;->b:[LX/03R;

    invoke-static {p2}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    aput-object v1, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394771
    monitor-exit p0

    return-object p0

    .line 394772
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;LX/03R;)LX/2LD;
    .locals 1

    .prologue
    .line 394758
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2LC;->a:LX/0Uh;

    invoke-static {v0, p1}, LX/0Uh;->f(LX/0Uh;Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0, p2}, LX/2LC;->a(ILX/03R;)LX/2LC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Z)LX/2LD;
    .locals 1

    .prologue
    .line 394757
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2LC;->a:LX/0Uh;

    invoke-static {v0, p1}, LX/0Uh;->f(LX/0Uh;Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0, p2}, LX/2LC;->c(IZ)LX/2LC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a([Z)LX/2LD;
    .locals 4

    .prologue
    .line 394749
    const/4 v0, 0x0

    .line 394750
    monitor-enter p0

    :try_start_0
    array-length v1, p1

    iget-object v2, p0, LX/2LC;->b:[LX/03R;

    array-length v2, v2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 394751
    iget-object v1, p0, LX/2LC;->b:[LX/03R;

    array-length v1, v1

    :goto_1
    if-ge v0, v1, :cond_1

    .line 394752
    iget-object v2, p0, LX/2LC;->b:[LX/03R;

    aget-boolean v3, p1, v0

    invoke-static {v3}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v3

    aput-object v3, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394753
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v1, v0

    .line 394754
    goto :goto_0

    .line 394755
    :cond_1
    monitor-exit p0

    return-object p0

    .line 394756
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 5

    .prologue
    .line 394746
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2LC;->a:LX/0Uh;

    iget-object v1, p0, LX/2LC;->b:[LX/03R;

    iget-object v2, p0, LX/2LC;->c:[LX/03R;

    iget-boolean v3, p0, LX/2LC;->d:Z

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, LX/0Uh;->a$redex0(LX/0Uh;[LX/03R;[LX/03R;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394747
    monitor-exit p0

    return-void

    .line 394748
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 4

    .prologue
    .line 394743
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2LC;->a:LX/0Uh;

    iget-object v1, p0, LX/2LC;->b:[LX/03R;

    iget-object v2, p0, LX/2LC;->c:[LX/03R;

    iget-boolean v3, p0, LX/2LC;->d:Z

    invoke-static {v0, v1, v2, v3, p1}, LX/0Uh;->a$redex0(LX/0Uh;[LX/03R;[LX/03R;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394744
    monitor-exit p0

    return-void

    .line 394745
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()LX/2LD;
    .locals 2

    .prologue
    .line 394740
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2LC;->c:[LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394741
    monitor-exit p0

    return-object p0

    .line 394742
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
