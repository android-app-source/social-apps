.class public LX/2Nm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2NK;

.field private final b:LX/2Nn;

.field private final c:LX/2N8;


# direct methods
.method public constructor <init>(LX/2NK;LX/2Nn;LX/2N8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 399979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399980
    iput-object p1, p0, LX/2Nm;->a:LX/2NK;

    .line 399981
    iput-object p2, p0, LX/2Nm;->b:LX/2Nn;

    .line 399982
    iput-object p3, p0, LX/2Nm;->c:LX/2N8;

    .line 399983
    return-void
.end method

.method public static a(LX/0QB;)LX/2Nm;
    .locals 4

    .prologue
    .line 399984
    new-instance v3, LX/2Nm;

    invoke-static {p0}, LX/2NK;->b(LX/0QB;)LX/2NK;

    move-result-object v0

    check-cast v0, LX/2NK;

    .line 399985
    new-instance v2, LX/2Nn;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v1

    check-cast v1, LX/2N8;

    invoke-direct {v2, v1}, LX/2Nn;-><init>(LX/2N8;)V

    .line 399986
    move-object v1, v2

    .line 399987
    check-cast v1, LX/2Nn;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v2

    check-cast v2, LX/2N8;

    invoke-direct {v3, v0, v1, v2}, LX/2Nm;-><init>(LX/2NK;LX/2Nn;LX/2N8;)V

    .line 399988
    move-object v0, v3

    .line 399989
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/model/share/SentShareAttachment;
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 399990
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 399991
    :goto_0
    return-object v0

    .line 399992
    :cond_0
    iget-object v1, p0, LX/2Nm;->c:LX/2N8;

    invoke-virtual {v1, p1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 399993
    const-string v2, "type"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/6fR;->fromDBSerialValue(Ljava/lang/String;)LX/6fR;

    move-result-object v2

    .line 399994
    const-string v3, "attachment"

    invoke-virtual {v1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 399995
    sget-object v3, LX/6ck;->a:[I

    invoke-virtual {v2}, LX/6fR;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 399996
    :pswitch_0
    iget-object v0, p0, LX/2Nm;->a:LX/2NK;

    invoke-virtual {v0, v1}, LX/2NK;->a(LX/0lF;)Lcom/facebook/messaging/model/share/Share;

    move-result-object v0

    .line 399997
    new-instance v1, Lcom/facebook/messaging/model/share/SentShareAttachment;

    sget-object v2, LX/6fR;->SHARE:LX/6fR;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/facebook/messaging/model/share/SentShareAttachment;-><init>(LX/6fR;Lcom/facebook/messaging/model/share/Share;Lcom/facebook/messaging/model/payment/SentPayment;)V

    move-object v0, v1

    .line 399998
    goto :goto_0

    .line 399999
    :pswitch_1
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v5, "currency"

    invoke-virtual {v1, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/math/BigDecimal;

    const-string v7, "amount"

    invoke-virtual {v1, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5, v6}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 400000
    invoke-static {}, Lcom/facebook/messaging/model/payment/SentPayment;->newBuilder()LX/5e5;

    move-result-object v5

    .line 400001
    iput-object v4, v5, LX/5e5;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 400002
    move-object v4, v5

    .line 400003
    const-string v5, "senderCredentialId"

    invoke-virtual {v1, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->c(LX/0lF;)J

    move-result-wide v6

    .line 400004
    iput-wide v6, v4, LX/5e5;->b:J

    .line 400005
    move-object v4, v4

    .line 400006
    const-string v5, "recipientId"

    invoke-virtual {v1, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 400007
    iput-object v5, v4, LX/5e5;->c:Ljava/lang/String;

    .line 400008
    move-object v4, v4

    .line 400009
    const-string v5, "memoText"

    invoke-virtual {v1, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 400010
    iput-object v5, v4, LX/5e5;->d:Ljava/lang/String;

    .line 400011
    move-object v4, v4

    .line 400012
    const-string v5, "pin"

    invoke-virtual {v1, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 400013
    iput-object v5, v4, LX/5e5;->e:Ljava/lang/String;

    .line 400014
    move-object v4, v4

    .line 400015
    const-string v5, "fromPaymentTrigger"

    invoke-virtual {v1, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->g(LX/0lF;)Z

    move-result v5

    .line 400016
    iput-boolean v5, v4, LX/5e5;->g:Z

    .line 400017
    move-object v4, v4

    .line 400018
    const-string v5, "groupThreadId"

    invoke-virtual {v1, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 400019
    iput-object v5, v4, LX/5e5;->j:Ljava/lang/String;

    .line 400020
    move-object v4, v4

    .line 400021
    invoke-virtual {v4}, LX/5e5;->l()Lcom/facebook/messaging/model/payment/SentPayment;

    move-result-object v4

    move-object v0, v4

    .line 400022
    new-instance v1, Lcom/facebook/messaging/model/share/SentShareAttachment;

    sget-object v2, LX/6fR;->PAYMENT:LX/6fR;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Lcom/facebook/messaging/model/share/SentShareAttachment;-><init>(LX/6fR;Lcom/facebook/messaging/model/share/Share;Lcom/facebook/messaging/model/payment/SentPayment;)V

    move-object v0, v1

    .line 400023
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
