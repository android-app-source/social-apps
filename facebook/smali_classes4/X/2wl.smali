.class public LX/2wl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2wl;


# instance fields
.field public final a:LX/0SG;

.field public final b:LX/0Zb;

.field public final c:LX/0Zm;

.field public final d:LX/0So;


# direct methods
.method public constructor <init>(LX/0SG;LX/0Zb;LX/0Zm;LX/0So;)V
    .locals 0
    .param p4    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 478766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478767
    iput-object p1, p0, LX/2wl;->a:LX/0SG;

    .line 478768
    iput-object p2, p0, LX/2wl;->b:LX/0Zb;

    .line 478769
    iput-object p3, p0, LX/2wl;->c:LX/0Zm;

    .line 478770
    iput-object p4, p0, LX/2wl;->d:LX/0So;

    .line 478771
    return-void
.end method

.method public static a(LX/0QB;)LX/2wl;
    .locals 7

    .prologue
    .line 478772
    sget-object v0, LX/2wl;->e:LX/2wl;

    if-nez v0, :cond_1

    .line 478773
    const-class v1, LX/2wl;

    monitor-enter v1

    .line 478774
    :try_start_0
    sget-object v0, LX/2wl;->e:LX/2wl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 478775
    if-eqz v2, :cond_0

    .line 478776
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 478777
    new-instance p0, LX/2wl;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v5

    check-cast v5, LX/0Zm;

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2wl;-><init>(LX/0SG;LX/0Zb;LX/0Zm;LX/0So;)V

    .line 478778
    move-object v0, p0

    .line 478779
    sput-object v0, LX/2wl;->e:LX/2wl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 478780
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 478781
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 478782
    :cond_1
    sget-object v0, LX/2wl;->e:LX/2wl;

    return-object v0

    .line 478783
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 478784
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
