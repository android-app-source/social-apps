.class public final LX/2L5;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field public static final p:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 394687
    new-instance v0, LX/0U1;

    const-string v1, "request_id"

    const-string v2, "TEXT PRIMARY KEY"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->a:LX/0U1;

    .line 394688
    new-instance v0, LX/0U1;

    const-string v1, "conflict_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->b:LX/0U1;

    .line 394689
    new-instance v0, LX/0U1;

    const-string v1, "operation_type"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->c:LX/0U1;

    .line 394690
    new-instance v0, LX/0U1;

    const-string v1, "serialized_param_data"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->d:LX/0U1;

    .line 394691
    new-instance v0, LX/0U1;

    const-string v1, "mutation_query_class"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->e:LX/0U1;

    .line 394692
    new-instance v0, LX/0U1;

    const-string v1, "serialized_mutation_param"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->f:LX/0U1;

    .line 394693
    new-instance v0, LX/0U1;

    const-string v1, "optimistic_model_class"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->g:LX/0U1;

    .line 394694
    new-instance v0, LX/0U1;

    const-string v1, "serialized_optimistic_model"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->h:LX/0U1;

    .line 394695
    new-instance v0, LX/0U1;

    const-string v1, "serialized_ignored_tags"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->i:LX/0U1;

    .line 394696
    new-instance v0, LX/0U1;

    const-string v1, "created_time"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->j:LX/0U1;

    .line 394697
    new-instance v0, LX/0U1;

    const-string v1, "expire_duration_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->k:LX/0U1;

    .line 394698
    new-instance v0, LX/0U1;

    const-string v1, "attempts_number"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->l:LX/0U1;

    .line 394699
    new-instance v0, LX/0U1;

    const-string v1, "max_attempts"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->m:LX/0U1;

    .line 394700
    new-instance v0, LX/0U1;

    const-string v1, "android_build_fingerprint"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->n:LX/0U1;

    .line 394701
    new-instance v0, LX/0U1;

    const-string v1, "app_version_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->o:LX/0U1;

    .line 394702
    new-instance v0, LX/0U1;

    const-string v1, "attach_request"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2L5;->p:LX/0U1;

    return-void
.end method
