.class public LX/2Vu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2Vu;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0lC;

.field private final c:LX/03V;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 418194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418195
    iput-object p1, p0, LX/2Vu;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 418196
    iput-object p2, p0, LX/2Vu;->b:LX/0lC;

    .line 418197
    iput-object p3, p0, LX/2Vu;->c:LX/03V;

    .line 418198
    return-void
.end method

.method public static a(LX/0QB;)LX/2Vu;
    .locals 6

    .prologue
    .line 418199
    sget-object v0, LX/2Vu;->d:LX/2Vu;

    if-nez v0, :cond_1

    .line 418200
    const-class v1, LX/2Vu;

    monitor-enter v1

    .line 418201
    :try_start_0
    sget-object v0, LX/2Vu;->d:LX/2Vu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 418202
    if-eqz v2, :cond_0

    .line 418203
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 418204
    new-instance p0, LX/2Vu;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/2Vu;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/03V;)V

    .line 418205
    move-object v0, p0

    .line 418206
    sput-object v0, LX/2Vu;->d:LX/2Vu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418207
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 418208
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 418209
    :cond_1
    sget-object v0, LX/2Vu;->d:LX/2Vu;

    return-object v0

    .line 418210
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 418211
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static c(LX/2Vu;Ljava/lang/String;)Lcom/facebook/dbllite/data/DblLiteCredentials;
    .locals 3

    .prologue
    .line 418212
    :try_start_0
    iget-object v0, p0, LX/2Vu;->b:LX/0lC;

    const-class v1, Lcom/facebook/dbllite/data/DblLiteCredentials;

    invoke-virtual {v0, p1, v1}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/dbllite/data/DblLiteCredentials;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 418213
    :goto_0
    return-object v0

    .line 418214
    :catch_0
    move-exception v0

    .line 418215
    iget-object v1, p0, LX/2Vu;->c:LX/03V;

    const-string v2, "Corrupt DblLiteCredentials Read"

    invoke-virtual {v1, v2, p1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 418216
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lcom/facebook/dbllite/data/DblLiteCredentials;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 418217
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/JHe;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    .line 418218
    iget-object v2, p0, LX/2Vu;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 418219
    if-eqz v1, :cond_0

    .line 418220
    invoke-static {p0, v1}, LX/2Vu;->c(LX/2Vu;Ljava/lang/String;)Lcom/facebook/dbllite/data/DblLiteCredentials;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 418221
    :cond_0
    monitor-exit p0

    return-object v0

    .line 418222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/dbllite/data/DblLiteCredentials;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418223
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 418224
    iget-object v0, p0, LX/2Vu;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/JHe;->b:LX/0Tn;

    invoke-interface {v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 418225
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, LX/2Vu;->c(LX/2Vu;Ljava/lang/String;)Lcom/facebook/dbllite/data/DblLiteCredentials;

    move-result-object v0

    .line 418226
    if-eqz v0, :cond_0

    .line 418227
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 418228
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 418229
    :cond_1
    monitor-exit p0

    return-object v1
.end method

.method public final declared-synchronized a(Lcom/facebook/dbllite/data/DblLiteCredentials;)V
    .locals 4

    .prologue
    .line 418230
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/facebook/dbllite/data/DblLiteCredentials;->userId:Ljava/lang/String;

    invoke-static {v0}, LX/JHe;->a(Ljava/lang/String;)LX/0Tn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 418231
    :try_start_1
    iget-object v1, p0, LX/2Vu;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v2, p0, LX/2Vu;->b:LX/0lC;

    invoke-virtual {v2, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_1
    .catch LX/28F; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418232
    :goto_0
    monitor-exit p0

    return-void

    .line 418233
    :catch_0
    move-exception v0

    .line 418234
    :try_start_2
    iget-object v1, p0, LX/2Vu;->c:LX/03V;

    const-string v2, "Corrupt DblLiteCredentials Write"

    const-string v3, ""

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 418235
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 418236
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/JHe;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 418237
    iget-object v1, p0, LX/2Vu;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418238
    monitor-exit p0

    return-void

    .line 418239
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
