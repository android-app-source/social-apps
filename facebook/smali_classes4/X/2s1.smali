.class public final LX/2s1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/5LG;

.field public b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

.field public c:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field private f:Z

.field public g:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 472264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472265
    iput-boolean v0, p0, LX/2s1;->e:Z

    .line 472266
    iput-boolean v0, p0, LX/2s1;->f:Z

    .line 472267
    const/4 v0, 0x0

    iput-object v0, p0, LX/2s1;->g:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    return-void
.end method

.method public static a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/2s1;
    .locals 2

    .prologue
    .line 472241
    new-instance v0, LX/2s1;

    invoke-direct {v0}, LX/2s1;-><init>()V

    iget-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 472242
    iput-object v1, v0, LX/2s1;->b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 472243
    move-object v0, v0

    .line 472244
    iget-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    .line 472245
    iput-object v1, v0, LX/2s1;->c:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    .line 472246
    move-object v0, v0

    .line 472247
    iget-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 472248
    iput-object v1, v0, LX/2s1;->a:LX/5LG;

    .line 472249
    move-object v0, v0

    .line 472250
    iget-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->suggestionMechanism:Ljava/lang/String;

    .line 472251
    iput-object v1, v0, LX/2s1;->d:Ljava/lang/String;

    .line 472252
    move-object v0, v0

    .line 472253
    iget-boolean v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->hideAttachment:Z

    .line 472254
    iput-boolean v1, v0, LX/2s1;->e:Z

    .line 472255
    move-object v0, v0

    .line 472256
    iget-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    .line 472257
    iput-object v1, v0, LX/2s1;->g:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    .line 472258
    move-object v0, v0

    .line 472259
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 3

    .prologue
    .line 472261
    iget-object v0, p0, LX/2s1;->b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2s1;->b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->d()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2s1;->b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2s1;->b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2s1;->a:LX/5LG;

    if-nez v0, :cond_1

    .line 472262
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required attribute is null. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 472263
    :cond_1
    new-instance v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-direct {v0, p0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;-><init>(LX/2s1;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 472260
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "verb"

    iget-object v2, p0, LX/2s1;->a:LX/5LG;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "object"

    iget-object v2, p0, LX/2s1;->b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "customIcon"

    iget-object v2, p0, LX/2s1;->c:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "suggestionMechanism"

    iget-object v2, p0, LX/2s1;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "hideAttachment"

    iget-boolean v2, p0, LX/2s1;->e:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "isAttachmentReadOnly"

    iget-boolean v2, p0, LX/2s1;->f:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "associatedPlacesInfo"

    iget-object v2, p0, LX/2s1;->g:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
