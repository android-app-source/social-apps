.class public LX/2OV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 402313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402314
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2OV;->a:Ljava/util/Map;

    .line 402315
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2OV;->b:Ljava/util/Map;

    .line 402316
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2OV;->c:Ljava/util/Map;

    .line 402317
    return-void
.end method

.method public static a(LX/0QB;)LX/2OV;
    .locals 3

    .prologue
    .line 402318
    const-class v1, LX/2OV;

    monitor-enter v1

    .line 402319
    :try_start_0
    sget-object v0, LX/2OV;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 402320
    sput-object v2, LX/2OV;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 402321
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402322
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 402323
    new-instance v0, LX/2OV;

    invoke-direct {v0}, LX/2OV;-><init>()V

    .line 402324
    move-object v0, v0

    .line 402325
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 402326
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2OV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 402327
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 402328
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
