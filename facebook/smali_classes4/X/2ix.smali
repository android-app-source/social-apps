.class public LX/2ix;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2ix;


# instance fields
.field private a:LX/2it;


# direct methods
.method public constructor <init>(LX/2it;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 452280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 452281
    iput-object p1, p0, LX/2ix;->a:LX/2it;

    .line 452282
    return-void
.end method

.method public static a(LX/0QB;)LX/2ix;
    .locals 4

    .prologue
    .line 452267
    sget-object v0, LX/2ix;->b:LX/2ix;

    if-nez v0, :cond_1

    .line 452268
    const-class v1, LX/2ix;

    monitor-enter v1

    .line 452269
    :try_start_0
    sget-object v0, LX/2ix;->b:LX/2ix;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 452270
    if-eqz v2, :cond_0

    .line 452271
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 452272
    new-instance p0, LX/2ix;

    invoke-static {v0}, LX/2it;->a(LX/0QB;)LX/2it;

    move-result-object v3

    check-cast v3, LX/2it;

    invoke-direct {p0, v3}, LX/2ix;-><init>(LX/2it;)V

    .line 452273
    move-object v0, p0

    .line 452274
    sput-object v0, LX/2ix;->b:LX/2ix;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 452275
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 452276
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 452277
    :cond_1
    sget-object v0, LX/2ix;->b:LX/2ix;

    return-object v0

    .line 452278
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 452279
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static synthetic a(LX/2ix;Landroid/view/MotionEvent;LX/0g8;)Z
    .locals 3

    .prologue
    .line 452257
    const/4 v1, 0x0

    .line 452258
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    .line 452259
    :cond_0
    move v0, v1

    .line 452260
    return v0

    :cond_1
    move v0, v1

    .line 452261
    :goto_0
    invoke-interface {p2}, LX/0g8;->p()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 452262
    invoke-interface {p2, v0}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v2

    .line 452263
    const p0, 0x7f0d2e68

    invoke-virtual {v2, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 452264
    if-eqz v2, :cond_2

    .line 452265
    invoke-virtual {v2, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 452266
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0g8;)V
    .locals 1

    .prologue
    .line 452252
    iget-object v0, p0, LX/2ix;->a:LX/2it;

    invoke-virtual {v0}, LX/2it;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 452253
    :goto_0
    return-void

    .line 452254
    :cond_0
    instance-of v0, p1, LX/2iI;

    if-eqz v0, :cond_1

    .line 452255
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    invoke-interface {p1, v0}, LX/0g8;->a(Landroid/graphics/drawable/Drawable;)V

    .line 452256
    :cond_1
    new-instance v0, LX/2jh;

    invoke-direct {v0, p0, p1}, LX/2jh;-><init>(LX/2ix;LX/0g8;)V

    invoke-interface {p1, v0}, LX/0g8;->a(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method public final a(LX/0g8;I)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 452241
    iget-object v0, p0, LX/2ix;->a:LX/2it;

    invoke-virtual {v0}, LX/2it;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 452242
    :cond_0
    return-void

    .line 452243
    :cond_1
    const/4 v0, 0x1

    if-eq p2, v0, :cond_2

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 452244
    :cond_2
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-interface {p1}, LX/0g8;->p()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 452245
    invoke-interface {p1, v2}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d2e68

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 452246
    instance-of v0, v1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 452247
    check-cast v0, Landroid/view/ViewGroup;

    .line 452248
    invoke-static {v0, v3}, LX/8t0;->a(Landroid/view/ViewGroup;I)V

    .line 452249
    :cond_3
    instance-of v0, v1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz v0, :cond_4

    .line 452250
    check-cast v1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-static {v1, v3}, LX/8t0;->a(Lcom/facebook/fbui/widget/layout/ImageBlockLayout;I)V

    .line 452251
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public final a(LX/0g8;Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 452238
    iget-object v0, p0, LX/2ix;->a:LX/2it;

    invoke-virtual {v0}, LX/2it;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 452239
    :goto_0
    return-void

    .line 452240
    :cond_0
    new-instance v0, LX/2ji;

    invoke-direct {v0, p0, p1}, LX/2ji;-><init>(LX/2ix;LX/0g8;)V

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OH;)V

    goto :goto_0
.end method
