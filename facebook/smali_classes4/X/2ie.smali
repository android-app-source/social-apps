.class public final enum LX/2ie;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2ie;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2ie;

.field public static final enum CONTACTS_SECTION:LX/2ie;

.field public static final enum CONTACTS_SECTION_HEADER:LX/2ie;

.field public static final enum FRIEND_REQUEST:LX/2ie;

.field public static final enum FRIEND_REQUESTS_HEADER:LX/2ie;

.field public static final enum FRIEND_REQUESTS_SHOW_MORE:LX/2ie;

.field public static final enum NO_REQUESTS:LX/2ie;

.field public static final enum PERSON_YOU_MAY_INVITE:LX/2ie;

.field public static final enum PERSON_YOU_MAY_KNOW:LX/2ie;

.field public static final enum PYMK_HEADER:LX/2ie;

.field public static final enum RESPONDED_PERSON_YOU_MAY_INVITE:LX/2ie;

.field public static final enum RESPONDED_PERSON_YOU_MAY_KNOW:LX/2ie;

.field public static final enum SEE_ALL_PYMK:LX/2ie;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 451901
    new-instance v0, LX/2ie;

    const-string v1, "FRIEND_REQUESTS_HEADER"

    invoke-direct {v0, v1, v3}, LX/2ie;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ie;->FRIEND_REQUESTS_HEADER:LX/2ie;

    .line 451902
    new-instance v0, LX/2ie;

    const-string v1, "NO_REQUESTS"

    invoke-direct {v0, v1, v4}, LX/2ie;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ie;->NO_REQUESTS:LX/2ie;

    .line 451903
    new-instance v0, LX/2ie;

    const-string v1, "FRIEND_REQUEST"

    invoke-direct {v0, v1, v5}, LX/2ie;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ie;->FRIEND_REQUEST:LX/2ie;

    .line 451904
    new-instance v0, LX/2ie;

    const-string v1, "PYMK_HEADER"

    invoke-direct {v0, v1, v6}, LX/2ie;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ie;->PYMK_HEADER:LX/2ie;

    .line 451905
    new-instance v0, LX/2ie;

    const-string v1, "PERSON_YOU_MAY_INVITE"

    invoke-direct {v0, v1, v7}, LX/2ie;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ie;->PERSON_YOU_MAY_INVITE:LX/2ie;

    .line 451906
    new-instance v0, LX/2ie;

    const-string v1, "RESPONDED_PERSON_YOU_MAY_INVITE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/2ie;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ie;->RESPONDED_PERSON_YOU_MAY_INVITE:LX/2ie;

    .line 451907
    new-instance v0, LX/2ie;

    const-string v1, "PERSON_YOU_MAY_KNOW"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/2ie;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ie;->PERSON_YOU_MAY_KNOW:LX/2ie;

    .line 451908
    new-instance v0, LX/2ie;

    const-string v1, "RESPONDED_PERSON_YOU_MAY_KNOW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/2ie;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ie;->RESPONDED_PERSON_YOU_MAY_KNOW:LX/2ie;

    .line 451909
    new-instance v0, LX/2ie;

    const-string v1, "SEE_ALL_PYMK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/2ie;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ie;->SEE_ALL_PYMK:LX/2ie;

    .line 451910
    new-instance v0, LX/2ie;

    const-string v1, "CONTACTS_SECTION_HEADER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/2ie;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ie;->CONTACTS_SECTION_HEADER:LX/2ie;

    .line 451911
    new-instance v0, LX/2ie;

    const-string v1, "CONTACTS_SECTION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/2ie;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ie;->CONTACTS_SECTION:LX/2ie;

    .line 451912
    new-instance v0, LX/2ie;

    const-string v1, "FRIEND_REQUESTS_SHOW_MORE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/2ie;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ie;->FRIEND_REQUESTS_SHOW_MORE:LX/2ie;

    .line 451913
    const/16 v0, 0xc

    new-array v0, v0, [LX/2ie;

    sget-object v1, LX/2ie;->FRIEND_REQUESTS_HEADER:LX/2ie;

    aput-object v1, v0, v3

    sget-object v1, LX/2ie;->NO_REQUESTS:LX/2ie;

    aput-object v1, v0, v4

    sget-object v1, LX/2ie;->FRIEND_REQUEST:LX/2ie;

    aput-object v1, v0, v5

    sget-object v1, LX/2ie;->PYMK_HEADER:LX/2ie;

    aput-object v1, v0, v6

    sget-object v1, LX/2ie;->PERSON_YOU_MAY_INVITE:LX/2ie;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/2ie;->RESPONDED_PERSON_YOU_MAY_INVITE:LX/2ie;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2ie;->PERSON_YOU_MAY_KNOW:LX/2ie;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2ie;->RESPONDED_PERSON_YOU_MAY_KNOW:LX/2ie;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2ie;->SEE_ALL_PYMK:LX/2ie;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2ie;->CONTACTS_SECTION_HEADER:LX/2ie;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/2ie;->CONTACTS_SECTION:LX/2ie;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/2ie;->FRIEND_REQUESTS_SHOW_MORE:LX/2ie;

    aput-object v2, v0, v1

    sput-object v0, LX/2ie;->$VALUES:[LX/2ie;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 451914
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2ie;
    .locals 1

    .prologue
    .line 451915
    const-class v0, LX/2ie;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2ie;

    return-object v0
.end method

.method public static values()[LX/2ie;
    .locals 1

    .prologue
    .line 451916
    sget-object v0, LX/2ie;->$VALUES:[LX/2ie;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2ie;

    return-object v0
.end method
