.class public LX/32f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 490797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490798
    iput-object p1, p0, LX/32f;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 490799
    return-void
.end method

.method public static a(LX/0QB;)LX/32f;
    .locals 4

    .prologue
    .line 490800
    const-class v1, LX/32f;

    monitor-enter v1

    .line 490801
    :try_start_0
    sget-object v0, LX/32f;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 490802
    sput-object v2, LX/32f;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 490803
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490804
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 490805
    new-instance p0, LX/32f;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/32f;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 490806
    move-object v0, p0

    .line 490807
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 490808
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/32f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 490809
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 490810
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
