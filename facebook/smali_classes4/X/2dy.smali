.class public final LX/2dy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17z;
.implements LX/0jQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/17z",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
        ">;",
        "LX/0jQ;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/2dx;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2dx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;",
            "LX/2dx;",
            ")V"
        }
    .end annotation

    .prologue
    .line 444675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444676
    iput-object p1, p0, LX/2dy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 444677
    iput-object p2, p0, LX/2dy;->b:LX/2dx;

    .line 444678
    return-void
.end method


# virtual methods
.method public final c()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 444679
    iget-object v0, p0, LX/2dy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 444680
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 444681
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final g()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444682
    iget-object v0, p0, LX/2dy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    return-object v0
.end method
