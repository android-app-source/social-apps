.class public final enum LX/2M7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2M7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2M7;

.field public static final enum ALWAYS:LX/2M7;

.field public static final enum ONLY_WHEN_SPACE_AVAILABLE:LX/2M7;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 396249
    new-instance v0, LX/2M7;

    const-string v1, "ALWAYS"

    invoke-direct {v0, v1, v2}, LX/2M7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2M7;->ALWAYS:LX/2M7;

    .line 396250
    new-instance v0, LX/2M7;

    const-string v1, "ONLY_WHEN_SPACE_AVAILABLE"

    invoke-direct {v0, v1, v3}, LX/2M7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2M7;->ONLY_WHEN_SPACE_AVAILABLE:LX/2M7;

    .line 396251
    const/4 v0, 0x2

    new-array v0, v0, [LX/2M7;

    sget-object v1, LX/2M7;->ALWAYS:LX/2M7;

    aput-object v1, v0, v2

    sget-object v1, LX/2M7;->ONLY_WHEN_SPACE_AVAILABLE:LX/2M7;

    aput-object v1, v0, v3

    sput-object v0, LX/2M7;->$VALUES:[LX/2M7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 396252
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2M7;
    .locals 1

    .prologue
    .line 396253
    const-class v0, LX/2M7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2M7;

    return-object v0
.end method

.method public static values()[LX/2M7;
    .locals 1

    .prologue
    .line 396254
    sget-object v0, LX/2M7;->$VALUES:[LX/2M7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2M7;

    return-object v0
.end method
