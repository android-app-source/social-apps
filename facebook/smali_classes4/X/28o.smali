.class public LX/28o;
.super LX/16B;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/28o;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/28p;


# direct methods
.method public constructor <init>(LX/0Or;LX/28p;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/28p;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374802
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 374803
    iput-object p1, p0, LX/28o;->a:LX/0Or;

    .line 374804
    iput-object p2, p0, LX/28o;->b:LX/28p;

    .line 374805
    return-void
.end method

.method public static a(LX/0QB;)LX/28o;
    .locals 5

    .prologue
    .line 374789
    sget-object v0, LX/28o;->c:LX/28o;

    if-nez v0, :cond_1

    .line 374790
    const-class v1, LX/28o;

    monitor-enter v1

    .line 374791
    :try_start_0
    sget-object v0, LX/28o;->c:LX/28o;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 374792
    if-eqz v2, :cond_0

    .line 374793
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 374794
    new-instance v4, LX/28o;

    const/16 v3, 0x15e7

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/28p;->a(LX/0QB;)LX/28p;

    move-result-object v3

    check-cast v3, LX/28p;

    invoke-direct {v4, p0, v3}, LX/28o;-><init>(LX/0Or;LX/28p;)V

    .line 374795
    move-object v0, v4

    .line 374796
    sput-object v0, LX/28o;->c:LX/28o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 374797
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 374798
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 374799
    :cond_1
    sget-object v0, LX/28o;->c:LX/28o;

    return-object v0

    .line 374800
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 374801
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/auth/component/AuthenticationResult;)V
    .locals 2
    .param p1    # Lcom/facebook/auth/component/AuthenticationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 374806
    if-eqz p1, :cond_0

    .line 374807
    iget-object v0, p0, LX/28o;->b:LX/28p;

    invoke-interface {p1}, Lcom/facebook/auth/component/AuthenticationResult;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/28p;->a(Ljava/lang/String;)V

    .line 374808
    :cond_0
    return-void
.end method

.method public final init()V
    .locals 2

    .prologue
    .line 374787
    iget-object v1, p0, LX/28o;->b:LX/28p;

    iget-object v0, p0, LX/28o;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/28p;->a(Ljava/lang/String;)V

    .line 374788
    return-void
.end method
