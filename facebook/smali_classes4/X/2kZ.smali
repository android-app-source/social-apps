.class public LX/2kZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Edge:",
        "Ljava/lang/Object;",
        "UserInfo:",
        "Ljava/lang/Object;",
        "ResponseModel:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Z

.field public final d:LX/1rs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1rs",
            "<TEdge;TUserInfo;TResponseModel;>;"
        }
    .end annotation
.end field

.field public final e:J


# direct methods
.method public constructor <init>(LX/1rs;JZLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # LX/1rs;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1rs",
            "<TEdge;TUserInfo;TResponseModel;>;JZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 456343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456344
    iput-object p1, p0, LX/2kZ;->d:LX/1rs;

    .line 456345
    iput-wide p2, p0, LX/2kZ;->e:J

    .line 456346
    iput-boolean p4, p0, LX/2kZ;->c:Z

    .line 456347
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/2kZ;->a:Ljava/lang/String;

    .line 456348
    iput-object p6, p0, LX/2kZ;->b:Ljava/lang/String;

    .line 456349
    return-void
.end method
