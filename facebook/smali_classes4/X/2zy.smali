.class public abstract enum LX/2zy;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2zy;",
        ">;",
        "LX/0QK",
        "<",
        "Ljava/util/Map$Entry",
        "<**>;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2zy;

.field public static final enum KEY:LX/2zy;

.field public static final enum VALUE:LX/2zy;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 483975
    new-instance v0, LX/2I6;

    const-string v1, "KEY"

    invoke-direct {v0, v1, v2}, LX/2I6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2zy;->KEY:LX/2zy;

    .line 483976
    new-instance v0, LX/35b;

    const-string v1, "VALUE"

    invoke-direct {v0, v1, v3}, LX/35b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2zy;->VALUE:LX/2zy;

    .line 483977
    const/4 v0, 0x2

    new-array v0, v0, [LX/2zy;

    sget-object v1, LX/2zy;->KEY:LX/2zy;

    aput-object v1, v0, v2

    sget-object v1, LX/2zy;->VALUE:LX/2zy;

    aput-object v1, v0, v3

    sput-object v0, LX/2zy;->$VALUES:[LX/2zy;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 483978
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2zy;
    .locals 1

    .prologue
    .line 483979
    const-class v0, LX/2zy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2zy;

    return-object v0
.end method

.method public static values()[LX/2zy;
    .locals 1

    .prologue
    .line 483980
    sget-object v0, LX/2zy;->$VALUES:[LX/2zy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2zy;

    return-object v0
.end method
