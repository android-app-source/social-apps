.class public LX/3Lv;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:LX/0ux;

.field private static final c:[Ljava/lang/String;

.field public static final d:LX/0ux;

.field private static final e:Ljava/util/regex/Pattern;


# instance fields
.field private final f:Landroid/content/Context;

.field private final g:LX/01T;

.field private final h:LX/2Oq;

.field private final i:LX/3Lw;

.field private final j:LX/3Lo;

.field private final k:LX/3Lx;

.field private final l:LX/1Ml;

.field private final m:LX/3ME;

.field private final n:LX/3MH;

.field private final o:LX/2Or;

.field private final p:LX/2Uq;

.field private final q:LX/3MQ;

.field private final r:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 552749
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "data1"

    aput-object v1, v0, v3

    const-string v1, "data2"

    aput-object v1, v0, v4

    const-string v1, "contact_id"

    aput-object v1, v0, v5

    const-string v1, "display_name"

    aput-object v1, v0, v6

    sput-object v0, LX/3Lv;->a:[Ljava/lang/String;

    .line 552750
    new-array v0, v6, [LX/0ux;

    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/email_v2"

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "display_name"

    invoke-static {v1}, LX/0uu;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "display_name"

    const-string v2, ""

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-static {v1}, LX/0uu;->a(LX/0ux;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    sput-object v0, LX/3Lv;->b:LX/0ux;

    .line 552751
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "data4"

    aput-object v1, v0, v5

    const-string v1, "data1"

    aput-object v1, v0, v6

    const-string v1, "data2"

    aput-object v1, v0, v7

    sput-object v0, LX/3Lv;->c:[Ljava/lang/String;

    .line 552752
    const/4 v0, 0x6

    new-array v0, v0, [LX/0ux;

    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/phone_v2"

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "has_phone_number"

    const-string v2, "1"

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "data1"

    invoke-static {v1}, LX/0uu;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v5

    const-string v1, "data1"

    const-string v2, ""

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-static {v1}, LX/0uu;->a(LX/0ux;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "display_name"

    invoke-static {v1}, LX/0uu;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "display_name"

    const-string v3, ""

    invoke-static {v2, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-static {v2}, LX/0uu;->a(LX/0ux;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    sput-object v0, LX/3Lv;->d:LX/0ux;

    .line 552753
    const-string v0, "\\s+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/3Lv;->e:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/01T;LX/2Oq;LX/3Lw;LX/3Lo;LX/3Lx;LX/1Ml;LX/3ME;LX/3MH;LX/2Or;LX/2Uq;LX/3MQ;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 552730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 552731
    iput-object p1, p0, LX/3Lv;->f:Landroid/content/Context;

    .line 552732
    iput-object p2, p0, LX/3Lv;->g:LX/01T;

    .line 552733
    iput-object p3, p0, LX/3Lv;->h:LX/2Oq;

    .line 552734
    iput-object p4, p0, LX/3Lv;->i:LX/3Lw;

    .line 552735
    iput-object p5, p0, LX/3Lv;->j:LX/3Lo;

    .line 552736
    iput-object p6, p0, LX/3Lv;->k:LX/3Lx;

    .line 552737
    iput-object p7, p0, LX/3Lv;->l:LX/1Ml;

    .line 552738
    iput-object p8, p0, LX/3Lv;->m:LX/3ME;

    .line 552739
    iput-object p9, p0, LX/3Lv;->n:LX/3MH;

    .line 552740
    iput-object p10, p0, LX/3Lv;->o:LX/2Or;

    .line 552741
    iput-object p11, p0, LX/3Lv;->p:LX/2Uq;

    .line 552742
    iput-object p12, p0, LX/3Lv;->q:LX/3MQ;

    .line 552743
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 552744
    const-string v1, "android.permission.READ_CONTACTS"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 552745
    iget-object v1, p0, LX/3Lv;->i:LX/3Lw;

    invoke-virtual {v1}, LX/3Lw;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 552746
    const-string v1, "android.permission.READ_SMS"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 552747
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, LX/3Lv;->r:[Ljava/lang/String;

    .line 552748
    return-void
.end method

.method private static a(LX/3Lv;Ljava/lang/String;LX/0XI;)LX/0XI;
    .locals 3

    .prologue
    .line 552725
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3Lv;->p:LX/2Uq;

    invoke-virtual {v0}, LX/2Uq;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 552726
    :cond_0
    :goto_0
    return-object p2

    .line 552727
    :cond_1
    iget-object v0, p0, LX/3Lv;->q:LX/3MQ;

    invoke-virtual {v0, p1}, LX/3MQ;->a(Ljava/lang/String;)LX/6lI;

    move-result-object v0

    .line 552728
    if-eqz v0, :cond_0

    iget v1, v0, LX/6lI;->b:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 552729
    invoke-static {v0, p2}, LX/6lI;->a(LX/6lI;LX/0XI;)LX/0XI;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase;)LX/0XI;
    .locals 3

    .prologue
    .line 552715
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    invoke-virtual {v0, p1, p3}, LX/0XI;->b(Ljava/lang/String;Ljava/lang/String;)LX/0XI;

    move-result-object v0

    .line 552716
    iput-object p2, v0, LX/0XI;->h:Ljava/lang/String;

    .line 552717
    move-object v0, v0

    .line 552718
    invoke-static {p0, p2, p5}, LX/3Lv;->a(LX/3Lv;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)Ljava/lang/String;

    move-result-object v1

    .line 552719
    if-eqz v1, :cond_0

    .line 552720
    iput-object v1, v0, LX/0XI;->s:Ljava/lang/String;

    .line 552721
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 552722
    new-instance v2, Lcom/facebook/user/model/UserEmailAddress;

    invoke-direct {v2, p3, p4}, Lcom/facebook/user/model/UserEmailAddress;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 552723
    iput-object v1, v0, LX/0XI;->c:Ljava/util/List;

    .line 552724
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase;)LX/0XI;
    .locals 3

    .prologue
    .line 552703
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    invoke-virtual {v0, p1, p4}, LX/0XI;->a(Ljava/lang/String;Ljava/lang/String;)LX/0XI;

    move-result-object v0

    .line 552704
    iput-object p2, v0, LX/0XI;->h:Ljava/lang/String;

    .line 552705
    move-object v0, v0

    .line 552706
    invoke-static {p0, p4, v0}, LX/3Lv;->a(LX/3Lv;Ljava/lang/String;LX/0XI;)LX/0XI;

    .line 552707
    iget-object v1, v0, LX/0XI;->h:Ljava/lang/String;

    move-object v1, v1

    .line 552708
    invoke-static {p0, v1, p6}, LX/3Lv;->a(LX/3Lv;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)Ljava/lang/String;

    move-result-object v1

    .line 552709
    if-eqz v1, :cond_0

    .line 552710
    iput-object v1, v0, LX/0XI;->s:Ljava/lang/String;

    .line 552711
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 552712
    new-instance v2, Lcom/facebook/user/model/UserPhoneNumber;

    invoke-direct {v2, p4, p3, p4, p5}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 552713
    iput-object v1, v0, LX/0XI;->d:Ljava/util/List;

    .line 552714
    return-object v0
.end method

.method public static a(LX/0QB;)LX/3Lv;
    .locals 1

    .prologue
    .line 552431
    invoke-static {p0}, LX/3Lv;->b(LX/0QB;)LX/3Lv;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/3Lv;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 552693
    invoke-static {p1}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 552694
    const-string v0, ""

    .line 552695
    :goto_0
    return-object v0

    .line 552696
    :cond_0
    new-instance v0, LX/3hD;

    invoke-direct {v0}, LX/3hD;-><init>()V

    .line 552697
    iput-object p1, v0, LX/3hD;->b:Ljava/lang/String;

    .line 552698
    move-object v0, v0

    .line 552699
    iput-object p1, v0, LX/3hD;->a:Ljava/lang/String;

    .line 552700
    move-object v0, v0

    .line 552701
    invoke-virtual {v0}, LX/3hD;->a()LX/3hE;

    move-result-object v0

    .line 552702
    iget-object v1, p0, LX/3Lv;->j:LX/3Lo;

    invoke-virtual {v1, p2, v0}, LX/3Lo;->a(Landroid/database/sqlite/SQLiteDatabase;LX/3hE;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/3Lv;Landroid/database/Cursor;Landroid/database/sqlite/SQLiteDatabase;IZ)Ljava/util/List;
    .locals 12
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "IZ)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 552669
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 552670
    const-string v0, "contact_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 552671
    const-string v0, "display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 552672
    const-string v0, "data1"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 552673
    const-string v0, "data2"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    .line 552674
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 552675
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, p3, :cond_5

    .line 552676
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 552677
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 552678
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 552679
    invoke-interface {v11, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 552680
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_1

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 552681
    :cond_1
    if-nez v0, :cond_2

    .line 552682
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 552683
    invoke-interface {v11, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552684
    :cond_2
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 552685
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object v0, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/3Lv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase;)LX/0XI;

    move-result-object v1

    .line 552686
    if-eqz p4, :cond_3

    .line 552687
    iget-object v0, p0, LX/3Lv;->o:LX/2Or;

    invoke-virtual {v0}, LX/2Or;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/3Lv;->n:LX/3MH;

    invoke-virtual {v0, v3}, LX/3MH;->c(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v0, v2

    .line 552688
    :goto_1
    invoke-virtual {v1}, LX/0XI;->t()F

    move-result v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_3

    .line 552689
    invoke-virtual {v1, v0}, LX/0XI;->a(F)LX/0XI;

    .line 552690
    :cond_3
    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 552691
    :cond_4
    iget-object v0, p0, LX/3Lv;->m:LX/3ME;

    invoke-virtual {v0, v3}, LX/3ME;->c(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v0, v2

    goto :goto_1

    .line 552692
    :cond_5
    return-object v6
.end method

.method private a(Landroid/net/Uri;LX/0ux;ILjava/lang/String;ZLX/3Oo;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "LX/0ux;",
            "I",
            "Ljava/lang/String;",
            "Z",
            "LX/3Oo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 552653
    invoke-static {p0}, LX/3Lv;->a(LX/3Lv;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 552654
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 552655
    :try_start_0
    iget-object v0, p0, LX/3Lv;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, LX/3Lv;->a:[Ljava/lang/String;

    invoke-virtual {p2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 552656
    if-eqz v1, :cond_5

    .line 552657
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Landroid/database/sqlite/SQLiteDatabase;->create(Landroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 552658
    invoke-static {p0, v1, v6, p3, p5}, LX/3Lv;->a(LX/3Lv;Landroid/database/Cursor;Landroid/database/sqlite/SQLiteDatabase;IZ)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 552659
    :goto_0
    if-eqz v1, :cond_0

    .line 552660
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 552661
    :cond_0
    if-eqz v6, :cond_1

    .line 552662
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 552663
    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_2
    return-object v0

    .line 552664
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_3

    .line 552665
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 552666
    :cond_3
    if-eqz v6, :cond_4

    .line 552667
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_4
    throw v0

    .line 552668
    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_5
    move-object v0, v6

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 552629
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 552630
    iget-object v0, p0, LX/3Lv;->o:LX/2Or;

    invoke-virtual {v0}, LX/2Or;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Lv;->n:LX/3MH;

    .line 552631
    invoke-static {v0, p1}, LX/3MH;->d(LX/3MH;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    move-object v0, v1

    .line 552632
    move-object v1, v0

    .line 552633
    :goto_0
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 552634
    new-instance v4, Lcom/facebook/user/model/UserEmailAddress;

    const/4 v5, 0x3

    invoke-direct {v4, v0, v5}, Lcom/facebook/user/model/UserEmailAddress;-><init>(Ljava/lang/String;I)V

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 552635
    new-instance v5, LX/0XI;

    invoke-direct {v5}, LX/0XI;-><init>()V

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v0}, LX/0XI;->b(Ljava/lang/String;Ljava/lang/String;)LX/0XI;

    move-result-object v5

    .line 552636
    iput-object v0, v5, LX/0XI;->h:Ljava/lang/String;

    .line 552637
    move-object v5, v5

    .line 552638
    iput-object v4, v5, LX/0XI;->c:Ljava/util/List;

    .line 552639
    move-object v4, v5

    .line 552640
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v0

    .line 552641
    iput v0, v4, LX/0XI;->t:F

    .line 552642
    move-object v0, v4

    .line 552643
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 552644
    :cond_0
    iget-object v0, p0, LX/3Lv;->m:LX/3ME;

    .line 552645
    invoke-static {v0}, LX/3ME;->a(LX/3ME;)V

    .line 552646
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 552647
    iget-object v1, v0, LX/3ME;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 552648
    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 552649
    iget-object v5, v0, LX/3ME;->f:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 552650
    :cond_2
    move-object v0, v3

    .line 552651
    move-object v1, v0

    goto :goto_0

    .line 552652
    :cond_3
    return-object v2
.end method

.method private a(Ljava/lang/String;Ljava/util/Set;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 552401
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 552402
    iget-object v0, p0, LX/3Lv;->o:LX/2Or;

    invoke-virtual {v0}, LX/2Or;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3Lv;->n:LX/3MH;

    .line 552403
    invoke-static {p1}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/3MH;->d(LX/3MH;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    move-object v0, v1

    .line 552404
    move-object v1, v0

    .line 552405
    :goto_0
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 552406
    iget-object v4, p0, LX/3Lv;->k:LX/3Lx;

    invoke-virtual {v4, v0}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 552407
    iget-object v5, p0, LX/3Lv;->k:LX/3Lx;

    invoke-virtual {v5, v0}, LX/3Lx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 552408
    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-interface {p2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-interface {p2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 552409
    new-instance v6, Lcom/facebook/user/model/UserPhoneNumber;

    const/4 v7, 0x7

    invoke-direct {v6, v5, v0, v4, v7}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    .line 552410
    new-instance v7, LX/0XI;

    invoke-direct {v7}, LX/0XI;-><init>()V

    const/4 v8, 0x0

    invoke-virtual {v7, v8, v4}, LX/0XI;->a(Ljava/lang/String;Ljava/lang/String;)LX/0XI;

    move-result-object v7

    .line 552411
    iput-object v5, v7, LX/0XI;->h:Ljava/lang/String;

    .line 552412
    move-object v5, v7

    .line 552413
    iput-object v6, v5, LX/0XI;->d:Ljava/util/List;

    .line 552414
    move-object v5, v5

    .line 552415
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v0

    .line 552416
    iput v0, v5, LX/0XI;->t:F

    .line 552417
    move-object v0, v5

    .line 552418
    invoke-static {p0, v4, v0}, LX/3Lv;->a(LX/3Lv;Ljava/lang/String;LX/0XI;)LX/0XI;

    .line 552419
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 552420
    :cond_1
    iget-object v0, p0, LX/3Lv;->m:LX/3ME;

    .line 552421
    invoke-static {v0}, LX/3ME;->a(LX/3ME;)V

    .line 552422
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 552423
    invoke-static {p1}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 552424
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 552425
    iget-object v1, v0, LX/3ME;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 552426
    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 552427
    iget-object v6, v0, LX/3ME;->e:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v3, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 552428
    :cond_3
    move-object v0, v3

    .line 552429
    move-object v1, v0

    goto/16 :goto_0

    .line 552430
    :cond_4
    return-object v2
.end method

.method private static a(Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 552432
    invoke-static {p0}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552433
    :goto_0
    return-void

    .line 552434
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 552435
    iget-object v2, v0, Lcom/facebook/user/model/User;->c:LX/0Px;

    move-object v0, v2

    .line 552436
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserEmailAddress;

    .line 552437
    iget-object v3, v0, Lcom/facebook/user/model/UserEmailAddress;->a:Ljava/lang/String;

    move-object v0, v3

    .line 552438
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 552439
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 552440
    new-instance v1, Lcom/facebook/user/model/UserEmailAddress;

    const/4 v2, 0x3

    invoke-direct {v1, p0, v2}, Lcom/facebook/user/model/UserEmailAddress;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 552441
    new-instance v1, LX/0XI;

    invoke-direct {v1}, LX/0XI;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p0}, LX/0XI;->b(Ljava/lang/String;Ljava/lang/String;)LX/0XI;

    move-result-object v1

    .line 552442
    iput-object p0, v1, LX/0XI;->h:Ljava/lang/String;

    .line 552443
    move-object v1, v1

    .line 552444
    iput-object v0, v1, LX/0XI;->c:Ljava/util/List;

    .line 552445
    move-object v0, v1

    .line 552446
    const/high16 v1, 0x42c60000    # 99.0f

    .line 552447
    iput v1, v0, LX/0XI;->t:F

    .line 552448
    move-object v0, v0

    .line 552449
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(LX/3Lv;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 552450
    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    iget-object v2, p0, LX/3Lv;->g:LX/01T;

    invoke-virtual {v1, v2}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 552451
    :cond_0
    :goto_0
    return v0

    .line 552452
    :cond_1
    iget-object v1, p0, LX/3Lv;->l:LX/1Ml;

    iget-object v2, p0, LX/3Lv;->r:[Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 552453
    iget-object v1, p0, LX/3Lv;->h:LX/2Oq;

    invoke-virtual {v1}, LX/2Oq;->a()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/3Lv;->i:LX/3Lw;

    invoke-virtual {v1}, LX/3Lw;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/3Lv;
    .locals 13

    .prologue
    .line 552454
    new-instance v0, LX/3Lv;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v2

    check-cast v2, LX/01T;

    invoke-static {p0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v3

    check-cast v3, LX/2Oq;

    invoke-static {p0}, LX/3Lw;->b(LX/0QB;)LX/3Lw;

    move-result-object v4

    check-cast v4, LX/3Lw;

    invoke-static {p0}, LX/3Lo;->a(LX/0QB;)LX/3Lo;

    move-result-object v5

    check-cast v5, LX/3Lo;

    invoke-static {p0}, LX/3Lx;->b(LX/0QB;)LX/3Lx;

    move-result-object v6

    check-cast v6, LX/3Lx;

    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v7

    check-cast v7, LX/1Ml;

    invoke-static {p0}, LX/3ME;->a(LX/0QB;)LX/3ME;

    move-result-object v8

    check-cast v8, LX/3ME;

    invoke-static {p0}, LX/3MH;->a(LX/0QB;)LX/3MH;

    move-result-object v9

    check-cast v9, LX/3MH;

    invoke-static {p0}, LX/2Or;->b(LX/0QB;)LX/2Or;

    move-result-object v10

    check-cast v10, LX/2Or;

    invoke-static {p0}, LX/2Uq;->a(LX/0QB;)LX/2Uq;

    move-result-object v11

    check-cast v11, LX/2Uq;

    invoke-static {p0}, LX/3MQ;->b(LX/0QB;)LX/3MQ;

    move-result-object v12

    check-cast v12, LX/3MQ;

    invoke-direct/range {v0 .. v12}, LX/3Lv;-><init>(Landroid/content/Context;LX/01T;LX/2Oq;LX/3Lw;LX/3Lo;LX/3Lx;LX/1Ml;LX/3ME;LX/3MH;LX/2Or;LX/2Uq;LX/3MQ;)V

    .line 552455
    return-object v0
.end method

.method public static b(LX/3Lv;Landroid/net/Uri;LX/0ux;ILjava/lang/String;ZLX/3Oo;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "LX/0ux;",
            "I",
            "Ljava/lang/String;",
            "Z",
            "LX/3Oo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 552456
    invoke-static {p0}, LX/3Lv;->a(LX/3Lv;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 552457
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 552458
    :try_start_0
    iget-object v0, p0, LX/3Lv;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, LX/3Lv;->c:[Ljava/lang/String;

    invoke-virtual {p2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 552459
    if-eqz v1, :cond_5

    .line 552460
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Landroid/database/sqlite/SQLiteDatabase;->create(Landroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 552461
    invoke-direct {p0, v1, v6, p3, p5}, LX/3Lv;->b(Landroid/database/Cursor;Landroid/database/sqlite/SQLiteDatabase;IZ)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 552462
    :goto_0
    if-eqz v1, :cond_0

    .line 552463
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 552464
    :cond_0
    if-eqz v6, :cond_1

    .line 552465
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 552466
    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_2
    return-object v0

    .line 552467
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_3

    .line 552468
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 552469
    :cond_3
    if-eqz v6, :cond_4

    .line 552470
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_4
    throw v0

    .line 552471
    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_5
    move-object v0, v6

    goto :goto_0
.end method

.method private b(Landroid/database/Cursor;Landroid/database/sqlite/SQLiteDatabase;IZ)Ljava/util/List;
    .locals 18
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "IZ)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 552472
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 552473
    const-string v2, "contact_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    .line 552474
    const-string v2, "data4"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 552475
    const-string v2, "data1"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 552476
    const-string v2, "display_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    .line 552477
    const-string v2, "data2"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 552478
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    .line 552479
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, p3

    if-gt v2, v0, :cond_6

    .line 552480
    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 552481
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 552482
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 552483
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 552484
    const-string v2, "*"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "#"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ";"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ","

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 552485
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 552486
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lv;->k:LX/3Lx;

    invoke-virtual {v2, v5}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 552487
    :cond_1
    invoke-static {v5}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 552488
    invoke-interface {v15, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    .line 552489
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    if-eqz v2, :cond_2

    invoke-interface {v2, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 552490
    :cond_2
    if-nez v2, :cond_3

    .line 552491
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 552492
    invoke-interface {v15, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552493
    :cond_3
    invoke-interface {v2, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 552494
    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 552495
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    move-object/from16 v2, p0

    move-object/from16 v8, p2

    invoke-direct/range {v2 .. v8}, LX/3Lv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase;)LX/0XI;

    move-result-object v3

    .line 552496
    if-eqz p4, :cond_4

    .line 552497
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lv;->o:LX/2Or;

    invoke-virtual {v2}, LX/2Or;->o()Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lv;->n:LX/3MH;

    invoke-virtual {v2, v5, v6}, LX/3MH;->a(Ljava/lang/String;Ljava/lang/String;)D

    move-result-wide v4

    double-to-float v2, v4

    .line 552498
    :goto_1
    invoke-virtual {v3}, LX/0XI;->t()F

    move-result v4

    cmpl-float v4, v2, v4

    if-lez v4, :cond_4

    .line 552499
    invoke-virtual {v3, v2}, LX/0XI;->a(F)LX/0XI;

    .line 552500
    :cond_4
    invoke-virtual {v3}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 552501
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Lv;->m:LX/3ME;

    invoke-virtual {v2, v5, v6}, LX/3ME;->a(Ljava/lang/String;Ljava/lang/String;)D

    move-result-wide v4

    double-to-float v2, v4

    goto :goto_1

    .line 552502
    :cond_6
    return-object v9
.end method

.method private b(Ljava/lang/String;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 552503
    invoke-static {p1}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 552504
    :cond_0
    :goto_0
    return-void

    .line 552505
    :cond_1
    iget-object v0, p0, LX/3Lv;->k:LX/3Lx;

    invoke-virtual {v0, p1}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 552506
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552507
    iget-object v0, p0, LX/3Lv;->k:LX/3Lx;

    invoke-virtual {v0, v1}, LX/3Lx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 552508
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 552509
    iget-object v4, p0, LX/3Lv;->k:LX/3Lx;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v0

    .line 552510
    iget-object v5, v0, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    move-object v0, v5

    .line 552511
    invoke-virtual {v4, v0}, LX/3Lx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 552512
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 552513
    new-instance v3, Lcom/facebook/user/model/UserPhoneNumber;

    const/4 v4, 0x7

    invoke-direct {v3, v2, p1, v1, v4}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 552514
    new-instance v3, LX/0XI;

    invoke-direct {v3}, LX/0XI;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v1}, LX/0XI;->a(Ljava/lang/String;Ljava/lang/String;)LX/0XI;

    move-result-object v3

    .line 552515
    iput-object v2, v3, LX/0XI;->h:Ljava/lang/String;

    .line 552516
    move-object v2, v3

    .line 552517
    iput-object v0, v2, LX/0XI;->d:Ljava/util/List;

    .line 552518
    move-object v0, v2

    .line 552519
    const/high16 v2, 0x42c60000    # 99.0f

    .line 552520
    iput v2, v0, LX/0XI;->t:F

    .line 552521
    move-object v0, v0

    .line 552522
    invoke-static {p0, v1, v0}, LX/3Lv;->a(LX/3Lv;Ljava/lang/String;LX/0XI;)LX/0XI;

    .line 552523
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(ILX/3Oo;)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/3Oo;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 552524
    invoke-static {p0}, LX/3Lv;->a(LX/3Lv;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552525
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 552526
    :goto_0
    return-object v0

    .line 552527
    :cond_0
    iget-object v0, p0, LX/3Lv;->o:LX/2Or;

    invoke-virtual {v0}, LX/2Or;->o()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/3Lv;->n:LX/3MH;

    .line 552528
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 552529
    iget-object v8, v0, LX/3MH;->d:LX/3MI;

    invoke-virtual {v8}, LX/3MI;->a()Ljava/util/List;

    move-result-object v8

    .line 552530
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/6jO;

    .line 552531
    iget-object v11, v0, LX/3MH;->a:Ljava/util/Map;

    iget-object v12, v8, LX/6jO;->a:Ljava/lang/String;

    invoke-interface {v11, v12, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552532
    iget-object v11, v8, LX/6jO;->a:Ljava/lang/String;

    invoke-static {v11}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 552533
    iget-object v11, v8, LX/6jO;->a:Ljava/lang/String;

    invoke-static {v11}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 552534
    invoke-static {v11}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 552535
    iget-object v12, v0, LX/3MH;->a:Ljava/util/Map;

    invoke-interface {v12, v11, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552536
    :cond_2
    iget-object v11, v8, LX/6jO;->a:Ljava/lang/String;

    invoke-static {v11}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_3

    iget-object v11, v8, LX/6jO;->a:Ljava/lang/String;

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 552537
    :cond_3
    iget-object v11, v8, LX/6jO;->a:Ljava/lang/String;

    invoke-static {v11}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 552538
    iget-object v8, v8, LX/6jO;->a:Ljava/lang/String;

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 552539
    :cond_4
    iget-object v11, v8, LX/6jO;->a:Ljava/lang/String;

    invoke-static {v11}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 552540
    invoke-static {v11}, LX/6jR;->a(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 552541
    iget-object v8, v8, LX/6jO;->a:Ljava/lang/String;

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 552542
    :cond_5
    iget-object v8, v0, LX/3MH;->c:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v10

    iput-wide v10, v0, LX/3MH;->b:J

    .line 552543
    const/4 v8, 0x0

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    invoke-static {p1, v10}, Ljava/lang/Math;->min(II)I

    move-result v10

    invoke-interface {v9, v8, v10}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v8

    move-object v0, v8

    .line 552544
    move-object v2, v0

    .line 552545
    :goto_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 552546
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 552547
    iget-object v4, p0, LX/3Lv;->k:LX/3Lx;

    invoke-virtual {v4, v0}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 552548
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 552549
    :cond_6
    iget-object v0, p0, LX/3Lv;->m:LX/3ME;

    invoke-virtual {v0, p1}, LX/3ME;->a(I)Ljava/util/List;

    move-result-object v0

    move-object v2, v0

    goto :goto_2

    .line 552550
    :cond_7
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 552551
    new-array v0, v6, [LX/0ux;

    sget-object v4, LX/3Lv;->d:LX/0ux;

    aput-object v4, v0, v7

    new-array v4, v6, [LX/0ux;

    const-string v6, "data1"

    invoke-static {v6, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v2

    aput-object v2, v4, v7

    const-string v2, "data4"

    invoke-static {v2, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v4}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v2

    aput-object v2, v0, v5

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v2

    .line 552552
    const/16 v3, 0x7d0

    const-string v4, "contact_id"

    move-object v0, p0

    move-object v6, p2

    invoke-static/range {v0 .. v6}, LX/3Lv;->b(LX/3Lv;Landroid/net/Uri;LX/0ux;ILjava/lang/String;ZLX/3Oo;)Ljava/util/List;

    move-result-object v0

    .line 552553
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, p1, :cond_8

    .line 552554
    new-instance v1, LX/6jH;

    invoke-direct {v1, p0}, LX/6jH;-><init>(LX/3Lv;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 552555
    invoke-interface {v0, v7, p1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 552556
    :cond_8
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;IZZZLX/3Oo;)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZZZ",
            "LX/3Oo;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 552557
    invoke-static {p0}, LX/3Lv;->a(LX/3Lv;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552558
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v0

    .line 552559
    :goto_0
    return-object v0

    .line 552560
    :cond_0
    if-eqz p4, :cond_6

    .line 552561
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 552562
    invoke-static {p1}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 552563
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_4

    .line 552564
    sget-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "search_display_name"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 552565
    sget-object v2, LX/3Lv;->d:LX/0ux;

    .line 552566
    const-string v4, "contact_id"

    const/4 v5, 0x1

    move-object v0, p0

    move v3, p2

    move-object/from16 v6, p6

    invoke-static/range {v0 .. v6}, LX/3Lv;->b(LX/3Lv;Landroid/net/Uri;LX/0ux;ILjava/lang/String;ZLX/3Oo;)Ljava/util/List;

    move-result-object v0

    .line 552567
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 552568
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 552569
    :cond_1
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 552570
    const/4 v0, 0x2

    new-array v0, v0, [LX/0ux;

    const/4 v2, 0x0

    sget-object v3, LX/3Lv;->d:LX/0ux;

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "data4"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "%"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v2

    .line 552571
    const-string v4, "contact_id"

    const/4 v5, 0x1

    move-object v0, p0

    move v3, p2

    move-object/from16 v6, p6

    invoke-static/range {v0 .. v6}, LX/3Lv;->b(LX/3Lv;Landroid/net/Uri;LX/0ux;ILjava/lang/String;ZLX/3Oo;)Ljava/util/List;

    move-result-object v0

    .line 552572
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 552573
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 552574
    :cond_2
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 552575
    invoke-virtual {v7}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 552576
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/user/model/UserPhoneNumber;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 552577
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/user/model/UserPhoneNumber;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 552578
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/user/model/UserPhoneNumber;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 552579
    :cond_3
    invoke-direct {p0, v8, v1}, LX/3Lv;->a(Ljava/lang/String;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    .line 552580
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 552581
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 552582
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 552583
    :goto_5
    if-eqz p3, :cond_5

    .line 552584
    invoke-direct {p0, p1, v0}, LX/3Lv;->b(Ljava/lang/String;Ljava/util/List;)V

    .line 552585
    invoke-static {p1, v0}, LX/3Lv;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 552586
    :cond_5
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 552587
    :cond_6
    if-eqz p5, :cond_9

    .line 552588
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 552589
    sget-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 552590
    sget-object v2, LX/3Lv;->b:LX/0ux;

    .line 552591
    const-string v4, "contact_id"

    const/4 v5, 0x1

    move-object v0, p0

    move v3, p2

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v6}, LX/3Lv;->a(Landroid/net/Uri;LX/0ux;ILjava/lang/String;ZLX/3Oo;)Ljava/util/List;

    move-result-object v0

    .line 552592
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 552593
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 552594
    :cond_7
    invoke-direct {p0, p1}, LX/3Lv;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 552595
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 552596
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    .line 552597
    :cond_8
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_5

    .line 552598
    :cond_9
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 552599
    iget-object v0, p0, LX/3Lv;->p:LX/2Uq;

    invoke-virtual {v0}, LX/2Uq;->l()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 552600
    iget-object v0, p0, LX/3Lv;->q:LX/3MQ;

    invoke-virtual {v0, p1}, LX/3MQ;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 552601
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 552602
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6lI;

    .line 552603
    iget-object v0, v0, LX/6lI;->a:Ljava/lang/String;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 552604
    :cond_a
    const/4 v0, 0x2

    new-array v0, v0, [LX/0ux;

    const/4 v1, 0x0

    sget-object v2, LX/3Lv;->d:LX/0ux;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "data1"

    invoke-static {v2, v9}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v2

    .line 552605
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "contact_id"

    const/4 v5, 0x1

    move-object v0, p0

    move v3, p2

    move-object/from16 v6, p6

    invoke-static/range {v0 .. v6}, LX/3Lv;->b(LX/3Lv;Landroid/net/Uri;LX/0ux;ILjava/lang/String;ZLX/3Oo;)Ljava/util/List;

    move-result-object v0

    .line 552606
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 552607
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/user/model/UserPhoneNumber;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 552608
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_9

    .line 552609
    :cond_b
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6lI;

    .line 552610
    iget-object v2, v0, LX/6lI;->a:Ljava/lang/String;

    invoke-interface {v9, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 552611
    iget-object v2, p0, LX/3Lv;->k:LX/3Lx;

    iget-object v3, v0, LX/6lI;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/3Lx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 552612
    new-instance v3, Lcom/facebook/user/model/UserPhoneNumber;

    iget-object v4, v0, LX/6lI;->a:Ljava/lang/String;

    iget-object v5, v0, LX/6lI;->a:Ljava/lang/String;

    const/4 v6, 0x7

    invoke-direct {v3, v2, v4, v5, v6}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 552613
    new-instance v3, LX/0XI;

    invoke-direct {v3}, LX/0XI;-><init>()V

    const/4 v4, 0x0

    iget-object v5, v0, LX/6lI;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0XI;->a(Ljava/lang/String;Ljava/lang/String;)LX/0XI;

    move-result-object v3

    iget-object v4, v0, LX/6lI;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/0XI;->b(Ljava/lang/String;)LX/0XI;

    move-result-object v3

    invoke-static {v0}, LX/6lI;->a(LX/6lI;)Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0XI;->c(Lcom/facebook/user/model/User;)LX/0XI;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0XI;->b(Ljava/util/List;)LX/0XI;

    move-result-object v0

    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 552614
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    .line 552615
    :cond_d
    iget-object v0, p0, LX/3Lv;->o:LX/2Or;

    invoke-virtual {v0}, LX/2Or;->k()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 552616
    sget-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "search_phone_number"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 552617
    sget-object v2, LX/3Lv;->d:LX/0ux;

    const-string v4, "contact_id"

    const/4 v5, 0x1

    move-object v0, p0

    move v3, p2

    move-object/from16 v6, p6

    invoke-static/range {v0 .. v6}, LX/3Lv;->b(LX/3Lv;Landroid/net/Uri;LX/0ux;ILjava/lang/String;ZLX/3Oo;)Ljava/util/List;

    move-result-object v0

    .line 552618
    :goto_b
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 552619
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_c

    .line 552620
    :cond_e
    sget-object v0, LX/3Lv;->e:Ljava/util/regex/Pattern;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;I)[Ljava/lang/String;

    move-result-object v1

    .line 552621
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v2

    .line 552622
    array-length v3, v1

    const/4 v0, 0x0

    :goto_d
    if-ge v0, v3, :cond_f

    aget-object v4, v1, v0

    .line 552623
    const/4 v5, 0x2

    new-array v5, v5, [LX/0ux;

    const/4 v6, 0x0

    const-string v8, "display_name"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v6, 0x1

    const-string v8, "display_name"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "% "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, "%"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v4}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    aput-object v4, v5, v6

    invoke-static {v5}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 552624
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 552625
    :cond_f
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 552626
    const/4 v0, 0x2

    new-array v0, v0, [LX/0ux;

    const/4 v3, 0x0

    sget-object v4, LX/3Lv;->d:LX/0ux;

    aput-object v4, v0, v3

    const/4 v3, 0x1

    aput-object v2, v0, v3

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v2

    .line 552627
    const-string v4, "contact_id"

    const/4 v5, 0x1

    move-object v0, p0

    move v3, p2

    move-object/from16 v6, p6

    invoke-static/range {v0 .. v6}, LX/3Lv;->b(LX/3Lv;Landroid/net/Uri;LX/0ux;ILjava/lang/String;ZLX/3Oo;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_b

    .line 552628
    :cond_10
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto/16 :goto_5
.end method
