.class public final LX/39t;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/39c;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/20X;

.field public c:LX/1Wk;

.field public d:Z

.field public e:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public f:I

.field public g:I

.field public h:I

.field public i:F

.field public j:F

.field public k:F

.field public final synthetic l:LX/39c;


# direct methods
.method public constructor <init>(LX/39c;)V
    .locals 2

    .prologue
    const v1, -0x6e685d

    .line 524015
    iput-object p1, p0, LX/39t;->l:LX/39c;

    .line 524016
    move-object v0, p1

    .line 524017
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 524018
    iput v1, p0, LX/39t;->f:I

    .line 524019
    iput v1, p0, LX/39t;->g:I

    .line 524020
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 524021
    const-string v0, "BasicFooterButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 524022
    if-ne p0, p1, :cond_1

    .line 524023
    :cond_0
    :goto_0
    return v0

    .line 524024
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 524025
    goto :goto_0

    .line 524026
    :cond_3
    check-cast p1, LX/39t;

    .line 524027
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 524028
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 524029
    if-eq v2, v3, :cond_0

    .line 524030
    iget-object v2, p0, LX/39t;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/39t;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/39t;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 524031
    goto :goto_0

    .line 524032
    :cond_5
    iget-object v2, p1, LX/39t;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 524033
    :cond_6
    iget-object v2, p0, LX/39t;->b:LX/20X;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/39t;->b:LX/20X;

    iget-object v3, p1, LX/39t;->b:LX/20X;

    invoke-virtual {v2, v3}, LX/20X;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 524034
    goto :goto_0

    .line 524035
    :cond_8
    iget-object v2, p1, LX/39t;->b:LX/20X;

    if-nez v2, :cond_7

    .line 524036
    :cond_9
    iget-object v2, p0, LX/39t;->c:LX/1Wk;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/39t;->c:LX/1Wk;

    iget-object v3, p1, LX/39t;->c:LX/1Wk;

    invoke-virtual {v2, v3}, LX/1Wk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 524037
    goto :goto_0

    .line 524038
    :cond_b
    iget-object v2, p1, LX/39t;->c:LX/1Wk;

    if-nez v2, :cond_a

    .line 524039
    :cond_c
    iget-boolean v2, p0, LX/39t;->d:Z

    iget-boolean v3, p1, LX/39t;->d:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 524040
    goto :goto_0

    .line 524041
    :cond_d
    iget-object v2, p0, LX/39t;->e:LX/1Pr;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/39t;->e:LX/1Pr;

    iget-object v3, p1, LX/39t;->e:LX/1Pr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 524042
    goto :goto_0

    .line 524043
    :cond_f
    iget-object v2, p1, LX/39t;->e:LX/1Pr;

    if-nez v2, :cond_e

    .line 524044
    :cond_10
    iget v2, p0, LX/39t;->f:I

    iget v3, p1, LX/39t;->f:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 524045
    goto :goto_0

    .line 524046
    :cond_11
    iget v2, p0, LX/39t;->g:I

    iget v3, p1, LX/39t;->g:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 524047
    goto :goto_0

    .line 524048
    :cond_12
    iget v2, p0, LX/39t;->h:I

    iget v3, p1, LX/39t;->h:I

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 524049
    goto/16 :goto_0

    .line 524050
    :cond_13
    iget v2, p0, LX/39t;->i:F

    iget v3, p1, LX/39t;->i:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_14

    move v0, v1

    .line 524051
    goto/16 :goto_0

    .line 524052
    :cond_14
    iget v2, p0, LX/39t;->j:F

    iget v3, p1, LX/39t;->j:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_15

    move v0, v1

    .line 524053
    goto/16 :goto_0

    .line 524054
    :cond_15
    iget v2, p0, LX/39t;->k:F

    iget v3, p1, LX/39t;->k:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 524055
    goto/16 :goto_0
.end method
