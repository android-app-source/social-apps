.class public LX/2Fi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/01T;

.field private final b:LX/0SI;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/ItV;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/2aZ;


# direct methods
.method public constructor <init>(LX/01T;LX/0SI;LX/0Or;LX/2aZ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/01T;",
            "LX/0SI;",
            "LX/0Or",
            "<",
            "LX/ItV;",
            ">;",
            "LX/2aZ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387206
    iput-object p1, p0, LX/2Fi;->a:LX/01T;

    .line 387207
    iput-object p2, p0, LX/2Fi;->b:LX/0SI;

    .line 387208
    iput-object p3, p0, LX/2Fi;->c:LX/0Or;

    .line 387209
    iput-object p4, p0, LX/2Fi;->d:LX/2aZ;

    .line 387210
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 6

    .prologue
    .line 387211
    iget-object v0, p0, LX/2Fi;->d:LX/2aZ;

    invoke-virtual {v0}, LX/2aZ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Fi;->a:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_1

    .line 387212
    :cond_0
    :goto_0
    return-void

    .line 387213
    :cond_1
    iget-object v0, p0, LX/2Fi;->b:LX/0SI;

    invoke-interface {v0}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 387214
    if-eqz v0, :cond_0

    .line 387215
    iget-object v0, p0, LX/2Fi;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ItV;

    .line 387216
    iget-object v2, v0, LX/ItV;->i:LX/01T;

    sget-object v3, LX/01T;->PAA:LX/01T;

    if-ne v2, v3, :cond_3

    .line 387217
    :cond_2
    :goto_1
    goto :goto_0

    .line 387218
    :cond_3
    iget-object v2, v0, LX/ItV;->k:LX/ItU;

    sget-object v3, LX/ItU;->NOT_STARTED:LX/ItU;

    if-ne v2, v3, :cond_2

    .line 387219
    sget-object v2, LX/ItU;->IN_PROGRESS:LX/ItU;

    iput-object v2, v0, LX/ItV;->k:LX/ItU;

    .line 387220
    iget-wide v2, v0, LX/ItV;->l:J

    const-wide/32 v4, 0x5265c00

    sub-long/2addr v2, v4

    .line 387221
    iget-object v4, v0, LX/ItV;->d:LX/0TD;

    new-instance v5, LX/ItS;

    invoke-direct {v5, v0, v2, v3}, LX/ItS;-><init>(LX/ItV;J)V

    invoke-interface {v4, v5}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 387222
    new-instance v3, LX/ItT;

    invoke-direct {v3, v0}, LX/ItT;-><init>(LX/ItV;)V

    iget-object v4, v0, LX/ItV;->c:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method
