.class public LX/2R5;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;",
        "Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Result;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409433
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 409434
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 409435
    const-class v0, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;

    .line 409436
    invoke-virtual {v0}, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 409437
    invoke-virtual {v0}, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;->a()J

    move-result-wide v2

    .line 409438
    new-instance v0, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Result;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Result;-><init>(Ljava/lang/String;J)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 409439
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 409440
    check-cast p1, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;

    .line 409441
    new-instance v0, LX/75X;

    invoke-direct {v0}, LX/75X;-><init>()V

    move-object v0, v0

    .line 409442
    new-instance v1, LX/4CU;

    invoke-direct {v1}, LX/4CU;-><init>()V

    iget-object v2, p1, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;->a:Ljava/lang/String;

    .line 409443
    const-string p0, "app_id"

    invoke-virtual {v1, p0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 409444
    move-object v1, v1

    .line 409445
    iget-object v2, p1, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;->b:Ljava/lang/String;

    .line 409446
    const-string p0, "key_hash"

    invoke-virtual {v1, p0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 409447
    move-object v1, v1

    .line 409448
    const-string v2, "params"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 409449
    return-object v0
.end method
