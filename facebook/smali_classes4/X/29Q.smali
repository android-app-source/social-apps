.class public final LX/29Q;
.super LX/1ID;
.source ""


# instance fields
.field private final match:C


# direct methods
.method public constructor <init>(C)V
    .locals 0

    .prologue
    .line 376281
    invoke-direct {p0}, LX/1ID;-><init>()V

    .line 376282
    iput-char p1, p0, LX/29Q;->match:C

    .line 376283
    return-void
.end method


# virtual methods
.method public final matches(C)Z
    .locals 1

    .prologue
    .line 376284
    iget-char v0, p0, LX/29Q;->match:C

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final or(LX/1IA;)LX/1IA;
    .locals 1

    .prologue
    .line 376285
    iget-char v0, p0, LX/29Q;->match:C

    invoke-virtual {p1, v0}, LX/1IA;->matches(C)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-super {p0, p1}, LX/1ID;->or(LX/1IA;)LX/1IA;

    move-result-object p1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 376286
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CharMatcher.is(\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-char v1, p0, LX/29Q;->match:C

    invoke-static {v1}, LX/1IA;->showCharacter(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
