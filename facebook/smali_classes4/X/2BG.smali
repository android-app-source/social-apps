.class public LX/2BG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/2BG;


# instance fields
.field private final a:Ljava/util/concurrent/ScheduledExecutorService;

.field private final b:LX/296;

.field public final c:LX/290;

.field private final d:LX/0ad;

.field public final e:LX/1qk;

.field private final f:Ljava/lang/Runnable;

.field public g:Ljava/util/concurrent/Future;


# direct methods
.method public constructor <init>(LX/1r1;LX/0ad;Ljava/util/concurrent/ScheduledExecutorService;LX/296;LX/290;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 378955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 378956
    new-instance v0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeupHelper$1;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeupHelper$1;-><init>(LX/2BG;)V

    iput-object v0, p0, LX/2BG;->f:Ljava/lang/Runnable;

    .line 378957
    new-instance v0, LX/292;

    const-string v1, "ZERO_PAYLOAD"

    invoke-direct {v0, p0, p1, v1}, LX/292;-><init>(LX/2BG;LX/1r1;Ljava/lang/String;)V

    iput-object v0, p0, LX/2BG;->e:LX/1qk;

    .line 378958
    iget-object v0, p0, LX/2BG;->e:LX/1qk;

    iget-object v0, v0, LX/1qk;->a:LX/1ql;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1ql;->a(Z)V

    .line 378959
    iput-object p2, p0, LX/2BG;->d:LX/0ad;

    .line 378960
    iput-object p3, p0, LX/2BG;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 378961
    iput-object p4, p0, LX/2BG;->b:LX/296;

    .line 378962
    iput-object p5, p0, LX/2BG;->c:LX/290;

    .line 378963
    return-void
.end method

.method public static a(LX/0QB;)LX/2BG;
    .locals 9

    .prologue
    .line 378964
    sget-object v0, LX/2BG;->h:LX/2BG;

    if-nez v0, :cond_1

    .line 378965
    const-class v1, LX/2BG;

    monitor-enter v1

    .line 378966
    :try_start_0
    sget-object v0, LX/2BG;->h:LX/2BG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 378967
    if-eqz v2, :cond_0

    .line 378968
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 378969
    new-instance v3, LX/2BG;

    invoke-static {v0}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v4

    check-cast v4, LX/1r1;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/296;->a(LX/0QB;)LX/296;

    move-result-object v7

    check-cast v7, LX/296;

    invoke-static {v0}, LX/290;->b(LX/0QB;)LX/290;

    move-result-object v8

    check-cast v8, LX/290;

    invoke-direct/range {v3 .. v8}, LX/2BG;-><init>(LX/1r1;LX/0ad;Ljava/util/concurrent/ScheduledExecutorService;LX/296;LX/290;)V

    .line 378970
    move-object v0, v3

    .line 378971
    sput-object v0, LX/2BG;->h:LX/2BG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 378972
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 378973
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 378974
    :cond_1
    sget-object v0, LX/2BG;->h:LX/2BG;

    return-object v0

    .line 378975
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 378976
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 378977
    iget-object v1, p0, LX/2BG;->e:LX/1qk;

    monitor-enter v1

    .line 378978
    :try_start_0
    iget-object v0, p0, LX/2BG;->g:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 378979
    iget-object v0, p0, LX/2BG;->g:Ljava/util/concurrent/Future;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 378980
    iget-object v0, p0, LX/2BG;->e:LX/1qk;

    iget-object v0, v0, LX/1qk;->a:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    .line 378981
    const/4 v0, 0x0

    iput-object v0, p0, LX/2BG;->g:Ljava/util/concurrent/Future;

    .line 378982
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/0lF;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 378983
    iget-object v0, p0, LX/2BG;->b:LX/296;

    .line 378984
    invoke-virtual {p1}, LX/0lF;->e()I

    move-result v1

    if-nez v1, :cond_2

    .line 378985
    :goto_0
    iget-object v0, p0, LX/2BG;->d:LX/0ad;

    sget v1, LX/IuZ;->c:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 378986
    if-nez v0, :cond_0

    .line 378987
    :goto_1
    return-void

    .line 378988
    :cond_0
    iget-object v1, p0, LX/2BG;->e:LX/1qk;

    monitor-enter v1

    .line 378989
    :try_start_0
    iget-object v2, p0, LX/2BG;->g:Ljava/util/concurrent/Future;

    if-eqz v2, :cond_1

    .line 378990
    iget-object v2, p0, LX/2BG;->g:Ljava/util/concurrent/Future;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 378991
    :cond_1
    iget-object v2, p0, LX/2BG;->e:LX/1qk;

    iget-object v2, v2, LX/1qk;->a:LX/1ql;

    invoke-virtual {v2}, LX/1ql;->c()V

    .line 378992
    iget-object v2, p0, LX/2BG;->a:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v3, p0, LX/2BG;->f:Ljava/lang/Runnable;

    int-to-long v4, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v4, v5, v0}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/2BG;->g:Ljava/util/concurrent/Future;

    .line 378993
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 378994
    :cond_2
    new-instance v1, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeUpLogger$2;

    invoke-direct {v1, v0, p1}, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeUpLogger$2;-><init>(LX/296;LX/0lF;)V

    .line 378995
    iget-object v3, v0, LX/296;->c:Ljava/util/concurrent/ExecutorService;

    const v4, 0x349cd627

    invoke-static {v3, v1, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
