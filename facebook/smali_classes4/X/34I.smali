.class public LX/34I;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile g:LX/34I;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Landroid/media/AudioManager;

.field private final d:LX/3GP;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 494934
    const-class v0, LX/34I;

    sput-object v0, LX/34I;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/media/AudioManager;LX/3GP;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 494927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494928
    iput-object p1, p0, LX/34I;->b:Landroid/content/Context;

    .line 494929
    iput-object p2, p0, LX/34I;->c:Landroid/media/AudioManager;

    .line 494930
    iput-object p3, p0, LX/34I;->d:LX/3GP;

    .line 494931
    iput-object p4, p0, LX/34I;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 494932
    iput-object p5, p0, LX/34I;->f:LX/0Uh;

    .line 494933
    return-void
.end method

.method public static a(LX/0QB;)LX/34I;
    .locals 9

    .prologue
    .line 494935
    sget-object v0, LX/34I;->g:LX/34I;

    if-nez v0, :cond_1

    .line 494936
    const-class v1, LX/34I;

    monitor-enter v1

    .line 494937
    :try_start_0
    sget-object v0, LX/34I;->g:LX/34I;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 494938
    if-eqz v2, :cond_0

    .line 494939
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 494940
    new-instance v3, LX/34I;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/19T;->b(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v5

    check-cast v5, Landroid/media/AudioManager;

    invoke-static {v0}, LX/3GP;->a(LX/0QB;)LX/3GP;

    move-result-object v6

    check-cast v6, LX/3GP;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct/range {v3 .. v8}, LX/34I;-><init>(Landroid/content/Context;Landroid/media/AudioManager;LX/3GP;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;)V

    .line 494941
    move-object v0, v3

    .line 494942
    sput-object v0, LX/34I;->g:LX/34I;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 494943
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 494944
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 494945
    :cond_1
    sget-object v0, LX/34I;->g:LX/34I;

    return-object v0

    .line 494946
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 494947
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 494924
    const-string v0, "android.permission.MODIFY_AUDIO_SETTINGS"

    .line 494925
    iget-object v1, p0, LX/34I;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    .line 494926
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()LX/7TP;
    .locals 2

    .prologue
    .line 494914
    iget-object v0, p0, LX/34I;->d:LX/3GP;

    invoke-virtual {v0}, LX/3GP;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/34I;->d:LX/3GP;

    .line 494915
    iget-boolean v1, v0, LX/3GP;->h:Z

    move v0, v1

    .line 494916
    if-eqz v0, :cond_0

    .line 494917
    sget-object v0, LX/7TP;->AudioOutputRouteBluetooth:LX/7TP;

    .line 494918
    :goto_0
    return-object v0

    .line 494919
    :cond_0
    iget-object v0, p0, LX/34I;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494920
    sget-object v0, LX/7TP;->AudioOutputRouteSpeakerphone:LX/7TP;

    goto :goto_0

    .line 494921
    :cond_1
    iget-object v0, p0, LX/34I;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 494922
    sget-object v0, LX/7TP;->AudioOutputRouteHeadset:LX/7TP;

    goto :goto_0

    .line 494923
    :cond_2
    sget-object v0, LX/7TP;->AudioOutputRouteEarpiece:LX/7TP;

    goto :goto_0
.end method
