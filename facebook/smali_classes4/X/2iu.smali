.class public LX/2iu;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/2kj;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 452189
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 452190
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/1PT;Landroid/support/v4/app/Fragment;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/2jZ;LX/2jY;LX/1PY;LX/2iv;)LX/2kj;
    .locals 19

    .prologue
    .line 452191
    new-instance v1, LX/2kj;

    invoke-static/range {p0 .. p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v12

    check-cast v12, LX/1Kf;

    invoke-static/range {p0 .. p0}, LX/1dy;->a(LX/0QB;)LX/1dy;

    move-result-object v13

    check-cast v13, LX/1dy;

    const-class v2, LX/2kq;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/2kq;

    const-class v2, LX/2kr;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/2kr;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v16

    check-cast v16, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/1ro;->a(LX/0QB;)LX/1ro;

    move-result-object v17

    check-cast v17, LX/1ro;

    const-class v2, LX/2ks;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/2ks;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v1 .. v18}, LX/2kj;-><init>(Landroid/content/Context;LX/1PT;Landroid/support/v4/app/Fragment;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/2jZ;LX/2jY;LX/1PY;LX/2iv;LX/1Kf;LX/1dy;LX/2kq;LX/2kr;Lcom/facebook/content/SecureContextHelper;LX/1ro;LX/2ks;)V

    .line 452192
    return-object v1
.end method
