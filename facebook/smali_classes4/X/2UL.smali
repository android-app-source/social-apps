.class public LX/2UL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415800
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415801
    iput-object p1, p0, LX/2UL;->a:LX/0Zb;

    .line 415802
    return-void
.end method

.method public static a(LX/0QB;)LX/2UL;
    .locals 4

    .prologue
    .line 415803
    const-class v1, LX/2UL;

    monitor-enter v1

    .line 415804
    :try_start_0
    sget-object v0, LX/2UL;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 415805
    sput-object v2, LX/2UL;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 415806
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415807
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 415808
    new-instance p0, LX/2UL;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/2UL;-><init>(LX/0Zb;)V

    .line 415809
    move-object v0, p0

    .line 415810
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 415811
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2UL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415812
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 415813
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/DA8;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 415794
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p0}, LX/DA8;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "contact_logs"

    .line 415795
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 415796
    move-object v0, v0

    .line 415797
    return-object v0
.end method


# virtual methods
.method public final a(LX/DA8;)V
    .locals 2

    .prologue
    .line 415798
    iget-object v0, p0, LX/2UL;->a:LX/0Zb;

    invoke-static {p1}, LX/2UL;->b(LX/DA8;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 415799
    return-void
.end method
