.class public LX/34r;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/concurrent/atomic/AtomicBoolean;


# instance fields
.field public final b:LX/18V;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/46m;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Wd;

.field public final e:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 495864
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, LX/34r;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>(LX/18V;LX/0Ot;LX/0Wd;)V
    .locals 1
    .param p3    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "LX/0Ot",
            "<",
            "LX/46m;",
            ">;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 495858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 495859
    iput-object p1, p0, LX/34r;->b:LX/18V;

    .line 495860
    iput-object p2, p0, LX/34r;->c:LX/0Ot;

    .line 495861
    iput-object p3, p0, LX/34r;->d:LX/0Wd;

    .line 495862
    new-instance v0, Lcom/facebook/common/udppriming/service/UDPPrimingServiceHandler$1;

    invoke-direct {v0, p0}, Lcom/facebook/common/udppriming/service/UDPPrimingServiceHandler$1;-><init>(LX/34r;)V

    iput-object v0, p0, LX/34r;->e:Ljava/lang/Runnable;

    .line 495863
    return-void
.end method
