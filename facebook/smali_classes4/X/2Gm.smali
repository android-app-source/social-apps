.class public LX/2Gm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final c:Ljava/lang/String;

.field private static volatile e:LX/2Gm;


# instance fields
.field public a:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Landroid/content/Context;
    .annotation build Lcom/facebook/inject/ForAppContext;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 389322
    const-class v0, LX/2Gm;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Gm;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 389323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2Gm;
    .locals 6

    .prologue
    .line 389324
    sget-object v0, LX/2Gm;->e:LX/2Gm;

    if-nez v0, :cond_1

    .line 389325
    const-class v1, LX/2Gm;

    monitor-enter v1

    .line 389326
    :try_start_0
    sget-object v0, LX/2Gm;->e:LX/2Gm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 389327
    if-eqz v2, :cond_0

    .line 389328
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 389329
    new-instance p0, LX/2Gm;

    invoke-direct {p0}, LX/2Gm;-><init>()V

    .line 389330
    const-class v3, Landroid/content/Context;

    const-class v4, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, v4}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    .line 389331
    iput-object v3, p0, LX/2Gm;->d:Landroid/content/Context;

    iput-object v4, p0, LX/2Gm;->a:LX/0Uh;

    iput-object v5, p0, LX/2Gm;->b:LX/0ad;

    .line 389332
    move-object v0, p0

    .line 389333
    sput-object v0, LX/2Gm;->e:LX/2Gm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389334
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 389335
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 389336
    :cond_1
    sget-object v0, LX/2Gm;->e:LX/2Gm;

    return-object v0

    .line 389337
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 389338
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 389339
    iget-object v0, p0, LX/2Gm;->a:LX/0Uh;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 389340
    iget-object v1, p0, LX/2Gm;->d:Landroid/content/Context;

    invoke-static {v1, p2, v0}, LX/00f;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 389341
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 7

    .prologue
    .line 389342
    const/4 v3, 0x0

    .line 389343
    iget-object v0, p0, LX/2Gm;->b:LX/0ad;

    sget-short v1, LX/2zh;->c:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 389344
    if-nez v0, :cond_3

    .line 389345
    const/4 v0, 0x0

    .line 389346
    :goto_0
    move-object v0, v0

    .line 389347
    sget-object v1, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mInstalledClassLoader:Lcom/facebook/common/dextricks/MultiDexClassLoader;

    .line 389348
    if-nez v1, :cond_5

    .line 389349
    const/4 v1, 0x0

    .line 389350
    :goto_1
    move-object v1, v1

    .line 389351
    if-eqz v1, :cond_0

    iget-object v2, v1, LX/02f;->dexExperiment:LX/02d;

    if-nez v2, :cond_4

    .line 389352
    :cond_0
    const/4 v1, 0x0

    .line 389353
    :goto_2
    move-object v1, v1

    .line 389354
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 389355
    iget-object v4, p0, LX/2Gm;->d:Landroid/content/Context;

    if-nez v4, :cond_6

    .line 389356
    :cond_1
    :goto_3
    const/16 v0, 0x436

    const-string v1, "fb4a_random_access_mode_for_oat_file_enabled"

    invoke-direct {p0, v0, v1}, LX/2Gm;->a(ILjava/lang/String;)V

    .line 389357
    const/16 v0, 0x445

    const-string v1, "fb4a_sequential_access_mode_for_oat_file_enabled"

    invoke-direct {p0, v0, v1}, LX/2Gm;->a(ILjava/lang/String;)V

    .line 389358
    const/16 v0, 0x433

    const-string v1, "fb4a_prot_exec_for_oat_file_enabled"

    invoke-direct {p0, v0, v1}, LX/2Gm;->a(ILjava/lang/String;)V

    .line 389359
    new-instance v0, LX/081;

    invoke-direct {v0}, LX/081;-><init>()V

    .line 389360
    iget-object v1, p0, LX/2Gm;->b:LX/0ad;

    sget v2, LX/2zh;->a:I

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    .line 389361
    if-ltz v1, :cond_2

    .line 389362
    iput v1, v0, LX/081;->mArtTruncatedDexSize:I

    .line 389363
    :cond_2
    :try_start_0
    invoke-static {}, LX/008;->getMainDexStore()LX/02U;

    move-result-object v1

    invoke-virtual {v0}, LX/081;->build()LX/080;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/02U;->atomicReplaceConfig(LX/080;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 389364
    :goto_4
    return-void

    .line 389365
    :cond_3
    iget-object v0, p0, LX/2Gm;->b:LX/0ad;

    sget v1, LX/2zh;->b:I

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v1

    .line 389366
    iget-object v0, p0, LX/2Gm;->b:LX/0ad;

    sget-short v2, LX/2zh;->d:S

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 389367
    new-instance v0, LX/02d;

    invoke-direct {v0, v1, v2}, LX/02d;-><init>(IZ)V

    goto :goto_0

    :cond_4
    iget-object v1, v1, LX/02f;->dexExperiment:LX/02d;

    goto :goto_2

    .line 389368
    :cond_5
    iget-object v2, v1, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mConfig:LX/02f;

    move-object v1, v2

    .line 389369
    goto :goto_1

    .line 389370
    :cond_6
    if-eqz v0, :cond_7

    move v5, v2

    .line 389371
    :goto_5
    if-eqz v1, :cond_8

    move v4, v2

    .line 389372
    :goto_6
    if-eqz v0, :cond_9

    invoke-virtual {v0, v1}, LX/02d;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 389373
    :goto_7
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 389374
    if-eqz v5, :cond_a

    if-nez v2, :cond_a

    .line 389375
    iget-object v2, p0, LX/2Gm;->d:Landroid/content/Context;

    invoke-virtual {v0, v2}, LX/02d;->writeExperimentToDisk(Landroid/content/Context;)V

    goto :goto_3

    :cond_7
    move v5, v3

    .line 389376
    goto :goto_5

    :cond_8
    move v4, v3

    .line 389377
    goto :goto_6

    :cond_9
    move v2, v3

    .line 389378
    goto :goto_7

    .line 389379
    :cond_a
    if-nez v5, :cond_1

    if-eqz v4, :cond_1

    .line 389380
    iget-object v2, p0, LX/2Gm;->d:Landroid/content/Context;

    .line 389381
    const-string v3, "fb4a_mlock_for_dex_files_enabled"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 389382
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_b

    .line 389383
    :goto_8
    goto/16 :goto_3

    .line 389384
    :cond_b
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_8

    .line 389385
    :catch_0
    move-exception v0

    .line 389386
    const-string v1, "ColdstartExperimentsController"

    const-string v2, "unable to set dex config"

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4
.end method
