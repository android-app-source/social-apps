.class public LX/2do;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/2f1;",
        "LX/2Iq;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2do;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444312
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 444313
    return-void
.end method

.method public static a(LX/0QB;)LX/2do;
    .locals 3

    .prologue
    .line 444314
    sget-object v0, LX/2do;->a:LX/2do;

    if-nez v0, :cond_1

    .line 444315
    const-class v1, LX/2do;

    monitor-enter v1

    .line 444316
    :try_start_0
    sget-object v0, LX/2do;->a:LX/2do;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 444317
    if-eqz v2, :cond_0

    .line 444318
    :try_start_1
    new-instance v0, LX/2do;

    invoke-direct {v0}, LX/2do;-><init>()V

    .line 444319
    move-object v0, v0

    .line 444320
    sput-object v0, LX/2do;->a:LX/2do;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444321
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 444322
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 444323
    :cond_1
    sget-object v0, LX/2do;->a:LX/2do;

    return-object v0

    .line 444324
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 444325
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
