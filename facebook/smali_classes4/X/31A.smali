.class public final enum LX/31A;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/31A;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/31A;

.field public static final enum CANCEL:LX/31A;

.field public static final enum CLIENT_CANCEL:LX/31A;

.field public static final enum CLIENT_FAIL:LX/31A;

.field public static final enum CLIENT_TTI:LX/31A;

.field public static final enum DATA_RECEIVED:LX/31A;

.field public static final enum DATA_REQUESTED:LX/31A;

.field public static final enum FAILURE:LX/31A;

.field public static final enum MARK:LX/31A;

.field public static final enum START:LX/31A;

.field public static final enum STOP:LX/31A;

.field public static final enum VALUE:LX/31A;


# instance fields
.field private final mMarkerName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 486710
    new-instance v0, LX/31A;

    const-string v1, "START"

    const-string v2, "start"

    invoke-direct {v0, v1, v4, v2}, LX/31A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/31A;->START:LX/31A;

    .line 486711
    new-instance v0, LX/31A;

    const-string v1, "STOP"

    const-string v2, "stop"

    invoke-direct {v0, v1, v5, v2}, LX/31A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/31A;->STOP:LX/31A;

    .line 486712
    new-instance v0, LX/31A;

    const-string v1, "MARK"

    const-string v2, "mark"

    invoke-direct {v0, v1, v6, v2}, LX/31A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/31A;->MARK:LX/31A;

    .line 486713
    new-instance v0, LX/31A;

    const-string v1, "FAILURE"

    const-string v2, "fail"

    invoke-direct {v0, v1, v7, v2}, LX/31A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/31A;->FAILURE:LX/31A;

    .line 486714
    new-instance v0, LX/31A;

    const-string v1, "CANCEL"

    const-string v2, "cancel"

    invoke-direct {v0, v1, v8, v2}, LX/31A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/31A;->CANCEL:LX/31A;

    .line 486715
    new-instance v0, LX/31A;

    const-string v1, "CLIENT_TTI"

    const/4 v2, 0x5

    const-string v3, "client_tti"

    invoke-direct {v0, v1, v2, v3}, LX/31A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/31A;->CLIENT_TTI:LX/31A;

    .line 486716
    new-instance v0, LX/31A;

    const-string v1, "CLIENT_FAIL"

    const/4 v2, 0x6

    const-string v3, "client_fail"

    invoke-direct {v0, v1, v2, v3}, LX/31A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/31A;->CLIENT_FAIL:LX/31A;

    .line 486717
    new-instance v0, LX/31A;

    const-string v1, "CLIENT_CANCEL"

    const/4 v2, 0x7

    const-string v3, "client_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/31A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/31A;->CLIENT_CANCEL:LX/31A;

    .line 486718
    new-instance v0, LX/31A;

    const-string v1, "VALUE"

    const/16 v2, 0x8

    const-string v3, "value"

    invoke-direct {v0, v1, v2, v3}, LX/31A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/31A;->VALUE:LX/31A;

    .line 486719
    new-instance v0, LX/31A;

    const-string v1, "DATA_REQUESTED"

    const/16 v2, 0x9

    const-string v3, "data_requested"

    invoke-direct {v0, v1, v2, v3}, LX/31A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/31A;->DATA_REQUESTED:LX/31A;

    .line 486720
    new-instance v0, LX/31A;

    const-string v1, "DATA_RECEIVED"

    const/16 v2, 0xa

    const-string v3, "data_received"

    invoke-direct {v0, v1, v2, v3}, LX/31A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/31A;->DATA_RECEIVED:LX/31A;

    .line 486721
    const/16 v0, 0xb

    new-array v0, v0, [LX/31A;

    sget-object v1, LX/31A;->START:LX/31A;

    aput-object v1, v0, v4

    sget-object v1, LX/31A;->STOP:LX/31A;

    aput-object v1, v0, v5

    sget-object v1, LX/31A;->MARK:LX/31A;

    aput-object v1, v0, v6

    sget-object v1, LX/31A;->FAILURE:LX/31A;

    aput-object v1, v0, v7

    sget-object v1, LX/31A;->CANCEL:LX/31A;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/31A;->CLIENT_TTI:LX/31A;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/31A;->CLIENT_FAIL:LX/31A;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/31A;->CLIENT_CANCEL:LX/31A;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/31A;->VALUE:LX/31A;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/31A;->DATA_REQUESTED:LX/31A;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/31A;->DATA_RECEIVED:LX/31A;

    aput-object v2, v0, v1

    sput-object v0, LX/31A;->$VALUES:[LX/31A;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 486722
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 486723
    iput-object p3, p0, LX/31A;->mMarkerName:Ljava/lang/String;

    .line 486724
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/31A;
    .locals 1

    .prologue
    .line 486709
    const-class v0, LX/31A;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/31A;

    return-object v0
.end method

.method public static values()[LX/31A;
    .locals 1

    .prologue
    .line 486708
    sget-object v0, LX/31A;->$VALUES:[LX/31A;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/31A;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 486707
    iget-object v0, p0, LX/31A;->mMarkerName:Ljava/lang/String;

    return-object v0
.end method
