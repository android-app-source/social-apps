.class public LX/2Fn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field private static final g:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 387314
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "background_location_reporting/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 387315
    sput-object v0, LX/2Fn;->g:LX/0Tn;

    const-string v1, "location_history_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Fn;->a:LX/0Tn;

    .line 387316
    sget-object v0, LX/2Fn;->g:LX/0Tn;

    const-string v1, "last_refresh_time_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Fn;->b:LX/0Tn;

    .line 387317
    sget-object v0, LX/2Fn;->g:LX/0Tn;

    const-string v1, "high_frequency_end_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Fn;->c:LX/0Tn;

    .line 387318
    sget-object v0, LX/2Fn;->g:LX/0Tn;

    const-string v1, "active_monitors"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Fn;->d:LX/0Tn;

    .line 387319
    sget-object v0, LX/2Fn;->g:LX/0Tn;

    const-string v1, "last_full_sync_ts"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Fn;->e:LX/0Tn;

    .line 387320
    sget-object v0, LX/2Fn;->g:LX/0Tn;

    const-string v1, "speed_change_monitor_record"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Fn;->f:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 387322
    sget-object v0, LX/2Fn;->a:LX/0Tn;

    sget-object v1, LX/2Fn;->b:LX/0Tn;

    sget-object v2, LX/2Fn;->c:LX/0Tn;

    sget-object v3, LX/2Fn;->f:LX/0Tn;

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
