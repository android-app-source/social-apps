.class public abstract LX/2we;
.super LX/2wf;
.source ""

# interfaces
.implements LX/2wh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::",
        "LX/2NW;",
        "A::",
        "LX/2wK;",
        ">",
        "LX/2wf",
        "<TR;>;",
        "LX/2wh",
        "<TR;>;"
    }
.end annotation


# instance fields
.field public final d:LX/2vo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vo",
            "<TA;>;"
        }
    .end annotation
.end field

.field public final e:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<*>;"
        }
    .end annotation
.end field

.field private f:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/2wj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2vs;LX/2wX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2vs",
            "<*>;",
            "LX/2wX;",
            ")V"
        }
    .end annotation

    const-string v0, "GoogleApiClient must not be null"

    invoke-static {p2, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wX;

    invoke-direct {p0, v0}, LX/2wf;-><init>(LX/2wX;)V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, LX/2we;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, LX/2vs;->d()LX/2vo;

    move-result-object v0

    iput-object v0, p0, LX/2we;->d:LX/2vo;

    iput-object p1, p0, LX/2we;->e:LX/2vs;

    return-void
.end method

.method private a(Landroid/os/RemoteException;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-virtual {p1}, Landroid/os/RemoteException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v0}, LX/2we;->b(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method


# virtual methods
.method public final a(LX/2wK;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)V"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, LX/2we;->b(LX/2wK;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, LX/2we;->a(Landroid/os/RemoteException;)V

    throw v0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, LX/2we;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method

.method public final a(LX/2wj;)V
    .locals 1

    iget-object v0, p0, LX/2we;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, LX/2NW;

    invoke-super {p0, p1}, LX/2wf;->a(LX/2NW;)V

    return-void
.end method

.method public abstract b(LX/2wK;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)V"
        }
    .end annotation
.end method

.method public final b(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Failed result must not be success"

    invoke-static {v0, v1}, LX/1ol;->b(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, LX/2wf;->a(Lcom/google/android/gms/common/api/Status;)LX/2NW;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2wf;->a(LX/2NW;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, LX/2we;->f:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wj;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, LX/2wj;->a(LX/2we;)V

    :cond_0
    return-void
.end method
