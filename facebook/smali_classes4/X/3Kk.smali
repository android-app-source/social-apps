.class public LX/3Kk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final I:Ljava/lang/Object;

.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private B:J

.field private C:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;"
        }
    .end annotation
.end field

.field private D:J

.field private E:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;"
        }
    .end annotation
.end field

.field private F:J

.field private G:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcVoicemailInfo;",
            ">;"
        }
    .end annotation
.end field

.field private H:J

.field private final b:LX/0SG;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3CA;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private e:J

.field private f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private g:J

.field private h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private i:J

.field private j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private k:J

.field private l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private m:J

.field private n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private o:J

.field private p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private q:J

.field private r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private s:J

.field private t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private u:J

.field private v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private w:J

.field private x:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private y:J

.field private z:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 549063
    const-class v0, LX/3Kk;

    sput-object v0, LX/3Kk;->a:Ljava/lang/Class;

    .line 549064
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3Kk;->I:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/3CA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 549065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549066
    iput-object p1, p0, LX/3Kk;->b:LX/0SG;

    .line 549067
    iput-object p2, p0, LX/3Kk;->c:LX/0Or;

    .line 549068
    return-void
.end method

.method public static a(LX/0QB;)LX/3Kk;
    .locals 8

    .prologue
    .line 549069
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 549070
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 549071
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 549072
    if-nez v1, :cond_0

    .line 549073
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 549074
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 549075
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 549076
    sget-object v1, LX/3Kk;->I:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 549077
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 549078
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 549079
    :cond_1
    if-nez v1, :cond_4

    .line 549080
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 549081
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 549082
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 549083
    new-instance v7, LX/3Kk;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    const/16 p0, 0xfa6

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v7, v1, p0}, LX/3Kk;-><init>(LX/0SG;LX/0Or;)V

    .line 549084
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 549085
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 549086
    if-nez v1, :cond_2

    .line 549087
    sget-object v0, LX/3Kk;->I:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Kk;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 549088
    :goto_1
    if-eqz v0, :cond_3

    .line 549089
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 549090
    :goto_3
    check-cast v0, LX/3Kk;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 549091
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 549092
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 549093
    :catchall_1
    move-exception v0

    .line 549094
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 549095
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 549096
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 549097
    :cond_2
    :try_start_8
    sget-object v0, LX/3Kk;->I:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Kk;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static e(LX/3Kk;)V
    .locals 10

    .prologue
    const-wide/32 v8, 0x493e0

    const-wide/32 v6, 0x36ee80

    const/4 v4, 0x0

    .line 549098
    iget-object v0, p0, LX/3Kk;->d:LX/0Px;

    if-eqz v0, :cond_0

    .line 549099
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->e:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    .line 549100
    iput-object v4, p0, LX/3Kk;->d:LX/0Px;

    .line 549101
    :cond_0
    iget-object v0, p0, LX/3Kk;->p:LX/0Px;

    if-eqz v0, :cond_1

    .line 549102
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->q:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v8

    if-lez v0, :cond_1

    .line 549103
    iput-object v4, p0, LX/3Kk;->p:LX/0Px;

    .line 549104
    :cond_1
    iget-object v0, p0, LX/3Kk;->r:LX/0Px;

    if-eqz v0, :cond_2

    .line 549105
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->s:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_2

    .line 549106
    iput-object v4, p0, LX/3Kk;->r:LX/0Px;

    .line 549107
    :cond_2
    iget-object v0, p0, LX/3Kk;->f:LX/0Px;

    if-eqz v0, :cond_3

    .line 549108
    iget-object v0, p0, LX/3Kk;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3CA;

    invoke-virtual {v0}, LX/3CA;->shouldShowPresence()Z

    move-result v0

    if-nez v0, :cond_f

    .line 549109
    iput-object v4, p0, LX/3Kk;->f:LX/0Px;

    .line 549110
    :cond_3
    :goto_0
    iget-object v0, p0, LX/3Kk;->h:LX/0Px;

    if-eqz v0, :cond_4

    .line 549111
    iget-object v0, p0, LX/3Kk;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3CA;

    invoke-virtual {v0}, LX/3CA;->shouldShowPresence()Z

    move-result v0

    if-nez v0, :cond_10

    .line 549112
    iput-object v4, p0, LX/3Kk;->h:LX/0Px;

    .line 549113
    :cond_4
    :goto_1
    iget-object v0, p0, LX/3Kk;->j:LX/0Px;

    if-eqz v0, :cond_5

    .line 549114
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->k:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_5

    .line 549115
    iput-object v4, p0, LX/3Kk;->j:LX/0Px;

    .line 549116
    :cond_5
    iget-object v0, p0, LX/3Kk;->l:LX/0Px;

    if-eqz v0, :cond_6

    .line 549117
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->m:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_6

    .line 549118
    iput-object v4, p0, LX/3Kk;->l:LX/0Px;

    .line 549119
    :cond_6
    iget-object v0, p0, LX/3Kk;->n:LX/0Px;

    if-eqz v0, :cond_7

    .line 549120
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->o:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_7

    .line 549121
    iput-object v4, p0, LX/3Kk;->n:LX/0Px;

    .line 549122
    :cond_7
    iget-object v0, p0, LX/3Kk;->t:LX/0Px;

    if-eqz v0, :cond_8

    .line 549123
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->u:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_8

    .line 549124
    iput-object v4, p0, LX/3Kk;->t:LX/0Px;

    .line 549125
    :cond_8
    iget-object v0, p0, LX/3Kk;->v:LX/0Px;

    if-eqz v0, :cond_9

    .line 549126
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->w:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_9

    .line 549127
    iput-object v4, p0, LX/3Kk;->v:LX/0Px;

    .line 549128
    :cond_9
    iget-object v0, p0, LX/3Kk;->x:LX/0Px;

    if-eqz v0, :cond_a

    .line 549129
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->y:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_a

    .line 549130
    iput-object v4, p0, LX/3Kk;->x:LX/0Px;

    .line 549131
    :cond_a
    iget-object v0, p0, LX/3Kk;->A:LX/0Px;

    if-eqz v0, :cond_b

    .line 549132
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->B:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_b

    .line 549133
    iput-object v4, p0, LX/3Kk;->A:LX/0Px;

    .line 549134
    :cond_b
    iget-object v0, p0, LX/3Kk;->C:LX/0Px;

    if-eqz v0, :cond_c

    .line 549135
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->D:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v8

    if-lez v0, :cond_c

    .line 549136
    iput-object v4, p0, LX/3Kk;->C:LX/0Px;

    .line 549137
    :cond_c
    iget-object v0, p0, LX/3Kk;->E:LX/0Px;

    if-eqz v0, :cond_d

    .line 549138
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->F:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v8

    if-lez v0, :cond_d

    .line 549139
    iput-object v4, p0, LX/3Kk;->E:LX/0Px;

    .line 549140
    :cond_d
    iget-object v0, p0, LX/3Kk;->G:LX/0Px;

    if-eqz v0, :cond_e

    .line 549141
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->H:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v8

    if-lez v0, :cond_e

    .line 549142
    iput-object v4, p0, LX/3Kk;->G:LX/0Px;

    .line 549143
    :cond_e
    return-void

    .line 549144
    :cond_f
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->g:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v8

    if-lez v0, :cond_3

    .line 549145
    iput-object v4, p0, LX/3Kk;->f:LX/0Px;

    goto/16 :goto_0

    .line 549146
    :cond_10
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Kk;->i:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v8

    if-lez v0, :cond_4

    .line 549147
    iput-object v4, p0, LX/3Kk;->h:LX/0Px;

    goto/16 :goto_1
.end method


# virtual methods
.method public final declared-synchronized a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 549148
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3Kk;->e(LX/3Kk;)V

    .line 549149
    iget-object v0, p0, LX/3Kk;->f:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 549150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 549151
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Kk;->d:LX/0Px;

    .line 549152
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549153
    monitor-exit p0

    return-void

    .line 549154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 549155
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3Kk;->e(LX/3Kk;)V

    .line 549156
    iget-object v0, p0, LX/3Kk;->j:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 549157
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 549158
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Kk;->p:LX/0Px;

    .line 549159
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->q:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549160
    monitor-exit p0

    return-void

    .line 549161
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 549162
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3Kk;->e(LX/3Kk;)V

    .line 549163
    iget-object v0, p0, LX/3Kk;->n:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 549164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 549165
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Kk;->C:LX/0Px;

    .line 549166
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->D:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549167
    monitor-exit p0

    return-void

    .line 549168
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 549169
    invoke-virtual {p0}, LX/3Kk;->d()V

    .line 549170
    return-void
.end method

.method public final declared-synchronized d()V
    .locals 2

    .prologue
    .line 549028
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/3Kk;->d:LX/0Px;

    .line 549029
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->p:LX/0Px;

    .line 549030
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->f:LX/0Px;

    .line 549031
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->h:LX/0Px;

    .line 549032
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->j:LX/0Px;

    .line 549033
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->l:LX/0Px;

    .line 549034
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->n:LX/0Px;

    .line 549035
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->t:LX/0Px;

    .line 549036
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->v:LX/0Px;

    .line 549037
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->x:LX/0Px;

    .line 549038
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->z:LX/0Px;

    .line 549039
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->A:LX/0Px;

    .line 549040
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->C:LX/0Px;

    .line 549041
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->E:LX/0Px;

    .line 549042
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kk;->G:LX/0Px;

    .line 549043
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->e:J

    .line 549044
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->q:J

    .line 549045
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->g:J

    .line 549046
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->i:J

    .line 549047
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->k:J

    .line 549048
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->m:J

    .line 549049
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->o:J

    .line 549050
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->u:J

    .line 549051
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->w:J

    .line 549052
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->y:J

    .line 549053
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->B:J

    .line 549054
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->D:J

    .line 549055
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->H:J

    .line 549056
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Kk;->F:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549057
    monitor-exit p0

    return-void

    .line 549058
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 549059
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Kk;->E:LX/0Px;

    .line 549060
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->F:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549061
    monitor-exit p0

    return-void

    .line 549062
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcVoicemailInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 548980
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Kk;->G:LX/0Px;

    .line 548981
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->H:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548982
    monitor-exit p0

    return-void

    .line 548983
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 548984
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Kk;->r:LX/0Px;

    .line 548985
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->s:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548986
    monitor-exit p0

    return-void

    .line 548987
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 548988
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3Kk;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3CA;

    invoke-virtual {v0}, LX/3CA;->shouldShowPresence()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 548989
    :goto_0
    monitor-exit p0

    return-void

    .line 548990
    :cond_0
    :try_start_1
    iput-object p1, p0, LX/3Kk;->f:LX/0Px;

    .line 548991
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->g:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 548992
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 548993
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3Kk;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3CA;

    invoke-virtual {v0}, LX/3CA;->shouldShowPresence()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 548994
    :goto_0
    monitor-exit p0

    return-void

    .line 548995
    :cond_0
    :try_start_1
    iput-object p1, p0, LX/3Kk;->h:LX/0Px;

    .line 548996
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->i:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 548997
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 548998
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Kk;->j:LX/0Px;

    .line 548999
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->k:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549000
    monitor-exit p0

    return-void

    .line 549001
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized j(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 549002
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Kk;->l:LX/0Px;

    .line 549003
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->m:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549004
    monitor-exit p0

    return-void

    .line 549005
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 549006
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Kk;->n:LX/0Px;

    .line 549007
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->o:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549008
    monitor-exit p0

    return-void

    .line 549009
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized l(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 549024
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Kk;->t:LX/0Px;

    .line 549025
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->u:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549026
    monitor-exit p0

    return-void

    .line 549027
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized m(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 549010
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Kk;->v:LX/0Px;

    .line 549011
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->w:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549012
    monitor-exit p0

    return-void

    .line 549013
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized n(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 549014
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Kk;->x:LX/0Px;

    .line 549015
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->y:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549016
    monitor-exit p0

    return-void

    .line 549017
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized o(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 549018
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Kk;->z:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549019
    monitor-exit p0

    return-void

    .line 549020
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final p(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 549021
    iput-object p1, p0, LX/3Kk;->A:LX/0Px;

    .line 549022
    iget-object v0, p0, LX/3Kk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Kk;->B:J

    .line 549023
    return-void
.end method
