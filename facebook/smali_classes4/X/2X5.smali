.class public LX/2X5;
.super Landroid/content/BroadcastReceiver;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0SG;

.field public volatile c:LX/2X6;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 419466
    const-class v0, LX/2X5;

    sput-object v0, LX/2X5;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 419467
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 419468
    iput-object p1, p0, LX/2X5;->b:LX/0SG;

    .line 419469
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x6bfad346

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 419470
    sget-object v0, LX/2X5;->a:Ljava/lang/Class;

    const-string v2, "getting the id response"

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 419471
    invoke-virtual {p0}, LX/2X5;->getResultData()Ljava/lang/String;

    move-result-object v2

    .line 419472
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/2X5;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "device_id_generated_timestamp_ms"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 419473
    invoke-virtual {p0}, LX/2X5;->getResultCode()I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    if-eqz v2, :cond_1

    .line 419474
    new-instance v0, LX/0dI;

    invoke-direct {v0, v2, v4, v5}, LX/0dI;-><init>(Ljava/lang/String;J)V

    .line 419475
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "response: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 419476
    :goto_0
    iget-object v2, p0, LX/2X5;->c:LX/2X6;

    if-eqz v2, :cond_0

    .line 419477
    iget-object v2, p0, LX/2X5;->c:LX/2X6;

    invoke-virtual {v2, v0}, LX/2X6;->a(LX/0dI;)V

    .line 419478
    :cond_0
    const v0, -0x2a92a6a8

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    return-void

    .line 419479
    :cond_1
    iget-object v0, p0, LX/2X5;->b:LX/0SG;

    invoke-static {v0}, LX/0dC;->a(LX/0SG;)LX/0dI;

    move-result-object v0

    .line 419480
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "generating new id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
