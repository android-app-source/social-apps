.class public LX/2IN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2IN;


# instance fields
.field private final a:LX/0dk;

.field private final b:LX/0dn;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/0dn;LX/0dk;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391621
    iput-object p1, p0, LX/2IN;->b:LX/0dn;

    .line 391622
    iput-object p2, p0, LX/2IN;->a:LX/0dk;

    .line 391623
    iput-object p3, p0, LX/2IN;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 391624
    return-void
.end method

.method public static a(LX/0QB;)LX/2IN;
    .locals 6

    .prologue
    .line 391625
    sget-object v0, LX/2IN;->d:LX/2IN;

    if-nez v0, :cond_1

    .line 391626
    const-class v1, LX/2IN;

    monitor-enter v1

    .line 391627
    :try_start_0
    sget-object v0, LX/2IN;->d:LX/2IN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391628
    if-eqz v2, :cond_0

    .line 391629
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 391630
    new-instance p0, LX/2IN;

    invoke-static {v0}, LX/0dn;->a(LX/0QB;)LX/0dn;

    move-result-object v3

    check-cast v3, LX/0dn;

    invoke-static {v0}, LX/0dk;->a(LX/0QB;)LX/0dk;

    move-result-object v4

    check-cast v4, LX/0dk;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5}, LX/2IN;-><init>(LX/0dn;LX/0dk;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 391631
    move-object v0, p0

    .line 391632
    sput-object v0, LX/2IN;->d:LX/2IN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391633
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391634
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391635
    :cond_1
    sget-object v0, LX/2IN;->d:LX/2IN;

    return-object v0

    .line 391636
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391637
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 5

    .prologue
    .line 391638
    iget-object v0, p0, LX/2IN;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dj;->a:LX/0Tn;

    iget-object v2, p0, LX/2IN;->b:LX/0dn;

    .line 391639
    sget-wide v3, LX/0X5;->jc:J

    invoke-static {v2, v3, v4}, LX/0dn;->a(LX/0dn;J)Z

    move-result v3

    move v2, v3

    .line 391640
    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 391641
    iget-object v0, p0, LX/2IN;->b:LX/0dn;

    .line 391642
    sget-wide v3, LX/0X5;->jd:J

    invoke-static {v0, v3, v4}, LX/0dn;->a(LX/0dn;J)Z

    move-result v3

    move v0, v3

    .line 391643
    if-eqz v0, :cond_0

    .line 391644
    const-string v0, "ComponentScriptLowPriINeedInit.init"

    const v1, 0x4235734e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 391645
    :try_start_0
    iget-object v0, p0, LX/2IN;->a:LX/0dk;

    invoke-virtual {v0}, LX/0dk;->a()Lcom/facebook/java2js/JSContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/java2js/JSContext;->garbageCollect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391646
    const v0, 0x17b67d54

    invoke-static {v0}, LX/02m;->a(I)V

    .line 391647
    :cond_0
    return-void

    .line 391648
    :catchall_0
    move-exception v0

    const v1, -0x6f5cbc4d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
