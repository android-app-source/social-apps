.class public final LX/2hE;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 0

    .prologue
    .line 449903
    iput-object p1, p0, LX/2hE;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 9

    .prologue
    .line 449904
    check-cast p1, LX/2f2;

    const/4 v0, 0x1

    .line 449905
    if-eqz p1, :cond_1

    iget-object v1, p0, LX/2hE;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/2hE;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-wide v2, p1, LX/2f2;->a:J

    .line 449906
    invoke-virtual {v1, v2, v3}, LX/2iK;->b(J)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1, v2, v3}, LX/2iK;->e(J)Z

    move-result v4

    if-eqz v4, :cond_8

    :cond_0
    const/4 v4, 0x1

    :goto_0
    move v1, v4

    .line 449907
    if-nez v1, :cond_2

    .line 449908
    :cond_1
    :goto_1
    return-void

    .line 449909
    :cond_2
    iget-object v1, p0, LX/2hE;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    .line 449910
    const/4 v1, 0x0

    .line 449911
    iget-object v2, p0, LX/2hE;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v2, v2, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-wide v4, p1, LX/2f2;->a:J

    invoke-virtual {v2, v4, v5}, LX/2iK;->e(J)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 449912
    iget-object v1, p0, LX/2hE;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-wide v2, p1, LX/2f2;->a:J

    iget-object v4, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 449913
    iget-object v5, v1, LX/2iK;->l:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 449914
    if-eqz v5, :cond_3

    .line 449915
    invoke-virtual {v5}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    .line 449916
    if-eq v6, v4, :cond_3

    .line 449917
    iput-object v6, v5, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 449918
    invoke-virtual {v5, v4}, Lcom/facebook/friends/model/PersonYouMayKnow;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 449919
    invoke-virtual {v1}, LX/2iK;->m()V

    .line 449920
    const v5, 0x3eb15319

    invoke-static {v1, v5}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 449921
    :cond_3
    :goto_2
    iget-boolean v1, p1, LX/2f2;->c:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/2hE;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 449922
    iget-object v0, p0, LX/2hE;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->m(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    goto :goto_1

    .line 449923
    :cond_4
    iget-object v2, p0, LX/2hE;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v2, v2, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-wide v4, p1, LX/2f2;->a:J

    invoke-virtual {v2, v4, v5}, LX/2iK;->b(J)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 449924
    iget-wide v2, p1, LX/2f2;->a:J

    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v7, 0x1

    .line 449925
    iget-object v4, p0, LX/2hE;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v4, v4, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    .line 449926
    iget-object v5, v4, LX/2iK;->j:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/friends/model/FriendRequest;

    .line 449927
    if-eqz v5, :cond_b

    invoke-virtual {v5}, Lcom/facebook/friends/model/FriendRequest;->k()Z

    move-result v5

    if-eqz v5, :cond_b

    const/4 v5, 0x1

    :goto_3
    move v4, v5

    .line 449928
    if-nez v4, :cond_5

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v1, v5, :cond_6

    :cond_5
    if-eqz v4, :cond_9

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, v4, :cond_9

    .line 449929
    :cond_6
    iget-object v4, p0, LX/2hE;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v4, v4, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    sget-object v6, LX/2lu;->ACCEPTED:LX/2lu;

    invoke-virtual {v4, v5, v6, v7}, LX/2iK;->a(Ljava/lang/String;LX/2lu;Z)V

    .line 449930
    :goto_4
    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_2

    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 449931
    :cond_9
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, v4, :cond_a

    .line 449932
    iget-object v4, p0, LX/2hE;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v4, v4, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    sget-object v6, LX/2lu;->REJECTED:LX/2lu;

    invoke-virtual {v4, v5, v6, v7}, LX/2iK;->a(Ljava/lang/String;LX/2lu;Z)V

    goto :goto_4

    .line 449933
    :cond_a
    iget-object v4, p0, LX/2hE;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->a$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    goto :goto_4

    :cond_b
    const/4 v5, 0x0

    goto :goto_3
.end method
