.class public LX/2KR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile k:LX/2KR;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0bH;

.field public d:LX/1AM;

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1B1;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/1B1;

.field public g:LX/1B1;

.field public h:LX/1B1;

.field public i:LX/0Xl;

.field public j:LX/0Yb;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    .line 393875
    const-string v0, "like_main"

    const-string v1, "comment"

    const-string v2, "share"

    const-string v3, "post_main"

    const-string v4, "post_comment"

    const-string v5, "like_comment"

    const-string v6, "pull_to_refresh"

    const-string v7, "pull_to_refresh_slow"

    const-string v8, "pull_to_refresh_medium"

    const-string v9, "pull_to_refresh_fast"

    const-string v10, "collapse_after_refresh"

    invoke-static/range {v0 .. v10}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2KR;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0bH;LX/1AM;LX/0Xl;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/0bH;",
            "LX/1AM;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "LX/1B1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 393876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393877
    iput-object p1, p0, LX/2KR;->b:LX/0Ot;

    .line 393878
    iput-object p2, p0, LX/2KR;->c:LX/0bH;

    .line 393879
    iput-object p3, p0, LX/2KR;->d:LX/1AM;

    .line 393880
    iput-object p4, p0, LX/2KR;->i:LX/0Xl;

    .line 393881
    iput-object p5, p0, LX/2KR;->e:LX/0Or;

    .line 393882
    return-void
.end method

.method public static a(LX/0QB;)LX/2KR;
    .locals 9

    .prologue
    .line 393883
    sget-object v0, LX/2KR;->k:LX/2KR;

    if-nez v0, :cond_1

    .line 393884
    const-class v1, LX/2KR;

    monitor-enter v1

    .line 393885
    :try_start_0
    sget-object v0, LX/2KR;->k:LX/2KR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 393886
    if-eqz v2, :cond_0

    .line 393887
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 393888
    new-instance v3, LX/2KR;

    const/16 v4, 0x3567

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v5

    check-cast v5, LX/0bH;

    invoke-static {v0}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v6

    check-cast v6, LX/1AM;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    const/16 v8, 0x45a

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/2KR;-><init>(LX/0Ot;LX/0bH;LX/1AM;LX/0Xl;LX/0Or;)V

    .line 393889
    move-object v0, v3

    .line 393890
    sput-object v0, LX/2KR;->k:LX/2KR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 393891
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 393892
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 393893
    :cond_1
    sget-object v0, LX/2KR;->k:LX/2KR;

    return-object v0

    .line 393894
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 393895
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 393896
    sget-object v0, LX/2KR;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b$redex0(LX/2KR;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 393897
    invoke-static {p1}, LX/2KR;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393898
    iget-object v0, p0, LX/2KR;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    invoke-virtual {v0, p1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 393899
    :cond_0
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 393900
    iget-object v0, p0, LX/2KR;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B1;

    iput-object v0, p0, LX/2KR;->f:LX/1B1;

    .line 393901
    iget-object v0, p0, LX/2KR;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B1;

    iput-object v0, p0, LX/2KR;->g:LX/1B1;

    .line 393902
    iget-object v0, p0, LX/2KR;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B1;

    iput-object v0, p0, LX/2KR;->h:LX/1B1;

    .line 393903
    iget-object v0, p0, LX/2KR;->f:LX/1B1;

    new-instance v1, LX/2KS;

    invoke-direct {v1, p0}, LX/2KS;-><init>(LX/2KR;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 393904
    iget-object v0, p0, LX/2KR;->f:LX/1B1;

    new-instance v1, LX/2KT;

    invoke-direct {v1, p0}, LX/2KT;-><init>(LX/2KR;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 393905
    iget-object v0, p0, LX/2KR;->f:LX/1B1;

    new-instance v1, LX/2KU;

    invoke-direct {v1, p0}, LX/2KU;-><init>(LX/2KR;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 393906
    iget-object v0, p0, LX/2KR;->f:LX/1B1;

    new-instance v1, LX/2KW;

    invoke-direct {v1, p0}, LX/2KW;-><init>(LX/2KR;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 393907
    iget-object v0, p0, LX/2KR;->f:LX/1B1;

    new-instance v1, LX/2KY;

    invoke-direct {v1, p0}, LX/2KY;-><init>(LX/2KR;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 393908
    iget-object v0, p0, LX/2KR;->f:LX/1B1;

    iget-object v1, p0, LX/2KR;->c:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 393909
    iget-object v0, p0, LX/2KR;->h:LX/1B1;

    new-instance v1, LX/2Kd;

    invoke-direct {v1, p0}, LX/2Kd;-><init>(LX/2KR;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 393910
    iget-object v0, p0, LX/2KR;->h:LX/1B1;

    new-instance v1, LX/2Kf;

    invoke-direct {v1, p0}, LX/2Kf;-><init>(LX/2KR;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 393911
    iget-object v0, p0, LX/2KR;->h:LX/1B1;

    iget-object v1, p0, LX/2KR;->c:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 393912
    iget-object v0, p0, LX/2KR;->g:LX/1B1;

    new-instance v1, LX/2Kj;

    invoke-direct {v1, p0}, LX/2Kj;-><init>(LX/2KR;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 393913
    iget-object v0, p0, LX/2KR;->g:LX/1B1;

    new-instance v1, LX/2Kl;

    invoke-direct {v1, p0}, LX/2Kl;-><init>(LX/2KR;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 393914
    iget-object v0, p0, LX/2KR;->g:LX/1B1;

    new-instance v1, LX/2Kn;

    invoke-direct {v1, p0}, LX/2Kn;-><init>(LX/2KR;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 393915
    iget-object v0, p0, LX/2KR;->g:LX/1B1;

    iget-object v1, p0, LX/2KR;->d:LX/1AM;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 393916
    iget-object v0, p0, LX/2KR;->i:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    new-instance v2, LX/2Ks;

    invoke-direct {v2, p0}, LX/2Ks;-><init>(LX/2KR;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    .line 393917
    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2KR;->j:LX/0Yb;

    .line 393918
    iget-object v0, p0, LX/2KR;->j:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 393919
    return-void
.end method
