.class public LX/2Hu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0Sh;

.field private final d:LX/0SG;

.field private final e:LX/2Hv;

.field private final f:LX/2Bs;

.field private final g:LX/0So;

.field private final h:LX/0ZR;

.field private final i:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 391272
    const-class v0, LX/2Hu;

    sput-object v0, LX/2Hu;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Sh;LX/0SG;LX/2Hv;LX/2Bs;LX/0So;LX/0ZR;Ljava/lang/Long;)V
    .locals 2
    .param p8    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/push/mqtt/service/MqttBindTimeoutMs;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391274
    iput-object p1, p0, LX/2Hu;->b:Landroid/content/Context;

    .line 391275
    iput-object p2, p0, LX/2Hu;->c:LX/0Sh;

    .line 391276
    iput-object p3, p0, LX/2Hu;->d:LX/0SG;

    .line 391277
    iput-object p4, p0, LX/2Hu;->e:LX/2Hv;

    .line 391278
    iput-object p6, p0, LX/2Hu;->g:LX/0So;

    .line 391279
    iput-object p7, p0, LX/2Hu;->h:LX/0ZR;

    .line 391280
    iput-object p5, p0, LX/2Hu;->f:LX/2Bs;

    .line 391281
    invoke-virtual {p8}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/2Hu;->i:J

    .line 391282
    return-void
.end method

.method public static a(LX/0QB;)LX/2Hu;
    .locals 12

    .prologue
    .line 391283
    const-class v1, LX/2Hu;

    monitor-enter v1

    .line 391284
    :try_start_0
    sget-object v0, LX/2Hu;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 391285
    sput-object v2, LX/2Hu;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 391286
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391287
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 391288
    new-instance v3, LX/2Hu;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v0}, LX/2Hv;->a(LX/0QB;)LX/2Hv;

    move-result-object v7

    check-cast v7, LX/2Hv;

    invoke-static {v0}, LX/2Bs;->a(LX/0QB;)LX/2Bs;

    move-result-object v8

    check-cast v8, LX/2Bs;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v9

    check-cast v9, LX/0So;

    invoke-static {v0}, LX/0ZR;->a(LX/0QB;)LX/0ZR;

    move-result-object v10

    check-cast v10, LX/0ZR;

    .line 391289
    invoke-static {}, LX/2Hw;->b()Ljava/lang/Long;

    move-result-object v11

    move-object v11, v11

    .line 391290
    check-cast v11, Ljava/lang/Long;

    invoke-direct/range {v3 .. v11}, LX/2Hu;-><init>(Landroid/content/Context;LX/0Sh;LX/0SG;LX/2Hv;LX/2Bs;LX/0So;LX/0ZR;Ljava/lang/Long;)V

    .line 391291
    move-object v0, v3

    .line 391292
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 391293
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2Hu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391294
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 391295
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2gV;
    .locals 8

    .prologue
    .line 391296
    new-instance v0, LX/2gV;

    iget-object v1, p0, LX/2Hu;->b:Landroid/content/Context;

    iget-object v2, p0, LX/2Hu;->e:LX/2Hv;

    iget-object v3, p0, LX/2Hu;->d:LX/0SG;

    iget-object v4, p0, LX/2Hu;->f:LX/2Bs;

    iget-object v5, p0, LX/2Hu;->c:LX/0Sh;

    iget-object v6, p0, LX/2Hu;->g:LX/0So;

    iget-object v7, p0, LX/2Hu;->h:LX/0ZR;

    invoke-direct/range {v0 .. v7}, LX/2gV;-><init>(Landroid/content/Context;LX/2Hv;LX/0SG;LX/2Bs;LX/0Sh;LX/0So;LX/0ZR;)V

    .line 391297
    :try_start_0
    iget-wide v2, p0, LX/2Hu;->i:J

    invoke-virtual {v0, v2, v3}, LX/2gV;->b(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 391298
    sget-object v1, LX/2Hu;->a:Ljava/lang/Class;

    const-string v2, "Failed to bind to MqttPushService"

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 391299
    :cond_0
    return-object v0

    .line 391300
    :catch_0
    move-exception v0

    .line 391301
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
