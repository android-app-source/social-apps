.class public final LX/2w0;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/2vn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vn",
            "<",
            "LX/4vo;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/2vn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vn",
            "<",
            "LX/4vo;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/2vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vq",
            "<",
            "LX/4vo;",
            "LX/2w4;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/2vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vq",
            "<",
            "LX/4vo;",
            "LX/4ve;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Lcom/google/android/gms/common/api/Scope;

.field public static final f:Lcom/google/android/gms/common/api/Scope;

.field public static final g:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<",
            "LX/2w4;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<",
            "LX/4ve;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, LX/2vn;

    invoke-direct {v0}, LX/2vn;-><init>()V

    sput-object v0, LX/2w0;->a:LX/2vn;

    new-instance v0, LX/2vn;

    invoke-direct {v0}, LX/2vn;-><init>()V

    sput-object v0, LX/2w0;->b:LX/2vn;

    new-instance v0, LX/2w1;

    invoke-direct {v0}, LX/2w1;-><init>()V

    sput-object v0, LX/2w0;->c:LX/2vq;

    new-instance v0, LX/2w2;

    invoke-direct {v0}, LX/2w2;-><init>()V

    sput-object v0, LX/2w0;->d:LX/2vq;

    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "profile"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/2w0;->e:Lcom/google/android/gms/common/api/Scope;

    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "email"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/2w0;->f:Lcom/google/android/gms/common/api/Scope;

    new-instance v0, LX/2vs;

    const-string v1, "SignIn.API"

    sget-object v2, LX/2w0;->c:LX/2vq;

    sget-object v3, LX/2w0;->a:LX/2vn;

    invoke-direct {v0, v1, v2, v3}, LX/2vs;-><init>(Ljava/lang/String;LX/2vq;LX/2vn;)V

    sput-object v0, LX/2w0;->g:LX/2vs;

    new-instance v0, LX/2vs;

    const-string v1, "SignIn.INTERNAL_API"

    sget-object v2, LX/2w0;->d:LX/2vq;

    sget-object v3, LX/2w0;->b:LX/2vn;

    invoke-direct {v0, v1, v2, v3}, LX/2vs;-><init>(Ljava/lang/String;LX/2vq;LX/2vn;)V

    sput-object v0, LX/2w0;->h:LX/2vs;

    return-void
.end method
