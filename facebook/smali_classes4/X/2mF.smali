.class public LX/2mF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field private final mHandlers:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/1qM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1qM;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 459418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459419
    invoke-static {p1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/2mF;->mHandlers:LX/0P1;

    .line 459420
    return-void
.end method


# virtual methods
.method public handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 459421
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v1, v0

    .line 459422
    iget-object v0, p0, LX/2mF;->mHandlers:LX/0P1;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qM;

    .line 459423
    if-nez v0, :cond_0

    .line 459424
    sget-object v0, LX/1nY;->ORCA_SERVICE_UNKNOWN_OPERATION:LX/1nY;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 459425
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method
