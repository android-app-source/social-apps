.class public LX/2k2;
.super LX/1qS;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2k2;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/2k3;)V
    .locals 6
    .param p2    # LX/0Tt;
        .annotation runtime Lcom/facebook/database/threadchecker/AllowAnyThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 455270
    invoke-static {p4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const-string v5, "graph_cursors"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 455271
    return-void
.end method

.method public static a(LX/0QB;)LX/2k2;
    .locals 7

    .prologue
    .line 455272
    sget-object v0, LX/2k2;->a:LX/2k2;

    if-nez v0, :cond_1

    .line 455273
    const-class v1, LX/2k2;

    monitor-enter v1

    .line 455274
    :try_start_0
    sget-object v0, LX/2k2;->a:LX/2k2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 455275
    if-eqz v2, :cond_0

    .line 455276
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 455277
    new-instance p0, LX/2k2;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0sm;->a(LX/0QB;)LX/0Tt;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v5

    check-cast v5, LX/1qU;

    invoke-static {v0}, LX/2k3;->a(LX/0QB;)LX/2k3;

    move-result-object v6

    check-cast v6, LX/2k3;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2k2;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/2k3;)V

    .line 455278
    move-object v0, p0

    .line 455279
    sput-object v0, LX/2k2;->a:LX/2k2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 455280
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 455281
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 455282
    :cond_1
    sget-object v0, LX/2k2;->a:LX/2k2;

    return-object v0

    .line 455283
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 455284
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final c()J
    .locals 2

    .prologue
    .line 455285
    const-wide/32 v0, 0x500000

    return-wide v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 455286
    const/16 v0, 0x6400

    return v0
.end method
