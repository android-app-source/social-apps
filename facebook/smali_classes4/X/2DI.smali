.class public LX/2DI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/io/File;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:LX/0pD;

.field public final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/2Eu;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 383990
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383991
    const-string v0, "uploader_class"

    invoke-interface {p1, v0, v6}, LX/2Eu;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 383992
    if-nez v0, :cond_0

    .line 383993
    new-instance v0, LX/403;

    const-string v1, "uploader_class is null or empty"

    invoke-direct {v0, v1}, LX/403;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383994
    :cond_0
    const-string v1, "flexible_sampling_updater"

    invoke-interface {p1, v1, v6}, LX/2Eu;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 383995
    const-string v2, "thread_handler_factory"

    invoke-interface {p1, v2, v6}, LX/2Eu;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 383996
    const-string v3, "priority_dir"

    invoke-interface {p1, v3, v6}, LX/2Eu;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 383997
    if-nez v3, :cond_1

    .line 383998
    new-instance v0, LX/403;

    const-string v1, "priority_dir is null or empty"

    invoke-direct {v0, v1}, LX/403;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383999
    :cond_1
    const-string v4, "network_priority"

    sget-object v5, LX/0pD;->NORMAL:LX/0pD;

    invoke-virtual {v5}, LX/0pD;->ordinal()I

    move-result v5

    invoke-interface {p1, v4, v5}, LX/2Eu;->a(Ljava/lang/String;I)I

    move-result v4

    .line 384000
    const-string v5, "marauder_tier"

    invoke-interface {p1, v5, v6}, LX/2Eu;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 384001
    if-nez v5, :cond_2

    .line 384002
    new-instance v0, LX/403;

    const-string v1, "marauder_tier is null or empty"

    invoke-direct {v0, v1}, LX/403;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384003
    :cond_2
    iput-object v0, p0, LX/2DI;->b:Ljava/lang/String;

    .line 384004
    iput-object v1, p0, LX/2DI;->c:Ljava/lang/String;

    .line 384005
    iput-object v2, p0, LX/2DI;->d:Ljava/lang/String;

    .line 384006
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/2DI;->a:Ljava/io/File;

    .line 384007
    invoke-static {}, LX/0pD;->values()[LX/0pD;

    move-result-object v0

    aget-object v0, v0, v4

    iput-object v0, p0, LX/2DI;->e:LX/0pD;

    .line 384008
    iput-object v5, p0, LX/2DI;->f:Ljava/lang/String;

    .line 384009
    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 383988
    new-instance v0, LX/2Et;

    invoke-direct {v0, p1}, LX/2Et;-><init>(Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, LX/2DI;-><init>(LX/2Eu;)V

    .line 383989
    return-void
.end method

.method public constructor <init>(Ljava/io/File;LX/0pC;)V
    .locals 2

    .prologue
    .line 383971
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383972
    iget-object v0, p2, LX/0pC;->a:Ljava/lang/Class;

    if-nez v0, :cond_0

    .line 383973
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "uploader required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383974
    :cond_0
    iget-object v0, p2, LX/0pC;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2DI;->b:Ljava/lang/String;

    .line 383975
    iget-object v0, p2, LX/0pC;->b:Ljava/lang/Class;

    if-eqz v0, :cond_1

    iget-object v0, p2, LX/0pC;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/2DI;->c:Ljava/lang/String;

    .line 383976
    iget-object v0, p2, LX/0pC;->c:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2DI;->d:Ljava/lang/String;

    .line 383977
    if-nez p1, :cond_2

    .line 383978
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "priorityDir required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383979
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 383980
    :cond_2
    iput-object p1, p0, LX/2DI;->a:Ljava/io/File;

    .line 383981
    iget-object v0, p2, LX/0pC;->d:LX/0pD;

    if-nez v0, :cond_3

    .line 383982
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "networkPriority required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383983
    :cond_3
    iget-object v0, p2, LX/0pC;->d:LX/0pD;

    iput-object v0, p0, LX/2DI;->e:LX/0pD;

    .line 383984
    iget-object v0, p2, LX/0pC;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 383985
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "marauderTier required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383986
    :cond_4
    iget-object v0, p2, LX/0pC;->e:Ljava/lang/String;

    iput-object v0, p0, LX/2DI;->f:Ljava/lang/String;

    .line 383987
    return-void
.end method


# virtual methods
.method public final a(LX/2Ev;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ev",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 383964
    const-string v0, "uploader_class"

    iget-object v1, p0, LX/2DI;->b:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, LX/2Ev;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 383965
    const-string v0, "flexible_sampling_updater"

    iget-object v1, p0, LX/2DI;->c:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, LX/2Ev;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 383966
    const-string v0, "thread_handler_factory"

    iget-object v1, p0, LX/2DI;->d:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, LX/2Ev;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 383967
    const-string v0, "priority_dir"

    iget-object v1, p0, LX/2DI;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/2Ev;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 383968
    const-string v0, "network_priority"

    iget-object v1, p0, LX/2DI;->e:LX/0pD;

    invoke-virtual {v1}, LX/0pD;->ordinal()I

    move-result v1

    invoke-interface {p1, v0, v1}, LX/2Ev;->b(Ljava/lang/String;I)V

    .line 383969
    const-string v0, "marauder_tier"

    iget-object v1, p0, LX/2DI;->f:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, LX/2Ev;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 383970
    invoke-interface {p1}, LX/2Ev;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
