.class public LX/3GZ;
.super Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;
.source ""


# instance fields
.field private final f:Landroid/view/View$OnClickListener;

.field public o:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 541191
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3GZ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 541192
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 541189
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3GZ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 541190
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 541185
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 541186
    new-instance v0, LX/3GU;

    invoke-direct {v0, p0}, LX/3GU;-><init>(LX/3GZ;)V

    iput-object v0, p0, LX/3GZ;->f:Landroid/view/View$OnClickListener;

    .line 541187
    const/4 v0, 0x0

    iput-object v0, p0, LX/3GZ;->o:Landroid/view/View$OnClickListener;

    .line 541188
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 541193
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/2v7;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCallToActionEndscreenReplayClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 541184
    iget-object v0, p0, LX/3GZ;->f:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 541183
    const/4 v0, 0x0

    return v0
.end method
