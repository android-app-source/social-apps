.class public LX/2um;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 476955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 65

    .prologue
    .line 476721
    const/16 v61, 0x0

    .line 476722
    const/16 v60, 0x0

    .line 476723
    const/16 v59, 0x0

    .line 476724
    const/16 v58, 0x0

    .line 476725
    const/16 v57, 0x0

    .line 476726
    const/16 v56, 0x0

    .line 476727
    const/16 v55, 0x0

    .line 476728
    const/16 v54, 0x0

    .line 476729
    const/16 v53, 0x0

    .line 476730
    const/16 v52, 0x0

    .line 476731
    const/16 v51, 0x0

    .line 476732
    const/16 v50, 0x0

    .line 476733
    const/16 v49, 0x0

    .line 476734
    const/16 v48, 0x0

    .line 476735
    const/16 v47, 0x0

    .line 476736
    const/16 v46, 0x0

    .line 476737
    const/16 v45, 0x0

    .line 476738
    const/16 v44, 0x0

    .line 476739
    const/16 v43, 0x0

    .line 476740
    const/16 v42, 0x0

    .line 476741
    const/16 v41, 0x0

    .line 476742
    const/16 v40, 0x0

    .line 476743
    const/16 v39, 0x0

    .line 476744
    const/16 v38, 0x0

    .line 476745
    const/16 v37, 0x0

    .line 476746
    const/16 v36, 0x0

    .line 476747
    const/16 v35, 0x0

    .line 476748
    const/16 v34, 0x0

    .line 476749
    const/16 v33, 0x0

    .line 476750
    const/16 v32, 0x0

    .line 476751
    const/16 v31, 0x0

    .line 476752
    const/16 v30, 0x0

    .line 476753
    const/16 v29, 0x0

    .line 476754
    const/16 v28, 0x0

    .line 476755
    const/16 v27, 0x0

    .line 476756
    const/16 v26, 0x0

    .line 476757
    const/16 v25, 0x0

    .line 476758
    const/16 v24, 0x0

    .line 476759
    const/16 v23, 0x0

    .line 476760
    const/16 v22, 0x0

    .line 476761
    const/16 v21, 0x0

    .line 476762
    const/16 v20, 0x0

    .line 476763
    const/16 v19, 0x0

    .line 476764
    const/16 v18, 0x0

    .line 476765
    const/16 v17, 0x0

    .line 476766
    const/16 v16, 0x0

    .line 476767
    const/4 v15, 0x0

    .line 476768
    const/4 v14, 0x0

    .line 476769
    const/4 v13, 0x0

    .line 476770
    const/4 v12, 0x0

    .line 476771
    const/4 v11, 0x0

    .line 476772
    const/4 v10, 0x0

    .line 476773
    const/4 v9, 0x0

    .line 476774
    const/4 v8, 0x0

    .line 476775
    const/4 v7, 0x0

    .line 476776
    const/4 v6, 0x0

    .line 476777
    const/4 v5, 0x0

    .line 476778
    const/4 v4, 0x0

    .line 476779
    const/4 v3, 0x0

    .line 476780
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v62

    sget-object v63, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v62

    move-object/from16 v1, v63

    if-eq v0, v1, :cond_1

    .line 476781
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 476782
    const/4 v3, 0x0

    .line 476783
    :goto_0
    return v3

    .line 476784
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 476785
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v62

    sget-object v63, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v62

    move-object/from16 v1, v63

    if-eq v0, v1, :cond_30

    .line 476786
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v62

    .line 476787
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 476788
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v63

    sget-object v64, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v63

    move-object/from16 v1, v64

    if-eq v0, v1, :cond_1

    if-eqz v62, :cond_1

    .line 476789
    const-string v63, "__type__"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-nez v63, :cond_2

    const-string v63, "__typename"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_3

    .line 476790
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v61

    invoke-virtual/range {v61 .. v61}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v61

    move-object/from16 v0, p1

    move-object/from16 v1, v61

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/String;)I

    move-result v61

    goto :goto_1

    .line 476791
    :cond_3
    const-string v63, "address"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_4

    .line 476792
    invoke-static/range {p0 .. p1}, LX/2t0;->a(LX/15w;LX/186;)I

    move-result v60

    goto :goto_1

    .line 476793
    :cond_4
    const-string v63, "can_viewer_claim"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_5

    .line 476794
    const/4 v15, 0x1

    .line 476795
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v59

    goto :goto_1

    .line 476796
    :cond_5
    const-string v63, "can_viewer_rate"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_6

    .line 476797
    const/4 v14, 0x1

    .line 476798
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v58

    goto :goto_1

    .line 476799
    :cond_6
    const-string v63, "category_icon"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_7

    .line 476800
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v57

    goto :goto_1

    .line 476801
    :cond_7
    const-string v63, "category_names"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_8

    .line 476802
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v56

    goto/16 :goto_1

    .line 476803
    :cond_8
    const-string v63, "category_type"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_9

    .line 476804
    const/4 v13, 0x1

    .line 476805
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v55 .. v55}, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v55

    goto/16 :goto_1

    .line 476806
    :cond_9
    const-string v63, "city"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_a

    .line 476807
    invoke-static/range {p0 .. p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v54

    goto/16 :goto_1

    .line 476808
    :cond_a
    const-string v63, "contextual_name"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_b

    .line 476809
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v53

    goto/16 :goto_1

    .line 476810
    :cond_b
    const-string v63, "does_viewer_like"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_c

    .line 476811
    const/4 v12, 0x1

    .line 476812
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v52

    goto/16 :goto_1

    .line 476813
    :cond_c
    const-string v63, "expressed_as_place"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_d

    .line 476814
    const/4 v11, 0x1

    .line 476815
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v51

    goto/16 :goto_1

    .line 476816
    :cond_d
    const-string v63, "full_name"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_e

    .line 476817
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v50

    goto/16 :goto_1

    .line 476818
    :cond_e
    const-string v63, "hours"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_f

    .line 476819
    invoke-static/range {p0 .. p1}, LX/4To;->b(LX/15w;LX/186;)I

    move-result v49

    goto/16 :goto_1

    .line 476820
    :cond_f
    const-string v63, "id"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_10

    .line 476821
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    goto/16 :goto_1

    .line 476822
    :cond_10
    const-string v63, "is_owned"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_11

    .line 476823
    const/4 v10, 0x1

    .line 476824
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v47

    goto/16 :goto_1

    .line 476825
    :cond_11
    const-string v63, "location"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_12

    .line 476826
    invoke-static/range {p0 .. p1}, LX/2sz;->a(LX/15w;LX/186;)I

    move-result v46

    goto/16 :goto_1

    .line 476827
    :cond_12
    const-string v63, "map_bounding_box"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_13

    .line 476828
    invoke-static/range {p0 .. p1}, LX/4NK;->a(LX/15w;LX/186;)I

    move-result v45

    goto/16 :goto_1

    .line 476829
    :cond_13
    const-string v63, "name"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_14

    .line 476830
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v44

    goto/16 :goto_1

    .line 476831
    :cond_14
    const-string v63, "overall_star_rating"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_15

    .line 476832
    invoke-static/range {p0 .. p1}, LX/2sB;->a(LX/15w;LX/186;)I

    move-result v43

    goto/16 :goto_1

    .line 476833
    :cond_15
    const-string v63, "page_likers"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_16

    .line 476834
    invoke-static/range {p0 .. p1}, LX/2bd;->a(LX/15w;LX/186;)I

    move-result v42

    goto/16 :goto_1

    .line 476835
    :cond_16
    const-string v63, "page_visits"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_17

    .line 476836
    invoke-static/range {p0 .. p1}, LX/2ui;->a(LX/15w;LX/186;)I

    move-result v41

    goto/16 :goto_1

    .line 476837
    :cond_17
    const-string v63, "permanently_closed_status"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_18

    .line 476838
    const/4 v9, 0x1

    .line 476839
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v40 .. v40}, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    move-result-object v40

    goto/16 :goto_1

    .line 476840
    :cond_18
    const-string v63, "placeProfilePicture"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_19

    .line 476841
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v39

    goto/16 :goto_1

    .line 476842
    :cond_19
    const-string v63, "place_open_status"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_1a

    .line 476843
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v38

    goto/16 :goto_1

    .line 476844
    :cond_1a
    const-string v63, "place_open_status_type"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_1b

    .line 476845
    const/4 v8, 0x1

    .line 476846
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v37 .. v37}, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v37

    goto/16 :goto_1

    .line 476847
    :cond_1b
    const-string v63, "place_topic_id"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_1c

    .line 476848
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    goto/16 :goto_1

    .line 476849
    :cond_1c
    const-string v63, "place_type"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_1d

    .line 476850
    const/4 v7, 0x1

    .line 476851
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Lcom/facebook/graphql/enums/GraphQLPlaceType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v35

    goto/16 :goto_1

    .line 476852
    :cond_1d
    const-string v63, "price_range_description"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_1e

    .line 476853
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v34

    goto/16 :goto_1

    .line 476854
    :cond_1e
    const-string v63, "profilePicture50"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_1f

    .line 476855
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v33

    goto/16 :goto_1

    .line 476856
    :cond_1f
    const-string v63, "profilePicture74"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_20

    .line 476857
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v32

    goto/16 :goto_1

    .line 476858
    :cond_20
    const-string v63, "profile_photo"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_21

    .line 476859
    invoke-static/range {p0 .. p1}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v31

    goto/16 :goto_1

    .line 476860
    :cond_21
    const-string v63, "profile_picture"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_22

    .line 476861
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v30

    goto/16 :goto_1

    .line 476862
    :cond_22
    const-string v63, "profile_picture_is_silhouette"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_23

    .line 476863
    const/4 v6, 0x1

    .line 476864
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto/16 :goto_1

    .line 476865
    :cond_23
    const-string v63, "raters"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_24

    .line 476866
    invoke-static/range {p0 .. p1}, LX/4QS;->a(LX/15w;LX/186;)I

    move-result v28

    goto/16 :goto_1

    .line 476867
    :cond_24
    const-string v63, "redirection_info"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_25

    .line 476868
    invoke-static/range {p0 .. p1}, LX/4SO;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 476869
    :cond_25
    const-string v63, "representative_place_photos"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_26

    .line 476870
    invoke-static/range {p0 .. p1}, LX/2sY;->b(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 476871
    :cond_26
    const-string v63, "saved_collection"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_27

    .line 476872
    invoke-static/range {p0 .. p1}, LX/39K;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 476873
    :cond_27
    const-string v63, "short_category_names"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_28

    .line 476874
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 476875
    :cond_28
    const-string v63, "should_show_reviews_on_profile"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_29

    .line 476876
    const/4 v5, 0x1

    .line 476877
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto/16 :goto_1

    .line 476878
    :cond_29
    const-string v63, "spotlight_locals_snippets"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_2a

    .line 476879
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 476880
    :cond_2a
    const-string v63, "super_category_type"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_2b

    .line 476881
    const/4 v4, 0x1

    .line 476882
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v21

    goto/16 :goto_1

    .line 476883
    :cond_2b
    const-string v63, "url"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_2c

    .line 476884
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 476885
    :cond_2c
    const-string v63, "viewer_profile_permissions"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_2d

    .line 476886
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 476887
    :cond_2d
    const-string v63, "viewer_saved_state"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_2e

    .line 476888
    const/4 v3, 0x1

    .line 476889
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/facebook/graphql/enums/GraphQLSavedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v18

    goto/16 :goto_1

    .line 476890
    :cond_2e
    const-string v63, "viewer_visits"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v63

    if-eqz v63, :cond_2f

    .line 476891
    invoke-static/range {p0 .. p1}, LX/2uj;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 476892
    :cond_2f
    const-string v63, "websites"

    invoke-virtual/range {v62 .. v63}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v62

    if-eqz v62, :cond_0

    .line 476893
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 476894
    :cond_30
    const/16 v62, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 476895
    const/16 v62, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v62

    move/from16 v2, v61

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 476896
    const/16 v61, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v61

    move/from16 v2, v60

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 476897
    if-eqz v15, :cond_31

    .line 476898
    const/4 v15, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v15, v1}, LX/186;->a(IZ)V

    .line 476899
    :cond_31
    if-eqz v14, :cond_32

    .line 476900
    const/4 v14, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v14, v1}, LX/186;->a(IZ)V

    .line 476901
    :cond_32
    const/4 v14, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v14, v1}, LX/186;->b(II)V

    .line 476902
    const/4 v14, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v14, v1}, LX/186;->b(II)V

    .line 476903
    if-eqz v13, :cond_33

    .line 476904
    const/4 v13, 0x6

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-virtual {v0, v13, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 476905
    :cond_33
    const/4 v13, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 476906
    const/16 v13, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 476907
    if-eqz v12, :cond_34

    .line 476908
    const/16 v12, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 476909
    :cond_34
    if-eqz v11, :cond_35

    .line 476910
    const/16 v11, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 476911
    :cond_35
    const/16 v11, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v11, v1}, LX/186;->b(II)V

    .line 476912
    const/16 v11, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v11, v1}, LX/186;->b(II)V

    .line 476913
    const/16 v11, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v11, v1}, LX/186;->b(II)V

    .line 476914
    if-eqz v10, :cond_36

    .line 476915
    const/16 v10, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 476916
    :cond_36
    const/16 v10, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 476917
    const/16 v10, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 476918
    const/16 v10, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 476919
    const/16 v10, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 476920
    const/16 v10, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 476921
    const/16 v10, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 476922
    if-eqz v9, :cond_37

    .line 476923
    const/16 v9, 0x15

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v9, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 476924
    :cond_37
    const/16 v9, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 476925
    const/16 v9, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 476926
    if-eqz v8, :cond_38

    .line 476927
    const/16 v8, 0x18

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v8, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 476928
    :cond_38
    const/16 v8, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 476929
    if-eqz v7, :cond_39

    .line 476930
    const/16 v7, 0x1a

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v7, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 476931
    :cond_39
    const/16 v7, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 476932
    const/16 v7, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 476933
    const/16 v7, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 476934
    const/16 v7, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 476935
    const/16 v7, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 476936
    if-eqz v6, :cond_3a

    .line 476937
    const/16 v6, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 476938
    :cond_3a
    const/16 v6, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 476939
    const/16 v6, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 476940
    const/16 v6, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 476941
    const/16 v6, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 476942
    const/16 v6, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 476943
    if-eqz v5, :cond_3b

    .line 476944
    const/16 v5, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 476945
    :cond_3b
    const/16 v5, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 476946
    if-eqz v4, :cond_3c

    .line 476947
    const/16 v4, 0x28

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 476948
    :cond_3c
    const/16 v4, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 476949
    const/16 v4, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 476950
    if-eqz v3, :cond_3d

    .line 476951
    const/16 v3, 0x2b

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 476952
    :cond_3d
    const/16 v3, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 476953
    const/16 v3, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 476954
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x18

    const/16 v5, 0x15

    const/4 v4, 0x6

    const/4 v3, 0x5

    const/4 v2, 0x0

    .line 476534
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 476535
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 476536
    if-eqz v0, :cond_0

    .line 476537
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476538
    invoke-static {p0, p1, v2, p2}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 476539
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476540
    if-eqz v0, :cond_1

    .line 476541
    const-string v1, "address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476542
    invoke-static {p0, v0, p2}, LX/2t0;->a(LX/15i;ILX/0nX;)V

    .line 476543
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 476544
    if-eqz v0, :cond_2

    .line 476545
    const-string v1, "can_viewer_claim"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476546
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 476547
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 476548
    if-eqz v0, :cond_3

    .line 476549
    const-string v1, "can_viewer_rate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476550
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 476551
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476552
    if-eqz v0, :cond_4

    .line 476553
    const-string v1, "category_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476554
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476555
    :cond_4
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 476556
    if-eqz v0, :cond_5

    .line 476557
    const-string v0, "category_names"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476558
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 476559
    :cond_5
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 476560
    if-eqz v0, :cond_6

    .line 476561
    const-string v0, "category_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476562
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476563
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476564
    if-eqz v0, :cond_7

    .line 476565
    const-string v1, "city"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476566
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 476567
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476568
    if-eqz v0, :cond_8

    .line 476569
    const-string v1, "contextual_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476570
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476571
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 476572
    if-eqz v0, :cond_9

    .line 476573
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476574
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 476575
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 476576
    if-eqz v0, :cond_a

    .line 476577
    const-string v1, "expressed_as_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476578
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 476579
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476580
    if-eqz v0, :cond_b

    .line 476581
    const-string v1, "full_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476582
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476583
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476584
    if-eqz v0, :cond_c

    .line 476585
    const-string v1, "hours"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476586
    invoke-static {p0, v0, p2, p3}, LX/4To;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 476587
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476588
    if-eqz v0, :cond_d

    .line 476589
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476590
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476591
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 476592
    if-eqz v0, :cond_e

    .line 476593
    const-string v1, "is_owned"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476594
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 476595
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476596
    if-eqz v0, :cond_f

    .line 476597
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476598
    invoke-static {p0, v0, p2}, LX/2sz;->a(LX/15i;ILX/0nX;)V

    .line 476599
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476600
    if-eqz v0, :cond_10

    .line 476601
    const-string v1, "map_bounding_box"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476602
    invoke-static {p0, v0, p2}, LX/4NK;->a(LX/15i;ILX/0nX;)V

    .line 476603
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476604
    if-eqz v0, :cond_11

    .line 476605
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476606
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476607
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476608
    if-eqz v0, :cond_12

    .line 476609
    const-string v1, "overall_star_rating"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476610
    invoke-static {p0, v0, p2}, LX/2sB;->a(LX/15i;ILX/0nX;)V

    .line 476611
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476612
    if-eqz v0, :cond_13

    .line 476613
    const-string v1, "page_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476614
    invoke-static {p0, v0, p2, p3}, LX/2bd;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 476615
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476616
    if-eqz v0, :cond_14

    .line 476617
    const-string v1, "page_visits"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476618
    invoke-static {p0, v0, p2}, LX/2ui;->a(LX/15i;ILX/0nX;)V

    .line 476619
    :cond_14
    invoke-virtual {p0, p1, v5, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 476620
    if-eqz v0, :cond_15

    .line 476621
    const-string v0, "permanently_closed_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476622
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476623
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476624
    if-eqz v0, :cond_16

    .line 476625
    const-string v1, "placeProfilePicture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476626
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476627
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476628
    if-eqz v0, :cond_17

    .line 476629
    const-string v1, "place_open_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476630
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 476631
    :cond_17
    invoke-virtual {p0, p1, v6, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 476632
    if-eqz v0, :cond_18

    .line 476633
    const-string v0, "place_open_status_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476634
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476635
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476636
    if-eqz v0, :cond_19

    .line 476637
    const-string v1, "place_topic_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476638
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476639
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 476640
    if-eqz v0, :cond_1a

    .line 476641
    const-string v0, "place_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476642
    const/16 v0, 0x1a

    const-class v1, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPlaceType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476643
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476644
    if-eqz v0, :cond_1b

    .line 476645
    const-string v1, "price_range_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476646
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476647
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476648
    if-eqz v0, :cond_1c

    .line 476649
    const-string v1, "profilePicture50"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476650
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476651
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476652
    if-eqz v0, :cond_1d

    .line 476653
    const-string v1, "profilePicture74"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476654
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476655
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476656
    if-eqz v0, :cond_1e

    .line 476657
    const-string v1, "profile_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476658
    invoke-static {p0, v0, p2, p3}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 476659
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476660
    if-eqz v0, :cond_1f

    .line 476661
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476662
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 476663
    :cond_1f
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 476664
    if-eqz v0, :cond_20

    .line 476665
    const-string v1, "profile_picture_is_silhouette"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476666
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 476667
    :cond_20
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476668
    if-eqz v0, :cond_21

    .line 476669
    const-string v1, "raters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476670
    invoke-static {p0, v0, p2}, LX/4QS;->a(LX/15i;ILX/0nX;)V

    .line 476671
    :cond_21
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476672
    if-eqz v0, :cond_22

    .line 476673
    const-string v1, "redirection_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476674
    invoke-static {p0, v0, p2, p3}, LX/4SO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 476675
    :cond_22
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476676
    if-eqz v0, :cond_23

    .line 476677
    const-string v1, "representative_place_photos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476678
    invoke-static {p0, v0, p2, p3}, LX/2sY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 476679
    :cond_23
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476680
    if-eqz v0, :cond_24

    .line 476681
    const-string v1, "saved_collection"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476682
    invoke-static {p0, v0, p2, p3}, LX/39K;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 476683
    :cond_24
    const/16 v0, 0x25

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476684
    if-eqz v0, :cond_25

    .line 476685
    const-string v0, "short_category_names"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476686
    const/16 v0, 0x25

    invoke-virtual {p0, p1, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 476687
    :cond_25
    const/16 v0, 0x26

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 476688
    if-eqz v0, :cond_26

    .line 476689
    const-string v1, "should_show_reviews_on_profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476690
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 476691
    :cond_26
    const/16 v0, 0x27

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476692
    if-eqz v0, :cond_27

    .line 476693
    const-string v0, "spotlight_locals_snippets"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476694
    const/16 v0, 0x27

    invoke-virtual {p0, p1, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 476695
    :cond_27
    const/16 v0, 0x28

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 476696
    if-eqz v0, :cond_28

    .line 476697
    const-string v0, "super_category_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476698
    const/16 v0, 0x28

    const-class v1, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476699
    :cond_28
    const/16 v0, 0x29

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 476700
    if-eqz v0, :cond_29

    .line 476701
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476702
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476703
    :cond_29
    const/16 v0, 0x2a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476704
    if-eqz v0, :cond_2a

    .line 476705
    const-string v0, "viewer_profile_permissions"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476706
    const/16 v0, 0x2a

    invoke-virtual {p0, p1, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 476707
    :cond_2a
    const/16 v0, 0x2b

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 476708
    if-eqz v0, :cond_2b

    .line 476709
    const-string v0, "viewer_saved_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476710
    const/16 v0, 0x2b

    const-class v1, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 476711
    :cond_2b
    const/16 v0, 0x2c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476712
    if-eqz v0, :cond_2c

    .line 476713
    const-string v1, "viewer_visits"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476714
    invoke-static {p0, v0, p2}, LX/2uj;->a(LX/15i;ILX/0nX;)V

    .line 476715
    :cond_2c
    const/16 v0, 0x2d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 476716
    if-eqz v0, :cond_2d

    .line 476717
    const-string v0, "websites"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 476718
    const/16 v0, 0x2d

    invoke-virtual {p0, p1, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 476719
    :cond_2d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 476720
    return-void
.end method
