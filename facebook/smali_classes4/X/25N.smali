.class public final LX/25N;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/25N;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/25Q;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/25P;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 369155
    const/4 v0, 0x0

    sput-object v0, LX/25N;->a:LX/25N;

    .line 369156
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/25N;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 369157
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 369158
    new-instance v0, LX/25P;

    invoke-direct {v0}, LX/25P;-><init>()V

    iput-object v0, p0, LX/25N;->c:LX/25P;

    .line 369159
    return-void
.end method

.method public static c(LX/1De;)LX/25Q;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 369160
    new-instance v1, LX/25O;

    invoke-direct {v1}, LX/25O;-><init>()V

    .line 369161
    sget-object v2, LX/25N;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/25Q;

    .line 369162
    if-nez v2, :cond_0

    .line 369163
    new-instance v2, LX/25Q;

    invoke-direct {v2}, LX/25Q;-><init>()V

    .line 369164
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/25Q;->a$redex0(LX/25Q;LX/1De;IILX/25O;)V

    .line 369165
    move-object v1, v2

    .line 369166
    move-object v0, v1

    .line 369167
    return-object v0
.end method

.method public static declared-synchronized q()LX/25N;
    .locals 2

    .prologue
    .line 369168
    const-class v1, LX/25N;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/25N;->a:LX/25N;

    if-nez v0, :cond_0

    .line 369169
    new-instance v0, LX/25N;

    invoke-direct {v0}, LX/25N;-><init>()V

    sput-object v0, LX/25N;->a:LX/25N;

    .line 369170
    :cond_0
    sget-object v0, LX/25N;->a:LX/25N;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 369171
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 369172
    check-cast p2, LX/25O;

    .line 369173
    iget v0, p2, LX/25O;->a:I

    .line 369174
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    sget-object p2, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, p2}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object p0

    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object p2

    invoke-virtual {p2, v0}, LX/1nh;->h(I)LX/1nh;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 369175
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 369176
    invoke-static {}, LX/1dS;->b()V

    .line 369177
    const/4 v0, 0x0

    return-object v0
.end method
