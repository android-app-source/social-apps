.class public LX/2CS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2CS;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0if;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 382908
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 382909
    iput-object p1, p0, LX/2CS;->a:LX/0Zb;

    .line 382910
    iput-object p2, p0, LX/2CS;->b:LX/0if;

    .line 382911
    return-void
.end method

.method public static a(LX/0QB;)LX/2CS;
    .locals 5

    .prologue
    .line 382895
    sget-object v0, LX/2CS;->c:LX/2CS;

    if-nez v0, :cond_1

    .line 382896
    const-class v1, LX/2CS;

    monitor-enter v1

    .line 382897
    :try_start_0
    sget-object v0, LX/2CS;->c:LX/2CS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 382898
    if-eqz v2, :cond_0

    .line 382899
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 382900
    new-instance p0, LX/2CS;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v4

    check-cast v4, LX/0if;

    invoke-direct {p0, v3, v4}, LX/2CS;-><init>(LX/0Zb;LX/0if;)V

    .line 382901
    move-object v0, p0

    .line 382902
    sput-object v0, LX/2CS;->c:LX/2CS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382903
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 382904
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 382905
    :cond_1
    sget-object v0, LX/2CS;->c:LX/2CS;

    return-object v0

    .line 382906
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 382907
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final d()V
    .locals 3

    .prologue
    .line 382892
    iget-object v0, p0, LX/2CS;->b:LX/0if;

    sget-object v1, LX/0ig;->e:LX/0ih;

    sget-object v2, LX/2Wl;->LOGIN_FINISH:LX/2Wl;

    invoke-virtual {v2}, LX/2Wl;->getEventName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 382893
    iget-object v0, p0, LX/2CS;->b:LX/0if;

    sget-object v1, LX/0ig;->f:LX/0ih;

    sget-object v2, LX/2Wl;->LOGIN_FINISH:LX/2Wl;

    invoke-virtual {v2}, LX/2Wl;->getEventName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 382894
    return-void
.end method
