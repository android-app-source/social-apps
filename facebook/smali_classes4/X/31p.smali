.class public LX/31p;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/31p;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/0en;

.field private final c:LX/0Wn;


# direct methods
.method public constructor <init>(LX/0en;LX/0Wn;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 488137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488138
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "i18n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, LX/31p;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/31p;->a:Ljava/lang/String;

    .line 488139
    iput-object p1, p0, LX/31p;->b:LX/0en;

    .line 488140
    iput-object p2, p0, LX/31p;->c:LX/0Wn;

    .line 488141
    return-void
.end method

.method public static a(LX/0QB;)LX/31p;
    .locals 5

    .prologue
    .line 488142
    sget-object v0, LX/31p;->d:LX/31p;

    if-nez v0, :cond_1

    .line 488143
    const-class v1, LX/31p;

    monitor-enter v1

    .line 488144
    :try_start_0
    sget-object v0, LX/31p;->d:LX/31p;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 488145
    if-eqz v2, :cond_0

    .line 488146
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 488147
    new-instance p0, LX/31p;

    invoke-static {v0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v3

    check-cast v3, LX/0en;

    invoke-static {v0}, LX/0Wn;->a(LX/0QB;)LX/0Wn;

    move-result-object v4

    check-cast v4, LX/0Wn;

    invoke-direct {p0, v3, v4}, LX/31p;-><init>(LX/0en;LX/0Wn;)V

    .line 488148
    move-object v0, p0

    .line 488149
    sput-object v0, LX/31p;->d:LX/31p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 488150
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 488151
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 488152
    :cond_1
    sget-object v0, LX/31p;->d:LX/31p;

    return-object v0

    .line 488153
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 488154
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/File;)V
    .locals 9

    .prologue
    .line 488155
    invoke-static {p1}, LX/0hW;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 488156
    invoke-static {v0}, LX/03l;->b([B)Ljava/lang/String;

    move-result-object v1

    .line 488157
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 488158
    iget-object v0, p0, LX/31p;->c:LX/0Wn;

    .line 488159
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "fbresources_corrupt_language_pack_download"

    invoke-direct {v6, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 488160
    new-instance v5, LX/0ff;

    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, LX/0ff;-><init>(Ljava/lang/String;)V

    .line 488161
    invoke-virtual {v5}, LX/0ff;->e()Ljava/lang/String;

    move-result-object v5

    .line 488162
    const-string v7, "locale"

    invoke-virtual {v6, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 488163
    const-string v5, "raw_file_size"

    invoke-virtual {p3}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-virtual {v6, v5, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 488164
    iget-object v5, v0, LX/0Wn;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-interface {v5, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 488165
    iget-object v0, p0, LX/31p;->a:Ljava/lang/String;

    const-string v1, "verifyAndSaveDownloadedFile(): downloaded (zipped) file checksum does not match for output file: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 488166
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Downloaded (zipped) file checksum does not match!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 488167
    :cond_0
    new-instance v1, Ljava/util/zip/ZipInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 488168
    invoke-virtual {v1}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    .line 488169
    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 488170
    iget-object v0, p0, LX/31p;->b:LX/0en;

    invoke-virtual {v0, v1, p3}, LX/0en;->a(Ljava/io/InputStream;Ljava/io/File;)V

    .line 488171
    return-void
.end method
