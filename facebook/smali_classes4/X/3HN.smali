.class public abstract LX/3HN;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/api/story/FetchSingleStoryParams;",
        "Lcom/facebook/api/story/FetchSingleStoryResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final b:Landroid/content/res/Resources;

.field public final c:LX/0sa;

.field public final d:LX/0rq;

.field public final e:LX/0sO;

.field public final f:LX/0se;

.field public final g:LX/0ad;

.field private final h:LX/0SG;

.field private i:LX/0tE;

.field private final j:LX/0tG;

.field private final k:LX/0tI;

.field private final l:LX/0wo;

.field private final m:LX/0sX;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0rq;LX/0sa;LX/0sO;LX/0se;LX/0SG;LX/0tE;LX/0tG;LX/0tI;LX/0wo;LX/0ad;LX/0sX;)V
    .locals 0

    .prologue
    .line 543327
    invoke-direct {p0, p4}, LX/0ro;-><init>(LX/0sO;)V

    .line 543328
    iput-object p1, p0, LX/3HN;->b:Landroid/content/res/Resources;

    .line 543329
    iput-object p2, p0, LX/3HN;->d:LX/0rq;

    .line 543330
    iput-object p3, p0, LX/3HN;->c:LX/0sa;

    .line 543331
    iput-object p4, p0, LX/3HN;->e:LX/0sO;

    .line 543332
    iput-object p5, p0, LX/3HN;->f:LX/0se;

    .line 543333
    iput-object p6, p0, LX/3HN;->h:LX/0SG;

    .line 543334
    iput-object p7, p0, LX/3HN;->i:LX/0tE;

    .line 543335
    iput-object p8, p0, LX/3HN;->j:LX/0tG;

    .line 543336
    iput-object p9, p0, LX/3HN;->k:LX/0tI;

    .line 543337
    iput-object p10, p0, LX/3HN;->l:LX/0wo;

    .line 543338
    iput-object p11, p0, LX/3HN;->g:LX/0ad;

    .line 543339
    iput-object p12, p0, LX/3HN;->m:LX/0sX;

    .line 543340
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/api/story/FetchSingleStoryParams;LX/0gW;)LX/0gW;
    .locals 4
    .param p1    # Lcom/facebook/api/story/FetchSingleStoryParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 543341
    const-string v0, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543342
    if-eqz p1, :cond_1

    .line 543343
    const-string v0, "node_id"

    iget-object v1, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->a:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543344
    const-string v0, "include_comments_disabled_fields"

    iget-boolean v1, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->i:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 543345
    invoke-static {p1}, LX/5Gm;->getQueryType(Lcom/facebook/api/story/FetchSingleStoryParams;)LX/5Gm;

    move-result-object v0

    sget-object v1, LX/5Gm;->COMMENTS_AND_LIKERS:LX/5Gm;

    if-ne v0, v1, :cond_1

    .line 543346
    const-string v0, "likers_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543347
    iget-object v0, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->f:LX/21y;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->f:LX/21y;

    sget-object v1, LX/21y;->DEFAULT_ORDER:LX/21y;

    invoke-virtual {v0, v1}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 543348
    const-string v0, "comment_order"

    iget-object v1, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->f:LX/21y;

    iget-object v1, v1, LX/21y;->toString:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543349
    :cond_0
    iget-object v0, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->h:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->h:Ljava/lang/String;

    .line 543350
    :goto_0
    if-eqz v0, :cond_4

    iget-object v1, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->f:LX/21y;

    sget-object v2, LX/21y;->THREADED_CHRONOLOGICAL_ORDER:LX/21y;

    if-ne v1, v2, :cond_4

    .line 543351
    const-string v1, "surround_comment_id"

    invoke-virtual {p2, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "num_before_surround"

    iget v2, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->e:I

    add-int/lit8 v2, v2, -0x1

    div-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "surround_max_comments"

    iget v2, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543352
    :cond_1
    :goto_1
    const-string v0, "angora_attachment_cover_image_size"

    iget-object v1, p0, LX/3HN;->c:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543353
    const-string v0, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543354
    const-string v0, "reading_attachment_profile_image_width"

    iget-object v1, p0, LX/3HN;->c:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->M()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543355
    const-string v0, "reading_attachment_profile_image_height"

    iget-object v1, p0, LX/3HN;->c:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->N()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543356
    const-string v0, "question_poll_count"

    .line 543357
    sget-object v1, LX/0sa;->c:Ljava/lang/Integer;

    move-object v1, v1

    .line 543358
    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543359
    const-string v0, "poll_voters_count"

    .line 543360
    sget-object v1, LX/0sa;->d:Ljava/lang/Integer;

    move-object v1, v1

    .line 543361
    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543362
    const-string v0, "poll_facepile_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543363
    const-string v0, "fetch_reshare_counts"

    iget-object v1, p0, LX/3HN;->g:LX/0ad;

    sget-short v2, LX/0wn;->aF:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 543364
    const-string v0, "automatic_photo_captioning_enabled"

    iget-object v1, p0, LX/3HN;->m:LX/0sX;

    invoke-virtual {v1}, LX/0sX;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543365
    const-string v0, "rich_text_posts_enabled"

    iget-boolean v1, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->k:Z

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543366
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v0

    .line 543367
    const-string v1, "default_image_scale"

    if-nez v0, :cond_2

    sget-object v0, LX/0wB;->a:LX/0wC;

    :cond_2
    invoke-virtual {p2, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 543368
    const-string v0, "action_location"

    sget-object v1, LX/0wD;->NEWSFEED:LX/0wD;

    invoke-virtual {v1}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543369
    invoke-virtual {p0, p1, p2}, LX/3HN;->b(Lcom/facebook/api/story/FetchSingleStoryParams;LX/0gW;)V

    .line 543370
    iget-object v0, p0, LX/3HN;->f:LX/0se;

    iget-object v1, p0, LX/3HN;->d:LX/0rq;

    invoke-virtual {v1}, LX/0rq;->c()LX/0wF;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 543371
    const-string v0, "image_large_aspect_height"

    iget-object v1, p0, LX/3HN;->c:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543372
    const-string v0, "image_large_aspect_width"

    iget-object v1, p0, LX/3HN;->c:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543373
    iget-object v0, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->h:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->g:Ljava/lang/String;

    .line 543374
    :goto_2
    iget-object v1, p0, LX/3HN;->i:LX/0tE;

    const/4 v2, 0x1

    invoke-virtual {v1, p2, v0, v2}, LX/0tE;->a(LX/0gW;Ljava/lang/String;Z)V

    .line 543375
    iget-object v0, p0, LX/3HN;->j:LX/0tG;

    invoke-virtual {v0, p2}, LX/0tG;->a(LX/0gW;)V

    .line 543376
    iget-object v0, p0, LX/3HN;->k:LX/0tI;

    invoke-virtual {v0, p2}, LX/0tI;->a(LX/0gW;)V

    .line 543377
    invoke-static {p2}, LX/0wo;->a(LX/0gW;)V

    .line 543378
    return-object p2

    .line 543379
    :cond_3
    iget-object v0, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 543380
    :cond_4
    const-string v1, "max_comments"

    iget v2, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543381
    iget-object v1, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->f:LX/21y;

    sget-object v2, LX/21y;->RANKED_ORDER:LX/21y;

    if-ne v1, v2, :cond_1

    .line 543382
    const-string v1, "comment_id"

    invoke-virtual {p2, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto/16 :goto_1

    .line 543383
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 543384
    iget-object v0, p0, LX/3HN;->e:LX/0sO;

    const-class v1, Lcom/facebook/graphql/model/GraphQLStory;

    const-string v2, "fetch_single_story"

    invoke-virtual {v0, p3, v1, v2}, LX/0sO;->a(LX/15w;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 543385
    iget-object v1, p0, LX/3HN;->h:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 543386
    invoke-static {v0, v2, v3}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 543387
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 543388
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(J)V

    .line 543389
    :cond_0
    new-instance v1, Lcom/facebook/api/story/FetchSingleStoryResult;

    sget-object v4, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-direct {v1, v0, v4, v2, v3}, Lcom/facebook/api/story/FetchSingleStoryResult;-><init>(Lcom/facebook/graphql/model/GraphQLStory;LX/0ta;J)V

    return-object v1
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 2

    .prologue
    .line 543390
    check-cast p1, Lcom/facebook/api/story/FetchSingleStoryParams;

    .line 543391
    iget-object v0, p1, Lcom/facebook/api/story/FetchSingleStoryParams;->d:LX/5Go;

    sget-object v1, LX/5Go;->GRAPHQL_PHOTO_CREATION_STORY:LX/5Go;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Lcom/facebook/api/story/FetchSingleStoryParams;LX/0gW;)V
    .locals 0
    .param p1    # Lcom/facebook/api/story/FetchSingleStoryParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 543392
    return-void
.end method
