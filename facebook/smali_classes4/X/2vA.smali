.class public LX/2vA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/32g;

.field public final b:LX/32i;

.field private final c:LX/1E1;


# direct methods
.method public constructor <init>(LX/32g;LX/32i;LX/1E1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 477578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 477579
    iput-object p1, p0, LX/2vA;->a:LX/32g;

    .line 477580
    iput-object p2, p0, LX/2vA;->b:LX/32i;

    .line 477581
    iput-object p3, p0, LX/2vA;->c:LX/1E1;

    .line 477582
    return-void
.end method

.method private static e(LX/2vA;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 477550
    if-nez p1, :cond_0

    .line 477551
    const/4 v0, 0x0

    .line 477552
    :goto_0
    return v0

    .line 477553
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 477554
    iget-object v0, p0, LX/2vA;->c:LX/1E1;

    const/4 v2, 0x0

    .line 477555
    iget-object v1, v0, LX/1E1;->a:LX/0ad;

    sget-short p0, LX/1Nu;->b:S

    invoke-interface {v1, p0, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, LX/1E1;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1kI;

    .line 477556
    iget-object p0, v1, LX/1kI;->a:LX/0Uh;

    sget p1, LX/1ko;->c:I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0Uh;->a(IZ)Z

    move-result p0

    move v1, p0

    .line 477557
    if-eqz v1, :cond_6

    :cond_1
    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 477558
    goto :goto_0

    .line 477559
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 477560
    iget-object v0, p0, LX/2vA;->c:LX/1E1;

    const/4 v1, 0x0

    .line 477561
    iget-object v2, v0, LX/1E1;->a:LX/0ad;

    sget-short p0, LX/1Nu;->c:S

    invoke-interface {v2, p0, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, v0, LX/1E1;->c:LX/0Uh;

    sget p0, LX/BMG;->a:I

    invoke-virtual {v2, p0, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    move v0, v1

    .line 477562
    goto :goto_0

    .line 477563
    :cond_5
    const/4 v0, 0x1

    goto :goto_0

    :cond_6
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final d(Ljava/lang/String;)Z
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 477564
    invoke-static {p0, p1}, LX/2vA;->e(LX/2vA;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 477565
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/2vA;->a:LX/32g;

    .line 477566
    if-eqz p1, :cond_4

    invoke-static {v1, p1}, LX/32g;->l(LX/32g;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v3, 0x0

    .line 477567
    invoke-static {v1, p1}, LX/32g;->h(LX/32g;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    .line 477568
    invoke-static {v1, p1}, LX/32g;->g(LX/32g;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    .line 477569
    if-eqz v4, :cond_2

    if-nez v5, :cond_5

    .line 477570
    :cond_2
    :goto_1
    move v2, v3

    .line 477571
    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    move v1, v2

    .line 477572
    if-nez v1, :cond_3

    iget-object v1, p0, LX/2vA;->b:LX/32i;

    .line 477573
    invoke-static {v1, p1}, LX/32i;->e(LX/32i;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    .line 477574
    if-eqz p1, :cond_6

    if-eqz v2, :cond_6

    invoke-static {v1, p1}, LX/32i;->h(LX/32i;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_6

    const/4 v2, 0x1

    :goto_3
    move v1, v2

    .line 477575
    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 477576
    :cond_5
    sget-object v6, LX/0SF;->a:LX/0SF;

    move-object v6, v6

    .line 477577
    invoke-virtual {v6}, LX/0SF;->a()J

    move-result-wide v7

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v5, v5

    sget-object v11, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v5, v6, v11}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v5

    add-long/2addr v5, v9

    cmp-long v4, v7, v5

    if-gez v4, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_3
.end method
