.class public LX/2e3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0oz;

.field public final b:LX/0ad;

.field public c:Z


# direct methods
.method public constructor <init>(LX/0oz;LX/0ad;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444916
    iput-object p1, p0, LX/2e3;->a:LX/0oz;

    .line 444917
    iput-object p2, p0, LX/2e3;->b:LX/0ad;

    .line 444918
    iget-object v0, p0, LX/2e3;->b:LX/0ad;

    sget-short v1, LX/2e4;->i:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2e3;->c:Z

    .line 444919
    return-void
.end method

.method public static a(LX/0QB;)LX/2e3;
    .locals 5

    .prologue
    .line 444920
    const-class v1, LX/2e3;

    monitor-enter v1

    .line 444921
    :try_start_0
    sget-object v0, LX/2e3;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444922
    sput-object v2, LX/2e3;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444923
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444924
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 444925
    new-instance p0, LX/2e3;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v3

    check-cast v3, LX/0oz;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/2e3;-><init>(LX/0oz;LX/0ad;)V

    .line 444926
    move-object v0, p0

    .line 444927
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444928
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2e3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444929
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444930
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final c()I
    .locals 3

    .prologue
    const/16 v0, 0xa

    .line 444931
    iget-boolean v1, p0, LX/2e3;->c:Z

    if-nez v1, :cond_0

    .line 444932
    :goto_0
    return v0

    .line 444933
    :cond_0
    sget-object v1, LX/33c;->a:[I

    iget-object v2, p0, LX/2e3;->a:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    invoke-virtual {v2}, LX/0p3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 444934
    :pswitch_0
    iget-object v1, p0, LX/2e3;->b:LX/0ad;

    sget v2, LX/2e4;->q:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0

    .line 444935
    :pswitch_1
    iget-object v1, p0, LX/2e3;->b:LX/0ad;

    sget v2, LX/2e4;->n:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0

    .line 444936
    :pswitch_2
    iget-object v1, p0, LX/2e3;->b:LX/0ad;

    sget v2, LX/2e4;->k:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
