.class public final LX/2FS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;
.implements LX/0YZ;


# instance fields
.field public final a:LX/69l;

.field private final b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/69o;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:LX/0Yb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/69l;LX/0Or;LX/0Xl;LX/0Or;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/69l;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "LX/69o;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 386907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386908
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2FS;->e:Z

    .line 386909
    iput-object p1, p0, LX/2FS;->a:LX/69l;

    .line 386910
    iput-object p2, p0, LX/2FS;->b:LX/0Or;

    .line 386911
    iput-object p3, p0, LX/2FS;->c:LX/0Xl;

    .line 386912
    iput-object p4, p0, LX/2FS;->d:LX/0Or;

    .line 386913
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 386884
    iget-object v0, p0, LX/2FS;->c:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2FS;->f:LX/0Yb;

    .line 386885
    iget-object v0, p0, LX/2FS;->f:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 386886
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 386901
    iget-boolean v0, p0, LX/2FS;->e:Z

    if-nez v0, :cond_0

    .line 386902
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2FS;->e:Z

    .line 386903
    iget-object v0, p0, LX/2FS;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/69o;

    .line 386904
    iget-object v1, v0, LX/69o;->f:LX/1fN;

    new-instance p0, LX/69n;

    invoke-direct {p0, v0}, LX/69n;-><init>(LX/69o;)V

    invoke-virtual {v1, p0}, LX/1fN;->a(LX/69n;)V

    .line 386905
    invoke-virtual {v0}, LX/69o;->b()V

    .line 386906
    :cond_0
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 1

    .prologue
    .line 386895
    iget-object v0, p0, LX/2FS;->a:LX/69l;

    invoke-virtual {v0}, LX/69l;->a()Z

    move-result v0

    move v0, v0

    .line 386896
    if-nez v0, :cond_0

    .line 386897
    :goto_0
    return-void

    .line 386898
    :cond_0
    iget-object v0, p0, LX/2FS;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 386899
    invoke-direct {p0}, LX/2FS;->b()V

    goto :goto_0

    .line 386900
    :cond_1
    invoke-direct {p0}, LX/2FS;->a()V

    goto :goto_0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x7472bb6b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 386887
    iget-object v1, p0, LX/2FS;->f:LX/0Yb;

    if-eqz v1, :cond_0

    .line 386888
    iget-object v1, p0, LX/2FS;->f:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 386889
    :cond_0
    iget-object v1, p0, LX/2FS;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 386890
    invoke-direct {p0}, LX/2FS;->a()V

    .line 386891
    const/16 v1, 0x27

    const v2, 0x307ffc40

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 386892
    :goto_0
    return-void

    .line 386893
    :cond_1
    invoke-direct {p0}, LX/2FS;->b()V

    .line 386894
    const v1, 0x684fb11

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
