.class public LX/2Dt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .param p1    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384764
    iput-object p1, p0, LX/2Dt;->a:LX/0Uh;

    .line 384765
    return-void
.end method

.method public static b(LX/0QB;)LX/2Dt;
    .locals 2

    .prologue
    .line 384771
    new-instance v1, LX/2Dt;

    invoke-static {p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v0}, LX/2Dt;-><init>(LX/0Uh;)V

    .line 384772
    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 384770
    iget-object v0, p0, LX/2Dt;->a:LX/0Uh;

    const/16 v1, 0x6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 384768
    iget-object v0, p0, LX/2Dt;->a:LX/0Uh;

    const/16 v1, 0x1c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 384769
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/2Dt;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 384767
    iget-object v0, p0, LX/2Dt;->a:LX/0Uh;

    const/16 v1, 0x1d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 384766
    iget-object v0, p0, LX/2Dt;->a:LX/0Uh;

    const/16 v1, 0x1b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
