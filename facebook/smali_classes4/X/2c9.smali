.class public LX/2c9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0if;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 440189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440190
    iput-object p1, p0, LX/2c9;->a:LX/0Zb;

    .line 440191
    iput-object p2, p0, LX/2c9;->b:LX/0if;

    .line 440192
    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 440185
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "privacy"

    .line 440186
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 440187
    move-object v0, v0

    .line 440188
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 440210
    invoke-static {p0}, LX/2c9;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "surface"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "education_type"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 440209
    if-eqz p0, :cond_0

    const-string v0, "blackbird"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "typeahead"

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2c9;
    .locals 3

    .prologue
    .line 440207
    new-instance v2, LX/2c9;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v1

    check-cast v1, LX/0if;

    invoke-direct {v2, v0, v1}, LX/2c9;-><init>(LX/0Zb;LX/0if;)V

    .line 440208
    return-object v2
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 440205
    iget-object v0, p0, LX/2c9;->a:LX/0Zb;

    const-string v1, "privacy_selector_specific_friends_open"

    invoke-static {v1}, LX/2c9;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "num_inclusions"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 440206
    return-void
.end method

.method public final a(LX/2Zv;)V
    .locals 2

    .prologue
    .line 440211
    iget-object v0, p0, LX/2c9;->a:LX/0Zb;

    iget-object v1, p1, LX/2Zv;->eventName:Ljava/lang/String;

    invoke-static {v1}, LX/2c9;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 440212
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;Z)V
    .locals 5

    .prologue
    .line 440201
    iget-object v0, p0, LX/2c9;->b:LX/0if;

    sget-object v1, LX/0ig;->aC:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 440202
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "optionType"

    invoke-static {p1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "option"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 440203
    iget-object v1, p0, LX/2c9;->b:LX/0if;

    sget-object v2, LX/0ig;->aC:LX/0ih;

    const-string v3, "open_audience_selector"

    invoke-static {p2}, LX/2c9;->a(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 440204
    return-void
.end method

.method public final a(Ljava/lang/String;ZLX/1rQ;)V
    .locals 3
    .param p3    # LX/1rQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 440199
    iget-object v0, p0, LX/2c9;->b:LX/0if;

    sget-object v1, LX/0ig;->aC:LX/0ih;

    invoke-static {p2}, LX/2c9;->a(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2, p3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 440200
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 440197
    iget-object v0, p0, LX/2c9;->a:LX/0Zb;

    const-string v1, "privacy_selector_specific_friends_close"

    invoke-static {v1}, LX/2c9;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "num_inclusions"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 440198
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;Z)V
    .locals 5

    .prologue
    .line 440193
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "optionType"

    invoke-static {p1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "option"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 440194
    iget-object v1, p0, LX/2c9;->b:LX/0if;

    sget-object v2, LX/0ig;->aC:LX/0ih;

    const-string v3, "close_audience_selector"

    invoke-static {p2}, LX/2c9;->a(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 440195
    iget-object v0, p0, LX/2c9;->b:LX/0if;

    sget-object v1, LX/0ig;->aC:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 440196
    return-void
.end method
