.class public final LX/2RO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2RL;


# instance fields
.field public final c:Landroid/net/Uri;

.field public final synthetic d:LX/2RE;


# direct methods
.method public constructor <init>(LX/2RE;)V
    .locals 2

    .prologue
    .line 409771
    iput-object p1, p0, LX/2RO;->d:LX/2RE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409772
    iget-object v0, p0, LX/2RO;->d:LX/2RE;

    iget-object v0, v0, LX/2RE;->b:Landroid/net/Uri;

    const-string v1, "search"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/2RO;->c:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)LX/6Lq;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    .line 409773
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    .line 409774
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v6, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v7, :cond_0

    .line 409775
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid uri"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 409776
    :cond_0
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 409777
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 409778
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v3

    .line 409779
    array-length v4, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v2, v0

    .line 409780
    invoke-static {v5}, LX/0PH;->valueOf(Ljava/lang/String;)LX/0PH;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 409781
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 409782
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v7, :cond_2

    const-string v0, ""

    .line 409783
    :goto_1
    new-instance v1, LX/6Lq;

    iget-object v2, p0, LX/2RO;->d:LX/2RE;

    invoke-virtual {v3}, LX/0cA;->b()LX/0Rf;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, LX/6Lq;-><init>(LX/2RE;Ljava/lang/String;LX/0Rf;)V

    return-object v1

    .line 409784
    :cond_2
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1
.end method
