.class public LX/2Xs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/335;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/2A0;

.field public final d:LX/2Bu;

.field public final e:LX/11Q;

.field public final f:LX/28W;

.field public final g:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public constructor <init>(Ljava/util/Set;Ljava/util/concurrent/ExecutorService;LX/2A0;LX/2Bu;LX/11Q;LX/28W;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/335;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/2A0;",
            "LX/2Bu;",
            "LX/11Q;",
            "LX/28W;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 420367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420368
    iput-object p1, p0, LX/2Xs;->a:Ljava/util/Set;

    .line 420369
    iput-object p2, p0, LX/2Xs;->b:Ljava/util/concurrent/ExecutorService;

    .line 420370
    iput-object p3, p0, LX/2Xs;->c:LX/2A0;

    .line 420371
    iput-object p4, p0, LX/2Xs;->d:LX/2Bu;

    .line 420372
    iput-object p5, p0, LX/2Xs;->e:LX/11Q;

    .line 420373
    iput-object p6, p0, LX/2Xs;->f:LX/28W;

    .line 420374
    iput-object p7, p0, LX/2Xs;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 420375
    return-void
.end method

.method public static a(LX/0QB;)LX/2Xs;
    .locals 15

    .prologue
    .line 420361
    new-instance v1, LX/2Xs;

    invoke-static {p0}, LX/28V;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v2

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/29y;->a(LX/0QB;)LX/2A0;

    move-result-object v4

    check-cast v4, LX/2A0;

    .line 420362
    new-instance v9, LX/2Bu;

    invoke-static {p0}, LX/2Xj;->a(LX/0QB;)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xb7d

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x1416

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v13

    check-cast v13, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v14, 0x259

    invoke-static {p0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v9 .. v14}, LX/2Bu;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;)V

    .line 420363
    move-object v5, v9

    .line 420364
    check-cast v5, LX/2Bu;

    invoke-static {p0}, LX/11Q;->a(LX/0QB;)LX/11Q;

    move-result-object v6

    check-cast v6, LX/11Q;

    invoke-static {p0}, LX/28W;->a(LX/0QB;)LX/28W;

    move-result-object v7

    check-cast v7, LX/28W;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v8

    check-cast v8, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct/range {v1 .. v8}, LX/2Xs;-><init>(Ljava/util/Set;Ljava/util/concurrent/ExecutorService;LX/2A0;LX/2Bu;LX/11Q;LX/28W;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 420365
    move-object v0, v1

    .line 420366
    return-object v0
.end method
