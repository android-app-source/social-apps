.class public LX/2a8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1fT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/2a8;


# instance fields
.field private final b:LX/2a9;

.field public final c:LX/0lC;

.field private final d:LX/0WJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 423397
    const-class v0, LX/2a8;

    sput-object v0, LX/2a8;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2a9;LX/0lC;LX/0WJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 423398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 423399
    iput-object p1, p0, LX/2a8;->b:LX/2a9;

    .line 423400
    iput-object p2, p0, LX/2a8;->c:LX/0lC;

    .line 423401
    iput-object p3, p0, LX/2a8;->d:LX/0WJ;

    .line 423402
    return-void
.end method

.method public static a(LX/0QB;)LX/2a8;
    .locals 6

    .prologue
    .line 423403
    sget-object v0, LX/2a8;->e:LX/2a8;

    if-nez v0, :cond_1

    .line 423404
    const-class v1, LX/2a8;

    monitor-enter v1

    .line 423405
    :try_start_0
    sget-object v0, LX/2a8;->e:LX/2a8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 423406
    if-eqz v2, :cond_0

    .line 423407
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 423408
    new-instance p0, LX/2a8;

    invoke-static {v0}, LX/2a9;->a(LX/0QB;)LX/2a9;

    move-result-object v3

    check-cast v3, LX/2a9;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v5

    check-cast v5, LX/0WJ;

    invoke-direct {p0, v3, v4, v5}, LX/2a8;-><init>(LX/2a9;LX/0lC;LX/0WJ;)V

    .line 423409
    move-object v0, p0

    .line 423410
    sput-object v0, LX/2a8;->e:LX/2a8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423411
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 423412
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 423413
    :cond_1
    sget-object v0, LX/2a8;->e:LX/2a8;

    return-object v0

    .line 423414
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 423415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a([B)LX/3lm;
    .locals 5

    .prologue
    .line 423416
    new-instance v0, LX/1sp;

    invoke-direct {v0}, LX/1sp;-><init>()V

    .line 423417
    new-instance v1, LX/1sr;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    const/4 v3, 0x0

    array-length v4, p0

    invoke-direct {v2, p0, v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    invoke-direct {v1, v2}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v0, v1}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    .line 423418
    :try_start_0
    invoke-static {v0}, LX/3ll;->b(LX/1su;)LX/3ll;

    .line 423419
    invoke-static {v0}, LX/3lm;->b(LX/1su;)LX/3lm;
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 423420
    return-object v0

    .line 423421
    :catch_0
    move-exception v0

    .line 423422
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(LX/0lF;Ljava/lang/String;)V
    .locals 16

    .prologue
    .line 423423
    const-string v2, "full"

    const-string v3, "list_type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    .line 423424
    const-string v2, "list"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 423425
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 423426
    invoke-virtual {v2}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0lF;

    .line 423427
    const-string v3, "u"

    invoke-virtual {v2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->c(LX/0lF;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    .line 423428
    const-string v3, "p"

    invoke-virtual {v2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->d(LX/0lF;)I

    move-result v13

    .line 423429
    const-string v3, "l"

    invoke-virtual {v2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    const-wide/16 v6, -0x1

    invoke-static {v3, v6, v7}, LX/16N;->a(LX/0lF;J)J

    move-result-wide v6

    .line 423430
    const-string v3, "d"

    invoke-virtual {v2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, LX/16N;->a(LX/0lF;I)I

    move-result v8

    .line 423431
    const-wide/16 v14, 0x0

    cmp-long v3, v6, v14

    if-nez v3, :cond_0

    .line 423432
    const-wide/16 v6, -0x1

    .line 423433
    :cond_0
    const-string v3, "vc"

    invoke-virtual {v2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 423434
    const/4 v9, 0x0

    .line 423435
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/0lF;->q()Z

    move-result v3

    if-nez v3, :cond_1

    .line 423436
    invoke-static {v2}, LX/16N;->c(LX/0lF;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 423437
    :cond_1
    new-instance v3, Lcom/facebook/presence/PresenceItem;

    new-instance v4, Lcom/facebook/user/model/UserKey;

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v4, v2, v5}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    const/4 v2, 0x2

    if-ne v13, v2, :cond_2

    const/4 v5, 0x1

    :goto_1
    invoke-direct/range {v3 .. v9}, Lcom/facebook/presence/PresenceItem;-><init>(Lcom/facebook/user/model/UserKey;ZJILjava/lang/Long;)V

    .line 423438
    invoke-virtual {v11, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 423439
    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    .line 423440
    :cond_3
    new-instance v2, Lcom/facebook/presence/PresenceList;

    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/presence/PresenceList;-><init>(LX/0Px;)V

    .line 423441
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2a8;->b:LX/2a9;

    move-object/from16 v0, p2

    invoke-virtual {v3, v0, v2, v10}, LX/2a9;->a(Ljava/lang/String;Lcom/facebook/presence/PresenceList;Z)V

    .line 423442
    return-void
.end method

.method private a(LX/3lm;Ljava/lang/String;)V
    .locals 16

    .prologue
    .line 423443
    move-object/from16 v0, p1

    iget-object v2, v0, LX/3lm;->isIncrementalUpdate:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    move v10, v2

    .line 423444
    :goto_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 423445
    move-object/from16 v0, p1

    iget-object v2, v0, LX/3lm;->updates:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6mY;

    .line 423446
    iget-object v3, v2, LX/6mY;->uid:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v5

    .line 423447
    iget-object v3, v2, LX/6mY;->state:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 423448
    iget-object v3, v2, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    if-eqz v3, :cond_0

    iget-object v3, v2, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v14, 0x0

    cmp-long v3, v6, v14

    if-nez v3, :cond_2

    :cond_0
    const-wide/16 v6, -0x1

    .line 423449
    :goto_2
    iget-object v3, v2, LX/6mY;->detailedClientPresence:Ljava/lang/Short;

    if-nez v3, :cond_3

    const/4 v8, 0x0

    .line 423450
    :goto_3
    new-instance v3, Lcom/facebook/presence/PresenceItem;

    new-instance v4, Lcom/facebook/user/model/UserKey;

    sget-object v13, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v4, v13, v5}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    const/4 v5, 0x1

    if-ne v9, v5, :cond_4

    const/4 v5, 0x1

    :goto_4
    iget-object v9, v2, LX/6mY;->voipCapabilities:Ljava/lang/Long;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/presence/PresenceItem;-><init>(Lcom/facebook/user/model/UserKey;ZJILjava/lang/Long;)V

    .line 423451
    invoke-virtual {v11, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 423452
    :cond_1
    const/4 v2, 0x0

    move v10, v2

    goto :goto_0

    .line 423453
    :cond_2
    iget-object v3, v2, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    goto :goto_2

    .line 423454
    :cond_3
    iget-object v3, v2, LX/6mY;->detailedClientPresence:Ljava/lang/Short;

    invoke-virtual {v3}, Ljava/lang/Short;->shortValue()S

    move-result v8

    goto :goto_3

    .line 423455
    :cond_4
    const/4 v5, 0x0

    goto :goto_4

    .line 423456
    :cond_5
    new-instance v2, Lcom/facebook/presence/PresenceList;

    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/presence/PresenceList;-><init>(LX/0Px;)V

    .line 423457
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2a8;->b:LX/2a9;

    move-object/from16 v0, p2

    invoke-virtual {v3, v0, v2, v10}, LX/2a9;->a(Ljava/lang/String;Lcom/facebook/presence/PresenceList;Z)V

    .line 423458
    return-void
.end method


# virtual methods
.method public final onMessage(Ljava/lang/String;[BJ)V
    .locals 1

    .prologue
    .line 423459
    :try_start_0
    iget-object v0, p0, LX/2a8;->d:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 423460
    :cond_0
    :goto_0
    return-void

    .line 423461
    :cond_1
    const-string v0, "/orca_presence"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 423462
    iget-object v0, p0, LX/2a8;->c:LX/0lC;

    invoke-static {p2}, LX/0YN;->a([B)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 423463
    const/4 p3, 0x2

    invoke-static {p3}, LX/01m;->b(I)Z

    .line 423464
    move-object v0, v0

    .line 423465
    invoke-direct {p0, v0, p1}, LX/2a8;->a(LX/0lF;Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    goto :goto_0

    .line 423466
    :cond_2
    const-string v0, "/t_p"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "/t_sp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423467
    :cond_3
    invoke-static {p2}, LX/2a8;->a([B)LX/3lm;

    move-result-object v0

    invoke-direct {p0, v0, p1}, LX/2a8;->a(LX/3lm;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method
