.class public LX/2Dg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/2Dg;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0SG;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/0lC;

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/aldrin/status/AldrinUserStatus;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/aldrin/status/AldrinUserStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z


# direct methods
.method public constructor <init>(LX/0Or;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0SG;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0lC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384557
    iput-object p1, p0, LX/2Dg;->a:LX/0Or;

    .line 384558
    iput-object p2, p0, LX/2Dg;->b:LX/0SG;

    .line 384559
    iput-object p3, p0, LX/2Dg;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 384560
    iput-object p4, p0, LX/2Dg;->d:LX/0lC;

    .line 384561
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2Dg;->e:Ljava/util/Map;

    .line 384562
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Dg;->f:Lcom/facebook/aldrin/status/AldrinUserStatus;

    .line 384563
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Dg;->g:Z

    .line 384564
    return-void
.end method

.method public static a(LX/0QB;)LX/2Dg;
    .locals 7

    .prologue
    .line 384572
    sget-object v0, LX/2Dg;->h:LX/2Dg;

    if-nez v0, :cond_1

    .line 384573
    const-class v1, LX/2Dg;

    monitor-enter v1

    .line 384574
    :try_start_0
    sget-object v0, LX/2Dg;->h:LX/2Dg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 384575
    if-eqz v2, :cond_0

    .line 384576
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 384577
    new-instance v6, LX/2Dg;

    const/16 v3, 0x12cb

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lC;

    invoke-direct {v6, p0, v3, v4, v5}, LX/2Dg;-><init>(LX/0Or;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;)V

    .line 384578
    move-object v0, v6

    .line 384579
    sput-object v0, LX/2Dg;->h:LX/2Dg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384580
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 384581
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 384582
    :cond_1
    sget-object v0, LX/2Dg;->h:LX/2Dg;

    return-object v0

    .line 384583
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 384584
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(Lcom/facebook/user/model/User;Z)Lcom/facebook/aldrin/status/AldrinUserStatus;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 384565
    monitor-enter p0

    if-nez p1, :cond_1

    :try_start_0
    iget-object v0, p0, LX/2Dg;->f:Lcom/facebook/aldrin/status/AldrinUserStatus;

    .line 384566
    :goto_0
    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    invoke-static {p0, v0}, LX/2Dg;->b(LX/2Dg;Lcom/facebook/aldrin/status/AldrinUserStatus;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 384567
    const/4 v0, 0x0

    .line 384568
    :cond_0
    monitor-exit p0

    return-object v0

    .line 384569
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2Dg;->e:Ljava/util/Map;

    .line 384570
    iget-object v1, p1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 384571
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/aldrin/status/AldrinUserStatus;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(LX/2Dg;Lcom/facebook/aldrin/status/AldrinUserStatus;)Z
    .locals 4

    .prologue
    .line 384547
    iget-object v0, p0, LX/2Dg;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p1, Lcom/facebook/aldrin/status/AldrinUserStatus;->fetchTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized f()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/aldrin/status/AldrinUserStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 384548
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 384549
    iget-object v0, p0, LX/2Dg;->f:Lcom/facebook/aldrin/status/AldrinUserStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Dg;->f:Lcom/facebook/aldrin/status/AldrinUserStatus;

    invoke-static {p0, v0}, LX/2Dg;->b(LX/2Dg;Lcom/facebook/aldrin/status/AldrinUserStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 384550
    iget-object v0, p0, LX/2Dg;->f:Lcom/facebook/aldrin/status/AldrinUserStatus;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384551
    :cond_0
    iget-object v0, p0, LX/2Dg;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/aldrin/status/AldrinUserStatus;

    .line 384552
    if-eqz v0, :cond_1

    invoke-static {p0, v0}, LX/2Dg;->b(LX/2Dg;Lcom/facebook/aldrin/status/AldrinUserStatus;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 384553
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 384554
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 384555
    :cond_2
    monitor-exit p0

    return-object v1
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/aldrin/status/AldrinUserStatus;)V
    .locals 6

    .prologue
    .line 384527
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/2Dg;->g:Z

    .line 384528
    if-eqz p1, :cond_0

    invoke-static {p0, p1}, LX/2Dg;->b(LX/2Dg;Lcom/facebook/aldrin/status/AldrinUserStatus;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 384529
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 384530
    :cond_1
    :try_start_1
    iget-object v1, p1, Lcom/facebook/aldrin/status/AldrinUserStatus;->userId:Ljava/lang/String;

    .line 384531
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 384532
    iget-object v0, p0, LX/2Dg;->f:Lcom/facebook/aldrin/status/AldrinUserStatus;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/2Dg;->f:Lcom/facebook/aldrin/status/AldrinUserStatus;

    iget-wide v0, v0, Lcom/facebook/aldrin/status/AldrinUserStatus;->fetchTime:J

    iget-wide v2, p1, Lcom/facebook/aldrin/status/AldrinUserStatus;->fetchTime:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 384533
    :cond_2
    iput-object p1, p0, LX/2Dg;->f:Lcom/facebook/aldrin/status/AldrinUserStatus;

    .line 384534
    :cond_3
    :goto_1
    iget-object v0, p0, LX/2Dg;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 384535
    if-nez v0, :cond_6

    const/4 v0, 0x0

    move-object v2, v0

    .line 384536
    :goto_2
    iget-object v0, p0, LX/2Dg;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 384537
    iget-object v1, p0, LX/2Dg;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/aldrin/status/AldrinUserStatus;

    .line 384538
    if-eqz v1, :cond_5

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static {p0, v1}, LX/2Dg;->b(LX/2Dg;Lcom/facebook/aldrin/status/AldrinUserStatus;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 384539
    :cond_5
    iget-object v1, p0, LX/2Dg;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 384540
    :cond_6
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 384541
    move-object v2, v0

    goto :goto_2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384542
    :cond_7
    goto :goto_0

    .line 384543
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 384544
    :cond_8
    :try_start_2
    iget-object v0, p0, LX/2Dg;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/aldrin/status/AldrinUserStatus;

    .line 384545
    if-eqz v0, :cond_9

    iget-wide v2, v0, Lcom/facebook/aldrin/status/AldrinUserStatus;->fetchTime:J

    iget-wide v4, p1, Lcom/facebook/aldrin/status/AldrinUserStatus;->fetchTime:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_3

    .line 384546
    :cond_9
    iget-object v0, p0, LX/2Dg;->e:Ljava/util/Map;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 384522
    :try_start_0
    iget-object v0, p0, LX/2Dg;->d:LX/0lC;

    invoke-direct {p0}, LX/2Dg;->f()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 384523
    iget-object v1, p0, LX/2Dg;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2Dh;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 384524
    :goto_0
    return-void

    .line 384525
    :catch_0
    move-exception v0

    .line 384526
    const-class v1, LX/2Dg;

    const-string v2, "Cannot persist status data"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final d()Lcom/facebook/aldrin/status/AldrinUserStatus;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 384521
    iget-object v0, p0, LX/2Dg;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/2Dg;->a(Lcom/facebook/user/model/User;Z)Lcom/facebook/aldrin/status/AldrinUserStatus;

    move-result-object v0

    return-object v0
.end method

.method public final init()V
    .locals 5

    .prologue
    .line 384509
    iget-boolean v0, p0, LX/2Dg;->g:Z

    if-nez v0, :cond_0

    .line 384510
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2Dg;->g:Z

    .line 384511
    iget-object v0, p0, LX/2Dg;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2Dh;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 384512
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 384513
    :cond_0
    :goto_0
    return-void

    .line 384514
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/2Dg;->d:LX/0lC;

    iget-object v2, p0, LX/2Dg;->d:LX/0lC;

    .line 384515
    iget-object v3, v2, LX/0lC;->_typeFactory:LX/0li;

    move-object v2, v3

    .line 384516
    const-class v3, Ljava/util/List;

    const-class v4, Lcom/facebook/aldrin/status/AldrinUserStatus;

    invoke-virtual {v2, v3, v4}, LX/0li;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/267;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(Ljava/lang/String;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 384517
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/aldrin/status/AldrinUserStatus;

    .line 384518
    invoke-virtual {p0, v0}, LX/2Dg;->a(Lcom/facebook/aldrin/status/AldrinUserStatus;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 384519
    :catch_0
    move-exception v0

    .line 384520
    const-class v1, LX/2Dg;

    const-string v2, "Cannot restore status data"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
