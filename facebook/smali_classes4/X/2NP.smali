.class public final enum LX/2NP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2NP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2NP;

.field public static final enum FETCHING_SSO_DATA:LX/2NP;

.field public static final enum LOGGING_IN:LX/2NP;

.field public static final enum LOGIN_APPPROVAL_UI:LX/2NP;

.field public static final enum PASSWORD_AUTH_UI:LX/2NP;

.field public static final enum SPLASHSCREEN:LX/2NP;

.field public static final enum SSO_AUTH_UI:LX/2NP;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 399799
    new-instance v0, LX/2NP;

    const-string v1, "SPLASHSCREEN"

    invoke-direct {v0, v1, v3}, LX/2NP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2NP;->SPLASHSCREEN:LX/2NP;

    .line 399800
    new-instance v0, LX/2NP;

    const-string v1, "FETCHING_SSO_DATA"

    invoke-direct {v0, v1, v4}, LX/2NP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2NP;->FETCHING_SSO_DATA:LX/2NP;

    .line 399801
    new-instance v0, LX/2NP;

    const-string v1, "SSO_AUTH_UI"

    invoke-direct {v0, v1, v5}, LX/2NP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2NP;->SSO_AUTH_UI:LX/2NP;

    .line 399802
    new-instance v0, LX/2NP;

    const-string v1, "PASSWORD_AUTH_UI"

    invoke-direct {v0, v1, v6}, LX/2NP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2NP;->PASSWORD_AUTH_UI:LX/2NP;

    .line 399803
    new-instance v0, LX/2NP;

    const-string v1, "LOGIN_APPPROVAL_UI"

    invoke-direct {v0, v1, v7}, LX/2NP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2NP;->LOGIN_APPPROVAL_UI:LX/2NP;

    .line 399804
    new-instance v0, LX/2NP;

    const-string v1, "LOGGING_IN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/2NP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2NP;->LOGGING_IN:LX/2NP;

    .line 399805
    const/4 v0, 0x6

    new-array v0, v0, [LX/2NP;

    sget-object v1, LX/2NP;->SPLASHSCREEN:LX/2NP;

    aput-object v1, v0, v3

    sget-object v1, LX/2NP;->FETCHING_SSO_DATA:LX/2NP;

    aput-object v1, v0, v4

    sget-object v1, LX/2NP;->SSO_AUTH_UI:LX/2NP;

    aput-object v1, v0, v5

    sget-object v1, LX/2NP;->PASSWORD_AUTH_UI:LX/2NP;

    aput-object v1, v0, v6

    sget-object v1, LX/2NP;->LOGIN_APPPROVAL_UI:LX/2NP;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/2NP;->LOGGING_IN:LX/2NP;

    aput-object v2, v0, v1

    sput-object v0, LX/2NP;->$VALUES:[LX/2NP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 399796
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2NP;
    .locals 1

    .prologue
    .line 399797
    const-class v0, LX/2NP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2NP;

    return-object v0
.end method

.method public static values()[LX/2NP;
    .locals 1

    .prologue
    .line 399798
    sget-object v0, LX/2NP;->$VALUES:[LX/2NP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2NP;

    return-object v0
.end method
