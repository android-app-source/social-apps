.class public LX/3HE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ti;

.field private final b:LX/0Sh;

.field private final c:LX/3HF;

.field private final d:LX/3HK;


# direct methods
.method public constructor <init>(LX/0ti;LX/0Sh;LX/3HF;LX/3HK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 543200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 543201
    iput-object p1, p0, LX/3HE;->a:LX/0ti;

    .line 543202
    iput-object p2, p0, LX/3HE;->b:LX/0Sh;

    .line 543203
    iput-object p3, p0, LX/3HE;->c:LX/3HF;

    .line 543204
    iput-object p4, p0, LX/3HE;->d:LX/3HK;

    .line 543205
    return-void
.end method

.method public static a(LX/0QB;)LX/3HE;
    .locals 1

    .prologue
    .line 543199
    invoke-static {p0}, LX/3HE;->b(LX/0QB;)LX/3HE;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3HE;
    .locals 5

    .prologue
    .line 543197
    new-instance v4, LX/3HE;

    invoke-static {p0}, LX/0ti;->b(LX/0QB;)LX/0ti;

    move-result-object v0

    check-cast v0, LX/0ti;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/3HF;->b(LX/0QB;)LX/3HF;

    move-result-object v2

    check-cast v2, LX/3HF;

    invoke-static {p0}, LX/3HK;->b(LX/0QB;)LX/3HK;

    move-result-object v3

    check-cast v3, LX/3HK;

    invoke-direct {v4, v0, v1, v2, v3}, LX/3HE;-><init>(LX/0ti;LX/0Sh;LX/3HF;LX/3HK;)V

    .line 543198
    return-object v4
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 4

    .prologue
    .line 543155
    iget-object v0, p0, LX/3HE;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 543156
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 543157
    if-eqz v0, :cond_1

    .line 543158
    new-instance v0, LX/3d9;

    invoke-direct {v0}, LX/3d9;-><init>()V

    new-instance v1, LX/3dA;

    invoke-direct {v1}, LX/3dA;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    .line 543159
    iput-object v2, v1, LX/3dA;->a:Ljava/lang/String;

    .line 543160
    move-object v1, v1

    .line 543161
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    .line 543162
    iput-object v2, v1, LX/3dA;->c:Ljava/lang/String;

    .line 543163
    move-object v1, v1

    .line 543164
    new-instance v2, LX/3dB;

    invoke-direct {v2}, LX/3dB;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v3

    .line 543165
    iput v3, v2, LX/3dB;->a:I

    .line 543166
    move-object v2, v2

    .line 543167
    invoke-virtual {v2}, LX/3dB;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ViewerFeedbackReactionModel;

    move-result-object v2

    .line 543168
    iput-object v2, v1, LX/3dA;->h:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ViewerFeedbackReactionModel;

    .line 543169
    move-object v1, v1

    .line 543170
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v2

    invoke-static {v2}, LX/3dC;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    .line 543171
    iput-object v2, v1, LX/3dA;->f:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 543172
    move-object v1, v1

    .line 543173
    new-instance v2, LX/3dD;

    invoke-direct {v2}, LX/3dD;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v3

    .line 543174
    iput v3, v2, LX/3dD;->a:I

    .line 543175
    move-object v2, v2

    .line 543176
    invoke-virtual {v2}, LX/3dD;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$LikersModel;

    move-result-object v2

    .line 543177
    iput-object v2, v1, LX/3dA;->d:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$LikersModel;

    .line 543178
    move-object v1, v1

    .line 543179
    new-instance v2, LX/3dE;

    invoke-direct {v2}, LX/3dE;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v3

    .line 543180
    iput v3, v2, LX/3dE;->a:I

    .line 543181
    move-object v2, v2

    .line 543182
    invoke-virtual {v2}, LX/3dE;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ReactorsModel;

    move-result-object v2

    .line 543183
    iput-object v2, v1, LX/3dA;->e:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ReactorsModel;

    .line 543184
    move-object v1, v1

    .line 543185
    invoke-virtual {v1}, LX/3dA;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    move-result-object v1

    .line 543186
    iput-object v1, v0, LX/3d9;->a:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    .line 543187
    move-object v0, v0

    .line 543188
    invoke-virtual {v0}, LX/3d9;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    move-result-object v0

    move-object v0, v0

    .line 543189
    iget-object v1, p0, LX/3HE;->c:LX/3HF;

    invoke-virtual {v1, v0}, LX/3HF;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;)LX/3Bq;

    move-result-object v1

    .line 543190
    if-eqz v1, :cond_0

    .line 543191
    iget-object v2, p0, LX/3HE;->a:LX/0ti;

    invoke-virtual {v2, v1}, LX/0ti;->b(LX/3Bq;)V

    .line 543192
    :cond_0
    iget-object v1, p0, LX/3HE;->d:LX/3HK;

    invoke-virtual {v1, v0}, LX/3HK;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;)LX/4VT;

    move-result-object v0

    .line 543193
    if-eqz v0, :cond_1

    .line 543194
    iget-object v1, p0, LX/3HE;->a:LX/0ti;

    .line 543195
    new-instance v2, LX/4VL;

    iget-object v3, v1, LX/0ti;->h:LX/0Uh;

    const/4 p0, 0x1

    new-array p0, p0, [LX/4VT;

    const/4 p1, 0x0

    aput-object v0, p0, p1

    invoke-direct {v2, v3, p0}, LX/4VL;-><init>(LX/0Uh;[LX/4VT;)V

    invoke-virtual {v1, v2}, LX/0ti;->b(LX/3Bq;)V

    .line 543196
    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
