.class public LX/2Vb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:LX/2Vb;

.field public static final b:LX/2Vb;

.field private static final serialVersionUID:J = 0x6e0fe282c0ebea86L


# instance fields
.field public final _namespace:Ljava/lang/String;

.field public final _simpleName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 417888
    new-instance v0, LX/2Vb;

    const-string v1, ""

    invoke-direct {v0, v1, v3}, LX/2Vb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Vb;->a:LX/2Vb;

    .line 417889
    new-instance v0, LX/2Vb;

    new-instance v1, Ljava/lang/String;

    const-string v2, "#disabled"

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v3}, LX/2Vb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Vb;->b:LX/2Vb;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 417857
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/2Vb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 417858
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 417884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417885
    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    iput-object p1, p0, LX/2Vb;->_simpleName:Ljava/lang/String;

    .line 417886
    iput-object p2, p0, LX/2Vb;->_namespace:Ljava/lang/String;

    .line 417887
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 417883
    iget-object v0, p0, LX/2Vb;->_simpleName:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 417882
    iget-object v0, p0, LX/2Vb;->_simpleName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 417870
    if-ne p1, p0, :cond_1

    .line 417871
    :cond_0
    :goto_0
    return v0

    .line 417872
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 417873
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    .line 417874
    :cond_3
    check-cast p1, LX/2Vb;

    .line 417875
    iget-object v2, p0, LX/2Vb;->_simpleName:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 417876
    iget-object v2, p1, LX/2Vb;->_simpleName:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    .line 417877
    :cond_4
    iget-object v2, p0, LX/2Vb;->_simpleName:Ljava/lang/String;

    iget-object v3, p1, LX/2Vb;->_simpleName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 417878
    goto :goto_0

    .line 417879
    :cond_5
    iget-object v2, p0, LX/2Vb;->_namespace:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 417880
    iget-object v2, p1, LX/2Vb;->_namespace:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 417881
    :cond_6
    iget-object v0, p0, LX/2Vb;->_namespace:Ljava/lang/String;

    iget-object v1, p1, LX/2Vb;->_namespace:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 417867
    iget-object v0, p0, LX/2Vb;->_namespace:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 417868
    iget-object v0, p0, LX/2Vb;->_simpleName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 417869
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2Vb;->_namespace:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, LX/2Vb;->_simpleName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    goto :goto_0
.end method

.method public readResolve()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 417862
    iget-object v0, p0, LX/2Vb;->_simpleName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, ""

    iget-object v1, p0, LX/2Vb;->_simpleName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 417863
    :cond_0
    sget-object p0, LX/2Vb;->a:LX/2Vb;

    .line 417864
    :cond_1
    :goto_0
    return-object p0

    .line 417865
    :cond_2
    iget-object v0, p0, LX/2Vb;->_simpleName:Ljava/lang/String;

    const-string v1, "#disabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 417866
    sget-object p0, LX/2Vb;->b:LX/2Vb;

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 417859
    iget-object v0, p0, LX/2Vb;->_namespace:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 417860
    iget-object v0, p0, LX/2Vb;->_simpleName:Ljava/lang/String;

    .line 417861
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2Vb;->_namespace:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2Vb;->_simpleName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
