.class public LX/2HH;
.super LX/2Gj;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2HH;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2HQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/mqttlite/persistence/HighestMqttPersistence;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2HQ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390230
    invoke-direct {p0}, LX/2Gj;-><init>()V

    .line 390231
    iput-object p1, p0, LX/2HH;->a:LX/0Or;

    .line 390232
    return-void
.end method

.method public static a(LX/0QB;)LX/2HH;
    .locals 4

    .prologue
    .line 390214
    sget-object v0, LX/2HH;->b:LX/2HH;

    if-nez v0, :cond_1

    .line 390215
    const-class v1, LX/2HH;

    monitor-enter v1

    .line 390216
    :try_start_0
    sget-object v0, LX/2HH;->b:LX/2HH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390217
    if-eqz v2, :cond_0

    .line 390218
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 390219
    new-instance v3, LX/2HH;

    const/16 p0, 0xe0b

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2HH;-><init>(LX/0Or;)V

    .line 390220
    move-object v0, v3

    .line 390221
    sput-object v0, LX/2HH;->b:LX/2HH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390222
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390223
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390224
    :cond_1
    sget-object v0, LX/2HH;->b:LX/2HH;

    return-object v0

    .line 390225
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390226
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0lF;
    .locals 1

    .prologue
    .line 390229
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 390228
    const-string v0, "persistent_mqtt"

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 390227
    iget-object v0, p0, LX/2HH;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/2HQ;->ALWAYS:LX/2HQ;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
