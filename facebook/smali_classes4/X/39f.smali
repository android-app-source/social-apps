.class public final LX/39f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/20F;


# instance fields
.field public final synthetic a:LX/39b;

.field private final b:LX/20z;

.field private final c:LX/21M;

.field private final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1PT;

.field private final f:LX/21H;

.field private g:LX/1De;

.field private h:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field public j:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(LX/39b;LX/20z;LX/21M;LX/0Px;LX/1PT;LX/21H;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/20z;",
            "LX/21M;",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;",
            "LX/1PT;",
            "Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 523656
    iput-object p1, p0, LX/39f;->a:LX/39b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 523657
    const/4 v0, 0x0

    iput v0, p0, LX/39f;->i:I

    .line 523658
    iput-object p2, p0, LX/39f;->b:LX/20z;

    .line 523659
    iput-object p3, p0, LX/39f;->c:LX/21M;

    .line 523660
    iput-object p4, p0, LX/39f;->d:LX/0Px;

    .line 523661
    iput-object p5, p0, LX/39f;->e:LX/1PT;

    .line 523662
    iput-object p6, p0, LX/39f;->f:LX/21H;

    .line 523663
    return-void
.end method

.method public synthetic constructor <init>(LX/39b;LX/20z;LX/21M;LX/0Px;LX/1PT;LX/21H;B)V
    .locals 0

    .prologue
    .line 523655
    invoke-direct/range {p0 .. p6}, LX/39f;-><init>(LX/39b;LX/20z;LX/21M;LX/0Px;LX/1PT;LX/21H;)V

    return-void
.end method

.method public static synthetic a(LX/39f;LX/1De;)LX/1De;
    .locals 0

    .prologue
    .line 523654
    iput-object p1, p0, LX/39f;->g:LX/1De;

    return-object p1
.end method

.method public static synthetic a(LX/39f;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 0

    .prologue
    .line 523653
    iput-object p1, p0, LX/39f;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    return-object p1
.end method

.method public static a$redex0(LX/39f;I)V
    .locals 1

    .prologue
    .line 523651
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/39f;->a$redex0(LX/39f;ILX/1zt;)V

    .line 523652
    return-void
.end method

.method public static a$redex0(LX/39f;ILX/1zt;)V
    .locals 4

    .prologue
    .line 523641
    iget v0, p0, LX/39f;->i:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/39f;->i:I

    if-eq p1, v0, :cond_0

    iget v0, p0, LX/39f;->i:I

    if-nez v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 523642
    :cond_0
    :goto_0
    return-void

    .line 523643
    :cond_1
    iput p1, p0, LX/39f;->i:I

    .line 523644
    iget-object v1, p0, LX/39f;->g:LX/1De;

    if-eqz p2, :cond_2

    iget-object v0, p0, LX/39f;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 523645
    :goto_1
    iget-object v2, v1, LX/1De;->g:LX/1X1;

    move-object v2, v2

    .line 523646
    if-nez v2, :cond_3

    .line 523647
    :goto_2
    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 523648
    :cond_3
    check-cast v2, LX/39W;

    .line 523649
    new-instance v3, LX/C46;

    iget-object p0, v2, LX/39W;->n:LX/1Wn;

    invoke-direct {v3, p0, p1, p2, v0}, LX/C46;-><init>(LX/1Wn;ILX/1zt;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v2, v3

    .line 523650
    invoke-virtual {v1, v2}, LX/1De;->a(LX/48B;)V

    goto :goto_2
.end method

.method public static a$redex0(LX/39f;LX/1zt;)V
    .locals 5

    .prologue
    .line 523664
    iget-object v0, p0, LX/39f;->j:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 523665
    iget-object v0, p0, LX/39f;->a:LX/39b;

    iget-object v0, v0, LX/39b;->k:Landroid/os/Handler;

    iget-object v1, p0, LX/39f;->j:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 523666
    :cond_0
    new-instance v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterButtonsComponentSpec$ComponentsReactionsDockSupport$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterButtonsComponentSpec$ComponentsReactionsDockSupport$1;-><init>(LX/39f;LX/1zt;)V

    iput-object v0, p0, LX/39f;->j:Ljava/lang/Runnable;

    .line 523667
    iget-object v0, p0, LX/39f;->a:LX/39b;

    iget-object v0, v0, LX/39b;->k:Landroid/os/Handler;

    iget-object v1, p0, LX/39f;->j:Ljava/lang/Runnable;

    const-wide/16 v2, 0x258

    const v4, -0x2bdc4334

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 523668
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1zt;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 523633
    iget-object v0, p0, LX/39f;->c:LX/21M;

    if-eqz v0, :cond_0

    sget-object v0, LX/1zt;->c:LX/1zt;

    if-ne p2, v0, :cond_1

    .line 523634
    :cond_0
    invoke-static {p0, v1}, LX/39f;->a$redex0(LX/39f;I)V

    .line 523635
    :goto_0
    return-void

    .line 523636
    :cond_1
    iget-object v0, p0, LX/39f;->a:LX/39b;

    iget-object v0, v0, LX/39b;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v2, "reactions_like_up"

    invoke-virtual {v0, v2}, LX/3RX;->a(Ljava/lang/String;)V

    .line 523637
    iget-object v0, p0, LX/39f;->c:LX/21M;

    iget-object v2, p0, LX/39f;->a:LX/39b;

    .line 523638
    new-instance v3, LX/C47;

    invoke-direct {v3, v2}, LX/C47;-><init>(LX/39b;)V

    move-object v2, v3

    .line 523639
    invoke-interface {v0, p1, p2, v2}, LX/21M;->a(Landroid/view/View;LX/1zt;LX/0Ve;)V

    .line 523640
    iget-object v0, p0, LX/39f;->f:LX/21H;

    invoke-virtual {v0}, LX/21H;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    :goto_1
    invoke-static {p0, v0, p2}, LX/39f;->a$redex0(LX/39f;ILX/1zt;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 523624
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v0}, LX/39f;->a$redex0(LX/39f;I)V

    .line 523625
    return-void

    .line 523626
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 523630
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-static {p0, v0}, LX/39f;->a$redex0(LX/39f;I)V

    .line 523631
    return-void

    .line 523632
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getDockTheme()LX/20I;
    .locals 1

    .prologue
    .line 523629
    iget-object v0, p0, LX/39f;->e:LX/1PT;

    invoke-static {v0}, LX/21N;->a(LX/1PT;)LX/20I;

    move-result-object v0

    return-object v0
.end method

.method public final getInteractionLogger()LX/20z;
    .locals 1

    .prologue
    .line 523628
    iget-object v0, p0, LX/39f;->b:LX/20z;

    return-object v0
.end method

.method public final getSupportedReactions()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 523627
    iget-object v0, p0, LX/39f;->d:LX/0Px;

    return-object v0
.end method
