.class public LX/2Uc;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Boolean;


# instance fields
.field private b:Landroid/content/ContentResolver;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 416386
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, LX/2Uc;->a:Ljava/lang/Boolean;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416388
    iput-object p1, p0, LX/2Uc;->b:Landroid/content/ContentResolver;

    .line 416389
    return-void
.end method

.method public static b(LX/0QB;)LX/2Uc;
    .locals 2

    .prologue
    .line 416390
    new-instance v1, LX/2Uc;

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    invoke-direct {v1, v0}, LX/2Uc;-><init>(Landroid/content/ContentResolver;)V

    .line 416391
    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 416392
    sget-object v2, LX/2Uc;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 416393
    iget-object v2, p0, LX/2Uc;->b:Landroid/content/ContentResolver;

    const-string v3, "auto_time"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    .line 416394
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 416395
    goto :goto_0

    .line 416396
    :cond_2
    iget-object v2, p0, LX/2Uc;->b:Landroid/content/ContentResolver;

    const-string v3, "auto_time"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
