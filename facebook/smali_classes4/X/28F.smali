.class public LX/28F;
.super Ljava/io/IOException;
.source ""


# static fields
.field public static final serialVersionUID:J = 0x7bL


# instance fields
.field public _location:LX/28G;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 373776
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 373777
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/28G;)V
    .locals 1

    .prologue
    .line 373778
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/28F;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    .line 373779
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 373780
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 373781
    if-eqz p3, :cond_0

    .line 373782
    invoke-virtual {p0, p3}, LX/28F;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 373783
    :cond_0
    iput-object p2, p0, LX/28F;->_location:LX/28G;

    .line 373784
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 373785
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, LX/28F;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    .line 373786
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 373787
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 5

    .prologue
    .line 373788
    invoke-super {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 373789
    if-nez v0, :cond_0

    .line 373790
    const-string v0, "N/A"

    .line 373791
    :cond_0
    iget-object v1, p0, LX/28F;->_location:LX/28G;

    move-object v1, v1

    .line 373792
    invoke-virtual {p0}, LX/28F;->a()Ljava/lang/String;

    move-result-object v2

    .line 373793
    if-nez v1, :cond_1

    if-eqz v2, :cond_4

    .line 373794
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 373795
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373796
    if-eqz v2, :cond_2

    .line 373797
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373798
    :cond_2
    if-eqz v1, :cond_3

    .line 373799
    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 373800
    const-string v0, " at "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373801
    invoke-virtual {v1}, LX/28G;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373802
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 373803
    :cond_4
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 373804
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/28F;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
