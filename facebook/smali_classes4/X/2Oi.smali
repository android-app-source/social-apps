.class public LX/2Oi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private final a:LX/0WJ;

.field private final b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 403023
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2Oi;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0WJ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 403019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403020
    iput-object p1, p0, LX/2Oi;->a:LX/0WJ;

    .line 403021
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/2Oi;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 403022
    return-void
.end method

.method public static a(LX/0QB;)LX/2Oi;
    .locals 7

    .prologue
    .line 402990
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 402991
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 402992
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 402993
    if-nez v1, :cond_0

    .line 402994
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402995
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 402996
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 402997
    sget-object v1, LX/2Oi;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 402998
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 402999
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 403000
    :cond_1
    if-nez v1, :cond_4

    .line 403001
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 403002
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 403003
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 403004
    new-instance p0, LX/2Oi;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v1

    check-cast v1, LX/0WJ;

    invoke-direct {p0, v1}, LX/2Oi;-><init>(LX/0WJ;)V

    .line 403005
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 403006
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 403007
    if-nez v1, :cond_2

    .line 403008
    sget-object v0, LX/2Oi;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oi;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 403009
    :goto_1
    if-eqz v0, :cond_3

    .line 403010
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 403011
    :goto_3
    check-cast v0, LX/2Oi;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 403012
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 403013
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 403014
    :catchall_1
    move-exception v0

    .line 403015
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 403016
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 403017
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 403018
    :cond_2
    :try_start_8
    sget-object v0, LX/2Oi;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oi;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402983
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 402984
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 402985
    invoke-virtual {p0, v0}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 402986
    if-eqz v0, :cond_0

    .line 402987
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 402988
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 402989
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 402955
    if-nez p1, :cond_0

    .line 402956
    const/4 v0, 0x0

    .line 402957
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2Oi;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    goto :goto_0
.end method

.method public final a()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402982
    iget-object v0, p0, LX/2Oi;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 402980
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/2Oi;->a(Ljava/util/Collection;Z)V

    .line 402981
    return-void
.end method

.method public final a(Ljava/util/Collection;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 402960
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 402961
    if-nez p2, :cond_1

    .line 402962
    iget-object v1, p0, LX/2Oi;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 402963
    iget-object v3, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v3, v3

    .line 402964
    invoke-interface {v1, v3}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 402965
    if-eqz v1, :cond_1

    .line 402966
    iget-wide v8, v1, Lcom/facebook/user/model/User;->M:J

    move-wide v4, v8

    .line 402967
    iget-wide v8, v0, Lcom/facebook/user/model/User;->M:J

    move-wide v6, v8

    .line 402968
    cmp-long v1, v4, v6

    if-gez v1, :cond_0

    .line 402969
    :cond_1
    iget-object v1, p0, LX/2Oi;->a:LX/0WJ;

    .line 402970
    invoke-virtual {v1}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v3

    .line 402971
    if-eqz v3, :cond_2

    .line 402972
    iget-object v4, v3, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v3, v4

    .line 402973
    iget-object v4, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 402974
    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 402975
    invoke-virtual {v1, v0}, LX/0WJ;->a(Lcom/facebook/user/model/User;)V

    .line 402976
    :cond_2
    iget-object v1, p0, LX/2Oi;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 402977
    iget-object v3, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v3, v3

    .line 402978
    invoke-interface {v1, v3, v0}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 402979
    :cond_3
    return-void
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 402958
    iget-object v0, p0, LX/2Oi;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 402959
    return-void
.end method
