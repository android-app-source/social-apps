.class public final LX/2ka;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2kb;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/2kF;

.field private c:Z


# direct methods
.method public constructor <init>(LX/2kF;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 456350
    iput-object p1, p0, LX/2ka;->b:LX/2kF;

    iput-object p2, p0, LX/2ka;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456351
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2ka;->c:Z

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 456352
    iget-object v0, p0, LX/2ka;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final declared-synchronized close()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 456353
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/2ka;->c:Z

    if-nez v1, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 456354
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2ka;->c:Z

    .line 456355
    iget-object v0, p0, LX/2ka;->b:LX/2kF;

    iget-object v1, p0, LX/2ka;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2kF;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456356
    monitor-exit p0

    return-void

    .line 456357
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
