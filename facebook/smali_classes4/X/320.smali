.class public abstract LX/320;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 488240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(D)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 488251
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not instantiate value of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from Floating-point number (double)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 488250
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not instantiate value of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from Integer number (int)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(J)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 488249
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not instantiate value of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from Integer number (long)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 488248
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not instantiate value of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " using delegate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(LX/0n3;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 488247
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not instantiate value of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from String value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Z)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 488246
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not instantiate value of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from Boolean value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 488245
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not instantiate value of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with arguments"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public a(LX/0mu;)[LX/32s;
    .locals 1

    .prologue
    .line 488244
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 488243
    invoke-virtual {p0}, LX/320;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/320;->i()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/320;->j()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/320;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/320;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/320;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/320;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/320;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 488242
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 488241
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 488229
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 488230
    const/4 v0, 0x0

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 488231
    const/4 v0, 0x0

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 488232
    invoke-virtual {p0}, LX/320;->m()LX/2Au;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 488233
    const/4 v0, 0x0

    return v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 488234
    const/4 v0, 0x0

    return v0
.end method

.method public k()LX/0lJ;
    .locals 1

    .prologue
    .line 488235
    const/4 v0, 0x0

    return-object v0
.end method

.method public l()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 488236
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not instantiate value of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/320;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; no default creator found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public m()LX/2Au;
    .locals 1

    .prologue
    .line 488237
    const/4 v0, 0x0

    return-object v0
.end method

.method public n()LX/2Au;
    .locals 1

    .prologue
    .line 488238
    const/4 v0, 0x0

    return-object v0
.end method

.method public o()LX/2Vd;
    .locals 1

    .prologue
    .line 488239
    const/4 v0, 0x0

    return-object v0
.end method
