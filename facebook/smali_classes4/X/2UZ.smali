.class public LX/2UZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static volatile j:LX/2UZ;


# instance fields
.field private final c:LX/0SG;

.field private final d:LX/2Hu;

.field private final e:LX/2Hv;

.field private final f:LX/2Uc;

.field private final g:LX/2Ud;

.field private h:LX/2Uf;

.field private final i:LX/1so;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 416192
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "/"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, LX/2Ua;->b:Ljava/util/Map;

    const/16 v2, 0x77

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2UZ;->a:Ljava/lang/String;

    .line 416193
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "/"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, LX/2Ua;->b:Ljava/util/Map;

    const/16 v2, 0x78

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2UZ;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/2Hu;LX/2Hv;LX/2Uc;LX/2Ud;LX/2Uf;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416195
    new-instance v0, LX/1so;

    new-instance v1, LX/1sp;

    invoke-direct {v1}, LX/1sp;-><init>()V

    invoke-direct {v0, v1}, LX/1so;-><init>(LX/1sq;)V

    iput-object v0, p0, LX/2UZ;->i:LX/1so;

    .line 416196
    iput-object p1, p0, LX/2UZ;->c:LX/0SG;

    .line 416197
    iput-object p2, p0, LX/2UZ;->d:LX/2Hu;

    .line 416198
    iput-object p3, p0, LX/2UZ;->e:LX/2Hv;

    .line 416199
    iput-object p4, p0, LX/2UZ;->f:LX/2Uc;

    .line 416200
    iput-object p5, p0, LX/2UZ;->g:LX/2Ud;

    .line 416201
    iput-object p6, p0, LX/2UZ;->h:LX/2Uf;

    .line 416202
    return-void
.end method

.method public static a(LX/0QB;)LX/2UZ;
    .locals 10

    .prologue
    .line 416203
    sget-object v0, LX/2UZ;->j:LX/2UZ;

    if-nez v0, :cond_1

    .line 416204
    const-class v1, LX/2UZ;

    monitor-enter v1

    .line 416205
    :try_start_0
    sget-object v0, LX/2UZ;->j:LX/2UZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 416206
    if-eqz v2, :cond_0

    .line 416207
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 416208
    new-instance v3, LX/2UZ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v5

    check-cast v5, LX/2Hu;

    invoke-static {v0}, LX/2Hv;->a(LX/0QB;)LX/2Hv;

    move-result-object v6

    check-cast v6, LX/2Hv;

    invoke-static {v0}, LX/2Uc;->b(LX/0QB;)LX/2Uc;

    move-result-object v7

    check-cast v7, LX/2Uc;

    invoke-static {v0}, LX/2Ud;->a(LX/0QB;)LX/2Ud;

    move-result-object v8

    check-cast v8, LX/2Ud;

    invoke-static {v0}, LX/2Uf;->b(LX/0QB;)LX/2Uf;

    move-result-object v9

    check-cast v9, LX/2Uf;

    invoke-direct/range {v3 .. v9}, LX/2UZ;-><init>(LX/0SG;LX/2Hu;LX/2Hv;LX/2Uc;LX/2Ud;LX/2Uf;)V

    .line 416209
    move-object v0, v3

    .line 416210
    sput-object v0, LX/2UZ;->j:LX/2UZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416211
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 416212
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 416213
    :cond_1
    sget-object v0, LX/2UZ;->j:LX/2UZ;

    return-object v0

    .line 416214
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 416215
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/2UZ;)J
    .locals 13

    .prologue
    .line 416216
    iget-object v0, p0, LX/2UZ;->d:LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;

    move-result-object v1

    .line 416217
    new-instance v0, LX/6cY;

    invoke-direct {v0}, LX/6cY;-><init>()V

    .line 416218
    :try_start_0
    iget-object v2, p0, LX/2UZ;->e:LX/2Hv;

    sget-object v3, LX/2UZ;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/2Hv;->a(Ljava/lang/String;LX/6cX;)LX/76J;

    move-result-object v0

    .line 416219
    iget-object v2, p0, LX/2UZ;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 416220
    iget-object v4, p0, LX/2UZ;->i:LX/1so;

    new-instance v5, LX/6mh;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v5, v6}, LX/6mh;-><init>(Ljava/lang/Long;)V

    invoke-virtual {v4, v5}, LX/1so;->a(LX/1u2;)[B

    move-result-object v4

    .line 416221
    array-length v5, v4

    add-int/lit8 v5, v5, 0x1

    new-array v5, v5, [B

    .line 416222
    const/4 v6, 0x0

    const/4 v7, 0x1

    array-length v8, v4

    invoke-static {v4, v6, v5, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 416223
    sget-object v4, LX/2UZ;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416224
    :try_start_1
    invoke-static {v1, v4, v5, v0}, LX/2gV;->b(LX/2gV;Ljava/lang/String;[BLX/76J;)LX/76M;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    move-result-object v9

    .line 416225
    :goto_0
    move-object v0, v9

    .line 416226
    iget-object v4, p0, LX/2UZ;->c:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 416227
    sub-long/2addr v4, v2

    const-wide/16 v6, 0x2

    div-long/2addr v4, v6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    add-long/2addr v2, v4

    .line 416228
    invoke-virtual {v1}, LX/2gV;->f()V

    .line 416229
    if-nez v0, :cond_0

    .line 416230
    new-instance v0, LX/6cP;

    const-string v1, "ipc call failed"

    invoke-direct {v0, v1}, LX/6cP;-><init>(Ljava/lang/String;)V

    throw v0

    .line 416231
    :catch_0
    :try_start_3
    new-instance v0, LX/6cP;

    const-string v2, "can not publish"

    invoke-direct {v0, v2}, LX/6cP;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 416232
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2gV;->f()V

    throw v0

    .line 416233
    :cond_0
    iget-boolean v1, v0, LX/76M;->a:Z

    if-nez v1, :cond_1

    .line 416234
    new-instance v1, LX/6cP;

    const-string v2, "Result was not success"

    iget-object v0, v0, LX/76M;->d:Ljava/lang/Exception;

    invoke-direct {v1, v2, v0}, LX/6cP;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 416235
    :cond_1
    iget-object v1, v0, LX/76M;->b:Ljava/lang/Object;

    if-nez v1, :cond_2

    .line 416236
    new-instance v0, LX/6cP;

    const-string v1, "empty response"

    invoke-direct {v0, v1}, LX/6cP;-><init>(Ljava/lang/String;)V

    throw v0

    .line 416237
    :cond_2
    iget-object v0, v0, LX/76M;->b:Ljava/lang/Object;

    check-cast v0, LX/6mi;

    iget-object v0, v0, LX/6mi;->time:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    return-wide v0

    .line 416238
    :catch_1
    move-exception v9

    .line 416239
    iget-object v10, v1, LX/2gV;->d:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v11

    invoke-static {v9, v11, v12}, LX/76M;->a(Ljava/lang/Exception;J)LX/76M;

    move-result-object v9

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 416240
    iget-object v0, p0, LX/2UZ;->f:LX/2Uc;

    invoke-virtual {v0}, LX/2Uc;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416241
    iget-object v0, p0, LX/2UZ;->g:LX/2Ud;

    invoke-virtual {v0}, LX/2Ud;->d()V

    .line 416242
    :goto_0
    return-void

    .line 416243
    :cond_0
    invoke-static {p0}, LX/2UZ;->b(LX/2UZ;)J

    move-result-wide v0

    .line 416244
    iget-object v2, p0, LX/2UZ;->h:LX/2Uf;

    .line 416245
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "time_skew_event"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "time_skew"

    invoke-virtual {v3, v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 416246
    iget-object v4, v2, LX/2Uf;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 416247
    iget-object v2, p0, LX/2UZ;->g:LX/2Ud;

    invoke-virtual {v2, v0, v1}, LX/2Ud;->b(J)V

    goto :goto_0
.end method
