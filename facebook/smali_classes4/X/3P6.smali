.class public final LX/3P6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "LX/3OU;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic b:Lcom/facebook/divebar/contacts/DivebarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/divebar/contacts/DivebarFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 561078
    iput-object p1, p0, LX/3P6;->b:Lcom/facebook/divebar/contacts/DivebarFragment;

    iput-object p2, p0, LX/3P6;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 561079
    iget-object v0, p0, LX/3P6;->b:Lcom/facebook/divebar/contacts/DivebarFragment;

    iget-object v0, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->r:LX/3LL;

    iget-object v1, p0, LX/3P6;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v4, 0x0

    .line 561080
    iget-object v2, v0, LX/3LL;->h:LX/0Uh;

    const/16 v3, 0x2eb

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 561081
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/common/util/concurrent/ListenableFuture;

    .line 561082
    iget-object v3, v0, LX/3LL;->c:LX/0tX;

    .line 561083
    new-instance v5, LX/3P8;

    invoke-direct {v5}, LX/3P8;-><init>()V

    move-object v5, v5

    .line 561084
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 561085
    new-instance v5, LX/3P9;

    invoke-direct {v5, v0}, LX/3P9;-><init>(LX/3LL;)V

    iget-object p0, v0, LX/3LL;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v5, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v3, v3

    .line 561086
    aput-object v3, v2, v4

    const/4 v3, 0x1

    .line 561087
    new-instance v4, LX/3PA;

    invoke-direct {v4, v0}, LX/3PA;-><init>(LX/3LL;)V

    iget-object v5, v0, LX/3LL;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v4, v5}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v4, v4

    .line 561088
    aput-object v4, v2, v3

    invoke-static {v2}, LX/0Vg;->b([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 561089
    new-instance v3, LX/3PB;

    invoke-direct {v3, v0}, LX/3PB;-><init>(LX/3LL;)V

    iget-object v4, v0, LX/3LL;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 561090
    return-object v0
.end method
