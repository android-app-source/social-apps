.class public LX/3Nk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/KeepGettersAndSetters;
.end annotation


# instance fields
.field public a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 559947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 559948
    iput-object p1, p0, LX/3Nk;->a:Landroid/view/View;

    .line 559949
    return-void
.end method


# virtual methods
.method public getAlpha()F
    .locals 1

    .prologue
    .line 559946
    iget-object v0, p0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    return v0
.end method

.method public getRotation()F
    .locals 1

    .prologue
    .line 559945
    iget-object v0, p0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRotation()F

    move-result v0

    return v0
.end method

.method public getScaleY()F
    .locals 1

    .prologue
    .line 559944
    iget-object v0, p0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScaleY()F

    move-result v0

    return v0
.end method

.method public getTranslationX()F
    .locals 1

    .prologue
    .line 559943
    iget-object v0, p0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    return v0
.end method

.method public getTranslationY()F
    .locals 1

    .prologue
    .line 559942
    iget-object v0, p0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    return v0
.end method

.method public getX()F
    .locals 1

    .prologue
    .line 559941
    iget-object v0, p0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 559930
    iget-object v0, p0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    return v0
.end method

.method public setAlpha(F)V
    .locals 1

    .prologue
    .line 559939
    iget-object v0, p0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 559940
    return-void
.end method

.method public setRotation(F)V
    .locals 1

    .prologue
    .line 559937
    iget-object v0, p0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setRotation(F)V

    .line 559938
    return-void
.end method

.method public setScaleX(F)V
    .locals 1

    .prologue
    .line 559935
    iget-object v0, p0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setScaleX(F)V

    .line 559936
    return-void
.end method

.method public setScaleY(F)V
    .locals 1

    .prologue
    .line 559933
    iget-object v0, p0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setScaleY(F)V

    .line 559934
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 559931
    iget-object v0, p0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 559932
    return-void
.end method
