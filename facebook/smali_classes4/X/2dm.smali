.class public LX/2dm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2dn;

.field public final b:LX/2dl;


# direct methods
.method public constructor <init>(LX/2dn;LX/2dl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444287
    iput-object p1, p0, LX/2dm;->a:LX/2dn;

    .line 444288
    iput-object p2, p0, LX/2dm;->b:LX/2dl;

    .line 444289
    return-void
.end method

.method public static a(LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1MF;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444290
    invoke-interface {p0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/399",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 444291
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 444292
    invoke-virtual {p0, p1, v0}, LX/2dm;->a(LX/399;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/399;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/399",
            "<TT;>;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 444293
    iget-object v0, p0, LX/2dm;->b:LX/2dl;

    .line 444294
    iget-object v1, v0, LX/2dl;->a:LX/0tX;

    invoke-virtual {v1, p1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 444295
    iget-object v1, v0, LX/2dl;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2io;

    invoke-virtual {v1, p2}, LX/2io;->a(LX/0Px;)LX/0TF;

    move-result-object v1

    iget-object p0, v0, LX/2dl;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v1, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 444296
    move-object v0, v2

    .line 444297
    return-object v0
.end method
