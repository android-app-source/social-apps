.class public LX/2SW;
.super LX/2SP;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2SW;


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(LX/2SX;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 412551
    invoke-direct {p0}, LX/2SP;-><init>()V

    .line 412552
    invoke-static {p1}, LX/2SX;->d(LX/2SX;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 412553
    const/4 v0, 0x0

    .line 412554
    :goto_0
    move v0, v0

    .line 412555
    iput-boolean v0, p0, LX/2SW;->a:Z

    .line 412556
    return-void

    :cond_0
    iget-object v0, p1, LX/2SX;->b:LX/0ad;

    sget-short v1, LX/FUh;->b:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2SW;
    .locals 4

    .prologue
    .line 412538
    sget-object v0, LX/2SW;->b:LX/2SW;

    if-nez v0, :cond_1

    .line 412539
    const-class v1, LX/2SW;

    monitor-enter v1

    .line 412540
    :try_start_0
    sget-object v0, LX/2SW;->b:LX/2SW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 412541
    if-eqz v2, :cond_0

    .line 412542
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 412543
    new-instance p0, LX/2SW;

    invoke-static {v0}, LX/2SX;->b(LX/0QB;)LX/2SX;

    move-result-object v3

    check-cast v3, LX/2SX;

    invoke-direct {p0, v3}, LX/2SW;-><init>(LX/2SX;)V

    .line 412544
    move-object v0, p0

    .line 412545
    sput-object v0, LX/2SW;->b:LX/2SW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412546
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 412547
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 412548
    :cond_1
    sget-object v0, LX/2SW;->b:LX/2SW;

    return-object v0

    .line 412549
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 412550
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/7BE;
    .locals 1

    .prologue
    .line 412537
    sget-object v0, LX/7BE;->READY:LX/7BE;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 412536
    iget-boolean v0, p0, LX/2SW;->a:Z

    return v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 412535
    return-void
.end method

.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 412534
    new-instance v0, Lcom/facebook/search/model/QRCodePromoUnit;

    invoke-direct {v0}, Lcom/facebook/search/model/QRCodePromoUnit;-><init>()V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
