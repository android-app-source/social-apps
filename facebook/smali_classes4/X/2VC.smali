.class public LX/2VC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/2VC;


# instance fields
.field private final b:LX/0WJ;

.field private final c:LX/0qj;

.field private final d:LX/0Uo;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 417219
    const-class v0, LX/2VC;

    sput-object v0, LX/2VC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0WJ;LX/0qj;LX/0Uo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 417220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417221
    iput-object p1, p0, LX/2VC;->b:LX/0WJ;

    .line 417222
    iput-object p2, p0, LX/2VC;->c:LX/0qj;

    .line 417223
    iput-object p3, p0, LX/2VC;->d:LX/0Uo;

    .line 417224
    return-void
.end method

.method public static a(LX/0QB;)LX/2VC;
    .locals 6

    .prologue
    .line 417225
    sget-object v0, LX/2VC;->e:LX/2VC;

    if-nez v0, :cond_1

    .line 417226
    const-class v1, LX/2VC;

    monitor-enter v1

    .line 417227
    :try_start_0
    sget-object v0, LX/2VC;->e:LX/2VC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 417228
    if-eqz v2, :cond_0

    .line 417229
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 417230
    new-instance p0, LX/2VC;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    invoke-static {v0}, LX/0qj;->b(LX/0QB;)LX/0qj;

    move-result-object v4

    check-cast v4, LX/0qj;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v5

    check-cast v5, LX/0Uo;

    invoke-direct {p0, v3, v4, v5}, LX/2VC;-><init>(LX/0WJ;LX/0qj;LX/0Uo;)V

    .line 417231
    move-object v0, p0

    .line 417232
    sput-object v0, LX/2VC;->e:LX/2VC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 417233
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 417234
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 417235
    :cond_1
    sget-object v0, LX/2VC;->e:LX/2VC;

    return-object v0

    .line 417236
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 417237
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2VC;LX/2VD;)Z
    .locals 3

    .prologue
    .line 417238
    sget-object v0, LX/2VE;->a:[I

    invoke-virtual {p1}, LX/2VD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 417239
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown prerequisite: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 417240
    :pswitch_0
    iget-object v0, p0, LX/2VC;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2VC;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 417241
    :goto_0
    return v0

    .line 417242
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 417243
    :pswitch_1
    iget-object v0, p0, LX/2VC;->c:LX/0qj;

    invoke-virtual {v0}, LX/0qj;->a()Z

    move-result v0

    goto :goto_0

    .line 417244
    :pswitch_2
    iget-object v0, p0, LX/2VC;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    goto :goto_0

    .line 417245
    :pswitch_3
    iget-object v0, p0, LX/2VC;->d:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->l()Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
