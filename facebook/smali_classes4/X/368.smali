.class public final LX/368;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/367;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/26N;

.field public b:I

.field public c:LX/1aZ;

.field public d:Landroid/graphics/PointF;

.field public e:I

.field public f:I

.field public g:Ljava/lang/String;

.field public h:LX/1dQ;

.field public final synthetic i:LX/367;


# direct methods
.method public constructor <init>(LX/367;)V
    .locals 1

    .prologue
    .line 498083
    iput-object p1, p0, LX/368;->i:LX/367;

    .line 498084
    move-object v0, p1

    .line 498085
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 498086
    const/4 v0, 0x0

    iput v0, p0, LX/368;->f:I

    .line 498087
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 498088
    const-string v0, "CollageAttachmentItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 498089
    if-ne p0, p1, :cond_1

    .line 498090
    :cond_0
    :goto_0
    return v0

    .line 498091
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 498092
    goto :goto_0

    .line 498093
    :cond_3
    check-cast p1, LX/368;

    .line 498094
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 498095
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 498096
    if-eq v2, v3, :cond_0

    .line 498097
    iget-object v2, p0, LX/368;->a:LX/26N;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/368;->a:LX/26N;

    iget-object v3, p1, LX/368;->a:LX/26N;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 498098
    goto :goto_0

    .line 498099
    :cond_5
    iget-object v2, p1, LX/368;->a:LX/26N;

    if-nez v2, :cond_4

    .line 498100
    :cond_6
    iget v2, p0, LX/368;->b:I

    iget v3, p1, LX/368;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 498101
    goto :goto_0

    .line 498102
    :cond_7
    iget-object v2, p0, LX/368;->c:LX/1aZ;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/368;->c:LX/1aZ;

    iget-object v3, p1, LX/368;->c:LX/1aZ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 498103
    goto :goto_0

    .line 498104
    :cond_9
    iget-object v2, p1, LX/368;->c:LX/1aZ;

    if-nez v2, :cond_8

    .line 498105
    :cond_a
    iget-object v2, p0, LX/368;->d:Landroid/graphics/PointF;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/368;->d:Landroid/graphics/PointF;

    iget-object v3, p1, LX/368;->d:Landroid/graphics/PointF;

    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 498106
    goto :goto_0

    .line 498107
    :cond_c
    iget-object v2, p1, LX/368;->d:Landroid/graphics/PointF;

    if-nez v2, :cond_b

    .line 498108
    :cond_d
    iget v2, p0, LX/368;->e:I

    iget v3, p1, LX/368;->e:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 498109
    goto :goto_0

    .line 498110
    :cond_e
    iget v2, p0, LX/368;->f:I

    iget v3, p1, LX/368;->f:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 498111
    goto :goto_0

    .line 498112
    :cond_f
    iget-object v2, p0, LX/368;->g:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/368;->g:Ljava/lang/String;

    iget-object v3, p1, LX/368;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 498113
    goto :goto_0

    .line 498114
    :cond_10
    iget-object v2, p1, LX/368;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
