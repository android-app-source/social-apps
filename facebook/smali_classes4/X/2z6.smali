.class public LX/2z6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile g:LX/2z6;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/0WV;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:LX/0Vv;

.field public final f:Lcom/facebook/resources/impl/loading/LanguagePackDownloader;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 483003
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "i18n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, LX/2z6;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2z6;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0WV;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Vv;Lcom/facebook/resources/impl/loading/LanguagePackDownloader;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 483004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483005
    iput-object p1, p0, LX/2z6;->b:Landroid/content/Context;

    .line 483006
    iput-object p2, p0, LX/2z6;->c:LX/0WV;

    .line 483007
    iput-object p3, p0, LX/2z6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 483008
    iput-object p4, p0, LX/2z6;->e:LX/0Vv;

    .line 483009
    iput-object p5, p0, LX/2z6;->f:Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

    .line 483010
    return-void
.end method

.method public static a(LX/0QB;)LX/2z6;
    .locals 9

    .prologue
    .line 483011
    sget-object v0, LX/2z6;->g:LX/2z6;

    if-nez v0, :cond_1

    .line 483012
    const-class v1, LX/2z6;

    monitor-enter v1

    .line 483013
    :try_start_0
    sget-object v0, LX/2z6;->g:LX/2z6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 483014
    if-eqz v2, :cond_0

    .line 483015
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 483016
    new-instance v3, LX/2z6;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v5

    check-cast v5, LX/0WV;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Vv;->a(LX/0QB;)LX/0Vv;

    move-result-object v7

    check-cast v7, LX/0Vv;

    invoke-static {v0}, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->a(LX/0QB;)Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

    move-result-object v8

    check-cast v8, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

    invoke-direct/range {v3 .. v8}, LX/2z6;-><init>(Landroid/content/Context;LX/0WV;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Vv;Lcom/facebook/resources/impl/loading/LanguagePackDownloader;)V

    .line 483017
    move-object v0, v3

    .line 483018
    sput-object v0, LX/2z6;->g:LX/2z6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483019
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 483020
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 483021
    :cond_1
    sget-object v0, LX/2z6;->g:LX/2z6;

    return-object v0

    .line 483022
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 483023
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
