.class public final LX/3Hm;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2pY;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;LX/2oy;)V
    .locals 0

    .prologue
    .line 544758
    iput-object p1, p0, LX/3Hm;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    .line 544759
    invoke-direct {p0, p2}, LX/2oa;-><init>(LX/2oy;)V

    .line 544760
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2pY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 544761
    const-class v0, LX/2pY;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 544762
    iget-object v0, p0, LX/3Hm;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-boolean v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->E:Z

    if-eqz v0, :cond_1

    .line 544763
    :cond_0
    :goto_0
    return-void

    .line 544764
    :cond_1
    iget-object v0, p0, LX/3Hm;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3Hm;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->name()Ljava/lang/String;

    :cond_2
    iget-object v0, p0, LX/3Hm;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 544765
    iget-object v0, p0, LX/3Hm;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_0

    .line 544766
    iget-object v0, p0, LX/3Hm;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    invoke-static {v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    .line 544767
    iget-object v0, p0, LX/3Hm;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->o:Landroid/os/Handler;

    iget-object v1, p0, LX/3Hm;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->K:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 544768
    iget-object v0, p0, LX/3Hm;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->o:Landroid/os/Handler;

    iget-object v1, p0, LX/3Hm;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->K:Ljava/lang/Runnable;

    const v2, 0x38153b6a

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 544769
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
