.class public LX/3NK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Sh;

.field public final b:LX/0tX;

.field private final c:LX/3MV;

.field public final d:LX/3MW;


# direct methods
.method public constructor <init>(LX/0Sh;LX/0tX;LX/3MV;LX/3MW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 559022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 559023
    iput-object p1, p0, LX/3NK;->a:LX/0Sh;

    .line 559024
    iput-object p2, p0, LX/3NK;->b:LX/0tX;

    .line 559025
    iput-object p3, p0, LX/3NK;->c:LX/3MV;

    .line 559026
    iput-object p4, p0, LX/3NK;->d:LX/3MW;

    .line 559027
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 559028
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 559029
    iget-object v3, p0, LX/3NK;->a:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->b()V

    .line 559030
    new-instance v3, LX/FDa;

    invoke-direct {v3}, LX/FDa;-><init>()V

    move-object v3, v3

    .line 559031
    const-string v4, "search_query"

    invoke-virtual {v3, v4, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "results_limit"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 559032
    iget-object v4, p0, LX/3NK;->d:LX/3MW;

    invoke-virtual {v4, v3}, LX/3MW;->a(LX/0gW;)V

    .line 559033
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const-wide/16 v5, 0x258

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    .line 559034
    iget-object v4, p0, LX/3NK;->b:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    const v4, -0x67c03c0f

    invoke-static {v3, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 559035
    iget-object v4, v3, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v4

    .line 559036
    check-cast v3, Lcom/facebook/messaging/collaboration/graphql/CollaborationQueriesModels$VcEndpointNameSearchQueryModel;

    .line 559037
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/messaging/collaboration/graphql/CollaborationQueriesModels$VcEndpointNameSearchQueryModel;->a()Lcom/facebook/messaging/collaboration/graphql/CollaborationQueriesModels$VcEndpointNameSearchQueryModel$SearchResultsModel;

    move-result-object v4

    if-nez v4, :cond_2

    .line 559038
    :cond_0
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 559039
    :goto_0
    move-object v0, v3

    .line 559040
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/collaboration/graphql/CollaborationQueriesModels$VcEndpointNameSearchQueryModel$SearchResultsModel$NodesModel;

    .line 559041
    const/4 p0, 0x0

    .line 559042
    invoke-virtual {v0}, Lcom/facebook/messaging/collaboration/graphql/CollaborationQueriesModels$VcEndpointNameSearchQueryModel$SearchResultsModel$NodesModel;->n()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/messaging/collaboration/graphql/CollaborationQueriesModels$VcEndpointNameSearchQueryModel$SearchResultsModel$NodesModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/messaging/collaboration/graphql/CollaborationQueriesModels$VcEndpointNameSearchQueryModel$SearchResultsModel$NodesModel;->l()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquare;

    move-result-object v3

    .line 559043
    new-instance v4, LX/0XI;

    invoke-direct {v4}, LX/0XI;-><init>()V

    sget-object v5, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v0}, Lcom/facebook/messaging/collaboration/graphql/CollaborationQueriesModels$VcEndpointNameSearchQueryModel$SearchResultsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v4

    new-instance v5, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/messaging/collaboration/graphql/CollaborationQueriesModels$VcEndpointNameSearchQueryModel$SearchResultsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 559044
    iput-object v5, v4, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 559045
    move-object v4, v4

    .line 559046
    const-string v5, "page"

    .line 559047
    iput-object v5, v4, LX/0XI;->y:Ljava/lang/String;

    .line 559048
    move-object v4, v4

    .line 559049
    iput-object v3, v4, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 559050
    move-object v3, v4

    .line 559051
    iput-boolean p0, v3, LX/0XI;->A:Z

    .line 559052
    move-object v3, v3

    .line 559053
    iput-boolean p0, v3, LX/0XI;->z:Z

    .line 559054
    move-object v3, v3

    .line 559055
    const/4 v4, 0x1

    .line 559056
    iput-boolean v4, v3, LX/0XI;->I:Z

    .line 559057
    move-object v3, v3

    .line 559058
    invoke-virtual {v3}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v3

    move-object v0, v3

    .line 559059
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 559060
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_2
    invoke-virtual {v3}, Lcom/facebook/messaging/collaboration/graphql/CollaborationQueriesModels$VcEndpointNameSearchQueryModel;->a()Lcom/facebook/messaging/collaboration/graphql/CollaborationQueriesModels$VcEndpointNameSearchQueryModel$SearchResultsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/collaboration/graphql/CollaborationQueriesModels$VcEndpointNameSearchQueryModel$SearchResultsModel;->a()LX/0Px;

    move-result-object v3

    goto :goto_0
.end method
