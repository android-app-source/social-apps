.class public LX/2S4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/0Tn;

.field public static final B:LX/0Tn;

.field public static final C:LX/0Tn;

.field public static final D:LX/0Tn;

.field public static final E:LX/0Tn;

.field public static final F:LX/0Tn;

.field public static final G:LX/0Tn;

.field public static final H:LX/0Tn;

.field public static final I:LX/0Tn;

.field public static final J:LX/0Tn;

.field public static final K:LX/0Tn;

.field public static final L:LX/0Tn;

.field public static final M:LX/0Tn;

.field public static final N:LX/0Tn;

.field public static final O:LX/0Tn;

.field public static final P:LX/0Tn;

.field public static final Q:LX/0Tn;

.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;

.field public static final r:LX/0Tn;

.field public static final s:LX/0Tn;

.field public static final t:LX/0Tn;

.field public static final u:LX/0Tn;

.field public static final v:LX/0Tn;

.field public static final w:LX/0Tn;

.field public static final x:LX/0Tn;

.field public static final y:LX/0Tn;

.field public static final z:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 410560
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "voip/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 410561
    sput-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "audio_mode"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->b:LX/0Tn;

    .line 410562
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "saved_audio_mode"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->c:LX/0Tn;

    .line 410563
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "audio_mode_test"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->d:LX/0Tn;

    .line 410564
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "logging_level"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->e:LX/0Tn;

    .line 410565
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "codec_mode_override3"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->f:LX/0Tn;

    .line 410566
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "codec_rate_override3"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->g:LX/0Tn;

    .line 410567
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "voip_bwe_logging"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->h:LX/0Tn;

    .line 410568
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "audio_ec"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->i:LX/0Tn;

    .line 410569
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "ispx_initial_sub_codec"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->j:LX/0Tn;

    .line 410570
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "opispx_initial_sub_codec"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->k:LX/0Tn;

    .line 410571
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "ispx_codec_switch_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->l:LX/0Tn;

    .line 410572
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "ispx_fec_override_mode"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->m:LX/0Tn;

    .line 410573
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "isac_initial_bitrate"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->n:LX/0Tn;

    .line 410574
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "speex_initial_bitrate"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->o:LX/0Tn;

    .line 410575
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "ssl_private_key_0"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->p:LX/0Tn;

    .line 410576
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "ssl_certificate_0"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->q:LX/0Tn;

    .line 410577
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "audio_agc"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->r:LX/0Tn;

    .line 410578
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "audio_ns"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->s:LX/0Tn;

    .line 410579
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "audio_high_pass_filter"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->t:LX/0Tn;

    .line 410580
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "audio_cng"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->u:LX/0Tn;

    .line 410581
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "audio_exp_agc"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->v:LX/0Tn;

    .line 410582
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "audio_ec_mode"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->w:LX/0Tn;

    .line 410583
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "audio_agc_mode"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->x:LX/0Tn;

    .line 410584
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "audio_ns_mode"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->y:LX/0Tn;

    .line 410585
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "audio_lafns_mode"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->z:LX/0Tn;

    .line 410586
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "starve_smoothing"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->A:LX/0Tn;

    .line 410587
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "record_remote_video"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->B:LX/0Tn;

    .line 410588
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "record_remote_raw_video"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->C:LX/0Tn;

    .line 410589
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "record_self_video"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->D:LX/0Tn;

    .line 410590
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "record_self_raw_video"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->E:LX/0Tn;

    .line 410591
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "load_self_raw_video"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->F:LX/0Tn;

    .line 410592
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "loop_video_playback"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->G:LX/0Tn;

    .line 410593
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "preprocess_frames"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->H:LX/0Tn;

    .line 410594
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "record_directory"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->I:LX/0Tn;

    .line 410595
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "play_sample"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->J:LX/0Tn;

    .line 410596
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "auto_answer"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->K:LX/0Tn;

    .line 410597
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "automated_test_support"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->L:LX/0Tn;

    .line 410598
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "video_width"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->M:LX/0Tn;

    .line 410599
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "video_height"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->N:LX/0Tn;

    .line 410600
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "campon_campers"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->O:LX/0Tn;

    .line 410601
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "video_codec_mode_override"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->P:LX/0Tn;

    .line 410602
    sget-object v0, LX/2S4;->a:LX/0Tn;

    const-string v1, "should_reset_bwe_history"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S4;->Q:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 410603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
