.class public final LX/3JV;
.super LX/3JO;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3JO",
        "<",
        "LX/3Jc;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 547734
    invoke-direct {p0}, LX/3JO;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/util/JsonReader;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 547735
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 547736
    new-instance v1, LX/3JW;

    invoke-direct {v1}, LX/3JW;-><init>()V

    .line 547737
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 547738
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 547739
    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result p0

    sparse-switch p0, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 547740
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 547741
    :sswitch_0
    const-string p0, "start_frame"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string p0, "data"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    .line 547742
    :pswitch_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v0

    iput v0, v1, LX/3JW;->a:I

    goto :goto_0

    .line 547743
    :pswitch_1
    invoke-static {p1}, LX/3JX;->a(Landroid/util/JsonReader;)[F

    move-result-object v0

    iput-object v0, v1, LX/3JW;->b:[F

    goto :goto_0

    .line 547744
    :cond_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 547745
    new-instance v0, LX/3Jc;

    iget v2, v1, LX/3JW;->a:I

    iget-object p0, v1, LX/3JW;->b:[F

    invoke-direct {v0, v2, p0}, LX/3Jc;-><init>(I[F)V

    move-object v0, v0

    .line 547746
    move-object v0, v0

    .line 547747
    return-object v0

    :sswitch_data_0
    .sparse-switch
        -0x5b8680b0 -> :sswitch_0
        0x2eefaa -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
