.class public final enum LX/2h7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2h7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2h7;

.field public static final enum EVENT_GYMK:LX/2h7;

.field public static final enum FEED_FRIENDABLE_HEADER:LX/2h7;

.field public static final enum FEED_REQUESTS:LX/2h7;

.field public static final enum FRIENDING_CARD:LX/2h7;

.field public static final enum FRIENDING_RADAR:LX/2h7;

.field public static final enum FRIENDS_CENTER_FRIENDS:LX/2h7;

.field public static final enum FRIENDS_CENTER_OUTGOING_REQUESTS:LX/2h7;

.field public static final enum FRIENDS_CENTER_REQUESTS:LX/2h7;

.field public static final enum FRIENDS_CENTER_REQUESTS_PYMK:LX/2h7;

.field public static final enum FRIENDS_CENTER_SEARCH:LX/2h7;

.field public static final enum FRIENDS_CENTER_SUGGESTIONS:LX/2h7;

.field public static final enum FRIENDS_TAB:LX/2h7;

.field public static final enum FRIEND_FINDER:LX/2h7;

.field public static final enum JEWEL:LX/2h7;

.field public static final enum NEARBY_FRIENDS:LX/2h7;

.field public static final enum NUX:LX/2h7;

.field public static final enum PROFILE_BROWSER:LX/2h7;

.field public static final enum PROTILES:LX/2h7;

.field public static final enum PYMK_FEED:LX/2h7;

.field public static final enum PYMK_FRIEND_FINDER:LX/2h7;

.field public static final enum PYMK_TIMELINE:LX/2h7;

.field public static final enum PYMK_TIMELINE_CHAIN:LX/2h7;

.field public static final enum QR_CODE:LX/2h7;

.field public static final enum QUICK_PROMOTION:LX/2h7;

.field public static final enum SEARCH:LX/2h7;


# instance fields
.field public final friendRequestCancelRef:LX/2h9;

.field public final friendRequestHowFound:LX/2h8;

.field public final friendRequestResponseRef:LX/2hA;

.field public final peopleYouMayKnowLocation:LX/2hC;

.field public final removeFriendRef:LX/2hB;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    .line 449731
    new-instance v0, LX/2h7;

    const-string v1, "EVENT_GYMK"

    const/4 v2, 0x0

    sget-object v3, LX/2h8;->PROFILE_BROWSER_EVENTS:LX/2h8;

    sget-object v4, LX/2h9;->PROFILE_BROWSER_EVENTS:LX/2h9;

    sget-object v5, LX/2hA;->PROFILE_BROWSER_EVENTS:LX/2hA;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->EVENT_GYMK:LX/2h7;

    .line 449732
    new-instance v0, LX/2h7;

    const-string v1, "FEED_FRIENDABLE_HEADER"

    const/4 v2, 0x1

    sget-object v3, LX/2h8;->FEED_FRIENDABLE_HEADER:LX/2h8;

    sget-object v4, LX/2h9;->FEED_FRIENDABLE_HEADER:LX/2h9;

    sget-object v5, LX/2hA;->FEED_FRIENDABLE_HEADER:LX/2hA;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->FEED_FRIENDABLE_HEADER:LX/2h7;

    .line 449733
    new-instance v0, LX/2h7;

    const-string v1, "FEED_REQUESTS"

    const/4 v2, 0x2

    sget-object v3, LX/2h8;->NEWSFEED:LX/2h8;

    sget-object v4, LX/2h9;->FEED:LX/2h9;

    sget-object v5, LX/2hA;->NEWSFEED_FRIEND_REQUESTS:LX/2hA;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->FEED_REQUESTS:LX/2h7;

    .line 449734
    new-instance v0, LX/2h7;

    const-string v1, "FRIEND_FINDER"

    const/4 v2, 0x3

    sget-object v3, LX/2h8;->CONTACT_IMPORTER:LX/2h8;

    sget-object v4, LX/2h9;->CONTACT_IMPORTER:LX/2h9;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->FRIEND_FINDER:LX/2h7;

    .line 449735
    new-instance v0, LX/2h7;

    const-string v1, "FRIENDING_CARD"

    const/4 v2, 0x4

    sget-object v3, LX/2h8;->FRIENDING_CARD:LX/2h8;

    sget-object v4, LX/2h9;->FRIENDING_CARD:LX/2h9;

    sget-object v5, LX/2hA;->FRIENDING_CARD:LX/2hA;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->FRIENDING_CARD:LX/2h7;

    .line 449736
    new-instance v0, LX/2h7;

    const-string v1, "FRIENDING_RADAR"

    const/4 v2, 0x5

    sget-object v3, LX/2h8;->FRIENDING_RADAR:LX/2h8;

    sget-object v4, LX/2h9;->FRIENDING_RADAR:LX/2h9;

    sget-object v5, LX/2hA;->FRIENDING_RADAR:LX/2hA;

    const/4 v6, 0x0

    sget-object v7, LX/2hB;->FRIENDING_RADAR:LX/2hB;

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->FRIENDING_RADAR:LX/2h7;

    .line 449737
    new-instance v0, LX/2h7;

    const-string v1, "FRIENDS_CENTER_FRIENDS"

    const/4 v2, 0x6

    sget-object v3, LX/2h8;->FRIENDS_CENTER_FRIENDS:LX/2h8;

    sget-object v4, LX/2h9;->FRIENDS_CENTER_FRIENDS:LX/2h9;

    sget-object v5, LX/2hA;->FRIENDS_CENTER_FRIENDS:LX/2hA;

    const/4 v6, 0x0

    sget-object v7, LX/2hB;->FRIENDS_CENTER_FRIENDS:LX/2hB;

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->FRIENDS_CENTER_FRIENDS:LX/2h7;

    .line 449738
    new-instance v0, LX/2h7;

    const-string v1, "FRIENDS_CENTER_OUTGOING_REQUESTS"

    const/4 v2, 0x7

    sget-object v3, LX/2h8;->FRIENDS_CENTER_OUTGOING_REQUESTS_TAB:LX/2h8;

    sget-object v4, LX/2h9;->FRIENDS_CENTER_OUTGOING_REQUESTS_TAB:LX/2h9;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->FRIENDS_CENTER_OUTGOING_REQUESTS:LX/2h7;

    .line 449739
    new-instance v0, LX/2h7;

    const-string v1, "FRIENDS_CENTER_REQUESTS"

    const/16 v2, 0x8

    sget-object v3, LX/2h8;->FRIENDS_CENTER_REQUESTS:LX/2h8;

    sget-object v4, LX/2h9;->FRIENDS_CENTER_REQUESTS:LX/2h9;

    sget-object v5, LX/2hA;->FRIENDS_CENTER_REQUESTS:LX/2hA;

    const/4 v6, 0x0

    sget-object v7, LX/2hB;->FRIENDS_CENTER_REQUESTS:LX/2hB;

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->FRIENDS_CENTER_REQUESTS:LX/2h7;

    .line 449740
    new-instance v0, LX/2h7;

    const-string v1, "FRIENDS_CENTER_REQUESTS_PYMK"

    const/16 v2, 0x9

    sget-object v3, LX/2h8;->FRIENDS_CENTER_SUGGESTIONS:LX/2h8;

    sget-object v4, LX/2h9;->FRIENDS_CENTER_SUGGESTIONS:LX/2h9;

    const/4 v5, 0x0

    sget-object v6, LX/2hC;->FRIENDS_CENTER:LX/2hC;

    sget-object v7, LX/2hB;->FRIENDS_CENTER_REQUESTS_PYMK:LX/2hB;

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->FRIENDS_CENTER_REQUESTS_PYMK:LX/2h7;

    .line 449741
    new-instance v0, LX/2h7;

    const-string v1, "FRIENDS_CENTER_SEARCH"

    const/16 v2, 0xa

    sget-object v3, LX/2h8;->FRIENDS_CENTER_SEARCH:LX/2h8;

    sget-object v4, LX/2h9;->FRIENDS_CENTER_SEARCH:LX/2h9;

    sget-object v5, LX/2hA;->FRIENDS_CENTER_SEARCH:LX/2hA;

    const/4 v6, 0x0

    sget-object v7, LX/2hB;->FRIENDS_CENTER_SEARCH:LX/2hB;

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->FRIENDS_CENTER_SEARCH:LX/2h7;

    .line 449742
    new-instance v0, LX/2h7;

    const-string v1, "FRIENDS_CENTER_SUGGESTIONS"

    const/16 v2, 0xb

    sget-object v3, LX/2h8;->FRIENDS_CENTER_SUGGESTIONS:LX/2h8;

    sget-object v4, LX/2h9;->FRIENDS_CENTER_SUGGESTIONS:LX/2h9;

    const/4 v5, 0x0

    sget-object v6, LX/2hC;->FRIENDS_CENTER:LX/2hC;

    sget-object v7, LX/2hB;->FRIENDS_CENTER_SUGGESTIONS:LX/2hB;

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->FRIENDS_CENTER_SUGGESTIONS:LX/2h7;

    .line 449743
    new-instance v0, LX/2h7;

    const-string v1, "JEWEL"

    const/16 v2, 0xc

    sget-object v3, LX/2h8;->PYMK:LX/2h8;

    sget-object v4, LX/2h9;->PYMK_JEWEL:LX/2h9;

    sget-object v5, LX/2hA;->MOBILE_JEWEL:LX/2hA;

    sget-object v6, LX/2hC;->JEWEL:LX/2hC;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->JEWEL:LX/2h7;

    .line 449744
    new-instance v0, LX/2h7;

    const-string v1, "NEARBY_FRIENDS"

    const/16 v2, 0xd

    sget-object v3, LX/2h8;->NEARBY_FRIENDS:LX/2h8;

    sget-object v4, LX/2h9;->NEARBY_FRIENDS:LX/2h9;

    sget-object v5, LX/2hA;->NEARBY_FRIENDS:LX/2hA;

    const/4 v6, 0x0

    sget-object v7, LX/2hB;->NEARBY_FRIENDS:LX/2hB;

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->NEARBY_FRIENDS:LX/2h7;

    .line 449745
    new-instance v0, LX/2h7;

    const-string v1, "NUX"

    const/16 v2, 0xe

    sget-object v3, LX/2h8;->PYMK_NUX:LX/2h8;

    sget-object v4, LX/2h9;->PYMK_NUX:LX/2h9;

    sget-object v5, LX/2hA;->NUX:LX/2hA;

    sget-object v6, LX/2hC;->NUX:LX/2hC;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->NUX:LX/2h7;

    .line 449746
    new-instance v0, LX/2h7;

    const-string v1, "PROFILE_BROWSER"

    const/16 v2, 0xf

    sget-object v3, LX/2h8;->PROFILE_BROWSER:LX/2h8;

    sget-object v4, LX/2h9;->PROFILE_BROWSER_LIKES:LX/2h9;

    sget-object v5, LX/2hA;->PROFILE_BROWSER:LX/2hA;

    const/4 v6, 0x0

    sget-object v7, LX/2hB;->PROFILE_BROWSER:LX/2hB;

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->PROFILE_BROWSER:LX/2h7;

    .line 449747
    new-instance v0, LX/2h7;

    const-string v1, "PYMK_FEED"

    const/16 v2, 0x10

    sget-object v3, LX/2h8;->NETEGO_PYMK:LX/2h8;

    sget-object v4, LX/2h9;->PYMK_FEED:LX/2h9;

    sget-object v5, LX/2hA;->PYMK_FEED:LX/2hA;

    sget-object v6, LX/2hC;->FEED:LX/2hC;

    sget-object v7, LX/2hB;->PYMK_FEED:LX/2hB;

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->PYMK_FEED:LX/2h7;

    .line 449748
    new-instance v0, LX/2h7;

    const-string v1, "PYMK_TIMELINE"

    const/16 v2, 0x11

    sget-object v3, LX/2h8;->PYMK:LX/2h8;

    sget-object v4, LX/2h9;->TIMELINE_FRIENDS_COLLECTION:LX/2h9;

    const/4 v5, 0x0

    sget-object v6, LX/2hC;->SELF_PROFILE:LX/2hC;

    sget-object v7, LX/2hB;->TIMELINE_FRIENDS_COLLECTION:LX/2hB;

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->PYMK_TIMELINE:LX/2h7;

    .line 449749
    new-instance v0, LX/2h7;

    const-string v1, "PYMK_TIMELINE_CHAIN"

    const/16 v2, 0x12

    sget-object v3, LX/2h8;->PYMK_TIMELINE_CHAIN:LX/2h8;

    sget-object v4, LX/2h9;->TIMELINE_FRIENDS_COLLECTION:LX/2h9;

    const/4 v5, 0x0

    sget-object v6, LX/2hC;->PYMK_TIMELINE_CHAIN:LX/2hC;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->PYMK_TIMELINE_CHAIN:LX/2h7;

    .line 449750
    new-instance v0, LX/2h7;

    const-string v1, "PYMK_FRIEND_FINDER"

    const/16 v2, 0x13

    sget-object v3, LX/2h8;->CI_PYMK:LX/2h8;

    sget-object v4, LX/2h9;->PYMK_CI:LX/2h9;

    const/4 v5, 0x0

    sget-object v6, LX/2hC;->FRIEND_FINDER:LX/2hC;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->PYMK_FRIEND_FINDER:LX/2h7;

    .line 449751
    new-instance v0, LX/2h7;

    const-string v1, "QR_CODE"

    const/16 v2, 0x14

    sget-object v3, LX/2h8;->ENTITY_CARDS:LX/2h8;

    sget-object v4, LX/2h9;->QR_CODE:LX/2h9;

    sget-object v5, LX/2hA;->QR_CODE:LX/2hA;

    const/4 v6, 0x0

    sget-object v7, LX/2hB;->QR_CODE:LX/2hB;

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->QR_CODE:LX/2h7;

    .line 449752
    new-instance v0, LX/2h7;

    const-string v1, "QUICK_PROMOTION"

    const/16 v2, 0x15

    sget-object v3, LX/2h8;->PYMK:LX/2h8;

    sget-object v4, LX/2h9;->PYMK_QUICK_PROMOTION:LX/2h9;

    sget-object v5, LX/2hA;->QUICK_PROMOTION:LX/2hA;

    sget-object v6, LX/2hC;->QUICK_PROMOTION:LX/2hC;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->QUICK_PROMOTION:LX/2h7;

    .line 449753
    new-instance v0, LX/2h7;

    const-string v1, "SEARCH"

    const/16 v2, 0x16

    sget-object v3, LX/2h8;->SEARCH:LX/2h8;

    sget-object v4, LX/2h9;->SEARCH:LX/2h9;

    sget-object v5, LX/2hA;->SEARCH:LX/2hA;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->SEARCH:LX/2h7;

    .line 449754
    new-instance v0, LX/2h7;

    const-string v1, "FRIENDS_TAB"

    const/16 v2, 0x17

    sget-object v3, LX/2h8;->FRIENDS_TAB:LX/2h8;

    sget-object v4, LX/2h9;->FRIENDS_TAB:LX/2h9;

    sget-object v5, LX/2hA;->FRIENDS_TAB:LX/2hA;

    const/4 v6, 0x0

    sget-object v7, LX/2hB;->FRIENDS_TAB:LX/2hB;

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->FRIENDS_TAB:LX/2h7;

    .line 449755
    new-instance v0, LX/2h7;

    const-string v1, "PROTILES"

    const/16 v2, 0x18

    sget-object v3, LX/2h8;->PROFILE_FRIENDS_BOX:LX/2h8;

    sget-object v4, LX/2h9;->FRIEND_LIST_PROFILE:LX/2h9;

    sget-object v5, LX/2hA;->FRIEND_LIST_PROFILE:LX/2hA;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/2h7;-><init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V

    sput-object v0, LX/2h7;->PROTILES:LX/2h7;

    .line 449756
    const/16 v0, 0x19

    new-array v0, v0, [LX/2h7;

    const/4 v1, 0x0

    sget-object v2, LX/2h7;->EVENT_GYMK:LX/2h7;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/2h7;->FEED_FRIENDABLE_HEADER:LX/2h7;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/2h7;->FEED_REQUESTS:LX/2h7;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/2h7;->FRIEND_FINDER:LX/2h7;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/2h7;->FRIENDING_CARD:LX/2h7;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/2h7;->FRIENDING_RADAR:LX/2h7;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2h7;->FRIENDS_CENTER_FRIENDS:LX/2h7;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2h7;->FRIENDS_CENTER_OUTGOING_REQUESTS:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2h7;->FRIENDS_CENTER_REQUESTS:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2h7;->FRIENDS_CENTER_REQUESTS_PYMK:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/2h7;->FRIENDS_CENTER_SEARCH:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/2h7;->FRIENDS_CENTER_SUGGESTIONS:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/2h7;->JEWEL:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/2h7;->NEARBY_FRIENDS:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/2h7;->NUX:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/2h7;->PROFILE_BROWSER:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/2h7;->PYMK_FEED:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/2h7;->PYMK_TIMELINE:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/2h7;->PYMK_TIMELINE_CHAIN:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/2h7;->PYMK_FRIEND_FINDER:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/2h7;->QR_CODE:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/2h7;->QUICK_PROMOTION:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/2h7;->SEARCH:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/2h7;->FRIENDS_TAB:LX/2h7;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/2h7;->PROTILES:LX/2h7;

    aput-object v2, v0, v1

    sput-object v0, LX/2h7;->$VALUES:[LX/2h7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/2h8;LX/2h9;LX/2hA;LX/2hC;LX/2hB;)V
    .locals 0
    .param p5    # LX/2hA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/2hC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/2hB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2h8;",
            "LX/2h9;",
            "LX/2hA;",
            "LX/2hC;",
            "LX/2hB;",
            ")V"
        }
    .end annotation

    .prologue
    .line 449757
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 449758
    iput-object p3, p0, LX/2h7;->friendRequestHowFound:LX/2h8;

    .line 449759
    iput-object p4, p0, LX/2h7;->friendRequestCancelRef:LX/2h9;

    .line 449760
    iput-object p5, p0, LX/2h7;->friendRequestResponseRef:LX/2hA;

    .line 449761
    iput-object p6, p0, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    .line 449762
    iput-object p7, p0, LX/2h7;->removeFriendRef:LX/2hB;

    .line 449763
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2h7;
    .locals 1

    .prologue
    .line 449764
    const-class v0, LX/2h7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2h7;

    return-object v0
.end method

.method public static values()[LX/2h7;
    .locals 1

    .prologue
    .line 449765
    sget-object v0, LX/2h7;->$VALUES:[LX/2h7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2h7;

    return-object v0
.end method
