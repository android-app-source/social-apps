.class public final LX/351;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/350;

.field private b:F

.field private c:F

.field private d:I

.field private final e:LX/31j;


# direct methods
.method public constructor <init>(LX/350;LX/31j;)V
    .locals 0

    .prologue
    .line 496341
    iput-object p1, p0, LX/351;->a:LX/350;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 496342
    iput-object p2, p0, LX/351;->e:LX/31j;

    .line 496343
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x2

    const v0, 0x3790f26f

    invoke-static {v4, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 496349
    iget v1, p0, LX/351;->d:I

    if-ne v1, v2, :cond_0

    .line 496350
    iget-object v1, p0, LX/351;->e:LX/31j;

    iget v2, p0, LX/351;->b:F

    iget v3, p0, LX/351;->c:F

    invoke-interface {v1, p1, v2, v3}, LX/31j;->onClick(Landroid/view/View;FF)V

    .line 496351
    :cond_0
    const v1, 0x16215e1

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 496344
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 496345
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, LX/351;->b:F

    .line 496346
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, LX/351;->c:F

    .line 496347
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    iput v0, p0, LX/351;->d:I

    .line 496348
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
