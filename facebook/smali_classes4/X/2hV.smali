.class public LX/2hV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2hV;


# instance fields
.field public a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/4nS;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field public final c:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 450016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450017
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/2hV;->c:Landroid/os/Handler;

    .line 450018
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/2hV;->a:Ljava/util/Queue;

    .line 450019
    return-void
.end method

.method public static a(LX/0QB;)LX/2hV;
    .locals 3

    .prologue
    .line 450024
    sget-object v0, LX/2hV;->d:LX/2hV;

    if-nez v0, :cond_1

    .line 450025
    const-class v1, LX/2hV;

    monitor-enter v1

    .line 450026
    :try_start_0
    sget-object v0, LX/2hV;->d:LX/2hV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 450027
    if-eqz v2, :cond_0

    .line 450028
    :try_start_1
    new-instance v0, LX/2hV;

    invoke-direct {v0}, LX/2hV;-><init>()V

    .line 450029
    move-object v0, v0

    .line 450030
    sput-object v0, LX/2hV;->d:LX/2hV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 450031
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 450032
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 450033
    :cond_1
    sget-object v0, LX/2hV;->d:LX/2hV;

    return-object v0

    .line 450034
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 450035
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2hV;)V
    .locals 3

    .prologue
    .line 450020
    iget-object v0, p0, LX/2hV;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4nS;

    .line 450021
    if-nez v0, :cond_0

    .line 450022
    :goto_0
    return-void

    .line 450023
    :cond_0
    iget-object v1, p0, LX/2hV;->c:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/ui/toaster/ClickableToastCoordinator$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/ui/toaster/ClickableToastCoordinator$2;-><init>(LX/2hV;LX/4nS;)V

    const v0, -0x77ca2075

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
