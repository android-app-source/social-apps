.class public final LX/39z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 0

    .prologue
    .line 524164
    iput-object p1, p0, LX/39z;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 2

    .prologue
    .line 524165
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;

    .line 524166
    iget-object v1, v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;->a:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 524167
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 524168
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$ApprovalStatus;

    .line 524169
    iget-object v1, v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$ApprovalStatus;->a:Ljava/lang/Boolean;

    move-object v0, v1

    .line 524170
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524171
    iget-object v0, p0, LX/39z;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    new-instance v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$25$1;

    invoke-direct {v1, p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$25$1;-><init>(LX/39z;)V

    invoke-virtual {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 524172
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 524173
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 524174
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/39z;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
