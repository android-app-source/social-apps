.class public final enum LX/2ej;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2ej;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2ej;

.field public static final enum NOT_SPONSORED:LX/2ej;

.field public static final enum SPONSORED:LX/2ej;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 445460
    new-instance v0, LX/2ej;

    const-string v1, "SPONSORED"

    invoke-direct {v0, v1, v2}, LX/2ej;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ej;->SPONSORED:LX/2ej;

    .line 445461
    new-instance v0, LX/2ej;

    const-string v1, "NOT_SPONSORED"

    invoke-direct {v0, v1, v3}, LX/2ej;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ej;->NOT_SPONSORED:LX/2ej;

    .line 445462
    const/4 v0, 0x2

    new-array v0, v0, [LX/2ej;

    sget-object v1, LX/2ej;->SPONSORED:LX/2ej;

    aput-object v1, v0, v2

    sget-object v1, LX/2ej;->NOT_SPONSORED:LX/2ej;

    aput-object v1, v0, v3

    sput-object v0, LX/2ej;->$VALUES:[LX/2ej;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 445463
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2ej;
    .locals 1

    .prologue
    .line 445464
    const-class v0, LX/2ej;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2ej;

    return-object v0
.end method

.method public static values()[LX/2ej;
    .locals 1

    .prologue
    .line 445465
    sget-object v0, LX/2ej;->$VALUES:[LX/2ej;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2ej;

    return-object v0
.end method
