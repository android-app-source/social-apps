.class public final LX/3Br;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/friends/model/PersonYouMayKnow;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V
    .locals 0

    .prologue
    .line 529168
    iput-object p1, p0, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iput-boolean p2, p0, LX/3Br;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 529169
    iget-object v0, p0, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-boolean v1, p0, LX/3Br;->a:Z

    invoke-static {v0, v1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->e(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    .line 529170
    iget-object v0, p0, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-boolean v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aB:Z

    if-eqz v0, :cond_0

    .line 529171
    iget-object v0, p0, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    iget-object v1, p0, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->D:LX/2e3;

    invoke-virtual {v1}, LX/2e3;->c()I

    move-result v1

    iget-object v2, p0, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v2, v2, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    iget-object v2, v2, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    sget-object v3, Lcom/facebook/friending/jewel/FriendRequestsFragment;->U:Lcom/facebook/common/callercontext/CallerContext;

    new-instance v4, LX/Eyj;

    invoke-direct {v4, p0}, LX/Eyj;-><init>(LX/3Br;)V

    .line 529172
    const/4 v7, 0x0

    move-object v5, v0

    move v6, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    invoke-static/range {v5 .. v10}, LX/2dj;->a(LX/2dj;ILjava/lang/Integer;LX/2hC;Lcom/facebook/common/callercontext/CallerContext;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 529173
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->w(Lcom/facebook/friending/jewel/FriendRequestsFragment;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
