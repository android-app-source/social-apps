.class public LX/2Zy;
.super LX/16B;
.source ""

# interfaces
.implements LX/2Zz;
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile m:LX/2Zy;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/2a0;

.field public final d:LX/0u7;

.field public final e:LX/0Xl;

.field public final f:Ljava/util/concurrent/ScheduledExecutorService;

.field public final g:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
    .end annotation
.end field

.field private final h:LX/0Wd;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Landroid/content/Intent;

.field public final k:Ljava/lang/Runnable;

.field public volatile l:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 423130
    const-class v0, LX/2Zy;

    sput-object v0, LX/2Zy;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/2a0;LX/0u7;LX/0Xl;Ljava/util/concurrent/ScheduledExecutorService;Landroid/os/Handler;LX/0Wd;LX/0Ot;)V
    .locals 3
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadWakeup;
        .end annotation
    .end param
    .param p6    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .param p7    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2a0;",
            "LX/0u7;",
            "LX/0Xl;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Landroid/os/Handler;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 423162
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 423163
    new-instance v0, Lcom/facebook/conditionalworker/ConditionalWorkerManager$1;

    sget-object v1, LX/2Zy;->a:Ljava/lang/Class;

    const-string v2, "StartingServiceRunnable"

    invoke-direct {v0, p0, v1, v2}, Lcom/facebook/conditionalworker/ConditionalWorkerManager$1;-><init>(LX/2Zy;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v0, p0, LX/2Zy;->k:Ljava/lang/Runnable;

    .line 423164
    iput-object p1, p0, LX/2Zy;->b:Landroid/content/Context;

    .line 423165
    iput-object p2, p0, LX/2Zy;->c:LX/2a0;

    .line 423166
    iput-object p3, p0, LX/2Zy;->d:LX/0u7;

    .line 423167
    iput-object p4, p0, LX/2Zy;->e:LX/0Xl;

    .line 423168
    iput-object p5, p0, LX/2Zy;->f:Ljava/util/concurrent/ScheduledExecutorService;

    .line 423169
    iput-object p6, p0, LX/2Zy;->g:Landroid/os/Handler;

    .line 423170
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2Zy;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/conditionalworker/ConditionalWorkerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, LX/2Zy;->j:Landroid/content/Intent;

    .line 423171
    iput-object p7, p0, LX/2Zy;->h:LX/0Wd;

    .line 423172
    iput-object p8, p0, LX/2Zy;->i:LX/0Ot;

    .line 423173
    return-void
.end method

.method public static a(LX/0QB;)LX/2Zy;
    .locals 12

    .prologue
    .line 423149
    sget-object v0, LX/2Zy;->m:LX/2Zy;

    if-nez v0, :cond_1

    .line 423150
    const-class v1, LX/2Zy;

    monitor-enter v1

    .line 423151
    :try_start_0
    sget-object v0, LX/2Zy;->m:LX/2Zy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 423152
    if-eqz v2, :cond_0

    .line 423153
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 423154
    new-instance v3, LX/2Zy;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/2a0;->a(LX/0QB;)LX/2a0;

    move-result-object v5

    check-cast v5, LX/2a0;

    invoke-static {v0}, LX/0u7;->a(LX/0QB;)LX/0u7;

    move-result-object v6

    check-cast v6, LX/0u7;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-static {v0}, LX/2a1;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Zw;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v9

    check-cast v9, Landroid/os/Handler;

    invoke-static {v0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v10

    check-cast v10, LX/0Wd;

    const/16 v11, 0x259

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, LX/2Zy;-><init>(Landroid/content/Context;LX/2a0;LX/0u7;LX/0Xl;Ljava/util/concurrent/ScheduledExecutorService;Landroid/os/Handler;LX/0Wd;LX/0Ot;)V

    .line 423155
    move-object v0, v3

    .line 423156
    sput-object v0, LX/2Zy;->m:LX/2Zy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423157
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 423158
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 423159
    :cond_1
    sget-object v0, LX/2Zy;->m:LX/2Zy;

    return-object v0

    .line 423160
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 423161
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static l(LX/2Zy;)V
    .locals 3

    .prologue
    .line 423142
    iget-object v0, p0, LX/2Zy;->c:LX/2a0;

    .line 423143
    iput-object p0, v0, LX/2a0;->d:LX/2Zy;

    .line 423144
    iget-object v1, v0, LX/2a0;->c:LX/0Yb;

    if-eqz v1, :cond_0

    .line 423145
    :goto_0
    return-void

    .line 423146
    :cond_0
    invoke-static {v0}, LX/2a0;->b(LX/2a0;)LX/2Im;

    move-result-object v1

    iput-object v1, v0, LX/2a0;->e:LX/2Im;

    .line 423147
    iget-object v1, v0, LX/2a0;->b:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    new-instance p0, LX/30N;

    invoke-direct {p0, v0}, LX/30N;-><init>(LX/2a0;)V

    invoke-interface {v1, v2, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, v0, LX/2a0;->c:LX/0Yb;

    .line 423148
    iget-object v1, v0, LX/2a0;->c:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    goto :goto_0
.end method

.method public static n(LX/2Zy;)V
    .locals 4

    .prologue
    .line 423174
    :try_start_0
    iget-object v0, p0, LX/2Zy;->b:Landroid/content/Context;

    iget-object v1, p0, LX/2Zy;->j:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 423175
    :goto_0
    return-void

    .line 423176
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 423177
    iget-object v0, p0, LX/2Zy;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "ConditionalWorkerManager"

    const-string v3, "Starting service failure"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 423140
    invoke-static {p0}, LX/2Zy;->n(LX/2Zy;)V

    .line 423141
    return-void
.end method

.method public final a(Lcom/facebook/auth/component/AuthenticationResult;)V
    .locals 3
    .param p1    # Lcom/facebook/auth/component/AuthenticationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 423138
    iget-object v0, p0, LX/2Zy;->h:LX/0Wd;

    new-instance v1, Lcom/facebook/conditionalworker/ConditionalWorkerManager$2;

    invoke-direct {v1, p0}, Lcom/facebook/conditionalworker/ConditionalWorkerManager$2;-><init>(LX/2Zy;)V

    const v2, 0x76faee58

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 423139
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 423136
    invoke-static {p0}, LX/2Zy;->n(LX/2Zy;)V

    .line 423137
    return-void
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 423131
    iget-object v0, p0, LX/2Zy;->e:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v2, LX/2IO;

    invoke-direct {v2, p0}, LX/2IO;-><init>(LX/2Zy;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v2, LX/2IQ;

    invoke-direct {v2, p0}, LX/2IQ;-><init>(LX/2Zy;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/2Zy;->g:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 423132
    invoke-static {p0}, LX/2Zy;->l(LX/2Zy;)V

    .line 423133
    iget-object v0, p0, LX/2Zy;->d:LX/0u7;

    invoke-virtual {v0, p0}, LX/0u7;->a(LX/2Zz;)V

    .line 423134
    invoke-static {p0}, LX/2Zy;->n(LX/2Zy;)V

    .line 423135
    return-void
.end method
