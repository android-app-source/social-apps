.class public LX/2OI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0SF;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 400699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400700
    iput-object p1, p0, LX/2OI;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 400701
    iput-object p2, p0, LX/2OI;->b:LX/0SF;

    .line 400702
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 400703
    iget-object v0, p0, LX/2OI;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 400704
    :cond_0
    return-void

    .line 400705
    :cond_1
    sget-object v0, LX/0db;->aN:LX/0Tn;

    sget-object v1, LX/0db;->aL:LX/0Tn;

    sget-object v2, LX/0db;->aM:LX/0Tn;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 400706
    iget-object v0, p0, LX/2OI;->b:LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v4

    .line 400707
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 400708
    iget-object v6, p0, LX/2OI;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6, v0, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    sub-long v6, v4, v6

    .line 400709
    cmp-long v8, v6, v10

    if-ltz v8, :cond_2

    const-wide/32 v8, 0x36ee80

    cmp-long v6, v6, v8

    if-lez v6, :cond_3

    .line 400710
    :cond_2
    iget-object v6, p0, LX/2OI;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    invoke-interface {v6, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 400711
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
