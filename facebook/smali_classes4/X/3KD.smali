.class public LX/3KD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Z9;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1wc;

.field private final c:LX/03V;

.field private d:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 548377
    const-class v0, LX/3KD;

    sput-object v0, LX/3KD;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1wc;LX/03V;)V
    .locals 2
    .param p1    # LX/1wc;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 548378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 548379
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/3KD;->d:J

    .line 548380
    iput-object p1, p0, LX/3KD;->b:LX/1wc;

    .line 548381
    iput-object p2, p0, LX/3KD;->c:LX/03V;

    .line 548382
    return-void
.end method
