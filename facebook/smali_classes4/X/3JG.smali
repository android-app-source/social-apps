.class public LX/3JG;
.super LX/2oV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "LX/392;",
        ":",
        "LX/3FR;",
        ">",
        "LX/2oV",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/2oL;

.field private final b:LX/2oO;

.field public final c:LX/093;

.field public final d:Lcom/facebook/video/engine/VideoPlayerParams;

.field public final e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

.field public final f:LX/04D;

.field public final g:LX/1Bv;

.field public final h:LX/1C2;

.field public final i:LX/198;

.field private final j:LX/1YQ;

.field public k:Z

.field private l:Z

.field public final m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0iY;

.field private final o:LX/0iX;


# direct methods
.method public constructor <init>(LX/1YQ;LX/2oL;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;LX/1Bv;LX/1C2;LX/198;LX/0iY;LX/0iX;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 547580
    iget-object v0, p4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/2oV;-><init>(Ljava/lang/String;)V

    .line 547581
    iput-boolean v1, p0, LX/3JG;->k:Z

    .line 547582
    iput-boolean v1, p0, LX/3JG;->l:Z

    .line 547583
    iput-object p2, p0, LX/3JG;->a:LX/2oL;

    .line 547584
    invoke-virtual {p2}, LX/2oL;->b()LX/2oO;

    move-result-object v0

    iput-object v0, p0, LX/3JG;->b:LX/2oO;

    .line 547585
    iput-object p3, p0, LX/3JG;->c:LX/093;

    .line 547586
    iput-object p4, p0, LX/3JG;->d:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 547587
    iput-object p5, p0, LX/3JG;->e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 547588
    iput-object p6, p0, LX/3JG;->f:LX/04D;

    .line 547589
    iput-object p7, p0, LX/3JG;->g:LX/1Bv;

    .line 547590
    iput-object p8, p0, LX/3JG;->h:LX/1C2;

    .line 547591
    iput-object p9, p0, LX/3JG;->i:LX/198;

    .line 547592
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/3JG;->m:Ljava/util/Set;

    .line 547593
    iput-object p1, p0, LX/3JG;->j:LX/1YQ;

    .line 547594
    iput-object p10, p0, LX/3JG;->n:LX/0iY;

    .line 547595
    iput-object p11, p0, LX/3JG;->o:LX/0iX;

    .line 547596
    return-void
.end method

.method private c(LX/392;)V
    .locals 3
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 547520
    iget-boolean v0, p0, LX/3JG;->k:Z

    if-nez v0, :cond_0

    .line 547521
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3JG;->k:Z

    .line 547522
    iget-object v0, p0, LX/3JG;->i:LX/198;

    invoke-static {}, LX/5Mr;->c()LX/5Mr;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/198;->a(LX/1YD;)V

    .line 547523
    :cond_0
    iget-object v0, p0, LX/3JG;->n:LX/0iY;

    invoke-virtual {v0}, LX/0iY;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3JG;->o:LX/0iX;

    .line 547524
    iget-object v1, v0, LX/0iX;->l:LX/1rN;

    move-object v0, v1

    .line 547525
    sget-object v1, LX/1rN;->UNKNOWN:LX/1rN;

    if-ne v0, v1, :cond_1

    .line 547526
    iget-object v0, p0, LX/3JG;->o:LX/0iX;

    invoke-virtual {v0}, LX/0iX;->d()V

    .line 547527
    iget-object v0, p0, LX/3JG;->o:LX/0iX;

    .line 547528
    iget-object v1, v0, LX/0iX;->l:LX/1rN;

    move-object v0, v1

    .line 547529
    sget-object v1, LX/1rN;->ON:LX/1rN;

    if-ne v0, v1, :cond_1

    .line 547530
    iget-object v0, p0, LX/3JG;->o:LX/0iX;

    const/4 v1, 0x0

    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1, v2}, LX/0iX;->b(ZLX/04g;)V

    .line 547531
    :cond_1
    iget-object v0, p0, LX/3JG;->a:LX/2oL;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, LX/2oL;->a(LX/04g;)V

    move-object v0, p1

    .line 547532
    check-cast v0, LX/3FR;

    invoke-interface {v0}, LX/3FR;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 547533
    iget-object v0, p0, LX/3JG;->e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    .line 547534
    iput-object v1, v0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    .line 547535
    move-object v0, p1

    .line 547536
    check-cast v0, LX/3FR;

    invoke-interface {v0}, LX/3FR;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iget-object v1, p0, LX/3JG;->e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 547537
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    move-object v1, v2

    .line 547538
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setChannelEligibility(LX/04H;)V

    .line 547539
    check-cast p1, LX/3FR;

    invoke-interface {p1}, LX/3FR;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iget-object v1, p0, LX/3JG;->a:LX/2oL;

    invoke-virtual {v1}, LX/2oL;->c()LX/04g;

    move-result-object v1

    iget-object v2, p0, LX/3JG;->a:LX/2oL;

    invoke-virtual {v2}, LX/2oL;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;I)V

    .line 547540
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 547562
    check-cast p1, LX/392;

    .line 547563
    iget-boolean v0, p0, LX/3JG;->l:Z

    if-nez v0, :cond_1

    .line 547564
    :cond_0
    :goto_0
    return-void

    .line 547565
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3JG;->l:Z

    .line 547566
    iget-object v0, p0, LX/3JG;->j:LX/1YQ;

    .line 547567
    iget-object v1, v0, LX/1YQ;->i:Ljava/util/Timer;

    if-eqz v1, :cond_2

    .line 547568
    iget-object v1, v0, LX/1YQ;->i:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 547569
    :cond_2
    move-object v0, p1

    .line 547570
    check-cast v0, LX/3FR;

    invoke-interface {v0}, LX/3FR;->y()V

    .line 547571
    iget-object v0, p0, LX/3JG;->b:LX/2oO;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3JG;->b:LX/2oO;

    .line 547572
    iget-boolean v1, v0, LX/2oO;->t:Z

    move v0, v1

    .line 547573
    if-nez v0, :cond_0

    .line 547574
    :cond_3
    iget-boolean v0, p0, LX/3JG;->k:Z

    if-eqz v0, :cond_4

    .line 547575
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3JG;->k:Z

    .line 547576
    iget-object v0, p0, LX/3JG;->i:LX/198;

    invoke-static {}, LX/5Mr;->c()LX/5Mr;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/198;->b(LX/1YD;)V

    .line 547577
    :cond_4
    iget-object v1, p0, LX/3JG;->a:LX/2oL;

    move-object v0, p1

    check-cast v0, LX/3FR;

    invoke-interface {v0}, LX/3FR;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    invoke-virtual {v1, v0}, LX/2oL;->a(I)V

    .line 547578
    check-cast p1, LX/3FR;

    invoke-interface {p1}, LX/3FR;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iget-object v1, p0, LX/3JG;->a:LX/2oL;

    invoke-virtual {v1}, LX/2oL;->c()LX/04g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 547579
    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 547541
    check-cast p1, LX/392;

    .line 547542
    iget-boolean v0, p0, LX/3JG;->l:Z

    if-eqz v0, :cond_0

    .line 547543
    :goto_0
    return-void

    .line 547544
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3JG;->l:Z

    .line 547545
    iget-object v0, p0, LX/3JG;->b:LX/2oO;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 547546
    iget-object v0, p0, LX/3JG;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 547547
    iget-object v0, p0, LX/3JG;->b:LX/2oO;

    iget-object v1, p0, LX/3JG;->m:Ljava/util/Set;

    iget-object v2, p0, LX/3JG;->a:LX/2oL;

    .line 547548
    iget-boolean v3, v2, LX/2oL;->d:Z

    move v2, v3

    .line 547549
    invoke-virtual {v0, v1, v2}, LX/2oO;->a(Ljava/util/Set;Z)Z

    move-result v0

    .line 547550
    iget-object v3, p0, LX/3JG;->c:LX/093;

    .line 547551
    iget-boolean v4, v3, LX/093;->l:Z

    move v3, v4

    .line 547552
    if-eqz v3, :cond_2

    .line 547553
    :goto_1
    if-eqz v0, :cond_1

    .line 547554
    invoke-direct {p0, p1}, LX/3JG;->c(LX/392;)V

    .line 547555
    :cond_1
    iget-object v1, p0, LX/3JG;->j:LX/1YQ;

    iget-object v2, p0, LX/3JG;->b:LX/2oO;

    invoke-virtual {v1, v2, v0}, LX/1YQ;->a(LX/2oO;Z)V

    .line 547556
    check-cast p1, LX/3FR;

    invoke-interface {p1}, LX/3FR;->x()V

    goto :goto_0

    .line 547557
    :cond_2
    iget-object v3, p0, LX/3JG;->c:LX/093;

    iget-object v4, p0, LX/3JG;->m:Ljava/util/Set;

    iget-object v5, p0, LX/3JG;->g:LX/1Bv;

    iget-object v6, p0, LX/3JG;->h:LX/1C2;

    iget-object v7, p0, LX/3JG;->d:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v7, v7, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    .line 547558
    iget-object v8, p0, LX/2oV;->a:Ljava/lang/String;

    move-object v8, v8

    .line 547559
    iget-object v9, p0, LX/3JG;->f:LX/04D;

    iget-object v10, p0, LX/3JG;->e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 547560
    iget-object v11, v10, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    move-object v10, v11

    .line 547561
    iget-object v11, p0, LX/3JG;->d:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-static/range {v3 .. v11}, LX/3In;->a(LX/093;Ljava/util/Set;LX/1Bv;LX/1C2;LX/0lF;Ljava/lang/String;LX/04D;LX/04H;LX/098;)V

    goto :goto_1
.end method
