.class public LX/2ld;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2lI;",
            ">;"
        }
    .end annotation
.end field

.field public final b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 458141
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 458142
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/2ld;->a:Ljava/util/List;

    .line 458143
    iput p1, p0, LX/2ld;->b:I

    .line 458144
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 458145
    iget-object v0, p0, LX/2ld;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 458146
    iget-object v0, p0, LX/2ld;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 458147
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 458148
    iget-object v0, p0, LX/2ld;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2lI;

    invoke-interface {v0}, LX/2lI;->a()LX/2lb;

    move-result-object v0

    invoke-interface {v0}, LX/2lb;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 458149
    iget-object v0, p0, LX/2ld;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2lI;

    .line 458150
    invoke-interface {v0, p1, p2, p3}, LX/2lI;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 458151
    iget v0, p0, LX/2ld;->b:I

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    .line 458152
    iget-object v0, p0, LX/2ld;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2lI;

    .line 458153
    instance-of v1, v0, LX/EhP;

    if-eqz v1, :cond_0

    check-cast v0, LX/EhP;

    iget-object v0, v0, LX/2lH;->d:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 458154
    const/4 v0, 0x0

    .line 458155
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
