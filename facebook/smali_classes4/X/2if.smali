.class public LX/2if;
.super LX/1Yh;
.source ""


# instance fields
.field private final d:LX/2iK;

.field public final e:Lcom/facebook/common/callercontext/CallerContext;

.field public final f:LX/1HI;

.field private final g:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1HI;Lcom/facebook/common/callercontext/CallerContext;LX/0g8;LX/2iK;I)V
    .locals 1
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0g8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/2iK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 451917
    invoke-direct {p0, p3, p5}, LX/1Yh;-><init>(LX/0g8;I)V

    .line 451918
    iput-object p4, p0, LX/2if;->d:LX/2iK;

    .line 451919
    iput-object p2, p0, LX/2if;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 451920
    iput-object p1, p0, LX/2if;->f:LX/1HI;

    .line 451921
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/2if;->g:Ljava/util/WeakHashMap;

    .line 451922
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 451923
    iget-object v0, p0, LX/2if;->d:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 451924
    :cond_0
    :goto_0
    return-void

    .line 451925
    :cond_1
    iget-object v0, p0, LX/2if;->d:LX/2iK;

    invoke-virtual {v0, p1}, LX/2iK;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 451926
    if-eqz v1, :cond_0

    instance-of v0, v1, LX/2lq;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 451927
    check-cast v0, LX/2lq;

    invoke-interface {v0}, LX/2lq;->d()Ljava/lang/String;

    move-result-object v0

    .line 451928
    if-eqz v0, :cond_0

    .line 451929
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 451930
    iget-object v2, p0, LX/2if;->f:LX/1HI;

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v3

    invoke-virtual {v3}, LX/1bX;->n()LX/1bf;

    move-result-object v3

    iget-object p1, p0, LX/2if;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, p1}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v2

    invoke-static {v2}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v2

    move-object v0, v2

    .line 451931
    iget-object v2, p0, LX/2if;->g:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(I)Z
    .locals 2

    .prologue
    .line 451932
    iget-object v0, p0, LX/2if;->d:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/2if;->g:Ljava/util/WeakHashMap;

    iget-object v1, p0, LX/2if;->d:LX/2iK;

    invoke-virtual {v1, p1}, LX/2iK;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 451933
    iget-object v0, p0, LX/2if;->d:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 451934
    :cond_0
    :goto_0
    return-void

    .line 451935
    :cond_1
    iget-object v0, p0, LX/2if;->d:LX/2iK;

    invoke-virtual {v0, p1}, LX/2iK;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 451936
    if-eqz v0, :cond_0

    .line 451937
    iget-object v1, p0, LX/2if;->g:Ljava/util/WeakHashMap;

    invoke-virtual {v1, v0}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
