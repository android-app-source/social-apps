.class public LX/2jt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2js;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2js",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 454269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 454270
    check-cast p1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    .line 454271
    new-instance v0, LX/0cA;

    invoke-direct {v0}, LX/0cA;-><init>()V

    .line 454272
    if-eqz p1, :cond_1

    .line 454273
    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->hZ_()Ljava/lang/String;

    move-result-object v1

    .line 454274
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 454275
    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 454276
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 454277
    if-eqz v1, :cond_1

    .line 454278
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 454279
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 454280
    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 454281
    :cond_1
    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method
