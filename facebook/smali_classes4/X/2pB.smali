.class public final enum LX/2pB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2pB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2pB;

.field public static final enum NEEDS_TO_RECYCLE_SURFACETEXTURE_FOR_REGULAR_VIDEO:LX/2pB;

.field public static final enum NEEDS_TO_RELEASE_SURFACETEXTURE:LX/2pB;

.field public static final enum NOT_INITIALIZED:LX/2pB;

.field public static final enum USES_MANAGED_SURFACETEXTURE:LX/2pB;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 467800
    new-instance v0, LX/2pB;

    const-string v1, "NOT_INITIALIZED"

    invoke-direct {v0, v1, v2}, LX/2pB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2pB;->NOT_INITIALIZED:LX/2pB;

    .line 467801
    new-instance v0, LX/2pB;

    const-string v1, "NEEDS_TO_RELEASE_SURFACETEXTURE"

    invoke-direct {v0, v1, v3}, LX/2pB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2pB;->NEEDS_TO_RELEASE_SURFACETEXTURE:LX/2pB;

    .line 467802
    new-instance v0, LX/2pB;

    const-string v1, "NEEDS_TO_RECYCLE_SURFACETEXTURE_FOR_REGULAR_VIDEO"

    invoke-direct {v0, v1, v4}, LX/2pB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2pB;->NEEDS_TO_RECYCLE_SURFACETEXTURE_FOR_REGULAR_VIDEO:LX/2pB;

    .line 467803
    new-instance v0, LX/2pB;

    const-string v1, "USES_MANAGED_SURFACETEXTURE"

    invoke-direct {v0, v1, v5}, LX/2pB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2pB;->USES_MANAGED_SURFACETEXTURE:LX/2pB;

    .line 467804
    const/4 v0, 0x4

    new-array v0, v0, [LX/2pB;

    sget-object v1, LX/2pB;->NOT_INITIALIZED:LX/2pB;

    aput-object v1, v0, v2

    sget-object v1, LX/2pB;->NEEDS_TO_RELEASE_SURFACETEXTURE:LX/2pB;

    aput-object v1, v0, v3

    sget-object v1, LX/2pB;->NEEDS_TO_RECYCLE_SURFACETEXTURE_FOR_REGULAR_VIDEO:LX/2pB;

    aput-object v1, v0, v4

    sget-object v1, LX/2pB;->USES_MANAGED_SURFACETEXTURE:LX/2pB;

    aput-object v1, v0, v5

    sput-object v0, LX/2pB;->$VALUES:[LX/2pB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 467805
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2pB;
    .locals 1

    .prologue
    .line 467806
    const-class v0, LX/2pB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2pB;

    return-object v0
.end method

.method public static values()[LX/2pB;
    .locals 1

    .prologue
    .line 467807
    sget-object v0, LX/2pB;->$VALUES:[LX/2pB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2pB;

    return-object v0
.end method
