.class public LX/2fv;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field private static final o:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 447660
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "/ia_sample"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 447661
    sput-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "bookmark"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->a:LX/0Tn;

    .line 447662
    sget-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "last_article_info"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->b:LX/0Tn;

    .line 447663
    sget-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "last_article_open_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->c:LX/0Tn;

    .line 447664
    sget-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "show_logged_events_at_session_exit"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->d:LX/0Tn;

    .line 447665
    sget-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "number_of_times_carousel_nux_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->e:LX/0Tn;

    .line 447666
    sget-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "number_of_times_carousel_nux_optout_action_performed"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->f:LX/0Tn;

    .line 447667
    sget-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "last_time_carousel_nux_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->g:LX/0Tn;

    .line 447668
    sget-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "snap_max_velocity"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->h:LX/0Tn;

    .line 447669
    sget-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "snap_high_velocity"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->i:LX/0Tn;

    .line 447670
    sget-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "snap_high_range"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->j:LX/0Tn;

    .line 447671
    sget-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "snap_low_velocity"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->k:LX/0Tn;

    .line 447672
    sget-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "snap_low_range"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->l:LX/0Tn;

    .line 447673
    sget-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "frame_rate_logging"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->m:LX/0Tn;

    .line 447674
    sget-object v0, LX/2fv;->o:LX/0Tn;

    const-string v1, "frame_rate_logging_only_drops"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fv;->n:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 447675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
