.class public LX/33l;
.super LX/2h2;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h2;",
        "LX/0e6",
        "<",
        "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;",
        "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0lC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 494151
    const-class v0, LX/33l;

    sput-object v0, LX/33l;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 494148
    invoke-direct {p0}, LX/2h2;-><init>()V

    .line 494149
    iput-object p1, p0, LX/33l;->b:LX/0lC;

    .line 494150
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 494142
    check-cast p1, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;

    .line 494143
    invoke-static {p1}, LX/2h2;->a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;)Ljava/util/List;

    move-result-object v4

    .line 494144
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "screen_scale"

    .line 494145
    iget-object v2, p1, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 494146
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494147
    new-instance v0, LX/14N;

    const-string v1, "zeroGetOptinContent"

    const-string v2, "GET"

    const-string v3, "method/mobile.zeroGetOptinContent"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 494136
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 494137
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 494138
    iget-object v1, p0, LX/33l;->b:LX/0lC;

    iget-object v2, p0, LX/33l;->b:LX/0lC;

    invoke-virtual {v0, v2}, LX/0lF;->a(LX/0lD;)LX/15w;

    move-result-object v0

    iget-object v2, p0, LX/33l;->b:LX/0lC;

    .line 494139
    iget-object v3, v2, LX/0lC;->_typeFactory:LX/0li;

    move-object v2, v3

    .line 494140
    const-class v3, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v2, v3}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(LX/15w;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    .line 494141
    return-object v0
.end method
