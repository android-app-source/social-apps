.class public LX/2G9;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Ljava/util/UUID;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Ljava/util/UUID;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 387843
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/UUID;
    .locals 3

    .prologue
    .line 387844
    sget-object v0, LX/2G9;->a:Ljava/util/UUID;

    if-nez v0, :cond_1

    .line 387845
    const-class v1, LX/2G9;

    monitor-enter v1

    .line 387846
    :try_start_0
    sget-object v0, LX/2G9;->a:Ljava/util/UUID;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387847
    if-eqz v2, :cond_0

    .line 387848
    :try_start_1
    invoke-static {}, LX/2GA;->a()Ljava/util/UUID;

    move-result-object v0

    sput-object v0, LX/2G9;->a:Ljava/util/UUID;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387849
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387850
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387851
    :cond_1
    sget-object v0, LX/2G9;->a:Ljava/util/UUID;

    return-object v0

    .line 387852
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387853
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 387854
    invoke-static {}, LX/2GA;->a()Ljava/util/UUID;

    move-result-object v0

    return-object v0
.end method
