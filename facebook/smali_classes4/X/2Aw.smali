.class public LX/2Aw;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final i:[LX/2Ax;


# instance fields
.field public final a:LX/0lS;

.field public b:LX/0m2;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2Ax;",
            ">;"
        }
    .end annotation
.end field

.field public d:[LX/2Ax;

.field public e:LX/4rL;

.field public f:Ljava/lang/Object;

.field public g:LX/2An;

.field public h:LX/4rR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 378440
    const/4 v0, 0x0

    new-array v0, v0, [LX/2Ax;

    sput-object v0, LX/2Aw;->i:[LX/2Ax;

    return-void
.end method

.method public constructor <init>(LX/0lS;)V
    .locals 0

    .prologue
    .line 378421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 378422
    iput-object p1, p0, LX/2Aw;->a:LX/0lS;

    .line 378423
    return-void
.end method


# virtual methods
.method public final a(LX/2An;)V
    .locals 3

    .prologue
    .line 378436
    iget-object v0, p0, LX/2Aw;->g:LX/2An;

    if-eqz v0, :cond_0

    .line 378437
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Multiple type ids specified with "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/2Aw;->g:LX/2An;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 378438
    :cond_0
    iput-object p1, p0, LX/2Aw;->g:LX/2An;

    .line 378439
    return-void
.end method

.method public final g()Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 378427
    iget-object v0, p0, LX/2Aw;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Aw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 378428
    :cond_0
    iget-object v0, p0, LX/2Aw;->e:LX/4rL;

    if-nez v0, :cond_1

    .line 378429
    const/4 v0, 0x0

    .line 378430
    :goto_0
    return-object v0

    .line 378431
    :cond_1
    sget-object v0, LX/2Aw;->i:[LX/2Ax;

    .line 378432
    :goto_1
    new-instance v1, Lcom/fasterxml/jackson/databind/ser/BeanSerializer;

    iget-object v2, p0, LX/2Aw;->a:LX/0lS;

    .line 378433
    iget-object v3, v2, LX/0lS;->a:LX/0lJ;

    move-object v2, v3

    .line 378434
    iget-object v3, p0, LX/2Aw;->d:[LX/2Ax;

    invoke-direct {v1, v2, p0, v0, v3}, Lcom/fasterxml/jackson/databind/ser/BeanSerializer;-><init>(LX/0lJ;LX/2Aw;[LX/2Ax;[LX/2Ax;)V

    move-object v0, v1

    goto :goto_0

    .line 378435
    :cond_2
    iget-object v0, p0, LX/2Aw;->c:Ljava/util/List;

    iget-object v1, p0, LX/2Aw;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [LX/2Ax;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Ax;

    goto :goto_1
.end method

.method public final h()Lcom/fasterxml/jackson/databind/ser/BeanSerializer;
    .locals 1

    .prologue
    .line 378424
    iget-object v0, p0, LX/2Aw;->a:LX/0lS;

    .line 378425
    iget-object p0, v0, LX/0lS;->a:LX/0lJ;

    move-object v0, p0

    .line 378426
    invoke-static {v0}, Lcom/fasterxml/jackson/databind/ser/BeanSerializer;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/ser/BeanSerializer;

    move-result-object v0

    return-object v0
.end method
