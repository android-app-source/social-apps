.class public LX/2UF;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Landroid/net/Uri;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static final e:Landroid/net/Uri;


# instance fields
.field private final f:Landroid/content/ContentResolver;

.field private final g:LX/1Ml;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 415724
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/2UG;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/%d/addr"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2UF;->a:Ljava/lang/String;

    .line 415725
    sget-object v0, LX/2UI;->a:Landroid/net/Uri;

    sput-object v0, LX/2UF;->b:Landroid/net/Uri;

    .line 415726
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/2UG;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/part"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2UF;->c:Ljava/lang/String;

    .line 415727
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/2UF;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/%d"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2UF;->d:Ljava/lang/String;

    .line 415728
    sget-object v0, LX/2UJ;->a:Landroid/net/Uri;

    sput-object v0, LX/2UF;->e:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/1Ml;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415721
    iput-object p1, p0, LX/2UF;->f:Landroid/content/ContentResolver;

    .line 415722
    iput-object p2, p0, LX/2UF;->g:LX/1Ml;

    .line 415723
    return-void
.end method

.method private static a(LX/2UF;J)Ljava/lang/String;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 415710
    :try_start_0
    iget-object v0, p0, LX/2UF;->f:Landroid/content/ContentResolver;

    sget-object v1, LX/2UF;->a:Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "address"

    aput-object v4, v2, v3

    const-string v3, "msg_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 415711
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 415712
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 415713
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 415714
    :cond_0
    if-eqz v1, :cond_1

    .line 415715
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 415716
    :cond_1
    return-object v6

    .line 415717
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_2

    .line 415718
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 415719
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private static a(LX/2UF;Ljava/lang/Long;)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 415690
    :try_start_0
    iget-object v1, p0, LX/2UF;->f:Landroid/content/ContentResolver;

    sget-object v2, LX/2UF;->d:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 415691
    if-nez v1, :cond_1

    .line 415692
    if-eqz v1, :cond_0

    .line 415693
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 415694
    :cond_0
    :goto_0
    return-object v0

    .line 415695
    :cond_1
    :try_start_2
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    const-string v4, "UTF-8"

    invoke-direct {v3, v1, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 415696
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 415697
    :goto_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 415698
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 415699
    :catch_0
    :goto_2
    if-eqz v1, :cond_0

    .line 415700
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 415701
    :catch_1
    goto :goto_0

    .line 415702
    :cond_2
    :try_start_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    .line 415703
    if-eqz v1, :cond_0

    .line 415704
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 415705
    :catch_2
    goto :goto_0

    .line 415706
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_3
    if-eqz v1, :cond_3

    .line 415707
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 415708
    :cond_3
    :goto_4
    throw v0

    :catch_3
    goto :goto_0

    :catch_4
    goto :goto_4

    .line 415709
    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_5
    move-object v1, v0

    goto :goto_2
.end method

.method public static a(LX/2UF;Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/EiD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415677
    invoke-static {p0}, LX/2UF;->b(LX/2UF;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 415678
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 415679
    :cond_0
    :goto_0
    return-object v0

    .line 415680
    :cond_1
    const/4 v6, 0x0

    .line 415681
    :try_start_0
    iget-object v0, p0, LX/2UF;->f:Landroid/content/ContentResolver;

    sget-object v1, LX/2UF;->e:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "date"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "address"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "body"

    aput-object v4, v2, v3

    const-string v3, "_id > ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v8, p1, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;->smsId:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const-string v5, "_id DESC LIMIT 1000"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 415682
    :try_start_1
    invoke-static {p1, v1}, LX/2UF;->a(Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;Landroid/database/Cursor;)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 415683
    if-eqz v1, :cond_2

    .line 415684
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 415685
    :cond_2
    if-nez v0, :cond_0

    .line 415686
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 415687
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_3

    .line 415688
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 415689
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private static a(Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;Landroid/database/Cursor;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/EiD;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 415729
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 415730
    :cond_0
    const/4 v0, 0x0

    .line 415731
    :cond_1
    return-object v0

    .line 415732
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 415733
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 415734
    const-string v1, "date"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 415735
    const-string v1, "address"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 415736
    const-string v1, "body"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 415737
    :cond_3
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 415738
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 415739
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 415740
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 415741
    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 415742
    iget-wide v12, p0, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;->smsId:J

    cmp-long v1, v2, v12

    if-lez v1, :cond_4

    .line 415743
    iput-wide v2, p0, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;->smsId:J

    .line 415744
    :cond_4
    invoke-static {v7}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 415745
    new-instance v1, LX/EiD;

    invoke-direct/range {v1 .. v7}, LX/EiD;-><init>(JJLjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static b(LX/2UF;J)Ljava/lang/String;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 415657
    :try_start_0
    iget-object v0, p0, LX/2UF;->f:Landroid/content/ContentResolver;

    sget-object v1, LX/2UF;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "ct"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "_data"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "text"

    aput-object v4, v2, v3

    const-string v3, "mid = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 415658
    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 415659
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 415660
    const-string v0, "ct"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 415661
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 415662
    const-string v0, "text"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object v0, v6

    .line 415663
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 415664
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 415665
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 415666
    const-string v9, "text/plain"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 415667
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 415668
    if-eqz v0, :cond_1

    .line 415669
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p0, v0}, LX/2UF;->a(LX/2UF;Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 415670
    :cond_1
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v6, v0

    .line 415671
    :cond_3
    if-eqz v1, :cond_4

    .line 415672
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 415673
    :cond_4
    return-object v6

    .line 415674
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_5

    .line 415675
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 415676
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private static b(LX/2UF;Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;Landroid/database/Cursor;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/EiD;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 415642
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 415643
    :cond_0
    const/4 v0, 0x0

    .line 415644
    :cond_1
    return-object v0

    .line 415645
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 415646
    const-string v1, "_id"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 415647
    const-string v1, "date"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 415648
    :cond_3
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 415649
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 415650
    invoke-interface {p2, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 415651
    iget-wide v6, p1, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;->mmsId:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_4

    .line 415652
    iput-wide v2, p1, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;->mmsId:J

    .line 415653
    :cond_4
    invoke-static {p0, v2, v3}, LX/2UF;->a(LX/2UF;J)Ljava/lang/String;

    move-result-object v6

    .line 415654
    invoke-static {p0, v2, v3}, LX/2UF;->b(LX/2UF;J)Ljava/lang/String;

    move-result-object v7

    .line 415655
    invoke-static {v7}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 415656
    new-instance v1, LX/EiD;

    invoke-direct/range {v1 .. v7}, LX/EiD;-><init>(JJLjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static b(LX/2UF;)Z
    .locals 2

    .prologue
    .line 415641
    iget-object v0, p0, LX/2UF;->g:LX/1Ml;

    const-string v1, "android.permission.READ_SMS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static c(LX/2UF;Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/EiD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415628
    invoke-static {p0}, LX/2UF;->b(LX/2UF;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 415629
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 415630
    :cond_0
    :goto_0
    return-object v0

    .line 415631
    :cond_1
    const/4 v6, 0x0

    .line 415632
    :try_start_0
    iget-object v0, p0, LX/2UF;->f:Landroid/content/ContentResolver;

    sget-object v1, LX/2UF;->b:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "date"

    aput-object v4, v2, v3

    const-string v3, "_id > ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v8, p1, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;->mmsId:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const-string v5, "_id DESC LIMIT 1000"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 415633
    :try_start_1
    invoke-static {p0, p1, v1}, LX/2UF;->b(LX/2UF;Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;Landroid/database/Cursor;)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 415634
    if-eqz v1, :cond_2

    .line 415635
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 415636
    :cond_2
    if-nez v0, :cond_0

    .line 415637
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 415638
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_3

    .line 415639
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 415640
    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;
    .locals 12

    .prologue
    const-wide/16 v8, -0x1

    const/4 v6, 0x0

    .line 415607
    invoke-static {p0}, LX/2UF;->b(LX/2UF;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 415608
    new-instance v0, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;

    invoke-direct {v0, v8, v9, v8, v9}, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;-><init>(JJ)V

    .line 415609
    :goto_0
    return-object v0

    .line 415610
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/2UF;->f:Landroid/content/ContentResolver;

    sget-object v1, LX/2UF;->e:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 415611
    if-eqz v6, :cond_6

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_6

    .line 415612
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 415613
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-wide v0

    move-wide v10, v0

    .line 415614
    :goto_1
    if-eqz v6, :cond_1

    .line 415615
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 415616
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/2UF;->f:Landroid/content/ContentResolver;

    sget-object v1, LX/2UF;->b:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 415617
    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 415618
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 415619
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-wide v0

    move-wide v2, v0

    .line 415620
    :goto_2
    if-eqz v6, :cond_2

    .line 415621
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 415622
    :cond_2
    new-instance v0, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;

    invoke-direct {v0, v10, v11, v2, v3}, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;-><init>(JJ)V

    goto :goto_0

    .line 415623
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_3

    .line 415624
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 415625
    :catchall_1
    move-exception v0

    if-eqz v6, :cond_4

    .line 415626
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 415627
    :catchall_2
    move-exception v0

    move-object v1, v6

    goto :goto_3

    :cond_5
    move-wide v2, v8

    goto :goto_2

    :cond_6
    move-wide v10, v8

    goto :goto_1
.end method
