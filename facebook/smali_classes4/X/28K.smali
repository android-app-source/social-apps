.class public final enum LX/28K;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/28K;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/28K;

.field public static final enum APP_REGISTRATION_LOGIN_NONCE:LX/28K;

.field public static final enum DBL_PASSWORD:LX/28K;

.field public static final enum PASSWORD:LX/28K;

.field public static final enum TRANSIENT_TOKEN:LX/28K;

.field public static final enum UNSET:LX/28K;

.field public static final enum WORK_ACCOUNT_PASSWORD:LX/28K;

.field public static final enum WORK_REGISTRATION_AUTOLOGIN_NONCE:LX/28K;

.field public static final enum WORK_SSO_NONCE:LX/28K;

.field public static final enum WORK_USERNAME_WITH_PERSONAL_PASSWORD:LX/28K;


# instance fields
.field private final mServerValue:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 373881
    new-instance v0, LX/28K;

    const-string v1, "APP_REGISTRATION_LOGIN_NONCE"

    const-string v2, "app_registration_login_nonce"

    invoke-direct {v0, v1, v4, v2}, LX/28K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/28K;->APP_REGISTRATION_LOGIN_NONCE:LX/28K;

    .line 373882
    new-instance v0, LX/28K;

    const-string v1, "PASSWORD"

    const-string v2, "password"

    invoke-direct {v0, v1, v5, v2}, LX/28K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/28K;->PASSWORD:LX/28K;

    .line 373883
    new-instance v0, LX/28K;

    const-string v1, "TRANSIENT_TOKEN"

    const-string v2, "transient_token"

    invoke-direct {v0, v1, v6, v2}, LX/28K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/28K;->TRANSIENT_TOKEN:LX/28K;

    .line 373884
    new-instance v0, LX/28K;

    const-string v1, "WORK_ACCOUNT_PASSWORD"

    const-string v2, "work_account_password"

    invoke-direct {v0, v1, v7, v2}, LX/28K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/28K;->WORK_ACCOUNT_PASSWORD:LX/28K;

    .line 373885
    new-instance v0, LX/28K;

    const-string v1, "WORK_SSO_NONCE"

    const-string v2, "work_sso_nonce"

    invoke-direct {v0, v1, v8, v2}, LX/28K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/28K;->WORK_SSO_NONCE:LX/28K;

    .line 373886
    new-instance v0, LX/28K;

    const-string v1, "WORK_USERNAME_WITH_PERSONAL_PASSWORD"

    const/4 v2, 0x5

    const-string v3, "personal_account_password_with_work_username"

    invoke-direct {v0, v1, v2, v3}, LX/28K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/28K;->WORK_USERNAME_WITH_PERSONAL_PASSWORD:LX/28K;

    .line 373887
    new-instance v0, LX/28K;

    const-string v1, "WORK_REGISTRATION_AUTOLOGIN_NONCE"

    const/4 v2, 0x6

    const-string v3, "work_registration_autologin_nonce"

    invoke-direct {v0, v1, v2, v3}, LX/28K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/28K;->WORK_REGISTRATION_AUTOLOGIN_NONCE:LX/28K;

    .line 373888
    new-instance v0, LX/28K;

    const-string v1, "DBL_PASSWORD"

    const/4 v2, 0x7

    const-string v3, "device_based_login_password"

    invoke-direct {v0, v1, v2, v3}, LX/28K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/28K;->DBL_PASSWORD:LX/28K;

    .line 373889
    new-instance v0, LX/28K;

    const-string v1, "UNSET"

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/28K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/28K;->UNSET:LX/28K;

    .line 373890
    const/16 v0, 0x9

    new-array v0, v0, [LX/28K;

    sget-object v1, LX/28K;->APP_REGISTRATION_LOGIN_NONCE:LX/28K;

    aput-object v1, v0, v4

    sget-object v1, LX/28K;->PASSWORD:LX/28K;

    aput-object v1, v0, v5

    sget-object v1, LX/28K;->TRANSIENT_TOKEN:LX/28K;

    aput-object v1, v0, v6

    sget-object v1, LX/28K;->WORK_ACCOUNT_PASSWORD:LX/28K;

    aput-object v1, v0, v7

    sget-object v1, LX/28K;->WORK_SSO_NONCE:LX/28K;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/28K;->WORK_USERNAME_WITH_PERSONAL_PASSWORD:LX/28K;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/28K;->WORK_REGISTRATION_AUTOLOGIN_NONCE:LX/28K;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/28K;->DBL_PASSWORD:LX/28K;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/28K;->UNSET:LX/28K;

    aput-object v2, v0, v1

    sput-object v0, LX/28K;->$VALUES:[LX/28K;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 373891
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 373892
    iput-object p3, p0, LX/28K;->mServerValue:Ljava/lang/String;

    .line 373893
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/28K;
    .locals 1

    .prologue
    .line 373894
    const-class v0, LX/28K;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/28K;

    return-object v0
.end method

.method public static values()[LX/28K;
    .locals 1

    .prologue
    .line 373895
    sget-object v0, LX/28K;->$VALUES:[LX/28K;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/28K;

    return-object v0
.end method


# virtual methods
.method public final getServerValue()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 373896
    iget-object v0, p0, LX/28K;->mServerValue:Ljava/lang/String;

    return-object v0
.end method
