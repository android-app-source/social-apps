.class public LX/3Go;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field public final b:LX/0aG;

.field private final c:Ljava/util/Random;

.field public final d:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0SG;LX/0aG;LX/0Zb;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 541844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 541845
    iput-object p1, p0, LX/3Go;->a:LX/0SG;

    .line 541846
    iput-object p2, p0, LX/3Go;->b:LX/0aG;

    .line 541847
    iput-object p3, p0, LX/3Go;->d:LX/0Zb;

    .line 541848
    new-instance v0, Ljava/util/Random;

    invoke-interface {p1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, LX/3Go;->c:Ljava/util/Random;

    .line 541849
    return-void
.end method

.method public static b(LX/0QB;)LX/3Go;
    .locals 4

    .prologue
    .line 541842
    new-instance v3, LX/3Go;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-direct {v3, v0, v1, v2}, LX/3Go;-><init>(LX/0SG;LX/0aG;LX/0Zb;)V

    .line 541843
    return-object v3
.end method
