.class public LX/3Cn;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/1eF;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityAtRange;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:LX/3Cq;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 530852
    new-instance v0, LX/3Co;

    invoke-direct {v0}, LX/3Co;-><init>()V

    sput-object v0, LX/3Cn;->a:Ljava/util/Comparator;

    .line 530853
    new-instance v0, LX/3Cp;

    invoke-direct {v0}, LX/3Cp;-><init>()V

    sput-object v0, LX/3Cn;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/3Cq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 530849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530850
    iput-object p1, p0, LX/3Cn;->c:LX/3Cq;

    .line 530851
    return-void
.end method

.method private a(LX/1yN;Landroid/text/SpannableStringBuilder;)V
    .locals 2

    .prologue
    .line 530842
    invoke-virtual {p1}, LX/1yN;->c()I

    move-result v0

    .line 530843
    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-interface {p2, v0}, Landroid/text/Spannable;->charAt(I)C

    move-result v1

    const/16 p1, 0x27

    if-ne v1, p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 530844
    if-eqz v1, :cond_0

    .line 530845
    add-int/lit8 v0, v0, 0x2

    .line 530846
    :cond_0
    const/4 p0, 0x0

    move-object v1, p0

    .line 530847
    invoke-static {p2, v0, v1}, LX/47q;->a(Landroid/text/SpannableStringBuilder;ILandroid/graphics/drawable/Drawable;)V

    .line 530848
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/3Cn;
    .locals 2

    .prologue
    .line 530840
    new-instance v1, LX/3Cn;

    invoke-static {p0}, LX/3Cq;->a(LX/0QB;)LX/3Cq;

    move-result-object v0

    check-cast v0, LX/3Cq;

    invoke-direct {v1, v0}, LX/3Cn;-><init>(LX/3Cq;)V

    .line 530841
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Landroid/text/SpannableStringBuilder;)V
    .locals 7

    .prologue
    .line 530809
    if-nez p1, :cond_1

    .line 530810
    :cond_0
    return-void

    .line 530811
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 530812
    sget-object v0, LX/3Cn;->b:Ljava/util/Comparator;

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 530813
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 530814
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v4

    if-nez v4, :cond_4

    const/4 v4, 0x0

    :goto_1
    move-object v4, v4

    .line 530815
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v5

    .line 530816
    const v6, 0x285feb

    if-ne v4, v6, :cond_5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEntity;->q()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEntity;->p()Z

    move-result v6

    if-nez v6, :cond_5

    const/4 v6, 0x1

    :goto_2
    move v4, v6

    .line 530817
    if-nez v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v4

    .line 530818
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEntity;->o()Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x1

    :goto_3
    move v4, v5

    .line 530819
    if-eqz v4, :cond_3

    .line 530820
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/1yL;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v0

    invoke-direct {v5, v6, v0}, LX/1yL;-><init>(II)V

    invoke-static {v4, v5}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v0

    .line 530821
    invoke-direct {p0, v0, p2}, LX/3Cn;->a(LX/1yN;Landroid/text/SpannableStringBuilder;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 530822
    :cond_3
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 530823
    :catch_0
    move-exception v0

    .line 530824
    const-string v4, "MultiCompanyBadgeApplicator"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    goto :goto_1

    :cond_5
    const/4 v6, 0x0

    goto :goto_2

    :cond_6
    const/4 v5, 0x0

    goto :goto_3
.end method

.method public final a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Landroid/text/SpannableStringBuilder;)V
    .locals 5

    .prologue
    .line 530825
    if-nez p1, :cond_1

    .line 530826
    :cond_0
    return-void

    .line 530827
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 530828
    sget-object v1, LX/3Cn;->a:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 530829
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1eF;

    .line 530830
    invoke-interface {v0}, LX/1eF;->d()LX/1y9;

    move-result-object v2

    if-nez v2, :cond_4

    const/4 v2, 0x0

    :goto_1
    move-object v2, v2

    .line 530831
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    invoke-interface {v0}, LX/1eF;->d()LX/1y9;

    move-result-object v3

    .line 530832
    const v4, 0x285feb

    if-ne v2, v4, :cond_5

    invoke-interface {v3}, LX/1y9;->l()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, LX/1y9;->k()Z

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x1

    :goto_2
    move v2, v4

    .line 530833
    if-nez v2, :cond_3

    invoke-interface {v0}, LX/1eF;->d()LX/1y9;

    move-result-object v2

    .line 530834
    if-eqz v2, :cond_6

    invoke-interface {v2}, LX/1y9;->r()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    :goto_3
    move v2, v3

    .line 530835
    if-eqz v2, :cond_2

    .line 530836
    :cond_3
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/1yL;

    invoke-interface {v0}, LX/1eF;->c()I

    move-result v4

    invoke-interface {v0}, LX/1eF;->b()I

    move-result v0

    invoke-direct {v3, v4, v0}, LX/1yL;-><init>(II)V

    invoke-static {v2, v3}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v0

    .line 530837
    invoke-direct {p0, v0, p2}, LX/3Cn;->a(LX/1yN;Landroid/text/SpannableStringBuilder;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 530838
    :catch_0
    move-exception v0

    .line 530839
    const-string v2, "MultiCompanyBadgeApplicator"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_4
    invoke-interface {v0}, LX/1eF;->d()LX/1y9;

    move-result-object v2

    invoke-interface {v2}, LX/1y9;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    goto :goto_2

    :cond_6
    const/4 v3, 0x0

    goto :goto_3
.end method
