.class public LX/2Qw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2Qw;


# instance fields
.field public final a:LX/2Qx;

.field public final b:Landroid/content/Context;

.field public final c:LX/2N8;


# direct methods
.method public constructor <init>(LX/2Qx;Landroid/content/Context;LX/2N8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409123
    iput-object p1, p0, LX/2Qw;->a:LX/2Qx;

    .line 409124
    iput-object p2, p0, LX/2Qw;->b:Landroid/content/Context;

    .line 409125
    iput-object p3, p0, LX/2Qw;->c:LX/2N8;

    .line 409126
    return-void
.end method

.method public static a(LX/0QB;)LX/2Qw;
    .locals 6

    .prologue
    .line 409132
    sget-object v0, LX/2Qw;->d:LX/2Qw;

    if-nez v0, :cond_1

    .line 409133
    const-class v1, LX/2Qw;

    monitor-enter v1

    .line 409134
    :try_start_0
    sget-object v0, LX/2Qw;->d:LX/2Qw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 409135
    if-eqz v2, :cond_0

    .line 409136
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 409137
    new-instance p0, LX/2Qw;

    invoke-static {v0}, LX/2Qx;->a(LX/0QB;)LX/2Qx;

    move-result-object v3

    check-cast v3, LX/2Qx;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v5

    check-cast v5, LX/2N8;

    invoke-direct {p0, v3, v4, v5}, LX/2Qw;-><init>(LX/2Qx;Landroid/content/Context;LX/2N8;)V

    .line 409138
    move-object v0, p0

    .line 409139
    sput-object v0, LX/2Qw;->d:LX/2Qw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 409140
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 409141
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 409142
    :cond_1
    sget-object v0, LX/2Qw;->d:LX/2Qw;

    return-object v0

    .line 409143
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 409144
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/8PL;)LX/8PF;
    .locals 6

    .prologue
    .line 409145
    new-instance v0, LX/8PF;

    iget-object v1, p0, LX/2Qw;->a:LX/2Qx;

    iget-object v2, p0, LX/2Qw;->b:Landroid/content/Context;

    .line 409146
    iget-object v3, p1, LX/8PL;->e:LX/0m9;

    move-object v3, v3

    .line 409147
    iget-object v4, p1, LX/8PL;->b:Ljava/lang/String;

    move-object v4, v4

    .line 409148
    iget-object v5, p1, LX/8PL;->c:Ljava/lang/String;

    move-object v5, v5

    .line 409149
    invoke-direct/range {v0 .. v5}, LX/8PF;-><init>(LX/2Qx;Landroid/content/Context;LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;)LX/8PF;
    .locals 6

    .prologue
    .line 409127
    new-instance v0, LX/8PF;

    iget-object v1, p0, LX/2Qw;->a:LX/2Qx;

    iget-object v2, p0, LX/2Qw;->b:Landroid/content/Context;

    iget-object v3, p0, LX/2Qw;->c:LX/2N8;

    iget-object v4, p1, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    check-cast v3, LX/0m9;

    iget-object v4, p1, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;->b:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, LX/8PF;-><init>(LX/2Qx;Landroid/content/Context;LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 409128
    :try_start_0
    invoke-virtual {v0}, LX/8PF;->h()V
    :try_end_0
    .catch LX/8PD; {:try_start_0 .. :try_end_0} :catch_0

    .line 409129
    return-object v0

    .line 409130
    :catch_0
    move-exception v0

    .line 409131
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
