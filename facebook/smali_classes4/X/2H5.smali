.class public final LX/2H5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;


# direct methods
.method public constructor <init>(Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;)V
    .locals 0

    .prologue
    .line 389866
    iput-object p1, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 389860
    iget-object v0, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    iget-object v0, v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->i:LX/03V;

    const-string v1, "foreground_location_framework"

    const-string v2, "Request future failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 389861
    iget-object v0, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    iget-object v0, v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->g:LX/1ra;

    invoke-virtual {v0, v3}, LX/1ra;->a(LX/2TS;)V

    .line 389862
    iget-object v0, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    .line 389863
    iput-object v3, v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->o:LX/1Mv;

    .line 389864
    iget-object v0, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    iget-object v0, v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->h:LX/1sN;

    invoke-virtual {v0}, LX/1sN;->c()V

    .line 389865
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 389823
    check-cast p1, Ljava/util/List;

    const/4 v2, 0x0

    .line 389824
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 389825
    instance-of v1, v0, Lcom/facebook/location/ImmutableLocation;

    if-eqz v1, :cond_0

    .line 389826
    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    move-object v1, v2

    move-object v3, v0

    .line 389827
    :goto_0
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 389828
    instance-of v4, v0, Ljava/util/List;

    if-eqz v4, :cond_1

    .line 389829
    check-cast v0, Ljava/util/List;

    move-object v4, v0

    move-object v0, v2

    .line 389830
    :goto_1
    iget-object v5, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    iget-object v5, v5, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->e:LX/1rl;

    .line 389831
    sget-char v8, LX/1rm;->i:C

    sget-wide v10, LX/0X5;->eV:J

    sget-object v9, LX/1sR;->MEDIUM_POWER:LX/1sR;

    invoke-static {v5, v8, v10, v11, v9}, LX/1rl;->a(LX/1rl;CJLX/1sR;)LX/1sR;

    move-result-object v8

    move-object v5, v8

    .line 389832
    invoke-static {v5}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->b(LX/1sR;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 389833
    :try_start_0
    iget-object v5, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    iget-object v5, v5, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->b:Landroid/content/Context;

    invoke-static {v5}, LX/2TE;->a(Landroid/content/Context;)Ljava/util/List;
    :try_end_0
    .catch LX/JcK; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 389834
    :goto_2
    new-instance v6, LX/2TS;

    new-instance v7, LX/2TT;

    invoke-direct {v7}, LX/2TT;-><init>()V

    .line 389835
    iput-object v3, v7, LX/2TT;->a:Lcom/facebook/location/ImmutableLocation;

    .line 389836
    move-object v3, v7

    .line 389837
    iget-object v7, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    iget-object v7, v7, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->k:LX/0Uo;

    invoke-virtual {v7}, LX/0Uo;->l()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 389838
    iput-object v7, v3, LX/2TT;->b:Ljava/lang/Boolean;

    .line 389839
    move-object v3, v3

    .line 389840
    iput-object v4, v3, LX/2TT;->d:Ljava/util/List;

    .line 389841
    move-object v4, v3

    .line 389842
    iget-object v3, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    iget-object v3, v3, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->e:LX/1rl;

    .line 389843
    sget-char v8, LX/1rm;->h:C

    sget-wide v10, LX/0X5;->eU:J

    sget-object v9, LX/1sR;->MEDIUM_POWER:LX/1sR;

    invoke-static {v3, v8, v10, v11, v9}, LX/1rl;->a(LX/1rl;CJLX/1sR;)LX/1sR;

    move-result-object v8

    move-object v3, v8

    .line 389844
    invoke-static {v3}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->b(LX/1sR;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    iget-object v3, v3, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->b:Landroid/content/Context;

    iget-object v7, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    iget-object v7, v7, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->l:LX/0SG;

    invoke-static {v3, v7}, LX/2TU;->a(Landroid/content/Context;LX/0SG;)Lcom/facebook/wifiscan/WifiScanResult;

    move-result-object v3

    .line 389845
    :goto_3
    iput-object v3, v4, LX/2TT;->c:Lcom/facebook/wifiscan/WifiScanResult;

    .line 389846
    move-object v3, v4

    .line 389847
    iget-object v4, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    invoke-static {v4}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->c$redex0(Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;)Lcom/facebook/location/GeneralCellInfo;

    move-result-object v4

    .line 389848
    iput-object v4, v3, LX/2TT;->e:Lcom/facebook/location/GeneralCellInfo;

    .line 389849
    move-object v3, v3

    .line 389850
    iput-object v5, v3, LX/2TT;->f:Ljava/util/List;

    .line 389851
    move-object v3, v3

    .line 389852
    invoke-virtual {v3}, LX/2TT;->a()Lcom/facebook/location/LocationSignalDataPackage;

    move-result-object v3

    new-instance v4, LX/2Tl;

    invoke-direct {v4, v1, v0}, LX/2Tl;-><init>(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    invoke-direct {v6, v3, v4}, LX/2TS;-><init>(Lcom/facebook/location/LocationSignalDataPackage;LX/2Tl;)V

    .line 389853
    iget-object v0, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    invoke-static {v0, v6}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->a$redex0(Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;LX/2TS;)V

    .line 389854
    iget-object v0, p0, LX/2H5;->a:Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    .line 389855
    iput-object v2, v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->o:LX/1Mv;

    .line 389856
    return-void

    .line 389857
    :cond_0
    check-cast v0, Ljava/lang/Throwable;

    move-object v1, v0

    move-object v3, v2

    goto/16 :goto_0

    .line 389858
    :cond_1
    check-cast v0, Ljava/lang/Throwable;

    move-object v4, v2

    goto/16 :goto_1

    :catch_0
    :cond_2
    move-object v5, v2

    goto :goto_2

    :cond_3
    move-object v3, v2

    .line 389859
    goto :goto_3
.end method
