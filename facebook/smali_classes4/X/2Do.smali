.class public LX/2Do;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2Do;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/2Dp;

.field public final c:LX/03V;

.field private final d:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/2Dp;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384662
    iput-object p1, p0, LX/2Do;->a:LX/0tX;

    .line 384663
    iput-object p2, p0, LX/2Do;->d:LX/1Ck;

    .line 384664
    iput-object p3, p0, LX/2Do;->b:LX/2Dp;

    .line 384665
    iput-object p4, p0, LX/2Do;->c:LX/03V;

    .line 384666
    return-void
.end method

.method public static a(LX/0QB;)LX/2Do;
    .locals 7

    .prologue
    .line 384667
    sget-object v0, LX/2Do;->e:LX/2Do;

    if-nez v0, :cond_1

    .line 384668
    const-class v1, LX/2Do;

    monitor-enter v1

    .line 384669
    :try_start_0
    sget-object v0, LX/2Do;->e:LX/2Do;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 384670
    if-eqz v2, :cond_0

    .line 384671
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 384672
    new-instance p0, LX/2Do;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/2Dp;->a(LX/0QB;)LX/2Dp;

    move-result-object v5

    check-cast v5, LX/2Dp;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2Do;-><init>(LX/0tX;LX/1Ck;LX/2Dp;LX/03V;)V

    .line 384673
    move-object v0, p0

    .line 384674
    sput-object v0, LX/2Do;->e:LX/2Do;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384675
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 384676
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 384677
    :cond_1
    sget-object v0, LX/2Do;->e:LX/2Do;

    return-object v0

    .line 384678
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 384679
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 384680
    iget-object v0, p0, LX/2Do;->d:LX/1Ck;

    const-string v1, "CONFIG_UPDATE_TASK"

    new-instance v2, LX/4p0;

    invoke-direct {v2, p0}, LX/4p0;-><init>(LX/2Do;)V

    new-instance v3, LX/4p1;

    invoke-direct {v3, p0}, LX/4p1;-><init>(LX/2Do;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 384681
    return-void
.end method
