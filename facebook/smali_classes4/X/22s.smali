.class public final LX/22s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0r4;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/data/FeedDataLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/data/FeedDataLoader;)V
    .locals 0

    .prologue
    .line 361330
    iput-object p1, p0, LX/22s;->a:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 361331
    iget-object v0, p0, LX/22s;->a:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->ae:LX/0ad;

    sget-short v1, LX/0fe;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361332
    iget-object v0, p0, LX/22s;->a:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/0gf;->POLLING:LX/0gf;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/data/FeedDataLoader;->a(LX/0gf;)Z

    .line 361333
    :goto_0
    return-void

    .line 361334
    :cond_0
    iget-object v0, p0, LX/22s;->a:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-virtual {v0}, Lcom/facebook/feed/data/FeedDataLoader;->m()V

    goto :goto_0
.end method

.method public final b()J
    .locals 8

    .prologue
    const-wide/32 v4, 0x83d60

    .line 361335
    iget-object v0, p0, LX/22s;->a:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-virtual {v0}, LX/0gD;->J()Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/22s;->a:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-virtual {v0}, LX/0gD;->J()Lcom/facebook/common/perftest/PerfTestConfig;

    .line 361336
    sget-wide v6, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->e:J

    move-wide v0, v6

    .line 361337
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 361338
    iget-object v0, p0, LX/22s;->a:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-virtual {v0}, LX/0gD;->J()Lcom/facebook/common/perftest/PerfTestConfig;

    .line 361339
    sget-wide v6, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->e:J

    move-wide v0, v6

    .line 361340
    :goto_0
    return-wide v0

    .line 361341
    :cond_0
    iget-object v0, p0, LX/22s;->a:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->ae:LX/0ad;

    sget-short v1, LX/0fe;->ay:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 361342
    iget-object v0, p0, LX/22s;->a:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->ae:LX/0ad;

    sget-wide v2, LX/0fe;->aw:J

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    goto :goto_0

    .line 361343
    :cond_1
    iget-object v0, p0, LX/22s;->a:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->ae:LX/0ad;

    sget-wide v2, LX/0fe;->n:J

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final c()J
    .locals 6

    .prologue
    .line 361344
    iget-object v0, p0, LX/22s;->a:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->ae:LX/0ad;

    sget-wide v2, LX/0fe;->av:J

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()J
    .locals 6

    .prologue
    .line 361345
    iget-object v0, p0, LX/22s;->a:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->ae:LX/0ad;

    sget-wide v2, LX/0fe;->ax:J

    const-wide/32 v4, 0x83d60

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method
