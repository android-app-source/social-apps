.class public final synthetic LX/3JA;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 547493
    invoke-static {}, LX/3J8;->values()[LX/3J8;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/3JA;->a:[I

    :try_start_0
    sget-object v0, LX/3JA;->a:[I

    sget-object v1, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    invoke-virtual {v1}, LX/3J8;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v0, LX/3JA;->a:[I

    sget-object v1, LX/3J8;->REGULAR_360_VIDEO:LX/3J8;

    invoke-virtual {v1}, LX/3J8;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v0, LX/3JA;->a:[I

    sget-object v1, LX/3J8;->LIVE_VIDEO:LX/3J8;

    invoke-virtual {v1}, LX/3J8;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_2
    :try_start_3
    sget-object v0, LX/3JA;->a:[I

    sget-object v1, LX/3J8;->PREVIOUSLY_LIVE_VIDEO:LX/3J8;

    invoke-virtual {v1}, LX/3J8;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v0, LX/3JA;->a:[I

    sget-object v1, LX/3J8;->LIVE_360_VIDEO:LX/3J8;

    invoke-virtual {v1}, LX/3J8;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    sget-object v0, LX/3JA;->a:[I

    sget-object v1, LX/3J8;->PREVIOUSLY_LIVE_360_VIDEO:LX/3J8;

    invoke-virtual {v1}, LX/3J8;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v0, LX/3JA;->a:[I

    sget-object v1, LX/3J8;->ANIMATED_GIF_VIDEO:LX/3J8;

    invoke-virtual {v1}, LX/3J8;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v0, LX/3JA;->a:[I

    sget-object v1, LX/3J8;->PURPLE_RAIN_VIDEO:LX/3J8;

    invoke-virtual {v1}, LX/3J8;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v0, LX/3JA;->a:[I

    sget-object v1, LX/3J8;->UNKNOWN_VIDEO:LX/3J8;

    invoke-virtual {v1}, LX/3J8;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    return-void

    :catch_0
    goto :goto_8

    :catch_1
    goto :goto_7

    :catch_2
    goto :goto_6

    :catch_3
    goto :goto_5

    :catch_4
    goto :goto_4

    :catch_5
    goto :goto_3

    :catch_6
    goto :goto_2

    :catch_7
    goto :goto_1

    :catch_8
    goto :goto_0
.end method
