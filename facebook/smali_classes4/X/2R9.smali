.class public LX/2R9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Params;",
        "Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409509
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 409510
    check-cast p1, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Params;

    .line 409511
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 409512
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "access_token"

    iget-object v2, p1, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Params;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409513
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "fb_access_token"

    iget-object v2, p1, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Params;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409514
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409515
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "grant_type"

    const-string v2, "fb_extend_sso_token"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409516
    new-instance v0, LX/14N;

    const-string v1, "extend_access_token_method"

    const-string v2, "GET"

    const-string v3, "oauth/access_token"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 409517
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 409518
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 409519
    const-string v1, "access_token"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    .line 409520
    const-string v2, "expires_at"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->D()J

    move-result-wide v2

    .line 409521
    new-instance v0, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;-><init>(Ljava/lang/String;J)V

    return-object v0
.end method
