.class public LX/2UR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static volatile l:LX/2UR;


# instance fields
.field private final f:Landroid/content/ContentResolver;

.field private final g:LX/2US;

.field private final h:LX/2UT;

.field private final i:LX/2UU;

.field private final j:LX/1Ml;

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 416087
    const-class v0, LX/2UR;

    sput-object v0, LX/2UR;->a:Ljava/lang/Class;

    .line 416088
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    sput-object v0, LX/2UR;->b:[Ljava/lang/String;

    .line 416089
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "has_phone_number"

    aput-object v1, v0, v4

    sput-object v0, LX/2UR;->c:[Ljava/lang/String;

    .line 416090
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "contact_id"

    aput-object v1, v0, v4

    const-string v1, "deleted"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "data_version"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "is_primary"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is_super_primary"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "data_version"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "data3"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "data4"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "data5"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "data6"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "data7"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "data8"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "data9"

    aput-object v2, v0, v1

    sput-object v0, LX/2UR;->d:[Ljava/lang/String;

    .line 416091
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "version"

    aput-object v1, v0, v4

    sput-object v0, LX/2UR;->e:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/2US;LX/2UT;LX/2UU;LX/1Ml;LX/0Or;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/annotations/IsPhoneEmailSourcesUploadEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "LX/2US;",
            "LX/2UT;",
            "LX/2UU;",
            "LX/1Ml;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415992
    iput-object p1, p0, LX/2UR;->f:Landroid/content/ContentResolver;

    .line 415993
    iput-object p2, p0, LX/2UR;->g:LX/2US;

    .line 415994
    iput-object p3, p0, LX/2UR;->h:LX/2UT;

    .line 415995
    iput-object p4, p0, LX/2UR;->i:LX/2UU;

    .line 415996
    iput-object p5, p0, LX/2UR;->j:LX/1Ml;

    .line 415997
    iput-object p6, p0, LX/2UR;->k:LX/0Or;

    .line 415998
    return-void
.end method

.method public static a(LX/0QB;)LX/2UR;
    .locals 10

    .prologue
    .line 415999
    sget-object v0, LX/2UR;->l:LX/2UR;

    if-nez v0, :cond_1

    .line 416000
    const-class v1, LX/2UR;

    monitor-enter v1

    .line 416001
    :try_start_0
    sget-object v0, LX/2UR;->l:LX/2UR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 416002
    if-eqz v2, :cond_0

    .line 416003
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 416004
    new-instance v3, LX/2UR;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v4

    check-cast v4, Landroid/content/ContentResolver;

    const-class v5, LX/2US;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/2US;

    const-class v6, LX/2UT;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/2UT;

    const-class v7, LX/2UU;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/2UU;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v8

    check-cast v8, LX/1Ml;

    const/16 v9, 0x309

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/2UR;-><init>(Landroid/content/ContentResolver;LX/2US;LX/2UT;LX/2UU;LX/1Ml;LX/0Or;)V

    .line 416005
    move-object v0, v3

    .line 416006
    sput-object v0, LX/2UR;->l:LX/2UR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416007
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 416008
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 416009
    :cond_1
    sget-object v0, LX/2UR;->l:LX/2UR;

    return-object v0

    .line 416010
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 416011
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/2UR;Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 416012
    iget-object v0, p0, LX/2UR;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416013
    sget-object v0, LX/6Mz;->a:[Ljava/lang/String;

    sget-object v1, LX/6Mz;->b:[Ljava/lang/String;

    const-class v2, Ljava/lang/String;

    const/4 v7, 0x0

    .line 416014
    array-length v3, v0

    array-length v4, v1

    add-int/2addr v3, v4

    invoke-static {v2, v3}, LX/0P8;->a(Ljava/lang/Class;I)[Ljava/lang/Object;

    move-result-object v3

    .line 416015
    array-length v4, v0

    invoke-static {v0, v7, v3, v7, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 416016
    array-length v4, v0

    array-length v5, v1

    invoke-static {v1, v7, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 416017
    move-object v0, v3

    .line 416018
    check-cast v0, [Ljava/lang/String;

    move-object v2, v0

    .line 416019
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/2UR;->f:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "contact_id"

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 416020
    :goto_1
    return-object v0

    .line 416021
    :cond_0
    sget-object v2, LX/6Mz;->a:[Ljava/lang/String;

    goto :goto_0

    .line 416022
    :catch_0
    move-exception v0

    .line 416023
    sget-object v1, LX/2UR;->a:Ljava/lang/Class;

    const-string v2, "Got Exception in getCursorByEndpoint, closing open cursor."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v6

    goto :goto_1
.end method

.method private a(Ljava/util/List;)Landroid/database/Cursor;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 416024
    const-string v0, "contact_id"

    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v4

    .line 416025
    iget-object v0, p0, LX/2UR;->f:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, LX/2UR;->d:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;ILjava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 416026
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 416027
    iget-object v0, p0, LX/2UR;->f:Landroid/content/ContentResolver;

    sget-object v2, LX/2UR;->b:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 416028
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, p2, :cond_0

    .line 416029
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 416030
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 416031
    return-void
.end method

.method private b(Ljava/lang/String;ILjava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 416032
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 416033
    iget-object v0, p0, LX/2UR;->f:Landroid/content/ContentResolver;

    sget-object v2, LX/2UR;->b:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 416034
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, p2, :cond_0

    .line 416035
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 416036
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 416037
    return-void
.end method


# virtual methods
.method public final a()LX/6N9;
    .locals 7

    .prologue
    .line 416038
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 416039
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 416040
    const-string v0, "has_phone_number"

    const-string v2, "1"

    invoke-static {v0, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 416041
    iget-object v0, p0, LX/2UR;->f:Landroid/content/ContentResolver;

    sget-object v2, LX/2UR;->c:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 416042
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416043
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 416044
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 416045
    invoke-direct {p0, v6}, LX/2UR;->a(Ljava/util/List;)Landroid/database/Cursor;

    move-result-object v0

    .line 416046
    iget-object v1, p0, LX/2UR;->g:LX/2US;

    invoke-virtual {v1, v0}, LX/2US;->a(Landroid/database/Cursor;)LX/6N9;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;I)LX/6N9;
    .locals 2

    .prologue
    .line 416047
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 416048
    invoke-direct {p0, p1, p2, v0}, LX/2UR;->a(Ljava/lang/String;ILjava/util/List;)V

    .line 416049
    invoke-direct {p0, p1, p2, v0}, LX/2UR;->b(Ljava/lang/String;ILjava/util/List;)V

    .line 416050
    invoke-direct {p0, v0}, LX/2UR;->a(Ljava/util/List;)Landroid/database/Cursor;

    move-result-object v0

    .line 416051
    iget-object v1, p0, LX/2UR;->g:LX/2US;

    invoke-virtual {v1, v0}, LX/2US;->a(Landroid/database/Cursor;)LX/6N9;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/6Mz;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 416052
    iget-object v0, p0, LX/2UR;->j:LX/1Ml;

    const-string v2, "android.permission.READ_CONTACTS"

    invoke-virtual {v0, v2}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 416053
    :cond_0
    :goto_0
    return-object v1

    .line 416054
    :cond_1
    sget-object v0, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v0}, LX/2UR;->a(LX/2UR;Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v3

    .line 416055
    sget-object v0, Landroid/provider/ContactsContract$RawContactsEntity;->PROFILE_CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v0}, LX/2UR;->a(LX/2UR;Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v4

    .line 416056
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 416057
    if-eqz v3, :cond_2

    .line 416058
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416059
    :cond_2
    if-eqz v4, :cond_3

    .line 416060
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416061
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_4

    move-object v0, v1

    .line 416062
    :goto_1
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/2UR;->h:LX/2UT;

    .line 416063
    new-instance v6, LX/6N8;

    const-class v7, LX/2J0;

    invoke-interface {v1, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/2J0;

    invoke-static {v1}, LX/6N3;->a(LX/0QB;)LX/6N3;

    move-result-object v8

    check-cast v8, LX/6N3;

    .line 416064
    new-instance v11, LX/6Op;

    invoke-static {v1}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v9

    check-cast v9, Landroid/content/ContentResolver;

    invoke-static {v1}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v10

    check-cast v10, LX/1Ml;

    const/16 v12, 0x147c

    invoke-static {v1, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-direct {v11, v9, v10, v12}, LX/6Op;-><init>(Landroid/content/ContentResolver;LX/1Ml;LX/0Or;)V

    .line 416065
    move-object v9, v11

    .line 416066
    check-cast v9, LX/6Op;

    const/16 v10, 0x309

    invoke-static {v1, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    move-object v11, v0

    invoke-direct/range {v6 .. v11}, LX/6N8;-><init>(LX/2J0;LX/6N3;LX/6Op;LX/0Or;Landroid/database/Cursor;)V

    .line 416067
    move-object v1, v6

    .line 416068
    goto :goto_0

    .line 416069
    :cond_4
    :try_start_1
    new-instance v2, Landroid/database/MergeCursor;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Landroid/database/Cursor;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/database/Cursor;

    invoke-direct {v2, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object v0, v2

    goto :goto_1

    .line 416070
    :catch_0
    move-exception v0

    .line 416071
    sget-object v1, LX/2UR;->a:Ljava/lang/Class;

    const-string v2, "Got Exception in getFB4AContactsUploadIterator, closing open cursors."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v5}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 416072
    if-eqz v3, :cond_5

    .line 416073
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 416074
    :cond_5
    if-eqz v4, :cond_6

    .line 416075
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 416076
    :cond_6
    throw v0
.end method

.method public final c()LX/6Mz;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 416077
    iget-object v0, p0, LX/2UR;->j:LX/1Ml;

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 416078
    :cond_0
    :goto_0
    return-object v3

    .line 416079
    :cond_1
    iget-object v0, p0, LX/2UR;->f:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, LX/6Mz;->a:[Ljava/lang/String;

    const-string v5, "contact_id"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 416080
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/2UR;->i:LX/2UU;

    invoke-virtual {v1, v0}, LX/2UU;->a(Landroid/database/Cursor;)LX/6NA;

    move-result-object v3

    goto :goto_0
.end method

.method public final d()Landroid/database/Cursor;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 416081
    iget-object v0, p0, LX/2UR;->j:LX/1Ml;

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 416082
    :goto_0
    return-object v6

    .line 416083
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/2UR;->f:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, LX/2UR;->e:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    move-object v6, v0

    .line 416084
    goto :goto_0

    .line 416085
    :catch_0
    move-exception v0

    .line 416086
    sget-object v1, LX/2UR;->a:Ljava/lang/Class;

    const-string v2, "Got Exception in getRawContactsIdAndVersionCursor, closing open cursor."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v6

    goto :goto_1
.end method
