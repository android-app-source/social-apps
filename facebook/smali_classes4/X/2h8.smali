.class public final enum LX/2h8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2h8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2h8;

.field public static final enum CI_PYMK:LX/2h8;

.field public static final enum CONTACT_IMPORTER:LX/2h8;

.field public static final enum ENTITY_CARDS:LX/2h8;

.field public static final enum FEED_FRIENDABLE_HEADER:LX/2h8;

.field public static final enum FRIENDING_CARD:LX/2h8;

.field public static final enum FRIENDING_RADAR:LX/2h8;

.field public static final enum FRIENDS_CENTER_FRIENDS:LX/2h8;

.field public static final enum FRIENDS_CENTER_OUTGOING_REQUESTS_TAB:LX/2h8;

.field public static final enum FRIENDS_CENTER_REQUESTS:LX/2h8;

.field public static final enum FRIENDS_CENTER_SEARCH:LX/2h8;

.field public static final enum FRIENDS_CENTER_SUGGESTIONS:LX/2h8;

.field public static final enum FRIENDS_TAB:LX/2h8;

.field public static final enum NEARBY_FRIENDS:LX/2h8;

.field public static final enum NETEGO_PYMK:LX/2h8;

.field public static final enum NEWSFEED:LX/2h8;

.field public static final enum PROFILE_BROWSER:LX/2h8;

.field public static final enum PROFILE_BROWSER_EVENTS:LX/2h8;

.field public static final enum PROFILE_BUTTON:LX/2h8;

.field public static final enum PROFILE_FRIENDS_BOX:LX/2h8;

.field public static final enum PYMK:LX/2h8;

.field public static final enum PYMK_NUX:LX/2h8;

.field public static final enum PYMK_PUSH_NOTIF:LX/2h8;

.field public static final enum PYMK_SIDESHOW:LX/2h8;

.field public static final enum PYMK_TIMELINE_CHAIN:LX/2h8;

.field public static final enum SEARCH:LX/2h8;

.field public static final enum SUGGESTION:LX/2h8;

.field public static final enum TIMELINE_FRIENDS_COLLECTION:LX/2h8;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 449768
    new-instance v0, LX/2h8;

    const-string v1, "CI_PYMK"

    const-string v2, "ci_pymk"

    invoke-direct {v0, v1, v4, v2}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->CI_PYMK:LX/2h8;

    .line 449769
    new-instance v0, LX/2h8;

    const-string v1, "CONTACT_IMPORTER"

    const-string v2, "friend_finder"

    invoke-direct {v0, v1, v5, v2}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->CONTACT_IMPORTER:LX/2h8;

    .line 449770
    new-instance v0, LX/2h8;

    const-string v1, "ENTITY_CARDS"

    const-string v2, "entity_cards"

    invoke-direct {v0, v1, v6, v2}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->ENTITY_CARDS:LX/2h8;

    .line 449771
    new-instance v0, LX/2h8;

    const-string v1, "FEED_FRIENDABLE_HEADER"

    const-string v2, "feed_friendable_header"

    invoke-direct {v0, v1, v7, v2}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->FEED_FRIENDABLE_HEADER:LX/2h8;

    .line 449772
    new-instance v0, LX/2h8;

    const-string v1, "FRIENDING_CARD"

    const-string v2, "friending_card"

    invoke-direct {v0, v1, v8, v2}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->FRIENDING_CARD:LX/2h8;

    .line 449773
    new-instance v0, LX/2h8;

    const-string v1, "FRIENDING_RADAR"

    const/4 v2, 0x5

    const-string v3, "friending_radar"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->FRIENDING_RADAR:LX/2h8;

    .line 449774
    new-instance v0, LX/2h8;

    const-string v1, "FRIENDS_CENTER_FRIENDS"

    const/4 v2, 0x6

    const-string v3, "friend_center_friends"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->FRIENDS_CENTER_FRIENDS:LX/2h8;

    .line 449775
    new-instance v0, LX/2h8;

    const-string v1, "FRIENDS_CENTER_OUTGOING_REQUESTS_TAB"

    const/4 v2, 0x7

    const-string v3, "friend_center_outgoing_req"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->FRIENDS_CENTER_OUTGOING_REQUESTS_TAB:LX/2h8;

    .line 449776
    new-instance v0, LX/2h8;

    const-string v1, "FRIENDS_CENTER_REQUESTS"

    const/16 v2, 0x8

    const-string v3, "friend_center_requests"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->FRIENDS_CENTER_REQUESTS:LX/2h8;

    .line 449777
    new-instance v0, LX/2h8;

    const-string v1, "FRIENDS_CENTER_SEARCH"

    const/16 v2, 0x9

    const-string v3, "friend_center_search"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->FRIENDS_CENTER_SEARCH:LX/2h8;

    .line 449778
    new-instance v0, LX/2h8;

    const-string v1, "FRIENDS_CENTER_SUGGESTIONS"

    const/16 v2, 0xa

    const-string v3, "friend_browser"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->FRIENDS_CENTER_SUGGESTIONS:LX/2h8;

    .line 449779
    new-instance v0, LX/2h8;

    const-string v1, "FRIENDS_TAB"

    const/16 v2, 0xb

    const-string v3, "timeline_friends_pagelet"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->FRIENDS_TAB:LX/2h8;

    .line 449780
    new-instance v0, LX/2h8;

    const-string v1, "NEARBY_FRIENDS"

    const/16 v2, 0xc

    const-string v3, "nearby_friends"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->NEARBY_FRIENDS:LX/2h8;

    .line 449781
    new-instance v0, LX/2h8;

    const-string v1, "NETEGO_PYMK"

    const/16 v2, 0xd

    const-string v3, "netego_pymk"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->NETEGO_PYMK:LX/2h8;

    .line 449782
    new-instance v0, LX/2h8;

    const-string v1, "NEWSFEED"

    const/16 v2, 0xe

    const-string v3, "newsfeed"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->NEWSFEED:LX/2h8;

    .line 449783
    new-instance v0, LX/2h8;

    const-string v1, "PROFILE_BROWSER"

    const/16 v2, 0xf

    const-string v3, "profile_browser"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->PROFILE_BROWSER:LX/2h8;

    .line 449784
    new-instance v0, LX/2h8;

    const-string v1, "PROFILE_BROWSER_EVENTS"

    const/16 v2, 0x10

    const-string v3, "profile_browser_events"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->PROFILE_BROWSER_EVENTS:LX/2h8;

    .line 449785
    new-instance v0, LX/2h8;

    const-string v1, "PROFILE_BUTTON"

    const/16 v2, 0x11

    const-string v3, "profile_button"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->PROFILE_BUTTON:LX/2h8;

    .line 449786
    new-instance v0, LX/2h8;

    const-string v1, "PROFILE_FRIENDS_BOX"

    const/16 v2, 0x12

    const-string v3, "friends_box"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->PROFILE_FRIENDS_BOX:LX/2h8;

    .line 449787
    new-instance v0, LX/2h8;

    const-string v1, "PYMK"

    const/16 v2, 0x13

    const-string v3, "people_you_may_know"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->PYMK:LX/2h8;

    .line 449788
    new-instance v0, LX/2h8;

    const-string v1, "PYMK_SIDESHOW"

    const/16 v2, 0x14

    const-string v3, "pymk_sideshow"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->PYMK_SIDESHOW:LX/2h8;

    .line 449789
    new-instance v0, LX/2h8;

    const-string v1, "PYMK_TIMELINE_CHAIN"

    const/16 v2, 0x15

    const-string v3, "pymk_timeline_chain"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->PYMK_TIMELINE_CHAIN:LX/2h8;

    .line 449790
    new-instance v0, LX/2h8;

    const-string v1, "PYMK_NUX"

    const/16 v2, 0x16

    const-string v3, "wizard_importers"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->PYMK_NUX:LX/2h8;

    .line 449791
    new-instance v0, LX/2h8;

    const-string v1, "SEARCH"

    const/16 v2, 0x17

    const-string v3, "search"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->SEARCH:LX/2h8;

    .line 449792
    new-instance v0, LX/2h8;

    const-string v1, "SUGGESTION"

    const/16 v2, 0x18

    const-string v3, "friend_suggestion"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->SUGGESTION:LX/2h8;

    .line 449793
    new-instance v0, LX/2h8;

    const-string v1, "TIMELINE_FRIENDS_COLLECTION"

    const/16 v2, 0x19

    const-string v3, "timeline_friends_collection"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->TIMELINE_FRIENDS_COLLECTION:LX/2h8;

    .line 449794
    new-instance v0, LX/2h8;

    const-string v1, "PYMK_PUSH_NOTIF"

    const/16 v2, 0x1a

    const-string v3, "pymk_push_notif"

    invoke-direct {v0, v1, v2, v3}, LX/2h8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2h8;->PYMK_PUSH_NOTIF:LX/2h8;

    .line 449795
    const/16 v0, 0x1b

    new-array v0, v0, [LX/2h8;

    sget-object v1, LX/2h8;->CI_PYMK:LX/2h8;

    aput-object v1, v0, v4

    sget-object v1, LX/2h8;->CONTACT_IMPORTER:LX/2h8;

    aput-object v1, v0, v5

    sget-object v1, LX/2h8;->ENTITY_CARDS:LX/2h8;

    aput-object v1, v0, v6

    sget-object v1, LX/2h8;->FEED_FRIENDABLE_HEADER:LX/2h8;

    aput-object v1, v0, v7

    sget-object v1, LX/2h8;->FRIENDING_CARD:LX/2h8;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/2h8;->FRIENDING_RADAR:LX/2h8;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2h8;->FRIENDS_CENTER_FRIENDS:LX/2h8;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2h8;->FRIENDS_CENTER_OUTGOING_REQUESTS_TAB:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2h8;->FRIENDS_CENTER_REQUESTS:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2h8;->FRIENDS_CENTER_SEARCH:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/2h8;->FRIENDS_CENTER_SUGGESTIONS:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/2h8;->FRIENDS_TAB:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/2h8;->NEARBY_FRIENDS:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/2h8;->NETEGO_PYMK:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/2h8;->NEWSFEED:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/2h8;->PROFILE_BROWSER:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/2h8;->PROFILE_BROWSER_EVENTS:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/2h8;->PROFILE_BUTTON:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/2h8;->PROFILE_FRIENDS_BOX:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/2h8;->PYMK:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/2h8;->PYMK_SIDESHOW:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/2h8;->PYMK_TIMELINE_CHAIN:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/2h8;->PYMK_NUX:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/2h8;->SEARCH:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/2h8;->SUGGESTION:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/2h8;->TIMELINE_FRIENDS_COLLECTION:LX/2h8;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/2h8;->PYMK_PUSH_NOTIF:LX/2h8;

    aput-object v2, v0, v1

    sput-object v0, LX/2h8;->$VALUES:[LX/2h8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 449796
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 449797
    iput-object p3, p0, LX/2h8;->value:Ljava/lang/String;

    .line 449798
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2h8;
    .locals 1

    .prologue
    .line 449767
    const-class v0, LX/2h8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2h8;

    return-object v0
.end method

.method public static values()[LX/2h8;
    .locals 1

    .prologue
    .line 449766
    sget-object v0, LX/2h8;->$VALUES:[LX/2h8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2h8;

    return-object v0
.end method
