.class public LX/2Ym;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/auth/protocol/WorkCommunityPeekResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/0SG;


# direct methods
.method public constructor <init>(LX/0sO;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 421325
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 421326
    iput-object p2, p0, LX/2Ym;->b:LX/0SG;

    .line 421327
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ym;
    .locals 3

    .prologue
    .line 421328
    new-instance v2, LX/2Ym;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v0

    check-cast v0, LX/0sO;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {v2, v0, v1}, LX/2Ym;-><init>(LX/0sO;LX/0SG;)V

    .line 421329
    move-object v0, v2

    .line 421330
    return-object v0
.end method

.method private a(LX/15w;)Lcom/facebook/auth/protocol/WorkCommunityPeekResult;
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v4, 0x0

    const/4 v12, 0x0

    .line 421331
    const-class v0, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;

    invoke-virtual {p1, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;

    .line 421332
    invoke-virtual {v5}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;->a()Z

    move-result v11

    .line 421333
    if-eqz v11, :cond_0

    .line 421334
    invoke-virtual {v5}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;->j()Lcom/facebook/work/config/community/protocol/WorkCommunityQueryModels$WorkCommunityDataFragmentModel$WorkCommunityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/config/community/protocol/WorkCommunityQueryModels$WorkCommunityDataFragmentModel$WorkCommunityModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 421335
    invoke-virtual {v5}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;->j()Lcom/facebook/work/config/community/protocol/WorkCommunityQueryModels$WorkCommunityDataFragmentModel$WorkCommunityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/config/community/protocol/WorkCommunityQueryModels$WorkCommunityDataFragmentModel$WorkCommunityModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 421336
    :goto_0
    new-instance v0, Lcom/facebook/work/auth/request/model/WorkCommunity;

    invoke-virtual {v5}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;->j()Lcom/facebook/work/config/community/protocol/WorkCommunityQueryModels$WorkCommunityDataFragmentModel$WorkCommunityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/work/config/community/protocol/WorkCommunityQueryModels$WorkCommunityDataFragmentModel$WorkCommunityModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;->j()Lcom/facebook/work/config/community/protocol/WorkCommunityQueryModels$WorkCommunityDataFragmentModel$WorkCommunityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/work/config/community/protocol/WorkCommunityQueryModels$WorkCommunityDataFragmentModel$WorkCommunityModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;->k()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/work/auth/request/model/WorkCommunity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v4

    move-object v5, v0

    .line 421337
    :goto_1
    new-instance v0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v2, p0, LX/2Ym;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    move v4, v11

    invoke-direct/range {v0 .. v6}, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;-><init>(LX/0ta;JZLcom/facebook/work/auth/request/model/WorkCommunity;LX/0Px;)V

    return-object v0

    .line 421338
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 421339
    invoke-virtual {v5}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;->l()LX/2uF;

    move-result-object v1

    invoke-virtual {v1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :cond_1
    :goto_2
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 421340
    invoke-virtual {v3, v2, v12}, LX/15i;->g(II)I

    move-result v5

    if-eqz v5, :cond_1

    .line 421341
    invoke-virtual {v3, v2, v12}, LX/15i;->g(II)I

    move-result v5

    invoke-virtual {v3, v5, v13}, LX/15i;->g(II)I

    move-result v5

    if-eqz v5, :cond_3

    .line 421342
    invoke-virtual {v3, v2, v12}, LX/15i;->g(II)I

    move-result v5

    invoke-virtual {v3, v5, v13}, LX/15i;->g(II)I

    move-result v5

    invoke-virtual {v3, v5, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    .line 421343
    :goto_3
    invoke-virtual {v3, v2, v12}, LX/15i;->g(II)I

    move-result v6

    .line 421344
    invoke-virtual {v3, v2, v12}, LX/15i;->g(II)I

    move-result v7

    .line 421345
    invoke-virtual {v3, v2, v12}, LX/15i;->g(II)I

    move-result v10

    .line 421346
    new-instance v5, Lcom/facebook/work/auth/request/model/WorkCommunity;

    const/4 v9, 0x2

    invoke-virtual {v3, v6, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v7, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v2, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    const/4 v2, 0x3

    invoke-virtual {v3, v10, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v5 .. v10}, Lcom/facebook/work/auth/request/model/WorkCommunity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 421347
    :cond_2
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    move-object v5, v4

    goto :goto_1

    :cond_3
    move-object v8, v4

    goto :goto_3

    :cond_4
    move-object v3, v4

    goto/16 :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 421348
    invoke-direct {p0, p3}, LX/2Ym;->a(LX/15w;)Lcom/facebook/auth/protocol/WorkCommunityPeekResult;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 421349
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 1

    .prologue
    .line 421350
    new-instance v0, LX/42U;

    invoke-direct {v0}, LX/42U;-><init>()V

    move-object v0, v0

    .line 421351
    return-object v0
.end method
