.class public LX/2HA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Gh;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile p:LX/2HA;


# instance fields
.field public final a:LX/2H7;

.field public final c:Landroid/content/Context;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:LX/2Gs;

.field private final f:LX/0kb;

.field public final g:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

.field public final h:LX/2H0;

.field private final i:LX/0SG;

.field private final j:LX/01T;

.field public final k:LX/2H4;

.field private final l:LX/2H3;

.field private final m:LX/2HD;

.field private final n:LX/2HC;

.field public final o:LX/0s6;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 390022
    const-class v0, LX/2HA;

    sput-object v0, LX/2HA;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Gs;LX/0kb;Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Gq;LX/0SG;LX/2Gr;LX/2Gp;LX/01T;LX/2HC;LX/0s6;LX/2HD;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390024
    iput-object p1, p0, LX/2HA;->c:Landroid/content/Context;

    .line 390025
    iput-object p2, p0, LX/2HA;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 390026
    iput-object p3, p0, LX/2HA;->e:LX/2Gs;

    .line 390027
    iput-object p4, p0, LX/2HA;->f:LX/0kb;

    .line 390028
    iput-object p5, p0, LX/2HA;->g:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    .line 390029
    iput-object p7, p0, LX/2HA;->i:LX/0SG;

    .line 390030
    iput-object p10, p0, LX/2HA;->j:LX/01T;

    .line 390031
    iput-object p11, p0, LX/2HA;->n:LX/2HC;

    .line 390032
    move-object/from16 v0, p12

    iput-object v0, p0, LX/2HA;->o:LX/0s6;

    .line 390033
    move-object/from16 v0, p13

    iput-object v0, p0, LX/2HA;->m:LX/2HD;

    .line 390034
    sget-object v1, LX/2Ge;->GCM:LX/2Ge;

    invoke-virtual {p6, v1}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v1

    iput-object v1, p0, LX/2HA;->h:LX/2H0;

    .line 390035
    sget-object v1, LX/2Ge;->GCM:LX/2Ge;

    invoke-virtual {p8, v1}, LX/2Gr;->a(LX/2Ge;)LX/2H3;

    move-result-object v1

    iput-object v1, p0, LX/2HA;->l:LX/2H3;

    .line 390036
    sget-object v1, LX/2Ge;->GCM:LX/2Ge;

    iget-object v2, p0, LX/2HA;->l:LX/2H3;

    iget-object v3, p0, LX/2HA;->h:LX/2H0;

    invoke-virtual {p9, v1, v2, v3}, LX/2Gp;->a(LX/2Ge;LX/2H3;LX/2H0;)LX/2H4;

    move-result-object v1

    iput-object v1, p0, LX/2HA;->k:LX/2H4;

    .line 390037
    new-instance v1, LX/2HF;

    invoke-direct {v1, p0}, LX/2HF;-><init>(LX/2HA;)V

    iput-object v1, p0, LX/2HA;->a:LX/2H7;

    .line 390038
    return-void
.end method

.method public static a(LX/0QB;)LX/2HA;
    .locals 3

    .prologue
    .line 390012
    sget-object v0, LX/2HA;->p:LX/2HA;

    if-nez v0, :cond_1

    .line 390013
    const-class v1, LX/2HA;

    monitor-enter v1

    .line 390014
    :try_start_0
    sget-object v0, LX/2HA;->p:LX/2HA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390015
    if-eqz v2, :cond_0

    .line 390016
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2HA;->b(LX/0QB;)LX/2HA;

    move-result-object v0

    sput-object v0, LX/2HA;->p:LX/2HA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390017
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390018
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390019
    :cond_1
    sget-object v0, LX/2HA;->p:LX/2HA;

    return-object v0

    .line 390020
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390021
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/2HA;
    .locals 14

    .prologue
    .line 390010
    new-instance v0, LX/2HA;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/2Gs;->a(LX/0QB;)LX/2Gs;

    move-result-object v3

    check-cast v3, LX/2Gs;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {p0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/0QB;)Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    move-result-object v5

    check-cast v5, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    invoke-static {p0}, LX/2Gq;->a(LX/0QB;)LX/2Gq;

    move-result-object v6

    check-cast v6, LX/2Gq;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {p0}, LX/2Gr;->a(LX/0QB;)LX/2Gr;

    move-result-object v8

    check-cast v8, LX/2Gr;

    const-class v9, LX/2Gp;

    invoke-interface {p0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/2Gp;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v10

    check-cast v10, LX/01T;

    invoke-static {p0}, LX/2HC;->a(LX/0QB;)LX/2HC;

    move-result-object v11

    check-cast v11, LX/2HC;

    invoke-static {p0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v12

    check-cast v12, LX/0s6;

    invoke-static {p0}, LX/2HD;->a(LX/0QB;)LX/2HD;

    move-result-object v13

    check-cast v13, LX/2HD;

    invoke-direct/range {v0 .. v13}, LX/2HA;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Gs;LX/0kb;Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Gq;LX/0SG;LX/2Gr;LX/2Gp;LX/01T;LX/2HC;LX/0s6;LX/2HD;)V

    .line 390011
    return-object v0
.end method

.method public static b(LX/2HA;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 390004
    invoke-virtual {p0}, LX/2HA;->a()Ljava/lang/String;

    move-result-object v1

    .line 390005
    if-nez v1, :cond_0

    .line 390006
    const/4 v0, 0x0

    .line 390007
    :goto_0
    return-object v0

    .line 390008
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 390009
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private e()LX/2H9;
    .locals 12

    .prologue
    const-wide/16 v10, 0x3e8

    .line 390039
    iget-object v0, p0, LX/2HA;->h:LX/2H0;

    invoke-virtual {v0}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v0

    .line 390040
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390041
    sget-object v0, LX/2H9;->NONE:LX/2H9;

    .line 390042
    :goto_0
    return-object v0

    .line 390043
    :cond_0
    iget-object v0, p0, LX/2HA;->h:LX/2H0;

    invoke-virtual {v0}, LX/2H0;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 390044
    sget-object v0, LX/2H9;->UPGRADED:LX/2H9;

    goto :goto_0

    .line 390045
    :cond_1
    invoke-static {p0}, LX/2HA;->f(LX/2HA;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/2HA;->h:LX/2H0;

    invoke-virtual {v0}, LX/2H0;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 390046
    sget-object v0, LX/2H9;->WRONG_TYPE:LX/2H9;

    goto :goto_0

    .line 390047
    :cond_2
    iget-object v0, p0, LX/2HA;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 390048
    iget-object v2, p0, LX/2HA;->h:LX/2H0;

    invoke-virtual {v2}, LX/2H0;->l()J

    move-result-wide v2

    .line 390049
    iget-object v4, p0, LX/2HA;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v5, p0, LX/2HA;->l:LX/2H3;

    .line 390050
    iget-object v6, v5, LX/2H3;->g:LX/0Tn;

    move-object v5, v6

    .line 390051
    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 390052
    iget-object v6, p0, LX/2HA;->m:LX/2HD;

    iget-object v7, p0, LX/2HA;->j:LX/01T;

    invoke-virtual {v7}, LX/01T;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2HD;->a(Ljava/lang/String;)LX/Ceo;

    move-result-object v6

    .line 390053
    sub-long v2, v0, v2

    iget-wide v8, v6, LX/Ceo;->a:J

    mul-long/2addr v8, v10

    cmp-long v2, v2, v8

    if-lez v2, :cond_3

    sub-long/2addr v0, v4

    iget-wide v2, v6, LX/Ceo;->b:J

    mul-long/2addr v2, v10

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 390054
    sget-object v0, LX/2H9;->EXPIRED:LX/2H9;

    goto :goto_0

    .line 390055
    :cond_3
    sget-object v0, LX/2H9;->CURRENT:LX/2H9;

    goto :goto_0
.end method

.method private static f(LX/2HA;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 390003
    iget-object v1, p0, LX/2HA;->o:LX/0s6;

    const-string v2, "com.google.android.gms"

    invoke-virtual {v1, v2, v0}, LX/01H;->e(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 389995
    invoke-static {p0}, LX/2HA;->f(LX/2HA;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389996
    const-string v0, "com.google.android.gms"

    .line 389997
    :goto_0
    return-object v0

    .line 389998
    :cond_0
    const/4 v0, 0x0

    .line 389999
    iget-object v1, p0, LX/2HA;->o:LX/0s6;

    const-string v2, "com.google.android.gsf"

    invoke-virtual {v1, v2, v0}, LX/01H;->e(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 390000
    if-eqz v0, :cond_2

    .line 390001
    const-string v0, "com.google.android.gsf"

    goto :goto_0

    .line 390002
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 8

    .prologue
    .line 389952
    invoke-direct {p0}, LX/2HA;->e()LX/2H9;

    move-result-object v0

    .line 389953
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 389954
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 389955
    const-string v2, "force_fb_reg"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389956
    sget-object v2, LX/2H9;->NONE:LX/2H9;

    if-eq v2, v0, :cond_0

    sget-object v2, LX/2H9;->UPGRADED:LX/2H9;

    if-eq v2, v0, :cond_0

    sget-object v2, LX/2H9;->WRONG_TYPE:LX/2H9;

    if-ne v2, v0, :cond_2

    .line 389957
    :cond_0
    iget-object v2, p0, LX/2HA;->n:LX/2HC;

    const/4 v3, 0x0

    .line 389958
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v4

    .line 389959
    const-string v5, "com.android.vending"

    const-string v6, "com.android.vending"

    invoke-static {v2, v6}, LX/2HC;->a(LX/2HC;Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389960
    iget-object v5, v2, LX/2HC;->c:LX/0s6;

    const-string v6, "com.google.android.gms"

    const/16 v7, 0x40

    invoke-virtual {v5, v6, v7}, LX/01H;->e(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    .line 389961
    if-nez v5, :cond_7

    .line 389962
    const-string v5, "com.google.android.gms"

    const-string v6, "0"

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389963
    const-string v5, "com.google.android.gsf"

    const-string v6, "com.google.android.gsf"

    invoke-static {v2, v6}, LX/2HC;->a(LX/2HC;Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389964
    iget-object v5, v2, LX/2HC;->b:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    const-string v6, "com.google"

    invoke-virtual {v5, v6}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v5

    .line 389965
    const-string v6, "has_g_account"

    array-length v5, v5

    if-lez v5, :cond_1

    const/4 v3, 0x1

    :cond_1
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389966
    :goto_0
    iget-object v3, v2, LX/2HC;->b:Landroid/content/Context;

    invoke-static {v3}, LX/1oW;->a(Landroid/content/Context;)I

    move-result v3

    .line 389967
    const-string v5, "gms_availability"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389968
    move-object v2, v4

    .line 389969
    invoke-interface {v1, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 389970
    :cond_2
    sget-object v2, LX/2H9;->CURRENT:LX/2H9;

    if-eq v0, v2, :cond_3

    .line 389971
    iget-object v2, p0, LX/2HA;->e:LX/2Gs;

    invoke-virtual {v0}, LX/2H9;->name()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/2HA;->h:LX/2H0;

    invoke-virtual {v4}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v4

    .line 389972
    const-string v5, "push_reg_gcm_initial_status"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "registration_id"

    invoke-static {v5, v6, v1, v7, v4}, LX/2gN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 389973
    invoke-static {v2, v5}, LX/2Gs;->a(LX/2Gs;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 389974
    :cond_3
    sget-object v1, LX/2gO;->a:[I

    invoke-virtual {v0}, LX/2H9;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 389975
    :goto_1
    return-void

    .line 389976
    :pswitch_0
    if-eqz p1, :cond_4

    .line 389977
    iget-object v0, p0, LX/2HA;->g:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->GCM:LX/2Ge;

    iget-object v2, p0, LX/2HA;->a:LX/2H7;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_1

    .line 389978
    :cond_4
    iget-object v0, p0, LX/2HA;->g:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->GCM:LX/2Ge;

    iget-object v2, p0, LX/2HA;->a:LX/2H7;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b(LX/2Ge;LX/2H7;)V

    goto :goto_1

    .line 389979
    :pswitch_1
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 389980
    iget-object v1, p0, LX/2HA;->e:LX/2Gs;

    sget-object v2, LX/Cem;->ATTEMPT:LX/Cem;

    invoke-virtual {v2}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/2HA;->h:LX/2H0;

    invoke-virtual {v3}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/2Gs;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 389981
    const-string v1, "com.google.android.c2dm.intent.UNREGISTER"

    invoke-static {p0, v1}, LX/2HA;->b(LX/2HA;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 389982
    if-eqz v1, :cond_5

    .line 389983
    const-string v2, "app"

    iget-object v3, p0, LX/2HA;->c:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-static {v3, v5, v4, v5}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 389984
    :try_start_0
    iget-object v2, p0, LX/2HA;->c:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 389985
    :cond_5
    :goto_2
    iget-object v1, p0, LX/2HA;->g:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v2, LX/2Ge;->GCM:LX/2Ge;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/String;)Z

    .line 389986
    iget-object v1, p0, LX/2HA;->h:LX/2H0;

    invoke-virtual {v1}, LX/2H0;->h()V

    .line 389987
    :cond_6
    :pswitch_2
    invoke-virtual {p0}, LX/2HA;->b()V

    goto :goto_1

    .line 389988
    :pswitch_3
    iget-object v0, p0, LX/2HA;->f:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_6

    goto :goto_1

    .line 389989
    :cond_7
    const-string v6, "com.google.android.gms"

    const-string v7, "1"

    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389990
    const-string v6, "gms_version"

    iget v5, v5, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389991
    iget-object v5, v2, LX/2HC;->c:LX/0s6;

    const-string v6, "com.google.android.gms"

    invoke-virtual {v5, v6, v3}, LX/01H;->f(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 389992
    if-nez v3, :cond_8

    .line 389993
    const-string v3, "com.google.android.gms"

    const-string v5, "0"

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 389994
    :cond_8
    const-string v5, "gms_enabled"

    iget-boolean v3, v3, Landroid/content/pm/ApplicationInfo;->enabled:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :catch_0
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 389928
    const-string v0, "com.google.android.c2dm.intent.REGISTER"

    invoke-static {p0, v0}, LX/2HA;->b(LX/2HA;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 389929
    iget-object v2, p0, LX/2HA;->k:LX/2H4;

    sget-object v0, LX/2gP;->ATTEMPT:LX/2gP;

    invoke-virtual {v0}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v3, v0}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 389930
    if-nez v1, :cond_2

    .line 389931
    :cond_0
    :goto_1
    return-void

    .line 389932
    :cond_1
    const-string v0, "missing-package"

    goto :goto_0

    .line 389933
    :cond_2
    iget-object v0, p0, LX/2HA;->k:LX/2H4;

    invoke-virtual {v0}, LX/2H4;->a()V

    .line 389934
    iget-object v0, p0, LX/2HA;->c:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-static {v0, v5, v2, v5}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 389935
    const-string v2, "com.google.android.gsf"

    invoke-virtual {v1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 389936
    const-string v3, "app"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 389937
    const-string v3, "sender"

    if-eqz v2, :cond_3

    const-string v0, "facebook.android@gmail.com"

    :goto_2
    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 389938
    iget-object v0, p0, LX/2HA;->h:LX/2H0;

    .line 389939
    iget-object v3, v0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    iget-object v4, v0, LX/2H0;->g:LX/2H3;

    .line 389940
    iget-object v0, v4, LX/2H3;->l:LX/0Tn;

    move-object v4, v0

    .line 389941
    invoke-interface {v3, v4, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 389942
    :try_start_0
    iget-object v0, p0, LX/2HA;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    .line 389943
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startService="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 389944
    if-nez v0, :cond_0

    .line 389945
    iget-object v0, p0, LX/2HA;->k:LX/2H4;

    sget-object v1, LX/2gP;->MISSING_COMPONENT:LX/2gP;

    invoke-virtual {v1}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 389946
    :catch_0
    move-exception v0

    .line 389947
    sget-object v1, LX/2HA;->b:Ljava/lang/Class;

    const-string v2, "Cannot start registraiton service %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "com.google.android.c2dm.intent.REGISTER"

    aput-object v4, v3, v5

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 389948
    iget-object v0, p0, LX/2HA;->k:LX/2H4;

    invoke-virtual {v0}, LX/2H4;->c()V

    .line 389949
    iget-object v0, p0, LX/2HA;->k:LX/2H4;

    sget-object v1, LX/2gP;->PERMISSION_DENIED:LX/2gP;

    invoke-virtual {v1}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 389950
    :cond_3
    const-string v0, "15057814354"

    goto :goto_2
.end method

.method public final c()LX/2H7;
    .locals 1

    .prologue
    .line 389951
    iget-object v0, p0, LX/2HA;->a:LX/2H7;

    return-object v0
.end method
