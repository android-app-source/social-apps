.class public LX/2GZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/2GZ;


# instance fields
.field public final b:Landroid/content/pm/PackageManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 388868
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "350685531728"

    const-string v2, "com.facebook.wakizashi"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "256002347743983"

    const-string v2, "com.facebook.orca"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "121876164619130"

    const-string v2, "com.facebook.pages.app"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "306069495113"

    const-string v2, "com.whatsapp"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "567067343352427"

    const-string v2, "com.instagram.android"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "295940867235646"

    const-string v2, "com.instagram.bolt"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "881555691867714"

    const-string v2, "com.instagram.layout"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "358698234273213"

    const-string v2, "com.facebook.groups"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "794956213882720"

    const-string v2, "com.facebook.moments"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "255620677933453"

    const-string v2, "com.facebook.slingshot"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "1548792348668883"

    const-string v2, "com.oculus.home"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "1437758943160428"

    const-string v2, "com.oculus.horizon"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/2GZ;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 388869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388870
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, LX/2GZ;->b:Landroid/content/pm/PackageManager;

    .line 388871
    return-void
.end method

.method public static a(LX/0QB;)LX/2GZ;
    .locals 4

    .prologue
    .line 388872
    sget-object v0, LX/2GZ;->c:LX/2GZ;

    if-nez v0, :cond_1

    .line 388873
    const-class v1, LX/2GZ;

    monitor-enter v1

    .line 388874
    :try_start_0
    sget-object v0, LX/2GZ;->c:LX/2GZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 388875
    if-eqz v2, :cond_0

    .line 388876
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 388877
    new-instance p0, LX/2GZ;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/2GZ;-><init>(Landroid/content/Context;)V

    .line 388878
    move-object v0, p0

    .line 388879
    sput-object v0, LX/2GZ;->c:LX/2GZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 388880
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 388881
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 388882
    :cond_1
    sget-object v0, LX/2GZ;->c:LX/2GZ;

    return-object v0

    .line 388883
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 388884
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 388885
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "app_installations"

    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 388886
    sget-object v0, LX/2GZ;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 388887
    sget-object v1, LX/2GZ;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 p1, 0x0

    .line 388888
    iget-object p2, p0, LX/2GZ;->b:Landroid/content/pm/PackageManager;

    if-nez p2, :cond_2

    .line 388889
    :goto_1
    move v1, p1

    .line 388890
    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_2

    .line 388891
    :cond_1
    return-object v2

    .line 388892
    :cond_2
    :try_start_0
    iget-object p2, p0, LX/2GZ;->b:Landroid/content/pm/PackageManager;

    const/4 p3, 0x0

    invoke-virtual {p2, v1, p3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 388893
    iget-boolean p1, p1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    goto :goto_1

    .line 388894
    :catch_0
    goto :goto_1
.end method
