.class public final LX/2GE;
.super LX/2GF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2GF",
        "<TK;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0cn;


# direct methods
.method public constructor <init>(LX/0cn;)V
    .locals 1

    .prologue
    .line 387996
    iput-object p1, p0, LX/2GE;->a:LX/0cn;

    invoke-direct {p0}, LX/2GF;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 388002
    iget-object v0, p0, LX/2GE;->a:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->clear()V

    .line 388003
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 388001
    iget-object v0, p0, LX/2GE;->a:LX/0cn;

    invoke-virtual {v0, p1}, LX/0cn;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 388000
    iget-object v0, p0, LX/2GE;->a:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 387999
    new-instance v0, LX/2GG;

    iget-object v1, p0, LX/2GE;->a:LX/0cn;

    invoke-direct {v0, v1}, LX/2GG;-><init>(LX/0cn;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 387998
    iget-object v0, p0, LX/2GE;->a:LX/0cn;

    invoke-virtual {v0, p1}, LX/0cn;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 387997
    iget-object v0, p0, LX/2GE;->a:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->size()I

    move-result v0

    return v0
.end method
