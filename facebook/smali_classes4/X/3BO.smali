.class public LX/3BO;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C6y;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3fQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 528237
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/3BO;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3fQ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 528234
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 528235
    iput-object p1, p0, LX/3BO;->b:LX/0Ot;

    .line 528236
    return-void
.end method

.method public static a(LX/0QB;)LX/3BO;
    .locals 4

    .prologue
    .line 528223
    const-class v1, LX/3BO;

    monitor-enter v1

    .line 528224
    :try_start_0
    sget-object v0, LX/3BO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 528225
    sput-object v2, LX/3BO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 528226
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528227
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 528228
    new-instance v3, LX/3BO;

    const/16 p0, 0x91d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3BO;-><init>(LX/0Ot;)V

    .line 528229
    move-object v0, v3

    .line 528230
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 528231
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3BO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 528232
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 528233
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 528147
    check-cast p2, LX/C6z;

    .line 528148
    iget-object v0, p0, LX/3BO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3fQ;

    iget-object v1, p2, LX/C6z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C6z;->b:LX/1Pm;

    const/4 v5, 0x1

    .line 528149
    invoke-static {v1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 528150
    iget-object v3, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 528151
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 528152
    iget-object v4, v0, LX/3fQ;->c:LX/3BI;

    invoke-static {v4, v3}, LX/3fQ;->a(LX/3BI;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v7

    .line 528153
    if-nez v7, :cond_0

    .line 528154
    const/4 v3, 0x0

    .line 528155
    :goto_0
    move-object v0, v3

    .line 528156
    return-object v0

    .line 528157
    :cond_0
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v8

    .line 528158
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPlace;->z()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v9

    .line 528159
    invoke-static {v3}, LX/3BI;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v10

    .line 528160
    if-nez v10, :cond_3

    move v4, v5

    .line 528161
    :goto_1
    if-eqz v10, :cond_4

    const-string v8, "checkin_story"

    invoke-static {v3, v8}, LX/3BI;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v3

    .line 528162
    :goto_2
    iget-object v8, v0, LX/3fQ;->e:LX/17V;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPlace;->Y()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v10

    invoke-static {v6}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v11

    const-string v12, "native_newsfeed"

    invoke-virtual {v8, v9, v10, v11, v12}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    .line 528163
    iget-object v9, v0, LX/3fQ;->d:LX/3BL;

    invoke-virtual {v9, v6}, LX/3BL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v9

    .line 528164
    const v10, 0x3ff33333    # 1.9f

    invoke-static {v9, v10}, LX/3BL;->a(IF)I

    move-result v10

    .line 528165
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v11

    const/4 v12, 0x6

    const/4 p0, 0x2

    invoke-interface {v11, v12, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v11

    iget-object v12, v0, LX/3fQ;->a:LX/CIw;

    const/4 p0, 0x0

    .line 528166
    new-instance p2, LX/CIv;

    invoke-direct {p2, v12}, LX/CIv;-><init>(LX/CIw;)V

    .line 528167
    sget-object v1, LX/CIw;->a:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CIu;

    .line 528168
    if-nez v1, :cond_1

    .line 528169
    new-instance v1, LX/CIu;

    invoke-direct {v1}, LX/CIu;-><init>()V

    .line 528170
    :cond_1
    invoke-static {v1, p1, p0, p0, p2}, LX/CIu;->a$redex0(LX/CIu;LX/1De;IILX/CIv;)V

    .line 528171
    move-object p2, v1

    .line 528172
    move-object p0, p2

    .line 528173
    move-object v12, p0

    .line 528174
    iget-object p0, v12, LX/CIu;->a:LX/CIv;

    iput v9, p0, LX/CIv;->a:I

    .line 528175
    iget-object p0, v12, LX/CIu;->d:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 528176
    move-object v9, v12

    .line 528177
    iget-object v12, v9, LX/CIu;->a:LX/CIv;

    iput v10, v12, LX/CIv;->b:I

    .line 528178
    iget-object v12, v9, LX/CIu;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v12, p0}, Ljava/util/BitSet;->set(I)V

    .line 528179
    move-object v9, v9

    .line 528180
    iget-object v10, v9, LX/CIu;->a:LX/CIv;

    iput-boolean v4, v10, LX/CIv;->f:Z

    .line 528181
    move-object v4, v9

    .line 528182
    iget-object v9, v4, LX/CIu;->a:LX/CIv;

    iput-object v3, v9, LX/CIv;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 528183
    iget-object v9, v4, LX/CIu;->d:Ljava/util/BitSet;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Ljava/util/BitSet;->set(I)V

    .line 528184
    move-object v3, v4

    .line 528185
    iget-object v4, v3, LX/CIu;->a:LX/CIv;

    iput-object v2, v4, LX/CIv;->d:LX/1Pt;

    .line 528186
    iget-object v4, v3, LX/CIu;->d:Ljava/util/BitSet;

    const/4 v9, 0x3

    invoke-virtual {v4, v9}, Ljava/util/BitSet;->set(I)V

    .line 528187
    move-object v3, v3

    .line 528188
    const/high16 v4, 0x3f000000    # 0.5f

    .line 528189
    iget-object v9, v3, LX/CIu;->a:LX/CIv;

    iput v4, v9, LX/CIv;->g:F

    .line 528190
    move-object v3, v3

    .line 528191
    const v4, 0x3f6e147b    # 0.93f

    .line 528192
    iget-object v9, v3, LX/CIu;->a:LX/CIv;

    iput v4, v9, LX/CIv;->h:F

    .line 528193
    move-object v3, v3

    .line 528194
    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    invoke-interface {v11, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/3fQ;->b:LX/Btm;

    const/4 v9, 0x0

    .line 528195
    new-instance v10, LX/Btl;

    invoke-direct {v10, v4}, LX/Btl;-><init>(LX/Btm;)V

    .line 528196
    sget-object v11, LX/Btm;->a:LX/0Zi;

    invoke-virtual {v11}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/Btk;

    .line 528197
    if-nez v11, :cond_2

    .line 528198
    new-instance v11, LX/Btk;

    invoke-direct {v11}, LX/Btk;-><init>()V

    .line 528199
    :cond_2
    invoke-static {v11, p1, v9, v9, v10}, LX/Btk;->a$redex0(LX/Btk;LX/1De;IILX/Btl;)V

    .line 528200
    move-object v10, v11

    .line 528201
    move-object v9, v10

    .line 528202
    move-object v4, v9

    .line 528203
    iget-object v9, v4, LX/Btk;->a:LX/Btl;

    iput-object v2, v9, LX/Btl;->d:LX/1Pm;

    .line 528204
    iget-object v9, v4, LX/Btk;->d:Ljava/util/BitSet;

    const/4 v10, 0x3

    invoke-virtual {v9, v10}, Ljava/util/BitSet;->set(I)V

    .line 528205
    move-object v4, v4

    .line 528206
    iget-object v9, v4, LX/Btk;->a:LX/Btl;

    iput-object v7, v9, LX/Btl;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 528207
    iget-object v9, v4, LX/Btk;->d:Ljava/util/BitSet;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Ljava/util/BitSet;->set(I)V

    .line 528208
    move-object v4, v4

    .line 528209
    iget-object v7, v4, LX/Btk;->a:LX/Btl;

    iput-object v6, v7, LX/Btl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 528210
    iget-object v7, v4, LX/Btk;->d:Ljava/util/BitSet;

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/util/BitSet;->set(I)V

    .line 528211
    move-object v4, v4

    .line 528212
    iget-object v6, v4, LX/Btk;->a:LX/Btl;

    iput-object v8, v6, LX/Btl;->e:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 528213
    iget-object v6, v4, LX/Btk;->d:Ljava/util/BitSet;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 528214
    move-object v4, v4

    .line 528215
    iget-object v6, v4, LX/Btk;->a:LX/Btl;

    iput-boolean v5, v6, LX/Btl;->c:Z

    .line 528216
    iget-object v6, v4, LX/Btk;->d:Ljava/util/BitSet;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 528217
    move-object v4, v4

    .line 528218
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    .line 528219
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 528220
    :cond_4
    const-string v3, "checkin_story"

    invoke-static {v9, v3, v8}, LX/3BX;->a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLocation;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v3

    goto/16 :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 528221
    invoke-static {}, LX/1dS;->b()V

    .line 528222
    const/4 v0, 0x0

    return-object v0
.end method
