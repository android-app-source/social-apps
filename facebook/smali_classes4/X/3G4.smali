.class public LX/3G4;
.super LX/3G3;
.source ""


# instance fields
.field public final h:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "LX/0zP;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0w7;

.field public final j:LX/0jT;

.field public final k:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/399;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JJIILjava/lang/Class;LX/0w7;LX/0jT;LX/0Rf;LX/399;)V
    .locals 11
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # LX/399;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJII",
            "Ljava/lang/Class",
            "<+",
            "LX/0zP;",
            ">;",
            "LX/0w7;",
            "LX/0jT;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/399;",
            ")V"
        }
    .end annotation

    .prologue
    .line 540393
    move-object v1, p0

    move/from16 v2, p7

    move-wide v3, p3

    move-object v5, p1

    move-object v6, p2

    move/from16 v7, p8

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, LX/3G3;-><init>(IJLjava/lang/String;Ljava/lang/String;IJ)V

    .line 540394
    move-object/from16 v0, p9

    iput-object v0, p0, LX/3G4;->h:Ljava/lang/Class;

    .line 540395
    move-object/from16 v0, p10

    iput-object v0, p0, LX/3G4;->i:LX/0w7;

    .line 540396
    move-object/from16 v0, p11

    iput-object v0, p0, LX/3G4;->j:LX/0jT;

    .line 540397
    invoke-static/range {p12 .. p12}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Rf;

    iput-object v1, p0, LX/3G4;->k:LX/0Rf;

    .line 540398
    move-object/from16 v0, p13

    iput-object v0, p0, LX/3G4;->l:LX/399;

    .line 540399
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;JJIILjava/lang/Class;LX/0w7;LX/0jT;LX/0Rf;LX/399;B)V
    .locals 1

    .prologue
    .line 540400
    invoke-direct/range {p0 .. p13}, LX/3G4;-><init>(Ljava/lang/String;Ljava/lang/String;JJIILjava/lang/Class;LX/0w7;LX/0jT;LX/0Rf;LX/399;)V

    return-void
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 540401
    iget-object v0, p0, LX/3G4;->h:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/399;
    .locals 3

    .prologue
    .line 540402
    iget-object v0, p0, LX/3G4;->l:LX/399;

    if-eqz v0, :cond_0

    .line 540403
    iget-object v0, p0, LX/3G4;->l:LX/399;

    .line 540404
    :goto_0
    return-object v0

    .line 540405
    :cond_0
    iget-object v0, p0, LX/3G4;->h:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zP;

    .line 540406
    iget-object v1, p0, LX/3G4;->i:LX/0w7;

    .line 540407
    iput-object v1, v0, LX/0gW;->e:LX/0w7;

    .line 540408
    new-instance v1, LX/399;

    iget-object v2, p0, LX/3G4;->k:LX/0Rf;

    invoke-direct {v1, v0, v2}, LX/399;-><init>(LX/0zP;LX/0Rf;)V

    iget-object v0, p0, LX/3G4;->j:LX/0jT;

    invoke-virtual {v1, v0}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()LX/3G4;
    .locals 6

    .prologue
    .line 540409
    new-instance v0, LX/3G1;

    invoke-direct {v0}, LX/3G1;-><init>()V

    .line 540410
    iget-object v3, p0, LX/3G3;->b:Ljava/lang/String;

    .line 540411
    iput-object v3, v0, LX/3G2;->a:Ljava/lang/String;

    .line 540412
    iget-object v3, p0, LX/3G3;->c:Ljava/lang/String;

    .line 540413
    iput-object v3, v0, LX/3G2;->b:Ljava/lang/String;

    .line 540414
    iget-wide v3, p0, LX/3G3;->e:J

    .line 540415
    iput-wide v3, v0, LX/3G2;->d:J

    .line 540416
    iget-wide v3, p0, LX/3G3;->d:J

    .line 540417
    iput-wide v3, v0, LX/3G2;->c:J

    .line 540418
    iget v3, p0, LX/3G3;->f:I

    .line 540419
    iput v3, v0, LX/3G2;->e:I

    .line 540420
    iget v3, p0, LX/3G3;->g:I

    .line 540421
    iput v3, v0, LX/3G2;->f:I

    .line 540422
    iget-object v2, p0, LX/3G4;->h:Ljava/lang/Class;

    .line 540423
    iput-object v2, v0, LX/3G1;->g:Ljava/lang/Class;

    .line 540424
    iget-object v2, p0, LX/3G4;->i:LX/0w7;

    .line 540425
    iput-object v2, v0, LX/3G1;->h:LX/0w7;

    .line 540426
    iget-object v2, p0, LX/3G4;->j:LX/0jT;

    invoke-virtual {v0, v2}, LX/3G1;->a(LX/0jT;)LX/3G1;

    .line 540427
    iget-object v2, p0, LX/3G4;->k:LX/0Rf;

    .line 540428
    iput-object v2, v0, LX/3G1;->k:LX/0Rf;

    .line 540429
    move-object v0, v0

    .line 540430
    iget v1, p0, LX/3G3;->f:I

    add-int/lit8 v1, v1, 0x1

    .line 540431
    iput v1, v0, LX/3G2;->e:I

    .line 540432
    move-object v0, v0

    .line 540433
    invoke-virtual {v0}, LX/3G2;->a()LX/3G3;

    move-result-object v0

    check-cast v0, LX/3G4;

    return-object v0
.end method
