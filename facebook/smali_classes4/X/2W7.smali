.class public final LX/2W7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2W8;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:[LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/0Ot",
            "<+",
            "LX/0Up;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2Cn;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2u7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Ct;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Cw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2E0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2EL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2EY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Ec;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2FJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1rz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2FZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Fi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/28x;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2GR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Bz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2C1;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Hk;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/29A;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2IH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 418544
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418545
    const/4 v0, -0x1

    iput v0, p0, LX/2W7;->b:I

    .line 418546
    const/16 v0, 0x14

    new-array v0, v0, [LX/0Ot;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    aput-object p4, v0, v1

    const/4 v1, 0x4

    aput-object p5, v0, v1

    const/4 v1, 0x5

    aput-object p6, v0, v1

    const/4 v1, 0x6

    aput-object p7, v0, v1

    const/4 v1, 0x7

    aput-object p8, v0, v1

    const/16 v1, 0x8

    aput-object p9, v0, v1

    const/16 v1, 0x9

    aput-object p10, v0, v1

    const/16 v1, 0xa

    aput-object p11, v0, v1

    const/16 v1, 0xb

    aput-object p12, v0, v1

    const/16 v1, 0xc

    aput-object p13, v0, v1

    const/16 v1, 0xd

    aput-object p14, v0, v1

    const/16 v1, 0xe

    aput-object p15, v0, v1

    const/16 v1, 0xf

    aput-object p16, v0, v1

    const/16 v1, 0x10

    aput-object p17, v0, v1

    const/16 v1, 0x11

    aput-object p18, v0, v1

    const/16 v1, 0x12

    aput-object p19, v0, v1

    const/16 v1, 0x13

    aput-object p20, v0, v1

    iput-object v0, p0, LX/2W7;->a:[LX/0Ot;

    .line 418547
    return-void
.end method

.method public static a(LX/0QB;)LX/2W7;
    .locals 3

    .prologue
    .line 418548
    const-class v1, LX/2W7;

    monitor-enter v1

    .line 418549
    :try_start_0
    sget-object v0, LX/2W7;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 418550
    sput-object v2, LX/2W7;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 418551
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418552
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/2W7;->b(LX/0QB;)LX/2W7;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 418553
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2W7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418554
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 418555
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/2W7;
    .locals 23

    .prologue
    .line 418556
    new-instance v2, LX/2W7;

    const/16 v3, 0xe5

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x119

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1a5

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1a8

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x205

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2c9

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2ca

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xab0

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xb40

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xb57

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xc82

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0xcf2

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0xd91

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0xeb5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0xf9b

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0xfe6

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x1010

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x1017

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x1020

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x11da

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    invoke-direct/range {v2 .. v22}, LX/2W7;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 418557
    return-object v2
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 418558
    iget v0, p0, LX/2W7;->b:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/2W7;->a:[LX/0Ot;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 418559
    invoke-virtual {p0}, LX/2W7;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 418560
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 418561
    :cond_0
    iget v0, p0, LX/2W7;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/2W7;->b:I

    .line 418562
    iget-object v0, p0, LX/2W7;->a:[LX/0Ot;

    iget v1, p0, LX/2W7;->b:I

    aget-object v0, v0, v1

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Up;

    return-object v0
.end method

.method public final remove()V
    .locals 3

    .prologue
    .line 418563
    iget-object v0, p0, LX/2W7;->a:[LX/0Ot;

    iget v1, p0, LX/2W7;->b:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 418564
    return-void
.end method
