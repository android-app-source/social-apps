.class public LX/2Fj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/2Fk;

.field public final c:Landroid/content/pm/PackageManager;

.field public final d:Ljava/lang/String;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/2Fk;Landroid/content/Context;Landroid/content/pm/PackageManager;LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387224
    iput-object p1, p0, LX/2Fj;->b:LX/2Fk;

    .line 387225
    iput-object p3, p0, LX/2Fj;->c:Landroid/content/pm/PackageManager;

    .line 387226
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2Fj;->d:Ljava/lang/String;

    .line 387227
    iput-object p4, p0, LX/2Fj;->a:LX/0Zb;

    .line 387228
    iput-object p5, p0, LX/2Fj;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 387229
    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/lang/Boolean;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/Signature;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    .line 387230
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/Signature;

    .line 387231
    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v2

    invoke-static {v2}, LX/03l;->a([B)Ljava/lang/String;

    move-result-object v2

    .line 387232
    const-string p0, "ijxLJi1yGs1JpL-X1SExmchvork"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    const-string p0, "pLdFLi7Y9fGRBYynu_0msNMhS_w"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    const-string p0, "Xo8WBi6jzSxKDVR4drqm84yr9iU"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 387233
    if-eqz v0, :cond_0

    .line 387234
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 387235
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 6

    .prologue
    .line 387236
    iget-object v0, p0, LX/2Fj;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->x:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 387237
    :cond_0
    :goto_0
    return-void

    .line 387238
    :cond_1
    iget-object v0, p0, LX/2Fj;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/26p;->x:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 387239
    iget-object v0, p0, LX/2Fj;->d:Ljava/lang/String;

    .line 387240
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 387241
    :try_start_0
    iget-object v2, p0, LX/2Fj;->c:Landroid/content/pm/PackageManager;

    const/16 v3, 0x40

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 387242
    iget-object v3, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 387243
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 387244
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 387245
    :catch_0
    :cond_2
    move-object v0, v1

    .line 387246
    invoke-static {v0}, LX/2Fj;->a(Ljava/util/List;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 387247
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "fb_sign_verification"

    invoke-direct {v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 387248
    const-string v1, "package_name"

    iget-object v3, p0, LX/2Fj;->d:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387249
    const-string v1, "installer"

    iget-object v3, p0, LX/2Fj;->c:Landroid/content/pm/PackageManager;

    iget-object v4, p0, LX/2Fj;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387250
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 387251
    const-string v3, "signature"

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/Signature;

    invoke-virtual {v1}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387252
    :cond_3
    const-string v1, "num_signatures"

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 387253
    iget-object v1, p0, LX/2Fj;->a:LX/0Zb;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 387254
    goto :goto_0
.end method
