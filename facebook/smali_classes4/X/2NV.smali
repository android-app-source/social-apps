.class public final enum LX/2NV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2NV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2NV;

.field public static final enum LOGIN_APPROVALS_CODE_ENTRY:LX/2NV;

.field public static final enum PASSWORD_ENTRY:LX/2NV;

.field public static final enum TRANSIENT_AUTH_TOKEN_ENTRY:LX/2NV;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 399866
    new-instance v0, LX/2NV;

    const-string v1, "PASSWORD_ENTRY"

    invoke-direct {v0, v1, v2}, LX/2NV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2NV;->PASSWORD_ENTRY:LX/2NV;

    .line 399867
    new-instance v0, LX/2NV;

    const-string v1, "LOGIN_APPROVALS_CODE_ENTRY"

    invoke-direct {v0, v1, v3}, LX/2NV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2NV;->LOGIN_APPROVALS_CODE_ENTRY:LX/2NV;

    .line 399868
    new-instance v0, LX/2NV;

    const-string v1, "TRANSIENT_AUTH_TOKEN_ENTRY"

    invoke-direct {v0, v1, v4}, LX/2NV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2NV;->TRANSIENT_AUTH_TOKEN_ENTRY:LX/2NV;

    .line 399869
    const/4 v0, 0x3

    new-array v0, v0, [LX/2NV;

    sget-object v1, LX/2NV;->PASSWORD_ENTRY:LX/2NV;

    aput-object v1, v0, v2

    sget-object v1, LX/2NV;->LOGIN_APPROVALS_CODE_ENTRY:LX/2NV;

    aput-object v1, v0, v3

    sget-object v1, LX/2NV;->TRANSIENT_AUTH_TOKEN_ENTRY:LX/2NV;

    aput-object v1, v0, v4

    sput-object v0, LX/2NV;->$VALUES:[LX/2NV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 399870
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2NV;
    .locals 1

    .prologue
    .line 399871
    const-class v0, LX/2NV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2NV;

    return-object v0
.end method

.method public static values()[LX/2NV;
    .locals 1

    .prologue
    .line 399872
    sget-object v0, LX/2NV;->$VALUES:[LX/2NV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2NV;

    return-object v0
.end method
