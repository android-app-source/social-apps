.class public final LX/2X6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/27t;


# direct methods
.method public constructor <init>(LX/27t;)V
    .locals 0

    .prologue
    .line 419481
    iput-object p1, p0, LX/2X6;->a:LX/27t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0dI;)V
    .locals 8

    .prologue
    .line 419482
    iget-object v0, p0, LX/2X6;->a:LX/27t;

    iget-object v0, v0, LX/27t;->b:LX/0dC;

    invoke-virtual {v0}, LX/0dC;->b()LX/0dI;

    move-result-object v1

    .line 419483
    iget-wide v6, v1, LX/0dI;->b:J

    move-wide v2, v6

    .line 419484
    iget-wide v6, p1, LX/0dI;->b:J

    move-wide v4, v6

    .line 419485
    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 419486
    sget-object v0, LX/27t;->a:Ljava/lang/Class;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "device id Changed from: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 419487
    iget-object v0, p0, LX/2X6;->a:LX/27t;

    iget-object v0, v0, LX/27t;->b:LX/0dC;

    invoke-virtual {v0, p1}, LX/0dC;->a(LX/0dI;)V

    .line 419488
    iget-object v0, p0, LX/2X6;->a:LX/27t;

    iget-object v0, v0, LX/27t;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/49e;

    .line 419489
    sget-object v3, LX/49d;->FB_SYNC:LX/49d;

    const/4 v4, 0x0

    invoke-interface {v0, v1, p1, v3, v4}, LX/49e;->a(LX/0dI;LX/0dI;LX/49d;Ljava/lang/String;)V

    goto :goto_0

    .line 419490
    :cond_0
    return-void
.end method
