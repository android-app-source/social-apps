.class public LX/2Ty;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 414919
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 414920
    return-void
.end method

.method public static a()LX/2U0;
    .locals 4
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 414918
    new-instance v0, LX/2U0;

    const/4 v1, 0x1

    const-wide/32 v2, 0x2932e00

    invoke-direct {v0, v1, v2, v3}, LX/2U0;-><init>(ZJ)V

    return-object v0
.end method

.method public static a(LX/GJ0;LX/GN1;LX/0ad;)LX/GEf;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostEvent;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 414917
    new-instance v0, LX/GEf;

    invoke-virtual {p0, p1}, LX/GJ0;->a(LX/GN0;)LX/GIz;

    move-result-object v1

    invoke-direct {v0, v1, p2}, LX/GEf;-><init>(LX/GIz;LX/0ad;)V

    return-object v0
.end method

.method public static a(LX/GJ0;LX/GNC;LX/0ad;)LX/GEf;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostPostBoostComponent;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 414916
    new-instance v0, LX/GEf;

    invoke-virtual {p0, p1}, LX/GJ0;->a(LX/GN0;)LX/GIz;

    move-result-object v1

    invoke-direct {v0, v1, p2}, LX/GEf;-><init>(LX/GIz;LX/0ad;)V

    return-object v0
.end method

.method public static a(LX/GKc;LX/GKU;LX/GKY;LX/GN1;LX/0ad;)LX/GEw;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostEvent;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 414915
    new-instance v0, LX/GEw;

    invoke-static {p3}, LX/GKU;->a(LX/GN0;)LX/GKT;

    move-result-object v1

    invoke-virtual {p2, p3}, LX/GKY;->a(LX/GN0;)LX/GKX;

    move-result-object v2

    invoke-static {p3, v1, v2}, LX/GKc;->a(LX/GN0;LX/GKT;LX/GKX;)LX/GKb;

    move-result-object v1

    invoke-direct {v0, v1, p4}, LX/GEw;-><init>(LX/GKb;LX/0ad;)V

    return-object v0
.end method

.method public static a(LX/GKc;LX/GKU;LX/GKY;LX/GNC;LX/0ad;)LX/GEw;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostPostBoostComponent;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 414921
    new-instance v0, LX/GEw;

    invoke-static {p3}, LX/GKU;->a(LX/GN0;)LX/GKT;

    move-result-object v1

    invoke-virtual {p2, p3}, LX/GKY;->a(LX/GN0;)LX/GKX;

    move-result-object v2

    invoke-static {p3, v1, v2}, LX/GKc;->a(LX/GN0;LX/GKT;LX/GKX;)LX/GKb;

    move-result-object v1

    invoke-direct {v0, v1, p4}, LX/GEw;-><init>(LX/GKb;LX/0ad;)V

    return-object v0
.end method

.method public static a(LX/GMI;LX/GN1;LX/0ad;)LX/GEx;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostEvent;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 414914
    new-instance v0, LX/GEx;

    invoke-virtual {p0, p1}, LX/GMI;->a(LX/GN0;)LX/GMH;

    move-result-object v1

    invoke-direct {v0, v1, p2}, LX/GEx;-><init>(LX/GMH;LX/0ad;)V

    return-object v0
.end method

.method public static a(LX/GMI;LX/GNC;LX/0ad;)LX/GEx;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostPostBoostComponent;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 414913
    new-instance v0, LX/GEx;

    invoke-virtual {p0, p1}, LX/GMI;->a(LX/GN0;)LX/GMH;

    move-result-object v1

    invoke-direct {v0, v1, p2}, LX/GEx;-><init>(LX/GMH;LX/0ad;)V

    return-object v0
.end method

.method public static a(LX/GMh;LX/GKR;)LX/GKQ;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostStory;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 414912
    invoke-virtual {p1, p0}, LX/GKR;->a(LX/GKO;)LX/GKQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/adinterfaces/ui/preview/AdInterfacesBoostedComponentPreviewFetcher;LX/GKR;)LX/GKQ;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostedComponent;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 414911
    invoke-virtual {p1, p0}, LX/GKR;->a(LX/GKO;)LX/GKQ;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/GKc;LX/GKU;LX/GKY;LX/GNC;LX/0ad;)LX/GEw;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostedComponent;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 414910
    new-instance v0, LX/GEw;

    invoke-static {p3}, LX/GKU;->a(LX/GN0;)LX/GKT;

    move-result-object v1

    invoke-virtual {p2, p3}, LX/GKY;->a(LX/GN0;)LX/GKX;

    move-result-object v2

    invoke-static {p3, v1, v2}, LX/GKc;->a(LX/GN0;LX/GKT;LX/GKX;)LX/GKb;

    move-result-object v1

    invoke-direct {v0, v1, p4}, LX/GEw;-><init>(LX/GKb;LX/0ad;)V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 414909
    return-void
.end method
