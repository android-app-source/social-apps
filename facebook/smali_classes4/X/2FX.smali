.class public LX/2FX;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/GRF;",
        "LX/GRb;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2FX;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387018
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 387019
    return-void
.end method

.method public static a(LX/0QB;)LX/2FX;
    .locals 3

    .prologue
    .line 387020
    sget-object v0, LX/2FX;->a:LX/2FX;

    if-nez v0, :cond_1

    .line 387021
    const-class v1, LX/2FX;

    monitor-enter v1

    .line 387022
    :try_start_0
    sget-object v0, LX/2FX;->a:LX/2FX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387023
    if-eqz v2, :cond_0

    .line 387024
    :try_start_1
    new-instance v0, LX/2FX;

    invoke-direct {v0}, LX/2FX;-><init>()V

    .line 387025
    move-object v0, v0

    .line 387026
    sput-object v0, LX/2FX;->a:LX/2FX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387027
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387028
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387029
    :cond_1
    sget-object v0, LX/2FX;->a:LX/2FX;

    return-object v0

    .line 387030
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387031
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
