.class public final LX/3FW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/392;",
        ":",
        "LX/3FP;",
        ":",
        "LX/3FT;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field public final a:LX/3FN;

.field private final b:LX/198;

.field private final c:LX/094;

.field private final d:LX/03V;

.field private final e:LX/2ms;

.field private final f:LX/2mr;


# direct methods
.method public constructor <init>(LX/3FN;LX/198;LX/094;LX/03V;LX/2ms;LX/2mr;)V
    .locals 0

    .prologue
    .line 539720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 539721
    iput-object p1, p0, LX/3FW;->a:LX/3FN;

    .line 539722
    iput-object p2, p0, LX/3FW;->b:LX/198;

    .line 539723
    iput-object p3, p0, LX/3FW;->c:LX/094;

    .line 539724
    iput-object p4, p0, LX/3FW;->d:LX/03V;

    .line 539725
    iput-object p5, p0, LX/3FW;->e:LX/2ms;

    .line 539726
    iput-object p6, p0, LX/3FW;->f:LX/2mr;

    .line 539727
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x2

    const/4 v3, 0x1

    const v0, 0xce893c8

    invoke-static {v1, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v5

    .line 539660
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-boolean v0, v0, LX/3FN;->r:Z

    if-eqz v0, :cond_0

    .line 539661
    const v0, 0x5abd9fd

    invoke-static {v1, v1, v0, v5}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 539662
    :goto_0
    return-void

    .line 539663
    :cond_0
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 539664
    :cond_1
    const v0, -0x7a0b802c

    invoke-static {v0, v5}, LX/02F;->a(II)V

    goto :goto_0

    .line 539665
    :cond_2
    instance-of v0, p1, LX/392;

    if-eqz v0, :cond_3

    instance-of v0, p1, LX/3FP;

    if-nez v0, :cond_4

    .line 539666
    :cond_3
    iget-object v0, p0, LX/3FW;->d:LX/03V;

    const-class v1, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Wrong view type: view should implement VideoAttachmentView and CanShowLiveCommentDialogFragment"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 539667
    const v0, 0x7bef313c

    invoke-static {v0, v5}, LX/02F;->a(II)V

    goto :goto_0

    :cond_4
    move-object v0, p1

    .line 539668
    check-cast v0, LX/3FT;

    invoke-interface {v0}, LX/3FT;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v6

    .line 539669
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iput-boolean v3, v0, LX/3FN;->r:Z

    .line 539670
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->j:LX/2oL;

    invoke-virtual {v0}, LX/2oL;->a()I

    move-result v0

    .line 539671
    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 539672
    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    .line 539673
    :cond_5
    iget-object v1, p0, LX/3FW;->a:LX/3FN;

    iget-object v1, v1, LX/3FN;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->ay()I

    move-result v1

    if-ne v0, v1, :cond_6

    .line 539674
    const/4 v0, 0x0

    .line 539675
    :cond_6
    move v2, v0

    .line 539676
    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v7

    .line 539677
    iget-object v0, p0, LX/3FW;->c:LX/094;

    iget-object v1, p0, LX/3FW;->a:LX/3FN;

    iget-object v1, v1, LX/3FN;->h:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/094;->a(Ljava/lang/String;)V

    .line 539678
    iget-object v0, p0, LX/3FW;->c:LX/094;

    iget-object v1, p0, LX/3FW;->a:LX/3FN;

    iget-object v1, v1, LX/3FN;->h:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v7}, LX/094;->a(Ljava/lang/String;Z)V

    .line 539679
    if-eqz v7, :cond_b

    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v0

    move v1, v0

    .line 539680
    :goto_1
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v8, p0, LX/3FW;->b:LX/198;

    sget-object v9, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    .line 539681
    iget-boolean v10, v0, LX/3FN;->q:Z

    if-eqz v10, :cond_7

    .line 539682
    const/4 v10, 0x0

    iput-boolean v10, v0, LX/3FN;->q:Z

    .line 539683
    invoke-static {}, LX/5Mr;->c()LX/5Mr;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/198;->b(LX/1YD;)V

    .line 539684
    :cond_7
    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v10

    if-lez v10, :cond_8

    .line 539685
    iget-object v10, v0, LX/3FN;->j:LX/2oL;

    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v11

    invoke-virtual {v10, v11}, LX/2oL;->a(I)V

    .line 539686
    :cond_8
    invoke-virtual {v6, v9}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 539687
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v8, LX/0f8;

    invoke-static {v0, v8}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f8;

    .line 539688
    if-nez v0, :cond_9

    .line 539689
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v8, Landroid/app/Activity;

    invoke-static {v0, v8}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 539690
    iget-object v8, p0, LX/3FW;->f:LX/2mr;

    .line 539691
    iput-object v0, v8, LX/2mr;->b:Landroid/app/Activity;

    .line 539692
    iget-object v0, p0, LX/3FW;->f:LX/2mr;

    .line 539693
    :cond_9
    iget-object v8, p0, LX/3FW;->a:LX/3FN;

    invoke-interface {v0}, LX/0f8;->i()LX/0hE;

    move-result-object v0

    iput-object v0, v8, LX/3FN;->s:LX/0hE;

    .line 539694
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->s:LX/0hE;

    iget-object v8, p0, LX/3FW;->a:LX/3FN;

    iget-object v8, v8, LX/3FN;->t:LX/3Iw;

    invoke-interface {v0, v8}, LX/0hE;->a(LX/394;)Ljava/lang/Object;

    .line 539695
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->s:LX/0hE;

    invoke-interface {v0, v3}, LX/0hE;->setAllowLooping(Z)V

    .line 539696
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v8, v0, LX/3FN;->s:LX/0hE;

    if-nez v7, :cond_c

    move v0, v3

    :goto_2
    invoke-interface {v8, v0}, LX/0hE;->setLogEnteringStartEvent(Z)V

    .line 539697
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->s:LX/0hE;

    invoke-interface {v0, v4}, LX/0hE;->setLogExitingPauseEvent(Z)V

    .line 539698
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v3, v0, LX/3FN;->o:LX/395;

    move-object v0, p1

    check-cast v0, LX/3FT;

    .line 539699
    iput-object v0, v3, LX/395;->h:LX/3FT;

    .line 539700
    const-class v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v6, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    .line 539701
    if-eqz v0, :cond_a

    .line 539702
    iget-object v3, p0, LX/3FW;->a:LX/3FN;

    iget-object v3, v3, LX/3FN;->o:LX/395;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->getViewportState()LX/7DJ;

    move-result-object v0

    .line 539703
    iput-object v0, v3, LX/395;->p:LX/7DJ;

    .line 539704
    :cond_a
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->i:LX/2oO;

    invoke-virtual {v0}, LX/2oO;->a()V

    .line 539705
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->o:LX/395;

    invoke-virtual {v0, v2}, LX/395;->a(I)LX/395;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/395;->b(I)LX/395;

    move-result-object v1

    move-object v0, p1

    check-cast v0, LX/3FP;

    invoke-interface {v0}, LX/3FP;->getAndClearShowLiveCommentDialogFragment()Z

    move-result v0

    .line 539706
    iput-boolean v0, v1, LX/395;->k:Z

    .line 539707
    if-eqz v7, :cond_d

    .line 539708
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->o:LX/395;

    sget-object v1, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    .line 539709
    iput-object v1, v0, LX/395;->j:LX/04g;

    .line 539710
    :goto_3
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->o:LX/395;

    const/4 v1, 0x0

    .line 539711
    iput-object v1, v0, LX/395;->g:LX/0P1;

    .line 539712
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->s:LX/0hE;

    iget-object v1, p0, LX/3FW;->a:LX/3FN;

    iget-object v1, v1, LX/3FN;->o:LX/395;

    invoke-interface {v0, v1}, LX/0hE;->a(LX/395;)V

    .line 539713
    iget-object v0, p0, LX/3FW;->e:LX/2ms;

    iget-object v1, p0, LX/3FW;->a:LX/3FN;

    iget-object v1, v1, LX/3FN;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/2ms;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 539714
    const v0, 0x391ff71a

    invoke-static {v0, v5}, LX/02F;->a(II)V

    goto/16 :goto_0

    :cond_b
    move v1, v2

    .line 539715
    goto/16 :goto_1

    :cond_c
    move v0, v4

    .line 539716
    goto :goto_2

    .line 539717
    :cond_d
    iget-object v0, p0, LX/3FW;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->o:LX/395;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    .line 539718
    iput-object v1, v0, LX/395;->j:LX/04g;

    .line 539719
    goto :goto_3
.end method
