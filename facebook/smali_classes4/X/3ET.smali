.class public final LX/3ET;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Dw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/3ET;


# instance fields
.field private final b:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 537622
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "enabled"

    sget v2, LX/3Dx;->dU:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "nbuckets"

    sget v2, LX/3Dx;->dV:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "_v"

    sget v2, LX/3Dx;->dT:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/3ET;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 537623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537624
    iput-object p1, p0, LX/3ET;->b:LX/0ad;

    .line 537625
    return-void
.end method

.method public static a(LX/0QB;)LX/3ET;
    .locals 4

    .prologue
    .line 537626
    sget-object v0, LX/3ET;->c:LX/3ET;

    if-nez v0, :cond_1

    .line 537627
    const-class v1, LX/3ET;

    monitor-enter v1

    .line 537628
    :try_start_0
    sget-object v0, LX/3ET;->c:LX/3ET;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 537629
    if-eqz v2, :cond_0

    .line 537630
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 537631
    new-instance p0, LX/3ET;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/3ET;-><init>(LX/0ad;)V

    .line 537632
    move-object v0, p0

    .line 537633
    sput-object v0, LX/3ET;->c:LX/3ET;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 537634
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 537635
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 537636
    :cond_1
    sget-object v0, LX/3ET;->c:LX/3ET;

    return-object v0

    .line 537637
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 537638
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 537639
    sget-object v0, LX/3ET;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 537640
    if-nez v0, :cond_0

    .line 537641
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 537642
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/3ET;->b:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v2, v3, v0, p2}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 537643
    const-string v0, "rtc_timeseries_log"

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 537644
    return-object p2
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 537645
    iget-object v0, p0, LX/3ET;->b:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget v2, LX/3Dx;->dU:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(LX/0c0;I)V

    .line 537646
    return-void
.end method
