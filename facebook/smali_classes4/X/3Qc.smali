.class public LX/3Qc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile r:LX/3Qc;


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/2Sc;

.field private final f:LX/3Qd;

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Bf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Bi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final i:LX/3Qe;

.field public final j:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final k:LX/0SG;

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Bc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final m:LX/0Sy;

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final o:LX/0WJ;

.field public p:LX/3Ql;

.field private q:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7By;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0Or;LX/2Sc;LX/3Qd;LX/3Qe;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0Sy;LX/0WJ;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/search/bootstrap/abtest/UsePeriodicPull;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/search/bootstrap/abtest/BootstrapNoRefresh;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Sc;",
            "LX/3Qd;",
            "LX/3Qe;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            "Lcom/facebook/common/userinteraction/UserInteractionController;",
            "LX/0WJ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 565516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565517
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 565518
    iput-object v0, p0, LX/3Qc;->g:LX/0Ot;

    .line 565519
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 565520
    iput-object v0, p0, LX/3Qc;->h:LX/0Ot;

    .line 565521
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 565522
    iput-object v0, p0, LX/3Qc;->l:LX/0Ot;

    .line 565523
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 565524
    iput-object v0, p0, LX/3Qc;->n:LX/0Ot;

    .line 565525
    iput-object p1, p0, LX/3Qc;->a:Ljava/util/concurrent/ExecutorService;

    .line 565526
    iput-object p2, p0, LX/3Qc;->b:Ljava/util/concurrent/ExecutorService;

    .line 565527
    iput-object p3, p0, LX/3Qc;->c:LX/0Or;

    .line 565528
    iput-object p4, p0, LX/3Qc;->d:LX/0Or;

    .line 565529
    iput-object p5, p0, LX/3Qc;->e:LX/2Sc;

    .line 565530
    iput-object p6, p0, LX/3Qc;->f:LX/3Qd;

    .line 565531
    iput-object p7, p0, LX/3Qc;->i:LX/3Qe;

    .line 565532
    iput-object p8, p0, LX/3Qc;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 565533
    iput-object p9, p0, LX/3Qc;->k:LX/0SG;

    .line 565534
    iput-object p10, p0, LX/3Qc;->m:LX/0Sy;

    .line 565535
    iput-object p11, p0, LX/3Qc;->o:LX/0WJ;

    .line 565536
    sget-object v0, LX/3Ql;->NO_ERROR:LX/3Ql;

    iput-object v0, p0, LX/3Qc;->p:LX/3Ql;

    .line 565537
    iget-object v0, p0, LX/3Qc;->i:LX/3Qe;

    invoke-virtual {p0}, LX/3Qc;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/3Qe;->a(Z)V

    .line 565538
    return-void
.end method

.method public static a(LX/0QB;)LX/3Qc;
    .locals 15

    .prologue
    .line 565496
    sget-object v0, LX/3Qc;->r:LX/3Qc;

    if-nez v0, :cond_1

    .line 565497
    const-class v1, LX/3Qc;

    monitor-enter v1

    .line 565498
    :try_start_0
    sget-object v0, LX/3Qc;->r:LX/3Qc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 565499
    if-eqz v2, :cond_0

    .line 565500
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 565501
    new-instance v3, LX/3Qc;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    const/16 v6, 0x364

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x156d

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v8

    check-cast v8, LX/2Sc;

    .line 565502
    new-instance v11, LX/3Qd;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-direct {v11, v9, v10}, LX/3Qd;-><init>(LX/0tX;LX/0Uh;)V

    .line 565503
    const/16 v9, 0x32b5

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x113f

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v12, 0x32ac

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    .line 565504
    iput-object v9, v11, LX/3Qd;->a:LX/0Ot;

    iput-object v10, v11, LX/3Qd;->c:LX/0Ot;

    iput-object v12, v11, LX/3Qd;->d:LX/0Ot;

    .line 565505
    move-object v9, v11

    .line 565506
    check-cast v9, LX/3Qd;

    invoke-static {v0}, LX/3Qe;->a(LX/0QB;)LX/3Qe;

    move-result-object v10

    check-cast v10, LX/3Qe;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    invoke-static {v0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v13

    check-cast v13, LX/0Sy;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v14

    check-cast v14, LX/0WJ;

    invoke-direct/range {v3 .. v14}, LX/3Qc;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0Or;LX/2Sc;LX/3Qd;LX/3Qe;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0Sy;LX/0WJ;)V

    .line 565507
    const/16 v4, 0x32bb

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x32bc

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x32b9

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1032

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    .line 565508
    iput-object v4, v3, LX/3Qc;->g:LX/0Ot;

    iput-object v5, v3, LX/3Qc;->h:LX/0Ot;

    iput-object v6, v3, LX/3Qc;->l:LX/0Ot;

    iput-object v7, v3, LX/3Qc;->n:LX/0Ot;

    .line 565509
    move-object v0, v3

    .line 565510
    sput-object v0, LX/3Qc;->r:LX/3Qc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565511
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 565512
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 565513
    :cond_1
    sget-object v0, LX/3Qc;->r:LX/3Qc;

    return-object v0

    .line 565514
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 565515
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/3Qc;I)V
    .locals 2

    .prologue
    .line 565494
    iget-object v0, p0, LX/3Qc;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3Qm;->h:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 565495
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/3Qc;J)V
    .locals 13

    .prologue
    .line 565465
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3Qc;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3Qc;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 565466
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 565467
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_2

    .line 565468
    :try_start_1
    iget-object v0, p0, LX/3Qc;->i:LX/3Qe;

    const-wide/16 v2, -0x1

    const-string v1, "time_to_load_bootstrap_entities"

    invoke-virtual {v0, v2, v3, v1}, LX/3Qe;->a(JLjava/lang/String;)V

    .line 565469
    :goto_1
    iget-object v0, p0, LX/3Qc;->f:LX/3Qd;

    .line 565470
    sget-object v5, LX/0Q7;->a:LX/0Px;

    move-object v5, v5

    .line 565471
    sget-object v6, Lcom/facebook/http/interfaces/RequestPriority;->DEFAULT_PRIORITY:Lcom/facebook/http/interfaces/RequestPriority;

    const/4 v12, 0x0

    .line 565472
    new-instance v8, LX/8bl;

    invoke-direct {v8}, LX/8bl;-><init>()V

    .line 565473
    const-string v9, "thumbnail_size"

    invoke-static {}, LX/0wB;->b()I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 565474
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 565475
    const-string v9, "enable_delta_update"

    iget-object v10, v0, LX/3Qd;->e:LX/0Uh;

    sget v11, LX/2SU;->a:I

    invoke-virtual {v10, v11}, LX/0Uh;->a(I)LX/03R;

    move-result-object v10

    invoke-virtual {v10, v12}, LX/03R;->asBoolean(Z)Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 565476
    const-string v9, "timestamp"

    const-wide/16 v10, 0x3e8

    div-long v10, p1, v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 565477
    :goto_2
    new-instance v9, LX/8bl;

    invoke-direct {v9}, LX/8bl;-><init>()V

    move-object v9, v9

    .line 565478
    invoke-static {v9}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v9

    sget-object v10, LX/0zS;->c:LX/0zS;

    invoke-virtual {v9, v10}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v9

    .line 565479
    iget-object v10, v8, LX/0gW;->e:LX/0w7;

    move-object v8, v10

    .line 565480
    invoke-virtual {v9, v8}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v9

    .line 565481
    iget-object v8, v0, LX/3Qd;->d:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/7B0;

    sget-object v10, LX/7Az;->BOOTSTRAP:LX/7Az;

    invoke-virtual {v8, v10, v9}, LX/7B0;->a(LX/7Az;LX/0zO;)V

    .line 565482
    iget-object v8, v0, LX/3Qd;->b:LX/0tX;

    invoke-virtual {v8, v9}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v8

    .line 565483
    new-instance v9, LX/8cW;

    invoke-direct {v9, v0}, LX/8cW;-><init>(LX/3Qd;)V

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object v10

    invoke-static {v8, v9, v10}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    move-object v5, v8

    .line 565484
    move-object v0, v5

    .line 565485
    iput-object v0, p0, LX/3Qc;->q:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565486
    :try_start_2
    new-instance v0, LX/8cZ;

    invoke-direct {v0, p0}, LX/8cZ;-><init>(LX/3Qc;)V

    .line 565487
    iget-object v1, p0, LX/3Qc;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v2, p0, LX/3Qc;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 565488
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 565489
    :cond_2
    :try_start_3
    iget-object v0, p0, LX/3Qc;->i:LX/3Qe;

    iget-object v1, p0, LX/3Qc;->k:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v2, p1

    const-string v1, "time_to_load_bootstrap_entities"

    invoke-virtual {v0, v2, v3, v1}, LX/3Qe;->a(JLjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 565490
    :catch_0
    move-exception v0

    .line 565491
    :try_start_4
    sget-object v1, LX/3Ql;->FETCH_DB_BOOTSTRAP_ENTITY_PRE_FAIL:LX/3Ql;

    iput-object v1, p0, LX/3Qc;->p:LX/3Ql;

    .line 565492
    iget-object v1, p0, LX/3Qc;->e:LX/2Sc;

    sget-object v2, LX/3Ql;->FETCH_DB_BOOTSTRAP_ENTITY_PRE_FAIL:LX/3Ql;

    invoke-virtual {v1, v2, v0}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 565493
    :cond_3
    const-string v9, "ids"

    invoke-virtual {v8, v9, v5}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v9

    const-string v10, "enable_delta_update"

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    goto :goto_2
.end method

.method public static a$redex0(LX/3Qc;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 565402
    iget-object v0, p0, LX/3Qc;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3Qm;->i:LX/0Tn;

    if-eqz p2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, LX/3Qc;->o(LX/3Qc;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 565403
    return-void
.end method

.method public static declared-synchronized h(LX/3Qc;)V
    .locals 1

    .prologue
    .line 565462
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/3Qc;->q:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565463
    monitor-exit p0

    return-void

    .line 565464
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static i$redex0(LX/3Qc;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 565457
    iget-object v0, p0, LX/3Qc;->o:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v1

    .line 565458
    const/4 v0, 0x0

    .line 565459
    if-eqz v1, :cond_0

    .line 565460
    invoke-virtual {v1}, Lcom/facebook/user/model/User;->m()Lcom/facebook/user/model/UserFbidIdentifier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/user/model/UserFbidIdentifier;->a()Ljava/lang/String;

    move-result-object v0

    .line 565461
    :cond_0
    return-object v0
.end method

.method public static j(LX/3Qc;)Z
    .locals 4

    .prologue
    .line 565539
    invoke-static {p0}, LX/3Qc;->i$redex0(LX/3Qc;)Ljava/lang/String;

    move-result-object v0

    .line 565540
    if-eqz v0, :cond_0

    .line 565541
    iget-object v1, p0, LX/3Qc;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Qm;->d:LX/0Tn;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 565542
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(LX/3Qc;)J
    .locals 4

    .prologue
    .line 565456
    iget-object v0, p0, LX/3Qc;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3Qm;->b:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static m(LX/3Qc;)Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 565449
    iget-object v0, p0, LX/3Qc;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Bc;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 565450
    const v0, -0x75b5737d

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 565451
    :try_start_0
    iget-object v0, p0, LX/3Qc;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Bi;

    sget-object v2, LX/7Bg;->a:LX/7Bh;

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, LX/2Iu;->a(LX/0To;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 565452
    const v2, 0xdce4802

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 565453
    if-eq v0, v4, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    .line 565454
    :catchall_0
    move-exception v0

    const v2, 0x176d4007

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 565455
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(LX/3Qc;)I
    .locals 3

    .prologue
    .line 565448
    iget-object v0, p0, LX/3Qc;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3Qm;->h:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method

.method public static o(LX/3Qc;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 565447
    iget-object v0, p0, LX/3Qc;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3Qm;->i:LX/0Tn;

    const-string v2, " "

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 565439
    invoke-virtual {p0}, LX/3Qc;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 565440
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, LX/3Qc;->a$redex0(LX/3Qc;J)V

    .line 565441
    :goto_0
    return-void

    .line 565442
    :cond_0
    invoke-static {p0}, LX/3Qc;->l(LX/3Qc;)J

    move-result-wide v2

    .line 565443
    iget-object v0, p0, LX/3Qc;->k:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 565444
    sub-long v4, v0, v2

    iget-object v0, p0, LX/3Qc;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v1, LX/100;->aW:I

    const/16 v6, 0x18

    invoke-interface {v0, v1, v6}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v6, 0x36ee80

    mul-long/2addr v0, v6

    cmp-long v0, v4, v0

    if-ltz v0, :cond_1

    .line 565445
    invoke-static {p0, v2, v3}, LX/3Qc;->a$redex0(LX/3Qc;J)V

    goto :goto_0

    .line 565446
    :cond_1
    iget-object v0, p0, LX/3Qc;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/search/bootstrap/sync/BootstrapEntitiesLoader$1;

    invoke-direct {v1, p0}, Lcom/facebook/search/bootstrap/sync/BootstrapEntitiesLoader$1;-><init>(LX/3Qc;)V

    const v2, -0x5e0f5313

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final declared-synchronized a(LX/0Px;LX/8ca;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "LX/8ca;",
            ")V"
        }
    .end annotation

    .prologue
    .line 565405
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3Qc;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3Qc;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 565406
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 565407
    :cond_1
    :try_start_1
    invoke-static {p0}, LX/3Qc;->n(LX/3Qc;)I

    move-result v0

    rsub-int/lit8 v1, v0, 0x14

    .line 565408
    if-lez v1, :cond_0

    .line 565409
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 565410
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 565411
    invoke-static {p0}, LX/3Qc;->o(LX/3Qc;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    move v5, v5

    .line 565412
    if-nez v5, :cond_4

    .line 565413
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 565414
    add-int/lit8 v0, v1, -0x1

    .line 565415
    if-eqz v0, :cond_2

    .line 565416
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 565417
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 565418
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 565419
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 565420
    invoke-virtual {p2}, LX/8ca;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 565421
    const-string v0, "-"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 565422
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 565423
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 565424
    const-string v0, " "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 565425
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 565426
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/3Qc;->f:LX/3Qd;

    .line 565427
    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    .line 565428
    new-instance v5, LX/8bm;

    invoke-direct {v5}, LX/8bm;-><init>()V

    move-object v5, v5

    .line 565429
    const-string v6, "ids"

    invoke-virtual {v5, v6, v3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/8bm;

    .line 565430
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    sget-object v6, LX/0zS;->c:LX/0zS;

    invoke-virtual {v5, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v6

    .line 565431
    iget-object v5, v0, LX/3Qd;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7B0;

    sget-object v7, LX/7Az;->BOOTSTRAP:LX/7Az;

    invoke-virtual {v5, v7, v6}, LX/7B0;->a(LX/7Az;LX/0zO;)V

    .line 565432
    iget-object v5, v0, LX/3Qd;->b:LX/0tX;

    invoke-virtual {v5, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    .line 565433
    new-instance v6, LX/8cV;

    invoke-direct {v6, v0}, LX/8cV;-><init>(LX/3Qd;)V

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object v7

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v4, v5

    .line 565434
    move-object v0, v4

    .line 565435
    iput-object v0, p0, LX/3Qc;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 565436
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 565437
    new-instance v1, LX/8cY;

    invoke-direct {v1, p0, v0, p2, v2}, LX/8cY;-><init>(LX/3Qc;Ljava/lang/String;LX/8ca;Ljava/lang/StringBuffer;)V

    .line 565438
    iget-object v0, p0, LX/3Qc;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v2, p0, LX/3Qc;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto/16 :goto_2
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 565404
    invoke-static {p0}, LX/3Qc;->j(LX/3Qc;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/3Qc;->l(LX/3Qc;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
