.class public LX/2l7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2l7;


# instance fields
.field public final a:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 457183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457184
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, LX/2l7;->a:Landroid/content/ContentResolver;

    .line 457185
    return-void
.end method

.method public static a(LX/0QB;)LX/2l7;
    .locals 4

    .prologue
    .line 457170
    sget-object v0, LX/2l7;->b:LX/2l7;

    if-nez v0, :cond_1

    .line 457171
    const-class v1, LX/2l7;

    monitor-enter v1

    .line 457172
    :try_start_0
    sget-object v0, LX/2l7;->b:LX/2l7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 457173
    if-eqz v2, :cond_0

    .line 457174
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 457175
    new-instance p0, LX/2l7;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-direct {p0, v3}, LX/2l7;-><init>(Landroid/content/ContentResolver;)V

    .line 457176
    move-object v0, p0

    .line 457177
    sput-object v0, LX/2l7;->b:LX/2l7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457178
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 457179
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 457180
    :cond_1
    sget-object v0, LX/2l7;->b:LX/2l7;

    return-object v0

    .line 457181
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 457182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/bookmark/model/Bookmark;Lcom/facebook/bookmark/model/BookmarksGroup;ZI)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 457125
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 457126
    sget-object v0, LX/2md;->b:LX/0U1;

    .line 457127
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 457128
    iget-wide v2, p0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 457129
    sget-object v0, LX/2md;->c:LX/0U1;

    .line 457130
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 457131
    iget-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457132
    sget-object v0, LX/2md;->e:LX/0U1;

    .line 457133
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 457134
    iget-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->pic:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457135
    sget-object v0, LX/2md;->f:LX/0U1;

    .line 457136
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 457137
    iget-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457138
    sget-object v0, LX/2md;->d:LX/0U1;

    .line 457139
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 457140
    iget-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457141
    sget-object v0, LX/2md;->i:LX/0U1;

    .line 457142
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 457143
    iget-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->clientToken:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457144
    sget-object v0, LX/2md;->g:LX/0U1;

    .line 457145
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 457146
    invoke-virtual {p0}, Lcom/facebook/bookmark/model/Bookmark;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 457147
    sget-object v0, LX/2md;->h:LX/0U1;

    .line 457148
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 457149
    invoke-virtual {p0}, Lcom/facebook/bookmark/model/Bookmark;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457150
    sget-object v0, LX/2md;->k:LX/0U1;

    .line 457151
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 457152
    iget-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->layout:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457153
    sget-object v0, LX/2md;->l:LX/0U1;

    .line 457154
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 457155
    iget-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->subName:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457156
    sget-object v0, LX/2md;->m:LX/0U1;

    .line 457157
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 457158
    iget-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->miniAppIcon:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457159
    sget-object v0, LX/2md;->j:LX/0U1;

    .line 457160
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 457161
    iget-object v2, p1, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457162
    sget-object v0, LX/2md;->n:LX/0U1;

    .line 457163
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 457164
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 457165
    sget-object v0, LX/2md;->o:LX/0U1;

    .line 457166
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 457167
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 457168
    return-object v1

    .line 457169
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/database/Cursor;)Lcom/facebook/bookmark/model/Bookmark;
    .locals 14

    .prologue
    .line 457088
    sget-object v0, LX/2md;->b:LX/0U1;

    invoke-virtual {v0, p0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 457089
    sget-object v1, LX/2md;->c:LX/0U1;

    invoke-virtual {v1, p0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    .line 457090
    sget-object v2, LX/2md;->d:LX/0U1;

    invoke-virtual {v2, p0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    .line 457091
    sget-object v2, LX/2md;->e:LX/0U1;

    invoke-virtual {v2, p0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v7

    .line 457092
    sget-object v2, LX/2md;->f:LX/0U1;

    invoke-virtual {v2, p0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v8

    .line 457093
    sget-object v2, LX/2md;->i:LX/0U1;

    invoke-virtual {v2, p0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v9

    .line 457094
    sget-object v2, LX/2md;->g:LX/0U1;

    invoke-virtual {v2, p0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v6

    .line 457095
    sget-object v2, LX/2md;->h:LX/0U1;

    invoke-virtual {v2, p0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v10

    .line 457096
    sget-object v2, LX/2md;->k:LX/0U1;

    invoke-virtual {v2, p0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v11

    .line 457097
    sget-object v2, LX/2md;->l:LX/0U1;

    invoke-virtual {v2, p0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v12

    .line 457098
    sget-object v2, LX/2md;->m:LX/0U1;

    invoke-virtual {v2, p0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v13

    .line 457099
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 457100
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 457101
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 457102
    invoke-interface {p0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 457103
    new-instance v1, LX/2lK;

    invoke-direct/range {v1 .. v6}, LX/2lK;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    invoke-interface {p0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 457104
    iput-object v0, v1, LX/2lK;->e:Ljava/lang/String;

    .line 457105
    move-object v0, v1

    .line 457106
    invoke-interface {p0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 457107
    iput-object v1, v0, LX/2lK;->f:Ljava/lang/String;

    .line 457108
    move-object v0, v0

    .line 457109
    invoke-interface {p0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 457110
    iput-object v1, v0, LX/2lK;->g:Ljava/lang/String;

    .line 457111
    move-object v0, v0

    .line 457112
    invoke-interface {p0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 457113
    iput-object v1, v0, LX/2lK;->h:Ljava/lang/String;

    .line 457114
    move-object v0, v0

    .line 457115
    invoke-interface {p0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 457116
    iput-object v1, v0, LX/2lK;->i:Ljava/lang/String;

    .line 457117
    move-object v0, v0

    .line 457118
    invoke-interface {p0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 457119
    iput-object v1, v0, LX/2lK;->j:Ljava/lang/String;

    .line 457120
    move-object v0, v0

    .line 457121
    invoke-interface {p0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 457122
    iput-object v1, v0, LX/2lK;->k:Ljava/lang/String;

    .line 457123
    move-object v0, v0

    .line 457124
    invoke-virtual {v0}, LX/2lK;->a()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2l7;LX/0Xu;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 457065
    :try_start_0
    iget-object v0, p0, LX/2l7;->a:Landroid/content/ContentResolver;

    sget-object v1, LX/EhG;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v6

    .line 457066
    if-nez v6, :cond_1

    .line 457067
    :try_start_1
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not query "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/EhG;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457068
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_0

    .line 457069
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 457070
    :cond_1
    :try_start_2
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v7

    .line 457071
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 457072
    sget-object v0, LX/2mf;->a:LX/0U1;

    invoke-virtual {v0, v6}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v9

    .line 457073
    sget-object v0, LX/2mf;->c:LX/0U1;

    invoke-virtual {v0, v6}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v10

    .line 457074
    sget-object v0, LX/2mf;->b:LX/0U1;

    invoke-virtual {v0, v6}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v11

    .line 457075
    sget-object v0, LX/2mf;->d:LX/0U1;

    invoke-virtual {v0, v6}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v12

    .line 457076
    sget-object v0, LX/2mf;->e:LX/0U1;

    invoke-virtual {v0, v6}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v13

    .line 457077
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 457078
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 457079
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 457080
    invoke-interface {v6, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 457081
    invoke-interface {v6, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 457082
    invoke-interface {v7, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457083
    new-instance v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-interface {p1, v5}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/bookmark/model/BookmarksGroup;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/List;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 457084
    :cond_2
    new-instance v0, LX/EhB;

    invoke-direct {v0, p0, v7}, LX/EhB;-><init>(LX/2l7;Ljava/util/Map;)V

    invoke-static {v8, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 457085
    if-eqz v6, :cond_3

    .line 457086
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    return-object v8

    .line 457087
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_0
.end method

.method private static a(LX/2l7;J)Z
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 457186
    :try_start_0
    iget-object v0, p0, LX/2l7;->a:Landroid/content/ContentResolver;

    sget-object v1, LX/EhH;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/2md;->b:LX/0U1;

    .line 457187
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 457188
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 457189
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-lez v0, :cond_1

    .line 457190
    if-eqz v1, :cond_0

    .line 457191
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    move v0, v6

    :goto_0
    return v0

    .line 457192
    :cond_1
    if-eqz v1, :cond_2

    .line 457193
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    move v0, v7

    goto :goto_0

    .line 457194
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    if-eqz v1, :cond_3

    .line 457195
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 457196
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public static d(LX/2l7;)LX/0Xu;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Xu",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 457048
    sget-object v0, LX/2md;->o:LX/0U1;

    .line 457049
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 457050
    const-string v1, "1"

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 457051
    :try_start_0
    iget-object v0, p0, LX/2l7;->a:Landroid/content/ContentResolver;

    sget-object v1, LX/EhH;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/2md;->n:LX/0U1;

    .line 457052
    iget-object p0, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, p0

    .line 457053
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 457054
    if-nez v1, :cond_1

    .line 457055
    :try_start_1
    new-instance v0, Landroid/database/SQLException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can not query "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/EhH;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457056
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v1, :cond_0

    .line 457057
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 457058
    :cond_1
    :try_start_2
    sget-object v0, LX/2md;->j:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 457059
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v2

    .line 457060
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 457061
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, LX/2l7;->a(Landroid/database/Cursor;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 457062
    :cond_2
    if-eqz v1, :cond_3

    .line 457063
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    return-object v2

    .line 457064
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 457034
    :try_start_0
    iget-object v0, p0, LX/2l7;->a:Landroid/content/ContentResolver;

    sget-object v1, LX/2mG;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, LX/2mV;->b:LX/0U1;

    .line 457035
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 457036
    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 457037
    if-eqz v2, :cond_1

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 457038
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v0

    .line 457039
    if-eqz v2, :cond_0

    .line 457040
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 457041
    :cond_0
    :goto_0
    return-wide v0

    .line 457042
    :cond_1
    if-eqz v2, :cond_2

    .line 457043
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 457044
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 457045
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_3

    .line 457046
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 457047
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Ljava/util/Collection;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 457015
    const/4 v0, 0x2

    new-array v0, v0, [LX/0ux;

    const/4 v1, 0x0

    sget-object v2, LX/2md;->j:LX/0U1;

    .line 457016
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 457017
    invoke-static {v2, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/2md;->o:LX/0U1;

    .line 457018
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 457019
    const-string v3, "0"

    invoke-static {v2, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v4

    .line 457020
    :try_start_0
    iget-object v0, p0, LX/2l7;->a:Landroid/content/ContentResolver;

    sget-object v1, LX/EhH;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/2md;->n:LX/0U1;

    .line 457021
    iget-object p0, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, p0

    .line 457022
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 457023
    if-nez v1, :cond_1

    .line 457024
    :try_start_1
    new-instance v0, Landroid/database/SQLException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can not query "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/EhH;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457025
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v1, :cond_0

    .line 457026
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 457027
    :cond_1
    :try_start_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 457028
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 457029
    invoke-static {v1}, LX/2l7;->a(Landroid/database/Cursor;)Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 457030
    :cond_2
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 457031
    if-eqz v1, :cond_3

    .line 457032
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    return-object v0

    .line 457033
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_0
.end method

.method public final a(Ljava/util/List;J)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;J)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 456985
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 456986
    sget-object v1, LX/EhH;->a:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456987
    sget-object v1, LX/EhG;->a:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456988
    sget-object v1, LX/2mG;->a:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    sget-object v2, LX/2mV;->a:LX/0U1;

    .line 456989
    iget-object v4, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v4

    .line 456990
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    sget-object v2, LX/2mV;->b:LX/0U1;

    .line 456991
    iget-object v4, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v4

    .line 456992
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456993
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 456994
    sget-object v2, LX/EhG;->a:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    sget-object v5, LX/2mf;->a:LX/0U1;

    .line 456995
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 456996
    iget-object v6, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    sget-object v2, LX/2mf;->c:LX/0U1;

    .line 456997
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v6

    .line 456998
    add-int/lit8 v2, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v6, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    sget-object v5, LX/2mf;->b:LX/0U1;

    .line 456999
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 457000
    iget-object v6, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    sget-object v5, LX/2mf;->d:LX/0U1;

    .line 457001
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 457002
    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->a()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    sget-object v5, LX/2mf;->e:LX/0U1;

    .line 457003
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 457004
    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->b()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 457005
    const/4 v6, 0x0

    .line 457006
    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->b()I

    move-result p1

    .line 457007
    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    move v5, v6

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/model/Bookmark;

    .line 457008
    sget-object v7, LX/EhH;->a:Landroid/net/Uri;

    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object p3

    if-ge v5, p1, :cond_0

    const/4 v7, 0x1

    move v8, v7

    :goto_2
    add-int/lit8 v7, v5, 0x1

    invoke-static {v1, v0, v8, v5}, LX/2l7;->a(Lcom/facebook/bookmark/model/Bookmark;Lcom/facebook/bookmark/model/BookmarksGroup;ZI)Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v5, v7

    .line 457009
    goto :goto_1

    :cond_0
    move v8, v6

    .line 457010
    goto :goto_2

    .line 457011
    :cond_1
    move v1, v2

    .line 457012
    goto/16 :goto_0

    .line 457013
    :cond_2
    iget-object v0, p0, LX/2l7;->a:Landroid/content/ContentResolver;

    sget-object v1, LX/0Ow;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 457014
    return-void
.end method

.method public final a(JI)Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 456972
    invoke-static {p0, p1, p2}, LX/2l7;->a(LX/2l7;J)Z

    move-result v2

    if-nez v2, :cond_1

    .line 456973
    :cond_0
    :goto_0
    return v0

    .line 456974
    :cond_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 456975
    sget-object v3, LX/EhH;->b:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    sget-object v4, LX/2md;->g:LX/0U1;

    .line 456976
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 456977
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/2md;->b:LX/0U1;

    .line 456978
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 456979
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456980
    sget-object v3, LX/2mG;->a:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    sget-object v4, LX/2mV;->b:LX/0U1;

    .line 456981
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 456982
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/ContentProviderOperation$Builder;->withExpectedCount(I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456983
    iget-object v3, p0, LX/2l7;->a:Landroid/content/ContentResolver;

    sget-object v4, LX/0Ow;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v2

    .line 456984
    aget-object v2, v2, v0

    iget-object v2, v2, Landroid/content/ContentProviderResult;->count:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final b()J
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 456961
    :try_start_0
    iget-object v0, p0, LX/2l7;->a:Landroid/content/ContentResolver;

    sget-object v1, LX/2mG;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, LX/2mV;->a:LX/0U1;

    .line 456962
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 456963
    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 456964
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    .line 456965
    :cond_0
    new-instance v0, Landroid/database/SQLException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can not query "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/2mG;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 456966
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v1, :cond_1

    .line 456967
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 456968
    :cond_2
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v2

    .line 456969
    if-eqz v1, :cond_3

    .line 456970
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    return-wide v2

    .line 456971
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_0
.end method
