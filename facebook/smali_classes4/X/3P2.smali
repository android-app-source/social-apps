.class public final LX/3P2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/location/ImmutableLocation;

.field public final synthetic b:Lcom/facebook/divebar/contacts/DivebarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/divebar/contacts/DivebarFragment;Lcom/facebook/location/ImmutableLocation;)V
    .locals 0

    .prologue
    .line 561038
    iput-object p1, p0, LX/3P2;->b:Lcom/facebook/divebar/contacts/DivebarFragment;

    iput-object p2, p0, LX/3P2;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 561035
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 561036
    const-string v1, "fetchChatContextParams"

    new-instance v2, Lcom/facebook/contacts/server/FetchChatContextParams;

    iget-object v3, p0, LX/3P2;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-static {v3}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/facebook/contacts/server/FetchChatContextParams;-><init>(LX/0am;Z)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 561037
    iget-object v1, p0, LX/3P2;->b:Lcom/facebook/divebar/contacts/DivebarFragment;

    iget-object v1, v1, Lcom/facebook/divebar/contacts/DivebarFragment;->p:LX/0aG;

    const-string v2, "sync_chat_context"

    sget-object v3, Lcom/facebook/divebar/contacts/DivebarFragment;->c:Lcom/facebook/common/callercontext/CallerContext;

    const v4, 0x4590ec0a

    invoke-static {v1, v2, v0, v3, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method
