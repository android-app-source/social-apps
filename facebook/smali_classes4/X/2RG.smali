.class public LX/2RG;
.super LX/2QZ;
.source ""


# instance fields
.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/2Qy;


# direct methods
.method public constructor <init>(LX/2Qy;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Qy;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409711
    const-string v0, "platform_publish_open_graph_action"

    invoke-direct {p0, v0}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 409712
    iput-object p1, p0, LX/2RG;->c:LX/2Qy;

    .line 409713
    iput-object p2, p0, LX/2RG;->b:LX/0Or;

    .line 409714
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 14

    .prologue
    .line 409715
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 409716
    iget-object v0, p0, LX/2RG;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, LX/11H;

    .line 409717
    const-string v0, "platformParams"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/facebook/platform/params/PlatformComposerParams;

    .line 409718
    const-string v0, "platform_publish_open_graph_action_params"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;

    .line 409719
    new-instance v0, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionMethod$Params;

    iget-object v1, v4, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;->a:Ljava/lang/String;

    iget-object v2, v4, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;->b:LX/0m9;

    iget-object v3, v12, Lcom/facebook/platform/params/PlatformComposerParams;->h:Ljava/lang/String;

    iget-object v4, v4, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;->c:Ljava/lang/String;

    iget-object v5, v12, Lcom/facebook/platform/params/PlatformComposerParams;->i:Ljava/util/Set;

    iget-object v6, v12, Lcom/facebook/platform/params/PlatformComposerParams;->d:Ljava/lang/String;

    iget-object v7, v12, Lcom/facebook/platform/params/PlatformComposerParams;->e:Ljava/lang/String;

    iget-boolean v8, v12, Lcom/facebook/platform/params/PlatformComposerParams;->c:Z

    iget-boolean v9, v12, Lcom/facebook/platform/params/PlatformComposerParams;->b:Z

    iget-object v10, v12, Lcom/facebook/platform/params/PlatformComposerParams;->f:Ljava/lang/String;

    iget-object v11, v12, Lcom/facebook/platform/params/PlatformComposerParams;->g:Ljava/lang/String;

    iget-object v12, v12, Lcom/facebook/platform/params/PlatformComposerParams;->a:Ljava/lang/String;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionMethod$Params;-><init>(Ljava/lang/String;LX/0m9;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 409720
    iget-object v1, p0, LX/2RG;->c:LX/2Qy;

    invoke-virtual {v13, v1, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 409721
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method
