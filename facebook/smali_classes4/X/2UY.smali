.class public LX/2UY;
.super LX/1Eg;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/2UY;


# instance fields
.field public final a:LX/0SG;

.field public final b:LX/2UZ;

.field private c:LX/2Uc;

.field public d:LX/2Ug;

.field private final e:LX/0TD;

.field public f:J

.field public g:J


# direct methods
.method public constructor <init>(LX/0SG;LX/2UZ;LX/2Uc;LX/2Ug;LX/0TD;)V
    .locals 1
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416161
    const-string v0, "ClockSkewCheckBackgroundTask"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 416162
    iput-object p1, p0, LX/2UY;->a:LX/0SG;

    .line 416163
    iput-object p2, p0, LX/2UY;->b:LX/2UZ;

    .line 416164
    iput-object p3, p0, LX/2UY;->c:LX/2Uc;

    .line 416165
    iput-object p4, p0, LX/2UY;->d:LX/2Ug;

    .line 416166
    iput-object p5, p0, LX/2UY;->e:LX/0TD;

    .line 416167
    return-void
.end method

.method public static a(LX/0QB;)LX/2UY;
    .locals 9

    .prologue
    .line 416168
    sget-object v0, LX/2UY;->h:LX/2UY;

    if-nez v0, :cond_1

    .line 416169
    const-class v1, LX/2UY;

    monitor-enter v1

    .line 416170
    :try_start_0
    sget-object v0, LX/2UY;->h:LX/2UY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 416171
    if-eqz v2, :cond_0

    .line 416172
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 416173
    new-instance v3, LX/2UY;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/2UZ;->a(LX/0QB;)LX/2UZ;

    move-result-object v5

    check-cast v5, LX/2UZ;

    invoke-static {v0}, LX/2Uc;->b(LX/0QB;)LX/2Uc;

    move-result-object v6

    check-cast v6, LX/2Uc;

    invoke-static {v0}, LX/2Ug;->b(LX/0QB;)LX/2Ug;

    move-result-object v7

    check-cast v7, LX/2Ug;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, LX/0TD;

    invoke-direct/range {v3 .. v8}, LX/2UY;-><init>(LX/0SG;LX/2UZ;LX/2Uc;LX/2Ug;LX/0TD;)V

    .line 416174
    move-object v0, v3

    .line 416175
    sput-object v0, LX/2UY;->h:LX/2UY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416176
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 416177
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 416178
    :cond_1
    sget-object v0, LX/2UY;->h:LX/2UY;

    return-object v0

    .line 416179
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 416180
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 416181
    const-class v0, Lcom/facebook/messaging/background/annotations/MessagesLocalTaskTag;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final f()J
    .locals 4

    .prologue
    .line 416182
    iget-object v0, p0, LX/2UY;->d:LX/2Ug;

    invoke-virtual {v0}, LX/2Ug;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2UY;->c:LX/2Uc;

    invoke-virtual {v0}, LX/2Uc;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416183
    :cond_0
    const-wide/16 v0, -0x1

    .line 416184
    :goto_0
    return-wide v0

    .line 416185
    :cond_1
    iget-wide v0, p0, LX/2UY;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    .line 416186
    iget-object v0, p0, LX/2UY;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/2UY;->f:J

    .line 416187
    :cond_2
    iget-wide v0, p0, LX/2UY;->f:J

    goto :goto_0
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416188
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    sget-object v1, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 4

    .prologue
    .line 416189
    iget-object v0, p0, LX/2UY;->d:LX/2Ug;

    invoke-virtual {v0}, LX/2Ug;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2UY;->c:LX/2Uc;

    invoke-virtual {v0}, LX/2Uc;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2UY;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-virtual {p0}, LX/1Eg;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416190
    new-instance v0, LX/6cN;

    invoke-direct {v0, p0}, LX/6cN;-><init>(LX/2UY;)V

    .line 416191
    iget-object v1, p0, LX/2UY;->e:LX/0TD;

    invoke-interface {v1, v0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
