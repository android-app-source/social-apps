.class public LX/2NC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2N8;


# direct methods
.method public constructor <init>(LX/2N8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 398895
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398896
    iput-object p1, p0, LX/2NC;->a:LX/2N8;

    .line 398897
    return-void
.end method

.method public static a(LX/0QB;)LX/2NC;
    .locals 1

    .prologue
    .line 398894
    invoke-static {p0}, LX/2NC;->b(LX/0QB;)LX/2NC;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/model/attribution/ContentAppAttribution;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 398802
    if-nez p0, :cond_0

    .line 398803
    const/4 v0, 0x0

    .line 398804
    :goto_0
    return-object v0

    .line 398805
    :cond_0
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 398806
    const-string v1, "attachment_fbid"

    iget-object v2, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398807
    const-string v1, "app_id"

    iget-object v2, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398808
    iget-object v1, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 398809
    const-string v1, "app_name"

    iget-object v2, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398810
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->d:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 398811
    const-string v1, "app_key_hash"

    iget-object v2, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398812
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->e:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 398813
    const-string v1, "app_package"

    iget-object v2, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398814
    :cond_3
    iget-object v1, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->f:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 398815
    const-string v1, "metadata"

    iget-object v2, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398816
    :cond_4
    iget-object v1, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->g:LX/0P1;

    invoke-static {v1}, LX/16N;->a(Ljava/util/Map;)LX/0m9;

    move-result-object v1

    .line 398817
    const-string v2, "app_scoped_user_ids"

    invoke-virtual {v0, v2, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 398818
    const-string v1, "visibility"

    invoke-virtual {v0, v1}, LX/0m9;->j(Ljava/lang/String;)LX/0m9;

    move-result-object v1

    .line 398819
    const-string v2, "hideAttribution"

    iget-object v3, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->h:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    iget-boolean v3, v3, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->c:Z

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 398820
    const-string v2, "hideInstallButton"

    iget-object v3, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->h:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    iget-boolean v3, v3, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->e:Z

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 398821
    const-string v2, "hideReplyButton"

    iget-object v3, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->h:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    iget-boolean v3, v3, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->f:Z

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 398822
    const-string v2, "hideAppIcon"

    iget-object v3, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->h:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    iget-boolean v3, v3, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->g:Z

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 398823
    iget-object v1, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->i:LX/5dc;

    if-eqz v1, :cond_6

    .line 398824
    const-string v1, "app_type"

    iget-object v2, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->i:LX/5dc;

    invoke-virtual {v2}, LX/5dc;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 398825
    :goto_1
    iget-object v1, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->j:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 398826
    const-string v1, "icon_uri"

    iget-object v2, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398827
    :cond_5
    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 398828
    :cond_6
    const-string v1, "app_type"

    sget-object v2, LX/5dc;->UNRECOGNIZED:LX/5dc;

    invoke-virtual {v2}, LX/5dc;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/2NC;
    .locals 2

    .prologue
    .line 398892
    new-instance v1, LX/2NC;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v0

    check-cast v0, LX/2N8;

    invoke-direct {v1, v0}, LX/2NC;-><init>(LX/2N8;)V

    .line 398893
    return-object v1
.end method


# virtual methods
.method public final b(Ljava/lang/String;)Lcom/facebook/messaging/model/attribution/ContentAppAttribution;
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 398829
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398830
    :goto_0
    return-object v2

    .line 398831
    :cond_0
    iget-object v0, p0, LX/2NC;->a:LX/2N8;

    invoke-virtual {v0, p1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    .line 398832
    const-string v0, "attachment_fbid"

    invoke-virtual {v8, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "attachment_fbid"

    invoke-virtual {v8, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 398833
    :goto_1
    const-string v0, "app_id"

    invoke-virtual {v8, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v9

    .line 398834
    const-string v0, "app_name"

    invoke-virtual {v8, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "app_name"

    invoke-virtual {v8, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 398835
    :goto_2
    const-string v0, "app_key_hash"

    invoke-virtual {v8, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "app_key_hash"

    invoke-virtual {v8, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 398836
    :goto_3
    const-string v0, "app_package"

    invoke-virtual {v8, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "app_package"

    invoke-virtual {v8, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    .line 398837
    :goto_4
    const-string v0, "metadata"

    invoke-virtual {v8, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "metadata"

    invoke-virtual {v8, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 398838
    :goto_5
    sget-object v0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->b:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    .line 398839
    const-string v7, "visibility"

    invoke-virtual {v8, v7}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 398840
    const-string v0, "visibility"

    invoke-virtual {v8, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 398841
    const-string v7, "hideAttribution"

    invoke-virtual {v0, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-virtual {v7}, LX/0lF;->F()Z

    move-result v7

    .line 398842
    const-string v10, "hideInstallButton"

    invoke-virtual {v0, v10}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v10

    invoke-virtual {v10}, LX/0lF;->F()Z

    move-result v10

    .line 398843
    const-string v11, "hideReplyButton"

    invoke-virtual {v0, v11}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v11

    invoke-virtual {v11}, LX/0lF;->F()Z

    move-result v11

    .line 398844
    const-string v12, "hideAppIcon"

    invoke-virtual {v0, v12}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const/4 v12, 0x0

    invoke-static {v0, v12}, LX/16N;->a(LX/0lF;Z)Z

    move-result v0

    .line 398845
    invoke-static {}, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->newBuilder()LX/5dZ;

    move-result-object v12

    .line 398846
    iput-boolean v7, v12, LX/5dZ;->a:Z

    .line 398847
    move-object v7, v12

    .line 398848
    iput-boolean v10, v7, LX/5dZ;->c:Z

    .line 398849
    move-object v7, v7

    .line 398850
    iput-boolean v11, v7, LX/5dZ;->d:Z

    .line 398851
    move-object v7, v7

    .line 398852
    iput-boolean v0, v7, LX/5dZ;->e:Z

    .line 398853
    move-object v0, v7

    .line 398854
    invoke-virtual {v0}, LX/5dZ;->h()Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    move-result-object v0

    move-object v7, v0

    .line 398855
    :goto_6
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v10

    .line 398856
    const-string v0, "app_scoped_user_ids"

    invoke-virtual {v8, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 398857
    const-string v0, "app_scoped_user_ids"

    invoke-virtual {v8, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 398858
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v11

    .line 398859
    :goto_7
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 398860
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 398861
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v12, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_7

    :cond_1
    move-object v1, v2

    .line 398862
    goto/16 :goto_1

    :cond_2
    move-object v3, v2

    .line 398863
    goto/16 :goto_2

    :cond_3
    move-object v4, v2

    .line 398864
    goto/16 :goto_3

    :cond_4
    move-object v5, v2

    .line 398865
    goto/16 :goto_4

    :cond_5
    move-object v6, v2

    .line 398866
    goto/16 :goto_5

    .line 398867
    :cond_6
    const-string v0, "app_type"

    invoke-virtual {v8, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 398868
    const-string v0, "app_type"

    invoke-virtual {v8, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->w()I

    move-result v0

    invoke-static {v0}, LX/5dc;->fromValue(I)LX/5dc;

    move-result-object v0

    .line 398869
    :goto_8
    const-string v11, "icon_uri"

    invoke-virtual {v8, v11}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_7

    const-string v2, "icon_uri"

    invoke-virtual {v8, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v2

    .line 398870
    :cond_7
    invoke-static {}, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->newBuilder()LX/5dd;

    move-result-object v8

    .line 398871
    iput-object v1, v8, LX/5dd;->a:Ljava/lang/String;

    .line 398872
    move-object v1, v8

    .line 398873
    iput-object v9, v1, LX/5dd;->b:Ljava/lang/String;

    .line 398874
    move-object v1, v1

    .line 398875
    iput-object v3, v1, LX/5dd;->c:Ljava/lang/String;

    .line 398876
    move-object v1, v1

    .line 398877
    iput-object v4, v1, LX/5dd;->d:Ljava/lang/String;

    .line 398878
    move-object v1, v1

    .line 398879
    iput-object v5, v1, LX/5dd;->e:Ljava/lang/String;

    .line 398880
    move-object v1, v1

    .line 398881
    iput-object v6, v1, LX/5dd;->f:Ljava/lang/String;

    .line 398882
    move-object v1, v1

    .line 398883
    invoke-virtual {v10}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/5dd;->a(Ljava/util/Map;)LX/5dd;

    move-result-object v1

    .line 398884
    iput-object v7, v1, LX/5dd;->j:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    .line 398885
    move-object v1, v1

    .line 398886
    iput-object v0, v1, LX/5dd;->g:LX/5dc;

    .line 398887
    move-object v0, v1

    .line 398888
    iput-object v2, v0, LX/5dd;->h:Ljava/lang/String;

    .line 398889
    move-object v0, v0

    .line 398890
    invoke-virtual {v0}, LX/5dd;->k()Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-result-object v2

    goto/16 :goto_0

    .line 398891
    :cond_8
    sget-object v0, LX/5dc;->UNRECOGNIZED:LX/5dc;

    goto :goto_8

    :cond_9
    move-object v7, v0

    goto/16 :goto_6
.end method
