.class public LX/2al;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 424871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 424872
    const/4 v15, 0x0

    .line 424873
    const/4 v14, 0x0

    .line 424874
    const/4 v13, 0x0

    .line 424875
    const/4 v12, 0x0

    .line 424876
    const/4 v11, 0x0

    .line 424877
    const/4 v10, 0x0

    .line 424878
    const/4 v7, 0x0

    .line 424879
    const-wide/16 v8, 0x0

    .line 424880
    const/4 v6, 0x0

    .line 424881
    const/4 v5, 0x0

    .line 424882
    const/4 v4, 0x0

    .line 424883
    const/4 v3, 0x0

    .line 424884
    const/4 v2, 0x0

    .line 424885
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_f

    .line 424886
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 424887
    const/4 v2, 0x0

    .line 424888
    :goto_0
    return v2

    .line 424889
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 424890
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_a

    .line 424891
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 424892
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 424893
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 424894
    const-string v17, "bump_reason"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 424895
    const/4 v7, 0x1

    .line 424896
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/facebook/graphql/enums/GraphQLBumpReason;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v15

    goto :goto_1

    .line 424897
    :cond_2
    const-string v17, "cursor"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 424898
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 424899
    :cond_3
    const-string v17, "deduplication_key"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 424900
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto :goto_1

    .line 424901
    :cond_4
    const-string v17, "disallow_first_position"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 424902
    const/4 v6, 0x1

    .line 424903
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 424904
    :cond_5
    const-string v17, "features_meta"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 424905
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 424906
    :cond_6
    const-string v17, "is_in_low_engagement_block"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 424907
    const/4 v3, 0x1

    .line 424908
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 424909
    :cond_7
    const-string v17, "node"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 424910
    invoke-static/range {p0 .. p1}, LX/2ao;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 424911
    :cond_8
    const-string v17, "ranking_weight"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 424912
    const/4 v2, 0x1

    .line 424913
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    goto/16 :goto_1

    .line 424914
    :cond_9
    const-string v17, "sort_key"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 424915
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 424916
    :cond_a
    const/16 v16, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 424917
    if-eqz v7, :cond_b

    .line 424918
    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v15}, LX/186;->a(ILjava/lang/Enum;)V

    .line 424919
    :cond_b
    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14}, LX/186;->b(II)V

    .line 424920
    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v13}, LX/186;->b(II)V

    .line 424921
    if-eqz v6, :cond_c

    .line 424922
    const/4 v6, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->a(IZ)V

    .line 424923
    :cond_c
    const/4 v6, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v11}, LX/186;->b(II)V

    .line 424924
    if-eqz v3, :cond_d

    .line 424925
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->a(IZ)V

    .line 424926
    :cond_d
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 424927
    if-eqz v2, :cond_e

    .line 424928
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 424929
    :cond_e
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 424930
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_f
    move/from16 v19, v4

    move/from16 v20, v5

    move-wide v4, v8

    move v8, v6

    move v9, v7

    move/from16 v7, v20

    move/from16 v6, v19

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 424931
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 424932
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(IIS)S

    move-result v0

    .line 424933
    if-eqz v0, :cond_0

    .line 424934
    const-string v0, "bump_reason"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 424935
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBumpReason;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 424936
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 424937
    if-eqz v0, :cond_1

    .line 424938
    const-string v1, "cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 424939
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 424940
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 424941
    if-eqz v0, :cond_2

    .line 424942
    const-string v1, "deduplication_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 424943
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 424944
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 424945
    if-eqz v0, :cond_3

    .line 424946
    const-string v1, "disallow_first_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 424947
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 424948
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 424949
    if-eqz v0, :cond_4

    .line 424950
    const-string v1, "features_meta"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 424951
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 424952
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 424953
    if-eqz v0, :cond_5

    .line 424954
    const-string v1, "is_in_low_engagement_block"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 424955
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 424956
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 424957
    if-eqz v0, :cond_6

    .line 424958
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 424959
    invoke-static {p0, v0, p2, p3}, LX/2ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 424960
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 424961
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_7

    .line 424962
    const-string v2, "ranking_weight"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 424963
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 424964
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 424965
    if-eqz v0, :cond_8

    .line 424966
    const-string v1, "sort_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 424967
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 424968
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 424969
    return-void
.end method
