.class public LX/2ah;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:[Ljava/lang/Object;

.field public b:[I

.field public c:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 424813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424814
    if-gtz p1, :cond_0

    .line 424815
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Non-positive capacity not allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 424816
    :cond_0
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, LX/2ah;->a:[Ljava/lang/Object;

    .line 424817
    new-array v0, p1, [I

    iput-object v0, p0, LX/2ah;->b:[I

    .line 424818
    const/4 v0, 0x0

    iput v0, p0, LX/2ah;->c:I

    .line 424819
    return-void
.end method

.method public static a(LX/2ah;Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .prologue
    .line 424820
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, LX/2ah;->c:I

    if-ge v0, v1, :cond_1

    .line 424821
    iget-object v1, p0, LX/2ah;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 424822
    :goto_1
    return v0

    .line 424823
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 424824
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
