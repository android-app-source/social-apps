.class public LX/2aL;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile b:LX/2aL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 424588
    const-class v0, LX/2aL;

    sput-object v0, LX/2aL;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 424589
    const-string v0, "mds_cache"

    const/4 v1, 0x2

    new-instance v2, LX/2aM;

    invoke-direct {v2}, LX/2aM;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 424590
    return-void
.end method

.method public static a(LX/0QB;)LX/2aL;
    .locals 3

    .prologue
    .line 424591
    sget-object v0, LX/2aL;->b:LX/2aL;

    if-nez v0, :cond_1

    .line 424592
    const-class v1, LX/2aL;

    monitor-enter v1

    .line 424593
    :try_start_0
    sget-object v0, LX/2aL;->b:LX/2aL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 424594
    if-eqz v2, :cond_0

    .line 424595
    :try_start_1
    new-instance v0, LX/2aL;

    invoke-direct {v0}, LX/2aL;-><init>()V

    .line 424596
    move-object v0, v0

    .line 424597
    sput-object v0, LX/2aL;->b:LX/2aL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 424598
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 424599
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 424600
    :cond_1
    sget-object v0, LX/2aL;->b:LX/2aL;

    return-object v0

    .line 424601
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 424602
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
