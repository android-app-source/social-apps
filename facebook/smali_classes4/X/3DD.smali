.class public LX/3DD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field private final a:LX/2nq;

.field private final b:LX/3D1;

.field private final c:I

.field private final d:LX/2jO;

.field private final e:LX/2it;

.field private final f:LX/1rU;


# direct methods
.method public constructor <init>(LX/2nq;ILX/3D1;LX/2jO;LX/2it;LX/1rU;)V
    .locals 0
    .param p1    # LX/2nq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 532018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 532019
    iput-object p1, p0, LX/3DD;->a:LX/2nq;

    .line 532020
    iput-object p3, p0, LX/3DD;->b:LX/3D1;

    .line 532021
    iput p2, p0, LX/3DD;->c:I

    .line 532022
    iput-object p4, p0, LX/3DD;->d:LX/2jO;

    .line 532023
    iput-object p5, p0, LX/3DD;->e:LX/2it;

    .line 532024
    iput-object p6, p0, LX/3DD;->f:LX/1rU;

    .line 532025
    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 532026
    iget-object v0, p0, LX/3DD;->b:LX/3D1;

    iget-object v1, p0, LX/3DD;->a:LX/2nq;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/3DD;->d:LX/2jO;

    iget-object v4, p0, LX/3DD;->a:LX/2nq;

    invoke-interface {v4}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/3DD;->a:LX/2nq;

    invoke-interface {v5}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/2jO;->a(Ljava/lang/String;Ljava/lang/String;)LX/3D3;

    move-result-object v4

    iget v5, p0, LX/3DD;->c:I

    iget-object v3, p0, LX/3DD;->f:LX/1rU;

    invoke-virtual {v3}, LX/1rU;->c()Z

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, LX/3D1;->a(LX/2nq;Landroid/content/Context;Landroid/view/View;LX/3D3;I)V

    .line 532027
    iget-object v0, p0, LX/3DD;->e:LX/2it;

    invoke-virtual {v0}, LX/2it;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 532028
    const v0, 0x7f0d2e68

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 532029
    instance-of v0, v1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 532030
    check-cast v0, Landroid/view/ViewGroup;

    .line 532031
    invoke-static {v0, v6}, LX/8t0;->a(Landroid/view/ViewGroup;I)V

    .line 532032
    :cond_0
    instance-of v0, v1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz v0, :cond_1

    .line 532033
    check-cast v1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-static {v1, v6}, LX/8t0;->a(Lcom/facebook/fbui/widget/layout/ImageBlockLayout;I)V

    .line 532034
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
