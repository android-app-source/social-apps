.class public LX/2NL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 399716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399717
    return-void
.end method

.method public static a(Lcom/facebook/messaging/business/attachments/model/LogoImage;)LX/0m9;
    .locals 3
    .param p0    # Lcom/facebook/messaging/business/attachments/model/LogoImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 399709
    new-instance v1, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 399710
    if-eqz p0, :cond_0

    .line 399711
    const-string v2, "url"

    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->a:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399712
    const-string v0, "width"

    iget v2, p0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->b:I

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 399713
    const-string v0, "height"

    iget v2, p0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->c:I

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 399714
    :cond_0
    return-object v1

    .line 399715
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;)LX/0m9;
    .locals 14
    .param p0    # Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 399620
    if-eqz p0, :cond_0

    .line 399621
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;->a:Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;

    move-object v0, v0

    .line 399622
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 399623
    :goto_0
    return-object v0

    .line 399624
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;->a:Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;

    move-object v0, v0

    .line 399625
    iget-object v2, p0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;->a:Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;

    move-object v2, v2

    .line 399626
    invoke-interface {v2}, Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;->a()LX/5TJ;

    move-result-object v2

    .line 399627
    sget-object v3, LX/5TJ;->RECEIPT:LX/5TJ;

    if-ne v2, v3, :cond_2

    .line 399628
    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    .line 399629
    new-instance v3, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 399630
    const-string v1, "receipt_id"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->a:Ljava/lang/String;

    invoke-virtual {v3, v1, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399631
    const-string v1, "order_id"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->b:Ljava/lang/String;

    invoke-virtual {v3, v1, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399632
    const-string v1, "shipping_method"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->c:Ljava/lang/String;

    invoke-virtual {v3, v1, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399633
    const-string v1, "payment_method"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->d:Ljava/lang/String;

    invoke-virtual {v3, v1, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399634
    const-string p0, "order_url"

    iget-object v1, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->e:Landroid/net/Uri;

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->e:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v3, p0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399635
    const-string p0, "cancellation_url"

    iget-object v1, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->f:Landroid/net/Uri;

    if-eqz v1, :cond_b

    iget-object v1, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->f:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v3, p0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399636
    const-string v1, "structured_address"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->g:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    invoke-static {p0}, LX/2NL;->a(Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;)LX/0m9;

    move-result-object p0

    invoke-virtual {v3, v1, p0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399637
    const-string v1, "status"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->h:Ljava/lang/String;

    invoke-virtual {v3, v1, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399638
    const-string v1, "total_cost"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->i:Ljava/lang/String;

    invoke-virtual {v3, v1, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399639
    const-string v1, "total_tax"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->j:Ljava/lang/String;

    invoke-virtual {v3, v1, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399640
    const-string v1, "shipping_cost"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->l:Ljava/lang/String;

    invoke-virtual {v3, v1, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399641
    const-string v1, "subtotal"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->m:Ljava/lang/String;

    invoke-virtual {v3, v1, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399642
    const-string v1, "order_time"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->o:Ljava/lang/String;

    invoke-virtual {v3, v1, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399643
    const-string v1, "partner_logo"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->n:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    invoke-static {p0}, LX/2NL;->a(Lcom/facebook/messaging/business/attachments/model/LogoImage;)LX/0m9;

    move-result-object p0

    invoke-virtual {v3, v1, p0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399644
    const-string v1, "items"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->u:LX/0Px;

    invoke-static {p0}, LX/2NL;->a(Ljava/util/List;)LX/0m9;

    move-result-object p0

    invoke-virtual {v3, v1, p0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399645
    const-string v1, "recipient_name"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->p:Ljava/lang/String;

    invoke-virtual {v3, v1, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399646
    const-string v1, "account_holder_name"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->q:Ljava/lang/String;

    invoke-virtual {v3, v1, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399647
    move-object v0, v3

    .line 399648
    :goto_3
    const-string v1, "messenger_commerce_bubble_type"

    invoke-virtual {v2}, LX/5TJ;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 399649
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 399650
    const-string v2, "fb_object_contents"

    invoke-virtual {v1, v2, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    move-object v0, v1

    .line 399651
    goto/16 :goto_0

    .line 399652
    :cond_2
    sget-object v3, LX/5TJ;->CANCELLATION:LX/5TJ;

    if-ne v2, v3, :cond_4

    .line 399653
    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/ReceiptCancellation;

    const/4 v3, 0x0

    .line 399654
    new-instance v4, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 399655
    const-string v1, "cancellation_id"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/ReceiptCancellation;->a:Ljava/lang/String;

    invoke-virtual {v4, v1, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399656
    const-string p0, "receipt_id"

    iget-object v1, v0, Lcom/facebook/messaging/business/commerce/model/retail/ReceiptCancellation;->b:Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/facebook/messaging/business/commerce/model/retail/ReceiptCancellation;->b:Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    iget-object v1, v1, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->a:Ljava/lang/String;

    :goto_4
    invoke-virtual {v4, p0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399657
    const-string p0, "order_id"

    iget-object v1, v0, Lcom/facebook/messaging/business/commerce/model/retail/ReceiptCancellation;->b:Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    if-eqz v1, :cond_d

    iget-object v1, v0, Lcom/facebook/messaging/business/commerce/model/retail/ReceiptCancellation;->b:Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    iget-object v1, v1, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->b:Ljava/lang/String;

    :goto_5
    invoke-virtual {v4, p0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399658
    const-string v1, "partner_logo"

    iget-object p0, v0, Lcom/facebook/messaging/business/commerce/model/retail/ReceiptCancellation;->b:Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    if-eqz p0, :cond_3

    iget-object v3, v0, Lcom/facebook/messaging/business/commerce/model/retail/ReceiptCancellation;->b:Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    iget-object v3, v3, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->n:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    :cond_3
    invoke-static {v3}, LX/2NL;->a(Lcom/facebook/messaging/business/attachments/model/LogoImage;)LX/0m9;

    move-result-object v3

    invoke-virtual {v4, v1, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399659
    const-string v1, "items"

    iget-object v3, v0, Lcom/facebook/messaging/business/commerce/model/retail/ReceiptCancellation;->d:LX/0Px;

    invoke-static {v3}, LX/2NL;->a(Ljava/util/List;)LX/0m9;

    move-result-object v3

    invoke-virtual {v4, v1, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399660
    move-object v0, v4

    .line 399661
    goto :goto_3

    .line 399662
    :cond_4
    sget-object v3, LX/5TJ;->SHIPMENT:LX/5TJ;

    if-eq v2, v3, :cond_5

    sget-object v3, LX/5TJ;->SHIPMENT_FOR_UNSUPPORTED_CARRIER:LX/5TJ;

    if-ne v2, v3, :cond_6

    .line 399663
    :cond_5
    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    const-wide/16 v12, 0x0

    const-wide/16 v10, 0x3e8

    .line 399664
    new-instance v5, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 399665
    const-string v4, "shipment_id"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->a:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399666
    const-string v4, "receipt_id"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->b:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399667
    const-string v4, "tracking_number"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->c:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399668
    const-string v4, "carrier"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    iget-object v6, v6, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->a:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399669
    const-string v6, "carrier_tracking_url"

    iget-object v4, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->e:Landroid/net/Uri;

    if-eqz v4, :cond_e

    iget-object v4, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->e:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_6
    invoke-virtual {v5, v6, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399670
    const-string v4, "ship_date"

    iget-wide v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->f:J

    div-long/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399671
    const-string v4, "display_ship_date"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->g:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399672
    const-string v4, "origin"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->h:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    invoke-static {v6}, LX/2NL;->a(Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;)LX/0m9;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399673
    const-string v4, "destination"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->i:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    invoke-static {v6}, LX/2NL;->a(Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;)LX/0m9;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399674
    const-string v6, "estimated_delivery_time"

    iget-wide v8, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->j:J

    cmp-long v4, v8, v12

    if-eqz v4, :cond_f

    iget-wide v8, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->j:J

    div-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    :goto_7
    invoke-virtual {v5, v6, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399675
    const-string v4, "estimated_delivery_display_time"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->k:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399676
    const-string v6, "delayed_delivery_time"

    iget-wide v8, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->l:J

    cmp-long v4, v8, v12

    if-eqz v4, :cond_10

    iget-wide v8, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->l:J

    div-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    :goto_8
    invoke-virtual {v5, v6, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399677
    const-string v4, "delayed_delivery_display_time"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->m:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399678
    const-string v4, "service_type"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->n:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399679
    const-string v4, "carrier_logo"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->o:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    invoke-static {v6}, LX/2NL;->a(Lcom/facebook/messaging/business/attachments/model/LogoImage;)LX/0m9;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399680
    const-string v4, "items"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->p:LX/0Px;

    invoke-static {v6}, LX/2NL;->a(Ljava/util/List;)LX/0m9;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399681
    move-object v0, v5

    .line 399682
    goto/16 :goto_3

    .line 399683
    :cond_6
    sget-object v3, LX/5TJ;->SHIPMENT_TRACKING_ETA:LX/5TJ;

    if-eq v2, v3, :cond_7

    sget-object v3, LX/5TJ;->SHIPMENT_TRACKING_IN_TRANSIT:LX/5TJ;

    if-eq v2, v3, :cond_7

    sget-object v3, LX/5TJ;->SHIPMENT_TRACKING_OUT_FOR_DELIVERY:LX/5TJ;

    if-eq v2, v3, :cond_7

    sget-object v3, LX/5TJ;->SHIPMENT_TRACKING_DELAYED:LX/5TJ;

    if-eq v2, v3, :cond_7

    sget-object v3, LX/5TJ;->SHIPMENT_TRACKING_DELIVERED:LX/5TJ;

    if-eq v2, v3, :cond_7

    sget-object v3, LX/5TJ;->SHIPMENT_ETA:LX/5TJ;

    if-ne v2, v3, :cond_9

    .line 399684
    :cond_7
    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;

    .line 399685
    new-instance v5, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 399686
    const-string v4, "id"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->a:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399687
    const-string v4, "tracking_number"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->f:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    iget-object v6, v6, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->c:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399688
    const-string v4, "timestamp"

    iget-wide v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->c:J

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399689
    const-string v4, "display_time"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->d:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399690
    const-string v4, "tracking_event_location"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->e:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    invoke-static {v6}, LX/2NL;->a(Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;)LX/0m9;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399691
    iget-object v4, v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->f:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    if-eqz v4, :cond_8

    .line 399692
    const-string v4, "shipment_id"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->f:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    iget-object v6, v6, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->a:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399693
    const-string v4, "carrier"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->f:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    iget-object v6, v6, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    iget-object v6, v6, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->a:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399694
    const-string v6, "carrier_tracking_url"

    iget-object v4, v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->f:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    iget-object v4, v4, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    iget-object v4, v4, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->c:Landroid/net/Uri;

    if-eqz v4, :cond_11

    iget-object v4, v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->f:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    iget-object v4, v4, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    iget-object v4, v4, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->c:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_9
    invoke-virtual {v5, v6, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399695
    const-string v4, "carrier_logo"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->f:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    iget-object v6, v6, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    iget-object v6, v6, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->b:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    invoke-static {v6}, LX/2NL;->a(Lcom/facebook/messaging/business/attachments/model/LogoImage;)LX/0m9;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399696
    const-string v4, "service_type"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->f:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    iget-object v6, v6, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->n:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399697
    const-string v4, "items"

    iget-object v6, v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->f:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    iget-object v6, v6, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->p:LX/0Px;

    invoke-static {v6}, LX/2NL;->a(Ljava/util/List;)LX/0m9;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399698
    :cond_8
    move-object v0, v5

    .line 399699
    goto/16 :goto_3

    :cond_9
    move-object v0, v1

    .line 399700
    goto/16 :goto_0

    .line 399701
    :cond_a
    const-string v1, ""

    goto/16 :goto_1

    .line 399702
    :cond_b
    const-string v1, ""

    goto/16 :goto_2

    :cond_c
    move-object v1, v3

    .line 399703
    goto/16 :goto_4

    :cond_d
    move-object v1, v3

    .line 399704
    goto/16 :goto_5

    .line 399705
    :cond_e
    const-string v4, ""

    goto/16 :goto_6

    .line 399706
    :cond_f
    const-string v4, ""

    goto/16 :goto_7

    .line 399707
    :cond_10
    const-string v4, ""

    goto/16 :goto_8

    .line 399708
    :cond_11
    const-string v4, ""

    goto :goto_9
.end method

.method public static a(Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;)LX/0m9;
    .locals 4

    .prologue
    .line 399608
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 399609
    if-eqz p0, :cond_0

    .line 399610
    const-string v1, "street_1"

    iget-object v2, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399611
    const-string v1, "street_2"

    iget-object v2, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399612
    const-string v1, "city"

    iget-object v2, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399613
    const-string v1, "state"

    iget-object v2, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399614
    const-string v1, "postal_code"

    iget-object v2, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399615
    const-string v1, "country"

    iget-object v2, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399616
    const-string v1, "timezone"

    iget-object v2, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399617
    const-string v1, "latitude"

    iget-wide v2, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->h:D

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399618
    const-string v1, "longitude"

    iget-wide v2, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->i:D

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399619
    :cond_0
    return-object v0
.end method

.method public static a(Ljava/util/List;)LX/0m9;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;",
            ">;)",
            "LX/0m9;"
        }
    .end annotation

    .prologue
    .line 399589
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 399590
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 399591
    :goto_0
    return-object v0

    .line 399592
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    .line 399593
    new-instance v4, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 399594
    const-string v1, "location"

    iget-object v5, v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->a:Ljava/lang/String;

    invoke-virtual {v4, v1, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399595
    const-string v1, "title"

    iget-object v5, v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->b:Ljava/lang/String;

    invoke-virtual {v4, v1, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399596
    const-string v1, "desc"

    iget-object v5, v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->c:Ljava/lang/String;

    invoke-virtual {v4, v1, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399597
    const-string v1, "price"

    iget-object v5, v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->f:Ljava/lang/String;

    invoke-virtual {v4, v1, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399598
    const-string v1, "quantity"

    iget v5, v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->g:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399599
    const-string v5, "thumb_url"

    iget-object v1, v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->d:Landroid/net/Uri;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->d:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v4, v5, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399600
    new-instance v1, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 399601
    const-string v5, "metaline_1"

    iget-object v6, v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->h:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399602
    const-string v5, "metaline_2"

    iget-object v6, v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->i:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399603
    const-string v5, "metaline_3"

    iget-object v6, v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->j:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399604
    const-string v5, "metalines"

    invoke-virtual {v4, v5, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399605
    iget-object v0, v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_1

    .line 399606
    :cond_2
    const-string v1, ""

    goto :goto_2

    :cond_3
    move-object v0, v2

    .line 399607
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2NL;
    .locals 1

    .prologue
    .line 399586
    new-instance v0, LX/2NL;

    invoke-direct {v0}, LX/2NL;-><init>()V

    .line 399587
    move-object v0, v0

    .line 399588
    return-object v0
.end method

.method public static a(LX/5TJ;LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 399452
    new-instance v0, LX/5TX;

    invoke-direct {v0}, LX/5TX;-><init>()V

    .line 399453
    const-string v1, "id"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399454
    iput-object v1, v0, LX/5TX;->b:Ljava/lang/String;

    .line 399455
    iput-object p0, v0, LX/5TX;->a:LX/5TJ;

    .line 399456
    const-string v1, "timestamp"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 399457
    iput-wide v2, v0, LX/5TX;->d:J

    .line 399458
    const-string v1, "display_time"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399459
    iput-object v1, v0, LX/5TX;->e:Ljava/lang/String;

    .line 399460
    const-string v1, "tracking_event_location"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/2NL;->g(LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-result-object v1

    .line 399461
    iput-object v1, v0, LX/5TX;->f:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 399462
    const-string v1, "shipment_id"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 399463
    new-instance v1, LX/5TV;

    invoke-direct {v1}, LX/5TV;-><init>()V

    .line 399464
    new-instance v2, LX/5TT;

    invoke-direct {v2}, LX/5TT;-><init>()V

    const-string v3, "carrier"

    invoke-virtual {p1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 399465
    iput-object v3, v2, LX/5TT;->a:Ljava/lang/String;

    .line 399466
    move-object v2, v2

    .line 399467
    const-string v3, "carrier_logo"

    invoke-virtual {p1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/2NL;->i(LX/0lF;)Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-result-object v3

    .line 399468
    iput-object v3, v2, LX/5TT;->b:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 399469
    move-object v2, v2

    .line 399470
    const-string v3, "carrier_tracking_url"

    invoke-virtual {p1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5TT;->b(Ljava/lang/String;)LX/5TT;

    move-result-object v2

    invoke-virtual {v2}, LX/5TT;->e()Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    move-result-object v2

    .line 399471
    iput-object v2, v1, LX/5TV;->d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    .line 399472
    const-string v2, "shipment_id"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 399473
    iput-object v2, v1, LX/5TV;->a:Ljava/lang/String;

    .line 399474
    const-string v2, "tracking_number"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 399475
    iput-object v2, v1, LX/5TV;->c:Ljava/lang/String;

    .line 399476
    const-string v2, "service_type"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 399477
    iput-object v2, v1, LX/5TV;->n:Ljava/lang/String;

    .line 399478
    const-string v2, "items"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/2NL;->h(LX/0lF;)Ljava/util/List;

    move-result-object v2

    .line 399479
    iput-object v2, v1, LX/5TV;->p:Ljava/util/List;

    .line 399480
    invoke-virtual {v1}, LX/5TV;->r()Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    move-result-object v1

    .line 399481
    iput-object v1, v0, LX/5TX;->g:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    .line 399482
    :cond_0
    new-instance v1, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    invoke-virtual {v0}, LX/5TX;->h()Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;-><init>(Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;)V

    return-object v1
.end method

.method public static f(LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    const-wide/16 v8, 0x3e8

    .line 399543
    const-string v0, "carrier"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 399544
    const-string v1, "carrier_logo"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/2NL;->i(LX/0lF;)Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-result-object v4

    .line 399545
    const-string v1, "carrier_tracking_url"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399546
    new-instance v5, LX/5TT;

    invoke-direct {v5}, LX/5TT;-><init>()V

    .line 399547
    iput-object v0, v5, LX/5TT;->a:Ljava/lang/String;

    .line 399548
    move-object v0, v5

    .line 399549
    iput-object v4, v0, LX/5TT;->b:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 399550
    move-object v0, v0

    .line 399551
    invoke-virtual {v0, v1}, LX/5TT;->b(Ljava/lang/String;)LX/5TT;

    move-result-object v0

    invoke-virtual {v0}, LX/5TT;->e()Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    move-result-object v0

    .line 399552
    new-instance v5, LX/5TV;

    invoke-direct {v5}, LX/5TV;-><init>()V

    .line 399553
    const-string v6, "shipment_id"

    invoke-virtual {p0, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 399554
    iput-object v6, v5, LX/5TV;->a:Ljava/lang/String;

    .line 399555
    const-string v6, "receipt_id"

    invoke-virtual {p0, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 399556
    iput-object v6, v5, LX/5TV;->b:Ljava/lang/String;

    .line 399557
    const-string v6, "tracking_number"

    invoke-virtual {p0, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 399558
    iput-object v6, v5, LX/5TV;->c:Ljava/lang/String;

    .line 399559
    iput-object v0, v5, LX/5TV;->d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    .line 399560
    invoke-virtual {v5, v1}, LX/5TV;->d(Ljava/lang/String;)LX/5TV;

    .line 399561
    const-string v0, "ship_date"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    mul-long/2addr v0, v8

    .line 399562
    iput-wide v0, v5, LX/5TV;->f:J

    .line 399563
    const-string v0, "display_ship_date"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 399564
    iput-object v0, v5, LX/5TV;->g:Ljava/lang/String;

    .line 399565
    const-string v0, "origin"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/2NL;->g(LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-result-object v0

    .line 399566
    iput-object v0, v5, LX/5TV;->h:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 399567
    const-string v0, "destination"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/2NL;->g(LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-result-object v0

    .line 399568
    iput-object v0, v5, LX/5TV;->i:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 399569
    const-string v0, "estimated_delivery_time"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 399570
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    mul-long/2addr v0, v8

    .line 399571
    :goto_0
    iput-wide v0, v5, LX/5TV;->j:J

    .line 399572
    const-string v0, "estimated_delivery_display_time"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 399573
    iput-object v0, v5, LX/5TV;->k:Ljava/lang/String;

    .line 399574
    const-string v0, "delayed_delivery_time"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 399575
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    mul-long v2, v0, v8

    .line 399576
    :cond_0
    iput-wide v2, v5, LX/5TV;->l:J

    .line 399577
    const-string v0, "delayed_delivery_display_time"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 399578
    iput-object v0, v5, LX/5TV;->m:Ljava/lang/String;

    .line 399579
    const-string v0, "service_type"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 399580
    iput-object v0, v5, LX/5TV;->n:Ljava/lang/String;

    .line 399581
    iput-object v4, v5, LX/5TV;->o:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 399582
    const-string v0, "items"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/2NL;->h(LX/0lF;)Ljava/util/List;

    move-result-object v0

    .line 399583
    iput-object v0, v5, LX/5TV;->p:Ljava/util/List;

    .line 399584
    new-instance v0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    invoke-virtual {v5}, LX/5TV;->r()Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;-><init>(Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;)V

    return-object v0

    :cond_1
    move-wide v0, v2

    .line 399585
    goto :goto_0
.end method

.method public static g(LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;
    .locals 5
    .param p0    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 399520
    if-nez p0, :cond_0

    .line 399521
    const/4 v0, 0x0

    .line 399522
    :goto_0
    return-object v0

    .line 399523
    :cond_0
    new-instance v0, LX/5TQ;

    invoke-direct {v0}, LX/5TQ;-><init>()V

    .line 399524
    const-string v1, "street_1"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399525
    iput-object v1, v0, LX/5TQ;->a:Ljava/lang/String;

    .line 399526
    const-string v1, "street_2"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399527
    iput-object v1, v0, LX/5TQ;->b:Ljava/lang/String;

    .line 399528
    const-string v1, "city"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399529
    iput-object v1, v0, LX/5TQ;->c:Ljava/lang/String;

    .line 399530
    const-string v1, "state"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399531
    iput-object v1, v0, LX/5TQ;->d:Ljava/lang/String;

    .line 399532
    const-string v1, "postal_code"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399533
    iput-object v1, v0, LX/5TQ;->e:Ljava/lang/String;

    .line 399534
    const-string v1, "country"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399535
    iput-object v1, v0, LX/5TQ;->f:Ljava/lang/String;

    .line 399536
    const-string v1, "timezone"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399537
    iput-object v1, v0, LX/5TQ;->g:Ljava/lang/String;

    .line 399538
    const-string v1, "latitude"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->e(LX/0lF;)D

    move-result-wide v2

    .line 399539
    iput-wide v2, v0, LX/5TQ;->h:D

    .line 399540
    const-string v1, "longitude"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->e(LX/0lF;)D

    move-result-wide v2

    .line 399541
    iput-wide v2, v0, LX/5TQ;->i:D

    .line 399542
    invoke-virtual {v0}, LX/5TQ;->j()Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-result-object v0

    goto :goto_0
.end method

.method public static h(LX/0lF;)Ljava/util/List;
    .locals 5
    .param p0    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 399493
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0lF;->e()I

    move-result v0

    if-nez v0, :cond_1

    .line 399494
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 399495
    :goto_0
    return-object v0

    .line 399496
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 399497
    invoke-virtual {p0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 399498
    new-instance v3, LX/5Sw;

    invoke-direct {v3}, LX/5Sw;-><init>()V

    .line 399499
    const-string v4, "location"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399500
    iput-object v4, v3, LX/5Sw;->a:Ljava/lang/String;

    .line 399501
    const-string v4, "title"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399502
    iput-object v4, v3, LX/5Sw;->b:Ljava/lang/String;

    .line 399503
    const-string v4, "desc"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399504
    iput-object v4, v3, LX/5Sw;->c:Ljava/lang/String;

    .line 399505
    const-string v4, "price"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399506
    iput-object v4, v3, LX/5Sw;->f:Ljava/lang/String;

    .line 399507
    const-string v4, "quantity"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->d(LX/0lF;)I

    move-result v4

    .line 399508
    iput v4, v3, LX/5Sw;->g:I

    .line 399509
    const-string v4, "thumb_url"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/5Sw;->d(Ljava/lang/String;)LX/5Sw;

    .line 399510
    const-string v4, "metalines"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 399511
    if-eqz v0, :cond_2

    .line 399512
    const-string v4, "metaline_1"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399513
    iput-object v4, v3, LX/5Sw;->h:Ljava/lang/String;

    .line 399514
    const-string v4, "metaline_2"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399515
    iput-object v4, v3, LX/5Sw;->i:Ljava/lang/String;

    .line 399516
    const-string v4, "metaline_3"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 399517
    iput-object v0, v3, LX/5Sw;->j:Ljava/lang/String;

    .line 399518
    :cond_2
    invoke-virtual {v3}, LX/5Sw;->p()Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    .line 399519
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static i(LX/0lF;)Lcom/facebook/messaging/business/attachments/model/LogoImage;
    .locals 2
    .param p0    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 399483
    if-nez p0, :cond_0

    .line 399484
    const/4 v0, 0x0

    .line 399485
    :goto_0
    return-object v0

    .line 399486
    :cond_0
    new-instance v0, LX/5Sq;

    invoke-direct {v0}, LX/5Sq;-><init>()V

    .line 399487
    const-string v1, "url"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/5Sq;->a(Ljava/lang/String;)LX/5Sq;

    .line 399488
    const-string v1, "width"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->d(LX/0lF;)I

    move-result v1

    .line 399489
    iput v1, v0, LX/5Sq;->b:I

    .line 399490
    const-string v1, "height"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->d(LX/0lF;)I

    move-result v1

    .line 399491
    iput v1, v0, LX/5Sq;->c:I

    .line 399492
    invoke-virtual {v0}, LX/5Sq;->d()Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-result-object v0

    goto :goto_0
.end method
