.class public final enum LX/2lT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2lT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2lT;

.field public static final enum CACHED_HEADER:LX/2lT;

.field public static final enum CACHED_STORIES:LX/2lT;

.field public static final enum FRESH_HEADER:LX/2lT;

.field public static final enum FRESH_STORIES:LX/2lT;

.field public static final GROUPS_FEED_TTI_SEQUENCE:LX/2lU;

.field public static final enum INITIAL_STORIES:LX/2lT;


# instance fields
.field private mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 458009
    new-instance v0, LX/2lT;

    const-string v1, "FRESH_STORIES"

    const-string v2, "GroupsFeedTTIFreshStories"

    invoke-direct {v0, v1, v3, v2}, LX/2lT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2lT;->FRESH_STORIES:LX/2lT;

    .line 458010
    new-instance v0, LX/2lT;

    const-string v1, "CACHED_STORIES"

    const-string v2, "GroupsFeedTTICachedStories"

    invoke-direct {v0, v1, v4, v2}, LX/2lT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2lT;->CACHED_STORIES:LX/2lT;

    .line 458011
    new-instance v0, LX/2lT;

    const-string v1, "FRESH_HEADER"

    const-string v2, "GroupsFeedTTIFreshHeader"

    invoke-direct {v0, v1, v5, v2}, LX/2lT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2lT;->FRESH_HEADER:LX/2lT;

    .line 458012
    new-instance v0, LX/2lT;

    const-string v1, "CACHED_HEADER"

    const-string v2, "GroupsFeedTTICachedHeader"

    invoke-direct {v0, v1, v6, v2}, LX/2lT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2lT;->CACHED_HEADER:LX/2lT;

    .line 458013
    new-instance v0, LX/2lT;

    const-string v1, "INITIAL_STORIES"

    const-string v2, "GroupsFeedTTIInitialStoriesRendered"

    invoke-direct {v0, v1, v7, v2}, LX/2lT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2lT;->INITIAL_STORIES:LX/2lT;

    .line 458014
    const/4 v0, 0x5

    new-array v0, v0, [LX/2lT;

    sget-object v1, LX/2lT;->FRESH_STORIES:LX/2lT;

    aput-object v1, v0, v3

    sget-object v1, LX/2lT;->CACHED_STORIES:LX/2lT;

    aput-object v1, v0, v4

    sget-object v1, LX/2lT;->FRESH_HEADER:LX/2lT;

    aput-object v1, v0, v5

    sget-object v1, LX/2lT;->CACHED_HEADER:LX/2lT;

    aput-object v1, v0, v6

    sget-object v1, LX/2lT;->INITIAL_STORIES:LX/2lT;

    aput-object v1, v0, v7

    sput-object v0, LX/2lT;->$VALUES:[LX/2lT;

    .line 458015
    new-instance v0, LX/2lU;

    invoke-direct {v0}, LX/2lU;-><init>()V

    sput-object v0, LX/2lT;->GROUPS_FEED_TTI_SEQUENCE:LX/2lU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 458016
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 458017
    iput-object p3, p0, LX/2lT;->mName:Ljava/lang/String;

    .line 458018
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2lT;
    .locals 1

    .prologue
    .line 458019
    const-class v0, LX/2lT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2lT;

    return-object v0
.end method

.method public static values()[LX/2lT;
    .locals 1

    .prologue
    .line 458020
    sget-object v0, LX/2lT;->$VALUES:[LX/2lT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2lT;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 458021
    iget-object v0, p0, LX/2lT;->mName:Ljava/lang/String;

    return-object v0
.end method
