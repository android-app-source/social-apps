.class public LX/3Mw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3MV;

.field private final b:LX/3My;

.field private final c:LX/3Mx;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/3N4;

.field private final f:LX/3N8;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6lh;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3N6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3MV;LX/3Mx;LX/3My;LX/0Or;LX/3N4;LX/3N8;LX/0Ot;LX/0Or;LX/0Ot;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/threads/annotations/IsThreadFetchWithoutUsersEnabledGk;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3MV;",
            "LX/3Mx;",
            "LX/3My;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/3N4;",
            "LX/3N8;",
            "LX/0Ot",
            "<",
            "LX/6lh;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3N6;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 556862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 556863
    iput-object p1, p0, LX/3Mw;->a:LX/3MV;

    .line 556864
    iput-object p3, p0, LX/3Mw;->b:LX/3My;

    .line 556865
    iput-object p2, p0, LX/3Mw;->c:LX/3Mx;

    .line 556866
    iput-object p4, p0, LX/3Mw;->d:LX/0Or;

    .line 556867
    iput-object p5, p0, LX/3Mw;->e:LX/3N4;

    .line 556868
    iput-object p6, p0, LX/3Mw;->f:LX/3N8;

    .line 556869
    iput-object p7, p0, LX/3Mw;->g:LX/0Ot;

    .line 556870
    iput-object p8, p0, LX/3Mw;->h:LX/0Or;

    .line 556871
    iput-object p9, p0, LX/3Mw;->i:LX/0Ot;

    .line 556872
    return-void
.end method

.method private static a(LX/5Vt;LX/0P1;Lcom/facebook/user/model/User;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5Vt;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;",
            "Lcom/facebook/user/model/User;",
            ")J"
        }
    .end annotation

    .prologue
    .line 556843
    invoke-interface {p0}, LX/5Vp;->Z()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 556844
    :goto_0
    if-eqz v0, :cond_1

    .line 556845
    invoke-interface {p0}, LX/5Vp;->aa()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 556846
    :goto_1
    return-wide v0

    .line 556847
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 556848
    :cond_1
    iget-object v0, p2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v0

    .line 556849
    invoke-virtual {p1, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 556850
    iget-object v0, p2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v0

    .line 556851
    invoke-virtual {p1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_1

    .line 556852
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_1
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;Lcom/facebook/user/model/User;)J
    .locals 3

    .prologue
    .line 556873
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;->a()LX/5Vu;

    move-result-object v0

    .line 556874
    if-eqz v0, :cond_0

    .line 556875
    iget-object v1, p1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 556876
    invoke-interface {v0}, LX/5Vr;->B()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 556877
    invoke-interface {v0}, LX/5Vr;->S()Ljava/lang/String;

    move-result-object v0

    .line 556878
    if-eqz v0, :cond_0

    .line 556879
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 556880
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/0Px;)LX/0P1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/5ZH;",
            ">;)",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 556881
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 556882
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5ZH;

    .line 556883
    invoke-interface {v0}, LX/5ZH;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonReceiptInfoModel$ActorModel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, LX/5ZH;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonReceiptInfoModel$ActorModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonReceiptInfoModel$ActorModel;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, LX/5ZH;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 556884
    invoke-interface {v0}, LX/5ZH;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 556885
    invoke-interface {v0}, LX/5ZH;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonReceiptInfoModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonReceiptInfoModel$ActorModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 556886
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 556887
    :cond_1
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/3Mw;
    .locals 1

    .prologue
    .line 556888
    invoke-static {p0}, LX/3Mw;->b(LX/0QB;)LX/3Mw;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/2uF;)Lcom/facebook/messaging/model/threads/NicknamesMap;
    .locals 6

    .prologue
    .line 556889
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 556890
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 556891
    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 556892
    :cond_0
    new-instance v1, Lcom/facebook/messaging/model/threads/NicknamesMap;

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/messaging/model/threads/NicknamesMap;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method private a(LX/5Vt;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 556893
    invoke-interface {p1}, LX/5Vp;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 556894
    invoke-interface {p1}, LX/5Vp;->aj()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 556895
    invoke-interface {p1}, LX/5Vp;->aj()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 556896
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 556897
    invoke-interface {p1}, LX/5Vp;->aj()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 556898
    :cond_0
    iget-object v1, p0, LX/3Mw;->c:LX/3Mx;

    .line 556899
    iget-object v2, v1, LX/3Mx;->a:LX/03V;

    const-string v3, "threads_fetch_archived"

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "Unexpected archived thread. Id: "

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 556900
    const-string v0, "archived"

    .line 556901
    :goto_0
    return-object v0

    .line 556902
    :cond_1
    invoke-interface {p1}, LX/5Vp;->y()Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 556903
    invoke-interface {p1}, LX/5Vp;->y()Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 556904
    invoke-interface {p1}, LX/5Vp;->y()Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/6g6;LX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6g6;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 556905
    iget-object v0, p0, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 556906
    iget-wide v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 556907
    invoke-static {v0}, LX/2Mv;->a(Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 556908
    iput-object v0, p0, LX/6g6;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 556909
    return-void
.end method

.method private static a(LX/6g6;LX/2uF;)V
    .locals 9
    .param p1    # LX/2uF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 556910
    if-nez p1, :cond_0

    .line 556911
    :goto_0
    return-void

    .line 556912
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 556913
    invoke-virtual {p1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 556914
    invoke-virtual {v3, v2, v8}, LX/15i;->g(II)I

    move-result v4

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 556915
    if-eqz v4, :cond_1

    .line 556916
    invoke-virtual {v3, v2, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lcom/facebook/messaging/model/threads/ThreadGameData;

    invoke-virtual {v3, v4, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v8}, LX/15i;->j(II)I

    move-result v3

    invoke-direct {v5, v6, v3}, Lcom/facebook/messaging/model/threads/ThreadGameData;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v2, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_1

    .line 556917
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 556918
    :cond_2
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 556919
    iput-object v0, p0, LX/6g6;->O:Ljava/util/Map;

    .line 556920
    goto :goto_0
.end method

.method private static a(LX/6g6;LX/5Vt;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;)V
    .locals 10
    .param p2    # Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 556921
    invoke-static {}, Lcom/facebook/messaging/model/threads/RoomThreadData;->newBuilder()LX/6fm;

    move-result-object v0

    invoke-interface {p1}, LX/5Vp;->v()Ljava/lang/String;

    move-result-object v3

    .line 556922
    iput-object v3, v0, LX/6fm;->f:Ljava/lang/String;

    .line 556923
    move-object v3, v0

    .line 556924
    if-nez p2, :cond_0

    .line 556925
    invoke-virtual {v3}, LX/6fm;->a()Lcom/facebook/messaging/model/threads/RoomThreadData;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/RoomThreadData;)LX/6g6;

    .line 556926
    :goto_0
    return-void

    .line 556927
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;->a()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    .line 556928
    :goto_1
    iput-boolean v0, v3, LX/6fm;->d:Z

    .line 556929
    move-object v4, v3

    .line 556930
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;->c()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    .line 556931
    :goto_2
    iput-boolean v0, v4, LX/6fm;->c:Z

    .line 556932
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;->e()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 556933
    if-eqz v0, :cond_2

    .line 556934
    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 556935
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 556936
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 556937
    iput-object v5, v3, LX/6fm;->a:Landroid/net/Uri;

    .line 556938
    :cond_1
    invoke-virtual {v4, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v1, :cond_6

    .line 556939
    :goto_3
    iput-boolean v1, v3, LX/6fm;->b:Z

    .line 556940
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ApprovalRequestsModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 556941
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 556942
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ApprovalRequestsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ApprovalRequestsModel;->a()LX/0Px;

    move-result-object v4

    .line 556943
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    :goto_4
    if-ge v2, v5, :cond_7

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ApprovalRequestsModel$EdgesModel;

    .line 556944
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ApprovalRequestsModel$EdgesModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ApprovalRequestsModel$EdgesModel$NodeModel;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ApprovalRequestsModel$EdgesModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ApprovalRequestsModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ApprovalRequestsModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 556945
    new-instance v6, Lcom/facebook/messaging/model/threads/ThreadJoinRequest;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ApprovalRequestsModel$EdgesModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ApprovalRequestsModel$EdgesModel$NodeModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ApprovalRequestsModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ApprovalRequestsModel$EdgesModel;->b()J

    move-result-wide v8

    invoke-direct {v6, v7, v8, v9}, Lcom/facebook/messaging/model/threads/ThreadJoinRequest;-><init>(Lcom/facebook/user/model/UserKey;J)V

    invoke-virtual {v1, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 556946
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_4
    move v0, v2

    .line 556947
    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    .line 556948
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    move v1, v2

    .line 556949
    goto :goto_3

    .line 556950
    :cond_7
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/6fm;->a(LX/0Px;)LX/6fm;

    .line 556951
    :cond_8
    invoke-virtual {v3}, LX/6fm;->a()Lcom/facebook/messaging/model/threads/RoomThreadData;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/RoomThreadData;)LX/6g6;

    goto/16 :goto_0
.end method

.method private static a(LX/6g6;Lcom/facebook/user/model/User;LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6g6;",
            "Lcom/facebook/user/model/User;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/graphql/threads/UserInfoInterfaces$MessagingActorInfo$;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 556853
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 556854
    :cond_0
    :goto_0
    return-void

    .line 556855
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;

    invoke-static {v0, p1}, LX/3Mw;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;Lcom/facebook/user/model/User;)J

    move-result-wide v0

    .line 556856
    cmp-long v2, v0, v4

    if-nez v2, :cond_2

    .line 556857
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;

    invoke-static {v0, p1}, LX/3Mw;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;Lcom/facebook/user/model/User;)J

    move-result-wide v0

    .line 556858
    :cond_2
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 556859
    invoke-static {v0, v1}, LX/2Mv;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 556860
    iput-object v0, p0, LX/6g6;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 556861
    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 556837
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->dd_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 556838
    :cond_0
    :goto_0
    return v0

    .line 556839
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 556840
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v1

    .line 556841
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 556842
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(LX/0Px;)LX/0P1;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesInterfaces$ThreadInfo$ReadReceipts$Nodes;",
            ">;)",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 556829
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 556830
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel;

    .line 556831
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonReceiptInfoModel$ActorModel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonReceiptInfoModel$ActorModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonReceiptInfoModel$ActorModel;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 556832
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 556833
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 556834
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonReceiptInfoModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonReceiptInfoModel$ActorModel;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v6, Landroid/util/Pair;

    invoke-direct {v6, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v0, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 556835
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 556836
    :cond_1
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3Mw;
    .locals 10

    .prologue
    .line 556824
    new-instance v0, LX/3Mw;

    invoke-static {p0}, LX/3MV;->a(LX/0QB;)LX/3MV;

    move-result-object v1

    check-cast v1, LX/3MV;

    invoke-static {p0}, LX/3Mx;->b(LX/0QB;)LX/3Mx;

    move-result-object v2

    check-cast v2, LX/3Mx;

    invoke-static {p0}, LX/3My;->b(LX/0QB;)LX/3My;

    move-result-object v3

    check-cast v3, LX/3My;

    const/16 v4, 0x1532

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/3N4;->a(LX/0QB;)LX/3N4;

    move-result-object v5

    check-cast v5, LX/3N4;

    .line 556825
    new-instance v7, LX/3N8;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-direct {v7, v6}, LX/3N8;-><init>(LX/0Uh;)V

    .line 556826
    move-object v6, v7

    .line 556827
    check-cast v6, LX/3N8;

    const/16 v7, 0x2a46

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x19e

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0xce4

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, LX/3Mw;-><init>(LX/3MV;LX/3Mx;LX/3My;LX/0Or;LX/3N4;LX/3N8;LX/0Ot;LX/0Or;LX/0Ot;)V

    .line 556828
    return-object v0
.end method

.method private b(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 556817
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 556818
    :cond_0
    const/4 v0, 0x0

    .line 556819
    :goto_0
    return-object v0

    .line 556820
    :cond_1
    iget-object v0, p0, LX/3Mw;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 556821
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 556822
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 556823
    iget-object v0, p0, LX/3Mw;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6lh;

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->dd_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v1

    invoke-static {v1}, LX/6lg;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)LX/6lg;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, LX/6lh;->a(LX/6lg;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->dd_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v2

    invoke-static {v1, v2}, LX/6lg;->a(Ljava/lang/String;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)LX/6lg;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/5Vt;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 556800
    invoke-interface {p1}, LX/5Vp;->aj()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v2

    .line 556801
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 556802
    if-nez v0, :cond_1

    .line 556803
    iget-object v0, p0, LX/3Mw;->c:LX/3Mx;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ThreadKey is invalid: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, LX/5Vp;->B()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/3Mx;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 556804
    :goto_1
    return-object v0

    .line 556805
    :cond_1
    invoke-interface {p1}, LX/5Vp;->F()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 556806
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 556807
    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_1

    .line 556808
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 556809
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Thread key is missing other user id. "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "Folder: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, LX/5Vp;->y()Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, LX/5Vp;->y()Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", Is archived: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, LX/5Vp;->A()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", Fbid: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 556810
    iget-object v2, p0, LX/3Mw;->c:LX/3Mx;

    invoke-virtual {v2, v0}, LX/3Mx;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 556811
    goto :goto_1

    .line 556812
    :cond_3
    const-string v0, "null"

    goto :goto_2

    .line 556813
    :cond_4
    iget-object v0, p2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v0

    .line 556814
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 556815
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 556816
    invoke-static {v2, v3, v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/5Vt;LX/0P1;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 17
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/5Vt;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/user/model/User;",
            ")",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;"
        }
    .end annotation

    .prologue
    .line 556598
    invoke-interface/range {p2 .. p2}, LX/5Vp;->at()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    move-result-object v2

    if-nez v2, :cond_0

    .line 556599
    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v5

    .line 556600
    :goto_0
    invoke-interface/range {p2 .. p2}, LX/5Vp;->al()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    move-result-object v2

    if-nez v2, :cond_1

    .line 556601
    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v6

    .line 556602
    :goto_1
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v14

    .line 556603
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 556604
    invoke-interface/range {p2 .. p2}, LX/5Vt;->ac()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;

    move-result-object v15

    .line 556605
    if-eqz v15, :cond_2

    invoke-virtual {v15}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;->d()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 556606
    invoke-virtual {v15}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;->d()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v8

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v8, :cond_2

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ThreadAdminsModel;

    .line 556607
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel$ThreadAdminsModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 556608
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 556609
    :cond_0
    invoke-interface/range {p2 .. p2}, LX/5Vp;->at()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;->a()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/3Mw;->a(LX/0Px;)LX/0P1;

    move-result-object v5

    goto :goto_0

    .line 556610
    :cond_1
    invoke-interface/range {p2 .. p2}, LX/5Vp;->al()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;->a()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/3Mw;->b(LX/0Px;)LX/0P1;

    move-result-object v6

    goto :goto_1

    .line 556611
    :cond_2
    invoke-interface/range {p2 .. p2}, LX/5Vt;->ai()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    move-result-object v2

    if-eqz v2, :cond_e

    .line 556612
    invoke-interface/range {p2 .. p2}, LX/5Vt;->ai()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;->a()LX/0Px;

    move-result-object v2

    .line 556613
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3Mw;->a:LX/3MV;

    invoke-virtual {v3, v2, v5, v6, v7}, LX/3MV;->a(LX/0Px;Ljava/util/Map;Ljava/util/Map;Ljava/util/HashSet;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v14, v2}, LX/6g6;->a(Ljava/util/List;)LX/6g6;

    .line 556614
    :goto_3
    invoke-interface/range {p2 .. p2}, LX/5Vt;->l()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_14

    .line 556615
    invoke-interface/range {p2 .. p2}, LX/5Vt;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 v4, 0x0

    const v5, 0x1f5d54e8

    invoke-static {v3, v2, v4, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 556616
    if-eqz v2, :cond_13

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_4
    invoke-static {v2}, LX/CMm;->a(LX/2uF;)Ljava/util/List;

    move-result-object v2

    .line 556617
    invoke-virtual {v14, v2}, LX/6g6;->b(Ljava/util/List;)LX/6g6;

    .line 556618
    :goto_5
    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, LX/6g6;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/6g6;

    .line 556619
    const-wide/16 v2, -0x1

    invoke-virtual {v14, v2, v3}, LX/6g6;->d(J)LX/6g6;

    .line 556620
    invoke-interface/range {p2 .. p2}, LX/5Vp;->B()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, LX/6g6;->a(Ljava/lang/String;)LX/6g6;

    .line 556621
    invoke-interface/range {p2 .. p2}, LX/5Vp;->U()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 556622
    invoke-interface/range {p2 .. p2}, LX/5Vp;->U()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, LX/6g6;->b(Ljava/lang/String;)LX/6g6;

    .line 556623
    :cond_3
    invoke-interface/range {p2 .. p2}, LX/5Vp;->ar()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-interface/range {p2 .. p2}, LX/5Vp;->ar()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    .line 556624
    invoke-interface/range {p2 .. p2}, LX/5Vp;->ar()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;->a()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;

    .line 556625
    const/4 v10, 0x0

    .line 556626
    const/4 v3, 0x0

    .line 556627
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->dc_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    move-result-object v4

    if-eqz v4, :cond_15

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->dc_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_15

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->dc_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel$MessagingActorModel;

    move-result-object v4

    if-eqz v4, :cond_15

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->dc_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel$MessagingActorModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel$MessagingActorModel;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_15

    .line 556628
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->dc_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;->b()Ljava/lang/String;

    move-result-object v10

    .line 556629
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->dc_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel$MessagingActorModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel$MessagingActorModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 556630
    :cond_4
    :goto_6
    new-instance v4, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v5, Lcom/facebook/user/model/UserKey;

    sget-object v7, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v5, v7, v10}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-direct {v4, v5, v3}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 556631
    invoke-virtual {v14, v4}, LX/6g6;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6g6;

    .line 556632
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 556633
    move-object/from16 v0, p0

    iget-object v7, v0, LX/3Mw;->b:LX/3My;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v8

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->c()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->b()Z

    move-result v3

    if-eqz v3, :cond_16

    :cond_5
    const/4 v11, 0x1

    :goto_7
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->o()Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    move-result-object v12

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->n()Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentMessageType;

    move-result-object v13

    move-object/from16 v9, p4

    invoke-virtual/range {v7 .. v13}, LX/3My;->a(ILcom/facebook/user/model/User;Ljava/lang/String;ZLcom/facebook/graphql/enums/GraphQLPageAdminReplyType;Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentMessageType;)LX/2uW;

    move-result-object v3

    .line 556634
    sget-object v4, LX/2uW;->REGULAR:LX/2uW;

    if-ne v3, v4, :cond_17

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$MessageModel;

    move-result-object v4

    if-eqz v4, :cond_17

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$MessageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_17

    .line 556635
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v4}, LX/6g6;->c(Ljava/lang/String;)LX/6g6;

    .line 556636
    :goto_8
    sget-object v4, LX/2uW;->MISSED_CALL:LX/2uW;

    if-ne v3, v4, :cond_19

    .line 556637
    sget-object v3, LX/6g5;->VOICE_CALL:LX/6g5;

    invoke-virtual {v14, v3}, LX/6g6;->a(LX/6g5;)LX/6g6;

    .line 556638
    :goto_9
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3Mw;->e:LX/3N4;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v2}, LX/3N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v3

    invoke-virtual {v14, v3}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadMediaPreview;)LX/6g6;

    .line 556639
    :cond_6
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->j()Z

    move-result v3

    invoke-virtual {v14, v3}, LX/6g6;->h(Z)LX/6g6;

    .line 556640
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3Mw;->i:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3N6;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->dd_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v5

    invoke-static {v4, v5}, LX/3N6;->a(Ljava/lang/String;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v3

    invoke-virtual {v14, v3}, LX/6g6;->a(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)LX/6g6;

    .line 556641
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->e()Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    move-result-object v3

    if-nez v3, :cond_1b

    const/4 v2, 0x0

    :goto_a
    invoke-virtual {v14, v2}, LX/6g6;->f(Ljava/lang/String;)LX/6g6;

    .line 556642
    :cond_7
    invoke-interface/range {p2 .. p2}, LX/5Vp;->aa()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 556643
    invoke-virtual {v14, v2, v3}, LX/6g6;->e(J)LX/6g6;

    .line 556644
    invoke-static {v2, v3}, LX/6fa;->b(J)J

    move-result-wide v2

    invoke-virtual {v14, v2, v3}, LX/6g6;->c(J)LX/6g6;

    .line 556645
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v6, v1}, LX/3Mw;->a(LX/5Vt;LX/0P1;Lcom/facebook/user/model/User;)J

    move-result-wide v2

    .line 556646
    invoke-virtual {v14, v2, v3}, LX/6g6;->f(J)LX/6g6;

    .line 556647
    invoke-interface/range {p2 .. p2}, LX/5Vp;->P()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v14, v2, v3}, LX/6g6;->g(J)LX/6g6;

    .line 556648
    invoke-interface/range {p2 .. p2}, LX/5Vp;->Z()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v14, v2, v3}, LX/6g6;->h(J)LX/6g6;

    .line 556649
    invoke-interface/range {p2 .. p2}, LX/5Vp;->p()Z

    move-result v2

    invoke-virtual {v14, v2}, LX/6g6;->a(Z)LX/6g6;

    .line 556650
    invoke-interface/range {p2 .. p2}, LX/5Vp;->q()Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    move-result-object v2

    invoke-virtual {v14, v2}, LX/6g6;->a(Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;)LX/6g6;

    .line 556651
    invoke-interface/range {p2 .. p2}, LX/5Vt;->C()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_1d

    invoke-interface/range {p2 .. p2}, LX/5Vt;->C()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1c

    const/4 v2, 0x1

    :goto_b
    if-eqz v2, :cond_8

    .line 556652
    invoke-interface/range {p2 .. p2}, LX/5Vt;->C()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v14, v2}, LX/6g6;->a(Landroid/net/Uri;)LX/6g6;

    .line 556653
    :cond_8
    invoke-interface/range {p2 .. p2}, LX/5Vp;->N()Z

    move-result v2

    invoke-virtual {v14, v2}, LX/6g6;->b(Z)LX/6g6;

    .line 556654
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, LX/3Mw;->a(LX/5Vt;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/6ek;->fromDbName(Ljava/lang/String;)LX/6ek;

    move-result-object v2

    invoke-virtual {v14, v2}, LX/6g6;->a(LX/6ek;)LX/6g6;

    .line 556655
    invoke-interface/range {p2 .. p2}, LX/5Vp;->T()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b(J)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v2

    invoke-virtual {v14, v2}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/NotificationSetting;)LX/6g6;

    .line 556656
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;->MENTIONS_MUTED:Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;

    invoke-interface/range {p2 .. p2}, LX/5Vp;->O()Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v14, v2}, LX/6g6;->e(Z)LX/6g6;

    .line 556657
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;->REACTIONS_MUTED:Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;

    invoke-interface/range {p2 .. p2}, LX/5Vp;->W()Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v14, v2}, LX/6g6;->f(Z)LX/6g6;

    .line 556658
    invoke-interface/range {p2 .. p2}, LX/5Vt;->u()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v6, v2, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 556659
    if-eqz v6, :cond_c

    .line 556660
    const/4 v2, 0x0

    .line 556661
    const/4 v3, 0x0

    .line 556662
    const/4 v4, 0x0

    .line 556663
    const/4 v7, 0x4

    invoke-virtual {v5, v6, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 556664
    const/4 v2, 0x4

    invoke-virtual {v5, v6, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v8

    long-to-int v2, v8

    .line 556665
    :cond_9
    const/4 v7, 0x2

    invoke-virtual {v5, v6, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_a

    .line 556666
    const/4 v3, 0x2

    invoke-virtual {v5, v6, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    const/16 v7, 0x10

    invoke-static {v3, v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v8

    long-to-int v3, v8

    .line 556667
    :cond_a
    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_b

    .line 556668
    const/4 v4, 0x1

    invoke-virtual {v5, v6, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    const/16 v7, 0x10

    invoke-static {v4, v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v8

    long-to-int v4, v8

    .line 556669
    :cond_b
    const/4 v7, 0x3

    const v8, 0x2e06d92b

    invoke-static {v5, v6, v7, v8}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v7

    .line 556670
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadCustomization;->newBuilder()LX/6fr;

    move-result-object v8

    invoke-virtual {v8, v2}, LX/6fr;->a(I)LX/6fr;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/6fr;->b(I)LX/6fr;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/6fr;->c(I)LX/6fr;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v5, v6, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6fr;->a(Ljava/lang/String;)LX/6fr;

    move-result-object v3

    if-eqz v7, :cond_1e

    invoke-static {v7}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_c
    invoke-static {v2}, LX/3Mw;->a(LX/2uF;)Lcom/facebook/messaging/model/threads/NicknamesMap;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/6fr;->a(Lcom/facebook/messaging/model/threads/NicknamesMap;)LX/6fr;

    move-result-object v2

    invoke-virtual {v2}, LX/6fr;->g()Lcom/facebook/messaging/model/threads/ThreadCustomization;

    move-result-object v2

    .line 556671
    invoke-virtual {v14, v2}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadCustomization;)LX/6g6;

    .line 556672
    :cond_c
    invoke-interface/range {p2 .. p2}, LX/5Vp;->x()I

    move-result v2

    invoke-virtual {v14, v2}, LX/6g6;->a(I)LX/6g6;

    .line 556673
    invoke-interface/range {p2 .. p2}, LX/5Vp;->as()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;

    move-result-object v2

    .line 556674
    if-eqz v2, :cond_21

    .line 556675
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 556676
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v2, 0x0

    move v5, v2

    :goto_d
    if-ge v5, v8, :cond_20

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel;

    .line 556677
    new-instance v9, LX/0P2;

    invoke-direct {v9}, LX/0P2;-><init>()V

    .line 556678
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel$EventReminderMembersModel;

    move-result-object v3

    if-eqz v3, :cond_1f

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel$EventReminderMembersModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel$EventReminderMembersModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1f

    .line 556679
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel$EventReminderMembersModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel$EventReminderMembersModel;->a()LX/0Px;

    move-result-object v10

    .line 556680
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    const/4 v3, 0x0

    move v4, v3

    :goto_e
    if-ge v4, v11, :cond_1f

    invoke-virtual {v10, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel$EventReminderMembersModel$EdgesModel;

    .line 556681
    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel$EventReminderMembersModel$EdgesModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel$EventReminderMembersModel$EdgesModel$NodeModel;

    move-result-object v12

    if-eqz v12, :cond_d

    .line 556682
    new-instance v12, Lcom/facebook/user/model/UserKey;

    sget-object v13, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel$EventReminderMembersModel$EdgesModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel$EventReminderMembersModel$EdgesModel$NodeModel;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel$EventReminderMembersModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v12, v13, v0}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel$EventReminderMembersModel$EdgesModel;->a()Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    move-result-object v3

    invoke-virtual {v9, v12, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 556683
    :cond_d
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_e

    .line 556684
    :cond_e
    if-eqz p3, :cond_10

    invoke-interface/range {p2 .. p2}, LX/5Vt;->m()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_f

    const/4 v2, 0x1

    :goto_f
    if-eqz v2, :cond_12

    .line 556685
    invoke-interface/range {p2 .. p2}, LX/5Vt;->m()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 v4, 0x0

    const v8, -0x58e53f56

    invoke-static {v3, v2, v4, v8}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v3

    .line 556686
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Mw;->a:LX/3MV;

    if-eqz v3, :cond_11

    invoke-static {v3}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v3

    :goto_10
    move-object/from16 v4, p3

    invoke-virtual/range {v2 .. v7}, LX/3MV;->a(LX/2uF;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/HashSet;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v14, v2}, LX/6g6;->a(Ljava/util/List;)LX/6g6;

    goto/16 :goto_3

    .line 556687
    :cond_f
    const/4 v2, 0x0

    goto :goto_f

    :cond_10
    const/4 v2, 0x0

    goto :goto_f

    .line 556688
    :cond_11
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v3

    goto :goto_10

    .line 556689
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Mw;->c:LX/3Mx;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid thread participant information: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p2 .. p2}, LX/5Vp;->B()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3Mx;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 556690
    :cond_13
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto/16 :goto_4

    .line 556691
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Mw;->c:LX/3Mx;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid bot thread participant information: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p2 .. p2}, LX/5Vp;->B()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3Mx;->a(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 556692
    :cond_15
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;->c()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 556693
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;->c()Ljava/lang/String;

    move-result-object v10

    .line 556694
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;->d()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    .line 556695
    :cond_16
    const/4 v11, 0x0

    goto/16 :goto_7

    .line 556696
    :cond_17
    sget-object v4, LX/2uW;->REGULAR:LX/2uW;

    if-ne v3, v4, :cond_18

    invoke-static {v2}, LX/3Mw;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 556697
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/3Mw;->b(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v4}, LX/6g6;->d(Ljava/lang/String;)LX/6g6;

    goto/16 :goto_8

    .line 556698
    :cond_18
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v4}, LX/6g6;->d(Ljava/lang/String;)LX/6g6;

    goto/16 :goto_8

    .line 556699
    :cond_19
    sget-object v4, LX/2uW;->MISSED_VIDEO_CALL:LX/2uW;

    if-ne v3, v4, :cond_1a

    .line 556700
    sget-object v3, LX/6g5;->VIDEO_CALL:LX/6g5;

    invoke-virtual {v14, v3}, LX/6g6;->a(LX/6g5;)LX/6g6;

    goto/16 :goto_9

    .line 556701
    :cond_1a
    sget-object v3, LX/6g5;->NONE:LX/6g5;

    invoke-virtual {v14, v3}, LX/6g6;->a(LX/6g5;)LX/6g6;

    goto/16 :goto_9

    .line 556702
    :cond_1b
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->e()Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->name()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_a

    .line 556703
    :cond_1c
    const/4 v2, 0x0

    goto/16 :goto_b

    :cond_1d
    const/4 v2, 0x0

    goto/16 :goto_b

    .line 556704
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 556705
    :cond_1e
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto/16 :goto_c

    .line 556706
    :cond_1f
    new-instance v3, LX/6ft;

    invoke-direct {v3}, LX/6ft;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/6ft;->a(Ljava/lang/String;)LX/6ft;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel;->da_()Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/6ft;->a(Lcom/facebook/graphql/enums/GraphQLLightweightEventType;)LX/6ft;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel;->j()J

    move-result-wide v10

    invoke-virtual {v3, v10, v11}, LX/6ft;->a(J)LX/6ft;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/6ft;->b(Ljava/lang/String;)LX/6ft;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/6ft;->a(Z)LX/6ft;

    move-result-object v3

    invoke-virtual {v9}, LX/0P2;->b()LX/0P1;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/6ft;->a(LX/0P1;)LX/6ft;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel;->b()Z

    move-result v4

    invoke-virtual {v3, v4}, LX/6ft;->b(Z)LX/6ft;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel$NodesModel;->db_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/6ft;->c(Ljava/lang/String;)LX/6ft;

    move-result-object v2

    invoke-virtual {v2}, LX/6ft;->i()Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 556707
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_d

    .line 556708
    :cond_20
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v14, v2}, LX/6g6;->d(Ljava/util/List;)LX/6g6;

    .line 556709
    :cond_21
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Mw;->f:LX/3N8;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v14}, LX/3N8;->a(LX/5Vt;LX/6g6;)V

    .line 556710
    invoke-interface/range {p2 .. p2}, LX/5Vp;->ak()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    move-result-object v2

    .line 556711
    if-eqz v2, :cond_22

    .line 556712
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->newBuilder()LX/6g1;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/6g1;->a(Ljava/lang/String;)LX/6g1;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/6g1;->b(Ljava/lang/String;)LX/6g1;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->b()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    move-result-object v4

    if-eqz v4, :cond_24

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->b()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;->b()Ljava/lang/String;

    move-result-object v2

    :goto_11
    invoke-virtual {v3, v2}, LX/6g1;->c(Ljava/lang/String;)LX/6g1;

    move-result-object v2

    invoke-virtual {v2}, LX/6g1;->d()Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    move-result-object v2

    .line 556713
    invoke-virtual {v14, v2}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;)LX/6g6;

    .line 556714
    :cond_22
    invoke-interface/range {p2 .. p2}, LX/5Vp;->z()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v14, v2}, LX/6g6;->a(F)LX/6g6;

    .line 556715
    invoke-interface/range {p2 .. p2}, LX/5Vp;->Y()Z

    move-result v2

    invoke-static {v2}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v2

    invoke-virtual {v14, v2}, LX/6g6;->a(LX/03R;)LX/6g6;

    .line 556716
    move-object/from16 v0, p2

    invoke-static {v14, v0, v15}, LX/3Mw;->a(LX/6g6;LX/5Vt;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;)V

    .line 556717
    invoke-interface/range {p2 .. p2}, LX/5Vt;->X()LX/2uF;

    move-result-object v2

    invoke-static {v14, v2}, LX/3Mw;->a(LX/6g6;LX/2uF;)V

    .line 556718
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b()Z

    move-result v2

    if-eqz v2, :cond_23

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d()Z

    move-result v2

    if-nez v2, :cond_23

    .line 556719
    if-eqz p3, :cond_25

    .line 556720
    move-object/from16 v0, p3

    invoke-static {v14, v0}, LX/3Mw;->a(LX/6g6;LX/0P1;)V

    .line 556721
    :cond_23
    :goto_12
    invoke-virtual {v14}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    return-object v2

    .line 556722
    :cond_24
    const/4 v2, 0x0

    goto :goto_11

    .line 556723
    :cond_25
    invoke-interface/range {p2 .. p2}, LX/5Vt;->ai()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    move-result-object v2

    if-eqz v2, :cond_23

    .line 556724
    invoke-interface/range {p2 .. p2}, LX/5Vt;->ai()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;->a()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-static {v14, v0, v2}, LX/3Mw;->a(LX/6g6;Lcom/facebook/user/model/User;LX/0Px;)V

    goto :goto_12
.end method

.method public final a(LX/0Px;LX/0P1;ZLcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadsCollection;
    .locals 7
    .param p2    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;Z",
            "Lcom/facebook/user/model/User;",
            ")",
            "Lcom/facebook/messaging/model/threads/ThreadsCollection;"
        }
    .end annotation

    .prologue
    .line 556791
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 556792
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    .line 556793
    invoke-virtual {p0, v0, p4}, LX/3Mw;->a(LX/5Vt;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v4

    .line 556794
    if-eqz v4, :cond_0

    .line 556795
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->y()Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    if-ne v5, v6, :cond_1

    .line 556796
    iget-object v0, p0, LX/3Mw;->c:LX/3Mx;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/3Mx;->a(Ljava/lang/Long;)V

    .line 556797
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 556798
    :cond_1
    invoke-virtual {p0, v4, v0, p2, p4}, LX/3Mw;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/5Vt;LX/0P1;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 556799
    :cond_2
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    return-object v0
.end method

.method public final a(LX/0Px;ZLcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadsCollection;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;",
            ">;Z",
            "Lcom/facebook/user/model/User;",
            ")",
            "Lcom/facebook/messaging/model/threads/ThreadsCollection;"
        }
    .end annotation

    .prologue
    .line 556781
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 556782
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    .line 556783
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->p()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->y()Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->OTHER:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    invoke-virtual {v4, v5}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 556784
    :cond_0
    invoke-virtual {p0, v0, p3}, LX/3Mw;->a(LX/5Vt;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v4

    .line 556785
    if-eqz v4, :cond_1

    .line 556786
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->y()Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    if-ne v5, v6, :cond_2

    .line 556787
    iget-object v0, p0, LX/3Mw;->c:LX/3Mx;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/3Mx;->a(Ljava/lang/Long;)V

    .line 556788
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 556789
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {p0, v4, v0, v5, p3}, LX/3Mw;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/5Vt;LX/0P1;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 556790
    :cond_3
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel$PinnedMessageThreadsModel;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;
    .locals 11
    .param p1    # Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel$PinnedMessageThreadsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 556752
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 556753
    if-eqz p1, :cond_3

    .line 556754
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel$PinnedMessageThreadsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    .line 556755
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->n()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v6

    if-nez v6, :cond_1

    .line 556756
    iget-object v6, p0, LX/3Mw;->c:LX/3Mx;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "ThreadKey is null: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/3Mx;->a(Ljava/lang/String;)V

    .line 556757
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 556758
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->F()Z

    move-result v6

    if-nez v6, :cond_2

    .line 556759
    iget-object v6, p0, LX/3Mw;->c:LX/3Mx;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->n()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 556760
    iget-object v7, v6, LX/3Mx;->a:LX/03V;

    const-string v8, "pinned_threads_fetch"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Pinned thread is not a group thread. One to one with user: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 556761
    goto :goto_1

    .line 556762
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->A()Z

    move-result v6

    if-nez v6, :cond_0

    .line 556763
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->n()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v0, v7, p2}, LX/3Mw;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/5Vt;LX/0P1;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 556764
    :cond_3
    iget-object v0, p0, LX/3Mw;->c:LX/3Mx;

    .line 556765
    iget-object v2, v0, LX/3Mx;->a:LX/03V;

    const-string v4, "pinned_threads_fetch"

    const-string v5, "Graphql pinned_message_threads is null"

    invoke-virtual {v2, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 556766
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 556767
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->newBuilder()LX/6hv;

    move-result-object v2

    .line 556768
    iput-object v0, v2, LX/6hv;->a:Ljava/util/List;

    .line 556769
    move-object v2, v2

    .line 556770
    if-eqz p1, :cond_5

    const/4 v0, 0x1

    .line 556771
    :goto_2
    iput-boolean v0, v2, LX/6hv;->b:Z

    .line 556772
    move-object v2, v2

    .line 556773
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel$PinnedMessageThreadsModel;->j()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    .line 556774
    :goto_3
    iput-wide v0, v2, LX/6hv;->c:J

    .line 556775
    move-object v0, v2

    .line 556776
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 556777
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    .line 556778
    iput-wide v2, v0, LX/6hv;->d:J

    .line 556779
    move-object v0, v0

    .line 556780
    invoke-virtual {v0}, LX/6hv;->e()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v0

    return-object v0

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    const-wide/16 v0, 0x0

    goto :goto_3
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;Lcom/facebook/user/model/User;LX/0Px;)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 9
    .param p4    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;",
            "Lcom/facebook/user/model/User;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "Lcom/facebook/messaging/service/model/FetchThreadResult;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 556725
    iget-object v0, p0, LX/3Mw;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 556726
    if-eqz v2, :cond_0

    if-nez p4, :cond_4

    .line 556727
    :cond_0
    invoke-virtual {p0, p1, p2, v1, p3}, LX/3Mw;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/5Vt;LX/0P1;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 556728
    :goto_0
    iget-object v1, p0, LX/3Mw;->b:LX/3My;

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;

    move-result-object v3

    invoke-virtual {v1, p1, v3, p3}, LX/3My;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v3

    .line 556729
    if-nez p4, :cond_1

    .line 556730
    iget-object v1, p0, LX/3Mw;->a:LX/3MV;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3MV;->a(Ljava/util/List;)LX/0Px;

    move-result-object v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    .line 556731
    :goto_1
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v2

    sget-object v4, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 556732
    iput-object v4, v2, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 556733
    move-object v2, v2

    .line 556734
    iput-object v1, v2, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 556735
    move-object v1, v2

    .line 556736
    iput-object v3, v1, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 556737
    move-object v1, v1

    .line 556738
    iput-object v0, v1, LX/6iO;->e:LX/0Px;

    .line 556739
    move-object v0, v1

    .line 556740
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 556741
    iput-wide v2, v0, LX/6iO;->g:J

    .line 556742
    move-object v0, v0

    .line 556743
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    return-object v0

    .line 556744
    :cond_1
    invoke-static {p3, p4}, LX/FFv;->a(Lcom/facebook/user/model/User;LX/0Px;)LX/0Px;

    move-result-object v1

    .line 556745
    if-eqz v2, :cond_3

    .line 556746
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 556747
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v5, :cond_2

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 556748
    iget-object v6, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v6, v6

    .line 556749
    invoke-interface {v4, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556750
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 556751
    :cond_2
    invoke-static {v4}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, p3}, LX/3Mw;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/5Vt;LX/0P1;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_1

    :cond_3
    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method
