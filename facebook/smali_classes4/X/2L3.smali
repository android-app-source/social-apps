.class public LX/2L3;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2L3;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 394663
    const-string v0, "offline_mode"

    const/16 v1, 0x10

    new-instance v2, LX/2L4;

    invoke-direct {v2}, LX/2L4;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 394664
    return-void
.end method

.method public static a(LX/0QB;)LX/2L3;
    .locals 3

    .prologue
    .line 394665
    sget-object v0, LX/2L3;->a:LX/2L3;

    if-nez v0, :cond_1

    .line 394666
    const-class v1, LX/2L3;

    monitor-enter v1

    .line 394667
    :try_start_0
    sget-object v0, LX/2L3;->a:LX/2L3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 394668
    if-eqz v2, :cond_0

    .line 394669
    :try_start_1
    new-instance v0, LX/2L3;

    invoke-direct {v0}, LX/2L3;-><init>()V

    .line 394670
    move-object v0, v0

    .line 394671
    sput-object v0, LX/2L3;->a:LX/2L3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394672
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 394673
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 394674
    :cond_1
    sget-object v0, LX/2L3;->a:LX/2L3;

    return-object v0

    .line 394675
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 394676
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
