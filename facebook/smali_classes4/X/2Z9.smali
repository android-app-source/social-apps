.class public LX/2Z9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;


# instance fields
.field public final inForegroundApp:Ljava/lang/Boolean;

.field public final inForegroundDevice:Ljava/lang/Boolean;

.field public final keepAliveTimeout:Ljava/lang/Integer;

.field public final subscribeGenericTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6mf;",
            ">;"
        }
    .end annotation
.end field

.field public final subscribeTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final unsubscribeGenericTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final unsubscribeTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x2

    const/16 v4, 0xf

    .line 421911
    new-instance v0, LX/1sv;

    const-string v1, "ForegroundState"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/2Z9;->b:LX/1sv;

    .line 421912
    new-instance v0, LX/1sw;

    const-string v1, "inForegroundApp"

    invoke-direct {v0, v1, v2, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/2Z9;->c:LX/1sw;

    .line 421913
    new-instance v0, LX/1sw;

    const-string v1, "inForegroundDevice"

    invoke-direct {v0, v1, v2, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/2Z9;->d:LX/1sw;

    .line 421914
    new-instance v0, LX/1sw;

    const-string v1, "keepAliveTimeout"

    const/16 v2, 0x8

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/2Z9;->e:LX/1sw;

    .line 421915
    new-instance v0, LX/1sw;

    const-string v1, "subscribeTopics"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/2Z9;->f:LX/1sw;

    .line 421916
    new-instance v0, LX/1sw;

    const-string v1, "subscribeGenericTopics"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/2Z9;->g:LX/1sw;

    .line 421917
    new-instance v0, LX/1sw;

    const-string v1, "unsubscribeTopics"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/2Z9;->h:LX/1sw;

    .line 421918
    new-instance v0, LX/1sw;

    const-string v1, "unsubscribeGenericTopics"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/2Z9;->i:LX/1sw;

    .line 421919
    sput-boolean v5, LX/2Z9;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/6mf;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 422097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422098
    iput-object p1, p0, LX/2Z9;->inForegroundApp:Ljava/lang/Boolean;

    .line 422099
    iput-object p2, p0, LX/2Z9;->inForegroundDevice:Ljava/lang/Boolean;

    .line 422100
    iput-object p3, p0, LX/2Z9;->keepAliveTimeout:Ljava/lang/Integer;

    .line 422101
    iput-object p4, p0, LX/2Z9;->subscribeTopics:Ljava/util/List;

    .line 422102
    iput-object p5, p0, LX/2Z9;->subscribeGenericTopics:Ljava/util/List;

    .line 422103
    iput-object p6, p0, LX/2Z9;->unsubscribeTopics:Ljava/util/List;

    .line 422104
    iput-object p7, p0, LX/2Z9;->unsubscribeGenericTopics:Ljava/util/List;

    .line 422105
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 422021
    if-eqz p2, :cond_c

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 422022
    :goto_0
    if-eqz p2, :cond_d

    const-string v0, "\n"

    move-object v3, v0

    .line 422023
    :goto_1
    if-eqz p2, :cond_e

    const-string v0, " "

    .line 422024
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "ForegroundState"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 422025
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422026
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422027
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422028
    const/4 v1, 0x1

    .line 422029
    iget-object v6, p0, LX/2Z9;->inForegroundApp:Ljava/lang/Boolean;

    if-eqz v6, :cond_0

    .line 422030
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422031
    const-string v1, "inForegroundApp"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422032
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422033
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422034
    iget-object v1, p0, LX/2Z9;->inForegroundApp:Ljava/lang/Boolean;

    if-nez v1, :cond_f

    .line 422035
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 422036
    :cond_0
    iget-object v6, p0, LX/2Z9;->inForegroundDevice:Ljava/lang/Boolean;

    if-eqz v6, :cond_2

    .line 422037
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422038
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422039
    const-string v1, "inForegroundDevice"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422040
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422041
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422042
    iget-object v1, p0, LX/2Z9;->inForegroundDevice:Ljava/lang/Boolean;

    if-nez v1, :cond_10

    .line 422043
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 422044
    :cond_2
    iget-object v6, p0, LX/2Z9;->keepAliveTimeout:Ljava/lang/Integer;

    if-eqz v6, :cond_4

    .line 422045
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422046
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422047
    const-string v1, "keepAliveTimeout"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422048
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422049
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422050
    iget-object v1, p0, LX/2Z9;->keepAliveTimeout:Ljava/lang/Integer;

    if-nez v1, :cond_11

    .line 422051
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 422052
    :cond_4
    iget-object v6, p0, LX/2Z9;->subscribeTopics:Ljava/util/List;

    if-eqz v6, :cond_6

    .line 422053
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422054
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422055
    const-string v1, "subscribeTopics"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422056
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422057
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422058
    iget-object v1, p0, LX/2Z9;->subscribeTopics:Ljava/util/List;

    if-nez v1, :cond_12

    .line 422059
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    move v1, v2

    .line 422060
    :cond_6
    iget-object v6, p0, LX/2Z9;->subscribeGenericTopics:Ljava/util/List;

    if-eqz v6, :cond_8

    .line 422061
    if-nez v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422062
    :cond_7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422063
    const-string v1, "subscribeGenericTopics"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422064
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422065
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422066
    iget-object v1, p0, LX/2Z9;->subscribeGenericTopics:Ljava/util/List;

    if-nez v1, :cond_13

    .line 422067
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    move v1, v2

    .line 422068
    :cond_8
    iget-object v6, p0, LX/2Z9;->unsubscribeTopics:Ljava/util/List;

    if-eqz v6, :cond_16

    .line 422069
    if-nez v1, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422070
    :cond_9
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422071
    const-string v1, "unsubscribeTopics"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422072
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422073
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422074
    iget-object v1, p0, LX/2Z9;->unsubscribeTopics:Ljava/util/List;

    if-nez v1, :cond_14

    .line 422075
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422076
    :goto_8
    iget-object v1, p0, LX/2Z9;->unsubscribeGenericTopics:Ljava/util/List;

    if-eqz v1, :cond_b

    .line 422077
    if-nez v2, :cond_a

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422078
    :cond_a
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422079
    const-string v1, "unsubscribeGenericTopics"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422080
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422081
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422082
    iget-object v0, p0, LX/2Z9;->unsubscribeGenericTopics:Ljava/util/List;

    if-nez v0, :cond_15

    .line 422083
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422084
    :cond_b
    :goto_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422085
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422086
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 422087
    :cond_c
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 422088
    :cond_d
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 422089
    :cond_e
    const-string v0, ""

    goto/16 :goto_2

    .line 422090
    :cond_f
    iget-object v1, p0, LX/2Z9;->inForegroundApp:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 422091
    :cond_10
    iget-object v1, p0, LX/2Z9;->inForegroundDevice:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 422092
    :cond_11
    iget-object v1, p0, LX/2Z9;->keepAliveTimeout:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 422093
    :cond_12
    iget-object v1, p0, LX/2Z9;->subscribeTopics:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 422094
    :cond_13
    iget-object v1, p0, LX/2Z9;->subscribeGenericTopics:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 422095
    :cond_14
    iget-object v1, p0, LX/2Z9;->unsubscribeTopics:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 422096
    :cond_15
    iget-object v0, p0, LX/2Z9;->unsubscribeGenericTopics:Ljava/util/List;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    :cond_16
    move v2, v1

    goto/16 :goto_8
.end method

.method public final a(LX/1su;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 421981
    invoke-virtual {p1}, LX/1su;->a()V

    .line 421982
    iget-object v0, p0, LX/2Z9;->inForegroundApp:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 421983
    iget-object v0, p0, LX/2Z9;->inForegroundApp:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 421984
    sget-object v0, LX/2Z9;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 421985
    iget-object v0, p0, LX/2Z9;->inForegroundApp:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 421986
    :cond_0
    iget-object v0, p0, LX/2Z9;->inForegroundDevice:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 421987
    iget-object v0, p0, LX/2Z9;->inForegroundDevice:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 421988
    sget-object v0, LX/2Z9;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 421989
    iget-object v0, p0, LX/2Z9;->inForegroundDevice:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 421990
    :cond_1
    iget-object v0, p0, LX/2Z9;->keepAliveTimeout:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 421991
    iget-object v0, p0, LX/2Z9;->keepAliveTimeout:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 421992
    sget-object v0, LX/2Z9;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 421993
    iget-object v0, p0, LX/2Z9;->keepAliveTimeout:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 421994
    :cond_2
    iget-object v0, p0, LX/2Z9;->subscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 421995
    iget-object v0, p0, LX/2Z9;->subscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 421996
    sget-object v0, LX/2Z9;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 421997
    new-instance v0, LX/1u3;

    iget-object v1, p0, LX/2Z9;->subscribeTopics:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v3, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 421998
    iget-object v0, p0, LX/2Z9;->subscribeTopics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 421999
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    goto :goto_0

    .line 422000
    :cond_3
    iget-object v0, p0, LX/2Z9;->subscribeGenericTopics:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 422001
    iget-object v0, p0, LX/2Z9;->subscribeGenericTopics:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 422002
    sget-object v0, LX/2Z9;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 422003
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/2Z9;->subscribeGenericTopics:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 422004
    iget-object v0, p0, LX/2Z9;->subscribeGenericTopics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6mf;

    .line 422005
    invoke-virtual {v0, p1}, LX/6mf;->a(LX/1su;)V

    goto :goto_1

    .line 422006
    :cond_4
    iget-object v0, p0, LX/2Z9;->unsubscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 422007
    iget-object v0, p0, LX/2Z9;->unsubscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 422008
    sget-object v0, LX/2Z9;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 422009
    new-instance v0, LX/1u3;

    iget-object v1, p0, LX/2Z9;->unsubscribeTopics:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v3, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 422010
    iget-object v0, p0, LX/2Z9;->unsubscribeTopics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 422011
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    goto :goto_2

    .line 422012
    :cond_5
    iget-object v0, p0, LX/2Z9;->unsubscribeGenericTopics:Ljava/util/List;

    if-eqz v0, :cond_6

    .line 422013
    iget-object v0, p0, LX/2Z9;->unsubscribeGenericTopics:Ljava/util/List;

    if-eqz v0, :cond_6

    .line 422014
    sget-object v0, LX/2Z9;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 422015
    new-instance v0, LX/1u3;

    const/16 v1, 0xb

    iget-object v2, p0, LX/2Z9;->unsubscribeGenericTopics:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 422016
    iget-object v0, p0, LX/2Z9;->unsubscribeGenericTopics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 422017
    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 422018
    :cond_6
    invoke-virtual {p1}, LX/1su;->c()V

    .line 422019
    invoke-virtual {p1}, LX/1su;->b()V

    .line 422020
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 421924
    if-nez p1, :cond_1

    .line 421925
    :cond_0
    :goto_0
    return v0

    .line 421926
    :cond_1
    instance-of v1, p1, LX/2Z9;

    if-eqz v1, :cond_0

    .line 421927
    check-cast p1, LX/2Z9;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 421928
    if-nez p1, :cond_3

    .line 421929
    :cond_2
    :goto_1
    move v0, v2

    .line 421930
    goto :goto_0

    .line 421931
    :cond_3
    iget-object v0, p0, LX/2Z9;->inForegroundApp:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    move v0, v1

    .line 421932
    :goto_2
    iget-object v3, p1, LX/2Z9;->inForegroundApp:Ljava/lang/Boolean;

    if-eqz v3, :cond_13

    move v3, v1

    .line 421933
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 421934
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 421935
    iget-object v0, p0, LX/2Z9;->inForegroundApp:Ljava/lang/Boolean;

    iget-object v3, p1, LX/2Z9;->inForegroundApp:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 421936
    :cond_5
    iget-object v0, p0, LX/2Z9;->inForegroundDevice:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    move v0, v1

    .line 421937
    :goto_4
    iget-object v3, p1, LX/2Z9;->inForegroundDevice:Ljava/lang/Boolean;

    if-eqz v3, :cond_15

    move v3, v1

    .line 421938
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 421939
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 421940
    iget-object v0, p0, LX/2Z9;->inForegroundDevice:Ljava/lang/Boolean;

    iget-object v3, p1, LX/2Z9;->inForegroundDevice:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 421941
    :cond_7
    iget-object v0, p0, LX/2Z9;->keepAliveTimeout:Ljava/lang/Integer;

    if-eqz v0, :cond_16

    move v0, v1

    .line 421942
    :goto_6
    iget-object v3, p1, LX/2Z9;->keepAliveTimeout:Ljava/lang/Integer;

    if-eqz v3, :cond_17

    move v3, v1

    .line 421943
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 421944
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 421945
    iget-object v0, p0, LX/2Z9;->keepAliveTimeout:Ljava/lang/Integer;

    iget-object v3, p1, LX/2Z9;->keepAliveTimeout:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 421946
    :cond_9
    iget-object v0, p0, LX/2Z9;->subscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_18

    move v0, v1

    .line 421947
    :goto_8
    iget-object v3, p1, LX/2Z9;->subscribeTopics:Ljava/util/List;

    if-eqz v3, :cond_19

    move v3, v1

    .line 421948
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 421949
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 421950
    iget-object v0, p0, LX/2Z9;->subscribeTopics:Ljava/util/List;

    iget-object v3, p1, LX/2Z9;->subscribeTopics:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 421951
    :cond_b
    iget-object v0, p0, LX/2Z9;->subscribeGenericTopics:Ljava/util/List;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 421952
    :goto_a
    iget-object v3, p1, LX/2Z9;->subscribeGenericTopics:Ljava/util/List;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 421953
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 421954
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 421955
    iget-object v0, p0, LX/2Z9;->subscribeGenericTopics:Ljava/util/List;

    iget-object v3, p1, LX/2Z9;->subscribeGenericTopics:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 421956
    :cond_d
    iget-object v0, p0, LX/2Z9;->unsubscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 421957
    :goto_c
    iget-object v3, p1, LX/2Z9;->unsubscribeTopics:Ljava/util/List;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 421958
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 421959
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 421960
    iget-object v0, p0, LX/2Z9;->unsubscribeTopics:Ljava/util/List;

    iget-object v3, p1, LX/2Z9;->unsubscribeTopics:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 421961
    :cond_f
    iget-object v0, p0, LX/2Z9;->unsubscribeGenericTopics:Ljava/util/List;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 421962
    :goto_e
    iget-object v3, p1, LX/2Z9;->unsubscribeGenericTopics:Ljava/util/List;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 421963
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 421964
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 421965
    iget-object v0, p0, LX/2Z9;->unsubscribeGenericTopics:Ljava/util/List;

    iget-object v3, p1, LX/2Z9;->unsubscribeGenericTopics:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_11
    move v2, v1

    .line 421966
    goto/16 :goto_1

    :cond_12
    move v0, v2

    .line 421967
    goto/16 :goto_2

    :cond_13
    move v3, v2

    .line 421968
    goto/16 :goto_3

    :cond_14
    move v0, v2

    .line 421969
    goto/16 :goto_4

    :cond_15
    move v3, v2

    .line 421970
    goto/16 :goto_5

    :cond_16
    move v0, v2

    .line 421971
    goto/16 :goto_6

    :cond_17
    move v3, v2

    .line 421972
    goto/16 :goto_7

    :cond_18
    move v0, v2

    .line 421973
    goto/16 :goto_8

    :cond_19
    move v3, v2

    .line 421974
    goto :goto_9

    :cond_1a
    move v0, v2

    .line 421975
    goto :goto_a

    :cond_1b
    move v3, v2

    .line 421976
    goto :goto_b

    :cond_1c
    move v0, v2

    .line 421977
    goto :goto_c

    :cond_1d
    move v3, v2

    .line 421978
    goto :goto_d

    :cond_1e
    move v0, v2

    .line 421979
    goto :goto_e

    :cond_1f
    move v3, v2

    .line 421980
    goto :goto_f
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 421923
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 421920
    sget-boolean v0, LX/2Z9;->a:Z

    .line 421921
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/2Z9;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 421922
    return-object v0
.end method
