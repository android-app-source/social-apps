.class public final enum LX/3EA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3EA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3EA;

.field public static final enum ORIGINAL:LX/3EA;

.field public static final enum SUBSEQUENT:LX/3EA;

.field public static final enum VIEWABILITY:LX/3EA;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 537094
    new-instance v0, LX/3EA;

    const-string v1, "ORIGINAL"

    invoke-direct {v0, v1, v2}, LX/3EA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3EA;->ORIGINAL:LX/3EA;

    .line 537095
    new-instance v0, LX/3EA;

    const-string v1, "SUBSEQUENT"

    invoke-direct {v0, v1, v3}, LX/3EA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3EA;->SUBSEQUENT:LX/3EA;

    .line 537096
    new-instance v0, LX/3EA;

    const-string v1, "VIEWABILITY"

    invoke-direct {v0, v1, v4}, LX/3EA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3EA;->VIEWABILITY:LX/3EA;

    .line 537097
    const/4 v0, 0x3

    new-array v0, v0, [LX/3EA;

    sget-object v1, LX/3EA;->ORIGINAL:LX/3EA;

    aput-object v1, v0, v2

    sget-object v1, LX/3EA;->SUBSEQUENT:LX/3EA;

    aput-object v1, v0, v3

    sget-object v1, LX/3EA;->VIEWABILITY:LX/3EA;

    aput-object v1, v0, v4

    sput-object v0, LX/3EA;->$VALUES:[LX/3EA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 537098
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3EA;
    .locals 1

    .prologue
    .line 537099
    const-class v0, LX/3EA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3EA;

    return-object v0
.end method

.method public static values()[LX/3EA;
    .locals 1

    .prologue
    .line 537100
    sget-object v0, LX/3EA;->$VALUES:[LX/3EA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3EA;

    return-object v0
.end method
