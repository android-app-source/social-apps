.class public LX/354;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/355;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/354",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/355;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 496551
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 496552
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/354;->b:LX/0Zi;

    .line 496553
    iput-object p1, p0, LX/354;->a:LX/0Ot;

    .line 496554
    return-void
.end method

.method public static a(LX/0QB;)LX/354;
    .locals 4

    .prologue
    .line 496555
    const-class v1, LX/354;

    monitor-enter v1

    .line 496556
    :try_start_0
    sget-object v0, LX/354;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 496557
    sput-object v2, LX/354;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 496558
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496559
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 496560
    new-instance v3, LX/354;

    const/16 p0, 0x9bc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/354;-><init>(LX/0Ot;)V

    .line 496561
    move-object v0, v3

    .line 496562
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 496563
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/354;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496564
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 496565
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 496566
    check-cast p2, LX/CAh;

    .line 496567
    iget-object v0, p0, LX/354;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/355;

    iget-object v1, p2, LX/CAh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/CAh;->b:LX/1Pp;

    const/4 v4, 0x0

    .line 496568
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 496569
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 496570
    invoke-static {v3}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v6

    .line 496571
    if-nez v6, :cond_0

    .line 496572
    :goto_0
    move-object v0, v4

    .line 496573
    return-object v0

    .line 496574
    :cond_0
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 496575
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 496576
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 496577
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v5

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->BIRTHDAY_WISHES:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    invoke-virtual {v5, v7}, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->equals(Ljava/lang/Object;)Z

    move-result v5

    move v5, v5

    .line 496578
    if-eqz v5, :cond_1

    .line 496579
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v5

    .line 496580
    new-instance v7, LX/CAi;

    invoke-direct {v7, v0, v3, v5}, LX/CAi;-><init>(LX/355;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLImageOverlay;)V

    move-object v3, v7

    .line 496581
    move-object v5, v4

    .line 496582
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    iget-object p0, v0, LX/355;->a:LX/1Vm;

    invoke-virtual {p0, p1}, LX/1Vm;->c(LX/1De;)LX/C2N;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/C2N;->a(Landroid/view/View$OnClickListener;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/C2N;->b(Landroid/view/View$OnClickListener;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/C2N;->c(Landroid/view/View$OnClickListener;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/C2N;->a(Landroid/net/Uri;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/C2N;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/C2N;->a(LX/1Pp;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v7, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    goto :goto_0

    .line 496583
    :cond_1
    new-instance v5, LX/CAk;

    invoke-direct {v5, v0, v6, v3}, LX/CAk;-><init>(LX/355;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v5, v5

    .line 496584
    new-instance v7, LX/CAj;

    invoke-direct {v7, v0, v6, v3}, LX/CAj;-><init>(LX/355;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v3, v7

    .line 496585
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 496586
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {v4}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v4

    move-object p2, v5

    move-object v5, v4

    move-object v4, p2

    goto :goto_1

    :cond_2
    move-object p2, v5

    move-object v5, v4

    move-object v4, p2

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 496587
    invoke-static {}, LX/1dS;->b()V

    .line 496588
    const/4 v0, 0x0

    return-object v0
.end method
