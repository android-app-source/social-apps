.class public LX/25U;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 369519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369520
    return-void
.end method

.method private static a()LX/25U;
    .locals 1

    .prologue
    .line 369521
    new-instance v0, LX/25U;

    invoke-direct {v0}, LX/25U;-><init>()V

    .line 369522
    return-object v0
.end method

.method public static a(LX/0QB;)LX/25U;
    .locals 3

    .prologue
    .line 369523
    const-class v1, LX/25U;

    monitor-enter v1

    .line 369524
    :try_start_0
    sget-object v0, LX/25U;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 369525
    sput-object v2, LX/25U;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 369526
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369527
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    invoke-static {}, LX/25U;->a()LX/25U;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 369528
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/25U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 369529
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 369530
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static final a(LX/1Od;III)[I
    .locals 3

    .prologue
    .line 369531
    invoke-virtual {p0, p1}, LX/1Od;->c(I)Landroid/view/View;

    move-result-object v0

    .line 369532
    const-string v1, "HScrollRecyclerViewChildMeasurer.measureView"

    const v2, 0xf3f086

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 369533
    :try_start_0
    invoke-static {v0, p2, p3}, LX/25U;->a(Landroid/view/View;II)[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 369534
    const v2, -0x68432bab

    invoke-static {v2}, LX/02m;->a(I)V

    .line 369535
    invoke-virtual {p0, v0}, LX/1Od;->a(Landroid/view/View;)V

    .line 369536
    return-object v1

    .line 369537
    :catchall_0
    move-exception v0

    const v1, 0x42842d01

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(Landroid/view/View;II)[I
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 369538
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 369539
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p1, v1, v2}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 369540
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v2, v3}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v2

    .line 369541
    invoke-virtual {p0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 369542
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 369543
    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v4

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v3, v4

    aput v3, v1, v2

    .line 369544
    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v3, v4

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v3

    aput v0, v1, v2

    .line 369545
    return-object v1
.end method

.method public static final a(Landroid/view/View;LX/2eO;II)[I
    .locals 4

    .prologue
    .line 369546
    const-string v0, "HScrollRecyclerViewChildMeasurer.measureView"

    const v1, -0x6e576d24

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 369547
    const/4 v0, 0x0

    :try_start_0
    const/4 v2, -0x1

    .line 369548
    iget-object v1, p1, LX/2eO;->d:LX/2eN;

    invoke-interface {v1, p0}, LX/2eN;->a(Landroid/view/View;)V

    .line 369549
    iget-object v1, p1, LX/2eO;->d:LX/2eN;

    invoke-interface {v1, p0, v2, v2}, LX/2eN;->a(Landroid/view/View;II)V

    .line 369550
    iget-object v1, p1, LX/2eO;->b:LX/2eL;

    invoke-virtual {v1, v0}, LX/2eL;->a(I)LX/2eT;

    move-result-object v1

    .line 369551
    if-nez v1, :cond_0

    .line 369552
    :goto_0
    invoke-static {p0, p2, p3}, LX/25U;->b(Landroid/view/View;II)[I

    move-result-object v0

    .line 369553
    const/4 v1, 0x0

    .line 369554
    iget-object v2, p1, LX/2eO;->b:LX/2eL;

    invoke-virtual {v2, v1}, LX/2eL;->a(I)LX/2eT;

    move-result-object v2

    .line 369555
    if-nez v2, :cond_1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369556
    :goto_1
    const v1, -0x6dcb41aa

    invoke-static {v1}, LX/02m;->a(I)V

    .line 369557
    return-object v0

    .line 369558
    :catchall_0
    move-exception v0

    const v1, -0x2152d2f2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 369559
    :cond_0
    :try_start_1
    iget-object v2, v1, LX/2eT;->b:LX/1Ra;

    move-object v1, v2

    .line 369560
    iget-object v2, p1, LX/2eO;->b:LX/2eL;

    .line 369561
    iget-object v3, v2, LX/2eL;->e:LX/1PW;

    move-object v2, v3

    .line 369562
    invoke-virtual {v1, v2}, LX/1Ra;->a(LX/1PW;)V

    .line 369563
    const v2, 0x7f0d00ff

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 369564
    invoke-virtual {v1, p0}, LX/1Ra;->a(Landroid/view/View;)V

    goto :goto_0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 369565
    :cond_1
    iget-object v3, v2, LX/2eT;->b:LX/1Ra;

    move-object v2, v3

    .line 369566
    invoke-virtual {v2, p0}, LX/1Ra;->b(Landroid/view/View;)V

    .line 369567
    const v2, 0x7f0d00ff

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method private static b(Landroid/view/View;II)[I
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 369568
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 369569
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p1, v1, v2}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 369570
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v2, v0}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v0

    .line 369571
    invoke-virtual {p0, v1, v0}, Landroid/view/View;->measure(II)V

    .line 369572
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 369573
    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    aput v2, v0, v1

    .line 369574
    const/4 v1, 0x1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    aput v2, v0, v1

    .line 369575
    return-object v0
.end method
