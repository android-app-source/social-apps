.class public LX/2H8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Gb;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/2H8;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2HA;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/push/registration/FacebookPushServerRegistrar;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2Gc;

.field public final f:LX/01T;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 389918
    const-class v0, LX/2H8;

    sput-object v0, LX/2H8;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Or;LX/2Gc;LX/01T;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/push/externalcloud/annotations/IsPreRegPushTokenRegistrationEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2HA;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/push/registration/FacebookPushServerRegistrar;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2Gc;",
            "LX/01T;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389911
    iput-object p1, p0, LX/2H8;->b:LX/0Ot;

    .line 389912
    iput-object p2, p0, LX/2H8;->c:LX/0Ot;

    .line 389913
    iput-object p3, p0, LX/2H8;->d:LX/0Or;

    .line 389914
    iput-object p4, p0, LX/2H8;->e:LX/2Gc;

    .line 389915
    iput-object p5, p0, LX/2H8;->f:LX/01T;

    .line 389916
    iput-object p6, p0, LX/2H8;->g:LX/0Or;

    .line 389917
    return-void
.end method

.method public static a(LX/0QB;)LX/2H8;
    .locals 10

    .prologue
    .line 389897
    sget-object v0, LX/2H8;->h:LX/2H8;

    if-nez v0, :cond_1

    .line 389898
    const-class v1, LX/2H8;

    monitor-enter v1

    .line 389899
    :try_start_0
    sget-object v0, LX/2H8;->h:LX/2H8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 389900
    if-eqz v2, :cond_0

    .line 389901
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 389902
    new-instance v3, LX/2H8;

    const/16 v4, 0xfec

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1027

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x15e7

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/2Gc;->a(LX/0QB;)LX/2Gc;

    move-result-object v7

    check-cast v7, LX/2Gc;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v8

    check-cast v8, LX/01T;

    const/16 v9, 0x354

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/2H8;-><init>(LX/0Ot;LX/0Ot;LX/0Or;LX/2Gc;LX/01T;LX/0Or;)V

    .line 389903
    move-object v0, v3

    .line 389904
    sput-object v0, LX/2H8;->h:LX/2H8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389905
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 389906
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 389907
    :cond_1
    sget-object v0, LX/2H8;->h:LX/2H8;

    return-object v0

    .line 389908
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 389909
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 389870
    iget-object v0, p0, LX/2H8;->e:LX/2Gc;

    sget-object v1, LX/2Ge;->GCM:LX/2Ge;

    invoke-virtual {v0, v1}, LX/2Gc;->a(LX/2Ge;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/2Ge;
    .locals 1

    .prologue
    .line 389896
    sget-object v0, LX/2Ge;->GCM:LX/2Ge;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 389893
    invoke-direct {p0}, LX/2H8;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 389894
    :goto_0
    return-void

    .line 389895
    :cond_0
    iget-object v0, p0, LX/2H8;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->GCM:LX/2Ge;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 389890
    invoke-direct {p0}, LX/2H8;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389891
    iget-object v1, p0, LX/2H8;->e:LX/2Gc;

    iget-object v0, p0, LX/2H8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2HA;

    invoke-virtual {v0}, LX/2HA;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Lcom/facebook/push/c2dm/C2DMBroadcastReceiver;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Lcom/facebook/push/c2dm/C2DMService;

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, LX/2Gc;->a(Ljava/lang/String;[Ljava/lang/Class;)V

    .line 389892
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 389887
    invoke-direct {p0}, LX/2H8;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 389888
    :goto_0
    return-void

    .line 389889
    :cond_0
    iget-object v0, p0, LX/2H8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2HA;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2HA;->a(Z)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 389873
    invoke-direct {p0}, LX/2H8;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 389874
    :cond_0
    :goto_0
    return-void

    .line 389875
    :cond_1
    const/4 v1, 0x0

    .line 389876
    iget-object v0, p0, LX/2H8;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 389877
    if-eqz v0, :cond_2

    .line 389878
    const/4 v0, 0x1

    .line 389879
    :goto_2
    move v0, v0

    .line 389880
    if-eqz v0, :cond_0

    .line 389881
    iget-object v0, p0, LX/2H8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2HA;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2HA;->a(Z)V

    goto :goto_0

    .line 389882
    :cond_2
    sget-object v0, LX/01T;->MESSENGER:LX/01T;

    iget-object v2, p0, LX/2H8;->f:LX/01T;

    if-eq v0, v2, :cond_3

    sget-object v0, LX/01T;->FB4A:LX/01T;

    iget-object v2, p0, LX/2H8;->f:LX/01T;

    if-ne v0, v2, :cond_6

    :cond_3
    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 389883
    if-nez v0, :cond_4

    move v0, v1

    .line 389884
    goto :goto_2

    .line 389885
    :cond_4
    iget-object v0, p0, LX/2H8;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 389886
    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 389871
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/2H8;->a(Ljava/lang/String;)V

    .line 389872
    return-void
.end method
