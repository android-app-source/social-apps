.class public final LX/2Lj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Pd;


# instance fields
.field private final a:LX/0Pd;

.field private final b:LX/0Pd;

.field private final c:LX/2Lk;

.field private final d:LX/2Ll;


# direct methods
.method public constructor <init>(LX/0Pd;LX/0Pd;)V
    .locals 3

    .prologue
    .line 395360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395361
    iput-object p1, p0, LX/2Lj;->a:LX/0Pd;

    .line 395362
    iput-object p2, p0, LX/2Lj;->b:LX/0Pd;

    .line 395363
    new-instance v0, LX/2Lk;

    iget-object v1, p0, LX/2Lj;->a:LX/0Pd;

    invoke-interface {v1}, LX/0Pd;->a()LX/0Pg;

    move-result-object v1

    iget-object v2, p0, LX/2Lj;->b:LX/0Pd;

    invoke-interface {v2}, LX/0Pd;->a()LX/0Pg;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/2Lk;-><init>(LX/0Pg;LX/0Pg;)V

    iput-object v0, p0, LX/2Lj;->c:LX/2Lk;

    .line 395364
    new-instance v0, LX/2Ll;

    iget-object v1, p0, LX/2Lj;->a:LX/0Pd;

    invoke-interface {v1}, LX/0Pd;->b()Lcom/facebook/loom/config/SystemControlConfiguration;

    move-result-object v1

    iget-object v2, p0, LX/2Lj;->b:LX/0Pd;

    invoke-interface {v2}, LX/0Pd;->b()Lcom/facebook/loom/config/SystemControlConfiguration;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/2Ll;-><init>(Lcom/facebook/loom/config/SystemControlConfiguration;Lcom/facebook/loom/config/SystemControlConfiguration;)V

    iput-object v0, p0, LX/2Lj;->d:LX/2Ll;

    .line 395365
    return-void
.end method


# virtual methods
.method public final a()LX/0Pg;
    .locals 1

    .prologue
    .line 395366
    iget-object v0, p0, LX/2Lj;->c:LX/2Lk;

    return-object v0
.end method

.method public final b()Lcom/facebook/loom/config/SystemControlConfiguration;
    .locals 1

    .prologue
    .line 395367
    iget-object v0, p0, LX/2Lj;->d:LX/2Ll;

    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 395368
    iget-object v0, p0, LX/2Lj;->a:LX/0Pd;

    invoke-interface {v0}, LX/0Pd;->c()I

    move-result v0

    iget-object v1, p0, LX/2Lj;->b:LX/0Pd;

    invoke-interface {v1}, LX/0Pd;->c()I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public final d()J
    .locals 4

    .prologue
    .line 395369
    iget-object v0, p0, LX/2Lj;->a:LX/0Pd;

    invoke-interface {v0}, LX/0Pd;->d()J

    move-result-wide v0

    iget-object v2, p0, LX/2Lj;->b:LX/0Pd;

    invoke-interface {v2}, LX/0Pd;->d()J

    move-result-wide v2

    xor-long/2addr v0, v2

    return-wide v0
.end method
