.class public abstract LX/2h1;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Vd",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 449673
    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 449674
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    .line 449675
    check-cast p1, Lcom/facebook/fbservice/service/ServiceException;

    invoke-virtual {p0, p1}, LX/2h1;->onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 449676
    :goto_0
    return-void

    .line 449677
    :cond_0
    invoke-static {p1}, LX/1Bz;->getRootCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    .line 449678
    instance-of v1, p1, Ljava/io/IOException;

    if-nez v1, :cond_1

    instance-of v0, v0, Ljava/io/IOException;

    if-eqz v0, :cond_2

    .line 449679
    :cond_1
    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2h1;->onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V

    goto :goto_0

    .line 449680
    :cond_2
    invoke-virtual {p0, p1}, LX/2h1;->onThrowable(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public abstract onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
.end method

.method public onThrowable(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 449681
    invoke-static {p1}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    .line 449682
    return-void
.end method
