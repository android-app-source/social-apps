.class public LX/2Gk;
.super LX/2Gj;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile o:LX/2Gk;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/content/pm/PackageManager;

.field private final c:LX/0y3;

.field public final d:LX/0ka;

.field private final e:LX/2Gl;

.field public final f:Landroid/content/ContentResolver;

.field public final g:Landroid/net/wifi/WifiManager;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0yH;

.field public final j:LX/1wY;

.field public final k:LX/03V;

.field public final l:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final m:Lcom/facebook/common/perftest/PerfTestConfig;

.field public final n:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/0y3;LX/0ka;LX/2Gl;Landroid/content/ContentResolver;Landroid/net/wifi/WifiManager;LX/0Or;LX/0yH;LX/1wY;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/common/perftest/PerfTestConfig;Landroid/hardware/SensorManager;)V
    .locals 0
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/location/IsForceAndroidPlatformImplEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/pm/PackageManager;",
            "LX/0y3;",
            "LX/0ka;",
            "LX/2Gl;",
            "Landroid/content/ContentResolver;",
            "Landroid/net/wifi/WifiManager;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "LX/1wY;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "Landroid/hardware/SensorManager;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389290
    invoke-direct {p0}, LX/2Gj;-><init>()V

    .line 389291
    iput-object p1, p0, LX/2Gk;->a:Landroid/content/Context;

    .line 389292
    iput-object p2, p0, LX/2Gk;->b:Landroid/content/pm/PackageManager;

    .line 389293
    iput-object p3, p0, LX/2Gk;->c:LX/0y3;

    .line 389294
    iput-object p4, p0, LX/2Gk;->d:LX/0ka;

    .line 389295
    iput-object p5, p0, LX/2Gk;->e:LX/2Gl;

    .line 389296
    iput-object p6, p0, LX/2Gk;->f:Landroid/content/ContentResolver;

    .line 389297
    iput-object p7, p0, LX/2Gk;->g:Landroid/net/wifi/WifiManager;

    .line 389298
    iput-object p8, p0, LX/2Gk;->h:LX/0Or;

    .line 389299
    iput-object p9, p0, LX/2Gk;->i:LX/0yH;

    .line 389300
    iput-object p10, p0, LX/2Gk;->j:LX/1wY;

    .line 389301
    iput-object p11, p0, LX/2Gk;->k:LX/03V;

    .line 389302
    iput-object p12, p0, LX/2Gk;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 389303
    iput-object p13, p0, LX/2Gk;->m:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 389304
    iput-object p14, p0, LX/2Gk;->n:Landroid/hardware/SensorManager;

    .line 389305
    return-void
.end method

.method public static a(Ljava/util/Collection;)LX/162;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 389286
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 389287
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 389288
    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 389289
    :cond_0
    return-object v1
.end method

.method public static a(LX/0QB;)LX/2Gk;
    .locals 3

    .prologue
    .line 389276
    sget-object v0, LX/2Gk;->o:LX/2Gk;

    if-nez v0, :cond_1

    .line 389277
    const-class v1, LX/2Gk;

    monitor-enter v1

    .line 389278
    :try_start_0
    sget-object v0, LX/2Gk;->o:LX/2Gk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 389279
    if-eqz v2, :cond_0

    .line 389280
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2Gk;->b(LX/0QB;)LX/2Gk;

    move-result-object v0

    sput-object v0, LX/2Gk;->o:LX/2Gk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389281
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 389282
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 389283
    :cond_1
    sget-object v0, LX/2Gk;->o:LX/2Gk;

    return-object v0

    .line 389284
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 389285
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 389272
    packed-switch p0, :pswitch_data_0

    .line 389273
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 389274
    :pswitch_0
    const-string v0, "unsupported"

    goto :goto_0

    .line 389275
    :pswitch_1
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static b(LX/0QB;)LX/2Gk;
    .locals 15

    .prologue
    .line 389270
    new-instance v0, LX/2Gk;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageManager;

    invoke-static {p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v3

    check-cast v3, LX/0y3;

    invoke-static {p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v4

    check-cast v4, LX/0ka;

    invoke-static {p0}, LX/2Gl;->a(LX/0QB;)LX/2Gl;

    move-result-object v5

    check-cast v5, LX/2Gl;

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v6

    check-cast v6, Landroid/content/ContentResolver;

    invoke-static {p0}, LX/0kt;->b(LX/0QB;)Landroid/net/wifi/WifiManager;

    move-result-object v7

    check-cast v7, Landroid/net/wifi/WifiManager;

    const/16 v8, 0x32f

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v9

    check-cast v9, LX/0yH;

    invoke-static {p0}, LX/1wY;->a(LX/0QB;)LX/1wY;

    move-result-object v10

    check-cast v10, LX/1wY;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v12

    check-cast v12, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v13

    check-cast v13, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {p0}, LX/2Fq;->b(LX/0QB;)Landroid/hardware/SensorManager;

    move-result-object v14

    check-cast v14, Landroid/hardware/SensorManager;

    invoke-direct/range {v0 .. v14}, LX/2Gk;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/0y3;LX/0ka;LX/2Gl;Landroid/content/ContentResolver;Landroid/net/wifi/WifiManager;LX/0Or;LX/0yH;LX/1wY;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/common/perftest/PerfTestConfig;Landroid/hardware/SensorManager;)V

    .line 389271
    return-object v0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 389266
    packed-switch p0, :pswitch_data_0

    .line 389267
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 389268
    :pswitch_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 389269
    :pswitch_1
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static e(LX/2Gk;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 389259
    :try_start_0
    iget-object v0, p0, LX/2Gk;->e:LX/2Gl;

    const-string v1, "wifi_sleep_policy"

    .line 389260
    iget-object p0, v0, LX/2Gl;->a:Landroid/content/ContentResolver;

    invoke-static {p0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result p0

    move v0, p0
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 389261
    packed-switch v0, :pswitch_data_0

    .line 389262
    :goto_0
    const-string v0, "unknown"

    :goto_1
    return-object v0

    .line 389263
    :pswitch_0
    :try_start_1
    const-string v0, "default"
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 389264
    :pswitch_1
    const-string v0, "never_while_plugged"

    goto :goto_1

    .line 389265
    :pswitch_2
    const-string v0, "never"

    goto :goto_1

    :catch_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/0lF;
    .locals 10

    .prologue
    .line 389171
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 389172
    iget-object v1, p0, LX/2Gk;->c:LX/0y3;

    invoke-virtual {v1}, LX/0y3;->b()LX/1rv;

    move-result-object v1

    .line 389173
    const-string v2, "state"

    iget-object v3, v1, LX/1rv;->a:LX/0yG;

    invoke-virtual {v3}, LX/0yG;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 389174
    const-string v2, "providers"

    .line 389175
    new-instance v3, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 389176
    const-string v4, "user_enabled"

    iget-object v5, v1, LX/1rv;->b:LX/0Rf;

    invoke-static {v5}, LX/2Gk;->a(Ljava/util/Collection;)LX/162;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 389177
    const-string v4, "user_disabled"

    iget-object v5, v1, LX/1rv;->c:LX/0Rf;

    invoke-static {v5}, LX/2Gk;->a(Ljava/util/Collection;)LX/162;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 389178
    move-object v1, v3

    .line 389179
    invoke-virtual {v0, v2, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 389180
    const-string v1, "wifi_info"

    .line 389181
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 389182
    const-string v3, "enabled"

    iget-object v4, p0, LX/2Gk;->d:LX/0ka;

    invoke-virtual {v4}, LX/0ka;->a()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 389183
    const-string v3, "sleep_policy"

    invoke-static {p0}, LX/2Gk;->e(LX/2Gk;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 389184
    const-string v3, "can_always_scan"

    .line 389185
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-ge v4, v5, :cond_4

    .line 389186
    const-string v4, "unsupported"

    .line 389187
    :goto_0
    move-object v4, v4

    .line 389188
    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 389189
    move-object v2, v2

    .line 389190
    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 389191
    const-string v1, "bluetooth_info"

    .line 389192
    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-virtual {v2}, LX/0mC;->c()LX/0m9;

    move-result-object v2

    .line 389193
    const-string v3, "system_supports_bluetooth"

    iget-object v4, p0, LX/2Gk;->b:Landroid/content/pm/PackageManager;

    const-string v5, "android.hardware.bluetooth"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 389194
    const-string v3, "system_supports_bluetooth_low_energy"

    iget-object v4, p0, LX/2Gk;->b:Landroid/content/pm/PackageManager;

    const-string v5, "android.hardware.bluetooth_le"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 389195
    const-string v3, "has_bluetooth_permission"

    iget-object v4, p0, LX/2Gk;->a:Landroid/content/Context;

    const-string v5, "android.permission.BLUETOOTH"

    invoke-virtual {v4, v5}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, LX/2Gk;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 389196
    const-string v3, "has_bluetooth_admin_permission"

    iget-object v4, p0, LX/2Gk;->a:Landroid/content/Context;

    const-string v5, "android.permission.BLUETOOTH_ADMIN"

    invoke-virtual {v4, v5}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, LX/2Gk;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 389197
    const-string v3, "bluetooth_enabled"

    .line 389198
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-lt v4, v5, :cond_6

    .line 389199
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-lt v4, v5, :cond_7

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 389200
    iget-object v4, p0, LX/2Gk;->f:Landroid/content/ContentResolver;

    const-string v5, "bluetooth_on"

    const/4 v6, -0x1

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, LX/2Gk;->a(I)Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 389201
    :goto_2
    move-object v4, v4

    .line 389202
    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 389203
    move-object v2, v2

    .line 389204
    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 389205
    const-string v1, "manager_impl"

    .line 389206
    iget-object v4, p0, LX/2Gk;->h:LX/0Or;

    iget-object v5, p0, LX/2Gk;->i:LX/0yH;

    iget-object v6, p0, LX/2Gk;->j:LX/1wY;

    iget-object v7, p0, LX/2Gk;->m:Lcom/facebook/common/perftest/PerfTestConfig;

    iget-object v8, p0, LX/2Gk;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v9, p0, LX/2Gk;->k:LX/03V;

    invoke-static/range {v4 .. v9}, LX/2vc;->a(LX/0Or;LX/0yH;LX/1wY;Lcom/facebook/common/perftest/PerfTestConfig;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;)LX/1wZ;

    move-result-object v4

    invoke-virtual {v4}, LX/1wZ;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v2, v4

    .line 389207
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 389208
    const-string v1, "passive_impl"

    .line 389209
    iget-object v2, p0, LX/2Gk;->h:LX/0Or;

    iget-object v3, p0, LX/2Gk;->j:LX/1wY;

    iget-object v4, p0, LX/2Gk;->k:LX/03V;

    invoke-static {v2, v3, v4}, LX/2Gw;->a(LX/0Or;LX/1wY;LX/03V;)LX/1wZ;

    move-result-object v2

    invoke-virtual {v2}, LX/1wZ;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 389210
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 389211
    const-string v1, "zero_rating_interstitial"

    iget-object v2, p0, LX/2Gk;->i:LX/0yH;

    sget-object v3, LX/0yY;->LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

    invoke-virtual {v2, v3}, LX/0yH;->a(LX/0yY;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 389212
    const-string v1, "sensors"

    .line 389213
    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-virtual {v2}, LX/0mC;->b()LX/162;

    move-result-object v3

    .line 389214
    iget-object v2, p0, LX/2Gk;->n:Landroid/hardware/SensorManager;

    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v2

    .line 389215
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Sensor;

    .line 389216
    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-virtual {v5}, LX/0mC;->c()LX/0m9;

    move-result-object v5

    .line 389217
    const-string v6, "name"

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 389218
    const-string v6, "type"

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getType()I

    move-result v7

    const-string v8, "unknown"

    .line 389219
    packed-switch v7, :pswitch_data_0

    .line 389220
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt p0, v2, :cond_0

    .line 389221
    packed-switch v7, :pswitch_data_1

    .line 389222
    :cond_0
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt p0, v2, :cond_1

    .line 389223
    packed-switch v7, :pswitch_data_2

    .line 389224
    :cond_1
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x14

    if-lt p0, v2, :cond_2

    .line 389225
    packed-switch v7, :pswitch_data_3

    .line 389226
    :cond_2
    :goto_4
    move-object v7, v8

    .line 389227
    invoke-virtual {v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 389228
    move-object v2, v5

    .line 389229
    invoke-virtual {v3, v2}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_3

    .line 389230
    :cond_3
    move-object v2, v3

    .line 389231
    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 389232
    return-object v0

    .line 389233
    :cond_4
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_5

    const/4 v4, 0x1

    :goto_5
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 389234
    iget-object v4, p0, LX/2Gk;->g:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->isScanAlwaysAvailable()Z

    move-result v4

    move v4, v4

    .line 389235
    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 389236
    :cond_5
    const/4 v4, 0x0

    goto :goto_5

    :cond_6
    iget-object v4, p0, LX/2Gk;->f:Landroid/content/ContentResolver;

    const-string v5, "bluetooth_on"

    const/4 v6, -0x1

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, LX/2Gk;->a(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    .line 389237
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 389238
    :pswitch_0
    const-string v8, "accelerometer"

    goto :goto_4

    .line 389239
    :pswitch_1
    const-string v8, "ambient_temperature"

    goto :goto_4

    .line 389240
    :pswitch_2
    const-string v8, "gravity"

    goto :goto_4

    .line 389241
    :pswitch_3
    const-string v8, "gyroscope"

    goto :goto_4

    .line 389242
    :pswitch_4
    const-string v8, "light"

    goto :goto_4

    .line 389243
    :pswitch_5
    const-string v8, "linear_acceleration"

    goto :goto_4

    .line 389244
    :pswitch_6
    const-string v8, "magnetic_field"

    goto :goto_4

    .line 389245
    :pswitch_7
    const-string v8, "orientation"

    goto :goto_4

    .line 389246
    :pswitch_8
    const-string v8, "pressure"

    goto :goto_4

    .line 389247
    :pswitch_9
    const-string v8, "proximity"

    goto :goto_4

    .line 389248
    :pswitch_a
    const-string v8, "relative_humidity"

    goto :goto_4

    .line 389249
    :pswitch_b
    const-string v8, "rotation_vector"

    goto :goto_4

    .line 389250
    :pswitch_c
    const-string v8, "temperature"

    goto :goto_4

    .line 389251
    :pswitch_d
    const-string v8, "game_rotation_vector"

    goto :goto_4

    .line 389252
    :pswitch_e
    const-string v8, "gyroscope_uncalibrated"

    goto :goto_4

    .line 389253
    :pswitch_f
    const-string v8, "magnetic_field_uncalibrated"

    goto :goto_4

    .line 389254
    :pswitch_10
    const-string v8, "significant_motion"

    goto :goto_4

    .line 389255
    :pswitch_11
    const-string v8, "geomagnetic_rotation_vector"

    goto :goto_4

    .line 389256
    :pswitch_12
    const-string v8, "step_counter"

    goto :goto_4

    .line 389257
    :pswitch_13
    const-string v8, "step_detector"

    goto :goto_4

    .line 389258
    :pswitch_14
    const-string v8, "heart_rate"

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_3
        :pswitch_4
        :pswitch_8
        :pswitch_c
        :pswitch_9
        :pswitch_2
        :pswitch_5
        :pswitch_b
        :pswitch_a
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xe
        :pswitch_f
        :pswitch_d
        :pswitch_e
        :pswitch_10
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x12
        :pswitch_13
        :pswitch_12
        :pswitch_11
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x15
        :pswitch_14
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 389169
    const-string v0, "location"

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 389170
    iget-object v0, p0, LX/2Gk;->c:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
