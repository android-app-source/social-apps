.class public LX/2V5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2V5;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 417134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417135
    return-void
.end method

.method public static a(Lcom/facebook/stickers/model/Sticker;)LX/03R;
    .locals 1

    .prologue
    .line 417136
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    if-nez v0, :cond_0

    sget-object v0, LX/03R;->YES:LX/03R;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2V5;
    .locals 3

    .prologue
    .line 417137
    sget-object v0, LX/2V5;->a:LX/2V5;

    if-nez v0, :cond_1

    .line 417138
    const-class v1, LX/2V5;

    monitor-enter v1

    .line 417139
    :try_start_0
    sget-object v0, LX/2V5;->a:LX/2V5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 417140
    if-eqz v2, :cond_0

    .line 417141
    :try_start_1
    new-instance v0, LX/2V5;

    invoke-direct {v0}, LX/2V5;-><init>()V

    .line 417142
    move-object v0, v0

    .line 417143
    sput-object v0, LX/2V5;->a:LX/2V5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 417144
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 417145
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 417146
    :cond_1
    sget-object v0, LX/2V5;->a:LX/2V5;

    return-object v0

    .line 417147
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 417148
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/stickers/model/Sticker;)Z
    .locals 1

    .prologue
    .line 417149
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
