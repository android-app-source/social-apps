.class public final LX/2fe;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public final b:Landroid/view/View$OnClickListener;

.field public final c:Landroid/view/View$OnClickListener;

.field public final d:Landroid/view/View$OnClickListener;

.field public final e:LX/1bf;

.field public final f:Lcom/facebook/video/engine/VideoPlayerParams;

.field public final g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLStoryActionLink;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;LX/1bf;Lcom/facebook/video/engine/VideoPlayerParams;LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/view/View$OnClickListener;",
            "LX/1bf;",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 447033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447034
    iput-object p1, p0, LX/2fe;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 447035
    iput-object p2, p0, LX/2fe;->b:Landroid/view/View$OnClickListener;

    .line 447036
    iput-object p3, p0, LX/2fe;->c:Landroid/view/View$OnClickListener;

    .line 447037
    iput-object p4, p0, LX/2fe;->d:Landroid/view/View$OnClickListener;

    .line 447038
    iput-object p5, p0, LX/2fe;->e:LX/1bf;

    .line 447039
    iput-object p6, p0, LX/2fe;->f:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 447040
    iput-object p7, p0, LX/2fe;->g:LX/0P1;

    .line 447041
    return-void
.end method
