.class public LX/2GW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/4gV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 388754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388755
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2GW;->a:Ljava/util/Map;

    return-void
.end method

.method private static declared-synchronized a(LX/2GW;Ljava/lang/String;Z)LX/4gV;
    .locals 3

    .prologue
    .line 388756
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2GW;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4gV;

    .line 388757
    if-nez v0, :cond_1

    .line 388758
    if-eqz p2, :cond_0

    .line 388759
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 388760
    if-eqz v0, :cond_0

    .line 388761
    new-instance v0, Ljava/security/InvalidParameterException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No histogram container found with ID:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388762
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 388763
    :cond_0
    :try_start_1
    new-instance v0, LX/4gV;

    invoke-direct {v0}, LX/4gV;-><init>()V

    .line 388764
    iget-object v1, p0, LX/2GW;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 388765
    :cond_1
    monitor-exit p0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/util/Map;)LX/0lF;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)",
            "LX/0lF;"
        }
    .end annotation

    .prologue
    .line 388766
    monitor-enter p0

    :try_start_0
    new-instance v3, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 388767
    iget-object v0, p0, LX/2GW;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 388768
    new-instance v5, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 388769
    const-string v6, "histograms"

    iget-object v1, p0, LX/2GW;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4gV;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, LX/4gV;->a(I)LX/0lF;

    move-result-object v1

    invoke-virtual {v5, v6, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 388770
    iget-object v1, p0, LX/2GW;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4gV;

    .line 388771
    iget-object v2, v1, LX/4gV;->b:Ljava/lang/String;

    move-object v1, v2

    .line 388772
    invoke-static {v5, v1}, LX/4gV;->a(LX/0m9;Ljava/lang/String;)V

    .line 388773
    invoke-virtual {v3, v0, v5}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 388774
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 388775
    :cond_0
    monitor-exit p0

    return-object v3
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 388776
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2GW;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388777
    monitor-exit p0

    return-void

    .line 388778
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 388779
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, p1, v0}, LX/2GW;->a(LX/2GW;Ljava/lang/String;Z)LX/4gV;

    move-result-object v0

    .line 388780
    iput-object p2, v0, LX/4gV;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388781
    monitor-exit p0

    return-void

    .line 388782
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 388783
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, p1, v0}, LX/2GW;->a(LX/2GW;Ljava/lang/String;Z)LX/4gV;

    move-result-object v0

    .line 388784
    invoke-virtual {v0, p2, p3, p4}, LX/4gV;->a(Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388785
    monitor-exit p0

    return-void

    .line 388786
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 7

    .prologue
    .line 388787
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, p1, v0}, LX/2GW;->a(LX/2GW;Ljava/lang/String;Z)LX/4gV;

    move-result-object v0

    move-object v1, p2

    move-wide v2, p3

    move-wide v4, p5

    .line 388788
    invoke-virtual/range {v0 .. v5}, LX/4gV;->a(Ljava/lang/String;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388789
    monitor-exit p0

    return-void

    .line 388790
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 388791
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, p1, v0}, LX/2GW;->a(LX/2GW;Ljava/lang/String;Z)LX/4gV;

    move-result-object v0

    .line 388792
    const/4 p1, 0x1

    invoke-static {v0, p2, p1}, LX/4gV;->a(LX/4gV;Ljava/lang/String;Z)V

    .line 388793
    iget-object p1, v0, LX/4gV;->a:Ljava/util/Map;

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/4gU;

    invoke-virtual {p1, p3}, LX/4gU;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388794
    monitor-exit p0

    return-void

    .line 388795
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/2GW;
    .locals 3

    .prologue
    .line 388796
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/2GW;

    invoke-direct {v0}, LX/2GW;-><init>()V

    .line 388797
    iget-object v1, v0, LX/2GW;->a:Ljava/util/Map;

    .line 388798
    iget-object v2, p0, LX/2GW;->a:Ljava/util/Map;

    iput-object v2, v0, LX/2GW;->a:Ljava/util/Map;

    .line 388799
    iput-object v1, p0, LX/2GW;->a:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388800
    monitor-exit p0

    return-object v0

    .line 388801
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 388802
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, p1, v0}, LX/2GW;->a(LX/2GW;Ljava/lang/String;Z)LX/4gV;

    move-result-object v0

    .line 388803
    const/4 v1, 0x1

    invoke-static {v0, p2, v1}, LX/4gV;->a(LX/4gV;Ljava/lang/String;Z)V

    .line 388804
    iget-object v1, v0, LX/4gV;->a:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4gU;

    .line 388805
    const-wide/16 v3, 0x1

    invoke-virtual {v1, p3, p4, v3, v4}, LX/4gU;->a(JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388806
    monitor-exit p0

    return-void

    .line 388807
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 7

    .prologue
    .line 388808
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, p1, v0}, LX/2GW;->a(LX/2GW;Ljava/lang/String;Z)LX/4gV;

    move-result-object v0

    move-object v1, p2

    move-wide v2, p3

    move-wide v4, p5

    .line 388809
    const/4 v6, 0x1

    invoke-static {v0, v1, v6}, LX/4gV;->a(LX/4gV;Ljava/lang/String;Z)V

    .line 388810
    iget-object v6, v0, LX/4gV;->a:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/4gU;

    invoke-virtual {v6, v2, v3, v4, v5}, LX/4gU;->a(JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388811
    monitor-exit p0

    return-void

    .line 388812
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
