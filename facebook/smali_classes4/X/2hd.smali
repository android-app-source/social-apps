.class public LX/2hd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 450251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450252
    iput-object p1, p0, LX/2hd;->a:LX/0Zb;

    .line 450253
    return-void
.end method

.method public static a(LX/0QB;)LX/2hd;
    .locals 4

    .prologue
    .line 450240
    const-class v1, LX/2hd;

    monitor-enter v1

    .line 450241
    :try_start_0
    sget-object v0, LX/2hd;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 450242
    sput-object v2, LX/2hd;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 450243
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450244
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 450245
    new-instance p0, LX/2hd;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/2hd;-><init>(LX/0Zb;)V

    .line 450246
    move-object v0, p0

    .line 450247
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 450248
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2hd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 450249
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 450250
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/2hd;LX/Ezt;Ljava/lang/String;Ljava/lang/String;JLX/2hC;LX/2hC;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/2hC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 450231
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v1, p1, LX/Ezt;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "pymk_id"

    invoke-virtual {v0, v1, p4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "pymk_location"

    iget-object v2, p6, LX/2hC;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 450232
    iput-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 450233
    move-object v0, v0

    .line 450234
    if-eqz p3, :cond_0

    .line 450235
    const-string v1, "pymk_signature"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 450236
    :cond_0
    if-eqz p7, :cond_1

    .line 450237
    const-string v1, "pymk_ref"

    iget-object v2, p7, LX/2hC;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 450238
    :cond_1
    iget-object v1, p0, LX/2hd;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 450239
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 450221
    sget-object v1, LX/Ezt;->PYMK_IMP_EVENT:LX/Ezt;

    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-static/range {v0 .. v7}, LX/2hd;->a(LX/2hd;LX/Ezt;Ljava/lang/String;Ljava/lang/String;JLX/2hC;LX/2hC;)V

    .line 450222
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JLX/2hC;LX/2hC;)V
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 450229
    sget-object v1, LX/Ezt;->PYMK_IMP_EVENT:LX/Ezt;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-static/range {v0 .. v7}, LX/2hd;->a(LX/2hd;LX/Ezt;Ljava/lang/String;Ljava/lang/String;JLX/2hC;LX/2hC;)V

    .line 450230
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 450227
    sget-object v1, LX/Ezt;->PYMK_ADD_EVENT:LX/Ezt;

    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-static/range {v0 .. v7}, LX/2hd;->a(LX/2hd;LX/Ezt;Ljava/lang/String;Ljava/lang/String;JLX/2hC;LX/2hC;)V

    .line 450228
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 450225
    sget-object v1, LX/Ezt;->PYMK_XOUT_EVENT:LX/Ezt;

    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-static/range {v0 .. v7}, LX/2hd;->a(LX/2hd;LX/Ezt;Ljava/lang/String;Ljava/lang/String;JLX/2hC;LX/2hC;)V

    .line 450226
    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 450223
    sget-object v1, LX/Ezt;->PYMK_PROFILE_EVENT:LX/Ezt;

    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-static/range {v0 .. v7}, LX/2hd;->a(LX/2hd;LX/Ezt;Ljava/lang/String;Ljava/lang/String;JLX/2hC;LX/2hC;)V

    .line 450224
    return-void
.end method
