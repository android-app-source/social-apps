.class public final LX/31a;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field public D:Z

.field public E:[Z

.field public F:Z

.field public G:Z

.field public H:I

.field public I:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

.field public J:Landroid/database/Cursor;

.field public K:Ljava/lang/String;

.field public L:Ljava/lang/String;

.field public M:Z

.field public N:Landroid/widget/AdapterView$OnItemSelectedListener;

.field public O:Z

.field public P:Z

.field public final a:Landroid/content/Context;

.field public final b:Landroid/view/LayoutInflater;

.field public c:I

.field public d:Landroid/graphics/drawable/Drawable;

.field public e:I

.field public f:Ljava/lang/CharSequence;

.field public g:Landroid/view/View;

.field public h:Ljava/lang/CharSequence;

.field public i:Landroid/graphics/drawable/Drawable;

.field public j:Ljava/lang/CharSequence;

.field public k:Ljava/lang/CharSequence;

.field public l:Landroid/content/DialogInterface$OnClickListener;

.field public m:Ljava/lang/CharSequence;

.field public n:Landroid/content/DialogInterface$OnClickListener;

.field public o:Ljava/lang/CharSequence;

.field public p:Landroid/content/DialogInterface$OnClickListener;

.field public q:Z

.field public r:Landroid/content/DialogInterface$OnCancelListener;

.field public s:Landroid/content/DialogInterface$OnDismissListener;

.field public t:Landroid/content/DialogInterface$OnKeyListener;

.field public u:[Ljava/lang/CharSequence;

.field public v:Landroid/widget/ListAdapter;

.field public w:Landroid/content/DialogInterface$OnClickListener;

.field public x:Landroid/view/View;

.field public y:Landroid/view/View;

.field public z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 487596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 487597
    iput v0, p0, LX/31a;->c:I

    .line 487598
    iput v0, p0, LX/31a;->e:I

    .line 487599
    iput-boolean v0, p0, LX/31a;->D:Z

    .line 487600
    const/4 v0, -0x1

    iput v0, p0, LX/31a;->H:I

    .line 487601
    iput-boolean v1, p0, LX/31a;->O:Z

    .line 487602
    iput-boolean v1, p0, LX/31a;->P:Z

    .line 487603
    iput-object p1, p0, LX/31a;->a:Landroid/content/Context;

    .line 487604
    iput-boolean v1, p0, LX/31a;->q:Z

    .line 487605
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, LX/31a;->b:Landroid/view/LayoutInflater;

    .line 487606
    return-void
.end method

.method private b(LX/4BW;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 487607
    iget-object v0, p0, LX/31a;->b:Landroid/view/LayoutInflater;

    iget v1, p1, LX/4BW;->H:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    .line 487608
    iget-boolean v0, p0, LX/31a;->F:Z

    if-eqz v0, :cond_4

    .line 487609
    iget-object v0, p0, LX/31a;->J:Landroid/database/Cursor;

    if-nez v0, :cond_3

    .line 487610
    new-instance v0, LX/4BQ;

    iget-object v2, p0, LX/31a;->a:Landroid/content/Context;

    iget v3, p1, LX/4BW;->I:I

    const v4, 0x7f0d0cec

    iget-object v5, p0, LX/31a;->u:[Ljava/lang/CharSequence;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, LX/4BQ;-><init>(LX/31a;Landroid/content/Context;II[Ljava/lang/CharSequence;Landroid/widget/ListView;)V

    .line 487611
    :goto_0
    iput-object v0, p1, LX/4BW;->E:Landroid/widget/ListAdapter;

    .line 487612
    iget v0, p0, LX/31a;->H:I

    .line 487613
    iput v0, p1, LX/4BW;->F:I

    .line 487614
    iget-object v0, p0, LX/31a;->w:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_8

    .line 487615
    new-instance v0, LX/4BS;

    invoke-direct {v0, p0, p1}, LX/4BS;-><init>(LX/31a;LX/4BW;)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 487616
    :cond_0
    :goto_1
    iget-object v0, p0, LX/31a;->N:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_1

    .line 487617
    iget-object v0, p0, LX/31a;->N:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 487618
    :cond_1
    iget-boolean v0, p0, LX/31a;->G:Z

    if-eqz v0, :cond_9

    .line 487619
    invoke-virtual {v6, v9}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 487620
    :cond_2
    :goto_2
    iput-object v6, p1, LX/4BW;->f:Landroid/widget/ListView;

    .line 487621
    return-void

    .line 487622
    :cond_3
    new-instance v1, LX/4BR;

    iget-object v3, p0, LX/31a;->a:Landroid/content/Context;

    iget-object v4, p0, LX/31a;->J:Landroid/database/Cursor;

    move-object v2, p0

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, LX/4BR;-><init>(LX/31a;Landroid/content/Context;Landroid/database/Cursor;ZLandroid/widget/ListView;LX/4BW;)V

    move-object v0, v1

    goto :goto_0

    .line 487623
    :cond_4
    iget-boolean v0, p0, LX/31a;->G:Z

    if-eqz v0, :cond_5

    iget v2, p1, LX/4BW;->J:I

    .line 487624
    :goto_3
    iget-object v0, p0, LX/31a;->J:Landroid/database/Cursor;

    if-nez v0, :cond_7

    .line 487625
    iget-object v0, p0, LX/31a;->v:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/31a;->v:Landroid/widget/ListAdapter;

    goto :goto_0

    .line 487626
    :cond_5
    iget v2, p1, LX/4BW;->K:I

    goto :goto_3

    .line 487627
    :cond_6
    new-instance v0, LX/4BV;

    iget-object v1, p0, LX/31a;->a:Landroid/content/Context;

    const v3, 0x7f0d0cec

    iget-object v4, p0, LX/31a;->u:[Ljava/lang/CharSequence;

    invoke-direct {v0, v1, v2, v3, v4}, LX/4BV;-><init>(Landroid/content/Context;II[Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 487628
    :cond_7
    new-instance v0, Landroid/widget/SimpleCursorAdapter;

    iget-object v1, p0, LX/31a;->a:Landroid/content/Context;

    iget-object v3, p0, LX/31a;->J:Landroid/database/Cursor;

    new-array v4, v9, [Ljava/lang/String;

    iget-object v7, p0, LX/31a;->K:Ljava/lang/String;

    aput-object v7, v4, v5

    new-array v7, v9, [I

    const v8, 0x7f0d0cec

    aput v8, v7, v5

    move-object v5, v7

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    goto :goto_0

    .line 487629
    :cond_8
    iget-object v0, p0, LX/31a;->I:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    if-eqz v0, :cond_0

    .line 487630
    new-instance v0, LX/4BT;

    invoke-direct {v0, p0, v6, p1}, LX/4BT;-><init>(LX/31a;Landroid/widget/ListView;LX/4BW;)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_1

    .line 487631
    :cond_9
    iget-boolean v0, p0, LX/31a;->F:Z

    if-eqz v0, :cond_2

    .line 487632
    const/4 v0, 0x2

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setChoiceMode(I)V

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/4BW;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 487633
    iget-object v0, p0, LX/31a;->g:Landroid/view/View;

    if-eqz v0, :cond_b

    .line 487634
    iget-object v0, p0, LX/31a;->g:Landroid/view/View;

    .line 487635
    iput-object v0, p1, LX/4BW;->y:Landroid/view/View;

    .line 487636
    :cond_0
    :goto_0
    iget-object v0, p0, LX/31a;->h:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 487637
    iget-object v0, p0, LX/31a;->h:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, LX/4BW;->b(Ljava/lang/CharSequence;)V

    .line 487638
    :cond_1
    iget-object v0, p0, LX/31a;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 487639
    iget-object v0, p0, LX/31a;->i:Landroid/graphics/drawable/Drawable;

    .line 487640
    iput-object v0, p1, LX/4BW;->A:Landroid/graphics/drawable/Drawable;

    .line 487641
    iget-object v1, p1, LX/4BW;->z:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 487642
    iget-object v1, p1, LX/4BW;->z:Landroid/widget/ImageView;

    iget-object v2, p1, LX/4BW;->A:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 487643
    :cond_2
    iget-object v0, p0, LX/31a;->j:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    .line 487644
    iget-object v0, p0, LX/31a;->j:Ljava/lang/CharSequence;

    .line 487645
    iput-object v0, p1, LX/4BW;->C:Ljava/lang/CharSequence;

    .line 487646
    iget-object v1, p1, LX/4BW;->B:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 487647
    iget-object v1, p1, LX/4BW;->B:Landroid/widget/TextView;

    iget-object v2, p1, LX/4BW;->C:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 487648
    :cond_3
    iget-object v0, p0, LX/31a;->y:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 487649
    iget-object v0, p0, LX/31a;->y:Landroid/view/View;

    .line 487650
    iput-object v0, p1, LX/4BW;->D:Landroid/view/View;

    .line 487651
    :cond_4
    iget-object v0, p0, LX/31a;->k:Ljava/lang/CharSequence;

    if-eqz v0, :cond_5

    .line 487652
    const/4 v0, -0x1

    iget-object v1, p0, LX/31a;->k:Ljava/lang/CharSequence;

    iget-object v2, p0, LX/31a;->l:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v0, v1, v2, v3}, LX/4BW;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 487653
    :cond_5
    iget-object v0, p0, LX/31a;->m:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    .line 487654
    const/4 v0, -0x2

    iget-object v1, p0, LX/31a;->m:Ljava/lang/CharSequence;

    iget-object v2, p0, LX/31a;->n:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v0, v1, v2, v3}, LX/4BW;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 487655
    :cond_6
    iget-object v0, p0, LX/31a;->o:Ljava/lang/CharSequence;

    if-eqz v0, :cond_7

    .line 487656
    const/4 v0, -0x3

    iget-object v1, p0, LX/31a;->o:Ljava/lang/CharSequence;

    iget-object v2, p0, LX/31a;->p:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v0, v1, v2, v3}, LX/4BW;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 487657
    :cond_7
    iget-object v0, p0, LX/31a;->u:[Ljava/lang/CharSequence;

    if-nez v0, :cond_8

    iget-object v0, p0, LX/31a;->J:Landroid/database/Cursor;

    if-nez v0, :cond_8

    iget-object v0, p0, LX/31a;->v:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_9

    .line 487658
    :cond_8
    invoke-direct {p0, p1}, LX/31a;->b(LX/4BW;)V

    .line 487659
    :cond_9
    iget-object v0, p0, LX/31a;->x:Landroid/view/View;

    if-eqz v0, :cond_a

    .line 487660
    iget-boolean v0, p0, LX/31a;->D:Z

    if-eqz v0, :cond_d

    .line 487661
    iget-object v1, p0, LX/31a;->x:Landroid/view/View;

    iget v2, p0, LX/31a;->z:I

    iget v3, p0, LX/31a;->A:I

    iget v4, p0, LX/31a;->B:I

    iget v5, p0, LX/31a;->C:I

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/4BW;->a(Landroid/view/View;IIII)V

    .line 487662
    :cond_a
    :goto_1
    iget-boolean v0, p0, LX/31a;->P:Z

    .line 487663
    iput-boolean v0, p1, LX/4BW;->L:Z

    .line 487664
    return-void

    .line 487665
    :cond_b
    iget-object v0, p0, LX/31a;->f:Ljava/lang/CharSequence;

    if-eqz v0, :cond_c

    .line 487666
    iget-object v0, p0, LX/31a;->f:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, LX/4BW;->a(Ljava/lang/CharSequence;)V

    .line 487667
    :cond_c
    iget v0, p0, LX/31a;->e:I

    if-lez v0, :cond_0

    .line 487668
    iget v0, p0, LX/31a;->e:I

    .line 487669
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 487670
    iget-object v2, p1, LX/4BW;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 487671
    goto/16 :goto_0

    .line 487672
    :cond_d
    iget-object v0, p0, LX/31a;->x:Landroid/view/View;

    invoke-virtual {p1, v0}, LX/4BW;->c(Landroid/view/View;)V

    goto :goto_1
.end method
