.class public LX/2Fw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2D5;


# instance fields
.field private final a:LX/0y3;

.field private final b:LX/1wc;

.field public final c:LX/03V;


# direct methods
.method public constructor <init>(LX/0y3;LX/1wc;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387483
    iput-object p1, p0, LX/2Fw;->a:LX/0y3;

    .line 387484
    iput-object p2, p0, LX/2Fw;->b:LX/1wc;

    .line 387485
    iput-object p3, p0, LX/2Fw;->c:LX/03V;

    .line 387486
    return-void
.end method

.method private static a(LX/2Cv;)I
    .locals 3

    .prologue
    .line 387487
    sget-object v0, LX/6ZN;->b:[I

    invoke-virtual {p0}, LX/2Cv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 387488
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown priority: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387489
    :pswitch_0
    const/16 v0, 0x69

    .line 387490
    :goto_0
    return v0

    .line 387491
    :pswitch_1
    const/16 v0, 0x68

    goto :goto_0

    .line 387492
    :pswitch_2
    const/16 v0, 0x66

    goto :goto_0

    .line 387493
    :pswitch_3
    const/16 v0, 0x64

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a(LX/2Cu;)Lcom/google/android/gms/location/LocationRequest;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 387463
    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iget-object v1, p0, LX/2Cu;->a:LX/2Cv;

    invoke-static {v1}, LX/2Fw;->a(LX/2Cv;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iget-wide v2, p0, LX/2Cu;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iget-wide v2, p0, LX/2Cu;->c:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->c(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iget v1, p0, LX/2Cu;->d:F

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(F)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/2wX;LX/6ZK;)V
    .locals 4

    .prologue
    .line 387496
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 387497
    :try_start_0
    invoke-virtual {p1}, LX/2wX;->e()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 387498
    :goto_0
    return-void

    .line 387499
    :catch_0
    move-exception v0

    .line 387500
    invoke-static {v0}, LX/3KX;->a(Ljava/lang/RuntimeException;)V

    .line 387501
    iget-object v1, p0, LX/2Fw;->c:LX/03V;

    const-string v2, "fb_location_continuous_listener_google_play"

    const-string v3, "Google exception on connect"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 387502
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/6ZK;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/2Fw;)V
    .locals 3

    .prologue
    .line 387494
    iget-object v0, p0, LX/2Fw;->c:LX/03V;

    const-string v1, "fb_location_continuous_listener_google_play"

    const-string v2, "Client disconnected unexpectedly"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 387495
    return-void
.end method

.method public static a$redex0(LX/2Fw;LX/2wX;Landroid/app/PendingIntent;LX/6ZK;)V
    .locals 4

    .prologue
    .line 387464
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 387465
    :try_start_0
    sget-object v0, LX/2vm;->b:LX/2vu;

    invoke-interface {v0, p1, p2}, LX/2vu;->a(LX/2wX;Landroid/app/PendingIntent;)LX/2wg;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 387466
    :goto_0
    return-void

    .line 387467
    :catch_0
    move-exception v0

    .line 387468
    iget-object v1, p0, LX/2Fw;->c:LX/03V;

    const-string v2, "fb_location_continuous_listener_google_play"

    const-string v3, "missing permissions"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 387469
    :catch_1
    move-exception v0

    .line 387470
    invoke-static {v0}, LX/3KX;->a(Ljava/lang/RuntimeException;)V

    .line 387471
    iget-object v1, p0, LX/2Fw;->c:LX/03V;

    const-string v2, "fb_location_continuous_listener_google_play"

    const-string v3, "Google exception on remove"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 387472
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, LX/6ZK;->a(I)V

    goto :goto_0
.end method

.method public static a$redex0(LX/2Fw;LX/2wX;Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;LX/6ZK;)V
    .locals 4

    .prologue
    .line 387473
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 387474
    :try_start_0
    sget-object v0, LX/2vm;->b:LX/2vu;

    invoke-interface {v0, p1, p2, p3}, LX/2vu;->a(LX/2wX;Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)LX/2wg;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 387475
    :goto_0
    return-void

    .line 387476
    :catch_0
    move-exception v0

    .line 387477
    iget-object v1, p0, LX/2Fw;->c:LX/03V;

    const-string v2, "fb_location_continuous_listener_google_play"

    const-string v3, "missing permissions"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 387478
    :catch_1
    move-exception v0

    .line 387479
    invoke-static {v0}, LX/3KX;->a(Ljava/lang/RuntimeException;)V

    .line 387480
    iget-object v1, p0, LX/2Fw;->c:LX/03V;

    const-string v2, "fb_location_continuous_listener_google_play"

    const-string v3, "Google exception on request"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 387481
    const/4 v0, 0x1

    invoke-virtual {p4, v0}, LX/6ZK;->a(I)V

    goto :goto_0
.end method

.method public static a$redex0(LX/2Fw;Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 4
    .param p0    # LX/2Fw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 387441
    iget-object v0, p0, LX/2Fw;->c:LX/03V;

    const-string v1, "fb_location_continuous_listener_google_play"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Client connection failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 387442
    return-void
.end method

.method public static b(LX/0QB;)LX/2Fw;
    .locals 4

    .prologue
    .line 387431
    new-instance v3, LX/2Fw;

    invoke-static {p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v0

    check-cast v0, LX/0y3;

    invoke-static {p0}, LX/1wc;->b(LX/0QB;)LX/1wc;

    move-result-object v1

    check-cast v1, LX/1wc;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-direct {v3, v0, v1, v2}, LX/2Fw;-><init>(LX/0y3;LX/1wc;LX/03V;)V

    .line 387432
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Lcom/facebook/location/ImmutableLocation;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 387433
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/location/LocationResult;->a(Landroid/content/Intent;)Lcom/google/android/gms/location/LocationResult;

    move-result-object v0

    .line 387434
    if-eqz v0, :cond_0

    .line 387435
    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationResult;->a()Landroid/location/Location;

    move-result-object v0

    .line 387436
    invoke-static {v0}, Lcom/facebook/location/ImmutableLocation;->c(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 387437
    :goto_0
    return-object v0

    .line 387438
    :catch_0
    move-exception v0

    .line 387439
    iget-object v1, p0, LX/2Fw;->c:LX/03V;

    const-string v2, "fb_location_continuous_listener_google_play"

    const-string v3, "Error unparceling location"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 387440
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 3

    .prologue
    .line 387443
    new-instance v0, LX/6ZM;

    invoke-direct {v0, p0, p1}, LX/6ZM;-><init>(LX/2Fw;Landroid/app/PendingIntent;)V

    .line 387444
    iget-object v1, p0, LX/2Fw;->b:LX/1wc;

    sget-object v2, LX/2vm;->a:LX/2vs;

    invoke-virtual {v1, v0, v0, v2}, LX/1wc;->a(LX/1qf;LX/1qg;LX/2vs;)LX/2wX;

    move-result-object v1

    .line 387445
    iput-object v1, v0, LX/6ZK;->a:LX/2wX;

    .line 387446
    invoke-direct {p0, v1, v0}, LX/2Fw;->a(LX/2wX;LX/6ZK;)V

    .line 387447
    return-void
.end method

.method public final a(Landroid/app/PendingIntent;LX/2Cu;)V
    .locals 1

    .prologue
    .line 387448
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 387449
    invoke-static {p2}, LX/2Fw;->a(LX/2Cu;)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/2Fw;->a(Landroid/app/PendingIntent;Lcom/google/android/gms/location/LocationRequest;)V

    .line 387450
    return-void
.end method

.method public final a(Landroid/app/PendingIntent;Lcom/google/android/gms/location/LocationRequest;)V
    .locals 4

    .prologue
    .line 387451
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 387452
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 387453
    iget-object v0, p0, LX/2Fw;->a:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    .line 387454
    sget-object v1, LX/6ZN;->a:[I

    invoke-virtual {v0}, LX/0yG;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 387455
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown location state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 387456
    :pswitch_0
    new-instance v0, LX/6ZF;

    sget-object v1, LX/6ZE;->LOCATION_UNSUPPORTED:LX/6ZE;

    invoke-direct {v0, v1}, LX/6ZF;-><init>(LX/6ZE;)V

    throw v0

    .line 387457
    :pswitch_1
    new-instance v0, LX/6ZF;

    sget-object v1, LX/6ZE;->PERMISSION_DENIED:LX/6ZE;

    invoke-direct {v0, v1}, LX/6ZF;-><init>(LX/6ZE;)V

    throw v0

    .line 387458
    :pswitch_2
    new-instance v0, LX/6ZL;

    invoke-direct {v0, p0, p2, p1}, LX/6ZL;-><init>(LX/2Fw;Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V

    .line 387459
    iget-object v1, p0, LX/2Fw;->b:LX/1wc;

    sget-object v2, LX/2vm;->a:LX/2vs;

    invoke-virtual {v1, v0, v0, v2}, LX/1wc;->a(LX/1qf;LX/1qg;LX/2vs;)LX/2wX;

    move-result-object v1

    .line 387460
    iput-object v1, v0, LX/6ZK;->a:LX/2wX;

    .line 387461
    invoke-direct {p0, v1, v0}, LX/2Fw;->a(LX/2wX;LX/6ZK;)V

    .line 387462
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
