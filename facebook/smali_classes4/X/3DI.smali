.class public LX/3DI;
.super Landroid/text/SpannableStringBuilder;
.source ""


# instance fields
.field private a:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 532093
    const-string v0, "\u00a0\u00a0\u2022\u00a0\u00a0"

    invoke-direct {p0, v0}, LX/3DI;-><init>(Ljava/lang/CharSequence;)V

    .line 532094
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 532090
    invoke-direct {p0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 532091
    iput-object p1, p0, LX/3DI;->a:Ljava/lang/CharSequence;

    .line 532092
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)LX/3DI;
    .locals 2

    .prologue
    .line 532102
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/3DI;->a(Ljava/lang/CharSequence;Ljava/lang/Object;I)LX/3DI;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/Object;I)LX/3DI;
    .locals 2

    .prologue
    .line 532095
    invoke-virtual {p0}, LX/3DI;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 532096
    iget-object v0, p0, LX/3DI;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, LX/3DI;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 532097
    :cond_0
    invoke-virtual {p0}, LX/3DI;->length()I

    move-result v0

    .line 532098
    invoke-virtual {p0, p1}, LX/3DI;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 532099
    if-eqz p2, :cond_1

    .line 532100
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p0, p2, v0, v1, p3}, LX/3DI;->setSpan(Ljava/lang/Object;III)V

    .line 532101
    :cond_1
    return-object p0
.end method
