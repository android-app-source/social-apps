.class public final LX/2SA;
.super LX/2SB;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation

.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/2SB",
        "<TE;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field private final delegate:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final maxSize:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method private constructor <init>(I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 411886
    invoke-direct {p0}, LX/2SB;-><init>()V

    .line 411887
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "maxSize (%s) must >= 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 411888
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0, p1}, Ljava/util/ArrayDeque;-><init>(I)V

    move-object v0, v0

    .line 411889
    iput-object v0, p0, LX/2SA;->delegate:Ljava/util/Queue;

    .line 411890
    iput p1, p0, LX/2SA;->maxSize:I

    .line 411891
    return-void

    :cond_0
    move v0, v2

    .line 411892
    goto :goto_0
.end method

.method public static a(I)LX/2SA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(I)",
            "LX/2SA",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 411885
    new-instance v0, LX/2SA;

    invoke-direct {v0, p0}, LX/2SA;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Queue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Queue",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 411884
    iget-object v0, p0, LX/2SA;->delegate:Ljava/util/Queue;

    return-object v0
.end method

.method public final add(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 411878
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 411879
    iget v0, p0, LX/2SA;->maxSize:I

    if-nez v0, :cond_0

    .line 411880
    :goto_0
    return v2

    .line 411881
    :cond_0
    invoke-virtual {p0}, LX/306;->size()I

    move-result v0

    iget v1, p0, LX/2SA;->maxSize:I

    if-ne v0, v1, :cond_1

    .line 411882
    iget-object v0, p0, LX/2SA;->delegate:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 411883
    :cond_1
    iget-object v0, p0, LX/2SA;->delegate:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 411877
    invoke-virtual {p0, p1}, LX/306;->b(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final synthetic b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 411872
    invoke-virtual {p0}, LX/2SA;->a()Ljava/util/Queue;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 411876
    invoke-virtual {p0}, LX/2SA;->a()Ljava/util/Queue;

    move-result-object v0

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 411875
    invoke-virtual {p0}, LX/2SA;->a()Ljava/util/Queue;

    move-result-object v0

    return-object v0
.end method

.method public final offer(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 411874
    invoke-virtual {p0, p1}, LX/306;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 411873
    invoke-virtual {p0}, LX/2SA;->a()Ljava/util/Queue;

    move-result-object v0

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
