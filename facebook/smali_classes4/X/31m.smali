.class public LX/31m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/31j;


# instance fields
.field public final a:LX/1PT;

.field public final b:LX/1EN;

.field private final c:LX/31k;

.field private final d:Landroid/content/res/Resources;

.field public e:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLComment;

.field public g:LX/1EO;


# direct methods
.method public constructor <init>(LX/1PT;LX/1EN;LX/31k;Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 488095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488096
    iput-object p1, p0, LX/31m;->a:LX/1PT;

    .line 488097
    iput-object p2, p0, LX/31m;->b:LX/1EN;

    .line 488098
    iput-object p3, p0, LX/31m;->c:LX/31k;

    .line 488099
    iput-object p4, p0, LX/31m;->d:Landroid/content/res/Resources;

    .line 488100
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1EO;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1EO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 488101
    const/4 v0, 0x0

    .line 488102
    iput-object p1, p0, LX/31m;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 488103
    iput-object v0, p0, LX/31m;->f:Lcom/facebook/graphql/model/GraphQLComment;

    .line 488104
    iput-object p2, p0, LX/31m;->g:LX/1EO;

    .line 488105
    return-void
.end method

.method public onClick(Landroid/view/View;FF)V
    .locals 9

    .prologue
    .line 488106
    iget-object v0, p0, LX/31m;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 488107
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 488108
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 488109
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 488110
    :cond_0
    :goto_0
    return-void

    .line 488111
    :cond_1
    iget-object v0, p0, LX/31m;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b0a3e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 488112
    iget-object v1, p0, LX/31m;->c:LX/31k;

    invoke-virtual {v1, p1, p2, p3, v0}, LX/31k;->a(Landroid/view/View;FFF)Landroid/text/style/ClickableSpan;

    move-result-object v0

    .line 488113
    if-nez v0, :cond_2

    .line 488114
    iget-object v2, p0, LX/31m;->b:LX/1EN;

    iget-object v3, p0, LX/31m;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p0, LX/31m;->a:LX/1PT;

    invoke-interface {v4}, LX/1PT;->a()LX/1Qt;

    move-result-object v4

    iget-object v5, p0, LX/31m;->f:Lcom/facebook/graphql/model/GraphQLComment;

    sget-object v7, LX/An0;->MESSAGE:LX/An0;

    iget-object v8, p0, LX/31m;->g:LX/1EO;

    move-object v6, p1

    invoke-virtual/range {v2 .. v8}, LX/1EN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View;LX/An0;LX/1EO;)V

    .line 488115
    goto :goto_0

    .line 488116
    :cond_2
    sget-object v1, LX/31k;->a:Landroid/text/style/ClickableSpan;

    if-eq v0, v1, :cond_0

    .line 488117
    invoke-virtual {v0, p1}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method
