.class public final LX/3HW;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/3JC;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)V
    .locals 0

    .prologue
    .line 543567
    iput-object p1, p0, LX/3HW;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/3JC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 543568
    const-class v0, LX/3JC;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 543569
    check-cast p1, LX/3JC;

    .line 543570
    sget-object v0, LX/D7A;->a:[I

    iget-object v1, p1, LX/3JC;->a:LX/2qV;

    invoke-virtual {v1}, LX/2qV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 543571
    :cond_0
    :goto_0
    return-void

    .line 543572
    :pswitch_0
    iget-object v0, p0, LX/3HW;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v0, :cond_1

    .line 543573
    iget-object v0, p0, LX/3HW;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    const-wide/16 v2, 0x0

    sget-object v1, LX/BST;->PLAYBACK_ERROR:LX/BST;

    invoke-virtual {v0, v2, v3, v1}, LX/D6v;->a(JLX/BST;)V

    .line 543574
    :cond_1
    iget-object v0, p0, LX/3HW;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->e:LX/03V;

    sget-object v1, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->q:Ljava/lang/String;

    const-string v2, "Commercial break RVP playback error"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 543575
    :pswitch_1
    iget-object v0, p0, LX/3HW;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v0, :cond_0

    .line 543576
    iget-object v0, p0, LX/3HW;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    invoke-virtual {v0}, LX/D6v;->c()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
