.class public final enum LX/32P;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/32P;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/32P;

.field public static final enum ABOUT_TITLE_CLICK:LX/32P;

.field public static final enum CARRIER_MANAGER:LX/32P;

.field public static final enum DEBUG:LX/32P;

.field public static final enum FACEWEB:LX/32P;

.field public static final enum FREE_DATA_CAPPING_WALLET_EXPIRED:LX/32P;

.field public static final enum FREE_DATA_CAP_REACHED_FORCE_FETCH:LX/32P;

.field public static final enum FREE_FACEBOOK_LAUNCH_PUSH:LX/32P;

.field public static final enum GATEKEEPER_CHANGED:LX/32P;

.field public static final enum GRAPHQL_VERIFICATION:LX/32P;

.field public static final enum HEADERS_REFRESH:LX/32P;

.field public static final enum HEADER_ERROR_FORCE_FETCH:LX/32P;

.field public static final enum HEADER_PARAM_MISMATCH:LX/32P;

.field public static final enum INCENTIVE_PROVISIONED_FORCE_FETCH:LX/32P;

.field public static final enum LOGIN:LX/32P;

.field public static final enum MCCMNC_CHANGED:LX/32P;

.field public static final enum MESSENGE_CAP_OPTIN:LX/32P;

.field public static final enum OFFPEAK_DOWNLOAD_REFRESH:LX/32P;

.field public static final enum OPTIN:LX/32P;

.field public static final enum PREFETCH:LX/32P;

.field public static final enum PUSH:LX/32P;

.field public static final enum TOKEN_FETCH_FAILED_RETRY:LX/32P;

.field public static final enum TTL_EXPIRED:LX/32P;

.field public static final enum UNKNOWN_REASON:LX/32P;

.field public static final enum UNKNOWN_STATE:LX/32P;

.field public static final enum UPSELL:LX/32P;


# instance fields
.field private final mRequestString:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 489948
    new-instance v0, LX/32P;

    const-string v1, "OPTIN"

    const-string v2, "optin"

    invoke-direct {v0, v1, v4, v2}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->OPTIN:LX/32P;

    .line 489949
    new-instance v0, LX/32P;

    const-string v1, "MESSENGE_CAP_OPTIN"

    const-string v2, "message_cap_optin"

    invoke-direct {v0, v1, v5, v2}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->MESSENGE_CAP_OPTIN:LX/32P;

    .line 489950
    new-instance v0, LX/32P;

    const-string v1, "CARRIER_MANAGER"

    const-string v2, "carrier_manager"

    invoke-direct {v0, v1, v6, v2}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->CARRIER_MANAGER:LX/32P;

    .line 489951
    new-instance v0, LX/32P;

    const-string v1, "PUSH"

    const-string v2, "token_push"

    invoke-direct {v0, v1, v7, v2}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->PUSH:LX/32P;

    .line 489952
    new-instance v0, LX/32P;

    const-string v1, "LOGIN"

    const-string v2, "login"

    invoke-direct {v0, v1, v8, v2}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->LOGIN:LX/32P;

    .line 489953
    new-instance v0, LX/32P;

    const-string v1, "HEADERS_REFRESH"

    const/4 v2, 0x5

    const-string v3, "headers"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->HEADERS_REFRESH:LX/32P;

    .line 489954
    new-instance v0, LX/32P;

    const-string v1, "MCCMNC_CHANGED"

    const/4 v2, 0x6

    const-string v3, "mccmnc_changed"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->MCCMNC_CHANGED:LX/32P;

    .line 489955
    new-instance v0, LX/32P;

    const-string v1, "UNKNOWN_STATE"

    const/4 v2, 0x7

    const-string v3, "unknown_state"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->UNKNOWN_STATE:LX/32P;

    .line 489956
    new-instance v0, LX/32P;

    const-string v1, "TTL_EXPIRED"

    const/16 v2, 0x8

    const-string v3, "ttl_expired"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->TTL_EXPIRED:LX/32P;

    .line 489957
    new-instance v0, LX/32P;

    const-string v1, "GATEKEEPER_CHANGED"

    const/16 v2, 0x9

    const-string v3, "gatekeeper_changed"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->GATEKEEPER_CHANGED:LX/32P;

    .line 489958
    new-instance v0, LX/32P;

    const-string v1, "FACEWEB"

    const/16 v2, 0xa

    const-string v3, "faceweb"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->FACEWEB:LX/32P;

    .line 489959
    new-instance v0, LX/32P;

    const-string v1, "ABOUT_TITLE_CLICK"

    const/16 v2, 0xb

    const-string v3, "about_title_click"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->ABOUT_TITLE_CLICK:LX/32P;

    .line 489960
    new-instance v0, LX/32P;

    const-string v1, "UPSELL"

    const/16 v2, 0xc

    const-string v3, "upsell"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->UPSELL:LX/32P;

    .line 489961
    new-instance v0, LX/32P;

    const-string v1, "DEBUG"

    const/16 v2, 0xd

    const-string v3, "debug"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->DEBUG:LX/32P;

    .line 489962
    new-instance v0, LX/32P;

    const-string v1, "PREFETCH"

    const/16 v2, 0xe

    const-string v3, "prefetch"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->PREFETCH:LX/32P;

    .line 489963
    new-instance v0, LX/32P;

    const-string v1, "GRAPHQL_VERIFICATION"

    const/16 v2, 0xf

    const-string v3, "debug_graphql_verification"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->GRAPHQL_VERIFICATION:LX/32P;

    .line 489964
    new-instance v0, LX/32P;

    const-string v1, "UNKNOWN_REASON"

    const/16 v2, 0x10

    const-string v3, "unknown_reason"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->UNKNOWN_REASON:LX/32P;

    .line 489965
    new-instance v0, LX/32P;

    const-string v1, "FREE_FACEBOOK_LAUNCH_PUSH"

    const/16 v2, 0x11

    const-string v3, "free_facebook_launch_push"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->FREE_FACEBOOK_LAUNCH_PUSH:LX/32P;

    .line 489966
    new-instance v0, LX/32P;

    const-string v1, "TOKEN_FETCH_FAILED_RETRY"

    const/16 v2, 0x12

    const-string v3, "token_fetch_failed_retry"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->TOKEN_FETCH_FAILED_RETRY:LX/32P;

    .line 489967
    new-instance v0, LX/32P;

    const-string v1, "HEADER_PARAM_MISMATCH"

    const/16 v2, 0x13

    const-string v3, "header_param_mismatch"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->HEADER_PARAM_MISMATCH:LX/32P;

    .line 489968
    new-instance v0, LX/32P;

    const-string v1, "HEADER_ERROR_FORCE_FETCH"

    const/16 v2, 0x14

    const-string v3, "header_error_force_fetch"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->HEADER_ERROR_FORCE_FETCH:LX/32P;

    .line 489969
    new-instance v0, LX/32P;

    const-string v1, "INCENTIVE_PROVISIONED_FORCE_FETCH"

    const/16 v2, 0x15

    const-string v3, "incentive_provisioned_force_fetch"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->INCENTIVE_PROVISIONED_FORCE_FETCH:LX/32P;

    .line 489970
    new-instance v0, LX/32P;

    const-string v1, "FREE_DATA_CAP_REACHED_FORCE_FETCH"

    const/16 v2, 0x16

    const-string v3, "free_data_cap_force_fetch"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->FREE_DATA_CAP_REACHED_FORCE_FETCH:LX/32P;

    .line 489971
    new-instance v0, LX/32P;

    const-string v1, "FREE_DATA_CAPPING_WALLET_EXPIRED"

    const/16 v2, 0x17

    const-string v3, "free_data_capping_wallet_expired"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->FREE_DATA_CAPPING_WALLET_EXPIRED:LX/32P;

    .line 489972
    new-instance v0, LX/32P;

    const-string v1, "OFFPEAK_DOWNLOAD_REFRESH"

    const/16 v2, 0x18

    const-string v3, "offpeak_download_refresh"

    invoke-direct {v0, v1, v2, v3}, LX/32P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/32P;->OFFPEAK_DOWNLOAD_REFRESH:LX/32P;

    .line 489973
    const/16 v0, 0x19

    new-array v0, v0, [LX/32P;

    sget-object v1, LX/32P;->OPTIN:LX/32P;

    aput-object v1, v0, v4

    sget-object v1, LX/32P;->MESSENGE_CAP_OPTIN:LX/32P;

    aput-object v1, v0, v5

    sget-object v1, LX/32P;->CARRIER_MANAGER:LX/32P;

    aput-object v1, v0, v6

    sget-object v1, LX/32P;->PUSH:LX/32P;

    aput-object v1, v0, v7

    sget-object v1, LX/32P;->LOGIN:LX/32P;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/32P;->HEADERS_REFRESH:LX/32P;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/32P;->MCCMNC_CHANGED:LX/32P;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/32P;->UNKNOWN_STATE:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/32P;->TTL_EXPIRED:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/32P;->GATEKEEPER_CHANGED:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/32P;->FACEWEB:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/32P;->ABOUT_TITLE_CLICK:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/32P;->UPSELL:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/32P;->DEBUG:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/32P;->PREFETCH:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/32P;->GRAPHQL_VERIFICATION:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/32P;->UNKNOWN_REASON:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/32P;->FREE_FACEBOOK_LAUNCH_PUSH:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/32P;->TOKEN_FETCH_FAILED_RETRY:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/32P;->HEADER_PARAM_MISMATCH:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/32P;->HEADER_ERROR_FORCE_FETCH:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/32P;->INCENTIVE_PROVISIONED_FORCE_FETCH:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/32P;->FREE_DATA_CAP_REACHED_FORCE_FETCH:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/32P;->FREE_DATA_CAPPING_WALLET_EXPIRED:LX/32P;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/32P;->OFFPEAK_DOWNLOAD_REFRESH:LX/32P;

    aput-object v2, v0, v1

    sput-object v0, LX/32P;->$VALUES:[LX/32P;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 489974
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 489975
    iput-object p3, p0, LX/32P;->mRequestString:Ljava/lang/String;

    .line 489976
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/32P;
    .locals 5

    .prologue
    .line 489977
    invoke-static {}, LX/32P;->values()[LX/32P;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 489978
    invoke-virtual {v0}, LX/32P;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 489979
    :goto_1
    return-object v0

    .line 489980
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 489981
    :cond_1
    sget-object v0, LX/32P;->UNKNOWN_REASON:LX/32P;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/32P;
    .locals 1

    .prologue
    .line 489982
    const-class v0, LX/32P;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/32P;

    return-object v0
.end method

.method public static values()[LX/32P;
    .locals 1

    .prologue
    .line 489983
    sget-object v0, LX/32P;->$VALUES:[LX/32P;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/32P;

    return-object v0
.end method


# virtual methods
.method public final getRequestString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 489984
    iget-object v0, p0, LX/32P;->mRequestString:Ljava/lang/String;

    return-object v0
.end method
