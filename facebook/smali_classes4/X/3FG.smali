.class public final enum LX/3FG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3FG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3FG;

.field public static final enum HANDLE_LOCATION_RESULT:LX/3FG;

.field public static final enum PROCESS_LOCATION:LX/3FG;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 538680
    new-instance v0, LX/3FG;

    const-string v1, "PROCESS_LOCATION"

    invoke-direct {v0, v1, v2}, LX/3FG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3FG;->PROCESS_LOCATION:LX/3FG;

    .line 538681
    new-instance v0, LX/3FG;

    const-string v1, "HANDLE_LOCATION_RESULT"

    invoke-direct {v0, v1, v3}, LX/3FG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3FG;->HANDLE_LOCATION_RESULT:LX/3FG;

    .line 538682
    const/4 v0, 0x2

    new-array v0, v0, [LX/3FG;

    sget-object v1, LX/3FG;->PROCESS_LOCATION:LX/3FG;

    aput-object v1, v0, v2

    sget-object v1, LX/3FG;->HANDLE_LOCATION_RESULT:LX/3FG;

    aput-object v1, v0, v3

    sput-object v0, LX/3FG;->$VALUES:[LX/3FG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 538684
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3FG;
    .locals 1

    .prologue
    .line 538685
    const-class v0, LX/3FG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3FG;

    return-object v0
.end method

.method public static values()[LX/3FG;
    .locals 1

    .prologue
    .line 538683
    sget-object v0, LX/3FG;->$VALUES:[LX/3FG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3FG;

    return-object v0
.end method
