.class public LX/2RB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2RB;


# instance fields
.field public a:LX/2RC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2RQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2RB;
    .locals 5

    .prologue
    .line 409538
    sget-object v0, LX/2RB;->c:LX/2RB;

    if-nez v0, :cond_1

    .line 409539
    const-class v1, LX/2RB;

    monitor-enter v1

    .line 409540
    :try_start_0
    sget-object v0, LX/2RB;->c:LX/2RB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 409541
    if-eqz v2, :cond_0

    .line 409542
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 409543
    new-instance p0, LX/2RB;

    invoke-direct {p0}, LX/2RB;-><init>()V

    .line 409544
    invoke-static {v0}, LX/2RC;->a(LX/0QB;)LX/2RC;

    move-result-object v3

    check-cast v3, LX/2RC;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v4

    check-cast v4, LX/2RQ;

    .line 409545
    iput-object v3, p0, LX/2RB;->a:LX/2RC;

    iput-object v4, p0, LX/2RB;->b:LX/2RQ;

    .line 409546
    move-object v0, p0

    .line 409547
    sput-object v0, LX/2RB;->c:LX/2RB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 409548
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 409549
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 409550
    :cond_1
    sget-object v0, LX/2RB;->c:LX/2RB;

    return-object v0

    .line 409551
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 409552
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
