.class public LX/3MI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static volatile f:LX/3MI;


# instance fields
.field public d:LX/3MK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 554246
    new-array v0, v6, [Ljava/lang/String;

    sget-object v1, LX/3MJ;->a:LX/0U1;

    .line 554247
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 554248
    aput-object v1, v0, v2

    sget-object v1, LX/3MJ;->e:LX/0U1;

    .line 554249
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 554250
    aput-object v1, v0, v3

    sget-object v1, LX/3MJ;->f:LX/0U1;

    .line 554251
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 554252
    aput-object v1, v0, v4

    sget-object v1, LX/3MJ;->g:LX/0U1;

    .line 554253
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 554254
    aput-object v1, v0, v5

    sput-object v0, LX/3MI;->a:[Ljava/lang/String;

    .line 554255
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, LX/3MJ;->c:LX/0U1;

    .line 554256
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 554257
    aput-object v1, v0, v2

    sget-object v1, LX/3MJ;->d:LX/0U1;

    .line 554258
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 554259
    aput-object v1, v0, v3

    sget-object v1, LX/3MJ;->e:LX/0U1;

    .line 554260
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 554261
    aput-object v1, v0, v4

    sget-object v1, LX/3MJ;->f:LX/0U1;

    .line 554262
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 554263
    aput-object v1, v0, v5

    sget-object v1, LX/3MJ;->g:LX/0U1;

    .line 554264
    iget-object v5, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v5

    .line 554265
    aput-object v1, v0, v6

    sput-object v0, LX/3MI;->b:[Ljava/lang/String;

    .line 554266
    new-array v0, v4, [Ljava/lang/String;

    sget-object v1, LX/3MJ;->c:LX/0U1;

    .line 554267
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 554268
    aput-object v1, v0, v2

    sget-object v1, LX/3MJ;->d:LX/0U1;

    .line 554269
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 554270
    aput-object v1, v0, v3

    sput-object v0, LX/3MI;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 554245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/3MI;
    .locals 5

    .prologue
    .line 554074
    sget-object v0, LX/3MI;->f:LX/3MI;

    if-nez v0, :cond_1

    .line 554075
    const-class v1, LX/3MI;

    monitor-enter v1

    .line 554076
    :try_start_0
    sget-object v0, LX/3MI;->f:LX/3MI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 554077
    if-eqz v2, :cond_0

    .line 554078
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 554079
    new-instance p0, LX/3MI;

    invoke-direct {p0}, LX/3MI;-><init>()V

    .line 554080
    invoke-static {v0}, LX/3MK;->a(LX/0QB;)LX/3MK;

    move-result-object v3

    check-cast v3, LX/3MK;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    .line 554081
    iput-object v3, p0, LX/3MI;->d:LX/3MK;

    iput-object v4, p0, LX/3MI;->e:LX/0SG;

    .line 554082
    move-object v0, p0

    .line 554083
    sput-object v0, LX/3MI;->f:LX/3MI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 554084
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 554085
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 554086
    :cond_1
    sget-object v0, LX/3MI;->f:LX/3MI;

    return-object v0

    .line 554087
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 554088
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/3MI;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 554231
    sget-object v0, LX/3MJ;->a:LX/0U1;

    .line 554232
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 554233
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 554234
    iget-object v1, p0, LX/3MI;->d:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, 0x7f7fa9c4

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 554235
    :try_start_0
    iget-object v1, p0, LX/3MI;->d:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "address_table"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, p2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 554236
    if-nez v0, :cond_0

    .line 554237
    sget-object v0, LX/3MJ;->a:LX/0U1;

    .line 554238
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 554239
    invoke-virtual {p2, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 554240
    iget-object v0, p0, LX/3MI;->d:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "address_table"

    const/4 v2, 0x0

    const v3, -0x260982ac

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {v0, v1, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x5e2114d7

    invoke-static {v0}, LX/03h;->a(I)V

    .line 554241
    :cond_0
    iget-object v0, p0, LX/3MI;->d:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 554242
    iget-object v0, p0, LX/3MI;->d:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, 0x16f26241

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 554243
    return-void

    .line 554244
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/3MI;->d:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, 0x44f78f20

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/6jO;
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    .line 554208
    sget-object v0, LX/3MJ;->a:LX/0U1;

    .line 554209
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 554210
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 554211
    :try_start_0
    iget-object v0, p0, LX/3MI;->d:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "address_table"

    sget-object v2, LX/3MI;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 554212
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 554213
    sget-object v0, LX/3MJ;->e:LX/0U1;

    invoke-virtual {v0, v8}, LX/0U1;->e(Landroid/database/Cursor;)D

    move-result-wide v2

    .line 554214
    sget-object v0, LX/3MJ;->f:LX/0U1;

    invoke-virtual {v0, v8}, LX/0U1;->e(Landroid/database/Cursor;)D

    move-result-wide v4

    .line 554215
    sget-object v0, LX/3MJ;->g:LX/0U1;

    invoke-virtual {v0, v8}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v6

    .line 554216
    new-instance v0, LX/6jO;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, LX/6jO;-><init>(Ljava/lang/String;DDJ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 554217
    if-eqz v8, :cond_0

    .line 554218
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 554219
    :cond_0
    :goto_0
    return-object v0

    .line 554220
    :cond_1
    if-eqz v8, :cond_2

    .line 554221
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 554222
    :cond_2
    :goto_1
    new-instance v0, LX/6jO;

    const-wide/16 v6, -0x1

    move-object v1, p1

    move-wide v2, v10

    move-wide v4, v10

    invoke-direct/range {v0 .. v7}, LX/6jO;-><init>(Ljava/lang/String;DDJ)V

    goto :goto_0

    .line 554223
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 554224
    :goto_2
    :try_start_2
    const-string v2, "SmsTakeoverAddressDbHandler"

    const-string v3, "Error getting spam info"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 554225
    if-eqz v1, :cond_2

    .line 554226
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 554227
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v8, :cond_3

    .line 554228
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 554229
    :catchall_1
    move-exception v0

    move-object v8, v1

    goto :goto_3

    .line 554230
    :catch_1
    move-exception v0

    move-object v1, v8

    goto :goto_2
.end method

.method public final a()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/6jO;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 554186
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 554187
    :try_start_0
    iget-object v0, p0, LX/3MI;->d:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "address_table"

    sget-object v2, LX/3MI;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, LX/3MJ;->f:LX/0U1;

    .line 554188
    iget-object p0, v10, LX/0U1;->d:Ljava/lang/String;

    move-object v10, p0

    .line 554189
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " DESC"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 554190
    if-eqz v8, :cond_1

    .line 554191
    :goto_0
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 554192
    sget-object v0, LX/3MJ;->e:LX/0U1;

    invoke-virtual {v0, v8}, LX/0U1;->e(Landroid/database/Cursor;)D

    move-result-wide v2

    .line 554193
    sget-object v0, LX/3MJ;->f:LX/0U1;

    invoke-virtual {v0, v8}, LX/0U1;->e(Landroid/database/Cursor;)D

    move-result-wide v4

    .line 554194
    sget-object v0, LX/3MJ;->g:LX/0U1;

    invoke-virtual {v0, v8}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v6

    .line 554195
    sget-object v0, LX/3MJ;->a:LX/0U1;

    invoke-virtual {v0, v8}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 554196
    new-instance v0, LX/6jO;

    invoke-direct/range {v0 .. v7}, LX/6jO;-><init>(Ljava/lang/String;DDJ)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 554197
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 554198
    :goto_1
    :try_start_2
    const-string v2, "SmsTakeoverAddressDbHandler"

    const-string v3, "Error getting spam info"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 554199
    if-eqz v1, :cond_0

    .line 554200
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 554201
    :cond_0
    :goto_2
    return-object v9

    .line 554202
    :cond_1
    if-eqz v8, :cond_0

    .line 554203
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 554204
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v8, :cond_2

    .line 554205
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 554206
    :catchall_1
    move-exception v0

    move-object v8, v1

    goto :goto_3

    .line 554207
    :catch_1
    move-exception v0

    move-object v1, v8

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Double;DJ)V
    .locals 3
    .param p2    # Ljava/lang/Double;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 554173
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 554174
    if-eqz p2, :cond_0

    .line 554175
    sget-object v1, LX/3MJ;->e:LX/0U1;

    .line 554176
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 554177
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 554178
    :cond_0
    sget-object v1, LX/3MJ;->f:LX/0U1;

    .line 554179
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 554180
    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 554181
    sget-object v1, LX/3MJ;->g:LX/0U1;

    .line 554182
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 554183
    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 554184
    invoke-static {p0, p1, v0}, LX/3MI;->a(LX/3MI;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 554185
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/6jM;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 554142
    sget-object v0, LX/3MJ;->a:LX/0U1;

    .line 554143
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 554144
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 554145
    :try_start_0
    iget-object v0, p0, LX/3MI;->d:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "address_table"

    sget-object v2, LX/3MI;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 554146
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 554147
    sget-object v0, LX/3MJ;->e:LX/0U1;

    invoke-virtual {v0, v9}, LX/0U1;->e(Landroid/database/Cursor;)D

    move-result-wide v2

    .line 554148
    sget-object v0, LX/3MJ;->f:LX/0U1;

    invoke-virtual {v0, v9}, LX/0U1;->e(Landroid/database/Cursor;)D

    move-result-wide v4

    .line 554149
    sget-object v0, LX/3MJ;->g:LX/0U1;

    invoke-virtual {v0, v9}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v6

    .line 554150
    sget-object v0, LX/3MJ;->c:LX/0U1;

    invoke-virtual {v0, v9}, LX/0U1;->e(Landroid/database/Cursor;)D

    move-result-wide v0

    .line 554151
    sget-object v10, LX/3MJ;->d:LX/0U1;

    invoke-virtual {v10, v9}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v10

    .line 554152
    invoke-static {}, LX/6jM;->newBuilder()LX/6jN;

    move-result-object v12

    .line 554153
    iput-object p1, v12, LX/6jN;->a:Ljava/lang/String;

    .line 554154
    move-object v12, v12

    .line 554155
    invoke-virtual {v12, v0, v1, v10, v11}, LX/6jN;->a(DJ)LX/6jN;

    move-result-object v10

    new-instance v0, LX/6jO;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, LX/6jO;-><init>(Ljava/lang/String;DDJ)V

    .line 554156
    iput-object v0, v10, LX/6jN;->f:LX/6jO;

    .line 554157
    move-object v0, v10

    .line 554158
    invoke-virtual {v0}, LX/6jN;->g()LX/6jM;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 554159
    if-eqz v9, :cond_0

    .line 554160
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 554161
    :cond_0
    :goto_0
    return-object v0

    .line 554162
    :cond_1
    if-eqz v9, :cond_2

    .line 554163
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_1
    move-object v0, v8

    .line 554164
    goto :goto_0

    .line 554165
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 554166
    :goto_2
    :try_start_2
    const-string v2, "SmsTakeoverAddressDbHandler"

    const-string v3, "Error getting spam info"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 554167
    if-eqz v1, :cond_2

    .line 554168
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 554169
    :catchall_0
    move-exception v0

    move-object v9, v8

    :goto_3
    if-eqz v9, :cond_3

    .line 554170
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 554171
    :catchall_1
    move-exception v0

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v9, v1

    goto :goto_3

    .line 554172
    :catch_1
    move-exception v0

    move-object v1, v9

    goto :goto_2
.end method

.method public final c(Ljava/lang/String;)V
    .locals 14

    .prologue
    const/4 v8, 0x0

    .line 554116
    iget-object v0, p0, LX/3MI;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v12

    .line 554117
    const-wide/16 v10, 0x0

    .line 554118
    sget-object v0, LX/3MJ;->a:LX/0U1;

    .line 554119
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 554120
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 554121
    :try_start_0
    iget-object v0, p0, LX/3MI;->d:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "address_table"

    sget-object v2, LX/3MI;->c:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 554122
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 554123
    sget-object v0, LX/3MJ;->c:LX/0U1;

    invoke-virtual {v0, v8}, LX/0U1;->e(Landroid/database/Cursor;)D
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 554124
    :try_start_2
    sget-object v2, LX/3MJ;->d:LX/0U1;

    invoke-virtual {v2, v8}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v2

    .line 554125
    const-wide v6, 0x9a7ec800L

    move-wide v4, v12

    invoke-static/range {v0 .. v7}, LX/6jP;->a(DJJJ)D
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v0

    .line 554126
    :goto_0
    if-eqz v8, :cond_0

    .line 554127
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 554128
    :cond_0
    :goto_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 554129
    sget-object v3, LX/3MJ;->c:LX/0U1;

    .line 554130
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 554131
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 554132
    sget-object v0, LX/3MJ;->d:LX/0U1;

    .line 554133
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 554134
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 554135
    invoke-static {p0, p1, v2}, LX/3MI;->a(LX/3MI;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 554136
    return-void

    .line 554137
    :catch_0
    move-object v2, v8

    move-wide v0, v10

    :goto_2
    if-eqz v2, :cond_0

    .line 554138
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 554139
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_1

    .line 554140
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 554141
    :catch_1
    move-object v2, v8

    move-wide v0, v10

    goto :goto_2

    :catch_2
    move-object v2, v8

    goto :goto_2

    :cond_2
    move-wide v0, v10

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/6jO;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x0

    .line 554089
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 554090
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v8

    .line 554091
    :goto_0
    return-object v0

    .line 554092
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [LX/0ux;

    sget-object v1, LX/3MJ;->a:LX/0U1;

    .line 554093
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 554094
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "%"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x1

    sget-object v2, LX/3MJ;->a:LX/0U1;

    .line 554095
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554096
    const-string v3, "% %"

    invoke-static {v2, v3}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-static {v2}, LX/0uu;->a(LX/0ux;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v4

    .line 554097
    :try_start_0
    iget-object v0, p0, LX/3MI;->d:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "address_table"

    sget-object v2, LX/3MI;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 554098
    if-eqz v9, :cond_2

    .line 554099
    :goto_1
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 554100
    sget-object v0, LX/3MJ;->e:LX/0U1;

    invoke-virtual {v0, v9}, LX/0U1;->e(Landroid/database/Cursor;)D

    move-result-wide v2

    .line 554101
    sget-object v0, LX/3MJ;->f:LX/0U1;

    invoke-virtual {v0, v9}, LX/0U1;->e(Landroid/database/Cursor;)D

    move-result-wide v4

    .line 554102
    sget-object v0, LX/3MJ;->g:LX/0U1;

    invoke-virtual {v0, v9}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v6

    .line 554103
    sget-object v0, LX/3MJ;->a:LX/0U1;

    invoke-virtual {v0, v9}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 554104
    new-instance v0, LX/6jO;

    invoke-direct/range {v0 .. v7}, LX/6jO;-><init>(Ljava/lang/String;DDJ)V

    invoke-virtual {v8, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 554105
    :catch_0
    move-exception v0

    move-object v1, v9

    .line 554106
    :goto_2
    :try_start_2
    const-string v2, "SmsTakeoverAddressDbHandler"

    const-string v3, "Error getting spam info"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 554107
    if-eqz v1, :cond_1

    .line 554108
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_3
    move-object v0, v8

    .line 554109
    goto/16 :goto_0

    .line 554110
    :cond_2
    if-eqz v9, :cond_1

    .line 554111
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 554112
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v9, :cond_3

    .line 554113
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 554114
    :catchall_1
    move-exception v0

    move-object v9, v1

    goto :goto_4

    .line 554115
    :catch_1
    move-exception v0

    move-object v1, v9

    goto :goto_2
.end method
