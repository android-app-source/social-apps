.class public final LX/3Ft;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 540054
    const-wide/16 v16, 0x0

    .line 540055
    const/4 v15, 0x0

    .line 540056
    const/4 v14, 0x0

    .line 540057
    const/4 v13, 0x0

    .line 540058
    const/4 v12, 0x0

    .line 540059
    const/4 v11, 0x0

    .line 540060
    const/4 v10, 0x0

    .line 540061
    const/4 v9, 0x0

    .line 540062
    const/4 v8, 0x0

    .line 540063
    const/4 v7, 0x0

    .line 540064
    const/4 v6, 0x0

    .line 540065
    const/4 v5, 0x0

    .line 540066
    const/4 v4, 0x0

    .line 540067
    const/4 v3, 0x0

    .line 540068
    const/4 v2, 0x0

    .line 540069
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_11

    .line 540070
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 540071
    const/4 v2, 0x0

    .line 540072
    :goto_0
    return v2

    .line 540073
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_e

    .line 540074
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 540075
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 540076
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 540077
    const-string v6, "added_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 540078
    const/4 v2, 0x1

    .line 540079
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 540080
    :cond_1
    const-string v6, "bigPictureUrl"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 540081
    invoke-static/range {p0 .. p1}, LX/3h5;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto :goto_1

    .line 540082
    :cond_2
    const-string v6, "graph_api_write_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 540083
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 540084
    :cond_3
    const-string v6, "hugePictureUrl"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 540085
    invoke-static/range {p0 .. p1}, LX/3h5;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 540086
    :cond_4
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 540087
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 540088
    :cond_5
    const-string v6, "imported_phone_entries"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 540089
    invoke-static/range {p0 .. p1}, LX/6Ma;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 540090
    :cond_6
    const-string v6, "is_on_viewer_contact_list"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 540091
    const/4 v2, 0x1

    .line 540092
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v8, v2

    move v15, v6

    goto/16 :goto_1

    .line 540093
    :cond_7
    const-string v6, "name_entries"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 540094
    invoke-static/range {p0 .. p1}, LX/3h3;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 540095
    :cond_8
    const-string v6, "phonetic_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 540096
    invoke-static/range {p0 .. p1}, LX/3h2;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 540097
    :cond_9
    const-string v6, "represented_profile"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 540098
    invoke-static/range {p0 .. p1}, LX/3h0;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 540099
    :cond_a
    const-string v6, "smallPictureUrl"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 540100
    invoke-static/range {p0 .. p1}, LX/3h5;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 540101
    :cond_b
    const-string v6, "structured_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 540102
    invoke-static/range {p0 .. p1}, LX/3h2;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 540103
    :cond_c
    const-string v6, "viewer_connection_status"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 540104
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 540105
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 540106
    :cond_e
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 540107
    if-eqz v3, :cond_f

    .line 540108
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 540109
    :cond_f
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 540110
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 540111
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 540112
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 540113
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 540114
    if-eqz v8, :cond_10

    .line 540115
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->a(IZ)V

    .line 540116
    :cond_10
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 540117
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 540118
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 540119
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 540120
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 540121
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 540122
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_11
    move/from16 v18, v13

    move/from16 v19, v14

    move/from16 v20, v15

    move v13, v8

    move v14, v9

    move v15, v10

    move v8, v2

    move v9, v4

    move v10, v5

    move-wide/from16 v4, v16

    move/from16 v16, v11

    move/from16 v17, v12

    move v12, v7

    move v11, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 540123
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 540124
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 540125
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/3Ft;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 540126
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 540127
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 540128
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 540129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 540130
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 540131
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 540132
    invoke-static {p0, p1}, LX/3Ft;->a(LX/15w;LX/186;)I

    move-result v1

    .line 540133
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 540134
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/16 v3, 0xc

    .line 540135
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 540136
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 540137
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 540138
    const-string v2, "added_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540139
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 540140
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 540141
    if-eqz v0, :cond_1

    .line 540142
    const-string v1, "bigPictureUrl"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540143
    invoke-static {p0, v0, p2}, LX/3h5;->a(LX/15i;ILX/0nX;)V

    .line 540144
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 540145
    if-eqz v0, :cond_2

    .line 540146
    const-string v1, "graph_api_write_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540147
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 540148
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 540149
    if-eqz v0, :cond_3

    .line 540150
    const-string v1, "hugePictureUrl"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540151
    invoke-static {p0, v0, p2}, LX/3h5;->a(LX/15i;ILX/0nX;)V

    .line 540152
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 540153
    if-eqz v0, :cond_4

    .line 540154
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540155
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 540156
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 540157
    if-eqz v0, :cond_8

    .line 540158
    const-string v1, "imported_phone_entries"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540159
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 540160
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_7

    .line 540161
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 540162
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 540163
    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4}, LX/15i;->b(II)Z

    move-result v4

    .line 540164
    if-eqz v4, :cond_5

    .line 540165
    const-string v5, "is_verified"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540166
    invoke-virtual {p2, v4}, LX/0nX;->a(Z)V

    .line 540167
    :cond_5
    const/4 v4, 0x1

    invoke-virtual {p0, v2, v4}, LX/15i;->g(II)I

    move-result v4

    .line 540168
    if-eqz v4, :cond_6

    .line 540169
    const-string v5, "primary_field"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540170
    invoke-static {p0, v4, p2, p3}, LX/6MZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 540171
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 540172
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 540173
    :cond_7
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 540174
    :cond_8
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 540175
    if-eqz v0, :cond_9

    .line 540176
    const-string v1, "is_on_viewer_contact_list"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540177
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 540178
    :cond_9
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 540179
    if-eqz v0, :cond_b

    .line 540180
    const-string v1, "name_entries"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540181
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 540182
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_a

    .line 540183
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/3h3;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 540184
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 540185
    :cond_a
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 540186
    :cond_b
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 540187
    if-eqz v0, :cond_c

    .line 540188
    const-string v1, "phonetic_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540189
    invoke-static {p0, v0, p2, p3}, LX/3h2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 540190
    :cond_c
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 540191
    if-eqz v0, :cond_d

    .line 540192
    const-string v1, "represented_profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540193
    invoke-static {p0, v0, p2, p3}, LX/3h0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 540194
    :cond_d
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 540195
    if-eqz v0, :cond_e

    .line 540196
    const-string v1, "smallPictureUrl"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540197
    invoke-static {p0, v0, p2}, LX/3h5;->a(LX/15i;ILX/0nX;)V

    .line 540198
    :cond_e
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 540199
    if-eqz v0, :cond_f

    .line 540200
    const-string v1, "structured_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540201
    invoke-static {p0, v0, p2, p3}, LX/3h2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 540202
    :cond_f
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 540203
    if-eqz v0, :cond_10

    .line 540204
    const-string v0, "viewer_connection_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540205
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 540206
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 540207
    return-void
.end method
