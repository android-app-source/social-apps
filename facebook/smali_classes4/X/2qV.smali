.class public final enum LX/2qV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2qV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2qV;

.field public static final enum ATTEMPT_TO_PAUSE:LX/2qV;

.field public static final enum ATTEMPT_TO_PLAY:LX/2qV;

.field public static final enum ERROR:LX/2qV;

.field public static final enum PAUSED:LX/2qV;

.field public static final enum PLAYBACK_COMPLETE:LX/2qV;

.field public static final enum PLAYING:LX/2qV;

.field public static final enum PREPARED:LX/2qV;

.field public static final enum SEEKING:LX/2qV;

.field public static final enum UNPREPARED:LX/2qV;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 471073
    new-instance v0, LX/2qV;

    const-string v1, "UNPREPARED"

    invoke-direct {v0, v1, v3}, LX/2qV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qV;->UNPREPARED:LX/2qV;

    .line 471074
    new-instance v0, LX/2qV;

    const-string v1, "PREPARED"

    invoke-direct {v0, v1, v4}, LX/2qV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qV;->PREPARED:LX/2qV;

    .line 471075
    new-instance v0, LX/2qV;

    const-string v1, "ATTEMPT_TO_PLAY"

    invoke-direct {v0, v1, v5}, LX/2qV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    .line 471076
    new-instance v0, LX/2qV;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v6}, LX/2qV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qV;->PLAYING:LX/2qV;

    .line 471077
    new-instance v0, LX/2qV;

    const-string v1, "SEEKING"

    invoke-direct {v0, v1, v7}, LX/2qV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qV;->SEEKING:LX/2qV;

    .line 471078
    new-instance v0, LX/2qV;

    const-string v1, "ATTEMPT_TO_PAUSE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/2qV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qV;->ATTEMPT_TO_PAUSE:LX/2qV;

    .line 471079
    new-instance v0, LX/2qV;

    const-string v1, "PAUSED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/2qV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qV;->PAUSED:LX/2qV;

    .line 471080
    new-instance v0, LX/2qV;

    const-string v1, "PLAYBACK_COMPLETE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/2qV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    .line 471081
    new-instance v0, LX/2qV;

    const-string v1, "ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/2qV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qV;->ERROR:LX/2qV;

    .line 471082
    const/16 v0, 0x9

    new-array v0, v0, [LX/2qV;

    sget-object v1, LX/2qV;->UNPREPARED:LX/2qV;

    aput-object v1, v0, v3

    sget-object v1, LX/2qV;->PREPARED:LX/2qV;

    aput-object v1, v0, v4

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    aput-object v1, v0, v5

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    aput-object v1, v0, v6

    sget-object v1, LX/2qV;->SEEKING:LX/2qV;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/2qV;->ATTEMPT_TO_PAUSE:LX/2qV;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2qV;->PAUSED:LX/2qV;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2qV;->ERROR:LX/2qV;

    aput-object v2, v0, v1

    sput-object v0, LX/2qV;->$VALUES:[LX/2qV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 471069
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2qV;
    .locals 1

    .prologue
    .line 471072
    const-class v0, LX/2qV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2qV;

    return-object v0
.end method

.method public static values()[LX/2qV;
    .locals 1

    .prologue
    .line 471071
    sget-object v0, LX/2qV;->$VALUES:[LX/2qV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2qV;

    return-object v0
.end method


# virtual methods
.method public final isPlayingState()Z
    .locals 1

    .prologue
    .line 471070
    sget-object v0, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/2qV;->PLAYING:LX/2qV;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
