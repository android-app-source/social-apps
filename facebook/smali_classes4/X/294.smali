.class public LX/294;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/294;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Zb;

.field private final c:LX/1rd;

.field private final d:LX/0W9;

.field private final e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Zb;LX/1rd;LX/0W9;Ljava/lang/Boolean;)V
    .locals 0
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 375435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 375436
    iput-object p1, p0, LX/294;->a:Landroid/content/Context;

    .line 375437
    iput-object p2, p0, LX/294;->b:LX/0Zb;

    .line 375438
    iput-object p3, p0, LX/294;->c:LX/1rd;

    .line 375439
    iput-object p4, p0, LX/294;->d:LX/0W9;

    .line 375440
    iput-object p5, p0, LX/294;->e:Ljava/lang/Boolean;

    .line 375441
    return-void
.end method

.method private a(ILjava/lang/String;)LX/0m9;
    .locals 10

    .prologue
    .line 375442
    iget-object v0, p0, LX/294;->c:LX/1rd;

    const/4 v2, 0x0

    .line 375443
    invoke-static {v0}, LX/1rd;->e(LX/1rd;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 375444
    :try_start_0
    new-instance v1, LX/1rg;

    invoke-direct {v1}, LX/1rg;-><init>()V

    invoke-static {}, LX/1rg;->e()I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessError; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    .line 375445
    :goto_0
    move v0, v1

    .line 375446
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 375447
    const/4 v0, 0x0

    .line 375448
    :goto_1
    return-object v0

    .line 375449
    :cond_0
    new-instance v2, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 375450
    const-string v1, "index"

    invoke-virtual {v2, v1, p1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 375451
    const-string v1, "state"

    invoke-static {v0}, LX/0km;->c(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 375452
    const-string v1, "carrier"

    iget-object v3, p0, LX/294;->c:LX/1rd;

    .line 375453
    invoke-static {v3}, LX/1rd;->e(LX/1rd;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 375454
    :try_start_1
    new-instance v4, LX/1rg;

    invoke-direct {v4}, LX/1rg;-><init>()V

    invoke-static {}, LX/1rg;->b()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessError; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v4

    .line 375455
    :cond_1
    :goto_2
    move-object v3, v4

    .line 375456
    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 375457
    const-string v1, "carrier_country_iso"

    iget-object v3, p0, LX/294;->c:LX/1rd;

    .line 375458
    invoke-static {v3}, LX/1rd;->e(LX/1rd;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 375459
    :try_start_2
    new-instance v4, LX/1rg;

    invoke-direct {v4}, LX/1rg;-><init>()V

    invoke-static {}, LX/1rg;->c()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/IllegalAccessError; {:try_start_2 .. :try_end_2} :catch_6

    move-result-object v4

    .line 375460
    :goto_3
    move-object v3, v4

    .line 375461
    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 375462
    iget-object v1, p0, LX/294;->c:LX/1rd;

    invoke-virtual {v1, p1}, LX/1rd;->c(I)I

    move-result v1

    .line 375463
    const-string v3, "network_type"

    invoke-static {v1}, LX/0km;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 375464
    iget-object v1, p0, LX/294;->c:LX/1rd;

    const/4 v4, -0x1

    .line 375465
    invoke-static {v1}, LX/1rd;->e(LX/1rd;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 375466
    :try_start_3
    new-instance v3, LX/1rg;

    invoke-direct {v3}, LX/1rg;-><init>()V

    invoke-static {}, LX/1rg;->a()I
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/lang/IllegalAccessError; {:try_start_3 .. :try_end_3} :catch_9

    move-result v3

    .line 375467
    :goto_4
    move v1, v3

    .line 375468
    const-string v3, "phone_type"

    invoke-static {v1}, LX/0km;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 375469
    const-string v1, "country_iso"

    iget-object v3, p0, LX/294;->c:LX/1rd;

    .line 375470
    invoke-static {v3}, LX/1rd;->e(LX/1rd;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 375471
    :try_start_4
    new-instance v4, LX/1rg;

    invoke-direct {v4}, LX/1rg;-><init>()V

    invoke-static {}, LX/1rg;->g()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljava/lang/IllegalAccessError; {:try_start_4 .. :try_end_4} :catch_b

    move-result-object v4

    .line 375472
    :cond_2
    :goto_5
    move-object v3, v4

    .line 375473
    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 375474
    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 375475
    const-string v0, "operator"

    iget-object v1, p0, LX/294;->c:LX/1rd;

    .line 375476
    invoke-static {v1}, LX/1rd;->e(LX/1rd;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 375477
    :try_start_5
    new-instance v3, LX/1rg;

    invoke-direct {v3}, LX/1rg;-><init>()V

    invoke-static {}, LX/1rg;->f()Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_c
    .catch Ljava/lang/IllegalAccessError; {:try_start_5 .. :try_end_5} :catch_d

    move-result-object v3

    .line 375478
    :cond_3
    :goto_6
    move-object v1, v3

    .line 375479
    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 375480
    :cond_4
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-static {v0}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 375481
    const-string v0, "phone_number"

    iget-object v1, p0, LX/294;->c:LX/1rd;

    invoke-virtual {v1, p1}, LX/1rd;->h(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 375482
    :try_start_6
    new-instance v3, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 375483
    iget-object v0, p0, LX/294;->c:LX/1rd;

    const/4 v4, 0x0

    .line 375484
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 375485
    invoke-static {v0}, LX/1rd;->d(LX/1rd;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-static {v0}, LX/1rd;->f(LX/1rd;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 375486
    :cond_5
    invoke-static {v0, p1}, LX/1rd;->q(LX/1rd;I)Ljava/lang/String;

    move-result-object v6

    .line 375487
    iget-object v1, v0, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_1b

    if-nez p1, :cond_1b

    .line 375488
    iget-object v1, v0, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    .line 375489
    :goto_7
    invoke-static {v0}, LX/1rd;->e(LX/1rd;)Z

    move-result v5

    if-eqz v5, :cond_6
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_0

    .line 375490
    :try_start_7
    new-instance v5, LX/1rg;

    invoke-direct {v5}, LX/1rg;-><init>()V

    invoke-static {}, LX/1rg;->j()Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_e
    .catch Ljava/lang/IllegalAccessError; {:try_start_7 .. :try_end_7} :catch_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_0

    :try_start_8
    move-result-object v4

    .line 375491
    :cond_6
    :goto_8
    iget-object v5, v0, LX/1rd;->e:LX/1re;

    invoke-virtual {v5, p1}, LX/1re;->a(I)Ljava/lang/String;

    move-result-object v5

    move-object p2, v5

    move-object v5, v1

    move-object v1, v4

    move-object v4, p2

    .line 375492
    :goto_9
    const-string v8, "android_subscription_manager"

    invoke-interface {v7, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375493
    const-string v6, "android_telephony_manager"

    invoke-interface {v7, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375494
    const-string v5, "mediatek"

    invoke-interface {v7, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375495
    const-string v1, "java_reflection"

    invoke-interface {v7, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375496
    move-object v0, v7

    .line 375497
    if-eqz v0, :cond_9

    .line 375498
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 375499
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_a

    .line 375500
    :catch_0
    move-exception v0

    .line 375501
    const-string v1, "DeviceInfoHelper"

    const-string v3, "Error attempting to convert phone number Map to ObjectNode."

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 375502
    :cond_7
    :goto_b
    const-string v0, "serial_number"

    iget-object v1, p0, LX/294;->c:LX/1rd;

    .line 375503
    invoke-static {v1}, LX/1rd;->d(LX/1rd;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 375504
    invoke-static {v1}, LX/1rd;->e(LX/1rd;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 375505
    :try_start_9
    new-instance v3, LX/1rg;

    invoke-direct {v3}, LX/1rg;-><init>()V

    invoke-static {}, LX/1rg;->h()Ljava/lang/String;
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_10
    .catch Ljava/lang/IllegalAccessError; {:try_start_9 .. :try_end_9} :catch_11

    move-result-object v3

    .line 375506
    :cond_8
    :goto_c
    move-object v1, v3

    .line 375507
    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 375508
    const-string v0, "subscriber_id"

    iget-object v1, p0, LX/294;->c:LX/1rd;

    .line 375509
    invoke-static {v1}, LX/1rd;->d(LX/1rd;)Z

    move-result v3

    if-eqz v3, :cond_23

    .line 375510
    invoke-static {v1}, LX/1rd;->e(LX/1rd;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 375511
    :try_start_a
    new-instance v3, LX/1rg;

    invoke-direct {v3}, LX/1rg;-><init>()V

    invoke-static {}, LX/1rg;->i()Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_12
    .catch Ljava/lang/IllegalAccessError; {:try_start_a .. :try_end_a} :catch_13

    move-result-object v3

    .line 375512
    :goto_d
    move-object v1, v3

    .line 375513
    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-object v0, v2

    .line 375514
    goto/16 :goto_1

    .line 375515
    :cond_9
    :try_start_b
    const-string v0, "phone_number_by_library"

    invoke-virtual {v2, v0, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;
    :try_end_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_0

    goto :goto_b

    .line 375516
    :catch_1
    move-exception v1

    .line 375517
    :goto_e
    const-string v3, "FbTelephonyManager"

    const-string v4, "Error attempting to get SIM card state from MediaTek API."

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 375518
    :cond_a
    invoke-static {v0, p1}, LX/1rd;->t(LX/1rd;I)Landroid/telephony/SubscriptionInfo;

    move-result-object v1

    if-eqz v1, :cond_b

    move v1, v2

    .line 375519
    goto/16 :goto_0

    .line 375520
    :cond_b
    iget-object v1, v0, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_c

    if-nez p1, :cond_c

    .line 375521
    iget-object v1, v0, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    goto/16 :goto_0

    .line 375522
    :cond_c
    iget-object v1, v0, LX/1rd;->e:LX/1re;

    if-eqz v1, :cond_d

    iget-object v1, v0, LX/1rd;->e:LX/1re;

    .line 375523
    iget-boolean v3, v1, LX/1re;->c:Z

    move v1, v3

    .line 375524
    if-eqz v1, :cond_d

    move v1, v2

    .line 375525
    goto/16 :goto_0

    .line 375526
    :cond_d
    const/4 v1, -0x1

    goto/16 :goto_0

    .line 375527
    :catch_2
    move-exception v1

    goto :goto_e

    .line 375528
    :catch_3
    move-exception v4

    .line 375529
    :goto_f
    const-string v5, "FbTelephonyManager"

    const-string v6, "Error attempting to get network operator name from MediaTek API."

    invoke-static {v5, v6, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 375530
    :cond_e
    invoke-static {v3, p1}, LX/1rd;->t(LX/1rd;I)Landroid/telephony/SubscriptionInfo;

    move-result-object v4

    .line 375531
    if-eqz v4, :cond_10

    invoke-virtual {v4}, Landroid/telephony/SubscriptionInfo;->getCarrierName()Ljava/lang/CharSequence;

    move-result-object v5

    if-eqz v5, :cond_10

    .line 375532
    invoke-virtual {v4}, Landroid/telephony/SubscriptionInfo;->getCarrierName()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 375533
    :goto_10
    move-object v4, v4

    .line 375534
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 375535
    iget-object v4, v3, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    if-eqz v4, :cond_f

    if-nez p1, :cond_f

    .line 375536
    iget-object v4, v3, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    .line 375537
    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 375538
    :catch_4
    move-exception v4

    goto :goto_f

    :cond_10
    const/4 v4, 0x0

    goto :goto_10

    .line 375539
    :catch_5
    move-exception v4

    .line 375540
    :goto_11
    const-string v5, "FbTelephonyManager"

    const-string v6, "Error attempting to get network country iso from MediaTek API."

    invoke-static {v5, v6, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 375541
    :cond_11
    iget-object v4, v3, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    if-eqz v4, :cond_12

    if-nez p1, :cond_12

    .line 375542
    iget-object v4, v3, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 375543
    :cond_12
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 375544
    :catch_6
    move-exception v4

    goto :goto_11

    .line 375545
    :catch_7
    move-exception v3

    .line 375546
    :goto_12
    const-string v5, "FbTelephonyManager"

    const-string v6, "Error attempting to get phone type from MediaTek API."

    invoke-static {v5, v6, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 375547
    :cond_13
    iget-object v3, v1, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    if-eqz v3, :cond_14

    if-nez p1, :cond_14

    .line 375548
    :try_start_c
    iget-object v3, v1, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I
    :try_end_c
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_c .. :try_end_c} :catch_8

    move-result v3

    goto/16 :goto_4

    .line 375549
    :catch_8
    move v3, v4

    goto/16 :goto_4

    :cond_14
    move v3, v4

    .line 375550
    goto/16 :goto_4

    .line 375551
    :catch_9
    move-exception v3

    goto :goto_12

    .line 375552
    :catch_a
    move-exception v4

    .line 375553
    :goto_13
    const-string v5, "FbTelephonyManager"

    const-string v6, "Error attempting to get sim country iso from MediaTek API."

    invoke-static {v5, v6, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 375554
    :cond_15
    invoke-static {v3, p1}, LX/1rd;->t(LX/1rd;I)Landroid/telephony/SubscriptionInfo;

    move-result-object v4

    .line 375555
    if-eqz v4, :cond_17

    invoke-virtual {v4}, Landroid/telephony/SubscriptionInfo;->getCountryIso()Ljava/lang/String;

    move-result-object v4

    :goto_14
    move-object v4, v4

    .line 375556
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 375557
    iget-object v4, v3, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    if-eqz v4, :cond_16

    if-nez p1, :cond_16

    .line 375558
    iget-object v4, v3, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_5

    .line 375559
    :cond_16
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 375560
    :catch_b
    move-exception v4

    goto :goto_13

    :cond_17
    const/4 v4, 0x0

    goto :goto_14

    .line 375561
    :catch_c
    move-exception v3

    .line 375562
    :goto_15
    const-string v4, "FbTelephonyManager"

    const-string v5, "Error attempting to get sim operator from MediaTek API."

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 375563
    :cond_18
    invoke-static {v1, p1}, LX/1rd;->t(LX/1rd;I)Landroid/telephony/SubscriptionInfo;

    move-result-object v3

    .line 375564
    if-eqz v3, :cond_1a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Landroid/telephony/SubscriptionInfo;->getMcc()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Landroid/telephony/SubscriptionInfo;->getMnc()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_16
    move-object v3, v3

    .line 375565
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 375566
    iget-object v3, v1, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    if-eqz v3, :cond_19

    if-nez p1, :cond_19

    .line 375567
    iget-object v3, v1, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    .line 375568
    :cond_19
    const/4 v3, 0x0

    goto/16 :goto_6

    .line 375569
    :catch_d
    move-exception v3

    goto :goto_15

    :cond_1a
    const/4 v3, 0x0

    goto :goto_16

    .line 375570
    :catch_e
    :try_start_d
    move-exception v5

    .line 375571
    :goto_17
    const-string v8, "FbTelephonyManager"

    const-string v9, "Error attempting to get phone number from MediaTek API."

    invoke-static {v8, v9, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    .line 375572
    :catch_f
    move-exception v5

    goto :goto_17

    :cond_1b
    move-object v1, v4

    goto/16 :goto_7

    :cond_1c
    move-object v1, v4

    move-object v5, v4

    move-object v6, v4

    goto/16 :goto_9
    :try_end_d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_d} :catch_0

    .line 375573
    :catch_10
    move-exception v3

    .line 375574
    :goto_18
    const-string v4, "FbTelephonyManager"

    const-string v5, "Error attempting to get SIM card serial number from MediaTek API."

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 375575
    :cond_1d
    invoke-static {v1, p1}, LX/1rd;->t(LX/1rd;I)Landroid/telephony/SubscriptionInfo;

    move-result-object v3

    .line 375576
    if-eqz v3, :cond_20

    invoke-virtual {v3}, Landroid/telephony/SubscriptionInfo;->getIccId()Ljava/lang/String;

    move-result-object v3

    :goto_19
    move-object v3, v3

    .line 375577
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 375578
    iget-object v3, v1, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    if-eqz v3, :cond_1e

    if-nez p1, :cond_1e

    .line 375579
    iget-object v3, v1, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_c

    .line 375580
    :cond_1e
    iget-object v3, v1, LX/1rd;->e:LX/1re;

    .line 375581
    const-string v4, "getIccSerialNumber"

    invoke-static {v3, v4, p1}, LX/1re;->a(LX/1re;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 375582
    goto/16 :goto_c

    .line 375583
    :cond_1f
    const/4 v3, 0x0

    goto/16 :goto_c

    .line 375584
    :catch_11
    move-exception v3

    goto :goto_18

    :cond_20
    const/4 v3, 0x0

    goto :goto_19

    .line 375585
    :catch_12
    move-exception v3

    .line 375586
    :goto_1a
    const-string v4, "FbTelephonyManager"

    const-string v5, "Error attempting to get subscriber ID from MediaTek API."

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 375587
    :cond_21
    iget-object v3, v1, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    if-eqz v3, :cond_22

    if-nez p1, :cond_22

    .line 375588
    iget-object v3, v1, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_d

    .line 375589
    :cond_22
    iget-object v3, v1, LX/1rd;->e:LX/1re;

    .line 375590
    const-string v4, "getSubscriberId"

    invoke-static {v3, v4, p1}, LX/1re;->a(LX/1re;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 375591
    goto/16 :goto_d

    .line 375592
    :cond_23
    const/4 v3, 0x0

    goto/16 :goto_d

    .line 375593
    :catch_13
    move-exception v3

    goto :goto_1a
.end method

.method public static a(LX/0QB;)LX/294;
    .locals 9

    .prologue
    .line 375594
    sget-object v0, LX/294;->f:LX/294;

    if-nez v0, :cond_1

    .line 375595
    const-class v1, LX/294;

    monitor-enter v1

    .line 375596
    :try_start_0
    sget-object v0, LX/294;->f:LX/294;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 375597
    if-eqz v2, :cond_0

    .line 375598
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 375599
    new-instance v3, LX/294;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {v0}, LX/1rd;->b(LX/0QB;)LX/1rd;

    move-result-object v6

    check-cast v6, LX/1rd;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v7

    check-cast v7, LX/0W9;

    invoke-static {v0}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-direct/range {v3 .. v8}, LX/294;-><init>(Landroid/content/Context;LX/0Zb;LX/1rd;LX/0W9;Ljava/lang/Boolean;)V

    .line 375600
    move-object v0, v3

    .line 375601
    sput-object v0, LX/294;->f:LX/294;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375602
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 375603
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 375604
    :cond_1
    sget-object v0, LX/294;->f:LX/294;

    return-object v0

    .line 375605
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 375606
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 6

    .prologue
    .line 375607
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 375608
    iput-wide p1, v1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 375609
    iput-object p3, v1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->f:Ljava/lang/String;

    .line 375610
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 375611
    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x2

    if-ge v0, v3, :cond_1

    .line 375612
    invoke-direct {p0, v0, p3}, LX/294;->a(ILjava/lang/String;)LX/0m9;

    move-result-object v3

    .line 375613
    if-eqz v3, :cond_0

    .line 375614
    invoke-virtual {v2, v3}, LX/162;->a(LX/0lF;)LX/162;

    .line 375615
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 375616
    :cond_1
    const-string v0, "sim_info"

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 375617
    iget-object v0, p0, LX/294;->d:LX/0W9;

    .line 375618
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 375619
    if-eqz v0, :cond_2

    .line 375620
    const-string v3, "device_locale"

    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375621
    const-string v3, "app_locale"

    invoke-virtual {v0}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375622
    :cond_2
    move-object v0, v2

    .line 375623
    invoke-virtual {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 375624
    iget-object v0, p0, LX/294;->a:Landroid/content/Context;

    invoke-static {v0}, LX/2FP;->a(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 375625
    const-string v0, "is_tablet"

    iget-object v2, p0, LX/294;->e:Ljava/lang/Boolean;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 375626
    const-string v0, "device"

    .line 375627
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 375628
    return-object v1
.end method

.method public final a(JLjava/lang/String;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 375629
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 375630
    const-string v0, "simplified_device_info"

    invoke-virtual {p0, p1, p2, p3, v0}, LX/294;->a(JLjava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 375631
    iget-object v1, p0, LX/294;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 375632
    :cond_0
    return-void
.end method
