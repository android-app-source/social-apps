.class public LX/2De;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/io/Writer;

.field public b:Z

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>(Ljava/io/Writer;)V
    .locals 0

    .prologue
    .line 384489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384490
    iput-object p1, p0, LX/2De;->a:Ljava/io/Writer;

    .line 384491
    return-void
.end method

.method public static a(LX/2De;)V
    .locals 2

    .prologue
    .line 384492
    invoke-static {p0}, LX/2De;->c(LX/2De;)V

    .line 384493
    iget-boolean v0, p0, LX/2De;->d:Z

    if-nez v0, :cond_0

    .line 384494
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2De;->d:Z

    .line 384495
    iget-object v0, p0, LX/2De;->a:Ljava/io/Writer;

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 384496
    :goto_0
    return-void

    .line 384497
    :cond_0
    iget-object v0, p0, LX/2De;->a:Ljava/io/Writer;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    goto :goto_0
.end method

.method public static c(LX/2De;)V
    .locals 2

    .prologue
    .line 384498
    iget-boolean v0, p0, LX/2De;->c:Z

    if-eqz v0, :cond_0

    .line 384499
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot perform action because we have ended the batch"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384500
    :cond_0
    return-void
.end method
