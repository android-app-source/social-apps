.class public LX/33M;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 492525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 492526
    iput v0, p0, LX/33M;->d:I

    .line 492527
    iput v0, p0, LX/33M;->e:I

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 492528
    sget-object v0, LX/03r;->FriendingButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 492529
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 492530
    const/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 492531
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 492532
    if-eq v1, v3, :cond_0

    .line 492533
    iput v1, p0, LX/33M;->d:I

    .line 492534
    :cond_0
    if-eq v2, v3, :cond_1

    .line 492535
    iput v2, p0, LX/33M;->e:I

    .line 492536
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 492509
    iget v0, p0, LX/33M;->d:I

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/33M;->e:I

    if-ne v0, v1, :cond_1

    .line 492510
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Resource for active and inactive button is needed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 492511
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p1, v0, :cond_2

    .line 492512
    iget v0, p0, LX/33M;->e:I

    iput v0, p0, LX/33M;->a:I

    .line 492513
    const v0, 0x7f080f9c

    iput v0, p0, LX/33M;->b:I

    .line 492514
    :goto_0
    return-void

    .line 492515
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p1, v0, :cond_3

    .line 492516
    iget v0, p0, LX/33M;->d:I

    iput v0, p0, LX/33M;->a:I

    .line 492517
    const v0, 0x7f080f9e

    iput v0, p0, LX/33M;->b:I

    goto :goto_0

    .line 492518
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p1, v0, :cond_4

    .line 492519
    iget v0, p0, LX/33M;->e:I

    iput v0, p0, LX/33M;->a:I

    .line 492520
    const v0, 0x7f080f9f

    iput v0, p0, LX/33M;->b:I

    goto :goto_0

    .line 492521
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p1, v0, :cond_5

    .line 492522
    iget v0, p0, LX/33M;->d:I

    iput v0, p0, LX/33M;->a:I

    .line 492523
    const v0, 0x7f080f9d

    iput v0, p0, LX/33M;->b:I

    goto :goto_0

    .line 492524
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t use Friending Button with this Friendship Status: %s"

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
