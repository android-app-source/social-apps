.class public final enum LX/2MB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2MB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2MB;

.field public static final enum CONNECTED:LX/2MB;

.field public static final enum CONNECTED_CAPTIVE_PORTAL:LX/2MB;

.field public static final enum CONNECTING:LX/2MB;

.field public static final enum NO_INTERNET:LX/2MB;

.field public static final enum WAITING_TO_CONNECT:LX/2MB;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 396329
    new-instance v0, LX/2MB;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v2}, LX/2MB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2MB;->CONNECTED:LX/2MB;

    .line 396330
    new-instance v0, LX/2MB;

    const-string v1, "CONNECTED_CAPTIVE_PORTAL"

    invoke-direct {v0, v1, v3}, LX/2MB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2MB;->CONNECTED_CAPTIVE_PORTAL:LX/2MB;

    .line 396331
    new-instance v0, LX/2MB;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v4}, LX/2MB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2MB;->CONNECTING:LX/2MB;

    .line 396332
    new-instance v0, LX/2MB;

    const-string v1, "WAITING_TO_CONNECT"

    invoke-direct {v0, v1, v5}, LX/2MB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2MB;->WAITING_TO_CONNECT:LX/2MB;

    .line 396333
    new-instance v0, LX/2MB;

    const-string v1, "NO_INTERNET"

    invoke-direct {v0, v1, v6}, LX/2MB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2MB;->NO_INTERNET:LX/2MB;

    .line 396334
    const/4 v0, 0x5

    new-array v0, v0, [LX/2MB;

    sget-object v1, LX/2MB;->CONNECTED:LX/2MB;

    aput-object v1, v0, v2

    sget-object v1, LX/2MB;->CONNECTED_CAPTIVE_PORTAL:LX/2MB;

    aput-object v1, v0, v3

    sget-object v1, LX/2MB;->CONNECTING:LX/2MB;

    aput-object v1, v0, v4

    sget-object v1, LX/2MB;->WAITING_TO_CONNECT:LX/2MB;

    aput-object v1, v0, v5

    sget-object v1, LX/2MB;->NO_INTERNET:LX/2MB;

    aput-object v1, v0, v6

    sput-object v0, LX/2MB;->$VALUES:[LX/2MB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 396335
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2MB;
    .locals 1

    .prologue
    .line 396336
    const-class v0, LX/2MB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2MB;

    return-object v0
.end method

.method public static values()[LX/2MB;
    .locals 1

    .prologue
    .line 396337
    sget-object v0, LX/2MB;->$VALUES:[LX/2MB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2MB;

    return-object v0
.end method
