.class public LX/2SL;
.super Landroid/database/ContentObserver;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;

.field private static final b:Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private final d:LX/0SG;

.field private final e:Landroid/content/Context;

.field public f:LX/2SI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 412110
    const-class v0, LX/2SL;

    sput-object v0, LX/2SL;->a:Ljava/lang/Class;

    .line 412111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[0-9]+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2SL;->b:Ljava/lang/String;

    .line 412112
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    sput-object v0, LX/2SL;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0SG;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0
    .param p2    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 412113
    invoke-direct {p0, p3}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 412114
    iput-object p1, p0, LX/2SL;->d:LX/0SG;

    .line 412115
    iput-object p2, p0, LX/2SL;->e:Landroid/content/Context;

    .line 412116
    return-void
.end method


# virtual methods
.method public final deliverSelfNotifications()Z
    .locals 1

    .prologue
    .line 412117
    const/4 v0, 0x0

    return v0
.end method

.method public final onChange(Z)V
    .locals 1

    .prologue
    .line 412118
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 412119
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/2SL;->onChange(ZLandroid/net/Uri;)V

    .line 412120
    return-void
.end method

.method public final onChange(ZLandroid/net/Uri;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0xa

    const/4 v6, 0x0

    .line 412121
    if-eqz p2, :cond_0

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    if-ne p2, v0, :cond_1

    .line 412122
    :cond_0
    :goto_0
    return-void

    .line 412123
    :cond_1
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/2SL;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412124
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, LX/2SL;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    .line 412125
    :try_start_0
    iget-object v0, p0, LX/2SL;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, LX/2SL;->c:[Ljava/lang/String;

    const-string v1, "%s > %s AND %s < %s AND %s LIKE %s"

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "date_added"

    aput-object v8, v3, v7

    const/4 v7, 0x1

    sub-long v8, v4, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v3, v7

    const/4 v7, 0x2

    const-string v8, "date_added"

    aput-object v8, v3, v7

    const/4 v7, 0x3

    add-long/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v7

    const/4 v4, 0x4

    const-string v5, "_display_name"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "\'%screenshot%\'"

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "date_added DESC LIMIT 1"

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 412126
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_3

    .line 412127
    :cond_2
    if-eqz v1, :cond_0

    .line 412128
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 412129
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/2SL;->f:LX/2SI;

    if-eqz v0, :cond_7

    .line 412130
    iget-object v0, p0, LX/2SL;->f:LX/2SI;

    const/4 v2, 0x0

    .line 412131
    iget-object v3, v0, LX/2SI;->j:LX/23S;

    if-eqz v3, :cond_9

    .line 412132
    iget-object v2, v0, LX/2SI;->j:LX/23S;

    invoke-interface {v2}, LX/23S;->l()Ljava/lang/String;

    move-result-object v4

    .line 412133
    iget-object v2, v0, LX/2SI;->j:LX/23S;

    invoke-interface {v2}, LX/23S;->m()Ljava/lang/String;

    move-result-object v3

    .line 412134
    iget-object v2, v0, LX/2SI;->j:LX/23S;

    invoke-interface {v2}, LX/23S;->n()Ljava/lang/String;

    move-result-object v2

    .line 412135
    :goto_1
    iget-object v5, v0, LX/2SI;->e:LX/2SJ;

    .line 412136
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "screenshot_taken"

    invoke-direct {v6, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 412137
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 412138
    const-string v0, "owner_id"

    invoke-virtual {v6, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 412139
    :cond_4
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 412140
    const-string v0, "media_id"

    invoke-virtual {v6, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 412141
    :cond_5
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 412142
    const-string v0, "location"

    invoke-virtual {v6, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 412143
    :cond_6
    iget-object v0, v5, LX/2SJ;->a:LX/0Zb;

    invoke-interface {v0, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 412144
    :cond_7
    if-eqz v1, :cond_0

    .line 412145
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 412146
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_8

    .line 412147
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    .line 412148
    :catchall_1
    move-exception v0

    goto :goto_2

    :cond_9
    move-object v3, v2

    move-object v4, v2

    goto :goto_1
.end method
