.class public LX/367;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/369;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/26R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 498076
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/367;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/26R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498073
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 498074
    iput-object p1, p0, LX/367;->b:LX/0Ot;

    .line 498075
    return-void
.end method

.method public static a(LX/1De;LX/1De;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 498072
    const v0, -0x360bf612    # -1999165.8f

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/367;
    .locals 4

    .prologue
    .line 498061
    const-class v1, LX/367;

    monitor-enter v1

    .line 498062
    :try_start_0
    sget-object v0, LX/367;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 498063
    sput-object v2, LX/367;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498064
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498065
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 498066
    new-instance v3, LX/367;

    const/16 p0, 0x5e5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/367;-><init>(LX/0Ot;)V

    .line 498067
    move-object v0, v3

    .line 498068
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 498069
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/367;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498070
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 498071
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 1

    .prologue
    .line 498077
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 498078
    if-nez v0, :cond_0

    .line 498079
    const/4 v0, 0x0

    .line 498080
    :goto_0
    return-object v0

    .line 498081
    :cond_0
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 498082
    check-cast v0, LX/368;

    iget-object v0, v0, LX/368;->h:LX/1dQ;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 498044
    invoke-static {}, LX/1dS;->b()V

    .line 498045
    iget v0, p1, LX/1dQ;->b:I

    .line 498046
    packed-switch v0, :pswitch_data_0

    .line 498047
    :goto_0
    return-object v3

    .line 498048
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 498049
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, LX/1De;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 498050
    check-cast v2, LX/368;

    .line 498051
    iget-object p1, p0, LX/367;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v2, LX/368;->a:LX/26N;

    iget p2, v2, LX/368;->b:I

    .line 498052
    invoke-static {v0}, LX/367;->d(LX/1De;)LX/1dQ;

    move-result-object p0

    .line 498053
    new-instance v2, LX/Aj8;

    invoke-direct {v2}, LX/Aj8;-><init>()V

    .line 498054
    iput-object v1, v2, LX/Aj8;->a:Landroid/view/View;

    .line 498055
    iput-object p1, v2, LX/Aj8;->b:LX/26N;

    .line 498056
    iput p2, v2, LX/Aj8;->c:I

    .line 498057
    iget-object v0, p0, LX/1dQ;->a:LX/1X1;

    .line 498058
    iget-object v1, v0, LX/1X1;->e:LX/1S3;

    move-object v0, v1

    .line 498059
    invoke-virtual {v0, p0, v2}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    .line 498060
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x360bf612
        :pswitch_0
    .end packed-switch
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 498041
    check-cast p4, LX/368;

    .line 498042
    iget-object v0, p0, LX/367;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26R;

    iget-object v4, p4, LX/368;->a:LX/26N;

    iget v5, p4, LX/368;->b:I

    iget-object v6, p4, LX/368;->c:LX/1aZ;

    iget-object v7, p4, LX/368;->d:Landroid/graphics/PointF;

    iget v8, p4, LX/368;->e:I

    iget v9, p4, LX/368;->f:I

    iget-object v10, p4, LX/368;->g:Ljava/lang/String;

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v10}, LX/26R;->a(LX/1De;IILX/26N;ILX/1aZ;Landroid/graphics/PointF;IILjava/lang/String;)LX/1Dg;

    move-result-object v0

    .line 498043
    return-object v0
.end method

.method public final c(LX/1De;)LX/369;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 498033
    new-instance v1, LX/368;

    invoke-direct {v1, p0}, LX/368;-><init>(LX/367;)V

    .line 498034
    sget-object v2, LX/367;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/369;

    .line 498035
    if-nez v2, :cond_0

    .line 498036
    new-instance v2, LX/369;

    invoke-direct {v2}, LX/369;-><init>()V

    .line 498037
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/369;->a$redex0(LX/369;LX/1De;IILX/368;)V

    .line 498038
    move-object v1, v2

    .line 498039
    move-object v0, v1

    .line 498040
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 498032
    const/4 v0, 0x1

    return v0
.end method
