.class public final LX/3DZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Da;


# instance fields
.field public final synthetic a:LX/1Lm;

.field private final b:Ljava/lang/String;

.field public final c:Ljava/io/File;

.field public final d:Ljava/io/File;

.field public e:LX/3Dd;

.field private final f:Ljava/io/FilenameFilter;

.field private final g:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Lm;Ljava/lang/String;Ljava/io/File;)V
    .locals 9

    .prologue
    .line 535233
    iput-object p1, p0, LX/3DZ;->a:LX/1Lm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 535234
    new-instance v0, LX/3Db;

    invoke-direct {v0, p0}, LX/3Db;-><init>(LX/3DZ;)V

    iput-object v0, p0, LX/3DZ;->f:Ljava/io/FilenameFilter;

    .line 535235
    new-instance v0, LX/3Dc;

    invoke-direct {v0, p0}, LX/3Dc;-><init>(LX/3DZ;)V

    iput-object v0, p0, LX/3DZ;->g:Ljava/util/Comparator;

    .line 535236
    iput-object p2, p0, LX/3DZ;->b:Ljava/lang/String;

    .line 535237
    iput-object p3, p0, LX/3DZ;->c:Ljava/io/File;

    .line 535238
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/3DZ;->c:Ljava/io/File;

    const-string v2, "metadata.json"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LX/3DZ;->d:Ljava/io/File;

    .line 535239
    const/4 v8, 0x2

    const/4 v3, 0x0

    .line 535240
    iget-object v4, p0, LX/3DZ;->d:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 535241
    :goto_0
    move-object v0, v3

    .line 535242
    iput-object v0, p0, LX/3DZ;->e:LX/3Dd;

    .line 535243
    const/4 v3, 0x0

    .line 535244
    iget-object v4, p0, LX/3DZ;->e:LX/3Dd;

    if-nez v4, :cond_6

    .line 535245
    :cond_0
    :goto_1
    move v0, v3

    .line 535246
    if-nez v0, :cond_1

    .line 535247
    invoke-virtual {p0}, LX/3DZ;->f()V

    .line 535248
    :cond_1
    return-void

    .line 535249
    :cond_2
    :try_start_0
    iget-object v4, p0, LX/3DZ;->a:LX/1Lm;

    iget-object v4, v4, LX/1Lm;->c:LX/0lC;

    iget-object v5, p0, LX/3DZ;->d:Ljava/io/File;

    invoke-virtual {v4, v5}, LX/0lC;->a(Ljava/io/File;)LX/0lF;

    move-result-object v4

    .line 535250
    const-string v5, "version"

    invoke-virtual {v4, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 535251
    const-string v6, "length"

    invoke-virtual {v4, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 535252
    const-string v7, "mimeType"

    invoke-virtual {v4, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 535253
    if-eqz v5, :cond_3

    if-eqz v6, :cond_3

    if-nez v7, :cond_4

    .line 535254
    :cond_3
    sget-object v5, LX/1Lm;->a:Ljava/lang/String;

    const-string v6, "Metadata is not complete. Ignoring. (%s)"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-static {v5, v6, v7}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 535255
    :catch_0
    move-exception v4

    .line 535256
    sget-object v5, LX/1Lm;->a:Ljava/lang/String;

    const-string v6, "Error reading partial file metadata"

    invoke-static {v5, v6, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 535257
    :cond_4
    const/4 v4, -0x1

    :try_start_1
    invoke-virtual {v5, v4}, LX/0lF;->b(I)I

    move-result v4

    .line 535258
    if-eq v4, v8, :cond_5

    .line 535259
    sget-object v5, LX/1Lm;->a:Ljava/lang/String;

    const-string v6, "Retrieved metadata version %d, ignoring (%d required)."

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v8

    const/4 v4, 0x1

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v5, v6, v7}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 535260
    :cond_5
    invoke-virtual {v6}, LX/0lF;->D()J

    move-result-wide v5

    .line 535261
    invoke-virtual {v7}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v7

    .line 535262
    new-instance v4, LX/3Dd;

    invoke-direct {v4, v5, v6, v7}, LX/3Dd;-><init>(JLjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object v3, v4

    goto :goto_0

    .line 535263
    :cond_6
    iget-object v4, p0, LX/3DZ;->e:LX/3Dd;

    iget-wide v5, v4, LX/3Dd;->a:J

    const-wide/16 v7, 0x0

    cmp-long v4, v5, v7

    if-lez v4, :cond_0

    .line 535264
    iget-object v4, p0, LX/3DZ;->e:LX/3Dd;

    iget-object v4, v4, LX/3Dd;->b:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 535265
    const/4 v3, 0x1

    goto/16 :goto_1
.end method

.method public static synthetic a(LX/3DZ;Ljava/io/File;)J
    .locals 2

    .prologue
    .line 535232
    invoke-static {p1}, LX/3DZ;->a(Ljava/io/File;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Ljava/io/File;)J
    .locals 4

    .prologue
    .line 535229
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 535230
    const/16 v2, 0x10

    invoke-static {v0, v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v2

    move-wide v0, v2

    .line 535231
    return-wide v0
.end method

.method public static n(LX/3DZ;)V
    .locals 3

    .prologue
    .line 535191
    iget-object v0, p0, LX/3DZ;->a:LX/1Lm;

    iget-object v0, v0, LX/1Lm;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 535192
    iget-object v2, p0, LX/3DZ;->d:Ljava/io/File;

    invoke-virtual {v2, v0, v1}, Ljava/io/File;->setLastModified(J)Z

    .line 535193
    return-void
.end method

.method public static o(LX/3DZ;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/2WF;",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 535215
    iget-object v0, p0, LX/3DZ;->c:Ljava/io/File;

    iget-object v1, p0, LX/3DZ;->f:Ljava/io/FilenameFilter;

    invoke-virtual {v0, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    .line 535216
    if-nez v2, :cond_1

    .line 535217
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    .line 535218
    :cond_0
    return-object v0

    .line 535219
    :cond_1
    iget-object v0, p0, LX/3DZ;->g:Ljava/util/Comparator;

    invoke-static {v2, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 535220
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 535221
    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 535222
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 535223
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_2

    .line 535224
    invoke-static {v4}, LX/3DZ;->a(Ljava/io/File;)J

    move-result-wide v8

    .line 535225
    add-long/2addr v6, v8

    .line 535226
    new-instance v5, LX/2WF;

    invoke-direct {v5, v8, v9, v6, v7}, LX/2WF;-><init>(JJ)V

    .line 535227
    invoke-virtual {v0, v5, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535228
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(J)Ljava/io/OutputStream;
    .locals 4

    .prologue
    .line 535208
    invoke-static {p0}, LX/3DZ;->n(LX/3DZ;)V

    .line 535209
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/3DZ;->c:Ljava/io/File;

    .line 535210
    const-string v2, "%1$016x"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 535211
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 535212
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 535213
    new-instance v2, LX/4nH;

    invoke-direct {v2, p0, v1, v0}, LX/4nH;-><init>(LX/3DZ;Ljava/io/OutputStream;Ljava/io/File;)V

    .line 535214
    return-object v2
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 535207
    iget-object v0, p0, LX/3DZ;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()LX/3Dd;
    .locals 1

    .prologue
    .line 535206
    iget-object v0, p0, LX/3DZ;->e:LX/3Dd;

    return-object v0
.end method

.method public final b(J)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 535204
    invoke-static {p0}, LX/3DZ;->n(LX/3DZ;)V

    .line 535205
    new-instance v0, LX/4nI;

    invoke-direct {v0, p0, p1, p2}, LX/4nI;-><init>(LX/3DZ;J)V

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 535203
    iget-object v0, p0, LX/3DZ;->a:LX/1Lm;

    iget-object v0, v0, LX/1Lm;->d:LX/0en;

    iget-object v1, p0, LX/3DZ;->c:Ljava/io/File;

    invoke-virtual {v0, v1}, LX/0en;->d(Ljava/io/File;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 535202
    iget-object v0, p0, LX/3DZ;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 535199
    iget-object v0, p0, LX/3DZ;->c:Ljava/io/File;

    invoke-static {v0}, LX/2W9;->b(Ljava/io/File;)Z

    .line 535200
    const/4 v0, 0x0

    iput-object v0, p0, LX/3DZ;->e:LX/3Dd;

    .line 535201
    return-void
.end method

.method public final g()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2WF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 535195
    invoke-static {p0}, LX/3DZ;->o(LX/3DZ;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 535196
    new-instance v1, LX/2WF;

    const-wide/16 v2, 0x0

    .line 535197
    iget-object v6, p0, LX/3DZ;->e:LX/3Dd;

    iget-wide v6, v6, LX/3Dd;->a:J

    move-wide v4, v6

    .line 535198
    invoke-direct {v1, v2, v3, v4, v5}, LX/2WF;-><init>(JJ)V

    invoke-virtual {v1, v0}, LX/2WF;->b(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 535194
    iget-object v0, p0, LX/3DZ;->b:Ljava/lang/String;

    return-object v0
.end method
