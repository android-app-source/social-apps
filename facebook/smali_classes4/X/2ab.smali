.class public LX/2ab;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 424673
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "qe/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 424674
    sput-object v0, LX/2ab;->a:LX/0Tn;

    const-string v1, "latest_users"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2ab;->b:LX/0Tn;

    .line 424675
    sget-object v0, LX/2ab;->a:LX/0Tn;

    const-string v1, "last_fetch_time_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2ab;->c:LX/0Tn;

    .line 424676
    sget-object v0, LX/2ab;->a:LX/0Tn;

    const-string v1, "last_fetch_locale"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2ab;->d:LX/0Tn;

    .line 424677
    sget-object v0, LX/2ab;->a:LX/0Tn;

    const-string v1, "expire_ttl_ms_override"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2ab;->e:LX/0Tn;

    .line 424678
    sget-object v0, LX/2ab;->a:LX/0Tn;

    const-string v1, "experiments_filter"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2ab;->f:LX/0Tn;

    .line 424679
    sget-object v0, LX/2ab;->a:LX/0Tn;

    const-string v1, "first_visible_item_index"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2ab;->g:LX/0Tn;

    .line 424680
    sget-object v0, LX/2ab;->a:LX/0Tn;

    const-string v1, "first_visible_item_offset"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2ab;->h:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 424671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424672
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 424681
    sget-object v0, LX/2ab;->c:LX/0Tn;

    sget-object v1, LX/2ab;->d:LX/0Tn;

    sget-object v2, LX/2ab;->e:LX/0Tn;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
