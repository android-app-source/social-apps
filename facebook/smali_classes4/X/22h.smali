.class public final LX/22h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/util/Printer;


# instance fields
.field public final synthetic a:Landroid/os/Looper;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/0Zr;

.field private d:LX/0cV;


# direct methods
.method public constructor <init>(LX/0Zr;Landroid/os/Looper;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 360256
    iput-object p1, p0, LX/22h;->c:LX/0Zr;

    iput-object p2, p0, LX/22h;->a:Landroid/os/Looper;

    iput-object p3, p0, LX/22h;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final println(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 360257
    iget-object v0, p0, LX/22h;->c:LX/0Zr;

    iget-object v0, v0, LX/0Zr;->c:LX/0Sj;

    invoke-interface {v0}, LX/0Sj;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 360258
    iget-object v1, p0, LX/22h;->a:Landroid/os/Looper;

    .line 360259
    invoke-static {v1}, LX/0Zr;->a(Landroid/os/Looper;)V

    .line 360260
    :cond_0
    :goto_0
    return-void

    .line 360261
    :cond_1
    sget-object v0, LX/0Zr;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 360262
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 360263
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 360264
    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 360265
    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 360266
    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 360267
    if-eqz v0, :cond_4

    .line 360268
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 360269
    :goto_2
    move-object v0, v0

    .line 360270
    if-eqz v0, :cond_2

    .line 360271
    iget-object v1, p0, LX/22h;->c:LX/0Zr;

    iget-object v1, v1, LX/0Zr;->c:LX/0Sj;

    const-string v2, "HandlerThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, LX/22h;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LX/0Sj;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0cV;

    move-result-object v0

    iput-object v0, p0, LX/22h;->d:LX/0cV;

    .line 360272
    iget-object v0, p0, LX/22h;->d:LX/0cV;

    invoke-interface {v0}, LX/0cV;->a()V

    goto :goto_0

    .line 360273
    :cond_2
    iget-object v0, p0, LX/22h;->d:LX/0cV;

    if-nez v0, :cond_3

    .line 360274
    const/4 v0, 0x6

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360275
    sget-object v0, LX/0Zr;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expecting start log. Str: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 360276
    :cond_3
    iget-object v0, p0, LX/22h;->d:LX/0cV;

    invoke-interface {v0}, LX/0cV;->b()V

    .line 360277
    const/4 v0, 0x0

    iput-object v0, p0, LX/22h;->d:LX/0cV;

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    .line 360278
    goto :goto_1

    .line 360279
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method
