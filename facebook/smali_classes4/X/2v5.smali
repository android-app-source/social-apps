.class public LX/2v5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 477269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 477270
    iput-object p1, p0, LX/2v5;->a:Landroid/content/Context;

    .line 477271
    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 477272
    iget-object v0, p0, LX/2v5;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2v5;
    .locals 2

    .prologue
    .line 477267
    new-instance v1, LX/2v5;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/2v5;-><init>(Landroid/content/Context;)V

    .line 477268
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 477261
    sget-object v0, LX/6RR;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 477262
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 477263
    :pswitch_0
    const v0, 0x7f081e83

    invoke-direct {p0, v0}, LX/2v5;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 477264
    :pswitch_1
    const v0, 0x7f081e84

    invoke-direct {p0, v0}, LX/2v5;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 477265
    :pswitch_2
    const v0, 0x7f081e86

    invoke-direct {p0, v0}, LX/2v5;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 477266
    :pswitch_3
    const v0, 0x7f081e89

    invoke-direct {p0, v0}, LX/2v5;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
