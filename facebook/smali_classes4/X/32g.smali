.class public LX/32g;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "LX/0Tn;",
            "LX/0Tn;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final c:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:LX/1E1;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 490811
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 490812
    sput-object v0, LX/32g;->d:Ljava/util/HashMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/util/Pair;

    sget-object v3, LX/1kp;->l:LX/0Tn;

    sget-object v4, LX/1kp;->h:LX/0Tn;

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490813
    sget-object v0, LX/32g;->d:Ljava/util/HashMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/util/Pair;

    sget-object v3, LX/1kp;->n:LX/0Tn;

    sget-object v4, LX/1kp;->k:LX/0Tn;

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490814
    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1E1;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 490815
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490816
    iput-object p1, p0, LX/32g;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 490817
    iput-object p2, p0, LX/32g;->f:LX/1E1;

    .line 490818
    iget-object v0, p2, LX/1E1;->a:LX/0ad;

    sget v1, LX/32h;->c:I

    const/4 p1, 0x0

    invoke-interface {v0, v1, p1}, LX/0ad;->a(II)I

    move-result v0

    move v0, v0

    .line 490819
    iput v0, p0, LX/32g;->a:I

    .line 490820
    iget-object v0, p2, LX/1E1;->a:LX/0ad;

    sget v1, LX/32h;->b:I

    const/4 p1, 0x0

    invoke-interface {v0, v1, p1}, LX/0ad;->a(II)I

    move-result v0

    move v0, v0

    .line 490821
    iput v0, p0, LX/32g;->b:I

    .line 490822
    iget-object v0, p2, LX/1E1;->a:LX/0ad;

    sget v1, LX/32h;->d:I

    const/4 p1, 0x0

    invoke-interface {v0, v1, p1}, LX/0ad;->a(II)I

    move-result v0

    move v0, v0

    .line 490823
    iput v0, p0, LX/32g;->c:I

    .line 490824
    return-void
.end method

.method public static e(Ljava/lang/String;)LX/0Tn;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490825
    sget-object v0, LX/32g;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 490826
    if-eqz p0, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/0Tn;

    goto :goto_0
.end method

.method public static f(Ljava/lang/String;)LX/0Tn;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490827
    sget-object v0, LX/32g;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 490828
    if-eqz p0, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, LX/0Tn;

    goto :goto_0
.end method

.method public static g(LX/32g;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 490829
    invoke-static {p1}, LX/32g;->e(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 490830
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/32g;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public static h(LX/32g;Ljava/lang/String;)Ljava/lang/Long;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 490831
    invoke-static {p1}, LX/32g;->f(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 490832
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/32g;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 490833
    sget-object v2, LX/0SF;->a:LX/0SF;

    move-object v2, v2

    .line 490834
    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public static l(LX/32g;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 490835
    if-nez p1, :cond_1

    .line 490836
    :cond_0
    :goto_0
    return v0

    .line 490837
    :cond_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 490838
    iget-object v0, p0, LX/32g;->f:LX/1E1;

    .line 490839
    iget-object v1, v0, LX/1E1;->a:LX/0ad;

    sget-short p0, LX/32h;->f:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 490840
    goto :goto_0

    .line 490841
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 490842
    const/4 v0, 0x1

    move v0, v0

    .line 490843
    goto :goto_0
.end method
