.class public LX/2Gc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile n:LX/2Gc;


# instance fields
.field private final b:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "LX/2Ge;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Go;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Go;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Uh;

.field public final f:Landroid/content/pm/PackageManager;

.field public final g:Landroid/content/Context;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/09k;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/01T;

.field public final j:Ljava/util/concurrent/ExecutorService;

.field public final k:LX/08X;

.field public final l:LX/0s6;

.field public final m:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 388995
    const-class v0, LX/2Gc;

    sput-object v0, LX/2Gc;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Uh;Landroid/content/pm/PackageManager;Landroid/content/Context;LX/0Ot;LX/01T;LX/08X;Ljava/util/concurrent/ExecutorService;LX/0s6;)V
    .locals 1
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/push/registration/FbnsService;
        .end annotation
    .end param
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/push/registration/FbnsLiteService;
        .end annotation
    .end param
    .param p9    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2Go;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Go;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Landroid/content/pm/PackageManager;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/09k;",
            ">;",
            "LX/01T;",
            "LX/08X;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0s6;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 388979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388980
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/2Gc;->b:LX/0UE;

    .line 388981
    new-instance v0, Lcom/facebook/push/externalcloud/PushServiceSelector$1;

    invoke-direct {v0, p0}, Lcom/facebook/push/externalcloud/PushServiceSelector$1;-><init>(LX/2Gc;)V

    iput-object v0, p0, LX/2Gc;->m:Ljava/lang/Runnable;

    .line 388982
    iput-object p3, p0, LX/2Gc;->e:LX/0Uh;

    .line 388983
    iput-object p1, p0, LX/2Gc;->c:LX/0Ot;

    .line 388984
    iput-object p2, p0, LX/2Gc;->d:LX/0Ot;

    .line 388985
    iput-object p4, p0, LX/2Gc;->f:Landroid/content/pm/PackageManager;

    .line 388986
    iput-object p5, p0, LX/2Gc;->g:Landroid/content/Context;

    .line 388987
    iput-object p6, p0, LX/2Gc;->h:LX/0Ot;

    .line 388988
    iput-object p7, p0, LX/2Gc;->i:LX/01T;

    .line 388989
    iput-object p9, p0, LX/2Gc;->j:Ljava/util/concurrent/ExecutorService;

    .line 388990
    iput-object p8, p0, LX/2Gc;->k:LX/08X;

    .line 388991
    iput-object p10, p0, LX/2Gc;->l:LX/0s6;

    .line 388992
    iget-object v0, p0, LX/2Gc;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/09k;

    invoke-virtual {v0}, LX/09k;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388993
    :goto_0
    return-void

    .line 388994
    :cond_0
    iget-object v0, p0, LX/2Gc;->j:Ljava/util/concurrent/ExecutorService;

    new-instance p1, Lcom/facebook/push/externalcloud/PushServiceSelector$2;

    invoke-direct {p1, p0}, Lcom/facebook/push/externalcloud/PushServiceSelector$2;-><init>(LX/2Gc;)V

    const p2, -0x292cf470

    invoke-static {v0, p1, p2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2Gc;
    .locals 14

    .prologue
    .line 388966
    sget-object v0, LX/2Gc;->n:LX/2Gc;

    if-nez v0, :cond_1

    .line 388967
    const-class v1, LX/2Gc;

    monitor-enter v1

    .line 388968
    :try_start_0
    sget-object v0, LX/2Gc;->n:LX/2Gc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 388969
    if-eqz v2, :cond_0

    .line 388970
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 388971
    new-instance v3, LX/2Gc;

    const/16 v4, 0xff9

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xffc

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v7

    check-cast v7, Landroid/content/pm/PackageManager;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    const/16 v9, 0x1125

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v10

    check-cast v10, LX/01T;

    invoke-static {v0}, LX/08X;->a(LX/0QB;)LX/08X;

    move-result-object v11

    check-cast v11, LX/08X;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v13

    check-cast v13, LX/0s6;

    invoke-direct/range {v3 .. v13}, LX/2Gc;-><init>(LX/0Ot;LX/0Ot;LX/0Uh;Landroid/content/pm/PackageManager;Landroid/content/Context;LX/0Ot;LX/01T;LX/08X;Ljava/util/concurrent/ExecutorService;LX/0s6;)V

    .line 388972
    move-object v0, v3

    .line 388973
    sput-object v0, LX/2Gc;->n:LX/2Gc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 388974
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 388975
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 388976
    :cond_1
    sget-object v0, LX/2Gc;->n:LX/2Gc;

    return-object v0

    .line 388977
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 388978
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 388965
    iget-object v0, p0, LX/2Gc;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/09k;

    invoke-virtual {v0}, LX/09k;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Gc;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/09k;

    invoke-virtual {v0}, LX/09k;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e()Z
    .locals 2

    .prologue
    .line 388996
    const-string v0, "Amazon"

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SD4930UR"

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()Z
    .locals 2

    .prologue
    .line 388964
    const-string v0, "Nokia"

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "N1"

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 3

    .prologue
    .line 388960
    const/4 v0, 0x0

    .line 388961
    sget-object v1, LX/01T;->FB4A:LX/01T;

    iget-object v2, p0, LX/2Gc;->i:LX/01T;

    if-eq v1, v2, :cond_0

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    iget-object v2, p0, LX/2Gc;->i:LX/01T;

    if-ne v1, v2, :cond_1

    .line 388962
    :cond_0
    const/4 v0, 0x1

    .line 388963
    :cond_1
    iget-object v1, p0, LX/2Gc;->e:LX/0Uh;

    const/16 v2, 0x7b

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2Ge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 388935
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Gc;->b:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 388936
    invoke-static {}, LX/2Gc;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388937
    iget-object v0, p0, LX/2Gc;->b:LX/0UE;

    sget-object v1, LX/2Ge;->ADM:LX/2Ge;

    invoke-virtual {v0, v1}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 388938
    :cond_0
    :goto_0
    new-instance v0, LX/0UE;

    iget-object v1, p0, LX/2Gc;->b:LX/0UE;

    invoke-direct {v0, v1}, LX/0UE;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 388939
    :cond_1
    :try_start_1
    invoke-static {}, LX/2Gc;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 388940
    iget-object v0, p0, LX/2Gc;->b:LX/0UE;

    sget-object v1, LX/2Ge;->NNA:LX/2Ge;

    invoke-virtual {v0, v1}, LX/0UE;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 388941
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 388942
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/2Gc;->b:LX/0UE;

    sget-object v1, LX/2Ge;->GCM:LX/2Ge;

    invoke-virtual {v0, v1}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 388943
    invoke-direct {p0}, LX/2Gc;->d()Z

    move-result v1

    .line 388944
    if-eqz v1, :cond_3

    .line 388945
    iget-object v0, p0, LX/2Gc;->b:LX/0UE;

    sget-object v2, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-virtual {v0, v2}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 388946
    :goto_1
    if-nez v1, :cond_4

    invoke-direct {p0}, LX/2Gc;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 388947
    iget-object v0, p0, LX/2Gc;->b:LX/0UE;

    sget-object v1, LX/2Ge;->FBNS:LX/2Ge;

    invoke-virtual {v0, v1}, LX/0UE;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 388948
    :cond_3
    iget-object v0, p0, LX/2Gc;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Go;

    invoke-interface {v0}, LX/2Go;->a()V

    goto :goto_1

    .line 388949
    :cond_4
    iget-object v0, p0, LX/2Gc;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Go;

    invoke-interface {v0}, LX/2Go;->a()V

    goto :goto_0

    .line 388950
    :cond_5
    invoke-static {}, LX/2Gc;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, LX/2Gc;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388951
    invoke-direct {p0}, LX/2Gc;->d()Z

    move-result v1

    .line 388952
    if-eqz v1, :cond_7

    .line 388953
    iget-object v0, p0, LX/2Gc;->b:LX/0UE;

    sget-object v2, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-virtual {v0, v2}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 388954
    :cond_6
    :goto_2
    if-nez v1, :cond_8

    invoke-direct {p0}, LX/2Gc;->g()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 388955
    iget-object v0, p0, LX/2Gc;->b:LX/0UE;

    sget-object v1, LX/2Ge;->FBNS:LX/2Ge;

    invoke-virtual {v0, v1}, LX/0UE;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 388956
    :cond_7
    iget-object v0, p0, LX/2Gc;->b:LX/0UE;

    sget-object v2, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-virtual {v0, v2}, LX/0UE;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 388957
    iget-object v0, p0, LX/2Gc;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Go;

    invoke-interface {v0}, LX/2Go;->a()V

    goto :goto_2

    .line 388958
    :cond_8
    iget-object v0, p0, LX/2Gc;->b:LX/0UE;

    sget-object v1, LX/2Ge;->FBNS:LX/2Ge;

    invoke-virtual {v0, v1}, LX/0UE;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388959
    iget-object v0, p0, LX/2Gc;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Go;

    invoke-interface {v0}, LX/2Go;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/Class;)V
    .locals 3

    .prologue
    .line 388933
    iget-object v0, p0, LX/2Gc;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/push/externalcloud/PushServiceSelector$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/facebook/push/externalcloud/PushServiceSelector$3;-><init>(LX/2Gc;[Ljava/lang/Class;Ljava/lang/String;)V

    const v2, 0x4dd624e0    # 4.49092608E8f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 388934
    return-void
.end method

.method public final a(LX/2Ge;)Z
    .locals 1

    .prologue
    .line 388932
    invoke-virtual {p0}, LX/2Gc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
