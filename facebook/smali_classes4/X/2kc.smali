.class public final LX/2kc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1vq",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
        "LX/3DK;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1rp;


# direct methods
.method public constructor <init>(LX/1rp;)V
    .locals 0

    .prologue
    .line 456408
    iput-object p1, p0, LX/2kc;->a:LX/1rp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;",
            "LX/2kM",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 456409
    sget-boolean v0, LX/3Cc;->a:Z

    move v0, v0

    .line 456410
    if-eqz v0, :cond_0

    .line 456411
    :goto_0
    return-void

    .line 456412
    :cond_0
    iget-object v0, p0, LX/2kc;->a:LX/1rp;

    .line 456413
    iget-object p0, v0, LX/1rp;->d:Ljava/util/concurrent/Executor;

    new-instance p1, Lcom/facebook/notifications/util/NotificationsConnectionControllerManager$2;

    invoke-direct {p1, v0}, Lcom/facebook/notifications/util/NotificationsConnectionControllerManager$2;-><init>(LX/1rp;)V

    const p2, -0x1fcc793

    invoke-static {p0, p1, p2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 456414
    goto :goto_0
.end method

.method public final a(LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 456415
    return-void
.end method

.method public final bridge synthetic a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 456416
    return-void
.end method

.method public final bridge synthetic a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 456417
    return-void
.end method

.method public final bridge synthetic b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 456418
    return-void
.end method
