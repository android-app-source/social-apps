.class public final LX/3A9;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1VJ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Ps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:LX/3AH;

.field public final synthetic d:LX/1VJ;


# direct methods
.method public constructor <init>(LX/1VJ;)V
    .locals 1

    .prologue
    .line 524803
    iput-object p1, p0, LX/3A9;->d:LX/1VJ;

    .line 524804
    move-object v0, p1

    .line 524805
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 524806
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 524807
    const-string v0, "SeeTranslationComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/1VJ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 524808
    check-cast p1, LX/3A9;

    .line 524809
    iget-object v0, p1, LX/3A9;->c:LX/3AH;

    iput-object v0, p0, LX/3A9;->c:LX/3AH;

    .line 524810
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 524811
    if-ne p0, p1, :cond_1

    .line 524812
    :cond_0
    :goto_0
    return v0

    .line 524813
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 524814
    goto :goto_0

    .line 524815
    :cond_3
    check-cast p1, LX/3A9;

    .line 524816
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 524817
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 524818
    if-eq v2, v3, :cond_0

    .line 524819
    iget-object v2, p0, LX/3A9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/3A9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/3A9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 524820
    goto :goto_0

    .line 524821
    :cond_5
    iget-object v2, p1, LX/3A9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 524822
    :cond_6
    iget-object v2, p0, LX/3A9;->b:LX/1Ps;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/3A9;->b:LX/1Ps;

    iget-object v3, p1, LX/3A9;->b:LX/1Ps;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 524823
    goto :goto_0

    .line 524824
    :cond_7
    iget-object v2, p1, LX/3A9;->b:LX/1Ps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 524825
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/3A9;

    .line 524826
    const/4 v1, 0x0

    iput-object v1, v0, LX/3A9;->c:LX/3AH;

    .line 524827
    return-object v0
.end method
