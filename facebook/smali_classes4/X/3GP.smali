.class public LX/3GP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile j:LX/3GP;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Landroid/media/AudioManager;

.field public d:LX/EDE;

.field public e:Landroid/bluetooth/BluetoothAdapter;

.field public f:Landroid/bluetooth/BluetoothHeadset;

.field public g:LX/EDp;

.field public h:Z

.field public final i:LX/0YZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 541048
    const-class v0, LX/3GP;

    sput-object v0, LX/3GP;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/media/AudioManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 541043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 541044
    new-instance v0, LX/3GH;

    invoke-direct {v0, p0}, LX/3GH;-><init>(LX/3GP;)V

    iput-object v0, p0, LX/3GP;->i:LX/0YZ;

    .line 541045
    iput-object p1, p0, LX/3GP;->b:Landroid/content/Context;

    .line 541046
    iput-object p2, p0, LX/3GP;->c:Landroid/media/AudioManager;

    .line 541047
    return-void
.end method

.method public static a(LX/0QB;)LX/3GP;
    .locals 5

    .prologue
    .line 541001
    sget-object v0, LX/3GP;->j:LX/3GP;

    if-nez v0, :cond_1

    .line 541002
    const-class v1, LX/3GP;

    monitor-enter v1

    .line 541003
    :try_start_0
    sget-object v0, LX/3GP;->j:LX/3GP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 541004
    if-eqz v2, :cond_0

    .line 541005
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 541006
    new-instance p0, LX/3GP;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/19T;->b(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    invoke-direct {p0, v3, v4}, LX/3GP;-><init>(Landroid/content/Context;Landroid/media/AudioManager;)V

    .line 541007
    move-object v0, p0

    .line 541008
    sput-object v0, LX/3GP;->j:LX/3GP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 541009
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 541010
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 541011
    :cond_1
    sget-object v0, LX/3GP;->j:LX/3GP;

    return-object v0

    .line 541012
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 541013
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 541028
    iput-object v3, p0, LX/3GP;->g:LX/EDp;

    .line 541029
    iget-boolean v0, p0, LX/3GP;->h:Z

    if-eqz v0, :cond_1

    .line 541030
    iget-object v0, p0, LX/3GP;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541031
    iget-object v0, p0, LX/3GP;->c:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    .line 541032
    :cond_0
    iget-object v0, p0, LX/3GP;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    .line 541033
    iput-boolean v1, p0, LX/3GP;->h:Z

    .line 541034
    :cond_1
    iget-object v0, p0, LX/3GP;->d:LX/EDE;

    if-eqz v0, :cond_2

    .line 541035
    iget-object v0, p0, LX/3GP;->b:Landroid/content/Context;

    iget-object v1, p0, LX/3GP;->d:LX/EDE;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 541036
    iput-object v3, p0, LX/3GP;->d:LX/EDE;

    .line 541037
    :cond_2
    iget-object v0, p0, LX/3GP;->e:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_3

    .line 541038
    iget-object v0, p0, LX/3GP;->f:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v0, :cond_3

    .line 541039
    iget-object v0, p0, LX/3GP;->e:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x1

    iget-object v2, p0, LX/3GP;->f:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 541040
    :cond_3
    iput-object v3, p0, LX/3GP;->f:Landroid/bluetooth/BluetoothHeadset;

    .line 541041
    iput-object v3, p0, LX/3GP;->e:Landroid/bluetooth/BluetoothAdapter;

    .line 541042
    return-void
.end method

.method public final a(Z)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 541015
    iget-object v0, p0, LX/3GP;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 541016
    if-eqz p1, :cond_2

    .line 541017
    invoke-virtual {p0}, LX/3GP;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541018
    iget-object v0, p0, LX/3GP;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->startBluetoothSco()V

    .line 541019
    iget-object v0, p0, LX/3GP;->c:Landroid/media/AudioManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 541020
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3GP;->h:Z

    .line 541021
    :cond_0
    :goto_0
    iget-boolean v0, p0, LX/3GP;->h:Z

    return v0

    .line 541022
    :cond_1
    iput-boolean v1, p0, LX/3GP;->h:Z

    goto :goto_0

    .line 541023
    :cond_2
    iget-boolean v0, p0, LX/3GP;->h:Z

    if-eqz v0, :cond_0

    .line 541024
    iget-object v0, p0, LX/3GP;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 541025
    iget-object v0, p0, LX/3GP;->c:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    .line 541026
    :cond_3
    iget-object v0, p0, LX/3GP;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    .line 541027
    iput-boolean v1, p0, LX/3GP;->h:Z

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 541014
    iget-object v0, p0, LX/3GP;->e:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3GP;->f:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3GP;->f:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
