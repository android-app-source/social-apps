.class public LX/3Bo;
.super LX/1jx;
.source ""


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field private final c:LX/18L;

.field private d:LX/2lj;

.field private e:LX/0t8;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 529107
    const-class v0, LX/3Bo;

    sput-object v0, LX/3Bo;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2lj;LX/0t8;)V
    .locals 1

    .prologue
    .line 529121
    invoke-direct {p0}, LX/1jx;-><init>()V

    .line 529122
    new-instance v0, LX/18L;

    invoke-direct {v0}, LX/18L;-><init>()V

    iput-object v0, p0, LX/3Bo;->c:LX/18L;

    .line 529123
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Bo;->a:Z

    .line 529124
    iput-object p1, p0, LX/3Bo;->d:LX/2lj;

    .line 529125
    iput-object p2, p0, LX/3Bo;->e:LX/0t8;

    .line 529126
    return-void
.end method

.method private a(Ljava/lang/String;LX/0jT;Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 529127
    instance-of v1, p2, LX/16f;

    if-nez v1, :cond_1

    .line 529128
    :cond_0
    :goto_0
    return v0

    .line 529129
    :cond_1
    check-cast p2, LX/16f;

    iget-object v1, p0, LX/3Bo;->c:LX/18L;

    invoke-interface {p2, p3, v1}, LX/16f;->a(Ljava/lang/String;LX/18L;)V

    .line 529130
    iget-object v1, p0, LX/3Bo;->c:LX/18L;

    iget-object v1, v1, LX/18L;->a:Ljava/lang/Object;

    .line 529131
    sget-object v2, LX/16f;->a:Ljava/lang/Object;

    if-eq v1, v2, :cond_0

    .line 529132
    iget-object v0, p0, LX/3Bo;->d:LX/2lj;

    invoke-virtual {v0, p1, p3, v1}, LX/2lj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0jT;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 529108
    instance-of v0, p1, LX/16i;

    if-nez v0, :cond_1

    .line 529109
    :cond_0
    :goto_0
    return v5

    :cond_1
    move-object v0, p1

    .line 529110
    check-cast v0, LX/16i;

    invoke-interface {v0}, LX/16i;->a()Ljava/lang/String;

    move-result-object v1

    .line 529111
    :try_start_0
    iget-object v0, p0, LX/3Bo;->e:LX/0t8;

    invoke-interface {p1}, LX/0jT;->f()I

    move-result v2

    invoke-virtual {v0, v2}, LX/0t8;->a(I)[Ljava/lang/String;

    move-result-object v2

    .line 529112
    if-eqz v2, :cond_0

    array-length v0, v2

    if-eqz v0, :cond_0

    .line 529113
    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 529114
    invoke-direct {p0, v1, p1, v4}, LX/3Bo;->a(Ljava/lang/String;LX/0jT;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 529115
    const/4 v4, 0x1

    iput-boolean v4, p0, LX/3Bo;->a:Z
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_1

    .line 529116
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 529117
    :catch_0
    move-exception v0

    .line 529118
    sget-object v1, LX/3Bo;->b:Ljava/lang/Class;

    const-string v2, "Failed to read field from model"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 529119
    :catch_1
    move-exception v0

    .line 529120
    sget-object v1, LX/3Bo;->b:Ljava/lang/Class;

    const-string v2, "Failed to serialize list field to json"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
