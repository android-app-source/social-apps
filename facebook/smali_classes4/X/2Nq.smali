.class public LX/2Nq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2N8;


# direct methods
.method public constructor <init>(LX/2N8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 400058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400059
    iput-object p1, p0, LX/2Nq;->a:LX/2N8;

    .line 400060
    return-void
.end method

.method public static a(LX/0QB;)LX/2Nq;
    .locals 2

    .prologue
    .line 400061
    new-instance v1, LX/2Nq;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v0

    check-cast v0, LX/2N8;

    invoke-direct {v1, v0}, LX/2Nq;-><init>(LX/2N8;)V

    .line 400062
    move-object v0, v1

    .line 400063
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/model/payment/PaymentTransactionData;
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 400064
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400065
    const/4 v0, 0x0

    .line 400066
    :goto_0
    return-object v0

    .line 400067
    :cond_0
    iget-object v0, p0, LX/2Nq;->a:LX/2N8;

    invoke-virtual {v0, p1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 400068
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    .line 400069
    const-string v2, "from"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->x()J

    move-result-wide v2

    .line 400070
    const-string v4, "to"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->x()J

    move-result-wide v4

    .line 400071
    const-string v6, "amount"

    invoke-virtual {v0, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-virtual {v6}, LX/0lF;->w()I

    move-result v6

    .line 400072
    const-string v7, "currency"

    invoke-virtual {v0, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v7

    .line 400073
    new-instance v0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/model/payment/PaymentTransactionData;-><init>(Ljava/lang/String;JJILjava/lang/String;)V

    goto :goto_0
.end method
