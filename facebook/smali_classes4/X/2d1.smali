.class public LX/2d1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2d1;


# instance fields
.field public final a:LX/0Uh;

.field private final b:LX/1SI;


# direct methods
.method public constructor <init>(LX/0Uh;LX/1SI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 442786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442787
    iput-object p1, p0, LX/2d1;->a:LX/0Uh;

    .line 442788
    iput-object p2, p0, LX/2d1;->b:LX/1SI;

    .line 442789
    return-void
.end method

.method public static a(LX/0QB;)LX/2d1;
    .locals 5

    .prologue
    .line 442773
    sget-object v0, LX/2d1;->c:LX/2d1;

    if-nez v0, :cond_1

    .line 442774
    const-class v1, LX/2d1;

    monitor-enter v1

    .line 442775
    :try_start_0
    sget-object v0, LX/2d1;->c:LX/2d1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 442776
    if-eqz v2, :cond_0

    .line 442777
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 442778
    new-instance p0, LX/2d1;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/1SI;->b(LX/0QB;)LX/1SI;

    move-result-object v4

    check-cast v4, LX/1SI;

    invoke-direct {p0, v3, v4}, LX/2d1;-><init>(LX/0Uh;LX/1SI;)V

    .line 442779
    move-object v0, p0

    .line 442780
    sput-object v0, LX/2d1;->c:LX/2d1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 442781
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 442782
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 442783
    :cond_1
    sget-object v0, LX/2d1;->c:LX/2d1;

    return-object v0

    .line 442784
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 442785
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 442772
    iget-object v0, p0, LX/2d1;->b:LX/1SI;

    invoke-virtual {v0}, LX/1SI;->a()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 442771
    iget-object v0, p0, LX/2d1;->a:LX/0Uh;

    const/16 v1, 0x5d8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 3

    .prologue
    .line 442768
    iget-object v0, p0, LX/2d1;->a:LX/0Uh;

    const/16 v1, 0x51e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 3

    .prologue
    .line 442770
    iget-object v0, p0, LX/2d1;->a:LX/0Uh;

    const/16 v1, 0x3f9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    .line 442769
    iget-object v0, p0, LX/2d1;->a:LX/0Uh;

    const/16 v1, 0x5dd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
