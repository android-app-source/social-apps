.class public LX/2V3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/Fk0;",
        "LX/Fk1;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0dC;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0dC;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 417054
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417055
    iput-object p1, p0, LX/2V3;->a:LX/0dC;

    .line 417056
    iput-object p2, p0, LX/2V3;->b:LX/0Or;

    .line 417057
    return-void
.end method

.method public static b(LX/0QB;)LX/2V3;
    .locals 3

    .prologue
    .line 417058
    new-instance v1, LX/2V3;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v0

    check-cast v0, LX/0dC;

    const/16 v2, 0x19e

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/2V3;-><init>(LX/0dC;LX/0Or;)V

    .line 417059
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 417060
    check-cast p1, LX/Fk0;

    .line 417061
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 417062
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417063
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "channel_app_id"

    const-string v3, "525967784198252"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417064
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_id"

    iget-object v3, p0, LX/2V3;->a:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417065
    iget-object v0, p0, LX/2V3;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 417066
    if-eqz v0, :cond_0

    .line 417067
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "access_token"

    .line 417068
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, p0

    .line 417069
    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417070
    :cond_0
    move-object v0, v1

    .line 417071
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "limit"

    const-string v3, "1"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417072
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 417073
    const-string v2, "{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417074
    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 417075
    iget-object v3, p1, LX/Fk0;->a:Ljava/lang/String;

    move-object v3, v3

    .line 417076
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\":{ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417077
    const-string v2, "\"version"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 417078
    iget v3, p1, LX/Fk0;->c:I

    move v3, v3

    .line 417079
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417080
    iget-object v2, p1, LX/Fk0;->b:Ljava/lang/String;

    move-object v2, v2

    .line 417081
    if-eqz v2, :cond_1

    .line 417082
    const-string v2, "\"signature"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 417083
    iget-object v3, p1, LX/Fk0;->b:Ljava/lang/String;

    move-object v3, v3

    .line 417084
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417085
    :cond_1
    const-string v2, "},}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417086
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "builds"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417087
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "noauth_get_release_info"

    .line 417088
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 417089
    move-object v1, v1

    .line 417090
    const-string v2, "GET"

    .line 417091
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 417092
    move-object v1, v1

    .line 417093
    const-string v2, "method/mobile.releaseUpdate"

    .line 417094
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 417095
    move-object v1, v1

    .line 417096
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 417097
    move-object v0, v1

    .line 417098
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 417099
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 417100
    move-object v0, v0

    .line 417101
    const/4 v1, 0x1

    .line 417102
    iput-boolean v1, v0, LX/14O;->t:Z

    .line 417103
    move-object v0, v0

    .line 417104
    const/4 v1, 0x0

    .line 417105
    iput-boolean v1, v0, LX/14O;->u:Z

    .line 417106
    move-object v0, v0

    .line 417107
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 417108
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 417109
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 417110
    const-string v1, "data"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 417111
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0lF;->e()I

    move-result v0

    if-nez v0, :cond_1

    .line 417112
    :cond_0
    const/4 v0, 0x0

    .line 417113
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/Fk1;

    invoke-direct {v0, v1}, LX/Fk1;-><init>(LX/0lF;)V

    goto :goto_0
.end method
