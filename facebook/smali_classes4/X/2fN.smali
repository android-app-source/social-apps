.class public LX/2fN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/io/Writer;

.field public b:I


# direct methods
.method public constructor <init>(Ljava/io/Writer;)V
    .locals 1

    .prologue
    .line 446389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 446390
    const/4 v0, 0x1

    iput v0, p0, LX/2fN;->b:I

    .line 446391
    iput-object p1, p0, LX/2fN;->a:Ljava/io/Writer;

    .line 446392
    return-void
.end method

.method public static a(LX/2fN;I)V
    .locals 3

    .prologue
    .line 446393
    iget v0, p0, LX/2fN;->b:I

    if-eq v0, p1, :cond_0

    .line 446394
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/2fN;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 446395
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2Xb;)V
    .locals 3

    .prologue
    .line 446396
    iget v0, p0, LX/2fN;->b:I

    packed-switch v0, :pswitch_data_0

    .line 446397
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "state="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/2fN;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 446398
    :pswitch_0
    iget-object v0, p0, LX/2fN;->a:Ljava/io/Writer;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 446399
    :goto_0
    iget-object v0, p0, LX/2fN;->a:Ljava/io/Writer;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/2Xb;->a(Ljava/io/Writer;Z)V

    .line 446400
    return-void

    .line 446401
    :pswitch_1
    const/4 v0, 0x3

    .line 446402
    iput v0, p0, LX/2fN;->b:I

    .line 446403
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/2Y2;)V
    .locals 3

    .prologue
    .line 446404
    iget v0, p0, LX/2fN;->b:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/2fN;->b:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 446405
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "state="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/2fN;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 446406
    :cond_0
    const/4 v0, 0x4

    .line 446407
    iput v0, p0, LX/2fN;->b:I

    .line 446408
    iget-object v0, p0, LX/2fN;->a:Ljava/io/Writer;

    const-string v1, "],"

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 446409
    iget-object v0, p0, LX/2fN;->a:Ljava/io/Writer;

    invoke-virtual {p1, v0}, LX/2Y2;->b(Ljava/io/Writer;)V

    .line 446410
    iget-object v0, p0, LX/2fN;->a:Ljava/io/Writer;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 446411
    return-void
.end method
