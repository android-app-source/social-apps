.class public abstract LX/2Wj;
.super Landroid/view/View;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/view/View;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/text/Layout;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public d:Landroid/content/res/ColorStateList;

.field public e:I

.field public f:I

.field public g:Landroid/graphics/Typeface;

.field public h:I

.field private i:Landroid/text/Layout$Alignment;

.field public j:LX/A8d;

.field private k:I

.field public l:F

.field public m:F

.field public n:F

.field public o:I

.field public p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/text/TextPaint;",
            ">;"
        }
    .end annotation
.end field

.field public q:Landroid/text/Layout;

.field public r:Landroid/graphics/drawable/Drawable;

.field public s:Landroid/graphics/drawable/Drawable;

.field private t:Landroid/graphics/Rect;

.field public u:I

.field public v:Landroid/view/ViewTreeObserver$OnPreDrawListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 419190
    const-class v0, LX/2Wj;

    sput-object v0, LX/2Wj;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 419191
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/2Wj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 419192
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 419185
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/2Wj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 419186
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 419193
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 419194
    new-instance v0, LX/0aq;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/2Wj;->b:LX/0aq;

    .line 419195
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    iput-object v0, p0, LX/2Wj;->g:Landroid/graphics/Typeface;

    .line 419196
    const/4 v0, 0x1

    iput v0, p0, LX/2Wj;->h:I

    .line 419197
    iput v4, p0, LX/2Wj;->u:I

    .line 419198
    new-instance v0, LX/A8c;

    invoke-direct {v0, p0}, LX/A8c;-><init>(LX/2Wj;)V

    iput-object v0, p0, LX/2Wj;->v:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 419199
    invoke-virtual {p0}, LX/2Wj;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->VariableTextLayoutView:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 419200
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, LX/2Wj;->d:Landroid/content/res/ColorStateList;

    .line 419201
    iget-object v1, p0, LX/2Wj;->d:Landroid/content/res/ColorStateList;

    if-nez v1, :cond_0

    .line 419202
    invoke-static {v4, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, LX/2Wj;->d:Landroid/content/res/ColorStateList;

    .line 419203
    :cond_0
    const/16 v1, 0x5

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, LX/2Wj;->e:I

    .line 419204
    const/16 v1, 0x6

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, LX/2Wj;->f:I

    .line 419205
    const/16 v1, 0x7

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    invoke-static {v1}, LX/2Wj;->a(I)Landroid/text/Layout$Alignment;

    move-result-object v1

    iput-object v1, p0, LX/2Wj;->i:Landroid/text/Layout$Alignment;

    .line 419206
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    invoke-static {v1}, LX/2Wj;->b(I)LX/A8d;

    move-result-object v1

    iput-object v1, p0, LX/2Wj;->j:LX/A8d;

    .line 419207
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, LX/2Wj;->k:I

    .line 419208
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 419209
    const/16 v2, 0x3

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 419210
    const/4 v3, 0x0

    .line 419211
    packed-switch v2, :pswitch_data_0

    .line 419212
    :goto_0
    invoke-static {p0, v3, v1}, LX/2Wj;->a(LX/2Wj;Landroid/graphics/Typeface;I)V

    .line 419213
    iget-object v1, p0, LX/2Wj;->g:Landroid/graphics/Typeface;

    .line 419214
    const/16 v2, 0x0

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 419215
    invoke-static {v2}, LX/0xq;->fromIndex(I)LX/0xq;

    move-result-object v2

    .line 419216
    sget-object v3, LX/0xr;->BUILTIN:LX/0xr;

    invoke-static {p1, v2, v3, v1}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v2

    .line 419217
    if-eq v1, v2, :cond_1

    .line 419218
    invoke-static {p0, v2, v4}, LX/2Wj;->a(LX/2Wj;Landroid/graphics/Typeface;I)V

    .line 419219
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 419220
    iget v0, p0, LX/2Wj;->f:I

    iget v1, p0, LX/2Wj;->e:I

    if-ge v0, v1, :cond_2

    .line 419221
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid text sizes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419222
    :cond_2
    return-void

    .line 419223
    :pswitch_0
    sget-object v3, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    goto :goto_0

    .line 419224
    :pswitch_1
    sget-object v3, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    goto :goto_0

    .line 419225
    :pswitch_2
    sget-object v3, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)I
    .locals 1
    .param p0    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 419226
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    goto :goto_0
.end method

.method private static a(I)Landroid/text/Layout$Alignment;
    .locals 3

    .prologue
    .line 419227
    packed-switch p0, :pswitch_data_0

    .line 419228
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid alignment: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419229
    :pswitch_0
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 419230
    :goto_0
    return-object v0

    .line 419231
    :pswitch_1
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_0

    .line 419232
    :pswitch_2
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/2Wj;Landroid/graphics/Typeface;I)V
    .locals 1
    .param p0    # LX/2Wj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 419233
    invoke-static {p1, p2}, LX/A7i;->a(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, LX/2Wj;->g:Landroid/graphics/Typeface;

    .line 419234
    iput p2, p0, LX/2Wj;->h:I

    .line 419235
    invoke-direct {p0}, LX/2Wj;->d()V

    .line 419236
    invoke-virtual {p0}, LX/2Wj;->invalidate()V

    .line 419237
    return-void
.end method

.method private static b(I)LX/A8d;
    .locals 3

    .prologue
    .line 419238
    packed-switch p0, :pswitch_data_0

    .line 419239
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid alignment: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419240
    :pswitch_0
    sget-object v0, LX/A8d;->LEADING:LX/A8d;

    .line 419241
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, LX/A8d;->TRAILING:LX/A8d;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static c(LX/2Wj;)V
    .locals 4

    .prologue
    .line 419242
    iget-object v0, p0, LX/2Wj;->p:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 419243
    invoke-virtual {p0}, LX/2Wj;->getTextColor()I

    move-result v1

    .line 419244
    iget-object v0, p0, LX/2Wj;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextPaint;

    .line 419245
    invoke-virtual {v0}, Landroid/text/TextPaint;->getColor()I

    move-result v3

    if-eq v1, v3, :cond_0

    .line 419246
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 419247
    invoke-virtual {p0}, LX/2Wj;->invalidate()V

    goto :goto_0

    .line 419248
    :cond_1
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 419259
    iput-object v0, p0, LX/2Wj;->p:Ljava/util/List;

    .line 419260
    iput-object v0, p0, LX/2Wj;->q:Landroid/text/Layout;

    .line 419261
    iget-object v0, p0, LX/2Wj;->b:LX/0aq;

    invoke-virtual {v0}, LX/0aq;->a()V

    .line 419262
    invoke-virtual {p0}, LX/2Wj;->requestLayout()V

    .line 419263
    invoke-virtual {p0}, LX/2Wj;->invalidate()V

    .line 419264
    const/4 v2, 0x1

    .line 419265
    invoke-virtual {p0}, LX/2Wj;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 419266
    if-nez v0, :cond_1

    .line 419267
    :cond_0
    :goto_0
    return-void

    .line 419268
    :cond_1
    iget v1, p0, LX/2Wj;->u:I

    if-nez v1, :cond_2

    .line 419269
    iget-object v1, p0, LX/2Wj;->v:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 419270
    iput v2, p0, LX/2Wj;->u:I

    goto :goto_0

    .line 419271
    :cond_2
    iget v0, p0, LX/2Wj;->u:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 419272
    iput v2, p0, LX/2Wj;->u:I

    goto :goto_0
.end method

.method private e(I)Landroid/text/Layout;
    .locals 7

    .prologue
    .line 419249
    invoke-virtual {p0}, LX/2Wj;->getVariableTextLayoutComputer()LX/3L3;

    move-result-object v0

    iget-object v1, p0, LX/2Wj;->c:Ljava/lang/Object;

    iget-object v2, p0, LX/2Wj;->p:Ljava/util/List;

    iget-object v4, p0, LX/2Wj;->i:Landroid/text/Layout$Alignment;

    iget v5, p0, LX/2Wj;->k:I

    const/4 v6, -0x1

    move v3, p1

    invoke-interface/range {v0 .. v6}, LX/3L3;->a(Ljava/lang/Object;Ljava/util/List;ILandroid/text/Layout$Alignment;II)Landroid/text/Layout;

    move-result-object v0

    return-object v0
.end method

.method public static h(LX/2Wj;)V
    .locals 7

    .prologue
    .line 419291
    iget-object v0, p0, LX/2Wj;->q:Landroid/text/Layout;

    if-nez v0, :cond_0

    .line 419292
    invoke-virtual {p0}, LX/2Wj;->getWidth()I

    move-result v0

    invoke-virtual {p0}, LX/2Wj;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, LX/2Wj;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, LX/2Wj;->r:Landroid/graphics/drawable/Drawable;

    invoke-static {v1}, LX/2Wj;->a(Landroid/graphics/drawable/Drawable;)I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, LX/2Wj;->s:Landroid/graphics/drawable/Drawable;

    invoke-static {v1}, LX/2Wj;->a(Landroid/graphics/drawable/Drawable;)I

    move-result v1

    sub-int v3, v0, v1

    .line 419293
    invoke-virtual {p0}, LX/2Wj;->getVariableTextLayoutComputer()LX/3L3;

    move-result-object v0

    iget-object v1, p0, LX/2Wj;->c:Ljava/lang/Object;

    iget-object v2, p0, LX/2Wj;->p:Ljava/util/List;

    iget-object v4, p0, LX/2Wj;->i:Landroid/text/Layout$Alignment;

    iget v5, p0, LX/2Wj;->k:I

    invoke-virtual {p0}, LX/2Wj;->getHeight()I

    move-result v6

    invoke-interface/range {v0 .. v6}, LX/3L3;->a(Ljava/lang/Object;Ljava/util/List;ILandroid/text/Layout$Alignment;II)Landroid/text/Layout;

    move-result-object v0

    iput-object v0, p0, LX/2Wj;->q:Landroid/text/Layout;

    .line 419294
    :cond_0
    return-void
.end method

.method public static i(LX/2Wj;)V
    .locals 7

    .prologue
    .line 419273
    iget-object v0, p0, LX/2Wj;->p:Ljava/util/List;

    if-nez v0, :cond_1

    .line 419274
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/2Wj;->p:Ljava/util/List;

    .line 419275
    iget v0, p0, LX/2Wj;->f:I

    :goto_0
    iget v1, p0, LX/2Wj;->e:I

    if-lt v0, v1, :cond_1

    .line 419276
    iget-object v1, p0, LX/2Wj;->p:Ljava/util/List;

    .line 419277
    new-instance v2, Landroid/text/TextPaint;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/TextPaint;-><init>(I)V

    .line 419278
    invoke-virtual {p0}, LX/2Wj;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    iput v3, v2, Landroid/text/TextPaint;->density:F

    .line 419279
    const/4 v3, -0x1

    if-ne v0, v3, :cond_2

    .line 419280
    const/high16 v3, -0x40800000    # -1.0f

    .line 419281
    :goto_1
    move v3, v3

    .line 419282
    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 419283
    invoke-virtual {p0}, LX/2Wj;->getTextColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 419284
    iget v3, p0, LX/2Wj;->l:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    .line 419285
    iget v3, p0, LX/2Wj;->l:F

    iget v4, p0, LX/2Wj;->m:F

    iget v5, p0, LX/2Wj;->n:F

    iget v6, p0, LX/2Wj;->o:I

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 419286
    :cond_0
    iget-object v3, p0, LX/2Wj;->g:Landroid/graphics/Typeface;

    iget v4, p0, LX/2Wj;->h:I

    invoke-static {v2, v3, v4}, LX/A7i;->a(Landroid/text/TextPaint;Landroid/graphics/Typeface;I)V

    .line 419287
    move-object v2, v2

    .line 419288
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 419289
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 419290
    :cond_1
    return-void

    :cond_2
    const/4 v3, 0x2

    int-to-float v4, v0

    invoke-virtual {p0}, LX/2Wj;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    goto :goto_1
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public final drawableStateChanged()V
    .locals 1

    .prologue
    .line 419255
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 419256
    iget-object v0, p0, LX/2Wj;->d:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    .line 419257
    invoke-static {p0}, LX/2Wj;->c(LX/2Wj;)V

    .line 419258
    :cond_0
    return-void
.end method

.method public getAlignment()Landroid/text/Layout$Alignment;
    .locals 1

    .prologue
    .line 419254
    iget-object v0, p0, LX/2Wj;->i:Landroid/text/Layout$Alignment;

    return-object v0
.end method

.method public getBaseline()I
    .locals 3

    .prologue
    .line 419251
    iget-object v0, p0, LX/2Wj;->q:Landroid/text/Layout;

    if-nez v0, :cond_0

    .line 419252
    invoke-super {p0}, Landroid/view/View;->getBaseline()I

    move-result v0

    .line 419253
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/2Wj;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, LX/2Wj;->q:Landroid/text/Layout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineBaseline(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getBottomPaddingOffset()I
    .locals 3

    .prologue
    .line 419250
    const/4 v0, 0x0

    iget v1, p0, LX/2Wj;->n:F

    iget v2, p0, LX/2Wj;->l:F

    add-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 419187
    iget-object v0, p0, LX/2Wj;->q:Landroid/text/Layout;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {v0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {v0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 419188
    iget-object v0, p0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {v0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 419189
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public getLeftPaddingOffset()I
    .locals 3

    .prologue
    .line 419061
    const/4 v0, 0x0

    iget v1, p0, LX/2Wj;->m:F

    iget v2, p0, LX/2Wj;->l:F

    sub-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getMaxLines()I
    .locals 1

    .prologue
    .line 419069
    iget v0, p0, LX/2Wj;->k:I

    return v0
.end method

.method public getRightPaddingOffset()I
    .locals 3

    .prologue
    .line 419070
    const/4 v0, 0x0

    iget v1, p0, LX/2Wj;->m:F

    iget v2, p0, LX/2Wj;->l:F

    add-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getTextColor()I
    .locals 3

    .prologue
    .line 419071
    iget-object v0, p0, LX/2Wj;->d:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, LX/2Wj;->getDrawableState()[I

    move-result-object v1

    iget-object v2, p0, LX/2Wj;->d:Landroid/content/res/ColorStateList;

    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    return v0
.end method

.method public getTopPaddingOffset()I
    .locals 3

    .prologue
    .line 419072
    const/4 v0, 0x0

    iget v1, p0, LX/2Wj;->n:F

    iget v2, p0, LX/2Wj;->l:F

    sub-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getTypeface()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 419073
    iget-object v0, p0, LX/2Wj;->g:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public abstract getVariableTextLayoutComputer()LX/3L3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/3L3",
            "<TT;>;"
        }
    .end annotation
.end method

.method public final isPaddingOffsetRequired()Z
    .locals 2

    .prologue
    .line 419074
    iget v0, p0, LX/2Wj;->l:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x10ff368

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 419075
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 419076
    invoke-virtual {p0}, LX/2Wj;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 419077
    if-eqz v1, :cond_0

    .line 419078
    iget v2, p0, LX/2Wj;->u:I

    if-eqz v2, :cond_0

    .line 419079
    iget-object v2, p0, LX/2Wj;->v:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 419080
    const/4 v1, 0x0

    iput v1, p0, LX/2Wj;->u:I

    .line 419081
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x3f3f893c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 419082
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 419083
    invoke-static {p0}, LX/2Wj;->i(LX/2Wj;)V

    .line 419084
    invoke-static {p0}, LX/2Wj;->h(LX/2Wj;)V

    .line 419085
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 419086
    invoke-virtual {p0}, LX/2Wj;->getPaddingTop()I

    move-result v7

    .line 419087
    invoke-virtual {p0}, LX/2Wj;->getPaddingRight()I

    move-result v4

    .line 419088
    invoke-virtual {p0}, LX/2Wj;->getPaddingBottom()I

    move-result v2

    .line 419089
    invoke-virtual {p0}, LX/2Wj;->getScrollX()I

    move-result v6

    .line 419090
    invoke-virtual {p0}, LX/2Wj;->getScrollY()I

    move-result v8

    .line 419091
    invoke-virtual {p0}, LX/2Wj;->getHeight()I

    move-result v0

    sub-int/2addr v0, v2

    sub-int v9, v0, v7

    .line 419092
    iget-object v0, p0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    move-result v0

    sub-int v10, v0, v9

    .line 419093
    invoke-virtual {p0}, LX/2Wj;->getPaddingLeft()I

    move-result v0

    add-int/2addr v0, v6

    int-to-float v5, v0

    .line 419094
    if-nez v8, :cond_3

    move v0, v1

    .line 419095
    :goto_0
    invoke-virtual {p0}, LX/2Wj;->getWidth()I

    move-result v11

    sub-int v4, v11, v4

    add-int/2addr v4, v6

    int-to-float v4, v4

    .line 419096
    invoke-virtual {p0}, LX/2Wj;->getHeight()I

    move-result v6

    add-int/2addr v6, v8

    if-ne v8, v10, :cond_0

    move v2, v3

    :cond_0
    sub-int v2, v6, v2

    int-to-float v2, v2

    .line 419097
    iget v6, p0, LX/2Wj;->l:F

    cmpl-float v6, v6, v1

    if-eqz v6, :cond_4

    .line 419098
    iget v6, p0, LX/2Wj;->m:F

    iget v8, p0, LX/2Wj;->l:F

    sub-float/2addr v6, v8

    invoke-static {v1, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    add-float/2addr v6, v5

    .line 419099
    iget v5, p0, LX/2Wj;->m:F

    iget v8, p0, LX/2Wj;->l:F

    add-float/2addr v5, v8

    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    add-float/2addr v4, v5

    .line 419100
    iget v5, p0, LX/2Wj;->n:F

    iget v8, p0, LX/2Wj;->l:F

    sub-float/2addr v5, v8

    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    add-float/2addr v5, v0

    .line 419101
    iget v0, p0, LX/2Wj;->n:F

    iget v8, p0, LX/2Wj;->l:F

    add-float/2addr v0, v8

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    add-float/2addr v0, v2

    move v1, v4

    move v2, v5

    move v4, v6

    .line 419102
    :goto_1
    invoke-virtual {p1, v4, v2, v1, v0}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 419103
    iget-object v0, p0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    move-result v0

    .line 419104
    sub-int v0, v9, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 419105
    div-int/lit8 v0, v0, 0x2

    .line 419106
    invoke-virtual {p0}, LX/2Wj;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, LX/2Wj;->r:Landroid/graphics/drawable/Drawable;

    invoke-static {v2}, LX/2Wj;->a(Landroid/graphics/drawable/Drawable;)I

    move-result v2

    add-int/2addr v1, v2

    .line 419107
    int-to-float v1, v1

    add-int/2addr v0, v7

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 419108
    iget-object v0, p0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {v0, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 419109
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 419110
    iget-object v0, p0, LX/2Wj;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 419111
    iget-object v0, p0, LX/2Wj;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 419112
    :cond_1
    iget-object v0, p0, LX/2Wj;->s:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 419113
    iget-object v0, p0, LX/2Wj;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 419114
    :cond_2
    return-void

    .line 419115
    :cond_3
    add-int v0, v7, v8

    int-to-float v0, v0

    goto/16 :goto_0

    :cond_4
    move v1, v4

    move v4, v5

    move v12, v0

    move v0, v2

    move v2, v12

    goto :goto_1
.end method

.method public final onMeasure(II)V
    .locals 12

    .prologue
    .line 419116
    const-string v0, "VariableTextLayoutView.onMeasure"

    const v1, 0x18ca40b0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 419117
    :try_start_0
    invoke-static {p0}, LX/2Wj;->i(LX/2Wj;)V

    .line 419118
    const/16 v0, 0x4000

    invoke-static {v0, p1}, LX/2Wj;->getDefaultSize(II)I

    move-result v1

    .line 419119
    iget-object v0, p0, LX/2Wj;->b:LX/0aq;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/Layout;

    .line 419120
    if-nez v0, :cond_0

    .line 419121
    invoke-virtual {p0}, LX/2Wj;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, LX/2Wj;->getPaddingRight()I

    move-result v2

    add-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 419122
    iget-object v2, p0, LX/2Wj;->r:Landroid/graphics/drawable/Drawable;

    invoke-static {v2}, LX/2Wj;->a(Landroid/graphics/drawable/Drawable;)I

    move-result v2

    sub-int/2addr v0, v2

    .line 419123
    iget-object v2, p0, LX/2Wj;->s:Landroid/graphics/drawable/Drawable;

    invoke-static {v2}, LX/2Wj;->a(Landroid/graphics/drawable/Drawable;)I

    move-result v2

    sub-int/2addr v0, v2

    .line 419124
    invoke-direct {p0, v0}, LX/2Wj;->e(I)Landroid/text/Layout;

    move-result-object v0

    .line 419125
    iget-object v2, p0, LX/2Wj;->b:LX/0aq;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419126
    :cond_0
    const/4 v8, 0x0

    .line 419127
    move v9, v8

    .line 419128
    :goto_0
    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v10

    if-ge v8, v10, :cond_1

    .line 419129
    invoke-virtual {v0, v8}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v10

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v10, v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 419130
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 419131
    :cond_1
    invoke-virtual {p0}, LX/2Wj;->getPaddingLeft()I

    move-result v8

    invoke-virtual {p0}, LX/2Wj;->getPaddingRight()I

    move-result v10

    add-int/2addr v8, v10

    iget-object v10, p0, LX/2Wj;->r:Landroid/graphics/drawable/Drawable;

    invoke-static {v10}, LX/2Wj;->a(Landroid/graphics/drawable/Drawable;)I

    move-result v10

    add-int/2addr v8, v10

    iget-object v10, p0, LX/2Wj;->s:Landroid/graphics/drawable/Drawable;

    invoke-static {v10}, LX/2Wj;->a(Landroid/graphics/drawable/Drawable;)I

    move-result v10

    add-int/2addr v8, v10

    add-int/2addr v8, v9

    .line 419132
    invoke-virtual {p0}, LX/2Wj;->getSuggestedMinimumWidth()I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 419133
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v10

    .line 419134
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    .line 419135
    sparse-switch v10, :sswitch_data_0

    move v8, v9

    .line 419136
    :goto_1
    :sswitch_0
    move v1, v8

    .line 419137
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 419138
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 419139
    invoke-virtual {p0}, LX/2Wj;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    move-result v5

    invoke-virtual {p0}, LX/2Wj;->getPaddingBottom()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {p0}, LX/2Wj;->getPaddingTop()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 419140
    sparse-switch v4, :sswitch_data_1

    move v2, v3

    .line 419141
    :goto_2
    :sswitch_1
    move v2, v2

    .line 419142
    iput-object v0, p0, LX/2Wj;->q:Landroid/text/Layout;

    .line 419143
    iget-object v0, p0, LX/2Wj;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    .line 419144
    iget-object v0, p0, LX/2Wj;->t:Landroid/graphics/Rect;

    if-nez v0, :cond_2

    .line 419145
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/2Wj;->t:Landroid/graphics/Rect;

    .line 419146
    :cond_2
    iget-object v0, p0, LX/2Wj;->r:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, LX/2Wj;->t:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    .line 419147
    iget-object v0, p0, LX/2Wj;->t:Landroid/graphics/Rect;

    invoke-virtual {p0}, LX/2Wj;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, LX/2Wj;->getBaseline()I

    move-result v4

    iget-object v5, p0, LX/2Wj;->t:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 419148
    iget-object v0, p0, LX/2Wj;->r:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, LX/2Wj;->t:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 419149
    :cond_3
    iget-object v0, p0, LX/2Wj;->s:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_5

    .line 419150
    iget-object v0, p0, LX/2Wj;->t:Landroid/graphics/Rect;

    if-nez v0, :cond_4

    .line 419151
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/2Wj;->t:Landroid/graphics/Rect;

    .line 419152
    :cond_4
    iget-object v0, p0, LX/2Wj;->s:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, LX/2Wj;->t:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    .line 419153
    iget-object v0, p0, LX/2Wj;->r:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, LX/2Wj;->a(Landroid/graphics/drawable/Drawable;)I

    move-result v0

    .line 419154
    iget-object v3, p0, LX/2Wj;->t:Landroid/graphics/Rect;

    invoke-virtual {p0}, LX/2Wj;->getPaddingLeft()I

    move-result v4

    iget-object v5, p0, LX/2Wj;->q:Landroid/text/Layout;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v5

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    add-int/2addr v4, v5

    add-int/2addr v0, v4

    invoke-virtual {p0}, LX/2Wj;->getBaseline()I

    move-result v4

    iget-object v5, p0, LX/2Wj;->t:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v3, v0, v4}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 419155
    iget-object v0, p0, LX/2Wj;->s:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, LX/2Wj;->t:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 419156
    :cond_5
    invoke-virtual {p0, v1, v2}, LX/2Wj;->setMeasuredDimension(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 419157
    const v0, -0xcdbf156

    invoke-static {v0}, LX/02m;->a(I)V

    .line 419158
    return-void

    .line 419159
    :catchall_0
    move-exception v0

    const v1, -0xdf7c3ce

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 419160
    :sswitch_2
    :try_start_1
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    goto/16 :goto_1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 419161
    :sswitch_3
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_2
        0x0 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x0 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1d6da555

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 419162
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 419163
    invoke-direct {p0}, LX/2Wj;->d()V

    .line 419164
    const/16 v1, 0x2d

    const v2, 0x46a73be0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAlignment(Landroid/text/Layout$Alignment;)V
    .locals 0

    .prologue
    .line 419165
    iput-object p1, p0, LX/2Wj;->i:Landroid/text/Layout$Alignment;

    .line 419166
    invoke-direct {p0}, LX/2Wj;->d()V

    .line 419167
    invoke-virtual {p0}, LX/2Wj;->invalidate()V

    .line 419168
    return-void
.end method

.method public setData(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 419062
    iput-object p1, p0, LX/2Wj;->c:Ljava/lang/Object;

    .line 419063
    if-eqz p1, :cond_0

    .line 419064
    invoke-virtual {p0, p1}, LX/2Wj;->a(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2Wj;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 419065
    :goto_0
    invoke-direct {p0}, LX/2Wj;->d()V

    .line 419066
    invoke-virtual {p0}, LX/2Wj;->invalidate()V

    .line 419067
    return-void

    .line 419068
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/2Wj;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setLeftDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 419169
    iget-object v0, p0, LX/2Wj;->r:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    .line 419170
    iput-object p1, p0, LX/2Wj;->r:Landroid/graphics/drawable/Drawable;

    .line 419171
    invoke-virtual {p0}, LX/2Wj;->requestLayout()V

    .line 419172
    :cond_0
    return-void
.end method

.method public setMaxLines(I)V
    .locals 0

    .prologue
    .line 419173
    iput p1, p0, LX/2Wj;->k:I

    .line 419174
    invoke-direct {p0}, LX/2Wj;->d()V

    .line 419175
    invoke-virtual {p0}, LX/2Wj;->invalidate()V

    .line 419176
    return-void
.end method

.method public setRightDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 419177
    iget-object v0, p0, LX/2Wj;->s:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    .line 419178
    iput-object p1, p0, LX/2Wj;->s:Landroid/graphics/drawable/Drawable;

    .line 419179
    invoke-virtual {p0}, LX/2Wj;->requestLayout()V

    .line 419180
    :cond_0
    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 419181
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 419182
    iput-object v0, p0, LX/2Wj;->d:Landroid/content/res/ColorStateList;

    .line 419183
    invoke-static {p0}, LX/2Wj;->c(LX/2Wj;)V

    .line 419184
    return-void
.end method
