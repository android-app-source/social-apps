.class public LX/2yV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/C0a;

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 482011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482012
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLProfile;)V
    .locals 2

    .prologue
    .line 482013
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->s()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 482014
    sget-object v0, LX/C0a;->LIKED_PAGE:LX/C0a;

    iput-object v0, p0, LX/2yV;->a:LX/C0a;

    .line 482015
    :goto_0
    return-void

    .line 482016
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->K()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v0, v1, :cond_1

    .line 482017
    sget-object v0, LX/C0a;->FOLLOWING_USER:LX/C0a;

    iput-object v0, p0, LX/2yV;->a:LX/C0a;

    goto :goto_0

    .line 482018
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->K()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v0, v1, :cond_2

    .line 482019
    sget-object v0, LX/C0a;->FOLLOW_USER:LX/C0a;

    iput-object v0, p0, LX/2yV;->a:LX/C0a;

    goto :goto_0

    .line 482020
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->s()Z

    move-result v0

    if-nez v0, :cond_3

    .line 482021
    sget-object v0, LX/C0a;->LIKE_PAGE:LX/C0a;

    iput-object v0, p0, LX/2yV;->a:LX/C0a;

    goto :goto_0

    .line 482022
    :cond_3
    sget-object v0, LX/C0a;->CANNOT_SUBSCRIBE:LX/C0a;

    iput-object v0, p0, LX/2yV;->a:LX/C0a;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 482023
    iput-boolean p1, p0, LX/2yV;->b:Z

    .line 482024
    return-void
.end method
