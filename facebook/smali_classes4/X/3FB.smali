.class public LX/3FB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3FB;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2cw;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2cw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 538616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538617
    iput-object p1, p0, LX/3FB;->a:LX/0Ot;

    .line 538618
    iput-object p2, p0, LX/3FB;->b:LX/0Ot;

    .line 538619
    return-void
.end method

.method public static a(LX/0QB;)LX/3FB;
    .locals 5

    .prologue
    .line 538620
    sget-object v0, LX/3FB;->c:LX/3FB;

    if-nez v0, :cond_1

    .line 538621
    const-class v1, LX/3FB;

    monitor-enter v1

    .line 538622
    :try_start_0
    sget-object v0, LX/3FB;->c:LX/3FB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 538623
    if-eqz v2, :cond_0

    .line 538624
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 538625
    new-instance v3, LX/3FB;

    const/16 v4, 0xf57

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x2ba

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/3FB;-><init>(LX/0Ot;LX/0Ot;)V

    .line 538626
    move-object v0, v3

    .line 538627
    sput-object v0, LX/3FB;->c:LX/3FB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 538628
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 538629
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 538630
    :cond_1
    sget-object v0, LX/3FB;->c:LX/3FB;

    return-object v0

    .line 538631
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 538632
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/3FC;)V
    .locals 2

    .prologue
    .line 538633
    iget-object v0, p0, LX/3FB;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cw;

    sget-object v1, LX/2cx;->GPS:LX/2cx;

    invoke-virtual {v0, p1, v1}, LX/2cw;->a(LX/3FC;LX/2cx;)V

    .line 538634
    return-void
.end method
