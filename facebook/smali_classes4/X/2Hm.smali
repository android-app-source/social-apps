.class public LX/2Hm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/2Hm;


# instance fields
.field public final d:LX/0Xl;

.field public final e:LX/0pu;

.field public final f:LX/0SG;

.field public volatile g:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 391073
    const-class v0, LX/2Hm;

    sput-object v0, LX/2Hm;->c:Ljava/lang/Class;

    .line 391074
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/2Hm;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".USER_ENTERED_DEVICE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Hm;->a:Ljava/lang/String;

    .line 391075
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/2Hm;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".USER_LEFT_DEVICE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Hm;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Xl;LX/0pu;LX/0SG;)V
    .locals 2
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391077
    iput-object p1, p0, LX/2Hm;->d:LX/0Xl;

    .line 391078
    iput-object p2, p0, LX/2Hm;->e:LX/0pu;

    .line 391079
    iput-object p3, p0, LX/2Hm;->f:LX/0SG;

    .line 391080
    iget-object v0, p0, LX/2Hm;->e:LX/0pu;

    new-instance v1, LX/2Hn;

    invoke-direct {v1, p0}, LX/2Hn;-><init>(LX/2Hm;)V

    invoke-virtual {v0, v1}, LX/0pu;->a(LX/0q0;)V

    .line 391081
    return-void
.end method

.method public static a(LX/0QB;)LX/2Hm;
    .locals 6

    .prologue
    .line 391082
    sget-object v0, LX/2Hm;->h:LX/2Hm;

    if-nez v0, :cond_1

    .line 391083
    const-class v1, LX/2Hm;

    monitor-enter v1

    .line 391084
    :try_start_0
    sget-object v0, LX/2Hm;->h:LX/2Hm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391085
    if-eqz v2, :cond_0

    .line 391086
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 391087
    new-instance p0, LX/2Hm;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {v0}, LX/0pu;->a(LX/0QB;)LX/0pu;

    move-result-object v4

    check-cast v4, LX/0pu;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {p0, v3, v4, v5}, LX/2Hm;-><init>(LX/0Xl;LX/0pu;LX/0SG;)V

    .line 391088
    move-object v0, p0

    .line 391089
    sput-object v0, LX/2Hm;->h:LX/2Hm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391090
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391091
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391092
    :cond_1
    sget-object v0, LX/2Hm;->h:LX/2Hm;

    return-object v0

    .line 391093
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391094
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
