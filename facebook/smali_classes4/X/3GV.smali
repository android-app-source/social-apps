.class public final LX/3GV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3GW;


# instance fields
.field public final synthetic a:LX/0x8;


# direct methods
.method public constructor <init>(LX/0x8;)V
    .locals 0

    .prologue
    .line 541179
    iput-object p1, p0, LX/3GV;->a:LX/0x8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 541172
    iget-object v0, p0, LX/3GV;->a:LX/0x8;

    iget-object v0, v0, LX/0x8;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0, v1}, LX/0tK;->a(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 541173
    iget-object v0, p0, LX/3GV;->a:LX/0x8;

    iget-object v0, v0, LX/0x8;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0tK;->d(Z)V

    .line 541174
    :goto_0
    iget-object v0, p0, LX/3GV;->a:LX/0x8;

    invoke-static {v0}, LX/0x8;->c(LX/0x8;)V

    .line 541175
    return-void

    .line 541176
    :cond_0
    iget-object v0, p0, LX/3GV;->a:LX/0x8;

    iget-object v0, v0, LX/0x8;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0, v1}, LX/0tK;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3GV;->a:LX/0x8;

    iget-object v0, v0, LX/0x8;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541177
    iget-object v0, p0, LX/3GV;->a:LX/0x8;

    iget-object v0, v0, LX/0x8;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0, v1}, LX/0tK;->f(Z)V

    goto :goto_0

    .line 541178
    :cond_1
    iget-object v0, p0, LX/3GV;->a:LX/0x8;

    iget-object v0, v0, LX/0x8;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0, v1}, LX/0tK;->d(Z)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 541160
    iget-object v0, p0, LX/3GV;->a:LX/0x8;

    iget-object v0, v0, LX/0x8;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    sget-object v1, LX/0ax;->gG:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 541161
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 541170
    iget-object v0, p0, LX/3GV;->a:LX/0x8;

    iget-object v0, v0, LX/0x8;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->e()V

    .line 541171
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 541162
    iget-object v0, p0, LX/3GV;->a:LX/0x8;

    iget-object v1, v0, LX/0x8;->c:Landroid/app/Activity;

    .line 541163
    iget-object v0, p0, LX/3GV;->a:LX/0x8;

    iget-object v0, v0, LX/0x8;->d:LX/13Z;

    sget-object v2, LX/13Z;->DSM_OFF:LX/13Z;

    if-eq v0, v2, :cond_0

    if-nez v1, :cond_1

    .line 541164
    :cond_0
    :goto_0
    return-void

    .line 541165
    :cond_1
    iget-object v0, p0, LX/3GV;->a:LX/0x8;

    iget-object v0, v0, LX/0x8;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    invoke-virtual {v0}, Lcom/facebook/datasensitivity/DataSaverBar;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 541166
    instance-of v2, v0, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v2, :cond_0

    .line 541167
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v0

    .line 541168
    const v2, 0x3f4ccccd    # 0.8f

    invoke-static {v0, v2}, LX/47Z;->a(IF)I

    move-result v2

    move v0, v2

    .line 541169
    invoke-static {v1, v0}, LX/470;->a(Landroid/view/Window;I)V

    goto :goto_0
.end method
