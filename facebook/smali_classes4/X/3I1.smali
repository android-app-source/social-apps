.class public LX/3I1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/3I1;


# instance fields
.field public final a:LX/0ad;

.field public final b:Z

.field public final c:I

.field public final d:I

.field public final e:Z

.field public final f:Z


# direct methods
.method public constructor <init>(LX/0ad;LX/0Uh;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 545432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 545433
    iput-object p1, p0, LX/3I1;->a:LX/0ad;

    .line 545434
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-object v1, LX/0c1;->Off:LX/0c1;

    sget-short v2, LX/2mm;->o:S

    invoke-interface {p1, v0, v1, v2, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3I1;->b:Z

    .line 545435
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-object v1, LX/0c1;->Off:LX/0c1;

    sget v2, LX/2mm;->p:I

    const/16 v3, 0x1e

    invoke-interface {p1, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    iput v0, p0, LX/3I1;->c:I

    .line 545436
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-object v1, LX/0c1;->Off:LX/0c1;

    sget v2, LX/2mm;->q:I

    const/16 v3, 0x1388

    invoke-interface {p1, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    iput v0, p0, LX/3I1;->d:I

    .line 545437
    const/16 v0, 0x467

    invoke-virtual {p2, v0, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3I1;->e:Z

    .line 545438
    const/16 v0, 0x315

    invoke-virtual {p2, v0, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3I1;->f:Z

    .line 545439
    return-void
.end method

.method public static a(LX/0QB;)LX/3I1;
    .locals 5

    .prologue
    .line 545440
    sget-object v0, LX/3I1;->g:LX/3I1;

    if-nez v0, :cond_1

    .line 545441
    const-class v1, LX/3I1;

    monitor-enter v1

    .line 545442
    :try_start_0
    sget-object v0, LX/3I1;->g:LX/3I1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 545443
    if-eqz v2, :cond_0

    .line 545444
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 545445
    new-instance p0, LX/3I1;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/3I1;-><init>(LX/0ad;LX/0Uh;)V

    .line 545446
    move-object v0, p0

    .line 545447
    sput-object v0, LX/3I1;->g:LX/3I1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 545448
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 545449
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 545450
    :cond_1
    sget-object v0, LX/3I1;->g:LX/3I1;

    return-object v0

    .line 545451
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 545452
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 545453
    iget-object v0, p0, LX/3I1;->a:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-short v2, LX/2mm;->o:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(LX/0c0;S)V

    .line 545454
    return-void
.end method
