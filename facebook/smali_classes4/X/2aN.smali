.class public LX/2aN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0So;

.field public final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/0So;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 424606
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424607
    iput-object p1, p0, LX/2aN;->a:LX/0So;

    .line 424608
    iput-object p2, p0, LX/2aN;->b:Landroid/content/Context;

    .line 424609
    return-void
.end method

.method public static b(LX/0QB;)LX/2aN;
    .locals 3

    .prologue
    .line 424613
    new-instance v2, LX/2aN;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v0, v1}, LX/2aN;-><init>(LX/0So;Landroid/content/Context;)V

    .line 424614
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Landroid/content/Intent;LX/8D4;LX/3B2;I)Landroid/app/PendingIntent;
    .locals 5

    .prologue
    const/high16 v4, 0x10000000

    .line 424615
    const-string v0, "notification_launch_source"

    const-string v1, "source_system_tray"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 424616
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2aN;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/notifications/service/SystemTrayLogService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "NOTIF_LOG"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EVENT_TYPE"

    if-nez p4, :cond_0

    sget-object p4, LX/3B2;->CLICK_FROM_TRAY:LX/3B2;

    :cond_0
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "COMPONENT_TYPE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "NOTIFICATION_ID"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, LX/Dqo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 424617
    iget-object v1, p0, LX/2aN;->b:Landroid/content/Context;

    iget-object v2, p0, LX/2aN;->a:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {v1, v2, v0, v4}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Landroid/content/Intent;LX/8D4;LX/3B2;I)Landroid/app/PendingIntent;
    .locals 5

    .prologue
    const/high16 v4, 0x10000000

    .line 424610
    const-string v0, "notification_launch_source"

    const-string v1, "source_system_tray"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 424611
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2aN;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/notifications/service/SystemTrayLogWrapperActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "NOTIF_LOG"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EVENT_TYPE"

    if-nez p4, :cond_0

    sget-object p4, LX/3B2;->CLICK_FROM_TRAY:LX/3B2;

    :cond_0
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "COMPONENT_TYPE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "NOTIFICATION_ID"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, LX/Dqo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 424612
    iget-object v1, p0, LX/2aN;->b:Landroid/content/Context;

    iget-object v2, p0, LX/2aN;->a:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {v1, v2, v0, v4}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method
