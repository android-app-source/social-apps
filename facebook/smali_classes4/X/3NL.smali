.class public LX/3NL;
.super LX/3Ml;
.source ""


# static fields
.field private static final c:Ljava/lang/Class;


# instance fields
.field private final d:LX/2P4;

.field private final e:LX/26j;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 559097
    const-class v0, LX/3NL;

    sput-object v0, LX/3NL;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zr;LX/2P4;LX/26j;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 559093
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 559094
    iput-object p2, p0, LX/3NL;->d:LX/2P4;

    .line 559095
    iput-object p3, p0, LX/3NL;->e:LX/26j;

    .line 559096
    return-void
.end method

.method public static a(LX/0QB;)LX/3NL;
    .locals 4

    .prologue
    .line 559061
    new-instance v3, LX/3NL;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v0

    check-cast v0, LX/0Zr;

    invoke-static {p0}, LX/2P4;->a(LX/0QB;)LX/2P4;

    move-result-object v1

    check-cast v1, LX/2P4;

    invoke-static {p0}, LX/26j;->b(LX/0QB;)LX/26j;

    move-result-object v2

    check-cast v2, LX/26j;

    invoke-direct {v3, v0, v1, v2}, LX/3NL;-><init>(LX/0Zr;LX/2P4;LX/26j;)V

    .line 559062
    move-object v0, v3

    .line 559063
    return-object v0
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 7
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 559065
    new-instance v1, LX/39y;

    invoke-direct {v1}, LX/39y;-><init>()V

    .line 559066
    invoke-virtual {p0}, LX/3NL;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 559067
    sget-object v0, LX/3NL;->c:Ljava/lang/Class;

    const-string v3, "Filtering called on a disabled filter."

    invoke-static {v0, v3}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    .line 559068
    iput v2, v1, LX/39y;->b:I

    .line 559069
    invoke-static {p1}, LX/3Og;->b(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 559070
    :goto_0
    return-object v0

    .line 559071
    :cond_0
    iget-object v0, p0, LX/3Ml;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 559072
    iput v2, v1, LX/39y;->b:I

    .line 559073
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 559074
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 559075
    goto :goto_0

    .line 559076
    :cond_1
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 559077
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 559078
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    .line 559079
    const/4 v0, -0x1

    iput v0, v1, LX/39y;->b:I

    move-object v0, v1

    .line 559080
    goto :goto_0

    .line 559081
    :cond_2
    const-string v0, ""

    goto :goto_1

    .line 559082
    :cond_3
    iget-object v3, p0, LX/3NL;->d:LX/2P4;

    invoke-virtual {v3, v0}, LX/2P4;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v3

    .line 559083
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 559084
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    :goto_2
    if-ge v2, v5, :cond_5

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 559085
    iget-object v6, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v6, v0}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v0

    .line 559086
    if-eqz v0, :cond_4

    .line 559087
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 559088
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 559089
    :cond_5
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 559090
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v2

    iput-object v2, v1, LX/39y;->a:Ljava/lang/Object;

    .line 559091
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iput v0, v1, LX/39y;->b:I

    move-object v0, v1

    .line 559092
    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 559064
    iget-object v0, p0, LX/3NL;->e:LX/26j;

    invoke-virtual {v0}, LX/26j;->a()Z

    move-result v0

    return v0
.end method
