.class public LX/2Wy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 419377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(IIILX/0ej;LX/26w;LX/0oc;)V
    .locals 3

    .prologue
    .line 419378
    sget-object v0, LX/0oc;->OVERRIDE:LX/0oc;

    if-ne p5, v0, :cond_1

    invoke-virtual {p3, p0}, LX/0ej;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 419379
    invoke-virtual {p4, p1}, LX/26w;->a(I)V

    .line 419380
    :cond_0
    :goto_0
    return-void

    .line 419381
    :cond_1
    packed-switch p2, :pswitch_data_0

    .line 419382
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419383
    :pswitch_0
    invoke-virtual {p3, p5, p0}, LX/0ej;->f(LX/0oc;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419384
    invoke-virtual {p3, p5, p0}, LX/0ej;->e(LX/0oc;I)Z

    move-result v0

    invoke-virtual {p4, p5, p1, v0}, LX/26w;->a(LX/0oc;IZ)V

    goto :goto_0

    .line 419385
    :pswitch_1
    invoke-virtual {p3, p5, p0}, LX/0ej;->f(LX/0oc;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419386
    invoke-virtual {p3, p5, p0}, LX/0ej;->b(LX/0oc;I)I

    move-result v0

    invoke-virtual {p4, p5, p1, v0}, LX/26w;->a(LX/0oc;II)V

    goto :goto_0

    .line 419387
    :pswitch_2
    invoke-virtual {p3, p5, p0}, LX/0ej;->f(LX/0oc;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419388
    invoke-virtual {p3, p5, p0}, LX/0ej;->c(LX/0oc;I)J

    move-result-wide v0

    invoke-virtual {p4, p5, p1, v0, v1}, LX/26w;->a(LX/0oc;IJ)V

    goto :goto_0

    .line 419389
    :pswitch_3
    invoke-virtual {p3, p5, p0}, LX/0ej;->f(LX/0oc;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419390
    invoke-virtual {p3, p5, p0}, LX/0ej;->d(LX/0oc;I)F

    move-result v0

    invoke-virtual {p4, p5, p1, v0}, LX/26w;->a(LX/0oc;IF)V

    goto :goto_0

    .line 419391
    :pswitch_4
    invoke-virtual {p3, p5, p0}, LX/0ej;->f(LX/0oc;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419392
    invoke-virtual {p3, p5, p0}, LX/0ej;->a(LX/0oc;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, p5, p1, v0}, LX/26w;->a(LX/0oc;ILjava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
