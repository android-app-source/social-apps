.class public LX/2e9;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DFF;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DFH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 445069
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/2e9;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DFH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 445066
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 445067
    iput-object p1, p0, LX/2e9;->b:LX/0Ot;

    .line 445068
    return-void
.end method

.method public static a(LX/0QB;)LX/2e9;
    .locals 4

    .prologue
    .line 445055
    const-class v1, LX/2e9;

    monitor-enter v1

    .line 445056
    :try_start_0
    sget-object v0, LX/2e9;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 445057
    sput-object v2, LX/2e9;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 445058
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445059
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 445060
    new-instance v3, LX/2e9;

    const/16 p0, 0x20df

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2e9;-><init>(LX/0Ot;)V

    .line 445061
    move-object v0, v3

    .line 445062
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 445063
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2e9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445064
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 445065
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 445070
    const v0, -0xa6c3dfb

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 445050
    iget-object v0, p0, LX/2e9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 p2, 0x0

    .line 445051
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f020a84

    invoke-interface {v0, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    const p0, 0x7f081868

    invoke-virtual {v1, p0}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    const p0, 0x7f0b0050

    invoke-virtual {v1, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const p0, 0x7f0a0443

    invoke-virtual {v1, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v1

    sget-object p0, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v1, p0}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const p0, 0x7f0b0f6d

    invoke-interface {v1, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    .line 445052
    const v1, -0xa6c3dfb

    const/4 p0, 0x0

    invoke-static {p1, v1, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 445053
    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 445054
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 445032
    invoke-static {}, LX/1dS;->b()V

    .line 445033
    iget v0, p1, LX/1dQ;->b:I

    .line 445034
    packed-switch v0, :pswitch_data_0

    .line 445035
    :goto_0
    return-object v2

    .line 445036
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 445037
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 445038
    iget-object p1, p0, LX/2e9;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/DFH;

    .line 445039
    sget-object p2, LX/0ax;->cV:Ljava/lang/String;

    sget-object p0, LX/5Oz;->FEED_PYMK:LX/5Oz;

    invoke-virtual {p0}, LX/5Oz;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p2, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 445040
    iget-object p0, p1, LX/DFH;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p0, v1, p2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 445041
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0xa6c3dfb
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/DFF;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 445042
    new-instance v1, LX/DFG;

    invoke-direct {v1, p0}, LX/DFG;-><init>(LX/2e9;)V

    .line 445043
    sget-object v2, LX/2e9;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/DFF;

    .line 445044
    if-nez v2, :cond_0

    .line 445045
    new-instance v2, LX/DFF;

    invoke-direct {v2}, LX/DFF;-><init>()V

    .line 445046
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/DFF;->a$redex0(LX/DFF;LX/1De;IILX/DFG;)V

    .line 445047
    move-object v1, v2

    .line 445048
    move-object v0, v1

    .line 445049
    return-object v0
.end method
