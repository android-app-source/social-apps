.class public LX/2oO;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:Ljava/lang/String;

.field public B:Landroid/net/Uri;

.field public C:I

.field public D:I

.field private E:Z

.field private F:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field private G:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/feed/autoplay/AutoplayStateManager$InteractionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final H:LX/19w;

.field private final b:Landroid/view/accessibility/AccessibilityManager;

.field public final c:LX/0ad;

.field public final d:LX/03V;

.field private final e:LX/1Bv;

.field public final f:LX/1m0;

.field public final g:LX/0ka;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public final j:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public final k:Z

.field public final l:Z

.field public final m:LX/0oz;

.field public final n:LX/19j;

.field private final o:LX/0xX;

.field private final p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/2oR;

.field private final r:LX/2oU;

.field private s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field private w:Z

.field private x:Z

.field public y:Z

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 466279
    const-class v0, LX/2oO;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2oO;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/accessibility/AccessibilityManager;LX/0ad;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;Ljava/lang/Integer;LX/03V;LX/1Bv;LX/1m0;LX/0ka;LX/0Or;LX/0oz;LX/19j;LX/0xX;LX/19w;LX/2oR;LX/2oU;)V
    .locals 5
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/graphql/model/GraphQLVideo;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/video/abtest/RemoveVideoAlreadySeen;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/accessibility/AccessibilityManager;",
            "LX/0ad;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "Ljava/lang/Integer;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Bv;",
            "LX/1m0;",
            "LX/0ka;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0oz;",
            "LX/19j;",
            "LX/0xX;",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            "LX/2oR;",
            "LX/2oU;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 466280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 466281
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, LX/2oO;->p:Ljava/util/Set;

    .line 466282
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2oO;->s:Z

    .line 466283
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2oO;->t:Z

    .line 466284
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2oO;->u:Z

    .line 466285
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2oO;->v:Z

    .line 466286
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2oO;->w:Z

    .line 466287
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2oO;->x:Z

    .line 466288
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2oO;->y:Z

    .line 466289
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2oO;->z:Z

    .line 466290
    iput-object p1, p0, LX/2oO;->b:Landroid/view/accessibility/AccessibilityManager;

    .line 466291
    iput-object p2, p0, LX/2oO;->c:LX/0ad;

    .line 466292
    iput-object p6, p0, LX/2oO;->d:LX/03V;

    .line 466293
    iput-object p7, p0, LX/2oO;->e:LX/1Bv;

    .line 466294
    iput-object p8, p0, LX/2oO;->f:LX/1m0;

    .line 466295
    iput-object p9, p0, LX/2oO;->g:LX/0ka;

    .line 466296
    iput-object p10, p0, LX/2oO;->h:LX/0Or;

    .line 466297
    if-nez p3, :cond_0

    const/4 v1, 0x0

    move-object v2, v1

    .line 466298
    :goto_0
    if-eqz v2, :cond_3

    .line 466299
    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 466300
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    .line 466301
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x0

    :goto_1
    iput-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 466302
    invoke-static {p3}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    iput-boolean v1, p0, LX/2oO;->l:Z

    .line 466303
    invoke-static {v3}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v1

    .line 466304
    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_2
    iput-boolean v1, p0, LX/2oO;->k:Z

    .line 466305
    :goto_3
    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v2, v1}, LX/2oO;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    move-result-object v1

    iput-object v1, p0, LX/2oO;->j:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 466306
    invoke-virtual {p0, p4}, LX/2oO;->a(Lcom/facebook/graphql/model/GraphQLVideo;)V

    .line 466307
    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLVideo;->af()Z

    move-result v1

    iput-boolean v1, p0, LX/2oO;->x:Z

    .line 466308
    move-object/from16 v0, p11

    iput-object v0, p0, LX/2oO;->m:LX/0oz;

    .line 466309
    move-object/from16 v0, p12

    iput-object v0, p0, LX/2oO;->n:LX/19j;

    .line 466310
    move-object/from16 v0, p13

    iput-object v0, p0, LX/2oO;->o:LX/0xX;

    .line 466311
    move-object/from16 v0, p14

    iput-object v0, p0, LX/2oO;->H:LX/19w;

    .line 466312
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2oO;->q:LX/2oR;

    .line 466313
    move-object/from16 v0, p16

    iput-object v0, p0, LX/2oO;->r:LX/2oU;

    .line 466314
    return-void

    .line 466315
    :cond_0
    invoke-virtual {p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v2, v1

    goto :goto_0

    .line 466316
    :cond_1
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_1

    .line 466317
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->k()Z

    move-result v1

    goto :goto_2

    .line 466318
    :cond_3
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    iput-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 466319
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2oO;->l:Z

    .line 466320
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/2oO;->k:Z

    goto :goto_3
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 466321
    if-ltz p1, :cond_0

    if-eqz p0, :cond_0

    invoke-static {p0}, LX/17E;->j(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/17E;->j(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/17E;->j(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 466322
    invoke-static {p0}, LX/17E;->j(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 466323
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/util/Set;Z)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 466324
    iget-object v0, p0, LX/2oO;->b:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466325
    sget-object v0, LX/046;->DISABLED_BY_ACCESSIBILITY:LX/046;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 466326
    :cond_0
    const/4 v0, 0x0

    .line 466327
    iget-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_DIRECT_RESPONSE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_CINEMAGRAPH:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NON_LINK_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RICH_MEDIA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RICH_MEDIA_COLLECTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v1, v2, :cond_3

    .line 466328
    iget-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, LX/2oO;->i:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NO_END_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v1, v2, :cond_d

    :cond_1
    iget-object v1, p0, LX/2oO;->j:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eqz v1, :cond_d

    iget-object v1, p0, LX/2oO;->j:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/2oO;->j:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_DIRECT_RESPONSE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v1, v2, :cond_d

    :cond_2
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 466329
    if-eqz v1, :cond_b

    .line 466330
    :cond_3
    :goto_1
    move v0, v0

    .line 466331
    if-eqz v0, :cond_4

    if-nez p2, :cond_4

    .line 466332
    sget-object v0, LX/046;->DISABLED_BY_SERVER:LX/046;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 466333
    :cond_4
    iget-object v0, p0, LX/2oO;->H:LX/19w;

    iget-object v1, p0, LX/2oO;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v0

    .line 466334
    if-eqz v0, :cond_5

    iget-boolean v1, v0, LX/2fs;->e:Z

    if-eqz v1, :cond_5

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-eq v0, v1, :cond_5

    .line 466335
    sget-object v0, LX/046;->DISABLED_BY_CACHE_NOT_READY:LX/046;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 466336
    :cond_5
    iget-object v0, p0, LX/2oO;->F:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_7

    .line 466337
    iget-object v0, p0, LX/2oO;->q:LX/2oR;

    iget-object v1, p0, LX/2oO;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2oR;->a(Ljava/lang/String;)Z

    move-result v5

    .line 466338
    new-instance v0, LX/1Bh;

    .line 466339
    iget-boolean v1, p0, LX/2oO;->z:Z

    if-eqz v1, :cond_e

    iget-object v1, p0, LX/2oO;->n:LX/19j;

    iget-object v1, v1, LX/19j;->j:LX/0p3;

    :goto_2
    move-object v1, v1

    .line 466340
    iget-boolean v2, p0, LX/2oO;->z:Z

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/2oO;->n:LX/19j;

    iget v2, v2, LX/19j;->k:I

    :goto_3
    move v2, v2

    .line 466341
    iget-object v3, p0, LX/2oO;->H:LX/19w;

    iget-object v6, p0, LX/2oO;->A:Ljava/lang/String;

    invoke-virtual {v3, v6}, LX/19w;->a(Ljava/lang/String;)Z

    move-result v3

    iget-object v6, p0, LX/2oO;->c:LX/0ad;

    sget-short v8, LX/0ws;->dB:S

    invoke-interface {v6, v8, v7}, LX/0ad;->a(SZ)Z

    move-result v6

    if-nez v6, :cond_6

    :goto_4
    invoke-direct/range {v0 .. v5}, LX/1Bh;-><init>(LX/0p3;IZZZ)V

    .line 466342
    iget-object v1, p0, LX/2oO;->e:LX/1Bv;

    invoke-virtual {v1, p1, v0}, LX/1Bv;->a(Ljava/util/Set;LX/1Bh;)Z

    .line 466343
    :goto_5
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    return v0

    :cond_6
    move v4, v7

    .line 466344
    goto :goto_4

    .line 466345
    :cond_7
    iget-boolean v0, p0, LX/2oO;->E:Z

    iget-object v1, p0, LX/2oO;->F:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-static {v0, v1}, LX/3In;->a(ZLcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 466346
    sget-object v0, LX/046;->DISABLED_BY_VOD_NOT_READY:LX/046;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 466347
    :cond_8
    iget-object v0, p0, LX/2oO;->r:LX/2oU;

    invoke-virtual {v0}, LX/2oU;->a()I

    move-result v0

    .line 466348
    if-eqz v0, :cond_a

    .line 466349
    iget v1, p0, LX/2oO;->D:I

    div-int/lit16 v1, v1, 0x3e8

    int-to-double v2, v1

    int-to-double v0, v0

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    div-double/2addr v0, v8

    mul-double/2addr v0, v2

    double-to-int v2, v0

    move v6, v4

    .line 466350
    :goto_6
    new-instance v0, LX/1Bh;

    sget-object v1, LX/0p3;->MODERATE:LX/0p3;

    iget-object v3, p0, LX/2oO;->H:LX/19w;

    iget-object v5, p0, LX/2oO;->A:Ljava/lang/String;

    invoke-virtual {v3, v5}, LX/19w;->a(Ljava/lang/String;)Z

    move-result v3

    iget-object v5, p0, LX/2oO;->c:LX/0ad;

    sget-short v8, LX/0ws;->dC:S

    invoke-interface {v5, v8, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    if-nez v5, :cond_9

    :goto_7
    move v5, v7

    invoke-direct/range {v0 .. v6}, LX/1Bh;-><init>(LX/0p3;IZZZZ)V

    .line 466351
    iget-object v1, p0, LX/2oO;->e:LX/1Bv;

    invoke-virtual {v1, p1, v0}, LX/1Bv;->a(Ljava/util/Set;LX/1Bh;)Z

    goto :goto_5

    :cond_9
    move v4, v7

    .line 466352
    goto :goto_7

    :cond_a
    move v6, v7

    move v2, v7

    goto :goto_6

    .line 466353
    :cond_b
    iget-boolean v1, p0, LX/2oO;->k:Z

    if-eqz v1, :cond_c

    iget-object v1, p0, LX/2oO;->g:LX/0ka;

    invoke-virtual {v1}, LX/0ka;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 466354
    :cond_c
    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_e
    iget-object v1, p0, LX/2oO;->n:LX/19j;

    iget-object v1, v1, LX/19j;->e:LX/0p3;

    goto/16 :goto_2

    :cond_f
    iget-object v2, p0, LX/2oO;->n:LX/19j;

    iget v2, v2, LX/19j;->f:I

    goto/16 :goto_3
.end method

.method private r()LX/EVQ;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 466355
    iget-object v0, p0, LX/2oO;->G:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2oO;->G:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EVQ;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 466356
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2oO;->t:Z

    .line 466357
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2oO;->u:Z

    .line 466358
    return-void
.end method

.method public final a(LX/EVQ;)V
    .locals 1
    .param p1    # LX/EVQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 466359
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2oO;->G:Ljava/lang/ref/WeakReference;

    .line 466360
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLVideo;)V
    .locals 1

    .prologue
    .line 466263
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2oO;->A:Ljava/lang/String;

    .line 466264
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/2oO;->B:Landroid/net/Uri;

    .line 466265
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideo;->m()I

    move-result v0

    iput v0, p0, LX/2oO;->C:I

    .line 466266
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideo;->q()I

    move-result v0

    iput v0, p0, LX/2oO;->D:I

    .line 466267
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideo;->ag()Z

    move-result v0

    iput-boolean v0, p0, LX/2oO;->E:Z

    .line 466268
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideo;->r()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    iput-object v0, p0, LX/2oO;->F:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466269
    return-void
.end method

.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 466270
    if-eqz p1, :cond_0

    .line 466271
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2oO;->s:Z

    .line 466272
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2oO;->t:Z

    .line 466273
    if-nez p1, :cond_1

    .line 466274
    iput-boolean p2, p0, LX/2oO;->u:Z

    .line 466275
    :cond_1
    invoke-direct {p0}, LX/2oO;->r()LX/EVQ;

    move-result-object v0

    .line 466276
    if-eqz v0, :cond_2

    .line 466277
    iget-object p0, v0, LX/EVQ;->a:Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;

    invoke-static {p0}, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->g(Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;)V

    .line 466278
    :cond_2
    return-void
.end method

.method public final a(Ljava/util/Set;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 466262
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/2oO;->a(Ljava/util/Set;Z)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/util/Set;Z)Z
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 466232
    iget-boolean v1, p0, LX/2oO;->y:Z

    if-eqz v1, :cond_1

    .line 466233
    :cond_0
    :goto_0
    return v0

    .line 466234
    :cond_1
    iget-boolean v1, p0, LX/2oO;->s:Z

    if-eqz v1, :cond_2

    .line 466235
    sget-object v1, LX/046;->DISABLED_BY_ALREADY_SEEN:LX/046;

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 466236
    :cond_2
    const/4 v10, -0x1

    const/4 v2, 0x0

    .line 466237
    iget-boolean v3, p0, LX/2oO;->l:Z

    if-nez v3, :cond_3

    iget-boolean v3, p0, LX/2oO;->v:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/2oO;->g:LX/0ka;

    invoke-virtual {v3}, LX/0ka;->b()Z

    move-result v3

    if-nez v3, :cond_8

    .line 466238
    :cond_3
    :goto_1
    move v1, v2

    .line 466239
    if-eqz v1, :cond_4

    .line 466240
    sget-object v1, LX/046;->DISABLED_BY_CACHE_NOT_READY:LX/046;

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 466241
    :cond_4
    iget-boolean v1, p0, LX/2oO;->x:Z

    if-eqz v1, :cond_5

    .line 466242
    if-eqz p1, :cond_5

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 466243
    :cond_5
    :goto_2
    iget-boolean v1, p0, LX/2oO;->w:Z

    if-eqz v1, :cond_6

    .line 466244
    sget-object v1, LX/046;->DISABLED_BY_PLAYER_ERROR_STATE:LX/046;

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 466245
    :cond_6
    invoke-direct {p0, p1, p2}, LX/2oO;->b(Ljava/util/Set;Z)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-boolean v1, p0, LX/2oO;->t:Z

    if-nez v1, :cond_7

    iget-boolean v1, p0, LX/2oO;->u:Z

    if-eqz v1, :cond_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_0

    .line 466246
    :cond_8
    iget-object v3, p0, LX/2oO;->c:LX/0ad;

    sget v4, LX/0ws;->b:I

    invoke-interface {v3, v4, v10}, LX/0ad;->a(II)I

    move-result v3

    .line 466247
    iget-object v4, p0, LX/2oO;->c:LX/0ad;

    sget v5, LX/0ws;->c:I

    invoke-interface {v4, v5, v10}, LX/0ad;->a(II)I

    move-result v4

    .line 466248
    if-ltz v4, :cond_a

    iget v5, p0, LX/2oO;->C:I

    if-lez v5, :cond_a

    iget v5, p0, LX/2oO;->D:I

    if-lez v5, :cond_a

    .line 466249
    iget v3, p0, LX/2oO;->C:I

    iget v5, p0, LX/2oO;->D:I

    div-int/lit8 v5, v5, 0x8

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    move v6, v3

    .line 466250
    :goto_3
    if-ltz v6, :cond_3

    .line 466251
    const-wide/16 v4, -0x1

    .line 466252
    :try_start_0
    iget-object v3, p0, LX/2oO;->f:LX/1m0;

    iget-object v7, p0, LX/2oO;->B:Landroid/net/Uri;

    .line 466253
    invoke-static {v3}, LX/1m0;->e(LX/1m0;)LX/7Po;

    move-result-object v11

    invoke-virtual {v11, v7}, LX/7Po;->a(Landroid/net/Uri;)J

    move-result-wide v11

    move-wide v4, v11
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 466254
    :goto_4
    const-wide/16 v8, 0x0

    cmp-long v3, v4, v8

    if-ltz v3, :cond_3

    int-to-long v8, v6

    cmp-long v3, v4, v8

    if-gez v3, :cond_3

    .line 466255
    long-to-int v3, v4

    sub-int v4, v6, v3

    .line 466256
    iget-object v3, p0, LX/2oO;->c:LX/0ad;

    sget v5, LX/0ws;->d:I

    invoke-interface {v3, v5, v10}, LX/0ad;->a(II)I

    move-result v3

    .line 466257
    if-lez v3, :cond_9

    .line 466258
    iget-object v5, p0, LX/2oO;->m:LX/0oz;

    invoke-virtual {v5}, LX/0oz;->f()D

    move-result-wide v6

    int-to-double v8, v3

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4020000000000000L    # 8.0

    div-double/2addr v6, v8

    double-to-int v3, v6

    .line 466259
    :goto_5
    if-ge v3, v4, :cond_3

    const/4 v2, 0x1

    goto/16 :goto_1

    .line 466260
    :catch_0
    move-exception v3

    .line 466261
    iget-object v7, p0, LX/2oO;->d:LX/03V;

    sget-object v8, LX/2oO;->a:Ljava/lang/String;

    const-string v9, "Error checking video cache for autoplay"

    invoke-virtual {v7, v8, v9, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_9
    move v3, v2

    goto :goto_5

    :cond_a
    move v6, v3

    goto :goto_3

    :cond_b
    goto/16 :goto_2
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 466230
    iput-boolean p1, p0, LX/2oO;->y:Z

    .line 466231
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 466225
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2oO;->s:Z

    .line 466226
    invoke-direct {p0}, LX/2oO;->r()LX/EVQ;

    move-result-object v0

    .line 466227
    if-eqz v0, :cond_0

    .line 466228
    iget-object p0, v0, LX/EVQ;->a:Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;

    invoke-static {p0}, Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;->g(Lcom/facebook/video/videohome/plugins/VideoHomeNonPlayingOverlayPlugin;)V

    .line 466229
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 466223
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2oO;->s:Z

    .line 466224
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 466221
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2oO;->w:Z

    .line 466222
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 466219
    iget-object v0, p0, LX/2oO;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 466220
    iget-object v0, p0, LX/2oO;->p:Ljava/util/Set;

    invoke-virtual {p0, v0}, LX/2oO;->a(Ljava/util/Set;)Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 466217
    iget-object v0, p0, LX/2oO;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 466218
    iget-object v0, p0, LX/2oO;->p:Ljava/util/Set;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/2oO;->b(Ljava/util/Set;Z)Z

    move-result v0

    return v0
.end method
