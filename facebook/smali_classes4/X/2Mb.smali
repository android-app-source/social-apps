.class public LX/2Mb;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/2MV;

.field private final c:LX/0TD;

.field public d:LX/2Mc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2Me;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2Mh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2Mi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 396992
    const-class v0, LX/2Mb;

    sput-object v0, LX/2Mb;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2MV;LX/0TD;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396989
    iput-object p1, p0, LX/2Mb;->b:LX/2MV;

    .line 396990
    iput-object p2, p0, LX/2Mb;->c:LX/0TD;

    .line 396991
    return-void
.end method

.method public static a(LX/0QB;)LX/2Mb;
    .locals 1

    .prologue
    .line 396993
    invoke-static {p0}, LX/2Mb;->b(LX/0QB;)LX/2Mb;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2Mb;
    .locals 5

    .prologue
    .line 396981
    new-instance v4, LX/2Mb;

    invoke-static {p0}, LX/2MU;->b(LX/0QB;)LX/2MU;

    move-result-object v0

    check-cast v0, LX/2MV;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-direct {v4, v0, v1}, LX/2Mb;-><init>(LX/2MV;LX/0TD;)V

    .line 396982
    new-instance v0, LX/2Mc;

    invoke-direct {v0}, LX/2Mc;-><init>()V

    .line 396983
    move-object v0, v0

    .line 396984
    move-object v0, v0

    .line 396985
    check-cast v0, LX/2Mc;

    invoke-static {p0}, LX/2Me;->b(LX/0QB;)LX/2Me;

    move-result-object v1

    check-cast v1, LX/2Me;

    invoke-static {p0}, LX/2Mh;->b(LX/0QB;)LX/2Mh;

    move-result-object v2

    check-cast v2, LX/2Mh;

    invoke-static {p0}, LX/2Mi;->a(LX/0QB;)LX/2Mi;

    move-result-object v3

    check-cast v3, LX/2Mi;

    .line 396986
    iput-object v0, v4, LX/2Mb;->d:LX/2Mc;

    iput-object v1, v4, LX/2Mb;->e:LX/2Me;

    iput-object v2, v4, LX/2Mb;->f:LX/2Mh;

    iput-object v3, v4, LX/2Mb;->g:LX/2Mi;

    .line 396987
    return-object v4
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)I
    .locals 5

    .prologue
    .line 396964
    :try_start_0
    iget-object v0, p0, LX/2Mb;->c:LX/0TD;

    new-instance v1, LX/FGe;

    invoke-direct {v1, p0, p1}, LX/FGe;-><init>(LX/2Mb;Lcom/facebook/ui/media/attachments/MediaResource;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 396965
    const-wide/16 v2, 0x12c

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v4, -0x550eeb64

    invoke-static {v0, v2, v3, v1, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/60x;

    .line 396966
    if-nez v0, :cond_0

    .line 396967
    const/4 v0, -0x1

    .line 396968
    :goto_0
    return v0

    .line 396969
    :cond_0
    sget-object v1, LX/FGf;->a:[I

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v2}, LX/5zj;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 396970
    iget-object v1, p0, LX/2Mb;->f:LX/2Mh;

    :goto_1
    move-object v1, v1

    .line 396971
    iget-object v2, p0, LX/2Mb;->g:LX/2Mi;

    iget v3, p1, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    iget v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    invoke-virtual {v2, v0, v1, v3, v4}, LX/2Mj;->a(LX/60x;LX/2Md;II)LX/7Sw;

    move-result-object v0

    .line 396972
    iget v0, v0, LX/7Sw;->c:I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 396973
    :catch_0
    move-exception v0

    .line 396974
    sget-object v1, LX/2Mb;->a:Ljava/lang/Class;

    const-string v2, "Timed out."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 396975
    const/4 v0, -0x2

    goto :goto_0

    .line 396976
    :catch_1
    move-exception v0

    .line 396977
    sget-object v1, LX/2Mb;->a:Ljava/lang/Class;

    const-string v2, "Cannot estimate num bytes in Media."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 396978
    const/4 v0, -0x3

    goto :goto_0

    .line 396979
    :pswitch_0
    iget-object v1, p0, LX/2Mb;->e:LX/2Me;

    goto :goto_1

    .line 396980
    :pswitch_1
    iget-object v1, p0, LX/2Mb;->d:LX/2Mc;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
