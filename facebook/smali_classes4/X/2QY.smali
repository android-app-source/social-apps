.class public abstract LX/2QY;
.super LX/2QZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PARAMS:",
        "Ljava/lang/Object;",
        "RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "LX/2QZ;"
    }
.end annotation


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0e6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Or;LX/0e6;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 408553
    invoke-direct {p0, p1}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 408554
    iput-object p2, p0, LX/2QY;->b:LX/0Or;

    .line 408555
    iput-object p3, p0, LX/2QY;->c:LX/0e6;

    .line 408556
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 408545
    iget-object v0, p0, LX/2QZ;->a:Ljava/lang/String;

    .line 408546
    iget-object v1, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v1, v1

    .line 408547
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 408548
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 408549
    invoke-virtual {p0, v0}, LX/2QY;->a(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v0

    .line 408550
    iget-object v1, p0, LX/2QY;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    move-object v1, v1

    .line 408551
    iget-object v2, p0, LX/2QY;->c:LX/0e6;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 408552
    invoke-virtual {p0, v0}, LX/2QY;->a(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRESU",
            "LT;",
            ")",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    .line 408543
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 408544
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")TPARAMS;"
        }
    .end annotation

    .prologue
    .line 408542
    const/4 v0, 0x0

    return-object v0
.end method
