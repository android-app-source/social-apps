.class public LX/3LL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0y3;

.field public final c:LX/0tX;

.field public final d:LX/3LM;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field public final f:Ljava/util/concurrent/ExecutorService;

.field public final g:LX/3LQ;

.field public final h:LX/0Uh;

.field public final i:LX/3LR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3LR",
            "<",
            "Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/3LU;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0y3;LX/0tX;LX/3LM;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/ExecutorService;LX/3LQ;LX/0Uh;)V
    .locals 1
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 550332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550333
    iput-object p1, p0, LX/3LL;->a:Landroid/content/Context;

    .line 550334
    iput-object p2, p0, LX/3LL;->b:LX/0y3;

    .line 550335
    iput-object p3, p0, LX/3LL;->c:LX/0tX;

    .line 550336
    iput-object p4, p0, LX/3LL;->d:LX/3LM;

    .line 550337
    iput-object p5, p0, LX/3LL;->e:Lcom/facebook/content/SecureContextHelper;

    .line 550338
    iput-object p6, p0, LX/3LL;->f:Ljava/util/concurrent/ExecutorService;

    .line 550339
    iput-object p7, p0, LX/3LL;->g:LX/3LQ;

    .line 550340
    iput-object p8, p0, LX/3LL;->h:LX/0Uh;

    .line 550341
    new-instance v0, LX/3LR;

    invoke-direct {v0}, LX/3LR;-><init>()V

    iput-object v0, p0, LX/3LL;->i:LX/3LR;

    .line 550342
    return-void
.end method

.method public static a(LX/0QB;)LX/3LL;
    .locals 10

    .prologue
    .line 550329
    new-instance v1, LX/3LL;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v3

    check-cast v3, LX/0y3;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/3LM;->b(LX/0QB;)LX/3LM;

    move-result-object v5

    check-cast v5, LX/3LM;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/3LQ;->a(LX/0QB;)LX/3LQ;

    move-result-object v8

    check-cast v8, LX/3LQ;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-direct/range {v1 .. v9}, LX/3LL;-><init>(Landroid/content/Context;LX/0y3;LX/0tX;LX/3LM;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/ExecutorService;LX/3LQ;LX/0Uh;)V

    .line 550330
    move-object v0, v1

    .line 550331
    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 550325
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 550326
    sget-object v1, LX/0ax;->dX:Ljava/lang/String;

    const-string v2, "divebar"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 550327
    iget-object v1, p0, LX/3LL;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/3LL;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 550328
    return-void
.end method

.method public static onClick(LX/3LL;LX/3OS;)V
    .locals 4

    .prologue
    .line 550298
    sget-object v0, LX/JHo;->a:[I

    iget-object v1, p1, LX/3OS;->a:LX/3OT;

    invoke-virtual {v1}, LX/3OT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 550299
    :goto_0
    return-void

    .line 550300
    :pswitch_0
    iget-object v0, p0, LX/3LL;->g:LX/3LQ;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3LQ;->b(Z)V

    .line 550301
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 550302
    sget-object v1, LX/0ax;->ea:Ljava/lang/String;

    const-string v2, "divebar"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const-string p1, "nearby_friends_undecided"

    invoke-static {v1, v2, v3, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 550303
    iget-object v1, p0, LX/3LL;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/3LL;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 550304
    goto :goto_0

    .line 550305
    :pswitch_1
    iget-object v0, p0, LX/3LL;->g:LX/3LQ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3LQ;->b(Z)V

    .line 550306
    invoke-direct {p0}, LX/3LL;->d()V

    goto :goto_0

    .line 550307
    :pswitch_2
    iget-object v0, p0, LX/3LL;->g:LX/3LQ;

    .line 550308
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "background_location_location_disabled_miniphone_selected"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "background_location"

    .line 550309
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 550310
    move-object v1, v1

    .line 550311
    iget-object v2, v0, LX/3LQ;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 550312
    invoke-direct {p0}, LX/3LL;->d()V

    goto :goto_0

    .line 550313
    :pswitch_3
    iget-object v0, p0, LX/3LL;->g:LX/3LQ;

    .line 550314
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "background_location_chat_context_disabled_miniphone_selected"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "background_location"

    .line 550315
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 550316
    move-object v1, v1

    .line 550317
    iget-object v2, v0, LX/3LQ;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 550318
    invoke-direct {p0}, LX/3LL;->d()V

    goto :goto_0

    .line 550319
    :pswitch_4
    iget-object v0, p0, LX/3LL;->g:LX/3LQ;

    .line 550320
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "background_location_miniphone_selected"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "background_location"

    .line 550321
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 550322
    move-object v1, v1

    .line 550323
    iget-object v2, v0, LX/3LQ;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 550324
    invoke-direct {p0}, LX/3LL;->d()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/3OU;
    .locals 3

    .prologue
    .line 550292
    iget-object v0, p0, LX/3LL;->h:LX/0Uh;

    const/16 v1, 0x2eb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3LL;->b:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->b()LX/1rv;

    move-result-object v0

    .line 550293
    iget-object v1, v0, LX/1rv;->a:LX/0yG;

    invoke-static {v1}, LX/1rv;->a(LX/0yG;)Z

    move-result v1

    move v0, v1

    .line 550294
    if-eqz v0, :cond_0

    .line 550295
    new-instance v0, LX/3OS;

    sget-object v1, LX/3OT;->LOADING:LX/3OT;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/3OS;-><init>(LX/3OT;LX/0Px;)V

    move-object v0, v0

    .line 550296
    new-instance v1, LX/3OU;

    new-instance v2, LX/3OV;

    invoke-direct {v2, p0, v0}, LX/3OV;-><init>(LX/3LL;LX/3OS;)V

    invoke-direct {v1, v0, v2}, LX/3OU;-><init>(LX/3OS;Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    .line 550297
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
