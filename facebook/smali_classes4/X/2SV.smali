.class public LX/2SV;
.super LX/2SP;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/2SV;


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2SP;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0ad;

.field private c:I

.field private d:LX/2SY;

.field private e:LX/Fho;

.field private final f:LX/2SR;

.field public g:LX/2SR;


# direct methods
.method public constructor <init>(LX/2SW;LX/2SY;LX/0Or;LX/0Or;LX/0ad;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2SW;",
            "LX/2SY;",
            "LX/0Or",
            "<",
            "LX/2Sl;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Fho;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 412446
    invoke-direct {p0}, LX/2SP;-><init>()V

    .line 412447
    const/16 v0, 0xf

    iput v0, p0, LX/2SV;->c:I

    .line 412448
    new-instance v0, LX/2Sk;

    invoke-direct {v0, p0}, LX/2Sk;-><init>(LX/2SV;)V

    iput-object v0, p0, LX/2SV;->f:LX/2SR;

    .line 412449
    iput-object p5, p0, LX/2SV;->b:LX/0ad;

    .line 412450
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 412451
    invoke-virtual {v1, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 412452
    iput-object p2, p0, LX/2SV;->d:LX/2SY;

    .line 412453
    invoke-virtual {v1, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 412454
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sl;

    .line 412455
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 412456
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 412457
    :cond_0
    :goto_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/2SV;->a:LX/0Px;

    .line 412458
    return-void

    .line 412459
    :cond_1
    invoke-virtual {p2}, LX/2SY;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 412460
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fho;

    iput-object v0, p0, LX/2SV;->e:LX/Fho;

    .line 412461
    iget-object v0, p0, LX/2SV;->e:LX/Fho;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2SV;
    .locals 9

    .prologue
    .line 412517
    sget-object v0, LX/2SV;->h:LX/2SV;

    if-nez v0, :cond_1

    .line 412518
    const-class v1, LX/2SV;

    monitor-enter v1

    .line 412519
    :try_start_0
    sget-object v0, LX/2SV;->h:LX/2SV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 412520
    if-eqz v2, :cond_0

    .line 412521
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 412522
    new-instance v3, LX/2SV;

    invoke-static {v0}, LX/2SW;->a(LX/0QB;)LX/2SW;

    move-result-object v4

    check-cast v4, LX/2SW;

    invoke-static {v0}, LX/2SY;->a(LX/0QB;)LX/2SY;

    move-result-object v5

    check-cast v5, LX/2SY;

    const/16 v6, 0x11b0

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x34ca

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/2SV;-><init>(LX/2SW;LX/2SY;LX/0Or;LX/0Or;LX/0ad;)V

    .line 412523
    move-object v0, v3

    .line 412524
    sput-object v0, LX/2SV;->h:LX/2SV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412525
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 412526
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 412527
    :cond_1
    sget-object v0, LX/2SV;->h:LX/2SV;

    return-object v0

    .line 412528
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 412529
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/2SP;)V
    .locals 2

    .prologue
    const/4 v0, 0x3

    .line 412503
    instance-of v1, p1, LX/2SY;

    if-eqz v1, :cond_3

    .line 412504
    check-cast p1, LX/2SY;

    .line 412505
    iget-boolean v1, p1, LX/2SY;->o:Z

    move v1, v1

    .line 412506
    if-eqz v1, :cond_1

    move v1, v0

    .line 412507
    :goto_0
    rsub-int/lit8 v1, v1, 0xf

    .line 412508
    if-lez v1, :cond_2

    if-ge v1, v0, :cond_2

    :goto_1
    iput v0, p0, LX/2SV;->c:I

    .line 412509
    :cond_0
    :goto_2
    return-void

    .line 412510
    :cond_1
    iget v1, p1, LX/2SY;->q:I

    move v1, v1

    .line 412511
    goto :goto_0

    :cond_2
    move v0, v1

    .line 412512
    goto :goto_1

    .line 412513
    :cond_3
    instance-of v0, p1, LX/2Sl;

    if-eqz v0, :cond_0

    .line 412514
    check-cast p1, LX/2Sl;

    iget v0, p0, LX/2SV;->c:I

    .line 412515
    iput v0, p1, LX/2Sl;->n:I

    .line 412516
    goto :goto_2
.end method


# virtual methods
.method public final a()LX/7BE;
    .locals 1

    .prologue
    .line 412502
    iget-object v0, p0, LX/2SV;->a:LX/0Px;

    invoke-static {v0}, LX/2SP;->a(LX/0Px;)LX/7BE;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2SR;LX/2Sp;)V
    .locals 4

    .prologue
    .line 412496
    iput-object p1, p0, LX/2SV;->g:LX/2SR;

    .line 412497
    iget-object v0, p0, LX/2SV;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, LX/2SV;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412498
    if-eqz p1, :cond_0

    iget-object v1, p0, LX/2SV;->f:LX/2SR;

    :goto_1
    invoke-virtual {v0, v1, p2}, LX/2SP;->a(LX/2SR;LX/2Sp;)V

    .line 412499
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 412500
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 412501
    :cond_1
    return-void
.end method

.method public final a(LX/Cwb;)V
    .locals 3

    .prologue
    .line 412530
    iget-object v0, p0, LX/2SV;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/2SV;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412531
    invoke-virtual {v0, p1}, LX/2SP;->a(LX/Cwb;)V

    .line 412532
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 412533
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 5
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 412491
    iget-object v0, p0, LX/2SV;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, LX/2SV;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412492
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, LX/EPu;->MEMORY:LX/EPu;

    if-ne p2, v3, :cond_0

    sget-object v3, LX/7BE;->READY:LX/7BE;

    invoke-virtual {v0}, LX/2SP;->a()LX/7BE;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/7BE;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 412493
    :cond_0
    invoke-virtual {v0, p1, p2}, LX/2SP;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V

    .line 412494
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 412495
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 3

    .prologue
    .line 412487
    iget-object v0, p0, LX/2SV;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/2SV;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412488
    invoke-virtual {v0, p1}, LX/2SP;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 412489
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 412490
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 412486
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 412482
    iget-object v0, p0, LX/2SV;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/2SV;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412483
    invoke-virtual {v0}, LX/2SP;->c()V

    .line 412484
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 412485
    :cond_0
    return-void
.end method

.method public final get()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 412462
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 412463
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 412464
    iget-object v0, p0, LX/2SV;->b:LX/0ad;

    sget-short v3, LX/100;->cf:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 412465
    new-instance v0, Lcom/facebook/search/model/GapTypeaheadUnit;

    invoke-direct {v0}, Lcom/facebook/search/model/GapTypeaheadUnit;-><init>()V

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 412466
    :cond_0
    iget-object v0, p0, LX/2SV;->d:LX/2SY;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/2SV;->e:LX/Fho;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/2SV;->e:LX/Fho;

    .line 412467
    iget-boolean v3, v0, LX/Fho;->n:Z

    move v0, v3

    .line 412468
    if-eqz v0, :cond_5

    move v0, v1

    .line 412469
    :goto_0
    iget-object v3, p0, LX/2SV;->d:LX/2SY;

    if-eqz v3, :cond_6

    iget-object v3, p0, LX/2SV;->b:LX/0ad;

    sget-short v5, LX/100;->bE:S

    invoke-interface {v3, v5, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v1

    .line 412470
    :goto_1
    if-nez v0, :cond_1

    if-eqz v3, :cond_2

    .line 412471
    :cond_1
    iget-object v0, p0, LX/2SV;->d:LX/2SY;

    .line 412472
    iput-boolean v1, v0, LX/2SY;->o:Z

    .line 412473
    :cond_2
    iget-object v0, p0, LX/2SV;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    :goto_2
    if-ge v2, v1, :cond_7

    iget-object v0, p0, LX/2SV;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412474
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, LX/7BE;->NOT_READY:LX/7BE;

    invoke-virtual {v0}, LX/2SP;->a()LX/7BE;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/7BE;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 412475
    :cond_3
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 412476
    invoke-direct {p0, v0}, LX/2SV;->a(LX/2SP;)V

    .line 412477
    invoke-virtual {v0}, LX/2SP;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {v4, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 412478
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    move v0, v2

    .line 412479
    goto :goto_0

    :cond_6
    move v3, v2

    .line 412480
    goto :goto_1

    .line 412481
    :cond_7
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
