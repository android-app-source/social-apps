.class public LX/2Og;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final l:Ljava/lang/Object;


# instance fields
.field public final b:LX/2Oh;

.field public final c:LX/2OQ;

.field public final d:LX/2OQ;

.field public final e:LX/2Om;

.field public final f:LX/2OQ;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ou;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/2Oq;

.field public final i:LX/26j;

.field private final j:LX/2Oj;

.field public final k:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/DdC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 402868
    const-class v0, LX/2Og;

    sput-object v0, LX/2Og;->a:Ljava/lang/Class;

    .line 402869
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2Og;->l:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2Oh;LX/2OQ;LX/2OQ;LX/2Om;LX/2OQ;LX/2Oq;LX/26j;LX/0Ot;LX/2Oj;)V
    .locals 1
    .param p2    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .param p3    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
        .end annotation
    .end param
    .param p5    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Oh;",
            "LX/2OQ;",
            "LX/2OQ;",
            "LX/2Om;",
            "LX/2OQ;",
            "LX/2Oq;",
            "LX/26j;",
            "LX/0Ot",
            "<",
            "LX/2Ou;",
            ">;",
            "LX/2Oj;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 402904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402905
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2Og;->k:Ljava/util/LinkedList;

    .line 402906
    iput-object p1, p0, LX/2Og;->b:LX/2Oh;

    .line 402907
    iput-object p2, p0, LX/2Og;->c:LX/2OQ;

    .line 402908
    iput-object p3, p0, LX/2Og;->d:LX/2OQ;

    .line 402909
    iput-object p5, p0, LX/2Og;->f:LX/2OQ;

    .line 402910
    iput-object p6, p0, LX/2Og;->h:LX/2Oq;

    .line 402911
    iput-object p7, p0, LX/2Og;->i:LX/26j;

    .line 402912
    iput-object p8, p0, LX/2Og;->g:LX/0Ot;

    .line 402913
    iput-object p9, p0, LX/2Og;->j:LX/2Oj;

    .line 402914
    iput-object p4, p0, LX/2Og;->e:LX/2Om;

    .line 402915
    return-void
.end method

.method public static a(LX/0QB;)LX/2Og;
    .locals 7

    .prologue
    .line 402877
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 402878
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 402879
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 402880
    if-nez v1, :cond_0

    .line 402881
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402882
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 402883
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 402884
    sget-object v1, LX/2Og;->l:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 402885
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 402886
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 402887
    :cond_1
    if-nez v1, :cond_4

    .line 402888
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 402889
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 402890
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/2Og;->b(LX/0QB;)LX/2Og;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 402891
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 402892
    if-nez v1, :cond_2

    .line 402893
    sget-object v0, LX/2Og;->l:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Og;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 402894
    :goto_1
    if-eqz v0, :cond_3

    .line 402895
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 402896
    :goto_3
    check-cast v0, LX/2Og;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 402897
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 402898
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 402899
    :catchall_1
    move-exception v0

    .line 402900
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 402901
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 402902
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 402903
    :cond_2
    :try_start_8
    sget-object v0, LX/2Og;->l:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Og;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static b(LX/2Og;LX/6en;)LX/2OQ;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 402870
    sget-object v1, LX/DdD;->b:[I

    invoke-virtual {p1}, LX/6en;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 402871
    :cond_0
    :goto_0
    return-object v0

    .line 402872
    :pswitch_0
    invoke-static {p0}, LX/2Og;->d(LX/2Og;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 402873
    iget-object v0, p0, LX/2Og;->d:LX/2OQ;

    goto :goto_0

    .line 402874
    :pswitch_1
    iget-object v0, p0, LX/2Og;->c:LX/2OQ;

    goto :goto_0

    .line 402875
    :pswitch_2
    iget-object v1, p0, LX/2Og;->i:LX/26j;

    invoke-virtual {v1}, LX/26j;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 402876
    iget-object v0, p0, LX/2Og;->f:LX/2OQ;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static b(LX/0QB;)LX/2Og;
    .locals 10

    .prologue
    .line 402866
    new-instance v0, LX/2Og;

    invoke-static {p0}, LX/2Oh;->a(LX/0QB;)LX/2Oh;

    move-result-object v1

    check-cast v1, LX/2Oh;

    invoke-static {p0}, LX/2Ok;->a(LX/0QB;)LX/2OQ;

    move-result-object v2

    check-cast v2, LX/2OQ;

    invoke-static {p0}, LX/2Ol;->a(LX/0QB;)LX/2OQ;

    move-result-object v3

    check-cast v3, LX/2OQ;

    invoke-static {p0}, LX/2Om;->a(LX/0QB;)LX/2Om;

    move-result-object v4

    check-cast v4, LX/2Om;

    invoke-static {p0}, LX/2OM;->a(LX/0QB;)LX/2OQ;

    move-result-object v5

    check-cast v5, LX/2OQ;

    invoke-static {p0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v6

    check-cast v6, LX/2Oq;

    invoke-static {p0}, LX/26j;->b(LX/0QB;)LX/26j;

    move-result-object v7

    check-cast v7, LX/26j;

    const/16 v8, 0xcee

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/2Oj;->a(LX/0QB;)LX/2Oj;

    move-result-object v9

    check-cast v9, LX/2Oj;

    invoke-direct/range {v0 .. v9}, LX/2Og;-><init>(LX/2Oh;LX/2OQ;LX/2OQ;LX/2Om;LX/2OQ;LX/2Oq;LX/26j;LX/0Ot;LX/2Oj;)V

    .line 402867
    return-object v0
.end method

.method public static c(LX/2Og;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/ParticipantInfo;)Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 402856
    invoke-virtual {p0, p1}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 402857
    if-nez v1, :cond_0

    .line 402858
    const/4 v0, 0x0

    .line 402859
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2Og;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ou;

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    const/4 p0, 0x0

    .line 402860
    iget-object v3, v0, LX/2Ou;->e:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_2

    move-object v3, p0

    .line 402861
    :cond_1
    :goto_1
    move-object v0, v3

    .line 402862
    goto :goto_0

    .line 402863
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    .line 402864
    iget-object p1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iget-object p1, p1, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    iget-object p2, v0, LX/2Ou;->a:LX/0lB;

    invoke-virtual {p1, v3, p2}, Lcom/facebook/messaging/model/threads/NicknamesMap;->a(Ljava/lang/String;LX/0lC;)Ljava/lang/String;

    move-result-object v3

    .line 402865
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    move-object v3, p0

    goto :goto_1
.end method

.method public static d(LX/2Og;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/2OQ;
    .locals 3

    .prologue
    .line 402850
    sget-object v0, LX/DdD;->a:[I

    iget-object v1, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v1}, LX/5e9;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 402851
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Thread Key with unexpected type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402852
    :pswitch_0
    iget-object v0, p0, LX/2Og;->c:LX/2OQ;

    .line 402853
    :goto_0
    return-object v0

    .line 402854
    :pswitch_1
    iget-object v0, p0, LX/2Og;->d:LX/2OQ;

    goto :goto_0

    .line 402855
    :pswitch_2
    iget-object v0, p0, LX/2Og;->f:LX/2OQ;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static d(LX/2Og;)Z
    .locals 1

    .prologue
    .line 402916
    iget-object v0, p0, LX/2Og;->h:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402849
    iget-object v0, p0, LX/2Og;->c:LX/2OQ;

    invoke-virtual {v0}, LX/2OQ;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 1
    .param p1    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 402848
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, LX/2Og;->d(LX/2Og;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/2OQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 402845
    iget-object v0, p0, LX/2Og;->c:LX/2OQ;

    .line 402846
    invoke-static {v0, p1}, LX/2OQ;->d(LX/2OQ;Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object p0

    move-object v0, p0

    .line 402847
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/ParticipantInfo;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 402831
    invoke-static {p0, p1, p2}, LX/2Og;->c(LX/2Og;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/ParticipantInfo;)Ljava/lang/String;

    move-result-object v0

    .line 402832
    if-eqz v0, :cond_0

    .line 402833
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2Og;->b:LX/2Oh;

    .line 402834
    iget-object p0, p2, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    if-eqz p0, :cond_3

    .line 402835
    iget-object p0, v0, LX/2Oh;->a:LX/2Oi;

    iget-object p1, p2, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {p0, p1}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object p0

    .line 402836
    if-eqz p0, :cond_2

    .line 402837
    iget-object p1, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object p0, p1

    .line 402838
    invoke-virtual {p0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object p0

    .line 402839
    :goto_1
    if-nez p0, :cond_1

    .line 402840
    iget-object p0, p2, Lcom/facebook/messaging/model/messages/ParticipantInfo;->d:Ljava/lang/String;

    .line 402841
    :cond_1
    move-object v0, p0

    .line 402842
    goto :goto_0

    .line 402843
    :cond_2
    iget-object p0, p2, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    goto :goto_1

    .line 402844
    :cond_3
    iget-object p0, p2, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(LX/6en;)V
    .locals 1

    .prologue
    .line 402823
    invoke-static {p0, p1}, LX/2Og;->b(LX/2Og;LX/6en;)LX/2OQ;

    move-result-object v0

    .line 402824
    if-eqz v0, :cond_0

    .line 402825
    invoke-virtual {v0}, LX/2OQ;->a()V

    .line 402826
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/messaging/service/model/MarkThreadFields;)V
    .locals 1

    .prologue
    .line 402829
    iget-object v0, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v0}, LX/2Og;->d(LX/2Og;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/2OQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2OQ;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)V

    .line 402830
    return-void
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 402828
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0, p1}, LX/2Og;->d(LX/2Og;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/2OQ;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 402827
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, LX/2Og;->d(LX/2Og;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/2OQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2OQ;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v0

    goto :goto_0
.end method
