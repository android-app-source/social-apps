.class public final LX/2ya;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 482115
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/Map;)V

    .line 482116
    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 482117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482118
    iput-object p1, p0, LX/2ya;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 482119
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 482120
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, LX/2ya;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 482121
    iput-object p2, p0, LX/2ya;->c:Ljava/util/Map;

    .line 482122
    return-void
.end method
