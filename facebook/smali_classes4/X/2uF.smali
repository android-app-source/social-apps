.class public abstract LX/2uF;
.super LX/3Sa;
.source ""

# interfaces
.implements LX/3Sb;
.implements Ljava/util/RandomAccess;


# static fields
.field private static final a:LX/2uF;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 475590
    new-instance v0, LX/39Q;

    sget-object v1, LX/3Sc;->a:LX/3Sd;

    invoke-direct {v0, v1}, LX/39Q;-><init>(LX/3Sd;)V

    sput-object v0, LX/2uF;->a:LX/2uF;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 475591
    invoke-direct {p0}, LX/3Sa;-><init>()V

    .line 475592
    return-void
.end method

.method public static a(LX/2sN;)LX/2uF;
    .locals 4

    .prologue
    .line 475593
    invoke-interface {p0}, LX/2sN;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 475594
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    .line 475595
    :goto_0
    return-object v0

    .line 475596
    :cond_0
    invoke-interface {p0}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    .line 475597
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 475598
    iget v2, v0, LX/1vs;->b:I

    .line 475599
    iget v0, v0, LX/1vs;->c:I

    .line 475600
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 475601
    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 475602
    invoke-interface {p0}, LX/2sN;->a()Z

    move-result v3

    if-nez v3, :cond_1

    .line 475603
    invoke-static {v1, v2, v0}, LX/2uF;->b(LX/15i;II)LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 475604
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 475605
    :cond_1
    new-instance v3, LX/3Si;

    invoke-direct {v3}, LX/3Si;-><init>()V

    invoke-virtual {v3, v1, v2, v0}, LX/3Si;->c(LX/15i;II)LX/3Si;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/3Si;->b(LX/2sN;)LX/3Si;

    move-result-object v0

    invoke-virtual {v0}, LX/3Si;->a()LX/2uF;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/15i;II)LX/2uF;
    .locals 1

    .prologue
    .line 475606
    new-instance v0, LX/4A7;

    invoke-direct {v0, p0, p1, p2}, LX/4A7;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public static b(LX/39P;)LX/2uF;
    .locals 2

    .prologue
    .line 475607
    instance-of v0, p0, LX/3Sp;

    if-eqz v0, :cond_1

    .line 475608
    check-cast p0, LX/3Sa;

    invoke-virtual {p0}, LX/3Sa;->f()LX/2uF;

    move-result-object v0

    .line 475609
    invoke-virtual {v0}, LX/3Sa;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/39O;->d()LX/3Sd;

    move-result-object v0

    invoke-static {v0}, LX/2uF;->b(LX/3Sd;)LX/2uF;

    move-result-object v0

    .line 475610
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0}, LX/39P;->d()LX/3Sd;

    move-result-object v0

    .line 475611
    iget v1, v0, LX/3Sd;->b:I

    invoke-static {v0, v1}, LX/3Sc;->b(LX/3Sd;I)LX/3Sd;

    move-result-object v1

    move-object v1, v1

    .line 475612
    invoke-static {v1}, LX/2uF;->b(LX/3Sd;)LX/2uF;

    move-result-object v1

    move-object v0, v1

    .line 475613
    goto :goto_0
.end method

.method public static b(LX/3Sd;)LX/2uF;
    .locals 1

    .prologue
    .line 475614
    iget v0, p0, LX/3Sd;->b:I

    invoke-static {p0, v0}, LX/2uF;->b(LX/3Sd;I)LX/2uF;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/3Sd;I)LX/2uF;
    .locals 5

    .prologue
    .line 475615
    packed-switch p1, :pswitch_data_0

    .line 475616
    iget v0, p0, LX/3Sd;->b:I

    if-ge p1, v0, :cond_0

    .line 475617
    invoke-static {p0, p1}, LX/3Sc;->a(LX/3Sd;I)LX/3Sd;

    move-result-object p0

    .line 475618
    :cond_0
    new-instance v0, LX/39Q;

    invoke-direct {v0, p0}, LX/39Q;-><init>(LX/3Sd;)V

    :goto_0
    return-object v0

    .line 475619
    :pswitch_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 475620
    :pswitch_1
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 475621
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, LX/3Sd;->b(I)LX/15i;

    move-result-object v2

    .line 475622
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3Sd;->c(I)I

    move-result v3

    .line 475623
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3Sd;->d(I)I

    move-result v4

    .line 475624
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 475625
    new-instance v0, LX/4A7;

    invoke-direct {v0, v2, v3, v4}, LX/4A7;-><init>(LX/15i;II)V

    goto :goto_0

    .line 475626
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static h()LX/2uF;
    .locals 1

    .prologue
    .line 475627
    sget-object v0, LX/2uF;->a:LX/2uF;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public static j()LX/3Si;
    .locals 1

    .prologue
    .line 475589
    new-instance v0, LX/3Si;

    invoke-direct {v0}, LX/3Si;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(LX/3Sd;I)I
    .locals 7

    .prologue
    .line 475628
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v1

    .line 475629
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 475630
    invoke-virtual {p0, v0}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    .line 475631
    iget-object v3, v2, LX/1vs;->a:LX/15i;

    .line 475632
    iget v4, v2, LX/1vs;->b:I

    .line 475633
    iget v2, v2, LX/1vs;->c:I

    .line 475634
    add-int v5, p2, v0

    .line 475635
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    .line 475636
    :try_start_0
    invoke-virtual {p1, v5, v3}, LX/3Sd;->a(ILX/15i;)LX/15i;

    .line 475637
    invoke-virtual {p1, v5, v4}, LX/3Sd;->a(II)I

    .line 475638
    invoke-virtual {p1, v5, v2}, LX/3Sd;->b(II)I

    .line 475639
    monitor-exit v6

    .line 475640
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 475641
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 475642
    :cond_0
    add-int v0, p2, v1

    return v0
.end method

.method public a(II)LX/2uF;
    .locals 3

    .prologue
    .line 475579
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v0

    invoke-static {p1, p2, v0}, LX/0PB;->checkPositionIndexes(III)V

    .line 475580
    sub-int v0, p2, p1

    .line 475581
    packed-switch v0, :pswitch_data_0

    .line 475582
    invoke-virtual {p0, p1, p2}, LX/2uF;->b(II)LX/2uF;

    move-result-object v0

    :goto_0
    return-object v0

    .line 475583
    :pswitch_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 475584
    :pswitch_1
    invoke-virtual {p0, p1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    .line 475585
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 475586
    iget v2, v0, LX/1vs;->b:I

    .line 475587
    iget v0, v0, LX/1vs;->c:I

    .line 475588
    invoke-static {v1, v2, v0}, LX/2uF;->b(LX/15i;II)LX/2uF;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public synthetic b()LX/2sN;
    .locals 1

    .prologue
    .line 475555
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    return-object v0
.end method

.method public b(II)LX/2uF;
    .locals 2

    .prologue
    .line 475578
    new-instance v0, LX/4A4;

    sub-int v1, p2, p1

    invoke-direct {v0, p0, p1, v1}, LX/4A4;-><init>(LX/2uF;II)V

    return-object v0
.end method

.method public b(I)LX/3Sg;
    .locals 2

    .prologue
    .line 475577
    new-instance v0, LX/4A3;

    invoke-virtual {p0}, LX/39O;->c()I

    move-result v1

    invoke-direct {v0, p0, v1, p1}, LX/4A3;-><init>(LX/2uF;II)V

    return-object v0
.end method

.method public e()LX/3Sh;
    .locals 1

    .prologue
    .line 475576
    invoke-virtual {p0}, LX/2uF;->i()LX/3Sg;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 475568
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 475569
    invoke-static {p0}, LX/3Sm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-ne p1, v2, :cond_1

    .line 475570
    :cond_0
    :goto_0
    move v0, v0

    .line 475571
    return v0

    .line 475572
    :cond_1
    instance-of v2, p1, LX/4AK;

    if-nez v2, :cond_2

    move v0, v1

    .line 475573
    goto :goto_0

    .line 475574
    :cond_2
    check-cast p1, LX/3Sb;

    .line 475575
    invoke-interface {p0}, LX/3Sb;->c()I

    move-result v2

    invoke-interface {p1}, LX/3Sb;->c()I

    move-result v3

    if-ne v2, v3, :cond_3

    invoke-interface {p0}, LX/3Sb;->b()LX/2sN;

    move-result-object v2

    invoke-interface {p1}, LX/3Sb;->b()LX/2sN;

    move-result-object v3

    invoke-static {v2, v3}, LX/3Se;->a(LX/2sN;LX/2sN;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()LX/2uF;
    .locals 0

    .prologue
    .line 475567
    return-object p0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 475557
    const/4 v1, 0x1

    .line 475558
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 475559
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 475560
    invoke-virtual {p0, v0}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    .line 475561
    iget-object v4, v3, LX/1vs;->a:LX/15i;

    .line 475562
    iget v5, v3, LX/1vs;->b:I

    .line 475563
    mul-int/lit8 v1, v1, 0x1f

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v3

    mul-int/lit8 v3, v3, 0x1f

    add-int/2addr v3, v5

    add-int/2addr v1, v3

    .line 475564
    xor-int/lit8 v1, v1, -0x1

    xor-int/lit8 v1, v1, -0x1

    .line 475565
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 475566
    :cond_0
    return v1
.end method

.method public final i()LX/3Sg;
    .locals 1

    .prologue
    .line 475556
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/2uF;->b(I)LX/3Sg;

    move-result-object v0

    return-object v0
.end method
