.class public LX/3Eh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Long;

.field private final b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 537955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537956
    iput-object p1, p0, LX/3Eh;->a:Ljava/lang/Long;

    .line 537957
    sget-object v0, LX/1rk;->a:Ljava/util/concurrent/ConcurrentMap;

    iput-object v0, p0, LX/3Eh;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 537958
    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/util/concurrent/ConcurrentMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 537951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537952
    iput-object p1, p0, LX/3Eh;->a:Ljava/lang/Long;

    .line 537953
    iput-object p2, p0, LX/3Eh;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 537954
    return-void
.end method

.method private declared-synchronized a()V
    .locals 2

    .prologue
    .line 537959
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3Eh;->b:Ljava/util/concurrent/ConcurrentMap;

    iget-object v1, p0, LX/3Eh;->a:Ljava/lang/Long;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 537960
    monitor-exit p0

    return-void

    .line 537961
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 0

    .prologue
    .line 537949
    invoke-direct {p0}, LX/3Eh;->a()V

    .line 537950
    return-void
.end method

.method public onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 537947
    invoke-direct {p0}, LX/3Eh;->a()V

    .line 537948
    return-void
.end method

.method public synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 537946
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/3Eh;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
