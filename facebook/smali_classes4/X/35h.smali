.class public LX/35h;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/39U;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/35h",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/39U;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 497499
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 497500
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/35h;->b:LX/0Zi;

    .line 497501
    iput-object p1, p0, LX/35h;->a:LX/0Ot;

    .line 497502
    return-void
.end method

.method public static a(LX/0QB;)LX/35h;
    .locals 4

    .prologue
    .line 497503
    const-class v1, LX/35h;

    monitor-enter v1

    .line 497504
    :try_start_0
    sget-object v0, LX/35h;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 497505
    sput-object v2, LX/35h;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 497506
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497507
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 497508
    new-instance v3, LX/35h;

    const/16 p0, 0x815

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/35h;-><init>(LX/0Ot;)V

    .line 497509
    move-object v0, v3

    .line 497510
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 497511
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/35h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 497512
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 497513
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 497397
    check-cast p2, LX/C0F;

    .line 497398
    iget-object v0, p0, LX/35h;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/39U;

    iget-object v1, p2, LX/C0F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C0F;->b:LX/1Pm;

    .line 497399
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 497400
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 497401
    iget-object p0, v0, LX/39U;->i:LX/1WM;

    invoke-virtual {p0, v1}, LX/1WM;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result p0

    .line 497402
    iget-object p2, v0, LX/39U;->b:LX/1V4;

    invoke-static {v1, p2}, LX/35m;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1V4;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 497403
    iget-object v3, v0, LX/39U;->f:LX/Bzg;

    const/4 p0, 0x0

    .line 497404
    new-instance p2, LX/Bzf;

    invoke-direct {p2, v3}, LX/Bzf;-><init>(LX/Bzg;)V

    .line 497405
    iget-object v0, v3, LX/Bzg;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bze;

    .line 497406
    if-nez v0, :cond_0

    .line 497407
    new-instance v0, LX/Bze;

    invoke-direct {v0, v3}, LX/Bze;-><init>(LX/Bzg;)V

    .line 497408
    :cond_0
    invoke-static {v0, p1, p0, p0, p2}, LX/Bze;->a$redex0(LX/Bze;LX/1De;IILX/Bzf;)V

    .line 497409
    move-object p2, v0

    .line 497410
    move-object p0, p2

    .line 497411
    move-object v3, p0

    .line 497412
    iget-object p0, v3, LX/Bze;->a:LX/Bzf;

    iput-object v1, p0, LX/Bzf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 497413
    iget-object p0, v3, LX/Bze;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 497414
    move-object v3, v3

    .line 497415
    iget-object p0, v3, LX/Bze;->a:LX/Bzf;

    iput-object v2, p0, LX/Bzf;->b:LX/1Pm;

    .line 497416
    iget-object p0, v3, LX/Bze;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 497417
    move-object v3, v3

    .line 497418
    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    .line 497419
    :goto_0
    move-object v0, v3

    .line 497420
    return-object v0

    .line 497421
    :cond_1
    invoke-static {v3}, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 497422
    iget-object p0, v0, LX/39U;->h:LX/C2y;

    const/4 p2, 0x0

    .line 497423
    new-instance v0, LX/C2x;

    invoke-direct {v0, p0}, LX/C2x;-><init>(LX/C2y;)V

    .line 497424
    sget-object v2, LX/C2y;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C2w;

    .line 497425
    if-nez v2, :cond_2

    .line 497426
    new-instance v2, LX/C2w;

    invoke-direct {v2}, LX/C2w;-><init>()V

    .line 497427
    :cond_2
    invoke-static {v2, p1, p2, p2, v0}, LX/C2w;->a$redex0(LX/C2w;LX/1De;IILX/C2x;)V

    .line 497428
    move-object v0, v2

    .line 497429
    move-object p2, v0

    .line 497430
    move-object p0, p2

    .line 497431
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object p2

    .line 497432
    iget-object v0, p0, LX/C2w;->a:LX/C2x;

    iput-object p2, v0, LX/C2x;->a:Ljava/lang/CharSequence;

    .line 497433
    iget-object v0, p0, LX/C2w;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    .line 497434
    move-object p2, p0

    .line 497435
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    if-eqz p0, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    .line 497436
    :goto_1
    iget-object v0, p2, LX/C2w;->a:LX/C2x;

    iput-object p0, v0, LX/C2x;->b:Ljava/lang/CharSequence;

    .line 497437
    iget-object v0, p2, LX/C2w;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    .line 497438
    move-object p0, p2

    .line 497439
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object p2

    .line 497440
    iget-object v0, p0, LX/C2w;->a:LX/C2x;

    iput-object p2, v0, LX/C2x;->c:Ljava/lang/CharSequence;

    .line 497441
    iget-object v0, p0, LX/C2w;->d:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    .line 497442
    move-object p0, p0

    .line 497443
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object p2

    if-eqz p2, :cond_c

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object p2

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_c

    .line 497444
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 497445
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object p2

    .line 497446
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 497447
    :goto_2
    move-object v3, p2

    .line 497448
    iget-object p2, p0, LX/C2w;->a:LX/C2x;

    iput-object v3, p2, LX/C2x;->d:Ljava/lang/CharSequence;

    .line 497449
    iget-object p2, p0, LX/C2w;->d:Ljava/util/BitSet;

    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Ljava/util/BitSet;->set(I)V

    .line 497450
    move-object v3, p0

    .line 497451
    iget-object p0, v3, LX/C2w;->a:LX/C2x;

    iput-object v1, p0, LX/C2x;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 497452
    iget-object p0, v3, LX/C2w;->d:Ljava/util/BitSet;

    const/4 p2, 0x4

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 497453
    move-object v3, v3

    .line 497454
    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    :cond_3
    const-string p0, ""

    goto :goto_1

    .line 497455
    :cond_4
    iget-object v3, v0, LX/39U;->a:LX/2y3;

    iget-object p2, v0, LX/39U;->b:LX/1V4;

    invoke-static {v1, v3, p2}, LX/35j;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2y3;LX/1V4;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 497456
    if-eqz p0, :cond_6

    .line 497457
    iget-object v3, v0, LX/39U;->g:LX/C05;

    const/4 p0, 0x0

    .line 497458
    new-instance p2, LX/C04;

    invoke-direct {p2, v3}, LX/C04;-><init>(LX/C05;)V

    .line 497459
    iget-object v0, v3, LX/C05;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C03;

    .line 497460
    if-nez v0, :cond_5

    .line 497461
    new-instance v0, LX/C03;

    invoke-direct {v0, v3}, LX/C03;-><init>(LX/C05;)V

    .line 497462
    :cond_5
    invoke-static {v0, p1, p0, p0, p2}, LX/C03;->a$redex0(LX/C03;LX/1De;IILX/C04;)V

    .line 497463
    move-object p2, v0

    .line 497464
    move-object p0, p2

    .line 497465
    move-object v3, p0

    .line 497466
    iget-object p0, v3, LX/C03;->a:LX/C04;

    iput-object v1, p0, LX/C04;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 497467
    iget-object p0, v3, LX/C03;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 497468
    move-object v3, v3

    .line 497469
    iget-object p0, v3, LX/C03;->a:LX/C04;

    iput-object v2, p0, LX/C04;->b:LX/1Po;

    .line 497470
    iget-object p0, v3, LX/C03;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 497471
    move-object v3, v3

    .line 497472
    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    .line 497473
    :cond_6
    iget-object v3, v0, LX/39U;->d:LX/BzX;

    invoke-virtual {v3, p1}, LX/BzX;->c(LX/1De;)LX/BzV;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/BzV;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/BzV;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/BzV;->a(LX/1Po;)LX/BzV;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    .line 497474
    :cond_7
    iget-object v3, v0, LX/39U;->c:LX/2sO;

    iget-object p2, v0, LX/39U;->b:LX/1V4;

    invoke-static {v1, v3, p2}, LX/35k;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2sO;LX/1V4;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 497475
    iget-object v3, v0, LX/39U;->e:LX/C0M;

    invoke-virtual {v3, p1}, LX/C0M;->c(LX/1De;)LX/C0K;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/C0K;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C0K;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/C0K;->a(LX/1Po;)LX/C0K;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/C0K;->a(Z)LX/C0K;

    move-result-object v3

    const/4 p0, 0x1

    .line 497476
    iget-object p2, v3, LX/C0K;->a:LX/C0L;

    iput-boolean p0, p2, LX/C0L;->d:Z

    .line 497477
    move-object v3, v3

    .line 497478
    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    .line 497479
    :cond_8
    iget-object v3, v0, LX/39U;->j:LX/35i;

    iget-object p2, v0, LX/39U;->b:LX/1V4;

    invoke-static {v1, v3, p2}, LX/35l;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/35i;LX/1V4;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 497480
    iget-object v3, v0, LX/39U;->k:LX/Bzl;

    const/4 p0, 0x0

    .line 497481
    new-instance p2, LX/Bzk;

    invoke-direct {p2, v3}, LX/Bzk;-><init>(LX/Bzl;)V

    .line 497482
    iget-object v0, v3, LX/Bzl;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bzj;

    .line 497483
    if-nez v0, :cond_9

    .line 497484
    new-instance v0, LX/Bzj;

    invoke-direct {v0, v3}, LX/Bzj;-><init>(LX/Bzl;)V

    .line 497485
    :cond_9
    invoke-static {v0, p1, p0, p0, p2}, LX/Bzj;->a$redex0(LX/Bzj;LX/1De;IILX/Bzk;)V

    .line 497486
    move-object p2, v0

    .line 497487
    move-object p0, p2

    .line 497488
    move-object v3, p0

    .line 497489
    iget-object p0, v3, LX/Bzj;->a:LX/Bzk;

    iput-object v1, p0, LX/Bzk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 497490
    iget-object p0, v3, LX/Bzj;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 497491
    move-object v3, v3

    .line 497492
    iget-object p0, v3, LX/Bzj;->a:LX/Bzk;

    iput-object v2, p0, LX/Bzk;->b:LX/1Pn;

    .line 497493
    iget-object p0, v3, LX/Bzj;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 497494
    move-object v3, v3

    .line 497495
    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    .line 497496
    :cond_a
    iget-object v3, v0, LX/39U;->b:LX/1V4;

    invoke-static {v3}, LX/35k;->a(LX/1V4;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 497497
    iget-object v3, v0, LX/39U;->e:LX/C0M;

    invoke-virtual {v3, p1}, LX/C0M;->c(LX/1De;)LX/C0K;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/C0K;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C0K;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/C0K;->a(LX/1Po;)LX/C0K;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/C0K;->a(Z)LX/C0K;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    .line 497498
    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0814ca

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 497395
    invoke-static {}, LX/1dS;->b()V

    .line 497396
    const/4 v0, 0x0

    return-object v0
.end method
