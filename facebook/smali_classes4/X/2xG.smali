.class public LX/2xG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2wq;


# instance fields
.field public final a:LX/2wm;

.field public b:Z


# direct methods
.method public constructor <init>(LX/2wm;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2xG;->b:Z

    iput-object p1, p0, LX/2xG;->a:LX/2wm;

    return-void
.end method


# virtual methods
.method public final a(LX/2we;)LX/2we;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LX/2wK;",
            "R::",
            "LX/2NW;",
            "T:",
            "LX/2we",
            "<TR;TA;>;>(TT;)TT;"
        }
    .end annotation

    invoke-virtual {p0, p1}, LX/2xG;->b(LX/2we;)LX/2we;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, LX/2xG;->a:LX/2wm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2wm;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    iget-object v0, p0, LX/2xG;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->h:LX/2wY;

    iget-boolean v1, p0, LX/2xG;->b:Z

    invoke-interface {v0, p1, v1}, LX/2wY;->a(IZ)V

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;LX/2vs;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "LX/2vs",
            "<*>;I)V"
        }
    .end annotation

    return-void
.end method

.method public final b(LX/2we;)LX/2we;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LX/2wK;",
            "T:",
            "LX/2we",
            "<+",
            "LX/2NW;",
            "TA;>;>(TT;)TT;"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, LX/2xG;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->g:LX/2wW;

    iget-object v0, v0, LX/2wW;->i:LX/2wd;

    invoke-virtual {v0, p1}, LX/2wd;->a(LX/2we;)V

    iget-object v0, p0, LX/2xG;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->g:LX/2wW;

    iget-object v1, p1, LX/2we;->d:LX/2vo;

    move-object v1, v1

    iget-object v2, v0, LX/2wW;->c:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2wJ;

    const-string v3, "Appropriate Api was not requested."

    invoke-static {v2, v3}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v2

    invoke-interface {v0}, LX/2wJ;->d()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/2xG;->a:LX/2wm;

    iget-object v1, v1, LX/2wm;->b:Ljava/util/Map;

    iget-object v2, p1, LX/2we;->d:LX/2vo;

    move-object v2, v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x11

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p1, v0}, LX/2we;->b(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object p1

    :catch_0
    iget-object v0, p0, LX/2xG;->a:LX/2wm;

    new-instance v1, LX/4uZ;

    invoke-direct {v1, p0, p0}, LX/4uZ;-><init>(LX/2xG;LX/2wq;)V

    invoke-virtual {v0, v1}, LX/2wm;->a(LX/4uY;)V

    goto :goto_0

    :cond_0
    instance-of v1, v0, LX/2xP;

    if-eqz v1, :cond_1

    check-cast v0, LX/2xP;

    iget-object v1, v0, LX/2xP;->d:LX/3KQ;

    move-object v0, v1

    :cond_1
    invoke-virtual {p1, v0}, LX/2we;->a(LX/2wK;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, LX/2xG;->b:Z

    if-eqz v2, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, LX/2xG;->a:LX/2wm;

    iget-object v2, v2, LX/2wm;->g:LX/2wW;

    invoke-virtual {v2}, LX/2wW;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    iput-boolean v0, p0, LX/2xG;->b:Z

    iget-object v0, p0, LX/2xG;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->g:LX/2wW;

    iget-object v0, v0, LX/2wW;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4v2;

    const/4 p0, 0x0

    iput-object p0, v0, LX/4v2;->c:LX/3KH;

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v1, p0, LX/2xG;->a:LX/2wm;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/2wm;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    iget-boolean v0, p0, LX/2xG;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2xG;->b:Z

    iget-object v0, p0, LX/2xG;->a:LX/2wm;

    new-instance v1, LX/4ua;

    invoke-direct {v1, p0, p0}, LX/4ua;-><init>(LX/2xG;LX/2wq;)V

    invoke-virtual {v0, v1}, LX/2wm;->a(LX/4uY;)V

    :cond_0
    return-void
.end method
