.class public final LX/3K6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/27U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/27U",
        "<",
        "Lcom/google/android/gms/location/LocationSettingsResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2wX;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/6ZU;


# direct methods
.method public constructor <init>(LX/6ZU;LX/2wX;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 548351
    iput-object p1, p0, LX/3K6;->c:LX/6ZU;

    iput-object p2, p0, LX/3K6;->a:LX/2wX;

    iput-object p3, p0, LX/3K6;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2NW;)V
    .locals 4

    .prologue
    .line 548352
    check-cast p1, Lcom/google/android/gms/location/LocationSettingsResult;

    .line 548353
    :try_start_0
    iget-object v0, p0, LX/3K6;->a:LX/2wX;

    invoke-virtual {v0}, LX/2wX;->g()V

    .line 548354
    iget-object v0, p0, LX/3K6;->b:Lcom/google/common/util/concurrent/SettableFuture;

    .line 548355
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 548356
    new-instance v1, LX/6ZS;

    invoke-virtual {p1}, Lcom/google/android/gms/location/LocationSettingsResult;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    iget v3, v2, Lcom/google/android/gms/common/api/Status;->i:I

    move v2, v3

    .line 548357
    invoke-static {v2}, LX/6ZT;->fromLocationSettingsResultStatus(I)LX/6ZT;

    move-result-object v2

    invoke-direct {v1, v2, p1}, LX/6ZS;-><init>(LX/6ZT;Lcom/google/android/gms/location/LocationSettingsResult;)V

    move-object v1, v1

    .line 548358
    const v2, 0x5fdd5a22

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 548359
    :goto_0
    return-void

    .line 548360
    :catch_0
    move-exception v0

    .line 548361
    iget-object v1, p0, LX/3K6;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
