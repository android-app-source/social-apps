.class public LX/2CA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/2CA;


# instance fields
.field private final a:LX/0bH;

.field private final b:LX/1AM;

.field public final c:LX/2CB;

.field public final d:LX/29r;

.field private final e:LX/29t;

.field private final f:LX/2CD;

.field private final g:LX/2CE;

.field private final h:LX/29u;


# direct methods
.method public constructor <init>(LX/2CB;LX/29r;LX/0bH;LX/1AM;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 382100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 382101
    iput-object p1, p0, LX/2CA;->c:LX/2CB;

    .line 382102
    iput-object p2, p0, LX/2CA;->d:LX/29r;

    .line 382103
    iput-object p3, p0, LX/2CA;->a:LX/0bH;

    .line 382104
    iput-object p4, p0, LX/2CA;->b:LX/1AM;

    .line 382105
    new-instance v0, LX/29t;

    invoke-direct {v0, p0}, LX/29t;-><init>(LX/2CA;)V

    iput-object v0, p0, LX/2CA;->e:LX/29t;

    .line 382106
    new-instance v0, LX/2CD;

    invoke-direct {v0, p0}, LX/2CD;-><init>(LX/2CA;)V

    iput-object v0, p0, LX/2CA;->f:LX/2CD;

    .line 382107
    new-instance v0, LX/2CE;

    invoke-direct {v0, p0}, LX/2CE;-><init>(LX/2CA;)V

    iput-object v0, p0, LX/2CA;->g:LX/2CE;

    .line 382108
    new-instance v0, LX/29u;

    invoke-direct {v0, p0}, LX/29u;-><init>(LX/2CA;)V

    iput-object v0, p0, LX/2CA;->h:LX/29u;

    .line 382109
    return-void
.end method

.method public static a(LX/0QB;)LX/2CA;
    .locals 7

    .prologue
    .line 382110
    sget-object v0, LX/2CA;->i:LX/2CA;

    if-nez v0, :cond_1

    .line 382111
    const-class v1, LX/2CA;

    monitor-enter v1

    .line 382112
    :try_start_0
    sget-object v0, LX/2CA;->i:LX/2CA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 382113
    if-eqz v2, :cond_0

    .line 382114
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 382115
    new-instance p0, LX/2CA;

    invoke-static {v0}, LX/2CB;->a(LX/0QB;)LX/2CB;

    move-result-object v3

    check-cast v3, LX/2CB;

    invoke-static {v0}, LX/29r;->a(LX/0QB;)LX/29r;

    move-result-object v4

    check-cast v4, LX/29r;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v5

    check-cast v5, LX/0bH;

    invoke-static {v0}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v6

    check-cast v6, LX/1AM;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2CA;-><init>(LX/2CB;LX/29r;LX/0bH;LX/1AM;)V

    .line 382116
    move-object v0, p0

    .line 382117
    sput-object v0, LX/2CA;->i:LX/2CA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382118
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 382119
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 382120
    :cond_1
    sget-object v0, LX/2CA;->i:LX/2CA;

    return-object v0

    .line 382121
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 382122
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 2

    .prologue
    .line 382123
    iget-object v0, p0, LX/2CA;->a:LX/0bH;

    iget-object v1, p0, LX/2CA;->e:LX/29t;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 382124
    iget-object v0, p0, LX/2CA;->a:LX/0bH;

    iget-object v1, p0, LX/2CA;->f:LX/2CD;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 382125
    iget-object v0, p0, LX/2CA;->a:LX/0bH;

    iget-object v1, p0, LX/2CA;->g:LX/2CE;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 382126
    iget-object v0, p0, LX/2CA;->b:LX/1AM;

    iget-object v1, p0, LX/2CA;->h:LX/29u;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 382127
    return-void
.end method
