.class public LX/3QA;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/03V;

.field private final d:LX/2aN;

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Drt;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/3Q0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 563252
    const-class v0, LX/3QA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3QA;->a:Ljava/lang/String;

    .line 563253
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->LA_AUTHENTICATE_APPROVE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->LA_AUTHENTICATE_DENY:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    sput-object v0, LX/3QA;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/2aN;Ljava/util/Set;LX/3Q0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2aN;",
            "Ljava/util/Set",
            "<",
            "LX/Drt;",
            ">;",
            "Lcom/facebook/notifications/lockscreen/util/PushNotificationIntentHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 563254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563255
    iput-object p1, p0, LX/3QA;->c:LX/03V;

    .line 563256
    iput-object p2, p0, LX/3QA;->d:LX/2aN;

    .line 563257
    iput-object p3, p0, LX/3QA;->e:Ljava/util/Set;

    .line 563258
    iput-object p4, p0, LX/3QA;->f:LX/3Q0;

    .line 563259
    return-void
.end method

.method public static a(LX/0QB;)LX/3QA;
    .locals 5

    .prologue
    .line 563260
    new-instance v3, LX/3QA;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/2aN;->b(LX/0QB;)LX/2aN;

    move-result-object v1

    check-cast v1, LX/2aN;

    invoke-static {p0}, LX/3QB;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v4

    invoke-static {p0}, LX/3Q0;->b(LX/0QB;)LX/3Q0;

    move-result-object v2

    check-cast v2, LX/3Q0;

    invoke-direct {v3, v0, v1, v4, v2}, LX/3QA;-><init>(LX/03V;LX/2aN;Ljava/util/Set;LX/3Q0;)V

    .line 563261
    move-object v0, v3

    .line 563262
    return-object v0
.end method

.method private static a(LX/3QA;Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;LX/0am;Lcom/facebook/notifications/model/SystemTrayNotification;ILX/BAa;LX/3B2;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/notifications/model/SystemTrayNotification;",
            "I",
            "LX/BAa;",
            "LX/3B2;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 563263
    iget-object v0, p0, LX/3QA;->f:LX/3Q0;

    const/4 v5, 0x1

    .line 563264
    const/4 v1, 0x0

    .line 563265
    const/4 v2, 0x0

    .line 563266
    sget-object v3, LX/Jbi;->b:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 563267
    :goto_0
    move-object v2, v2

    .line 563268
    sget-object v3, LX/Jbi;->b:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 563269
    :cond_0
    :goto_1
    move-object v2, v1

    .line 563270
    const/4 v0, 0x0

    .line 563271
    if-eqz v2, :cond_3

    .line 563272
    const-string v0, "EXTRA_COMPONENT_TYPE"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, LX/8D4;

    .line 563273
    sget-object v0, LX/3QA;->b:LX/0Rf;

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 563274
    iget-object v0, p0, LX/3QA;->d:LX/2aN;

    invoke-virtual {p3}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v1

    move-object v4, p6

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LX/2aN;->b(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Landroid/content/Intent;LX/8D4;LX/3B2;I)Landroid/app/PendingIntent;

    move-result-object v0

    move-object v1, v0

    .line 563275
    :goto_2
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 563276
    sget-object v2, LX/Jbi;->b:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_2

    .line 563277
    const v2, 0x7f0208ec

    :goto_3
    move v2, v2

    .line 563278
    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 563279
    iget-object v3, p5, LX/BAa;->k:LX/2HB;

    invoke-virtual {v3, v2, v0, v1}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 563280
    const/4 v3, 0x1

    iput-boolean v3, p5, LX/BAa;->h:Z

    .line 563281
    const/4 v0, 0x1

    .line 563282
    :goto_4
    return v0

    .line 563283
    :cond_1
    iget-object v0, p0, LX/3QA;->d:LX/2aN;

    invoke-virtual {p3}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v1

    move-object v4, p6

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LX/2aN;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Landroid/content/Intent;LX/8D4;LX/3B2;I)Landroid/app/PendingIntent;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 563284
    :cond_2
    const/4 v0, 0x0

    goto :goto_4

    :cond_3
    move-object v1, v0

    goto :goto_2

    .line 563285
    :pswitch_0
    invoke-static {v2}, Lcom/facebook/auth/login/LoginApprovalNotificationData;->a(Ljava/lang/String;)Lcom/facebook/auth/login/LoginApprovalNotificationData;

    move-result-object v2

    .line 563286
    if-eqz v2, :cond_0

    .line 563287
    new-instance v1, Landroid/content/Intent;

    iget-object v3, v0, LX/3Q0;->a:Landroid/content/Context;

    const-class v4, Lcom/facebook/auth/login/LoginApprovalNotificationService;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "extra_login_approval_notification_data"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "arg_action"

    const-string v3, "action_approve"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_show_toast"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_COMPONENT_TYPE"

    sget-object v3, LX/8D4;->SERVICE:LX/8D4;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 563288
    :pswitch_1
    iget-object v3, v0, LX/3Q0;->f:LX/3Q1;

    invoke-virtual {v3}, LX/3Q1;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 563289
    invoke-static {v2}, Lcom/facebook/auth/login/LoginApprovalNotificationData;->a(Ljava/lang/String;)Lcom/facebook/auth/login/LoginApprovalNotificationData;

    move-result-object v2

    .line 563290
    if-eqz v2, :cond_0

    .line 563291
    iget-object v1, v0, LX/3Q0;->d:LX/17X;

    iget-object v3, v0, LX/3Q0;->a:Landroid/content/Context;

    sget-object v4, LX/0ax;->dm:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "extra_login_approval_notification_data"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "DIRECT_TO_DENY"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_COMPONENT_TYPE"

    sget-object v3, LX/8D4;->ACTIVITY:LX/8D4;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 563292
    :cond_4
    const/4 v1, 0x0

    .line 563293
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 563294
    iget-object v1, v0, LX/3Q0;->h:LX/17Y;

    iget-object v3, v0, LX/3Q0;->a:Landroid/content/Context;

    sget-object v4, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 563295
    :cond_5
    if-nez v1, :cond_6

    .line 563296
    iget-object v1, v0, LX/3Q0;->h:LX/17Y;

    iget-object v3, v0, LX/3Q0;->a:Landroid/content/Context;

    sget-object v4, LX/0ax;->dd:Ljava/lang/String;

    invoke-interface {v1, v3, v4}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 563297
    :cond_6
    move-object v1, v1

    .line 563298
    const-string v2, "EXTRA_COMPONENT_TYPE"

    sget-object v3, LX/8D4;->ACTIVITY:LX/8D4;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_1

    .line 563299
    :pswitch_2
    invoke-virtual {p3}, Lcom/facebook/notifications/model/SystemTrayNotification;->A()LX/0am;

    move-result-object v2

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto/16 :goto_0

    .line 563300
    :pswitch_3
    iget-object v2, v0, LX/3Q0;->f:LX/3Q1;

    invoke-virtual {v2}, LX/3Q1;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 563301
    invoke-virtual {p3}, Lcom/facebook/notifications/model/SystemTrayNotification;->A()LX/0am;

    move-result-object v2

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto/16 :goto_0

    .line 563302
    :cond_7
    invoke-virtual {p3}, Lcom/facebook/notifications/model/SystemTrayNotification;->x()LX/0am;

    move-result-object v2

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto/16 :goto_0

    .line 563303
    :pswitch_4
    const v2, 0x7f0207d7

    goto/16 :goto_3

    .line 563304
    :pswitch_5
    const v2, 0x7f020819

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static b(LX/3QA;Lcom/facebook/notifications/model/SystemTrayNotification;LX/BAa;Landroid/content/Intent;I)V
    .locals 9

    .prologue
    .line 563305
    :try_start_0
    new-instance v7, Lorg/json/JSONArray;

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->v()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v7, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 563306
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v6, v0, :cond_2

    .line 563307
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 563308
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_TYPE:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    move-result-object v1

    .line 563309
    iget-object v0, p0, LX/3QA;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drt;

    .line 563310
    invoke-interface {v0, v1}, LX/Drt;->a(Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;)LX/Drs;

    move-result-object v0

    .line 563311
    if-eqz v0, :cond_0

    .line 563312
    :goto_1
    move-object v8, v0

    .line 563313
    if-eqz v8, :cond_1

    .line 563314
    new-instance v5, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->i()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v1, v6, v0, v2}, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;-><init>(Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;ILjava/lang/String;Ljava/lang/String;)V

    .line 563315
    new-instance v0, LX/Drw;

    move-object v1, p1

    move-object v2, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, LX/Drw;-><init>(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Intent;Lorg/json/JSONObject;ILcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)V

    .line 563316
    invoke-interface {v8, v0}, LX/Drs;->a(LX/Drw;)LX/3pb;

    move-result-object v0

    .line 563317
    iget-object v1, p2, LX/BAa;->k:LX/2HB;

    invoke-virtual {v1, v0}, LX/2HB;->a(LX/3pb;)LX/2HB;

    .line 563318
    const/4 v1, 0x1

    iput-boolean v1, p2, LX/BAa;->h:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 563319
    :cond_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 563320
    :catch_0
    move-exception v0

    .line 563321
    iget-object v1, p0, LX/3QA;->c:LX/03V;

    sget-object v2, LX/3QA;->a:Ljava/lang/String;

    const-string v3, "Error parsing JSON from push notification action v2"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 563322
    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/notifications/model/SystemTrayNotification;LX/BAa;Landroid/content/Intent;I)V
    .locals 7

    .prologue
    .line 563323
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->v()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 563324
    invoke-static {p0, p1, p2, p3, p4}, LX/3QA;->b(LX/3QA;Lcom/facebook/notifications/model/SystemTrayNotification;LX/BAa;Landroid/content/Intent;I)V

    .line 563325
    :cond_0
    :goto_0
    return-void

    .line 563326
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 563327
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->u()Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->z()LX/0am;

    move-result-object v2

    sget-object v6, LX/3B2;->click_secondary_action:LX/3B2;

    move-object v0, p0

    move-object v3, p1

    move v4, p4

    move-object v5, p2

    invoke-static/range {v0 .. v6}, LX/3QA;->a(LX/3QA;Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;LX/0am;Lcom/facebook/notifications/model/SystemTrayNotification;ILX/BAa;LX/3B2;)Z

    .line 563328
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563329
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->t()Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->y()LX/0am;

    move-result-object v2

    sget-object v6, LX/3B2;->click_primary_action:LX/3B2;

    move-object v0, p0

    move-object v3, p1

    move v4, p4

    move-object v5, p2

    invoke-static/range {v0 .. v6}, LX/3QA;->a(LX/3QA;Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;LX/0am;Lcom/facebook/notifications/model/SystemTrayNotification;ILX/BAa;LX/3B2;)Z

    goto :goto_0
.end method
