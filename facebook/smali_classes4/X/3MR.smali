.class public final LX/3MR;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field private static final k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private static final l:LX/0sv;

.field private static final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    .line 554587
    new-instance v0, LX/0U1;

    const-string v1, "address"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MR;->a:LX/0U1;

    .line 554588
    new-instance v0, LX/0U1;

    const-string v1, "status"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MR;->b:LX/0U1;

    .line 554589
    new-instance v0, LX/0U1;

    const-string v1, "fb_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MR;->c:LX/0U1;

    .line 554590
    new-instance v0, LX/0U1;

    const-string v1, "match_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MR;->d:LX/0U1;

    .line 554591
    new-instance v0, LX/0U1;

    const-string v1, "first_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MR;->e:LX/0U1;

    .line 554592
    new-instance v0, LX/0U1;

    const-string v1, "timestamp"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MR;->f:LX/0U1;

    .line 554593
    new-instance v0, LX/0U1;

    const-string v1, "relationship"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MR;->g:LX/0U1;

    .line 554594
    new-instance v0, LX/0U1;

    const-string v1, "profile_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MR;->h:LX/0U1;

    .line 554595
    new-instance v0, LX/0U1;

    const-string v1, "match_username"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MR;->i:LX/0U1;

    .line 554596
    new-instance v0, LX/0U1;

    const-string v1, "times_shown"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MR;->j:LX/0U1;

    .line 554597
    sget-object v0, LX/3MR;->a:LX/0U1;

    sget-object v1, LX/3MR;->b:LX/0U1;

    sget-object v2, LX/3MR;->c:LX/0U1;

    sget-object v3, LX/3MR;->d:LX/0U1;

    sget-object v4, LX/3MR;->e:LX/0U1;

    sget-object v5, LX/3MR;->f:LX/0U1;

    sget-object v6, LX/3MR;->g:LX/0U1;

    sget-object v7, LX/3MR;->h:LX/0U1;

    sget-object v8, LX/3MR;->i:LX/0U1;

    sget-object v9, LX/3MR;->j:LX/0U1;

    invoke-static/range {v0 .. v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3MR;->k:LX/0Px;

    .line 554598
    new-instance v0, LX/0su;

    sget-object v1, LX/3MR;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    .line 554599
    sput-object v0, LX/3MR;->l:LX/0sv;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3MR;->m:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 554585
    const-string v0, "match_table"

    sget-object v1, LX/3MR;->k:LX/0Px;

    sget-object v2, LX/3MR;->m:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 554586
    return-void
.end method
