.class public LX/3Lg;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile b:LX/3Lg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 551817
    const-class v0, LX/3Lg;

    sput-object v0, LX/3Lg;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 551818
    const-string v0, "call_summary"

    const/4 v1, 0x4

    new-instance v2, LX/3Lh;

    invoke-direct {v2}, LX/3Lh;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 551819
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;I)I
    .locals 3

    .prologue
    const/4 v0, 0x4

    .line 551820
    add-int/lit8 v1, p2, 0x1

    .line 551821
    if-ne p2, v0, :cond_0

    .line 551822
    :goto_0
    return v0

    .line 551823
    :cond_0
    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    .line 551824
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 551825
    const-string v2, "ALTER TABLE "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551826
    const-string v2, "person_summary"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551827
    const-string v2, " ADD COLUMN "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551828
    sget-object v2, LX/3Li;->k:LX/0U1;

    .line 551829
    iget-object p0, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, p0

    .line 551830
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551831
    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551832
    sget-object v2, LX/3Li;->k:LX/0U1;

    .line 551833
    iget-object p0, v2, LX/0U1;->e:Ljava/lang/String;

    move-object v2, p0

    .line 551834
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551835
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v2, -0x20750f5e

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x19790033

    invoke-static {v0}, LX/03h;->a(I)V

    move v0, v1

    .line 551836
    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 551837
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 551838
    const-string v2, "ALTER TABLE "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551839
    const-string v2, "person_summary"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551840
    const-string v2, " ADD COLUMN "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551841
    sget-object v2, LX/3Li;->j:LX/0U1;

    .line 551842
    iget-object p0, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, p0

    .line 551843
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551844
    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551845
    sget-object v2, LX/3Li;->j:LX/0U1;

    .line 551846
    iget-object p0, v2, LX/0U1;->e:Ljava/lang/String;

    move-object v2, p0

    .line 551847
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551848
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v2, -0x1ad745af

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x6b848d50

    invoke-static {v0}, LX/03h;->a(I)V

    move v0, v1

    .line 551849
    goto/16 :goto_0

    .line 551850
    :cond_2
    const-string v0, "person_summary"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v2, -0x5cd1790b

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x379f8bfe

    invoke-static {v0}, LX/03h;->a(I)V

    .line 551851
    invoke-virtual {p0, p1}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    move v0, v1

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)LX/3Lg;
    .locals 3

    .prologue
    .line 551852
    sget-object v0, LX/3Lg;->b:LX/3Lg;

    if-nez v0, :cond_1

    .line 551853
    const-class v1, LX/3Lg;

    monitor-enter v1

    .line 551854
    :try_start_0
    sget-object v0, LX/3Lg;->b:LX/3Lg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 551855
    if-eqz v2, :cond_0

    .line 551856
    :try_start_1
    new-instance v0, LX/3Lg;

    invoke-direct {v0}, LX/3Lg;-><init>()V

    .line 551857
    move-object v0, v0

    .line 551858
    sput-object v0, LX/3Lg;->b:LX/3Lg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551859
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 551860
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551861
    :cond_1
    sget-object v0, LX/3Lg;->b:LX/3Lg;

    return-object v0

    .line 551862
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 551863
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 551864
    :goto_0
    if-ge p2, p3, :cond_0

    .line 551865
    invoke-direct {p0, p1, p2}, LX/3Lg;->a(Landroid/database/sqlite/SQLiteDatabase;I)I

    move-result p2

    goto :goto_0

    .line 551866
    :cond_0
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 551867
    return-void
.end method
