.class public final LX/2t6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2t5;


# direct methods
.method public constructor <init>(LX/2t5;)V
    .locals 0

    .prologue
    .line 474640
    iput-object p1, p0, LX/2t6;->a:LX/2t5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 474641
    iget-object v0, p0, LX/2t6;->a:LX/2t5;

    iget-object v0, v0, LX/2t5;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/2t5;->a:Ljava/lang/String;

    const-string v2, "Badge count query request failed"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 474642
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 474643
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 474644
    if-eqz p1, :cond_0

    .line 474645
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 474646
    if-nez v0, :cond_1

    .line 474647
    :cond_0
    iget-object v0, p0, LX/2t6;->a:LX/2t5;

    invoke-static {v0}, LX/2t5;->h(LX/2t5;)V

    .line 474648
    :goto_0
    return-void

    .line 474649
    :cond_1
    iget-object v1, p0, LX/2t6;->a:LX/2t5;

    .line 474650
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 474651
    check-cast v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;

    move-result-object v2

    .line 474652
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 474653
    check-cast v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->j()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, LX/2t5;->a$redex0(LX/2t5;Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;Z)V

    goto :goto_0
.end method
