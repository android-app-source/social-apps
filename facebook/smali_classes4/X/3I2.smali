.class public LX/3I2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/3I2;


# instance fields
.field public final a:LX/0ad;

.field public final b:Z

.field public final c:I

.field public final d:I


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 545455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 545456
    iput-object p1, p0, LX/3I2;->a:LX/0ad;

    .line 545457
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-object v1, LX/0c1;->Off:LX/0c1;

    sget-short v2, LX/2mm;->j:S

    const/4 v3, 0x0

    invoke-interface {p1, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/3I2;->b:Z

    .line 545458
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-object v1, LX/0c1;->Off:LX/0c1;

    sget v2, LX/2mm;->l:I

    const/16 v3, 0x2710

    invoke-interface {p1, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    iput v0, p0, LX/3I2;->c:I

    .line 545459
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-object v1, LX/0c1;->Off:LX/0c1;

    sget v2, LX/2mm;->k:I

    const/16 v3, 0x1e

    invoke-interface {p1, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    iput v0, p0, LX/3I2;->d:I

    .line 545460
    return-void
.end method

.method public static a(LX/0QB;)LX/3I2;
    .locals 4

    .prologue
    .line 545461
    sget-object v0, LX/3I2;->e:LX/3I2;

    if-nez v0, :cond_1

    .line 545462
    const-class v1, LX/3I2;

    monitor-enter v1

    .line 545463
    :try_start_0
    sget-object v0, LX/3I2;->e:LX/3I2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 545464
    if-eqz v2, :cond_0

    .line 545465
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 545466
    new-instance p0, LX/3I2;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/3I2;-><init>(LX/0ad;)V

    .line 545467
    move-object v0, p0

    .line 545468
    sput-object v0, LX/3I2;->e:LX/3I2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 545469
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 545470
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 545471
    :cond_1
    sget-object v0, LX/3I2;->e:LX/3I2;

    return-object v0

    .line 545472
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 545473
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
