.class public LX/2tC;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2tC;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 474804
    invoke-direct {p0}, LX/398;-><init>()V

    .line 474805
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 474806
    const-string v1, "target_tab_name"

    sget-object v2, Lcom/facebook/marketplace/tab/MarketplaceTab;->m:Lcom/facebook/marketplace/tab/MarketplaceTab;

    invoke-virtual {v2}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 474807
    sget-object v1, LX/0ax;->hx:Ljava/lang/String;

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->MARKETPLACE_TAB_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 474808
    return-void
.end method

.method public static a(LX/0QB;)LX/2tC;
    .locals 3

    .prologue
    .line 474809
    sget-object v0, LX/2tC;->a:LX/2tC;

    if-nez v0, :cond_1

    .line 474810
    const-class v1, LX/2tC;

    monitor-enter v1

    .line 474811
    :try_start_0
    sget-object v0, LX/2tC;->a:LX/2tC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 474812
    if-eqz v2, :cond_0

    .line 474813
    :try_start_1
    new-instance v0, LX/2tC;

    invoke-direct {v0}, LX/2tC;-><init>()V

    .line 474814
    move-object v0, v0

    .line 474815
    sput-object v0, LX/2tC;->a:LX/2tC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 474816
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 474817
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 474818
    :cond_1
    sget-object v0, LX/2tC;->a:LX/2tC;

    return-object v0

    .line 474819
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 474820
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
