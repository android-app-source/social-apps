.class public final LX/3QC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:LX/1rk;

.field public final synthetic b:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;LX/1rk;)V
    .locals 0

    .prologue
    .line 563376
    iput-object p1, p0, LX/3QC;->b:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iput-object p2, p0, LX/3QC;->a:LX/1rk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 563377
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 563378
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 563379
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 563380
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 563381
    iget-object v1, p0, LX/3QC;->a:LX/1rk;

    .line 563382
    iget-object p0, v1, LX/1rk;->c:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/CSD;

    .line 563383
    if-eqz v0, :cond_0

    .line 563384
    const/4 p1, 0x1

    new-array p1, p1, [Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    aput-object v0, p1, v1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-static {p0, p1}, LX/CSD;->a(LX/CSD;Ljava/util/List;)V

    .line 563385
    :cond_0
    return-void
.end method
