.class public final LX/36f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/8G1;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/8G1;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 499815
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 499816
    iput-object p1, p0, LX/36f;->a:LX/0QB;

    .line 499817
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 499818
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/36f;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 499819
    packed-switch p2, :pswitch_data_0

    .line 499820
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 499821
    :pswitch_0
    new-instance p2, LX/8G2;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {p1}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(LX/0QB;)Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    move-result-object p0

    check-cast p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    invoke-direct {p2, v0, v1, v2, p0}, LX/8G2;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0ad;Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;)V

    .line 499822
    move-object v0, p2

    .line 499823
    :goto_0
    return-object v0

    .line 499824
    :pswitch_1
    new-instance p0, LX/BGX;

    invoke-static {p1}, LX/1kR;->b(LX/0QB;)LX/1kR;

    move-result-object v0

    check-cast v0, LX/1kR;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p1}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v2

    check-cast v2, LX/0fO;

    invoke-direct {p0, v0, v1, v2}, LX/BGX;-><init>(LX/1kR;LX/0tX;LX/0fO;)V

    .line 499825
    move-object v0, p0

    .line 499826
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 499827
    const/4 v0, 0x2

    return v0
.end method
