.class public LX/2WF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:J

.field public final b:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 3

    .prologue
    .line 418632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418633
    cmp-long v0, p1, p3

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "From must be lower than to ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 418634
    iput-wide p1, p0, LX/2WF;->a:J

    .line 418635
    iput-wide p3, p0, LX/2WF;->b:J

    .line 418636
    return-void

    .line 418637
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 418690
    iget-wide v0, p0, LX/2WF;->b:J

    iget-wide v2, p0, LX/2WF;->a:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final a(Ljava/lang/Iterable;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/2WF;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/2WF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418680
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 418681
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2WF;

    .line 418682
    iget-wide v3, p0, LX/2WF;->a:J

    iget-wide v5, v0, LX/2WF;->a:J

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    .line 418683
    iget-wide v3, p0, LX/2WF;->b:J

    iget-wide v7, v0, LX/2WF;->b:J

    invoke-static {v3, v4, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    .line 418684
    cmp-long v3, v5, v7

    if-ltz v3, :cond_2

    .line 418685
    const/4 v3, 0x0

    .line 418686
    :goto_1
    move-object v0, v3

    .line 418687
    if-eqz v0, :cond_0

    .line 418688
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 418689
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_2
    new-instance v3, LX/2WF;

    invoke-direct {v3, v5, v6, v7, v8}, LX/2WF;-><init>(JJ)V

    goto :goto_1
.end method

.method public final a(J)Z
    .locals 3

    .prologue
    .line 418679
    iget-wide v0, p0, LX/2WF;->a:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_0

    iget-wide v0, p0, LX/2WF;->b:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Iterable;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/2WF;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/2WF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418668
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 418669
    invoke-virtual {p0, p1}, LX/2WF;->a(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v2

    .line 418670
    iget-wide v0, p0, LX/2WF;->a:J

    .line 418671
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2WF;

    .line 418672
    iget-wide v6, v0, LX/2WF;->a:J

    cmp-long v1, v2, v6

    if-gez v1, :cond_0

    .line 418673
    new-instance v1, LX/2WF;

    iget-wide v6, v0, LX/2WF;->a:J

    invoke-direct {v1, v2, v3, v6, v7}, LX/2WF;-><init>(JJ)V

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 418674
    :cond_0
    iget-wide v0, v0, LX/2WF;->b:J

    move-wide v2, v0

    .line 418675
    goto :goto_0

    .line 418676
    :cond_1
    iget-wide v0, p0, LX/2WF;->b:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    .line 418677
    new-instance v0, LX/2WF;

    iget-wide v6, p0, LX/2WF;->b:J

    invoke-direct {v0, v2, v3, v6, v7}, LX/2WF;-><init>(JJ)V

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 418678
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Iterable;)LX/0Px;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/2WF;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/2WF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418647
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 418648
    iget-wide v4, p0, LX/2WF;->a:J

    .line 418649
    iget-wide v2, p0, LX/2WF;->b:J

    .line 418650
    const/4 v0, 0x0

    .line 418651
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v1, v0

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2WF;

    .line 418652
    iget-wide v6, v0, LX/2WF;->b:J

    iget-wide v10, p0, LX/2WF;->a:J

    cmp-long v6, v6, v10

    if-gez v6, :cond_0

    const/4 v6, 0x1

    .line 418653
    :goto_1
    iget-wide v10, p0, LX/2WF;->b:J

    iget-wide v12, v0, LX/2WF;->a:J

    cmp-long v7, v10, v12

    if-gez v7, :cond_1

    const/4 v7, 0x1

    .line 418654
    :goto_2
    if-eqz v6, :cond_2

    .line 418655
    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 418656
    :cond_0
    const/4 v6, 0x0

    goto :goto_1

    .line 418657
    :cond_1
    const/4 v7, 0x0

    goto :goto_2

    .line 418658
    :cond_2
    if-nez v7, :cond_3

    .line 418659
    iget-wide v6, v0, LX/2WF;->a:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 418660
    iget-wide v6, v0, LX/2WF;->b:J

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    goto :goto_0

    .line 418661
    :cond_3
    if-nez v1, :cond_4

    .line 418662
    const/4 v1, 0x1

    .line 418663
    new-instance v6, LX/2WF;

    invoke-direct {v6, v4, v5, v2, v3}, LX/2WF;-><init>(JJ)V

    invoke-virtual {v8, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 418664
    :cond_4
    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 418665
    :cond_5
    if-nez v1, :cond_6

    .line 418666
    new-instance v0, LX/2WF;

    invoke-direct {v0, v4, v5, v2, v3}, LX/2WF;-><init>(JJ)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 418667
    :cond_6
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 418640
    if-nez p1, :cond_1

    .line 418641
    :cond_0
    :goto_0
    return v0

    .line 418642
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    .line 418643
    goto :goto_0

    .line 418644
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 418645
    check-cast p1, LX/2WF;

    .line 418646
    iget-wide v2, p0, LX/2WF;->a:J

    iget-wide v4, p1, LX/2WF;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, LX/2WF;->b:J

    iget-wide v4, p1, LX/2WF;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 418639
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, LX/2WF;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, LX/2WF;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 418638
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/2WF;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/2WF;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
