.class public final enum LX/27f;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/27f;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/27f;

.field public static final enum FB4A_LOGIN_FIRST_BOOT_PAGE:LX/27f;

.field public static final enum FB4A_LOGIN_PREFILL_AND_REDIRECT:LX/27f;

.field public static final enum MESSENGER_ASK_SMS_READ_PERMISSION_ABOVE_MM:LX/27f;

.field public static final enum MESSENGER_LOGIN_SHOW_PASSWORD:LX/27f;

.field public static final enum MESSENGER_SHOW_ONE_STEP_PROFILE_WITH_FOOTERS:LX/27f;

.field public static final enum SSO_TARGETED_ERROR_HANDLING:LX/27f;


# instance fields
.field public final endDate:Ljava/util/Date;

.field public final groupCount:I

.field public final groupNames:[Ljava/lang/String;

.field public final groupSize:I

.field public final mConditionalFilter:LX/8DP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final name:Ljava/lang/String;

.field public final startDate:Ljava/util/Date;


# direct methods
.method public static constructor <clinit>()V
    .locals 12

    .prologue
    .line 373027
    new-instance v0, LX/27f;

    const-string v1, "MESSENGER_SHOW_ONE_STEP_PROFILE_WITH_FOOTERS"

    const/4 v2, 0x0

    const-string v3, "messenger_show_one_step_profile_with_footers"

    const/16 v4, 0x9c4

    const/4 v5, 0x4

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "show_footers_only"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "show_one_step_profile_only"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "show_both"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "control"

    aput-object v8, v6, v7

    new-instance v7, Ljava/util/GregorianCalendar;

    const/16 v8, 0x7e0

    const/16 v9, 0x8

    const/4 v10, 0x1

    invoke-direct {v7, v8, v9, v10}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v7}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v7

    new-instance v8, Ljava/util/GregorianCalendar;

    const/16 v9, 0x7e0

    const/16 v10, 0x8

    const/16 v11, 0x1e

    invoke-direct {v8, v9, v10, v11}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v8}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, LX/27f;-><init>(Ljava/lang/String;ILjava/lang/String;II[Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;LX/8DP;)V

    sput-object v0, LX/27f;->MESSENGER_SHOW_ONE_STEP_PROFILE_WITH_FOOTERS:LX/27f;

    .line 373028
    new-instance v0, LX/27f;

    const-string v1, "MESSENGER_ASK_SMS_READ_PERMISSION_ABOVE_MM"

    const/4 v2, 0x1

    const-string v3, "messenger_ask_sms_read_permission_above_mm"

    const/16 v4, 0x1388

    new-instance v5, Ljava/util/GregorianCalendar;

    const/16 v6, 0x7e0

    const/16 v7, 0xa

    const/16 v8, 0x15

    invoke-direct {v5, v6, v7, v8}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v5}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v5

    new-instance v6, Ljava/util/GregorianCalendar;

    const/16 v7, 0x7e0

    const/16 v8, 0xb

    const/16 v9, 0x1f

    invoke-direct {v6, v7, v8, v9}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v6}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/27f;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/util/Date;Ljava/util/Date;LX/8DP;)V

    sput-object v0, LX/27f;->MESSENGER_ASK_SMS_READ_PERMISSION_ABOVE_MM:LX/27f;

    .line 373029
    new-instance v0, LX/27f;

    const-string v1, "MESSENGER_LOGIN_SHOW_PASSWORD"

    const/4 v2, 0x2

    const-string v3, "messenger_login_show_password"

    const/16 v4, 0x3e8

    new-instance v5, Ljava/util/GregorianCalendar;

    const/16 v6, 0x7e0

    const/4 v7, 0x7

    const/16 v8, 0x1d

    invoke-direct {v5, v6, v7, v8}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v5}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v5

    new-instance v6, Ljava/util/GregorianCalendar;

    const/16 v7, 0x7e0

    const/16 v8, 0x8

    const/16 v9, 0x1e

    invoke-direct {v6, v7, v8, v9}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v6}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/27f;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/util/Date;Ljava/util/Date;LX/8DP;)V

    sput-object v0, LX/27f;->MESSENGER_LOGIN_SHOW_PASSWORD:LX/27f;

    .line 373030
    new-instance v0, LX/27f;

    const-string v1, "SSO_TARGETED_ERROR_HANDLING"

    const/4 v2, 0x3

    const-string v3, "sso_targeted_error_handling"

    const/16 v4, 0x1388

    new-instance v5, Ljava/util/GregorianCalendar;

    const/16 v6, 0x7e0

    const/16 v7, 0xa

    const/16 v8, 0x12

    invoke-direct {v5, v6, v7, v8}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v5}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v5

    new-instance v6, Ljava/util/GregorianCalendar;

    const/16 v7, 0x7e1

    const/4 v8, 0x0

    const/16 v9, 0x1f

    invoke-direct {v6, v7, v8, v9}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v6}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LX/27f;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/util/Date;Ljava/util/Date;LX/8DP;)V

    sput-object v0, LX/27f;->SSO_TARGETED_ERROR_HANDLING:LX/27f;

    .line 373031
    new-instance v0, LX/27f;

    const-string v1, "FB4A_LOGIN_PREFILL_AND_REDIRECT"

    const/4 v2, 0x4

    const-string v3, "fb4a_login_prefill_and_redirect"

    const/16 v4, 0x1f4

    const/16 v5, 0x8

    const/16 v6, 0x8

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "tweak_prefill"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "fast_redirect"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "medium_redirect"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "slow_redirect"

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "tweak_prefill_and_fast_redirect"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const-string v8, "tweak_prefill_and_medium_redirect"

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "tweak_prefill_and_slow_redirect"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    const-string v8, "control"

    aput-object v8, v6, v7

    new-instance v7, Ljava/util/GregorianCalendar;

    const/16 v8, 0x7e0

    const/16 v9, 0xa

    const/16 v10, 0x1c

    invoke-direct {v7, v8, v9, v10}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v7}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v7

    new-instance v8, Ljava/util/GregorianCalendar;

    const/16 v9, 0x7e0

    const/16 v10, 0xb

    const/16 v11, 0x17

    invoke-direct {v8, v9, v10, v11}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v8}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, LX/27f;-><init>(Ljava/lang/String;ILjava/lang/String;II[Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;LX/8DP;)V

    sput-object v0, LX/27f;->FB4A_LOGIN_PREFILL_AND_REDIRECT:LX/27f;

    .line 373032
    new-instance v0, LX/27f;

    const-string v1, "FB4A_LOGIN_FIRST_BOOT_PAGE"

    const/4 v2, 0x5

    const-string v3, "fb4a_login_first_boot_page"

    const/16 v4, 0x1f4

    const/4 v5, 0x4

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "normal_button"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "radio_button"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "radio_button_with_default"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "control"

    aput-object v8, v6, v7

    new-instance v7, Ljava/util/GregorianCalendar;

    const/16 v8, 0x7e0

    const/16 v9, 0xb

    const/4 v10, 0x5

    invoke-direct {v7, v8, v9, v10}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v7}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v7

    new-instance v8, Ljava/util/GregorianCalendar;

    const/16 v9, 0x7e1

    const/4 v10, 0x0

    const/16 v11, 0xc

    invoke-direct {v8, v9, v10, v11}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v8}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, LX/27f;-><init>(Ljava/lang/String;ILjava/lang/String;II[Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;LX/8DP;)V

    sput-object v0, LX/27f;->FB4A_LOGIN_FIRST_BOOT_PAGE:LX/27f;

    .line 373033
    const/4 v0, 0x6

    new-array v0, v0, [LX/27f;

    const/4 v1, 0x0

    sget-object v2, LX/27f;->MESSENGER_SHOW_ONE_STEP_PROFILE_WITH_FOOTERS:LX/27f;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/27f;->MESSENGER_ASK_SMS_READ_PERMISSION_ABOVE_MM:LX/27f;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/27f;->MESSENGER_LOGIN_SHOW_PASSWORD:LX/27f;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/27f;->SSO_TARGETED_ERROR_HANDLING:LX/27f;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/27f;->FB4A_LOGIN_PREFILL_AND_REDIRECT:LX/27f;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/27f;->FB4A_LOGIN_FIRST_BOOT_PAGE:LX/27f;

    aput-object v2, v0, v1

    sput-object v0, LX/27f;->$VALUES:[LX/27f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;II[Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;LX/8DP;)V
    .locals 2
    .param p9    # LX/8DP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II[",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "LX/8DP;",
            ")V"
        }
    .end annotation

    .prologue
    .line 373010
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 373011
    const/4 v0, 0x2

    if-ge p5, v0, :cond_0

    .line 373012
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not enough groups in a single experiment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 373013
    :cond_0
    mul-int v0, p5, p4

    const/16 v1, 0x2710

    if-le v0, v1, :cond_1

    .line 373014
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Too many segment allocated in experiment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 373015
    :cond_1
    array-length v0, p6

    if-eq v0, p5, :cond_2

    .line 373016
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Group names/count mismatched"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 373017
    :cond_2
    iput-object p3, p0, LX/27f;->name:Ljava/lang/String;

    .line 373018
    iput-object p7, p0, LX/27f;->startDate:Ljava/util/Date;

    .line 373019
    iput-object p8, p0, LX/27f;->endDate:Ljava/util/Date;

    .line 373020
    iput-object p9, p0, LX/27f;->mConditionalFilter:LX/8DP;

    .line 373021
    if-eqz p7, :cond_3

    if-nez p8, :cond_4

    .line 373022
    :cond_3
    const/4 p4, 0x0

    .line 373023
    :cond_4
    iput p4, p0, LX/27f;->groupSize:I

    .line 373024
    iput p5, p0, LX/27f;->groupCount:I

    .line 373025
    iput-object p6, p0, LX/27f;->groupNames:[Ljava/lang/String;

    .line 373026
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;ILjava/util/Date;Ljava/util/Date;LX/8DP;)V
    .locals 10
    .param p7    # LX/8DP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "LX/8DP;",
            ")V"
        }
    .end annotation

    .prologue
    .line 373008
    const/4 v5, 0x2

    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "test"

    aput-object v1, v6, v0

    const/4 v0, 0x1

    const-string v1, "control"

    aput-object v1, v6, v0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, LX/27f;-><init>(Ljava/lang/String;ILjava/lang/String;II[Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;LX/8DP;)V

    .line 373009
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/27f;
    .locals 1

    .prologue
    .line 373006
    const-class v0, LX/27f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/27f;

    return-object v0
.end method

.method public static values()[LX/27f;
    .locals 1

    .prologue
    .line 373007
    sget-object v0, LX/27f;->$VALUES:[LX/27f;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/27f;

    return-object v0
.end method
