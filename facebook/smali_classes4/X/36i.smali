.class public LX/36i;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/1nu;

.field private final b:LX/1zC;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Uf;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1nu;LX/1zC;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 499968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 499969
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 499970
    iput-object v0, p0, LX/36i;->c:LX/0Ot;

    .line 499971
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 499972
    iput-object v0, p0, LX/36i;->d:LX/0Ot;

    .line 499973
    iput-object p1, p0, LX/36i;->a:LX/1nu;

    .line 499974
    iput-object p2, p0, LX/36i;->b:LX/1zC;

    .line 499975
    return-void
.end method

.method public static a(LX/1De;Ljava/lang/CharSequence;)LX/1ne;
    .locals 2

    .prologue
    .line 499976
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a0443

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    .line 499977
    return-object v0
.end method

.method public static a(LX/0QB;)LX/36i;
    .locals 5

    .prologue
    .line 499978
    const-class v1, LX/36i;

    monitor-enter v1

    .line 499979
    :try_start_0
    sget-object v0, LX/36i;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499980
    sput-object v2, LX/36i;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499981
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499982
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499983
    new-instance p0, LX/36i;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v4

    check-cast v4, LX/1zC;

    invoke-direct {p0, v3, v4}, LX/36i;-><init>(LX/1nu;LX/1zC;)V

    .line 499984
    const/16 v3, 0x129c

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x775

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    .line 499985
    iput-object v3, p0, LX/36i;->c:LX/0Ot;

    iput-object v4, p0, LX/36i;->d:LX/0Ot;

    .line 499986
    move-object v0, p0

    .line 499987
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499988
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/36i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499989
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499990
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/common/callercontext/CallerContext;II)LX/1Di;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "II)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    .line 499991
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499992
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 499993
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 499994
    :goto_0
    iget-object v1, p0, LX/36i;->a:LX/1nu;

    invoke-virtual {v1, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v0

    const v1, 0x7f02111f

    invoke-virtual {v0, v1}, LX/1nw;->h(I)LX/1nw;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, p4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x5

    const v2, 0x7f0b00d5

    invoke-interface {v0, v1, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {v0, v1, p5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    return-object v0

    .line 499995
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .locals 4

    .prologue
    .line 499996
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 499997
    iget-object v1, p0, LX/36i;->b:LX/1zC;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/1zC;->a(Landroid/text/Editable;I)Z

    .line 499998
    return-object v0
.end method

.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;TE;",
            "Ljava/lang/CharSequence;",
            "I)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 499999
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 500000
    new-instance v2, LX/36j;

    invoke-direct {v2, v0}, LX/36j;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v1, p3

    .line 500001
    check-cast v1, LX/1Pr;

    invoke-interface {v1, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 500002
    const/4 v0, 0x0

    .line 500003
    if-nez v1, :cond_0

    .line 500004
    iget-object v0, p0, LX/36i;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Uf;

    .line 500005
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 500006
    new-instance v3, LX/36k;

    invoke-direct {v3, p0, p3, v2, v1}, LX/36k;-><init>(LX/36i;LX/1Pn;LX/36j;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v1, v3

    .line 500007
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081032

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p4, v1, p5, v2}, LX/1Uf;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;ILjava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    .line 500008
    :cond_0
    if-eqz v0, :cond_1

    move-object p4, v0

    :cond_1
    return-object p4
.end method

.method public final a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1EO;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;",
            "LX/1EO;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 500009
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 500010
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 500011
    if-eqz p4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 500012
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    move-object v3, v0

    .line 500013
    :goto_0
    iget-object v0, p0, LX/36i;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EN;

    .line 500014
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 500015
    sget-object v2, LX/1Qt;->FEED:LX/1Qt;

    sget-object v5, LX/An0;->INLINE_COMMENT_PREVIEW:LX/An0;

    move-object v4, p1

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, LX/1EN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View;LX/An0;LX/1EO;)V

    .line 500016
    return-void

    :cond_0
    move-object v3, v0

    goto :goto_0
.end method
