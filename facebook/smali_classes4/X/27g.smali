.class public LX/27g;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/2JA;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/0dz;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2JA;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0dz;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 373034
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373035
    iput-object p1, p0, LX/27g;->a:Landroid/content/Context;

    .line 373036
    iput-object p2, p0, LX/27g;->b:LX/2JA;

    .line 373037
    iput-object p3, p0, LX/27g;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 373038
    iput-object p4, p0, LX/27g;->d:LX/0dz;

    .line 373039
    iget-object v0, p0, LX/27g;->b:LX/2JA;

    invoke-virtual {v0}, LX/2JA;->a()V

    .line 373040
    return-void
.end method

.method private a(Z)LX/27h;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 373041
    const/4 v0, -0x1

    .line 373042
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v4

    .line 373043
    iget-object v2, p0, LX/27g;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0eC;->b:LX/0Tn;

    const-string v5, "device"

    invoke-interface {v2, v3, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v5, v2

    .line 373044
    if-eqz p1, :cond_6

    .line 373045
    const-string v2, "device"

    iget-object v3, p0, LX/27g;->a:Landroid/content/Context;

    const v6, 0x7f0835c5

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 373046
    const-string v2, "device"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 373047
    :cond_0
    const/4 v2, 0x1

    move v9, v2

    move v2, v0

    move v0, v9

    .line 373048
    :goto_0
    iget-object v3, p0, LX/27g;->b:LX/2JA;

    invoke-virtual {v3}, LX/2JA;->c()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v1

    move v1, v0

    :goto_1
    if-ge v3, v7, :cond_1

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 373049
    invoke-static {v0}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v8

    invoke-static {v8}, LX/0e9;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v0, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 373050
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 373051
    :goto_2
    add-int/lit8 v2, v1, 0x1

    .line 373052
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v2

    move v2, v0

    goto :goto_1

    .line 373053
    :cond_1
    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v3

    .line 373054
    iget-object v0, p0, LX/27g;->d:LX/0dz;

    invoke-virtual {v0}, LX/0dz;->c()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 373055
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v7

    .line 373056
    invoke-virtual {v3, v7}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 373057
    invoke-static {v0}, LX/0e9;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 373058
    invoke-virtual {v4, v7, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 373059
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 373060
    :goto_4
    add-int/lit8 v1, v1, 0x1

    :goto_5
    move v2, v0

    .line 373061
    goto :goto_3

    .line 373062
    :cond_2
    new-instance v0, LX/27h;

    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-direct {v0, v1, v2}, LX/27h;-><init>(LX/0P1;I)V

    return-object v0

    :cond_3
    move v0, v2

    goto :goto_4

    :cond_4
    move v0, v2

    goto :goto_5

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v2, v0

    move v0, v1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/27g;
    .locals 5

    .prologue
    .line 373063
    new-instance v4, LX/27g;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/2JA;->b(LX/0QB;)LX/2JA;

    move-result-object v1

    check-cast v1, LX/2JA;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0dz;->a(LX/0QB;)LX/0dz;

    move-result-object v3

    check-cast v3, LX/0dz;

    invoke-direct {v4, v0, v1, v2, v3}, LX/27g;-><init>(Landroid/content/Context;LX/2JA;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0dz;)V

    .line 373064
    return-object v4
.end method


# virtual methods
.method public final a()LX/27h;
    .locals 1

    .prologue
    .line 373065
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/27g;->a(Z)LX/27h;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/27h;
    .locals 1

    .prologue
    .line 373066
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/27g;->a(Z)LX/27h;

    move-result-object v0

    return-object v0
.end method
