.class public LX/2Ec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static volatile n:LX/2Ec;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/view/WindowManager;

.field public final d:Landroid/os/Handler;

.field public final e:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field private final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final g:LX/0Xl;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/15D",
            "<*>;>;"
        }
    .end annotation
.end field

.field public i:Landroid/widget/TextView;

.field private final j:Lcom/facebook/http/common/ActiveRequestsOverlayController$Callback;

.field private k:Z

.field private l:Z

.field public m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 385644
    sget-object v0, LX/0dU;->g:LX/0Tn;

    sput-object v0, LX/2Ec;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/WindowManager;Landroid/os/Handler;Lcom/facebook/http/common/FbHttpRequestProcessor;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;)V
    .locals 2
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p6    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 385645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385646
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/2Ec;->h:Ljava/util/List;

    .line 385647
    new-instance v0, Lcom/facebook/http/common/ActiveRequestsOverlayController$Callback;

    invoke-direct {v0, p0}, Lcom/facebook/http/common/ActiveRequestsOverlayController$Callback;-><init>(LX/2Ec;)V

    iput-object v0, p0, LX/2Ec;->j:Lcom/facebook/http/common/ActiveRequestsOverlayController$Callback;

    .line 385648
    iput-boolean v1, p0, LX/2Ec;->k:Z

    .line 385649
    iput-boolean v1, p0, LX/2Ec;->l:Z

    .line 385650
    iput-boolean v1, p0, LX/2Ec;->m:Z

    .line 385651
    iput-object p1, p0, LX/2Ec;->b:Landroid/content/Context;

    .line 385652
    iput-object p2, p0, LX/2Ec;->c:Landroid/view/WindowManager;

    .line 385653
    iput-object p3, p0, LX/2Ec;->d:Landroid/os/Handler;

    .line 385654
    iput-object p4, p0, LX/2Ec;->e:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 385655
    iput-object p5, p0, LX/2Ec;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 385656
    iput-object p6, p0, LX/2Ec;->g:LX/0Xl;

    .line 385657
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ec;
    .locals 10

    .prologue
    .line 385667
    sget-object v0, LX/2Ec;->n:LX/2Ec;

    if-nez v0, :cond_1

    .line 385668
    const-class v1, LX/2Ec;

    monitor-enter v1

    .line 385669
    :try_start_0
    sget-object v0, LX/2Ec;->n:LX/2Ec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 385670
    if-eqz v2, :cond_0

    .line 385671
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 385672
    new-instance v3, LX/2Ec;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v6

    check-cast v6, Landroid/os/Handler;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v7

    check-cast v7, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v9

    check-cast v9, LX/0Xl;

    invoke-direct/range {v3 .. v9}, LX/2Ec;-><init>(Landroid/content/Context;Landroid/view/WindowManager;Landroid/os/Handler;Lcom/facebook/http/common/FbHttpRequestProcessor;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;)V

    .line 385673
    move-object v0, v3

    .line 385674
    sput-object v0, LX/2Ec;->n:LX/2Ec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 385675
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 385676
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 385677
    :cond_1
    sget-object v0, LX/2Ec;->n:LX/2Ec;

    return-object v0

    .line 385678
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 385679
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static f(LX/2Ec;)V
    .locals 6

    .prologue
    const/4 v1, -0x2

    .line 385658
    iget-object v0, p0, LX/2Ec;->i:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 385659
    :goto_0
    return-void

    .line 385660
    :cond_0
    new-instance v0, Landroid/widget/TextView;

    iget-object v2, p0, LX/2Ec;->b:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/2Ec;->i:Landroid/widget/TextView;

    .line 385661
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d6

    const/16 v4, 0x18

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 385662
    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 385663
    iget-object v1, p0, LX/2Ec;->i:Landroid/widget/TextView;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const v3, -0x55000001

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 385664
    iget-object v1, p0, LX/2Ec;->i:Landroid/widget/TextView;

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 385665
    iget-object v1, p0, LX/2Ec;->i:Landroid/widget/TextView;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 385666
    iget-object v1, p0, LX/2Ec;->c:Landroid/view/WindowManager;

    iget-object v2, p0, LX/2Ec;->i:Landroid/widget/TextView;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 385630
    iget-object v1, p0, LX/2Ec;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2Ec;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    iput-boolean v1, p0, LX/2Ec;->k:Z

    .line 385631
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    iget-object v1, p0, LX/2Ec;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, LX/2Ec;->l:Z

    .line 385632
    iget-boolean v0, p0, LX/2Ec;->k:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/2Ec;->l:Z

    if-nez v0, :cond_2

    .line 385633
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.action.MANAGE_OVERLAY_PERMISSION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 385634
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "package:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/2Ec;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 385635
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 385636
    iget-object v1, p0, LX/2Ec;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 385637
    :cond_2
    invoke-virtual {p0}, LX/2Ec;->c()V

    .line 385638
    invoke-virtual {p0}, LX/2Ec;->e()Z

    move-result v0

    if-nez v0, :cond_3

    .line 385639
    invoke-virtual {p0}, LX/2Ec;->b()V

    .line 385640
    :cond_3
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 385641
    iget-object v0, p0, LX/2Ec;->i:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 385642
    iget-object v0, p0, LX/2Ec;->i:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 385643
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 385617
    invoke-virtual {p0}, LX/2Ec;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385618
    invoke-static {p0}, LX/2Ec;->f(LX/2Ec;)V

    .line 385619
    iget-object v0, p0, LX/2Ec;->i:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 385620
    invoke-virtual {p0}, LX/2Ec;->d()V

    .line 385621
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 385622
    iget-object v0, p0, LX/2Ec;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/2Ec;->j:Lcom/facebook/http/common/ActiveRequestsOverlayController$Callback;

    const-wide/16 v2, 0x1f4

    const v4, -0x2a4538c3

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 385623
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 385624
    iget-boolean v0, p0, LX/2Ec;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/2Ec;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/2Ec;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 385625
    invoke-virtual {p0}, LX/2Ec;->a()V

    .line 385626
    iget-object v0, p0, LX/2Ec;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2Ec;->a:LX/0Tn;

    new-instance v2, LX/2Ef;

    invoke-direct {v2, p0}, LX/2Ef;-><init>(LX/2Ec;)V

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 385627
    iget-object v0, p0, LX/2Ec;->g:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v2, LX/2Eg;

    invoke-direct {v2, p0}, LX/2Eg;-><init>(LX/2Ec;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 385628
    iget-object v0, p0, LX/2Ec;->g:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v2, LX/2Eh;

    invoke-direct {v2, p0}, LX/2Eh;-><init>(LX/2Ec;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 385629
    return-void
.end method
