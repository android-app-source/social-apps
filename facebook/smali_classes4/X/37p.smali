.class public final LX/37p;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/37j;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/38W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/37j;)V
    .locals 1

    .prologue
    .line 502330
    iput-object p1, p0, LX/37p;->a:LX/37j;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 502331
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/37p;->b:Ljava/util/ArrayList;

    return-void
.end method

.method private static a(LX/38W;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 502294
    iget-object v0, p0, LX/38W;->b:LX/38V;

    .line 502295
    const v1, 0xff00

    and-int/2addr v1, p1

    packed-switch v1, :pswitch_data_0

    .line 502296
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 502297
    :pswitch_1
    check-cast p2, LX/384;

    .line 502298
    iget v1, p0, LX/38W;->d:I

    and-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_1

    iget-object v1, p0, LX/38W;->c:LX/38T;

    invoke-virtual {p2, v1}, LX/384;->a(LX/38T;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 502299
    if-eqz v1, :cond_0

    .line 502300
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 502301
    :pswitch_2
    invoke-virtual {v0, p2}, LX/38V;->b(LX/384;)V

    goto :goto_0

    .line 502302
    :pswitch_3
    invoke-virtual {v0, p2}, LX/38V;->c(LX/384;)V

    goto :goto_0

    .line 502303
    :pswitch_4
    invoke-virtual {v0, p2}, LX/38V;->d(LX/384;)V

    goto :goto_0

    .line 502304
    :pswitch_5
    invoke-virtual {v0, p2}, LX/38V;->a(LX/384;)V

    goto :goto_0

    .line 502305
    :pswitch_6
    invoke-virtual {v0}, LX/38V;->a()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x100
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x101
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private b(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 502324
    packed-switch p1, :pswitch_data_0

    .line 502325
    :goto_0
    :pswitch_0
    return-void

    .line 502326
    :pswitch_1
    iget-object v0, p0, LX/37p;->a:LX/37j;

    iget-object v0, v0, LX/37j;->j:LX/37u;

    check-cast p2, LX/384;

    invoke-virtual {v0, p2}, LX/37u;->a(LX/384;)V

    goto :goto_0

    .line 502327
    :pswitch_2
    iget-object v0, p0, LX/37p;->a:LX/37j;

    iget-object v0, v0, LX/37j;->j:LX/37u;

    check-cast p2, LX/384;

    invoke-virtual {v0, p2}, LX/37u;->b(LX/384;)V

    goto :goto_0

    .line 502328
    :pswitch_3
    iget-object v0, p0, LX/37p;->a:LX/37j;

    iget-object v0, v0, LX/37j;->j:LX/37u;

    check-cast p2, LX/384;

    invoke-virtual {v0, p2}, LX/37u;->c(LX/384;)V

    goto :goto_0

    .line 502329
    :pswitch_4
    iget-object v0, p0, LX/37p;->a:LX/37j;

    iget-object v0, v0, LX/37j;->j:LX/37u;

    check-cast p2, LX/384;

    invoke-virtual {v0, p2}, LX/37u;->d(LX/384;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 502322
    invoke-virtual {p0, p1, p2}, LX/37p;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 502323
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 502306
    iget v2, p1, Landroid/os/Message;->what:I

    .line 502307
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 502308
    invoke-direct {p0, v2, v3}, LX/37p;->b(ILjava/lang/Object;)V

    .line 502309
    :try_start_0
    iget-object v0, p0, LX/37p;->a:LX/37j;

    iget-object v0, v0, LX/37j;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_1

    .line 502310
    iget-object v0, p0, LX/37p;->a:LX/37j;

    iget-object v0, v0, LX/37j;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/37i;

    .line 502311
    if-nez v0, :cond_0

    .line 502312
    iget-object v0, p0, LX/37p;->a:LX/37j;

    iget-object v0, v0, LX/37j;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v1

    goto :goto_0

    .line 502313
    :cond_0
    iget-object v4, p0, LX/37p;->b:Ljava/util/ArrayList;

    iget-object v0, v0, LX/37i;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move v0, v1

    .line 502314
    goto :goto_0

    .line 502315
    :cond_1
    iget-object v0, p0, LX/37p;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 502316
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    .line 502317
    iget-object v0, p0, LX/37p;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38W;

    invoke-static {v0, v2, v3}, LX/37p;->a(LX/38W;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502318
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 502319
    :cond_2
    iget-object v0, p0, LX/37p;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 502320
    return-void

    .line 502321
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/37p;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    throw v0
.end method
