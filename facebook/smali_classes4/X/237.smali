.class public final LX/237;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final className:Ljava/lang/String;

.field private holderHead:LX/238;

.field public holderTail:LX/238;

.field private omitNullValues:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 361921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361922
    new-instance v0, LX/238;

    invoke-direct {v0}, LX/238;-><init>()V

    iput-object v0, p0, LX/237;->holderHead:LX/238;

    .line 361923
    iget-object v0, p0, LX/237;->holderHead:LX/238;

    iput-object v0, p0, LX/237;->holderTail:LX/238;

    .line 361924
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/237;->omitNullValues:Z

    .line 361925
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/237;->className:Ljava/lang/String;

    .line 361926
    return-void
.end method

.method private addHolder(Ljava/lang/String;Ljava/lang/Object;)LX/237;
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 361927
    new-instance v0, LX/238;

    invoke-direct {v0}, LX/238;-><init>()V

    .line 361928
    iget-object v1, p0, LX/237;->holderTail:LX/238;

    iput-object v0, v1, LX/238;->next:LX/238;

    iput-object v0, p0, LX/237;->holderTail:LX/238;

    .line 361929
    move-object v1, v0

    .line 361930
    iput-object p2, v1, LX/238;->value:Ljava/lang/Object;

    .line 361931
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, LX/238;->name:Ljava/lang/String;

    .line 361932
    return-object p0
.end method


# virtual methods
.method public final add(Ljava/lang/String;F)LX/237;
    .locals 1

    .prologue
    .line 361919
    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/237;->addHolder(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/String;I)LX/237;
    .locals 1

    .prologue
    .line 361918
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/237;->addHolder(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/String;J)LX/237;
    .locals 2

    .prologue
    .line 361920
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/237;->addHolder(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/String;Ljava/lang/Object;)LX/237;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 361917
    invoke-direct {p0, p1, p2}, LX/237;->addHolder(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/String;Z)LX/237;
    .locals 1

    .prologue
    .line 361916
    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/237;->addHolder(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 361903
    iget-boolean v2, p0, LX/237;->omitNullValues:Z

    .line 361904
    const-string v1, ""

    .line 361905
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v3, p0, LX/237;->className:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x7b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 361906
    iget-object v0, p0, LX/237;->holderHead:LX/238;

    iget-object v0, v0, LX/238;->next:LX/238;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 361907
    :goto_0
    if-eqz v1, :cond_3

    .line 361908
    if-eqz v2, :cond_0

    iget-object v4, v1, LX/238;->value:Ljava/lang/Object;

    if-eqz v4, :cond_2

    .line 361909
    :cond_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 361910
    const-string v0, ", "

    .line 361911
    iget-object v4, v1, LX/238;->name:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 361912
    iget-object v4, v1, LX/238;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x3d

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 361913
    :cond_1
    iget-object v4, v1, LX/238;->value:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 361914
    :cond_2
    iget-object v1, v1, LX/238;->next:LX/238;

    goto :goto_0

    .line 361915
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
