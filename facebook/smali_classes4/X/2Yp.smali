.class public final LX/2Yp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0ug;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0ug;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 421386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421387
    iput-object p1, p0, LX/2Yp;->a:LX/0QB;

    .line 421388
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/0ug;",
            ">;"
        }
    .end annotation

    .prologue
    .line 421389
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/2Yp;

    invoke-direct {v2, p0}, LX/2Yp;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 421390
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2Yp;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 421391
    packed-switch p2, :pswitch_data_0

    .line 421392
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 421393
    :pswitch_0
    invoke-static {p1}, LX/0tt;->a(LX/0QB;)LX/0uf;

    move-result-object v0

    .line 421394
    :goto_0
    return-object v0

    .line 421395
    :pswitch_1
    invoke-static {p1}, LX/3SL;->a(LX/0QB;)LX/3SL;

    move-result-object v0

    goto :goto_0

    .line 421396
    :pswitch_2
    invoke-static {p1}, LX/3SN;->a(LX/0QB;)LX/3SN;

    move-result-object v0

    goto :goto_0

    .line 421397
    :pswitch_3
    new-instance p2, LX/3SO;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/0dz;->a(LX/0QB;)LX/0dz;

    move-result-object v1

    check-cast v1, LX/0dz;

    invoke-static {p1}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v2

    check-cast v2, LX/0W9;

    invoke-static {p1}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object p0

    check-cast p0, LX/0W3;

    invoke-direct {p2, v0, v1, v2, p0}, LX/3SO;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0dz;LX/0W9;LX/0W3;)V

    .line 421398
    move-object v0, p2

    .line 421399
    goto :goto_0

    .line 421400
    :pswitch_4
    invoke-static {p1}, LX/2Li;->a(LX/0QB;)LX/2Li;

    move-result-object v0

    goto :goto_0

    .line 421401
    :pswitch_5
    new-instance v1, LX/3SP;

    invoke-static {p1}, LX/1hv;->a(LX/0QB;)LX/1hv;

    move-result-object v0

    check-cast v0, LX/1hv;

    invoke-direct {v1, v0}, LX/3SP;-><init>(LX/1hv;)V

    .line 421402
    move-object v0, v1

    .line 421403
    goto :goto_0

    .line 421404
    :pswitch_6
    new-instance v1, LX/3SQ;

    invoke-static {p1}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->a(LX/0QB;)Lcom/facebook/rtc/voicemail/VoicemailHandler;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;

    invoke-direct {v1, v0}, LX/3SQ;-><init>(Lcom/facebook/rtc/voicemail/VoicemailHandler;)V

    .line 421405
    move-object v0, v1

    .line 421406
    goto :goto_0

    .line 421407
    :pswitch_7
    invoke-static {p1}, LX/2TC;->a(LX/0QB;)LX/2TC;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 421408
    const/16 v0, 0x8

    return v0
.end method
