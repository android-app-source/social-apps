.class public LX/3AD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 524927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524928
    iput-object p1, p0, LX/3AD;->a:LX/0tX;

    .line 524929
    return-void
.end method

.method public static a(LX/3AD;LX/4Jw;Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4Jw;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 524930
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 524931
    const-string v1, "rating"

    invoke-virtual {p1, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 524932
    move-object v0, p1

    .line 524933
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 524934
    const-string p2, "story_id"

    invoke-virtual {v0, p2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 524935
    new-instance v0, LX/7fq;

    invoke-direct {v0}, LX/7fq;-><init>()V

    move-object v0, v0

    .line 524936
    const-string v1, "input"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 524937
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 524938
    iget-object v1, p0, LX/3AD;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3AD;
    .locals 2

    .prologue
    .line 524939
    new-instance v1, LX/3AD;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, v0}, LX/3AD;-><init>(LX/0tX;)V

    .line 524940
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 524941
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->m()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v0

    .line 524942
    new-instance v1, LX/4Jw;

    invoke-direct {v1}, LX/4Jw;-><init>()V

    const-string v2, "AUTO_TRANSLATION"

    invoke-virtual {v1, v2}, LX/4Jw;->d(Ljava/lang/String;)LX/4Jw;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTranslation;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4Jw;->c(Ljava/lang/String;)LX/4Jw;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTranslation;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4Jw;->b(Ljava/lang/String;)LX/4Jw;

    move-result-object v0

    .line 524943
    invoke-static {p0, v0, p1, p2}, LX/3AD;->a(LX/3AD;LX/4Jw;Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
