.class public LX/2Li;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Pb;
.implements LX/0ug;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[I

.field private static volatile h:LX/2Li;


# instance fields
.field public final b:LX/0W3;

.field public final c:LX/0lC;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/content/Context;

.field private f:LX/0Pd;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:LX/0Pc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 395359
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/2Li;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0xa00a6
        0x230013
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;LX/0W3;LX/0lB;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0lB;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395353
    iput-object p1, p0, LX/2Li;->e:Landroid/content/Context;

    .line 395354
    iput-object p2, p0, LX/2Li;->b:LX/0W3;

    .line 395355
    iput-object p3, p0, LX/2Li;->c:LX/0lC;

    .line 395356
    iput-object p4, p0, LX/2Li;->d:LX/0Ot;

    .line 395357
    invoke-direct {p0}, LX/2Li;->c()LX/0Pd;

    move-result-object v0

    iput-object v0, p0, LX/2Li;->f:LX/0Pd;

    .line 395358
    return-void
.end method

.method public static a(LX/0QB;)LX/2Li;
    .locals 7

    .prologue
    .line 395339
    sget-object v0, LX/2Li;->h:LX/2Li;

    if-nez v0, :cond_1

    .line 395340
    const-class v1, LX/2Li;

    monitor-enter v1

    .line 395341
    :try_start_0
    sget-object v0, LX/2Li;->h:LX/2Li;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 395342
    if-eqz v2, :cond_0

    .line 395343
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 395344
    new-instance v6, LX/2Li;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lB;

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/2Li;-><init>(Landroid/content/Context;LX/0W3;LX/0lB;LX/0Ot;)V

    .line 395345
    move-object v0, v6

    .line 395346
    sput-object v0, LX/2Li;->h:LX/2Li;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 395347
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 395348
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 395349
    :cond_1
    sget-object v0, LX/2Li;->h:LX/2Li;

    return-object v0

    .line 395350
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 395351
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/loom/config/LoomConfiguration;)V
    .locals 14

    .prologue
    .line 395322
    invoke-virtual {p1}, Lcom/facebook/loom/config/LoomConfiguration;->a()LX/0Pg;

    move-result-object v10

    .line 395323
    if-nez v10, :cond_0

    .line 395324
    :goto_0
    return-void

    .line 395325
    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 395326
    const/16 v0, 0x8

    invoke-interface {v10, v0}, LX/0Pg;->a(I)LX/0Pk;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;

    .line 395327
    if-eqz v7, :cond_1

    .line 395328
    new-instance v0, LX/1vM;

    const/16 v1, 0x8

    invoke-virtual {v7}, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->e()I

    move-result v2

    invoke-virtual {v7}, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->f()I

    move-result v3

    invoke-virtual {v7}, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->d()I

    move-result v4

    invoke-virtual {v7}, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->b()I

    move-result v5

    invoke-virtual {v7}, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->c()I

    move-result v6

    invoke-virtual {v7}, Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;->g()I

    move-result v7

    invoke-direct/range {v0 .. v7}, LX/1vM;-><init>(IIIIIII)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395329
    :cond_1
    const/4 v0, 0x1

    invoke-interface {v10, v0}, LX/0Pg;->a(I)LX/0Pk;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/0Pl;

    .line 395330
    if-eqz v8, :cond_3

    .line 395331
    sget-object v12, LX/2Li;->a:[I

    array-length v13, v12

    const/4 v0, 0x0

    move v9, v0

    :goto_1
    if-ge v9, v13, :cond_3

    aget v6, v12, v9

    .line 395332
    invoke-virtual {v8, v6}, LX/0Pl;->a(I)Lcom/facebook/loom/config/QPLTraceControlConfiguration;

    move-result-object v7

    .line 395333
    if-eqz v7, :cond_2

    .line 395334
    new-instance v0, LX/1vM;

    const/4 v1, 0x1

    invoke-interface {v10}, LX/0Pg;->b()I

    move-result v2

    invoke-interface {v10}, LX/0Pg;->c()I

    move-result v3

    invoke-virtual {v7}, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->b()I

    move-result v4

    invoke-virtual {v7}, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->c()I

    move-result v5

    invoke-virtual {v7}, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->d()I

    move-result v7

    invoke-direct/range {v0 .. v7}, LX/1vM;-><init>(IIIIIII)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395335
    :cond_2
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    .line 395336
    :cond_3
    :try_start_0
    iget-object v0, p0, LX/2Li;->e:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/facebook/loom/config/LoomConfiguration;->b()Lcom/facebook/loom/config/SystemControlConfiguration;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/loom/config/LoomConfiguration;->d()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3, v11}, LX/0Pa;->a(Landroid/content/Context;Lcom/facebook/loom/config/SystemControlConfiguration;JLjava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 395337
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 395338
    iget-object v0, p0, LX/2Li;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "LoomConfigProvider"

    const-string v3, "Could not write init file based config"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method private c()LX/0Pd;
    .locals 7
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 395291
    const/4 v3, 0x0

    .line 395292
    iget-object v2, p0, LX/2Li;->b:LX/0W3;

    invoke-virtual {v2}, LX/0W3;->a()LX/0W4;

    move-result-object v2

    sget-wide v4, LX/0X5;->ib:J

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 395293
    if-eqz v2, :cond_3

    .line 395294
    :try_start_0
    iget-object v4, p0, LX/2Li;->c:LX/0lC;

    const-class v5, Lcom/facebook/loom/config/LoomConfiguration;

    invoke-virtual {v4, v2, v5}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/loom/config/LoomConfiguration;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 395295
    :goto_0
    move-object v0, v2

    .line 395296
    :try_start_1
    iget-object v1, p0, LX/2Li;->e:Landroid/content/Context;

    invoke-static {v1}, LX/0Pa;->a(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 395297
    :goto_1
    if-nez v0, :cond_2

    .line 395298
    sget-object v0, LX/02o;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 395299
    if-eqz v0, :cond_1

    .line 395300
    invoke-static {}, LX/02o;->b()LX/02o;

    move-result-object v0

    .line 395301
    iget-object v1, v0, LX/02o;->e:LX/0Pd;

    move-object v0, v1

    .line 395302
    :cond_0
    :goto_3
    return-object v0

    .line 395303
    :cond_1
    sget-object v0, LX/0Pf;->a:LX/0Pd;

    goto :goto_3

    .line 395304
    :cond_2
    invoke-direct {p0, v0}, LX/2Li;->a(Lcom/facebook/loom/config/LoomConfiguration;)V

    .line 395305
    invoke-virtual {v0}, Lcom/facebook/loom/config/LoomConfiguration;->a()LX/0Pg;

    move-result-object v1

    if-nez v1, :cond_0

    .line 395306
    sget-object v0, LX/0Pf;->a:LX/0Pd;

    goto :goto_3

    .line 395307
    :catch_0
    move-exception v2

    move-object v4, v2

    .line 395308
    iget-object v2, p0, LX/2Li;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v5, "LoomConfigProvider"

    const-string v6, "Failure in parsing json from MobileConfig."

    invoke-virtual {v2, v5, v6, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    move-object v2, v3

    .line 395309
    goto :goto_0

    .line 395310
    :catch_1
    move-exception v1

    move-object v2, v1

    .line 395311
    iget-object v1, p0, LX/2Li;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v3, "LoomConfigProvider"

    const-string v4, "Could not remove init file based config"

    invoke-virtual {v1, v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 395321
    const/16 v0, 0xf8

    return v0
.end method

.method public final declared-synchronized a(I)V
    .locals 2

    .prologue
    .line 395316
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/2Li;->c()LX/0Pd;

    move-result-object v0

    iput-object v0, p0, LX/2Li;->f:LX/0Pd;

    .line 395317
    iget-object v0, p0, LX/2Li;->g:LX/0Pc;

    if-eqz v0, :cond_0

    .line 395318
    iget-object v0, p0, LX/2Li;->g:LX/0Pc;

    iget-object v1, p0, LX/2Li;->f:LX/0Pd;

    invoke-interface {v0, v1}, LX/0Pc;->a(LX/0Pd;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395319
    :cond_0
    monitor-exit p0

    return-void

    .line 395320
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0Pc;)V
    .locals 1
    .param p1    # LX/0Pc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 395313
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/2Li;->g:LX/0Pc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395314
    monitor-exit p0

    return-void

    .line 395315
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/0Pd;
    .locals 1

    .prologue
    .line 395312
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Li;->f:LX/0Pd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
