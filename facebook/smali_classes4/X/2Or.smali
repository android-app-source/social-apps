.class public LX/2Or;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 403317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403318
    iput-object p1, p0, LX/2Or;->a:LX/0Uh;

    .line 403319
    return-void
.end method

.method public static a(LX/0QB;)LX/2Or;
    .locals 1

    .prologue
    .line 403322
    invoke-static {p0}, LX/2Or;->b(LX/0QB;)LX/2Or;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2Or;
    .locals 2

    .prologue
    .line 403320
    new-instance v1, LX/2Or;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v0}, LX/2Or;-><init>(LX/0Uh;)V

    .line 403321
    return-object v1
.end method


# virtual methods
.method public final d()Z
    .locals 3

    .prologue
    .line 403316
    iget-object v0, p0, LX/2Or;->a:LX/0Uh;

    const/16 v1, 0x631

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 3

    .prologue
    .line 403315
    iget-object v0, p0, LX/2Or;->a:LX/0Uh;

    const/16 v1, 0x629

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 3

    .prologue
    .line 403323
    iget-object v0, p0, LX/2Or;->a:LX/0Uh;

    const/16 v1, 0x628

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 3

    .prologue
    .line 403314
    iget-object v0, p0, LX/2Or;->a:LX/0Uh;

    const/16 v1, 0x1d7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 3

    .prologue
    .line 403313
    iget-object v0, p0, LX/2Or;->a:LX/0Uh;

    const/16 v1, 0x62a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 3

    .prologue
    .line 403312
    iget-object v0, p0, LX/2Or;->a:LX/0Uh;

    const/16 v1, 0x639

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 3

    .prologue
    .line 403309
    iget-object v0, p0, LX/2Or;->a:LX/0Uh;

    const/16 v1, 0x62f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 3

    .prologue
    .line 403311
    iget-object v0, p0, LX/2Or;->a:LX/0Uh;

    const/16 v1, 0x634

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 3

    .prologue
    .line 403310
    iget-object v0, p0, LX/2Or;->a:LX/0Uh;

    const/16 v1, 0x627

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
