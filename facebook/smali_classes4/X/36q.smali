.class public final LX/36q;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 500086
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 5
    .param p0    # Lcom/facebook/graphql/model/GraphQLMedia;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 500087
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4984e12

    if-eq v0, v1, :cond_1

    .line 500088
    :cond_0
    const/4 v0, 0x0

    .line 500089
    :goto_0
    return-object v0

    .line 500090
    :cond_1
    new-instance v0, LX/4Xy;

    invoke-direct {v0}, LX/4Xy;-><init>()V

    .line 500091
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->k()Ljava/lang/String;

    move-result-object v1

    .line 500092
    iput-object v1, v0, LX/4Xy;->b:Ljava/lang/String;

    .line 500093
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->l()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 500094
    iput-object v1, v0, LX/4Xy;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 500095
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500096
    iput-object v1, v0, LX/4Xy;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500097
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->n()I

    move-result v1

    .line 500098
    iput v1, v0, LX/4Xy;->g:I

    .line 500099
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->o()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    .line 500100
    iput-object v1, v0, LX/4Xy;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 500101
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->p()Ljava/lang/String;

    move-result-object v1

    .line 500102
    iput-object v1, v0, LX/4Xy;->i:Ljava/lang/String;

    .line 500103
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->r()I

    move-result v1

    .line 500104
    iput v1, v0, LX/4Xy;->j:I

    .line 500105
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->t()Z

    move-result v1

    .line 500106
    iput-boolean v1, v0, LX/4Xy;->k:Z

    .line 500107
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->u()Z

    move-result v1

    .line 500108
    iput-boolean v1, v0, LX/4Xy;->l:Z

    .line 500109
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bP()Z

    move-result v1

    .line 500110
    iput-boolean v1, v0, LX/4Xy;->m:Z

    .line 500111
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->v()Z

    move-result v1

    .line 500112
    iput-boolean v1, v0, LX/4Xy;->n:Z

    .line 500113
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->w()Z

    move-result v1

    .line 500114
    iput-boolean v1, v0, LX/4Xy;->o:Z

    .line 500115
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->x()Z

    move-result v1

    .line 500116
    iput-boolean v1, v0, LX/4Xy;->p:Z

    .line 500117
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->y()Z

    move-result v1

    .line 500118
    iput-boolean v1, v0, LX/4Xy;->q:Z

    .line 500119
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->z()Z

    move-result v1

    .line 500120
    iput-boolean v1, v0, LX/4Xy;->r:Z

    .line 500121
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->A()Z

    move-result v1

    .line 500122
    iput-boolean v1, v0, LX/4Xy;->s:Z

    .line 500123
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bW()Z

    move-result v1

    .line 500124
    iput-boolean v1, v0, LX/4Xy;->t:Z

    .line 500125
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->B()Z

    move-result v1

    .line 500126
    iput-boolean v1, v0, LX/4Xy;->u:Z

    .line 500127
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->C()Z

    move-result v1

    .line 500128
    iput-boolean v1, v0, LX/4Xy;->v:Z

    .line 500129
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->E()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 500130
    iput-object v1, v0, LX/4Xy;->w:Lcom/facebook/graphql/model/GraphQLStory;

    .line 500131
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->F()J

    move-result-wide v2

    .line 500132
    iput-wide v2, v0, LX/4Xy;->y:J

    .line 500133
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 500134
    iput-object v1, v0, LX/4Xy;->z:Lcom/facebook/graphql/model/GraphQLStory;

    .line 500135
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bD()Ljava/lang/String;

    move-result-object v1

    .line 500136
    iput-object v1, v0, LX/4Xy;->A:Ljava/lang/String;

    .line 500137
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->H()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    .line 500138
    iput-object v1, v0, LX/4Xy;->B:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 500139
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->I()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v1

    .line 500140
    iput-object v1, v0, LX/4Xy;->C:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    .line 500141
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 500142
    iput-object v1, v0, LX/4Xy;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 500143
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->K()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v1

    .line 500144
    iput-object v1, v0, LX/4Xy;->E:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 500145
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->M()Z

    move-result v1

    .line 500146
    iput-boolean v1, v0, LX/4Xy;->F:Z

    .line 500147
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->P()I

    move-result v1

    .line 500148
    iput v1, v0, LX/4Xy;->G:I

    .line 500149
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Q()I

    move-result v1

    .line 500150
    iput v1, v0, LX/4Xy;->H:I

    .line 500151
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    .line 500152
    iput-object v1, v0, LX/4Xy;->I:Ljava/lang/String;

    .line 500153
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500154
    iput-object v1, v0, LX/4Xy;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500155
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500156
    iput-object v1, v0, LX/4Xy;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500157
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500158
    iput-object v1, v0, LX/4Xy;->N:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500159
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500160
    iput-object v1, v0, LX/4Xy;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500161
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500162
    iput-object v1, v0, LX/4Xy;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500163
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500164
    iput-object v1, v0, LX/4Xy;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500165
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500166
    iput-object v1, v0, LX/4Xy;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500167
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500168
    iput-object v1, v0, LX/4Xy;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500169
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ac()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500170
    iput-object v1, v0, LX/4Xy;->aa:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500171
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500172
    iput-object v1, v0, LX/4Xy;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500173
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500174
    iput-object v1, v0, LX/4Xy;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500175
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v1

    .line 500176
    iput-object v1, v0, LX/4Xy;->ad:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 500177
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ak()Z

    move-result v1

    .line 500178
    iput-boolean v1, v0, LX/4Xy;->af:Z

    .line 500179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->al()Z

    move-result v1

    .line 500180
    iput-boolean v1, v0, LX/4Xy;->ag:Z

    .line 500181
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->am()Z

    move-result v1

    .line 500182
    iput-boolean v1, v0, LX/4Xy;->ah:Z

    .line 500183
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aq()Z

    move-result v1

    .line 500184
    iput-boolean v1, v0, LX/4Xy;->ai:Z

    .line 500185
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->av()Z

    move-result v1

    .line 500186
    iput-boolean v1, v0, LX/4Xy;->aj:Z

    .line 500187
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500188
    iput-object v1, v0, LX/4Xy;->ak:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500189
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ax()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500190
    iput-object v1, v0, LX/4Xy;->am:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500191
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500192
    iput-object v1, v0, LX/4Xy;->an:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500193
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bE()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 500194
    iput-object v1, v0, LX/4Xy;->ao:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 500195
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aA()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    move-result-object v1

    .line 500196
    iput-object v1, v0, LX/4Xy;->ap:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 500197
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aC()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500198
    iput-object v1, v0, LX/4Xy;->aq:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500199
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 500200
    iput-object v1, v0, LX/4Xy;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 500201
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aE()Ljava/lang/String;

    move-result-object v1

    .line 500202
    iput-object v1, v0, LX/4Xy;->au:Ljava/lang/String;

    .line 500203
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aG()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500204
    iput-object v1, v0, LX/4Xy;->av:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500205
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aH()Ljava/lang/String;

    move-result-object v1

    .line 500206
    iput-object v1, v0, LX/4Xy;->aw:Ljava/lang/String;

    .line 500207
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500208
    iput-object v1, v0, LX/4Xy;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500209
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500210
    iput-object v1, v0, LX/4Xy;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500211
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 500212
    iput-object v1, v0, LX/4Xy;->aA:Lcom/facebook/graphql/model/GraphQLActor;

    .line 500213
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bH()Ljava/lang/String;

    move-result-object v1

    .line 500214
    iput-object v1, v0, LX/4Xy;->aB:Ljava/lang/String;

    .line 500215
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aL()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    .line 500216
    iput-object v1, v0, LX/4Xy;->aC:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 500217
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aM()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    .line 500218
    iput-object v1, v0, LX/4Xy;->aD:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 500219
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v1

    .line 500220
    iput-object v1, v0, LX/4Xy;->aE:LX/0Px;

    .line 500221
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bI()Ljava/lang/String;

    move-result-object v1

    .line 500222
    iput-object v1, v0, LX/4Xy;->aG:Ljava/lang/String;

    .line 500223
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aP()Ljava/lang/String;

    move-result-object v1

    .line 500224
    iput-object v1, v0, LX/4Xy;->aH:Ljava/lang/String;

    .line 500225
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aR()I

    move-result v1

    .line 500226
    iput v1, v0, LX/4Xy;->aI:I

    .line 500227
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v1

    .line 500228
    iput-object v1, v0, LX/4Xy;->aJ:Ljava/lang/String;

    .line 500229
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aU()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500230
    iput-object v1, v0, LX/4Xy;->aK:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500231
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aW()Ljava/lang/String;

    move-result-object v1

    .line 500232
    iput-object v1, v0, LX/4Xy;->aL:Ljava/lang/String;

    .line 500233
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 500234
    iput-object v1, v0, LX/4Xy;->aN:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 500235
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bR()Ljava/lang/String;

    move-result-object v1

    .line 500236
    iput-object v1, v0, LX/4Xy;->aO:Ljava/lang/String;

    .line 500237
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aY()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v1

    .line 500238
    iput-object v1, v0, LX/4Xy;->aS:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 500239
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500240
    iput-object v1, v0, LX/4Xy;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500241
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bf()Z

    move-result v1

    .line 500242
    iput-boolean v1, v0, LX/4Xy;->aV:Z

    .line 500243
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bg()Z

    move-result v1

    .line 500244
    iput-boolean v1, v0, LX/4Xy;->aW:Z

    .line 500245
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bp()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500246
    iput-object v1, v0, LX/4Xy;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500247
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v1

    .line 500248
    iput-object v1, v0, LX/4Xy;->aZ:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 500249
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bx()I

    move-result v1

    .line 500250
    iput v1, v0, LX/4Xy;->bb:I

    .line 500251
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500252
    iput-object v1, v0, LX/4Xy;->bc:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500253
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bC()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v1

    .line 500254
    iput-object v1, v0, LX/4Xy;->bd:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 500255
    invoke-virtual {v0}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 5
    .param p0    # Lcom/facebook/graphql/model/GraphQLMedia;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 500256
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4ed245b

    if-eq v0, v1, :cond_1

    .line 500257
    :cond_0
    const/4 v0, 0x0

    .line 500258
    :goto_0
    return-object v0

    .line 500259
    :cond_1
    new-instance v0, LX/2oI;

    invoke-direct {v0}, LX/2oI;-><init>()V

    .line 500260
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500261
    iput-object v1, v0, LX/2oI;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500262
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->n()I

    move-result v1

    .line 500263
    iput v1, v0, LX/2oI;->e:I

    .line 500264
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->o()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    .line 500265
    iput-object v1, v0, LX/2oI;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 500266
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->p()Ljava/lang/String;

    move-result-object v1

    .line 500267
    iput-object v1, v0, LX/2oI;->g:Ljava/lang/String;

    .line 500268
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bJ()Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    move-result-object v1

    .line 500269
    iput-object v1, v0, LX/2oI;->h:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    .line 500270
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->q()J

    move-result-wide v2

    .line 500271
    iput-wide v2, v0, LX/2oI;->i:J

    .line 500272
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->r()I

    move-result v1

    .line 500273
    iput v1, v0, LX/2oI;->j:I

    .line 500274
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    .line 500275
    iput-object v1, v0, LX/2oI;->k:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 500276
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->u()Z

    move-result v1

    .line 500277
    iput-boolean v1, v0, LX/2oI;->m:Z

    .line 500278
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->z()Z

    move-result v1

    .line 500279
    iput-boolean v1, v0, LX/2oI;->n:Z

    .line 500280
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->A()Z

    move-result v1

    .line 500281
    iput-boolean v1, v0, LX/2oI;->o:Z

    .line 500282
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->D()Ljava/lang/String;

    move-result-object v1

    .line 500283
    iput-object v1, v0, LX/2oI;->p:Ljava/lang/String;

    .line 500284
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bL()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    move-result-object v1

    .line 500285
    iput-object v1, v0, LX/2oI;->q:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    .line 500286
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->F()J

    move-result-wide v2

    .line 500287
    iput-wide v2, v0, LX/2oI;->t:J

    .line 500288
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 500289
    iput-object v1, v0, LX/2oI;->u:Lcom/facebook/graphql/model/GraphQLStory;

    .line 500290
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bM()Z

    move-result v1

    .line 500291
    iput-boolean v1, v0, LX/2oI;->x:Z

    .line 500292
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->H()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    .line 500293
    iput-object v1, v0, LX/2oI;->y:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 500294
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 500295
    iput-object v1, v0, LX/2oI;->A:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 500296
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bN()D

    move-result-wide v2

    .line 500297
    iput-wide v2, v0, LX/2oI;->B:D

    .line 500298
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->L()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v1

    .line 500299
    iput-object v1, v0, LX/2oI;->D:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 500300
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->N()Z

    move-result v1

    .line 500301
    iput-boolean v1, v0, LX/2oI;->E:Z

    .line 500302
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->O()Z

    move-result v1

    .line 500303
    iput-boolean v1, v0, LX/2oI;->F:Z

    .line 500304
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->P()I

    move-result v1

    .line 500305
    iput v1, v0, LX/2oI;->G:I

    .line 500306
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Q()I

    move-result v1

    .line 500307
    iput v1, v0, LX/2oI;->H:I

    .line 500308
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bS()Ljava/lang/String;

    move-result-object v1

    .line 500309
    iput-object v1, v0, LX/2oI;->I:Ljava/lang/String;

    .line 500310
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bT()Ljava/lang/String;

    move-result-object v1

    .line 500311
    iput-object v1, v0, LX/2oI;->J:Ljava/lang/String;

    .line 500312
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->R()Ljava/lang/String;

    move-result-object v1

    .line 500313
    iput-object v1, v0, LX/2oI;->L:Ljava/lang/String;

    .line 500314
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v1

    .line 500315
    iput v1, v0, LX/2oI;->M:I

    .line 500316
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    .line 500317
    iput-object v1, v0, LX/2oI;->N:Ljava/lang/String;

    .line 500318
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500319
    iput-object v1, v0, LX/2oI;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500320
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500321
    iput-object v1, v0, LX/2oI;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500322
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500323
    iput-object v1, v0, LX/2oI;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500324
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500325
    iput-object v1, v0, LX/2oI;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500326
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500327
    iput-object v1, v0, LX/2oI;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500328
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500329
    iput-object v1, v0, LX/2oI;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500330
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500331
    iput-object v1, v0, LX/2oI;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500332
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500333
    iput-object v1, v0, LX/2oI;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500334
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ac()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500335
    iput-object v1, v0, LX/2oI;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500336
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500337
    iput-object v1, v0, LX/2oI;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500338
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500339
    iput-object v1, v0, LX/2oI;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500340
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->af()I

    move-result v1

    .line 500341
    iput v1, v0, LX/2oI;->aa:I

    .line 500342
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ag()I

    move-result v1

    .line 500343
    iput v1, v0, LX/2oI;->ab:I

    .line 500344
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ah()I

    move-result v1

    .line 500345
    iput v1, v0, LX/2oI;->ac:I

    .line 500346
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v1

    .line 500347
    iput-object v1, v0, LX/2oI;->ad:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 500348
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aj()LX/0Px;

    move-result-object v1

    .line 500349
    iput-object v1, v0, LX/2oI;->ae:LX/0Px;

    .line 500350
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ak()Z

    move-result v1

    .line 500351
    iput-boolean v1, v0, LX/2oI;->af:Z

    .line 500352
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->am()Z

    move-result v1

    .line 500353
    iput-boolean v1, v0, LX/2oI;->ag:Z

    .line 500354
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->an()Z

    move-result v1

    .line 500355
    iput-boolean v1, v0, LX/2oI;->ah:Z

    .line 500356
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bQ()Z

    move-result v1

    .line 500357
    iput-boolean v1, v0, LX/2oI;->aj:Z

    .line 500358
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v1

    .line 500359
    iput-boolean v1, v0, LX/2oI;->ak:Z

    .line 500360
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ap()Z

    move-result v1

    .line 500361
    iput-boolean v1, v0, LX/2oI;->al:Z

    .line 500362
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aq()Z

    move-result v1

    .line 500363
    iput-boolean v1, v0, LX/2oI;->am:Z

    .line 500364
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bY()Z

    move-result v1

    .line 500365
    iput-boolean v1, v0, LX/2oI;->an:Z

    .line 500366
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ar()Z

    move-result v1

    .line 500367
    iput-boolean v1, v0, LX/2oI;->ao:Z

    .line 500368
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->as()Z

    move-result v1

    .line 500369
    iput-boolean v1, v0, LX/2oI;->ap:Z

    .line 500370
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->at()Z

    move-result v1

    .line 500371
    iput-boolean v1, v0, LX/2oI;->aq:Z

    .line 500372
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v1

    .line 500373
    iput-boolean v1, v0, LX/2oI;->ar:Z

    .line 500374
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500375
    iput-object v1, v0, LX/2oI;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500376
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ax()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500377
    iput-object v1, v0, LX/2oI;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500378
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500379
    iput-object v1, v0, LX/2oI;->au:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500380
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bK()I

    move-result v1

    .line 500381
    iput v1, v0, LX/2oI;->ax:I

    .line 500382
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->az()I

    move-result v1

    .line 500383
    iput v1, v0, LX/2oI;->ay:I

    .line 500384
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aB()I

    move-result v1

    .line 500385
    iput v1, v0, LX/2oI;->az:I

    .line 500386
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aC()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500387
    iput-object v1, v0, LX/2oI;->aA:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500388
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bX()I

    move-result v1

    .line 500389
    iput v1, v0, LX/2oI;->aB:I

    .line 500390
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 500391
    iput-object v1, v0, LX/2oI;->aC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 500392
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aE()Ljava/lang/String;

    move-result-object v1

    .line 500393
    iput-object v1, v0, LX/2oI;->aE:Ljava/lang/String;

    .line 500394
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aG()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500395
    iput-object v1, v0, LX/2oI;->aF:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500396
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aH()Ljava/lang/String;

    move-result-object v1

    .line 500397
    iput-object v1, v0, LX/2oI;->aG:Ljava/lang/String;

    .line 500398
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500399
    iput-object v1, v0, LX/2oI;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500400
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500401
    iput-object v1, v0, LX/2oI;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500402
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bO()D

    move-result-wide v2

    .line 500403
    iput-wide v2, v0, LX/2oI;->aL:D

    .line 500404
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 500405
    iput-object v1, v0, LX/2oI;->aN:Lcom/facebook/graphql/model/GraphQLActor;

    .line 500406
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aO()I

    move-result v1

    .line 500407
    iput v1, v0, LX/2oI;->aP:I

    .line 500408
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aP()Ljava/lang/String;

    move-result-object v1

    .line 500409
    iput-object v1, v0, LX/2oI;->aQ:Ljava/lang/String;

    .line 500410
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aQ()I

    move-result v1

    .line 500411
    iput v1, v0, LX/2oI;->aR:I

    .line 500412
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aR()I

    move-result v1

    .line 500413
    iput v1, v0, LX/2oI;->aS:I

    .line 500414
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v1

    .line 500415
    iput-object v1, v0, LX/2oI;->aT:Ljava/lang/String;

    .line 500416
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aT()Ljava/lang/String;

    move-result-object v1

    .line 500417
    iput-object v1, v0, LX/2oI;->aW:Ljava/lang/String;

    .line 500418
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aU()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500419
    iput-object v1, v0, LX/2oI;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500420
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aV()I

    move-result v1

    .line 500421
    iput v1, v0, LX/2oI;->aY:I

    .line 500422
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aW()Ljava/lang/String;

    move-result-object v1

    .line 500423
    iput-object v1, v0, LX/2oI;->aZ:Ljava/lang/String;

    .line 500424
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aZ()Ljava/lang/String;

    move-result-object v1

    .line 500425
    iput-object v1, v0, LX/2oI;->bk:Ljava/lang/String;

    .line 500426
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ba()Ljava/lang/String;

    move-result-object v1

    .line 500427
    iput-object v1, v0, LX/2oI;->bl:Ljava/lang/String;

    .line 500428
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 500429
    iput-object v1, v0, LX/2oI;->bm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 500430
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500431
    iput-object v1, v0, LX/2oI;->bo:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500432
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bU()Ljava/lang/String;

    move-result-object v1

    .line 500433
    iput-object v1, v0, LX/2oI;->br:Ljava/lang/String;

    .line 500434
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bd()Z

    move-result v1

    .line 500435
    iput-boolean v1, v0, LX/2oI;->bs:Z

    .line 500436
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->be()Z

    move-result v1

    .line 500437
    iput-boolean v1, v0, LX/2oI;->bt:Z

    .line 500438
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bg()Z

    move-result v1

    .line 500439
    iput-boolean v1, v0, LX/2oI;->bu:Z

    .line 500440
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bh()Z

    move-result v1

    .line 500441
    iput-boolean v1, v0, LX/2oI;->bv:Z

    .line 500442
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bi()Z

    move-result v1

    .line 500443
    iput-boolean v1, v0, LX/2oI;->bw:Z

    .line 500444
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bj()D

    move-result-wide v2

    .line 500445
    iput-wide v2, v0, LX/2oI;->bx:D

    .line 500446
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bk()D

    move-result-wide v2

    .line 500447
    iput-wide v2, v0, LX/2oI;->by:D

    .line 500448
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bl()Ljava/lang/String;

    move-result-object v1

    .line 500449
    iput-object v1, v0, LX/2oI;->bz:Ljava/lang/String;

    .line 500450
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bm()Ljava/lang/String;

    move-result-object v1

    .line 500451
    iput-object v1, v0, LX/2oI;->bA:Ljava/lang/String;

    .line 500452
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bn()I

    move-result v1

    .line 500453
    iput v1, v0, LX/2oI;->bC:I

    .line 500454
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bV()Ljava/lang/String;

    move-result-object v1

    .line 500455
    iput-object v1, v0, LX/2oI;->bD:Ljava/lang/String;

    .line 500456
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bo()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    .line 500457
    iput-object v1, v0, LX/2oI;->bE:Lcom/facebook/graphql/model/GraphQLPage;

    .line 500458
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bp()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500459
    iput-object v1, v0, LX/2oI;->bF:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500460
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bq()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v1

    .line 500461
    iput-object v1, v0, LX/2oI;->bG:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 500462
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->br()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v1

    .line 500463
    iput-object v1, v0, LX/2oI;->bH:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 500464
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bs()Z

    move-result v1

    .line 500465
    iput-boolean v1, v0, LX/2oI;->bI:Z

    .line 500466
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bu()I

    move-result v1

    .line 500467
    iput v1, v0, LX/2oI;->bM:I

    .line 500468
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bv()LX/0Px;

    move-result-object v1

    .line 500469
    iput-object v1, v0, LX/2oI;->bP:LX/0Px;

    .line 500470
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bw()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v1

    .line 500471
    iput-object v1, v0, LX/2oI;->bR:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 500472
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bx()I

    move-result v1

    .line 500473
    iput v1, v0, LX/2oI;->bS:I

    .line 500474
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->by()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    move-result-object v1

    .line 500475
    iput-object v1, v0, LX/2oI;->bU:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    .line 500476
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bz()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v1

    .line 500477
    iput-object v1, v0, LX/2oI;->bV:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 500478
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 500479
    iput-object v1, v0, LX/2oI;->cc:Lcom/facebook/graphql/model/GraphQLImage;

    .line 500480
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v1

    .line 500481
    iput v1, v0, LX/2oI;->cd:I

    .line 500482
    invoke-virtual {v0}, LX/2oI;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    goto/16 :goto_0
.end method
