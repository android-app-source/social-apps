.class public LX/31l;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

.field public static final b:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Landroid/os/Handler;

.field public static d:I

.field private static e:I

.field private static final f:I

.field private static final g:[Lcom/facebook/android/maps/internal/GrandCentralDispatch$DispatchThread;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const v3, 0x7fffffff

    const/4 v1, 0x0

    .line 488077
    new-instance v0, Lcom/facebook/android/maps/internal/GrandCentralDispatch$1;

    invoke-direct {v0}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$1;-><init>()V

    sput-object v0, LX/31l;->a:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 488078
    new-instance v0, Ljava/util/concurrent/DelayQueue;

    invoke-direct {v0}, Ljava/util/concurrent/DelayQueue;-><init>()V

    sput-object v0, LX/31l;->b:Ljava/util/concurrent/BlockingQueue;

    .line 488079
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, LX/31l;->c:Landroid/os/Handler;

    .line 488080
    sput v3, LX/31l;->d:I

    .line 488081
    sput v3, LX/31l;->e:I

    .line 488082
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    const/4 v2, 0x3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 488083
    sput v0, LX/31l;->f:I

    new-array v0, v0, [Lcom/facebook/android/maps/internal/GrandCentralDispatch$DispatchThread;

    sput-object v0, LX/31l;->g:[Lcom/facebook/android/maps/internal/GrandCentralDispatch$DispatchThread;

    move v0, v1

    .line 488084
    :goto_0
    sget-object v2, LX/31l;->g:[Lcom/facebook/android/maps/internal/GrandCentralDispatch$DispatchThread;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 488085
    sget-object v2, LX/31l;->g:[Lcom/facebook/android/maps/internal/GrandCentralDispatch$DispatchThread;

    new-instance v3, Lcom/facebook/android/maps/internal/GrandCentralDispatch$DispatchThread;

    invoke-direct {v3}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$DispatchThread;-><init>()V

    aput-object v3, v2, v0

    .line 488086
    sget-object v2, LX/31l;->g:[Lcom/facebook/android/maps/internal/GrandCentralDispatch$DispatchThread;

    aget-object v2, v2, v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GCD-Thread #"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$DispatchThread;->setName(Ljava/lang/String;)V

    .line 488087
    sget-object v2, LX/31l;->g:[Lcom/facebook/android/maps/internal/GrandCentralDispatch$DispatchThread;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$DispatchThread;->start()V

    .line 488088
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 488089
    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 488075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488076
    return-void
.end method

.method public static a(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V
    .locals 4

    .prologue
    .line 488067
    sget v0, LX/31l;->d:I

    add-int/lit8 v1, v0, -0x1

    sput v1, LX/31l;->d:I

    int-to-long v0, v0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    .line 488068
    iput-wide v0, p0, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;->a:J

    .line 488069
    const/4 v0, 0x0

    .line 488070
    iput-object v0, p0, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;->c:Ljava/lang/String;

    .line 488071
    const-wide/16 v0, 0x0

    .line 488072
    iput-wide v0, p0, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;->b:J

    .line 488073
    sget-object v0, LX/31l;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p0}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 488074
    return-void
.end method

.method public static a(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;Ljava/lang/String;J)V
    .locals 8

    .prologue
    .line 488064
    sget-object v1, LX/31l;->c:Landroid/os/Handler;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    add-long v4, v2, p2

    const v6, -0x6d4dfab3

    move-object v2, p0

    move-object v3, p1

    .line 488065
    invoke-static {v2, v6}, LX/03a;->a(Ljava/lang/Runnable;I)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {v1, v0, v3, v4, v5}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;Ljava/lang/Object;J)Z

    .line 488066
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 488090
    sget-object v0, LX/31l;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 488091
    iget-object v2, v0, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;->c:Ljava/lang/String;

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 488092
    sget-object v2, LX/31l;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2, v0}, Ljava/util/concurrent/BlockingQueue;->remove(Ljava/lang/Object;)Z

    .line 488093
    invoke-virtual {v0}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;->a()V

    goto :goto_0

    .line 488094
    :cond_1
    return-void
.end method

.method public static b(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;J)V
    .locals 3

    .prologue
    .line 488062
    sget-object v0, LX/31l;->c:Landroid/os/Handler;

    const v1, -0x22d55248

    invoke-static {v0, p0, p1, p2, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 488063
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 488056
    sget-object v0, LX/31l;->c:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 488057
    return-void
.end method

.method public static c(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V
    .locals 2

    .prologue
    .line 488060
    sget-object v0, LX/31l;->c:Landroid/os/Handler;

    const v1, 0x56ef937c

    invoke-static {v0, p0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 488061
    return-void
.end method

.method public static d(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V
    .locals 1

    .prologue
    .line 488058
    sget-object v0, LX/31l;->c:Landroid/os/Handler;

    invoke-static {v0, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 488059
    return-void
.end method
