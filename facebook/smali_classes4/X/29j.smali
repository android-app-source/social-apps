.class public LX/29j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "LX/0Ot",
        "<+",
        "LX/0Up;",
        ">;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:[LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/0Ot",
            "<+",
            "LX/0Up;",
            ">;"
        }
    .end annotation
.end field

.field public final c:[Ljava/lang/Integer;

.field public final d:[Z

.field public final e:[Z

.field public final f:LX/0Uh;

.field private g:I

.field private h:I

.field public i:I


# direct methods
.method public constructor <init>([LX/0Ot;[Ljava/lang/Integer;[Z[ZLX/0Uh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LX/0Ot",
            "<+",
            "LX/0Up;",
            ">;[",
            "Ljava/lang/Integer;",
            "[Z[Z",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 376483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376484
    const-string v0, "GatekeerBackedIterator"

    iput-object v0, p0, LX/29j;->a:Ljava/lang/String;

    .line 376485
    iput v1, p0, LX/29j;->g:I

    .line 376486
    iput v1, p0, LX/29j;->h:I

    .line 376487
    iput v1, p0, LX/29j;->i:I

    .line 376488
    iput-object p1, p0, LX/29j;->b:[LX/0Ot;

    .line 376489
    iput-object p2, p0, LX/29j;->c:[Ljava/lang/Integer;

    .line 376490
    iput-object p3, p0, LX/29j;->d:[Z

    .line 376491
    iput-object p4, p0, LX/29j;->e:[Z

    .line 376492
    iget-object v0, p0, LX/29j;->b:[LX/0Ot;

    array-length v0, v0

    array-length v1, p2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/29j;->b:[LX/0Ot;

    array-length v0, v0

    array-length v1, p3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/29j;->b:[LX/0Ot;

    array-length v0, v0

    array-length v1, p4

    if-eq v0, v1, :cond_1

    .line 376493
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "length of arrays does not match up!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 376494
    :cond_1
    iput-object p5, p0, LX/29j;->f:LX/0Uh;

    .line 376495
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 376496
    iget v0, p0, LX/29j;->i:I

    iget v1, p0, LX/29j;->h:I

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/29j;->i:I

    iget-object v1, p0, LX/29j;->b:[LX/0Ot;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 376497
    :cond_0
    :goto_0
    return-void

    .line 376498
    :cond_1
    iget v0, p0, LX/29j;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/29j;->i:I

    iget-object v1, p0, LX/29j;->b:[LX/0Ot;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    .line 376499
    iget-object v1, p0, LX/29j;->c:[Ljava/lang/Integer;

    iget v2, p0, LX/29j;->i:I

    aget-object v1, v1, v2

    if-nez v1, :cond_3

    .line 376500
    :cond_2
    :goto_1
    move v0, v0

    .line 376501
    if-eqz v0, :cond_1

    goto :goto_0

    .line 376502
    :cond_3
    iget-object v1, p0, LX/29j;->f:LX/0Uh;

    iget-object v2, p0, LX/29j;->c:[Ljava/lang/Integer;

    iget v3, p0, LX/29j;->i:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, LX/29j;->d:[Z

    iget v4, p0, LX/29j;->i:I

    aget-boolean v3, v3, v4

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 376503
    iget-object v2, p0, LX/29j;->e:[Z

    iget v3, p0, LX/29j;->i:I

    aget-boolean v2, v2, v3

    .line 376504
    if-ne v2, v1, :cond_2

    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Ot",
            "<+",
            "LX/0Up;",
            ">;"
        }
    .end annotation

    .prologue
    .line 376505
    invoke-direct {p0}, LX/29j;->b()V

    .line 376506
    iget v0, p0, LX/29j;->i:I

    iput v0, p0, LX/29j;->g:I

    .line 376507
    iget v0, p0, LX/29j;->i:I

    iput v0, p0, LX/29j;->h:I

    .line 376508
    iget-object v0, p0, LX/29j;->b:[LX/0Ot;

    iget v1, p0, LX/29j;->h:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 376509
    invoke-direct {p0}, LX/29j;->b()V

    .line 376510
    iget v0, p0, LX/29j;->i:I

    iget-object v1, p0, LX/29j;->b:[LX/0Ot;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 376511
    invoke-virtual {p0}, LX/29j;->a()LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 3

    .prologue
    .line 376512
    iget v0, p0, LX/29j;->g:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/29j;->g:I

    iget-object v1, p0, LX/29j;->b:[LX/0Ot;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 376513
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Need to call next() before calling remove()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 376514
    :cond_1
    iget-object v0, p0, LX/29j;->b:[LX/0Ot;

    iget v1, p0, LX/29j;->g:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 376515
    const/4 v0, -0x1

    iput v0, p0, LX/29j;->g:I

    .line 376516
    return-void
.end method
