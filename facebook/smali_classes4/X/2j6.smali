.class public LX/2j6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2j1;


# instance fields
.field private final a:LX/0SG;

.field public final b:LX/2iz;

.field private final c:LX/2j3;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CfU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2iz;LX/0SG;LX/2j3;LX/0Ot;)V
    .locals 0
    .param p1    # LX/2iz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2iz;",
            "LX/0SG;",
            "LX/2j3;",
            "LX/0Ot",
            "<",
            "LX/CfU;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 452631
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 452632
    iput-object p1, p0, LX/2j6;->b:LX/2iz;

    .line 452633
    iput-object p2, p0, LX/2j6;->a:LX/0SG;

    .line 452634
    iput-object p3, p0, LX/2j6;->c:LX/2j3;

    .line 452635
    iput-object p4, p0, LX/2j6;->d:LX/0Ot;

    .line 452636
    return-void
.end method

.method private a(LX/2jY;LX/9qT;JJZ)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 452704
    invoke-virtual {p1, p2}, LX/2jY;->a(LX/9qT;)V

    .line 452705
    instance-of v0, p2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    if-eqz v0, :cond_0

    .line 452706
    check-cast p2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/2jY;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;)V

    .line 452707
    :cond_0
    invoke-virtual {p1}, LX/2jY;->E()V

    .line 452708
    if-eqz p7, :cond_2

    .line 452709
    iget-object v0, p1, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v0, v0

    .line 452710
    if-eqz v0, :cond_1

    .line 452711
    iget-object v0, p1, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v0, v0

    .line 452712
    iget-boolean v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->A:Z

    move v0, v1

    .line 452713
    if-nez v0, :cond_2

    .line 452714
    :cond_1
    iput-boolean v4, p1, LX/2jY;->o:Z

    .line 452715
    :cond_2
    iput-wide p3, p1, LX/2jY;->g:J

    .line 452716
    iput-wide p5, p1, LX/2jY;->h:J

    .line 452717
    iget-object v0, p0, LX/2j6;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 452718
    iput-wide v0, p1, LX/2jY;->i:J

    .line 452719
    iget-boolean v0, p1, LX/2jY;->q:Z

    move v0, v0

    .line 452720
    invoke-virtual {p1}, LX/2jY;->A()Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez v0, :cond_4

    .line 452721
    invoke-virtual {p1}, LX/2jY;->t()LX/CfG;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, LX/2jY;->t()LX/CfG;

    move-result-object v0

    invoke-interface {v0}, LX/CfG;->kJ_()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 452722
    invoke-virtual {p1}, LX/2jY;->t()LX/CfG;

    move-result-object v0

    invoke-interface {v0}, LX/CfG;->kL_()V

    .line 452723
    :cond_3
    :goto_0
    return-void

    .line 452724
    :cond_4
    if-eqz v0, :cond_5

    .line 452725
    iput-boolean v4, p1, LX/2jY;->q:Z

    .line 452726
    iget-object v0, p0, LX/2j6;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CfU;

    iget-object v1, p0, LX/2j6;->b:LX/2iz;

    .line 452727
    iget-object v2, p1, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v2, v2

    .line 452728
    invoke-virtual {v0, v1, p1, v2}, LX/CfU;->a(LX/2iz;LX/2jY;Lcom/facebook/reaction/ReactionQueryParams;)Z

    move-result v0

    .line 452729
    if-nez v0, :cond_3

    .line 452730
    :cond_5
    iget-boolean v0, p1, LX/2jY;->l:Z

    move v0, v0

    .line 452731
    if-eqz v0, :cond_3

    .line 452732
    iget-wide v5, p1, LX/2jY;->i:J

    move-wide v0, v5

    .line 452733
    iget-wide v5, p1, LX/2jY;->j:J

    move-wide v2, v5

    .line 452734
    sub-long/2addr v0, v2

    .line 452735
    const-wide/16 v2, 0xfa0

    cmp-long v2, v0, v2

    if-lez v2, :cond_6

    .line 452736
    iget-object v2, p0, LX/2j6;->b:LX/2iz;

    .line 452737
    invoke-virtual {p1}, LX/2jY;->r()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v2, p1, v0, v1, v5}, LX/2iz;->a(LX/2iz;LX/2jY;JLjava/lang/Long;)V

    .line 452738
    invoke-static {v2}, LX/2iz;->a(LX/2iz;)V

    .line 452739
    iget-object v5, p1, LX/2jY;->a:Ljava/lang/String;

    move-object v5, v5

    .line 452740
    invoke-virtual {v2, v5}, LX/2iz;->g(Ljava/lang/String;)LX/2jY;

    .line 452741
    goto :goto_0

    .line 452742
    :cond_6
    iput-boolean v4, p1, LX/2jY;->l:Z

    .line 452743
    iget-object v0, p0, LX/2j6;->b:LX/2iz;

    invoke-virtual {v0, p1}, LX/2iz;->a(LX/2jY;)V

    goto :goto_0
.end method

.method private a(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 2
    .param p3    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 452695
    invoke-static {p1, p2, p3}, LX/2j6;->c(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452696
    invoke-virtual {p1}, LX/2jY;->t()LX/CfG;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, LX/2jY;->t()LX/CfG;

    move-result-object v0

    invoke-interface {v0}, LX/CfG;->kJ_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 452697
    invoke-virtual {p1}, LX/2jY;->t()LX/CfG;

    move-result-object v0

    invoke-interface {v0}, LX/CfG;->kK_()V

    .line 452698
    :cond_0
    :goto_0
    return-void

    .line 452699
    :cond_1
    iget-boolean v0, p1, LX/2jY;->l:Z

    move v0, v0

    .line 452700
    if-nez v0, :cond_2

    .line 452701
    iget-boolean v0, p1, LX/2jY;->q:Z

    move v0, v0

    .line 452702
    if-eqz v0, :cond_0

    .line 452703
    :cond_2
    iget-object v0, p0, LX/2j6;->b:LX/2iz;

    invoke-virtual {p1}, LX/2jY;->G()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, LX/2iz;->a(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0
.end method

.method public static c(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0
    .param p2    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 452690
    iput-object p1, p0, LX/2jY;->e:Ljava/lang/String;

    .line 452691
    iput-object p2, p0, LX/2jY;->f:Ljava/lang/Long;

    .line 452692
    invoke-virtual {p0}, LX/2jY;->a()V

    .line 452693
    invoke-virtual {p0}, LX/2jY;->E()V

    .line 452694
    return-void
.end method


# virtual methods
.method public final a(LX/2jY;)V
    .locals 2

    .prologue
    .line 452688
    const-string v0, "EMPTY_REQUEST"

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/2j6;->a(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452689
    return-void
.end method

.method public final a(LX/2jY;LX/2jQ;)V
    .locals 8

    .prologue
    .line 452680
    iget-object v0, p0, LX/2j6;->c:LX/2j3;

    .line 452681
    iget-object v1, p1, LX/2jY;->a:Ljava/lang/String;

    move-object v1, v1

    .line 452682
    iget-object v2, p1, LX/2jY;->b:Ljava/lang/String;

    move-object v2, v2

    .line 452683
    invoke-virtual {p1}, LX/2jY;->e()Ljava/lang/String;

    move-result-object v3

    .line 452684
    iget-object v4, p2, LX/2jQ;->a:Ljava/lang/Throwable;

    move-object v4, v4

    .line 452685
    iget-object v5, v0, LX/2j3;->a:LX/0Zb;

    sget-object v6, LX/Cff;->REACTION_OVERLAY_ERROR:LX/Cff;

    const-string v7, "reaction_overlay"

    invoke-static {v6, v1, v7, v2}, LX/2j3;->a(LX/Cff;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "error"

    const-string p2, "NETWORK_FAILURE"

    invoke-virtual {v6, v7, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "error_message"

    invoke-virtual {v6, v7, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "gravity_suggestifier_id"

    invoke-virtual {v6, v7, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 452686
    const-string v0, "NETWORK_FAILURE"

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/2j6;->a(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452687
    return-void
.end method

.method public final a(LX/2jY;LX/2jS;)V
    .locals 8

    .prologue
    .line 452671
    iget-object v0, p0, LX/2j6;->c:LX/2j3;

    .line 452672
    iget-object v1, p1, LX/2jY;->a:Ljava/lang/String;

    move-object v1, v1

    .line 452673
    iget-object v2, p1, LX/2jY;->b:Ljava/lang/String;

    move-object v2, v2

    .line 452674
    invoke-virtual {p1}, LX/2jY;->e()Ljava/lang/String;

    move-result-object v3

    .line 452675
    iget-object v4, p2, LX/2jS;->a:Ljava/lang/String;

    move-object v4, v4

    .line 452676
    iget-object v5, v0, LX/2j3;->a:LX/0Zb;

    sget-object v6, LX/Cff;->REACTION_OVERLAY_ERROR:LX/Cff;

    const-string v7, "reaction_overlay"

    invoke-static {v6, v1, v7, v2}, LX/2j3;->a(LX/Cff;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "error"

    invoke-virtual {v6, v7, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "gravity_suggestifier_id"

    invoke-virtual {v6, v7, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 452677
    iget-object v0, p2, LX/2jS;->a:Ljava/lang/String;

    move-object v0, v0

    .line 452678
    iget-object v1, p0, LX/2j6;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, LX/2j6;->a(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452679
    return-void
.end method

.method public final a(LX/2jY;LX/2jT;)V
    .locals 4

    .prologue
    .line 452666
    iget-object v0, p2, LX/2jT;->a:Ljava/lang/String;

    move-object v0, v0

    .line 452667
    iget-object v1, p0, LX/2j6;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 452668
    invoke-static {p1, v0, v1}, LX/2j6;->c(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452669
    iget-object v2, p0, LX/2j6;->b:LX/2iz;

    invoke-virtual {p1}, LX/2jY;->G()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, p1, v0, v3}, LX/2iz;->a(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452670
    return-void
.end method

.method public final a(LX/2jY;LX/2jV;)V
    .locals 11

    .prologue
    .line 452658
    iget-object v0, p2, LX/2jV;->c:LX/9qT;

    move-object v0, v0

    .line 452659
    if-nez v0, :cond_0

    const/4 v3, 0x0

    .line 452660
    :goto_0
    iget-wide v9, p2, LX/2jV;->a:J

    move-wide v4, v9

    .line 452661
    iget-wide v9, p2, LX/2jV;->b:J

    move-wide v6, v9

    .line 452662
    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v8}, LX/2j6;->a(LX/2jY;LX/9qT;JJZ)V

    .line 452663
    return-void

    .line 452664
    :cond_0
    iget-object v0, p2, LX/2jV;->c:LX/9qT;

    move-object v3, v0

    .line 452665
    goto :goto_0
.end method

.method public final a(LX/2jY;LX/2jW;)V
    .locals 11

    .prologue
    .line 452650
    iget-object v0, p2, LX/2jW;->c:LX/9qT;

    move-object v0, v0

    .line 452651
    if-nez v0, :cond_0

    const/4 v3, 0x0

    .line 452652
    :goto_0
    iget-wide v9, p2, LX/2jW;->a:J

    move-wide v4, v9

    .line 452653
    iget-wide v9, p2, LX/2jW;->b:J

    move-wide v6, v9

    .line 452654
    const/4 v8, 0x1

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v8}, LX/2j6;->a(LX/2jY;LX/9qT;JJZ)V

    .line 452655
    return-void

    .line 452656
    :cond_0
    iget-object v0, p2, LX/2jW;->c:LX/9qT;

    move-object v3, v0

    .line 452657
    goto :goto_0
.end method

.method public final a(LX/2jY;LX/2jX;)V
    .locals 4

    .prologue
    .line 452637
    invoke-virtual {p1}, LX/2jY;->a()V

    .line 452638
    const/4 v0, 0x0

    .line 452639
    iput-object v0, p1, LX/2jY;->e:Ljava/lang/String;

    .line 452640
    iget-object v0, p2, LX/2jX;->a:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v0, v0

    .line 452641
    iput-object v0, p1, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    .line 452642
    const/4 v0, 0x1

    .line 452643
    iput-boolean v0, p1, LX/2jY;->n:Z

    .line 452644
    iget-object v0, p0, LX/2j6;->c:LX/2j3;

    .line 452645
    iget-object v1, p1, LX/2jY;->a:Ljava/lang/String;

    move-object v1, v1

    .line 452646
    iget-object v2, p1, LX/2jY;->b:Ljava/lang/String;

    move-object v2, v2

    .line 452647
    invoke-virtual {p1}, LX/2jY;->e()Ljava/lang/String;

    move-result-object v3

    .line 452648
    iget-object p0, v0, LX/2j3;->a:LX/0Zb;

    sget-object p1, LX/Cff;->REACTION_FETCH:LX/Cff;

    const-string p2, "reaction_overlay"

    invoke-static {p1, v1, p2, v2}, LX/2j3;->a(LX/Cff;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "gravity_suggestifier_id"

    invoke-virtual {p1, p2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 452649
    return-void
.end method
