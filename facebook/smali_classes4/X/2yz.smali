.class public final LX/2yz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/resources/impl/WaitingForStringsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/resources/impl/WaitingForStringsActivity;)V
    .locals 0

    .prologue
    .line 482840
    iput-object p1, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 482865
    iget-object v0, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    iget-object v0, v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->f:LX/0Wn;

    .line 482866
    iget-object v1, v0, LX/0Wn;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    const-string v2, "fbresources_waiting_complete"

    invoke-interface {v1, v2}, LX/0Zb;->a(Ljava/lang/String;)V

    .line 482867
    iget-object v0, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    .line 482868
    invoke-static {v0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->c$redex0(Lcom/facebook/resources/impl/WaitingForStringsActivity;)V

    .line 482869
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 482842
    sget-object v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->a:Ljava/lang/String;

    const-string v1, "Failed to fetch string from server."

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 482843
    iget-object v0, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    const/4 v1, 0x1

    .line 482844
    iput-boolean v1, v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->p:Z

    .line 482845
    iget-object v0, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    iget-object v0, v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 482846
    iget-object v0, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    iget-object v0, v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 482847
    iget-object v0, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    iget-object v0, v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->f:LX/0Wn;

    .line 482848
    const v1, 0x440004

    const-string v2, "FbResourcesWaitingActivity"

    invoke-static {v0, v1, v2}, LX/0Wn;->c(LX/0Wn;ILjava/lang/String;)V

    .line 482849
    iget-object v0, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    iget-object v0, v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->f:LX/0Wn;

    sget-object v1, LX/0ec;->DOWNLOADED:LX/0ec;

    iget-object v2, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    iget-object v2, v2, Lcom/facebook/resources/impl/WaitingForStringsActivity;->b:LX/0Vv;

    .line 482850
    iget-object v5, v2, LX/0Vv;->m:LX/0ed;

    move-object v2, v5

    .line 482851
    invoke-virtual {v0, v1, v2, p1}, LX/0Wn;->a(LX/0ec;LX/0ed;Ljava/lang/Throwable;)V

    .line 482852
    instance-of v0, p1, LX/K1k;

    .line 482853
    iget-object v1, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    iget-boolean v1, v1, Lcom/facebook/resources/impl/WaitingForStringsActivity;->q:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_2

    .line 482854
    :cond_0
    iget-object v0, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    iget-object v0, v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->l:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 482855
    iget-object v0, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    iget-object v0, v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->m:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 482856
    :goto_0
    const-string v1, "Could not fetch strings from server: "

    .line 482857
    sget-object v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->a:Ljava/lang/String;

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 482858
    iget-object v0, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    iget-object v0, v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->r:LX/0Uh;

    const/16 v2, 0x4fc

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 482859
    if-eqz v0, :cond_1

    .line 482860
    iget-object v0, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    iget-object v0, v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    .line 482861
    new-instance v2, LX/27k;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Internal build only: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 482862
    :cond_1
    return-void

    .line 482863
    :cond_2
    iget-object v0, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    iget-object v0, v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->l:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 482864
    iget-object v0, p0, LX/2yz;->a:Lcom/facebook/resources/impl/WaitingForStringsActivity;

    iget-object v0, v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 482841
    invoke-direct {p0}, LX/2yz;->a()V

    return-void
.end method
