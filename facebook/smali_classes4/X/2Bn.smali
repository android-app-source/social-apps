.class public LX/2Bn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/421;",
        "Lcom/facebook/auth/component/AuthenticationResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0dC;

.field private final b:LX/2Xm;


# direct methods
.method public constructor <init>(LX/0dC;LX/2Xm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 381446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 381447
    iput-object p2, p0, LX/2Bn;->b:LX/2Xm;

    .line 381448
    iput-object p1, p0, LX/2Bn;->a:LX/0dC;

    .line 381449
    return-void
.end method

.method public static a(LX/0QB;)LX/2Bn;
    .locals 3

    .prologue
    .line 381450
    new-instance v2, LX/2Bn;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v0

    check-cast v0, LX/0dC;

    invoke-static {p0}, LX/2Xm;->b(LX/0QB;)LX/2Xm;

    move-result-object v1

    check-cast v1, LX/2Xm;

    invoke-direct {v2, v0, v1}, LX/2Bn;-><init>(LX/0dC;LX/2Xm;)V

    .line 381451
    move-object v0, v2

    .line 381452
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 381453
    check-cast p1, LX/421;

    .line 381454
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 381455
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381456
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "phone_number"

    iget-object v3, p1, LX/421;->a:Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;

    iget-object v3, v3, Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381457
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "first_name"

    iget-object v3, p1, LX/421;->a:Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;

    iget-object v3, v3, Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381458
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "last_name"

    iget-object v3, p1, LX/421;->a:Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;

    iget-object v3, v3, Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381459
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_id"

    iget-object v3, p0, LX/2Bn;->a:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381460
    iget-object v1, p1, LX/421;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 381461
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "machine_id"

    iget-object v3, p1, LX/421;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381462
    :goto_0
    iget-boolean v1, p1, LX/421;->b:Z

    if-eqz v1, :cond_0

    .line 381463
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "generate_session_cookies"

    const-string v3, "1"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381464
    :cond_0
    iget-boolean v1, p1, LX/421;->c:Z

    if-eqz v1, :cond_1

    .line 381465
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "seek_match"

    const-string v3, "1"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381466
    :cond_1
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "account_recovery_id"

    iget-object v3, p1, LX/421;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381467
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    sget-object v2, LX/11I;->CREATE_MESSENGER_ACCOUNT:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    .line 381468
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 381469
    move-object v1, v1

    .line 381470
    const-string v2, "POST"

    .line 381471
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 381472
    move-object v1, v1

    .line 381473
    const-string v2, "method/user.createMessengerOnlyAccount"

    .line 381474
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 381475
    move-object v1, v1

    .line 381476
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 381477
    move-object v0, v1

    .line 381478
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 381479
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 381480
    move-object v0, v0

    .line 381481
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 381482
    :cond_2
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "generate_machine_id"

    const-string v3, "1"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 381483
    check-cast p1, LX/421;

    .line 381484
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 381485
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 381486
    const-string v1, "suggested_facebook_user"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 381487
    const-string v1, "suggested_facebook_user"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 381488
    const-string v1, "account_id"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 381489
    new-instance v1, Lcom/facebook/auth/protocol/SuggestedFacebookAccountInfo;

    const-string v2, "account_id"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "first_name"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "last_name"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "profile_pic"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/facebook/auth/protocol/SuggestedFacebookAccountInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 381490
    new-instance v0, LX/42R;

    invoke-direct {v0, v1}, LX/42R;-><init>(Lcom/facebook/auth/protocol/SuggestedFacebookAccountInfo;)V

    throw v0

    .line 381491
    :cond_0
    iget-object v0, p1, LX/421;->a:Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;

    iget-object v0, v0, Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;->a:Ljava/lang/String;

    .line 381492
    iget-object v1, p0, LX/2Bn;->b:LX/2Xm;

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v2

    iget-boolean v3, p1, LX/421;->b:Z

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, LX/2Xm;->a(LX/0lF;Ljava/lang/String;ZLjava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method
