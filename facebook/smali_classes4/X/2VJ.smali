.class public abstract LX/2VJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2VK;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2Vj",
            "<**>;>;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Exception;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/2VL;

.field public f:LX/4cv;

.field public g:LX/4cw;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 417319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417320
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/2VJ;->a:Ljava/util/List;

    .line 417321
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/2VJ;->b:Ljava/util/List;

    .line 417322
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2VJ;->c:Ljava/util/Map;

    .line 417323
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2VJ;->d:Ljava/util/Map;

    .line 417324
    sget-object v0, LX/2VL;->NORMAL:LX/2VL;

    iput-object v0, p0, LX/2VJ;->e:LX/2VL;

    return-void
.end method


# virtual methods
.method public final a(LX/2VL;)LX/2VK;
    .locals 0

    .prologue
    .line 417328
    iput-object p1, p0, LX/2VJ;->e:LX/2VL;

    .line 417329
    return-object p0
.end method

.method public final a(LX/4cv;)LX/2VK;
    .locals 0

    .prologue
    .line 417326
    iput-object p1, p0, LX/2VJ;->f:LX/4cv;

    .line 417327
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 417325
    iget-object v0, p0, LX/2VJ;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2Vj",
            "<**>;>;"
        }
    .end annotation

    .prologue
    .line 417318
    iget-object v0, p0, LX/2VJ;->a:Ljava/util/List;

    return-object v0
.end method

.method public final a(LX/2Vj;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/2Vj",
            "<TPARAMS;TRESU",
            "LT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 417330
    iget-object v0, p0, LX/2VJ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417331
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 417316
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/2VJ;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;LX/14U;)V

    .line 417317
    return-void
.end method

.method public final b()LX/2VL;
    .locals 1

    .prologue
    .line 417312
    iget-object v0, p0, LX/2VJ;->e:LX/2VL;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/Exception;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 417315
    iget-object v0, p0, LX/2VJ;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 417314
    iget-object v0, p0, LX/2VJ;->f:LX/4cv;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()LX/4cw;
    .locals 1

    .prologue
    .line 417313
    iget-object v0, p0, LX/2VJ;->g:LX/4cw;

    return-object v0
.end method
