.class public final enum LX/2Jq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Jq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Jq;

.field public static final enum LOGGED_IN:LX/2Jq;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 393218
    new-instance v0, LX/2Jq;

    const-string v1, "LOGGED_IN"

    invoke-direct {v0, v1, v2}, LX/2Jq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Jq;->LOGGED_IN:LX/2Jq;

    .line 393219
    const/4 v0, 0x1

    new-array v0, v0, [LX/2Jq;

    sget-object v1, LX/2Jq;->LOGGED_IN:LX/2Jq;

    aput-object v1, v0, v2

    sput-object v0, LX/2Jq;->$VALUES:[LX/2Jq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 393217
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Jq;
    .locals 1

    .prologue
    .line 393221
    const-class v0, LX/2Jq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Jq;

    return-object v0
.end method

.method public static values()[LX/2Jq;
    .locals 1

    .prologue
    .line 393220
    sget-object v0, LX/2Jq;->$VALUES:[LX/2Jq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Jq;

    return-object v0
.end method
