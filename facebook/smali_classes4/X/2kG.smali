.class public LX/2kG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0po;
.implements LX/2k1;
.implements LX/0sk;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/2kG;


# instance fields
.field private a:J

.field private final b:LX/0SG;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final e:LX/0t9;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/9JS;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0vV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vV",
            "<",
            "Ljava/lang/String;",
            "LX/2kR;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/2kD;

.field private i:Ljava/io/File;

.field private j:Z

.field private k:J


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0SG;LX/0pr;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0t9;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 455752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 455753
    iput-wide v2, p0, LX/2kG;->a:J

    .line 455754
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2kG;->f:Ljava/util/Map;

    .line 455755
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v0

    iput-object v0, p0, LX/2kG;->g:LX/0vV;

    .line 455756
    iput-object v1, p0, LX/2kG;->h:LX/2kD;

    .line 455757
    iput-object v1, p0, LX/2kG;->i:Ljava/io/File;

    .line 455758
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2kG;->j:Z

    .line 455759
    iput-wide v2, p0, LX/2kG;->k:J

    .line 455760
    iput-object p1, p0, LX/2kG;->c:Landroid/content/Context;

    .line 455761
    iput-object p2, p0, LX/2kG;->b:LX/0SG;

    .line 455762
    iput-object p4, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 455763
    iput-object p5, p0, LX/2kG;->e:LX/0t9;

    .line 455764
    invoke-interface {p3, p0}, LX/0pr;->a(LX/0po;)V

    .line 455765
    return-void
.end method

.method public static a(LX/0QB;)LX/2kG;
    .locals 9

    .prologue
    .line 455766
    sget-object v0, LX/2kG;->l:LX/2kG;

    if-nez v0, :cond_1

    .line 455767
    const-class v1, LX/2kG;

    monitor-enter v1

    .line 455768
    :try_start_0
    sget-object v0, LX/2kG;->l:LX/2kG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 455769
    if-eqz v2, :cond_0

    .line 455770
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 455771
    new-instance v3, LX/2kG;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v6

    check-cast v6, LX/0pr;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v7

    check-cast v7, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0t9;->b(LX/0QB;)LX/0t9;

    move-result-object v8

    check-cast v8, LX/0t9;

    invoke-direct/range {v3 .. v8}, LX/2kG;-><init>(Landroid/content/Context;LX/0SG;LX/0pr;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0t9;)V

    .line 455772
    move-object v0, v3

    .line 455773
    sput-object v0, LX/2kG;->l:LX/2kG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 455774
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 455775
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 455776
    :cond_1
    sget-object v0, LX/2kG;->l:LX/2kG;

    return-object v0

    .line 455777
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 455778
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "LX/9JQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 455779
    invoke-static {p0}, LX/2kG;->g(LX/2kG;)V

    .line 455780
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 455781
    iget-object v0, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JS;

    .line 455782
    if-eqz p2, :cond_1

    .line 455783
    iget-object v1, v0, LX/9JS;->a:Ljava/lang/String;

    move-object v1, v1

    .line 455784
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 455785
    :cond_1
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 455786
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 455787
    iget-object v4, v0, LX/9JS;->e:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    .line 455788
    if-eqz v1, :cond_2

    .line 455789
    array-length v7, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v7, :cond_2

    aget v8, v1, v4

    .line 455790
    const/4 p0, -0x1

    if-eq v8, p0, :cond_3

    .line 455791
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 455792
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 455793
    :cond_4
    move-object v1, v5

    .line 455794
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 455795
    iget-object v5, v0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/9JQ;

    move-object v1, v5

    .line 455796
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 455797
    :cond_5
    return-object v2
.end method

.method private static a(LX/2kG;Ljava/util/Map;Ljava/lang/Boolean;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "LX/9JT;",
            ">;>;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 455798
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 455799
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    invoke-static {v1}, LX/1Yt;->a(I)LX/1Yt;

    move-result-object v1

    move-object v4, v1

    .line 455800
    :goto_1
    iget-object v1, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9JS;

    .line 455801
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v3, 0x0

    move v6, v3

    :goto_2
    if-ge v6, v8, :cond_4

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9JT;

    .line 455802
    iget-object v9, v1, LX/9JS;->c:LX/9JP;

    if-eqz v9, :cond_1

    .line 455803
    invoke-static {v1, v3}, LX/9JS;->b(LX/9JS;LX/9JT;)V

    .line 455804
    :cond_1
    invoke-static {v1, v3}, LX/9JS;->c(LX/9JS;LX/9JT;)Z

    .line 455805
    if-eqz v4, :cond_2

    .line 455806
    iget v9, v3, LX/9JT;->b:I

    move v3, v9

    .line 455807
    int-to-long v10, v3

    invoke-virtual {v4, v10, v11}, LX/1Yt;->a(J)V

    .line 455808
    :cond_2
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_2

    :cond_3
    move-object v4, v5

    .line 455809
    goto :goto_1

    .line 455810
    :cond_4
    if-eqz v4, :cond_0

    .line 455811
    invoke-direct {p0, v0, v4, v5}, LX/2kG;->a(Ljava/lang/String;LX/1Yt;LX/1Yt;)V

    goto :goto_0

    .line 455812
    :cond_5
    return-void
.end method

.method private static a(LX/9JS;Ljava/io/File;)V
    .locals 6

    .prologue
    .line 455813
    invoke-virtual {p0}, LX/9JS;->k()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JQ;

    .line 455814
    iget-object v4, v0, LX/9JQ;->b:LX/9JR;

    move-object v4, v4

    .line 455815
    if-eqz v4, :cond_0

    .line 455816
    new-instance v4, Ljava/io/File;

    .line 455817
    iget-object v5, v0, LX/9JQ;->b:LX/9JR;

    move-object v5, v5

    .line 455818
    invoke-virtual {v5}, LX/9JR;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 455819
    :cond_0
    new-instance v4, Ljava/io/File;

    .line 455820
    iget-object v5, v0, LX/9JQ;->a:LX/9JR;

    move-object v0, v5

    .line 455821
    invoke-virtual {v0}, LX/9JR;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 455822
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 455823
    :cond_1
    invoke-virtual {p0}, LX/9JS;->l()V

    .line 455824
    return-void
.end method

.method private a(Ljava/lang/String;LX/1Yt;LX/1Yt;)V
    .locals 6
    .param p2    # LX/1Yt;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/1Yt;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 455825
    new-instance v3, Landroid/os/Bundle;

    const/4 v0, 0x1

    invoke-direct {v3, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 455826
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/1Yt;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 455827
    const-string v0, "CHANGED_ROW_IDS"

    invoke-virtual {p2}, LX/1Yt;->b()[J

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 455828
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, LX/1Yt;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 455829
    const-string v0, "DELETED_ROW_IDS"

    invoke-virtual {p3}, LX/1Yt;->b()[J

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 455830
    :cond_1
    const-string v1, "SESSION_VERSION"

    iget-object v0, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JS;

    invoke-virtual {v0}, LX/9JS;->n()I

    move-result v0

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 455831
    iget-object v0, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JS;

    .line 455832
    if-nez v0, :cond_3

    .line 455833
    :cond_2
    return-void

    .line 455834
    :cond_3
    iget-object v1, p0, LX/2kG;->g:LX/0vV;

    invoke-virtual {v1, p1}, LX/0vW;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2kR;

    .line 455835
    new-instance v5, LX/9JV;

    invoke-direct {v5, v0}, LX/9JV;-><init>(LX/9JS;)V

    .line 455836
    invoke-virtual {p0, v5}, LX/2kG;->a(LX/2kb;)LX/2nf;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2nf;

    .line 455837
    invoke-interface {v2, v5}, LX/2nf;->a(LX/2kb;)V

    .line 455838
    if-eqz v2, :cond_4

    .line 455839
    invoke-interface {v2}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 455840
    :cond_4
    invoke-interface {v1, v2}, LX/2kR;->a(LX/2nf;)V

    goto :goto_0
.end method

.method private declared-synchronized b(LX/2kb;Ljava/nio/ByteBuffer;LX/95b;JLcom/facebook/graphql/cursor/edgestore/PageInfo;Z)Landroid/os/Bundle;
    .locals 12
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 455876
    monitor-enter p0

    :try_start_0
    check-cast p1, LX/9JV;

    invoke-virtual {p1}, LX/9JV;->b()LX/9JS;

    move-result-object v11

    .line 455877
    iget-object v2, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850009

    invoke-interface {v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 455878
    iget-object v2, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850009

    const-string v4, "SimpleEdgeStore"

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 455879
    iget-object v2, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850009

    invoke-virtual {v11}, LX/9JS;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455880
    :try_start_1
    invoke-static {p0}, LX/2kG;->c(LX/2kG;)LX/2kD;

    move-result-object v3

    if-eqz p3, :cond_2

    invoke-interface {p3}, LX/95b;->a()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v2, 0x0

    invoke-interface {p3, v2}, LX/95b;->b(I)LX/0w5;

    move-result-object v5

    :goto_0
    iget-object v2, p0, LX/2kG;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v6

    const/4 v8, 0x0

    move-object v4, p2

    invoke-virtual/range {v3 .. v8}, LX/2kD;->b(Ljava/nio/ByteBuffer;LX/0w5;JZ)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    .line 455881
    :try_start_2
    iget-object v2, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x850009

    const/16 v5, 0xc8

    invoke-interface {v2, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 455882
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 455883
    iget-wide v4, p0, LX/2kG;->k:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/2kG;->k:J

    .line 455884
    const-string v2, "CHANGE_NUMBER"

    invoke-virtual {v10, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 455885
    if-eqz p7, :cond_3

    .line 455886
    :try_start_3
    invoke-virtual {v11}, LX/9JS;->g()[J

    move-result-object v2

    .line 455887
    array-length v4, v2

    if-lez v4, :cond_0

    .line 455888
    const-string v4, "DELETED_ROW_IDS"

    invoke-virtual {v10, v4, v2}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 455889
    :cond_0
    invoke-static {p0}, LX/2kG;->c(LX/2kG;)LX/2kD;

    move-result-object v2

    invoke-virtual {v2}, LX/2kD;->a()Ljava/io/File;

    move-result-object v2

    invoke-static {v11, v2}, LX/2kG;->a(LX/9JS;Ljava/io/File;)V

    .line 455890
    :cond_1
    :goto_1
    if-eqz p3, :cond_8

    invoke-interface {p3}, LX/95b;->a()I

    move-result v2

    if-lez v2, :cond_8

    .line 455891
    invoke-virtual {v11, p3, v3}, LX/9JS;->a(LX/95b;I)Ljava/util/ArrayList;

    move-result-object v4

    .line 455892
    iget-object v2, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850009

    const/16 v5, 0x1e

    invoke-interface {v2, v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 455893
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v5, v2, [J

    .line 455894
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_7

    .line 455895
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9JQ;

    invoke-virtual {v2}, LX/9JQ;->b()I

    move-result v2

    int-to-long v6, v2

    aput-wide v6, v5, v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 455896
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 455897
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 455898
    :catch_0
    move-exception v2

    .line 455899
    :try_start_4
    const-string v3, "SimpleEdgeStore"

    const-string v4, "Unable to write model"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 455900
    iget-object v2, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850009

    const/4 v4, 0x3

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 455901
    const/4 v2, 0x0

    .line 455902
    :goto_3
    monitor-exit p0

    return-object v2

    .line 455903
    :cond_3
    if-eqz p3, :cond_1

    :try_start_5
    invoke-interface {p3}, LX/95b;->a()I

    move-result v2

    if-lez v2, :cond_1

    .line 455904
    invoke-interface {p3}, LX/95b;->a()I

    move-result v2

    invoke-static {v2}, LX/1Yt;->a(I)LX/1Yt;

    move-result-object v4

    .line 455905
    const/4 v2, 0x0

    :goto_4
    invoke-interface {p3}, LX/95b;->a()I

    move-result v5

    if-ge v2, v5, :cond_5

    .line 455906
    invoke-interface {p3, v2}, LX/95b;->e(I)Ljava/lang/String;

    move-result-object v5

    .line 455907
    if-eqz v5, :cond_4

    .line 455908
    invoke-interface {p3, v2}, LX/95b;->e(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, LX/9JS;->c(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    .line 455909
    if-eqz v5, :cond_4

    .line 455910
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, LX/1Yt;->a(J)V

    .line 455911
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 455912
    :cond_5
    invoke-virtual {v4}, LX/1Yt;->b()[J

    move-result-object v4

    .line 455913
    const/4 v2, 0x0

    :goto_5
    array-length v5, v4

    if-ge v2, v5, :cond_6

    .line 455914
    aget-wide v6, v4, v2

    long-to-int v5, v6

    invoke-virtual {v11, v5}, LX/9JS;->b(I)V

    .line 455915
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 455916
    :cond_6
    array-length v2, v4

    if-lez v2, :cond_1

    .line 455917
    const-string v2, "DELETED_ROW_IDS"

    invoke-virtual {v10, v2, v4}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 455918
    :catch_1
    move-exception v2

    .line 455919
    :try_start_6
    const-string v3, "SimpleEdgeStore"

    const-string v4, "Unable to put"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 455920
    iget-object v2, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850009

    const/4 v4, 0x3

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 455921
    const/4 v2, 0x0

    goto :goto_3

    .line 455922
    :cond_7
    :try_start_7
    const-string v2, "INSERTED_ROW_IDS"

    invoke-virtual {v10, v2, v5}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 455923
    new-instance v2, LX/2tx;

    invoke-static {p0}, LX/2kG;->c(LX/2kG;)LX/2kD;

    move-result-object v3

    iget-object v6, p0, LX/2kG;->b:LX/0SG;

    invoke-direct {v2, v4, v3, v6}, LX/2tx;-><init>(Ljava/util/ArrayList;LX/2kD;LX/0SG;)V

    .line 455924
    iget-object v3, p0, LX/2kG;->e:LX/0t9;

    invoke-virtual {v3, v2, v5}, LX/0t9;->a(LX/2vL;[J)V

    .line 455925
    invoke-virtual {v2}, LX/2tx;->e()Ljava/util/Map;

    move-result-object v2

    .line 455926
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {p0, v2, v3}, LX/2kG;->a(LX/2kG;Ljava/util/Map;Ljava/lang/Boolean;)V

    .line 455927
    iget-object v2, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850009

    const/16 v4, 0xcc

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 455928
    :cond_8
    if-eqz p6, :cond_9

    .line 455929
    move-object/from16 v0, p6

    iget-object v2, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->b:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->c:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->d:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v6, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->e:Z

    move-object/from16 v0, p6

    iget-boolean v7, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->f:Z

    iget-object v8, p0, LX/2kG;->b:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    invoke-static/range {v2 .. v9}, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZJ)Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-result-object v2

    invoke-virtual {v11, v2}, LX/9JS;->a(Lcom/facebook/graphql/cursor/edgestore/PageInfo;)V

    .line 455930
    :cond_9
    iget-object v2, p0, LX/2kG;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x19bfcc00

    move-wide/from16 v0, p4

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {v11, v2}, LX/9JS;->c(I)V

    .line 455931
    const-string v2, "SESSION_VERSION"

    invoke-virtual {v11}, LX/9JS;->n()I

    move-result v3

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 455932
    iget-object v2, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850009

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-object v2, v10

    .line 455933
    goto/16 :goto_3

    .line 455934
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public static declared-synchronized b(LX/2kG;Ljava/lang/String;LX/2kR;)V
    .locals 1

    .prologue
    .line 455841
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2kG;->g:LX/0vV;

    invoke-virtual {v0, p1, p2}, LX/0Xt;->c(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455842
    monitor-exit p0

    return-void

    .line 455843
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static c(LX/2kG;)LX/2kD;
    .locals 3

    .prologue
    .line 455844
    iget-object v0, p0, LX/2kG;->h:LX/2kD;

    if-nez v0, :cond_0

    .line 455845
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/2kG;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "simple_edge_store_models"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, LX/2kD;->a(Ljava/io/File;)LX/2kD;

    move-result-object v0

    iput-object v0, p0, LX/2kG;->h:LX/2kD;

    .line 455846
    :cond_0
    iget-object v0, p0, LX/2kG;->h:LX/2kD;

    return-object v0
.end method

.method private c(Ljava/lang/String;)LX/9JS;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const v6, 0x850020

    .line 455847
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 455848
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "SimpleEdgeStore"

    invoke-interface {v0, v6, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 455849
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 455850
    new-instance v0, Ljava/io/File;

    invoke-direct {p0}, LX/2kG;->d()Ljava/io/File;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".log"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 455851
    :try_start_0
    invoke-static {p1, v0}, LX/9JS;->a(Ljava/lang/String;Ljava/io/File;)LX/9JS;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 455852
    :goto_0
    iget-object v2, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v3, 0xe0

    invoke-interface {v2, v6, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 455853
    if-nez v0, :cond_0

    .line 455854
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x3

    invoke-interface {v0, v6, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 455855
    :goto_1
    return-object v1

    .line 455856
    :catch_0
    move-exception v2

    .line 455857
    const-string v3, "SimpleEdgeStore"

    const-string v4, "Can\'t recover session. Deleting the log file."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 455858
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-object v0, v1

    goto :goto_0

    .line 455859
    :cond_0
    :try_start_1
    invoke-static {p1}, LX/2kG;->d(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/9JS;->d(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 455860
    :goto_2
    invoke-virtual {v0}, LX/9JS;->a()J

    move-result-wide v2

    const-wide/32 v4, 0xf000

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 455861
    invoke-virtual {v0}, LX/9JS;->b()V

    .line 455862
    iget-object v1, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v2, 0xe1

    invoke-interface {v1, v6, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 455863
    :cond_1
    iget-object v1, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "log_size"

    invoke-virtual {v0}, LX/9JS;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v6, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 455864
    iget-object v1, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "num_models"

    .line 455865
    iget-object v3, v0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v3, v3

    .line 455866
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v6, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 455867
    iget-object v1, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x2

    invoke-interface {v1, v6, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    move-object v1, v0

    .line 455868
    goto :goto_1

    :catch_1
    goto :goto_2
.end method

.method private static d(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 455869
    const-string v0, "FriendsCenter"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455870
    const/16 v0, 0x1388

    .line 455871
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x64

    goto :goto_0
.end method

.method private d()Ljava/io/File;
    .locals 3

    .prologue
    .line 455872
    iget-object v0, p0, LX/2kG;->i:Ljava/io/File;

    if-nez v0, :cond_0

    .line 455873
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/2kG;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "simple_edge_store_logs"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LX/2kG;->i:Ljava/io/File;

    .line 455874
    iget-object v0, p0, LX/2kG;->i:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 455875
    :cond_0
    iget-object v0, p0, LX/2kG;->i:Ljava/io/File;

    return-object v0
.end method

.method private declared-synchronized e()V
    .locals 5

    .prologue
    .line 455697
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2kG;->g(LX/2kG;)V

    .line 455698
    invoke-static {p0}, LX/2kG;->c(LX/2kG;)LX/2kD;

    move-result-object v0

    invoke-virtual {v0}, LX/2kD;->b()V

    .line 455699
    iget-object v0, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455700
    :try_start_1
    invoke-virtual {v0}, LX/9JS;->l()V

    .line 455701
    invoke-virtual {v0}, LX/9JS;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 455702
    :catch_0
    move-exception v0

    .line 455703
    :try_start_2
    const-string v2, "SimpleEdgeStore"

    const-string v3, "Unable to clear the session"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 455704
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 455705
    :cond_0
    :try_start_3
    iget-object v0, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 455706
    monitor-exit p0

    return-void
.end method

.method private f()V
    .locals 11

    .prologue
    const v10, 0x850018

    .line 455707
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 455708
    invoke-static {p0}, LX/2kG;->g(LX/2kG;)V

    .line 455709
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455710
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "SimpleEdgeStore"

    invoke-interface {v0, v10, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 455711
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "disk_footprint_before"

    invoke-static {p0}, LX/2kG;->h(LX/2kG;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v10, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 455712
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 455713
    iget-object v0, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JS;

    .line 455714
    iget-object v3, v0, LX/9JS;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    move v3, v3

    .line 455715
    if-nez v3, :cond_1

    .line 455716
    iget v3, v0, LX/9JS;->k:I

    move v3, v3

    .line 455717
    int-to-long v4, v3

    iget-object v3, p0, LX/2kG;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-gez v3, :cond_1

    .line 455718
    :try_start_0
    invoke-virtual {v0}, LX/9JS;->l()V

    .line 455719
    invoke-virtual {v0}, LX/9JS;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 455720
    :goto_1
    iget-object v3, v0, LX/9JS;->a:Ljava/lang/String;

    move-object v0, v3

    .line 455721
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 455722
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 455723
    iget-object v4, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 455724
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 455725
    :cond_3
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 455726
    iget-object v0, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JS;

    .line 455727
    const/4 v3, 0x0

    move v4, v3

    :goto_4
    iget-object v3, v0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v4, v3, :cond_5

    .line 455728
    iget-object v3, v0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9JQ;

    .line 455729
    iget-object v5, v3, LX/9JQ;->a:LX/9JR;

    move-object v5, v5

    .line 455730
    iget v6, v5, LX/9JR;->a:I

    move v5, v6

    .line 455731
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 455732
    iget-object v5, v3, LX/9JQ;->b:LX/9JR;

    move-object v5, v5

    .line 455733
    if-eqz v5, :cond_4

    .line 455734
    iget-object v5, v3, LX/9JQ;->b:LX/9JR;

    move-object v3, v5

    .line 455735
    iget v5, v3, LX/9JR;->a:I

    move v3, v5

    .line 455736
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 455737
    :cond_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_4

    .line 455738
    :cond_5
    goto :goto_3

    .line 455739
    :cond_6
    invoke-static {p0}, LX/2kG;->c(LX/2kG;)LX/2kD;

    move-result-object v0

    .line 455740
    iget-object v2, v0, LX/2kD;->a:Ljava/io/File;

    invoke-static {v2, v1}, LX/39R;->a(Ljava/io/File;Ljava/util/Set;)V

    .line 455741
    iget-object v0, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JS;

    .line 455742
    :try_start_1
    iget-object v2, v0, LX/9JS;->a:Ljava/lang/String;

    move-object v2, v2

    .line 455743
    invoke-static {v2}, LX/2kG;->d(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, LX/9JS;->d(I)V

    .line 455744
    invoke-virtual {v0}, LX/9JS;->a()J

    move-result-wide v2

    const-wide/32 v4, 0xf000

    cmp-long v2, v2, v4

    if-lez v2, :cond_7

    .line 455745
    invoke-virtual {v0}, LX/9JS;->b()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5

    .line 455746
    :catch_0
    goto :goto_5

    .line 455747
    :cond_8
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 455748
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "disk_footprint_after"

    invoke-static {p0}, LX/2kG;->h(LX/2kG;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v10, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 455749
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "total_sessions"

    iget-object v2, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v10, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 455750
    :cond_9
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    invoke-interface {v0, v10, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 455751
    return-void

    :catch_1
    goto/16 :goto_1
.end method

.method private static g(LX/2kG;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const v7, 0x850021

    .line 455559
    iget-boolean v1, p0, LX/2kG;->j:Z

    if-eqz v1, :cond_1

    .line 455560
    :cond_0
    :goto_0
    return-void

    .line 455561
    :cond_1
    iget-object v1, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 455562
    invoke-direct {p0}, LX/2kG;->d()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 455563
    if-eqz v2, :cond_0

    .line 455564
    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 455565
    invoke-static {v4}, LX/1t3;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "log"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 455566
    new-instance v5, Ljava/io/File;

    invoke-direct {p0}, LX/2kG;->d()Ljava/io/File;

    move-result-object v6

    invoke-direct {v5, v6, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 455567
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 455568
    :cond_3
    invoke-static {v4}, LX/1t3;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 455569
    iget-object v5, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 455570
    add-int/lit8 v0, v0, 0x1

    .line 455571
    invoke-direct {p0, v4}, LX/2kG;->c(Ljava/lang/String;)LX/9JS;

    move-result-object v5

    .line 455572
    if-eqz v5, :cond_2

    .line 455573
    iget-object v6, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v6, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 455574
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/2kG;->j:Z

    .line 455575
    iget-object v1, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "opened_sessions"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v7, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 455576
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "total_sessions"

    iget-object v2, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v7, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 455577
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    invoke-interface {v0, v7, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_0
.end method

.method private static h(LX/2kG;)J
    .locals 5

    .prologue
    .line 455578
    const-wide/16 v0, 0x0

    .line 455579
    iget-object v2, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JS;

    .line 455580
    invoke-virtual {v0}, LX/9JS;->a()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 455581
    goto :goto_0

    .line 455582
    :cond_0
    invoke-static {p0}, LX/2kG;->c(LX/2kG;)LX/2kD;

    move-result-object v0

    invoke-virtual {v0}, LX/2kD;->c()J

    move-result-wide v0

    add-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final declared-synchronized U_()V
    .locals 6
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 455583
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2kG;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 455584
    iget-wide v2, p0, LX/2kG;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long v2, v0, v2

    const-wide/16 v4, 0xe10

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 455585
    :goto_0
    monitor-exit p0

    return-void

    .line 455586
    :cond_0
    :try_start_1
    iput-wide v0, p0, LX/2kG;->a:J

    .line 455587
    invoke-direct {p0}, LX/2kG;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 455588
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;LX/2kR;)LX/2kT;
    .locals 1

    .prologue
    .line 455589
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2kG;->g:LX/0vV;

    invoke-virtual {v0, p1, p2}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 455590
    new-instance v0, LX/9JU;

    invoke-direct {v0, p0, p1, p2}, LX/9JU;-><init>(LX/2kG;Ljava/lang/String;LX/2kR;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 455591
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)LX/2kb;
    .locals 2

    .prologue
    .line 455592
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JS;

    .line 455593
    if-nez v0, :cond_1

    .line 455594
    invoke-direct {p0, p1}, LX/2kG;->c(Ljava/lang/String;)LX/9JS;

    move-result-object v0

    .line 455595
    if-nez v0, :cond_0

    .line 455596
    const-string v0, "SimpleEdgeStore"

    const-string v1, "Falling back to in-memory session"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 455597
    new-instance v0, LX/9JS;

    invoke-direct {v0, p1}, LX/9JS;-><init>(Ljava/lang/String;)V

    move-object v0, v0

    .line 455598
    :cond_0
    iget-object v1, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455599
    :cond_1
    new-instance v1, LX/9JV;

    invoke-direct {v1, v0}, LX/9JV;-><init>(LX/9JS;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 455600
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/2kb;)LX/2nf;
    .locals 6
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 455601
    monitor-enter p0

    :try_start_0
    check-cast p1, LX/9JV;

    .line 455602
    iget-object v0, p1, LX/9JV;->a:LX/9JS;

    move-object v0, v0

    .line 455603
    iget-object v1, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x85001f

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 455604
    iget-object v1, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x85001f

    const-string v3, "SimpleEdgeStore"

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 455605
    iget-object v1, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x85001f

    .line 455606
    iget-object v3, v0, LX/9JS;->a:Ljava/lang/String;

    move-object v3, v3

    .line 455607
    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 455608
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 455609
    const-string v2, "CHANGE_NUMBER"

    iget-wide v4, p0, LX/2kG;->k:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 455610
    const-string v2, "CHUNKS"

    .line 455611
    iget-object v3, v0, LX/9JS;->j:Ljava/util/ArrayList;

    move-object v3, v3

    .line 455612
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 455613
    const-string v2, "SESSION_VERSION"

    .line 455614
    iget v3, v0, LX/9JS;->l:I

    move v3, v3

    .line 455615
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 455616
    invoke-virtual {v0}, LX/9JS;->k()Ljava/util/ArrayList;

    move-result-object v0

    .line 455617
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 455618
    new-instance v2, LX/9JW;

    invoke-static {p0}, LX/2kG;->c(LX/2kG;)LX/2kD;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/9JW;-><init>(Ljava/util/ArrayList;Landroid/os/Bundle;LX/2kD;)V

    .line 455619
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85001f

    const/4 v3, 0x2

    invoke-interface {v0, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455620
    monitor-exit p0

    return-object v2

    .line 455621
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/2kb;Ljava/lang/String;)LX/2nf;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 455622
    monitor-enter p0

    :try_start_0
    check-cast p1, LX/9JV;

    .line 455623
    iget-object v0, p1, LX/9JV;->a:LX/9JS;

    move-object v0, v0

    .line 455624
    if-eqz p2, :cond_0

    .line 455625
    invoke-virtual {v0, p2}, LX/9JS;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 455626
    :goto_0
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 455627
    new-instance v1, LX/9JW;

    const/4 v2, 0x0

    invoke-static {p0}, LX/2kG;->c(LX/2kG;)LX/2kD;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, LX/9JW;-><init>(Ljava/util/ArrayList;Landroid/os/Bundle;LX/2kD;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 455628
    :cond_0
    :try_start_1
    invoke-virtual {v0}, LX/9JS;->k()Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 455629
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/2kb;Ljava/nio/ByteBuffer;LX/95b;JLcom/facebook/graphql/cursor/edgestore/PageInfo;Z)LX/2nf;
    .locals 4

    .prologue
    .line 455630
    monitor-enter p0

    :try_start_0
    invoke-direct/range {p0 .. p7}, LX/2kG;->b(LX/2kb;Ljava/nio/ByteBuffer;LX/95b;JLcom/facebook/graphql/cursor/edgestore/PageInfo;Z)Landroid/os/Bundle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 455631
    if-nez v1, :cond_0

    .line 455632
    const/4 v0, 0x0

    .line 455633
    :goto_0
    monitor-exit p0

    return-object v0

    .line 455634
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, LX/2kG;->a(LX/2kb;)LX/2nf;

    move-result-object v0

    .line 455635
    invoke-interface {v0}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 455636
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/2kb;LX/0Rl;Ljava/lang/String;)V
    .locals 13
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2kb;",
            "LX/0Rl",
            "<TTEdge;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 455637
    monitor-enter p0

    :try_start_0
    move-object v0, p1

    check-cast v0, LX/9JV;

    move-object v2, v0

    invoke-virtual {v2}, LX/9JV;->b()LX/9JS;

    move-result-object v4

    .line 455638
    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, LX/9JS;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 455639
    const/4 v2, 0x1

    invoke-static {v2}, LX/1Yt;->a(I)LX/1Yt;

    move-result-object v6

    .line 455640
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    :goto_0
    if-ge v3, v7, :cond_1

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9JQ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455641
    :try_start_1
    invoke-static {p0}, LX/2kG;->c(LX/2kG;)LX/2kD;

    move-result-object v8

    invoke-virtual {v2}, LX/9JQ;->e()I

    move-result v9

    invoke-virtual {v2}, LX/9JQ;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2}, LX/9JQ;->g()LX/0w5;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v10, v11, v12}, LX/2kD;->a(ILjava/lang/String;LX/0w5;[B)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v8

    .line 455642
    invoke-interface {p2, v8}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 455643
    invoke-virtual {v2}, LX/9JQ;->b()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v6, v8, v9}, LX/1Yt;->a(J)V

    .line 455644
    invoke-virtual {v2}, LX/9JQ;->b()I

    move-result v2

    invoke-virtual {v4, v2}, LX/9JS;->b(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 455645
    :cond_0
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 455646
    :catch_0
    move-exception v2

    .line 455647
    :try_start_2
    const-string v8, "SimpleEdgeStore"

    const-string v9, "Error deleting row by predicate"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v8, v2, v9, v10}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 455648
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 455649
    :cond_1
    :try_start_3
    invoke-interface {p1}, LX/2kb;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3, v6}, LX/2kG;->a(Ljava/lang/String;LX/1Yt;LX/1Yt;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 455650
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;Ljava/lang/String;)V
    .locals 5
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Bq;",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 455651
    monitor-enter p0

    :try_start_0
    new-instance v2, LX/2tx;

    invoke-direct {p0, p1, p4}, LX/2kG;->a(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p0}, LX/2kG;->c(LX/2kG;)LX/2kD;

    move-result-object v1

    iget-object v3, p0, LX/2kG;->b:LX/0SG;

    invoke-direct {v2, v0, v1, v3}, LX/2tx;-><init>(Ljava/util/ArrayList;LX/2kD;LX/0SG;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x0

    .line 455652
    :try_start_1
    iget-object v0, p0, LX/2kG;->e:LX/0t9;

    invoke-virtual {v0, v2, p1, p2, p3}, LX/0t9;->a(LX/2vL;Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;)V

    .line 455653
    iget-object v0, v2, LX/2tx;->d:Ljava/util/Map;

    move-object v0, v0

    .line 455654
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {p0, v0, v3}, LX/2kG;->a(LX/2kG;Ljava/util/Map;Ljava/lang/Boolean;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 455655
    :try_start_2
    invoke-virtual {v2}, LX/2tx;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 455656
    :goto_0
    monitor-exit p0

    return-void

    .line 455657
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 455658
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v1, :cond_0

    :try_start_4
    invoke-virtual {v2}, LX/2tx;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_2
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_1
    move-exception v0

    .line 455659
    :try_start_6
    const-string v1, "SimpleEdgeStore"

    const-string v2, "Unable to apply confirmed operation"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    .line 455660
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 455661
    :catch_2
    move-exception v2

    :try_start_7
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_0
    invoke-virtual {v2}, LX/2tx;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    :catchall_2
    move-exception v0

    goto :goto_1
.end method

.method public final declared-synchronized a(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 6
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 455662
    monitor-enter p0

    :try_start_0
    new-instance v2, LX/2tx;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/2kG;->a(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p0}, LX/2kG;->c(LX/2kG;)LX/2kD;

    move-result-object v3

    iget-object v4, p0, LX/2kG;->b:LX/0SG;

    invoke-direct {v2, v0, v3, v4}, LX/2tx;-><init>(Ljava/util/ArrayList;LX/2kD;LX/0SG;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 455663
    :try_start_1
    iget-object v0, p0, LX/2kG;->e:LX/0t9;

    invoke-virtual {v0, v2, p1, p2}, LX/0t9;->a(LX/2vL;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 455664
    iget-object v0, v2, LX/2tx;->d:Ljava/util/Map;

    move-object v0, v0

    .line 455665
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {p0, v0, v3}, LX/2kG;->a(LX/2kG;Ljava/util/Map;Ljava/lang/Boolean;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 455666
    :try_start_2
    invoke-virtual {v2}, LX/2tx;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 455667
    :goto_0
    monitor-exit p0

    return-void

    .line 455668
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 455669
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v1, :cond_0

    :try_start_4
    invoke-virtual {v2}, LX/2tx;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_2
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_1
    move-exception v0

    .line 455670
    :try_start_6
    const-string v1, "SimpleEdgeStore"

    const-string v2, "Unable to apply optimistic operation"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    .line 455671
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 455672
    :catch_2
    move-exception v2

    :try_start_7
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_0
    invoke-virtual {v2}, LX/2tx;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    :catchall_2
    move-exception v0

    goto :goto_1
.end method

.method public final declared-synchronized b()V
    .locals 1
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 455673
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/2kG;->U_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455674
    monitor-exit p0

    return-void

    .line 455675
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 4
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 455676
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85000a

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 455677
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85000a

    const-string v2, "SimpleEdgeStore"

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 455678
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85000a

    invoke-interface {v0, v1, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 455679
    iget-object v0, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JS;

    .line 455680
    if-nez v0, :cond_0

    .line 455681
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85000a

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455682
    :goto_0
    monitor-exit p0

    return-void

    .line 455683
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v0}, LX/9JS;->g()[J

    move-result-object v2

    .line 455684
    new-instance v3, LX/1Yt;

    invoke-direct {v3, v2}, LX/1Yt;-><init>([J)V

    move-object v2, v3

    .line 455685
    invoke-direct {p0, p1, v1, v2}, LX/2kG;->a(Ljava/lang/String;LX/1Yt;LX/1Yt;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 455686
    :try_start_2
    invoke-virtual {v0}, LX/9JS;->l()V

    .line 455687
    invoke-virtual {v0}, LX/9JS;->close()V

    .line 455688
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85000a

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 455689
    :goto_1
    :try_start_3
    iget-object v0, p0, LX/2kG;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 455690
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 455691
    :catch_0
    move-exception v0

    .line 455692
    :try_start_4
    const-string v1, "SimpleEdgeStore"

    const-string v2, "Error deleting a session"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 455693
    iget-object v0, p0, LX/2kG;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85000a

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized clearUserData()V
    .locals 1

    .prologue
    .line 455694
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/2kG;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455695
    monitor-exit p0

    return-void

    .line 455696
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
