.class public LX/39j;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/39o;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/39j",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/39o;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 523742
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 523743
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/39j;->b:LX/0Zi;

    .line 523744
    iput-object p1, p0, LX/39j;->a:LX/0Ot;

    .line 523745
    return-void
.end method

.method public static a(LX/0QB;)LX/39j;
    .locals 4

    .prologue
    .line 523746
    const-class v1, LX/39j;

    monitor-enter v1

    .line 523747
    :try_start_0
    sget-object v0, LX/39j;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 523748
    sput-object v2, LX/39j;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 523749
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523750
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 523751
    new-instance v3, LX/39j;

    const/16 p0, 0x864

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/39j;-><init>(LX/0Ot;)V

    .line 523752
    move-object v0, v3

    .line 523753
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 523754
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/39j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 523755
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 523756
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 17

    .prologue
    .line 523757
    check-cast p2, LX/39k;

    .line 523758
    move-object/from16 v0, p0

    iget-object v1, v0, LX/39j;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/39o;

    move-object/from16 v0, p2

    iget-object v3, v0, LX/39k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v0, p2

    iget-object v4, v0, LX/39k;->b:LX/1Pr;

    move-object/from16 v0, p2

    iget-object v5, v0, LX/39k;->c:LX/1Wk;

    move-object/from16 v0, p2

    iget-object v6, v0, LX/39k;->d:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v7, v0, LX/39k;->e:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v8, v0, LX/39k;->f:Landroid/util/SparseArray;

    move-object/from16 v0, p2

    iget-object v9, v0, LX/39k;->g:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget v10, v0, LX/39k;->h:I

    move-object/from16 v0, p2

    iget v11, v0, LX/39k;->i:I

    move-object/from16 v0, p2

    iget v12, v0, LX/39k;->j:F

    move-object/from16 v0, p2

    iget v13, v0, LX/39k;->k:F

    move-object/from16 v0, p2

    iget v14, v0, LX/39k;->l:F

    move-object/from16 v0, p2

    iget-object v15, v0, LX/39k;->m:LX/1dc;

    move-object/from16 v0, p2

    iget v0, v0, LX/39k;->n:I

    move/from16 v16, v0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v16}, LX/39o;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;LX/1Wk;Ljava/lang/String;Ljava/lang/CharSequence;Landroid/util/SparseArray;Ljava/lang/CharSequence;IIFFFLX/1dc;I)LX/1Dg;

    move-result-object v1

    .line 523759
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 523760
    invoke-static {}, LX/1dS;->b()V

    .line 523761
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/39l;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 523762
    new-instance v1, LX/39k;

    invoke-direct {v1, p0}, LX/39k;-><init>(LX/39j;)V

    .line 523763
    iget-object v2, p0, LX/39j;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/39l;

    .line 523764
    if-nez v2, :cond_0

    .line 523765
    new-instance v2, LX/39l;

    invoke-direct {v2, p0}, LX/39l;-><init>(LX/39j;)V

    .line 523766
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/39l;->a$redex0(LX/39l;LX/1De;IILX/39k;)V

    .line 523767
    move-object v1, v2

    .line 523768
    move-object v0, v1

    .line 523769
    return-object v0
.end method
