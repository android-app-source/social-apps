.class public LX/3NN;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/3NP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 559105
    new-instance v0, LX/3NO;

    invoke-direct {v0}, LX/3NO;-><init>()V

    sput-object v0, LX/3NN;->a:LX/3NP;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 559106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 559107
    return-void
.end method

.method private static a(LX/3N9;LX/3NA;Ljava/util/concurrent/ScheduledExecutorService;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3N9;",
            "LX/3NA;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ")",
            "LX/0Px",
            "<",
            "LX/3NU;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 559108
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 559109
    new-instance v1, LX/3NU;

    invoke-direct {v1, p0, v4, v3}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 559110
    new-instance v1, LX/3NU;

    invoke-static {p1, p2}, LX/3NN;->a(LX/3Mi;Ljava/util/concurrent/ScheduledExecutorService;)LX/3NV;

    move-result-object v2

    invoke-direct {v1, v2, v4, v3}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 559111
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;Landroid/content/res/Resources;LX/3Mk;Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;LX/3N9;LX/3NA;LX/3NE;LX/3NH;LX/3NI;LX/3NJ;LX/3NL;ZLjava/util/ArrayList;)LX/3Mi;
    .locals 2
    .param p8    # LX/3NE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/3NH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # LX/3NI;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # LX/3NJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p14    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SF;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/content/res/Resources;",
            "LX/3Mk;",
            "Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;",
            "LX/3N9;",
            "LX/3NA;",
            "LX/3NE;",
            "LX/3NH;",
            "LX/3NI;",
            "LX/3NJ;",
            "LX/3NL;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "LX/3NQ;",
            ">;)",
            "LX/3Mi;"
        }
    .end annotation

    .prologue
    .line 559112
    new-instance v0, LX/3NS;

    invoke-static/range {p0 .. p14}, LX/3NN;->b(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;Landroid/content/res/Resources;LX/3Mk;Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;LX/3N9;LX/3NA;LX/3NE;LX/3NH;LX/3NI;LX/3NJ;LX/3NL;ZLjava/util/ArrayList;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1, p2}, LX/3NS;-><init>(LX/0Px;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;)V

    return-object v0
.end method

.method private static a(LX/3Mi;Ljava/util/concurrent/ScheduledExecutorService;)LX/3NV;
    .locals 2

    .prologue
    .line 559113
    new-instance v0, LX/3NV;

    sget-object v1, LX/3NN;->a:LX/3NP;

    invoke-direct {v0, p0, p1, v1}, LX/3NV;-><init>(LX/3Mi;Ljava/util/concurrent/ScheduledExecutorService;LX/3NP;)V

    return-object v0
.end method

.method public static a()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/3NQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 559114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 559115
    sget-object v1, LX/3NQ;->M_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559116
    sget-object v1, LX/3NQ;->FRIEND_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559117
    sget-object v1, LX/3NQ;->GROUP_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559118
    sget-object v1, LX/3NQ;->TINCAN_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559119
    sget-object v1, LX/3NQ;->VC_ENDPOINTS_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559120
    sget-object v1, LX/3NQ;->NON_FRIENDS_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559121
    sget-object v1, LX/3NQ;->PAGES_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559122
    sget-object v1, LX/3NQ;->BOTS_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559123
    return-object v0
.end method

.method private static b(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;Landroid/content/res/Resources;LX/3Mk;Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;LX/3N9;LX/3NA;LX/3NE;LX/3NH;LX/3NI;LX/3NJ;LX/3NL;ZLjava/util/ArrayList;)LX/0Px;
    .locals 8
    .param p8    # LX/3NE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/3NH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # LX/3NI;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # LX/3NJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p14    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SF;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/content/res/Resources;",
            "LX/3Mk;",
            "Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;",
            "LX/3N9;",
            "LX/3NA;",
            "LX/3NE;",
            "LX/3NH;",
            "LX/3NI;",
            "LX/3NJ;",
            "LX/3NL;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "LX/3NQ;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/3NU;",
            ">;"
        }
    .end annotation

    .prologue
    .line 559124
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 559125
    if-nez p14, :cond_0

    .line 559126
    invoke-static {}, LX/3NN;->b()Ljava/util/ArrayList;

    move-result-object p14

    .line 559127
    :cond_0
    invoke-virtual/range {p14 .. p14}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    move-object/from16 v0, p14

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3NQ;

    .line 559128
    sget-object v5, LX/3NT;->a:[I

    invoke-virtual {v1}, LX/3NQ;->ordinal()I

    move-result v1

    aget v1, v5, v1

    packed-switch v1, :pswitch_data_0

    .line 559129
    :cond_1
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 559130
    :pswitch_0
    if-eqz p10, :cond_1

    .line 559131
    new-instance v1, LX/3NU;

    move-object/from16 v0, p10

    invoke-static {v0, p1}, LX/3NN;->a(LX/3Mi;Ljava/util/concurrent/ScheduledExecutorService;)LX/3NV;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct {v1, v5, v6, v7}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 559132
    :pswitch_1
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, LX/3Mk;->b(Z)V

    .line 559133
    if-nez p13, :cond_2

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {p4, v1}, LX/3Ml;->a(Z)V

    .line 559134
    new-instance v1, LX/3NU;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-direct {v1, p4, v5, v6}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 559135
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 559136
    :pswitch_2
    new-instance v1, LX/3NU;

    new-instance v5, LX/3NS;

    invoke-static {p6, p7, p1}, LX/3NN;->a(LX/3N9;LX/3NA;Ljava/util/concurrent/ScheduledExecutorService;)LX/0Px;

    move-result-object v6

    invoke-direct {v5, v6, p0, p1, p2}, LX/3NS;-><init>(LX/0Px;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;)V

    const v6, 0x7f0802d5

    invoke-virtual {p3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v1, v5, v6, v7}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 559137
    :pswitch_3
    invoke-virtual/range {p12 .. p12}, LX/3NL;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 559138
    new-instance v1, LX/3NU;

    const v5, 0x7f0806ea

    invoke-virtual {p3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, p12

    invoke-direct {v1, v0, v5, v6}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 559139
    :pswitch_4
    if-eqz p11, :cond_1

    .line 559140
    new-instance v1, LX/3NU;

    move-object/from16 v0, p11

    invoke-static {v0, p1}, LX/3NN;->a(LX/3Mi;Ljava/util/concurrent/ScheduledExecutorService;)LX/3NV;

    move-result-object v5

    const v6, 0x7f0802d6

    invoke-virtual {p3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v1, v5, v6, v7}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 559141
    :pswitch_5
    if-eqz p8, :cond_1

    .line 559142
    new-instance v1, LX/3NU;

    move-object/from16 v0, p8

    invoke-static {v0, p1}, LX/3NN;->a(LX/3Mi;Ljava/util/concurrent/ScheduledExecutorService;)LX/3NV;

    move-result-object v5

    const v6, 0x7f0802d7

    invoke-virtual {p3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v1, v5, v6, v7}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    .line 559143
    :pswitch_6
    if-eqz p9, :cond_1

    .line 559144
    new-instance v1, LX/3NU;

    move-object/from16 v0, p9

    invoke-static {v0, p1}, LX/3NN;->a(LX/3Mi;Ljava/util/concurrent/ScheduledExecutorService;)LX/3NV;

    move-result-object v5

    const v6, 0x7f0802d8

    invoke-virtual {p3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v1, v5, v6, v7}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    .line 559145
    :pswitch_7
    new-instance v1, LX/3NU;

    invoke-static {p5, p1}, LX/3NN;->a(LX/3Mi;Ljava/util/concurrent/ScheduledExecutorService;)LX/3NV;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v1, v5, v6, v7}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    .line 559146
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static b()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/3NQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 559147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 559148
    sget-object v1, LX/3NQ;->M_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559149
    sget-object v1, LX/3NQ;->FRIEND_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559150
    sget-object v1, LX/3NQ;->GROUP_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559151
    sget-object v1, LX/3NQ;->TINCAN_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559152
    sget-object v1, LX/3NQ;->VC_ENDPOINTS_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559153
    sget-object v1, LX/3NQ;->PAGES_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559154
    sget-object v1, LX/3NQ;->BOTS_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559155
    sget-object v1, LX/3NQ;->NON_FRIENDS_FILTER:LX/3NQ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559156
    return-object v0
.end method
