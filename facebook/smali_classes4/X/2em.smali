.class public final LX/2em;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 445604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLUser;)Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 445605
    new-instance v0, LX/25F;

    invoke-direct {v0}, LX/25F;-><init>()V

    .line 445606
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v1

    .line 445607
    iput-object v1, v0, LX/25F;->b:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 445608
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->l()LX/0Px;

    move-result-object v1

    .line 445609
    iput-object v1, v0, LX/25F;->e:LX/0Px;

    .line 445610
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->m()Z

    move-result v1

    .line 445611
    iput-boolean v1, v0, LX/25F;->h:Z

    .line 445612
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->n()Z

    move-result v1

    .line 445613
    iput-boolean v1, v0, LX/25F;->i:Z

    .line 445614
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->o()D

    move-result-wide v2

    .line 445615
    iput-wide v2, v0, LX/25F;->l:D

    .line 445616
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->p()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    .line 445617
    iput-object v1, v0, LX/25F;->o:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 445618
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->q()LX/0Px;

    move-result-object v1

    .line 445619
    iput-object v1, v0, LX/25F;->q:LX/0Px;

    .line 445620
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 445621
    iput-object v1, v0, LX/25F;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 445622
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->t()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v1

    .line 445623
    iput-object v1, v0, LX/25F;->w:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 445624
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 445625
    iput-object v1, v0, LX/25F;->x:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 445626
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v1

    .line 445627
    iput-object v1, v0, LX/25F;->C:Ljava/lang/String;

    .line 445628
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->w()Z

    move-result v1

    .line 445629
    iput-boolean v1, v0, LX/25F;->E:Z

    .line 445630
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ah()Z

    move-result v1

    .line 445631
    iput-boolean v1, v0, LX/25F;->F:Z

    .line 445632
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->x()Z

    move-result v1

    .line 445633
    iput-boolean v1, v0, LX/25F;->I:Z

    .line 445634
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->y()Z

    move-result v1

    .line 445635
    iput-boolean v1, v0, LX/25F;->L:Z

    .line 445636
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->A()Z

    move-result v1

    .line 445637
    iput-boolean v1, v0, LX/25F;->N:Z

    .line 445638
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->B()Z

    move-result v1

    .line 445639
    iput-boolean v1, v0, LX/25F;->P:Z

    .line 445640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->C()Z

    move-result v1

    .line 445641
    iput-boolean v1, v0, LX/25F;->Q:Z

    .line 445642
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->D()Z

    move-result v1

    .line 445643
    iput-boolean v1, v0, LX/25F;->R:Z

    .line 445644
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->E()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v1

    .line 445645
    iput-object v1, v0, LX/25F;->S:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 445646
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v1

    .line 445647
    iput-object v1, v0, LX/25F;->T:Ljava/lang/String;

    .line 445648
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->G()LX/0Px;

    move-result-object v1

    .line 445649
    iput-object v1, v0, LX/25F;->U:LX/0Px;

    .line 445650
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->H()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 445651
    iput-object v1, v0, LX/25F;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 445652
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 445653
    iput-object v1, v0, LX/25F;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 445654
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 445655
    iput-object v1, v0, LX/25F;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 445656
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 445657
    iput-object v1, v0, LX/25F;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    .line 445658
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 445659
    iput-object v1, v0, LX/25F;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 445660
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 445661
    iput-object v1, v0, LX/25F;->af:Lcom/facebook/graphql/model/GraphQLImage;

    .line 445662
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 445663
    iput-object v1, v0, LX/25F;->ag:Lcom/facebook/graphql/model/GraphQLImage;

    .line 445664
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->P()Z

    move-result v1

    .line 445665
    iput-boolean v1, v0, LX/25F;->ah:Z

    .line 445666
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 445667
    iput-object v1, v0, LX/25F;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    .line 445668
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->Q()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v1

    .line 445669
    iput-object v1, v0, LX/25F;->ak:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 445670
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->R()Ljava/lang/String;

    move-result-object v1

    .line 445671
    iput-object v1, v0, LX/25F;->al:Ljava/lang/String;

    .line 445672
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->S()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 445673
    iput-object v1, v0, LX/25F;->am:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 445674
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->T()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v1

    .line 445675
    iput-object v1, v0, LX/25F;->an:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 445676
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->U()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v1

    .line 445677
    iput-object v1, v0, LX/25F;->ao:Lcom/facebook/graphql/model/GraphQLName;

    .line 445678
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->V()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    .line 445679
    iput-object v1, v0, LX/25F;->ap:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 445680
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 445681
    iput-object v1, v0, LX/25F;->aq:Lcom/facebook/graphql/model/GraphQLImage;

    .line 445682
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->X()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 445683
    iput-object v1, v0, LX/25F;->ar:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 445684
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aa()I

    move-result v1

    .line 445685
    iput v1, v0, LX/25F;->av:I

    .line 445686
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ab()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v1

    .line 445687
    iput-object v1, v0, LX/25F;->aw:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 445688
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ac()Ljava/lang/String;

    move-result-object v1

    .line 445689
    iput-object v1, v0, LX/25F;->ax:Ljava/lang/String;

    .line 445690
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ad()Ljava/lang/String;

    move-result-object v1

    .line 445691
    iput-object v1, v0, LX/25F;->ay:Ljava/lang/String;

    .line 445692
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ae()D

    move-result-wide v2

    .line 445693
    iput-wide v2, v0, LX/25F;->az:D

    .line 445694
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ai()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    .line 445695
    iput-object v1, v0, LX/25F;->aD:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 445696
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->af()LX/0Px;

    move-result-object v1

    .line 445697
    iput-object v1, v0, LX/25F;->aG:LX/0Px;

    .line 445698
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x285feb

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 445699
    iput-object v1, v0, LX/25F;->aH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 445700
    invoke-virtual {v0}, LX/25F;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    return-object v0
.end method
