.class public LX/25g;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Ljava/nio/ByteBuffer;

.field public final d:I

.field public final e:I

.field public f:Z


# direct methods
.method public constructor <init>(IILjava/nio/ByteBuffer;II)V
    .locals 0
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 369866
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369867
    iput p1, p0, LX/25g;->a:I

    .line 369868
    iput p2, p0, LX/25g;->b:I

    .line 369869
    iput-object p3, p0, LX/25g;->c:Ljava/nio/ByteBuffer;

    .line 369870
    iput p4, p0, LX/25g;->d:I

    .line 369871
    iput p5, p0, LX/25g;->e:I

    .line 369872
    return-void
.end method

.method public static a(LX/25g;LX/186;)I
    .locals 3

    .prologue
    .line 369873
    const/16 v0, 0x8

    iget v1, p0, LX/25g;->e:I

    invoke-virtual {p1, v0, v1}, LX/186;->a(II)V

    .line 369874
    iget-object v0, p0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    iget v1, p0, LX/25g;->d:I

    iget v2, p0, LX/25g;->e:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a([BII)V

    .line 369875
    iget v0, p0, LX/25g;->e:I

    invoke-virtual {p1, v0}, LX/186;->a(I)V

    .line 369876
    iget v0, p0, LX/25g;->b:I

    invoke-virtual {p1, v0}, LX/186;->a(I)V

    .line 369877
    iget v0, p0, LX/25g;->a:I

    invoke-virtual {p1, v0}, LX/186;->a(I)V

    .line 369878
    invoke-virtual {p1}, LX/186;->b()I

    move-result v0

    return v0
.end method

.method public static b(Ljava/nio/ByteBuffer;I)LX/25g;
    .locals 6

    .prologue
    .line 369856
    invoke-virtual {p0, p1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    .line 369857
    add-int/lit8 v0, p1, 0x4

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    .line 369858
    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v5

    .line 369859
    add-int/lit8 v4, p1, 0xc

    .line 369860
    new-instance v0, LX/25g;

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, LX/25g;-><init>(IILjava/nio/ByteBuffer;II)V

    return-object v0
.end method

.method public static c(Ljava/nio/ByteBuffer;I)I
    .locals 1

    .prologue
    .line 369864
    add-int/lit8 v0, p1, 0x4

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 369865
    iget v0, p0, LX/25g;->a:I

    iget v1, p0, LX/25g;->e:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 369863
    iget v0, p0, LX/25g;->a:I

    sub-int v0, p1, v0

    iget v1, p0, LX/25g;->d:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 369861
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/25g;->f:Z

    .line 369862
    return-void
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 369855
    iget v0, p0, LX/25g;->a:I

    add-int/2addr v0, p1

    iget v1, p0, LX/25g;->d:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 369854
    iget-boolean v0, p0, LX/25g;->f:Z

    return v0
.end method
