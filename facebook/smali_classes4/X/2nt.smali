.class public LX/2nt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16E;
.implements LX/0i1;


# instance fields
.field private final a:LX/0wM;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0wM;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 465362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465363
    iput-object p1, p0, LX/2nt;->a:LX/0wM;

    .line 465364
    iput-object p2, p0, LX/2nt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 465365
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 465373
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 465370
    iget-object v0, p0, LX/2nt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2ns;->c:LX/0Tn;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2nt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2ns;->b:LX/0Tn;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 465371
    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 465372
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 465369
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 465374
    iget-object v0, p0, LX/2nt;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 465375
    if-nez v0, :cond_0

    .line 465376
    :goto_0
    return-void

    .line 465377
    :cond_0
    new-instance v1, LX/0hs;

    const/4 v2, 0x2

    invoke-direct {v1, p1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 465378
    iget-object v2, p0, LX/2nt;->a:LX/0wM;

    const v3, 0x7f02096e

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 465379
    const v2, 0x7f082258

    invoke-virtual {v1, v2}, LX/0hs;->a(I)V

    .line 465380
    const v2, 0x7f082259

    invoke-virtual {v1, v2}, LX/0hs;->b(I)V

    .line 465381
    sget-object v2, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v1, v2}, LX/0ht;->a(LX/3AV;)V

    .line 465382
    iput v4, v1, LX/0hs;->t:I

    .line 465383
    invoke-virtual {v1, v0}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 465368
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 465367
    const-string v0, "4111"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 465366
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_HOME_TAB_TOOLTIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
