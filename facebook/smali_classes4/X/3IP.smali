.class public LX/3IP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/animation/ValueAnimator;

.field private b:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 546136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 546137
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 546131
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546132
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 546133
    :cond_0
    iget-object v0, p0, LX/3IP;->b:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3IP;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546134
    iget-object v0, p0, LX/3IP;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 546135
    :cond_1
    return-void
.end method

.method public final a(FFILX/3II;)V
    .locals 4

    .prologue
    .line 546118
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 546119
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 546120
    :cond_0
    invoke-interface {p4}, LX/3II;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 546121
    :goto_0
    return-void

    .line 546122
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    .line 546123
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 546124
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    int-to-long v2, p3

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 546125
    invoke-interface {p4}, LX/3II;->get360TextureView()LX/2qW;

    move-result-object v0

    .line 546126
    iget-object v1, v0, LX/2qW;->c:LX/7Cy;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/2qW;->c:LX/7Cy;

    iget-object v1, v1, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v1, :cond_2

    .line 546127
    iget-object v1, v0, LX/2qW;->c:LX/7Cy;

    iget-object v1, v1, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-virtual {v1, p1, p2}, Lcom/facebook/spherical/GlMediaRenderThread;->e(FF)V

    .line 546128
    :cond_2
    new-instance v0, LX/7Cw;

    invoke-direct {v0, p4}, LX/7Cw;-><init>(LX/3II;)V

    .line 546129
    iget-object v1, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 546130
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final a(FILX/3II;)V
    .locals 4

    .prologue
    .line 546107
    iget-object v0, p0, LX/3IP;->b:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 546108
    iget-object v0, p0, LX/3IP;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 546109
    :cond_0
    invoke-interface {p3}, LX/3II;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 546110
    :goto_0
    return-void

    .line 546111
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/3IP;->b:Landroid/animation/ValueAnimator;

    .line 546112
    iget-object v0, p0, LX/3IP;->b:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 546113
    iget-object v0, p0, LX/3IP;->b:Landroid/animation/ValueAnimator;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 546114
    new-instance v0, LX/7Cv;

    invoke-interface {p3}, LX/3II;->get360TextureView()LX/2qW;

    move-result-object v1

    invoke-virtual {v1}, LX/2qW;->getFov()F

    move-result v1

    invoke-direct {v0, p3, v1, p1}, LX/7Cv;-><init>(LX/3II;FF)V

    .line 546115
    iget-object v1, p0, LX/3IP;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 546116
    iget-object v0, p0, LX/3IP;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 546117
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final b(FFILX/3II;)V
    .locals 4

    .prologue
    .line 546093
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 546094
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 546095
    :cond_0
    invoke-interface {p4}, LX/3II;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 546096
    :goto_0
    return-void

    .line 546097
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    .line 546098
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 546099
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    int-to-long v2, p3

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 546100
    new-instance v0, LX/7Cx;

    invoke-direct {v0, p4, p1, p2}, LX/7Cx;-><init>(LX/3II;FF)V

    .line 546101
    iget-object v1, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 546102
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 546103
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 546104
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 546105
    const/4 v0, 0x0

    .line 546106
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/3IP;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    goto :goto_0
.end method
