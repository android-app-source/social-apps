.class public LX/37K;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 500950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 500951
    return-void
.end method

.method public static a(LX/0QB;)LX/37K;
    .locals 3

    .prologue
    .line 500952
    const-class v1, LX/37K;

    monitor-enter v1

    .line 500953
    :try_start_0
    sget-object v0, LX/37K;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 500954
    sput-object v2, LX/37K;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 500955
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500956
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 500957
    new-instance v0, LX/37K;

    invoke-direct {v0}, LX/37K;-><init>()V

    .line 500958
    move-object v0, v0

    .line 500959
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 500960
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/37K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 500961
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 500962
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
