.class public LX/2mN;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CBi;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/2mN",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CBi;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460260
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 460261
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/2mN;->b:LX/0Zi;

    .line 460262
    iput-object p1, p0, LX/2mN;->a:LX/0Ot;

    .line 460263
    return-void
.end method

.method public static a(LX/0QB;)LX/2mN;
    .locals 4

    .prologue
    .line 460249
    const-class v1, LX/2mN;

    monitor-enter v1

    .line 460250
    :try_start_0
    sget-object v0, LX/2mN;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460251
    sput-object v2, LX/2mN;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460252
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460253
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460254
    new-instance v3, LX/2mN;

    const/16 p0, 0x213f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2mN;-><init>(LX/0Ot;)V

    .line 460255
    move-object v0, v3

    .line 460256
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460257
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2mN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460258
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460259
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 460264
    const v0, 0x58e27275

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 460182
    check-cast p2, LX/CBh;

    .line 460183
    iget-object v0, p0, LX/2mN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CBi;

    iget-object v1, p2, LX/CBh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/CBh;->b:Landroid/view/View$OnClickListener;

    iget-object v3, p2, LX/CBh;->c:LX/1Pn;

    const/4 v5, 0x0

    .line 460184
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 460185
    check-cast v4, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460186
    invoke-static {v4}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 460187
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->t()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Px;

    .line 460188
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v6, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v6

    .line 460189
    const v7, 0x58e27275

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 460190
    invoke-interface {v6, v7}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v7

    .line 460191
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 460192
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v9

    move v6, v5

    :goto_0
    if-ge v6, v9, :cond_1

    invoke-virtual {v4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 460193
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 460194
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    .line 460195
    if-eqz v5, :cond_0

    const-string v10, ""

    if-eq v5, v10, :cond_0

    .line 460196
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v8, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 460197
    :cond_0
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_0

    .line 460198
    :cond_1
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 460199
    iget-object v4, v0, LX/CBi;->e:LX/1DR;

    invoke-virtual {v4}, LX/1DR;->a()I

    move-result v6

    .line 460200
    iget v4, v0, LX/CBi;->c:I

    int-to-float v4, v4

    invoke-static {p1, v4}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v8

    .line 460201
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v4

    iget v9, v0, LX/CBi;->b:I

    mul-int/2addr v4, v9

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    iget v10, v0, LX/CBi;->c:I

    mul-int/2addr v9, v10

    add-int/2addr v9, v4

    .line 460202
    const/4 v4, 0x0

    .line 460203
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v10

    const/4 p0, 0x3

    if-lt v10, p0, :cond_4

    .line 460204
    iget-object v4, v0, LX/CBi;->a:LX/CBZ;

    const/4 v10, 0x0

    .line 460205
    new-instance p0, LX/CBY;

    invoke-direct {p0, v4}, LX/CBY;-><init>(LX/CBZ;)V

    .line 460206
    iget-object p2, v4, LX/CBZ;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/CBX;

    .line 460207
    if-nez p2, :cond_2

    .line 460208
    new-instance p2, LX/CBX;

    invoke-direct {p2, v4}, LX/CBX;-><init>(LX/CBZ;)V

    .line 460209
    :cond_2
    invoke-static {p2, p1, v10, v10, p0}, LX/CBX;->a$redex0(LX/CBX;LX/1De;IILX/CBY;)V

    .line 460210
    move-object p0, p2

    .line 460211
    move-object v10, p0

    .line 460212
    move-object v4, v10

    .line 460213
    iget-object v10, v4, LX/CBX;->a:LX/CBY;

    iput-object v3, v10, LX/CBY;->a:LX/1Pn;

    .line 460214
    iget-object v10, v4, LX/CBX;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v10, p0}, Ljava/util/BitSet;->set(I)V

    .line 460215
    move-object v4, v4

    .line 460216
    iget-object v10, v4, LX/CBX;->a:LX/CBY;

    iput-object v5, v10, LX/CBY;->e:LX/0Px;

    .line 460217
    iget-object v10, v4, LX/CBX;->e:Ljava/util/BitSet;

    const/4 p0, 0x4

    invoke-virtual {v10, p0}, Ljava/util/BitSet;->set(I)V

    .line 460218
    move-object v4, v4

    .line 460219
    iget-object v5, v4, LX/CBX;->a:LX/CBY;

    iput-object v2, v5, LX/CBY;->d:Landroid/view/View$OnClickListener;

    .line 460220
    iget-object v5, v4, LX/CBX;->e:Ljava/util/BitSet;

    const/4 v10, 0x3

    invoke-virtual {v5, v10}, Ljava/util/BitSet;->set(I)V

    .line 460221
    move-object v4, v4

    .line 460222
    iget-object v5, v4, LX/CBX;->a:LX/CBY;

    iput v8, v5, LX/CBY;->c:I

    .line 460223
    iget-object v5, v4, LX/CBX;->e:Ljava/util/BitSet;

    const/4 v10, 0x2

    invoke-virtual {v5, v10}, Ljava/util/BitSet;->set(I)V

    .line 460224
    move-object v4, v4

    .line 460225
    iget v5, v0, LX/CBi;->b:I

    .line 460226
    iget-object v8, v4, LX/CBX;->a:LX/CBY;

    iput v5, v8, LX/CBY;->b:I

    .line 460227
    iget-object v8, v4, LX/CBX;->e:Ljava/util/BitSet;

    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Ljava/util/BitSet;->set(I)V

    .line 460228
    move-object v4, v4

    .line 460229
    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x7

    const v8, 0x7f0b1cde

    invoke-interface {v4, v5, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    .line 460230
    if-ge v9, v6, :cond_3

    .line 460231
    invoke-interface {v4, v9}, LX/1Di;->g(I)LX/1Di;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    .line 460232
    :cond_3
    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    .line 460233
    :cond_4
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v5

    const v6, 0x7f0a0160

    invoke-virtual {v5, v6}, LX/25Q;->i(I)LX/25Q;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b0033

    invoke-interface {v5, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    const/4 v6, 0x6

    const v8, 0x7f0b1cdf

    invoke-interface {v5, v6, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x4

    invoke-interface {v5, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v5

    invoke-interface {v7, v5}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/CBi;->f:LX/1V0;

    check-cast v3, LX/1Ps;

    new-instance v6, LX/1X6;

    sget-object v7, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v6, v1, v7}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    iget-object v7, v0, LX/CBi;->d:LX/2mO;

    invoke-virtual {v7, p1}, LX/2mO;->c(LX/1De;)LX/CBU;

    move-result-object v7

    invoke-virtual {v7, v1}, LX/CBU;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/CBU;

    move-result-object v7

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    .line 460234
    iget-object v9, v7, LX/CBU;->a:LX/CBV;

    iput-object v8, v9, LX/CBV;->a:Landroid/text/Layout$Alignment;

    .line 460235
    move-object v7, v7

    .line 460236
    invoke-virtual {v7}, LX/1X5;->d()LX/1X1;

    move-result-object v7

    invoke-virtual {v5, p1, v3, v6, v7}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 460237
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 460238
    invoke-static {}, LX/1dS;->b()V

    .line 460239
    iget v0, p1, LX/1dQ;->b:I

    .line 460240
    packed-switch v0, :pswitch_data_0

    .line 460241
    :goto_0
    return-object v2

    .line 460242
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 460243
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 460244
    check-cast v1, LX/CBh;

    .line 460245
    iget-object p1, p0, LX/2mN;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/CBh;->b:Landroid/view/View$OnClickListener;

    .line 460246
    if-eqz p1, :cond_0

    .line 460247
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 460248
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x58e27275
        :pswitch_0
    .end packed-switch
.end method
