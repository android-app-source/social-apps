.class public LX/2cl;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hj;


# instance fields
.field private final a:LX/1k4;


# direct methods
.method public constructor <init>(LX/1k4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 441835
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 441836
    iput-object p1, p0, LX/2cl;->a:LX/1k4;

    .line 441837
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 441810
    const-class v0, LX/1kW;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441811
    const-class v0, LX/1kW;

    .line 441812
    :goto_0
    return-object v0

    .line 441813
    :cond_0
    const-class v0, LX/1kJ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 441814
    const-class v0, LX/1kJ;

    goto :goto_0

    .line 441815
    :cond_1
    const-class v0, LX/1kV;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 441816
    const-class v0, LX/1kV;

    goto :goto_0

    .line 441817
    :cond_2
    const-class v0, LX/Ayb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 441818
    const-class v0, LX/Ayb;

    goto :goto_0

    .line 441819
    :cond_3
    const-class v0, LX/1kK;

    goto :goto_0
.end method

.method private static b(IILandroid/content/Intent;)Z
    .locals 1
    .param p2    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 441829
    const/16 v0, 0x6dc

    if-eq p0, v0, :cond_0

    const/16 v0, 0x20b3

    if-eq p0, v0, :cond_0

    const/16 v0, 0x20b4

    if-eq p0, v0, :cond_0

    const/16 v0, 0x20b5

    if-eq p0, v0, :cond_0

    const/16 v0, 0x20b6

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc37

    if-ne p0, v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 441830
    if-eqz v0, :cond_2

    .line 441831
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    if-nez p1, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 441832
    if-eqz v0, :cond_2

    .line 441833
    if-eqz p2, :cond_5

    const-string v0, "prompt_entry_point_analytics_extra"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "prompt_object_class_name_extra"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 441834
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 441820
    invoke-static {p1, p2, p3}, LX/2cl;->b(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 441821
    :cond_0
    :goto_0
    return-void

    .line 441822
    :cond_1
    const-string v0, "prompt_object_class_name_extra"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 441823
    const-string v0, "prompt_entry_point_analytics_extra"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 441824
    const/4 v2, -0x1

    if-ne p2, v2, :cond_2

    .line 441825
    const-string v2, "did_use_prompt_extra"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 441826
    iget-object v3, p0, LX/2cl;->a:LX/1k4;

    new-instance v4, LX/1k5;

    invoke-static {v1}, LX/2cl;->a(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v4, v1, v0, v2}, LX/1k5;-><init>(Ljava/lang/Class;Lcom/facebook/productionprompts/logging/PromptAnalytics;Z)V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 441827
    :cond_2
    if-nez p2, :cond_0

    .line 441828
    iget-object v2, p0, LX/2cl;->a:LX/1k4;

    new-instance v3, LX/1k7;

    invoke-static {v1}, LX/2cl;->a(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v3, v1, v0}, LX/1k7;-><init>(Ljava/lang/Class;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method
