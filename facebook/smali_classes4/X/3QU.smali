.class public LX/3QU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/34G;


# direct methods
.method public constructor <init>(LX/34G;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 565151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565152
    iput-object p1, p0, LX/3QU;->a:LX/34G;

    .line 565153
    return-void
.end method

.method public static a(LX/0QB;)LX/3QU;
    .locals 1

    .prologue
    .line 565154
    invoke-static {p0}, LX/3QU;->b(LX/0QB;)LX/3QU;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Lcom/facebook/messaging/model/messages/Message;
    .locals 2

    .prologue
    .line 565155
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 565156
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    iget-object v1, p0, LX/3QU;->a:LX/34G;

    invoke-virtual {v1, p1, p2}, LX/34G;->b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v1

    .line 565157
    iput-object v1, v0, LX/6f7;->f:Ljava/lang/String;

    .line 565158
    move-object v0, v0

    .line 565159
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object p1

    .line 565160
    :cond_0
    return-object p1
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/GroupMessageInfo;LX/DiC;Lcom/facebook/push/PushProperty;LX/Dhq;ZLcom/facebook/messaging/push/flags/ServerMessageAlertFlags;LX/DiB;)Lcom/facebook/messaging/notify/NewMessageNotification;
    .locals 11
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/messaging/model/threads/GroupMessageInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/DiC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/Dhq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 565161
    new-instance v0, Lcom/facebook/messaging/notify/NewDefaultMessageNotification;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/facebook/messaging/notify/NewDefaultMessageNotification;-><init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/GroupMessageInfo;LX/DiC;Lcom/facebook/push/PushProperty;LX/Dhq;ZLcom/facebook/messaging/push/flags/ServerMessageAlertFlags;LX/DiB;)V

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3QU;
    .locals 2

    .prologue
    .line 565162
    new-instance v1, LX/3QU;

    invoke-static {p0}, LX/34G;->b(LX/0QB;)LX/34G;

    move-result-object v0

    check-cast v0, LX/34G;

    invoke-direct {v1, v0}, LX/3QU;-><init>(LX/34G;)V

    .line 565163
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadCustomization;Lcom/facebook/push/PushProperty;LX/03R;)Lcom/facebook/messaging/notify/NewMessageNotification;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 565164
    new-instance v0, LX/Dnh;

    invoke-direct {v0}, LX/Dnh;-><init>()V

    invoke-virtual {p5, v6}, LX/03R;->asBoolean(Z)Z

    move-result v1

    .line 565165
    iput-boolean v1, v0, LX/Dnh;->d:Z

    .line 565166
    move-object v0, v0

    .line 565167
    invoke-virtual {v0}, LX/Dnh;->a()Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 565168
    invoke-virtual/range {v0 .. v6}, LX/3QU;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadCustomization;Lcom/facebook/push/PushProperty;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Z)Lcom/facebook/messaging/notify/NewMessageNotification;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadCustomization;Lcom/facebook/push/PushProperty;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Z)Lcom/facebook/messaging/notify/NewMessageNotification;
    .locals 11

    .prologue
    .line 565169
    iget-object v1, p0, LX/3QU;->a:LX/34G;

    sget-object v2, LX/Di4;->ALWAYS:LX/Di4;

    move/from16 v0, p6

    invoke-virtual {v1, p1, p3, v2, v0}, LX/34G;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;LX/Di4;Z)Ljava/lang/String;

    move-result-object v1

    .line 565170
    invoke-direct {p0, p1, p3}, LX/3QU;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 565171
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v10, LX/DiB;->UNKNOWN:LX/DiB;

    move-object v3, p2

    move-object v6, p4

    move-object/from16 v9, p5

    invoke-static/range {v1 .. v10}, LX/3QU;->a(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/GroupMessageInfo;LX/DiC;Lcom/facebook/push/PushProperty;LX/Dhq;ZLcom/facebook/messaging/push/flags/ServerMessageAlertFlags;LX/DiB;)Lcom/facebook/messaging/notify/NewMessageNotification;

    move-result-object v1

    return-object v1
.end method
