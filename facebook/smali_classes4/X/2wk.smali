.class public final enum LX/2wk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2wk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2wk;

.field public static final enum CLIENT_CONNECT:LX/2wk;


# instance fields
.field public final perfLoggerName:Ljava/lang/String;

.field public final perfMarkerId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 478758
    new-instance v0, LX/2wk;

    const-string v1, "CLIENT_CONNECT"

    const v2, 0x330005

    invoke-direct {v0, v1, v3, v2}, LX/2wk;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/2wk;->CLIENT_CONNECT:LX/2wk;

    .line 478759
    const/4 v0, 0x1

    new-array v0, v0, [LX/2wk;

    sget-object v1, LX/2wk;->CLIENT_CONNECT:LX/2wk;

    aput-object v1, v0, v3

    sput-object v0, LX/2wk;->$VALUES:[LX/2wk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 478760
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 478761
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GooglePlayFbLocationManager."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/2wk;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2wk;->perfLoggerName:Ljava/lang/String;

    .line 478762
    iput p3, p0, LX/2wk;->perfMarkerId:I

    .line 478763
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2wk;
    .locals 1

    .prologue
    .line 478764
    const-class v0, LX/2wk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2wk;

    return-object v0
.end method

.method public static values()[LX/2wk;
    .locals 1

    .prologue
    .line 478765
    sget-object v0, LX/2wk;->$VALUES:[LX/2wk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2wk;

    return-object v0
.end method
