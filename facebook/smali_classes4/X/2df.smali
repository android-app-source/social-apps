.class public final enum LX/2df;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2df;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2df;

.field public static final enum HEAD:LX/2df;

.field public static final enum TAIL:LX/2df;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 443668
    new-instance v0, LX/2df;

    const-string v1, "HEAD"

    invoke-direct {v0, v1, v2}, LX/2df;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2df;->HEAD:LX/2df;

    .line 443669
    new-instance v0, LX/2df;

    const-string v1, "TAIL"

    invoke-direct {v0, v1, v3}, LX/2df;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2df;->TAIL:LX/2df;

    .line 443670
    const/4 v0, 0x2

    new-array v0, v0, [LX/2df;

    sget-object v1, LX/2df;->HEAD:LX/2df;

    aput-object v1, v0, v2

    sget-object v1, LX/2df;->TAIL:LX/2df;

    aput-object v1, v0, v3

    sput-object v0, LX/2df;->$VALUES:[LX/2df;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 443671
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2df;
    .locals 1

    .prologue
    .line 443667
    const-class v0, LX/2df;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2df;

    return-object v0
.end method

.method public static values()[LX/2df;
    .locals 1

    .prologue
    .line 443666
    sget-object v0, LX/2df;->$VALUES:[LX/2df;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2df;

    return-object v0
.end method
