.class public abstract LX/2x6;
.super LX/2x7;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2x7",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:Landroid/os/Bundle;

.field public final synthetic c:LX/2wI;


# direct methods
.method public constructor <init>(LX/2wI;ILandroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/support/annotation/BinderThread;
    .end annotation

    iput-object p1, p0, LX/2x6;->c:LX/2wI;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/2x7;-><init>(LX/2wI;Ljava/lang/Object;)V

    iput p2, p0, LX/2x6;->a:I

    iput-object p3, p0, LX/2x6;->b:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public abstract a(Lcom/google/android/gms/common/ConnectionResult;)V
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Ljava/lang/Boolean;

    const/4 v2, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_1

    iget-object v1, p0, LX/2x6;->c:LX/2wI;

    invoke-static {v1, v2, v0}, LX/2wI;->a$redex0(LX/2wI;ILandroid/os/IInterface;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, LX/2x6;->a:I

    sparse-switch v1, :sswitch_data_0

    iget-object v1, p0, LX/2x6;->c:LX/2wI;

    invoke-static {v1, v2, v0}, LX/2wI;->a$redex0(LX/2wI;ILandroid/os/IInterface;)V

    iget-object v1, p0, LX/2x6;->b:Landroid/os/Bundle;

    if-eqz v1, :cond_2

    iget-object v0, p0, LX/2x6;->b:Landroid/os/Bundle;

    const-string v1, "pendingIntent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    :cond_2
    new-instance v1, Lcom/google/android/gms/common/ConnectionResult;

    iget v2, p0, LX/2x6;->a:I

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {p0, v1}, LX/2x6;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0

    :sswitch_0
    invoke-virtual {p0}, LX/2x6;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/2x6;->c:LX/2wI;

    invoke-static {v1, v2, v0}, LX/2wI;->a$redex0(LX/2wI;ILandroid/os/IInterface;)V

    new-instance v1, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v2, 0x8

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {p0, v1}, LX/2x6;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, LX/2x6;->c:LX/2wI;

    invoke-static {v1, v2, v0}, LX/2wI;->a$redex0(LX/2wI;ILandroid/os/IInterface;)V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A fatal developer error has occurred. Check the logs for further information."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public abstract a()Z
.end method
