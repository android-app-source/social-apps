.class public LX/2v2;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CCh;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/2v2",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CCh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 477199
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 477200
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/2v2;->b:LX/0Zi;

    .line 477201
    iput-object p1, p0, LX/2v2;->a:LX/0Ot;

    .line 477202
    return-void
.end method

.method public static a(LX/0QB;)LX/2v2;
    .locals 4

    .prologue
    .line 477203
    const-class v1, LX/2v2;

    monitor-enter v1

    .line 477204
    :try_start_0
    sget-object v0, LX/2v2;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 477205
    sput-object v2, LX/2v2;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 477206
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477207
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 477208
    new-instance v3, LX/2v2;

    const/16 p0, 0x2197

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2v2;-><init>(LX/0Ot;)V

    .line 477209
    move-object v0, v3

    .line 477210
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 477211
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2v2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 477212
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 477213
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 477214
    check-cast p2, LX/CCf;

    .line 477215
    iget-object v0, p0, LX/2v2;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CCh;

    iget-object v1, p2, LX/CCf;->a:Ljava/lang/String;

    iget-object v2, p2, LX/CCf;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/CCf;->c:LX/1Pf;

    .line 477216
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v4

    .line 477217
    iget-object v5, v0, LX/CCh;->b:Landroid/content/res/Resources;

    const v6, 0x7f0218ad

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 477218
    iget-object v6, v0, LX/CCh;->c:LX/0wM;

    const v7, 0x7f020874

    const/4 p0, -0x1

    invoke-virtual {v6, v7, p0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 477219
    iget-object v7, v0, LX/CCh;->b:Landroid/content/res/Resources;

    const p0, 0x7f0b1bb2

    invoke-virtual {v7, p0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    .line 477220
    new-instance p0, Landroid/graphics/drawable/InsetDrawable;

    invoke-direct {p0, v6, v7}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 477221
    new-instance v6, Landroid/graphics/drawable/LayerDrawable;

    const/4 v7, 0x2

    new-array v7, v7, [Landroid/graphics/drawable/Drawable;

    const/4 p2, 0x0

    aput-object v5, v7, p2

    const/4 v5, 0x1

    aput-object p0, v7, v5

    invoke-direct {v6, v7}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    move-object v5, v6

    .line 477222
    invoke-virtual {v4, v5}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v4

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    .line 477223
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    iget-object v4, v0, LX/CCh;->e:LX/1Vm;

    invoke-virtual {v4, p1}, LX/1Vm;->c(LX/1De;)LX/C2N;

    move-result-object v4

    .line 477224
    new-instance v7, LX/CCg;

    invoke-direct {v7, v0, v1}, LX/CCg;-><init>(LX/CCh;Ljava/lang/String;)V

    move-object v7, v7

    .line 477225
    invoke-virtual {v4, v7}, LX/C2N;->a(Landroid/view/View$OnClickListener;)LX/C2N;

    move-result-object v7

    .line 477226
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 477227
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v7, v4}, LX/C2N;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/C2N;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/C2N;->a(LX/1Pp;)LX/C2N;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/C2N;->a(LX/1dc;)LX/C2N;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/CCh;->f:LX/CCk;

    const/4 v6, 0x0

    .line 477228
    new-instance v7, LX/CCj;

    invoke-direct {v7, v5}, LX/CCj;-><init>(LX/CCk;)V

    .line 477229
    sget-object p0, LX/CCk;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/CCi;

    .line 477230
    if-nez p0, :cond_0

    .line 477231
    new-instance p0, LX/CCi;

    invoke-direct {p0}, LX/CCi;-><init>()V

    .line 477232
    :cond_0
    invoke-static {p0, p1, v6, v6, v7}, LX/CCi;->a$redex0(LX/CCi;LX/1De;IILX/CCj;)V

    .line 477233
    move-object v7, p0

    .line 477234
    move-object v6, v7

    .line 477235
    move-object v5, v6

    .line 477236
    iget-object v6, v5, LX/CCi;->a:LX/CCj;

    iput-object v1, v6, LX/CCj;->a:Ljava/lang/String;

    .line 477237
    iget-object v6, v5, LX/CCi;->d:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 477238
    move-object v5, v5

    .line 477239
    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 477240
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 477241
    invoke-static {}, LX/1dS;->b()V

    .line 477242
    const/4 v0, 0x0

    return-object v0
.end method
