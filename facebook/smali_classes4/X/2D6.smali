.class public LX/2D6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:I

.field private static volatile f:LX/2D6;


# instance fields
.field private c:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public d:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/content/Context;
    .annotation build Lcom/facebook/inject/ForAppContext;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 383638
    const-class v0, LX/2D6;

    sput-object v0, LX/2D6;->a:Ljava/lang/Class;

    .line 383639
    const v0, 0x7f0d0220

    sput v0, LX/2D6;->b:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 383691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383692
    return-void
.end method

.method public static a(LX/0QB;)LX/2D6;
    .locals 6

    .prologue
    .line 383676
    sget-object v0, LX/2D6;->f:LX/2D6;

    if-nez v0, :cond_1

    .line 383677
    const-class v1, LX/2D6;

    monitor-enter v1

    .line 383678
    :try_start_0
    sget-object v0, LX/2D6;->f:LX/2D6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 383679
    if-eqz v2, :cond_0

    .line 383680
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 383681
    new-instance v5, LX/2D6;

    invoke-direct {v5}, LX/2D6;-><init>()V

    .line 383682
    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    const-class v4, Landroid/content/Context;

    const-class p0, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v4, p0}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    .line 383683
    iput-object v3, v5, LX/2D6;->d:LX/0W3;

    iput-object v4, v5, LX/2D6;->e:Landroid/content/Context;

    .line 383684
    move-object v0, v5

    .line 383685
    sput-object v0, LX/2D6;->f:LX/2D6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 383686
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 383687
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 383688
    :cond_1
    sget-object v0, LX/2D6;->f:LX/2D6;

    return-object v0

    .line 383689
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 383690
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 383664
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2D6;->e:Landroid/content/Context;

    invoke-static {v0}, LX/45m;->a(Landroid/content/Context;)LX/45m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 383665
    if-nez v0, :cond_1

    .line 383666
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 383667
    :cond_1
    :try_start_1
    sget-object v1, LX/1vX;->c:LX/1vX;

    move-object v1, v1

    .line 383668
    iget-object v2, p0, LX/2D6;->e:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/1od;->a(Landroid/content/Context;)I

    move-result v1

    .line 383669
    if-nez v1, :cond_0

    .line 383670
    iget-object v1, p0, LX/2D6;->d:LX/0W3;

    sget-wide v2, LX/0X5;->W:J

    invoke-interface {v1, v2, v3}, LX/0W4;->a(J)Z

    move-result v1

    .line 383671
    sget v2, LX/2D6;->b:I

    invoke-virtual {v0, v2}, LX/45m;->a(I)V

    .line 383672
    if-eqz v1, :cond_0

    .line 383673
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2D6;->c:Z

    .line 383674
    invoke-virtual {p0}, LX/2D6;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 383675
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 8

    .prologue
    .line 383647
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2D6;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 383648
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 383649
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2D6;->e:Landroid/content/Context;

    invoke-static {v0}, LX/45m;->a(Landroid/content/Context;)LX/45m;

    move-result-object v0

    .line 383650
    if-eqz v0, :cond_0

    .line 383651
    sget v1, LX/2D6;->b:I

    invoke-virtual {v0, v1}, LX/45m;->a(I)V

    .line 383652
    iget-object v1, p0, LX/2D6;->d:LX/0W3;

    sget-wide v2, LX/0X5;->X:J

    invoke-interface {v1, v2, v3}, LX/0W4;->c(J)J

    move-result-wide v2

    .line 383653
    iget-object v1, p0, LX/2D6;->d:LX/0W3;

    sget-wide v4, LX/0X5;->Y:J

    invoke-interface {v1, v4, v5}, LX/0W4;->c(J)J

    move-result-wide v4

    .line 383654
    new-instance v1, LX/45t;

    sget v6, LX/2D6;->b:I

    invoke-direct {v1, v6}, LX/45t;-><init>(I)V

    const/4 v6, 0x0

    .line 383655
    iput v6, v1, LX/45t;->b:I

    .line 383656
    move-object v1, v1

    .line 383657
    iput-wide v2, v1, LX/45t;->d:J

    .line 383658
    move-object v1, v1

    .line 383659
    iput-wide v4, v1, LX/45t;->e:J

    .line 383660
    move-object v1, v1

    .line 383661
    invoke-virtual {v1}, LX/45t;->a()LX/45u;

    move-result-object v1

    .line 383662
    invoke-virtual {v0, v1}, LX/45m;->a(LX/45u;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 383663
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 383641
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/2D6;->c:Z

    .line 383642
    iget-object v0, p0, LX/2D6;->e:Landroid/content/Context;

    invoke-static {v0}, LX/45m;->a(Landroid/content/Context;)LX/45m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 383643
    if-nez v0, :cond_0

    .line 383644
    :goto_0
    monitor-exit p0

    return-void

    .line 383645
    :cond_0
    :try_start_1
    sget v1, LX/2D6;->b:I

    invoke-virtual {v0, v1}, LX/45m;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 383646
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    .prologue
    .line 383640
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2D6;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
