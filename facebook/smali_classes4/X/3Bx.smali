.class public LX/3Bx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3Bx;


# instance fields
.field public final a:LX/04J;

.field private final b:LX/3By;

.field private final c:LX/1CC;


# direct methods
.method public constructor <init>(LX/04J;LX/3By;LX/1CC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 529303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529304
    iput-object p1, p0, LX/3Bx;->a:LX/04J;

    .line 529305
    iput-object p2, p0, LX/3Bx;->b:LX/3By;

    .line 529306
    iput-object p3, p0, LX/3Bx;->c:LX/1CC;

    .line 529307
    return-void
.end method

.method public static a(LX/0QB;)LX/3Bx;
    .locals 6

    .prologue
    .line 529308
    sget-object v0, LX/3Bx;->d:LX/3Bx;

    if-nez v0, :cond_1

    .line 529309
    const-class v1, LX/3Bx;

    monitor-enter v1

    .line 529310
    :try_start_0
    sget-object v0, LX/3Bx;->d:LX/3Bx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 529311
    if-eqz v2, :cond_0

    .line 529312
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 529313
    new-instance p0, LX/3Bx;

    invoke-static {v0}, LX/04J;->a(LX/0QB;)LX/04J;

    move-result-object v3

    check-cast v3, LX/04J;

    invoke-static {v0}, LX/3By;->a(LX/0QB;)LX/3By;

    move-result-object v4

    check-cast v4, LX/3By;

    invoke-static {v0}, LX/1CC;->a(LX/0QB;)LX/1CC;

    move-result-object v5

    check-cast v5, LX/1CC;

    invoke-direct {p0, v3, v4, v5}, LX/3Bx;-><init>(LX/04J;LX/3By;LX/1CC;)V

    .line 529314
    move-object v0, p0

    .line 529315
    sput-object v0, LX/3Bx;->d:LX/3Bx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529316
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 529317
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 529318
    :cond_1
    sget-object v0, LX/3Bx;->d:LX/3Bx;

    return-object v0

    .line 529319
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 529320
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    .line 529321
    invoke-interface {p2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    .line 529322
    if-nez v0, :cond_1

    .line 529323
    :cond_0
    :goto_0
    return-void

    .line 529324
    :cond_1
    iget-object v1, p0, LX/3Bx;->b:LX/3By;

    .line 529325
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 529326
    iget-object p0, v1, LX/3By;->b:Ljava/util/Map;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Map;

    .line 529327
    if-nez p0, :cond_2

    .line 529328
    const/4 p0, 0x0

    .line 529329
    :goto_1
    move-object v0, p0

    .line 529330
    if-eqz v0, :cond_0

    .line 529331
    invoke-virtual {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    :cond_2
    invoke-static {p0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object p0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    .line 529332
    instance-of v0, p2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v0, :cond_2

    .line 529333
    check-cast p2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 529334
    invoke-virtual {p2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->a()Ljava/lang/String;

    move-result-object v0

    .line 529335
    if-nez v0, :cond_3

    .line 529336
    :cond_0
    :goto_0
    iget-object v0, p0, LX/3Bx;->c:LX/1CC;

    invoke-virtual {v0}, LX/1CC;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 529337
    sget-object v0, LX/0JS;->SESSION_ID:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v1, p0, LX/3Bx;->c:LX/1CC;

    .line 529338
    iget-object p0, v1, LX/1CC;->d:Ljava/lang/String;

    move-object v1, p0

    .line 529339
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 529340
    :cond_1
    sget-object v0, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    sget-object v1, LX/04D;->VIDEO_HOME:LX/04D;

    iget-object v1, v1, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 529341
    return-void

    .line 529342
    :cond_2
    invoke-direct {p0, p1, p2}, LX/3Bx;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_0

    .line 529343
    :cond_3
    iget-object v1, p0, LX/3Bx;->a:LX/04J;

    invoke-virtual {v1, v0}, LX/04J;->a(Ljava/lang/String;)LX/0P1;

    move-result-object v0

    .line 529344
    if-eqz v0, :cond_0

    .line 529345
    invoke-virtual {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method
