.class public LX/3QV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3QW;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/3QW;LX/0Or;Landroid/content/res/Resources;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3QW;",
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 565172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565173
    iput-object p1, p0, LX/3QV;->a:LX/3QW;

    .line 565174
    iput-object p2, p0, LX/3QV;->b:LX/0Or;

    .line 565175
    iput-object p3, p0, LX/3QV;->c:Landroid/content/res/Resources;

    .line 565176
    return-void
.end method

.method public static a(LX/0QB;)LX/3QV;
    .locals 4

    .prologue
    .line 565177
    new-instance v2, LX/3QV;

    invoke-static {p0}, LX/3QW;->a(LX/0QB;)LX/3QW;

    move-result-object v0

    check-cast v0, LX/3QW;

    const/16 v1, 0x2847

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-direct {v2, v0, v3, v1}, LX/3QV;-><init>(LX/3QW;LX/0Or;Landroid/content/res/Resources;)V

    .line 565178
    move-object v0, v2

    .line 565179
    return-object v0
.end method
