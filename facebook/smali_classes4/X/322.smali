.class public final LX/322;
.super LX/32s;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _annotated:LX/2Am;

.field public final transient c:Ljava/lang/reflect/Field;


# direct methods
.method public constructor <init>(LX/2Aq;LX/0lJ;LX/4qw;LX/0lQ;LX/2Am;)V
    .locals 1

    .prologue
    .line 489146
    invoke-direct {p0, p1, p2, p3, p4}, LX/32s;-><init>(LX/2Aq;LX/0lJ;LX/4qw;LX/0lQ;)V

    .line 489147
    iput-object p5, p0, LX/322;->_annotated:LX/2Am;

    .line 489148
    iget-object v0, p5, LX/2Am;->a:Ljava/lang/reflect/Field;

    move-object v0, v0

    .line 489149
    iput-object v0, p0, LX/322;->c:Ljava/lang/reflect/Field;

    .line 489150
    return-void
.end method

.method private constructor <init>(LX/322;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/322;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 489151
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 489152
    iget-object v0, p1, LX/322;->_annotated:LX/2Am;

    iput-object v0, p0, LX/322;->_annotated:LX/2Am;

    .line 489153
    iget-object v0, p1, LX/322;->c:Ljava/lang/reflect/Field;

    iput-object v0, p0, LX/322;->c:Ljava/lang/reflect/Field;

    .line 489154
    return-void
.end method

.method private constructor <init>(LX/322;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 489155
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Ljava/lang/String;)V

    .line 489156
    iget-object v0, p1, LX/322;->_annotated:LX/2Am;

    iput-object v0, p0, LX/322;->_annotated:LX/2Am;

    .line 489157
    iget-object v0, p1, LX/322;->c:Ljava/lang/reflect/Field;

    iput-object v0, p0, LX/322;->c:Ljava/lang/reflect/Field;

    .line 489158
    return-void
.end method

.method private constructor <init>(LX/322;Ljava/lang/reflect/Field;)V
    .locals 3

    .prologue
    .line 489159
    invoke-direct {p0, p1}, LX/32s;-><init>(LX/32s;)V

    .line 489160
    iget-object v0, p1, LX/322;->_annotated:LX/2Am;

    iput-object v0, p0, LX/322;->_annotated:LX/2Am;

    .line 489161
    if-nez p2, :cond_0

    .line 489162
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No Field passed for property \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 489163
    iget-object v2, p1, LX/32s;->_propName:Ljava/lang/String;

    move-object v2, v2

    .line 489164
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' (class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LX/32s;->h()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 489165
    :cond_0
    iput-object p2, p0, LX/322;->c:Ljava/lang/reflect/Field;

    .line 489166
    return-void
.end method

.method private a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/322;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "LX/322;"
        }
    .end annotation

    .prologue
    .line 489167
    new-instance v0, LX/322;

    invoke-direct {v0, p0, p1}, LX/322;-><init>(LX/322;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    return-object v0
.end method

.method private c(Ljava/lang/String;)LX/322;
    .locals 1

    .prologue
    .line 489144
    new-instance v0, LX/322;

    invoke-direct {v0, p0, p1}, LX/322;-><init>(LX/322;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)LX/32s;
    .locals 1

    .prologue
    .line 489145
    invoke-direct {p0, p1}, LX/322;->c(Ljava/lang/String;)LX/322;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 489142
    invoke-virtual {p0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 489143
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 489138
    :try_start_0
    iget-object v0, p0, LX/322;->c:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p1, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 489139
    :goto_0
    return-void

    .line 489140
    :catch_0
    move-exception v0

    .line 489141
    invoke-virtual {p0, v0, p2}, LX/32s;->a(Ljava/lang/Exception;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()LX/2An;
    .locals 1

    .prologue
    .line 489137
    iget-object v0, p0, LX/322;->_annotated:LX/2Am;

    return-object v0
.end method

.method public final synthetic b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;
    .locals 1

    .prologue
    .line 489136
    invoke-direct {p0, p1}, LX/322;->a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/322;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 489135
    invoke-virtual {p0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, LX/32s;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 489131
    :try_start_0
    iget-object v0, p0, LX/322;->c:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p1, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 489132
    :goto_0
    return-object p1

    .line 489133
    :catch_0
    move-exception v0

    .line 489134
    invoke-virtual {p0, v0, p2}, LX/32s;->a(Ljava/lang/Exception;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final readResolve()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 489128
    new-instance v0, LX/322;

    iget-object v1, p0, LX/322;->_annotated:LX/2Am;

    .line 489129
    iget-object v2, v1, LX/2Am;->a:Ljava/lang/reflect/Field;

    move-object v1, v2

    .line 489130
    invoke-direct {v0, p0, v1}, LX/322;-><init>(LX/322;Ljava/lang/reflect/Field;)V

    return-object v0
.end method
