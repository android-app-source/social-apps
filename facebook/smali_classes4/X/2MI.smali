.class public LX/2MI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2MI;


# instance fields
.field public final b:LX/0WJ;

.field private final c:LX/0Xl;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Kk;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Yb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 396405
    const-class v0, LX/2MI;

    sput-object v0, LX/2MI;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0WJ;LX/0Xl;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "LX/3Kk;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396385
    iput-object p1, p0, LX/2MI;->b:LX/0WJ;

    .line 396386
    iput-object p2, p0, LX/2MI;->c:LX/0Xl;

    .line 396387
    iput-object p3, p0, LX/2MI;->d:LX/0Or;

    .line 396388
    return-void
.end method

.method public static a(LX/0QB;)LX/2MI;
    .locals 6

    .prologue
    .line 396392
    sget-object v0, LX/2MI;->f:LX/2MI;

    if-nez v0, :cond_1

    .line 396393
    const-class v1, LX/2MI;

    monitor-enter v1

    .line 396394
    :try_start_0
    sget-object v0, LX/2MI;->f:LX/2MI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 396395
    if-eqz v2, :cond_0

    .line 396396
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 396397
    new-instance v5, LX/2MI;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    const/16 p0, 0xd06

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/2MI;-><init>(LX/0WJ;LX/0Xl;LX/0Or;)V

    .line 396398
    move-object v0, v5

    .line 396399
    sput-object v0, LX/2MI;->f:LX/2MI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396400
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 396401
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 396402
    :cond_1
    sget-object v0, LX/2MI;->f:LX/2MI;

    return-object v0

    .line 396403
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 396404
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 396389
    iget-object v0, p0, LX/2MI;->c:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.contacts.CONTACT_BULK_DELETE"

    new-instance v2, LX/2MJ;

    invoke-direct {v2, p0}, LX/2MJ;-><init>(LX/2MI;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2MI;->e:LX/0Yb;

    .line 396390
    iget-object v0, p0, LX/2MI;->e:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 396391
    return-void
.end method
