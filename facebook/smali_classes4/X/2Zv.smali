.class public final enum LX/2Zv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Zv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Zv;

.field public static final enum BLOCK_FOR_CACHED_COMPOSER_OPTIONS:LX/2Zv;

.field public static final enum COMPOSER_OPTIONS_FETCHED:LX/2Zv;

.field public static final enum COMPOSER_OPTIONS_REQUEST_RESULT:LX/2Zv;

.field public static final enum EDIT_PRIVACY_CANCEL:LX/2Zv;

.field public static final enum EDIT_PRIVACY_OPEN:LX/2Zv;

.field public static final enum EDIT_PRIVACY_RETURNED:LX/2Zv;

.field public static final enum EDIT_PRIVACY_SAVED:LX/2Zv;

.field public static final enum EDIT_PRIVACY_SAVE_FAILED:LX/2Zv;

.field public static final enum EDIT_STORY_PRIVACY_CANCEL:LX/2Zv;

.field public static final enum EDIT_STORY_PRIVACY_DISCARD:LX/2Zv;

.field public static final enum EDIT_STORY_PRIVACY_OPEN:LX/2Zv;

.field public static final enum EDIT_STORY_PRIVACY_SAVED:LX/2Zv;

.field public static final enum EDIT_STORY_PRIVACY_SAVE_FAILED:LX/2Zv;

.field public static final enum INLINE_PRIVACY_SURVEY_HIDE_TO_DEDUP:LX/2Zv;

.field public static final enum PRIVACY_CHANGE_IGNORED_ON_FETCH:LX/2Zv;

.field public static final enum PRIVACY_OPTION_COMPARE_MATCH:LX/2Zv;

.field public static final enum STICKY_PRIVACY_CHANGED_BY_FETCH:LX/2Zv;


# instance fields
.field public final eventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 423103
    new-instance v0, LX/2Zv;

    const-string v1, "COMPOSER_OPTIONS_FETCHED"

    const-string v2, "composer_options_fetched"

    invoke-direct {v0, v1, v4, v2}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->COMPOSER_OPTIONS_FETCHED:LX/2Zv;

    .line 423104
    new-instance v0, LX/2Zv;

    const-string v1, "COMPOSER_OPTIONS_REQUEST_RESULT"

    const-string v2, "composer_options_request_result"

    invoke-direct {v0, v1, v5, v2}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->COMPOSER_OPTIONS_REQUEST_RESULT:LX/2Zv;

    .line 423105
    new-instance v0, LX/2Zv;

    const-string v1, "STICKY_PRIVACY_CHANGED_BY_FETCH"

    const-string v2, "sticky_changed_by_fetch"

    invoke-direct {v0, v1, v6, v2}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->STICKY_PRIVACY_CHANGED_BY_FETCH:LX/2Zv;

    .line 423106
    new-instance v0, LX/2Zv;

    const-string v1, "PRIVACY_CHANGE_IGNORED_ON_FETCH"

    const-string v2, "composer_privacy_ignored_on_fetch"

    invoke-direct {v0, v1, v7, v2}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->PRIVACY_CHANGE_IGNORED_ON_FETCH:LX/2Zv;

    .line 423107
    new-instance v0, LX/2Zv;

    const-string v1, "BLOCK_FOR_CACHED_COMPOSER_OPTIONS"

    const-string v2, "block_for_cached_composer_options"

    invoke-direct {v0, v1, v8, v2}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->BLOCK_FOR_CACHED_COMPOSER_OPTIONS:LX/2Zv;

    .line 423108
    new-instance v0, LX/2Zv;

    const-string v1, "PRIVACY_OPTION_COMPARE_MATCH"

    const/4 v2, 0x5

    const-string v3, "privacy_options_compare_match"

    invoke-direct {v0, v1, v2, v3}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->PRIVACY_OPTION_COMPARE_MATCH:LX/2Zv;

    .line 423109
    new-instance v0, LX/2Zv;

    const-string v1, "EDIT_PRIVACY_OPEN"

    const/4 v2, 0x6

    const-string v3, "edit_privacy_open"

    invoke-direct {v0, v1, v2, v3}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->EDIT_PRIVACY_OPEN:LX/2Zv;

    .line 423110
    new-instance v0, LX/2Zv;

    const-string v1, "EDIT_PRIVACY_CANCEL"

    const/4 v2, 0x7

    const-string v3, "edit_privacy_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->EDIT_PRIVACY_CANCEL:LX/2Zv;

    .line 423111
    new-instance v0, LX/2Zv;

    const-string v1, "EDIT_PRIVACY_RETURNED"

    const/16 v2, 0x8

    const-string v3, "edit_privacy_returned"

    invoke-direct {v0, v1, v2, v3}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->EDIT_PRIVACY_RETURNED:LX/2Zv;

    .line 423112
    new-instance v0, LX/2Zv;

    const-string v1, "EDIT_PRIVACY_SAVED"

    const/16 v2, 0x9

    const-string v3, "edit_privacy_saved"

    invoke-direct {v0, v1, v2, v3}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->EDIT_PRIVACY_SAVED:LX/2Zv;

    .line 423113
    new-instance v0, LX/2Zv;

    const-string v1, "EDIT_PRIVACY_SAVE_FAILED"

    const/16 v2, 0xa

    const-string v3, "edit_privacy_save_failed"

    invoke-direct {v0, v1, v2, v3}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->EDIT_PRIVACY_SAVE_FAILED:LX/2Zv;

    .line 423114
    new-instance v0, LX/2Zv;

    const-string v1, "EDIT_STORY_PRIVACY_OPEN"

    const/16 v2, 0xb

    const-string v3, "edit_story_privacy_open"

    invoke-direct {v0, v1, v2, v3}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->EDIT_STORY_PRIVACY_OPEN:LX/2Zv;

    .line 423115
    new-instance v0, LX/2Zv;

    const-string v1, "EDIT_STORY_PRIVACY_CANCEL"

    const/16 v2, 0xc

    const-string v3, "edit_story_privacy_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->EDIT_STORY_PRIVACY_CANCEL:LX/2Zv;

    .line 423116
    new-instance v0, LX/2Zv;

    const-string v1, "EDIT_STORY_PRIVACY_DISCARD"

    const/16 v2, 0xd

    const-string v3, "edit_story_privacy_discard"

    invoke-direct {v0, v1, v2, v3}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->EDIT_STORY_PRIVACY_DISCARD:LX/2Zv;

    .line 423117
    new-instance v0, LX/2Zv;

    const-string v1, "EDIT_STORY_PRIVACY_SAVED"

    const/16 v2, 0xe

    const-string v3, "edit_story_privacy_saved"

    invoke-direct {v0, v1, v2, v3}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->EDIT_STORY_PRIVACY_SAVED:LX/2Zv;

    .line 423118
    new-instance v0, LX/2Zv;

    const-string v1, "EDIT_STORY_PRIVACY_SAVE_FAILED"

    const/16 v2, 0xf

    const-string v3, "edit_story_privacy_save_failed"

    invoke-direct {v0, v1, v2, v3}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->EDIT_STORY_PRIVACY_SAVE_FAILED:LX/2Zv;

    .line 423119
    new-instance v0, LX/2Zv;

    const-string v1, "INLINE_PRIVACY_SURVEY_HIDE_TO_DEDUP"

    const/16 v2, 0x10

    const-string v3, "inline_privacy_survey_hide_to_dedup"

    invoke-direct {v0, v1, v2, v3}, LX/2Zv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Zv;->INLINE_PRIVACY_SURVEY_HIDE_TO_DEDUP:LX/2Zv;

    .line 423120
    const/16 v0, 0x11

    new-array v0, v0, [LX/2Zv;

    sget-object v1, LX/2Zv;->COMPOSER_OPTIONS_FETCHED:LX/2Zv;

    aput-object v1, v0, v4

    sget-object v1, LX/2Zv;->COMPOSER_OPTIONS_REQUEST_RESULT:LX/2Zv;

    aput-object v1, v0, v5

    sget-object v1, LX/2Zv;->STICKY_PRIVACY_CHANGED_BY_FETCH:LX/2Zv;

    aput-object v1, v0, v6

    sget-object v1, LX/2Zv;->PRIVACY_CHANGE_IGNORED_ON_FETCH:LX/2Zv;

    aput-object v1, v0, v7

    sget-object v1, LX/2Zv;->BLOCK_FOR_CACHED_COMPOSER_OPTIONS:LX/2Zv;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/2Zv;->PRIVACY_OPTION_COMPARE_MATCH:LX/2Zv;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2Zv;->EDIT_PRIVACY_OPEN:LX/2Zv;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2Zv;->EDIT_PRIVACY_CANCEL:LX/2Zv;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2Zv;->EDIT_PRIVACY_RETURNED:LX/2Zv;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2Zv;->EDIT_PRIVACY_SAVED:LX/2Zv;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/2Zv;->EDIT_PRIVACY_SAVE_FAILED:LX/2Zv;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/2Zv;->EDIT_STORY_PRIVACY_OPEN:LX/2Zv;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/2Zv;->EDIT_STORY_PRIVACY_CANCEL:LX/2Zv;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/2Zv;->EDIT_STORY_PRIVACY_DISCARD:LX/2Zv;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/2Zv;->EDIT_STORY_PRIVACY_SAVED:LX/2Zv;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/2Zv;->EDIT_STORY_PRIVACY_SAVE_FAILED:LX/2Zv;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/2Zv;->INLINE_PRIVACY_SURVEY_HIDE_TO_DEDUP:LX/2Zv;

    aput-object v2, v0, v1

    sput-object v0, LX/2Zv;->$VALUES:[LX/2Zv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 423121
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 423122
    iput-object p3, p0, LX/2Zv;->eventName:Ljava/lang/String;

    .line 423123
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Zv;
    .locals 1

    .prologue
    .line 423124
    const-class v0, LX/2Zv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Zv;

    return-object v0
.end method

.method public static values()[LX/2Zv;
    .locals 1

    .prologue
    .line 423125
    sget-object v0, LX/2Zv;->$VALUES:[LX/2Zv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Zv;

    return-object v0
.end method
