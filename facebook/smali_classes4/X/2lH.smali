.class public abstract LX/2lH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2lI;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<H:",
        "Ljava/lang/Object;",
        "D:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/2lI;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field public final b:LX/2lb;

.field public final c:I

.field public final d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(LX/2lb;ILjava/lang/Object;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2lb;",
            "ITD;",
            "Landroid/view/LayoutInflater;",
            ")V"
        }
    .end annotation

    .prologue
    .line 457525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457526
    iput-object p1, p0, LX/2lH;->b:LX/2lb;

    .line 457527
    iput p2, p0, LX/2lH;->c:I

    .line 457528
    iput-object p3, p0, LX/2lH;->d:Ljava/lang/Object;

    .line 457529
    iput-object p4, p0, LX/2lH;->e:Landroid/view/LayoutInflater;

    .line 457530
    return-void
.end method


# virtual methods
.method public final a()LX/2lb;
    .locals 1

    .prologue
    .line 457531
    iget-object v0, p0, LX/2lH;->b:LX/2lb;

    return-object v0
.end method

.method public a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 457532
    if-nez p2, :cond_0

    .line 457533
    iget-object v0, p0, LX/2lH;->e:Landroid/view/LayoutInflater;

    iget v1, p0, LX/2lH;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 457534
    invoke-virtual {p0, p2}, LX/2lH;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    .line 457535
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 457536
    :goto_0
    invoke-virtual {p0, v0}, LX/2lH;->a(Ljava/lang/Object;)V

    .line 457537
    return-object p2

    .line 457538
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract a(Landroid/view/View;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")TH;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TH;)V"
        }
    .end annotation
.end method

.method public final c()Lcom/facebook/bookmark/model/Bookmark;
    .locals 1

    .prologue
    .line 457539
    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    instance-of v0, v0, Lcom/facebook/bookmark/model/Bookmark;

    if-eqz v0, :cond_0

    .line 457540
    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    .line 457541
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
