.class public final LX/2Vk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PARAMS:",
        "Ljava/lang/Object;",
        "RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/0e6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TPARAMS;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:LX/4cn;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:LX/03R;


# direct methods
.method public constructor <init>(LX/0e6;Ljava/lang/Object;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;)V"
        }
    .end annotation

    .prologue
    .line 418060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418061
    iput-object p1, p0, LX/2Vk;->a:LX/0e6;

    .line 418062
    iput-object p2, p0, LX/2Vk;->b:Ljava/lang/Object;

    .line 418063
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2Vk;->h:LX/03R;

    .line 418064
    return-void
.end method


# virtual methods
.method public final a()LX/2Vj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2Vj",
            "<TPARAMS;TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418065
    new-instance v0, LX/2Vj;

    invoke-direct {v0, p0}, LX/2Vj;-><init>(LX/2Vk;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)LX/2Vk;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/2Vk",
            "<TPARAMS;TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418066
    iput-object p1, p0, LX/2Vk;->c:Ljava/lang/String;

    .line 418067
    return-object p0
.end method

.method public final a(Z)LX/2Vk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/2Vk",
            "<TPARAMS;TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418068
    if-eqz p1, :cond_0

    sget-object v0, LX/03R;->YES:LX/03R;

    :goto_0
    iput-object v0, p0, LX/2Vk;->h:LX/03R;

    .line 418069
    return-object p0

    .line 418070
    :cond_0
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/2Vk;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/2Vk",
            "<TPARAMS;TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418071
    iput-object p1, p0, LX/2Vk;->d:Ljava/lang/String;

    .line 418072
    return-object p0
.end method
