.class public LX/3Qx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final a:LX/3Qy;

.field public final b:LX/13l;

.field public final c:LX/3E1;

.field public final d:LX/3Qw;

.field public final e:LX/2oM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/D6L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/3FQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3Qy;LX/13l;LX/3E1;LX/3Qw;LX/2oM;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D6L;)V
    .locals 0
    .param p4    # LX/3Qw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/2oM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/atomic/AtomicReference;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/3FQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/D6L;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Qy;",
            "LX/13l;",
            "LX/3E1;",
            "LX/3Qw;",
            "LX/2oM;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;",
            "LX/3FQ;",
            "LX/D6L;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 566668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566669
    iput-object p1, p0, LX/3Qx;->a:LX/3Qy;

    .line 566670
    iput-object p2, p0, LX/3Qx;->b:LX/13l;

    .line 566671
    iput-object p3, p0, LX/3Qx;->c:LX/3E1;

    .line 566672
    iput-object p4, p0, LX/3Qx;->d:LX/3Qw;

    .line 566673
    iput-object p5, p0, LX/3Qx;->e:LX/2oM;

    .line 566674
    iput-object p6, p0, LX/3Qx;->f:Ljava/util/concurrent/atomic/AtomicReference;

    .line 566675
    iput-object p7, p0, LX/3Qx;->h:LX/3FQ;

    .line 566676
    iput-object p8, p0, LX/3Qx;->g:LX/D6L;

    .line 566677
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x52b093d3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 566678
    iget-object v0, p0, LX/3Qx;->h:LX/3FQ;

    if-nez v0, :cond_0

    instance-of v0, p1, LX/3FQ;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 566679
    check-cast v0, LX/3FQ;

    iput-object v0, p0, LX/3Qx;->h:LX/3FQ;

    .line 566680
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, LX/0f8;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f8;

    .line 566681
    iget-object v4, p0, LX/3Qx;->e:LX/2oM;

    if-eqz v4, :cond_2

    iget-object v4, p0, LX/3Qx;->e:LX/2oM;

    invoke-interface {v4}, LX/2oM;->b()LX/2oO;

    move-result-object v4

    .line 566682
    :goto_0
    if-eqz v4, :cond_3

    .line 566683
    iget-boolean v5, v4, LX/2oO;->t:Z

    move v5, v5

    .line 566684
    if-eqz v5, :cond_3

    .line 566685
    :goto_1
    iget-object v0, p0, LX/3Qx;->g:LX/D6L;

    if-eqz v0, :cond_1

    .line 566686
    iget-object v0, p0, LX/3Qx;->g:LX/D6L;

    invoke-interface {v0}, LX/D6L;->a()V

    .line 566687
    :cond_1
    const v0, 0x27739529

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 566688
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 566689
    :cond_3
    iget-object v5, p0, LX/3Qx;->a:LX/3Qy;

    iget-object v6, p0, LX/3Qx;->d:LX/3Qw;

    .line 566690
    new-instance v8, LX/D6I;

    invoke-static {v5}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v11

    check-cast v11, LX/0sV;

    invoke-static {v5}, LX/1YQ;->a(LX/0QB;)LX/1YQ;

    move-result-object v12

    check-cast v12, LX/1YQ;

    invoke-static {v5}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v13

    check-cast v13, LX/2xj;

    move-object v9, v0

    move-object v10, v6

    invoke-direct/range {v8 .. v13}, LX/D6I;-><init>(LX/0f8;LX/3Qw;LX/0sV;LX/1YQ;LX/2xj;)V

    .line 566691
    move-object v5, v8

    .line 566692
    iget-object v6, p0, LX/3Qx;->b:LX/13l;

    sget-object v7, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v6, v7}, LX/13l;->a(LX/04g;)V

    .line 566693
    if-eqz v4, :cond_4

    .line 566694
    invoke-virtual {v4}, LX/2oO;->a()V

    .line 566695
    :cond_4
    iget-object v4, p0, LX/3Qx;->c:LX/3E1;

    invoke-virtual {v4}, LX/3E1;->a()V

    .line 566696
    iget-object v4, p0, LX/3Qx;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3J0;

    .line 566697
    instance-of v6, v4, LX/D6J;

    if-nez v6, :cond_5

    .line 566698
    iget-object v6, p0, LX/3Qx;->f:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v7, LX/D6K;

    invoke-direct {v7, p0, v4}, LX/D6K;-><init>(LX/3Qx;LX/3J0;)V

    invoke-virtual {v6, v7}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 566699
    :cond_5
    iget-object v4, p0, LX/3Qx;->h:LX/3FQ;

    iget-object v6, p0, LX/3Qx;->f:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v11, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 566700
    iget-object v7, v5, LX/D6I;->c:LX/2xj;

    invoke-virtual {v7}, LX/2xj;->i()Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, v5, LX/D6I;->c:LX/2xj;

    invoke-virtual {v7}, LX/2xj;->k()Z

    move-result v7

    if-nez v7, :cond_6

    .line 566701
    iget-object v7, v5, LX/D6I;->c:LX/2xj;

    invoke-virtual {v7}, LX/2xj;->d()V

    .line 566702
    :cond_6
    if-eqz v4, :cond_7

    .line 566703
    iget-object v7, v5, LX/D6I;->e:LX/3Qw;

    invoke-static {v7}, LX/3Qv;->a(LX/3Qw;)LX/3Qv;

    move-result-object v7

    invoke-interface {v4}, LX/3FQ;->getLastStartPosition()I

    move-result v10

    .line 566704
    iput v10, v7, LX/3Qv;->f:I

    .line 566705
    move-object v7, v7

    .line 566706
    invoke-interface {v4}, LX/3FQ;->getSeekPosition()I

    move-result v10

    .line 566707
    iput v10, v7, LX/3Qv;->e:I

    .line 566708
    move-object v7, v7

    .line 566709
    invoke-interface {v4}, LX/3FQ;->getProjectionType()LX/19o;

    move-result-object v10

    .line 566710
    iput-object v10, v7, LX/3Qv;->q:LX/19o;

    .line 566711
    move-object v7, v7

    .line 566712
    invoke-interface {v4}, LX/3FQ;->getAudioChannelLayout()LX/03z;

    move-result-object v10

    .line 566713
    iput-object v10, v7, LX/3Qv;->r:LX/03z;

    .line 566714
    move-object v7, v7

    .line 566715
    invoke-virtual {v7}, LX/3Qv;->a()LX/3Qw;

    move-result-object v7

    iput-object v7, v5, LX/D6I;->e:LX/3Qw;

    .line 566716
    :cond_7
    iget-object v7, v5, LX/D6I;->e:LX/3Qw;

    iget-object v7, v7, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v7, :cond_b

    iget-object v7, v5, LX/D6I;->e:LX/3Qw;

    iget-object v7, v7, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 566717
    iget-object v10, v7, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v10

    .line 566718
    if-eqz v7, :cond_b

    move v7, v8

    .line 566719
    :goto_2
    iget-object v10, v5, LX/D6I;->e:LX/3Qw;

    iget-object v10, v10, LX/3Qw;->c:Ljava/util/List;

    if-eqz v10, :cond_c

    iget-object v10, v5, LX/D6I;->e:LX/3Qw;

    iget-object v10, v10, LX/3Qw;->c:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_c

    move v10, v8

    .line 566720
    :goto_3
    if-nez v7, :cond_8

    if-eqz v10, :cond_d

    :cond_8
    move v10, v8

    :goto_4
    const-string v12, "Either storyProps or videoChannelIds must be provided"

    invoke-static {v10, v12}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 566721
    if-eqz v7, :cond_e

    iget-object v7, v5, LX/D6I;->e:LX/3Qw;

    iget-object v7, v7, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v12, 0x0

    .line 566722
    if-nez v7, :cond_11

    move-object v10, v12

    .line 566723
    :goto_5
    move-object v7, v10

    .line 566724
    :goto_6
    iput-object v7, v5, LX/D6I;->k:Ljava/lang/String;

    .line 566725
    iget-object v7, v5, LX/D6I;->a:LX/0f8;

    invoke-interface {v7}, LX/0f8;->l()LX/0hE;

    move-result-object v7

    check-cast v7, Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    .line 566726
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 566727
    new-instance v10, LX/D6H;

    iget-object v12, v5, LX/D6I;->d:LX/1YQ;

    invoke-direct {v10, v6, v12}, LX/D6H;-><init>(Ljava/util/concurrent/atomic/AtomicReference;LX/1YQ;)V

    invoke-virtual {v7, v10}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(LX/394;)Ljava/lang/Object;

    .line 566728
    if-eqz v4, :cond_f

    invoke-interface {v4}, LX/3FQ;->getTransitionNode()LX/3FT;

    move-result-object v10

    .line 566729
    :goto_7
    iget-object v12, v5, LX/D6I;->b:LX/0sV;

    iget-boolean v12, v12, LX/0sV;->g:Z

    if-eqz v12, :cond_10

    if-eqz v10, :cond_10

    invoke-interface {v10}, LX/3FT;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v12

    if-eqz v12, :cond_10

    invoke-interface {v10}, LX/3FT;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v12

    .line 566730
    iget-object v13, v12, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v12, v13

    .line 566731
    if-eqz v12, :cond_10

    .line 566732
    :goto_8
    if-eqz v8, :cond_9

    .line 566733
    iput-object v10, v5, LX/D6I;->f:LX/3FT;

    .line 566734
    invoke-interface {v10}, LX/3FT;->c()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v9

    iput-object v9, v5, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    .line 566735
    iget-object v9, v5, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    .line 566736
    iget-object v10, v9, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v9, v10

    .line 566737
    iput-object v9, v5, LX/D6I;->j:LX/2pa;

    .line 566738
    iget-object v9, v5, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v9}, Lcom/facebook/video/player/RichVideoPlayer;->o()Ljava/util/List;

    move-result-object v9

    iput-object v9, v5, LX/D6I;->h:Ljava/util/List;

    .line 566739
    iget-object v9, v5, LX/D6I;->g:Lcom/facebook/video/player/RichVideoPlayer;

    .line 566740
    iget-object v10, v9, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    move-object v9, v10

    .line 566741
    iput-object v9, v5, LX/D6I;->i:LX/3It;

    .line 566742
    const/4 v9, 0x0

    iput-boolean v9, v5, LX/D6I;->l:Z

    .line 566743
    :cond_9
    iget-object v9, v5, LX/D6I;->e:LX/3Qw;

    invoke-static {v9}, LX/3Qv;->a(LX/3Qw;)LX/3Qv;

    move-result-object v9

    if-eqz v8, :cond_a

    move-object v11, v5

    .line 566744
    :cond_a
    iput-object v11, v9, LX/3Qv;->i:LX/D6I;

    .line 566745
    move-object v8, v9

    .line 566746
    invoke-virtual {v8}, LX/3Qv;->a()LX/3Qw;

    move-result-object v8

    iput-object v8, v5, LX/D6I;->e:LX/3Qw;

    .line 566747
    iget-object v8, v5, LX/D6I;->e:LX/3Qw;

    invoke-virtual {v7, v8}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(LX/3Qw;)V

    .line 566748
    goto/16 :goto_1

    :cond_b
    move v7, v9

    .line 566749
    goto/16 :goto_2

    :cond_c
    move v10, v9

    .line 566750
    goto/16 :goto_3

    :cond_d
    move v10, v9

    .line 566751
    goto/16 :goto_4

    :cond_e
    move-object v7, v11

    .line 566752
    goto :goto_6

    :cond_f
    move-object v10, v11

    .line 566753
    goto :goto_7

    :cond_10
    move v8, v9

    .line 566754
    goto :goto_8

    .line 566755
    :cond_11
    invoke-static {v7}, LX/182;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v10

    .line 566756
    iget-object v13, v10, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v10, v13

    .line 566757
    check-cast v10, Lcom/facebook/graphql/model/GraphQLStory;

    .line 566758
    invoke-static {v10}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v10

    .line 566759
    if-eqz v10, :cond_12

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v13

    if-nez v13, :cond_13

    :cond_12
    move-object v10, v12

    .line 566760
    goto/16 :goto_5

    .line 566761
    :cond_13
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_5
.end method
