.class public LX/2Is;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final f:Ljava/lang/Object;


# instance fields
.field private final b:LX/2J0;

.field private final c:LX/30d;

.field private final d:Landroid/content/ContentResolver;

.field private final e:LX/1Ml;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 392281
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "deleted"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data3"

    aput-object v2, v0, v1

    sput-object v0, LX/2Is;->a:[Ljava/lang/String;

    .line 392282
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2Is;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2J0;LX/30d;Landroid/content/ContentResolver;LX/1Ml;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392284
    iput-object p1, p0, LX/2Is;->b:LX/2J0;

    .line 392285
    iput-object p2, p0, LX/2Is;->c:LX/30d;

    .line 392286
    iput-object p3, p0, LX/2Is;->d:Landroid/content/ContentResolver;

    .line 392287
    iput-object p4, p0, LX/2Is;->e:LX/1Ml;

    .line 392288
    return-void
.end method

.method public static a(LX/0QB;)LX/2Is;
    .locals 10

    .prologue
    .line 392289
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 392290
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 392291
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 392292
    if-nez v1, :cond_0

    .line 392293
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392294
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 392295
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 392296
    sget-object v1, LX/2Is;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 392297
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 392298
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 392299
    :cond_1
    if-nez v1, :cond_4

    .line 392300
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 392301
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 392302
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 392303
    new-instance p0, LX/2Is;

    const-class v1, LX/2J0;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/2J0;

    invoke-static {v0}, LX/30d;->b(LX/0QB;)LX/30d;

    move-result-object v7

    check-cast v7, LX/30d;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v8

    check-cast v8, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v9

    check-cast v9, LX/1Ml;

    invoke-direct {p0, v1, v7, v8, v9}, LX/2Is;-><init>(LX/2J0;LX/30d;Landroid/content/ContentResolver;LX/1Ml;)V

    .line 392304
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 392305
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 392306
    if-nez v1, :cond_2

    .line 392307
    sget-object v0, LX/2Is;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Is;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 392308
    :goto_1
    if-eqz v0, :cond_3

    .line 392309
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 392310
    :goto_3
    check-cast v0, LX/2Is;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 392311
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 392312
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 392313
    :catchall_1
    move-exception v0

    .line 392314
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 392315
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 392316
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 392317
    :cond_2
    :try_start_8
    sget-object v0, LX/2Is;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Is;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a()LX/EjP;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 392318
    iget-object v0, p0, LX/2Is;->e:LX/1Ml;

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v6

    .line 392319
    :goto_0
    return-object v0

    .line 392320
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/2Is;->d:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, LX/2Is;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "contact_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 392321
    :goto_1
    if-nez v1, :cond_1

    .line 392322
    iget-object v0, p0, LX/2Is;->c:LX/30d;

    const-string v1, "contacts_iterator_cursor_null"

    invoke-virtual {v0, v1}, LX/30d;->a(Ljava/lang/String;)V

    move-object v0, v6

    .line 392323
    goto :goto_0

    :catch_0
    move-object v1, v6

    goto :goto_1

    .line 392324
    :cond_1
    new-instance v0, LX/EjP;

    invoke-static {v1}, LX/2J0;->a(Landroid/database/Cursor;)LX/6LS;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EjP;-><init>(Landroid/database/Cursor;)V

    goto :goto_0
.end method
