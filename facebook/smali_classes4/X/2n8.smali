.class public LX/2n8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/2n8;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:LX/2n8;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3m2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 463375
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/2n9;->a:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/2n9;->b:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/2n9;->c:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/2n9;->d:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, LX/2n8;->a:[Ljava/lang/String;

    .line 463376
    const-class v0, LX/2n8;

    sput-object v0, LX/2n8;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3m2;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463378
    iput-object p1, p0, LX/2n8;->b:LX/0Ot;

    .line 463379
    return-void
.end method

.method public static a(LX/0QB;)LX/2n8;
    .locals 4

    .prologue
    .line 463346
    sget-object v0, LX/2n8;->d:LX/2n8;

    if-nez v0, :cond_1

    .line 463347
    const-class v1, LX/2n8;

    monitor-enter v1

    .line 463348
    :try_start_0
    sget-object v0, LX/2n8;->d:LX/2n8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 463349
    if-eqz v2, :cond_0

    .line 463350
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 463351
    new-instance v3, LX/2n8;

    const/16 p0, 0x1fa

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2n8;-><init>(LX/0Ot;)V

    .line 463352
    move-object v0, v3

    .line 463353
    sput-object v0, LX/2n8;->d:LX/2n8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463354
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 463355
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 463356
    :cond_1
    sget-object v0, LX/2n8;->d:LX/2n8;

    return-object v0

    .line 463357
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 463358
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/7i3;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 463359
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 463360
    const-string v1, "browser_prefetch_cache"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 463361
    iget-object v1, p0, LX/2n8;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3m2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/2n8;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 463362
    sget-object v0, LX/2n9;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 463363
    sget-object v2, LX/2n9;->b:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 463364
    sget-object v3, LX/2n9;->c:LX/0U1;

    invoke-virtual {v3, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 463365
    sget-object v4, LX/2n9;->d:LX/0U1;

    invoke-virtual {v4, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 463366
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 463367
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 463368
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 463369
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 463370
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 463371
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 463372
    new-instance v10, LX/7i3;

    invoke-direct {v10, v6, v7, v8, v9}, LX/7i3;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 463373
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 463374
    return-object v5
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 463326
    const/4 v0, 0x1

    new-array v0, v0, [LX/0ux;

    const/4 v1, 0x0

    sget-object v2, LX/2n9;->b:LX/0U1;

    invoke-virtual {v2, p1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v2

    .line 463327
    const/4 v1, 0x0

    .line 463328
    :try_start_0
    iget-object v0, p0, LX/2n8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3m2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 463329
    const v0, 0x16063636

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 463330
    const-string v0, "browser_prefetch_cache"

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 463331
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463332
    const v0, 0x15065f7c

    :try_start_1
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 463333
    :goto_0
    return-void

    .line 463334
    :catch_0
    move-exception v0

    .line 463335
    sget-object v1, LX/2n8;->c:Ljava/lang/Class;

    const-string v2, "Failed to close the connection to the DB!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 463336
    :catch_1
    move-exception v0

    .line 463337
    :try_start_2
    sget-object v2, LX/2n8;->c:Ljava/lang/Class;

    const-string v3, "Delete cache meta info failed!"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 463338
    const v0, -0x524712cf

    :try_start_3
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 463339
    :catch_2
    move-exception v0

    .line 463340
    sget-object v1, LX/2n8;->c:Ljava/lang/Class;

    const-string v2, "Failed to close the connection to the DB!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 463341
    :catchall_0
    move-exception v0

    .line 463342
    const v2, 0xa2cc316

    :try_start_4
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 463343
    :goto_1
    throw v0

    .line 463344
    :catch_3
    move-exception v1

    .line 463345
    sget-object v2, LX/2n8;->c:Ljava/lang/Class;

    const-string v3, "Failed to close the connection to the DB!"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 463302
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "initial_url_key is null"

    invoke-static {v0, v1}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 463303
    iget-object v0, p0, LX/2n8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3m2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 463304
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 463305
    const-string v2, "browser_prefetch_cache"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 463306
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 463307
    sget-object v2, LX/2n9;->a:LX/0U1;

    .line 463308
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 463309
    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 463310
    sget-object v2, LX/2n9;->b:LX/0U1;

    .line 463311
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 463312
    invoke-virtual {v0, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 463313
    sget-object v2, LX/2n9;->c:LX/0U1;

    .line 463314
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 463315
    invoke-virtual {v0, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 463316
    sget-object v2, LX/2n9;->d:LX/0U1;

    .line 463317
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 463318
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 463319
    const v2, -0x793190f0

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 463320
    :try_start_0
    const-string v2, "browser_prefetch_cache"

    const-string v3, ""

    const/4 v4, 0x5

    const v5, 0x3ba6df5a

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v0, 0x7ce1bc4a

    invoke-static {v0}, LX/03h;->a(I)V

    .line 463321
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463322
    const v0, -0x64ebb2c5

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 463323
    return-void

    .line 463324
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 463325
    :catchall_0
    move-exception v0

    const v2, -0x1edc9f1c

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 463282
    const/4 v0, 0x1

    new-array v0, v0, [LX/0ux;

    const/4 v1, 0x0

    sget-object v2, LX/2n9;->a:LX/0U1;

    invoke-virtual {v2, p1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v2

    .line 463283
    const/4 v1, 0x0

    .line 463284
    :try_start_0
    iget-object v0, p0, LX/2n8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3m2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 463285
    const v0, 0x53cb23f6

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 463286
    const-string v0, "browser_prefetch_cache"

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 463287
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463288
    const v0, -0x1365a8b1

    :try_start_1
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 463289
    :goto_0
    return-void

    .line 463290
    :catch_0
    move-exception v0

    .line 463291
    sget-object v1, LX/2n8;->c:Ljava/lang/Class;

    const-string v2, "Failed to close the connection to the DB!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 463292
    :catch_1
    move-exception v0

    .line 463293
    :try_start_2
    sget-object v2, LX/2n8;->c:Ljava/lang/Class;

    const-string v3, "Delete cache meta info failed!"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 463294
    const v0, -0xc9e70cf

    :try_start_3
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 463295
    :catch_2
    move-exception v0

    .line 463296
    sget-object v1, LX/2n8;->c:Ljava/lang/Class;

    const-string v2, "Failed to close the connection to the DB!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 463297
    :catchall_0
    move-exception v0

    .line 463298
    const v2, -0x38fedeb7

    :try_start_4
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 463299
    :goto_1
    throw v0

    .line 463300
    :catch_3
    move-exception v1

    .line 463301
    sget-object v2, LX/2n8;->c:Ljava/lang/Class;

    const-string v3, "Failed to close the connection to the DB!"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
