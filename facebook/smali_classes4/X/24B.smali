.class public LX/24B;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1RH;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0ad;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;LX/0Or;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;",
            "LX/0Or",
            "<",
            "LX/1RH;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 366759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 366760
    iput-object p1, p0, LX/24B;->a:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    .line 366761
    iput-object p2, p0, LX/24B;->b:LX/0Or;

    .line 366762
    iput-object p3, p0, LX/24B;->c:LX/0ad;

    .line 366763
    return-void
.end method

.method public static a(LX/1RN;)LX/1KL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1RN;",
            ")",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/24O;",
            ">;"
        }
    .end annotation

    .prologue
    .line 366758
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/Ak1;

    invoke-direct {v0, p0}, LX/Ak1;-><init>(LX/1RN;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/24B;
    .locals 4

    .prologue
    .line 366764
    new-instance v2, LX/24B;

    invoke-static {p0}, Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    const/16 v1, 0xfd1

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v3, v1}, LX/24B;-><init>(Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;LX/0Or;LX/0ad;)V

    .line 366765
    return-object v2
.end method


# virtual methods
.method public final a(LX/1RN;LX/1Qa;LX/5S9;)V
    .locals 2

    .prologue
    .line 366752
    iget-object v0, p1, LX/1RN;->c:LX/32e;

    iget-object v0, v0, LX/32e;->a:LX/24P;

    sget-object v1, LX/24P;->MAXIMIZED:LX/24P;

    if-ne v0, v1, :cond_0

    .line 366753
    invoke-interface {p2, p1, p3}, LX/1Qa;->b(LX/1RN;LX/5S9;)V

    .line 366754
    iget-object v0, p0, LX/24B;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RH;

    invoke-virtual {v0, p1}, LX/1RI;->c(LX/1RN;)V

    .line 366755
    :goto_0
    return-void

    .line 366756
    :cond_0
    invoke-interface {p2, p1, p3}, LX/1Qa;->a(LX/1RN;LX/5S9;)V

    .line 366757
    iget-object v0, p0, LX/24B;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RH;

    invoke-virtual {v0, p1}, LX/1RI;->b(LX/1RN;)V

    goto :goto_0
.end method

.method public final a(LX/1aD;LX/1RN;LX/0jW;Ljava/lang/Class;LX/24J;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/1RN;",
            "LX/0jW;",
            "Ljava/lang/Class;",
            "LX/24J;",
            ")V"
        }
    .end annotation

    .prologue
    .line 366749
    if-eqz p2, :cond_0

    .line 366750
    iget-object v6, p0, LX/24B;->a:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    new-instance v0, LX/82M;

    iget-object v1, p2, LX/1RN;->c:LX/32e;

    iget-object v1, v1, LX/32e;->a:LX/24P;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Animation_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, LX/1RN;->a:LX/1kK;

    invoke-interface {v3}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v5, LX/24P;->MINIMIZED:LX/24P;

    move-object v3, p3

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, LX/82M;-><init>(Ljava/lang/Object;Ljava/lang/String;LX/0jW;LX/24J;Ljava/lang/Object;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 366751
    :cond_0
    return-void
.end method

.method public final b(LX/1RN;)Z
    .locals 5

    .prologue
    .line 366743
    iget-object v0, p0, LX/24B;->c:LX/0ad;

    sget-wide v2, LX/1RY;->w:D

    const-class v1, LX/24E;

    sget-object v4, LX/24E;->NONE:LX/24E;

    invoke-interface {v0, v2, v3, v1, v4}, LX/0ad;->a(DLjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/24E;

    .line 366744
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v1

    instance-of v1, v1, LX/1kJ;

    .line 366745
    sget-object v2, LX/24F;->a:[I

    invoke-virtual {v0}, LX/24E;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 366746
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    move v0, v1

    .line 366747
    goto :goto_0

    .line 366748
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
