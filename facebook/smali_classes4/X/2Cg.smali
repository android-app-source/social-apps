.class public LX/2Cg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2Cg;


# instance fields
.field public final a:LX/0SG;

.field public final b:LX/0So;

.field public final c:LX/2wl;

.field public final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/0SG;LX/0So;LX/2wl;LX/0ad;)V
    .locals 0
    .param p2    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 383173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383174
    iput-object p1, p0, LX/2Cg;->a:LX/0SG;

    .line 383175
    iput-object p2, p0, LX/2Cg;->b:LX/0So;

    .line 383176
    iput-object p3, p0, LX/2Cg;->c:LX/2wl;

    .line 383177
    iput-object p4, p0, LX/2Cg;->d:LX/0ad;

    .line 383178
    return-void
.end method

.method public static a(LX/0QB;)LX/2Cg;
    .locals 7

    .prologue
    .line 383135
    sget-object v0, LX/2Cg;->e:LX/2Cg;

    if-nez v0, :cond_1

    .line 383136
    const-class v1, LX/2Cg;

    monitor-enter v1

    .line 383137
    :try_start_0
    sget-object v0, LX/2Cg;->e:LX/2Cg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 383138
    if-eqz v2, :cond_0

    .line 383139
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 383140
    new-instance p0, LX/2Cg;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/2wl;->a(LX/0QB;)LX/2wl;

    move-result-object v5

    check-cast v5, LX/2wl;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2Cg;-><init>(LX/0SG;LX/0So;LX/2wl;LX/0ad;)V

    .line 383141
    move-object v0, p0

    .line 383142
    sput-object v0, LX/2Cg;->e:LX/2Cg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 383143
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 383144
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 383145
    :cond_1
    sget-object v0, LX/2Cg;->e:LX/2Cg;

    return-object v0

    .line 383146
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 383147
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 383148
    if-nez p1, :cond_1

    .line 383149
    :cond_0
    return-void

    .line 383150
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    .line 383151
    if-nez v0, :cond_3

    .line 383152
    :cond_2
    :goto_1
    goto :goto_0

    .line 383153
    :cond_3
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_2

    .line 383154
    iget-object v2, p0, LX/2Cg;->c:LX/2wl;

    .line 383155
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x11

    if-ge v8, v9, :cond_5

    .line 383156
    :cond_4
    :goto_2
    iget-object v2, p0, LX/2Cg;->d:LX/0ad;

    sget-short v3, LX/2Ci;->a:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 383157
    if-eqz v2, :cond_2

    .line 383158
    iget-object v2, p0, LX/2Cg;->d:LX/0ad;

    sget v3, LX/2Ci;->b:I

    const v4, 0x493e0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    .line 383159
    iget-object v3, p0, LX/2Cg;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 383160
    iget-wide v6, v0, Landroid/net/wifi/ScanResult;->timestamp:J

    sub-long/2addr v4, v6

    .line 383161
    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    int-to-long v2, v2

    cmp-long v2, v6, v2

    if-gtz v2, :cond_2

    .line 383162
    iget-object v2, p0, LX/2Cg;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iput-wide v2, v0, Landroid/net/wifi/ScanResult;->timestamp:J

    goto :goto_1

    .line 383163
    :cond_5
    iget-object v8, v2, LX/2wl;->c:LX/0Zm;

    const-string v9, "android_wifi_scan_timestamp"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, LX/0Zm;->a(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 383164
    const-string v8, "android_wifi_scan_timestamp"

    .line 383165
    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v9, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v10, "background_location"

    .line 383166
    iput-object v10, v9, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 383167
    move-object v9, v9

    .line 383168
    move-object v8, v9

    .line 383169
    const-string v9, "wifi_scan_timestamp"

    iget-wide v10, v0, Landroid/net/wifi/ScanResult;->timestamp:J

    invoke-virtual {v8, v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 383170
    const-string v9, "wall_clock_time_ms"

    iget-object v10, v2, LX/2wl;->a:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v10

    invoke-virtual {v8, v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 383171
    const-string v9, "since_boot_clock_time_ms"

    iget-object v10, v2, LX/2wl;->d:LX/0So;

    invoke-interface {v10}, LX/0So;->now()J

    move-result-wide v10

    invoke-virtual {v8, v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 383172
    iget-object v9, v2, LX/2wl;->b:LX/0Zb;

    invoke-interface {v9, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_2
.end method
