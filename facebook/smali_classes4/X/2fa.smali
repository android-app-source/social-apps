.class public LX/2fa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/2fa;


# instance fields
.field public final b:LX/11H;

.field public final c:LX/6FS;

.field public final d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 446644
    const-class v0, LX/2fa;

    sput-object v0, LX/2fa;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/11H;LX/6FS;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 446645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 446646
    iput-object p1, p0, LX/2fa;->b:LX/11H;

    .line 446647
    iput-object p2, p0, LX/2fa;->c:LX/6FS;

    .line 446648
    iput-object p3, p0, LX/2fa;->d:LX/03V;

    .line 446649
    return-void
.end method

.method public static a(LX/0QB;)LX/2fa;
    .locals 6

    .prologue
    .line 446650
    sget-object v0, LX/2fa;->e:LX/2fa;

    if-nez v0, :cond_1

    .line 446651
    const-class v1, LX/2fa;

    monitor-enter v1

    .line 446652
    :try_start_0
    sget-object v0, LX/2fa;->e:LX/2fa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 446653
    if-eqz v2, :cond_0

    .line 446654
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 446655
    new-instance p0, LX/2fa;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v3

    check-cast v3, LX/11H;

    .line 446656
    new-instance v4, LX/6FS;

    invoke-direct {v4}, LX/6FS;-><init>()V

    .line 446657
    move-object v4, v4

    .line 446658
    move-object v4, v4

    .line 446659
    check-cast v4, LX/6FS;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/2fa;-><init>(LX/11H;LX/6FS;LX/03V;)V

    .line 446660
    move-object v0, p0

    .line 446661
    sput-object v0, LX/2fa;->e:LX/2fa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446662
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 446663
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 446664
    :cond_1
    sget-object v0, LX/2fa;->e:LX/2fa;

    return-object v0

    .line 446665
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 446666
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
