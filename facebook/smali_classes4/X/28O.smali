.class public final LX/28O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 373923
    iput-object p1, p0, LX/28O;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iput-object p2, p0, LX/28O;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 373924
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 373925
    const-string v1, "accepted"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 373926
    iget-object v1, p0, LX/28O;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v1, v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->L:LX/10O;

    const-string v2, "login_openid_dialog_shown"

    invoke-virtual {v1, v2, v0}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 373927
    iget-object v0, p0, LX/28O;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ao:LX/2NM;

    iget-object v1, p0, LX/28O;->a:Ljava/lang/String;

    .line 373928
    new-instance v2, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;

    invoke-static {v0, v1}, LX/2NM;->e(LX/2NM;Ljava/lang/String;)Lcom/facebook/openidconnect/model/OpenIDCredential;

    move-result-object p1

    sget-object p2, LX/41D;->OPENID_CONNECT_TYPE:LX/41D;

    invoke-direct {v2, v1, p1, p2}, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;-><init>(Ljava/lang/String;Lcom/facebook/openidconnect/model/OpenIDCredential;LX/41D;)V

    move-object v0, v2

    .line 373929
    iget-object v1, p0, LX/28O;->b:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    .line 373930
    invoke-static {v1, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Lcom/facebook/auth/credentials/OpenIDLoginCredentials;)V

    .line 373931
    return-void
.end method
