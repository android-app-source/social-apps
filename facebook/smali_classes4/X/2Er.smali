.class public final LX/2Er;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 386058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386059
    cmp-long v0, p1, v2

    if-gez v0, :cond_0

    .line 386060
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minDelayMs < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 386061
    :cond_0
    cmp-long v0, p3, v2

    if-gez v0, :cond_1

    .line 386062
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxDelayMs < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 386063
    :cond_1
    cmp-long v0, p1, p3

    if-lez v0, :cond_2

    .line 386064
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "minDelay="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; maxDelay="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 386065
    :cond_2
    iput-wide p1, p0, LX/2Er;->a:J

    .line 386066
    iput-wide p3, p0, LX/2Er;->b:J

    .line 386067
    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 386068
    const-string v0, "min_delay_ms"

    invoke-virtual {p1, v0, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    const-string v2, "max_delay_ms"

    invoke-virtual {p1, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, LX/2Er;-><init>(JJ)V

    .line 386069
    return-void
.end method
