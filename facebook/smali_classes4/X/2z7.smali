.class public LX/2z7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2z7;


# instance fields
.field public a:I

.field public final b:LX/3zZ;


# direct methods
.method public constructor <init>(LX/3zZ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 483024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483025
    const/4 v0, -0x1

    iput v0, p0, LX/2z7;->a:I

    .line 483026
    iput-object p1, p0, LX/2z7;->b:LX/3zZ;

    .line 483027
    return-void
.end method

.method public static a(LX/0QB;)LX/2z7;
    .locals 5

    .prologue
    .line 483028
    sget-object v0, LX/2z7;->c:LX/2z7;

    if-nez v0, :cond_1

    .line 483029
    const-class v1, LX/2z7;

    monitor-enter v1

    .line 483030
    :try_start_0
    sget-object v0, LX/2z7;->c:LX/2z7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 483031
    if-eqz v2, :cond_0

    .line 483032
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 483033
    new-instance v4, LX/2z7;

    .line 483034
    new-instance p0, LX/3zZ;

    invoke-static {v0}, LX/3zS;->a(LX/0QB;)LX/3zS;

    move-result-object v3

    check-cast v3, LX/3zS;

    invoke-direct {p0, v3}, LX/3zZ;-><init>(LX/3zS;)V

    .line 483035
    move-object v3, p0

    .line 483036
    check-cast v3, LX/3zZ;

    invoke-direct {v4, v3}, LX/2z7;-><init>(LX/3zZ;)V

    .line 483037
    move-object v0, v4

    .line 483038
    sput-object v0, LX/2z7;->c:LX/2z7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483039
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 483040
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 483041
    :cond_1
    sget-object v0, LX/2z7;->c:LX/2z7;

    return-object v0

    .line 483042
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 483043
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
