.class public LX/2Za;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Zb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2Zb",
        "<",
        "Ljava/lang/Object;",
        "LX/0cG",
        "<",
        "Lcom/facebook/katana/urimap/api/UriHandler;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:LX/2ZQ;

.field public final b:LX/2ZX;

.field public final c:LX/11H;

.field private final d:LX/0TD;


# direct methods
.method public constructor <init>(LX/2ZQ;LX/2ZX;LX/11H;LX/0TD;)V
    .locals 0
    .param p2    # LX/2ZX;
        .annotation runtime Lcom/facebook/katana/urimap/fetchable/FacewebUriTemplateMapParser;
        .end annotation
    .end param
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 422582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422583
    iput-object p1, p0, LX/2Za;->a:LX/2ZQ;

    .line 422584
    iput-object p2, p0, LX/2Za;->b:LX/2ZX;

    .line 422585
    iput-object p3, p0, LX/2Za;->c:LX/11H;

    .line 422586
    iput-object p4, p0, LX/2Za;->d:LX/0TD;

    .line 422587
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 422581
    invoke-virtual {p0, p1}, LX/2Za;->b(Ljava/lang/String;)LX/0cG;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 422580
    const-class v0, LX/2ZY;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 422572
    const-string v0, "urimap"

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;LX/2Zg;)V
    .locals 3
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Object;",
            "Lcom/facebook/manageddatastore/NetworkRequestCallback",
            "<",
            "Ljava/lang/Object;",
            "LX/0cG",
            "<",
            "Lcom/facebook/katana/urimap/api/UriHandler;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 422578
    iget-object v0, p0, LX/2Za;->d:LX/0TD;

    new-instance v1, Lcom/facebook/katana/urimap/fetchable/FacewebUriMapClient$1;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/facebook/katana/urimap/fetchable/FacewebUriMapClient$1;-><init>(LX/2Za;LX/2Zg;Landroid/content/Context;Ljava/lang/Object;)V

    const v2, 0x19c32ff3

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 422579
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 422577
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 422576
    const/16 v0, 0xe10

    return v0
.end method

.method public final b(Ljava/lang/String;)LX/0cG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0cG",
            "<",
            "Lcom/facebook/katana/urimap/api/UriHandler;",
            ">;"
        }
    .end annotation

    .prologue
    .line 422574
    iget-object v0, p0, LX/2Za;->b:LX/2ZX;

    move-object v0, v0

    .line 422575
    invoke-virtual {v0, p1}, LX/2ZX;->a(Ljava/lang/String;)LX/0cG;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 422573
    const/16 v0, 0xe10

    return v0
.end method
