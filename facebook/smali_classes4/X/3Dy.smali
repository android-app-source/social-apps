.class public final LX/3Dy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/3Dw;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/3Dw;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 536651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 536652
    iput-object p1, p0, LX/3Dy;->a:LX/0QB;

    .line 536653
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/3Dw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 536649
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/3Dy;

    invoke-direct {v2, p0}, LX/3Dy;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 536650
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/3Dy;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 536607
    packed-switch p2, :pswitch_data_0

    .line 536608
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 536609
    :pswitch_0
    invoke-static {p1}, LX/3Dz;->a(LX/0QB;)LX/3Dz;

    move-result-object v0

    .line 536610
    :goto_0
    return-object v0

    .line 536611
    :pswitch_1
    invoke-static {p1}, LX/3E0;->a(LX/0QB;)LX/3E0;

    move-result-object v0

    goto :goto_0

    .line 536612
    :pswitch_2
    invoke-static {p1}, LX/3Ev;->a(LX/0QB;)LX/3Ev;

    move-result-object v0

    goto :goto_0

    .line 536613
    :pswitch_3
    invoke-static {p1}, LX/3Ew;->a(LX/0QB;)LX/3Ew;

    move-result-object v0

    goto :goto_0

    .line 536614
    :pswitch_4
    invoke-static {p1}, LX/3Ex;->a(LX/0QB;)LX/3Ex;

    move-result-object v0

    goto :goto_0

    .line 536615
    :pswitch_5
    invoke-static {p1}, LX/3Ey;->a(LX/0QB;)LX/3Ey;

    move-result-object v0

    goto :goto_0

    .line 536616
    :pswitch_6
    invoke-static {p1}, LX/3Ez;->a(LX/0QB;)LX/3Ez;

    move-result-object v0

    goto :goto_0

    .line 536617
    :pswitch_7
    invoke-static {p1}, LX/3F0;->a(LX/0QB;)LX/3F0;

    move-result-object v0

    goto :goto_0

    .line 536618
    :pswitch_8
    invoke-static {p1}, LX/3F1;->a(LX/0QB;)LX/3F1;

    move-result-object v0

    goto :goto_0

    .line 536619
    :pswitch_9
    invoke-static {p1}, LX/3F2;->a(LX/0QB;)LX/3F2;

    move-result-object v0

    goto :goto_0

    .line 536620
    :pswitch_a
    invoke-static {p1}, LX/3F3;->a(LX/0QB;)LX/3F3;

    move-result-object v0

    goto :goto_0

    .line 536621
    :pswitch_b
    invoke-static {p1}, LX/3F4;->a(LX/0QB;)LX/3F4;

    move-result-object v0

    goto :goto_0

    .line 536622
    :pswitch_c
    invoke-static {p1}, LX/3F5;->a(LX/0QB;)LX/3F5;

    move-result-object v0

    goto :goto_0

    .line 536623
    :pswitch_d
    invoke-static {p1}, LX/3F6;->a(LX/0QB;)LX/3F6;

    move-result-object v0

    goto :goto_0

    .line 536624
    :pswitch_e
    invoke-static {p1}, LX/3F7;->a(LX/0QB;)LX/3F7;

    move-result-object v0

    goto :goto_0

    .line 536625
    :pswitch_f
    invoke-static {p1}, LX/3F8;->a(LX/0QB;)LX/3F8;

    move-result-object v0

    goto :goto_0

    .line 536626
    :pswitch_10
    invoke-static {p1}, LX/3F9;->a(LX/0QB;)LX/3F9;

    move-result-object v0

    goto :goto_0

    .line 536627
    :pswitch_11
    invoke-static {p1}, LX/3GQ;->a(LX/0QB;)LX/3GQ;

    move-result-object v0

    goto :goto_0

    .line 536628
    :pswitch_12
    invoke-static {p1}, LX/3Eq;->a(LX/0QB;)LX/3Eq;

    move-result-object v0

    goto :goto_0

    .line 536629
    :pswitch_13
    invoke-static {p1}, LX/3Er;->a(LX/0QB;)LX/3Er;

    move-result-object v0

    goto :goto_0

    .line 536630
    :pswitch_14
    invoke-static {p1}, LX/3EL;->a(LX/0QB;)LX/3EL;

    move-result-object v0

    goto :goto_0

    .line 536631
    :pswitch_15
    invoke-static {p1}, LX/3EM;->a(LX/0QB;)LX/3EM;

    move-result-object v0

    goto :goto_0

    .line 536632
    :pswitch_16
    invoke-static {p1}, LX/3EN;->a(LX/0QB;)LX/3EN;

    move-result-object v0

    goto :goto_0

    .line 536633
    :pswitch_17
    invoke-static {p1}, LX/3EO;->a(LX/0QB;)LX/3EO;

    move-result-object v0

    goto :goto_0

    .line 536634
    :pswitch_18
    invoke-static {p1}, LX/3EP;->a(LX/0QB;)LX/3EP;

    move-result-object v0

    goto :goto_0

    .line 536635
    :pswitch_19
    invoke-static {p1}, LX/3EQ;->a(LX/0QB;)LX/3EQ;

    move-result-object v0

    goto :goto_0

    .line 536636
    :pswitch_1a
    invoke-static {p1}, LX/3At;->a(LX/0QB;)LX/3At;

    move-result-object v0

    goto/16 :goto_0

    .line 536637
    :pswitch_1b
    invoke-static {p1}, LX/3Au;->a(LX/0QB;)LX/3Au;

    move-result-object v0

    goto/16 :goto_0

    .line 536638
    :pswitch_1c
    invoke-static {p1}, LX/3Av;->a(LX/0QB;)LX/3Av;

    move-result-object v0

    goto/16 :goto_0

    .line 536639
    :pswitch_1d
    invoke-static {p1}, LX/3ER;->a(LX/0QB;)LX/3ER;

    move-result-object v0

    goto/16 :goto_0

    .line 536640
    :pswitch_1e
    invoke-static {p1}, LX/3ES;->a(LX/0QB;)LX/3ES;

    move-result-object v0

    goto/16 :goto_0

    .line 536641
    :pswitch_1f
    invoke-static {p1}, LX/3ET;->a(LX/0QB;)LX/3ET;

    move-result-object v0

    goto/16 :goto_0

    .line 536642
    :pswitch_20
    invoke-static {p1}, LX/3EU;->a(LX/0QB;)LX/3EU;

    move-result-object v0

    goto/16 :goto_0

    .line 536643
    :pswitch_21
    invoke-static {p1}, LX/3EV;->a(LX/0QB;)LX/3EV;

    move-result-object v0

    goto/16 :goto_0

    .line 536644
    :pswitch_22
    invoke-static {p1}, LX/3EW;->a(LX/0QB;)LX/3EW;

    move-result-object v0

    goto/16 :goto_0

    .line 536645
    :pswitch_23
    invoke-static {p1}, LX/3EX;->a(LX/0QB;)LX/3EX;

    move-result-object v0

    goto/16 :goto_0

    .line 536646
    :pswitch_24
    invoke-static {p1}, LX/3EY;->a(LX/0QB;)LX/3EY;

    move-result-object v0

    goto/16 :goto_0

    .line 536647
    :pswitch_25
    invoke-static {p1}, LX/3EZ;->a(LX/0QB;)LX/3EZ;

    move-result-object v0

    goto/16 :goto_0

    .line 536648
    :pswitch_26
    invoke-static {p1}, LX/3Ea;->a(LX/0QB;)LX/3Ea;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 536606
    const/16 v0, 0x27

    return v0
.end method
