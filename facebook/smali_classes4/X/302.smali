.class public LX/302;
.super LX/303;
.source ""

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/303",
        "<TE;>;",
        "Ljava/util/Set",
        "<TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/0Rl;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<TE;>;",
            "LX/0Rl",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 484016
    invoke-direct {p0, p1, p2}, LX/303;-><init>(Ljava/util/Collection;LX/0Rl;)V

    .line 484017
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 484014
    invoke-static {p0, p1}, LX/0RA;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 484015
    invoke-static {p0}, LX/0RA;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method
