.class public LX/35C;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/216;


# direct methods
.method public constructor <init>(LX/216;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 496844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 496845
    iput-object p1, p0, LX/35C;->a:LX/216;

    .line 496846
    return-void
.end method

.method public static a(LX/0QB;)LX/35C;
    .locals 4

    .prologue
    .line 496847
    const-class v1, LX/35C;

    monitor-enter v1

    .line 496848
    :try_start_0
    sget-object v0, LX/35C;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 496849
    sput-object v2, LX/35C;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 496850
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496851
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 496852
    new-instance p0, LX/35C;

    invoke-static {v0}, LX/216;->a(LX/0QB;)LX/216;

    move-result-object v3

    check-cast v3, LX/216;

    invoke-direct {p0, v3}, LX/35C;-><init>(LX/216;)V

    .line 496853
    move-object v0, p0

    .line 496854
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 496855
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/35C;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496856
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 496857
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1De;IILX/1no;LX/1dc;ILX/1np;LX/1np;)V
    .locals 11
    .param p4    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1no;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;I",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 496858
    if-nez p4, :cond_0

    .line 496859
    const/4 v2, 0x0

    iput v2, p3, LX/1no;->a:I

    .line 496860
    const/4 v2, 0x0

    iput v2, p3, LX/1no;->b:I

    .line 496861
    :goto_0
    return-void

    .line 496862
    :cond_0
    invoke-static {p0, p4}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Landroid/graphics/drawable/Drawable;

    .line 496863
    :try_start_0
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    .line 496864
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v10

    .line 496865
    if-lez v9, :cond_1

    if-gtz v10, :cond_2

    .line 496866
    :cond_1
    const/4 v2, 0x0

    iput v2, p3, LX/1no;->a:I

    .line 496867
    const/4 v2, 0x0

    iput v2, p3, LX/1no;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 496868
    invoke-static {p0, v8, p4}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    goto :goto_0

    .line 496869
    :cond_2
    int-to-float v2, v9

    const/high16 v3, 0x3fc00000    # 1.5f

    mul-float/2addr v2, v3

    float-to-double v2, v2

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v4, v2

    .line 496870
    int-to-float v2, v10

    const/high16 v3, 0x3fc00000    # 1.5f

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v5, v2

    .line 496871
    const/4 v2, 0x0

    mul-int/lit8 v3, p5, 0x2

    sub-int v6, v4, v9

    sub-int/2addr v3, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, LX/1np;->a(Ljava/lang/Object;)V

    .line 496872
    const/4 v2, 0x0

    mul-int/lit8 v3, p5, 0x2

    sub-int v6, v5, v10

    sub-int/2addr v3, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, LX/1np;->a(Ljava/lang/Object;)V

    .line 496873
    invoke-static {p1}, LX/1mh;->a(I)I

    move-result v2

    if-nez v2, :cond_3

    invoke-static {p2}, LX/1mh;->a(I)I

    move-result v2

    if-nez v2, :cond_3

    .line 496874
    invoke-virtual/range {p6 .. p6}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, v4

    iput v2, p3, LX/1no;->a:I

    .line 496875
    invoke-virtual/range {p7 .. p7}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, v5

    iput v2, p3, LX/1no;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496876
    invoke-static {p0, v8, p4}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    goto/16 :goto_0

    .line 496877
    :cond_3
    int-to-float v2, v9

    int-to-float v3, v10

    div-float v6, v2, v3

    move v2, p1

    move v3, p2

    move-object v7, p3

    :try_start_2
    invoke-static/range {v2 .. v7}, LX/1oC;->a(IIIIFLX/1no;)V

    .line 496878
    const/4 v2, 0x0

    mul-int/lit8 v3, p5, 0x2

    iget v4, p3, LX/1no;->a:I

    sub-int/2addr v4, v9

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, LX/1np;->a(Ljava/lang/Object;)V

    .line 496879
    const/4 v2, 0x0

    mul-int/lit8 v3, p5, 0x2

    iget v4, p3, LX/1no;->b:I

    sub-int/2addr v4, v10

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, LX/1np;->a(Ljava/lang/Object;)V

    .line 496880
    invoke-static {p1}, LX/1mh;->a(I)I

    move-result v2

    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_6

    .line 496881
    iget v3, p3, LX/1no;->a:I

    invoke-virtual/range {p6 .. p6}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {p1}, LX/1mh;->b(I)I

    move-result v4

    iget v5, p3, LX/1no;->a:I

    sub-int/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int/2addr v2, v3

    iput v2, p3, LX/1no;->a:I

    .line 496882
    :cond_4
    :goto_1
    invoke-static {p2}, LX/1mh;->a(I)I

    move-result v2

    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_7

    .line 496883
    iget v3, p3, LX/1no;->b:I

    invoke-virtual/range {p7 .. p7}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {p2}, LX/1mh;->b(I)I

    move-result v4

    iget v5, p3, LX/1no;->b:I

    sub-int/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int/2addr v2, v3

    iput v2, p3, LX/1no;->b:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 496884
    :cond_5
    :goto_2
    invoke-static {p0, v8, p4}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    goto/16 :goto_0

    .line 496885
    :cond_6
    :try_start_3
    invoke-static {p1}, LX/1mh;->a(I)I

    move-result v2

    if-nez v2, :cond_4

    .line 496886
    iget v3, p3, LX/1no;->a:I

    invoke-virtual/range {p6 .. p6}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, v3

    iput v2, p3, LX/1no;->a:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 496887
    :catchall_0
    move-exception v2

    invoke-static {p0, v8, p4}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    throw v2

    .line 496888
    :cond_7
    :try_start_4
    invoke-static {p2}, LX/1mh;->a(I)I

    move-result v2

    if-nez v2, :cond_5

    .line 496889
    iget v3, p3, LX/1no;->b:I

    invoke-virtual/range {p7 .. p7}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, v3

    iput v2, p3, LX/1no;->b:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method
