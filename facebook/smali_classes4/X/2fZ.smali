.class public LX/2fZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 446617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1Ok;LX/1P5;Landroid/view/View;Landroid/view/View;LX/1OR;Z)I
    .locals 2

    .prologue
    .line 446618
    invoke-virtual {p4}, LX/1OR;->v()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1Ok;->e()I

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 446619
    :cond_0
    const/4 v0, 0x0

    .line 446620
    :goto_0
    return v0

    .line 446621
    :cond_1
    if-nez p5, :cond_2

    .line 446622
    invoke-static {p2}, LX/1OR;->d(Landroid/view/View;)I

    move-result v0

    invoke-static {p3}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 446623
    :cond_2
    invoke-virtual {p1, p3}, LX/1P5;->b(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p1, p2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 446624
    invoke-virtual {p1}, LX/1P5;->f()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/1Ok;LX/1P5;Landroid/view/View;Landroid/view/View;LX/1OR;ZZ)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 446625
    invoke-virtual {p4}, LX/1OR;->v()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/1Ok;->e()I

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 446626
    :cond_0
    :goto_0
    return v0

    .line 446627
    :cond_1
    invoke-static {p2}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    invoke-static {p3}, LX/1OR;->d(Landroid/view/View;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 446628
    invoke-static {p2}, LX/1OR;->d(Landroid/view/View;)I

    move-result v2

    invoke-static {p3}, LX/1OR;->d(Landroid/view/View;)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 446629
    if-eqz p6, :cond_2

    invoke-virtual {p0}, LX/1Ok;->e()I

    move-result v1

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 446630
    :goto_1
    if-eqz p5, :cond_0

    .line 446631
    invoke-virtual {p1, p3}, LX/1P5;->b(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p1, p2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 446632
    invoke-static {p2}, LX/1OR;->d(Landroid/view/View;)I

    move-result v2

    invoke-static {p3}, LX/1OR;->d(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 446633
    int-to-float v1, v1

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 446634
    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-virtual {p1}, LX/1P5;->c()I

    move-result v1

    invoke-virtual {p1, p2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 446635
    :cond_2
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1
.end method

.method public static b(LX/1Ok;LX/1P5;Landroid/view/View;Landroid/view/View;LX/1OR;Z)I
    .locals 3

    .prologue
    .line 446636
    invoke-virtual {p4}, LX/1OR;->v()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1Ok;->e()I

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 446637
    :cond_0
    const/4 v0, 0x0

    .line 446638
    :goto_0
    return v0

    .line 446639
    :cond_1
    if-nez p5, :cond_2

    .line 446640
    invoke-virtual {p0}, LX/1Ok;->e()I

    move-result v0

    goto :goto_0

    .line 446641
    :cond_2
    invoke-virtual {p1, p3}, LX/1P5;->b(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p1, p2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 446642
    invoke-static {p2}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    invoke-static {p3}, LX/1OR;->d(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 446643
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-virtual {p0}, LX/1Ok;->e()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method
