.class public LX/2da;
.super LX/1cr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/0wO;",
        ">",
        "LX/1cr;"
    }
.end annotation


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4od;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field private d:Landroid/text/Spanned;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 443414
    invoke-direct {p0, p1}, LX/1cr;-><init>(Landroid/view/View;)V

    .line 443415
    iput-object p1, p0, LX/2da;->c:Landroid/view/View;

    .line 443416
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/2da;->b:Ljava/util/List;

    .line 443417
    return-void
.end method

.method private a(LX/4od;)Landroid/graphics/Rect;
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 443446
    iget-boolean v0, p1, LX/4od;->d:Z

    if-eqz v0, :cond_0

    .line 443447
    new-instance v0, Landroid/graphics/Rect;

    iget-object v2, p0, LX/2da;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iget-object v3, p0, LX/2da;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-direct {v0, v1, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 443448
    :goto_0
    return-object v0

    .line 443449
    :cond_0
    iget-object v0, p0, LX/2da;->c:Landroid/view/View;

    check-cast v0, LX/0wO;

    invoke-interface {v0}, LX/0wO;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 443450
    if-nez v2, :cond_1

    .line 443451
    new-instance v0, Landroid/graphics/Rect;

    iget-object v2, p0, LX/2da;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iget-object v3, p0, LX/2da;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-direct {v0, v1, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0

    .line 443452
    :cond_1
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 443453
    iget v0, p1, LX/4od;->b:I

    int-to-double v4, v0

    .line 443454
    iget v0, p1, LX/4od;->c:I

    int-to-double v6, v0

    .line 443455
    double-to-int v0, v4

    invoke-virtual {v2, v0}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v0

    float-to-double v8, v0

    .line 443456
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    .line 443457
    iget-object v0, p0, LX/2da;->c:Landroid/view/View;

    check-cast v0, LX/0wO;

    invoke-interface {v0}, LX/0wO;->getTextSize()F

    move-result v0

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 443458
    iget-object v0, p1, LX/4od;->a:Ljava/lang/String;

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-double v10, v0

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v10, v10

    .line 443459
    double-to-int v0, v4

    invoke-virtual {v2, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v4

    .line 443460
    double-to-int v0, v6

    invoke-virtual {v2, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v0

    .line 443461
    if-eq v4, v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 443462
    :cond_2
    invoke-virtual {v2, v4, v3}, Landroid/text/Layout;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 443463
    iget-object v0, p0, LX/2da;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v2

    iget-object v0, p0, LX/2da;->c:Landroid/view/View;

    check-cast v0, LX/0wO;

    invoke-interface {v0}, LX/0wO;->getTotalPaddingTop()I

    move-result v0

    add-int/2addr v0, v2

    .line 443464
    iget v2, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v0

    iput v2, v3, Landroid/graphics/Rect;->top:I

    .line 443465
    iget v2, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    iput v0, v3, Landroid/graphics/Rect;->bottom:I

    .line 443466
    iget v0, v3, Landroid/graphics/Rect;->left:I

    int-to-double v4, v0

    iget-object v0, p0, LX/2da;->c:Landroid/view/View;

    check-cast v0, LX/0wO;

    invoke-interface {v0}, LX/0wO;->getTotalPaddingLeft()I

    move-result v0

    int-to-double v6, v0

    add-double/2addr v6, v8

    iget-object v0, p0, LX/2da;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v0

    int-to-double v8, v0

    sub-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v0, v4

    iput v0, v3, Landroid/graphics/Rect;->left:I

    .line 443467
    if-eqz v1, :cond_3

    .line 443468
    new-instance v0, Landroid/graphics/Rect;

    iget v1, v3, Landroid/graphics/Rect;->left:I

    iget v2, v3, Landroid/graphics/Rect;->top:I

    iget v4, v3, Landroid/graphics/Rect;->right:I

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v0, v1, v2, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto/16 :goto_0

    .line 443469
    :cond_3
    new-instance v0, Landroid/graphics/Rect;

    iget v1, v3, Landroid/graphics/Rect;->left:I

    iget v2, v3, Landroid/graphics/Rect;->top:I

    iget v4, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v10

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v0, v1, v2, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto/16 :goto_0
.end method

.method private d()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 443418
    iget-object v0, p0, LX/2da;->c:Landroid/view/View;

    check-cast v0, LX/0wO;

    invoke-interface {v0}, LX/0wO;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, LX/2da;->d:Landroid/text/Spanned;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/2da;->c:Landroid/view/View;

    check-cast v0, LX/0wO;

    invoke-interface {v0}, LX/0wO;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    .line 443419
    iget-object v0, p0, LX/2da;->c:Landroid/view/View;

    check-cast v0, LX/0wO;

    invoke-interface {v0}, LX/0wO;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    iput-object v0, p0, LX/2da;->d:Landroid/text/Spanned;

    .line 443420
    iget-object v0, p0, LX/2da;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 443421
    new-instance v0, LX/4od;

    invoke-direct {v0}, LX/4od;-><init>()V

    .line 443422
    iget-object v1, p0, LX/2da;->d:Landroid/text/Spanned;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4od;->a:Ljava/lang/String;

    .line 443423
    iput v3, v0, LX/4od;->b:I

    .line 443424
    iget-object v1, p0, LX/2da;->d:Landroid/text/Spanned;

    invoke-interface {v1}, Landroid/text/Spanned;->length()I

    move-result v1

    iput v1, v0, LX/4od;->c:I

    .line 443425
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/4od;->d:Z

    .line 443426
    iget-object v1, p0, LX/2da;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 443427
    iget-object v0, p0, LX/2da;->d:Landroid/text/Spanned;

    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v0

    invoke-virtual {p0, v3, v0}, LX/2da;->c(II)[Landroid/text/style/ClickableSpan;

    move-result-object v4

    .line 443428
    if-nez v4, :cond_1

    .line 443429
    :cond_0
    return-void

    .line 443430
    :cond_1
    array-length v5, v4

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v1, v4, v2

    .line 443431
    iget-object v0, p0, LX/2da;->d:Landroid/text/Spanned;

    invoke-interface {v0, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    .line 443432
    iget-object v0, p0, LX/2da;->d:Landroid/text/Spanned;

    invoke-interface {v0, v1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    .line 443433
    new-instance v8, LX/4od;

    invoke-direct {v8}, LX/4od;-><init>()V

    .line 443434
    instance-of v0, v1, LX/4oc;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, LX/4oc;

    .line 443435
    iget-object v9, v0, LX/4oc;->a:Ljava/lang/String;

    move-object v0, v9

    .line 443436
    if-eqz v0, :cond_2

    .line 443437
    check-cast v1, LX/4oc;

    .line 443438
    iget-object v0, v1, LX/4oc;->a:Ljava/lang/String;

    move-object v0, v0

    .line 443439
    iput-object v0, v8, LX/4od;->a:Ljava/lang/String;

    .line 443440
    :goto_1
    iput v6, v8, LX/4od;->b:I

    .line 443441
    iput v7, v8, LX/4od;->c:I

    .line 443442
    iput-boolean v3, v8, LX/4od;->d:Z

    .line 443443
    iget-object v0, p0, LX/2da;->b:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 443444
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 443445
    :cond_2
    iget-object v0, p0, LX/2da;->d:Landroid/text/Spanned;

    invoke-interface {v0, v6, v7}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, LX/4od;->a:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public final a(FF)I
    .locals 7

    .prologue
    .line 443345
    float-to-int v0, p1

    float-to-int v1, p2

    const/4 v3, 0x0

    .line 443346
    iget-object v2, p0, LX/2da;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/2da;->c:Landroid/view/View;

    check-cast v2, LX/0wO;

    invoke-interface {v2}, LX/0wO;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    instance-of v2, v2, Landroid/text/Spanned;

    if-nez v2, :cond_2

    .line 443347
    :cond_0
    const/4 v2, -0x1

    .line 443348
    :goto_0
    move v0, v2

    .line 443349
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 443350
    const/high16 v0, -0x80000000

    .line 443351
    :cond_1
    return v0

    .line 443352
    :cond_2
    iget-object v2, p0, LX/2da;->c:Landroid/view/View;

    check-cast v2, LX/0wO;

    invoke-interface {v2}, LX/0wO;->getTotalPaddingLeft()I

    move-result v2

    sub-int v4, v0, v2

    .line 443353
    iget-object v2, p0, LX/2da;->c:Landroid/view/View;

    check-cast v2, LX/0wO;

    invoke-interface {v2}, LX/0wO;->getTotalPaddingTop()I

    move-result v2

    sub-int v2, v1, v2

    .line 443354
    iget-object v5, p0, LX/2da;->c:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getScrollX()I

    move-result v5

    add-int/2addr v4, v5

    .line 443355
    iget-object v5, p0, LX/2da;->c:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getScrollY()I

    move-result v5

    add-int/2addr v5, v2

    .line 443356
    iget-object v2, p0, LX/2da;->c:Landroid/view/View;

    check-cast v2, LX/0wO;

    invoke-interface {v2}, LX/0wO;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 443357
    if-nez v2, :cond_3

    move v2, v3

    .line 443358
    goto :goto_0

    .line 443359
    :cond_3
    invoke-virtual {v2, v5}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v5

    .line 443360
    int-to-float v4, v4

    invoke-virtual {v2, v5, v4}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v2

    .line 443361
    invoke-virtual {p0, v2, v2}, LX/2da;->c(II)[Landroid/text/style/ClickableSpan;

    move-result-object v4

    .line 443362
    if-eqz v4, :cond_4

    array-length v2, v4

    if-nez v2, :cond_5

    :cond_4
    move v2, v3

    .line 443363
    goto :goto_0

    .line 443364
    :cond_5
    iget-object v2, p0, LX/2da;->c:Landroid/view/View;

    check-cast v2, LX/0wO;

    invoke-interface {v2}, LX/0wO;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Landroid/text/Spanned;

    .line 443365
    aget-object v5, v4, v3

    invoke-interface {v2, v5}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 443366
    aget-object v4, v4, v3

    invoke-interface {v2, v4}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 443367
    iget-object v2, p0, LX/2da;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p1

    move v4, v3

    .line 443368
    :goto_1
    if-ge v4, p1, :cond_7

    .line 443369
    iget-object v2, p0, LX/2da;->b:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4od;

    .line 443370
    iget p2, v2, LX/4od;->b:I

    if-ne p2, v5, :cond_6

    iget v2, v2, LX/4od;->c:I

    if-ne v2, v6, :cond_6

    move v2, v4

    .line 443371
    goto :goto_0

    .line 443372
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_7
    move v2, v3

    .line 443373
    goto :goto_0
.end method

.method public final a(ILX/3sp;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 443400
    invoke-virtual {p0, p1}, LX/2da;->b(I)LX/4od;

    move-result-object v0

    .line 443401
    if-nez v0, :cond_0

    .line 443402
    const-string v0, ""

    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 443403
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, v4, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p2, v0}, LX/3sp;->b(Landroid/graphics/Rect;)V

    .line 443404
    :goto_0
    return-void

    .line 443405
    :cond_0
    invoke-direct {p0, v0}, LX/2da;->a(LX/4od;)Landroid/graphics/Rect;

    move-result-object v1

    .line 443406
    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 443407
    invoke-virtual {v1, v3, v3, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 443408
    :cond_1
    iget-object v0, v0, LX/4od;->a:Ljava/lang/String;

    .line 443409
    if-nez v0, :cond_2

    .line 443410
    const-string v0, ""

    .line 443411
    :cond_2
    invoke-virtual {p2, v0}, LX/3sp;->c(Ljava/lang/CharSequence;)V

    .line 443412
    const/16 v0, 0x10

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 443413
    invoke-virtual {p2, v1}, LX/3sp;->b(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public final a(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 443470
    invoke-virtual {p0, p1}, LX/2da;->b(I)LX/4od;

    move-result-object v0

    .line 443471
    if-nez v0, :cond_0

    .line 443472
    const-string v0, ""

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 443473
    :goto_0
    return-void

    .line 443474
    :cond_0
    iget-object v0, v0, LX/4od;->a:Ljava/lang/String;

    .line 443475
    if-nez v0, :cond_1

    .line 443476
    const-string v0, ""

    .line 443477
    :cond_1
    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 443394
    invoke-direct {p0}, LX/2da;->d()V

    .line 443395
    iget-object v0, p0, LX/2da;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 443396
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 443397
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 443398
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 443399
    :cond_0
    return-void
.end method

.method public final b(I)LX/4od;
    .locals 1

    .prologue
    .line 443391
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/2da;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 443392
    :cond_0
    const/4 v0, 0x0

    .line 443393
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/2da;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4od;

    goto :goto_0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 443381
    packed-switch p2, :pswitch_data_0

    .line 443382
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 443383
    :pswitch_0
    const/4 v0, 0x1

    .line 443384
    invoke-virtual {p0, p1}, LX/2da;->b(I)LX/4od;

    move-result-object p2

    .line 443385
    if-nez p2, :cond_0

    .line 443386
    const/4 v0, 0x0

    .line 443387
    :goto_1
    move v0, v0

    .line 443388
    goto :goto_0

    .line 443389
    :cond_0
    invoke-virtual {p0, p1}, LX/1cr;->a(I)V

    .line 443390
    invoke-virtual {p0, p1, v0}, LX/1cr;->a(II)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
    .end packed-switch
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/4od;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443379
    invoke-direct {p0}, LX/2da;->d()V

    .line 443380
    iget-object v0, p0, LX/2da;->b:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public c(II)[Landroid/text/style/ClickableSpan;
    .locals 2

    .prologue
    .line 443374
    iget-object v0, p0, LX/2da;->c:Landroid/view/View;

    check-cast v0, LX/0wO;

    invoke-interface {v0}, LX/0wO;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-nez v0, :cond_0

    .line 443375
    const/4 v0, 0x0

    .line 443376
    :goto_0
    return-object v0

    .line 443377
    :cond_0
    iget-object v0, p0, LX/2da;->c:Landroid/view/View;

    check-cast v0, LX/0wO;

    invoke-interface {v0}, LX/0wO;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 443378
    const-class v1, Landroid/text/style/ClickableSpan;

    invoke-interface {v0, p1, p2, v1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ClickableSpan;

    goto :goto_0
.end method
