.class public LX/2cQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile j:LX/2cQ;


# instance fields
.field private final b:LX/0iA;

.field public final c:LX/119;

.field private final d:Ljava/util/concurrent/ScheduledExecutorService;

.field private final e:LX/0So;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/Object;

.field private h:J

.field private i:Lcom/facebook/omnistore/Collection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 440860
    const-class v0, LX/2cQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2cQ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0iA;LX/119;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;LX/0Ot;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0iA;",
            "LX/119;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0So;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 440839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440840
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/2cQ;->g:Ljava/lang/Object;

    .line 440841
    iput-object p1, p0, LX/2cQ;->b:LX/0iA;

    .line 440842
    iput-object p2, p0, LX/2cQ;->c:LX/119;

    .line 440843
    iput-object p3, p0, LX/2cQ;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 440844
    iput-object p4, p0, LX/2cQ;->e:LX/0So;

    .line 440845
    iput-object p5, p0, LX/2cQ;->f:LX/0Ot;

    .line 440846
    return-void
.end method

.method public static a(LX/0QB;)LX/2cQ;
    .locals 9

    .prologue
    .line 440847
    sget-object v0, LX/2cQ;->j:LX/2cQ;

    if-nez v0, :cond_1

    .line 440848
    const-class v1, LX/2cQ;

    monitor-enter v1

    .line 440849
    :try_start_0
    sget-object v0, LX/2cQ;->j:LX/2cQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 440850
    if-eqz v2, :cond_0

    .line 440851
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 440852
    new-instance v3, LX/2cQ;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v4

    check-cast v4, LX/0iA;

    invoke-static {v0}, LX/119;->a(LX/0QB;)LX/119;

    move-result-object v5

    check-cast v5, LX/119;

    invoke-static {v0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    const/16 v8, 0x259

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/2cQ;-><init>(LX/0iA;LX/119;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;LX/0Ot;)V

    .line 440853
    move-object v0, v3

    .line 440854
    sput-object v0, LX/2cQ;->j:LX/2cQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 440855
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 440856
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 440857
    :cond_1
    sget-object v0, LX/2cQ;->j:LX/2cQ;

    return-object v0

    .line 440858
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 440859
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized b(LX/2cQ;)V
    .locals 13

    .prologue
    .line 440811
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2cQ;->i:Lcom/facebook/omnistore/Collection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    .line 440812
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 440813
    :cond_1
    :try_start_1
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 440814
    iget-object v0, p0, LX/2cQ;->i:Lcom/facebook/omnistore/Collection;

    const-string v1, "00000000"

    const/16 v3, 0x64

    sget-object v4, Lcom/facebook/omnistore/Collection$SortDirection;->ASCENDING:Lcom/facebook/omnistore/Collection$SortDirection;

    invoke-virtual {v0, v1, v3, v4}, Lcom/facebook/omnistore/Collection;->query(Ljava/lang/String;ILcom/facebook/omnistore/Collection$SortDirection;)Lcom/facebook/omnistore/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    const/4 v1, 0x0

    .line 440815
    :cond_2
    :goto_1
    :try_start_2
    invoke-virtual {v3}, Lcom/facebook/omnistore/Cursor;->step()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 440816
    invoke-virtual {v3}, Lcom/facebook/omnistore/Cursor;->getBlob()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 440817
    sget-object v4, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    new-instance v4, LX/6Y1;

    invoke-direct {v4}, LX/6Y1;-><init>()V

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v5

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    add-int/2addr v5, v6

    .line 440818
    iput v5, v4, LX/6Y1;->a:I

    iput-object v0, v4, LX/6Y1;->b:Ljava/nio/ByteBuffer;

    move-object v4, v4

    .line 440819
    move-object v0, v4

    .line 440820
    if-nez v0, :cond_4

    .line 440821
    iget-object v0, p0, LX/2cQ;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v4, "cannot convert flat buffer to interstitial"

    const-string v5, "Fetched an interstitial from omnistore colection whose all fields are not set"

    invoke-virtual {v0, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    .line 440822
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 440823
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_2
    if-eqz v3, :cond_3

    if-eqz v1, :cond_6

    :try_start_4
    invoke-virtual {v3}, Lcom/facebook/omnistore/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_3
    :goto_3
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 440824
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 440825
    :cond_4
    :try_start_6
    invoke-virtual {v0}, LX/6Y1;->d()J

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-nez v7, :cond_7

    .line 440826
    const/4 v7, 0x0

    .line 440827
    :goto_4
    move-object v0, v7

    .line 440828
    if-eqz v0, :cond_2

    .line 440829
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 440830
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 440831
    :cond_5
    iget-object v0, p0, LX/2cQ;->b:LX/0iA;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0iA;->a(Ljava/util/List;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 440832
    if-eqz v3, :cond_0

    :try_start_7
    invoke-virtual {v3}, Lcom/facebook/omnistore/Cursor;->close()V

    goto :goto_0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_6
    invoke-virtual {v3}, Lcom/facebook/omnistore/Cursor;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_3

    .line 440833
    :cond_7
    const/4 v7, 0x4

    invoke-virtual {v0, v7}, LX/0eW;->a(I)I

    move-result v7

    if-eqz v7, :cond_8

    iget v8, v0, LX/0eW;->a:I

    add-int/2addr v7, v8

    invoke-virtual {v0, v7}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v7

    :goto_5
    move-object v9, v7

    .line 440834
    new-instance v7, Lcom/facebook/interstitial/api/FetchInterstitialResult;

    .line 440835
    const/4 v8, 0x6

    invoke-virtual {v0, v8}, LX/0eW;->a(I)I

    move-result v8

    if-eqz v8, :cond_9

    iget-object v10, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v11, v0, LX/0eW;->a:I

    add-int/2addr v8, v11

    invoke-virtual {v10, v8}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v8

    :goto_6
    move v8, v8

    .line 440836
    iget-object v10, p0, LX/2cQ;->c:LX/119;

    .line 440837
    const/16 v11, 0x8

    invoke-virtual {v0, v11}, LX/0eW;->a(I)I

    move-result v11

    if-eqz v11, :cond_a

    iget v12, v0, LX/0eW;->a:I

    add-int/2addr v11, v12

    invoke-virtual {v0, v11}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v11

    :goto_7
    move-object v11, v11

    .line 440838
    invoke-virtual {v10, v11, v9}, LX/119;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    invoke-virtual {v0}, LX/6Y1;->d()J

    move-result-wide v11

    invoke-direct/range {v7 .. v12}, Lcom/facebook/interstitial/api/FetchInterstitialResult;-><init>(ILjava/lang/String;Landroid/os/Parcelable;J)V

    goto :goto_4

    :cond_8
    const/4 v7, 0x0

    goto :goto_5

    :cond_9
    const/4 v8, 0x0

    goto :goto_6

    :cond_a
    const/4 v11, 0x0

    goto :goto_7
.end method


# virtual methods
.method public final a()V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 440805
    iget-object v1, p0, LX/2cQ;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 440806
    :try_start_0
    iget-object v0, p0, LX/2cQ;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/2cQ;->h:J

    sub-long/2addr v2, v4

    .line 440807
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x5

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    .line 440808
    iget-object v0, p0, LX/2cQ;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/2cQ;->h:J

    .line 440809
    iget-object v0, p0, LX/2cQ;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/facebook/interstitial/omnistore/InterstitialConfigurationOmnistoreSubscriber$1;

    invoke-direct {v2, p0}, Lcom/facebook/interstitial/omnistore/InterstitialConfigurationOmnistoreSubscriber$1;-><init>(LX/2cQ;)V

    const-wide/16 v4, 0x5

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v4, v5, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 440810
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/omnistore/Collection;)V
    .locals 1
    .param p1    # Lcom/facebook/omnistore/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 440802
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/2cQ;->i:Lcom/facebook/omnistore/Collection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 440803
    monitor-exit p0

    return-void

    .line 440804
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
