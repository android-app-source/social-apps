.class public final enum LX/2Cv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Cv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Cv;

.field public static final enum BALANCED_POWER_AND_ACCURACY:LX/2Cv;

.field public static final enum HIGH_ACCURACY:LX/2Cv;

.field public static final enum LOW_POWER:LX/2Cv;

.field public static final enum NO_POWER:LX/2Cv;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 383412
    new-instance v0, LX/2Cv;

    const-string v1, "NO_POWER"

    invoke-direct {v0, v1, v2}, LX/2Cv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Cv;->NO_POWER:LX/2Cv;

    .line 383413
    new-instance v0, LX/2Cv;

    const-string v1, "LOW_POWER"

    invoke-direct {v0, v1, v3}, LX/2Cv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Cv;->LOW_POWER:LX/2Cv;

    .line 383414
    new-instance v0, LX/2Cv;

    const-string v1, "BALANCED_POWER_AND_ACCURACY"

    invoke-direct {v0, v1, v4}, LX/2Cv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Cv;->BALANCED_POWER_AND_ACCURACY:LX/2Cv;

    .line 383415
    new-instance v0, LX/2Cv;

    const-string v1, "HIGH_ACCURACY"

    invoke-direct {v0, v1, v5}, LX/2Cv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Cv;->HIGH_ACCURACY:LX/2Cv;

    .line 383416
    const/4 v0, 0x4

    new-array v0, v0, [LX/2Cv;

    sget-object v1, LX/2Cv;->NO_POWER:LX/2Cv;

    aput-object v1, v0, v2

    sget-object v1, LX/2Cv;->LOW_POWER:LX/2Cv;

    aput-object v1, v0, v3

    sget-object v1, LX/2Cv;->BALANCED_POWER_AND_ACCURACY:LX/2Cv;

    aput-object v1, v0, v4

    sget-object v1, LX/2Cv;->HIGH_ACCURACY:LX/2Cv;

    aput-object v1, v0, v5

    sput-object v0, LX/2Cv;->$VALUES:[LX/2Cv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 383417
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Cv;
    .locals 1

    .prologue
    .line 383418
    const-class v0, LX/2Cv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Cv;

    return-object v0
.end method

.method public static values()[LX/2Cv;
    .locals 1

    .prologue
    .line 383419
    sget-object v0, LX/2Cv;->$VALUES:[LX/2Cv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Cv;

    return-object v0
.end method
