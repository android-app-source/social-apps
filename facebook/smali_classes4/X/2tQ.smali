.class public LX/2tQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 474957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 474958
    iput-object p1, p0, LX/2tQ;->a:Landroid/view/ViewGroup;

    .line 474959
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    .line 474960
    const-string v0, "PercentLayout"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474961
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "adjustChildren: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2tQ;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " widthMeasureSpec: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " heightMeasureSpec: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 474962
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 474963
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 474964
    const/4 v0, 0x0

    iget-object v1, p0, LX/2tQ;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_b

    .line 474965
    iget-object v0, p0, LX/2tQ;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 474966
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 474967
    const-string v6, "PercentLayout"

    invoke-static {v6, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 474968
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "should adjust "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 474969
    :cond_1
    instance-of v0, v1, LX/54o;

    if-eqz v0, :cond_9

    move-object v0, v1

    .line 474970
    check-cast v0, LX/54o;

    invoke-interface {v0}, LX/54o;->a()LX/2tR;

    move-result-object v0

    .line 474971
    const-string v6, "PercentLayout"

    invoke-static {v6, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 474972
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "using "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 474973
    :cond_2
    if-eqz v0, :cond_9

    .line 474974
    instance-of v6, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v6, :cond_a

    .line 474975
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 p1, 0x0

    .line 474976
    invoke-virtual {v0, v1, v3, v4}, LX/2tR;->a(Landroid/view/ViewGroup$LayoutParams;II)V

    .line 474977
    iget-object v6, v0, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 474978
    iget-object v6, v0, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 474979
    iget-object v6, v0, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 474980
    iget-object v6, v0, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 474981
    iget-object v6, v0, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-static {v1}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v7

    invoke-static {v6, v7}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 474982
    iget-object v6, v0, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-static {v1}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v7

    invoke-static {v6, v7}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 474983
    iget v6, v0, LX/2tR;->c:F

    cmpl-float v6, v6, p1

    if-ltz v6, :cond_3

    .line 474984
    int-to-float v6, v3

    iget v7, v0, LX/2tR;->c:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 474985
    :cond_3
    iget v6, v0, LX/2tR;->d:F

    cmpl-float v6, v6, p1

    if-ltz v6, :cond_4

    .line 474986
    int-to-float v6, v4

    iget v7, v0, LX/2tR;->d:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 474987
    :cond_4
    iget v6, v0, LX/2tR;->e:F

    cmpl-float v6, v6, p1

    if-ltz v6, :cond_5

    .line 474988
    int-to-float v6, v3

    iget v7, v0, LX/2tR;->e:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 474989
    :cond_5
    iget v6, v0, LX/2tR;->f:F

    cmpl-float v6, v6, p1

    if-ltz v6, :cond_6

    .line 474990
    int-to-float v6, v4

    iget v7, v0, LX/2tR;->f:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 474991
    :cond_6
    iget v6, v0, LX/2tR;->g:F

    cmpl-float v6, v6, p1

    if-ltz v6, :cond_7

    .line 474992
    int-to-float v6, v3

    iget v7, v0, LX/2tR;->g:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-static {v1, v6}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 474993
    :cond_7
    iget v6, v0, LX/2tR;->h:F

    cmpl-float v6, v6, p1

    if-ltz v6, :cond_8

    .line 474994
    int-to-float v6, v3

    iget v7, v0, LX/2tR;->h:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-static {v1, v6}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 474995
    :cond_8
    const-string v6, "PercentLayout"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 474996
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "after fillMarginLayoutParams: ("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 474997
    :cond_9
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 474998
    :cond_a
    invoke-virtual {v0, v1, v3, v4}, LX/2tR;->a(Landroid/view/ViewGroup$LayoutParams;II)V

    goto :goto_1

    .line 474999
    :cond_b
    return-void
.end method
