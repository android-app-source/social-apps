.class public LX/2UQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Sh;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/2Iv;


# direct methods
.method public constructor <init>(LX/0Sh;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Iv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415987
    iput-object p1, p0, LX/2UQ;->a:LX/0Sh;

    .line 415988
    iput-object p2, p0, LX/2UQ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 415989
    iput-object p3, p0, LX/2UQ;->c:LX/2Iv;

    .line 415990
    return-void
.end method

.method public static a(LX/0QB;)LX/2UQ;
    .locals 1

    .prologue
    .line 415959
    invoke-static {p0}, LX/2UQ;->b(LX/0QB;)LX/2UQ;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2UQ;
    .locals 4

    .prologue
    .line 415984
    new-instance v3, LX/2UQ;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/2Iv;->a(LX/0QB;)LX/2Iv;

    move-result-object v2

    check-cast v2, LX/2Iv;

    invoke-direct {v3, v0, v1, v2}, LX/2UQ;-><init>(LX/0Sh;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Iv;)V

    .line 415985
    return-object v3
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 415980
    iget-object v0, p0, LX/2UQ;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 415981
    iget-object v0, p0, LX/2UQ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/32V;->o:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 415982
    iget-object v0, p0, LX/2UQ;->c:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "phone_address_book_snapshot"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 415983
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/95C;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 415960
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415961
    :goto_0
    return-void

    .line 415962
    :cond_0
    const-string v0, "UpdatePhoneAddressBookSnapshot(%d)"

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, -0x2cfef204    # -5.542854E11f

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 415963
    :try_start_0
    iget-object v0, p0, LX/2UQ;->c:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 415964
    const v0, 0x7562350a

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 415965
    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/95C;

    .line 415966
    sget-object v3, LX/957;->a:[I

    iget-object v4, v0, LX/95C;->a:LX/95B;

    invoke-virtual {v4}, LX/95B;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 415967
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown change type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, LX/95C;->a:LX/95B;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415968
    :catchall_0
    move-exception v0

    const v2, 0x386701fd

    :try_start_2
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 415969
    :catchall_1
    move-exception v0

    const v1, 0x7eabd93c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 415970
    :pswitch_0
    :try_start_3
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 415971
    const-string v6, "local_contact_id"

    iget-object v7, v0, LX/95C;->c:LX/95A;

    iget-wide v7, v7, LX/95A;->a:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 415972
    const-string v6, "contact_hash"

    iget-object v7, v0, LX/95C;->c:LX/95A;

    iget-object v7, v7, LX/95A;->b:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 415973
    iget-object v6, p0, LX/2UQ;->c:LX/2Iv;

    invoke-virtual {v6}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const-string v7, "phone_address_book_snapshot"

    const/4 v8, 0x0

    const v9, 0x2f0724fd

    invoke-static {v9}, LX/03h;->a(I)V

    invoke-virtual {v6, v7, v8, v5}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v5, 0x6b5f8e9e

    invoke-static {v5}, LX/03h;->a(I)V

    .line 415974
    goto :goto_1

    .line 415975
    :pswitch_1
    iget-object v5, p0, LX/2UQ;->c:LX/2Iv;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "phone_address_book_snapshot"

    const-string v7, "local_contact_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-wide v11, v0, LX/95C;->b:J

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 415976
    goto/16 :goto_1

    .line 415977
    :cond_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 415978
    const v0, 0x4667fc91

    :try_start_4
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 415979
    const v0, 0x4f023fe1

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
