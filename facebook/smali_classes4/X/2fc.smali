.class public final LX/2fc;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

.field public final synthetic d:Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)V
    .locals 0

    .prologue
    .line 447020
    iput-object p1, p0, LX/2fc;->d:Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;

    iput-object p2, p0, LX/2fc;->a:LX/0Px;

    iput-object p3, p0, LX/2fc;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/2fc;->c:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 447021
    iget-object v0, p0, LX/2fc;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/2fc;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    .line 447022
    iget-object v4, p0, LX/2fc;->d:Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;

    iget-object v4, v4, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->e:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    new-instance v5, LX/2fd;

    iget-object v6, p0, LX/2fc;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v5, v6, v0, v2}, LX/2fd;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;Z)V

    invoke-virtual {p1, v4, v5}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 447023
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 447024
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 447025
    iget-object v0, p0, LX/2fc;->d:Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->f:LX/1LV;

    iget-object v1, p0, LX/2fc;->c:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    invoke-virtual {v0, v1, p1}, LX/1LV;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 447026
    iget-object v0, p0, LX/2fc;->c:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    iget-object v1, p0, LX/2fc;->a:LX/0Px;

    invoke-static {v0, v1, p1}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/util/List;I)V

    .line 447027
    return-void
.end method
