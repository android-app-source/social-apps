.class public LX/2lM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/03V;

.field public final d:Landroid/content/pm/PackageManager;

.field private e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/03V;Landroid/content/pm/PackageManager;LX/0Or;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/onavo/annotations/IsOnavoBookmarkInAppsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/content/pm/PackageManager;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 457711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457712
    iput-object p1, p0, LX/2lM;->a:Landroid/content/Context;

    .line 457713
    iput-object p2, p0, LX/2lM;->b:Lcom/facebook/content/SecureContextHelper;

    .line 457714
    iput-object p3, p0, LX/2lM;->c:LX/03V;

    .line 457715
    iput-object p4, p0, LX/2lM;->d:Landroid/content/pm/PackageManager;

    .line 457716
    iput-object p5, p0, LX/2lM;->e:LX/0Or;

    .line 457717
    return-void
.end method

.method public static a(LX/0QB;)LX/2lM;
    .locals 1

    .prologue
    .line 457710
    invoke-static {p0}, LX/2lM;->b(LX/0QB;)LX/2lM;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2lM;
    .locals 6

    .prologue
    .line 457706
    new-instance v0, LX/2lM;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageManager;

    const/16 v5, 0x33d

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/2lM;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/03V;Landroid/content/pm/PackageManager;LX/0Or;)V

    .line 457707
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 457709
    iget-object v0, p0, LX/2lM;->a:Landroid/content/Context;

    const v1, 0x7f08340e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 457708
    iget-object v0, p0, LX/2lM;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method
