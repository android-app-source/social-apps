.class public LX/2KJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2KJ;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ps;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0ps;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 393631
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393632
    iput-object p1, p0, LX/2KJ;->a:LX/0Ot;

    .line 393633
    return-void
.end method

.method public static a(LX/0QB;)LX/2KJ;
    .locals 4

    .prologue
    .line 393649
    sget-object v0, LX/2KJ;->b:LX/2KJ;

    if-nez v0, :cond_1

    .line 393650
    const-class v1, LX/2KJ;

    monitor-enter v1

    .line 393651
    :try_start_0
    sget-object v0, LX/2KJ;->b:LX/2KJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 393652
    if-eqz v2, :cond_0

    .line 393653
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 393654
    new-instance v3, LX/2KJ;

    const/16 p0, 0x4c1

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2KJ;-><init>(LX/0Ot;)V

    .line 393655
    move-object v0, v3

    .line 393656
    sput-object v0, LX/2KJ;->b:LX/2KJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 393657
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 393658
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 393659
    :cond_1
    sget-object v0, LX/2KJ;->b:LX/2KJ;

    return-object v0

    .line 393660
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 393661
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 393634
    iget-object v0, p0, LX/2KJ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ps;

    .line 393635
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 393636
    iget-object v2, v0, LX/0ps;->p:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 393637
    iget-object v1, v0, LX/0ps;->l:LX/0pt;

    .line 393638
    iput-object v0, v1, LX/0pt;->b:LX/0ps;

    .line 393639
    iget-object v2, v1, LX/0pt;->j:LX/0pu;

    invoke-virtual {v2}, LX/0pu;->a()Z

    move-result v2

    iput-boolean v2, v1, LX/0pt;->g:Z

    .line 393640
    iget-object v2, v1, LX/0pt;->k:LX/0Uo;

    invoke-virtual {v2}, LX/0Uo;->l()Z

    move-result v2

    iput-boolean v2, v1, LX/0pt;->h:Z

    .line 393641
    invoke-static {v1}, LX/0pt;->b$redex0(LX/0pt;)V

    .line 393642
    iget-object v2, v1, LX/0pt;->n:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->b()V

    .line 393643
    iget-object v2, v1, LX/0pt;->j:LX/0pu;

    iget-object p0, v1, LX/0pt;->l:LX/0q0;

    invoke-virtual {v2, p0}, LX/0pu;->a(LX/0q0;)V

    .line 393644
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0ps;->H:Z

    .line 393645
    iget-object v1, v0, LX/0ps;->w:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    iget-object p0, v0, LX/0ps;->y:LX/0YZ;

    invoke-interface {v1, v2, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    iget-object p0, v0, LX/0ps;->z:LX/0YZ;

    invoke-interface {v1, v2, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    iget-object v2, v0, LX/0ps;->v:Landroid/os/Handler;

    invoke-interface {v1, v2}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, v0, LX/0ps;->x:LX/0Yb;

    .line 393646
    iget-object v1, v0, LX/0ps;->x:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 393647
    invoke-virtual {v0}, LX/0ps;->g()V

    .line 393648
    return-void
.end method
