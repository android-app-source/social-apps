.class public final LX/2VV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/2Mo;


# direct methods
.method public constructor <init>(LX/2Mo;)V
    .locals 0

    .prologue
    .line 417752
    iput-object p1, p0, LX/2VV;->a:LX/2Mo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x68f79b14

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 417753
    iget-object v1, p0, LX/2VV;->a:LX/2Mo;

    .line 417754
    iget-object v2, v1, LX/2Mo;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object p0, v1, LX/2Mo;->c:LX/0Tn;

    invoke-interface {v2, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 417755
    iget-object v2, v1, LX/2Mo;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object p0, v1, LX/2Mo;->c:LX/0Tn;

    const-string p1, ""

    invoke-interface {v2, p0, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 417756
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_1

    .line 417757
    const-string p0, ","

    invoke-virtual {v2, p0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 417758
    iget-object v2, v1, LX/2Mo;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p2

    .line 417759
    const/4 v2, 0x0

    move p0, v2

    :goto_0
    array-length v2, p1

    if-ge p0, v2, :cond_0

    .line 417760
    iget-object v2, v1, LX/2Mo;->b:LX/0Tn;

    aget-object p3, p1, p0

    invoke-virtual {v2, p3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/0Tn;

    invoke-interface {p2, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 417761
    add-int/lit8 v2, p0, 0x1

    move p0, v2

    goto :goto_0

    .line 417762
    :cond_0
    iget-object v2, v1, LX/2Mo;->c:LX/0Tn;

    invoke-interface {p2, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 417763
    :cond_1
    const/16 v1, 0x27

    const v2, -0x45f5e48a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
