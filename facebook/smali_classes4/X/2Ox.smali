.class public LX/2Ox;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile g:LX/2Ox;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2NA;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/2Oy;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Dom;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/2Oz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 404110
    const-class v0, LX/2Ox;

    sput-object v0, LX/2Ox;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Ot;LX/2Oy;LX/0Or;LX/2Oz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2NA;",
            ">;",
            "LX/2Oy;",
            "LX/0Or",
            "<",
            "LX/Dom;",
            ">;",
            "LX/2Oz;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 404111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 404112
    iput-object p1, p0, LX/2Ox;->b:LX/0Or;

    .line 404113
    iput-object p2, p0, LX/2Ox;->c:LX/0Ot;

    .line 404114
    iput-object p3, p0, LX/2Ox;->d:LX/2Oy;

    .line 404115
    iput-object p4, p0, LX/2Ox;->e:LX/0Or;

    .line 404116
    iput-object p5, p0, LX/2Ox;->f:LX/2Oz;

    .line 404117
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)I
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 404118
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, LX/2Ox;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)I
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 404119
    sget-object v0, LX/2P5;->a:LX/0U1;

    .line 404120
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 404121
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 404122
    const-string v1, "threads"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, p2, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static a(LX/0QB;)LX/2Ox;
    .locals 9

    .prologue
    .line 404123
    sget-object v0, LX/2Ox;->g:LX/2Ox;

    if-nez v0, :cond_1

    .line 404124
    const-class v1, LX/2Ox;

    monitor-enter v1

    .line 404125
    :try_start_0
    sget-object v0, LX/2Ox;->g:LX/2Ox;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 404126
    if-eqz v2, :cond_0

    .line 404127
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 404128
    new-instance v3, LX/2Ox;

    const/16 v4, 0xdc6

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0xd1c

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/2Oy;->a(LX/0QB;)LX/2Oy;

    move-result-object v6

    check-cast v6, LX/2Oy;

    const/16 v7, 0x2a17

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/2Oz;->b(LX/0QB;)LX/2Oz;

    move-result-object v8

    check-cast v8, LX/2Oz;

    invoke-direct/range {v3 .. v8}, LX/2Ox;-><init>(LX/0Or;LX/0Ot;LX/2Oy;LX/0Or;LX/2Oz;)V

    .line 404129
    move-object v0, v3

    .line 404130
    sput-object v0, LX/2Ox;->g:LX/2Ox;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404131
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 404132
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 404133
    :cond_1
    sget-object v0, LX/2Ox;->g:LX/2Ox;

    return-object v0

    .line 404134
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 404135
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/2Ox;Lcom/facebook/messaging/model/messages/Message;[B)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 404136
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 404137
    sget-object v1, LX/2P1;->a:LX/0U1;

    .line 404138
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404139
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404140
    sget-object v1, LX/2P1;->b:LX/0U1;

    .line 404141
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404142
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404143
    sget-object v1, LX/2P1;->c:LX/0U1;

    .line 404144
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404145
    invoke-direct {p0, p1, p2}, LX/2Ox;->b(Lcom/facebook/messaging/model/messages/Message;[B)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 404146
    sget-object v1, LX/2P1;->e:LX/0U1;

    .line 404147
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404148
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404149
    sget-object v1, LX/2P1;->f:LX/0U1;

    .line 404150
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404151
    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404152
    sget-object v1, LX/2P1;->g:LX/0U1;

    .line 404153
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404154
    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404155
    sget-object v1, LX/2P1;->i:LX/0U1;

    .line 404156
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404157
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    iget v2, v2, LX/2uW;->dbKeyValue:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 404158
    sget-object v1, LX/2P1;->j:LX/0U1;

    .line 404159
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404160
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404161
    sget-object v1, LX/2P1;->l:LX/0U1;

    .line 404162
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404163
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->K:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404164
    return-object v0
.end method

.method private static a(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 404165
    sget-object v0, LX/2P5;->g:LX/0U1;

    .line 404166
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 404167
    invoke-direct {p0, v0, p1, p2, p3}, LX/2Ox;->a(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 404168
    return-void
.end method

.method private static a(LX/2Ox;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 404169
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;

    const/4 v2, 0x0

    .line 404170
    :try_start_0
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 404171
    sget-object v3, LX/2P1;->a:LX/0U1;

    .line 404172
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 404173
    invoke-static {v3, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v3

    .line 404174
    const-string v4, "messages"

    invoke-virtual {v3}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v4, p2, p0, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 404175
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/2gJ;->close()V

    .line 404176
    :cond_0
    return-void

    .line 404177
    :catch_0
    move-exception v1

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404178
    :catchall_0
    move-exception v2

    move-object v3, v2

    move-object v2, v1

    move-object v1, v3

    :goto_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v1

    :catch_1
    move-exception v0

    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, LX/2gJ;->close()V

    goto :goto_1

    :catchall_1
    move-exception v1

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    .locals 4

    .prologue
    .line 404179
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 404180
    sget-object v1, LX/2P5;->e:LX/0U1;

    .line 404181
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404182
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404183
    sget-object v1, LX/2P5;->a:LX/0U1;

    .line 404184
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 404185
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 404186
    const-string v2, "threads"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 404187
    return-void
.end method

.method private static a(Lcom/facebook/messaging/model/send/SendError;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 404188
    sget-object v0, LX/2P1;->m:LX/0U1;

    .line 404189
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 404190
    iget-object v1, p0, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    iget-object v1, v1, LX/6fP;->serializedString:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404191
    sget-object v0, LX/2P1;->n:LX/0U1;

    .line 404192
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 404193
    iget-object v1, p0, Lcom/facebook/messaging/model/send/SendError;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404194
    sget-object v0, LX/2P1;->o:LX/0U1;

    .line 404195
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 404196
    iget-wide v2, p0, Lcom/facebook/messaging/model/send/SendError;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404197
    return-void
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 404198
    sget-object v0, LX/2P5;->a:LX/0U1;

    .line 404199
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 404200
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 404201
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;

    .line 404202
    :try_start_0
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 404203
    const v4, -0x15a95b38

    invoke-static {v3, v4}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 404204
    :try_start_1
    const-string v4, "threads"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, p2, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 404205
    if-eqz v1, :cond_1

    .line 404206
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404207
    const v1, 0x2293f6c

    :try_start_2
    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 404208
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/2gJ;->close()V

    .line 404209
    :cond_0
    :goto_0
    return-void

    .line 404210
    :cond_1
    :try_start_3
    invoke-static {p0, p1, p2}, LX/2Ox;->c(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)[B

    .line 404211
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v1

    iget-wide v4, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v1, v4, v5, p2}, LX/2Ox;->a(Ljava/lang/String;JLandroid/content/ContentValues;)V

    .line 404212
    const-string v1, "threads"

    const/4 v4, 0x0

    const v5, -0x6645406f

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {v3, v1, v4, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, 0x209db947

    invoke-static {v1}, LX/03h;->a(I)V

    .line 404213
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 404214
    const v1, 0x54f49213

    :try_start_4
    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 404215
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/2gJ;->close()V

    goto :goto_0

    .line 404216
    :catchall_0
    move-exception v1

    const v4, -0x301829d5

    :try_start_5
    invoke-static {v3, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 404217
    :catch_0
    move-exception v1

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 404218
    :catchall_1
    move-exception v2

    move-object v6, v2

    move-object v2, v1

    move-object v1, v6

    :goto_1
    if-eqz v0, :cond_2

    if-eqz v2, :cond_3

    :try_start_7
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1

    :cond_2
    :goto_2
    throw v1

    :catch_1
    move-exception v0

    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v0}, LX/2gJ;->close()V

    goto :goto_2

    :catchall_2
    move-exception v1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;JLandroid/content/ContentValues;)V
    .locals 3

    .prologue
    .line 404219
    sget-object v0, LX/2P5;->a:LX/0U1;

    .line 404220
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 404221
    invoke-virtual {p3, v0, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404222
    sget-object v0, LX/2P5;->b:LX/0U1;

    .line 404223
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 404224
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404225
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 404226
    invoke-static {p0, p2, p4}, LX/2Ox;->c(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)[B

    move-result-object v1

    .line 404227
    const/4 v0, 0x0

    .line 404228
    if-eqz p3, :cond_0

    .line 404229
    :try_start_0
    iget-object v0, p0, LX/2Ox;->d:LX/2Oy;

    const-string v2, "UTF-8"

    invoke-virtual {p3, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2Oy;->a([B[B)[B

    move-result-object v0

    .line 404230
    :cond_0
    invoke-virtual {p4, p1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 404231
    return-void

    .line 404232
    :catch_0
    move-exception v0

    .line 404233
    :goto_0
    sget-object v1, LX/2Ox;->a:Ljava/lang/Class;

    const-string v2, "Failed to encrypt crypto session for local storage"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 404234
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 404235
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public static a$redex0(LX/2Ox;[B)[B
    .locals 3

    .prologue
    .line 404236
    :try_start_0
    iget-object v0, p0, LX/2Ox;->d:LX/2Oy;

    invoke-virtual {v0, p1}, LX/2Oy;->a([B)[B
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 404237
    :catch_0
    move-exception v0

    .line 404238
    :goto_0
    sget-object v1, LX/2Ox;->a:Ljava/lang/Class;

    const-string v2, "Failed to encrypt message for local storage"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 404239
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 404240
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method private static b(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 404241
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;

    const/4 v2, 0x0

    .line 404242
    :try_start_0
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 404243
    invoke-static {v1, p1, p2}, LX/2Ox;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 404244
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/2gJ;->close()V

    .line 404245
    :cond_0
    return-void

    .line 404246
    :catch_0
    move-exception v1

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404247
    :catchall_0
    move-exception v2

    move-object v3, v2

    move-object v2, v1

    move-object v1, v3

    :goto_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v1

    :catch_1
    move-exception v0

    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, LX/2gJ;->close()V

    goto :goto_1

    :catchall_1
    move-exception v1

    goto :goto_0
.end method

.method private b(Lcom/facebook/messaging/model/messages/Message;[B)[B
    .locals 3

    .prologue
    .line 404248
    iget-object v0, p0, LX/2Ox;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dom;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/Dom;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)[B

    move-result-object v0

    .line 404249
    :try_start_0
    iget-object v1, p0, LX/2Ox;->d:LX/2Oy;

    invoke-virtual {v1, v0, p2}, LX/2Oy;->a([B[B)[B
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 404250
    :catch_0
    move-exception v0

    .line 404251
    :goto_0
    sget-object v1, LX/2Ox;->a:Ljava/lang/Class;

    const-string v2, "Failed to encrypt message for local storage"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 404252
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 404253
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method private static c(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)[B
    .locals 4

    .prologue
    .line 404254
    :try_start_0
    iget-object v0, p0, LX/2Ox;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dom;

    .line 404255
    invoke-virtual {v0, p1}, LX/Dom;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)[B

    move-result-object v1

    .line 404256
    if-nez v1, :cond_0

    .line 404257
    iget-object v1, p0, LX/2Ox;->d:LX/2Oy;

    .line 404258
    sget-object v2, LX/2Oy;->a:LX/1Hy;

    iget v2, v2, LX/1Hy;->keyLength:I

    new-array v2, v2, [B

    .line 404259
    iget-object v3, v1, LX/2Oy;->c:LX/1Hn;

    iget-object v3, v3, LX/1Ho;->b:Ljava/security/SecureRandom;

    invoke-virtual {v3, v2}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 404260
    move-object v1, v2

    .line 404261
    :cond_0
    iget-object v2, v0, LX/Dom;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404262
    iget-object v0, p0, LX/2Ox;->d:LX/2Oy;

    invoke-virtual {v0, v1}, LX/2Oy;->a([B)[B

    move-result-object v0

    .line 404263
    sget-object v2, LX/2P5;->p:LX/0U1;

    .line 404264
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 404265
    invoke-virtual {p2, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 404266
    return-object v1

    .line 404267
    :catch_0
    move-exception v0

    .line 404268
    :goto_0
    sget-object v1, LX/2Ox;->a:Ljava/lang/Class;

    const-string v2, "Failed to encrypt key for local storage"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 404269
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 404270
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 404271
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 404272
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 404273
    move-object v4, v0

    .line 404274
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 404275
    sget-object v0, LX/2P5;->g:LX/0U1;

    .line 404276
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404277
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404278
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 404279
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 404280
    const-string v5, "threads"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v5, v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 404281
    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 404282
    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 404283
    :cond_1
    return-void

    .line 404284
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 404285
    :catchall_0
    move-exception v2

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    :goto_0
    if-eqz v0, :cond_2

    if-eqz v2, :cond_4

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_2
    :goto_1
    :try_start_5
    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 404286
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 404287
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_2
    if-eqz v4, :cond_3

    if-eqz v3, :cond_5

    :try_start_7
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :cond_3
    :goto_3
    throw v0

    .line 404288
    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 404289
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 404290
    :cond_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_1

    .line 404291
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_3

    .line 404292
    :catchall_3
    move-exception v1

    move-object v2, v3

    goto :goto_0
.end method

.method public final a(J)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 403828
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403829
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403830
    move-object v4, v0

    .line 403831
    :try_start_0
    sget-object v0, LX/2P1;->l:LX/0U1;

    .line 403832
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 403833
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->c(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 403834
    sget-object v1, LX/2P1;->p:LX/0U1;

    .line 403835
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 403836
    const-string v2, "1"

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 403837
    const/4 v2, 0x2

    new-array v2, v2, [LX/0ux;

    const/4 v5, 0x0

    aput-object v0, v2, v5

    const/4 v0, 0x1

    aput-object v1, v2, v0

    invoke-static {v2}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v1

    .line 403838
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 403839
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 403840
    const-string v5, "messages"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v5, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 403841
    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 403842
    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 403843
    :cond_1
    return-void

    .line 403844
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 403845
    :catchall_0
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    :goto_0
    if-eqz v0, :cond_2

    if-eqz v2, :cond_4

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_2
    :goto_1
    :try_start_5
    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 403846
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 403847
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_2
    if-eqz v4, :cond_3

    if-eqz v3, :cond_5

    :try_start_7
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :cond_3
    :goto_3
    throw v0

    .line 403848
    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 403849
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 403850
    :cond_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_1

    .line 403851
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_3

    .line 403852
    :catchall_3
    move-exception v1

    move-object v2, v3

    goto :goto_0
.end method

.method public final a(JLjava/lang/String;)V
    .locals 11
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi",
            "Recycle"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 404293
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 404294
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 404295
    move-object v4, v0

    .line 404296
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 404297
    sget-object v0, LX/2P5;->c:LX/0U1;

    .line 404298
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 404299
    invoke-virtual {v2, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404300
    sget-object v0, LX/2P5;->b:LX/0U1;

    .line 404301
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 404302
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v5

    .line 404303
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 404304
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 404305
    const-string v7, "threads"

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v7, v2, v8, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 404306
    if-ne v2, v1, :cond_2

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 404307
    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 404308
    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 404309
    :cond_1
    return-void

    .line 404310
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 404311
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 404312
    :catchall_0
    move-exception v2

    move-object v9, v2

    move-object v2, v1

    move-object v1, v9

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v2, :cond_5

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_3
    :goto_2
    :try_start_5
    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 404313
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 404314
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_3
    if-eqz v4, :cond_4

    if-eqz v3, :cond_6

    :try_start_7
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :cond_4
    :goto_4
    throw v0

    .line 404315
    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 404316
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 404317
    :cond_5
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 404318
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_4

    .line 404319
    :catchall_3
    move-exception v1

    move-object v2, v3

    goto :goto_1
.end method

.method public final a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 404320
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 404321
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 404322
    move-object v4, v0

    .line 404323
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 404324
    sget-object v0, LX/2P2;->a:LX/0U1;

    .line 404325
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404326
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404327
    sget-object v0, LX/2P2;->b:LX/0U1;

    .line 404328
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404329
    invoke-virtual {v1, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404330
    sget-object v0, LX/2P2;->c:LX/0U1;

    .line 404331
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404332
    invoke-virtual {v1, v0, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404333
    sget-object v0, LX/2P2;->d:LX/0U1;

    .line 404334
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404335
    invoke-virtual {v1, v0, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404336
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 404337
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 404338
    const-string v5, "thread_participants"

    const/4 v6, 0x0

    const v7, 0x2a793ead

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v2, v5, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, -0x5c7bfd41

    invoke-static {v1}, LX/03h;->a(I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 404339
    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 404340
    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 404341
    :cond_1
    return-void

    .line 404342
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 404343
    :catchall_0
    move-exception v2

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    :goto_0
    if-eqz v0, :cond_2

    if-eqz v2, :cond_4

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_2
    :goto_1
    :try_start_5
    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 404344
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 404345
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_2
    if-eqz v4, :cond_3

    if-eqz v3, :cond_5

    :try_start_7
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :cond_3
    :goto_3
    throw v0

    .line 404346
    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 404347
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 404348
    :cond_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_1

    .line 404349
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_3

    .line 404350
    :catchall_3
    move-exception v1

    move-object v2, v3

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;[BLjava/lang/String;)V
    .locals 9
    .param p2    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 404351
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 404352
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 404353
    move-object v4, v0

    .line 404354
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 404355
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 404356
    sget-object v0, LX/2P1;->a:LX/0U1;

    .line 404357
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404358
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404359
    sget-object v0, LX/2P1;->b:LX/0U1;

    .line 404360
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404361
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404362
    sget-object v0, LX/2P1;->c:LX/0U1;

    .line 404363
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404364
    invoke-direct {p0, p1, p2}, LX/2Ox;->b(Lcom/facebook/messaging/model/messages/Message;[B)[B

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 404365
    sget-object v0, LX/2P1;->e:LX/0U1;

    .line 404366
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404367
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404368
    sget-object v0, LX/2P1;->f:LX/0U1;

    .line 404369
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404370
    iget-wide v6, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404371
    sget-object v0, LX/2P1;->g:LX/0U1;

    .line 404372
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404373
    iget-wide v6, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404374
    sget-object v0, LX/2P1;->i:LX/0U1;

    .line 404375
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404376
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    iget v2, v2, LX/2uW;->dbKeyValue:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 404377
    sget-object v0, LX/2P1;->j:LX/0U1;

    .line 404378
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404379
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 404380
    if-eqz p3, :cond_2

    .line 404381
    :try_start_1
    const-string v0, "UTF-8"

    invoke-virtual {p3, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/2Ox;->b(Lcom/facebook/messaging/model/messages/Message;[B)[B
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v0

    .line 404382
    :goto_0
    :try_start_2
    sget-object v2, LX/2P1;->k:LX/0U1;

    .line 404383
    iget-object v5, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v5

    .line 404384
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 404385
    sget-object v0, LX/2P1;->l:LX/0U1;

    .line 404386
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 404387
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->K:Ljava/lang/Long;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404388
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    invoke-static {v0, v1}, LX/2Ox;->a(Lcom/facebook/messaging/model/send/SendError;Landroid/content/ContentValues;)V

    .line 404389
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 404390
    :try_start_3
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 404391
    const-string v5, "messages"

    const/4 v6, 0x0

    const v7, 0x3102a97e

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v2, v5, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, 0x3f181bb8

    invoke-static {v1}, LX/03h;->a(I)V

    .line 404392
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v6, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {v2, v1, v6, v7}, LX/2Ox;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 404393
    if-eqz v0, :cond_0

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 404394
    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 404395
    :cond_1
    return-void

    .line 404396
    :catch_0
    move-exception v0

    .line 404397
    :try_start_5
    sget-object v2, LX/2Ox;->a:Ljava/lang/Class;

    const-string v5, "Failed to encrypt attachment for local storage"

    invoke-static {v2, v5, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_2
    move-object v0, v3

    goto :goto_0

    .line 404398
    :catch_1
    move-exception v1

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 404399
    :catchall_0
    move-exception v2

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v2, :cond_5

    :try_start_7
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :cond_3
    :goto_2
    :try_start_8
    throw v1
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 404400
    :catch_2
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 404401
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_3
    if-eqz v4, :cond_4

    if-eqz v3, :cond_6

    :try_start_a
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_4

    :cond_4
    :goto_4
    throw v0

    .line 404402
    :catch_3
    move-exception v0

    :try_start_b
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 404403
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 404404
    :cond_5
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto :goto_2

    .line 404405
    :catch_4
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_4

    .line 404406
    :catchall_3
    move-exception v1

    move-object v2, v3

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;[B[BLjava/lang/String;)V
    .locals 10
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 404077
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 404078
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 404079
    move-object v4, v0

    .line 404080
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->f()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 404081
    if-eqz p4, :cond_7

    .line 404082
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 404083
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v1, p4, v0}, LX/2Ox;->a(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Landroid/content/ContentValues;)V

    move-object v1, v0

    .line 404084
    :goto_0
    invoke-static {p0, p1, p2}, LX/2Ox;->a(LX/2Ox;Lcom/facebook/messaging/model/messages/Message;[B)Landroid/content/ContentValues;

    move-result-object v2

    .line 404085
    sget-object v0, LX/2P1;->d:LX/0U1;

    .line 404086
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 404087
    invoke-virtual {v2, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 404088
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 404089
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 404090
    const v6, 0x1b097911

    invoke-static {v5, v6}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 404091
    :try_start_2
    const-string v6, "messages"

    const/4 v7, 0x0

    const v8, -0x13a4d31a

    invoke-static {v8}, LX/03h;->a(I)V

    invoke-virtual {v5, v6, v7, v2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v2, -0x1f0e63bb

    invoke-static {v2}, LX/03h;->a(I)V

    .line 404092
    if-eqz v1, :cond_0

    .line 404093
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v5, v2, v1}, LX/2Ox;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)I

    .line 404094
    :cond_0
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v6, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {v5, v1, v6, v7}, LX/2Ox;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    .line 404095
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 404096
    const v1, -0x23e8f4f6

    :try_start_3
    invoke-static {v5, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 404097
    if-eqz v0, :cond_1

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 404098
    :cond_1
    if-eqz v4, :cond_2

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 404099
    :cond_2
    return-void

    .line 404100
    :catchall_0
    move-exception v1

    const v2, 0x6ddf5695

    :try_start_5
    invoke-static {v5, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 404101
    :catch_0
    move-exception v1

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 404102
    :catchall_1
    move-exception v2

    move-object v9, v2

    move-object v2, v1

    move-object v1, v9

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v2, :cond_5

    :try_start_7
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :cond_3
    :goto_2
    :try_start_8
    throw v1
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 404103
    :catch_1
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 404104
    :catchall_2
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_3
    if-eqz v4, :cond_4

    if-eqz v3, :cond_6

    :try_start_a
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_3

    :cond_4
    :goto_4
    throw v0

    .line 404105
    :catch_2
    move-exception v0

    :try_start_b
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 404106
    :catchall_3
    move-exception v0

    goto :goto_3

    .line 404107
    :cond_5
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto :goto_2

    .line 404108
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_4

    .line 404109
    :catchall_4
    move-exception v1

    move-object v2, v3

    goto :goto_1

    :cond_7
    move-object v1, v3

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;[B[BLjava/lang/String;Ljava/lang/String;)V
    .locals 14
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 404407
    iget-object v2, p0, LX/2Ox;->f:LX/2Oz;

    invoke-virtual {v2}, LX/2Oz;->a()LX/2Oz;

    move-result-object v6

    const/4 v5, 0x0

    .line 404408
    :try_start_0
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 404409
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-static {v2, v0}, LX/Dpq;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 404410
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 404411
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object/from16 v0, p4

    invoke-static {p0, v2, v0, v7}, LX/2Ox;->a(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 404412
    sget-object v2, LX/2P5;->e:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    iget-wide v8, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v7, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404413
    invoke-static/range {p0 .. p2}, LX/2Ox;->a(LX/2Ox;Lcom/facebook/messaging/model/messages/Message;[B)Landroid/content/ContentValues;

    move-result-object v8

    .line 404414
    sget-object v2, LX/2P1;->d:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v8, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 404415
    iget-object v2, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    const/4 v4, 0x0

    .line 404416
    :try_start_1
    invoke-virtual {v2}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    .line 404417
    const v10, -0x34a12afe    # -1.4603522E7f

    invoke-static {v9, v10}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 404418
    :try_start_2
    const-string v10, "messages"

    const/4 v11, 0x0

    const v12, -0x79e78c56    # -2.8673E-35f

    invoke-static {v12}, LX/03h;->a(I)V

    invoke-virtual {v9, v10, v11, v8}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v8, -0x6a532068

    invoke-static {v8}, LX/03h;->a(I)V

    .line 404419
    invoke-static {v9, v3, v7}, LX/2Ox;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v8

    if-nez v8, :cond_0

    .line 404420
    iget-object v8, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v10, v8, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v3, v10, v11, v7}, LX/2Ox;->a(Ljava/lang/String;JLandroid/content/ContentValues;)V

    .line 404421
    sget-object v3, LX/2P5;->l:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 404422
    const-string v3, "threads"

    const/4 v8, 0x0

    const v10, -0x7a4c119c

    invoke-static {v10}, LX/03h;->a(I)V

    invoke-virtual {v9, v3, v8, v7}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v3, 0x6dfdd56d

    invoke-static {v3}, LX/03h;->a(I)V

    .line 404423
    :cond_0
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 404424
    const v3, 0x198bdb7b

    :try_start_3
    invoke-static {v9, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 404425
    if-eqz v2, :cond_1

    :try_start_4
    invoke-virtual {v2}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 404426
    :cond_1
    if-eqz v6, :cond_2

    invoke-virtual {v6}, LX/2Oz;->close()V

    .line 404427
    :cond_2
    return-void

    .line 404428
    :catchall_0
    move-exception v3

    const v7, -0x146704a5

    :try_start_5
    invoke-static {v9, v7}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v3
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 404429
    :catch_0
    move-exception v3

    :try_start_6
    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 404430
    :catchall_1
    move-exception v4

    move-object v13, v4

    move-object v4, v3

    move-object v3, v13

    :goto_0
    if-eqz v2, :cond_3

    if-eqz v4, :cond_5

    :try_start_7
    invoke-virtual {v2}, LX/2gJ;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :cond_3
    :goto_1
    :try_start_8
    throw v3
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 404431
    :catch_1
    move-exception v2

    :try_start_9
    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 404432
    :catchall_2
    move-exception v3

    move-object v13, v3

    move-object v3, v2

    move-object v2, v13

    :goto_2
    if-eqz v6, :cond_4

    if-eqz v3, :cond_6

    :try_start_a
    invoke-virtual {v6}, LX/2Oz;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_3

    :cond_4
    :goto_3
    throw v2

    .line 404433
    :catch_2
    move-exception v2

    :try_start_b
    invoke-static {v4, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 404434
    :catchall_3
    move-exception v2

    move-object v3, v5

    goto :goto_2

    .line 404435
    :cond_5
    invoke-virtual {v2}, LX/2gJ;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto :goto_1

    .line 404436
    :catch_3
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_6
    invoke-virtual {v6}, LX/2Oz;->close()V

    goto :goto_3

    .line 404437
    :catchall_4
    move-exception v3

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 403733
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403734
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403735
    move-object v2, v0

    .line 403736
    const/4 v1, 0x0

    .line 403737
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 403738
    sget-object v3, LX/2P5;->b:LX/0U1;

    .line 403739
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 403740
    iget-wide v4, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 403741
    sget-object v3, LX/2P5;->l:LX/0U1;

    .line 403742
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 403743
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 403744
    sget-object v3, LX/2P5;->e:LX/0U1;

    .line 403745
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 403746
    sget-object v4, LX/Doq;->a:LX/Doq;

    move-object v4, v4

    .line 403747
    invoke-virtual {v4}, LX/Doq;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 403748
    invoke-direct {p0, p1, v0}, LX/2Ox;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 403749
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    .line 403750
    :cond_0
    return-void

    .line 403751
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403752
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 403810
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403811
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403812
    move-object v4, v0

    .line 403813
    :try_start_0
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 403814
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 403815
    invoke-static {v1, p1, p2, p3}, LX/2Ox;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 403816
    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 403817
    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 403818
    :cond_1
    return-void

    .line 403819
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 403820
    :catchall_0
    move-exception v2

    move-object v5, v2

    move-object v2, v1

    move-object v1, v5

    :goto_0
    if-eqz v0, :cond_2

    if-eqz v2, :cond_4

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_2
    :goto_1
    :try_start_5
    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 403821
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 403822
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_2
    if-eqz v4, :cond_3

    if-eqz v3, :cond_5

    :try_start_7
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :cond_3
    :goto_3
    throw v0

    .line 403823
    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 403824
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 403825
    :cond_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_1

    .line 403826
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_3

    .line 403827
    :catchall_3
    move-exception v1

    move-object v2, v3

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dop;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 403797
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403798
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403799
    move-object v2, v0

    .line 403800
    const/4 v1, 0x0

    .line 403801
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 403802
    sget-object v3, LX/2P5;->q:LX/0U1;

    .line 403803
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 403804
    invoke-virtual {p2}, LX/Dop;->getValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 403805
    invoke-static {p0, p1, v0}, LX/2Ox;->b(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 403806
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    .line 403807
    :cond_0
    return-void

    .line 403808
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403809
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/Integer;)V
    .locals 5
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 403784
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403785
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403786
    move-object v2, v0

    .line 403787
    const/4 v1, 0x0

    .line 403788
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 403789
    sget-object v3, LX/2P5;->m:LX/0U1;

    .line 403790
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 403791
    invoke-virtual {v0, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 403792
    invoke-static {p0, p1, v0}, LX/2Ox;->b(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 403793
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    .line 403794
    :cond_0
    return-void

    .line 403795
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403796
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi",
            "Recycle"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 403753
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403754
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403755
    move-object v4, v0

    .line 403756
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 403757
    invoke-static {p0, p1, p3, v1}, LX/2Ox;->a(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 403758
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, LX/Dpq;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 403759
    sget-object v0, LX/2P5;->a:LX/0U1;

    .line 403760
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 403761
    invoke-static {v0, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v5

    .line 403762
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 403763
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 403764
    const v7, -0x59a80a1e

    invoke-static {v6, v7}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 403765
    :try_start_2
    const-string v7, "threads"

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v7, v1, v8, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 403766
    if-nez v5, :cond_0

    .line 403767
    iget-wide v8, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v2, v8, v9, v1}, LX/2Ox;->a(Ljava/lang/String;JLandroid/content/ContentValues;)V

    .line 403768
    const-string v2, "threads"

    const/4 v5, 0x0

    const v7, 0x21c61671

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v6, v2, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, -0x52a9f7bd

    invoke-static {v1}, LX/03h;->a(I)V

    .line 403769
    :cond_0
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 403770
    const v1, -0x65884c09

    :try_start_3
    invoke-static {v6, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 403771
    if-eqz v0, :cond_1

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 403772
    :cond_1
    if-eqz v4, :cond_2

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 403773
    :cond_2
    return-void

    .line 403774
    :catchall_0
    move-exception v1

    const v2, -0x4df110c0

    :try_start_5
    invoke-static {v6, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 403775
    :catch_0
    move-exception v1

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 403776
    :catchall_1
    move-exception v2

    move-object v10, v2

    move-object v2, v1

    move-object v1, v10

    :goto_0
    if-eqz v0, :cond_3

    if-eqz v2, :cond_5

    :try_start_7
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :cond_3
    :goto_1
    :try_start_8
    throw v1
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 403777
    :catch_1
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 403778
    :catchall_2
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_2
    if-eqz v4, :cond_4

    if-eqz v3, :cond_6

    :try_start_a
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_3

    :cond_4
    :goto_3
    throw v0

    .line 403779
    :catch_2
    move-exception v0

    :try_start_b
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 403780
    :catchall_3
    move-exception v0

    goto :goto_2

    .line 403781
    :cond_5
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto :goto_1

    .line 403782
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_6
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_3

    .line 403783
    :catchall_4
    move-exception v1

    move-object v2, v3

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 403714
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403715
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403716
    move-object v2, v0

    .line 403717
    const/4 v1, 0x0

    .line 403718
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 403719
    sget-object v3, LX/2P5;->k:LX/0U1;

    .line 403720
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 403721
    invoke-direct {p0, v3, p1, p2, v0}, LX/2Ox;->a(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 403722
    sget-object v3, LX/2P5;->i:LX/0U1;

    .line 403723
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 403724
    invoke-direct {p0, v3, p1, p3, v0}, LX/2Ox;->a(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 403725
    sget-object v3, LX/2P5;->j:LX/0U1;

    .line 403726
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 403727
    invoke-virtual {v0, v3, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 403728
    invoke-static {p0, p1, v0}, LX/2Ox;->b(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 403729
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    .line 403730
    :cond_0
    return-void

    .line 403731
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403732
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Set;)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 403689
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403690
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403691
    move-object v4, v0

    .line 403692
    :try_start_0
    sget-object v0, LX/2P1;->b:LX/0U1;

    .line 403693
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 403694
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 403695
    sget-object v1, LX/2P1;->a:LX/0U1;

    .line 403696
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 403697
    invoke-static {v1, p2}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v1

    .line 403698
    const/4 v2, 0x2

    new-array v2, v2, [LX/0ux;

    const/4 v5, 0x0

    aput-object v0, v2, v5

    const/4 v0, 0x1

    aput-object v1, v2, v0

    invoke-static {v2}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v1

    .line 403699
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 403700
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 403701
    const-string v5, "messages"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v5, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 403702
    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 403703
    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 403704
    :cond_1
    return-void

    .line 403705
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 403706
    :catchall_0
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    :goto_0
    if-eqz v0, :cond_2

    if-eqz v2, :cond_4

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_2
    :goto_1
    :try_start_5
    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 403707
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 403708
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_2
    if-eqz v4, :cond_3

    if-eqz v3, :cond_5

    :try_start_7
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :cond_3
    :goto_3
    throw v0

    .line 403709
    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 403710
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 403711
    :cond_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_1

    .line 403712
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_3

    .line 403713
    :catchall_3
    move-exception v1

    move-object v2, v3

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Z)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 403675
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403676
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403677
    move-object v2, v0

    .line 403678
    const/4 v1, 0x0

    .line 403679
    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 403680
    sget-object v0, LX/2P5;->l:LX/0U1;

    .line 403681
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v4

    .line 403682
    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 403683
    invoke-static {p0, p1, v3}, LX/2Ox;->b(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 403684
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    .line 403685
    :cond_0
    return-void

    .line 403686
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 403687
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403688
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 403654
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403655
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403656
    move-object v4, v0

    .line 403657
    :try_start_0
    sget-object v0, LX/3GM;->a:LX/0U1;

    .line 403658
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 403659
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 403660
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 403661
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 403662
    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, p1, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 403663
    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 403664
    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 403665
    :cond_1
    return-void

    .line 403666
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 403667
    :catchall_0
    move-exception v2

    move-object v6, v2

    move-object v2, v1

    move-object v1, v6

    :goto_0
    if-eqz v0, :cond_2

    if-eqz v2, :cond_4

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_2
    :goto_1
    :try_start_5
    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 403668
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 403669
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_2
    if-eqz v4, :cond_3

    if-eqz v3, :cond_5

    :try_start_7
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :cond_3
    :goto_3
    throw v0

    .line 403670
    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 403671
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 403672
    :cond_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_1

    .line 403673
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_3

    .line 403674
    :catchall_3
    move-exception v1

    move-object v2, v3

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I[BJ)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 403624
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403625
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403626
    move-object v4, v0

    .line 403627
    if-eqz p3, :cond_2

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 403628
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 403629
    sget-object v0, LX/3GM;->a:LX/0U1;

    .line 403630
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 403631
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 403632
    sget-object v0, LX/3GM;->b:LX/0U1;

    .line 403633
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 403634
    invoke-static {p0, p3}, LX/2Ox;->a$redex0(LX/2Ox;[B)[B

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 403635
    sget-object v0, LX/3GM;->c:LX/0U1;

    .line 403636
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 403637
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 403638
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 403639
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 403640
    const/4 v5, 0x0

    const v6, -0x2f1d4a82

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {v2, p1, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, -0x9a0d059

    invoke-static {v1}, LX/03h;->a(I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 403641
    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 403642
    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 403643
    :cond_1
    return-void

    .line 403644
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 403645
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 403646
    :catchall_0
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v2, :cond_5

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_3
    :goto_2
    :try_start_5
    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 403647
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 403648
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_3
    if-eqz v4, :cond_4

    if-eqz v3, :cond_6

    :try_start_7
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :cond_4
    :goto_4
    throw v0

    .line 403649
    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 403650
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 403651
    :cond_5
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 403652
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_4

    .line 403653
    :catchall_3
    move-exception v1

    move-object v2, v3

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 403611
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403612
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403613
    move-object v2, v0

    .line 403614
    const/4 v1, 0x0

    .line 403615
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 403616
    sget-object v3, LX/2P1;->l:LX/0U1;

    .line 403617
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 403618
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 403619
    invoke-static {p0, p1, v0}, LX/2Ox;->a(LX/2Ox;Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 403620
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    .line 403621
    :cond_0
    return-void

    .line 403622
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403623
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/2uW;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 403853
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403854
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403855
    move-object v2, v0

    .line 403856
    const/4 v1, 0x0

    .line 403857
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 403858
    sget-object v3, LX/2P1;->i:LX/0U1;

    .line 403859
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 403860
    iget v4, p2, LX/2uW;->dbKeyValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 403861
    invoke-static {p0, p1, v0}, LX/2Ox;->a(LX/2Ox;Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 403862
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    .line 403863
    :cond_0
    return-void

    .line 403864
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403865
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/messaging/model/send/SendError;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 403866
    iget-object v1, p0, LX/2Ox;->f:LX/2Oz;

    .line 403867
    invoke-virtual {v1}, LX/2Oz;->b()V

    .line 403868
    move-object v4, v1

    .line 403869
    const/4 v1, 0x0

    .line 403870
    if-eqz p2, :cond_1

    move v3, v0

    :goto_0
    :try_start_0
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 403871
    iget-object v3, p2, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    sget-object v5, LX/6fP;->NONE:LX/6fP;

    if-eq v3, v5, :cond_2

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 403872
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 403873
    invoke-static {p2, v0}, LX/2Ox;->a(Lcom/facebook/messaging/model/send/SendError;Landroid/content/ContentValues;)V

    .line 403874
    sget-object v2, LX/2P1;->i:LX/0U1;

    .line 403875
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 403876
    sget-object v3, LX/2uW;->FAILED_SEND:LX/2uW;

    iget v3, v3, LX/2uW;->dbKeyValue:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 403877
    invoke-static {p0, p1, v0}, LX/2Ox;->a(LX/2Ox;Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 403878
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 403879
    :cond_0
    return-void

    :cond_1
    move v3, v2

    .line 403880
    goto :goto_0

    :cond_2
    move v0, v2

    .line 403881
    goto :goto_1

    .line 403882
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403883
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_2
    if-eqz v4, :cond_3

    if-eqz v1, :cond_4

    :try_start_2
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_3
    :goto_3
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_4
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/util/Collection;J)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "[B>;>;J)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 403884
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403885
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403886
    move-object v4, v0

    .line 403887
    if-eqz p2, :cond_2

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 403888
    new-instance v0, LX/Doh;

    invoke-direct {v0, p0, p3, p4}, LX/Doh;-><init>(LX/2Ox;J)V

    invoke-static {p2, v0}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v1

    .line 403889
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 403890
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 403891
    const v5, -0x6e1b5436

    invoke-static {v2, v5}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 403892
    :try_start_2
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentValues;

    .line 403893
    const/4 v6, 0x0

    const v7, -0x47320a6f

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v2, p1, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, 0x7532a66f

    invoke-static {v1}, LX/03h;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 403894
    :catchall_0
    move-exception v1

    const v5, 0x71aee557

    :try_start_3
    invoke-static {v2, v5}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 403895
    :catch_0
    move-exception v1

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 403896
    :catchall_1
    move-exception v2

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    :goto_2
    if-eqz v0, :cond_0

    if-eqz v2, :cond_6

    :try_start_5
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :cond_0
    :goto_3
    :try_start_6
    throw v1
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 403897
    :catch_1
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 403898
    :catchall_2
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_4
    if-eqz v4, :cond_1

    if-eqz v3, :cond_7

    :try_start_8
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    :cond_1
    :goto_5
    throw v0

    .line 403899
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 403900
    :cond_3
    :try_start_9
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 403901
    const v1, -0x3749f286

    :try_start_a
    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 403902
    if-eqz v0, :cond_4

    :try_start_b
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 403903
    :cond_4
    if-eqz v4, :cond_5

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 403904
    :cond_5
    return-void

    .line 403905
    :catch_2
    move-exception v0

    :try_start_c
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 403906
    :catchall_3
    move-exception v0

    goto :goto_4

    .line 403907
    :cond_6
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    goto :goto_3

    .line 403908
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_7
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_5

    .line 403909
    :catchall_4
    move-exception v1

    move-object v2, v3

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 403910
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403911
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403912
    move-object v4, v0

    .line 403913
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 403914
    sget-object v0, LX/3Rv;->b:LX/0U1;

    .line 403915
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 403916
    invoke-virtual {v1, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 403917
    sget-object v0, LX/3Rv;->a:LX/0U1;

    .line 403918
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 403919
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    .line 403920
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 403921
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 403922
    const v6, -0x63744f45

    invoke-static {v5, v6}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 403923
    :try_start_2
    const-string v6, "identity_keys"

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v6, v1, v7, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 403924
    if-nez v2, :cond_0

    .line 403925
    sget-object v2, LX/3Rv;->a:LX/0U1;

    .line 403926
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 403927
    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 403928
    const-string v2, "identity_keys"

    const/4 v6, 0x0

    const v7, -0x1e5853b6

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v5, v2, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, 0x1666f5db

    invoke-static {v1}, LX/03h;->a(I)V

    .line 403929
    :cond_0
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 403930
    const v1, 0x4badf9b5    # 2.2803306E7f

    :try_start_3
    invoke-static {v5, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 403931
    if-eqz v0, :cond_1

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 403932
    :cond_1
    if-eqz v4, :cond_2

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 403933
    :cond_2
    return-void

    .line 403934
    :catchall_0
    move-exception v1

    const v2, 0x34429f5

    :try_start_5
    invoke-static {v5, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 403935
    :catch_0
    move-exception v1

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 403936
    :catchall_1
    move-exception v2

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    :goto_0
    if-eqz v0, :cond_3

    if-eqz v2, :cond_5

    :try_start_7
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :cond_3
    :goto_1
    :try_start_8
    throw v1
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 403937
    :catch_1
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 403938
    :catchall_2
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_2
    if-eqz v4, :cond_4

    if-eqz v3, :cond_6

    :try_start_a
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_3

    :cond_4
    :goto_3
    throw v0

    .line 403939
    :catch_2
    move-exception v0

    :try_start_b
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 403940
    :catchall_3
    move-exception v0

    goto :goto_2

    .line 403941
    :cond_5
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto :goto_1

    .line 403942
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_6
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_3

    .line 403943
    :catchall_4
    move-exception v1

    move-object v2, v3

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 403944
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403945
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403946
    move-object v4, v0

    .line 403947
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 403948
    sget-object v0, LX/2P5;->l:LX/0U1;

    .line 403949
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 403950
    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 403951
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 403952
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 403953
    const-string v5, "threads"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v5, v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 403954
    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 403955
    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 403956
    :cond_1
    return-void

    .line 403957
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 403958
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 403959
    :catchall_0
    move-exception v2

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v2, :cond_5

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_3
    :goto_2
    :try_start_5
    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 403960
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 403961
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_3
    if-eqz v4, :cond_4

    if-eqz v3, :cond_6

    :try_start_7
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :cond_4
    :goto_4
    throw v0

    .line 403962
    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 403963
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 403964
    :cond_5
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 403965
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_4

    .line 403966
    :catchall_3
    move-exception v1

    move-object v2, v3

    goto :goto_1
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 403967
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403968
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403969
    move-object v4, v0

    .line 403970
    :try_start_0
    sget-object v0, LX/2P5;->a:LX/0U1;

    .line 403971
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 403972
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 403973
    iget-object v0, p0, LX/2Ox;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 403974
    :try_start_1
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 403975
    const v5, 0x57ea8909

    invoke-static {v2, v5}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 403976
    :try_start_2
    const-string v5, "threads"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 403977
    const-string v5, "messages"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v5, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 403978
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 403979
    const v1, -0x3356c908    # -8.8717248E7f

    :try_start_3
    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 403980
    if-eqz v0, :cond_0

    :try_start_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 403981
    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/2Oz;->close()V

    .line 403982
    :cond_1
    return-void

    .line 403983
    :catchall_0
    move-exception v1

    const v5, 0x3de99ce2

    :try_start_5
    invoke-static {v2, v5}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 403984
    :catch_0
    move-exception v1

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 403985
    :catchall_1
    move-exception v2

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    :goto_0
    if-eqz v0, :cond_2

    if-eqz v2, :cond_4

    :try_start_7
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :cond_2
    :goto_1
    :try_start_8
    throw v1
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 403986
    :catch_1
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 403987
    :catchall_2
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_2
    if-eqz v4, :cond_3

    if-eqz v3, :cond_5

    :try_start_a
    invoke-virtual {v4}, LX/2Oz;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_3

    :cond_3
    :goto_3
    throw v0

    .line 403988
    :catch_2
    move-exception v0

    :try_start_b
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 403989
    :catchall_3
    move-exception v0

    goto :goto_2

    .line 403990
    :cond_4
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto :goto_1

    .line 403991
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v4}, LX/2Oz;->close()V

    goto :goto_3

    .line 403992
    :catchall_4
    move-exception v1

    move-object v2, v3

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 403993
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 403994
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 403995
    move-object v2, v0

    .line 403996
    const/4 v1, 0x0

    .line 403997
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 403998
    sget-object v3, LX/2P5;->o:LX/0U1;

    .line 403999
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 404000
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404001
    invoke-static {p0, p1, v0}, LX/2Ox;->b(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 404002
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    .line 404003
    :cond_0
    return-void

    .line 404004
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404005
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;J)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 404006
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 404007
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 404008
    move-object v2, v0

    .line 404009
    const/4 v1, 0x0

    .line 404010
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 404011
    sget-object v3, LX/2P1;->f:LX/0U1;

    .line 404012
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 404013
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404014
    invoke-static {p0, p1, v0}, LX/2Ox;->a(LX/2Ox;Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 404015
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    .line 404016
    :cond_0
    return-void

    .line 404017
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404018
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;[B)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 404019
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 404020
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 404021
    move-object v2, v0

    .line 404022
    const/4 v1, 0x0

    .line 404023
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 404024
    sget-object v3, LX/2P1;->d:LX/0U1;

    .line 404025
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 404026
    invoke-virtual {v0, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 404027
    invoke-static {p0, p1, v0}, LX/2Ox;->a(LX/2Ox;Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 404028
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    .line 404029
    :cond_0
    return-void

    .line 404030
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404031
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 404032
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 404033
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 404034
    move-object v2, v0

    .line 404035
    :try_start_0
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 404036
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 404037
    sget-object v3, LX/2P5;->g:LX/0U1;

    .line 404038
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 404039
    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404040
    invoke-static {p0, p1, v0}, LX/2Ox;->b(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 404041
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    .line 404042
    :cond_0
    return-void

    .line 404043
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404044
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 404045
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 404046
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 404047
    move-object v2, v0

    .line 404048
    const/4 v1, 0x0

    .line 404049
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 404050
    sget-object v3, LX/2P5;->n:LX/0U1;

    .line 404051
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 404052
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404053
    invoke-static {p0, p1, v0}, LX/2Ox;->b(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 404054
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    .line 404055
    :cond_0
    return-void

    .line 404056
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404057
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 404058
    iget-object v0, p0, LX/2Ox;->f:LX/2Oz;

    .line 404059
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 404060
    move-object v2, v0

    .line 404061
    :try_start_0
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 404062
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 404063
    sget-object v3, LX/2P5;->g:LX/0U1;

    .line 404064
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 404065
    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404066
    sget-object v3, LX/2P5;->c:LX/0U1;

    .line 404067
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 404068
    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404069
    sget-object v3, LX/2P5;->q:LX/0U1;

    .line 404070
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 404071
    sget-object v4, LX/Dop;->NOT_STARTED:LX/Dop;

    invoke-virtual {v4}, LX/Dop;->getValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 404072
    invoke-static {p0, p1, v0}, LX/2Ox;->b(LX/2Ox;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 404073
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    .line 404074
    :cond_0
    return-void

    .line 404075
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404076
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method
