.class public LX/3NG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3Km;

.field private final b:LX/3MV;

.field public final c:Landroid/content/Context;

.field public final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/3Km;LX/3MV;LX/0ad;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 558796
    iput-object p1, p0, LX/3NG;->a:LX/3Km;

    .line 558797
    iput-object p2, p0, LX/3NG;->b:LX/3MV;

    .line 558798
    iput-object p3, p0, LX/3NG;->d:LX/0ad;

    .line 558799
    iput-object p4, p0, LX/3NG;->c:Landroid/content/Context;

    .line 558800
    return-void
.end method

.method public static a(LX/3NG;LX/5ZG;Z)Lcom/facebook/messaging/business/search/model/PlatformSearchData;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const v8, 0x49b7fc49

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 558801
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 558802
    new-instance v3, LX/Dd7;

    invoke-direct {v3}, LX/Dd7;-><init>()V

    .line 558803
    invoke-interface {p1}, LX/5ZE;->cY_()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    invoke-interface {p1}, LX/5ZE;->cZ_()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v4

    invoke-interface {p1}, LX/5ZE;->e()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v5

    invoke-static {v0, v4, v5}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquare;

    move-result-object v0

    .line 558804
    invoke-interface {p1}, LX/5ZE;->b()Ljava/lang/String;

    move-result-object v4

    .line 558805
    iput-object v4, v3, LX/Dd7;->a:Ljava/lang/String;

    .line 558806
    invoke-interface {p1}, LX/5ZE;->j()Ljava/lang/String;

    move-result-object v4

    .line 558807
    iput-object v4, v3, LX/Dd7;->b:Ljava/lang/String;

    .line 558808
    iput-object v0, v3, LX/Dd3;->b:Lcom/facebook/user/model/PicSquare;

    .line 558809
    invoke-static {p1}, LX/3NG;->b(LX/5ZD;)LX/4nY;

    move-result-object v0

    .line 558810
    iput-object v0, v3, LX/Dd7;->c:LX/4nY;

    .line 558811
    new-instance v0, Lcom/facebook/user/model/Name;

    invoke-interface {p1}, LX/5ZE;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 558812
    iput-object v0, v3, LX/Dd3;->a:Lcom/facebook/user/model/Name;

    .line 558813
    iput-boolean p2, v3, LX/Dd3;->c:Z

    .line 558814
    invoke-interface {p1}, LX/5ZE;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, LX/Dd2;->MESSENGER:LX/Dd2;

    .line 558815
    :goto_0
    iput-object v0, v3, LX/Dd3;->d:LX/Dd2;

    .line 558816
    invoke-interface {p1}, LX/5ZG;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 558817
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 558818
    if-eqz v5, :cond_6

    invoke-virtual {v4, v5, v1}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    if-eqz v0, :cond_8

    .line 558819
    invoke-virtual {v4, v5, v1}, LX/15i;->g(II)I

    move-result v0

    .line 558820
    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    .line 558821
    invoke-virtual {v4, v5, v1}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 558822
    iput-object v0, v3, LX/Dd7;->d:Ljava/lang/String;

    .line 558823
    :cond_0
    if-eqz v5, :cond_b

    .line 558824
    invoke-static {v4, v5, v2, v8}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 558825
    if-eqz v0, :cond_9

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    if-eqz v0, :cond_a

    move v0, v1

    :goto_4
    if-eqz v0, :cond_e

    .line 558826
    invoke-static {v4, v5, v2, v8}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 558827
    if-eqz v0, :cond_c

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_5
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v1

    :goto_6
    if-eqz v0, :cond_11

    .line 558828
    invoke-static {v4, v5, v2, v8}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_7
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget v6, v0, LX/1vs;->b:I

    .line 558829
    if-eqz v6, :cond_10

    move v0, v1

    :goto_8
    if-eqz v0, :cond_14

    .line 558830
    invoke-static {v4, v5, v2, v8}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_12

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_9
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v7, v0, LX/1vs;->b:I

    .line 558831
    invoke-virtual {v6, v7, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    move v0, v1

    :goto_a
    if-eqz v0, :cond_1

    .line 558832
    invoke-static {v4, v5, v2, v8}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_15

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_b
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    invoke-virtual {v4, v5, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 558833
    iput-object v0, v3, LX/Dd7;->e:Ljava/lang/String;

    .line 558834
    :cond_1
    invoke-interface {p1}, LX/5ZG;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_17

    .line 558835
    invoke-interface {p1}, LX/5ZG;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 558836
    const-class v5, Lcom/facebook/graphql/enums/GraphQLTimespanCategory;

    invoke-virtual {v4, v0, v2, v5, v9}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    if-eqz v0, :cond_16

    move v0, v1

    :goto_c
    if-eqz v0, :cond_2

    .line 558837
    invoke-interface {p1}, LX/5ZG;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 558838
    iget-object v5, p0, LX/3NG;->c:Landroid/content/Context;

    const-class v6, Lcom/facebook/graphql/enums/GraphQLTimespanCategory;

    invoke-virtual {v4, v0, v2, v6, v9}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTimespanCategory;

    invoke-static {v0}, LX/FQi;->fromGraphQL(Lcom/facebook/graphql/enums/GraphQLTimespanCategory;)LX/FQi;

    move-result-object v0

    .line 558839
    if-eqz v2, :cond_1a

    .line 558840
    const v4, 0x7f083256

    invoke-virtual {v5, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 558841
    :goto_d
    move-object v0, v4

    .line 558842
    iput-object v0, v3, LX/Dd7;->g:Ljava/lang/String;

    .line 558843
    :cond_2
    invoke-interface {p1}, LX/5ZG;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_19

    .line 558844
    invoke-interface {p1}, LX/5ZG;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 558845
    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_18

    :goto_e
    if-eqz v1, :cond_3

    .line 558846
    invoke-interface {p1}, LX/5ZG;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 558847
    iput-object v0, v3, LX/Dd7;->f:Ljava/lang/String;

    .line 558848
    :cond_3
    new-instance v0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;

    invoke-direct {v0, v3}, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;-><init>(LX/Dd7;)V

    move-object v0, v0

    .line 558849
    return-object v0

    .line 558850
    :cond_4
    sget-object v0, LX/Dd2;->FACEBOOK:LX/Dd2;

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    move v0, v2

    .line 558851
    goto/16 :goto_1

    :cond_6
    move v0, v2

    goto/16 :goto_1

    :cond_7
    move v0, v2

    goto/16 :goto_2

    :cond_8
    move v0, v2

    goto/16 :goto_2

    .line 558852
    :cond_9
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_3

    :cond_a
    move v0, v2

    goto/16 :goto_4

    :cond_b
    move v0, v2

    goto/16 :goto_4

    :cond_c
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_5

    :cond_d
    move v0, v2

    goto/16 :goto_6

    :cond_e
    move v0, v2

    goto/16 :goto_6

    .line 558853
    :cond_f
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_7

    :cond_10
    move v0, v2

    .line 558854
    goto/16 :goto_8

    :cond_11
    move v0, v2

    goto/16 :goto_8

    .line 558855
    :cond_12
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_9

    :cond_13
    move v0, v2

    .line 558856
    goto/16 :goto_a

    :cond_14
    move v0, v2

    goto/16 :goto_a

    .line 558857
    :cond_15
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_b

    :cond_16
    move v0, v2

    .line 558858
    goto/16 :goto_c

    :cond_17
    move v0, v2

    goto/16 :goto_c

    :cond_18
    move v1, v2

    .line 558859
    goto :goto_e

    :cond_19
    move v1, v2

    goto :goto_e

    .line 558860
    :cond_1a
    sget-object v4, LX/FQm;->a:[I

    invoke-virtual {v0}, LX/FQi;->ordinal()I

    move-result v6

    aget v4, v4, v6

    packed-switch v4, :pswitch_data_0

    .line 558861
    const-string v4, ""

    goto/16 :goto_d

    .line 558862
    :pswitch_0
    const v4, 0x7f083257

    invoke-virtual {v5, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_d

    .line 558863
    :pswitch_1
    const v4, 0x7f083258

    invoke-virtual {v5, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_d

    .line 558864
    :pswitch_2
    const v4, 0x7f083259

    invoke-virtual {v5, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_d

    .line 558865
    :pswitch_3
    const v4, 0x7f08325a

    invoke-virtual {v5, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_d

    .line 558866
    :pswitch_4
    const v4, 0x7f08325b

    invoke-virtual {v5, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_d

    .line 558867
    :pswitch_5
    const v4, 0x7f08325c

    invoke-virtual {v5, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_d

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static final a(LX/5ZD;)Lcom/facebook/user/model/User;
    .locals 4

    .prologue
    .line 558868
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 558869
    invoke-interface {p0}, LX/5ZD;->cY_()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    invoke-interface {p0}, LX/5ZD;->cZ_()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v1

    invoke-interface {p0}, LX/5ZD;->e()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquare;

    move-result-object v0

    .line 558870
    new-instance v1, LX/0XI;

    invoke-direct {v1}, LX/0XI;-><init>()V

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-interface {p0}, LX/5ZD;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v1

    new-instance v2, Lcom/facebook/user/model/Name;

    invoke-interface {p0}, LX/5ZD;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 558871
    iput-object v2, v1, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 558872
    move-object v1, v1

    .line 558873
    invoke-interface {p0}, LX/5ZD;->j()Ljava/lang/String;

    move-result-object v2

    .line 558874
    iput-object v2, v1, LX/0XI;->l:Ljava/lang/String;

    .line 558875
    move-object v1, v1

    .line 558876
    iput-object v0, v1, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 558877
    move-object v0, v1

    .line 558878
    const/4 v1, 0x1

    .line 558879
    iput-boolean v1, v0, LX/0XI;->A:Z

    .line 558880
    move-object v0, v0

    .line 558881
    invoke-interface {p0}, LX/5ZD;->c()Z

    move-result v1

    .line 558882
    iput-boolean v1, v0, LX/0XI;->z:Z

    .line 558883
    move-object v0, v0

    .line 558884
    const-string v1, "page"

    .line 558885
    iput-object v1, v0, LX/0XI;->y:Ljava/lang/String;

    .line 558886
    move-object v0, v0

    .line 558887
    invoke-static {p0}, LX/3NG;->b(LX/5ZD;)LX/4nY;

    move-result-object v1

    .line 558888
    iput-object v1, v0, LX/0XI;->B:LX/4nY;

    .line 558889
    move-object v0, v0

    .line 558890
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/5ZD;)LX/4nY;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 558891
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 558892
    invoke-interface {p0}, LX/5ZD;->n()Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 558893
    sget-object v0, LX/FDG;->a:[I

    invoke-interface {p0}, LX/5ZD;->n()Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 558894
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 558895
    :pswitch_0
    sget-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_AGENT:LX/4nY;

    goto :goto_0

    .line 558896
    :pswitch_1
    sget-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_BANK:LX/4nY;

    goto :goto_0

    .line 558897
    :pswitch_2
    sget-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_BUSINESS:LX/4nY;

    goto :goto_0

    .line 558898
    :pswitch_3
    sget-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_RIDE_SHARE:LX/4nY;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
