.class public LX/2Fb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>(Landroid/app/NotificationManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387105
    iput-object p1, p0, LX/2Fb;->a:Landroid/app/NotificationManager;

    .line 387106
    return-void
.end method


# virtual methods
.method public final a(I)Z
    .locals 2

    .prologue
    .line 387107
    const/4 v0, 0x0

    .line 387108
    :try_start_0
    iget-object v1, p0, LX/2Fb;->a:Landroid/app/NotificationManager;

    invoke-virtual {v1, p1}, Landroid/app/NotificationManager;->cancel(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 387109
    const/4 v0, 0x1

    .line 387110
    :goto_0
    return v0

    :catch_0
    goto :goto_0
.end method

.method public final a(ILandroid/app/Notification;)Z
    .locals 2

    .prologue
    .line 387111
    if-nez p2, :cond_0

    .line 387112
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "notification cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387113
    :cond_0
    const/4 v0, 0x0

    .line 387114
    :try_start_0
    iget-object v1, p0, LX/2Fb;->a:Landroid/app/NotificationManager;

    invoke-virtual {v1, p1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 387115
    const/4 v0, 0x1

    .line 387116
    :goto_0
    return v0

    :catch_0
    goto :goto_0
.end method
