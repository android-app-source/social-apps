.class public LX/3K1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3Jf;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3Jf;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/3Jf;

.field private final d:Ljava/lang/String;

.field public final e:I

.field public final f:I

.field public final g:F

.field public final h:F

.field public final i:F

.field public final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3Jt;",
            ">;"
        }
    .end annotation
.end field

.field public final k:[[[F

.field public final l:I

.field public final m:Landroid/graphics/Paint$Cap;

.field public final n:LX/3K1;

.field public final o:LX/3Jf;

.field public final p:LX/3kN;

.field public final q:Ljava/lang/String;

.field public final r:LX/3K2;


# direct methods
.method public constructor <init>(Ljava/lang/String;IIFFFLjava/util/List;[[[FILandroid/graphics/Paint$Cap;LX/3K1;Ljava/util/List;LX/3kN;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIFFF",
            "Ljava/util/List",
            "<",
            "LX/3Jt;",
            ">;[[[FI",
            "Landroid/graphics/Paint$Cap;",
            "LX/3K1;",
            "Ljava/util/List",
            "<",
            "LX/3Jf;",
            ">;",
            "LX/3kN;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 548219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 548220
    iput-object p1, p0, LX/3K1;->d:Ljava/lang/String;

    .line 548221
    iput p2, p0, LX/3K1;->e:I

    .line 548222
    iput p3, p0, LX/3K1;->f:I

    .line 548223
    iput p4, p0, LX/3K1;->g:F

    .line 548224
    iput p5, p0, LX/3K1;->h:F

    .line 548225
    iput p6, p0, LX/3K1;->i:F

    .line 548226
    invoke-static {p7}, LX/3Jh;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/3K1;->j:Ljava/util/List;

    .line 548227
    iget-object v1, p0, LX/3K1;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {p8, v1}, LX/3Je;->a([[[FI)Z

    move-result v1

    const-string v2, "timing_curves"

    invoke-static {p8, v1, v2}, LX/3Je;->a(Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[[F

    iput-object v1, p0, LX/3K1;->k:[[[F

    .line 548228
    iput p9, p0, LX/3K1;->l:I

    .line 548229
    iput-object p10, p0, LX/3K1;->m:Landroid/graphics/Paint$Cap;

    .line 548230
    iput-object p11, p0, LX/3K1;->n:LX/3K1;

    .line 548231
    sget-object v1, LX/3JT;->STROKE_WIDTH:LX/3JT;

    invoke-static {p12, v1}, LX/3Jm;->a(Ljava/util/List;LX/3JT;)LX/3Jf;

    move-result-object v1

    iput-object v1, p0, LX/3K1;->a:LX/3Jf;

    .line 548232
    sget-object v1, LX/3JT;->ANCHOR_POINT:LX/3JT;

    invoke-static {p12, v1}, LX/3Jm;->a(Ljava/util/List;LX/3JT;)LX/3Jf;

    move-result-object v1

    iput-object v1, p0, LX/3K1;->c:LX/3Jf;

    .line 548233
    sget-object v1, LX/3JT;->OPACITY:LX/3JT;

    invoke-static {p12, v1}, LX/3Jm;->a(Ljava/util/List;LX/3JT;)LX/3Jf;

    move-result-object v1

    iput-object v1, p0, LX/3K1;->o:LX/3Jf;

    .line 548234
    sget-object v1, LX/3Jf;->a:Ljava/util/Comparator;

    invoke-static {p12, v1}, LX/3Jh;->a(Ljava/util/List;Ljava/util/Comparator;)V

    .line 548235
    invoke-static {p12}, LX/3Jh;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/3K1;->b:Ljava/util/List;

    .line 548236
    move-object/from16 v0, p13

    iput-object v0, p0, LX/3K1;->p:LX/3kN;

    .line 548237
    move-object/from16 v0, p14

    iput-object v0, p0, LX/3K1;->q:Ljava/lang/String;

    .line 548238
    iget-object v1, p0, LX/3K1;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput-object v1, p0, LX/3K1;->r:LX/3K2;

    .line 548239
    return-void

    .line 548240
    :cond_0
    invoke-static {p0}, LX/3K2;->a(LX/3K1;)LX/3K2;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;F)V
    .locals 4

    .prologue
    .line 548241
    if-nez p1, :cond_1

    .line 548242
    :cond_0
    return-void

    .line 548243
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    .line 548244
    iget-object v0, p0, LX/3K1;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 548245
    iget-object v0, p0, LX/3K1;->c:LX/3Jf;

    if-eqz v0, :cond_2

    .line 548246
    iget-object v0, p0, LX/3K1;->c:LX/3Jf;

    .line 548247
    iget-object v1, v0, LX/3Jf;->f:LX/3Jj;

    move-object v0, v1

    .line 548248
    invoke-virtual {v0, p2, p1}, LX/3Jj;->a(FLjava/lang/Object;)V

    .line 548249
    :cond_2
    const/4 v0, 0x0

    iget-object v1, p0, LX/3K1;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 548250
    iget-object v0, p0, LX/3K1;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Jf;

    .line 548251
    iget-object v3, v0, LX/3Jf;->f:LX/3Jj;

    move-object v0, v3

    .line 548252
    invoke-virtual {v0, p2, p1}, LX/3Jj;->a(FLjava/lang/Object;)V

    .line 548253
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
