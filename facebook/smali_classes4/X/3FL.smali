.class public final LX/3FL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2qG;


# instance fields
.field public final synthetic a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)V
    .locals 0

    .prologue
    .line 539447
    iput-object p1, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 539448
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 539449
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    iget-object v1, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    iget-object v1, v1, LX/2q6;->m:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->K:J

    .line 539450
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    iget-boolean v0, v0, LX/2q6;->L:Z

    if-eqz v0, :cond_0

    .line 539451
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    iget-object v0, v0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    iget-object v1, v1, LX/2q6;->y:LX/04G;

    iget-object v2, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    iget-object v2, v2, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v3, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    iget-object v3, v3, LX/2q6;->w:LX/04D;

    iget-object v4, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    iget-object v4, v4, LX/2q6;->z:LX/04g;

    iget-object v4, v4, LX/04g;->value:Ljava/lang/String;

    iget-object v5, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/1C2;->a(LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;)LX/1C2;

    .line 539452
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->L:Z

    .line 539453
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    iget-object v0, v0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 539454
    iget-object v2, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    iget-object v2, v2, LX/2q6;->M:LX/04g;

    invoke-interface {v0, v2}, LX/2pf;->c(LX/04g;)V

    goto :goto_0

    .line 539455
    :cond_0
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    sget-object v1, LX/2qE;->STATE_UPDATED:LX/2qE;

    iput-object v1, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->V:LX/2qE;

    .line 539456
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 539457
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    invoke-virtual {v0}, LX/2q6;->y()V

    .line 539458
    return-void
.end method

.method public final a(LX/7KG;)V
    .locals 3

    .prologue
    .line 539459
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    const-string v1, "MediaPlayer::VideoSurfaceTarget::onSurfaceUnavailable"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539460
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    invoke-virtual {v0}, LX/2q6;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539461
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, LX/2q6;->c(LX/04g;)V

    .line 539462
    :cond_0
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->z()V

    .line 539463
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    sget-object v1, LX/2qk;->FROM_DESTROY_SURFACE:LX/2qk;

    invoke-virtual {v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(LX/2qk;)V

    .line 539464
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->I:Landroid/view/Surface;

    .line 539465
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    sget-object v1, LX/2qE;->STATE_DESTROYED:LX/2qE;

    iput-object v1, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->V:LX/2qE;

    .line 539466
    invoke-virtual {p1}, LX/7KG;->a()V

    .line 539467
    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 3

    .prologue
    .line 539468
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    const-string v1, "MediaPlayer::VideoSurfaceTarget::onSurfaceAvailable"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539469
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    iput-object p1, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->I:Landroid/view/Surface;

    .line 539470
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b(Landroid/view/Surface;)V

    .line 539471
    iget-object v0, p0, LX/3FL;->a:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    sget-object v1, LX/2qE;->STATE_CREATED:LX/2qE;

    iput-object v1, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->V:LX/2qE;

    .line 539472
    return-void
.end method
