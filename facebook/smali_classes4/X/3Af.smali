.class public LX/3Af;
.super LX/3Ag;
.source ""

# interfaces
.implements LX/3Ah;


# static fields
.field private static final c:[F


# instance fields
.field public d:LX/3if;

.field private e:F

.field private f:F

.field private g:I
    .annotation build Lcom/facebook/widget/bottomsheet/BottomSheetDialog$BottomSheetShowRatioType;
    .end annotation
.end field

.field private h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0wL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 525858
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, LX/3Af;->c:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x3f000000    # 0.5f
        0x3f400000    # 0.75f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/high16 v1, 0x20000

    .line 525847
    const v0, 0x7f0e02d4

    invoke-direct {p0, p1, v0}, LX/3Ag;-><init>(Landroid/content/Context;I)V

    .line 525848
    const/4 v0, 0x0

    iput v0, p0, LX/3Af;->g:I

    .line 525849
    invoke-virtual {p0}, LX/3Af;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 525850
    new-instance v0, LX/3if;

    invoke-virtual {p0}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3if;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3Af;->d:LX/3if;

    .line 525851
    iget-object v0, p0, LX/3Af;->d:LX/3if;

    new-instance v1, LX/3ip;

    invoke-direct {v1, p0}, LX/3ip;-><init>(LX/3Af;)V

    .line 525852
    iput-object v1, v0, LX/3if;->p:LX/3iq;

    .line 525853
    iget-object v0, p0, LX/3Af;->d:LX/3if;

    new-instance v1, LX/3ir;

    invoke-direct {v1, p0}, LX/3ir;-><init>(LX/3Af;)V

    .line 525854
    iput-object v1, v0, LX/3if;->q:LX/3ih;

    .line 525855
    iget-object v0, p0, LX/3Af;->d:LX/3if;

    invoke-virtual {p0, v0}, LX/3Af;->setContentView(Landroid/view/View;)V

    .line 525856
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v0

    check-cast v0, LX/0wL;

    iput-object v0, p0, LX/3Af;->i:LX/0wL;

    .line 525857
    return-void
.end method

.method private a(FZFF)V
    .locals 6

    .prologue
    .line 525828
    invoke-virtual {p0}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 525829
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 525830
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 525831
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 525832
    move-object v1, v1

    .line 525833
    iget v0, v1, Landroid/graphics/Point;->x:I

    iget v2, v1, Landroid/graphics/Point;->y:I

    if-le v0, v2, :cond_0

    iget v0, v1, Landroid/graphics/Point;->x:I

    .line 525834
    :goto_0
    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v3, v1, Landroid/graphics/Point;->y:I

    if-le v2, v3, :cond_1

    iget v1, v1, Landroid/graphics/Point;->y:I

    .line 525835
    :goto_1
    if-eqz p2, :cond_2

    .line 525836
    :goto_2
    invoke-static {p0}, LX/3Af;->d(LX/3Af;)I

    move-result v2

    sub-int/2addr v0, v2

    .line 525837
    invoke-static {p0}, LX/3Af;->d(LX/3Af;)I

    move-result v2

    sub-int/2addr v1, v2

    .line 525838
    sget-object v2, LX/3Af;->c:[F

    iget v3, p0, LX/3Af;->g:I

    aget v2, v2, v3

    .line 525839
    int-to-float v3, v0

    mul-float/2addr v3, v2

    sub-float/2addr v3, p3

    sub-float/2addr v3, p4

    div-float/2addr v3, p1

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 525840
    int-to-float v4, v1

    mul-float/2addr v4, v2

    sub-float/2addr v4, p3

    sub-float/2addr v4, p4

    div-float/2addr v4, p1

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 525841
    int-to-float v3, v3

    add-float/2addr v3, v2

    mul-float/2addr v3, p1

    add-float/2addr v3, p3

    add-float/2addr v3, p4

    int-to-float v0, v0

    div-float v0, v3, v0

    iput v0, p0, LX/3Af;->e:F

    .line 525842
    int-to-float v0, v4

    add-float/2addr v0, v2

    mul-float/2addr v0, p1

    add-float/2addr v0, p3

    add-float/2addr v0, p4

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, LX/3Af;->f:F

    .line 525843
    return-void

    .line 525844
    :cond_0
    iget v0, v1, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 525845
    :cond_1
    iget v1, v1, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 525846
    :cond_2
    const/4 p3, 0x0

    goto :goto_2
.end method

.method private a(IIZZFF)V
    .locals 2

    .prologue
    .line 525777
    invoke-virtual {p0}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz p3, :cond_0

    :goto_0
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-direct {p0, v0, p4, p5, p6}, LX/3Af;->a(FZFF)V

    .line 525778
    iget-object v0, p0, LX/3Af;->d:LX/3if;

    iget v1, p0, LX/3Af;->e:F

    .line 525779
    iput v1, v0, LX/3if;->d:F

    .line 525780
    iget-object v0, p0, LX/3Af;->d:LX/3if;

    iget v1, p0, LX/3Af;->f:F

    .line 525781
    iput v1, v0, LX/3if;->e:F

    .line 525782
    return-void

    :cond_0
    move p2, p1

    .line 525783
    goto :goto_0
.end method

.method public static synthetic a(LX/3Af;)V
    .locals 0

    .prologue
    .line 525827
    invoke-super {p0}, LX/3Ag;->dismiss()V

    return-void
.end method

.method public static synthetic b(LX/3Af;)V
    .locals 0

    .prologue
    .line 525826
    invoke-super {p0}, Landroid/app/Dialog;->cancel()V

    return-void
.end method

.method private static d(LX/3Af;)I
    .locals 5

    .prologue
    .line 525821
    const/4 v0, 0x0

    .line 525822
    invoke-virtual {p0}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "status_bar_height"

    const-string v3, "dimen"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 525823
    if-lez v1, :cond_0

    .line 525824
    invoke-virtual {p0}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 525825
    :cond_0
    return v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 525819
    invoke-virtual {p0}, LX/3Af;->show()V

    .line 525820
    return-void
.end method

.method public final a(I)V
    .locals 2
    .param p1    # I
        .annotation build Lcom/facebook/widget/bottomsheet/BottomSheetDialog$BottomSheetShowRatioType;
        .end annotation
    .end param

    .prologue
    .line 525811
    iput p1, p0, LX/3Af;->g:I

    .line 525812
    sget-object v0, LX/3Af;->c:[F

    iget v1, p0, LX/3Af;->g:I

    aget v0, v0, v1

    iput v0, p0, LX/3Af;->e:F

    .line 525813
    sget-object v0, LX/3Af;->c:[F

    iget v1, p0, LX/3Af;->g:I

    aget v0, v0, v1

    iput v0, p0, LX/3Af;->f:F

    .line 525814
    iget-object v0, p0, LX/3Af;->d:LX/3if;

    iget v1, p0, LX/3Af;->e:F

    .line 525815
    iput v1, v0, LX/3if;->d:F

    .line 525816
    iget-object v0, p0, LX/3Af;->d:LX/3if;

    iget v1, p0, LX/3Af;->f:F

    .line 525817
    iput v1, v0, LX/3if;->e:F

    .line 525818
    return-void
.end method

.method public final a(LX/1OM;)V
    .locals 7

    .prologue
    .line 525790
    iget-object v0, p0, LX/3Af;->d:LX/3if;

    invoke-virtual {v0, p1}, LX/3if;->setAdapter(LX/1OM;)V

    .line 525791
    instance-of v0, p1, LX/34c;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 525792
    check-cast v0, LX/34c;

    invoke-virtual {v0, p0}, LX/34c;->a(LX/3Ah;)V

    .line 525793
    :cond_0
    instance-of v0, p1, LX/34b;

    if-eqz v0, :cond_1

    .line 525794
    const v1, 0x7f0b1127

    const v2, 0x7f0b1126

    move-object v0, p1

    check-cast v0, LX/34b;

    .line 525795
    iget-boolean v3, v0, LX/34b;->d:Z

    move v3, v3

    .line 525796
    move-object v0, p1

    check-cast v0, LX/34b;

    invoke-virtual {v0}, LX/34b;->h()Z

    move-result v4

    move-object v0, p1

    check-cast v0, LX/34b;

    .line 525797
    iget-object v5, v0, LX/34b;->e:LX/6WD;

    sget-object v6, LX/6WD;->CUSTOM:LX/6WD;

    if-ne v5, v6, :cond_3

    .line 525798
    iget v5, v0, LX/34b;->h:F

    .line 525799
    :goto_0
    move v5, v5

    .line 525800
    const v0, 0x7f0b112c

    int-to-float v6, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LX/3Af;->a(IIZZFF)V

    .line 525801
    :cond_1
    instance-of v0, p1, LX/7TY;

    if-eqz v0, :cond_2

    .line 525802
    const v1, 0x7f0b0a45

    const v2, 0x7f0b0a44

    move-object v0, p1

    check-cast v0, LX/7TY;

    .line 525803
    iget-boolean v3, v0, LX/7TY;->c:Z

    move v3, v3

    .line 525804
    move-object v0, p1

    check-cast v0, LX/7TY;

    .line 525805
    iget-boolean v4, v0, LX/7TY;->d:Z

    move v4, v4

    .line 525806
    check-cast p1, LX/7TY;

    .line 525807
    iget-object v0, p1, LX/34c;->c:Landroid/content/Context;

    move-object v0, v0

    .line 525808
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0b0a46

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    move v5, v0

    .line 525809
    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LX/3Af;->a(IIZZFF)V

    .line 525810
    :cond_2
    return-void

    :cond_3
    iget-object v5, v0, LX/34b;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b1128

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 525784
    invoke-super {p0}, LX/3Ag;->show()V

    .line 525785
    iget-object v0, p0, LX/3Af;->h:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 525786
    iget-object v0, p0, LX/3Af;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 525787
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/3Af;->h:Ljava/lang/ref/WeakReference;

    .line 525788
    iget-object v0, p0, LX/3Af;->d:LX/3if;

    new-instance v1, Lcom/facebook/widget/bottomsheet/BottomSheetDialog$3;

    invoke-direct {v1, p0}, Lcom/facebook/widget/bottomsheet/BottomSheetDialog$3;-><init>(LX/3Af;)V

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, LX/3if;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 525789
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 525775
    invoke-virtual {p0}, LX/3Af;->dismiss()V

    .line 525776
    return-void
.end method

.method public final dismiss()V
    .locals 2

    .prologue
    .line 525771
    iget-object v0, p0, LX/3Af;->d:LX/3if;

    invoke-virtual {v0}, LX/3if;->a()V

    .line 525772
    iget-object v0, p0, LX/3Af;->h:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Af;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 525773
    iget-object v1, p0, LX/3Af;->i:LX/0wL;

    iget-object v0, p0, LX/3Af;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, LX/0wL;->b(Landroid/view/View;)V

    .line 525774
    :cond_0
    return-void
.end method

.method public final show()V
    .locals 1

    .prologue
    .line 525769
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3Af;->a(Landroid/view/View;)V

    .line 525770
    return-void
.end method
