.class public final LX/2j7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/2iz;


# direct methods
.method public constructor <init>(LX/2iz;)V
    .locals 0

    .prologue
    .line 452771
    iput-object p1, p0, LX/2j7;->a:LX/2iz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2jR;)V
    .locals 5

    .prologue
    .line 452744
    iget-object v0, p0, LX/2j7;->a:LX/2iz;

    .line 452745
    if-nez p1, :cond_9

    .line 452746
    const/4 v1, 0x0

    .line 452747
    :goto_0
    move-object v1, v1

    .line 452748
    if-nez v1, :cond_1

    .line 452749
    :cond_0
    :goto_1
    return-void

    .line 452750
    :cond_1
    iget-boolean v0, v1, LX/2jY;->p:Z

    move v0, v0

    .line 452751
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/2j7;->a:LX/2iz;

    iget-object v0, v0, LX/2iz;->d:LX/2j0;

    .line 452752
    :goto_2
    instance-of v2, p1, LX/2jS;

    if-eqz v2, :cond_3

    .line 452753
    check-cast p1, LX/2jS;

    invoke-interface {v0, v1, p1}, LX/2j1;->a(LX/2jY;LX/2jS;)V

    goto :goto_1

    .line 452754
    :cond_2
    iget-object v0, p0, LX/2j7;->a:LX/2iz;

    iget-object v0, v0, LX/2iz;->f:LX/2j6;

    goto :goto_2

    .line 452755
    :cond_3
    instance-of v2, p1, LX/2jU;

    if-eqz v2, :cond_4

    .line 452756
    invoke-interface {v0, v1}, LX/2j1;->a(LX/2jY;)V

    goto :goto_1

    .line 452757
    :cond_4
    instance-of v2, p1, LX/2jV;

    if-eqz v2, :cond_5

    .line 452758
    check-cast p1, LX/2jV;

    invoke-interface {v0, v1, p1}, LX/2j1;->a(LX/2jY;LX/2jV;)V

    goto :goto_1

    .line 452759
    :cond_5
    instance-of v2, p1, LX/2jT;

    if-eqz v2, :cond_6

    .line 452760
    :try_start_0
    check-cast p1, LX/2jT;

    invoke-interface {v0, v1, p1}, LX/2j1;->a(LX/2jY;LX/2jT;)V
    :try_end_0
    .catch Lorg/apache/http/MethodNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 452761
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 452762
    iget-object v0, p0, LX/2j7;->a:LX/2iz;

    iget-object v0, v0, LX/2iz;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v2, LX/2iz;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onInvalidCacheResponseEvent method is not supported by the callback:\n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/http/MethodNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 452763
    :cond_6
    instance-of v2, p1, LX/2jW;

    if-eqz v2, :cond_7

    .line 452764
    :try_start_1
    check-cast p1, LX/2jW;

    invoke-interface {v0, v1, p1}, LX/2j1;->a(LX/2jY;LX/2jW;)V
    :try_end_1
    .catch Lorg/apache/http/MethodNotSupportedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 452765
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 452766
    iget-object v0, p0, LX/2j7;->a:LX/2iz;

    iget-object v0, v0, LX/2iz;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v2, LX/2iz;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onReactionCacheResultEvent method is not supported by the callback:\n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/http/MethodNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 452767
    :cond_7
    instance-of v2, p1, LX/2jX;

    if-eqz v2, :cond_8

    .line 452768
    check-cast p1, LX/2jX;

    invoke-interface {v0, v1, p1}, LX/2j1;->a(LX/2jY;LX/2jX;)V

    goto/16 :goto_1

    .line 452769
    :cond_8
    instance-of v2, p1, LX/2jQ;

    if-eqz v2, :cond_0

    .line 452770
    check-cast p1, LX/2jQ;

    invoke-interface {v0, v1, p1}, LX/2j1;->a(LX/2jY;LX/2jQ;)V

    goto/16 :goto_1

    :cond_9
    invoke-interface {p1}, LX/2jR;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v1

    goto/16 :goto_0
.end method
