.class public LX/3LF;
.super LX/3LG;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/3L7;

.field private final c:LX/3LA;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<+",
            "LX/3Mi;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/3LB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field public k:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/3OQ;",
            "LX/Jib;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/3Mi;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 550229
    const-class v0, LX/3LF;

    sput-object v0, LX/3LF;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3L7;LX/3LA;LX/0Or;LX/3LB;)V
    .locals 1
    .param p4    # LX/3LB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3L7;",
            "LX/3LA;",
            "LX/0Or",
            "<+",
            "LX/3Mi;",
            ">;",
            "LX/3LB;",
            ")V"
        }
    .end annotation

    .prologue
    .line 550217
    invoke-direct {p0}, LX/3LG;-><init>()V

    .line 550218
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 550219
    iput-object v0, p0, LX/3LF;->f:LX/0Px;

    .line 550220
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 550221
    iput-object v0, p0, LX/3LF;->g:LX/0Px;

    .line 550222
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 550223
    iput-object v0, p0, LX/3LF;->h:LX/0Px;

    .line 550224
    iput-object p1, p0, LX/3LF;->b:LX/3L7;

    .line 550225
    iput-object p2, p0, LX/3LF;->c:LX/3LA;

    .line 550226
    iput-object p3, p0, LX/3LF;->d:LX/0Or;

    .line 550227
    iput-object p4, p0, LX/3LF;->e:LX/3LB;

    .line 550228
    return-void
.end method

.method private a(I)LX/3OQ;
    .locals 1

    .prologue
    .line 550214
    iget-object v0, p0, LX/3LF;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 550215
    sget-object v0, LX/Ji5;->f:LX/3OQ;

    .line 550216
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3LF;->g:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    goto :goto_0
.end method

.method private b(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 550188
    iget-boolean v0, p0, LX/3LF;->j:Z

    if-eqz v0, :cond_2

    .line 550189
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 550190
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 550191
    instance-of v1, v0, LX/3OO;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 550192
    check-cast v1, LX/3OO;

    .line 550193
    iget-object v5, v1, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v1, v5

    .line 550194
    iget-object v5, v1, Lcom/facebook/user/model/User;->b:LX/0XG;

    move-object v1, v5

    .line 550195
    sget-object v5, LX/0XG;->EMAIL:LX/0XG;

    if-eq v1, v5, :cond_0

    .line 550196
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 550197
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 550198
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    .line 550199
    :cond_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_5

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 550200
    instance-of v1, v0, LX/3OO;

    if-eqz v1, :cond_4

    move-object v1, v0

    check-cast v1, LX/3OO;

    .line 550201
    iget-object v4, v1, LX/3OO;->B:LX/DAf;

    move-object v1, v4

    .line 550202
    if-eqz v1, :cond_4

    .line 550203
    iget-object v1, p0, LX/3LF;->l:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    .line 550204
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/3LF;->l:Ljava/util/ArrayList;

    .line 550205
    :cond_3
    iget-object v1, p0, LX/3LF;->l:Ljava/util/ArrayList;

    check-cast v0, LX/3OO;

    .line 550206
    iget-object v4, v0, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v0, v4

    .line 550207
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 550208
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 550209
    :cond_5
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v1, p0, LX/3LF;->h:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3LF;->g:LX/0Px;

    .line 550210
    invoke-virtual {p0}, LX/3LF;->getCount()I

    move-result v0

    if-lez v0, :cond_6

    .line 550211
    const v0, -0x2e9bda47

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 550212
    :goto_2
    return-void

    .line 550213
    :cond_6
    const v0, 0x498a2011

    invoke-static {p0, v0}, LX/08p;->b(Landroid/widget/BaseAdapter;I)V

    goto :goto_2
.end method


# virtual methods
.method public final synthetic a()LX/333;
    .locals 1

    .prologue
    .line 550124
    invoke-virtual {p0}, LX/3LG;->d()LX/3Mi;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 550174
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v1, p0, LX/3LF;->h:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3LF;->f:LX/0Px;

    .line 550175
    iget-object v0, p0, LX/3LF;->f:LX/0Px;

    iput-object v0, p0, LX/3LF;->g:LX/0Px;

    .line 550176
    iget-boolean v0, p0, LX/3LF;->i:Z

    if-eqz v0, :cond_1

    .line 550177
    const/4 v0, 0x0

    .line 550178
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/3LF;->k:Ljava/util/HashMap;

    .line 550179
    const/4 v2, 0x0

    .line 550180
    iget-object v1, p0, LX/3LF;->f:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    move v3, v0

    move v1, v0

    :goto_0
    if-ge v3, v4, :cond_1

    iget-object v0, p0, LX/3LF;->f:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 550181
    instance-of p1, v0, LX/DAa;

    if-eqz p1, :cond_0

    .line 550182
    new-instance v2, LX/Jib;

    check-cast v0, LX/DAa;

    invoke-virtual {v0}, LX/DAa;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v0}, LX/Jib;-><init>(ILjava/lang/String;)V

    .line 550183
    add-int/lit8 v0, v1, 0x1

    move-object v1, v2

    .line 550184
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    move v1, v0

    goto :goto_0

    .line 550185
    :cond_0
    iget-object p1, p0, LX/3LF;->k:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    move-object v1, v2

    goto :goto_1

    .line 550186
    :cond_1
    const v0, -0x23506207

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 550187
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;LX/3Og;)V
    .locals 2

    .prologue
    .line 550165
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 550166
    sget-object v0, LX/3Os;->a:[I

    .line 550167
    iget-object v1, p2, LX/3Og;->a:LX/3Oh;

    move-object v1, v1

    .line 550168
    invoke-virtual {v1}, LX/3Oh;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 550169
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 550170
    invoke-direct {p0, v0}, LX/3LF;->b(LX/0Px;)V

    .line 550171
    :goto_0
    return-void

    .line 550172
    :pswitch_0
    invoke-virtual {p2}, LX/3Og;->e()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/3LF;->b(LX/0Px;)V

    goto :goto_0

    .line 550173
    :pswitch_1
    invoke-virtual {p0}, LX/3LG;->e()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 550164
    const/4 v0, 0x0

    return v0
.end method

.method public final d()LX/3Mi;
    .locals 4

    .prologue
    .line 550159
    iget-object v0, p0, LX/3LF;->m:LX/3Mi;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3LF;->d:LX/0Or;

    if-eqz v0, :cond_0

    .line 550160
    new-instance v1, LX/3Mh;

    iget-object v0, p0, LX/3LF;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Mi;

    new-instance v2, LX/3NX;

    invoke-direct {v2, p0}, LX/3NX;-><init>(LX/3LF;)V

    invoke-direct {v1, v0, v2}, LX/3Mh;-><init>(LX/3Mi;LX/3NY;)V

    iput-object v1, p0, LX/3LF;->m:LX/3Mi;

    .line 550161
    iget-object v0, p0, LX/3LF;->m:LX/3Mi;

    invoke-interface {v0, p0}, LX/3Mi;->a(LX/3LI;)V

    .line 550162
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/3LF;->l:Ljava/util/ArrayList;

    .line 550163
    iget-object v0, p0, LX/3LF;->m:LX/3Mi;

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 550154
    iget-object v0, p0, LX/3LF;->f:LX/0Px;

    iput-object v0, p0, LX/3LF;->g:LX/0Px;

    .line 550155
    invoke-virtual {p0}, LX/3LF;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 550156
    const v0, 0x6e290080

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 550157
    :goto_0
    return-void

    .line 550158
    :cond_0
    const v0, -0x5ee15d45

    invoke-static {p0, v0}, LX/08p;->b(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 550153
    iget-object v0, p0, LX/3LF;->m:LX/3Mi;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3LF;->m:LX/3Mi;

    invoke-interface {v0}, LX/333;->b()LX/3Mr;

    move-result-object v0

    sget-object v1, LX/3Mr;->FILTERING:LX/3Mr;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/3LF;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/3LF;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 550152
    invoke-direct {p0, p1}, LX/3LF;->a(I)LX/3OQ;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 550151
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 4

    .prologue
    .line 550146
    invoke-direct {p0, p1}, LX/3LF;->a(I)LX/3OQ;

    move-result-object v1

    .line 550147
    iget-object v0, p0, LX/3LF;->b:LX/3L7;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LX/3OR;->a(LX/3L9;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Nt;

    .line 550148
    if-eqz v0, :cond_0

    .line 550149
    invoke-virtual {v0}, LX/3Nt;->ordinal()I

    move-result v0

    return v0

    .line 550150
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown object type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 550141
    invoke-direct {p0, p1}, LX/3LF;->a(I)LX/3OQ;

    move-result-object v1

    .line 550142
    iget-object v0, p0, LX/3LF;->c:LX/3LA;

    invoke-interface {v1, v0, p2}, LX/3OR;->a(LX/3L9;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 550143
    if-eqz v0, :cond_0

    .line 550144
    return-object v0

    .line 550145
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown object type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 550140
    invoke-static {}, LX/3Nt;->values()[LX/3Nt;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 550125
    invoke-direct {p0, p1}, LX/3LF;->a(I)LX/3OQ;

    move-result-object v0

    .line 550126
    instance-of v2, v0, LX/DAa;

    if-nez v2, :cond_0

    instance-of v2, v0, LX/DAX;

    if-nez v2, :cond_0

    instance-of v2, v0, LX/3Ou;

    if-nez v2, :cond_0

    sget-object v2, LX/Ji5;->f:LX/3OQ;

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 550127
    :goto_0
    return v0

    .line 550128
    :cond_1
    instance-of v2, v0, LX/3OO;

    if-eqz v2, :cond_3

    .line 550129
    check-cast v0, LX/3OO;

    .line 550130
    iget-boolean v2, v0, LX/3OO;->w:Z

    move v2, v2

    .line 550131
    if-eqz v2, :cond_2

    .line 550132
    iget-boolean v2, v0, LX/3OO;->E:Z

    move v2, v2

    .line 550133
    if-nez v2, :cond_3

    .line 550134
    iget-boolean v2, v0, LX/3OO;->C:Z

    move v2, v2

    .line 550135
    if-nez v2, :cond_2

    .line 550136
    iget-boolean v2, v0, LX/3OO;->D:Z

    move v0, v2

    .line 550137
    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    .line 550138
    goto :goto_0

    .line 550139
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
