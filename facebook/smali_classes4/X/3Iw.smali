.class public final LX/3Iw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/394;


# instance fields
.field private final a:LX/3FN;

.field private final b:LX/198;

.field private final c:LX/1C2;

.field private final d:LX/1Yd;

.field private final e:LX/0bH;

.field private f:LX/3J0;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/CDf;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/1YQ;

.field public i:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/player/RichVideoPlayer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1YQ;LX/3FN;LX/198;LX/1C2;LX/1Yd;LX/0bH;LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1YQ;",
            "LX/3FN;",
            "LX/198;",
            "LX/1C2;",
            "LX/1Yd;",
            "LX/0bH;",
            "LX/0am",
            "<",
            "LX/CDf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 547316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547317
    iput-object p2, p0, LX/3Iw;->a:LX/3FN;

    .line 547318
    iput-object p3, p0, LX/3Iw;->b:LX/198;

    .line 547319
    iput-object p4, p0, LX/3Iw;->c:LX/1C2;

    .line 547320
    iput-object p5, p0, LX/3Iw;->d:LX/1Yd;

    .line 547321
    iput-object p6, p0, LX/3Iw;->e:LX/0bH;

    .line 547322
    iput-object p7, p0, LX/3Iw;->g:LX/0am;

    .line 547323
    iput-object p1, p0, LX/3Iw;->h:LX/1YQ;

    .line 547324
    return-void
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 10

    .prologue
    .line 547325
    iget-object v0, p0, LX/3Iw;->d:LX/1Yd;

    iget-object v1, p0, LX/3Iw;->a:LX/3FN;

    iget-object v1, v1, LX/3FN;->o:LX/395;

    invoke-virtual {v0, v1}, LX/1Yd;->a(LX/395;)V

    .line 547326
    iget-object v0, p0, LX/3Iw;->i:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 547327
    if-nez v0, :cond_1

    .line 547328
    :cond_0
    :goto_0
    return-void

    .line 547329
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setBackgroundResource(I)V

    .line 547330
    iget-object v0, p0, LX/3Iw;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->s:LX/0hE;

    invoke-interface {v0}, LX/0hE;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3Iw;->c:LX/1C2;

    .line 547331
    iget-boolean v1, v0, LX/1C2;->B:Z

    move v0, v1

    .line 547332
    if-nez v0, :cond_2

    iget-object v0, p0, LX/3Iw;->c:LX/1C2;

    .line 547333
    iget-boolean v1, v0, LX/1C2;->C:Z

    move v0, v1

    .line 547334
    if-eqz v0, :cond_3

    .line 547335
    :cond_2
    iget-object v0, p0, LX/3Iw;->c:LX/1C2;

    iget-object v1, p0, LX/3Iw;->a:LX/3FN;

    iget-object v1, v1, LX/3FN;->o:LX/395;

    invoke-virtual {v1}, LX/395;->s()LX/162;

    move-result-object v1

    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    iget-object v3, p1, LX/04g;->value:Ljava/lang/String;

    iget-object v4, p0, LX/3Iw;->a:LX/3FN;

    iget-object v4, v4, LX/3FN;->o:LX/395;

    invoke-virtual {v4}, LX/395;->p()I

    move-result v4

    iget-object v5, p0, LX/3Iw;->a:LX/3FN;

    iget-object v5, v5, LX/3FN;->o:LX/395;

    invoke-virtual {v5}, LX/395;->y()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/3Iw;->a:LX/3FN;

    iget-object v6, v6, LX/3FN;->o:LX/395;

    .line 547336
    iget-object v7, v6, LX/395;->d:Lcom/facebook/video/analytics/VideoPlayerInfo;

    move-object v6, v7

    .line 547337
    iget-object v7, v6, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    move-object v6, v7

    .line 547338
    new-instance v7, LX/AnR;

    iget-object v8, p0, LX/3Iw;->a:LX/3FN;

    iget-object v8, v8, LX/3FN;->o:LX/395;

    iget-object v9, p0, LX/3Iw;->a:LX/3FN;

    iget-object v9, v9, LX/3FN;->h:LX/2pa;

    iget-object v9, v9, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-direct {v7, v8, v9}, LX/AnR;-><init>(LX/395;Lcom/facebook/video/engine/VideoPlayerParams;)V

    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 547339
    :cond_3
    iget-object v0, p0, LX/3Iw;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 547340
    if-eqz v0, :cond_0

    .line 547341
    invoke-static {v0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 547342
    iget-object v2, p0, LX/3Iw;->e:LX/0bH;

    new-instance v3, LX/1Ze;

    .line 547343
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 547344
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v3, v4, v0}, LX/1Ze;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/04g;LX/7Jv;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 547279
    iget-object v0, p0, LX/3Iw;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->s:LX/0hE;

    invoke-interface {v0}, LX/0hE;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Iw;->c:LX/1C2;

    .line 547280
    iget-boolean v1, v0, LX/1C2;->B:Z

    move v0, v1

    .line 547281
    if-nez v0, :cond_0

    iget-object v0, p0, LX/3Iw;->c:LX/1C2;

    .line 547282
    iget-boolean v1, v0, LX/1C2;->C:Z

    move v0, v1

    .line 547283
    if-eqz v0, :cond_1

    .line 547284
    :cond_0
    iget-object v0, p0, LX/3Iw;->c:LX/1C2;

    iget-object v1, p0, LX/3Iw;->a:LX/3FN;

    iget-object v1, v1, LX/3FN;->o:LX/395;

    invoke-virtual {v1}, LX/395;->s()LX/162;

    move-result-object v1

    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    iget-object v3, p1, LX/04g;->value:Ljava/lang/String;

    iget v4, p2, LX/7Jv;->c:I

    iget-object v5, p0, LX/3Iw;->a:LX/3FN;

    iget-object v5, v5, LX/3FN;->o:LX/395;

    invoke-virtual {v5}, LX/395;->y()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/3Iw;->a:LX/3FN;

    iget-object v6, v6, LX/3FN;->o:LX/395;

    .line 547285
    iget-object v7, v6, LX/395;->d:Lcom/facebook/video/analytics/VideoPlayerInfo;

    move-object v6, v7

    .line 547286
    iget-object v7, v6, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    move-object v6, v7

    .line 547287
    new-instance v7, LX/AnR;

    iget-object v8, p0, LX/3Iw;->a:LX/3FN;

    iget-object v8, v8, LX/3FN;->o:LX/395;

    iget-object v9, p0, LX/3Iw;->a:LX/3FN;

    iget-object v9, v9, LX/3FN;->h:LX/2pa;

    iget-object v9, v9, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-direct {v7, v8, v9}, LX/AnR;-><init>(LX/395;Lcom/facebook/video/engine/VideoPlayerParams;)V

    invoke-virtual/range {v0 .. v7}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 547288
    :cond_1
    iget-object v0, p0, LX/3Iw;->a:LX/3FN;

    iput-boolean v10, v0, LX/3FN;->r:Z

    .line 547289
    iget-object v0, p0, LX/3Iw;->i:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 547290
    if-nez v0, :cond_2

    .line 547291
    :goto_0
    return-void

    .line 547292
    :cond_2
    iget-object v1, p0, LX/3Iw;->g:LX/0am;

    invoke-static {v1}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/0am;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setBackgroundResource(I)V

    .line 547293
    iget-object v1, p0, LX/3Iw;->a:LX/3FN;

    iget-object v1, v1, LX/3FN;->i:LX/2oO;

    iget-boolean v2, p2, LX/7Jv;->b:Z

    iget-boolean v3, p2, LX/7Jv;->a:Z

    invoke-virtual {v1, v2, v3}, LX/2oO;->a(ZZ)V

    .line 547294
    iget-object v1, p0, LX/3Iw;->a:LX/3FN;

    iget-object v1, v1, LX/3FN;->j:LX/2oL;

    iget v2, p2, LX/7Jv;->c:I

    invoke-virtual {v1, v2}, LX/2oL;->a(I)V

    .line 547295
    iget-boolean v1, p2, LX/7Jv;->b:Z

    if-nez v1, :cond_7

    iget-boolean v1, p2, LX/7Jv;->a:Z

    if-nez v1, :cond_7

    iget v1, p2, LX/7Jv;->c:I

    if-lez v1, :cond_7

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 547296
    if-eqz v1, :cond_6

    .line 547297
    iget-object v1, p0, LX/3Iw;->a:LX/3FN;

    iget-object v1, v1, LX/3FN;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 547298
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    move-object v1, v2

    .line 547299
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 547300
    iget-object v1, p0, LX/3Iw;->a:LX/3FN;

    iget-object v1, v1, LX/3FN;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 547301
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    move-object v1, v2

    .line 547302
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setChannelEligibility(LX/04H;)V

    .line 547303
    iget-object v1, p0, LX/3Iw;->a:LX/3FN;

    iget-object v2, p0, LX/3Iw;->b:LX/198;

    sget-object v3, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    new-instance v4, LX/7K4;

    iget v5, p2, LX/7Jv;->c:I

    iget v6, p2, LX/7Jv;->d:I

    invoke-direct {v4, v5, v6}, LX/7K4;-><init>(II)V

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/3FN;LX/198;LX/04g;LX/7K4;)V

    .line 547304
    :cond_3
    :goto_2
    iget-boolean v0, p2, LX/7Jv;->b:Z

    if-eqz v0, :cond_4

    .line 547305
    iget-object v0, p0, LX/3Iw;->a:LX/3FN;

    sget-object v1, LX/04g;->UNSET:LX/04g;

    invoke-static {v0, v1}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/3FN;LX/04g;)V

    .line 547306
    :cond_4
    iget-object v0, p0, LX/3Iw;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->j:LX/2oL;

    iget-object v1, p2, LX/7Jv;->i:LX/7DJ;

    .line 547307
    iput-object v1, v0, LX/2oL;->i:LX/7DJ;

    .line 547308
    iget-object v0, p0, LX/3Iw;->f:LX/3J0;

    if-eqz v0, :cond_5

    .line 547309
    iget-object v0, p0, LX/3Iw;->f:LX/3J0;

    invoke-interface {v0, p2}, LX/3J0;->a(LX/7Jv;)V

    .line 547310
    :cond_5
    iget-object v0, p0, LX/3Iw;->h:LX/1YQ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v10}, LX/1YQ;->a(LX/2oO;Z)V

    .line 547311
    iget-object v0, p0, LX/3Iw;->d:LX/1Yd;

    invoke-virtual {v0}, LX/1Yd;->a()V

    goto :goto_0

    .line 547312
    :cond_6
    iget-boolean v0, p2, LX/7Jv;->b:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3Iw;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2v7;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 547313
    iget-object v0, p0, LX/3Iw;->a:LX/3FN;

    iget-object v0, v0, LX/3FN;->j:LX/2oL;

    const/4 v1, 0x1

    .line 547314
    iput-boolean v1, v0, LX/2oL;->a:Z

    .line 547315
    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(LX/3J0;)V
    .locals 1

    .prologue
    .line 547276
    const-string v0, "listener already set"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 547277
    iput-object p1, p0, LX/3Iw;->f:LX/3J0;

    .line 547278
    return-void
.end method
