.class public final enum LX/2Jh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Jh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Jh;

.field public static final enum BACKGROUND:LX/2Jh;

.field public static final enum FOREGROUND:LX/2Jh;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 393164
    new-instance v0, LX/2Jh;

    const-string v1, "FOREGROUND"

    invoke-direct {v0, v1, v2}, LX/2Jh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Jh;->FOREGROUND:LX/2Jh;

    .line 393165
    new-instance v0, LX/2Jh;

    const-string v1, "BACKGROUND"

    invoke-direct {v0, v1, v3}, LX/2Jh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Jh;->BACKGROUND:LX/2Jh;

    .line 393166
    const/4 v0, 0x2

    new-array v0, v0, [LX/2Jh;

    sget-object v1, LX/2Jh;->FOREGROUND:LX/2Jh;

    aput-object v1, v0, v2

    sget-object v1, LX/2Jh;->BACKGROUND:LX/2Jh;

    aput-object v1, v0, v3

    sput-object v0, LX/2Jh;->$VALUES:[LX/2Jh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 393163
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Jh;
    .locals 1

    .prologue
    .line 393162
    const-class v0, LX/2Jh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Jh;

    return-object v0
.end method

.method public static values()[LX/2Jh;
    .locals 1

    .prologue
    .line 393161
    sget-object v0, LX/2Jh;->$VALUES:[LX/2Jh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Jh;

    return-object v0
.end method
