.class public final LX/26m;
.super LX/26n;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/26n",
        "<",
        "LX/0bE;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0cI;


# direct methods
.method public constructor <init>(LX/0cI;)V
    .locals 0

    .prologue
    .line 372322
    iput-object p1, p0, LX/26m;->a:LX/0cI;

    invoke-direct {p0}, LX/26n;-><init>()V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 372315
    iget-object v0, p0, LX/26m;->a:LX/0cI;

    iget-object v0, v0, LX/0cI;->n:LX/0bD;

    iget-object v1, p0, LX/26m;->a:LX/0cI;

    iget-object v1, v1, LX/0cI;->q:LX/26n;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 372316
    iget-object v0, p0, LX/26m;->a:LX/0cI;

    invoke-static {v0}, LX/0cI;->d(LX/0cI;)Ljava/lang/String;

    move-result-object v0

    .line 372317
    if-nez v0, :cond_0

    .line 372318
    iget-object v0, p0, LX/26m;->a:LX/0cI;

    iget-object v0, v0, LX/0cI;->e:LX/03V;

    const-class v1, LX/0cJ;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "userId is still null after AuthLoggedInEvent fired"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 372319
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/0bE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 372323
    const-class v0, LX/0bE;

    return-object v0
.end method

.method public final synthetic a(LX/0bF;)V
    .locals 0

    .prologue
    .line 372321
    invoke-direct {p0}, LX/26m;->b()V

    return-void
.end method

.method public final bridge synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 372320
    invoke-direct {p0}, LX/26m;->b()V

    return-void
.end method
