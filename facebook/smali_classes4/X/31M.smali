.class public final enum LX/31M;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/31M;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/31M;

.field public static final enum DOWN:LX/31M;

.field public static final enum LEFT:LX/31M;

.field public static final enum RIGHT:LX/31M;

.field public static final enum UP:LX/31M;


# instance fields
.field private mFlag:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 486858
    new-instance v0, LX/31M;

    const-string v1, "UP"

    invoke-direct {v0, v1, v5, v3}, LX/31M;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/31M;->UP:LX/31M;

    .line 486859
    new-instance v0, LX/31M;

    const-string v1, "DOWN"

    invoke-direct {v0, v1, v3, v4}, LX/31M;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/31M;->DOWN:LX/31M;

    .line 486860
    new-instance v0, LX/31M;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v4, v7}, LX/31M;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/31M;->LEFT:LX/31M;

    .line 486861
    new-instance v0, LX/31M;

    const-string v1, "RIGHT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v6, v2}, LX/31M;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/31M;->RIGHT:LX/31M;

    .line 486862
    new-array v0, v7, [LX/31M;

    sget-object v1, LX/31M;->UP:LX/31M;

    aput-object v1, v0, v5

    sget-object v1, LX/31M;->DOWN:LX/31M;

    aput-object v1, v0, v3

    sget-object v1, LX/31M;->LEFT:LX/31M;

    aput-object v1, v0, v4

    sget-object v1, LX/31M;->RIGHT:LX/31M;

    aput-object v1, v0, v6

    sput-object v0, LX/31M;->$VALUES:[LX/31M;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 486863
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 486864
    iput p3, p0, LX/31M;->mFlag:I

    .line 486865
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/31M;
    .locals 1

    .prologue
    .line 486866
    const-class v0, LX/31M;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/31M;

    return-object v0
.end method

.method public static values()[LX/31M;
    .locals 1

    .prologue
    .line 486867
    sget-object v0, LX/31M;->$VALUES:[LX/31M;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/31M;

    return-object v0
.end method


# virtual methods
.method public final flag()I
    .locals 1

    .prologue
    .line 486868
    iget v0, p0, LX/31M;->mFlag:I

    return v0
.end method

.method public final isSetInFlags(I)Z
    .locals 2

    .prologue
    .line 486869
    iget v0, p0, LX/31M;->mFlag:I

    and-int/2addr v0, p1

    iget v1, p0, LX/31M;->mFlag:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isXAxis()Z
    .locals 1

    .prologue
    .line 486870
    sget-object v0, LX/31M;->LEFT:LX/31M;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/31M;->RIGHT:LX/31M;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isYAxis()Z
    .locals 1

    .prologue
    .line 486871
    sget-object v0, LX/31M;->UP:LX/31M;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/31M;->DOWN:LX/31M;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
