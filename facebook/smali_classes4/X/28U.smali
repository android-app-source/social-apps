.class public LX/28U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/41t;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374134
    return-void
.end method

.method public static a(LX/0QB;)LX/28U;
    .locals 1

    .prologue
    .line 374135
    new-instance v0, LX/28U;

    invoke-direct {v0}, LX/28U;-><init>()V

    .line 374136
    move-object v0, v0

    .line 374137
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 374138
    check-cast p1, LX/41t;

    .line 374139
    const/4 v0, 0x1

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 374140
    iget-object v1, p1, LX/41t;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 374141
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "reason"

    iget-object v3, p1, LX/41t;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374142
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "retain_for_dbl"

    iget-boolean v3, p1, LX/41t;->b:Z

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374143
    :cond_0
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    sget-object v2, LX/11I;->LOGOUT:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    .line 374144
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 374145
    move-object v1, v1

    .line 374146
    const-string v2, "POST"

    .line 374147
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 374148
    move-object v1, v1

    .line 374149
    const-string v2, "method/auth.expireSession"

    .line 374150
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 374151
    move-object v1, v1

    .line 374152
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 374153
    move-object v0, v1

    .line 374154
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 374155
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 374156
    move-object v0, v0

    .line 374157
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 374158
    const/4 v0, 0x0

    return-object v0
.end method
