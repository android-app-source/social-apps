.class public LX/2hz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:[LX/63j;

.field private b:[I

.field private c:[Landroid/view/MotionEvent$PointerCoords;

.field private d:LX/63i;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 450944
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/2hz;-><init>(Z)V

    .line 450945
    return-void
.end method

.method private constructor <init>(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x6

    .line 450946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450947
    new-array v0, v1, [LX/63j;

    iput-object v0, p0, LX/2hz;->a:[LX/63j;

    .line 450948
    new-array v0, v1, [I

    iput-object v0, p0, LX/2hz;->b:[I

    .line 450949
    new-array v0, v1, [Landroid/view/MotionEvent$PointerCoords;

    iput-object v0, p0, LX/2hz;->c:[Landroid/view/MotionEvent$PointerCoords;

    .line 450950
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-gt v0, v1, :cond_0

    .line 450951
    iget-object v1, p0, LX/2hz;->a:[LX/63j;

    new-instance v2, LX/63j;

    invoke-direct {v2, p1}, LX/63j;-><init>(Z)V

    aput-object v2, v1, v0

    .line 450952
    iget-object v1, p0, LX/2hz;->c:[Landroid/view/MotionEvent$PointerCoords;

    new-instance v2, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v2}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v2, v1, v0

    .line 450953
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 450954
    :cond_0
    new-instance v0, LX/63i;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v4, v4, v2, v3}, LX/63i;-><init>(FFJ)V

    iput-object v0, p0, LX/2hz;->d:LX/63i;

    .line 450955
    return-void
.end method

.method private static final a(LX/2hz;I)LX/63j;
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 450956
    if-ge p1, v1, :cond_0

    .line 450957
    iget-object v0, p0, LX/2hz;->a:[LX/63j;

    aget-object v0, v0, p1

    .line 450958
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2hz;->a:[LX/63j;

    aget-object v0, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;IJ)Landroid/view/MotionEvent;
    .locals 17

    .prologue
    .line 450959
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-lt v2, v3, :cond_1

    .line 450960
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x5

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 450961
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v7, :cond_0

    .line 450962
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    .line 450963
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2hz;->b:[I

    aput v3, v4, v2

    .line 450964
    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/2hz;->a(LX/2hz;I)LX/63j;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2hz;->d:LX/63i;

    move-wide/from16 v0, p3

    invoke-virtual {v3, v0, v1, v4}, LX/63j;->a(JLX/63i;)V

    .line 450965
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2hz;->c:[Landroid/view/MotionEvent$PointerCoords;

    aget-object v3, v3, v2

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2hz;->d:LX/63i;

    invoke-virtual {v4}, LX/63i;->b()F

    move-result v4

    iput v4, v3, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 450966
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2hz;->c:[Landroid/view/MotionEvent$PointerCoords;

    aget-object v3, v3, v2

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2hz;->d:LX/63i;

    invoke-virtual {v4}, LX/63i;->c()F

    move-result v4

    iput v4, v3, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 450967
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 450968
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v8, v0, LX/2hz;->b:[I

    move-object/from16 v0, p0

    iget-object v9, v0, LX/2hz;->c:[Landroid/view/MotionEvent$PointerCoords;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v10

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getXPrecision()F

    move-result v11

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getYPrecision()F

    move-result v12

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v13

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v14

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v15

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getFlags()I

    move-result v16

    move-wide/from16 v4, p3

    move/from16 v6, p2

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v2

    .line 450969
    return-object v2

    .line 450970
    :cond_1
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v3, "Cannot synthesize motion events on pre-GB devices"

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 450971
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 450972
    and-int/lit16 v0, v0, 0xff

    sparse-switch v0, :sswitch_data_0

    .line 450973
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    .line 450974
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v4

    move v2, v1

    .line 450975
    :goto_0
    if-ge v2, v3, :cond_4

    .line 450976
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 450977
    invoke-static {p0, v0}, LX/2hz;->a(LX/2hz;I)LX/63j;

    move-result-object v5

    move v0, v1

    .line 450978
    :goto_1
    if-ge v0, v4, :cond_3

    .line 450979
    invoke-virtual {p1, v2, v0}, Landroid/view/MotionEvent;->getHistoricalX(II)F

    move-result v6

    .line 450980
    invoke-virtual {p1, v2, v0}, Landroid/view/MotionEvent;->getHistoricalY(II)F

    move-result v7

    .line 450981
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v8

    .line 450982
    invoke-virtual {v5, v6, v7, v8, v9}, LX/63j;->a(FFJ)V

    .line 450983
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 450984
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    .line 450985
    :goto_2
    if-ge v1, v0, :cond_4

    .line 450986
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    .line 450987
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    .line 450988
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    .line 450989
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v6

    .line 450990
    invoke-static {p0, v6}, LX/2hz;->a(LX/2hz;I)LX/63j;

    move-result-object v6

    .line 450991
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v7

    const/4 v8, 0x5

    if-ne v7, v8, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v7

    if-eq v7, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v7

    if-nez v7, :cond_2

    .line 450992
    :cond_1
    invoke-virtual {v6}, LX/63j;->a()V

    .line 450993
    :cond_2
    invoke-virtual {v6, v2, v3, v4, v5}, LX/63j;->a(FFJ)V

    .line 450994
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 450995
    :cond_3
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    .line 450996
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    .line 450997
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    .line 450998
    invoke-virtual {v5, v0, v6, v8, v9}, LX/63j;->a(FFJ)V

    .line 450999
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 451000
    :cond_4
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_0
    .end sparse-switch
.end method
