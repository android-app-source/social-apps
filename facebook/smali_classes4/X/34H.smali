.class public LX/34H;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/3CP;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3CP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 494882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494883
    iput-object p1, p0, LX/34H;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 494884
    iput-object p2, p0, LX/34H;->b:LX/3CP;

    .line 494885
    return-void
.end method

.method public static a(LX/0QB;)LX/34H;
    .locals 1

    .prologue
    .line 494886
    invoke-static {p0}, LX/34H;->b(LX/0QB;)LX/34H;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/34H;
    .locals 5

    .prologue
    .line 494887
    new-instance v2, LX/34H;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 494888
    new-instance v4, LX/3CP;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {v4, v1, v3}, LX/3CP;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;)V

    .line 494889
    move-object v1, v4

    .line 494890
    check-cast v1, LX/3CP;

    invoke-direct {v2, v0, v1}, LX/34H;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3CP;)V

    .line 494891
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)LX/CMk;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 494892
    iget-object v0, p0, LX/34H;->b:LX/3CP;

    .line 494893
    iget-object v2, v0, LX/3CP;->b:LX/0Uh;

    const/16 v3, 0x15f

    const/4 p0, 0x0

    invoke-virtual {v2, v3, p0}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v0, v2

    .line 494894
    if-nez v0, :cond_0

    move-object v0, v1

    .line 494895
    :goto_0
    return-object v0

    .line 494896
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    const-string v2, "gift_wrap"

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 494897
    if-nez v0, :cond_1

    move-object v0, v1

    .line 494898
    goto :goto_0

    .line 494899
    :cond_1
    new-instance v0, LX/CMj;

    invoke-direct {v0}, LX/CMj;-><init>()V

    const/16 v1, -0x3431

    .line 494900
    iput v1, v0, LX/CMj;->d:I

    .line 494901
    move-object v0, v0

    .line 494902
    const v1, -0x5c3b4

    .line 494903
    iput v1, v0, LX/CMj;->a:I

    .line 494904
    move-object v0, v0

    .line 494905
    const v1, -0x867c

    .line 494906
    iput v1, v0, LX/CMj;->c:I

    .line 494907
    move-object v0, v0

    .line 494908
    const-string v1, "hearts"

    .line 494909
    iput-object v1, v0, LX/CMj;->b:Ljava/lang/String;

    .line 494910
    move-object v0, v0

    .line 494911
    new-instance v1, LX/CMk;

    invoke-direct {v1, v0}, LX/CMk;-><init>(LX/CMj;)V

    move-object v0, v1

    .line 494912
    move-object v0, v0

    .line 494913
    goto :goto_0
.end method
