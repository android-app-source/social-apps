.class public abstract LX/2DZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:LX/2D7;

.field public final b:Ljava/lang/Object;

.field private c:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public d:Ljava/lang/Object;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2D7;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 384304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384305
    iput-object p1, p0, LX/2DZ;->a:LX/2D7;

    .line 384306
    iput-object p2, p0, LX/2DZ;->b:Ljava/lang/Object;

    .line 384307
    return-void
.end method

.method public static declared-synchronized h(LX/2DZ;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 384248
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2DZ;->d:Ljava/lang/Object;

    if-ne v0, p1, :cond_0

    .line 384249
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to re-enter the lock"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 384251
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private static declared-synchronized i(LX/2DZ;)Z
    .locals 1

    .prologue
    .line 384303
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2DZ;->d:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 384296
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/2DZ;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/2DZ;->c:I

    .line 384297
    iget v0, p0, LX/2DZ;->c:I

    if-gez v0, :cond_0

    .line 384298
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unbalance calls to acquire/release"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384299
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 384300
    :cond_0
    :try_start_1
    iget v0, p0, LX/2DZ;->c:I

    if-nez v0, :cond_1

    .line 384301
    invoke-virtual {p0}, LX/2DZ;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384302
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 384293
    invoke-virtual {p0, p1}, LX/2DZ;->e(Ljava/lang/Object;)V

    .line 384294
    invoke-virtual {p0}, LX/2DZ;->d()V

    .line 384295
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 384290
    iget-object v1, p0, LX/2DZ;->a:LX/2D7;

    monitor-enter v1

    .line 384291
    :try_start_0
    iget-object v0, p0, LX/2DZ;->a:LX/2D7;

    iget-object v0, v0, LX/2D7;->a:Ljava/util/HashMap;

    iget-object v2, p0, LX/2DZ;->b:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 384292
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 384289
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2DZ;->d:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 384286
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/2DZ;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/2DZ;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384287
    monitor-exit p0

    return-void

    .line 384288
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 384275
    monitor-enter p0

    .line 384276
    :try_start_0
    invoke-virtual {p0}, LX/2DZ;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384277
    const-string v0, "BatchLockState"

    const-string v1, "Attempting to lock a deleted entry: %s (owned by %s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/2DZ;->b:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/2DZ;->d:Ljava/lang/Object;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 384278
    :cond_0
    invoke-static {p0, p1}, LX/2DZ;->h(LX/2DZ;Ljava/lang/Object;)V

    .line 384279
    :goto_0
    invoke-static {p0}, LX/2DZ;->i(LX/2DZ;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 384280
    const v0, 0x25760954

    :try_start_1
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 384281
    :catch_0
    goto :goto_0

    .line 384282
    :cond_1
    :try_start_2
    invoke-virtual {p0}, LX/2DZ;->f()V

    .line 384283
    iput-object p1, p0, LX/2DZ;->d:Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 384284
    monitor-exit p0

    return-void

    .line 384285
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract d()V
.end method

.method public final declared-synchronized d(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 384267
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/2DZ;->h(LX/2DZ;Ljava/lang/Object;)V

    .line 384268
    invoke-virtual {p0}, LX/2DZ;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2DZ;->d:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    .line 384269
    :cond_0
    const/4 v0, 0x0

    .line 384270
    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    .line 384271
    :cond_2
    :try_start_1
    invoke-virtual {p0}, LX/2DZ;->g()Z

    move-result v0

    .line 384272
    if-eqz v0, :cond_1

    .line 384273
    iput-object p1, p0, LX/2DZ;->d:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 384274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 384263
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2DZ;->d:Ljava/lang/Object;

    if-eq v0, p1, :cond_0

    .line 384264
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    const-string v1, "Lock is not held by the provided owner"

    invoke-direct {v0, v1}, Ljava/lang/IllegalMonitorStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 384266
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public abstract e()Z
.end method

.method public abstract f()V
.end method

.method public final declared-synchronized f(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 384255
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/2DZ;->e(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 384256
    :try_start_1
    invoke-virtual {p0}, LX/2DZ;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384257
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, LX/2DZ;->d:Ljava/lang/Object;

    .line 384258
    const v0, 0x15681a7c

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 384259
    monitor-exit p0

    return-void

    .line 384260
    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, LX/2DZ;->d:Ljava/lang/Object;

    .line 384261
    const v1, -0x566f7223

    invoke-static {p0, v1}, LX/02L;->c(Ljava/lang/Object;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 384262
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract g()Z
.end method

.method public abstract h()V
.end method

.method public final declared-synchronized toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 384252
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2DZ;->d:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2DZ;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 384253
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[key="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/2DZ;->b:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",refCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/2DZ;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",lockOwner="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",isDeleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/2DZ;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 384254
    :cond_0
    :try_start_1
    const-string v0, "null"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
