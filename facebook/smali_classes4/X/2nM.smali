.class public final enum LX/2nM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2nM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2nM;

.field public static final enum NEWSFEED:LX/2nM;

.field public static final enum NEWSFEED_BRANDED_BACKGROUND_COLORED_IMAGE:LX/2nM;

.field public static final enum NEWSFEED_BRANDED_VIDEO:LX/2nM;

.field public static final enum NEWSFEED_LARGE_IMAGE:LX/2nM;

.field public static final enum UNKNOWN:LX/2nM;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 464005
    new-instance v0, LX/2nM;

    const-string v1, "NEWSFEED"

    invoke-direct {v0, v1, v2}, LX/2nM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2nM;->NEWSFEED:LX/2nM;

    .line 464006
    new-instance v0, LX/2nM;

    const-string v1, "NEWSFEED_BRANDED_VIDEO"

    invoke-direct {v0, v1, v3}, LX/2nM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2nM;->NEWSFEED_BRANDED_VIDEO:LX/2nM;

    .line 464007
    new-instance v0, LX/2nM;

    const-string v1, "NEWSFEED_BRANDED_BACKGROUND_COLORED_IMAGE"

    invoke-direct {v0, v1, v4}, LX/2nM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2nM;->NEWSFEED_BRANDED_BACKGROUND_COLORED_IMAGE:LX/2nM;

    .line 464008
    new-instance v0, LX/2nM;

    const-string v1, "NEWSFEED_LARGE_IMAGE"

    invoke-direct {v0, v1, v5}, LX/2nM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2nM;->NEWSFEED_LARGE_IMAGE:LX/2nM;

    .line 464009
    new-instance v0, LX/2nM;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/2nM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2nM;->UNKNOWN:LX/2nM;

    .line 464010
    const/4 v0, 0x5

    new-array v0, v0, [LX/2nM;

    sget-object v1, LX/2nM;->NEWSFEED:LX/2nM;

    aput-object v1, v0, v2

    sget-object v1, LX/2nM;->NEWSFEED_BRANDED_VIDEO:LX/2nM;

    aput-object v1, v0, v3

    sget-object v1, LX/2nM;->NEWSFEED_BRANDED_BACKGROUND_COLORED_IMAGE:LX/2nM;

    aput-object v1, v0, v4

    sget-object v1, LX/2nM;->NEWSFEED_LARGE_IMAGE:LX/2nM;

    aput-object v1, v0, v5

    sget-object v1, LX/2nM;->UNKNOWN:LX/2nM;

    aput-object v1, v0, v6

    sput-object v0, LX/2nM;->$VALUES:[LX/2nM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 464004
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/2nM;
    .locals 1

    .prologue
    .line 463997
    if-nez p0, :cond_0

    .line 463998
    :try_start_0
    sget-object v0, LX/2nM;->UNKNOWN:LX/2nM;

    .line 463999
    :goto_0
    return-object v0

    .line 464000
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2nM;->valueOf(Ljava/lang/String;)LX/2nM;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 464001
    :catch_0
    sget-object v0, LX/2nM;->UNKNOWN:LX/2nM;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/2nM;
    .locals 1

    .prologue
    .line 464003
    const-class v0, LX/2nM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2nM;

    return-object v0
.end method

.method public static values()[LX/2nM;
    .locals 1

    .prologue
    .line 464002
    sget-object v0, LX/2nM;->$VALUES:[LX/2nM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2nM;

    return-object v0
.end method
