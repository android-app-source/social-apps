.class public LX/2GR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2GR;


# instance fields
.field private final a:LX/0Tj;


# direct methods
.method public constructor <init>(LX/0Tj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 388451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388452
    iput-object p1, p0, LX/2GR;->a:LX/0Tj;

    .line 388453
    return-void
.end method

.method public static a(LX/0QB;)LX/2GR;
    .locals 4

    .prologue
    .line 388434
    sget-object v0, LX/2GR;->b:LX/2GR;

    if-nez v0, :cond_1

    .line 388435
    const-class v1, LX/2GR;

    monitor-enter v1

    .line 388436
    :try_start_0
    sget-object v0, LX/2GR;->b:LX/2GR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 388437
    if-eqz v2, :cond_0

    .line 388438
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 388439
    new-instance p0, LX/2GR;

    invoke-static {v0}, LX/0Tj;->a(LX/0QB;)LX/0Tj;

    move-result-object v3

    check-cast v3, LX/0Tj;

    invoke-direct {p0, v3}, LX/2GR;-><init>(LX/0Tj;)V

    .line 388440
    move-object v0, p0

    .line 388441
    sput-object v0, LX/2GR;->b:LX/2GR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 388442
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 388443
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 388444
    :cond_1
    sget-object v0, LX/2GR;->b:LX/2GR;

    return-object v0

    .line 388445
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 388446
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 1

    .prologue
    .line 388447
    iget-object v0, p0, LX/2GR;->a:LX/0Tj;

    .line 388448
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/0Tj;->m:Z

    .line 388449
    invoke-static {v0}, LX/0Tj;->c(LX/0Tj;)V

    .line 388450
    return-void
.end method
