.class public final enum LX/2rJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2rJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2rJ;

.field public static final enum CACHED_SECTION:LX/2rJ;

.field public static final enum NORMAL:LX/2rJ;

.field public static final enum PAGINATION:LX/2rJ;

.field public static final enum PREFETCH:LX/2rJ;

.field public static final enum PULL_TO_REFRESH:LX/2rJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 471816
    new-instance v0, LX/2rJ;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, LX/2rJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2rJ;->NORMAL:LX/2rJ;

    .line 471817
    new-instance v0, LX/2rJ;

    const-string v1, "PAGINATION"

    invoke-direct {v0, v1, v3}, LX/2rJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2rJ;->PAGINATION:LX/2rJ;

    .line 471818
    new-instance v0, LX/2rJ;

    const-string v1, "CACHED_SECTION"

    invoke-direct {v0, v1, v4}, LX/2rJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2rJ;->CACHED_SECTION:LX/2rJ;

    .line 471819
    new-instance v0, LX/2rJ;

    const-string v1, "PREFETCH"

    invoke-direct {v0, v1, v5}, LX/2rJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2rJ;->PREFETCH:LX/2rJ;

    .line 471820
    new-instance v0, LX/2rJ;

    const-string v1, "PULL_TO_REFRESH"

    invoke-direct {v0, v1, v6}, LX/2rJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2rJ;->PULL_TO_REFRESH:LX/2rJ;

    .line 471821
    const/4 v0, 0x5

    new-array v0, v0, [LX/2rJ;

    sget-object v1, LX/2rJ;->NORMAL:LX/2rJ;

    aput-object v1, v0, v2

    sget-object v1, LX/2rJ;->PAGINATION:LX/2rJ;

    aput-object v1, v0, v3

    sget-object v1, LX/2rJ;->CACHED_SECTION:LX/2rJ;

    aput-object v1, v0, v4

    sget-object v1, LX/2rJ;->PREFETCH:LX/2rJ;

    aput-object v1, v0, v5

    sget-object v1, LX/2rJ;->PULL_TO_REFRESH:LX/2rJ;

    aput-object v1, v0, v6

    sput-object v0, LX/2rJ;->$VALUES:[LX/2rJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 471822
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2rJ;
    .locals 1

    .prologue
    .line 471815
    const-class v0, LX/2rJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2rJ;

    return-object v0
.end method

.method public static values()[LX/2rJ;
    .locals 1

    .prologue
    .line 471814
    sget-object v0, LX/2rJ;->$VALUES:[LX/2rJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2rJ;

    return-object v0
.end method
