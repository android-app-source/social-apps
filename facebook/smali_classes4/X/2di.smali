.class public LX/2di;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:LX/0ti;


# direct methods
.method public constructor <init>(LX/0ti;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 443680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 443681
    iput-object p1, p0, LX/2di;->a:LX/0ti;

    .line 443682
    return-void
.end method

.method public static a(LX/0QB;)LX/2di;
    .locals 1

    .prologue
    .line 443683
    invoke-static {p0}, LX/2di;->b(LX/0QB;)LX/2di;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2di;
    .locals 2

    .prologue
    .line 443684
    new-instance v1, LX/2di;

    invoke-static {p0}, LX/0ti;->b(LX/0QB;)LX/0ti;

    move-result-object v0

    check-cast v0, LX/0ti;

    invoke-direct {v1, v0}, LX/2di;-><init>(LX/0ti;)V

    .line 443685
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U::",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">(TU;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 443686
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    .line 443687
    if-nez v0, :cond_0

    .line 443688
    :goto_0
    return-void

    .line 443689
    :cond_0
    iget-object v1, p0, LX/2di;->a:LX/0ti;

    new-instance v2, LX/4Uj;

    invoke-direct {v2, p0, p1, v0}, LX/4Uj;-><init>(LX/2di;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0ti;->a(LX/3Bq;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
