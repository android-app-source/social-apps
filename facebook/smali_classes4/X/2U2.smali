.class public LX/2U2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:LX/0re;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0re",
            "<TK;",
            "LX/8FT",
            "<TV;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final b:I

.field private c:LX/0SG;


# direct methods
.method public constructor <init>(LX/0rd;LX/0SG;I)V
    .locals 2

    .prologue
    .line 415050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415051
    iput p3, p0, LX/2U2;->b:I

    .line 415052
    iget v0, p0, LX/2U2;->b:I

    const-string v1, "Pages"

    invoke-virtual {p1, v0, v1}, LX/0rd;->b(ILjava/lang/String;)LX/0re;

    move-result-object v0

    iput-object v0, p0, LX/2U2;->a:LX/0re;

    .line 415053
    iput-object p2, p0, LX/2U2;->c:LX/0SG;

    .line 415054
    return-void
.end method

.method private static declared-synchronized b(LX/2U2;Ljava/lang/Object;J)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;J)TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 415040
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2U2;->a:LX/0re;

    invoke-virtual {v0, p1}, LX/0re;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8FT;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415041
    if-nez v0, :cond_0

    move-object v0, v1

    .line 415042
    :goto_0
    monitor-exit p0

    return-object v0

    .line 415043
    :cond_0
    :try_start_1
    iget-wide v4, v0, LX/8FT;->b:J

    move-wide v2, v4

    .line 415044
    cmp-long v2, v2, p2

    if-lez v2, :cond_1

    .line 415045
    iget-object v1, v0, LX/8FT;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 415046
    goto :goto_0

    .line 415047
    :cond_1
    iget-object v0, p0, LX/2U2;->a:LX/0re;

    invoke-virtual {v0, p1}, LX/0re;->b(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    .line 415048
    goto :goto_0

    .line 415049
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/Object;)LX/8FT;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LX/8FT",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 415039
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2U2;->a:LX/0re;

    invoke-virtual {v0, p1}, LX/0re;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8FT;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;J)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;J)TV;"
        }
    .end annotation

    .prologue
    .line 415038
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2U2;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long/2addr v0, p2

    invoke-static {p0, p1, v0, v1}, LX/2U2;->b(LX/2U2;Ljava/lang/Object;J)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 415055
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2U2;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/2U2;->a(Ljava/lang/Object;Ljava/lang/Object;J)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;Ljava/lang/Object;J)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;J)TV;"
        }
    .end annotation

    .prologue
    .line 415033
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2U2;->a:LX/0re;

    new-instance v1, LX/8FT;

    invoke-direct {v1, p2, p3, p4}, LX/8FT;-><init>(Ljava/lang/Object;J)V

    invoke-virtual {v0, p1, v1}, LX/0re;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8FT;

    .line 415034
    if-eqz v0, :cond_0

    .line 415035
    iget-object v1, v0, LX/8FT;->a:Ljava/lang/Object;

    move-object v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415036
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 415037
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 415030
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2U2;->a:LX/0re;

    invoke-virtual {v0}, LX/0re;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415031
    monitor-exit p0

    return-void

    .line 415032
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 415029
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2U2;->a:LX/0re;

    invoke-virtual {v0}, LX/0re;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 415022
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2U2;->a:LX/0re;

    invoke-virtual {v0, p1}, LX/0re;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8FT;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415023
    if-nez v0, :cond_0

    .line 415024
    const/4 v0, 0x0

    .line 415025
    :goto_0
    monitor-exit p0

    return-object v0

    .line 415026
    :cond_0
    :try_start_1
    iget-object p1, v0, LX/8FT;->a:Ljava/lang/Object;

    move-object v0, p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415027
    goto :goto_0

    .line 415028
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    .line 415019
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2U2;->a:LX/0re;

    invoke-virtual {v0, p1}, LX/0re;->b(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415020
    monitor-exit p0

    return-void

    .line 415021
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
