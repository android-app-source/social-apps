.class public final LX/2ng;
.super LX/2nh;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/2nh",
        "<TT;>;",
        "Ljava/io/Closeable;"
    }
.end annotation


# static fields
.field private static final a:LX/2nj;


# instance fields
.field public final b:LX/0Sh;

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final d:LX/2nf;

.field public final e:LX/2nn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2ng",
            "<TT;>.SimpleConnectionCache;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 464976
    new-instance v0, LX/2nj;

    .line 464977
    const-string v1, "@LOCAL_BATCH:"

    move-object v1, v1

    .line 464978
    sget-object v2, LX/2nk;->AFTER:LX/2nk;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, LX/2nj;-><init>(Ljava/lang/String;LX/2nk;Z)V

    sput-object v0, LX/2ng;->a:LX/2nj;

    return-void
.end method

.method public constructor <init>(LX/2nf;Ljava/util/ArrayList;LX/0Sh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nf;",
            "Ljava/util/ArrayList",
            "<TT;>;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ")V"
        }
    .end annotation

    .prologue
    .line 464970
    invoke-direct {p0, p1, p3}, LX/2nh;-><init>(LX/2nf;LX/0Sh;)V

    .line 464971
    iput-object p1, p0, LX/2ng;->d:LX/2nf;

    .line 464972
    iput-object p2, p0, LX/2ng;->c:Ljava/util/ArrayList;

    .line 464973
    iput-object p3, p0, LX/2ng;->b:LX/0Sh;

    .line 464974
    new-instance v0, LX/2nn;

    invoke-direct {v0, p0}, LX/2nn;-><init>(LX/2ng;)V

    iput-object v0, p0, LX/2ng;->e:LX/2nn;

    .line 464975
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 464967
    iget-object v0, p0, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 464968
    iget-object v0, p0, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 464969
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/2nj;
    .locals 2

    .prologue
    .line 464955
    iget-object v0, p0, LX/2ng;->d:LX/2nf;

    invoke-interface {v0}, LX/2nf;->getCount()I

    move-result v0

    invoke-virtual {p0}, LX/2ng;->c()I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 464956
    invoke-super {p0}, LX/2nh;->b()LX/2nj;

    move-result-object v1

    .line 464957
    iget-object v0, v1, LX/2nj;->b:Ljava/lang/String;

    move-object v0, v0

    .line 464958
    invoke-static {v0}, LX/3DQ;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-object v0, v1

    .line 464959
    :goto_1
    return-object v0

    .line 464960
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 464961
    :cond_1
    sget-object v0, LX/2ng;->a:LX/2nj;

    goto :goto_1
.end method

.method public final declared-synchronized c()I
    .locals 1

    .prologue
    .line 464966
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 464964
    iget-object v0, p0, LX/2ng;->e:LX/2nn;

    invoke-virtual {v0}, LX/2nn;->a()V

    .line 464965
    return-void
.end method

.method public final e()LX/2nf;
    .locals 1

    .prologue
    .line 464963
    iget-object v0, p0, LX/2ng;->d:LX/2nf;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 464962
    iget-object v0, p0, LX/2ng;->d:LX/2nf;

    invoke-interface {v0}, LX/2nf;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2ng;->e:LX/2nn;

    invoke-virtual {v0}, LX/2nn;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
