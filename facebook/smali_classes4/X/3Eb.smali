.class public LX/3Eb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final e:Ljava/lang/String;

.field private static volatile f:LX/3Eb;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Z

.field public d:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 537828
    const-class v0, LX/3Eb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Eb;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 537829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537830
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Eb;->c:Z

    .line 537831
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Eb;->d:J

    .line 537832
    return-void
.end method

.method public static a(LX/0QB;)LX/3Eb;
    .locals 5

    .prologue
    .line 537833
    sget-object v0, LX/3Eb;->f:LX/3Eb;

    if-nez v0, :cond_1

    .line 537834
    const-class v1, LX/3Eb;

    monitor-enter v1

    .line 537835
    :try_start_0
    sget-object v0, LX/3Eb;->f:LX/3Eb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 537836
    if-eqz v2, :cond_0

    .line 537837
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 537838
    new-instance p0, LX/3Eb;

    invoke-direct {p0}, LX/3Eb;-><init>()V

    .line 537839
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    .line 537840
    iput-object v3, p0, LX/3Eb;->a:LX/03V;

    iput-object v4, p0, LX/3Eb;->b:LX/0Uh;

    .line 537841
    move-object v0, p0

    .line 537842
    sput-object v0, LX/3Eb;->f:LX/3Eb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 537843
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 537844
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 537845
    :cond_1
    sget-object v0, LX/3Eb;->f:LX/3Eb;

    return-object v0

    .line 537846
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 537847
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 537848
    new-instance v0, Ljava/lang/Error;

    const-string v1, "RTC Privacy check failure"

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    .line 537849
    iget-object v1, p0, LX/3Eb;->a:LX/03V;

    const-string v2, "webrtc"

    const-string v3, "Failed to assert user consent before enabling self audio or video."

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 537850
    sget-object v1, LX/3Eb;->e:Ljava/lang/String;

    const-string v2, "Privacy check failure"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 537851
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 537852
    if-eqz v1, :cond_0

    .line 537853
    throw v0

    .line 537854
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(JZ)V
    .locals 1

    .prologue
    .line 537855
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, LX/3Eb;->d:J

    .line 537856
    iput-boolean p3, p0, LX/3Eb;->c:Z

    .line 537857
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 537858
    monitor-exit p0

    return-void

    .line 537859
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 537860
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LX/3Eb;->b:LX/0Uh;

    const/16 v3, 0x5ea

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 537861
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 537862
    :cond_1
    :try_start_1
    iget-boolean v5, p0, LX/3Eb;->c:Z

    if-eqz v5, :cond_2

    iget-wide v5, p0, LX/3Eb;->d:J

    cmp-long v5, v5, p1

    if-nez v5, :cond_2

    const/4 v5, 0x1

    :goto_1
    move v2, v5

    .line 537863
    if-nez v2, :cond_0

    .line 537864
    invoke-direct {p0}, LX/3Eb;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 537865
    goto :goto_0

    .line 537866
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method
