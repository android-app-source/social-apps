.class public LX/3AP;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/lang/String;

.field private final e:LX/1Gm;

.field private final f:LX/0Zb;

.field private final g:LX/1Gk;

.field private final h:LX/1Gl;

.field private final i:LX/1Go;

.field public final j:LX/13t;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 525280
    const-class v0, LX/3AP;

    sput-object v0, LX/3AP;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;Ljava/lang/String;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param

    .prologue
    .line 525269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 525270
    iput-object p1, p0, LX/3AP;->c:Landroid/content/Context;

    .line 525271
    iput-object p2, p0, LX/3AP;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 525272
    iput-object p3, p0, LX/3AP;->d:Ljava/lang/String;

    .line 525273
    iput-object p4, p0, LX/3AP;->e:LX/1Gm;

    .line 525274
    iput-object p5, p0, LX/3AP;->f:LX/0Zb;

    .line 525275
    iput-object p6, p0, LX/3AP;->g:LX/1Gk;

    .line 525276
    iput-object p7, p0, LX/3AP;->h:LX/1Gl;

    .line 525277
    iput-object p8, p0, LX/3AP;->i:LX/1Go;

    .line 525278
    iput-object p9, p0, LX/3AP;->j:LX/13t;

    .line 525279
    return-void
.end method

.method private static a(LX/3AP;Landroid/net/Uri;)Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 525261
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Downloading contact photo from: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 525262
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$DisplayPhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 525263
    invoke-static {p0, p1}, LX/3AP;->b(LX/3AP;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 525264
    :cond_0
    return-object v0

    .line 525265
    :cond_1
    iget-object v0, p0, LX/3AP;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 525266
    invoke-static {v0, p1}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 525267
    if-nez v0, :cond_0

    .line 525268
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Contact photo not found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static b(LX/3AP;Landroid/net/Uri;)Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 525256
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Downloading media from generic content resolver: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 525257
    iget-object v0, p0, LX/3AP;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 525258
    if-nez v0, :cond_0

    .line 525259
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Media not found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 525260
    :cond_0
    return-object v0
.end method

.method public static d(LX/3AP;LX/34X;)LX/15D;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/34X",
            "<TT;>;)",
            "LX/15D",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 525212
    new-instance v9, LX/1uw;

    .line 525213
    iget-object v0, p1, LX/34X;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 525214
    iget-object v1, p0, LX/3AP;->e:LX/1Gm;

    invoke-direct {v9, v0, v1}, LX/1uw;-><init>(Landroid/net/Uri;LX/1Gm;)V

    .line 525215
    invoke-virtual {p1}, LX/34X;->a()Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    .line 525216
    iget-object v0, p0, LX/3AP;->j:LX/13t;

    invoke-virtual {v0}, LX/13t;->a()Ljava/lang/String;

    move-result-object v0

    .line 525217
    const-string v1, "X-FB-Connection-Type"

    invoke-interface {v2, v1, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 525218
    iget-object v0, p1, LX/34X;->f:LX/0P1;

    move-object v0, v0

    .line 525219
    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 525220
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 525221
    :cond_0
    move-object v10, v2

    .line 525222
    invoke-interface {v10}, Lorg/apache/http/client/methods/HttpUriRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 525223
    iget-object v0, p0, LX/3AP;->e:LX/1Gm;

    .line 525224
    iget-object v1, p1, LX/34X;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 525225
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 525226
    invoke-virtual {v0, v1}, LX/1Gm;->b(Ljava/lang/String;)V

    .line 525227
    new-instance v0, LX/1uz;

    .line 525228
    iget-object v1, p1, LX/34X;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 525229
    iget-object v2, p1, LX/34X;->g:LX/1uy;

    move-object v2, v2

    .line 525230
    iget-object v3, p0, LX/3AP;->d:Ljava/lang/String;

    iget-object v4, p0, LX/3AP;->e:LX/1Gm;

    iget-object v5, p0, LX/3AP;->f:LX/0Zb;

    iget-object v6, p0, LX/3AP;->g:LX/1Gk;

    iget-object v7, p0, LX/3AP;->h:LX/1Gl;

    iget-object v8, p0, LX/3AP;->i:LX/1Go;

    invoke-direct/range {v0 .. v8}, LX/1uz;-><init>(Landroid/net/Uri;LX/1uy;Ljava/lang/String;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;)V

    .line 525231
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v1

    iget-object v2, p0, LX/3AP;->d:Ljava/lang/String;

    .line 525232
    iput-object v2, v1, LX/15E;->c:Ljava/lang/String;

    .line 525233
    move-object v1, v1

    .line 525234
    iget-object v2, p1, LX/34X;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 525235
    iput-object v2, v1, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 525236
    move-object v1, v1

    .line 525237
    const-string v2, "MediaDownloader"

    .line 525238
    iput-object v2, v1, LX/15E;->e:Ljava/lang/String;

    .line 525239
    move-object v1, v1

    .line 525240
    iput-object v10, v1, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 525241
    move-object v1, v1

    .line 525242
    const/4 v2, 0x1

    .line 525243
    iput-boolean v2, v1, LX/15E;->p:Z

    .line 525244
    move-object v1, v1

    .line 525245
    sget-object v2, LX/14P;->RETRY_SAFE:LX/14P;

    .line 525246
    iput-object v2, v1, LX/15E;->j:LX/14P;

    .line 525247
    move-object v1, v1

    .line 525248
    iget-object v2, p1, LX/34X;->e:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v2, v2

    .line 525249
    iput-object v2, v1, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 525250
    move-object v1, v1

    .line 525251
    iput-object v9, v1, LX/15E;->h:Lorg/apache/http/client/RedirectHandler;

    .line 525252
    move-object v1, v1

    .line 525253
    iput-object v0, v1, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 525254
    move-object v0, v1

    .line 525255
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    return-object v0
.end method

.method private f(LX/34X;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/34X",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 525204
    iget-object v0, p1, LX/34X;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 525205
    const-string v1, "com.android.contacts"

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 525206
    invoke-static {p0, v0}, LX/3AP;->a(LX/3AP;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    move-object v1, v0

    .line 525207
    :goto_0
    :try_start_0
    iget-object v0, p1, LX/34X;->g:LX/1uy;

    move-object v0, v0

    .line 525208
    const-wide/16 v2, -0x1

    sget-object v4, LX/2ur;->NOT_IN_GK:LX/2ur;

    invoke-interface {v0, v1, v2, v3, v4}, LX/1uy;->a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 525209
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object v0

    .line 525210
    :cond_0
    invoke-static {p0, v0}, LX/3AP;->b(LX/3AP;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 525211
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method private static g(LX/34X;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/34X",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 525197
    new-instance v0, Ljava/io/File;

    .line 525198
    iget-object v1, p0, LX/34X;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 525199
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 525200
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 525201
    :try_start_0
    iget-object v2, p0, LX/34X;->g:LX/1uy;

    move-object v2, v2

    .line 525202
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    sget-object v0, LX/2ur;->NOT_IN_GK:LX/2ur;

    invoke-interface {v2, v1, v4, v5, v0}, LX/1uy;->a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 525203
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method


# virtual methods
.method public a(LX/34X;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/34X",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 525190
    sget-object v0, LX/3AQ;->a:[I

    .line 525191
    iget-object v1, p1, LX/34X;->c:LX/3AO;

    move-object v1, v1

    .line 525192
    invoke-virtual {v1}, LX/3AO;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 525193
    iget-object v0, p0, LX/3AP;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p0, p1}, LX/3AP;->d(LX/3AP;LX/34X;)LX/15D;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    move-result-object v0

    move-object v0, v0

    .line 525194
    :goto_0
    return-object v0

    .line 525195
    :pswitch_0
    invoke-direct {p0, p1}, LX/3AP;->f(LX/34X;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 525196
    :pswitch_1
    invoke-static {p1}, LX/3AP;->g(LX/34X;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(LX/34X;)LX/1j2;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/34X",
            "<TT;>;)",
            "LX/1j2",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 525185
    iget-object v0, p1, LX/34X;->c:LX/3AO;

    move-object v0, v0

    .line 525186
    sget-object v1, LX/3AO;->HTTP:LX/3AO;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/3AO;->HTTPS:LX/3AO;

    if-eq v0, v1, :cond_0

    .line 525187
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Only http and https supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 525188
    :cond_0
    invoke-static {p0, p1}, LX/3AP;->d(LX/3AP;LX/34X;)LX/15D;

    move-result-object v0

    .line 525189
    iget-object v1, p0, LX/3AP;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v0

    return-object v0
.end method
