.class public LX/2d2;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/03V;

.field public final c:LX/0Uh;

.field private final d:LX/0tX;

.field public final e:LX/2cw;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/1Ck;

.field private final i:LX/0SG;

.field public j:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 442794
    const-class v0, LX/2d2;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2d2;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Uh;LX/0tX;LX/2cw;LX/0Or;LX/0Or;LX/1Ck;LX/0SG;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/placetips/bootstrap/IsUserPlaceTipsDebugEmployee;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0tX;",
            "LX/2cw;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1Ck;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 442795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442796
    iput-object p1, p0, LX/2d2;->b:LX/03V;

    .line 442797
    iput-object p2, p0, LX/2d2;->c:LX/0Uh;

    .line 442798
    iput-object p3, p0, LX/2d2;->d:LX/0tX;

    .line 442799
    iput-object p4, p0, LX/2d2;->e:LX/2cw;

    .line 442800
    iput-object p5, p0, LX/2d2;->f:LX/0Or;

    .line 442801
    iput-object p6, p0, LX/2d2;->g:LX/0Or;

    .line 442802
    iput-object p7, p0, LX/2d2;->h:LX/1Ck;

    .line 442803
    iput-object p8, p0, LX/2d2;->i:LX/0SG;

    .line 442804
    return-void
.end method

.method private a(Lcom/facebook/placetips/bootstrap/PresenceDescription;)LX/4FW;
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    .line 442805
    invoke-virtual {p1}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->k()Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object v4

    .line 442806
    new-instance v1, LX/4FW;

    invoke-direct {v1}, LX/4FW;-><init>()V

    invoke-virtual {v4}, Lcom/facebook/placetips/bootstrap/PresenceSource;->b()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v4}, Lcom/facebook/placetips/bootstrap/PresenceSource;->b()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4FW;->a(Ljava/lang/Integer;)LX/4FW;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/placetips/bootstrap/PresenceSource;->c()Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v4}, Lcom/facebook/placetips/bootstrap/PresenceSource;->c()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/4FW;->a(Ljava/lang/Double;)LX/4FW;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/placetips/bootstrap/PresenceSource;->d()Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v4}, Lcom/facebook/placetips/bootstrap/PresenceSource;->d()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    :goto_2
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/4FW;->b(Ljava/lang/Double;)LX/4FW;

    move-result-object v0

    iget-object v1, p0, LX/2d2;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, LX/1lQ;->m(J)J

    move-result-wide v6

    long-to-int v1, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4FW;->c(Ljava/lang/Integer;)LX/4FW;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->a()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-int v1, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4FW;->b(Ljava/lang/Integer;)LX/4FW;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/placetips/bootstrap/PresenceSource;->e()Ljava/lang/Float;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v4}, Lcom/facebook/placetips/bootstrap/PresenceSource;->e()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v0

    :goto_3
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/4FW;->c(Ljava/lang/Double;)LX/4FW;

    move-result-object v0

    invoke-virtual {v4}, Lcom/facebook/placetips/bootstrap/PresenceSource;->e()Ljava/lang/Float;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v4}, Lcom/facebook/placetips/bootstrap/PresenceSource;->e()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v2

    :cond_0
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4FW;->d(Ljava/lang/Double;)LX/4FW;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_2
    move-wide v0, v2

    goto :goto_1

    :cond_3
    move-wide v0, v2

    goto :goto_2

    :cond_4
    move-wide v0, v2

    goto :goto_3
.end method

.method public static a$redex0(LX/2d2;Lcom/facebook/placetips/bootstrap/PresenceDescription;Ljava/lang/String;)V
    .locals 5
    .param p1    # Lcom/facebook/placetips/bootstrap/PresenceDescription;
        .annotation build Lcom/facebook/graphql/calls/PlaceTipFooterResponseEnum;
        .end annotation
    .end param

    .prologue
    .line 442807
    new-instance v0, LX/4Jd;

    invoke-direct {v0}, LX/4Jd;-><init>()V

    const-string v1, "android_suggestifier_voting"

    .line 442808
    const-string v2, "endpoint"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 442809
    move-object v0, v0

    .line 442810
    const-string v1, "android_place_tips"

    .line 442811
    const-string v2, "entry_point"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 442812
    move-object v0, v0

    .line 442813
    invoke-virtual {p1}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v1

    .line 442814
    const-string v2, "page_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 442815
    move-object v0, v0

    .line 442816
    const-string v1, "sentiment"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 442817
    move-object v0, v0

    .line 442818
    invoke-virtual {p1}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->o()Ljava/lang/String;

    move-result-object v1

    .line 442819
    const-string v2, "suggetifier_response_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 442820
    move-object v0, v0

    .line 442821
    invoke-direct {p0, p1}, LX/2d2;->a(Lcom/facebook/placetips/bootstrap/PresenceDescription;)LX/4FW;

    move-result-object v1

    .line 442822
    const-string v2, "location_data"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 442823
    move-object v0, v0

    .line 442824
    new-instance v1, LX/DEk;

    invoke-direct {v1}, LX/DEk;-><init>()V

    move-object v1, v1

    .line 442825
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 442826
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 442827
    iget-object v1, p0, LX/2d2;->h:LX/1Ck;

    const-string v2, "submit_suggestifier_answer"

    iget-object v3, p0, LX/2d2;->d:LX/0tX;

    sget-object v4, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v3, v0, v4}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v3, LX/DEc;

    invoke-direct {v3, p0}, LX/DEc;-><init>(LX/2d2;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 442828
    return-void
.end method

.method public static b(LX/0QB;)LX/2d2;
    .locals 9

    .prologue
    .line 442829
    new-instance v0, LX/2d2;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/2cw;->a(LX/0QB;)LX/2cw;

    move-result-object v4

    check-cast v4, LX/2cw;

    const/16 v5, 0x1556

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x2fd

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-direct/range {v0 .. v8}, LX/2d2;-><init>(LX/03V;LX/0Uh;LX/0tX;LX/2cw;LX/0Or;LX/0Or;LX/1Ck;LX/0SG;)V

    .line 442830
    return-object v0
.end method
