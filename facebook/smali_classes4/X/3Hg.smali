.class public final enum LX/3Hg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3Hg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3Hg;

.field public static final enum BROADCAST_COMMERCIAL_BREAK:LX/3Hg;

.field public static final enum LIVE:LX/3Hg;

.field public static final enum VIEWER_COMMERCIAL_BREAK_FULLSCREEN:LX/3Hg;

.field public static final enum VIEWER_COMMERCIAL_BREAK_INLINE:LX/3Hg;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 544611
    new-instance v0, LX/3Hg;

    const-string v1, "LIVE"

    invoke-direct {v0, v1, v2}, LX/3Hg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Hg;->LIVE:LX/3Hg;

    .line 544612
    new-instance v0, LX/3Hg;

    const-string v1, "BROADCAST_COMMERCIAL_BREAK"

    invoke-direct {v0, v1, v3}, LX/3Hg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Hg;->BROADCAST_COMMERCIAL_BREAK:LX/3Hg;

    .line 544613
    new-instance v0, LX/3Hg;

    const-string v1, "VIEWER_COMMERCIAL_BREAK_INLINE"

    invoke-direct {v0, v1, v4}, LX/3Hg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Hg;->VIEWER_COMMERCIAL_BREAK_INLINE:LX/3Hg;

    .line 544614
    new-instance v0, LX/3Hg;

    const-string v1, "VIEWER_COMMERCIAL_BREAK_FULLSCREEN"

    invoke-direct {v0, v1, v5}, LX/3Hg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Hg;->VIEWER_COMMERCIAL_BREAK_FULLSCREEN:LX/3Hg;

    .line 544615
    const/4 v0, 0x4

    new-array v0, v0, [LX/3Hg;

    sget-object v1, LX/3Hg;->LIVE:LX/3Hg;

    aput-object v1, v0, v2

    sget-object v1, LX/3Hg;->BROADCAST_COMMERCIAL_BREAK:LX/3Hg;

    aput-object v1, v0, v3

    sget-object v1, LX/3Hg;->VIEWER_COMMERCIAL_BREAK_INLINE:LX/3Hg;

    aput-object v1, v0, v4

    sget-object v1, LX/3Hg;->VIEWER_COMMERCIAL_BREAK_FULLSCREEN:LX/3Hg;

    aput-object v1, v0, v5

    sput-object v0, LX/3Hg;->$VALUES:[LX/3Hg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 544616
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3Hg;
    .locals 1

    .prologue
    .line 544617
    const-class v0, LX/3Hg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3Hg;

    return-object v0
.end method

.method public static values()[LX/3Hg;
    .locals 1

    .prologue
    .line 544618
    sget-object v0, LX/3Hg;->$VALUES:[LX/3Hg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3Hg;

    return-object v0
.end method
