.class public final LX/2QW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/platform/common/server/PlatformOperation;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/platform/common/server/PlatformOperation;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 408425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 408426
    iput-object p1, p0, LX/2QW;->a:LX/0QB;

    .line 408427
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 408428
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2QW;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 408429
    packed-switch p2, :pswitch_data_0

    .line 408430
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 408431
    :pswitch_0
    new-instance v1, LX/2QX;

    const/16 v0, 0xb83

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 408432
    new-instance v0, LX/2Qa;

    invoke-direct {v0}, LX/2Qa;-><init>()V

    .line 408433
    move-object v0, v0

    .line 408434
    move-object v0, v0

    .line 408435
    check-cast v0, LX/2Qa;

    invoke-direct {v1, p0, v0}, LX/2QX;-><init>(LX/0Or;LX/2Qa;)V

    .line 408436
    move-object v0, v1

    .line 408437
    :goto_0
    return-object v0

    .line 408438
    :pswitch_1
    new-instance v1, LX/2Qb;

    const/16 v0, 0xb83

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 408439
    new-instance p2, LX/2Qc;

    invoke-direct {p2}, LX/2Qc;-><init>()V

    .line 408440
    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 408441
    iput-object v0, p2, LX/2Qc;->b:LX/03V;

    .line 408442
    move-object v0, p2

    .line 408443
    check-cast v0, LX/2Qc;

    invoke-direct {v1, p0, v0}, LX/2Qb;-><init>(LX/0Or;LX/2Qc;)V

    .line 408444
    move-object v0, v1

    .line 408445
    goto :goto_0

    .line 408446
    :pswitch_2
    new-instance p2, LX/2Qd;

    invoke-static {p1}, LX/2Qe;->a(LX/0QB;)LX/2Qe;

    move-result-object v0

    check-cast v0, LX/2Qe;

    invoke-static {p1}, LX/2Qr;->a(LX/0QB;)LX/2Qr;

    move-result-object v1

    check-cast v1, LX/2Qr;

    invoke-static {p1}, LX/2Qi;->a(LX/0QB;)LX/2Qi;

    move-result-object p0

    check-cast p0, LX/2Qi;

    invoke-direct {p2, v0, v1, p0}, LX/2Qd;-><init>(LX/2Qe;LX/2Qr;LX/2Qi;)V

    .line 408447
    move-object v0, p2

    .line 408448
    goto :goto_0

    .line 408449
    :pswitch_3
    new-instance p2, LX/2Qs;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/2Qt;->a(LX/0QB;)LX/2Qt;

    move-result-object v1

    check-cast v1, LX/2Qt;

    invoke-static {p1}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object p0

    check-cast p0, LX/0en;

    invoke-direct {p2, v0, v1, p0}, LX/2Qs;-><init>(Landroid/content/Context;LX/2Qt;LX/0en;)V

    .line 408450
    move-object v0, p2

    .line 408451
    goto :goto_0

    .line 408452
    :pswitch_4
    new-instance p2, LX/2Qu;

    invoke-static {p1}, LX/2Qe;->a(LX/0QB;)LX/2Qe;

    move-result-object v0

    check-cast v0, LX/2Qe;

    invoke-static {p1}, LX/2Qr;->a(LX/0QB;)LX/2Qr;

    move-result-object v1

    check-cast v1, LX/2Qr;

    invoke-static {p1}, LX/2Qi;->a(LX/0QB;)LX/2Qi;

    move-result-object p0

    check-cast p0, LX/2Qi;

    invoke-direct {p2, v0, v1, p0}, LX/2Qu;-><init>(LX/2Qe;LX/2Qr;LX/2Qi;)V

    .line 408453
    move-object v0, p2

    .line 408454
    goto :goto_0

    .line 408455
    :pswitch_5
    new-instance p2, Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation;

    invoke-static {p1}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v0

    check-cast v0, LX/18V;

    invoke-static {p1}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-static {p1}, LX/2Qv;->b(LX/0QB;)LX/2Qv;

    move-result-object p0

    check-cast p0, LX/2Qv;

    invoke-direct {p2, v0, v1, p0}, Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation;-><init>(LX/18V;LX/0lC;LX/2Qv;)V

    .line 408456
    move-object v0, p2

    .line 408457
    goto/16 :goto_0

    .line 408458
    :pswitch_6
    new-instance v2, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;

    invoke-static {p1}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v3

    check-cast v3, LX/18V;

    invoke-static {p1}, LX/2Qw;->a(LX/0QB;)LX/2Qw;

    move-result-object v4

    check-cast v4, LX/2Qw;

    invoke-static {p1}, LX/2Qy;->a(LX/0QB;)LX/2Qy;

    move-result-object v5

    check-cast v5, LX/2Qy;

    invoke-static {p1}, LX/2Qz;->a(LX/0QB;)LX/2Qz;

    move-result-object v6

    check-cast v6, LX/2Qz;

    invoke-static {p1}, LX/2R0;->b(LX/0QB;)LX/2R0;

    move-result-object v7

    check-cast v7, LX/2R0;

    const/16 v8, 0x11ea

    invoke-static {p1, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;-><init>(LX/18V;LX/2Qw;LX/2Qy;LX/2Qz;LX/2R0;LX/0Or;)V

    .line 408459
    move-object v0, v2

    .line 408460
    goto/16 :goto_0

    .line 408461
    :pswitch_7
    new-instance v1, LX/2R4;

    const/16 v0, 0xb83

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 408462
    new-instance v3, LX/2R5;

    invoke-static {p1}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v0

    check-cast v0, LX/0sO;

    invoke-direct {v3, v0}, LX/2R5;-><init>(LX/0sO;)V

    .line 408463
    move-object v0, v3

    .line 408464
    check-cast v0, LX/2R5;

    invoke-direct {v1, v2, v0}, LX/2R4;-><init>(LX/0Or;LX/2R5;)V

    .line 408465
    move-object v0, v1

    .line 408466
    goto/16 :goto_0

    .line 408467
    :pswitch_8
    new-instance v1, LX/2R6;

    const/16 v0, 0xb83

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 408468
    new-instance v0, LX/2R7;

    invoke-direct {v0}, LX/2R7;-><init>()V

    .line 408469
    move-object v0, v0

    .line 408470
    move-object v0, v0

    .line 408471
    check-cast v0, LX/2R7;

    invoke-direct {v1, v2, v0}, LX/2R6;-><init>(LX/0Or;LX/2R7;)V

    .line 408472
    move-object v0, v1

    .line 408473
    goto/16 :goto_0

    .line 408474
    :pswitch_9
    new-instance v1, LX/2R8;

    const/16 v0, 0xb83

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 408475
    new-instance v0, LX/2R9;

    invoke-direct {v0}, LX/2R9;-><init>()V

    .line 408476
    move-object v0, v0

    .line 408477
    move-object v0, v0

    .line 408478
    check-cast v0, LX/2R9;

    invoke-direct {v1, v2, v0}, LX/2R8;-><init>(LX/0Or;LX/2R9;)V

    .line 408479
    move-object v0, v1

    .line 408480
    goto/16 :goto_0

    .line 408481
    :pswitch_a
    new-instance v1, LX/2RA;

    invoke-static {p1}, LX/2Qt;->a(LX/0QB;)LX/2Qt;

    move-result-object v0

    check-cast v0, LX/2Qt;

    invoke-direct {v1, v0}, LX/2RA;-><init>(LX/2Qt;)V

    .line 408482
    move-object v0, v1

    .line 408483
    goto/16 :goto_0

    .line 408484
    :pswitch_b
    new-instance v1, LX/2RF;

    const/16 v0, 0xb83

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p1}, LX/2R0;->b(LX/0QB;)LX/2R0;

    move-result-object v0

    check-cast v0, LX/2R0;

    invoke-direct {v1, v2, v0}, LX/2RF;-><init>(LX/0Or;LX/2R0;)V

    .line 408485
    move-object v0, v1

    .line 408486
    goto/16 :goto_0

    .line 408487
    :pswitch_c
    new-instance v1, LX/2RG;

    invoke-static {p1}, LX/2Qy;->a(LX/0QB;)LX/2Qy;

    move-result-object v0

    check-cast v0, LX/2Qy;

    const/16 v2, 0xb83

    invoke-static {p1, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/2RG;-><init>(LX/2Qy;LX/0Or;)V

    .line 408488
    move-object v0, v1

    .line 408489
    goto/16 :goto_0

    .line 408490
    :pswitch_d
    new-instance v1, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation;

    const/16 v0, 0xb79

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 408491
    new-instance v0, LX/2RH;

    invoke-direct {v0}, LX/2RH;-><init>()V

    .line 408492
    move-object v0, v0

    .line 408493
    move-object v0, v0

    .line 408494
    check-cast v0, LX/2RH;

    invoke-direct {v1, v2, v0}, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation;-><init>(LX/0Or;LX/2RH;)V

    .line 408495
    move-object v0, v1

    .line 408496
    goto/16 :goto_0

    .line 408497
    :pswitch_e
    new-instance v1, LX/2RI;

    const/16 v0, 0xb83

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p1}, LX/2Qz;->a(LX/0QB;)LX/2Qz;

    move-result-object v0

    check-cast v0, LX/2Qz;

    invoke-direct {v1, v2, v0}, LX/2RI;-><init>(LX/0Or;LX/2Qz;)V

    .line 408498
    move-object v0, v1

    .line 408499
    goto/16 :goto_0

    .line 408500
    :pswitch_f
    new-instance v1, LX/2Rk;

    const/16 v0, 0xb83

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 408501
    new-instance v0, LX/2Rm;

    invoke-direct {v0}, LX/2Rm;-><init>()V

    .line 408502
    move-object v0, v0

    .line 408503
    move-object v0, v0

    .line 408504
    check-cast v0, LX/2Rm;

    invoke-direct {v1, v2, v0}, LX/2Rk;-><init>(LX/0Or;LX/2Rm;)V

    .line 408505
    move-object v0, v1

    .line 408506
    goto/16 :goto_0

    .line 408507
    :pswitch_10
    new-instance v1, LX/2Rn;

    const/16 v0, 0xb83

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 408508
    new-instance v0, LX/2Ro;

    invoke-direct {v0}, LX/2Ro;-><init>()V

    .line 408509
    move-object v0, v0

    .line 408510
    move-object v0, v0

    .line 408511
    check-cast v0, LX/2Ro;

    invoke-direct {v1, v2, v0}, LX/2Rn;-><init>(LX/0Or;LX/2Ro;)V

    .line 408512
    move-object v0, v1

    .line 408513
    goto/16 :goto_0

    .line 408514
    :pswitch_11
    new-instance v1, LX/2Rp;

    const/16 v0, 0xb83

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 408515
    new-instance v0, LX/2Rq;

    invoke-direct {v0}, LX/2Rq;-><init>()V

    .line 408516
    move-object v0, v0

    .line 408517
    move-object v0, v0

    .line 408518
    check-cast v0, LX/2Rq;

    invoke-direct {v1, v2, v0}, LX/2Rp;-><init>(LX/0Or;LX/2Rq;)V

    .line 408519
    move-object v0, v1

    .line 408520
    goto/16 :goto_0

    .line 408521
    :pswitch_12
    new-instance v2, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;

    invoke-static {p1}, LX/2Rs;->a(LX/0QB;)LX/2Rs;

    move-result-object v0

    check-cast v0, LX/2Rs;

    invoke-static {p1}, LX/2QR;->a(LX/0QB;)LX/2QR;

    move-result-object v1

    check-cast v1, LX/2QR;

    invoke-direct {v2, v0, v1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;-><init>(LX/2Rs;LX/2QR;)V

    .line 408522
    move-object v0, v2

    .line 408523
    goto/16 :goto_0

    .line 408524
    :pswitch_13
    new-instance v1, LX/2Rt;

    invoke-static {p1}, LX/2QR;->a(LX/0QB;)LX/2QR;

    move-result-object v0

    check-cast v0, LX/2QR;

    invoke-direct {v1, v0}, LX/2Rt;-><init>(LX/2QR;)V

    .line 408525
    move-object v0, v1

    .line 408526
    goto/16 :goto_0

    .line 408527
    :pswitch_14
    new-instance v1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;

    invoke-static {p1}, LX/2Rs;->a(LX/0QB;)LX/2Rs;

    move-result-object v0

    check-cast v0, LX/2Rs;

    invoke-direct {v1, v0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;-><init>(LX/2Rs;)V

    .line 408528
    move-object v0, v1

    .line 408529
    goto/16 :goto_0

    .line 408530
    :pswitch_15
    new-instance v1, LX/2Ru;

    invoke-static {p1}, LX/2QQ;->a(LX/0QB;)LX/2QQ;

    move-result-object v0

    check-cast v0, LX/2QQ;

    invoke-direct {v1, v0}, LX/2Ru;-><init>(LX/2QQ;)V

    .line 408531
    move-object v0, v1

    .line 408532
    goto/16 :goto_0

    .line 408533
    :pswitch_16
    new-instance v1, LX/2Rv;

    invoke-static {p1}, LX/2QQ;->a(LX/0QB;)LX/2QQ;

    move-result-object v0

    check-cast v0, LX/2QQ;

    invoke-direct {v1, v0}, LX/2Rv;-><init>(LX/2QQ;)V

    .line 408534
    move-object v0, v1

    .line 408535
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 408536
    const/16 v0, 0x17

    return v0
.end method
