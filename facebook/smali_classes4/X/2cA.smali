.class public LX/2cA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 440333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;LX/1oS;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/1oS;",
            ">;",
            "LX/1oS;",
            ")I"
        }
    .end annotation

    .prologue
    .line 440334
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 440335
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oS;

    .line 440336
    invoke-static {v0, p1}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440337
    :goto_1
    return v1

    .line 440338
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 440339
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public static a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;
    .locals 1

    .prologue
    .line 440340
    invoke-interface {p0}, LX/1oV;->b()LX/1Fd;

    move-result-object v0

    if-nez v0, :cond_0

    .line 440341
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 440342
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, LX/1oV;->b()LX/1Fd;

    move-result-object v0

    invoke-interface {v0}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->fromIconName(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 440343
    invoke-interface {p0}, LX/1oR;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 440344
    :goto_0
    return-object v0

    .line 440345
    :cond_0
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    invoke-interface {p0}, LX/1oR;->c()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/facebook/privacy/model/PrivacyParameter;

    invoke-virtual {v0, v2, v3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/PrivacyParameter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 440346
    :catch_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/privacy/model/PrivacyParameter;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 440347
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440348
    const-string v0, ""

    .line 440349
    :goto_0
    return-object v0

    .line 440350
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    sget-object v1, LX/8QT;->CUSTOM:LX/8QT;

    invoke-virtual {v1}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 440351
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    goto :goto_0

    .line 440352
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->deny:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 440353
    sget-object v0, LX/8QT;->CUSTOM:LX/8QT;

    invoke-virtual {v0}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 440354
    :cond_2
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    invoke-static {v0}, LX/2cA;->a(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v0

    .line 440355
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 440356
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    sget-object v1, LX/8QR;->ALL_FRIENDS:LX/8QR;

    invoke-virtual {v1}, LX/8QR;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 440357
    sget-object v0, LX/8QT;->ALL_FRIENDS:LX/8QT;

    invoke-virtual {v0}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 440358
    :cond_3
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    sget-object v1, LX/8QR;->FRIENDS_OF_FRIENDS:LX/8QR;

    invoke-virtual {v1}, LX/8QR;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 440359
    sget-object v0, LX/8QT;->FRIENDS_OF_FRIENDS:LX/8QT;

    invoke-virtual {v0}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 440360
    :cond_4
    sget-object v0, LX/8QT;->CUSTOM:LX/8QT;

    invoke-virtual {v0}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/util/HashSet;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 440361
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 440362
    if-nez p0, :cond_0

    move-object v0, v1

    .line 440363
    :goto_0
    return-object v0

    .line 440364
    :cond_0
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 440365
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 440366
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 440367
    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 440368
    new-instance v1, LX/0UE;

    invoke-direct {v1}, LX/0UE;-><init>()V

    .line 440369
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 440370
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0UE;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 440371
    :cond_0
    return-object v1
.end method

.method public static a(LX/1oS;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 440372
    invoke-static {p0}, LX/2cA;->i(LX/1oS;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 440373
    :goto_0
    return v0

    .line 440374
    :cond_0
    invoke-interface {p0}, LX/1oS;->j()LX/0Px;

    move-result-object v2

    .line 440375
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x285feb

    if-eq v0, v2, :cond_1

    move v0, v1

    .line 440376
    goto :goto_0

    .line 440377
    :cond_1
    invoke-static {p0}, LX/2cA;->a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;

    move-result-object v0

    .line 440378
    if-nez v0, :cond_2

    move v0, v1

    .line 440379
    goto :goto_0

    .line 440380
    :cond_2
    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    sget-object v1, LX/8QT;->CUSTOM:LX/8QT;

    invoke-virtual {v1}, LX/8QT;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/1oS;LX/1oS;)Z
    .locals 2

    .prologue
    .line 440381
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 440382
    const/4 v0, 0x1

    .line 440383
    :goto_0
    return v0

    .line 440384
    :cond_0
    invoke-static {p0, p1}, LX/2cA;->b(LX/1oU;LX/1oU;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 440385
    const/4 v0, 0x0

    goto :goto_0

    .line 440386
    :cond_1
    invoke-interface {p0}, LX/1oS;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v0

    invoke-interface {p1}, LX/1oS;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/1oU;LX/1oU;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 440317
    invoke-static {p0}, LX/2cA;->a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;

    move-result-object v2

    .line 440318
    invoke-static {p1}, LX/2cA;->a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;

    move-result-object v3

    .line 440319
    iget-object v4, v2, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    iget-object v5, v3, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 440320
    invoke-static {v2}, LX/2cA;->a(Lcom/facebook/privacy/model/PrivacyParameter;)Ljava/lang/String;

    move-result-object v4

    .line 440321
    invoke-static {v3}, LX/2cA;->a(Lcom/facebook/privacy/model/PrivacyParameter;)Ljava/lang/String;

    move-result-object v5

    .line 440322
    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 440323
    :cond_0
    :goto_0
    return v0

    .line 440324
    :cond_1
    sget-object v5, LX/8QT;->CUSTOM:LX/8QT;

    invoke-virtual {v5}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    move v0, v1

    .line 440325
    goto :goto_0

    .line 440326
    :cond_2
    iget-object v4, v2, Lcom/facebook/privacy/model/PrivacyParameter;->deny:Ljava/lang/String;

    invoke-static {v4}, LX/2cA;->a(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v4

    iget-object v5, v3, Lcom/facebook/privacy/model/PrivacyParameter;->deny:Ljava/lang/String;

    invoke-static {v5}, LX/2cA;->a(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v5

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    .line 440327
    if-eqz v4, :cond_0

    .line 440328
    iget-object v2, v2, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    invoke-static {v2}, LX/2cA;->a(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v2

    iget-object v3, v3, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    invoke-static {v3}, LX/2cA;->a(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    .line 440329
    if-eqz v2, :cond_0

    move v0, v1

    .line 440330
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPrivacyScope;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 440331
    if-nez p0, :cond_1

    .line 440332
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->fromIconName(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;LX/1oS;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "LX/1oS;",
            ">;",
            "LX/1oS;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 440313
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oS;

    .line 440314
    invoke-static {v0, p1}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440315
    const/4 v0, 0x1

    .line 440316
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/1oS;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 440306
    invoke-static {p0}, LX/2cA;->i(LX/1oS;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 440307
    :cond_0
    :goto_0
    return v0

    .line 440308
    :cond_1
    invoke-static {p0}, LX/2cA;->a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;

    move-result-object v2

    .line 440309
    if-eqz v2, :cond_0

    .line 440310
    iget-object v3, v2, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    sget-object v4, LX/8QT;->ALL_FRIENDS:LX/8QT;

    invoke-virtual {v4}, LX/8QT;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 440311
    goto :goto_0

    .line 440312
    :cond_2
    iget-object v3, v2, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    sget-object v4, LX/8QT;->CUSTOM:LX/8QT;

    invoke-virtual {v4}, LX/8QT;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, v2, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    sget-object v3, LX/8QR;->ALL_FRIENDS:LX/8QR;

    invoke-virtual {v3}, LX/8QR;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static b(LX/1oU;LX/1oU;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 440296
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 440297
    :cond_0
    :goto_0
    return v0

    .line 440298
    :cond_1
    if-eqz p1, :cond_2

    if-nez p0, :cond_3

    :cond_2
    move v0, v1

    .line 440299
    goto :goto_0

    .line 440300
    :cond_3
    invoke-interface {p0}, LX/1oU;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/1oU;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p0}, LX/1oU;->b()LX/1Fd;

    move-result-object v2

    invoke-interface {p1}, LX/1oU;->b()LX/1Fd;

    move-result-object v3

    const/4 v4, 0x1

    .line 440301
    if-nez v2, :cond_6

    if-nez v3, :cond_6

    .line 440302
    :cond_4
    :goto_1
    move v2, v4

    .line 440303
    if-eqz v2, :cond_5

    invoke-interface {p0}, LX/1oU;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/1oU;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0

    .line 440304
    :cond_6
    if-eqz v2, :cond_7

    if-eqz v3, :cond_7

    invoke-interface {v2}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 440305
    :cond_7
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static c(LX/1oS;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 440290
    invoke-static {p0}, LX/2cA;->i(LX/1oS;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 440291
    :cond_0
    :goto_0
    return v0

    .line 440292
    :cond_1
    invoke-static {p0}, LX/2cA;->j(LX/1oS;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 440293
    invoke-static {p0}, LX/2cA;->a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;

    move-result-object v1

    .line 440294
    if-eqz v1, :cond_0

    .line 440295
    iget-object v2, v1, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    sget-object v3, LX/8QT;->CUSTOM:LX/8QT;

    invoke-virtual {v3}, LX/8QT;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, v1, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    sget-object v2, LX/8QR;->ALL_FRIENDS:LX/8QR;

    invoke-virtual {v2}, LX/8QR;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static d(LX/1oS;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 440283
    invoke-static {p0}, LX/2cA;->j(LX/1oS;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 440284
    :cond_0
    :goto_0
    return v0

    .line 440285
    :cond_1
    invoke-static {p0}, LX/2cA;->a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;

    move-result-object v1

    .line 440286
    if-eqz v1, :cond_0

    .line 440287
    iget-object v0, v1, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    sget-object v2, LX/8QT;->SELF:LX/8QT;

    invoke-virtual {v2}, LX/8QT;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 440288
    const/4 v0, 0x1

    goto :goto_0

    .line 440289
    :cond_2
    iget-object v0, v1, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    sget-object v1, LX/8QT;->CUSTOM:LX/8QT;

    invoke-virtual {v1}, LX/8QT;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static e(LX/1oS;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 440272
    invoke-static {p0}, LX/2cA;->j(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 440273
    :goto_0
    return v0

    .line 440274
    :cond_0
    invoke-static {p0}, LX/2cA;->i(LX/1oS;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 440275
    goto :goto_0

    .line 440276
    :cond_1
    invoke-interface {p0}, LX/1oS;->j()LX/0Px;

    move-result-object v2

    .line 440277
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x285feb

    if-eq v0, v2, :cond_2

    move v0, v1

    .line 440278
    goto :goto_0

    .line 440279
    :cond_2
    invoke-static {p0}, LX/2cA;->a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;

    move-result-object v0

    .line 440280
    if-nez v0, :cond_3

    move v0, v1

    .line 440281
    goto :goto_0

    .line 440282
    :cond_3
    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    sget-object v1, LX/8QT;->CUSTOM:LX/8QT;

    invoke-virtual {v1}, LX/8QT;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static f(LX/1oS;)Z
    .locals 1

    .prologue
    .line 440271
    invoke-static {p0}, LX/2cA;->c(LX/1oS;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/2cA;->e(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(LX/1oS;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 440264
    invoke-static {p0}, LX/2cA;->a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;

    move-result-object v2

    .line 440265
    if-eqz v2, :cond_1

    iget-object v3, v2, Lcom/facebook/privacy/model/PrivacyParameter;->settings:Lcom/facebook/privacy/model/PrivacyParameter$Settings;

    if-eqz v3, :cond_1

    iget-object v2, v2, Lcom/facebook/privacy/model/PrivacyParameter;->settings:Lcom/facebook/privacy/model/PrivacyParameter$Settings;

    iget-boolean v2, v2, Lcom/facebook/privacy/model/PrivacyParameter$Settings;->noTagExpansion:Z

    if-eqz v2, :cond_1

    .line 440266
    :cond_0
    :goto_0
    return v0

    .line 440267
    :cond_1
    invoke-interface {p0}, LX/1oS;->l()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, LX/1oS;->l()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-gt v2, v0, :cond_3

    :cond_2
    move v0, v1

    .line 440268
    goto :goto_0

    .line 440269
    :cond_3
    invoke-interface {p0}, LX/1oS;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 440270
    goto :goto_0
.end method

.method public static h(LX/1oS;)Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;
    .locals 6

    .prologue
    .line 440215
    new-instance v0, LX/4YK;

    invoke-direct {v0}, LX/4YK;-><init>()V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440216
    iput-object v1, v0, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440217
    move-object v0, v0

    .line 440218
    invoke-static {p0}, LX/2cA;->a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;

    move-result-object v1

    .line 440219
    if-nez v1, :cond_0

    .line 440220
    invoke-virtual {v0}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    .line 440221
    :goto_0
    return-object v0

    .line 440222
    :cond_0
    iget-object v2, v1, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    sget-object v3, LX/8QT;->SELF:LX/8QT;

    invoke-virtual {v3}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 440223
    invoke-virtual {v0}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    goto :goto_0

    .line 440224
    :cond_1
    iget-object v2, v1, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    sget-object v3, LX/8QT;->ALL_FRIENDS:LX/8QT;

    invoke-virtual {v3}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 440225
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440226
    iput-object v1, v0, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440227
    move-object v0, v0

    .line 440228
    invoke-virtual {v0}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    goto :goto_0

    .line 440229
    :cond_2
    iget-object v2, v1, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    sget-object v3, LX/8QT;->FRIENDS_OF_FRIENDS:LX/8QT;

    invoke-virtual {v3}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 440230
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440231
    iput-object v1, v0, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440232
    move-object v0, v0

    .line 440233
    invoke-virtual {v0}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    goto :goto_0

    .line 440234
    :cond_3
    iget-object v2, v1, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    sget-object v3, LX/8QT;->EVERYONE:LX/8QT;

    invoke-virtual {v3}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 440235
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440236
    iput-object v1, v0, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440237
    move-object v0, v0

    .line 440238
    invoke-virtual {v0}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    goto :goto_0

    .line 440239
    :cond_4
    iget-object v2, v1, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    sget-object v3, LX/8QT;->CUSTOM:LX/8QT;

    invoke-virtual {v3}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 440240
    const-class v1, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const-string v2, "unexpected_privacy_json_when_convert_option_to_row_input: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p0}, LX/1oS;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 440241
    invoke-virtual {v0}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    goto :goto_0

    .line 440242
    :cond_5
    iget-object v2, v1, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    invoke-static {v2}, LX/2cA;->a(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v2

    .line 440243
    iget-object v1, v1, Lcom/facebook/privacy/model/PrivacyParameter;->deny:Ljava/lang/String;

    invoke-static {v1}, LX/2cA;->a(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v1

    .line 440244
    sget-object v3, LX/8QR;->SOME_FRIENDS:LX/8QR;

    invoke-virtual {v3}, LX/8QR;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 440245
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440246
    iput-object v3, v0, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440247
    sget-object v3, LX/8QR;->SOME_FRIENDS:LX/8QR;

    invoke-virtual {v3}, LX/8QR;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 440248
    :cond_6
    :goto_1
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 440249
    iput-object v2, v0, LX/4YK;->b:LX/0Px;

    .line 440250
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 440251
    iput-object v1, v0, LX/4YK;->d:LX/0Px;

    .line 440252
    invoke-static {p0}, LX/2cA;->g(LX/1oS;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 440253
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 440254
    iput-object v1, v0, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 440255
    :cond_7
    invoke-virtual {v0}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    goto/16 :goto_0

    .line 440256
    :cond_8
    sget-object v3, LX/8QR;->ALL_FRIENDS:LX/8QR;

    invoke-virtual {v3}, LX/8QR;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 440257
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440258
    iput-object v3, v0, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440259
    sget-object v3, LX/8QR;->ALL_FRIENDS:LX/8QR;

    invoke-virtual {v3}, LX/8QR;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 440260
    :cond_9
    sget-object v3, LX/8QR;->FRIENDS_OF_FRIENDS:LX/8QR;

    invoke-virtual {v3}, LX/8QR;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 440261
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440262
    iput-object v3, v0, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 440263
    sget-object v3, LX/8QR;->FRIENDS_OF_FRIENDS:LX/8QR;

    invoke-virtual {v3}, LX/8QR;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private static i(LX/1oS;)Z
    .locals 1

    .prologue
    .line 440214
    invoke-interface {p0}, LX/1oS;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/1oS;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static j(LX/1oS;)Z
    .locals 1

    .prologue
    .line 440213
    invoke-interface {p0}, LX/1oS;->x_()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/1oS;->x_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
