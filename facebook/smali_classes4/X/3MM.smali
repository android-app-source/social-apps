.class public final LX/3MM;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:LX/0sv;

.field private static final l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 554361
    new-instance v0, LX/0U1;

    const-string v1, "threadid"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MM;->a:LX/0U1;

    .line 554362
    new-instance v0, LX/0U1;

    const-string v1, "state"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MM;->b:LX/0U1;

    .line 554363
    new-instance v0, LX/0U1;

    const-string v1, "ts"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MM;->c:LX/0U1;

    .line 554364
    new-instance v0, LX/0U1;

    const-string v1, "group_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MM;->d:LX/0U1;

    .line 554365
    new-instance v0, LX/0U1;

    const-string v1, "recipient_ids"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MM;->e:LX/0U1;

    .line 554366
    new-instance v0, LX/0U1;

    const-string v1, "me_bubble_color"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MM;->f:LX/0U1;

    .line 554367
    new-instance v0, LX/0U1;

    const-string v1, "other_bubble_color"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MM;->g:LX/0U1;

    .line 554368
    new-instance v0, LX/0U1;

    const-string v1, "wallpaper_color"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MM;->h:LX/0U1;

    .line 554369
    new-instance v0, LX/0U1;

    const-string v1, "custom_like_emoji"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MM;->i:LX/0U1;

    .line 554370
    sget-object v0, LX/3MM;->a:LX/0U1;

    sget-object v1, LX/3MM;->b:LX/0U1;

    sget-object v2, LX/3MM;->c:LX/0U1;

    sget-object v3, LX/3MM;->d:LX/0U1;

    sget-object v4, LX/3MM;->e:LX/0U1;

    sget-object v5, LX/3MM;->f:LX/0U1;

    sget-object v6, LX/3MM;->g:LX/0U1;

    sget-object v7, LX/3MM;->h:LX/0U1;

    sget-object v8, LX/3MM;->i:LX/0U1;

    invoke-static/range {v0 .. v8}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3MM;->j:LX/0Px;

    .line 554371
    new-instance v0, LX/0su;

    sget-object v1, LX/3MM;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    .line 554372
    sput-object v0, LX/3MM;->k:LX/0sv;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3MM;->l:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 554373
    const-string v0, "threads_table"

    sget-object v1, LX/3MM;->j:LX/0Px;

    sget-object v2, LX/3MM;->l:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 554374
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 554375
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 554376
    const-string v0, "threads_table"

    const-string v1, "INDEX_THREAD_ID"

    sget-object v2, LX/3MM;->b:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x45ea8891

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x6dad3d19

    invoke-static {v0}, LX/03h;->a(I)V

    .line 554377
    return-void
.end method
