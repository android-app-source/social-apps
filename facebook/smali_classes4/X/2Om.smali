.class public LX/2Om;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field private final a:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "LX/6el;",
            "LX/DdN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 403136
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2Om;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 403137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403138
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/2Om;->a:LX/01J;

    .line 403139
    return-void
.end method

.method public static a(LX/0QB;)LX/2Om;
    .locals 7

    .prologue
    .line 403140
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 403141
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 403142
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 403143
    if-nez v1, :cond_0

    .line 403144
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 403145
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 403146
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 403147
    sget-object v1, LX/2Om;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 403148
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 403149
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 403150
    :cond_1
    if-nez v1, :cond_4

    .line 403151
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 403152
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 403153
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    .line 403154
    new-instance v0, LX/2Om;

    invoke-direct {v0}, LX/2Om;-><init>()V

    .line 403155
    move-object v1, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 403156
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 403157
    if-nez v1, :cond_2

    .line 403158
    sget-object v0, LX/2Om;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Om;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 403159
    :goto_1
    if-eqz v0, :cond_3

    .line 403160
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 403161
    :goto_3
    check-cast v0, LX/2Om;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 403162
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 403163
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 403164
    :catchall_1
    move-exception v0

    .line 403165
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 403166
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 403167
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 403168
    :cond_2
    :try_start_8
    sget-object v0, LX/2Om;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Om;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private declared-synchronized a()V
    .locals 1

    .prologue
    .line 403169
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Om;->a:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403170
    monitor-exit p0

    return-void

    .line 403171
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static f(LX/2Om;LX/6el;)LX/DdN;
    .locals 2

    .prologue
    .line 403172
    iget-object v0, p0, LX/2Om;->a:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdN;

    .line 403173
    if-nez v0, :cond_0

    .line 403174
    new-instance v0, LX/DdN;

    invoke-direct {v0}, LX/DdN;-><init>()V

    .line 403175
    iget-object v1, p0, LX/2Om;->a:LX/01J;

    invoke-virtual {v1, p1, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403176
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/6el;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V
    .locals 3

    .prologue
    .line 403177
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/2Om;->f(LX/2Om;LX/6el;)LX/DdN;

    move-result-object v0

    .line 403178
    iput-object p2, v0, LX/DdN;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 403179
    iput-wide p3, v0, LX/DdN;->d:J

    .line 403180
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/DdN;->b:Z

    .line 403181
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/DdN;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403182
    monitor-exit p0

    return-void

    .line 403183
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/6el;)Z
    .locals 1

    .prologue
    .line 403184
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/2Om;->f(LX/2Om;LX/6el;)LX/DdN;

    move-result-object v0

    iget-boolean v0, v0, LX/DdN;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/6el;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 1

    .prologue
    .line 403185
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/2Om;->f(LX/2Om;LX/6el;)LX/DdN;

    move-result-object v0

    iget-object v0, v0, LX/DdN;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(LX/6el;)J
    .locals 2

    .prologue
    .line 403186
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/2Om;->f(LX/2Om;LX/6el;)LX/DdN;

    move-result-object v0

    iget-wide v0, v0, LX/DdN;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 403187
    invoke-direct {p0}, LX/2Om;->a()V

    .line 403188
    return-void
.end method

.method public final declared-synchronized d(LX/6el;)V
    .locals 2

    .prologue
    .line 403189
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/2Om;->f(LX/2Om;LX/6el;)LX/DdN;

    move-result-object v0

    .line 403190
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/DdN;->b:Z

    .line 403191
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/DdN;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403192
    monitor-exit p0

    return-void

    .line 403193
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e(LX/6el;)V
    .locals 1

    .prologue
    .line 403194
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Om;->a:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403195
    monitor-exit p0

    return-void

    .line 403196
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
