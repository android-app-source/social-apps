.class public LX/2iI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g8;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:Lcom/facebook/widget/listview/BetterListView;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0fx;",
            "Landroid/widget/AbsListView$OnScrollListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/widget/listview/BetterListView;)V
    .locals 3

    .prologue
    .line 451169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 451170
    iput-object p1, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    .line 451171
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2iI;->b:Ljava/util/Map;

    .line 451172
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    const v1, 0x7f0d0043

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/listview/BetterListView;->setTag(ILjava/lang/Object;)V

    .line 451173
    return-void
.end method


# virtual methods
.method public final A()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 451168
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public final B()Z
    .locals 1

    .prologue
    .line 451165
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    .line 451166
    iget-boolean p0, v0, Lcom/facebook/widget/listview/BetterListView;->v:Z

    move v0, p0

    .line 451167
    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 451163
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 451164
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 451160
    new-instance v0, Lcom/facebook/widget/listview/ListViewProxy$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/widget/listview/ListViewProxy$1;-><init>(LX/2iI;II)V

    .line 451161
    invoke-virtual {p0, v0}, LX/2iI;->a(Ljava/lang/Runnable;)V

    .line 451162
    return-void
.end method

.method public final a(IIII)V
    .locals 1

    .prologue
    .line 451158
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/widget/listview/BetterListView;->setPadding(IIII)V

    .line 451159
    return-void
.end method

.method public final a(LX/0fu;)V
    .locals 1

    .prologue
    .line 451156
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setOnDrawListenerTo(LX/0fu;)V

    .line 451157
    return-void
.end method

.method public final a(LX/0fx;)V
    .locals 2

    .prologue
    .line 451126
    if-eqz p1, :cond_0

    new-instance v0, LX/2ig;

    invoke-direct {v0, p1, p0}, LX/2ig;-><init>(LX/0fx;LX/2iI;)V

    .line 451127
    :goto_0
    iget-object v1, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 451128
    return-void

    .line 451129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1St;)V
    .locals 2

    .prologue
    .line 451152
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/625;

    invoke-direct {v1, p0, p1}, LX/625;-><init>(LX/2iI;LX/1St;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 451153
    return-void
.end method

.method public final a(LX/1Z7;)V
    .locals 2

    .prologue
    .line 451149
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/626;

    invoke-direct {v1, p0, p1}, LX/626;-><init>(LX/2iI;LX/1Z7;)V

    .line 451150
    iput-object v1, v0, Lcom/facebook/widget/listview/BetterListView;->y:LX/2i6;

    .line 451151
    return-void
.end method

.method public final a(LX/2i4;)V
    .locals 1

    .prologue
    .line 451147
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->a(LX/2i4;)V

    .line 451148
    return-void
.end method

.method public final a(LX/2ii;)V
    .locals 2

    .prologue
    .line 451143
    if-nez p1, :cond_0

    .line 451144
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 451145
    :goto_0
    return-void

    .line 451146
    :cond_0
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/2ij;

    invoke-direct {v1, p0, p1}, LX/2ij;-><init>(LX/2iI;LX/2ii;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method public final a(LX/3TV;)V
    .locals 2

    .prologue
    .line 451139
    if-nez p1, :cond_0

    .line 451140
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 451141
    :goto_0
    return-void

    .line 451142
    :cond_0
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/3TW;

    invoke-direct {v1, p0, p1}, LX/3TW;-><init>(LX/2iI;LX/3TV;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    goto :goto_0
.end method

.method public final a(LX/62F;)V
    .locals 2

    .prologue
    .line 451136
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/627;

    invoke-direct {v1, p0, p1}, LX/627;-><init>(LX/2iI;LX/62F;)V

    .line 451137
    iput-object v1, v0, Lcom/facebook/widget/listview/BetterListView;->z:LX/2i9;

    .line 451138
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 451134
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 451135
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 451132
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 451133
    return-void
.end method

.method public final a(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 451130
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 451131
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 451179
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->removeHeaderView(Landroid/view/View;)Z

    .line 451180
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 451177
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 451178
    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 451206
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 451207
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 451204
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->post(Ljava/lang/Runnable;)Z

    .line 451205
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 451202
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setClipToPadding(Z)V

    .line 451203
    return-void
.end method

.method public final b()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 451201
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    return-object v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 451199
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setDividerHeight(I)V

    .line 451200
    return-void
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 451198
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This function is not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(LX/0fu;)V
    .locals 1

    .prologue
    .line 451196
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->a(LX/0fu;)V

    .line 451197
    return-void
.end method

.method public final b(LX/0fx;)V
    .locals 2

    .prologue
    .line 451192
    new-instance v0, LX/2ig;

    invoke-direct {v0, p1, p0}, LX/2ig;-><init>(LX/0fx;LX/2iI;)V

    .line 451193
    iget-object v1, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 451194
    iget-object v1, p0, LX/2iI;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451195
    return-void
.end method

.method public final b(LX/2i4;)V
    .locals 1

    .prologue
    .line 451190
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->b(LX/2i4;)V

    .line 451191
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 451188
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->removeFooterView(Landroid/view/View;)Z

    .line 451189
    return-void
.end method

.method public final b(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 451186
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 451187
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 451184
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setVerticalScrollBarEnabled(Z)V

    .line 451185
    return-void
.end method

.method public final c(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 451183
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public final c(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 451182
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/widget/listview/BetterListView;
    .locals 1

    .prologue
    .line 451181
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    return-object v0
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 451154
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/widget/listview/BetterListView;->smoothScrollToPositionFromTop(II)V

    .line 451155
    return-void
.end method

.method public final c(LX/0fx;)V
    .locals 2

    .prologue
    .line 451174
    iget-object v0, p0, LX/2iI;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$OnScrollListener;

    .line 451175
    iget-object v1, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/BetterListView;->b(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 451176
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 451071
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 451072
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 451098
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getHeight()I

    move-result v0

    return v0
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 451096
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->smoothScrollToPosition(I)V

    .line 451097
    return-void
.end method

.method public final d(II)V
    .locals 1

    .prologue
    .line 451092
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getClipToPadding()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451093
    :goto_0
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/widget/listview/BetterListView;->setSelectionFromTop(II)V

    .line 451094
    return-void

    .line 451095
    :cond_0
    invoke-virtual {p0}, LX/2iI;->g()I

    move-result v0

    sub-int/2addr p2, v0

    goto :goto_0
.end method

.method public final d(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 451090
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;)V

    .line 451091
    return-void
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 451088
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setBroadcastInteractionChanges(Z)V

    .line 451089
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 451087
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getPaddingBottom()I

    move-result v0

    return v0
.end method

.method public final e(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 451086
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final e(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 451084
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;)V

    .line 451085
    return-void
.end method

.method public final f(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 451083
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final f(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 451081
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 451082
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 451078
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    .line 451079
    iget p0, v0, Lcom/facebook/widget/listview/BetterListView;->j:I

    move v0, p0

    .line 451080
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 451077
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getPaddingTop()I

    move-result v0

    return v0
.end method

.method public final g(I)V
    .locals 1

    .prologue
    .line 451075
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setSelection(I)V

    .line 451076
    return-void
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 451074
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getPaddingLeft()I

    move-result v0

    return v0
.end method

.method public final h(I)J
    .locals 2

    .prologue
    .line 451073
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->getItemIdAtPosition(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 451070
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getPaddingRight()I

    move-result v0

    return v0
.end method

.method public final ih_()Landroid/view/View;
    .locals 1

    .prologue
    .line 451113
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 451125
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getClipToPadding()Z

    move-result v0

    return v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 451123
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->b()V

    .line 451124
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 451122
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->isAtBottom()Z

    move-result v0

    return v0
.end method

.method public final m()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 451118
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/4 v4, 0x3

    const/4 v7, 0x0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 451119
    iget-object v1, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/BetterListView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 451120
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 451121
    return-void
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 451117
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->a()Z

    move-result v0

    return v0
.end method

.method public final o()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 451116
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 451115
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getChildCount()I

    move-result v0

    return v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 451114
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v0

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 451099
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getLastVisiblePosition()I

    move-result v0

    return v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 451110
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 451111
    const/4 v0, 0x0

    .line 451112
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 451109
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getHeaderViewsCount()I

    move-result v0

    return v0
.end method

.method public final u()I
    .locals 1

    .prologue
    .line 451108
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getFooterViewsCount()I

    move-result v0

    return v0
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 451106
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->setSelectionAfterHeaderView()V

    .line 451107
    return-void
.end method

.method public final w()V
    .locals 1

    .prologue
    .line 451104
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->c()V

    .line 451105
    return-void
.end method

.method public final x()V
    .locals 1

    .prologue
    .line 451102
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->destroyDrawingCache()V

    .line 451103
    return-void
.end method

.method public final y()I
    .locals 1

    .prologue
    .line 451101
    const/4 v0, 0x0

    return v0
.end method

.method public final z()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 451100
    iget-object v0, p0, LX/2iI;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getOffsetsOfVisibleItems()LX/0P1;

    move-result-object v0

    return-object v0
.end method
