.class public LX/2lj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2lk;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0si;

.field private final c:LX/0t8;

.field private final d:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final e:LX/0sT;


# direct methods
.method public constructor <init>(LX/0t8;LX/0si;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;)V
    .locals 1

    .prologue
    .line 458324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 458325
    iput-object p1, p0, LX/2lj;->c:LX/0t8;

    .line 458326
    iput-object p2, p0, LX/2lj;->b:LX/0si;

    .line 458327
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2lj;->a:Ljava/util/Map;

    .line 458328
    iput-object p3, p0, LX/2lj;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 458329
    iput-object p4, p0, LX/2lj;->e:LX/0sT;

    .line 458330
    return-void
.end method

.method public constructor <init>(LX/0t8;LX/0si;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/cache/ConsistencyConfig;",
            "LX/0si;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0sT;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 458317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 458318
    iput-object p1, p0, LX/2lj;->c:LX/0t8;

    .line 458319
    iput-object p2, p0, LX/2lj;->b:LX/0si;

    .line 458320
    iput-object p3, p0, LX/2lj;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 458321
    iput-object p4, p0, LX/2lj;->e:LX/0sT;

    .line 458322
    iget-object v0, p0, LX/2lj;->b:LX/0si;

    invoke-interface {v0, p5}, LX/0si;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/2lj;->a:Ljava/util/Map;

    .line 458323
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 458313
    if-eqz p1, :cond_0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 458314
    :cond_0
    :goto_0
    return v0

    .line 458315
    :cond_1
    instance-of v1, p1, Ljava/lang/Enum;

    if-eqz v1, :cond_2

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    check-cast p1, Ljava/lang/Enum;

    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 458316
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 458312
    iget-object v0, p0, LX/2lj;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized a(LX/0jT;)Z
    .locals 2

    .prologue
    .line 458265
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/3Bo;

    iget-object v1, p0, LX/2lj;->c:LX/0t8;

    invoke-direct {v0, p0, v1}, LX/3Bo;-><init>(LX/2lj;LX/0t8;)V

    .line 458266
    invoke-virtual {v0, p1}, LX/1jx;->b(LX/0jT;)LX/0jT;

    .line 458267
    iget-boolean v0, v0, LX/3Bo;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 458268
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 458303
    invoke-static {p1, p3}, LX/2lj;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 458304
    const/4 v0, 0x0

    .line 458305
    :goto_0
    return v0

    .line 458306
    :cond_0
    iget-object v0, p0, LX/2lj;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 458307
    if-nez v0, :cond_1

    .line 458308
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 458309
    iget-object v1, p0, LX/2lj;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458310
    :cond_1
    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458311
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/util/List;)Z
    .locals 4

    .prologue
    .line 458295
    monitor-enter p0

    const/4 v1, 0x0

    .line 458296
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 458297
    instance-of v3, v0, LX/0jT;

    if-eqz v3, :cond_1

    .line 458298
    check-cast v0, LX/0jT;

    invoke-virtual {p0, v0}, LX/2lj;->a(LX/0jT;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 458299
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 458300
    goto :goto_0

    .line 458301
    :cond_0
    monitor-exit p0

    return v1

    .line 458302
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final declared-synchronized b(LX/0jT;)LX/0jT;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 458286
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2lj;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x31001f    # 4.499983E-39f

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 458287
    new-instance v0, LX/3d2;

    const-class v1, LX/16i;

    new-instance v2, LX/3d3;

    iget-object v3, p0, LX/2lj;->a:Ljava/util/Map;

    invoke-direct {v2, v3}, LX/3d3;-><init>(Ljava/util/Map;)V

    invoke-direct {v0, v1, v2}, LX/3d2;-><init>(Ljava/lang/Class;LX/3d4;)V

    .line 458288
    invoke-virtual {v0, p1}, LX/3d2;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jT;

    .line 458289
    iget-object v1, p0, LX/2lj;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x31001f    # 4.499983E-39f

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 458290
    monitor-exit p0

    return-object v0

    .line 458291
    :catch_0
    move-exception v0

    .line 458292
    :try_start_1
    iget-object v1, p0, LX/2lj;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x31001f    # 4.499983E-39f

    const/4 v3, 0x3

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 458293
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 458294
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 458283
    iget-object v0, p0, LX/2lj;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 458284
    iget-object v0, p0, LX/2lj;->b:LX/0si;

    iget-object v1, p0, LX/2lj;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, LX/0si;->a(Ljava/util/Map;)V

    .line 458285
    :cond_0
    return-void
.end method

.method public final declared-synchronized c(LX/0jT;)LX/0jT;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 458278
    monitor-enter p0

    :try_start_0
    instance-of v0, p1, LX/16i;

    if-eqz v0, :cond_0

    .line 458279
    new-instance v0, LX/3d3;

    iget-object v1, p0, LX/2lj;->a:Ljava/util/Map;

    invoke-direct {v0, v1}, LX/3d3;-><init>(Ljava/util/Map;)V

    .line 458280
    check-cast p1, LX/16i;

    invoke-virtual {v0, p1}, LX/3d3;->a(LX/16i;)LX/16i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p1

    .line 458281
    :cond_0
    monitor-exit p0

    return-object p1

    .line 458282
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 458277
    iget-object v0, p0, LX/2lj;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/3Bq;
    .locals 1

    .prologue
    .line 458276
    new-instance v0, LX/3Bp;

    invoke-direct {v0, p0}, LX/3Bp;-><init>(LX/2lk;)V

    return-object v0
.end method

.method public final d(LX/0jT;)Z
    .locals 3

    .prologue
    .line 458269
    instance-of v0, p1, Lcom/facebook/graphql/modelutil/FragmentModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2lj;->e:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 458270
    check-cast p1, Lcom/facebook/graphql/modelutil/FragmentModel;

    new-instance v0, LX/JaO;

    invoke-direct {v0}, LX/JaO;-><init>()V

    iget-object v1, p0, LX/2lj;->c:LX/0t8;

    iget-object v2, p0, LX/2lj;->a:Ljava/util/Map;

    invoke-static {p1, v0, v1, v2}, LX/4VC;->a(Lcom/facebook/graphql/modelutil/FragmentModel;LX/4Ud;LX/0t8;Ljava/util/Map;)Z

    move-result v0

    .line 458271
    :goto_0
    return v0

    .line 458272
    :cond_0
    new-instance v0, LX/2lm;

    iget-object v1, p0, LX/2lj;->a:Ljava/util/Map;

    invoke-direct {v0, v1}, LX/2lm;-><init>(Ljava/util/Map;)V

    .line 458273
    invoke-virtual {v0, p1}, LX/1jx;->b(LX/0jT;)LX/0jT;

    .line 458274
    iget-boolean v1, v0, LX/2lm;->c:Z

    move v0, v1

    .line 458275
    goto :goto_0
.end method
