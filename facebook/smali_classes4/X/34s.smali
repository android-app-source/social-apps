.class public LX/34s;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/37M;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/37N;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 496001
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/34s;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/37N;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 495998
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 495999
    iput-object p1, p0, LX/34s;->b:LX/0Ot;

    .line 496000
    return-void
.end method

.method public static a(LX/0QB;)LX/34s;
    .locals 4

    .prologue
    .line 496002
    const-class v1, LX/34s;

    monitor-enter v1

    .line 496003
    :try_start_0
    sget-object v0, LX/34s;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 496004
    sput-object v2, LX/34s;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 496005
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496006
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 496007
    new-instance v3, LX/34s;

    const/16 p0, 0x93f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/34s;-><init>(LX/0Ot;)V

    .line 496008
    move-object v0, v3

    .line 496009
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 496010
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/34s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496011
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 496012
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 495990
    check-cast p2, LX/37L;

    .line 495991
    iget-object v0, p0, LX/34s;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/37L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 495992
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 495993
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 495994
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    .line 495995
    invoke-static {p0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/37O;

    move-result-object p0

    .line 495996
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p2

    invoke-static {p1, v1}, LX/37P;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const p2, 0x7f0b0050

    invoke-virtual {v1, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const/4 p2, 0x1

    invoke-virtual {v1, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v1

    invoke-static {p1, p0}, LX/37Q;->a(Landroid/content/Context;LX/37O;)I

    move-result p0

    invoke-virtual {v1, p0}, LX/1ne;->m(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->b()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 495997
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 495988
    invoke-static {}, LX/1dS;->b()V

    .line 495989
    const/4 v0, 0x0

    return-object v0
.end method
