.class public final LX/3DM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 534178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 534179
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 534180
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 534181
    invoke-static {p0, p1}, LX/3DM;->b(LX/15w;LX/186;)I

    move-result v1

    .line 534182
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 534183
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 534184
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 534185
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 534186
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/3DM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 534187
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 534188
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 534189
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 534190
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 534191
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 534192
    :goto_0
    return v1

    .line 534193
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_6

    .line 534194
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 534195
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 534196
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 534197
    const-string v8, "is_seen"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 534198
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 534199
    :cond_1
    const-string v8, "node"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 534200
    invoke-static {p0, p1}, LX/3DN;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 534201
    :cond_2
    const-string v8, "suggesters"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 534202
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 534203
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_3

    .line 534204
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_3

    .line 534205
    invoke-static {p0, p1}, LX/84u;->b(LX/15w;LX/186;)I

    move-result v7

    .line 534206
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 534207
    :cond_3
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 534208
    goto :goto_1

    .line 534209
    :cond_4
    const-string v8, "tracking"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 534210
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 534211
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 534212
    :cond_6
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 534213
    if-eqz v0, :cond_7

    .line 534214
    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 534215
    :cond_7
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 534216
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 534217
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 534218
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 534219
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 534220
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 534221
    if-eqz v0, :cond_0

    .line 534222
    const-string v1, "is_seen"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 534223
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 534224
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 534225
    if-eqz v0, :cond_1

    .line 534226
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 534227
    invoke-static {p0, v0, p2, p3}, LX/3DN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 534228
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 534229
    if-eqz v0, :cond_3

    .line 534230
    const-string v1, "suggesters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 534231
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 534232
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 534233
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/84u;->a(LX/15i;ILX/0nX;)V

    .line 534234
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 534235
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 534236
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 534237
    if-eqz v0, :cond_4

    .line 534238
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 534239
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 534240
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 534241
    return-void
.end method
