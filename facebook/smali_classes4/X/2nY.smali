.class public LX/2nY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static m:LX/0Xm;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 464839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 464840
    const v0, 0x7f080f85

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2nY;->a:Ljava/lang/String;

    .line 464841
    const v0, 0x7f080f8a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2nY;->b:Ljava/lang/String;

    .line 464842
    const v0, 0x7f081fe5

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2nY;->c:Ljava/lang/String;

    .line 464843
    const v0, 0x7f080f8b

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2nY;->d:Ljava/lang/String;

    .line 464844
    const v0, 0x7f080f7b

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2nY;->e:Ljava/lang/String;

    .line 464845
    const v0, 0x7f080f7c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2nY;->f:Ljava/lang/String;

    .line 464846
    const v0, 0x7f080f83

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2nY;->g:Ljava/lang/String;

    .line 464847
    const v0, 0x7f080f80

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2nY;->h:Ljava/lang/String;

    .line 464848
    const v0, 0x7f080f81

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2nY;->i:Ljava/lang/String;

    .line 464849
    const v0, 0x7f080020

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2nY;->j:Ljava/lang/String;

    .line 464850
    const v0, 0x7f080021

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2nY;->k:Ljava/lang/String;

    .line 464851
    const v0, 0x7f080f84

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2nY;->l:Ljava/lang/String;

    .line 464852
    return-void
.end method

.method public static a(LX/2na;)LX/2lu;
    .locals 2

    .prologue
    .line 464853
    sget-object v0, LX/2nZ;->b:[I

    invoke-virtual {p0}, LX/2na;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 464854
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected value for FriendRequestResponse"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 464855
    :pswitch_0
    sget-object v0, LX/2lu;->ACCEPTED:LX/2lu;

    .line 464856
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, LX/2lu;->REJECTED:LX/2lu;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/2nY;
    .locals 4

    .prologue
    .line 464817
    const-class v1, LX/2nY;

    monitor-enter v1

    .line 464818
    :try_start_0
    sget-object v0, LX/2nY;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 464819
    sput-object v2, LX/2nY;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 464820
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464821
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 464822
    new-instance p0, LX/2nY;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/2nY;-><init>(Landroid/content/res/Resources;)V

    .line 464823
    move-object v0, p0

    .line 464824
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 464825
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2nY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 464826
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 464827
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/2na;Z)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 2

    .prologue
    .line 464829
    sget-object v0, LX/2nZ;->b:[I

    invoke-virtual {p0}, LX/2na;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 464830
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected value for FriendRequestResponse"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 464831
    :pswitch_0
    if-eqz p1, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 464832
    :goto_0
    return-object v0

    .line 464833
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    .line 464834
    :pswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/2lu;Z)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 464835
    sget-object v0, LX/2nZ;->a:[I

    invoke-virtual {p1}, LX/2lu;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 464836
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 464837
    :pswitch_0
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/2nY;->a:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v0, p0, LX/2nY;->b:Ljava/lang/String;

    goto :goto_0

    .line 464838
    :pswitch_1
    if-eqz p2, :cond_1

    iget-object v0, p0, LX/2nY;->c:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/2nY;->d:Ljava/lang/String;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 464828
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/2nY;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2nY;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public final d(Z)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 464816
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/2nY;->f:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 464815
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/2nY;->h:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2nY;->i:Ljava/lang/String;

    goto :goto_0
.end method
