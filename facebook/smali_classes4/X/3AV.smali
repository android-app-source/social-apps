.class public final enum LX/3AV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3AV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3AV;

.field public static final enum ABOVE:LX/3AV;

.field public static final enum BELOW:LX/3AV;

.field public static final enum CENTER:LX/3AV;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 525318
    new-instance v0, LX/3AV;

    const-string v1, "ABOVE"

    invoke-direct {v0, v1, v2}, LX/3AV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3AV;->ABOVE:LX/3AV;

    .line 525319
    new-instance v0, LX/3AV;

    const-string v1, "BELOW"

    invoke-direct {v0, v1, v3}, LX/3AV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3AV;->BELOW:LX/3AV;

    .line 525320
    new-instance v0, LX/3AV;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v4}, LX/3AV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3AV;->CENTER:LX/3AV;

    .line 525321
    const/4 v0, 0x3

    new-array v0, v0, [LX/3AV;

    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    aput-object v1, v0, v2

    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    aput-object v1, v0, v3

    sget-object v1, LX/3AV;->CENTER:LX/3AV;

    aput-object v1, v0, v4

    sput-object v0, LX/3AV;->$VALUES:[LX/3AV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 525317
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3AV;
    .locals 1

    .prologue
    .line 525316
    const-class v0, LX/3AV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3AV;

    return-object v0
.end method

.method public static values()[LX/3AV;
    .locals 1

    .prologue
    .line 525315
    sget-object v0, LX/3AV;->$VALUES:[LX/3AV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3AV;

    return-object v0
.end method
