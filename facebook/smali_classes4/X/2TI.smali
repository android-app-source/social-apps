.class public LX/2TI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2TI;


# instance fields
.field private a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 414007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414008
    iput-object p1, p0, LX/2TI;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 414009
    return-void
.end method

.method public static a(LX/0QB;)LX/2TI;
    .locals 4

    .prologue
    .line 413994
    sget-object v0, LX/2TI;->b:LX/2TI;

    if-nez v0, :cond_1

    .line 413995
    const-class v1, LX/2TI;

    monitor-enter v1

    .line 413996
    :try_start_0
    sget-object v0, LX/2TI;->b:LX/2TI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413997
    if-eqz v2, :cond_0

    .line 413998
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 413999
    new-instance p0, LX/2TI;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/2TI;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 414000
    move-object v0, p0

    .line 414001
    sput-object v0, LX/2TI;->b:LX/2TI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414002
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 414003
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 414004
    :cond_1
    sget-object v0, LX/2TI;->b:LX/2TI;

    return-object v0

    .line 414005
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 414006
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 413993
    iget-object v0, p0, LX/2TI;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2TR;->f:LX/0Tn;

    const-string v2, "OFF"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 413989
    iget-object v0, p0, LX/2TI;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 413990
    sget-object v1, LX/2TR;->f:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 413991
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 413992
    return-void
.end method
