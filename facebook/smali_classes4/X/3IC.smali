.class public LX/3IC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3IF;

.field private final b:LX/3IE;

.field private final c:LX/3ID;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3IA;LX/3IB;)V
    .locals 2

    .prologue
    .line 545627
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 545628
    new-instance v0, LX/3ID;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    invoke-direct {v0, v1}, LX/3ID;-><init>(I)V

    iput-object v0, p0, LX/3IC;->c:LX/3ID;

    .line 545629
    new-instance v0, LX/3IE;

    iget-object v1, p0, LX/3IC;->c:LX/3ID;

    invoke-direct {v0, p2, v1}, LX/3IE;-><init>(LX/3IA;LX/3ID;)V

    iput-object v0, p0, LX/3IC;->b:LX/3IE;

    .line 545630
    new-instance v0, LX/3IF;

    invoke-direct {v0, p1, p3}, LX/3IF;-><init>(Landroid/content/Context;LX/3IB;)V

    iput-object v0, p0, LX/3IC;->a:LX/3IF;

    .line 545631
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 545632
    iget-object v0, p0, LX/3IC;->a:LX/3IF;

    const/4 v1, 0x0

    .line 545633
    sget-object v2, LX/2qn;->UNSET:LX/2qn;

    iput-object v2, v0, LX/3IF;->d:LX/2qn;

    .line 545634
    iget-object v2, v0, LX/3IF;->a:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 545635
    sget-object v2, LX/6Lg;->a:[I

    iget-object v3, v0, LX/3IF;->d:LX/2qn;

    invoke-virtual {v3}, LX/2qn;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 545636
    :goto_0
    :pswitch_0
    move v0, v1

    .line 545637
    if-nez v0, :cond_0

    iget-object v0, p0, LX/3IC;->b:LX/3IE;

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 545638
    iget-object v1, v0, LX/3IE;->a:LX/3IA;

    if-nez v1, :cond_2

    .line 545639
    :goto_1
    move v0, v3

    .line 545640
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 545641
    :pswitch_1
    const/4 v1, 0x1

    goto :goto_0

    .line 545642
    :cond_2
    iget-boolean v1, v0, LX/3IE;->e:Z

    if-nez v1, :cond_b

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 545643
    if-eqz v1, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-eqz v1, :cond_3

    .line 545644
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object p1

    .line 545645
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->setAction(I)V

    .line 545646
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 545647
    and-int/lit16 v1, v1, 0xff

    packed-switch v1, :pswitch_data_1

    :cond_4
    :goto_4
    :pswitch_2
    move v3, v2

    .line 545648
    goto :goto_1

    .line 545649
    :pswitch_3
    iget-object v1, v0, LX/3IE;->b:LX/3ID;

    invoke-virtual {v1, p1}, LX/3ID;->a(Landroid/view/MotionEvent;)Z

    .line 545650
    iput-boolean v2, v0, LX/3IE;->e:Z

    .line 545651
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, v0, LX/3IE;->f:I

    .line 545652
    iput-boolean v3, v0, LX/3IE;->d:Z

    .line 545653
    invoke-static {v0}, LX/3IE;->b(LX/3IE;)V

    .line 545654
    invoke-static {v0, p1}, LX/3IE;->b(LX/3IE;Landroid/view/MotionEvent;)V

    .line 545655
    :pswitch_4
    iget-object v1, v0, LX/3IE;->b:LX/3ID;

    invoke-virtual {v1, p1}, LX/3ID;->a(Landroid/view/MotionEvent;)Z

    .line 545656
    invoke-static {v0, p1}, LX/3IE;->b(LX/3IE;Landroid/view/MotionEvent;)V

    .line 545657
    iget v1, v0, LX/3IE;->f:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    .line 545658
    if-ltz v1, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 545659
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    .line 545660
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    .line 545661
    iget v4, v0, LX/3IE;->j:F

    sub-float/2addr v4, v3

    .line 545662
    iget v5, v0, LX/3IE;->k:F

    sub-float/2addr v5, v1

    .line 545663
    iget-object p0, v0, LX/3IE;->b:LX/3ID;

    invoke-virtual {p0}, LX/3ID;->a()Z

    move-result p0

    if-eqz p0, :cond_6

    .line 545664
    iget-boolean p0, v0, LX/3IE;->c:Z

    if-nez p0, :cond_5

    .line 545665
    iget-object p0, v0, LX/3IE;->a:LX/3IA;

    invoke-interface {p0}, LX/3IA;->b()V

    .line 545666
    iput-boolean v2, v0, LX/3IE;->c:Z

    .line 545667
    :cond_5
    iget-object p0, v0, LX/3IE;->a:LX/3IA;

    invoke-interface {p0, v4, v5}, LX/3IA;->a(FF)V

    .line 545668
    :cond_6
    iput v3, v0, LX/3IE;->j:F

    .line 545669
    iput v1, v0, LX/3IE;->k:F

    goto :goto_4

    .line 545670
    :pswitch_5
    iput-boolean v3, v0, LX/3IE;->e:Z

    .line 545671
    iput-boolean v3, v0, LX/3IE;->c:Z

    .line 545672
    invoke-static {v0, p1}, LX/3IE;->b(LX/3IE;Landroid/view/MotionEvent;)V

    .line 545673
    const/4 v1, -0x1

    iput v1, v0, LX/3IE;->f:I

    .line 545674
    iget-object v1, v0, LX/3IE;->b:LX/3ID;

    invoke-virtual {v1}, LX/3ID;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 545675
    iget-object v1, v0, LX/3IE;->g:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 545676
    iget-object v1, v0, LX/3IE;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v1

    iput v1, v0, LX/3IE;->h:F

    .line 545677
    iget-object v1, v0, LX/3IE;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    iput v1, v0, LX/3IE;->i:F

    .line 545678
    iget-object v1, v0, LX/3IE;->a:LX/3IA;

    iget v2, v0, LX/3IE;->h:F

    iget v4, v0, LX/3IE;->i:F

    invoke-interface {v1, v2, v4}, LX/3IA;->b(FF)V

    .line 545679
    :goto_5
    iget-object v1, v0, LX/3IE;->b:LX/3ID;

    invoke-virtual {v1, p1}, LX/3ID;->a(Landroid/view/MotionEvent;)Z

    goto/16 :goto_1

    .line 545680
    :cond_7
    iget-boolean v1, v0, LX/3IE;->d:Z

    if-eqz v1, :cond_8

    .line 545681
    iget-object v1, v0, LX/3IE;->a:LX/3IA;

    invoke-interface {v1, v4, v4}, LX/3IA;->b(FF)V

    goto :goto_5

    .line 545682
    :cond_8
    iget-object v1, v0, LX/3IE;->a:LX/3IA;

    invoke-interface {v1}, LX/3IA;->a()V

    goto :goto_5

    .line 545683
    :pswitch_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    .line 545684
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v4

    .line 545685
    iget v5, v0, LX/3IE;->f:I

    if-ne v4, v5, :cond_9

    .line 545686
    if-nez v1, :cond_a

    move v1, v2

    .line 545687
    :goto_6
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, v0, LX/3IE;->f:I

    .line 545688
    :cond_9
    iget v1, v0, LX/3IE;->f:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    .line 545689
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    iput v4, v0, LX/3IE;->j:F

    .line 545690
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iput v1, v0, LX/3IE;->k:F

    .line 545691
    invoke-static {v0}, LX/3IE;->b(LX/3IE;)V

    .line 545692
    iput-boolean v2, v0, LX/3IE;->d:Z

    goto/16 :goto_1

    :cond_a
    move v1, v3

    .line 545693
    goto :goto_6

    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_2
        :pswitch_6
    .end packed-switch
.end method
