.class public final LX/2kS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2kT;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/2kR;

.field public final synthetic c:LX/2k0;

.field private volatile d:Z


# direct methods
.method public constructor <init>(LX/2k0;Ljava/lang/String;LX/2kR;)V
    .locals 1

    .prologue
    .line 456040
    iput-object p1, p0, LX/2kS;->c:LX/2k0;

    iput-object p2, p0, LX/2kS;->a:Ljava/lang/String;

    iput-object p3, p0, LX/2kS;->b:LX/2kR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456041
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2kS;->d:Z

    return-void
.end method


# virtual methods
.method public final close()V
    .locals 3

    .prologue
    .line 456042
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2kS;->d:Z

    .line 456043
    iget-object v0, p0, LX/2kS;->c:LX/2k0;

    iget-object v1, p0, LX/2kS;->a:Ljava/lang/String;

    iget-object v2, p0, LX/2kS;->b:LX/2kR;

    invoke-static {v0, v1, v2}, LX/2k0;->b(LX/2k0;Ljava/lang/String;LX/2kR;)V

    .line 456044
    return-void
.end method

.method public final finalize()V
    .locals 5

    .prologue
    .line 456045
    iget-boolean v0, p0, LX/2kS;->d:Z

    if-nez v0, :cond_0

    .line 456046
    sget-object v0, LX/2k0;->a:Ljava/lang/String;

    const-string v1, "Finalizing EdgeStore.CallbackHandle for session %s that was not manually closed"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/2kS;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 456047
    invoke-virtual {p0}, LX/2kS;->close()V

    .line 456048
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 456049
    return-void
.end method
