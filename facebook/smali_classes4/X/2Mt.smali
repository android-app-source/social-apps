.class public LX/2Mt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile k:LX/2Mt;


# instance fields
.field public final b:LX/18V;

.field private final c:LX/0TD;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:LX/0Xl;

.field private final f:LX/0Yb;

.field public final g:LX/2Mu;

.field public final h:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final j:LX/2Mv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 397783
    const-class v0, LX/2Mt;

    sput-object v0, LX/2Mt;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/18V;LX/0TD;Ljava/util/concurrent/Executor;LX/0Xl;LX/2Mu;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Mv;)V
    .locals 4
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForLightweightTaskHandlerThread;
        .end annotation
    .end param
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 397784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397785
    iput-object p1, p0, LX/2Mt;->b:LX/18V;

    .line 397786
    iput-object p2, p0, LX/2Mt;->c:LX/0TD;

    .line 397787
    iput-object p3, p0, LX/2Mt;->d:Ljava/util/concurrent/Executor;

    .line 397788
    iput-object p4, p0, LX/2Mt;->e:LX/0Xl;

    .line 397789
    iput-object p5, p0, LX/2Mt;->g:LX/2Mu;

    .line 397790
    iput-object p6, p0, LX/2Mt;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 397791
    iput-object p7, p0, LX/2Mt;->j:LX/2Mv;

    .line 397792
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0x7

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, LX/0QN;->a(J)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/2Mt;->h:LX/0QI;

    .line 397793
    iget-object v0, p0, LX/2Mt;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2N1;->c:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 397794
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 397795
    :cond_0
    iget-object v0, p0, LX/2Mt;->e:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    sget-object v1, LX/0aY;->x:Ljava/lang/String;

    new-instance v2, LX/2N2;

    invoke-direct {v2, p0}, LX/2N2;-><init>(LX/2Mt;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2Mt;->f:LX/0Yb;

    .line 397796
    iget-object v0, p0, LX/2Mt;->f:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 397797
    return-void

    .line 397798
    :cond_1
    invoke-static {v0}, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;->a(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    .line 397799
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;

    .line 397800
    invoke-virtual {v0}, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 397801
    iget-object v2, v0, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;->threadKey:Ljava/lang/String;

    invoke-static {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 397802
    if-eqz v2, :cond_2

    .line 397803
    invoke-virtual {v0}, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;->b()V

    .line 397804
    iget-object v3, p0, LX/2Mt;->h:LX/0QI;

    invoke-interface {v3, v2, v0}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2Mt;
    .locals 11

    .prologue
    .line 397805
    sget-object v0, LX/2Mt;->k:LX/2Mt;

    if-nez v0, :cond_1

    .line 397806
    const-class v1, LX/2Mt;

    monitor-enter v1

    .line 397807
    :try_start_0
    sget-object v0, LX/2Mt;->k:LX/2Mt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 397808
    if-eqz v2, :cond_0

    .line 397809
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 397810
    new-instance v3, LX/2Mt;

    invoke-static {v0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v4

    check-cast v4, LX/18V;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-static {v0}, LX/2Mu;->a(LX/0QB;)LX/2Mu;

    move-result-object v8

    check-cast v8, LX/2Mu;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Mv;->b(LX/0QB;)LX/2Mv;

    move-result-object v10

    check-cast v10, LX/2Mv;

    invoke-direct/range {v3 .. v10}, LX/2Mt;-><init>(LX/18V;LX/0TD;Ljava/util/concurrent/Executor;LX/0Xl;LX/2Mu;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Mv;)V

    .line 397811
    move-object v0, v3

    .line 397812
    sput-object v0, LX/2Mt;->k:LX/2Mt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397813
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 397814
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 397815
    :cond_1
    sget-object v0, LX/2Mt;->k:LX/2Mt;

    return-object v0

    .line 397816
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 397817
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/2Mt;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 3

    .prologue
    .line 397818
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Mt;->j:LX/2Mv;

    invoke-virtual {v0, p1}, LX/2Mv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397819
    :cond_0
    :goto_0
    return-void

    .line 397820
    :cond_1
    iget-object v0, p0, LX/2Mt;->h:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;

    .line 397821
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 397822
    :cond_2
    iget-object v0, p0, LX/2Mt;->c:LX/0TD;

    new-instance v1, LX/CMy;

    invoke-direct {v1, p0, p1}, LX/CMy;-><init>(LX/2Mt;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 397823
    new-instance v1, LX/CMz;

    invoke-direct {v1, p0, p1}, LX/CMz;-><init>(LX/2Mt;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    iget-object v2, p0, LX/2Mt;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method


# virtual methods
.method public final finalize()V
    .locals 1

    .prologue
    .line 397824
    iget-object v0, p0, LX/2Mt;->f:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 397825
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 397826
    return-void
.end method
