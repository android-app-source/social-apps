.class public final LX/3MP;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0sv;

.field private static final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 554399
    new-instance v0, LX/0U1;

    const-string v1, "address"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MP;->a:LX/0U1;

    .line 554400
    new-instance v0, LX/0U1;

    const-string v1, "not_business"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MP;->b:LX/0U1;

    .line 554401
    sget-object v0, LX/3MP;->a:LX/0U1;

    sget-object v1, LX/3MP;->b:LX/0U1;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3MP;->c:LX/0Px;

    .line 554402
    new-instance v0, LX/0su;

    sget-object v1, LX/3MP;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    .line 554403
    sput-object v0, LX/3MP;->d:LX/0sv;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3MP;->e:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 554397
    const-string v0, "sms_business_address_list"

    sget-object v1, LX/3MP;->c:LX/0Px;

    sget-object v2, LX/3MP;->e:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 554398
    return-void
.end method
