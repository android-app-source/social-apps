.class public final LX/36J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/36K;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;)V
    .locals 0

    .prologue
    .line 498742
    iput-object p1, p0, LX/36J;->a:Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<",
            "LX/1Pf;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 498743
    iget-object v0, p0, LX/36J;->a:Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 498744
    iget-object v0, p0, LX/36J;->a:Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 498745
    iget-object v0, p0, LX/36J;->a:Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->h:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, LX/36J;->a:Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    iget-object v1, v1, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->d:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 498746
    iget-object v0, p0, LX/36J;->a:Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->e:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 498747
    iget-object v0, p0, LX/36J;->a:Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->f:Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 498748
    iget-object v0, p0, LX/36J;->a:Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->g:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 498749
    return-void
.end method
