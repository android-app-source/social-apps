.class public abstract LX/2B6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 378681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 378682
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)LX/2B6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/2B6;"
        }
    .end annotation
.end method

.method public final a(LX/0lJ;LX/0my;LX/2Ay;)LX/2Bd;
    .locals 3

    .prologue
    .line 378683
    invoke-virtual {p2, p1, p3}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 378684
    new-instance v1, LX/2Bd;

    .line 378685
    iget-object v2, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v2

    .line 378686
    invoke-virtual {p0, v2, v0}, LX/2B6;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)LX/2B6;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/2Bd;-><init>(Lcom/fasterxml/jackson/databind/JsonSerializer;LX/2B6;)V

    return-object v1
.end method

.method public final a(Ljava/lang/Class;LX/0my;LX/2Ay;)LX/2Bd;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0my;",
            "LX/2Ay;",
            ")",
            "LX/2Bd;"
        }
    .end annotation

    .prologue
    .line 378687
    invoke-virtual {p2, p1, p3}, LX/0my;->a(Ljava/lang/Class;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 378688
    new-instance v1, LX/2Bd;

    invoke-virtual {p0, p1, v0}, LX/2B6;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)LX/2B6;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/2Bd;-><init>(Lcom/fasterxml/jackson/databind/JsonSerializer;LX/2B6;)V

    return-object v1
.end method

.method public abstract a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method
