.class public LX/2BH;
.super Ljava/io/IOException;
.source ""


# instance fields
.field public final tigonError:Lcom/facebook/tigon/tigonapi/TigonError;


# direct methods
.method public constructor <init>(Lcom/facebook/tigon/tigonapi/TigonError;)V
    .locals 6

    .prologue
    .line 378996
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 378997
    iget-object v0, p1, Lcom/facebook/tigon/tigonapi/TigonError;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/tigon/tigonapi/TigonError;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 378998
    :goto_0
    const-string v3, "TigonError(%d): %s(%d)%s%s"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p1, Lcom/facebook/tigon/tigonapi/TigonError;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v2, p1, Lcom/facebook/tigon/tigonapi/TigonError;->b:Ljava/lang/String;

    aput-object v2, v4, v1

    const/4 v1, 0x2

    iget v2, p1, Lcom/facebook/tigon/tigonapi/TigonError;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    const/4 v2, 0x3

    if-eqz v0, :cond_1

    const-string v1, " "

    :goto_1
    aput-object v1, v4, v2

    const/4 v1, 0x4

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/tigon/tigonapi/TigonError;->d:Ljava/lang/String;

    :goto_2
    aput-object v0, v4, v1

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 378999
    move-object v0, v0

    .line 379000
    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 379001
    iput-object p1, p0, LX/2BH;->tigonError:Lcom/facebook/tigon/tigonapi/TigonError;

    .line 379002
    return-void

    :cond_0
    move v0, v2

    .line 379003
    goto :goto_0

    .line 379004
    :cond_1
    const-string v1, ""

    goto :goto_1

    :cond_2
    const-string v0, ""

    goto :goto_2
.end method
