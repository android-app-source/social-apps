.class public LX/2U9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:Lcom/facebook/confirmation/model/AccountConfirmationData;

.field private final c:LX/0if;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;Lcom/facebook/confirmation/model/AccountConfirmationData;LX/0if;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "Lcom/facebook/confirmation/model/AccountConfirmationData;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415398
    iput-object p1, p0, LX/2U9;->a:LX/0Zb;

    .line 415399
    iput-object p2, p0, LX/2U9;->b:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 415400
    iput-object p3, p0, LX/2U9;->c:LX/0if;

    .line 415401
    iput-object p4, p0, LX/2U9;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 415402
    iput-object p5, p0, LX/2U9;->e:LX/0Or;

    .line 415403
    return-void
.end method

.method public static a(Lcom/facebook/fbservice/service/ServiceException;)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 415404
    if-nez p0, :cond_0

    move v0, v1

    .line 415405
    :goto_0
    return v0

    .line 415406
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 415407
    if-nez v0, :cond_1

    move v0, v1

    .line 415408
    goto :goto_0

    .line 415409
    :cond_1
    const-string v2, "result"

    invoke-virtual {v0, v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 415410
    if-nez v0, :cond_2

    move v0, v1

    .line 415411
    goto :goto_0

    .line 415412
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2U9;
    .locals 6

    .prologue
    .line 415413
    new-instance v0, LX/2U9;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, Lcom/facebook/confirmation/model/AccountConfirmationData;->a(LX/0QB;)Lcom/facebook/confirmation/model/AccountConfirmationData;

    move-result-object v2

    check-cast v2, Lcom/facebook/confirmation/model/AccountConfirmationData;

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v5, 0x15e7

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/2U9;-><init>(LX/0Zb;Lcom/facebook/confirmation/model/AccountConfirmationData;LX/0if;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V

    .line 415414
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 415421
    iget-object v0, p0, LX/2U9;->b:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 415422
    iget-boolean v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    move v0, v1

    .line 415423
    if-nez v0, :cond_0

    .line 415424
    :goto_0
    return-void

    .line 415425
    :cond_0
    iget-object v0, p0, LX/2U9;->c:LX/0if;

    sget-object v1, LX/0ig;->K:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 415415
    iget-object v0, p0, LX/2U9;->a:LX/0Zb;

    sget-object v1, LX/Eiw;->BACKGROUND_SMS_DETECTED:LX/Eiw;

    invoke-virtual {v1}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 415416
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    if-lez p1, :cond_0

    .line 415417
    const-string v1, "confirmation"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 415418
    const-string v1, "sms_detected"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 415419
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 415420
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 415390
    iget-object v0, p0, LX/2U9;->a:LX/0Zb;

    sget-object v1, LX/Eiw;->BACKGROUND_SMS_CONFIRMATION_ATTEMPT:LX/Eiw;

    invoke-virtual {v1}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 415391
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 415392
    const-string v1, "confirmation"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 415393
    const-string v1, "confirmation_code_count"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 415394
    const-string v1, "pending_contactpoint_count"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 415395
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 415396
    :cond_0
    return-void
.end method

.method public final a(LX/Eiw;Ljava/lang/String;LX/1rQ;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/1rQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 415380
    iget-object v0, p0, LX/2U9;->b:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 415381
    iget-boolean v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    move v0, v1

    .line 415382
    if-nez v0, :cond_0

    .line 415383
    :goto_0
    return-void

    .line 415384
    :cond_0
    if-nez p3, :cond_1

    .line 415385
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object p3

    .line 415386
    :cond_1
    const-string v0, "qpid"

    iget-object v1, p0, LX/2U9;->b:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 415387
    iget-object v2, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->d:Ljava/lang/String;

    move-object v1, v2

    .line 415388
    invoke-virtual {p3, v0, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 415389
    iget-object v0, p0, LX/2U9;->c:LX/0if;

    sget-object v1, LX/0ig;->K:LX/0ih;

    invoke-virtual {p1}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2, p3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 415348
    sget-object v0, LX/Eiw;->BACKGROUND_SMS_CONFIRMATION_SUCCESS:LX/Eiw;

    .line 415349
    iget-object v1, p0, LX/2U9;->a:LX/0Zb;

    invoke-virtual {v0}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 415350
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 415351
    const-string v1, "confirmation"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 415352
    const-string v1, "code_type"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 415353
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 415354
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 415355
    sget-object v0, LX/Eiw;->BACKGROUND_SMS_CONFIRMATION_FAILURE:LX/Eiw;

    .line 415356
    iget-object v1, p0, LX/2U9;->a:LX/0Zb;

    invoke-virtual {v0}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 415357
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 415358
    const-string v0, "confirmation"

    invoke-virtual {v1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 415359
    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 415360
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    goto :goto_0

    .line 415361
    :cond_0
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 415362
    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    .line 415363
    iget-object v0, p0, LX/2U9;->a:LX/0Zb;

    sget-object v1, LX/Eiw;->BACKGROUND_CONFIRMATION_START:LX/Eiw;

    invoke-virtual {v1}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 415364
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2U9;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 415365
    iget-object v2, p0, LX/2U9;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/3df;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    .line 415366
    if-eqz v2, :cond_0

    .line 415367
    iget-object v3, p0, LX/2U9;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    invoke-static {v0}, LX/3df;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 415368
    :cond_0
    move v0, v2

    .line 415369
    if-eqz v0, :cond_1

    .line 415370
    const-string v0, "confirmation"

    invoke-virtual {v1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 415371
    const-string v0, "sms_perm_granted"

    invoke-virtual {v1, v0, p1}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 415372
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 415373
    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 415374
    iget-object v0, p0, LX/2U9;->a:LX/0Zb;

    sget-object v1, LX/Eiw;->BACKGROUND_EMAIL_CONFIRMATION_FAILURE:LX/Eiw;

    invoke-virtual {v1}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 415375
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 415376
    const-string v1, "confirmation"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 415377
    const-string v1, "error_message"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 415378
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 415379
    :cond_0
    return-void
.end method
