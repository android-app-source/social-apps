.class public LX/2FE;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 386642
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;
    .locals 3

    .prologue
    .line 386643
    sget-object v0, LX/2FE;->a:Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    if-nez v0, :cond_1

    .line 386644
    const-class v1, LX/2FE;

    monitor-enter v1

    .line 386645
    :try_start_0
    sget-object v0, LX/2FE;->a:Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 386646
    if-eqz v2, :cond_0

    .line 386647
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 386648
    invoke-static {v0}, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;->a(LX/0QB;)Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;

    move-result-object p0

    check-cast p0, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;

    invoke-static {p0}, LX/2FB;->a(Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;)Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    move-result-object p0

    move-object v0, p0

    .line 386649
    sput-object v0, LX/2FE;->a:Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 386650
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 386651
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 386652
    :cond_1
    sget-object v0, LX/2FE;->a:Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    return-object v0

    .line 386653
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 386654
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 386655
    invoke-static {p0}, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;->a(LX/0QB;)Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;

    invoke-static {v0}, LX/2FB;->a(Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;)Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    move-result-object v0

    return-object v0
.end method
