.class public final LX/2ww;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field public final synthetic a:LX/2wI;

.field private final b:I


# direct methods
.method public constructor <init>(LX/2wI;I)V
    .locals 0

    iput-object p1, p0, LX/2ww;->a:LX/2wI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, LX/2ww;->b:I

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    const-string v0, "Expecting a valid IBinder"

    invoke-static {p2, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, LX/2ww;->a:LX/2wI;

    iget-object v1, v0, LX/2wI;->n:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/2ww;->a:LX/2wI;

    if-nez p2, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object v2, v2

    iput-object v2, v0, LX/2wI;->o:LX/2x4;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LX/2ww;->a:LX/2wI;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, LX/2ww;->b:I

    invoke-virtual {v0, v1, v2, v3}, LX/2wI;->a(ILandroid/os/Bundle;I)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    const-string v2, "com.google.android.gms.common.internal.IGmsServiceBroker"

    invoke-interface {p2, v2}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v2

    if-eqz v2, :cond_1

    instance-of v3, v2, LX/2x4;

    if-eqz v3, :cond_1

    check-cast v2, LX/2x4;

    goto :goto_0

    :cond_1
    new-instance v2, LX/2x1;

    invoke-direct {v2, p2}, LX/2x1;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 5

    iget-object v0, p0, LX/2ww;->a:LX/2wI;

    iget-object v1, v0, LX/2wI;->n:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/2ww;->a:LX/2wI;

    const/4 v2, 0x0

    iput-object v2, v0, LX/2wI;->o:LX/2x4;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LX/2ww;->a:LX/2wI;

    iget-object v0, v0, LX/2wI;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/2ww;->a:LX/2wI;

    iget-object v1, v1, LX/2wI;->a:Landroid/os/Handler;

    const/4 v2, 0x4

    iget v3, p0, LX/2ww;->b:I

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
