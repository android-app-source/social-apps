.class public LX/2Q6;
.super LX/18f;
.source ""

# interfaces
.implements LX/0vX;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/18f",
        "<TK;TV;>;",
        "LX/0vX",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "not needed in emulated source."
    .end annotation
.end field


# instance fields
.field private final transient a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<TV;>;"
        }
    .end annotation
.end field

.field private transient d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0P1;ILjava/util/Comparator;)V
    .locals 1
    .param p3    # Ljava/util/Comparator;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<TK;",
            "LX/0Rf",
            "<TV;>;>;I",
            "Ljava/util/Comparator",
            "<-TV;>;)V"
        }
    .end annotation

    .prologue
    .line 407707
    invoke-direct {p0, p1, p2}, LX/18f;-><init>(LX/0P1;I)V

    .line 407708
    invoke-static {p3}, LX/2Q6;->a(Ljava/util/Comparator;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/2Q6;->a:LX/0Rf;

    .line 407709
    return-void
.end method

.method private static A()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<TV;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 407706
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method private B()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 407704
    iget-object v0, p0, LX/2Q6;->d:LX/0Rf;

    .line 407705
    if-nez v0, :cond_0

    new-instance v0, LX/4yc;

    invoke-direct {v0, p0}, LX/4yc;-><init>(LX/2Q6;)V

    iput-object v0, p0, LX/2Q6;->d:LX/0Rf;

    :cond_0
    return-object v0
.end method

.method private C()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 407703
    iget-object v0, p0, LX/2Q6;->a:LX/0Rf;

    instance-of v0, v0, LX/0dW;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Q6;->a:LX/0Rf;

    check-cast v0, LX/0dW;

    invoke-virtual {v0}, LX/0dW;->comparator()Ljava/util/Comparator;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/Comparator;)LX/0Rf;
    .locals 1
    .param p0    # Ljava/util/Comparator;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TV;>;)",
            "LX/0Rf",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 407700
    if-nez p0, :cond_0

    .line 407701
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 407702
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/0dW;->a(Ljava/util/Comparator;)LX/50U;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/util/Comparator;Ljava/util/Collection;)LX/0Rf;
    .locals 2
    .param p0    # Ljava/util/Comparator;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TV;>;",
            "Ljava/util/Collection",
            "<+TV;>;)",
            "LX/0Rf",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 407690
    if-nez p0, :cond_0

    invoke-static {p1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    :goto_0
    return-object v0

    .line 407691
    :cond_0
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 407692
    invoke-static {p0, p1}, LX/50b;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v0

    .line 407693
    if-eqz v0, :cond_1

    instance-of v0, p1, LX/0dW;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 407694
    check-cast v0, LX/0dW;

    .line 407695
    invoke-virtual {v0}, LX/0Py;->isPartialView()Z

    move-result v1

    if-nez v1, :cond_1

    .line 407696
    :goto_1
    move-object v0, v0

    .line 407697
    goto :goto_0

    .line 407698
    :cond_1
    invoke-static {p1}, LX/0Ph;->e(Ljava/lang/Iterable;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 407699
    array-length v1, v0

    invoke-static {p0, v1, v0}, LX/0dW;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)LX/0dW;

    move-result-object v0

    goto :goto_1
.end method

.method private static b(Ljava/util/Comparator;)LX/0cA;
    .locals 1
    .param p0    # Ljava/util/Comparator;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TV;>;)",
            "LX/0cA",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 407689
    if-nez p0, :cond_0

    new-instance v0, LX/0cA;

    invoke-direct {v0}, LX/0cA;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4yi;

    invoke-direct {v0, p0}, LX/4yi;-><init>(Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public static b(LX/0Xu;Ljava/util/Comparator;)LX/2Q6;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Xu",
            "<+TK;+TV;>;",
            "Ljava/util/Comparator",
            "<-TV;>;)",
            "LX/2Q6",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 407671
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 407672
    invoke-interface {p0}, LX/0Xu;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 407673
    sget-object v0, LX/4xU;->a:LX/4xU;

    move-object v0, v0

    .line 407674
    :cond_0
    :goto_0
    return-object v0

    .line 407675
    :cond_1
    instance-of v0, p0, LX/2Q6;

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 407676
    check-cast v0, LX/2Q6;

    .line 407677
    invoke-virtual {v0}, LX/18f;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 407678
    :cond_2
    new-instance v2, LX/0P2;

    invoke-interface {p0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, LX/0P2;-><init>(I)V

    .line 407679
    const/4 v0, 0x0

    .line 407680
    invoke-interface {p0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 407681
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    .line 407682
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 407683
    invoke-static {p1, v0}, LX/2Q6;->a(Ljava/util/Comparator;Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    .line 407684
    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 407685
    invoke-virtual {v2, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 407686
    invoke-virtual {v0}, LX/0Rf;->size()I

    move-result v0

    add-int/2addr v0, v1

    :goto_2
    move v1, v0

    .line 407687
    goto :goto_1

    .line 407688
    :cond_3
    new-instance v0, LX/2Q6;

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    invoke-direct {v0, v2, v1, p1}, LX/2Q6;-><init>(LX/0P1;ILjava/util/Comparator;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 11
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 407643
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 407644
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    .line 407645
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v5

    .line 407646
    if-gez v5, :cond_0

    .line 407647
    new-instance v0, Ljava/io/InvalidObjectException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid key count "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407648
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v6

    move v3, v2

    move v4, v2

    .line 407649
    :goto_0
    if-ge v3, v5, :cond_4

    .line 407650
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v7

    .line 407651
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v8

    .line 407652
    if-gtz v8, :cond_1

    .line 407653
    new-instance v0, Ljava/io/InvalidObjectException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid value count "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407654
    :cond_1
    invoke-static {v0}, LX/2Q6;->b(Ljava/util/Comparator;)LX/0cA;

    move-result-object v9

    move v1, v2

    .line 407655
    :goto_1
    if-ge v1, v8, :cond_2

    .line 407656
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 407657
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 407658
    :cond_2
    invoke-virtual {v9}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    .line 407659
    invoke-virtual {v1}, LX/0Rf;->size()I

    move-result v9

    if-eq v9, v8, :cond_3

    .line 407660
    new-instance v0, Ljava/io/InvalidObjectException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Duplicate key-value pairs exist for key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407661
    :cond_3
    invoke-virtual {v6, v7, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 407662
    add-int/2addr v4, v8

    .line 407663
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 407664
    :cond_4
    :try_start_0
    invoke-virtual {v6}, LX/0P2;->b()LX/0P1;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 407665
    sget-object v2, LX/4yN;->a:LX/50W;

    invoke-virtual {v2, p0, v1}, LX/50W;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 407666
    sget-object v1, LX/4yN;->b:LX/50W;

    invoke-virtual {v1, p0, v4}, LX/50W;->a(Ljava/lang/Object;I)V

    .line 407667
    sget-object v1, LX/4yN;->c:LX/50W;

    invoke-static {v0}, LX/2Q6;->a(Ljava/util/Comparator;)LX/0Rf;

    move-result-object v0

    invoke-virtual {v1, p0, v0}, LX/50W;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 407668
    return-void

    .line 407669
    :catch_0
    move-exception v0

    .line 407670
    new-instance v1, Ljava/io/InvalidObjectException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/io/InvalidObjectException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/InvalidObjectException;

    throw v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 407639
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 407640
    invoke-direct {p0}, LX/2Q6;->C()Ljava/util/Comparator;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 407641
    invoke-static {p0, p1}, LX/50X;->a(LX/0Xu;Ljava/io/ObjectOutputStream;)V

    .line 407642
    return-void
.end method

.method public static z()LX/2Q7;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LX/2Q7",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 407638
    new-instance v0, LX/2Q7;

    invoke-direct {v0}, LX/2Q7;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 407637
    invoke-virtual {p0, p1}, LX/2Q6;->e(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 407627
    invoke-static {}, LX/2Q6;->A()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 407636
    invoke-virtual {p0, p1}, LX/2Q6;->e(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 407635
    invoke-static {}, LX/2Q6;->A()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/Object;)LX/0Rf;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LX/0Rf",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 407633
    iget-object v0, p0, LX/18f;->b:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rf;

    .line 407634
    iget-object v1, p0, LX/2Q6;->a:LX/0Rf;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rf;

    return-object v0
.end method

.method public final synthetic h(Ljava/lang/Object;)LX/0Py;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 407632
    invoke-virtual {p0, p1}, LX/2Q6;->e(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i(Ljava/lang/Object;)LX/0Py;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 407631
    invoke-static {}, LX/2Q6;->A()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 407630
    invoke-direct {p0}, LX/2Q6;->B()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic t()Ljava/util/Set;
    .locals 1

    .prologue
    .line 407629
    invoke-direct {p0}, LX/2Q6;->B()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic v()LX/0Py;
    .locals 1

    .prologue
    .line 407628
    invoke-direct {p0}, LX/2Q6;->B()LX/0Rf;

    move-result-object v0

    return-object v0
.end method
