.class public LX/2J4;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392531
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 392532
    return-void
.end method


# virtual methods
.method public final a(LX/10M;LX/10O;)Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;
    .locals 9

    .prologue
    .line 392533
    new-instance v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    invoke-direct {v0, p1, p2}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;-><init>(LX/10M;LX/10O;)V

    .line 392534
    invoke-virtual {p0}, LX/0Wl;->getApplicationInjector()LX/0QA;

    move-result-object v1

    const/16 v2, 0x542

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v4, 0x12cb

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/284;->b(LX/0QB;)LX/284;

    move-result-object v5

    check-cast v5, LX/284;

    invoke-static {p0}, LX/0fW;->a(LX/0QB;)LX/0fW;

    move-result-object v6

    check-cast v6, LX/0fW;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v7

    check-cast v7, LX/0WJ;

    invoke-static {p0}, LX/2Di;->b(LX/0QB;)LX/2Di;

    move-result-object v8

    check-cast v8, LX/2Di;

    .line 392535
    iput-object v1, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->f:LX/0Ot;

    iput-object v2, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->g:Ljava/util/concurrent/ExecutorService;

    iput-object v3, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v4, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->i:LX/0Or;

    iput-object v5, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->j:LX/284;

    iput-object v6, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a:LX/0fW;

    iput-object v7, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->b:LX/0WJ;

    iput-object v8, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->c:LX/2Di;

    .line 392536
    return-object v0
.end method
