.class public LX/3MQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public b:LX/3MS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/3Lx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 554404
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/3MR;->a:LX/0U1;

    .line 554405
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554406
    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/3MR;->b:LX/0U1;

    .line 554407
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554408
    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/3MR;->c:LX/0U1;

    .line 554409
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554410
    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/3MR;->d:LX/0U1;

    .line 554411
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554412
    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/3MR;->e:LX/0U1;

    .line 554413
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554414
    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/3MR;->f:LX/0U1;

    .line 554415
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554416
    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3MR;->g:LX/0U1;

    .line 554417
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554418
    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3MR;->h:LX/0U1;

    .line 554419
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554420
    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/3MR;->i:LX/0U1;

    .line 554421
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554422
    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/3MR;->j:LX/0U1;

    .line 554423
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554424
    aput-object v2, v0, v1

    sput-object v0, LX/3MQ;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 554425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 554426
    return-void
.end method

.method private static a(LX/3MQ;Lcom/facebook/user/model/User;I)Landroid/content/ContentValues;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 554427
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 554428
    sget-object v2, LX/3MR;->b:LX/0U1;

    .line 554429
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554430
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 554431
    sget-object v2, LX/3MR;->c:LX/0U1;

    .line 554432
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554433
    iget-object v3, p1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v3

    .line 554434
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 554435
    sget-object v2, LX/3MR;->d:LX/0U1;

    .line 554436
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554437
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 554438
    sget-object v2, LX/3MR;->e:LX/0U1;

    .line 554439
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554440
    iget-object v3, p1, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v3, v3

    .line 554441
    invoke-virtual {v3}, Lcom/facebook/user/model/Name;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 554442
    sget-object v2, LX/3MR;->f:LX/0U1;

    .line 554443
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554444
    iget-object v3, p0, LX/3MQ;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 554445
    sget-object v2, LX/3MR;->h:LX/0U1;

    .line 554446
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554447
    iget-object v3, p1, Lcom/facebook/user/model/User;->r:Ljava/lang/String;

    move-object v3, v3

    .line 554448
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 554449
    sget-object v2, LX/3MR;->i:LX/0U1;

    .line 554450
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554451
    iget-object v3, p1, Lcom/facebook/user/model/User;->g:Ljava/lang/String;

    move-object v3, v3

    .line 554452
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 554453
    iget-boolean v2, p1, Lcom/facebook/user/model/User;->y:Z

    move v2, v2

    .line 554454
    if-eqz v2, :cond_1

    move p2, v0

    .line 554455
    :cond_0
    :goto_0
    sget-object v0, LX/3MR;->g:LX/0U1;

    .line 554456
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 554457
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 554458
    return-object v1

    .line 554459
    :cond_1
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->z:Z

    move v0, v0

    .line 554460
    if-eqz v0, :cond_0

    .line 554461
    const/4 p2, 0x2

    goto :goto_0
.end method

.method public static a(LX/3MQ;LX/0ux;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 554462
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, 0x270be6fd

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 554463
    :try_start_0
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "match_table"

    invoke-virtual {p1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 554464
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 554465
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, 0x2b01139d

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 554466
    return-void

    .line 554467
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, 0x2e9cf0fb

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public static a(LX/3MQ;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 3

    .prologue
    .line 554468
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, 0x2c3d55ba

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 554469
    :try_start_0
    invoke-static {p0, p1, p2}, LX/3MQ;->b(LX/3MQ;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 554470
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 554471
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, -0x287acd02

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 554472
    return-void

    .line 554473
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, -0x57c8dc8a

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public static b(LX/0QB;)LX/3MQ;
    .locals 4

    .prologue
    .line 554474
    new-instance v3, LX/3MQ;

    invoke-direct {v3}, LX/3MQ;-><init>()V

    .line 554475
    invoke-static {p0}, LX/3MS;->a(LX/0QB;)LX/3MS;

    move-result-object v0

    check-cast v0, LX/3MS;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/3Lx;->b(LX/0QB;)LX/3Lx;

    move-result-object v2

    check-cast v2, LX/3Lx;

    .line 554476
    iput-object v0, v3, LX/3MQ;->b:LX/3MS;

    iput-object v1, v3, LX/3MQ;->c:LX/0SG;

    iput-object v2, v3, LX/3MQ;->d:LX/3Lx;

    .line 554477
    return-object v3
.end method

.method private static b(LX/3MQ;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 554478
    invoke-static {p1}, LX/3MQ;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 554479
    iget-object v1, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "match_table"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, p2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 554480
    if-nez v0, :cond_0

    .line 554481
    sget-object v0, LX/3MR;->a:LX/0U1;

    .line 554482
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 554483
    invoke-virtual {p2, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 554484
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "match_table"

    const/4 v2, 0x0

    const v3, 0x1bb175e2

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {v0, v1, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x24c0a7d3

    invoke-static {v0}, LX/03h;->a(I)V

    .line 554485
    :cond_0
    return-void
.end method

.method private static c(Ljava/lang/String;)LX/0ux;
    .locals 2

    .prologue
    .line 554486
    sget-object v0, LX/3MR;->a:LX/0U1;

    .line 554487
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 554488
    invoke-static {v0, p0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/6lI;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 554489
    iget-object v0, p0, LX/3MQ;->d:LX/3Lx;

    invoke-virtual {v0, p1}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 554490
    :try_start_0
    invoke-static {v11}, LX/3MQ;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 554491
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "match_table"

    sget-object v2, LX/3MQ;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 554492
    if-eqz v10, :cond_2

    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 554493
    sget-object v0, LX/3MR;->f:LX/0U1;

    .line 554494
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 554495
    invoke-static {v10, v0}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v0

    .line 554496
    sget-object v2, LX/3MR;->b:LX/0U1;

    .line 554497
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554498
    invoke-static {v10, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    .line 554499
    packed-switch v2, :pswitch_data_0

    .line 554500
    :pswitch_0
    const/4 v3, 0x1

    :goto_0
    move v3, v3

    .line 554501
    if-eqz v3, :cond_0

    iget-object v3, p0, LX/3MQ;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v0, v4, v0

    const-wide/32 v4, 0x240c8400

    cmp-long v0, v0, v4

    if-gez v0, :cond_2

    .line 554502
    :cond_0
    new-instance v0, LX/6lI;

    sget-object v1, LX/3MR;->c:LX/0U1;

    .line 554503
    iget-object v3, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v3

    .line 554504
    invoke-static {v10, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v1, LX/3MR;->d:LX/0U1;

    .line 554505
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 554506
    invoke-static {v10, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v1, LX/3MR;->e:LX/0U1;

    .line 554507
    iget-object v5, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v5

    .line 554508
    invoke-static {v10, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v1, LX/3MR;->g:LX/0U1;

    .line 554509
    iget-object v6, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v6

    .line 554510
    invoke-static {v10, v1}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v6

    sget-object v1, LX/3MR;->h:LX/0U1;

    .line 554511
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 554512
    invoke-static {v10, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    sget-object v1, LX/3MR;->i:LX/0U1;

    .line 554513
    iget-object v8, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v8

    .line 554514
    invoke-static {v10, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sget-object v1, LX/3MR;->j:LX/0U1;

    .line 554515
    iget-object v9, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v9

    .line 554516
    invoke-static {v10, v1}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v9

    move-object v1, v11

    invoke-direct/range {v0 .. v9}, LX/6lI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 554517
    if-eqz v10, :cond_1

    .line 554518
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 554519
    :cond_1
    :goto_1
    return-object v0

    .line 554520
    :cond_2
    if-eqz v10, :cond_3

    .line 554521
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v8

    .line 554522
    goto :goto_1

    .line 554523
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v8, :cond_4

    .line 554524
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 554525
    :catchall_1
    move-exception v0

    move-object v8, v10

    goto :goto_2

    .line 554526
    :pswitch_1
    const/4 v3, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/util/Collection;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 554527
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, -0x4cb8c810

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 554528
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 554529
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 554530
    sget-object v3, LX/3MR;->b:LX/0U1;

    .line 554531
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 554532
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 554533
    sget-object v3, LX/3MR;->g:LX/0U1;

    .line 554534
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 554535
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 554536
    iget-object v3, p0, LX/3MQ;->d:LX/3Lx;

    invoke-virtual {v3, v0}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, LX/3MQ;->b(LX/3MQ;Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 554537
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, -0x627ec217

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 554538
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 554539
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, -0x55a37de8

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 554540
    return-void
.end method

.method public final a(Ljava/util/Map;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 554541
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, 0x5ac0c5e2

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 554542
    :try_start_0
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 554543
    iget-object v3, p0, LX/3MQ;->d:LX/3Lx;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 554544
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-static {p0, v0, p2}, LX/3MQ;->a(LX/3MQ;Lcom/facebook/user/model/User;I)Landroid/content/ContentValues;

    move-result-object v0

    invoke-static {p0, v1, v0}, LX/3MQ;->b(LX/3MQ;Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 554545
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, 0x198537b3

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 554546
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 554547
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, -0x70304611

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 554548
    return-void
.end method

.method public final b(Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/6lI;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v8, 0x0

    .line 554549
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 554550
    new-array v0, v5, [LX/0ux;

    new-array v1, v5, [LX/0ux;

    sget-object v2, LX/3MR;->d:LX/0U1;

    .line 554551
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554552
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v1, v6

    sget-object v2, LX/3MR;->d:LX/0U1;

    .line 554553
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554554
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "% "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v1}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, LX/3MR;->b:LX/0U1;

    .line 554555
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 554556
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v4

    .line 554557
    :try_start_0
    iget-object v0, p0, LX/3MQ;->b:LX/3MS;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "match_table"

    sget-object v2, LX/3MQ;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v10

    .line 554558
    if-eqz v10, :cond_1

    .line 554559
    :goto_0
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 554560
    new-instance v0, LX/6lI;

    sget-object v1, LX/3MR;->a:LX/0U1;

    .line 554561
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 554562
    invoke-static {v10, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/3MR;->b:LX/0U1;

    .line 554563
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 554564
    invoke-static {v10, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    sget-object v3, LX/3MR;->c:LX/0U1;

    .line 554565
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 554566
    invoke-static {v10, v3}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/3MR;->d:LX/0U1;

    .line 554567
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 554568
    invoke-static {v10, v4}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/3MR;->e:LX/0U1;

    .line 554569
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 554570
    invoke-static {v10, v5}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v6, LX/3MR;->g:LX/0U1;

    .line 554571
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 554572
    invoke-static {v10, v6}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v6

    sget-object v7, LX/3MR;->h:LX/0U1;

    .line 554573
    iget-object v8, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v8

    .line 554574
    invoke-static {v10, v7}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    sget-object v8, LX/3MR;->i:LX/0U1;

    .line 554575
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 554576
    invoke-static {v10, v8}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sget-object v9, LX/3MR;->j:LX/0U1;

    .line 554577
    iget-object p0, v9, LX/0U1;->d:Ljava/lang/String;

    move-object v9, p0

    .line 554578
    invoke-static {v10, v9}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v9

    invoke-direct/range {v0 .. v9}, LX/6lI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 554579
    :catchall_0
    move-exception v0

    move-object v1, v10

    :goto_1
    if-eqz v1, :cond_0

    .line 554580
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 554581
    :cond_1
    if-eqz v10, :cond_2

    .line 554582
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 554583
    :cond_2
    return-object v11

    .line 554584
    :catchall_1
    move-exception v0

    move-object v1, v8

    goto :goto_1
.end method
