.class public LX/28Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field public final a:LX/0lC;

.field private final b:LX/0WJ;

.field public final c:LX/03V;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2BQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lC;LX/0WJ;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lC;",
            "LX/0WJ;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/2BQ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 373935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373936
    iput-object p1, p0, LX/28Q;->a:LX/0lC;

    .line 373937
    iput-object p2, p0, LX/28Q;->b:LX/0WJ;

    .line 373938
    iput-object p3, p0, LX/28Q;->c:LX/03V;

    .line 373939
    iput-object p4, p0, LX/28Q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 373940
    iput-object p5, p0, LX/28Q;->e:LX/0Ot;

    .line 373941
    return-void
.end method

.method public static a(LX/28Q;Lcom/facebook/katana/model/FacebookSessionInfo;)V
    .locals 14

    .prologue
    .line 373942
    invoke-virtual {p1}, Lcom/facebook/katana/model/FacebookSessionInfo;->getSessionCookies()Ljava/util/List;

    move-result-object v5

    .line 373943
    const/4 v4, 0x0

    .line 373944
    if-eqz v5, :cond_0

    .line 373945
    :try_start_0
    iget-object v6, p0, LX/28Q;->a:LX/0lC;

    invoke-virtual {v6, v5}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    :cond_0
    move-object v7, v4

    .line 373946
    :goto_0
    new-instance v12, Lcom/facebook/auth/protocol/AuthenticationResultImpl;

    iget-wide v4, p1, Lcom/facebook/katana/model/FacebookSessionInfo;->userId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    new-instance v4, Lcom/facebook/auth/credentials/FacebookCredentials;

    iget-wide v8, p1, Lcom/facebook/katana/model/FacebookSessionInfo;->userId:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Lcom/facebook/katana/model/FacebookSessionInfo;->oAuthToken:Ljava/lang/String;

    iget-object v8, p1, Lcom/facebook/katana/model/FacebookSessionInfo;->sessionSecret:Ljava/lang/String;

    iget-object v9, p1, Lcom/facebook/katana/model/FacebookSessionInfo;->sessionKey:Ljava/lang/String;

    iget-object v10, p1, Lcom/facebook/katana/model/FacebookSessionInfo;->username:Ljava/lang/String;

    invoke-direct/range {v4 .. v10}, Lcom/facebook/auth/credentials/FacebookCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p1, Lcom/facebook/katana/model/FacebookSessionInfo;->machineID:Ljava/lang/String;

    sget-object v9, LX/03R;->UNSET:LX/03R;

    const-string v10, ""

    const-string v11, ""

    move-object v5, v12

    move-object v6, v13

    move-object v7, v4

    invoke-direct/range {v5 .. v11}, Lcom/facebook/auth/protocol/AuthenticationResultImpl;-><init>(Ljava/lang/String;Lcom/facebook/auth/credentials/FacebookCredentials;Ljava/lang/String;LX/03R;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v12

    .line 373947
    invoke-interface {v0}, Lcom/facebook/auth/component/AuthenticationResult;->b()Lcom/facebook/auth/credentials/FacebookCredentials;

    move-result-object v1

    .line 373948
    iget-object v2, p0, LX/28Q;->b:LX/0WJ;

    invoke-virtual {v2, v1}, LX/0WJ;->a(Lcom/facebook/auth/credentials/FacebookCredentials;)V

    .line 373949
    iget-object v2, p0, LX/28Q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/26p;->f:LX/0Tn;

    invoke-interface {v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 373950
    iget-object v2, p0, LX/28Q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/26p;->f:LX/0Tn;

    invoke-interface {v0}, Lcom/facebook/auth/component/AuthenticationResult;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 373951
    :cond_1
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    .line 373952
    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    .line 373953
    iget-object v3, v1, Lcom/facebook/auth/credentials/FacebookCredentials;->a:Ljava/lang/String;

    move-object v1, v3

    .line 373954
    invoke-virtual {v0, v2, v1}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 373955
    invoke-virtual {p1}, Lcom/facebook/katana/model/FacebookSessionInfo;->a()Lcom/facebook/ipc/model/FacebookUser;

    move-result-object v1

    .line 373956
    if-eqz v1, :cond_3

    .line 373957
    invoke-virtual {v1}, Lcom/facebook/ipc/model/FacebookUser;->a()Ljava/lang/String;

    move-result-object v2

    .line 373958
    iput-object v2, v0, LX/0XI;->h:Ljava/lang/String;

    .line 373959
    iget-object v2, v1, Lcom/facebook/ipc/model/FacebookUser;->mFirstName:Ljava/lang/String;

    .line 373960
    iput-object v2, v0, LX/0XI;->i:Ljava/lang/String;

    .line 373961
    iget-object v2, v1, Lcom/facebook/ipc/model/FacebookUser;->mLastName:Ljava/lang/String;

    .line 373962
    iput-object v2, v0, LX/0XI;->j:Ljava/lang/String;

    .line 373963
    iget-object v2, v1, Lcom/facebook/ipc/model/FacebookUser;->mImageUrl:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 373964
    iget-object v2, v1, Lcom/facebook/ipc/model/FacebookUser;->mImageUrl:Ljava/lang/String;

    .line 373965
    iput-object v2, v0, LX/0XI;->n:Ljava/lang/String;

    .line 373966
    :cond_2
    iget-object v2, v1, Lcom/facebook/ipc/model/FacebookUser;->mCoverPhoto:Lcom/facebook/ipc/model/FacebookUserCoverPhoto;

    if-eqz v2, :cond_3

    .line 373967
    iget-object v1, v1, Lcom/facebook/ipc/model/FacebookUser;->mCoverPhoto:Lcom/facebook/ipc/model/FacebookUserCoverPhoto;

    iget-object v1, v1, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->source:Ljava/lang/String;

    .line 373968
    iput-object v1, v0, LX/0XI;->o:Ljava/lang/String;

    .line 373969
    :cond_3
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 373970
    iget-object v1, p0, LX/28Q;->b:LX/0WJ;

    invoke-virtual {v1, v0}, LX/0WJ;->c(Lcom/facebook/user/model/User;)V

    .line 373971
    return-void

    .line 373972
    :catch_0
    move-exception v5

    .line 373973
    iget-object v6, p0, LX/28Q;->c:LX/03V;

    const-string v7, "AppSession_SerializeSessionInfo"

    const-string v8, "Unable to serialize session info into string."

    invoke-virtual {v6, v7, v8, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v7, v4

    goto/16 :goto_0
.end method

.method public static b(LX/0QB;)LX/28Q;
    .locals 6

    .prologue
    .line 373974
    new-instance v0, LX/28Q;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v2

    check-cast v2, LX/0WJ;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v5, 0xc77

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/28Q;-><init>(LX/0lC;LX/0WJ;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;)V

    .line 373975
    return-object v0
.end method

.method public static d(LX/28Q;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 373976
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 373977
    const-string v1, "user_values"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 373978
    iget-object v1, p0, LX/28Q;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2BQ;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "value"

    aput-object v3, v2, v5

    const-string v3, "name=\'active_session_info\'"

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 373979
    if-eqz v1, :cond_1

    .line 373980
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373981
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 373982
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 373983
    :cond_1
    return-object v4

    .line 373984
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static e(LX/28Q;)V
    .locals 4

    .prologue
    .line 373985
    iget-object v0, p0, LX/28Q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v0, LX/1CA;->c:LX/0Tn;

    const-string v2, "active_session_info"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 373986
    iget-object v0, p0, LX/28Q;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2BQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "user_values"

    const-string v2, "name=\'active_session_info\'"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 373987
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/katana/model/FacebookSessionInfo;
    .locals 14
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 373988
    iget-object v1, p0, LX/28Q;->b:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 373989
    iget-object v1, p0, LX/28Q;->b:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v9

    .line 373990
    iget-object v1, p0, LX/28Q;->a:LX/0lC;

    .line 373991
    iget-object v2, v9, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v2, v2

    .line 373992
    invoke-static {v1, v2}, Lcom/facebook/auth/credentials/SessionCookie;->a(LX/0lC;Ljava/lang/String;)LX/0Px;

    move-result-object v12

    .line 373993
    iget-object v1, p0, LX/28Q;->b:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v10

    .line 373994
    new-instance v1, Lcom/facebook/ipc/model/FacebookUser;

    .line 373995
    iget-object v2, v9, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v2

    .line 373996
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v10}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10}, Lcom/facebook/user/model/User;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;

    .line 373997
    iget-object v11, v10, Lcom/facebook/user/model/User;->j:Ljava/lang/String;

    move-object v10, v11

    .line 373998
    invoke-direct {v8, v10}, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v1 .. v8}, Lcom/facebook/ipc/model/FacebookUser;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/model/FacebookUserCoverPhoto;)V

    .line 373999
    new-instance v3, Lcom/facebook/katana/model/FacebookSessionInfo;

    .line 374000
    iget-object v2, v9, Lcom/facebook/auth/viewercontext/ViewerContext;->g:Ljava/lang/String;

    move-object v4, v2

    .line 374001
    iget-object v2, v9, Lcom/facebook/auth/viewercontext/ViewerContext;->f:Ljava/lang/String;

    move-object v5, v2

    .line 374002
    iget-object v2, v9, Lcom/facebook/auth/viewercontext/ViewerContext;->e:Ljava/lang/String;

    move-object v6, v2

    .line 374003
    iget-object v2, v9, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v7, v2

    .line 374004
    iget-object v2, v9, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v2

    .line 374005
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    iget-object v2, p0, LX/28Q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v10, LX/26p;->f:LX/0Tn;

    invoke-interface {v2, v10, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iget-object v13, p0, LX/28Q;->c:LX/03V;

    move-object v11, v1

    invoke-direct/range {v3 .. v13}, Lcom/facebook/katana/model/FacebookSessionInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/facebook/ipc/model/FacebookUser;Ljava/util/List;LX/03V;)V

    .line 374006
    :goto_0
    return-object v3

    :cond_0
    move-object v3, v0

    goto :goto_0
.end method

.method public final init()V
    .locals 6

    .prologue
    .line 374007
    iget-object v0, p0, LX/28Q;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374008
    invoke-static {p0}, LX/28Q;->e(LX/28Q;)V

    .line 374009
    :goto_0
    return-void

    .line 374010
    :cond_0
    const/4 v2, 0x0

    .line 374011
    iget-object v1, p0, LX/28Q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/1CA;->c:LX/0Tn;

    const-string v3, "active_session_info"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 374012
    if-nez v0, :cond_4

    .line 374013
    invoke-static {p0}, LX/28Q;->d(LX/28Q;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 374014
    :goto_1
    invoke-static {p0}, LX/28Q;->e(LX/28Q;)V

    .line 374015
    if-eqz v1, :cond_2

    .line 374016
    :try_start_0
    iget-object v0, p0, LX/28Q;->a:LX/0lC;

    const-class v3, Lcom/facebook/katana/model/FacebookSessionInfo;

    invoke-virtual {v0, v1, v3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/model/FacebookSessionInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 374017
    :goto_2
    if-eqz v0, :cond_1

    .line 374018
    invoke-static {v0}, Lcom/facebook/katana/model/FacebookSessionInfo;->a(Lcom/facebook/katana/model/FacebookSessionInfo;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 374019
    invoke-static {p0, v0}, LX/28Q;->a(LX/28Q;Lcom/facebook/katana/model/FacebookSessionInfo;)V

    .line 374020
    :cond_1
    :goto_3
    goto :goto_0

    .line 374021
    :catch_0
    iget-object v0, p0, LX/28Q;->c:LX/03V;

    const-string v3, "CorruptedSessionOnDisk"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Couldn\'t resume session from disk because it was corrupt."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v0, v2

    goto :goto_2

    .line 374022
    :cond_3
    iget-object v0, p0, LX/28Q;->c:LX/03V;

    const-string v2, "InvalidSessionOnDisk"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Couldn\'t resume session from disk because it was invalid."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    move-object v1, v0

    goto :goto_1
.end method
