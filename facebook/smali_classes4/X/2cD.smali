.class public LX/2cD;
.super LX/2PI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2PI",
        "<",
        "Lcom/facebook/messaging/tincan/outbound/TincanCreateMultiEndpointThread$TincanCreateMultiEndpointThreadListener;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2cD;


# instance fields
.field private final c:LX/2PJ;

.field private final d:LX/0SF;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 440482
    const-class v0, LX/2cD;

    sput-object v0, LX/2cD;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2PJ;LX/0SF;LX/0Or;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2PJ;",
            "LX/0SF;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 440477
    const/16 v0, 0x11

    invoke-direct {p0, v0}, LX/2PI;-><init>(I)V

    .line 440478
    iput-object p1, p0, LX/2cD;->c:LX/2PJ;

    .line 440479
    iput-object p2, p0, LX/2cD;->d:LX/0SF;

    .line 440480
    iput-object p3, p0, LX/2cD;->e:LX/0Or;

    .line 440481
    return-void
.end method

.method public static a(LX/0QB;)LX/2cD;
    .locals 6

    .prologue
    .line 440459
    sget-object v0, LX/2cD;->f:LX/2cD;

    if-nez v0, :cond_1

    .line 440460
    const-class v1, LX/2cD;

    monitor-enter v1

    .line 440461
    :try_start_0
    sget-object v0, LX/2cD;->f:LX/2cD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 440462
    if-eqz v2, :cond_0

    .line 440463
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 440464
    new-instance v5, LX/2cD;

    invoke-static {v0}, LX/2PJ;->a(LX/0QB;)LX/2PJ;

    move-result-object v3

    check-cast v3, LX/2PJ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SF;

    const/16 p0, 0x15e8

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/2cD;-><init>(LX/2PJ;LX/0SF;LX/0Or;)V

    .line 440465
    move-object v0, v5

    .line 440466
    sput-object v0, LX/2cD;->f:LX/2cD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 440467
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 440468
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 440469
    :cond_1
    sget-object v0, LX/2cD;->f:LX/2cD;

    return-object v0

    .line 440470
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 440471
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b([B)V
    .locals 2

    .prologue
    .line 440473
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuP;

    .line 440474
    sget-object p0, LX/IuQ;->m:Ljava/lang/String;

    const-string p1, "Unable to create multi-endpoint thread"

    invoke-static {p0, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 440475
    goto :goto_0

    .line 440476
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 440472
    return-void
.end method

.method public final a(LX/Dph;)V
    .locals 13

    .prologue
    .line 440429
    if-nez p1, :cond_1

    .line 440430
    sget-object v0, LX/2cD;->b:Ljava/lang/Class;

    const-string v1, "Could not deserialise create thread response"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 440431
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/2cD;->b([B)V

    .line 440432
    :cond_0
    :goto_0
    return-void

    .line 440433
    :cond_1
    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    if-eqz v0, :cond_2

    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, LX/6kT;->a(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 440434
    :cond_2
    sget-object v0, LX/2cD;->b:Ljava/lang/Class;

    const-string v1, "Could not deserialise LookupResponsePayload"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 440435
    iget-object v0, p1, LX/Dph;->nonce:[B

    invoke-direct {p0, v0}, LX/2cD;->b([B)V

    goto :goto_0

    .line 440436
    :cond_3
    iget-object v0, p1, LX/Dph;->result:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    iget-object v0, p1, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_7

    .line 440437
    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    invoke-virtual {v0}, LX/Dpi;->f()LX/DpH;

    move-result-object v1

    .line 440438
    if-eqz v1, :cond_6

    .line 440439
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuP;

    .line 440440
    iget-object v3, p1, LX/Dph;->nonce:[B

    iget-object v4, v1, LX/DpH;->user_devices:LX/DpR;

    iget-object v4, v4, LX/DpR;->participants_list:Ljava/util/Map;

    .line 440441
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 440442
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 440443
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    .line 440444
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 440445
    iget-object v9, v0, LX/IuP;->a:LX/IuQ;

    iget-object v9, v9, LX/IuO;->a:LX/3Ec;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    invoke-virtual {v9, v11, v12}, LX/3Ec;->c(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 440446
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    const/4 v9, 0x1

    if-ne v6, v9, :cond_4

    .line 440447
    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/DpM;

    .line 440448
    iget-object v6, v0, LX/IuP;->a:LX/IuQ;

    iget-object v6, v6, LX/IuO;->b:LX/2Ox;

    iget-object v9, v5, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    iget-object v11, v5, LX/DpM;->instance_id:Ljava/lang/String;

    invoke-virtual {v6, v9, v10, v11}, LX/2Ox;->a(JLjava/lang/String;)V

    .line 440449
    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 440450
    :cond_4
    invoke-virtual {v7, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_2

    .line 440451
    :cond_5
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v3}, Ljava/lang/String;-><init>([B)V

    invoke-static {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v5

    .line 440452
    if-nez v5, :cond_8

    .line 440453
    sget-object v5, LX/IuQ;->m:Ljava/lang/String;

    const-string v6, "Unable to parse threadkey for nonce"

    invoke-static {v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 440454
    :goto_3
    goto :goto_1

    .line 440455
    :cond_6
    iget-object v0, p1, LX/Dph;->nonce:[B

    invoke-direct {p0, v0}, LX/2cD;->b([B)V

    goto/16 :goto_0

    .line 440456
    :cond_7
    iget-object v0, p1, LX/Dph;->nonce:[B

    invoke-direct {p0, v0}, LX/2cD;->b([B)V

    goto/16 :goto_0

    .line 440457
    :cond_8
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 440458
    iget-object v7, v0, LX/IuP;->a:LX/IuQ;

    iget-object v7, v7, LX/IuQ;->n:LX/0TD;

    new-instance v8, Lcom/facebook/messaging/tincan/messenger/senders/MultiEndpointSender$MultiEndpointThreadCreationListener$1;

    invoke-direct {v8, v0, v5, v6}, Lcom/facebook/messaging/tincan/messenger/senders/MultiEndpointSender$MultiEndpointThreadCreationListener$1;-><init>(LX/IuP;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/List;)V

    const v5, 0x1fee2898

    invoke-static {v7, v8, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_3
.end method

.method public final declared-synchronized a([BJ)Z
    .locals 10

    .prologue
    .line 440416
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/2PI;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 440417
    sget-object v0, LX/2cD;->b:Ljava/lang/Class;

    const-string v1, "Stored procedure sender not available to create multi-endpoint thread"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 440418
    const/4 v0, 0x0

    .line 440419
    :goto_0
    monitor-exit p0

    return v0

    .line 440420
    :cond_0
    :try_start_1
    new-instance v7, LX/DpG;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-direct {v7, v0, v1}, LX/DpG;-><init>(Ljava/util/List;Ljava/lang/Boolean;)V

    .line 440421
    const/4 v1, 0x0

    new-instance v2, LX/DpM;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, LX/DpM;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    new-instance v3, LX/DpM;

    iget-object v0, p0, LX/2cD;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v4, p0, LX/2cD;->c:LX/2PJ;

    invoke-virtual {v4}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, LX/DpM;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    iget-object v0, p0, LX/2cD;->d:LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v4

    const-wide/16 v8, 0x3e8

    mul-long/2addr v4, v8

    const/16 v6, 0x3c

    .line 440422
    new-instance v0, LX/DpO;

    invoke-direct {v0}, LX/DpO;-><init>()V

    .line 440423
    invoke-static {v0, v7}, LX/DpO;->b(LX/DpO;LX/DpG;)V

    .line 440424
    move-object v7, v0

    .line 440425
    move-object v8, p1

    invoke-static/range {v1 .. v8}, LX/Dpm;->a(LX/Dpe;LX/DpM;LX/DpM;JILX/DpO;[B)LX/DpN;

    move-result-object v0

    .line 440426
    invoke-static {v0}, LX/Dpn;->a(LX/1u2;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2PI;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 440427
    const/4 v0, 0x1

    goto :goto_0

    .line 440428
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
