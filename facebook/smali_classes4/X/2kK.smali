.class public LX/2kK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/2kH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/graphql/cursor/CursorPriorityQueue$Listener",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2nf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "LX/2nf;",
            "TT;>;"
        }
    .end annotation
.end field

.field private d:I


# direct methods
.method public constructor <init>(LX/2kH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/cursor/CursorPriorityQueue$Listener",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 455959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 455960
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2kK;->b:Ljava/util/List;

    .line 455961
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/2kK;->c:Ljava/util/WeakHashMap;

    .line 455962
    const/4 v0, -0x1

    iput v0, p0, LX/2kK;->d:I

    .line 455963
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2kH;

    iput-object v0, p0, LX/2kK;->a:LX/2kH;

    .line 455964
    return-void
.end method

.method private static a(LX/2nf;)I
    .locals 2

    .prologue
    .line 455965
    invoke-interface {p0}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 455966
    const-string v1, "SESSION_VERSION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 455967
    const-string v1, "SESSION_VERSION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private a(LX/2nf;Ljava/lang/Object;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nf;",
            "TT;I)V"
        }
    .end annotation

    .prologue
    .line 455968
    iput p3, p0, LX/2kK;->d:I

    .line 455969
    iget-object v0, p0, LX/2kK;->a:LX/2kH;

    .line 455970
    check-cast p2, Ljava/lang/Runnable;

    .line 455971
    if-nez p2, :cond_0

    .line 455972
    :goto_0
    return-void

    .line 455973
    :cond_0
    iget-object p0, v0, LX/2kH;->a:LX/2jx;

    iget-object p0, p0, LX/2jx;->k:Ljava/util/concurrent/Executor;

    const p3, 0x62d18fa3

    invoke-static {p0, p2, p3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 455974
    iget-object v0, p0, LX/2kK;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nf;

    .line 455975
    invoke-static {v0}, LX/2kK;->a(LX/2nf;)I

    move-result v2

    .line 455976
    iget v3, p0, LX/2kK;->d:I

    add-int/lit8 v3, v3, 0x1

    if-ne v2, v3, :cond_0

    .line 455977
    iget-object v1, p0, LX/2kK;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v1, v0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 455978
    iget-object v3, p0, LX/2kK;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v3, v0}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 455979
    iget-object v3, p0, LX/2kK;->b:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 455980
    invoke-direct {p0, v0, v1, v2}, LX/2kK;->a(LX/2nf;Ljava/lang/Object;I)V

    .line 455981
    invoke-direct {p0}, LX/2kK;->b()V

    .line 455982
    :cond_1
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 455983
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2kK;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 455984
    iget-object v0, p0, LX/2kK;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->clear()V

    .line 455985
    const/4 v0, -0x1

    iput v0, p0, LX/2kK;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455986
    monitor-exit p0

    return-void

    .line 455987
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/2nf;Ljava/lang/Object;)V
    .locals 5
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nf;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 455988
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/2kK;->a(LX/2nf;)I

    move-result v0

    .line 455989
    iget v1, p0, LX/2kK;->d:I

    if-gt v0, v1, :cond_0

    .line 455990
    const-string v1, "CursorPriorityQueue"

    const-string v2, "Discarding cursor that is older than current version %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455991
    :goto_0
    monitor-exit p0

    return-void

    .line 455992
    :cond_0
    :try_start_1
    iget v1, p0, LX/2kK;->d:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 455993
    invoke-direct {p0, p1, p2, v0}, LX/2kK;->a(LX/2nf;Ljava/lang/Object;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 455994
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 455995
    :cond_1
    :try_start_2
    iget v1, p0, LX/2kK;->d:I

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_2

    .line 455996
    invoke-direct {p0, p1, p2, v0}, LX/2kK;->a(LX/2nf;Ljava/lang/Object;I)V

    .line 455997
    invoke-direct {p0}, LX/2kK;->b()V

    goto :goto_0

    .line 455998
    :cond_2
    iget-object v0, p0, LX/2kK;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->size()I

    .line 455999
    iget-object v0, p0, LX/2kK;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 456000
    iget-object v0, p0, LX/2kK;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
