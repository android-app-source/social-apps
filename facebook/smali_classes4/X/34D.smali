.class public final LX/34D;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 494561
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 494562
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 494563
    :goto_0
    return v1

    .line 494564
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 494565
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 494566
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 494567
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 494568
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 494569
    const-string v4, "marketplace_badge_count"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 494570
    invoke-static {p0, p1}, LX/34E;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 494571
    :cond_2
    const-string v4, "notif_readness"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 494572
    invoke-static {p0, p1}, LX/34F;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 494573
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 494574
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 494575
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 494576
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 494577
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 494578
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 494579
    if-eqz v0, :cond_0

    .line 494580
    const-string v1, "marketplace_badge_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 494581
    invoke-static {p0, v0, p2}, LX/34E;->a(LX/15i;ILX/0nX;)V

    .line 494582
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 494583
    if-eqz v0, :cond_1

    .line 494584
    const-string v1, "notif_readness"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 494585
    invoke-static {p0, v0, p2, p3}, LX/34F;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 494586
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 494587
    return-void
.end method
