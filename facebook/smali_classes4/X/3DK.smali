.class public LX/3DK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

.field public final b:LX/2ub;

.field public final c:LX/3DO;

.field public final d:Z

.field public final e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;LX/2ub;LX/3DO;)V
    .locals 1

    .prologue
    .line 532104
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/3DK;-><init>(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;LX/2ub;LX/3DO;Z)V

    .line 532105
    return-void
.end method

.method public constructor <init>(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;LX/2ub;LX/3DO;Z)V
    .locals 6

    .prologue
    .line 532106
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, LX/3DK;-><init>(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;LX/2ub;LX/3DO;ZZ)V

    .line 532107
    return-void
.end method

.method private constructor <init>(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;LX/2ub;LX/3DO;ZZ)V
    .locals 0

    .prologue
    .line 532108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 532109
    iput-object p1, p0, LX/3DK;->a:Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    .line 532110
    iput-object p2, p0, LX/3DK;->b:LX/2ub;

    .line 532111
    iput-object p3, p0, LX/3DK;->c:LX/3DO;

    .line 532112
    iput-boolean p4, p0, LX/3DK;->d:Z

    .line 532113
    iput-boolean p5, p0, LX/3DK;->e:Z

    .line 532114
    return-void
.end method
