.class public final LX/2lY;
.super LX/0xA;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)V
    .locals 0

    .prologue
    .line 458098
    iput-object p1, p0, LX/2lY;->a:Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;

    invoke-direct {p0}, LX/0xA;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/12j;I)V
    .locals 9

    .prologue
    .line 458099
    sget-object v0, LX/12j;->INBOX:LX/12j;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/2lY;->a:Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;

    iget-object v0, v0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ag:Lcom/facebook/bookmark/model/Bookmark;

    if-eqz v0, :cond_0

    .line 458100
    iget-object v0, p0, LX/2lY;->a:Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;

    .line 458101
    iget-object v1, v0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ah:LX/0g8;

    if-nez v1, :cond_1

    .line 458102
    :cond_0
    :goto_0
    return-void

    .line 458103
    :cond_1
    iget-object v1, v0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ah:LX/0g8;

    invoke-interface {v1}, LX/0g8;->q()I

    move-result v1

    .line 458104
    iget-object v2, v0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ah:LX/0g8;

    invoke-interface {v2}, LX/0g8;->r()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    .line 458105
    iget-object v1, v0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ah:LX/0g8;

    invoke-interface {v1, v2}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2lH;

    .line 458106
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/2lH;->c()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, LX/2lH;->c()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v4

    iget-wide v5, v4, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const-wide v7, 0xc63f291b142bL

    cmp-long v4, v5, v7

    if-nez v4, :cond_2

    invoke-virtual {v1}, LX/2lH;->c()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/bookmark/model/Bookmark;->b()I

    move-result v1

    iget-object v4, v0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->v:LX/0xB;

    sget-object v5, LX/12j;->INBOX:LX/12j;

    invoke-virtual {v4, v5}, LX/0xB;->a(LX/12j;)I

    move-result v4

    if-eq v1, v4, :cond_2

    .line 458107
    iget-object v1, v0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ah:LX/0g8;

    invoke-interface {v1, v2}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2lH;

    .line 458108
    iget-object v3, v0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ah:LX/0g8;

    invoke-interface {v3, v2}, LX/0g8;->c(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2lH;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 458109
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method
