.class public LX/270;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 372719
    const-class v0, LX/270;

    sput-object v0, LX/270;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 372676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(ILjava/util/List;Landroid/util/SparseArray;)Ljava/lang/String;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "LX/0eu;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "LX/0eu;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 372677
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 372678
    const-wide/16 v8, -0x1

    .line 372679
    invoke-static {}, LX/0PZ;->a()J

    move-result-wide v12

    .line 372680
    const/4 v7, 0x0

    .line 372681
    const/4 v6, 0x0

    .line 372682
    const-wide/16 v4, -0x1

    .line 372683
    const/4 v2, 0x0

    move v10, v7

    move/from16 v17, v6

    move-wide v6, v8

    move v9, v2

    move/from16 v8, v17

    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v9, v2, :cond_3

    .line 372684
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0eu;

    .line 372685
    if-eqz v2, :cond_8

    .line 372686
    if-nez v8, :cond_9

    .line 372687
    invoke-virtual {v2}, LX/0eu;->b()I

    move-result v11

    move/from16 v0, p0

    if-ne v11, v0, :cond_8

    .line 372688
    const/4 v8, 0x1

    .line 372689
    invoke-virtual {v2}, LX/0eu;->f()J

    move-result-wide v4

    move v11, v8

    .line 372690
    :goto_1
    invoke-virtual {v2}, LX/0eu;->c()LX/49P;

    move-result-object v14

    .line 372691
    sget-object v8, LX/49P;->SPAWN:LX/49P;

    if-eq v14, v8, :cond_7

    .line 372692
    sget-object v8, LX/49P;->STOP:LX/49P;

    if-eq v14, v8, :cond_0

    sget-object v8, LX/49P;->STOP_ASYNC:LX/49P;

    if-ne v14, v8, :cond_6

    .line 372693
    :cond_0
    if-nez v10, :cond_2

    .line 372694
    sget-object v8, LX/270;->a:Ljava/lang/Class;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Trace contains a stop event without a corresponding start: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v8, v15}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    move v8, v10

    .line 372695
    :goto_2
    const-string v10, " "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372696
    invoke-virtual/range {v2 .. v8}, LX/0eu;->a(Ljava/lang/StringBuilder;JJI)V

    .line 372697
    const-string v6, " "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372698
    invoke-virtual {v2}, LX/0eu;->f()J

    move-result-wide v6

    .line 372699
    const-string v2, "\n"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372700
    sget-object v2, LX/49P;->START:LX/49P;

    if-eq v14, v2, :cond_1

    sget-object v2, LX/49P;->START_ASYNC:LX/49P;

    if-ne v14, v2, :cond_5

    .line 372701
    :cond_1
    add-int/lit8 v10, v8, 0x1

    move/from16 v17, v11

    move/from16 v18, v10

    move-wide v10, v6

    move/from16 v6, v17

    move/from16 v7, v18

    .line 372702
    :goto_3
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    move v8, v6

    move/from16 v17, v7

    move-wide v6, v10

    move/from16 v10, v17

    goto :goto_0

    .line 372703
    :cond_2
    add-int/lit8 v10, v10, -0x1

    move v8, v10

    goto :goto_2

    .line 372704
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-eqz v2, :cond_4

    .line 372705
    const-string v2, " Unstopped timers:\n"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372706
    invoke-virtual/range {p2 .. p2}, Landroid/util/SparseArray;->size()I

    move-result v5

    .line 372707
    const/4 v2, 0x0

    move v4, v2

    :goto_4
    if-ge v4, v5, :cond_4

    .line 372708
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0eu;

    .line 372709
    invoke-virtual {v2}, LX/0eu;->f()J

    move-result-wide v6

    .line 372710
    const-string v8, "  "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372711
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 372712
    const-string v2, " ("

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372713
    sub-long v8, v12, v6

    const-wide/32 v10, 0xf4240

    div-long/2addr v8, v10

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 372714
    const-string v2, " ms, started at "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372715
    const-wide/32 v8, 0xf4240

    div-long/2addr v6, v8

    invoke-static {v6, v7}, LX/0eu;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372716
    const-string v2, "\n"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372717
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_4

    .line 372718
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_5
    move/from16 v17, v11

    move-wide v10, v6

    move v7, v8

    move/from16 v6, v17

    goto :goto_3

    :cond_6
    move v8, v10

    goto/16 :goto_2

    :cond_7
    move/from16 v17, v11

    move/from16 v18, v10

    move-wide v10, v6

    move/from16 v6, v17

    move/from16 v7, v18

    goto :goto_3

    :cond_8
    move/from16 v17, v10

    move-wide v10, v6

    move v6, v8

    move/from16 v7, v17

    goto :goto_3

    :cond_9
    move v11, v8

    goto/16 :goto_1
.end method

.method public static a(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 372642
    const-string v0, "Thread trace:("

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372643
    const/16 v0, 0xa

    if-ge p1, v0, :cond_1

    .line 372644
    const-string v0, "    "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372645
    :cond_0
    :goto_0
    const-string v0, "                 "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372646
    invoke-static {p0, p2, p3}, LX/270;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 372647
    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372648
    const-string v0, " .                   TOTAL   THREAD  "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372649
    invoke-static {p0, p2, p3}, LX/270;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 372650
    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372651
    return-void

    .line 372652
    :cond_1
    const/16 v0, 0x64

    if-ge p1, v0, :cond_2

    .line 372653
    const-string v0, "   "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 372654
    :cond_2
    const/16 v0, 0x3e8

    if-ge p1, v0, :cond_0

    .line 372655
    const-string v0, "  "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .prologue
    const/16 v10, 0x7c

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 372656
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 372657
    :cond_0
    return-void

    .line 372658
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v8

    move v7, v1

    move v2, v1

    move v5, v1

    .line 372659
    :goto_0
    if-ge v7, v8, :cond_0

    .line 372660
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v7, v0, :cond_a

    .line 372661
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 372662
    :goto_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v7, v6, :cond_9

    .line 372663
    invoke-virtual {p2, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 372664
    :goto_2
    if-nez v5, :cond_6

    if-ne v0, v10, :cond_6

    move v5, v3

    .line 372665
    :cond_2
    :goto_3
    if-nez v2, :cond_7

    if-ne v6, v10, :cond_7

    move v2, v3

    .line 372666
    :cond_3
    :goto_4
    if-ne v5, v4, :cond_4

    if-eq v2, v4, :cond_0

    .line 372667
    :cond_4
    if-ne v5, v3, :cond_8

    .line 372668
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 372669
    :cond_5
    :goto_5
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 372670
    :cond_6
    if-ne v5, v3, :cond_2

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v9

    if-nez v9, :cond_2

    if-eq v0, v10, :cond_2

    move v5, v4

    .line 372671
    goto :goto_3

    .line 372672
    :cond_7
    if-ne v2, v3, :cond_3

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v9

    if-nez v9, :cond_3

    if-eq v6, v10, :cond_3

    move v2, v4

    .line 372673
    goto :goto_4

    .line 372674
    :cond_8
    if-ne v2, v3, :cond_5

    .line 372675
    invoke-virtual {p0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_9
    move v6, v1

    goto :goto_2

    :cond_a
    move v0, v1

    goto :goto_1
.end method
