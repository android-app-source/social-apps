.class public LX/2Q3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/2Q3;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/pm/PackageManager;

.field private final c:LX/03V;

.field private final d:LX/0Xl;

.field private final e:LX/0Xw;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/1fX;

.field private final i:LX/2Q4;

.field private final j:LX/0Yd;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/03V;LX/0Xw;LX/0Xl;LX/1fX;LX/0Or;LX/0Or;LX/2Q4;)V
    .locals 2
    .param p5    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
        .end annotation
    .end param
    .param p6    # LX/1fX;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserTrustedTester;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/pm/PackageManager;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Xw;",
            "LX/0Xl;",
            "LX/1fX;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/2Q4;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 407549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407550
    iput-object p1, p0, LX/2Q3;->a:Landroid/content/Context;

    .line 407551
    iput-object p2, p0, LX/2Q3;->b:Landroid/content/pm/PackageManager;

    .line 407552
    iput-object p3, p0, LX/2Q3;->c:LX/03V;

    .line 407553
    iput-object p4, p0, LX/2Q3;->e:LX/0Xw;

    .line 407554
    iput-object p5, p0, LX/2Q3;->d:LX/0Xl;

    .line 407555
    iput-object p7, p0, LX/2Q3;->f:LX/0Or;

    .line 407556
    iput-object p6, p0, LX/2Q3;->h:LX/1fX;

    .line 407557
    iput-object p8, p0, LX/2Q3;->g:LX/0Or;

    .line 407558
    iput-object p9, p0, LX/2Q3;->i:LX/2Q4;

    .line 407559
    new-instance v0, LX/0Yd;

    .line 407560
    new-instance v1, LX/2QL;

    invoke-direct {v1, p0}, LX/2QL;-><init>(LX/2Q3;)V

    .line 407561
    new-instance p1, LX/2QM;

    invoke-direct {p1, p0}, LX/2QM;-><init>(LX/2Q3;)V

    .line 407562
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object p2

    const-string p3, "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"

    invoke-virtual {p2, p3, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string p2, "com.facebook.orca.login.AuthStateMachineMonitor.LOGOUT_COMPLETE"

    invoke-virtual {v1, p2, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    move-object v1, v1

    .line 407563
    invoke-direct {v0, v1}, LX/0Yd;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, LX/2Q3;->j:LX/0Yd;

    .line 407564
    return-void
.end method

.method public static a(LX/0QB;)LX/2Q3;
    .locals 13

    .prologue
    .line 407536
    sget-object v0, LX/2Q3;->k:LX/2Q3;

    if-nez v0, :cond_1

    .line 407537
    const-class v1, LX/2Q3;

    monitor-enter v1

    .line 407538
    :try_start_0
    sget-object v0, LX/2Q3;->k:LX/2Q3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 407539
    if-eqz v2, :cond_0

    .line 407540
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 407541
    new-instance v3, LX/2Q3;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v5

    check-cast v5, Landroid/content/pm/PackageManager;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/29m;->a(LX/0QB;)LX/0Xw;

    move-result-object v7

    check-cast v7, LX/0Xw;

    invoke-static {v0}, LX/0aQ;->a(LX/0QB;)LX/0aQ;

    move-result-object v8

    check-cast v8, LX/0Xl;

    invoke-static {v0}, LX/1fV;->b(LX/0QB;)LX/1fW;

    move-result-object v9

    check-cast v9, LX/1fX;

    const/16 v10, 0x2fd

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x2ff

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, LX/2Q4;->b(LX/0QB;)LX/2Q4;

    move-result-object v12

    check-cast v12, LX/2Q4;

    invoke-direct/range {v3 .. v12}, LX/2Q3;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/03V;LX/0Xw;LX/0Xl;LX/1fX;LX/0Or;LX/0Or;LX/2Q4;)V

    .line 407542
    move-object v0, v3

    .line 407543
    sput-object v0, LX/2Q3;->k:LX/2Q3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 407544
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 407545
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 407546
    :cond_1
    sget-object v0, LX/2Q3;->k:LX/2Q3;

    return-object v0

    .line 407547
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 407548
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2Q3;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 407531
    iget-object v0, p0, LX/2Q3;->b:Landroid/content/pm/PackageManager;

    iget-object v1, p0, LX/2Q3;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/1ZP;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 407532
    if-nez v0, :cond_0

    .line 407533
    sget-object v0, LX/1ZP;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 407534
    iget-object v0, p0, LX/2Q3;->d:LX/0Xl;

    invoke-interface {v0, p1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 407535
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/2Q3;Z)V
    .locals 2

    .prologue
    .line 407507
    iget-object v0, p0, LX/2Q3;->h:LX/1fX;

    new-instance v1, Lcom/facebook/oxygen/preloads/integration/dogfooding/AuthListener$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/oxygen/preloads/integration/dogfooding/AuthListener$1;-><init>(LX/2Q3;Z)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 407508
    return-void
.end method

.method public static b(LX/2Q3;Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 407523
    :try_start_0
    iget-object v1, p0, LX/2Q3;->b:Landroid/content/pm/PackageManager;

    sget-object v2, LX/1ZP;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 407524
    :try_start_1
    iget-object v1, p0, LX/2Q3;->i:LX/2Q4;

    .line 407525
    iget-object v2, v1, LX/2Q4;->b:LX/2QK;

    invoke-virtual {v2}, LX/2QK;->a()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 407526
    sget-object v0, LX/1ZP;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 407527
    iget-object v0, p0, LX/2Q3;->a:Landroid/content/Context;

    sget-object v1, LX/1ZP;->b:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 407528
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 407529
    :catch_0
    iget-object v1, p0, LX/2Q3;->c:LX/03V;

    const-string v2, "AuthListener"

    const-string v3, "AppManager does not own permission."

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 407530
    :catch_1
    goto :goto_0
.end method

.method public static c(LX/2Q3;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 407517
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 407518
    new-instance v2, Landroid/content/ComponentName;

    iget-object v0, p0, LX/2Q3;->a:Landroid/content/Context;

    const-class v3, Lcom/facebook/oxygen/preloads/integration/dogfooding/AuthListener$EmployeeLoggedInMarkerReceiver;

    invoke-direct {v2, v0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 407519
    if-eqz p1, :cond_0

    move v0, v1

    .line 407520
    :goto_0
    iget-object v3, p0, LX/2Q3;->b:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v2, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 407521
    return-void

    .line 407522
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static c(LX/2Q3;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 407516
    iget-object v0, p0, LX/2Q3;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2Q3;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 407509
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 407510
    const-string v1, "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 407511
    const-string v1, "com.facebook.orca.login.AuthStateMachineMonitor.LOGOUT_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 407512
    iget-object v1, p0, LX/2Q3;->e:LX/0Xw;

    iget-object v2, p0, LX/2Q3;->j:LX/0Yd;

    invoke-virtual {v1, v2, v0}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 407513
    invoke-static {p0}, LX/2Q3;->c(LX/2Q3;)Z

    move-result v0

    .line 407514
    iget-object v1, p0, LX/2Q3;->h:LX/1fX;

    new-instance v2, Lcom/facebook/oxygen/preloads/integration/dogfooding/AuthListener$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/oxygen/preloads/integration/dogfooding/AuthListener$2;-><init>(LX/2Q3;Z)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 407515
    return-void
.end method
