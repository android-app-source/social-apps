.class public final LX/2rF;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 471783
    const-class v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel;

    const v0, 0xb28b445

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FBDirectSharingInboxQuery"

    const-string v6, "f1a570c54b9f4c8924e8dabd24fd2db9"

    const-string v7, "me"

    const-string v8, "10155069964361729"

    const-string v9, "10155259086866729"

    .line 471784
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 471785
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 471786
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 471787
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 471788
    sparse-switch v0, :sswitch_data_0

    .line 471789
    :goto_0
    return-object p1

    .line 471790
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 471791
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 471792
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 471793
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 471794
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 471795
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x16be9c4b -> :sswitch_1
        -0xdbacfae -> :sswitch_0
        -0x9c6725b -> :sswitch_2
        0x35e001 -> :sswitch_4
        0x683094a -> :sswitch_3
        0x6f885587 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 471796
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 471797
    :goto_1
    return v0

    .line 471798
    :pswitch_0
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 471799
    :pswitch_1
    const/16 v0, 0x14

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x35
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
