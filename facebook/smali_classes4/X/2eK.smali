.class public LX/2eK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<SubProps:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/2eJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2eJ",
            "<TSubProps;TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/2eG;

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/03V;

.field public e:I


# direct methods
.method public constructor <init>(LX/2eJ;LX/2eG;LX/03V;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2eJ",
            "<TSubProps;TE;>;",
            "LX/2eG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 445165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2eK;->c:Ljava/util/ArrayList;

    .line 445167
    const/4 v0, 0x0

    iput v0, p0, LX/2eK;->e:I

    .line 445168
    iput-object p1, p0, LX/2eK;->a:LX/2eJ;

    .line 445169
    iput-object p2, p0, LX/2eK;->b:LX/2eG;

    .line 445170
    iput-object p3, p0, LX/2eK;->d:LX/03V;

    .line 445171
    iget-object v0, p0, LX/2eK;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 445172
    const/4 v0, 0x0

    iget-object v1, p0, LX/2eK;->a:LX/2eJ;

    invoke-interface {v1}, LX/2eJ;->a()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_2

    .line 445173
    iget-object p1, p0, LX/2eK;->a:LX/2eJ;

    invoke-interface {p1, v0}, LX/2eJ;->a(I)LX/1RC;

    move-result-object p1

    iget-object p2, p0, LX/2eK;->a:LX/2eJ;

    invoke-interface {p2, v0}, LX/2eJ;->b(I)Ljava/lang/Object;

    move-result-object p2

    .line 445174
    instance-of p3, p1, LX/1RB;

    if-eqz p3, :cond_0

    check-cast p1, LX/1RB;

    invoke-interface {p1, p2}, LX/1RB;->a(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_3

    :cond_0
    const/4 p3, 0x1

    :goto_1
    move p1, p3

    .line 445175
    if-eqz p1, :cond_1

    .line 445176
    iget-object p1, p0, LX/2eK;->c:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445177
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 445178
    :cond_2
    return-void

    :cond_3
    const/4 p3, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final e()I
    .locals 1

    .prologue
    .line 445164
    iget-object v0, p0, LX/2eK;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
