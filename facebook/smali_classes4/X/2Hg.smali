.class public LX/2Hg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Fp;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final o:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public e:LX/0So;
    .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Landroid/content/Context;
    .annotation build Lcom/facebook/inject/ForAppContext;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/2Hb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/2Fw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/2Fu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 390912
    const-class v0, LX/2Hg;

    sput-object v0, LX/2Hg;->o:Ljava/lang/Class;

    .line 390913
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/2Hg;->o:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_Acceleration"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Hg;->a:Ljava/lang/String;

    .line 390914
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/2Hg;->o:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_Decelaration"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Hg;->b:Ljava/lang/String;

    .line 390915
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/2Hg;->o:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_SpeedIncrease"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Hg;->c:Ljava/lang/String;

    .line 390916
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/2Hg;->o:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_SpeedDecrease"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Hg;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390911
    return-void
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/Hlw;Landroid/location/Location;J)F
    .locals 11
    .param p0    # Lcom/facebook/analytics/logger/HoneyClientEvent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x0

    .line 390901
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 390902
    iget-wide v0, p1, LX/Hlw;->a:D

    iget-wide v2, p1, LX/Hlw;->b:D

    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 390903
    iget-wide v0, p1, LX/Hlw;->d:J

    sub-long v0, p3, v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 390904
    aget v2, v8, v9

    long-to-float v3, v0

    div-float/2addr v2, v3

    .line 390905
    if-eqz p0, :cond_0

    .line 390906
    const-string v3, "distanceMeters"

    aget v4, v8, v9

    float-to-double v4, v4

    invoke-virtual {p0, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390907
    const-string v3, "intervalMs"

    invoke-virtual {p0, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390908
    const-string v0, "speedMetersPerSec"

    float-to-double v4, v2

    invoke-virtual {p0, v0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390909
    :cond_0
    return v2
.end method

.method private static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 390893
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 390894
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 390895
    :sswitch_0
    const-string v1, "no_power"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "low_power"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "balanced_power_accuracy"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "high_accuracy"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 390896
    :pswitch_0
    const/16 v0, 0x69

    .line 390897
    :goto_1
    return v0

    .line 390898
    :pswitch_1
    const/16 v0, 0x68

    goto :goto_1

    .line 390899
    :pswitch_2
    const/16 v0, 0x66

    goto :goto_1

    .line 390900
    :pswitch_3
    const/16 v0, 0x64

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x46b8bd0a -> :sswitch_3
        -0x28fe3fa6 -> :sswitch_1
        -0x2c0f8f6 -> :sswitch_2
        0x3b0fe507 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/2Hg;
    .locals 11

    .prologue
    .line 390917
    new-instance v0, LX/2Hg;

    invoke-direct {v0}, LX/2Hg;-><init>()V

    .line 390918
    invoke-static {p0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    const-class v2, Landroid/content/Context;

    const-class v3, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v2, v3}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/2Hb;->b(LX/0QB;)LX/2Hb;

    move-result-object v7

    check-cast v7, LX/2Hb;

    invoke-static {p0}, LX/2Fw;->b(LX/0QB;)LX/2Fw;

    move-result-object v8

    check-cast v8, LX/2Fw;

    invoke-static {p0}, LX/2Fu;->b(LX/0QB;)LX/2Fu;

    move-result-object v9

    check-cast v9, LX/2Fu;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v10

    check-cast v10, LX/0W3;

    .line 390919
    iput-object v1, v0, LX/2Hg;->e:LX/0So;

    iput-object v2, v0, LX/2Hg;->f:Landroid/content/Context;

    iput-object v3, v0, LX/2Hg;->g:Ljava/util/concurrent/Executor;

    iput-object v4, v0, LX/2Hg;->h:LX/0Zb;

    iput-object v5, v0, LX/2Hg;->i:LX/03V;

    iput-object v6, v0, LX/2Hg;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v7, v0, LX/2Hg;->k:LX/2Hb;

    iput-object v8, v0, LX/2Hg;->l:LX/2Fw;

    iput-object v9, v0, LX/2Hg;->m:LX/2Fu;

    iput-object v10, v0, LX/2Hg;->n:LX/0W3;

    .line 390920
    return-object v0
.end method

.method private e()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 390839
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2Hg;->f:Landroid/content/Context;

    const-class v2, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitorReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 390840
    iget-object v1, p0, LX/2Hg;->f:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 390841
    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 390842
    sget-wide v0, LX/0X5;->S:J

    return-wide v0
.end method

.method public final a(Landroid/location/Location;)V
    .locals 12
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    const/high16 v6, -0x40800000    # -1.0f

    const-wide/16 v10, 0x0

    .line 390843
    iget-object v0, p0, LX/2Hg;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v7

    .line 390844
    const/4 v0, 0x0

    .line 390845
    iget-object v1, p0, LX/2Hg;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2Fn;->f:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 390846
    if-nez v1, :cond_6

    .line 390847
    :goto_0
    move-object v0, v0

    .line 390848
    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "speed_change_monitor_update"

    invoke-direct {v9, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 390849
    const-string v1, "collection_reason"

    const-string v2, "collection_not_started"

    invoke-virtual {v9, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390850
    if-nez v0, :cond_1

    .line 390851
    new-instance v1, LX/Hlw;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct/range {v1 .. v8}, LX/Hlw;-><init>(DDFJ)V

    .line 390852
    :cond_0
    :goto_1
    invoke-virtual {v9}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g()Ljava/lang/String;

    .line 390853
    iget-object v0, p0, LX/2Hg;->h:LX/0Zb;

    invoke-interface {v0, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 390854
    iget-object v0, p0, LX/2Hg;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/2Fn;->f:LX/0Tn;

    invoke-virtual {v1}, LX/Hlw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 390855
    return-void

    .line 390856
    :cond_1
    iget v1, v0, LX/Hlw;->c:F

    cmpl-float v1, v1, v6

    if-nez v1, :cond_2

    .line 390857
    new-instance v1, LX/Hlw;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v9, v0, p1, v7, v8}, LX/2Hg;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/Hlw;Landroid/location/Location;J)F

    move-result v6

    invoke-direct/range {v1 .. v8}, LX/Hlw;-><init>(DDFJ)V

    goto :goto_1

    .line 390858
    :cond_2
    new-instance v1, LX/Hlw;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v9, v0, p1, v7, v8}, LX/2Hg;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/Hlw;Landroid/location/Location;J)F

    move-result v6

    invoke-direct/range {v1 .. v8}, LX/Hlw;-><init>(DDFJ)V

    .line 390859
    iget-object v2, p0, LX/2Hg;->n:LX/0W3;

    sget-wide v4, LX/0X5;->N:J

    invoke-interface {v2, v4, v5}, LX/0W4;->g(J)D

    move-result-wide v2

    .line 390860
    iget-object v4, p0, LX/2Hg;->n:LX/0W3;

    sget-wide v6, LX/0X5;->L:J

    invoke-interface {v4, v6, v7}, LX/0W4;->g(J)D

    move-result-wide v4

    .line 390861
    cmpl-double v6, v2, v10

    if-lez v6, :cond_4

    iget v6, v0, LX/Hlw;->c:F

    float-to-double v6, v6

    cmpg-double v6, v6, v2

    if-gtz v6, :cond_4

    iget v6, v1, LX/Hlw;->c:F

    float-to-double v6, v6

    cmpl-double v2, v6, v2

    if-lez v2, :cond_4

    .line 390862
    const-string v2, "collection_reason"

    sget-object v3, LX/2Hg;->c:Ljava/lang/String;

    invoke-virtual {v9, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390863
    iget-object v2, p0, LX/2Hg;->m:LX/2Fu;

    sget-object v3, LX/2Hg;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/2Fu;->a(Ljava/lang/String;)V

    .line 390864
    :cond_3
    :goto_2
    iget v0, v0, LX/Hlw;->c:F

    const v2, 0x3c23d70a    # 0.01f

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 390865
    iget v2, v1, LX/Hlw;->c:F

    div-float v0, v2, v0

    .line 390866
    const-string v2, "pct_change"

    float-to-double v4, v0

    invoke-virtual {v9, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390867
    iget-object v2, p0, LX/2Hg;->n:LX/0W3;

    sget-wide v4, LX/0X5;->Q:J

    invoke-interface {v2, v4, v5}, LX/0W4;->g(J)D

    move-result-wide v2

    .line 390868
    iget-object v4, p0, LX/2Hg;->n:LX/0W3;

    sget-wide v6, LX/0X5;->P:J

    invoke-interface {v4, v6, v7}, LX/0W4;->g(J)D

    move-result-wide v4

    .line 390869
    cmpl-double v6, v2, v10

    if-lez v6, :cond_5

    float-to-double v6, v0

    cmpl-double v2, v6, v2

    if-lez v2, :cond_5

    .line 390870
    const-string v0, "collection_reason"

    sget-object v2, LX/2Hg;->a:Ljava/lang/String;

    invoke-virtual {v9, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390871
    iget-object v0, p0, LX/2Hg;->m:LX/2Fu;

    sget-object v2, LX/2Hg;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/2Fu;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 390872
    :cond_4
    cmpl-double v2, v4, v10

    if-lez v2, :cond_3

    iget v2, v0, LX/Hlw;->c:F

    float-to-double v2, v2

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_3

    iget v2, v1, LX/Hlw;->c:F

    float-to-double v2, v2

    cmpg-double v2, v2, v4

    if-gez v2, :cond_3

    .line 390873
    const-string v2, "collection_reason"

    sget-object v3, LX/2Hg;->d:Ljava/lang/String;

    invoke-virtual {v9, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390874
    iget-object v2, p0, LX/2Hg;->m:LX/2Fu;

    sget-object v3, LX/2Hg;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/2Fu;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 390875
    :cond_5
    cmpl-double v2, v4, v10

    if-lez v2, :cond_0

    float-to-double v2, v0

    cmpg-double v0, v2, v4

    if-gez v0, :cond_0

    .line 390876
    const-string v0, "collection_reason"

    sget-object v2, LX/2Hg;->b:Ljava/lang/String;

    invoke-virtual {v9, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390877
    iget-object v0, p0, LX/2Hg;->m:LX/2Fu;

    sget-object v2, LX/2Hg;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/2Fu;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_6
    new-instance v0, LX/Hlw;

    invoke-direct {v0, v1}, LX/Hlw;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 390878
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 390879
    iget-object v1, p0, LX/2Hg;->f:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/1od;->a(Landroid/content/Context;)I

    move-result v0

    .line 390880
    if-nez v0, :cond_0

    .line 390881
    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iget-object v1, p0, LX/2Hg;->n:LX/0W3;

    sget-wide v2, LX/0X5;->R:J

    invoke-interface {v1, v2, v3}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/2Hg;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iget-object v1, p0, LX/2Hg;->n:LX/0W3;

    sget-wide v2, LX/0X5;->O:J

    invoke-interface {v1, v2, v3}, LX/0W4;->c(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iget-object v1, p0, LX/2Hg;->n:LX/0W3;

    sget-wide v2, LX/0X5;->M:J

    invoke-interface {v1, v2, v3}, LX/0W4;->c(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->c(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    .line 390882
    :try_start_0
    iget-object v1, p0, LX/2Hg;->l:LX/2Fw;

    invoke-direct {p0}, LX/2Hg;->e()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/2Fw;->a(Landroid/app/PendingIntent;Lcom/google/android/gms/location/LocationRequest;)V
    :try_end_0
    .catch LX/6ZF; {:try_start_0 .. :try_end_0} :catch_0

    .line 390883
    :cond_0
    :goto_0
    return-void

    .line 390884
    :catch_0
    move-exception v0

    .line 390885
    iget-object v1, p0, LX/2Hg;->i:LX/03V;

    sget-object v2, LX/2Hg;->o:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error starting monitor"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 390886
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 390887
    iget-object v1, p0, LX/2Hg;->f:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/1od;->a(Landroid/content/Context;)I

    move-result v0

    .line 390888
    if-nez v0, :cond_0

    .line 390889
    invoke-direct {p0}, LX/2Hg;->e()Landroid/app/PendingIntent;

    move-result-object v0

    .line 390890
    iget-object v1, p0, LX/2Hg;->l:LX/2Fw;

    invoke-virtual {v1, v0}, LX/2Fw;->a(Landroid/app/PendingIntent;)V

    .line 390891
    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 390892
    :cond_0
    return-void
.end method
