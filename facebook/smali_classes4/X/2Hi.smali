.class public LX/2Hi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile e:LX/2Hi;


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:Landroid/content/Context;

.field private d:LX/0dN;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 390949
    sget-object v0, LX/0eJ;->n:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/2Hi;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390925
    iput-object p1, p0, LX/2Hi;->c:Landroid/content/Context;

    .line 390926
    iput-object p2, p0, LX/2Hi;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 390927
    return-void
.end method

.method public static a(LX/0QB;)LX/2Hi;
    .locals 5

    .prologue
    .line 390936
    sget-object v0, LX/2Hi;->e:LX/2Hi;

    if-nez v0, :cond_1

    .line 390937
    const-class v1, LX/2Hi;

    monitor-enter v1

    .line 390938
    :try_start_0
    sget-object v0, LX/2Hi;->e:LX/2Hi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390939
    if-eqz v2, :cond_0

    .line 390940
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 390941
    new-instance p0, LX/2Hi;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4}, LX/2Hi;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 390942
    move-object v0, p0

    .line 390943
    sput-object v0, LX/2Hi;->e:LX/2Hi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390944
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390945
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390946
    :cond_1
    sget-object v0, LX/2Hi;->e:LX/2Hi;

    return-object v0

    .line 390947
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390948
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2Hi;)V
    .locals 3

    .prologue
    .line 390932
    iget-object v0, p0, LX/2Hi;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0eJ;->n:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390933
    iget-object v0, p0, LX/2Hi;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/breakpad/BreakpadManager;->a(Landroid/content/Context;)Z

    .line 390934
    :goto_0
    return-void

    .line 390935
    :cond_0
    invoke-static {}, Lcom/facebook/breakpad/BreakpadManager;->d()Z

    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 390928
    new-instance v0, LX/2Hj;

    invoke-direct {v0, p0}, LX/2Hj;-><init>(LX/2Hi;)V

    iput-object v0, p0, LX/2Hi;->d:LX/0dN;

    .line 390929
    iget-object v0, p0, LX/2Hi;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2Hi;->a:Ljava/util/Set;

    iget-object v2, p0, LX/2Hi;->d:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;LX/0dN;)V

    .line 390930
    invoke-static {p0}, LX/2Hi;->a$redex0(LX/2Hi;)V

    .line 390931
    return-void
.end method
