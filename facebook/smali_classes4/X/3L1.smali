.class public LX/3L1;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 549804
    invoke-direct {p0}, LX/0Q6;-><init>()V

    return-void
.end method

.method public static a(LX/1zC;Landroid/content/res/Resources;LX/3L0;LX/3GL;LX/0Uh;)LX/3L2;
    .locals 7
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/ui/name/DefaultThreadNameViewComputer;
    .end annotation

    .prologue
    .line 549803
    new-instance v0, LX/3L2;

    sget-object v4, LX/3L4;->USE_THREAD_NAME_IF_AVAILABLE:LX/3L4;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/3L2;-><init>(LX/1zC;Landroid/content/res/Resources;LX/3L0;LX/3L4;LX/3GL;LX/0Uh;)V

    return-object v0
.end method

.method public static b(LX/1zC;Landroid/content/res/Resources;LX/3L0;LX/3GL;LX/0Uh;)LX/3L2;
    .locals 7
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/ui/name/NamesOnlyThreadNameViewComputer;
    .end annotation

    .prologue
    .line 549802
    new-instance v0, LX/3L2;

    sget-object v4, LX/3L4;->USE_PARTICIPANTS_NAMES_ONLY:LX/3L4;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/3L2;-><init>(LX/1zC;Landroid/content/res/Resources;LX/3L0;LX/3L4;LX/3GL;LX/0Uh;)V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 549801
    return-void
.end method
