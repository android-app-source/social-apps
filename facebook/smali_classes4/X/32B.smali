.class public final enum LX/32B;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/32B;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/32B;

.field public static final enum BOOL:LX/32B;

.field public static final enum COLOR:LX/32B;

.field public static final enum DIMEN_OFFSET:LX/32B;

.field public static final enum DIMEN_SIZE:LX/32B;

.field public static final enum DIMEN_TEXT:LX/32B;

.field public static final enum DRAWABLE:LX/32B;

.field public static final enum FLOAT:LX/32B;

.field public static final enum INT:LX/32B;

.field public static final enum INT_ARRAY:LX/32B;

.field public static final enum NONE:LX/32B;

.field public static final enum STRING:LX/32B;

.field public static final enum STRING_ARRAY:LX/32B;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 489521
    new-instance v0, LX/32B;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/32B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32B;->NONE:LX/32B;

    .line 489522
    new-instance v0, LX/32B;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v4}, LX/32B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32B;->STRING:LX/32B;

    .line 489523
    new-instance v0, LX/32B;

    const-string v1, "STRING_ARRAY"

    invoke-direct {v0, v1, v5}, LX/32B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32B;->STRING_ARRAY:LX/32B;

    .line 489524
    new-instance v0, LX/32B;

    const-string v1, "INT"

    invoke-direct {v0, v1, v6}, LX/32B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32B;->INT:LX/32B;

    .line 489525
    new-instance v0, LX/32B;

    const-string v1, "INT_ARRAY"

    invoke-direct {v0, v1, v7}, LX/32B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32B;->INT_ARRAY:LX/32B;

    .line 489526
    new-instance v0, LX/32B;

    const-string v1, "BOOL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/32B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32B;->BOOL:LX/32B;

    .line 489527
    new-instance v0, LX/32B;

    const-string v1, "COLOR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/32B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32B;->COLOR:LX/32B;

    .line 489528
    new-instance v0, LX/32B;

    const-string v1, "DIMEN_SIZE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/32B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32B;->DIMEN_SIZE:LX/32B;

    .line 489529
    new-instance v0, LX/32B;

    const-string v1, "DIMEN_OFFSET"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/32B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32B;->DIMEN_OFFSET:LX/32B;

    .line 489530
    new-instance v0, LX/32B;

    const-string v1, "DIMEN_TEXT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/32B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32B;->DIMEN_TEXT:LX/32B;

    .line 489531
    new-instance v0, LX/32B;

    const-string v1, "FLOAT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/32B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32B;->FLOAT:LX/32B;

    .line 489532
    new-instance v0, LX/32B;

    const-string v1, "DRAWABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/32B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32B;->DRAWABLE:LX/32B;

    .line 489533
    const/16 v0, 0xc

    new-array v0, v0, [LX/32B;

    sget-object v1, LX/32B;->NONE:LX/32B;

    aput-object v1, v0, v3

    sget-object v1, LX/32B;->STRING:LX/32B;

    aput-object v1, v0, v4

    sget-object v1, LX/32B;->STRING_ARRAY:LX/32B;

    aput-object v1, v0, v5

    sget-object v1, LX/32B;->INT:LX/32B;

    aput-object v1, v0, v6

    sget-object v1, LX/32B;->INT_ARRAY:LX/32B;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/32B;->BOOL:LX/32B;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/32B;->COLOR:LX/32B;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/32B;->DIMEN_SIZE:LX/32B;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/32B;->DIMEN_OFFSET:LX/32B;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/32B;->DIMEN_TEXT:LX/32B;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/32B;->FLOAT:LX/32B;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/32B;->DRAWABLE:LX/32B;

    aput-object v2, v0, v1

    sput-object v0, LX/32B;->$VALUES:[LX/32B;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 489520
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/32B;
    .locals 1

    .prologue
    .line 489519
    const-class v0, LX/32B;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/32B;

    return-object v0
.end method

.method public static values()[LX/32B;
    .locals 1

    .prologue
    .line 489518
    sget-object v0, LX/32B;->$VALUES:[LX/32B;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/32B;

    return-object v0
.end method
