.class public final LX/2pq;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2pb;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/03V;

.field public final c:LX/0So;

.field private final d:J

.field private final e:J

.field public volatile f:LX/2pr;

.field public g:J

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(LX/2pb;LX/03V;LX/0So;Z)V
    .locals 2

    .prologue
    .line 468977
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 468978
    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, LX/2pq;->d:J

    .line 468979
    const-wide/16 v0, 0x9c4

    iput-wide v0, p0, LX/2pq;->e:J

    .line 468980
    sget-object v0, LX/2pr;->UNKNOWN_OR_UNSET:LX/2pr;

    iput-object v0, p0, LX/2pq;->f:LX/2pr;

    .line 468981
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2pq;->a:Ljava/lang/ref/WeakReference;

    .line 468982
    iput-object p2, p0, LX/2pq;->b:LX/03V;

    .line 468983
    iput-boolean p4, p0, LX/2pq;->h:Z

    .line 468984
    iput-object p3, p0, LX/2pq;->c:LX/0So;

    .line 468985
    return-void
.end method

.method public static c(LX/2pq;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 468974
    invoke-virtual {p0, v2}, LX/2pq;->removeMessages(I)V

    .line 468975
    const-wide/16 v0, 0xc8

    invoke-virtual {p0, v2, v0, v1}, LX/2pq;->sendEmptyMessageDelayed(IJ)Z

    .line 468976
    return-void
.end method

.method private d()V
    .locals 12

    .prologue
    const/4 v8, 0x0

    .line 468986
    iget-object v0, p0, LX/2pq;->f:LX/2pr;

    sget-object v1, LX/2pr;->UNKNOWN_OR_UNSET:LX/2pr;

    if-ne v0, v1, :cond_1

    .line 468987
    sget-object v0, LX/2pb;->a:Ljava/lang/String;

    const-string v1, "PlayedForNSeconds status shouldnt be unknown when attempting to send event"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 468988
    :cond_0
    :goto_0
    return-void

    .line 468989
    :cond_1
    iget-object v0, p0, LX/2pq;->f:LX/2pr;

    sget-object v1, LX/2pr;->EVENT_PUBLISHED:LX/2pr;

    if-eq v0, v1, :cond_0

    .line 468990
    iget-object v0, p0, LX/2pq;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, LX/2pb;

    .line 468991
    if-nez v9, :cond_2

    .line 468992
    sget-object v0, LX/2pb;->a:Ljava/lang/String;

    const-string v1, "Trying to send event for a PBC that\'s been destroyed/GC\'ed"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 468993
    sget-object v0, LX/2pr;->UNKNOWN_OR_UNSET:LX/2pr;

    iput-object v0, p0, LX/2pq;->f:LX/2pr;

    goto :goto_0

    .line 468994
    :cond_2
    iget-object v0, p0, LX/2pq;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 468995
    invoke-virtual {v9}, LX/2pb;->h()I

    move-result v2

    int-to-long v6, v2

    .line 468996
    iget-object v2, v9, LX/2pb;->E:LX/2q7;

    invoke-interface {v2}, LX/2q7;->m()I

    move-result v5

    .line 468997
    iget-boolean v2, p0, LX/2pq;->h:Z

    if-eqz v2, :cond_3

    iget-wide v2, p0, LX/2pq;->g:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x9c4

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 468998
    iget-boolean v0, p0, LX/2pq;->i:Z

    if-nez v0, :cond_0

    int-to-long v0, v5

    sub-long v0, v6, v0

    const-wide/16 v2, 0x1770

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 468999
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2pq;->i:Z

    .line 469000
    iget-object v0, p0, LX/2pq;->b:LX/03V;

    const-string v1, "playback"

    const-string v2, "Played event for 3 seconds fired before 3 physical seconds have passed"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 469001
    :cond_3
    int-to-long v0, v5

    sub-long v0, v6, v0

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 469002
    iget-object v0, v9, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->w()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v10

    .line 469003
    iget-object v0, v9, LX/2pb;->g:LX/1C2;

    if-eqz v10, :cond_5

    iget-object v1, v10, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    .line 469004
    :goto_1
    iget-object v2, v9, LX/2pb;->D:LX/04G;

    move-object v2, v2

    .line 469005
    invoke-virtual {v9}, LX/2pb;->w()Ljava/lang/String;

    move-result-object v3

    long-to-int v4, v6

    iget-object v6, v9, LX/2pb;->K:Ljava/lang/String;

    invoke-virtual {v9}, LX/2pb;->s()LX/04D;

    move-result-object v7

    iget-object v11, v9, LX/2pb;->Q:LX/04g;

    if-eqz v11, :cond_4

    iget-object v8, v9, LX/2pb;->Q:LX/04g;

    iget-object v8, v8, LX/04g;->value:Ljava/lang/String;

    :cond_4
    iget-object v9, v9, LX/2pb;->E:LX/2q7;

    invoke-interface {v9}, LX/2q7;->r()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/098;)LX/1C2;

    .line 469006
    sget-object v0, LX/2pr;->EVENT_PUBLISHED:LX/2pr;

    iput-object v0, p0, LX/2pq;->f:LX/2pr;

    goto/16 :goto_0

    :cond_5
    move-object v1, v8

    .line 469007
    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 468970
    invoke-direct {p0}, LX/2pq;->d()V

    .line 468971
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/2pq;->removeMessages(I)V

    .line 468972
    sget-object v0, LX/2pr;->UNKNOWN_OR_UNSET:LX/2pr;

    iput-object v0, p0, LX/2pq;->f:LX/2pr;

    .line 468973
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 468965
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 468966
    :cond_0
    :goto_0
    return-void

    .line 468967
    :pswitch_0
    invoke-direct {p0}, LX/2pq;->d()V

    .line 468968
    iget-object v0, p0, LX/2pq;->f:LX/2pr;

    sget-object v1, LX/2pr;->TIMER_STARTED:LX/2pr;

    if-ne v0, v1, :cond_0

    .line 468969
    invoke-static {p0}, LX/2pq;->c(LX/2pq;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
