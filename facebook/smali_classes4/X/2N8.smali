.class public LX/2N8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2N8;


# instance fields
.field private final a:LX/0lC;


# direct methods
.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 398567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398568
    iput-object p1, p0, LX/2N8;->a:LX/0lC;

    .line 398569
    return-void
.end method

.method public static a(LX/0QB;)LX/2N8;
    .locals 4

    .prologue
    .line 398570
    sget-object v0, LX/2N8;->b:LX/2N8;

    if-nez v0, :cond_1

    .line 398571
    const-class v1, LX/2N8;

    monitor-enter v1

    .line 398572
    :try_start_0
    sget-object v0, LX/2N8;->b:LX/2N8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 398573
    if-eqz v2, :cond_0

    .line 398574
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 398575
    new-instance p0, LX/2N8;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-direct {p0, v3}, LX/2N8;-><init>(LX/0lC;)V

    .line 398576
    move-object v0, p0

    .line 398577
    sput-object v0, LX/2N8;->b:LX/2N8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398578
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 398579
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 398580
    :cond_1
    sget-object v0, LX/2N8;->b:LX/2N8;

    return-object v0

    .line 398581
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 398582
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0lF;
    .locals 2

    .prologue
    .line 398583
    :try_start_0
    iget-object v0, p0, LX/2N8;->a:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 398584
    :catch_0
    move-exception v0

    .line 398585
    new-instance v1, LX/462;

    invoke-direct {v1, v0}, LX/462;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
