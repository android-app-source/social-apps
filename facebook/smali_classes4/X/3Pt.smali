.class public LX/3Pt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/3Pt;


# instance fields
.field public final b:LX/0SG;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 562276
    const-class v0, LX/3Pt;

    sput-object v0, LX/3Pt;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562278
    iput-object p1, p0, LX/3Pt;->b:LX/0SG;

    .line 562279
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/3Pt;->c:Ljava/util/List;

    .line 562280
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/3Pt;->d:Ljava/util/HashMap;

    .line 562281
    return-void
.end method

.method public static a(LX/0QB;)LX/3Pt;
    .locals 4

    .prologue
    .line 562282
    sget-object v0, LX/3Pt;->e:LX/3Pt;

    if-nez v0, :cond_1

    .line 562283
    const-class v1, LX/3Pt;

    monitor-enter v1

    .line 562284
    :try_start_0
    sget-object v0, LX/3Pt;->e:LX/3Pt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 562285
    if-eqz v2, :cond_0

    .line 562286
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 562287
    new-instance p0, LX/3Pt;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p0, v3}, LX/3Pt;-><init>(LX/0SG;)V

    .line 562288
    move-object v0, p0

    .line 562289
    sput-object v0, LX/3Pt;->e:LX/3Pt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 562290
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 562291
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 562292
    :cond_1
    sget-object v0, LX/3Pt;->e:LX/3Pt;

    return-object v0

    .line 562293
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 562294
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 562295
    if-nez p1, :cond_1

    .line 562296
    :cond_0
    :goto_0
    return-void

    .line 562297
    :cond_1
    invoke-virtual {p0, p1}, LX/3Pt;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 562298
    iget-object v0, p0, LX/3Pt;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 562299
    iget-object v0, p0, LX/3Pt;->d:Ljava/util/HashMap;

    iget-object v1, p0, LX/3Pt;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562300
    const/4 v12, 0x0

    .line 562301
    iget-object v4, p0, LX/3Pt;->b:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v6

    .line 562302
    :goto_1
    iget-object v4, p0, LX/3Pt;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 562303
    iget-object v4, p0, LX/3Pt;->c:Ljava/util/List;

    invoke-interface {v4, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 562304
    iget-object v5, p0, LX/3Pt;->d:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 562305
    sub-long v8, v6, v8

    const-wide/32 v10, 0x1b7740

    cmp-long v5, v8, v10

    if-lez v5, :cond_2

    .line 562306
    iget-object v5, p0, LX/3Pt;->c:Ljava/util/List;

    invoke-interface {v5, v12}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 562307
    iget-object v5, p0, LX/3Pt;->d:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 562308
    :cond_2
    iget-object v4, p0, LX/3Pt;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/16 v5, 0x64

    if-le v4, v5, :cond_3

    .line 562309
    :goto_2
    iget-object v4, p0, LX/3Pt;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v4, v4

    const-wide/16 v6, 0x42

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 562310
    iget-object v4, p0, LX/3Pt;->c:Ljava/util/List;

    invoke-interface {v4, v12}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 562311
    iget-object v5, p0, LX/3Pt;->d:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 562312
    :cond_3
    iget-object v4, p0, LX/3Pt;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 562313
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 562314
    iget-object v0, p0, LX/3Pt;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
