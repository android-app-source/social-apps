.class public final LX/2MP;
.super Ljava/util/AbstractQueue;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractQueue",
        "<",
        "LX/0R1",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 396631
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 396632
    new-instance v0, LX/2MQ;

    invoke-direct {v0, p0}, LX/2MQ;-><init>(LX/2MP;)V

    iput-object v0, p0, LX/2MP;->a:LX/0R1;

    return-void
.end method

.method private a()LX/0R1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 396633
    iget-object v0, p0, LX/2MP;->a:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getNextInWriteQueue()LX/0R1;

    move-result-object v0

    .line 396634
    iget-object v1, p0, LX/2MP;->a:LX/0R1;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final clear()V
    .locals 2

    .prologue
    .line 396635
    iget-object v0, p0, LX/2MP;->a:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getNextInWriteQueue()LX/0R1;

    move-result-object v0

    .line 396636
    :goto_0
    iget-object v1, p0, LX/2MP;->a:LX/0R1;

    if-eq v0, v1, :cond_0

    .line 396637
    invoke-interface {v0}, LX/0R1;->getNextInWriteQueue()LX/0R1;

    move-result-object v1

    .line 396638
    invoke-static {v0}, LX/0Qd;->c(LX/0R1;)V

    move-object v0, v1

    .line 396639
    goto :goto_0

    .line 396640
    :cond_0
    iget-object v0, p0, LX/2MP;->a:LX/0R1;

    iget-object v1, p0, LX/2MP;->a:LX/0R1;

    invoke-interface {v0, v1}, LX/0R1;->setNextInWriteQueue(LX/0R1;)V

    .line 396641
    iget-object v0, p0, LX/2MP;->a:LX/0R1;

    iget-object v1, p0, LX/2MP;->a:LX/0R1;

    invoke-interface {v0, v1}, LX/0R1;->setPreviousInWriteQueue(LX/0R1;)V

    .line 396642
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 396643
    check-cast p1, LX/0R1;

    .line 396644
    invoke-interface {p1}, LX/0R1;->getNextInWriteQueue()LX/0R1;

    move-result-object v0

    sget-object v1, LX/0SZ;->INSTANCE:LX/0SZ;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 2

    .prologue
    .line 396645
    iget-object v0, p0, LX/2MP;->a:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getNextInWriteQueue()LX/0R1;

    move-result-object v0

    iget-object v1, p0, LX/2MP;->a:LX/0R1;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/0R1",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 396646
    new-instance v0, LX/4wZ;

    invoke-direct {p0}, LX/2MP;->a()LX/0R1;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/4wZ;-><init>(LX/2MP;LX/0R1;)V

    return-object v0
.end method

.method public final offer(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 396647
    check-cast p1, LX/0R1;

    .line 396648
    invoke-interface {p1}, LX/0R1;->getPreviousInWriteQueue()LX/0R1;

    move-result-object v0

    invoke-interface {p1}, LX/0R1;->getNextInWriteQueue()LX/0R1;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qd;->b(LX/0R1;LX/0R1;)V

    .line 396649
    iget-object v0, p0, LX/2MP;->a:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getPreviousInWriteQueue()LX/0R1;

    move-result-object v0

    invoke-static {v0, p1}, LX/0Qd;->b(LX/0R1;LX/0R1;)V

    .line 396650
    iget-object v0, p0, LX/2MP;->a:LX/0R1;

    invoke-static {p1, v0}, LX/0Qd;->b(LX/0R1;LX/0R1;)V

    .line 396651
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic peek()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 396652
    invoke-direct {p0}, LX/2MP;->a()LX/0R1;

    move-result-object v0

    return-object v0
.end method

.method public final poll()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 396653
    iget-object v0, p0, LX/2MP;->a:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getNextInWriteQueue()LX/0R1;

    move-result-object v0

    .line 396654
    iget-object v1, p0, LX/2MP;->a:LX/0R1;

    if-ne v0, v1, :cond_0

    .line 396655
    const/4 v0, 0x0

    .line 396656
    :goto_0
    return-object v0

    .line 396657
    :cond_0
    invoke-virtual {p0, v0}, LX/2MP;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 396658
    check-cast p1, LX/0R1;

    .line 396659
    invoke-interface {p1}, LX/0R1;->getPreviousInWriteQueue()LX/0R1;

    move-result-object v0

    .line 396660
    invoke-interface {p1}, LX/0R1;->getNextInWriteQueue()LX/0R1;

    move-result-object v1

    .line 396661
    invoke-static {v0, v1}, LX/0Qd;->b(LX/0R1;LX/0R1;)V

    .line 396662
    invoke-static {p1}, LX/0Qd;->c(LX/0R1;)V

    .line 396663
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 3

    .prologue
    .line 396664
    const/4 v1, 0x0

    .line 396665
    iget-object v0, p0, LX/2MP;->a:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getNextInWriteQueue()LX/0R1;

    move-result-object v0

    :goto_0
    iget-object v2, p0, LX/2MP;->a:LX/0R1;

    if-eq v0, v2, :cond_0

    .line 396666
    add-int/lit8 v1, v1, 0x1

    .line 396667
    invoke-interface {v0}, LX/0R1;->getNextInWriteQueue()LX/0R1;

    move-result-object v0

    goto :goto_0

    .line 396668
    :cond_0
    return v1
.end method
