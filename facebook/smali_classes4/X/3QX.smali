.class public LX/3QX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field private final a:LX/2cR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 565212
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3QX;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2cR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 565213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565214
    iput-object p1, p0, LX/3QX;->a:LX/2cR;

    .line 565215
    return-void
.end method

.method public static a(LX/0QB;)LX/3QX;
    .locals 7

    .prologue
    .line 565216
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 565217
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 565218
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 565219
    if-nez v1, :cond_0

    .line 565220
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 565221
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 565222
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 565223
    sget-object v1, LX/3QX;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 565224
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 565225
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 565226
    :cond_1
    if-nez v1, :cond_4

    .line 565227
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 565228
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 565229
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 565230
    new-instance p0, LX/3QX;

    invoke-static {v0}, LX/2cR;->a(LX/0QB;)LX/2cR;

    move-result-object v1

    check-cast v1, LX/2cR;

    invoke-direct {p0, v1}, LX/3QX;-><init>(LX/2cR;)V

    .line 565231
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 565232
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 565233
    if-nez v1, :cond_2

    .line 565234
    sget-object v0, LX/3QX;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3QX;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 565235
    :goto_1
    if-eqz v0, :cond_3

    .line 565236
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 565237
    :goto_3
    check-cast v0, LX/3QX;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 565238
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 565239
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 565240
    :catchall_1
    move-exception v0

    .line 565241
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 565242
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 565243
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 565244
    :cond_2
    :try_start_8
    sget-object v0, LX/3QX;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3QX;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/6hP;)LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6hP;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 565245
    invoke-virtual {p0, p1}, LX/3QX;->b(LX/6hP;)LX/0am;

    move-result-object v0

    .line 565246
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(LX/6hP;I)V
    .locals 4

    .prologue
    .line 565247
    iget-object v0, p0, LX/3QX;->a:LX/2cR;

    .line 565248
    iget-object v1, v0, LX/2cR;->d:Lcom/facebook/omnistore/Collection;

    move-object v0, v1

    .line 565249
    if-nez v0, :cond_0

    .line 565250
    new-instance v0, LX/6hO;

    const-string v1, "Unable to access messenger_user_prefs collection to write"

    invoke-direct {v0, v1}, LX/6hO;-><init>(Ljava/lang/String;)V

    throw v0

    .line 565251
    :cond_0
    iget-object v1, p1, LX/6hP;->keyString:Ljava/lang/String;

    const-string v2, ""

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/omnistore/Collection;->saveObject(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 565252
    return-void
.end method

.method public final b(LX/6hP;)LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6hP;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 565253
    iget-object v0, p0, LX/3QX;->a:LX/2cR;

    .line 565254
    iget-object v1, v0, LX/2cR;->d:Lcom/facebook/omnistore/Collection;

    move-object v0, v1

    .line 565255
    if-nez v0, :cond_0

    .line 565256
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 565257
    :goto_0
    return-object v0

    .line 565258
    :cond_0
    iget-object v1, p1, LX/6hP;->keyString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/omnistore/Collection;->getObject(Ljava/lang/String;)Lcom/facebook/omnistore/Cursor;

    move-result-object v0

    .line 565259
    invoke-virtual {v0}, Lcom/facebook/omnistore/Cursor;->step()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/omnistore/Cursor;->getBlob()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method
