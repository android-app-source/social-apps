.class public LX/2NN;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/2NN;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/03V;

.field public final e:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 399742
    const-class v0, LX/2NN;

    sput-object v0, LX/2NN;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;Ljava/lang/Boolean;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 399743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399744
    iput-object p1, p0, LX/2NN;->b:Landroid/content/Context;

    .line 399745
    iput-object p2, p0, LX/2NN;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 399746
    iput-object p3, p0, LX/2NN;->d:LX/03V;

    .line 399747
    iput-object p4, p0, LX/2NN;->e:Ljava/lang/Boolean;

    .line 399748
    return-void
.end method

.method public static a(LX/0QB;)LX/2NN;
    .locals 5

    .prologue
    .line 399749
    new-instance v4, LX/2NN;

    const-class v0, Landroid/content/Context;

    const-class v1, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    .line 399750
    invoke-static {}, LX/0eG;->j()Ljava/lang/Boolean;

    move-result-object v3

    move-object v3, v3

    .line 399751
    check-cast v3, Ljava/lang/Boolean;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2NN;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;Ljava/lang/Boolean;)V

    .line 399752
    move-object v0, v4

    .line 399753
    return-object v0
.end method
