.class public final LX/3OY;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/3MZ;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Lb;


# direct methods
.method public constructor <init>(LX/3Lb;)V
    .locals 0

    .prologue
    .line 560580
    iput-object p1, p0, LX/3OY;->a:LX/3Lb;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 560581
    sget-object v0, LX/3Lb;->f:Ljava/lang/Class;

    const-string v1, "ContactsLoader.onNonCancellationFailure"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 560582
    iget-object v0, p0, LX/3OY;->a:LX/3Lb;

    iget-object v0, v0, LX/3Lb;->p:LX/03V;

    const-string v1, "ContactsLoader"

    const-string v2, "onNonCancellationFailure"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 560583
    iget-object v0, p0, LX/3OY;->a:LX/3Lb;

    iput-object v3, v0, LX/3Lb;->e:LX/1Mv;

    .line 560584
    iget-object v0, p0, LX/3OY;->a:LX/3Lb;

    iget-object v0, v0, LX/3Lb;->B:LX/3Mb;

    if-eqz v0, :cond_0

    .line 560585
    iget-object v0, p0, LX/3OY;->a:LX/3Lb;

    iget-object v0, v0, LX/3Lb;->B:LX/3Mb;

    invoke-interface {v0, v3, p1}, LX/3Mb;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 560586
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 560587
    check-cast p1, LX/3MZ;

    const/4 v2, 0x0

    .line 560588
    iget-object v0, p0, LX/3OY;->a:LX/3Lb;

    .line 560589
    iput-object p1, v0, LX/3Lb;->A:LX/3MZ;

    .line 560590
    iget-object v0, p0, LX/3OY;->a:LX/3Lb;

    iput-object v2, v0, LX/3Lb;->e:LX/1Mv;

    .line 560591
    iget-object v0, p0, LX/3OY;->a:LX/3Lb;

    iget-object v0, v0, LX/3Lb;->B:LX/3Mb;

    if-eqz v0, :cond_0

    .line 560592
    iget-object v0, p0, LX/3OY;->a:LX/3Lb;

    iget-object v0, v0, LX/3Lb;->B:LX/3Mb;

    iget-object v1, p0, LX/3OY;->a:LX/3Lb;

    iget-object v1, v1, LX/3Lb;->A:LX/3MZ;

    invoke-interface {v0, v2, v1}, LX/3Mb;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 560593
    iget-object v0, p0, LX/3OY;->a:LX/3Lb;

    iget-object v0, v0, LX/3Lb;->B:LX/3Mb;

    iget-object v1, p0, LX/3OY;->a:LX/3Lb;

    iget-object v1, v1, LX/3Lb;->A:LX/3MZ;

    invoke-interface {v0, v2, v1}, LX/3Mb;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 560594
    :cond_0
    return-void
.end method
