.class public LX/2Pn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final mMonotonicClock:LX/0So;

.field public final mMqttPushServiceClientManager:LX/2Hu;


# direct methods
.method public constructor <init>(LX/2Hu;LX/0So;)V
    .locals 0
    .param p2    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 407216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407217
    iput-object p1, p0, LX/2Pn;->mMqttPushServiceClientManager:LX/2Hu;

    .line 407218
    iput-object p2, p0, LX/2Pn;->mMonotonicClock:LX/0So;

    .line 407219
    return-void
.end method

.method public static createInstance__com_facebook_omnistore_mqtt_MessagePublisher__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pn;
    .locals 3

    .prologue
    .line 407220
    new-instance v2, LX/2Pn;

    invoke-static {p0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v0

    check-cast v0, LX/2Hu;

    invoke-static {p0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-direct {v2, v0, v1}, LX/2Pn;-><init>(LX/2Hu;LX/0So;)V

    .line 407221
    return-object v2
.end method
