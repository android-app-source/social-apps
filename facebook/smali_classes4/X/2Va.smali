.class public LX/2Va;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 417808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/2At;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 417809
    invoke-virtual {p0}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v1

    .line 417810
    invoke-static {p0, v1}, LX/2Va;->b(LX/2At;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 417811
    if-nez v0, :cond_0

    .line 417812
    invoke-static {p0, v1}, LX/2Va;->a(LX/2At;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 417813
    :cond_0
    return-object v0
.end method

.method public static a(LX/2At;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 417814
    const-string v1, "get"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417815
    const-string v1, "getCallbacks"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 417816
    invoke-static {p0}, LX/2Va;->b(LX/2At;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 417817
    :cond_0
    :goto_0
    return-object v0

    .line 417818
    :cond_1
    const-string v1, "getMetaClass"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 417819
    invoke-static {p0}, LX/2Va;->c(LX/2At;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 417820
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Va;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 417821
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 417822
    if-nez v2, :cond_1

    move-object p0, v0

    .line 417823
    :cond_0
    :goto_0
    return-object p0

    .line 417824
    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_3

    .line 417825
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 417826
    invoke-static {v3}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v4

    .line 417827
    if-eq v3, v4, :cond_3

    .line 417828
    if-nez v0, :cond_2

    .line 417829
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 417830
    :cond_2
    invoke-virtual {v0, v1, v4}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 417831
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 417832
    :cond_3
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static b(LX/2At;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 417833
    const-string v1, "is"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417834
    invoke-virtual {p0}, LX/0lO;->d()Ljava/lang/Class;

    move-result-object v1

    .line 417835
    const-class v2, Ljava/lang/Boolean;

    if-eq v1, v2, :cond_1

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-eq v1, v2, :cond_1

    .line 417836
    :cond_0
    :goto_0
    return-object v0

    .line 417837
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Va;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(LX/2At;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 417838
    invoke-virtual {p0}, LX/0lO;->d()Ljava/lang/Class;

    move-result-object v1

    .line 417839
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v2

    if-nez v2, :cond_1

    .line 417840
    :cond_0
    :goto_0
    return v0

    .line 417841
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v1

    .line 417842
    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    .line 417843
    if-eqz v1, :cond_0

    .line 417844
    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    .line 417845
    const-string v2, "net.sf.cglib"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "org.hibernate.repackage.cglib"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417846
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(LX/2At;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 417847
    invoke-virtual {p0}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v0

    .line 417848
    invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417849
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Va;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 417850
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(LX/2At;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 417851
    invoke-virtual {p0}, LX/0lO;->d()Ljava/lang/Class;

    move-result-object v1

    .line 417852
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 417853
    :cond_0
    :goto_0
    return v0

    .line 417854
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    .line 417855
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "groovy.lang"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417856
    const/4 v0, 0x1

    goto :goto_0
.end method
