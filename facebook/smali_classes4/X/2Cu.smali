.class public final LX/2Cu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/2Cv;

.field public final b:J

.field public final c:J

.field public final d:F


# direct methods
.method public constructor <init>(LX/2Cv;JJF)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 383397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383398
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 383399
    cmp-long v0, p2, p4

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 383400
    cmp-long v0, p2, v4

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 383401
    cmp-long v0, p4, v4

    if-lez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 383402
    const/4 v0, 0x0

    cmpl-float v0, p6, v0

    if-ltz v0, :cond_3

    :goto_3
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 383403
    iput-object p1, p0, LX/2Cu;->a:LX/2Cv;

    .line 383404
    iput-wide p2, p0, LX/2Cu;->b:J

    .line 383405
    iput-wide p4, p0, LX/2Cu;->c:J

    .line 383406
    iput p6, p0, LX/2Cu;->d:F

    .line 383407
    return-void

    :cond_0
    move v0, v2

    .line 383408
    goto :goto_0

    :cond_1
    move v0, v2

    .line 383409
    goto :goto_1

    :cond_2
    move v0, v2

    .line 383410
    goto :goto_2

    :cond_3
    move v1, v2

    .line 383411
    goto :goto_3
.end method
