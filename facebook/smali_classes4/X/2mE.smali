.class public LX/2mE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field public final a:LX/2lw;

.field private final b:LX/2l7;


# direct methods
.method public constructor <init>(LX/2lw;LX/2l7;)V
    .locals 0

    .prologue
    .line 459398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459399
    iput-object p1, p0, LX/2mE;->a:LX/2lw;

    .line 459400
    iput-object p2, p0, LX/2mE;->b:LX/2l7;

    .line 459401
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 459402
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 459403
    sget-object v1, LX/2md;->b:LX/0U1;

    .line 459404
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 459405
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 459406
    sget-object v1, LX/2md;->g:LX/0U1;

    .line 459407
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 459408
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 459409
    iget-object v1, p0, LX/2mE;->b:LX/2l7;

    invoke-virtual {v1, v2, v3, v0}, LX/2l7;->a(JI)Z

    move-result v1

    .line 459410
    if-eqz v1, :cond_0

    .line 459411
    new-instance v1, Landroid/content/Intent;

    sget-object v4, LX/0Ow;->c:Ljava/lang/String;

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 459412
    const-string v4, "vnd.android.cursor.item/vnd.facebook.katana.bookmark_unread_count"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 459413
    const-string v4, "bookmark_fbid"

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 459414
    const-string v4, "bookmark_unread_count"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 459415
    iget-object v4, p0, LX/2mE;->a:LX/2lw;

    invoke-virtual {v4, v1}, LX/2lw;->a(Landroid/content/Intent;)V

    .line 459416
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 459417
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Fail to update unread count for bookmark "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method
