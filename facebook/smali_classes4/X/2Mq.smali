.class public LX/2Mq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile o:LX/2Mq;


# instance fields
.field public final a:LX/0Sh;

.field public final b:LX/0Sg;

.field public final c:LX/0Uo;

.field public final d:LX/2Mr;

.field public final e:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final f:LX/0Xp;

.field private final g:LX/0kd;

.field public h:Z

.field public i:I

.field private j:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field

.field private final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/49N;",
            ">;"
        }
    .end annotation
.end field

.field public volatile l:Z

.field public volatile m:Z

.field private volatile n:Z


# direct methods
.method public constructor <init>(LX/0Sh;LX/0Sg;LX/0Uo;LX/2Mr;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Xp;LX/0kd;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/16 v3, 0xa

    .line 397701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397702
    iput v0, p0, LX/2Mq;->i:I

    .line 397703
    iput v0, p0, LX/2Mq;->j:I

    .line 397704
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2Mq;->k:Ljava/util/Map;

    .line 397705
    iput-object p2, p0, LX/2Mq;->b:LX/0Sg;

    .line 397706
    iput-object p3, p0, LX/2Mq;->c:LX/0Uo;

    .line 397707
    iput-object p1, p0, LX/2Mq;->a:LX/0Sh;

    .line 397708
    iput-object p4, p0, LX/2Mq;->d:LX/2Mr;

    .line 397709
    iput-object p5, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 397710
    iput-object p6, p0, LX/2Mq;->f:LX/0Xp;

    .line 397711
    iput-object p7, p0, LX/2Mq;->g:LX/0kd;

    .line 397712
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540011

    const/16 v2, 0x64

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(II)V

    .line 397713
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540024

    const/16 v2, 0x3e8

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(II)V

    .line 397714
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540012

    invoke-interface {v0, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(II)V

    .line 397715
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540001

    invoke-interface {v0, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(II)V

    .line 397716
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540019

    invoke-interface {v0, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(II)V

    .line 397717
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540017

    invoke-interface {v0, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(II)V

    .line 397718
    return-void
.end method

.method public static a(LX/0QB;)LX/2Mq;
    .locals 11

    .prologue
    .line 397688
    sget-object v0, LX/2Mq;->o:LX/2Mq;

    if-nez v0, :cond_1

    .line 397689
    const-class v1, LX/2Mq;

    monitor-enter v1

    .line 397690
    :try_start_0
    sget-object v0, LX/2Mq;->o:LX/2Mq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 397691
    if-eqz v2, :cond_0

    .line 397692
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 397693
    new-instance v3, LX/2Mq;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v5

    check-cast v5, LX/0Sg;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v6

    check-cast v6, LX/0Uo;

    invoke-static {v0}, LX/2Mr;->a(LX/0QB;)LX/2Mr;

    move-result-object v7

    check-cast v7, LX/2Mr;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v8

    check-cast v8, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v9

    check-cast v9, LX/0Xp;

    invoke-static {v0}, LX/0kd;->a(LX/0QB;)LX/0kd;

    move-result-object v10

    check-cast v10, LX/0kd;

    invoke-direct/range {v3 .. v10}, LX/2Mq;-><init>(LX/0Sh;LX/0Sg;LX/0Uo;LX/2Mr;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Xp;LX/0kd;)V

    .line 397694
    move-object v0, v3

    .line 397695
    sput-object v0, LX/2Mq;->o:LX/2Mq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397696
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 397697
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 397698
    :cond_1
    sget-object v0, LX/2Mq;->o:LX/2Mq;

    return-object v0

    .line 397699
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 397700
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2Mq;IILcom/facebook/fbservice/results/DataFetchDisposition;)V
    .locals 3

    .prologue
    .line 397676
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "data_fetch_disposition_has_data"

    iget-boolean v2, p3, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 397677
    iget-object v0, p3, Lcom/facebook/fbservice/results/DataFetchDisposition;->isStaleData:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397678
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "data_fetch_disposition_stale_data"

    iget-object v2, p3, Lcom/facebook/fbservice/results/DataFetchDisposition;->isStaleData:LX/03R;

    invoke-virtual {v2}, LX/03R;->asBoolean()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 397679
    :cond_0
    iget-object v0, p3, Lcom/facebook/fbservice/results/DataFetchDisposition;->dataSource:LX/4B1;

    if-eqz v0, :cond_1

    .line 397680
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "data_fetch_disposition_data_source"

    iget-object v2, p3, Lcom/facebook/fbservice/results/DataFetchDisposition;->dataSource:LX/4B1;

    invoke-virtual {v2}, LX/4B1;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 397681
    :cond_1
    iget-object v0, p3, Lcom/facebook/fbservice/results/DataFetchDisposition;->wasFetchSynchronous:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 397682
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "data_fetch_disposition_synchronous_fetch"

    iget-object v2, p3, Lcom/facebook/fbservice/results/DataFetchDisposition;->wasFetchSynchronous:LX/03R;

    invoke-virtual {v2}, LX/03R;->asBoolean()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 397683
    :cond_2
    iget-object v0, p3, Lcom/facebook/fbservice/results/DataFetchDisposition;->isIncompleteData:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 397684
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "data_fetch_disposition_incomplete_data"

    iget-object v2, p3, Lcom/facebook/fbservice/results/DataFetchDisposition;->isIncompleteData:LX/03R;

    invoke-virtual {v2}, LX/03R;->asBoolean()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 397685
    :cond_3
    iget-object v0, p3, Lcom/facebook/fbservice/results/DataFetchDisposition;->fellbackToCachedDataAfterFailedToHitServer:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 397686
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "data_fetch_disposition_server_error_fallback"

    iget-object v2, p3, Lcom/facebook/fbservice/results/DataFetchDisposition;->fellbackToCachedDataAfterFailedToHitServer:LX/03R;

    invoke-virtual {v2}, LX/03R;->asBoolean()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 397687
    :cond_4
    return-void
.end method

.method public static b(LX/2Mq;ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const v2, 0x540004

    .line 397666
    if-eq p1, v2, :cond_0

    const v0, 0x540003

    if-ne p1, v0, :cond_2

    .line 397667
    :cond_0
    if-ne p1, v2, :cond_1

    iget-boolean v0, p0, LX/2Mq;->n:Z

    if-eqz v0, :cond_1

    .line 397668
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "mainactivity_closed"

    invoke-interface {v0, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 397669
    :cond_1
    iput-boolean v3, p0, LX/2Mq;->n:Z

    .line 397670
    :cond_2
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 397671
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    invoke-interface {v0, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 397672
    if-ne p1, v2, :cond_3

    .line 397673
    iput-boolean v3, p0, LX/2Mq;->l:Z

    .line 397674
    :cond_3
    iget-object v0, p0, LX/2Mq;->g:LX/0kd;

    invoke-virtual {v0}, LX/0kd;->c()V

    .line 397675
    return-void
.end method

.method public static c(LX/2Mq;Ljava/lang/String;)V
    .locals 3

    .prologue
    const v2, 0x540004

    const v1, 0x540002

    .line 397661
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397662
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v1, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 397663
    :cond_0
    :goto_0
    return-void

    .line 397664
    :cond_1
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397665
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    const v1, 0x540011

    .line 397644
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v1, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 397645
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v1, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 397646
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p2, 0x540004

    invoke-interface {v0, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397647
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string p2, "luke_warm_start"

    invoke-interface {v0, v1, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 397648
    :cond_0
    :goto_0
    return-void

    .line 397649
    :cond_1
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p2, 0x540002

    invoke-interface {v0, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 397650
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string p2, "warm_start"

    invoke-interface {v0, v1, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    goto :goto_0

    .line 397651
    :cond_2
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p2, 0x540003

    invoke-interface {v0, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397652
    iget-object v0, p0, LX/2Mq;->c:LX/0Uo;

    .line 397653
    iget-boolean p2, v0, LX/0Uo;->U:Z

    move v0, p2

    .line 397654
    if-nez v0, :cond_3

    iget-object v0, p0, LX/2Mq;->c:LX/0Uo;

    .line 397655
    iget-boolean p2, v0, LX/0Uo;->V:Z

    move v0, p2

    .line 397656
    if-eqz v0, :cond_5

    :cond_3
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 397657
    if-eqz v0, :cond_4

    .line 397658
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string p2, "first_cold_start"

    invoke-interface {v0, v1, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 397659
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2Mq;->m:Z

    goto :goto_0

    .line 397660
    :cond_4
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string p2, "cold_start"

    invoke-interface {v0, v1, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 397629
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540001

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->g(II)V

    .line 397630
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540034

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->g(II)V

    .line 397631
    goto :goto_0

    .line 397632
    :goto_0
    iget-boolean v1, p0, LX/2Mq;->h:Z

    if-eqz v1, :cond_0

    .line 397633
    :goto_1
    const-string v0, "send_message: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 397634
    sget-object v3, LX/0PW;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0PW;

    .line 397635
    const/4 v3, 0x1

    invoke-virtual {v5, v0, v1, v3}, LX/0PW;->a(Ljava/lang/String;[Ljava/lang/Object;Z)I

    move-result v6

    .line 397636
    iget-object v3, v5, LX/0PW;->e:Landroid/util/SparseArray;

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0eu;

    move-object v3, v3

    .line 397637
    invoke-virtual {v3}, LX/0eu;->d()Ljava/lang/String;

    move-result-object v9

    .line 397638
    new-instance v4, LX/49N;

    invoke-static {}, LX/0PZ;->a()J

    move-result-wide v7

    invoke-direct/range {v4 .. v9}, LX/49N;-><init>(LX/0PW;IJLjava/lang/String;)V

    .line 397639
    const-wide/16 v5, 0x20

    iget v3, v4, LX/49N;->b:I

    invoke-static {v5, v6, v9, v3}, LX/018;->b(JLjava/lang/String;I)V

    .line 397640
    move-object v0, v4

    .line 397641
    iget-object v1, p0, LX/2Mq;->k:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397642
    return-void

    .line 397643
    :cond_0
    iget-object v1, p0, LX/2Mq;->a:LX/0Sh;

    new-instance v2, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    invoke-direct {v2, p0}, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;-><init>(LX/2Mq;)V

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 397605
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540001

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 397606
    return-void
.end method

.method public final a(Ljava/lang/String;S)V
    .locals 3

    .prologue
    .line 397627
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540001

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 397628
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 397624
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540012

    const/16 v2, 0x17

    invoke-interface {v0, v1, p1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 397625
    const-string v0, "db_threadlist"

    invoke-static {p0, v0}, LX/2Mq;->c(LX/2Mq;Ljava/lang/String;)V

    .line 397626
    return-void
.end method

.method public final b(ILcom/facebook/fbservice/results/DataFetchDisposition;)V
    .locals 3
    .param p2    # Lcom/facebook/fbservice/results/DataFetchDisposition;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v2, 0x540011

    .line 397615
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/facebook/fbservice/results/DataFetchDisposition;->dataSource:LX/4B1;

    if-nez v0, :cond_1

    .line 397616
    :cond_0
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v1, 0x21

    invoke-interface {v0, v2, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 397617
    :goto_0
    return-void

    .line 397618
    :cond_1
    invoke-static {p0, v2, p1, p2}, LX/2Mq;->a(LX/2Mq;IILcom/facebook/fbservice/results/DataFetchDisposition;)V

    .line 397619
    sget-object v0, LX/FCT;->a:[I

    iget-object v1, p2, Lcom/facebook/fbservice/results/DataFetchDisposition;->dataSource:LX/4B1;

    invoke-virtual {v1}, LX/4B1;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 397620
    :pswitch_0
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v1, 0x19

    invoke-interface {v0, v2, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_0

    .line 397621
    :pswitch_1
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v1, 0x1a

    invoke-interface {v0, v2, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_0

    .line 397622
    :pswitch_2
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v1, 0x1c

    invoke-interface {v0, v2, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_0

    .line 397623
    :pswitch_3
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v1, 0x1b

    invoke-interface {v0, v2, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final f(I)V
    .locals 3

    .prologue
    .line 397613
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540011

    const/16 v2, 0x22

    invoke-interface {v0, v1, p1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 397614
    return-void
.end method

.method public final i(I)V
    .locals 3

    .prologue
    .line 397611
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540011

    const/16 v2, 0x1d

    invoke-interface {v0, v1, p1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 397612
    return-void
.end method

.method public final k(I)V
    .locals 3

    .prologue
    .line 397609
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540011

    const/4 v2, 0x2

    invoke-interface {v0, v1, p1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 397610
    return-void
.end method

.method public final l(I)V
    .locals 3

    .prologue
    .line 397607
    iget-object v0, p0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x540011

    const/4 v2, 0x3

    invoke-interface {v0, v1, p1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 397608
    return-void
.end method
