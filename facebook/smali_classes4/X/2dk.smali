.class public LX/2dk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/2dl;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0rq;

.field public f:I

.field public g:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;LX/2dl;LX/0Ot;LX/0rq;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/2dl;",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;",
            "LX/0rq;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 444241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444242
    iput v0, p0, LX/2dk;->f:I

    .line 444243
    iput v0, p0, LX/2dk;->g:I

    .line 444244
    const v0, 0x7f0b147f

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/2dk;->a:I

    .line 444245
    iput-object p2, p0, LX/2dk;->b:Ljava/util/concurrent/ExecutorService;

    .line 444246
    iput-object p3, p0, LX/2dk;->c:LX/2dl;

    .line 444247
    iput-object p4, p0, LX/2dk;->d:LX/0Ot;

    .line 444248
    iput-object p5, p0, LX/2dk;->e:LX/0rq;

    .line 444249
    return-void
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/friends/model/FriendRequest;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 444224
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v13

    .line 444225
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v14

    move v12, v11

    :goto_0
    if-ge v12, v14, :cond_4

    invoke-virtual {p0, v12}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityModel;

    .line 444226
    invoke-virtual {v9}, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityModel;->j()Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityUserModel;

    move-result-object v7

    .line 444227
    invoke-virtual {v9}, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityModel;->k()LX/0Px;

    move-result-object v0

    .line 444228
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 444229
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityModel$SuggestersModel;

    .line 444230
    invoke-virtual {v1}, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityModel$SuggestersModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 444231
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 444232
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v6, v1

    .line 444233
    invoke-virtual {v7}, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityUserModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_1

    move-object v3, v10

    .line 444234
    :goto_2
    invoke-virtual {v7}, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityUserModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_2

    move-object v5, v10

    .line 444235
    :goto_3
    new-instance v0, Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v7}, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityUserModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityUserModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityUserModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    invoke-virtual {v7}, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityUserModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v8

    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    const/4 v7, 0x1

    :goto_4
    invoke-static {v8, v7}, LX/2dk;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)LX/2lu;

    move-result-object v7

    invoke-virtual {v9}, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityModel;->a()Z

    move-result v8

    invoke-virtual {v9}, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityModel;->l()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lcom/facebook/friends/model/FriendRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Ljava/lang/String;LX/0Px;LX/2lu;ZLjava/lang/String;)V

    invoke-virtual {v13, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 444236
    add-int/lit8 v0, v12, 0x1

    move v12, v0

    goto :goto_0

    .line 444237
    :cond_1
    invoke-virtual {v7}, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityUserModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 444238
    :cond_2
    invoke-virtual {v7}, Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendingPossibilityUserModel;->n()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_3
    move v7, v11

    .line 444239
    goto :goto_4

    .line 444240
    :cond_4
    invoke-virtual {v13}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Px;Ljava/lang/String;)LX/0Px;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PersonYouMayKnowEdgeModel;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444214
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v12

    .line 444215
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v13

    const/4 v0, 0x0

    move v11, v0

    :goto_0
    if-ge v11, v13, :cond_2

    invoke-virtual {p0, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PersonYouMayKnowEdgeModel;

    .line 444216
    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PersonYouMayKnowEdgeModel;->a()Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PersonYouMayKnowFieldsModel;

    move-result-object v8

    .line 444217
    invoke-virtual {v8}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PersonYouMayKnowFieldsModel;->o()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_0

    invoke-virtual {v8}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PersonYouMayKnowFieldsModel;->o()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 444218
    :goto_1
    invoke-virtual {v8}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PersonYouMayKnowFieldsModel;->m()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_1

    invoke-virtual {v8}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PersonYouMayKnowFieldsModel;->m()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/15i;->j(II)I

    move-result v6

    .line 444219
    :goto_2
    new-instance v1, Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v8}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PersonYouMayKnowFieldsModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v8}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PersonYouMayKnowFieldsModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PersonYouMayKnowFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v7

    invoke-virtual {v8}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PersonYouMayKnowFieldsModel;->l()Z

    move-result v8

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PersonYouMayKnowEdgeModel;->j()Ljava/lang/String;

    move-result-object v9

    move-object v10, p1

    invoke-direct/range {v1 .. v10}, Lcom/facebook/friends/model/PersonYouMayKnow;-><init>(JLjava/lang/String;Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLFriendshipStatus;ZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v12, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 444220
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_0

    .line 444221
    :cond_0
    const-string v5, ""

    goto :goto_1

    .line 444222
    :cond_1
    const/4 v6, 0x0

    goto :goto_2

    .line 444223
    :cond_2
    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2uF;)LX/0Px;
    .locals 10
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "mapPeopleYouMayInvite"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2uF;",
            ")",
            "LX/0Px",
            "<",
            "LX/2no;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444207
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 444208
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v9, v0, LX/1vs;->b:I

    .line 444209
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;

    invoke-virtual {v6, v9, v0, v1}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;

    .line 444210
    invoke-static {v0}, LX/2dk;->a(Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;)Ljava/lang/String;

    move-result-object v1

    .line 444211
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 444212
    new-instance v1, LX/2no;

    const/4 v2, 0x0

    invoke-virtual {v6, v9, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, LX/2dk;->a(Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x2

    invoke-virtual {v6, v9, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v0, 0x3

    invoke-virtual {v6, v9, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, LX/2no;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 444213
    :cond_1
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)LX/2lu;
    .locals 1

    .prologue
    .line 444198
    if-eqz p1, :cond_0

    .line 444199
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p0, v0, :cond_2

    .line 444200
    sget-object v0, LX/2lu;->ACCEPTED:LX/2lu;

    .line 444201
    :goto_0
    return-object v0

    .line 444202
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p0, v0, :cond_1

    .line 444203
    sget-object v0, LX/2lu;->ACCEPTED:LX/2lu;

    goto :goto_0

    .line 444204
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p0, v0, :cond_2

    .line 444205
    sget-object v0, LX/2lu;->REJECTED:LX/2lu;

    goto :goto_0

    .line 444206
    :cond_2
    sget-object v0, LX/2lu;->NEEDS_RESPONSE:LX/2lu;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 2

    .prologue
    .line 444185
    new-instance v0, LX/17L;

    invoke-direct {v0}, LX/17L;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->c()Z

    move-result v1

    .line 444186
    iput-boolean v1, v0, LX/17L;->e:Z

    .line 444187
    move-object v0, v0

    .line 444188
    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v1

    .line 444189
    iput-boolean v1, v0, LX/17L;->d:Z

    .line 444190
    move-object v0, v0

    .line 444191
    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->p_()Ljava/lang/String;

    move-result-object v1

    .line 444192
    iput-object v1, v0, LX/17L;->f:Ljava/lang/String;

    .line 444193
    move-object v0, v0

    .line 444194
    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 444195
    iput-object v1, v0, LX/17L;->c:Ljava/lang/String;

    .line 444196
    move-object v0, v0

    .line 444197
    invoke-virtual {v0}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 444180
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-nez v1, :cond_1

    .line 444181
    :cond_0
    :goto_0
    return-object v0

    .line 444182
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 444183
    :sswitch_0
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 444184
    :sswitch_1
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3609cb28 -> :sswitch_0
        0x1c4e6237 -> :sswitch_1
    .end sparse-switch
.end method

.method public static b(LX/2dk;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 5
    .param p0    # LX/2dk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendRequestQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 444132
    if-lez p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 444133
    new-instance v0, LX/2in;

    invoke-direct {v0}, LX/2in;-><init>()V

    move-object v0, v0

    .line 444134
    const-string v2, "after_param"

    invoke-virtual {v0, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "first_param"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "media_type"

    iget-object v4, p0, LX/2dk;->e:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->b()LX/0wF;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v2

    const-string v3, "picture_size"

    iget v4, p0, LX/2dk;->a:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 444135
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-string v2, "REQUESTS_TAB_REQUESTS_QUERY_TAG"

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    .line 444136
    iput-object v2, v0, LX/0zO;->d:Ljava/util/Set;

    .line 444137
    move-object v0, v0

    .line 444138
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 444139
    move-object v0, v0

    .line 444140
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v2}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 444141
    iput-object p3, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 444142
    move-object v0, v0

    .line 444143
    const-wide/16 v2, 0xe10

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 444144
    iget-object v2, v0, LX/0zO;->m:LX/0gW;

    move-object v2, v2

    .line 444145
    iput-boolean v1, v2, LX/0gW;->l:Z

    .line 444146
    return-object v0

    .line 444147
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2dk;
    .locals 6

    .prologue
    .line 444250
    new-instance v0, LX/2dk;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/2dl;->a(LX/0QB;)LX/2dl;

    move-result-object v3

    check-cast v3, LX/2dl;

    const/16 v4, 0xafc

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v5

    check-cast v5, LX/0rq;

    invoke-direct/range {v0 .. v5}, LX/2dk;-><init>(Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;LX/2dl;LX/0Ot;LX/0rq;)V

    .line 444251
    return-object v0
.end method

.method public static c(LX/2dk;)LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friends/protocol/FetchFriendRequestsGraphQLModels$FriendRequestQueryModel;",
            ">;",
            "LX/2lp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444179
    new-instance v0, LX/2iq;

    invoke-direct {v0, p0}, LX/2iq;-><init>(LX/2dk;)V

    return-object v0
.end method

.method private static e(LX/2dk;)LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel;",
            ">;",
            "LX/3Fw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444178
    new-instance v0, LX/3Bu;

    invoke-direct {v0, p0}, LX/3Bu;-><init>(LX/2dk;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2lp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444175
    invoke-static {p0, p1, p2, p3}, LX/2dk;->b(LX/2dk;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v0

    .line 444176
    iget-object v1, p0, LX/2dk;->c:LX/2dl;

    const-string v2, "REQUESTS_TAB_REQUESTS_QUERY_TAG"

    invoke-virtual {v1, v0, v2}, LX/2dl;->a(LX/0zO;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 444177
    invoke-static {p0}, LX/2dk;->c(LX/2dk;)LX/0QK;

    move-result-object v1

    iget-object v2, p0, LX/2dk;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;ILjava/lang/Integer;LX/2hC;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Integer;",
            "LX/2hC;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/3Fw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444172
    invoke-virtual/range {p0 .. p5}, LX/2dk;->b(Ljava/lang/String;ILjava/lang/Integer;LX/2hC;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v0

    .line 444173
    iget-object v1, p0, LX/2dk;->c:LX/2dl;

    const-string v2, "REQUESTS_TAB_PYMK_QUERY_TAG"

    invoke-virtual {v1, v0, v2}, LX/2dl;->a(LX/0zO;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 444174
    invoke-static {p0}, LX/2dk;->e(LX/2dk;)LX/0QK;

    move-result-object v1

    iget-object v2, p0, LX/2dk;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;ILjava/lang/Integer;LX/2hC;Lcom/facebook/common/callercontext/CallerContext;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Integer;",
            "LX/2hC;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0TF",
            "<",
            "LX/3Fw;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/3Fw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444165
    invoke-virtual/range {p0 .. p5}, LX/2dk;->b(Ljava/lang/String;ILjava/lang/Integer;LX/2hC;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v1

    .line 444166
    invoke-static {p0}, LX/2dk;->e(LX/2dk;)LX/0QK;

    move-result-object v4

    .line 444167
    iget-object v0, p0, LX/2dk;->c:LX/2dl;

    iget-object v2, p0, LX/2dk;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1My;

    new-instance v3, LX/844;

    invoke-direct {v3, p0, p6, v4}, LX/844;-><init>(LX/2dk;LX/0TF;LX/0QK;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 444168
    iget-object v5, v1, LX/0zO;->m:LX/0gW;

    move-object v5, v5

    .line 444169
    iget-object v6, v5, LX/0gW;->f:Ljava/lang/String;

    move-object v5, v6

    .line 444170
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, LX/2dk;->g:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/2dk;->g:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "REQUESTS_TAB_PYMK_QUERY_TAG"

    invoke-virtual/range {v0 .. v5}, LX/2dl;->a(LX/0zO;LX/1My;LX/0TF;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 444171
    invoke-static {p0}, LX/2dk;->e(LX/2dk;)LX/0QK;

    move-result-object v1

    iget-object v2, p0, LX/2dk;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;ILjava/lang/Integer;LX/2hC;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Integer;",
            "LX/2hC;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 444149
    if-lez p2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 444150
    if-nez p3, :cond_0

    .line 444151
    iget v0, p0, LX/2dk;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    .line 444152
    :cond_0
    new-instance v0, LX/3Bt;

    invoke-direct {v0}, LX/3Bt;-><init>()V

    move-object v0, v0

    .line 444153
    const-string v2, "after_param"

    invoke-virtual {v0, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "first_param"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "location"

    iget-object v4, p4, LX/2hC;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "max"

    const-string v4, "250"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "media_type"

    iget-object v4, p0, LX/2dk;->e:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->b()LX/0wF;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v2

    const-string v3, "picture_size"

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    .line 444154
    iput-boolean v1, v2, LX/0gW;->l:Z

    .line 444155
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-string v2, "REQUESTS_TAB_PYMK_QUERY_TAG"

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    .line 444156
    iput-object v2, v0, LX/0zO;->d:Ljava/util/Set;

    .line 444157
    move-object v0, v0

    .line 444158
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 444159
    move-object v0, v0

    .line 444160
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 444161
    iput-object p5, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 444162
    move-object v0, v0

    .line 444163
    const-wide/16 v2, 0xe10

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    return-object v0

    .line 444164
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;ILX/2hC;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "LX/2hC;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/3Fw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444148
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/2dk;->a(Ljava/lang/String;ILjava/lang/Integer;LX/2hC;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
