.class public final LX/3C5;
.super LX/0Tz;
.source ""


# static fields
.field public static a:Ljava/lang/String;

.field public static b:LX/0U1;

.field public static c:LX/0U1;

.field public static d:LX/0U1;

.field public static e:LX/0U1;

.field public static f:LX/0U1;

.field private static final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 529645
    const-string v0, "user_activity_snapshots"

    sput-object v0, LX/3C5;->a:Ljava/lang/String;

    .line 529646
    new-instance v0, LX/0U1;

    const-string v1, "snapshot"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3C5;->b:LX/0U1;

    .line 529647
    new-instance v0, LX/0U1;

    const-string v1, "accelerometer_stddev"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3C5;->c:LX/0U1;

    .line 529648
    new-instance v0, LX/0U1;

    const-string v1, "accelerometer_max"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3C5;->d:LX/0U1;

    .line 529649
    new-instance v0, LX/0U1;

    const-string v1, "accelerometer_min"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3C5;->e:LX/0U1;

    .line 529650
    new-instance v0, LX/0U1;

    const-string v1, "timestamp"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3C5;->f:LX/0U1;

    .line 529651
    sget-object v0, LX/3C5;->b:LX/0U1;

    sget-object v1, LX/3C5;->c:LX/0U1;

    sget-object v2, LX/3C5;->d:LX/0U1;

    sget-object v3, LX/3C5;->e:LX/0U1;

    sget-object v4, LX/3C5;->f:LX/0U1;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3C5;->g:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 529652
    sget-object v0, LX/3C5;->a:Ljava/lang/String;

    sget-object v1, LX/3C5;->g:LX/0Px;

    invoke-direct {p0, v0, v1}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 529653
    return-void
.end method
