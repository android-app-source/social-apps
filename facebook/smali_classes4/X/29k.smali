.class public final enum LX/29k;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/29k;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/29k;

.field public static final enum APPMANAGER_NOT_INSTALLED:LX/29k;

.field public static final enum DEFAULT_COMPONENT_STATE:LX/29k;

.field public static final enum EXPLICIT_COMPONENT_STATE:LX/29k;

.field public static final enum EXPLICIT_DECISION:LX/29k;

.field public static final enum FALLBACK_V13_EU_CANADA:LX/29k;

.field public static final enum FALLBACK_V13_NO_SIM:LX/29k;

.field public static final enum FALLBACK_V13_OUTSIDE_EU_CANADA:LX/29k;

.field public static final enum OLD_SIGNATURE:LX/29k;

.field public static final enum PACKAGE_MANAGER_ERROR:LX/29k;

.field public static final enum UNEXPECTED_SIGNATURES_STATE:LX/29k;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 376519
    new-instance v0, LX/29k;

    const-string v1, "PACKAGE_MANAGER_ERROR"

    invoke-direct {v0, v1, v3}, LX/29k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29k;->PACKAGE_MANAGER_ERROR:LX/29k;

    .line 376520
    new-instance v0, LX/29k;

    const-string v1, "EXPLICIT_COMPONENT_STATE"

    invoke-direct {v0, v1, v4}, LX/29k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29k;->EXPLICIT_COMPONENT_STATE:LX/29k;

    .line 376521
    new-instance v0, LX/29k;

    const-string v1, "DEFAULT_COMPONENT_STATE"

    invoke-direct {v0, v1, v5}, LX/29k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29k;->DEFAULT_COMPONENT_STATE:LX/29k;

    .line 376522
    new-instance v0, LX/29k;

    const-string v1, "UNEXPECTED_SIGNATURES_STATE"

    invoke-direct {v0, v1, v6}, LX/29k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29k;->UNEXPECTED_SIGNATURES_STATE:LX/29k;

    .line 376523
    new-instance v0, LX/29k;

    const-string v1, "APPMANAGER_NOT_INSTALLED"

    invoke-direct {v0, v1, v7}, LX/29k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29k;->APPMANAGER_NOT_INSTALLED:LX/29k;

    .line 376524
    new-instance v0, LX/29k;

    const-string v1, "OLD_SIGNATURE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/29k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29k;->OLD_SIGNATURE:LX/29k;

    .line 376525
    new-instance v0, LX/29k;

    const-string v1, "FALLBACK_V13_NO_SIM"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/29k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29k;->FALLBACK_V13_NO_SIM:LX/29k;

    .line 376526
    new-instance v0, LX/29k;

    const-string v1, "FALLBACK_V13_EU_CANADA"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/29k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29k;->FALLBACK_V13_EU_CANADA:LX/29k;

    .line 376527
    new-instance v0, LX/29k;

    const-string v1, "FALLBACK_V13_OUTSIDE_EU_CANADA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/29k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29k;->FALLBACK_V13_OUTSIDE_EU_CANADA:LX/29k;

    .line 376528
    new-instance v0, LX/29k;

    const-string v1, "EXPLICIT_DECISION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/29k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29k;->EXPLICIT_DECISION:LX/29k;

    .line 376529
    const/16 v0, 0xa

    new-array v0, v0, [LX/29k;

    sget-object v1, LX/29k;->PACKAGE_MANAGER_ERROR:LX/29k;

    aput-object v1, v0, v3

    sget-object v1, LX/29k;->EXPLICIT_COMPONENT_STATE:LX/29k;

    aput-object v1, v0, v4

    sget-object v1, LX/29k;->DEFAULT_COMPONENT_STATE:LX/29k;

    aput-object v1, v0, v5

    sget-object v1, LX/29k;->UNEXPECTED_SIGNATURES_STATE:LX/29k;

    aput-object v1, v0, v6

    sget-object v1, LX/29k;->APPMANAGER_NOT_INSTALLED:LX/29k;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/29k;->OLD_SIGNATURE:LX/29k;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/29k;->FALLBACK_V13_NO_SIM:LX/29k;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/29k;->FALLBACK_V13_EU_CANADA:LX/29k;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/29k;->FALLBACK_V13_OUTSIDE_EU_CANADA:LX/29k;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/29k;->EXPLICIT_DECISION:LX/29k;

    aput-object v2, v0, v1

    sput-object v0, LX/29k;->$VALUES:[LX/29k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 376518
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/29k;
    .locals 1

    .prologue
    .line 376530
    const-class v0, LX/29k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/29k;

    return-object v0
.end method

.method public static values()[LX/29k;
    .locals 1

    .prologue
    .line 376517
    sget-object v0, LX/29k;->$VALUES:[LX/29k;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/29k;

    return-object v0
.end method
