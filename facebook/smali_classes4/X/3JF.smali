.class public LX/3JF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 547512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/2pa;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 547513
    invoke-static {p0}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 547514
    if-nez v1, :cond_1

    .line 547515
    :cond_0
    :goto_0
    return v0

    .line 547516
    :cond_1
    iget-object v2, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->an()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 547517
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aj()LX/0Px;

    move-result-object v1

    .line 547518
    iget-object v2, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-nez v2, :cond_3

    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 547519
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
