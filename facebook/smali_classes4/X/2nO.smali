.class public final enum LX/2nO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2nO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2nO;

.field public static final enum COMPOSE:LX/2nO;

.field public static final enum DISCLOSURE:LX/2nO;

.field public static final enum SHARE:LX/2nO;

.field public static final enum UNKNOWN:LX/2nO;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 464116
    new-instance v0, LX/2nO;

    const-string v1, "COMPOSE"

    invoke-direct {v0, v1, v2}, LX/2nO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2nO;->COMPOSE:LX/2nO;

    .line 464117
    new-instance v0, LX/2nO;

    const-string v1, "DISCLOSURE"

    invoke-direct {v0, v1, v3}, LX/2nO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2nO;->DISCLOSURE:LX/2nO;

    .line 464118
    new-instance v0, LX/2nO;

    const-string v1, "SHARE"

    invoke-direct {v0, v1, v4}, LX/2nO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2nO;->SHARE:LX/2nO;

    .line 464119
    new-instance v0, LX/2nO;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5}, LX/2nO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2nO;->UNKNOWN:LX/2nO;

    .line 464120
    const/4 v0, 0x4

    new-array v0, v0, [LX/2nO;

    sget-object v1, LX/2nO;->COMPOSE:LX/2nO;

    aput-object v1, v0, v2

    sget-object v1, LX/2nO;->DISCLOSURE:LX/2nO;

    aput-object v1, v0, v3

    sget-object v1, LX/2nO;->SHARE:LX/2nO;

    aput-object v1, v0, v4

    sget-object v1, LX/2nO;->UNKNOWN:LX/2nO;

    aput-object v1, v0, v5

    sput-object v0, LX/2nO;->$VALUES:[LX/2nO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 464115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/2nO;
    .locals 1

    .prologue
    .line 464110
    if-nez p0, :cond_0

    .line 464111
    :try_start_0
    sget-object v0, LX/2nO;->UNKNOWN:LX/2nO;

    .line 464112
    :goto_0
    return-object v0

    .line 464113
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2nO;->valueOf(Ljava/lang/String;)LX/2nO;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 464114
    :catch_0
    sget-object v0, LX/2nO;->UNKNOWN:LX/2nO;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/2nO;
    .locals 1

    .prologue
    .line 464108
    const-class v0, LX/2nO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2nO;

    return-object v0
.end method

.method public static values()[LX/2nO;
    .locals 1

    .prologue
    .line 464109
    sget-object v0, LX/2nO;->$VALUES:[LX/2nO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2nO;

    return-object v0
.end method
