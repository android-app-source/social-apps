.class public LX/33b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/33b;


# instance fields
.field private final a:D

.field private final b:D

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 493881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 493882
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, LX/33b;->a:D

    .line 493883
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, LX/33b;->b:D

    .line 493884
    iput-object p1, p0, LX/33b;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 493885
    return-void
.end method

.method public static a(LX/0QB;)LX/33b;
    .locals 4

    .prologue
    .line 493886
    sget-object v0, LX/33b;->d:LX/33b;

    if-nez v0, :cond_1

    .line 493887
    const-class v1, LX/33b;

    monitor-enter v1

    .line 493888
    :try_start_0
    sget-object v0, LX/33b;->d:LX/33b;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 493889
    if-eqz v2, :cond_0

    .line 493890
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 493891
    new-instance p0, LX/33b;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/33b;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 493892
    move-object v0, p0

    .line 493893
    sput-object v0, LX/33b;->d:LX/33b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 493894
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 493895
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 493896
    :cond_1
    sget-object v0, LX/33b;->d:LX/33b;

    return-object v0

    .line 493897
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 493898
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/Display;I)I
    .locals 10

    .prologue
    .line 493899
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const/4 v3, 0x4

    .line 493900
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v0, v4

    if-nez v2, :cond_0

    sget-object v2, LX/0hM;->g:LX/0Tn;

    .line 493901
    :goto_0
    iget-object v4, p0, LX/33b;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 493902
    iget-object v4, p0, LX/33b;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    .line 493903
    :goto_1
    move v0, v2

    .line 493904
    return v0

    .line 493905
    :cond_0
    sget-object v2, LX/0hM;->h:LX/0Tn;

    goto :goto_0

    .line 493906
    :cond_1
    int-to-double v4, p2

    .line 493907
    new-instance v7, Landroid/util/DisplayMetrics;

    invoke-direct {v7}, Landroid/util/DisplayMetrics;-><init>()V

    .line 493908
    invoke-virtual {p1, v7}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 493909
    iget v8, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v8, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    int-to-double v7, v7

    .line 493910
    div-double/2addr v7, v4

    .line 493911
    invoke-static {v7, v8}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v7

    double-to-int v7, v7

    move v4, v7

    .line 493912
    const/16 v5, 0x1e

    if-gt v4, v5, :cond_3

    const/4 v5, 0x4

    if-lt v4, v5, :cond_3

    const/4 v5, 0x1

    :goto_2
    move v5, v5

    .line 493913
    if-eqz v5, :cond_2

    .line 493914
    int-to-double v4, v4

    mul-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 493915
    iget-object v4, p0, LX/33b;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    invoke-interface {v4, v2, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    :cond_2
    move v2, v3

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    goto :goto_2
.end method
