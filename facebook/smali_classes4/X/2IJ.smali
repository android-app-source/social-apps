.class public LX/2IJ;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/compactdisk/LazyDispatcher;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/compactdisk/LazyDispatcher;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 391592
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/compactdisk/LazyDispatcher;
    .locals 3

    .prologue
    .line 391593
    sget-object v0, LX/2IJ;->a:Lcom/facebook/compactdisk/LazyDispatcher;

    if-nez v0, :cond_1

    .line 391594
    const-class v1, LX/2IJ;

    monitor-enter v1

    .line 391595
    :try_start_0
    sget-object v0, LX/2IJ;->a:Lcom/facebook/compactdisk/LazyDispatcher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391596
    if-eqz v2, :cond_0

    .line 391597
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 391598
    invoke-static {v0}, LX/30E;->a(LX/0QB;)Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    move-result-object p0

    check-cast p0, Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    invoke-static {p0}, LX/2FB;->a(Lcom/facebook/compactdisk/TaskQueueFactoryHolder;)Lcom/facebook/compactdisk/LazyDispatcher;

    move-result-object p0

    move-object v0, p0

    .line 391599
    sput-object v0, LX/2IJ;->a:Lcom/facebook/compactdisk/LazyDispatcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391600
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391601
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391602
    :cond_1
    sget-object v0, LX/2IJ;->a:Lcom/facebook/compactdisk/LazyDispatcher;

    return-object v0

    .line 391603
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391604
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 391605
    invoke-static {p0}, LX/30E;->a(LX/0QB;)Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    invoke-static {v0}, LX/2FB;->a(Lcom/facebook/compactdisk/TaskQueueFactoryHolder;)Lcom/facebook/compactdisk/LazyDispatcher;

    move-result-object v0

    return-object v0
.end method
