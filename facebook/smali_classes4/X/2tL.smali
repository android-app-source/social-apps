.class public final LX/2tL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel$HistogramModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:I

.field public c:I

.field public d:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 474922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 474923
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 474924
    iget-object v1, p0, LX/2tL;->a:LX/0Px;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 474925
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 474926
    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 474927
    iget v1, p0, LX/2tL;->b:I

    invoke-virtual {v0, v8, v1, v6}, LX/186;->a(III)V

    .line 474928
    const/4 v1, 0x2

    iget v2, p0, LX/2tL;->c:I

    invoke-virtual {v0, v1, v2, v6}, LX/186;->a(III)V

    .line 474929
    const/4 v1, 0x3

    iget-wide v2, p0, LX/2tL;->d:D

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 474930
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 474931
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 474932
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 474933
    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 474934
    new-instance v0, LX/15i;

    move-object v2, v7

    move-object v3, v7

    move v4, v8

    move-object v5, v7

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 474935
    new-instance v1, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    invoke-direct {v1, v0}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;-><init>(LX/15i;)V

    .line 474936
    return-object v1
.end method
