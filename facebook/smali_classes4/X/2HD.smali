.class public LX/2HD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/2HD;


# instance fields
.field private final b:LX/0W3;

.field private final c:LX/0lC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 390189
    const-class v0, LX/2HD;

    sput-object v0, LX/2HD;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0W3;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390191
    iput-object p1, p0, LX/2HD;->b:LX/0W3;

    .line 390192
    iput-object p2, p0, LX/2HD;->c:LX/0lC;

    .line 390193
    return-void
.end method

.method public static a(LX/0QB;)LX/2HD;
    .locals 5

    .prologue
    .line 390160
    sget-object v0, LX/2HD;->d:LX/2HD;

    if-nez v0, :cond_1

    .line 390161
    const-class v1, LX/2HD;

    monitor-enter v1

    .line 390162
    :try_start_0
    sget-object v0, LX/2HD;->d:LX/2HD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390163
    if-eqz v2, :cond_0

    .line 390164
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 390165
    new-instance p0, LX/2HD;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-direct {p0, v3, v4}, LX/2HD;-><init>(LX/0W3;LX/0lC;)V

    .line 390166
    move-object v0, p0

    .line 390167
    sput-object v0, LX/2HD;->d:LX/2HD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390168
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390169
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390170
    :cond_1
    sget-object v0, LX/2HD;->d:LX/2HD;

    return-object v0

    .line 390171
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390172
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/Ceo;
    .locals 8

    .prologue
    .line 390173
    new-instance v6, LX/Cen;

    iget-object v0, p0, LX/2HD;->b:LX/0W3;

    invoke-direct {v6, v0}, LX/Cen;-><init>(LX/0W3;)V

    .line 390174
    iget-wide v2, v6, LX/Cen;->a:J

    .line 390175
    iget-wide v4, v6, LX/Cen;->b:J

    .line 390176
    iget-object v0, v6, LX/Cen;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 390177
    :try_start_0
    iget-object v0, p0, LX/2HD;->c:LX/0lC;

    iget-object v1, v6, LX/Cen;->c:Ljava/lang/String;

    new-instance v7, LX/Cep;

    invoke-direct {v7, p0}, LX/Cep;-><init>(LX/2HD;)V

    invoke-virtual {v0, v1, v7}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 390178
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 390179
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_0
    move-wide v2, v0

    .line 390180
    :cond_0
    :goto_1
    iget-object v0, v6, LX/Cen;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 390181
    :try_start_1
    iget-object v0, p0, LX/2HD;->c:LX/0lC;

    iget-object v1, v6, LX/Cen;->d:Ljava/lang/String;

    new-instance v6, LX/Ceq;

    invoke-direct {v6, p0}, LX/Ceq;-><init>(LX/2HD;)V

    invoke-virtual {v0, v1, v6}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 390182
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 390183
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v0

    :goto_2
    move-wide v4, v0

    .line 390184
    :cond_1
    :goto_3
    new-instance v0, LX/Ceo;

    invoke-direct {v0, v2, v3, v4, v5}, LX/Ceo;-><init>(JJ)V

    return-object v0

    .line 390185
    :catch_0
    move-exception v0

    .line 390186
    sget-object v1, LX/2HD;->a:Ljava/lang/Class;

    const-string v7, ""

    invoke-static {v1, v7, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 390187
    :catch_1
    move-exception v0

    .line 390188
    sget-object v1, LX/2HD;->a:Ljava/lang/Class;

    const-string v6, ""

    invoke-static {v1, v6, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_2
    move-wide v0, v4

    goto :goto_2

    :cond_3
    move-wide v0, v2

    goto :goto_0
.end method
