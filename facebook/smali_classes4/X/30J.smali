.class public LX/30J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/30J;


# instance fields
.field private a:LX/30K;


# direct methods
.method public constructor <init>(LX/30K;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 484254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484255
    iput-object p1, p0, LX/30J;->a:LX/30K;

    .line 484256
    return-void
.end method

.method public static a(LX/0QB;)LX/30J;
    .locals 4

    .prologue
    .line 484257
    sget-object v0, LX/30J;->b:LX/30J;

    if-nez v0, :cond_1

    .line 484258
    const-class v1, LX/30J;

    monitor-enter v1

    .line 484259
    :try_start_0
    sget-object v0, LX/30J;->b:LX/30J;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 484260
    if-eqz v2, :cond_0

    .line 484261
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 484262
    new-instance p0, LX/30J;

    invoke-static {v0}, LX/30K;->a(LX/0QB;)LX/30K;

    move-result-object v3

    check-cast v3, LX/30K;

    invoke-direct {p0, v3}, LX/30J;-><init>(LX/30K;)V

    .line 484263
    move-object v0, p0

    .line 484264
    sput-object v0, LX/30J;->b:LX/30J;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 484265
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 484266
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 484267
    :cond_1
    sget-object v0, LX/30J;->b:LX/30J;

    return-object v0

    .line 484268
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 484269
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 484270
    iget-object v0, p0, LX/30J;->a:LX/30K;

    invoke-virtual {v0}, LX/30K;->a()[LX/30L;

    move-result-object v1

    .line 484271
    if-nez v1, :cond_0

    .line 484272
    const/4 v0, 0x0

    .line 484273
    :goto_0
    return-object v0

    .line 484274
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 484275
    array-length v3, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    .line 484276
    if-eqz v4, :cond_1

    .line 484277
    new-instance v5, LX/0m9;

    sget-object v6, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v6}, LX/0m9;-><init>(LX/0mC;)V

    .line 484278
    const-string v6, "size"

    iget v7, v4, LX/30L;->a:I

    invoke-virtual {v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 484279
    const-string v6, "extra_size"

    iget v7, v4, LX/30L;->b:I

    invoke-virtual {v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 484280
    const-string v6, "capacity"

    iget v7, v4, LX/30L;->c:I

    invoke-virtual {v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 484281
    const-string v6, "object_count"

    iget v7, v4, LX/30L;->d:I

    invoke-virtual {v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 484282
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 484283
    const-string v7, "last_eden_gc_length"

    iget-wide v9, v4, LX/30L;->e:D

    invoke-virtual {v6, v7, v9, v10}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 484284
    const-string v7, "last_gull_gc_length"

    iget-wide v9, v4, LX/30L;->f:D

    invoke-virtual {v6, v7, v9, v10}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 484285
    new-instance v7, LX/0m9;

    sget-object v8, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v8}, LX/0m9;-><init>(LX/0mC;)V

    .line 484286
    const-string v8, "heap"

    invoke-virtual {v7, v8, v5}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 484287
    const-string v5, "gc"

    invoke-virtual {v7, v5, v6}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 484288
    move-object v4, v7

    .line 484289
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 484290
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 484291
    :cond_2
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "jsc_perf_event"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 484292
    const-string v1, "records"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method
