.class public abstract LX/2EA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:LX/3BD;

.field public h:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, LX/3BD;->a:LX/3BD;

    iput-object v0, p0, LX/2EA;->g:LX/3BD;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    iget-object v0, p0, LX/2EA;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide an endpoint for this task by calling setService(ComponentName)."

    invoke-static {v0, v1}, LX/1ol;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, LX/2EA;->c:Ljava/lang/String;

    invoke-static {v0}, LX/2E6;->a(Ljava/lang/String;)V

    iget-object v0, p0, LX/2EA;->g:LX/3BD;

    invoke-static {v0}, Lcom/google/android/gms/gcm/Task;->a(LX/3BD;)V

    iget-boolean v0, p0, LX/2EA;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2EA;->h:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/gms/gcm/Task;->b(Landroid/os/Bundle;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b(I)LX/2EA;
.end method

.method public abstract b(Landroid/os/Bundle;)LX/2EA;
.end method

.method public abstract b(Ljava/lang/Class;)LX/2EA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/2fD;",
            ">;)",
            "LX/2EA;"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/String;)LX/2EA;
.end method

.method public abstract c()Lcom/google/android/gms/gcm/Task;
.end method

.method public abstract d(Z)LX/2EA;
.end method

.method public abstract e(Z)LX/2EA;
.end method
