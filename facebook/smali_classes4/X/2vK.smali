.class public LX/2vK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2vL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2vL",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final o:Ljava/lang/String;


# instance fields
.field public final a:Landroid/database/sqlite/SQLiteDatabase;

.field private final b:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final c:Ljava/io/File;

.field private final d:LX/0tC;

.field private final e:LX/0tD;

.field private final f:LX/0SG;

.field private final g:LX/0sT;

.field private final h:LX/0t5;

.field private final i:LX/0t8;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4Ud;",
            ">;"
        }
    .end annotation
.end field

.field private final k:I
    .annotation build Lcom/facebook/graphql/executor/cache/GraphQLDiskCacheModelStore$VisitScope;
    .end annotation
.end field

.field private final l:Ljava/lang/String;

.field public m:Z

.field private n:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 477783
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/0sr;->a:LX/0U1;

    .line 477784
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477785
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->f:LX/0U1;

    .line 477786
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477787
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->h:LX/0U1;

    .line 477788
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477789
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->k:LX/0U1;

    .line 477790
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477791
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->l:LX/0U1;

    .line 477792
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477793
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sx;->b:LX/0U1;

    .line 477794
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477795
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sx;->c:LX/0U1;

    .line 477796
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477797
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sx;->d:LX/0U1;

    .line 477798
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477799
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->c:LX/0U1;

    .line 477800
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477801
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->g:LX/0U1;

    .line 477802
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477803
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM queries"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INNER JOIN models"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON queries"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->k:LX/0U1;

    .line 477804
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477805
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "   = models"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sx;->a:LX/0U1;

    .line 477806
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477807
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2vK;->o:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/io/File;LX/0tC;LX/0SG;LX/0sT;LX/0t5;LX/0t8;LX/0Ot;ILjava/lang/String;)V
    .locals 2
    .param p10    # I
        .annotation build Lcom/facebook/graphql/executor/cache/GraphQLDiskCacheModelStore$VisitScope;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "Ljava/io/File;",
            "LX/0tC;",
            "LX/0SG;",
            "LX/0sT;",
            "Lcom/facebook/graphql/executor/iface/ObserverRegistry;",
            "Lcom/facebook/graphql/executor/cache/ConsistencyConfig;",
            "LX/0Ot",
            "<",
            "LX/4Ud;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 477808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 477809
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2vK;->m:Z

    .line 477810
    const/4 v0, 0x0

    iput-object v0, p0, LX/2vK;->n:[Ljava/lang/String;

    .line 477811
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    iput-object v0, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 477812
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, LX/2vK;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 477813
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LX/2vK;->c:Ljava/io/File;

    .line 477814
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tC;

    iput-object v0, p0, LX/2vK;->d:LX/0tC;

    .line 477815
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    iput-object v0, p0, LX/2vK;->f:LX/0SG;

    .line 477816
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sT;

    iput-object v0, p0, LX/2vK;->g:LX/0sT;

    .line 477817
    iput-object p7, p0, LX/2vK;->h:LX/0t5;

    .line 477818
    iput-object p8, p0, LX/2vK;->i:LX/0t8;

    .line 477819
    iput-object p9, p0, LX/2vK;->j:LX/0Ot;

    .line 477820
    iput p10, p0, LX/2vK;->k:I

    .line 477821
    if-nez p11, :cond_0

    const-string p11, "_no_query_"

    :cond_0
    iput-object p11, p0, LX/2vK;->l:Ljava/lang/String;

    .line 477822
    new-instance v0, LX/0tD;

    iget-object v1, p0, LX/2vK;->c:Ljava/io/File;

    invoke-direct {v0, v1}, LX/0tD;-><init>(Ljava/io/File;)V

    iput-object v0, p0, LX/2vK;->e:LX/0tD;

    .line 477823
    return-void
.end method

.method private static a(Lcom/facebook/flatbuffers/MutableFlattenable;)LX/0w5;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ")",
            "LX/0w5",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 477824
    instance-of v0, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;

    if-eqz v0, :cond_0

    .line 477825
    check-cast p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;

    .line 477826
    iget-object v0, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->b:LX/0w5;

    if-eqz v0, :cond_1

    .line 477827
    iget-object v0, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->b:LX/0w5;

    .line 477828
    :goto_0
    move-object v0, v0

    .line 477829
    :goto_1
    return-object v0

    :cond_0
    invoke-static {p0}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/database/Cursor;
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 477830
    const-string v2, "tags"

    new-array v3, v1, [Ljava/lang/String;

    sget-object v0, LX/0st;->b:LX/0U1;

    .line 477831
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 477832
    aput-object v0, v3, v7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/0st;->a:LX/0U1;

    .line 477833
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 477834
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " = ?"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    move-object v0, p0

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static a(ILX/15i;LX/0w5;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 3
    .param p2    # LX/0w5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 477835
    invoke-static {p0}, LX/1lN;->a(I)Z

    move-result v0

    .line 477836
    invoke-static {p0}, LX/1lN;->b(I)Z

    move-result v1

    .line 477837
    invoke-static {p1, p2, v0, v1}, LX/1lN;->a(LX/15i;LX/0w5;ZZ)Ljava/lang/Object;

    move-result-object v0

    .line 477838
    instance-of v2, v0, Ljava/util/List;

    if-eqz v2, :cond_1

    .line 477839
    if-eqz v1, :cond_0

    .line 477840
    new-instance v0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;

    sget-object v1, LX/16Z;->a:LX/16Z;

    invoke-direct {v0, v1, p1}, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;-><init>(LX/16a;LX/15i;)V

    .line 477841
    :goto_0
    return-object v0

    .line 477842
    :cond_0
    new-instance v0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;

    invoke-direct {v0, p2, p1}, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;-><init>(LX/0w5;LX/15i;)V

    goto :goto_0

    .line 477843
    :cond_1
    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    goto :goto_0
.end method

.method private static a(ILjava/lang/Object;)Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 477844
    instance-of v0, p1, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;

    if-eqz v0, :cond_0

    .line 477845
    invoke-static {p0}, LX/1lN;->a(I)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 477846
    check-cast p1, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;

    invoke-virtual {p1}, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->q_()LX/15i;

    move-result-object v0

    .line 477847
    invoke-virtual {v0}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 477848
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, LX/1lN;->a(ILjava/lang/Object;)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/2vK;JJLcom/facebook/flatbuffers/MutableFlattenable;I)V
    .locals 9

    .prologue
    .line 477849
    instance-of v0, p5, Lcom/facebook/graphql/modelutil/FragmentModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2vK;->g:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477850
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 477851
    invoke-interface {p5}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    .line 477852
    check-cast p5, Lcom/facebook/graphql/modelutil/FragmentModel;

    invoke-interface {p5}, Lcom/facebook/graphql/modelutil/FragmentModel;->d_()I

    move-result v1

    .line 477853
    invoke-static {p6}, LX/1lN;->a(I)Z

    move-result v2

    iget-object v3, p0, LX/2vK;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/4Ud;

    iget-object v4, p0, LX/2vK;->i:LX/0t8;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v7}, LX/4V0;->a(LX/15i;IZLX/4Ud;LX/0t8;ILjava/util/Collection;Ljava/util/Collection;)V

    .line 477854
    :goto_0
    iget-object v0, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0, p1, p2, v7}, LX/2vK;->a(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/Collection;)V

    .line 477855
    iget-object v0, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 477856
    const-string v1, "consistency_index"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/0t1;->a:LX/0U1;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 477857
    return-void

    .line 477858
    :cond_0
    invoke-static {p5}, LX/11F;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v7

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .locals 13
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param

    .prologue
    .line 477859
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 477860
    const-string v0, "confirmed"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 477861
    :goto_0
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 477862
    iget-object v0, p0, LX/2vK;->c:Ljava/io/File;

    invoke-static {v0}, LX/39R;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 477863
    invoke-interface/range {p3 .. p3}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {v1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 477864
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    invoke-static/range {p3 .. p3}, LX/2vK;->a(Lcom/facebook/flatbuffers/MutableFlattenable;)LX/0w5;

    move-result-object v8

    iget-object v9, p0, LX/2vK;->f:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v10

    invoke-static {v1, v8, v10, v11}, LX/1lJ;->a(ILX/0w5;J)[B

    move-result-object v8

    .line 477865
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v1, 0x0

    .line 477866
    :try_start_0
    invoke-static {v9, v6}, LX/31S;->a(Ljava/io/FileOutputStream;Ljava/nio/ByteBuffer;)I

    .line 477867
    invoke-virtual {v9, v8}, Ljava/io/FileOutputStream;->write([B)V

    .line 477868
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->flush()V

    .line 477869
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 477870
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 477871
    iget-object v1, p0, LX/2vK;->d:LX/0tC;

    invoke-virtual {v1}, LX/0tC;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 477872
    iget-object v1, p0, LX/2vK;->e:LX/0tD;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9, v6, v8}, LX/0tD;->a(Ljava/lang/String;Ljava/nio/ByteBuffer;[B)V

    .line 477873
    iget-object v1, p0, LX/2vK;->e:LX/0tD;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/0tD;->a(Ljava/lang/String;)V

    .line 477874
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 477875
    sget-object v6, LX/0sx;->b:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 477876
    sget-object v0, LX/0sx;->c:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    invoke-virtual {v1, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 477877
    sget-object v0, LX/0sx;->d:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    invoke-virtual {v1, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 477878
    iget-object v0, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "models"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, LX/0sx;->a:LX/0U1;

    invoke-virtual {v9}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " = ?"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v0, v6, v1, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 477879
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-object v1, p0

    move-object/from16 v6, p3

    .line 477880
    invoke-static/range {v1 .. v7}, LX/2vK;->a(LX/2vK;JJLcom/facebook/flatbuffers/MutableFlattenable;I)V

    .line 477881
    return-void

    .line 477882
    :cond_1
    invoke-static {p0, p1}, LX/2vK;->i(LX/2vK;Landroid/database/Cursor;)J

    move-result-wide v4

    goto/16 :goto_0

    .line 477883
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 477884
    :catchall_0
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    :goto_2
    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_2
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    goto :goto_3

    .line 477885
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 477886
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/Collection;)V
    .locals 9
    .param p3    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 477953
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 477954
    :cond_0
    :goto_0
    return-void

    .line 477955
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "INSERT OR REPLACE INTO tags ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/0st;->a:LX/0U1;

    .line 477956
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477957
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0st;->b:LX/0U1;

    .line 477958
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 477959
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") VALUES (?, ?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 477960
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    .line 477961
    :try_start_0
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 477962
    if-eqz v0, :cond_2

    .line 477963
    :try_start_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 477964
    const/4 v3, 0x1

    invoke-virtual {v2, v3, p1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 477965
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 477966
    const v3, 0x7fdcc6c4

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    const v3, 0x1e4629a3

    invoke-static {v3}, LX/03h;->a(I)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 477967
    :catch_0
    move-exception v1

    .line 477968
    :try_start_2
    const-string v3, "GraphQLDiskCacheModelStore"

    const-string v4, "Error inserting data with rowid %d and tag %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v0, v5, v6

    invoke-static {v3, v1, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 477969
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 477970
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0

    :cond_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    goto :goto_0
.end method

.method private e(Landroid/database/Cursor;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 477887
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 477888
    const/4 v1, 0x0

    .line 477889
    :try_start_0
    iget-object v0, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0, v2, v3}, LX/2vK;->a(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/database/Cursor;

    move-result-object v1

    .line 477890
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 477891
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 477892
    sget-object v2, LX/0st;->b:LX/0U1;

    .line 477893
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 477894
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 477895
    :cond_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 477896
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 477897
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 477898
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 477899
    invoke-static {v1}, LX/0sh;->a(Landroid/database/Cursor;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/0sh;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static i(LX/2vK;Landroid/database/Cursor;)J
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 477900
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 477901
    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 477902
    cmp-long v5, v6, v2

    if-eqz v5, :cond_0

    move-wide v0, v2

    .line 477903
    :goto_0
    return-wide v0

    .line 477904
    :cond_0
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 477905
    :try_start_0
    iget-object v2, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 477906
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "SELECT "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, LX/0sx;->b:LX/0U1;

    .line 477907
    iget-object v10, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v10

    .line 477908
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, LX/0sx;->c:LX/0U1;

    .line 477909
    iget-object v10, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v10

    .line 477910
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, LX/0sx;->d:LX/0U1;

    .line 477911
    iget-object v10, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v10

    .line 477912
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " FROM models"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " WHERE "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, LX/0sx;->a:LX/0U1;

    .line 477913
    iget-object v10, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v10

    .line 477914
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " = ?"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v10

    invoke-virtual {v2, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    move-object v4, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 477915
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 477916
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 477917
    sget-object v2, LX/0sx;->b:LX/0U1;

    .line 477918
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 477919
    invoke-interface {v4, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 477920
    sget-object v3, LX/0sx;->c:LX/0U1;

    .line 477921
    iget-object v5, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v5

    .line 477922
    invoke-interface {v4, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 477923
    sget-object v5, LX/0sx;->d:LX/0U1;

    .line 477924
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 477925
    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 477926
    new-instance v6, Landroid/content/ContentValues;

    const/4 v7, 0x2

    invoke-direct {v6, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 477927
    sget-object v7, LX/0sx;->b:LX/0U1;

    .line 477928
    iget-object v10, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v10

    .line 477929
    invoke-interface {v4, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v7, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 477930
    sget-object v2, LX/0sx;->c:LX/0U1;

    .line 477931
    iget-object v7, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v7

    .line 477932
    invoke-interface {v4, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 477933
    sget-object v2, LX/0sx;->d:LX/0U1;

    .line 477934
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 477935
    invoke-interface {v4, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 477936
    iget-object v2, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "models"

    const/4 v5, 0x0

    const v7, -0x5e5b0b98

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v2, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const v5, 0x68f8515c

    invoke-static {v5}, LX/03h;->a(I)V

    .line 477937
    const-wide/16 v6, -0x1

    cmp-long v5, v2, v6

    if-eqz v5, :cond_2

    move v5, v0

    :goto_2
    invoke-static {v5}, LX/0PB;->checkState(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 477938
    invoke-static {v4}, LX/0sh;->a(Landroid/database/Cursor;)V

    .line 477939
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 477940
    sget-object v5, LX/0sr;->l:LX/0U1;

    .line 477941
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 477942
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 477943
    iget-object v5, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "queries"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, LX/0sr;->a:LX/0U1;

    .line 477944
    iget-object v11, v10, LX/0U1;->d:Ljava/lang/String;

    move-object v10, v11

    .line 477945
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " = ?"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v10, v0, [Ljava/lang/String;

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v10, v1

    invoke-virtual {v5, v6, v4, v7, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 477946
    if-ne v4, v0, :cond_3

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-wide v0, v2

    .line 477947
    goto/16 :goto_0

    :cond_1
    move v2, v1

    .line 477948
    goto/16 :goto_1

    :cond_2
    move v5, v1

    .line 477949
    goto :goto_2

    .line 477950
    :catchall_0
    move-exception v0

    move-object v1, v4

    :goto_4
    invoke-static {v1}, LX/0sh;->a(Landroid/database/Cursor;)V

    throw v0

    :cond_3
    move v0, v1

    .line 477951
    goto :goto_3

    .line 477952
    :catchall_1
    move-exception v0

    move-object v1, v4

    goto :goto_4
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 477679
    check-cast p1, Landroid/database/Cursor;

    .line 477680
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 7

    .prologue
    .line 477971
    check-cast p1, Landroid/database/Cursor;

    const/4 v2, 0x0

    .line 477972
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 477973
    new-instance v0, LX/15i;

    invoke-static {v6, p2}, LX/2vK;->a(ILjava/lang/Object;)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 477974
    const-string v1, "GraphQLDiskCacheModelStore.reflattenMutableFlatbuffer"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    .line 477975
    invoke-static {p2}, LX/2vK;->a(Lcom/facebook/flatbuffers/MutableFlattenable;)LX/0w5;

    move-result-object v1

    invoke-static {v6, v0, v1}, LX/2vK;->a(ILX/15i;LX/0w5;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Collection;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 477976
    sget-object v0, LX/0st;->b:LX/0U1;

    .line 477977
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 477978
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v1

    .line 477979
    iget-object v0, p0, LX/2vK;->h:LX/0t5;

    invoke-virtual {v0}, LX/0t5;->a()Ljava/util/Collection;

    move-result-object v0

    .line 477980
    sget-object v2, LX/0sr;->b:LX/0U1;

    .line 477981
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 477982
    invoke-static {v2, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v2

    .line 477983
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 477984
    iget-object v3, p0, LX/2vK;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 477985
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v4

    .line 477986
    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 477987
    iget-object v3, p0, LX/2vK;->l:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 477988
    iget v3, p0, LX/2vK;->k:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 477989
    iget-object v3, p0, LX/2vK;->f:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 477990
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 477991
    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 477992
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 477993
    iget-object v3, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/2vK;->o:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LX/0sr;->a:LX/0U1;

    .line 477994
    iget-object p0, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, p0

    .line 477995
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " IN (   SELECT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LX/0st;->a:LX/0U1;

    .line 477996
    iget-object p0, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, p0

    .line 477997
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "   FROM tags"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "   WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ") AND "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v4, LX/0sr;->d:LX/0U1;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " = ? AND "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v4, LX/0sr;->b:LX/0U1;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " != ? AND "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v4, LX/0sx;->b:LX/0U1;

    .line 477998
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 477999
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " IS NOT NULL AND "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v4, LX/0sr;->m:LX/0U1;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " >= ? AND ("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "  (? - timestamp) < max_age_ms   OR "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 478000
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 478001
    return-object v0
.end method

.method public final a([J)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 478002
    array-length v0, p1

    new-array v1, v0, [Ljava/lang/String;

    .line 478003
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 478004
    aget-wide v2, p1, v0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 478005
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 478006
    :cond_0
    sget-object v0, LX/0sr;->a:LX/0U1;

    .line 478007
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 478008
    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 478009
    iget-object v2, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/2vK;->o:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " WHERE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 478010
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 478011
    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 477781
    iget-object v0, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    const v1, 0x52d7bc4a

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 477782
    return-void
.end method

.method public final a(Ljava/lang/Object;I)V
    .locals 1

    .prologue
    .line 478012
    check-cast p1, Landroid/database/Cursor;

    .line 478013
    invoke-interface {p1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 478014
    const/4 v0, 0x0

    iput-object v0, p0, LX/2vK;->n:[Ljava/lang/String;

    .line 478015
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param

    .prologue
    .line 477678
    check-cast p1, Landroid/database/Cursor;

    invoke-direct {p0, p1, p2, p3}, LX/2vK;->a(Landroid/database/Cursor;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/util/Set;)Z
    .locals 5

    .prologue
    .line 477681
    check-cast p1, Landroid/database/Cursor;

    const/4 v0, 0x0

    .line 477682
    if-nez p2, :cond_1

    .line 477683
    :cond_0
    :goto_0
    return v0

    .line 477684
    :cond_1
    iget-object v1, p0, LX/2vK;->n:[Ljava/lang/String;

    if-nez v1, :cond_2

    .line 477685
    invoke-direct {p0, p1}, LX/2vK;->e(Landroid/database/Cursor;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/2vK;->n:[Ljava/lang/String;

    .line 477686
    :cond_2
    iget-object v1, p0, LX/2vK;->n:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 477687
    iget-object v2, p0, LX/2vK;->n:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 477688
    invoke-interface {p2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 477689
    const/4 v0, 0x1

    goto :goto_0

    .line 477690
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 7

    .prologue
    .line 477691
    check-cast p1, Landroid/database/Cursor;

    const/4 v4, 0x1

    .line 477692
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 477693
    invoke-interface {p2}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v3

    .line 477694
    new-instance v0, LX/15i;

    invoke-virtual {v3}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v3}, LX/15i;->c()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v3}, LX/15i;->e()Ljava/nio/ByteBuffer;

    move-result-object v3

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 477695
    const-string v1, "GraphQLDiskCacheModelStore.reloadFromMutableFlatbuffer"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    .line 477696
    invoke-virtual {v0}, LX/15i;->b()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 477697
    invoke-static {p2}, LX/2vK;->a(Lcom/facebook/flatbuffers/MutableFlattenable;)LX/0w5;

    move-result-object v1

    invoke-static {v6, v0, v1}, LX/2vK;->a(ILX/15i;LX/0w5;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    return-object v0

    .line 477698
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 477699
    iget-object v0, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 477700
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 477701
    check-cast p1, Landroid/database/Cursor;

    .line 477702
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 477703
    const/4 v0, 0x0

    iput-object v0, p0, LX/2vK;->n:[Ljava/lang/String;

    .line 477704
    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .locals 12
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param

    .prologue
    .line 477705
    check-cast p1, Landroid/database/Cursor;

    .line 477706
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 477707
    const-string v0, "confirmed"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 477708
    :goto_0
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 477709
    invoke-interface {p3}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    .line 477710
    invoke-virtual {v0}, LX/15i;->c()Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 477711
    invoke-virtual {v0}, LX/15i;->e()Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 477712
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 477713
    sget-object v0, LX/0sx;->c:LX/0U1;

    .line 477714
    iget-object v9, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v9, v9

    .line 477715
    if-eqz v1, :cond_1

    invoke-static {v1}, LX/31S;->a(Ljava/nio/ByteBuffer;)[B

    move-result-object v0

    :goto_1
    invoke-virtual {v8, v9, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 477716
    sget-object v0, LX/0sx;->d:LX/0U1;

    .line 477717
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v1

    .line 477718
    if-eqz v6, :cond_2

    invoke-static {v6}, LX/31S;->a(Ljava/nio/ByteBuffer;)[B

    move-result-object v0

    :goto_2
    invoke-virtual {v8, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 477719
    iget-object v0, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "models"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, LX/0sx;->a:LX/0U1;

    .line 477720
    iget-object v10, v9, LX/0U1;->d:Ljava/lang/String;

    move-object v9, v10

    .line 477721
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " = ?"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v0, v1, v8, v6, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 477722
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-object v1, p0

    move-object v6, p3

    .line 477723
    invoke-static/range {v1 .. v7}, LX/2vK;->a(LX/2vK;JJLcom/facebook/flatbuffers/MutableFlattenable;I)V

    .line 477724
    return-void

    .line 477725
    :cond_0
    invoke-static {p0, p1}, LX/2vK;->i(LX/2vK;Landroid/database/Cursor;)J

    move-result-wide v4

    goto :goto_0

    .line 477726
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 477727
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 477728
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 477729
    check-cast p1, Landroid/database/Cursor;

    .line 477730
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 477731
    iget-object v0, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    const v1, -0x3a4a4aa0

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 477732
    return-void
.end method

.method public final d(Ljava/lang/Object;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 10

    .prologue
    .line 477733
    check-cast p1, Landroid/database/Cursor;

    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 477734
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    .line 477735
    new-instance v4, Ljava/io/File;

    iget-object v0, p0, LX/2vK;->c:Ljava/io/File;

    invoke-direct {v4, v0, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 477736
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 477737
    :try_start_0
    invoke-virtual {v8}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    sget-object v1, Ljava/nio/channels/FileChannel$MapMode;->READ_ONLY:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v2, 0x0

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v1

    .line 477738
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 477739
    iget-object v0, p0, LX/2vK;->d:LX/0tC;

    invoke-virtual {v0}, LX/0tC;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477740
    iget-object v0, p0, LX/2vK;->e:LX/0tD;

    invoke-virtual {v0, v6, v1}, LX/0tD;->a(Ljava/lang/String;Ljava/nio/ByteBuffer;)V

    .line 477741
    :cond_0
    invoke-static {v1}, LX/1lJ;->a(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 477742
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V

    .line 477743
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 477744
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 477745
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    .line 477746
    const/16 v0, 0x9

    invoke-static {p1, v0, v9}, LX/0sh;->a(Landroid/database/Cursor;II)LX/0w5;

    move-result-object v8

    .line 477747
    new-instance v0, LX/15i;

    if-nez v2, :cond_2

    move-object v2, v7

    :goto_0
    if-nez v3, :cond_3

    move-object v3, v7

    :goto_1
    move v4, v9

    move-object v5, v7

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 477748
    const-string v1, "GraphQLDiskCacheModelStore.getConfirmedModel"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    .line 477749
    invoke-static {v6, v0, v8}, LX/2vK;->a(ILX/15i;LX/0w5;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    return-object v0

    .line 477750
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 477751
    :catchall_0
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    :goto_2
    if-eqz v7, :cond_1

    :try_start_2
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    throw v0

    :catch_1
    move-exception v1

    invoke-static {v7, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_1
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V

    goto :goto_3

    .line 477752
    :cond_2
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    goto :goto_1

    .line 477753
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 477754
    const-string v0, "GraphQLDiskCache"

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 477755
    iget-boolean v0, p0, LX/2vK;->m:Z

    return v0
.end method

.method public final e(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 477756
    check-cast p1, Landroid/database/Cursor;

    .line 477757
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 477758
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 477759
    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 477760
    check-cast p1, Landroid/database/Cursor;

    const/4 v6, 0x0

    .line 477761
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 477762
    iget-object v2, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "queries"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/0sr;->a:LX/0U1;

    .line 477763
    iget-object p0, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, p0

    .line 477764
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = ?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 477765
    return-void
.end method

.method public final g(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 477766
    check-cast p1, Landroid/database/Cursor;

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 477767
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 477768
    const/4 v4, 0x3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 477769
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 477770
    sget-object v7, LX/0sr;->l:LX/0U1;

    .line 477771
    iget-object v8, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v8

    .line 477772
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 477773
    iget-object v4, p0, LX/2vK;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "queries"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, LX/0sr;->a:LX/0U1;

    .line 477774
    iget-object p0, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, p0

    .line 477775
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " = ?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v0, [Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v1

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 477776
    if-ne v2, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 477777
    return-void

    :cond_0
    move v0, v1

    .line 477778
    goto :goto_0
.end method

.method public final h(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 477779
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2vK;->m:Z

    .line 477780
    return-void
.end method
