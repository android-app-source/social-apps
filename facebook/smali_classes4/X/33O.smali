.class public final LX/33O;
.super LX/0ur;
.source ""


# instance fields
.field public A:I

.field public B:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Z

.field public F:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLEventsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public Q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/enums/GraphQLGender;

.field public S:Z

.field public T:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

.field public Y:Z

.field public Z:Z

.field public aA:Lcom/facebook/graphql/model/GraphQLContact;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:J

.field public aC:D

.field public aD:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Z

.field public aF:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:I

.field public aH:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public aK:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aN:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aO:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aV:Lcom/facebook/graphql/model/GraphQLProfileBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aX:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aY:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aZ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Z

.field public ab:Z

.field public ac:Z

.field public ad:Z

.field public ae:Z

.field public af:Z

.field public ag:Z

.field public ah:Z

.field public ai:Z

.field public aj:Z

.field public ak:Z

.field public al:Z

.field public am:Z

.field public an:Z

.field public ao:Z

.field public ap:Z

.field public aq:Z

.field public ar:Z

.field public as:Z

.field public at:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

.field public av:Z

.field public aw:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

.field public ax:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bA:Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bB:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bC:I

.field public bD:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bE:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bF:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bG:Z

.field public bH:Z

.field public bI:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bJ:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bK:Z

.field public bL:Z

.field public bM:Z

.field public bN:Z

.field public bO:I

.field public bP:I

.field public bQ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bS:D

.field public bT:Z

.field public bU:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public bV:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public bW:D

.field public ba:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bb:J

.field public bc:Z

.field public bd:Lcom/facebook/graphql/model/GraphQLProfileVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public be:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bf:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bg:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bh:Lcom/facebook/graphql/model/GraphQLEvent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bi:J

.field public bj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bk:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bl:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

.field public bm:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bn:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bo:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bp:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bq:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public br:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bs:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bt:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bu:Lcom/facebook/graphql/model/GraphQLName;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bv:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLName;",
            ">;"
        }
    .end annotation
.end field

.field public bw:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public bx:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public by:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bz:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLDate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLBylineFragment;",
            ">;"
        }
    .end annotation
.end field

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:D

.field public x:Lcom/facebook/graphql/model/GraphQLContact;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 492955
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 492956
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/33O;->P:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 492957
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGender;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGender;

    iput-object v0, p0, LX/33O;->R:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 492958
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    iput-object v0, p0, LX/33O;->X:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 492959
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    iput-object v0, p0, LX/33O;->au:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 492960
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    iput-object v0, p0, LX/33O;->aw:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    .line 492961
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, LX/33O;->bl:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 492962
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, LX/33O;->bw:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 492963
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, LX/33O;->bU:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 492964
    instance-of v0, p0, LX/33O;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 492965
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 2

    .prologue
    .line 492966
    new-instance v0, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLUser;-><init>(LX/33O;)V

    .line 492967
    return-object v0
.end method
