.class public LX/2Sm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2Sm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/0tX;

.field private final c:LX/1mR;

.field public final d:LX/2Sn;

.field public final e:LX/0TD;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0tX;LX/1mR;LX/2Sn;LX/0TD;)V
    .locals 0
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 413281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413282
    iput-object p1, p0, LX/2Sm;->a:Landroid/content/res/Resources;

    .line 413283
    iput-object p2, p0, LX/2Sm;->b:LX/0tX;

    .line 413284
    iput-object p3, p0, LX/2Sm;->c:LX/1mR;

    .line 413285
    iput-object p4, p0, LX/2Sm;->d:LX/2Sn;

    .line 413286
    iput-object p5, p0, LX/2Sm;->e:LX/0TD;

    .line 413287
    return-void
.end method

.method public static a(LX/0QB;)LX/2Sm;
    .locals 9

    .prologue
    .line 413268
    sget-object v0, LX/2Sm;->f:LX/2Sm;

    if-nez v0, :cond_1

    .line 413269
    const-class v1, LX/2Sm;

    monitor-enter v1

    .line 413270
    :try_start_0
    sget-object v0, LX/2Sm;->f:LX/2Sm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413271
    if-eqz v2, :cond_0

    .line 413272
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 413273
    new-instance v3, LX/2Sm;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v6

    check-cast v6, LX/1mR;

    invoke-static {v0}, LX/2Sn;->a(LX/0QB;)LX/2Sn;

    move-result-object v7

    check-cast v7, LX/2Sn;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, LX/0TD;

    invoke-direct/range {v3 .. v8}, LX/2Sm;-><init>(Landroid/content/res/Resources;LX/0tX;LX/1mR;LX/2Sn;LX/0TD;)V

    .line 413274
    move-object v0, v3

    .line 413275
    sput-object v0, LX/2Sm;->f:LX/2Sm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413276
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413277
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413278
    :cond_1
    sget-object v0, LX/2Sm;->f:LX/2Sm;

    return-object v0

    .line 413279
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413280
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
