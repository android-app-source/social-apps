.class public LX/3N1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3N2;

.field private final b:LX/3N3;


# direct methods
.method public constructor <init>(LX/3N2;LX/3N3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558086
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 558087
    iput-object p1, p0, LX/3N1;->a:LX/3N2;

    .line 558088
    iput-object p2, p0, LX/3N1;->b:LX/3N3;

    .line 558089
    return-void
.end method

.method public static a(LX/5dd;Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AppAttributionInfoModel;)LX/5dd;
    .locals 4

    .prologue
    .line 558062
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AppAttributionInfoModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 558063
    iput-object v0, p0, LX/5dd;->b:Ljava/lang/String;

    .line 558064
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AppAttributionInfoModel;->m()Ljava/lang/String;

    move-result-object v0

    .line 558065
    iput-object v0, p0, LX/5dd;->c:Ljava/lang/String;

    .line 558066
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AppAttributionInfoModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 558067
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AppAttributionInfoModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 558068
    iput-object v0, p0, LX/5dd;->e:Ljava/lang/String;

    .line 558069
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AppAttributionInfoModel;->l()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 558070
    const/4 p1, 0x1

    .line 558071
    invoke-static {}, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->newBuilder()LX/5dZ;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/15i;->h(II)Z

    move-result v3

    .line 558072
    iput-boolean v3, v2, LX/5dZ;->a:Z

    .line 558073
    move-object v2, v2

    .line 558074
    invoke-virtual {v1, v0, p1}, LX/15i;->h(II)Z

    move-result v3

    .line 558075
    iput-boolean v3, v2, LX/5dZ;->c:Z

    .line 558076
    move-object v2, v2

    .line 558077
    const/4 v3, 0x2

    invoke-virtual {v1, v0, v3}, LX/15i;->h(II)Z

    move-result v3

    .line 558078
    iput-boolean v3, v2, LX/5dZ;->d:Z

    .line 558079
    move-object v2, v2

    .line 558080
    invoke-virtual {v1, v0, p1}, LX/15i;->h(II)Z

    move-result v3

    .line 558081
    iput-boolean v3, v2, LX/5dZ;->e:Z

    .line 558082
    move-object v2, v2

    .line 558083
    invoke-virtual {v2}, LX/5dZ;->h()Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    move-result-object v2

    move-object v0, v2

    .line 558084
    iput-object v0, p0, LX/5dd;->j:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    .line 558085
    return-object p0
.end method

.method public static b(LX/0QB;)LX/3N1;
    .locals 3

    .prologue
    .line 558060
    new-instance v2, LX/3N1;

    invoke-static {p0}, LX/3N2;->b(LX/0QB;)LX/3N2;

    move-result-object v0

    check-cast v0, LX/3N2;

    invoke-static {p0}, LX/3N3;->a(LX/0QB;)LX/3N3;

    move-result-object v1

    check-cast v1, LX/3N3;

    invoke-direct {v2, v0, v1}, LX/3N1;-><init>(LX/3N2;LX/3N3;)V

    .line 558061
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/content/Intent;Ljava/lang/String;)Lcom/facebook/messaging/model/attribution/ContentAppAttribution;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 558027
    const-string v1, "com.facebook.orca.extra.PROTOCOL_VERSION"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 558028
    if-ne v1, v0, :cond_4

    .line 558029
    const-string v3, "com.facebook.orca.extra.PROTOCOL_VERSION"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 558030
    if-eqz v3, :cond_4

    .line 558031
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 558032
    :goto_0
    iget-object v1, p0, LX/3N1;->b:LX/3N3;

    const v3, 0x13354a2

    const/4 v4, 0x0

    .line 558033
    sget-object v5, LX/3N3;->a:LX/0Rf;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 558034
    sparse-switch v0, :sswitch_data_0

    .line 558035
    const/4 v5, 0x0

    :goto_1
    move v5, v5

    .line 558036
    if-nez v5, :cond_5

    .line 558037
    :cond_0
    :goto_2
    move v0, v4

    .line 558038
    if-nez v0, :cond_1

    move-object v0, v2

    .line 558039
    :goto_3
    return-object v0

    .line 558040
    :cond_1
    const-string v0, "com.facebook.orca.extra.APPLICATION_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 558041
    if-nez v0, :cond_2

    move-object v0, v2

    .line 558042
    goto :goto_3

    .line 558043
    :cond_2
    iget-object v1, p0, LX/3N1;->a:LX/3N2;

    invoke-virtual {v1, p2}, LX/3N2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 558044
    if-nez v1, :cond_3

    move-object v0, v2

    .line 558045
    goto :goto_3

    .line 558046
    :cond_3
    const-string v2, "com.facebook.orca.extra.METADATA"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 558047
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/high16 v4, 0x40000

    if-gt v3, v4, :cond_6

    :goto_4
    move-object v2, v2

    .line 558048
    invoke-static {}, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->newBuilder()LX/5dd;

    move-result-object v3

    .line 558049
    iput-object v0, v3, LX/5dd;->b:Ljava/lang/String;

    .line 558050
    move-object v0, v3

    .line 558051
    iput-object v1, v0, LX/5dd;->d:Ljava/lang/String;

    .line 558052
    move-object v0, v0

    .line 558053
    iput-object v2, v0, LX/5dd;->f:Ljava/lang/String;

    .line 558054
    move-object v0, v0

    .line 558055
    invoke-virtual {v0}, LX/5dd;->k()Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-result-object v0

    goto :goto_3

    .line 558056
    :catch_0
    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    if-lt v0, v3, :cond_0

    const/4 v4, 0x1

    goto :goto_2

    .line 558057
    :sswitch_0
    iget-object v5, v1, LX/3N3;->b:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    goto :goto_1

    .line 558058
    :sswitch_1
    iget-object v5, v1, LX/3N3;->c:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    goto :goto_1

    .line 558059
    :sswitch_2
    iget-object v5, v1, LX/3N3;->d:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    :sswitch_data_0
    .sparse-switch
        0x13354a2 -> :sswitch_0
        0x1337827 -> :sswitch_1
        0x133782a -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;)Lcom/facebook/messaging/model/attribution/ContentAppAttribution;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 557982
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->v()Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AppAttributionInfoModel;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->H()Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 557983
    const/4 v0, 0x0

    .line 557984
    :goto_0
    return-object v0

    .line 557985
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 557986
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->w()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 557987
    invoke-virtual {v3, v2, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 557988
    invoke-virtual {v3, v2, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_1

    .line 557989
    :cond_2
    invoke-static {}, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->newBuilder()LX/5dd;

    move-result-object v0

    .line 557990
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->v()Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AppAttributionInfoModel;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 557991
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->v()Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AppAttributionInfoModel;

    move-result-object v2

    invoke-static {v0, v2}, LX/3N1;->a(LX/5dd;Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AppAttributionInfoModel;)LX/5dd;

    move-result-object v0

    .line 557992
    :cond_3
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->x()Ljava/lang/String;

    move-result-object v2

    .line 557993
    iput-object v2, v0, LX/5dd;->f:Ljava/lang/String;

    .line 557994
    move-object v0, v0

    .line 557995
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->t()Ljava/lang/String;

    move-result-object v2

    .line 557996
    iput-object v2, v0, LX/5dd;->a:Ljava/lang/String;

    .line 557997
    move-object v0, v0

    .line 557998
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/5dd;->a(Ljava/util/Map;)LX/5dd;

    move-result-object v0

    invoke-virtual {v0}, LX/5dd;->k()Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-result-object v0

    goto :goto_0

    .line 557999
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->H()Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 558000
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->H()Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;

    move-result-object v0

    const/4 v5, 0x0

    .line 558001
    invoke-static {}, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->newBuilder()LX/5dd;

    move-result-object v3

    .line 558002
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 558003
    iput-object v2, v3, LX/5dd;->c:Ljava/lang/String;

    .line 558004
    move-object v2, v3

    .line 558005
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 558006
    iput-object v4, v2, LX/5dd;->b:Ljava/lang/String;

    .line 558007
    move-object v4, v2

    .line 558008
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    move-result-object v2

    invoke-static {v2}, LX/5dc;->fromGraphQLMessageAttributionType(Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;)LX/5dc;

    move-result-object v2

    .line 558009
    :goto_3
    iput-object v2, v4, LX/5dd;->g:LX/5dc;

    .line 558010
    move-object v2, v4

    .line 558011
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;->d()Ljava/lang/String;

    move-result-object v4

    .line 558012
    iput-object v4, v2, LX/5dd;->c:Ljava/lang/String;

    .line 558013
    move-object v2, v2

    .line 558014
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;->c()Ljava/lang/String;

    move-result-object v4

    .line 558015
    iput-object v4, v2, LX/5dd;->h:Ljava/lang/String;

    .line 558016
    move-object v2, v2

    .line 558017
    invoke-static {}, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->newBuilder()LX/5dZ;

    move-result-object v4

    .line 558018
    iput-boolean v5, v4, LX/5dZ;->a:Z

    .line 558019
    move-object v4, v4

    .line 558020
    iput-boolean v5, v4, LX/5dZ;->b:Z

    .line 558021
    move-object v4, v4

    .line 558022
    invoke-virtual {v4}, LX/5dZ;->h()Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    move-result-object v4

    .line 558023
    iput-object v4, v2, LX/5dd;->j:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    .line 558024
    move-object v0, v3

    .line 558025
    goto :goto_2

    .line 558026
    :cond_5
    sget-object v2, LX/5dc;->UNRECOGNIZED:LX/5dc;

    goto :goto_3
.end method
