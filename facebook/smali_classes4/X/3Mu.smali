.class public LX/3Mu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/3Mv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 556510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/service/model/SearchUserParams;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/messaging/service/model/SearchUserResult;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/service/model/SearchUserParams;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel;",
            ">;)",
            "Lcom/facebook/messaging/service/model/SearchUserResult;"
        }
    .end annotation

    .prologue
    .line 556511
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/service/model/SearchUserParams;->a()LX/0Rf;

    move-result-object v0

    .line 556512
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v6

    .line 556513
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v7

    .line 556514
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v8

    .line 556515
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v9

    .line 556516
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel;->a()Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel;->a()LX/0Px;

    move-result-object v10

    .line 556517
    if-eqz v6, :cond_1

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    move-object v2, v0

    .line 556518
    :goto_0
    if-eqz v7, :cond_2

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    move-object v3, v0

    .line 556519
    :goto_1
    if-eqz v8, :cond_3

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    move-object v4, v0

    .line 556520
    :goto_2
    if-eqz v9, :cond_4

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    move-object v5, v0

    .line 556521
    :goto_3
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, v11, :cond_8

    invoke-virtual {v10, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;

    .line 556522
    invoke-static {v0}, LX/3Mv;->b(Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;)Lcom/facebook/user/model/User;

    move-result-object v12

    .line 556523
    if-eqz v6, :cond_5

    if-eqz v2, :cond_5

    sget-object v13, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->o()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 556524
    invoke-virtual {v2, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 556525
    :cond_0
    :goto_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 556526
    :cond_1
    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_0

    .line 556527
    :cond_2
    const/4 v0, 0x0

    move-object v3, v0

    goto :goto_1

    .line 556528
    :cond_3
    const/4 v0, 0x0

    move-object v4, v0

    goto :goto_2

    .line 556529
    :cond_4
    const/4 v0, 0x0

    move-object v5, v0

    goto :goto_3

    .line 556530
    :cond_5
    if-eqz v7, :cond_6

    if-eqz v3, :cond_6

    invoke-virtual {v0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->s()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 556531
    invoke-virtual {v3, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 556532
    :cond_6
    if-eqz v8, :cond_7

    if-eqz v4, :cond_7

    invoke-static {v0}, LX/3Mu;->a(Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 556533
    invoke-virtual {v4, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 556534
    :cond_7
    if-eqz v9, :cond_0

    .line 556535
    invoke-virtual {v5, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 556536
    :cond_8
    new-instance v0, Lcom/facebook/messaging/service/model/SearchUserResult;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/service/model/SearchUserParams;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v6, :cond_9

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    :goto_6
    if-eqz v7, :cond_a

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    :goto_7
    if-eqz v8, :cond_b

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    :goto_8
    if-eqz v9, :cond_c

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    :goto_9
    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/service/model/SearchUserResult;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;LX/0Px;)V

    return-object v0

    :cond_9
    const/4 v2, 0x0

    goto :goto_6

    :cond_a
    const/4 v3, 0x0

    goto :goto_7

    :cond_b
    const/4 v4, 0x0

    goto :goto_8

    :cond_c
    const/4 v5, 0x0

    goto :goto_9
.end method

.method private static a(Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;)Z
    .locals 1

    .prologue
    .line 556537
    invoke-virtual {p0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->w()Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel$MutualFriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel$MutualFriendsModel;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
