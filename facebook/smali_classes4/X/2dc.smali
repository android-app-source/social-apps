.class public final LX/2dc;
.super LX/0ur;
.source ""


# instance fields
.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:I

.field public d:Z

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:D

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I

.field public j:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 443519
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 443520
    const/4 v0, 0x0

    iput-object v0, p0, LX/2dc;->j:LX/0x2;

    .line 443521
    instance-of v0, p0, LX/2dc;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 443522
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLImage;)LX/2dc;
    .locals 4

    .prologue
    .line 443506
    new-instance v1, LX/2dc;

    invoke-direct {v1}, LX/2dc;-><init>()V

    .line 443507
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 443508
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/2dc;->b:Ljava/lang/String;

    .line 443509
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    iput v0, v1, LX/2dc;->c:I

    .line 443510
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->n()Z

    move-result v0

    iput-boolean v0, v1, LX/2dc;->d:Z

    .line 443511
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/2dc;->e:Ljava/lang/String;

    .line 443512
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/2dc;->f:Ljava/lang/String;

    .line 443513
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->k()D

    move-result-wide v2

    iput-wide v2, v1, LX/2dc;->g:D

    .line 443514
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/2dc;->h:Ljava/lang/String;

    .line 443515
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    iput v0, v1, LX/2dc;->i:I

    .line 443516
    invoke-static {v1, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 443517
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->L_()LX/0x2;

    move-result-object v0

    invoke-virtual {v0}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    iput-object v0, v1, LX/2dc;->j:LX/0x2;

    .line 443518
    return-object v1
.end method


# virtual methods
.method public final a(I)LX/2dc;
    .locals 0

    .prologue
    .line 443504
    iput p1, p0, LX/2dc;->c:I

    .line 443505
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/2dc;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 443502
    iput-object p1, p0, LX/2dc;->f:Ljava/lang/String;

    .line 443503
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 443496
    new-instance v0, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLImage;-><init>(LX/2dc;)V

    .line 443497
    return-object v0
.end method

.method public final b(I)LX/2dc;
    .locals 0

    .prologue
    .line 443500
    iput p1, p0, LX/2dc;->i:I

    .line 443501
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/2dc;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 443498
    iput-object p1, p0, LX/2dc;->h:Ljava/lang/String;

    .line 443499
    return-object p0
.end method
