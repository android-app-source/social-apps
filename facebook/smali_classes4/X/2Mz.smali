.class public final LX/2Mz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/2My;

.field public final b:J

.field public final c:J

.field public final d:S

.field public final e:S

.field public final f:Z

.field public g:LX/03R;

.field public h:LX/03R;

.field public i:LX/03R;


# direct methods
.method private constructor <init>(LX/2My;JJSSZ)V
    .locals 2

    .prologue
    .line 397981
    iput-object p1, p0, LX/2Mz;->a:LX/2My;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397982
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2Mz;->g:LX/03R;

    .line 397983
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2Mz;->h:LX/03R;

    .line 397984
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2Mz;->i:LX/03R;

    .line 397985
    iput-wide p2, p0, LX/2Mz;->b:J

    .line 397986
    iput-wide p4, p0, LX/2Mz;->c:J

    .line 397987
    iput-short p6, p0, LX/2Mz;->d:S

    .line 397988
    iput-short p7, p0, LX/2Mz;->e:S

    .line 397989
    iput-boolean p8, p0, LX/2Mz;->f:Z

    .line 397990
    return-void
.end method

.method public synthetic constructor <init>(LX/2My;JJSSZB)V
    .locals 0

    .prologue
    .line 397991
    invoke-direct/range {p0 .. p8}, LX/2Mz;-><init>(LX/2My;JJSSZ)V

    return-void
.end method

.method public static a(LX/2Mz;LX/03R;SZ)LX/03R;
    .locals 2

    .prologue
    .line 397992
    invoke-virtual {p1}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397993
    :goto_0
    return-object p1

    :cond_0
    iget-object v0, p0, LX/2Mz;->a:LX/2My;

    iget-object v0, v0, LX/2My;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    invoke-interface {v0, v1, p2, p3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 397994
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2Mz;->g:LX/03R;

    .line 397995
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2Mz;->h:LX/03R;

    .line 397996
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2Mz;->i:LX/03R;

    .line 397997
    return-void
.end method

.method public final b()Z
    .locals 8

    .prologue
    .line 397998
    iget-object v0, p0, LX/2Mz;->a:LX/2My;

    iget-object v0, v0, LX/2My;->a:LX/0Uh;

    const/16 v1, 0x18a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397999
    iget-object v3, p0, LX/2Mz;->g:LX/03R;

    invoke-virtual {v3}, LX/03R;->isSet()Z

    move-result v3

    if-nez v3, :cond_0

    .line 398000
    iget-boolean v4, p0, LX/2Mz;->f:Z

    .line 398001
    iget-object v3, p0, LX/2Mz;->a:LX/2My;

    iget-object v3, v3, LX/2My;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0W3;

    iget-object v5, p0, LX/2Mz;->a:LX/2My;

    iget-object v5, v5, LX/2My;->g:LX/2Mz;

    iget-wide v5, v5, LX/2Mz;->b:J

    iget-object v7, p0, LX/2Mz;->a:LX/2My;

    iget-object v7, v7, LX/2My;->g:LX/2Mz;

    iget-boolean v7, v7, LX/2Mz;->f:Z

    invoke-interface {v3, v5, v6, v7}, LX/0W4;->b(JZ)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 398002
    iget-object v3, p0, LX/2Mz;->a:LX/2My;

    iget-object v3, v3, LX/2My;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0W3;

    iget-wide v5, p0, LX/2Mz;->b:J

    iget-boolean v4, p0, LX/2Mz;->f:Z

    invoke-interface {v3, v5, v6, v4}, LX/0W4;->a(JZ)Z

    move-result v3

    .line 398003
    :goto_0
    invoke-static {v3}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v3

    iput-object v3, p0, LX/2Mz;->g:LX/03R;

    .line 398004
    :cond_0
    iget-object v3, p0, LX/2Mz;->g:LX/03R;

    iget-boolean v4, p0, LX/2Mz;->f:Z

    invoke-virtual {v3, v4}, LX/03R;->asBoolean(Z)Z

    move-result v3

    move v0, v3

    .line 398005
    :goto_1
    return v0

    .line 398006
    :cond_1
    iget-object v0, p0, LX/2Mz;->h:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 398007
    iget-object v0, p0, LX/2Mz;->h:LX/03R;

    iget-boolean v1, p0, LX/2Mz;->f:Z

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    .line 398008
    :goto_2
    move v0, v0

    .line 398009
    goto :goto_1

    .line 398010
    :cond_2
    iget-object v3, p0, LX/2Mz;->a:LX/2My;

    iget-object v3, v3, LX/2My;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0W3;

    iget-object v5, p0, LX/2Mz;->a:LX/2My;

    iget-object v5, v5, LX/2My;->g:LX/2Mz;

    iget-wide v5, v5, LX/2Mz;->c:J

    iget-object v7, p0, LX/2Mz;->a:LX/2My;

    iget-object v7, v7, LX/2My;->g:LX/2Mz;

    iget-boolean v7, v7, LX/2Mz;->f:Z

    invoke-interface {v3, v5, v6, v7}, LX/0W4;->b(JZ)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 398011
    iget-object v3, p0, LX/2Mz;->a:LX/2My;

    iget-object v3, v3, LX/2My;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0W3;

    iget-wide v5, p0, LX/2Mz;->c:J

    iget-boolean v4, p0, LX/2Mz;->f:Z

    invoke-interface {v3, v5, v6, v4}, LX/0W4;->a(JZ)Z

    move-result v3

    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_0

    .line 398012
    :cond_4
    iget-object v0, p0, LX/2Mz;->i:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 398013
    iget-object v0, p0, LX/2Mz;->i:LX/03R;

    iget-boolean v1, p0, LX/2Mz;->f:Z

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_2

    .line 398014
    :cond_5
    iget-object v0, p0, LX/2Mz;->a:LX/2My;

    iget-object v0, v0, LX/2My;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ae;

    sget-object v1, LX/0oc;->EFFECTIVE:LX/0oc;

    const-string v2, "messenger_montage_poland_universe"

    invoke-interface {v0, v1, v2}, LX/0ae;->a(LX/0oc;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 398015
    iget-object v0, p0, LX/2Mz;->h:LX/03R;

    iget-short v1, p0, LX/2Mz;->d:S

    iget-boolean v2, p0, LX/2Mz;->f:Z

    invoke-static {p0, v0, v1, v2}, LX/2Mz;->a(LX/2Mz;LX/03R;SZ)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/2Mz;->h:LX/03R;

    .line 398016
    iget-object v0, p0, LX/2Mz;->h:LX/03R;

    iget-boolean v1, p0, LX/2Mz;->f:Z

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_2

    .line 398017
    :cond_6
    iget-object v0, p0, LX/2Mz;->a:LX/2My;

    iget-object v0, v0, LX/2My;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ae;

    sget-object v1, LX/0oc;->EFFECTIVE:LX/0oc;

    const-string v2, "messenger_montage_wave2_universe"

    invoke-interface {v0, v1, v2}, LX/0ae;->a(LX/0oc;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 398018
    iget-object v0, p0, LX/2Mz;->i:LX/03R;

    iget-short v1, p0, LX/2Mz;->e:S

    iget-boolean v2, p0, LX/2Mz;->f:Z

    invoke-static {p0, v0, v1, v2}, LX/2Mz;->a(LX/2Mz;LX/03R;SZ)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/2Mz;->i:LX/03R;

    .line 398019
    iget-object v0, p0, LX/2Mz;->i:LX/03R;

    iget-boolean v1, p0, LX/2Mz;->f:Z

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto/16 :goto_2

    .line 398020
    :cond_7
    iget-boolean v0, p0, LX/2Mz;->f:Z

    goto/16 :goto_2
.end method
