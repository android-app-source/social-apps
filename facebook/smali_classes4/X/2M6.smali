.class public final LX/2M6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Z

.field public c:Landroid/graphics/drawable/Drawable;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field public g:Landroid/graphics/drawable/Drawable;

.field public h:LX/2M7;

.field public i:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 396233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396234
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 396235
    iput-object v0, p0, LX/2M6;->d:LX/0Px;

    .line 396236
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 396237
    iput-object v0, p0, LX/2M6;->e:LX/0Px;

    .line 396238
    sget-object v0, LX/2M7;->ONLY_WHEN_SPACE_AVAILABLE:LX/2M7;

    iput-object v0, p0, LX/2M6;->h:LX/2M7;

    return-void
.end method

.method private a(LX/0Px;LX/0Px;)LX/2M6;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "LX/2M6;"
        }
    .end annotation

    .prologue
    .line 396239
    iput-object p1, p0, LX/2M6;->d:LX/0Px;

    .line 396240
    iput-object p2, p0, LX/2M6;->e:LX/0Px;

    .line 396241
    return-object p0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/2M6;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 396242
    if-nez p1, :cond_0

    .line 396243
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 396244
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/2M6;->a(LX/0Px;LX/0Px;)LX/2M6;

    .line 396245
    :goto_0
    return-object p0

    .line 396246
    :cond_0
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/2M6;->a(LX/0Px;LX/0Px;)LX/2M6;

    goto :goto_0
.end method

.method public final a()LX/6LR;
    .locals 1

    .prologue
    .line 396247
    iget-object v0, p0, LX/2M6;->a:Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 396248
    new-instance v0, LX/6LR;

    invoke-direct {v0, p0}, LX/6LR;-><init>(LX/2M6;)V

    return-object v0
.end method
