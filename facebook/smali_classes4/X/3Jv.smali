.class public abstract LX/3Jv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3Jx;

.field public final b:[F

.field private c:[F


# direct methods
.method public constructor <init>(LX/3Jx;[F)V
    .locals 0

    .prologue
    .line 548142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 548143
    iput-object p1, p0, LX/3Jv;->a:LX/3Jx;

    .line 548144
    iput-object p2, p0, LX/3Jv;->b:[F

    .line 548145
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 548146
    iget-object v0, p0, LX/3Jv;->b:[F

    array-length v0, v0

    return v0
.end method

.method public static a(Ljava/lang/String;)LX/3Jv;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 548148
    invoke-virtual {p0, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3Jw;->valueOf(Ljava/lang/String;)LX/3Jw;

    move-result-object v4

    .line 548149
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 548150
    array-length v0, v5

    new-array v6, v0, [F

    .line 548151
    array-length v7, v5

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v7, :cond_0

    aget-object v8, v5, v0

    .line 548152
    add-int/lit8 v3, v2, 0x1

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    aput v8, v6, v2

    .line 548153
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_0

    .line 548154
    :cond_0
    sget-object v0, LX/3Jy;->a:[I

    invoke-virtual {v4}, LX/3Jw;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 548155
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Unhandled vector command: %s"

    new-array v4, v9, [Ljava/lang/Object;

    aput-object p0, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548156
    :pswitch_0
    invoke-static {v4, v6}, LX/3Jv;->a(LX/3Jw;[F)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 548157
    new-instance v0, LX/3Jz;

    iget-object v1, v4, LX/3Jw;->argFormat:LX/3Jx;

    invoke-direct {v0, v1, v6}, LX/3Jz;-><init>(LX/3Jx;[F)V

    .line 548158
    :goto_1
    return-object v0

    .line 548159
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "VectorCommand MoveTo requires two arguments, but got %s"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548160
    :pswitch_1
    invoke-static {v4, v6}, LX/3Jv;->a(LX/3Jw;[F)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 548161
    new-instance v0, LX/3K4;

    iget-object v1, v4, LX/3Jw;->argFormat:LX/3Jx;

    invoke-direct {v0, v1, v6}, LX/3K4;-><init>(LX/3Jx;[F)V

    goto :goto_1

    .line 548162
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "VectorCommand QuadraticTo requires four arguments, but got %s"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548163
    :pswitch_2
    invoke-static {v4, v6}, LX/3Jv;->a(LX/3Jw;[F)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 548164
    new-instance v0, LX/3K0;

    iget-object v1, v4, LX/3Jw;->argFormat:LX/3Jx;

    invoke-direct {v0, v1, v6}, LX/3K0;-><init>(LX/3Jx;[F)V

    goto :goto_1

    .line 548165
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "VectorCommand CubicTo requires six arguments, but got %s"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548166
    :pswitch_3
    invoke-static {v4, v6}, LX/3Jv;->a(LX/3Jw;[F)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 548167
    new-instance v0, LX/3hJ;

    iget-object v1, v4, LX/3Jw;->argFormat:LX/3Jx;

    invoke-direct {v0, v1, v6}, LX/3hJ;-><init>(LX/3Jx;[F)V

    goto :goto_1

    .line 548168
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "VectorCommand LineTo requires two arguments, but got %s"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private static a(LX/3Jw;[F)Z
    .locals 2

    .prologue
    .line 548147
    iget v0, p0, LX/3Jw;->argCount:I

    array-length v1, p1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([F[F[F)[F
    .locals 8

    .prologue
    const v7, 0x3f2aaaab

    const/4 v6, 0x3

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 548115
    array-length v0, p1

    array-length v1, p2

    if-lt v0, v1, :cond_0

    .line 548116
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "convertUp should only be called to convert a lower order argument array to a higher one."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548117
    :cond_0
    array-length v0, p1

    if-ne v0, v3, :cond_3

    .line 548118
    array-length v0, p2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 548119
    aget v0, p0, v4

    aget v1, p1, v4

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    aput v0, p2, v4

    .line 548120
    aget v0, p0, v5

    aget v1, p1, v5

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    aput v0, p2, v5

    .line 548121
    aget v0, p1, v4

    aput v0, p2, v3

    .line 548122
    aget v0, p1, v5

    aput v0, p2, v6

    .line 548123
    :goto_0
    return-object p2

    .line 548124
    :cond_1
    array-length v0, p2

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    .line 548125
    aget v0, p0, v4

    aget v1, p1, v4

    aget v2, p0, v4

    sub-float/2addr v1, v2

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    aput v0, p2, v4

    .line 548126
    aget v0, p0, v5

    aget v1, p1, v5

    aget v2, p0, v5

    sub-float/2addr v1, v2

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    aput v0, p2, v5

    .line 548127
    aget v0, p1, v4

    aget v1, p0, v4

    aget v2, p1, v4

    sub-float/2addr v1, v2

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    aput v0, p2, v3

    .line 548128
    aget v0, p1, v5

    aget v1, p0, v5

    aget v2, p1, v5

    sub-float/2addr v1, v2

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    aput v0, p2, v6

    .line 548129
    const/4 v0, 0x4

    aget v1, p1, v4

    aput v1, p2, v0

    .line 548130
    const/4 v0, 0x5

    aget v1, p1, v5

    aput v1, p2, v0

    goto :goto_0

    .line 548131
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown conversion from %d args to %d"

    new-array v2, v3, [Ljava/lang/Object;

    array-length v3, p1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    array-length v3, p2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548132
    :cond_3
    array-length v0, p1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    .line 548133
    array-length v0, p2

    const/4 v1, 0x6

    if-ne v0, v1, :cond_4

    .line 548134
    aget v0, p0, v4

    aget v1, p1, v4

    aget v2, p0, v4

    sub-float/2addr v1, v2

    mul-float/2addr v1, v7

    add-float/2addr v0, v1

    aput v0, p2, v4

    .line 548135
    aget v0, p0, v5

    aget v1, p1, v5

    aget v2, p0, v5

    sub-float/2addr v1, v2

    mul-float/2addr v1, v7

    add-float/2addr v0, v1

    aput v0, p2, v5

    .line 548136
    aget v0, p1, v3

    aget v1, p1, v4

    aget v2, p1, v3

    sub-float/2addr v1, v2

    mul-float/2addr v1, v7

    add-float/2addr v0, v1

    aput v0, p2, v3

    .line 548137
    aget v0, p1, v6

    aget v1, p1, v5

    aget v2, p1, v6

    sub-float/2addr v1, v2

    mul-float/2addr v1, v7

    add-float/2addr v0, v1

    aput v0, p2, v6

    .line 548138
    const/4 v0, 0x4

    aget v1, p1, v3

    aput v1, p2, v0

    .line 548139
    const/4 v0, 0x5

    aget v1, p1, v6

    aput v1, p2, v0

    goto/16 :goto_0

    .line 548140
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown conversion from %d args to %d"

    new-array v2, v3, [Ljava/lang/Object;

    array-length v3, p1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    array-length v3, p2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548141
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown conversion from %d args to %d"

    new-array v2, v3, [Ljava/lang/Object;

    array-length v3, p1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    array-length v3, p2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b()[F
    .locals 1

    .prologue
    .line 548089
    iget-object v0, p0, LX/3Jv;->c:[F

    if-nez v0, :cond_0

    .line 548090
    iget-object v0, p0, LX/3Jv;->b:[F

    array-length v0, v0

    new-array v0, v0, [F

    iput-object v0, p0, LX/3Jv;->c:[F

    .line 548091
    :cond_0
    iget-object v0, p0, LX/3Jv;->c:[F

    return-object v0
.end method


# virtual methods
.method public a(LX/3Jv;FLX/9Ua;)V
    .locals 8

    .prologue
    .line 548092
    iget-object v0, p0, LX/3Jv;->a:LX/3Jx;

    iget-object v1, p1, LX/3Jv;->a:LX/3Jx;

    if-eq v0, v1, :cond_0

    .line 548093
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument format must match between interpolated commands. RELATIVE and ABSOLUTE coordinates should stay consistent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548094
    :cond_0
    invoke-direct {p0}, LX/3Jv;->a()I

    move-result v0

    invoke-direct {p1}, LX/3Jv;->a()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 548095
    iget-object v2, p0, LX/3Jv;->b:[F

    .line 548096
    iget-object v0, p3, LX/9Ua;->b:[F

    move-object v0, v0

    .line 548097
    iget-object v1, p1, LX/3Jv;->b:[F

    invoke-direct {p0}, LX/3Jv;->b()[F

    move-result-object v3

    invoke-static {v0, v1, v3}, LX/3Jv;->a([F[F[F)[F

    move-result-object v1

    .line 548098
    invoke-direct {p0}, LX/3Jv;->b()[F

    move-result-object v0

    move-object p1, p0

    .line 548099
    :goto_0
    const/4 v3, 0x0

    array-length v4, v0

    :goto_1
    if-ge v3, v4, :cond_3

    .line 548100
    aget v5, v2, v3

    aget v6, v1, v3

    .line 548101
    sub-float v7, v6, v5

    mul-float/2addr v7, p2

    add-float/2addr v7, v5

    move v5, v7

    .line 548102
    aput v5, v0, v3

    .line 548103
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 548104
    :cond_1
    invoke-direct {p0}, LX/3Jv;->a()I

    move-result v0

    invoke-direct {p1}, LX/3Jv;->a()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 548105
    iget-object v0, p3, LX/9Ua;->b:[F

    move-object v0, v0

    .line 548106
    iget-object v1, p0, LX/3Jv;->b:[F

    invoke-direct {p1}, LX/3Jv;->b()[F

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/3Jv;->a([F[F[F)[F

    move-result-object v2

    .line 548107
    iget-object v1, p1, LX/3Jv;->b:[F

    .line 548108
    invoke-direct {p1}, LX/3Jv;->b()[F

    move-result-object v0

    goto :goto_0

    .line 548109
    :cond_2
    iget-object v2, p0, LX/3Jv;->b:[F

    .line 548110
    iget-object v1, p1, LX/3Jv;->b:[F

    .line 548111
    invoke-direct {p0}, LX/3Jv;->b()[F

    move-result-object v0

    move-object p1, p0

    .line 548112
    goto :goto_0

    .line 548113
    :cond_3
    iget-object v1, p0, LX/3Jv;->a:LX/3Jx;

    invoke-virtual {p1, p3, v1, v0}, LX/3Jv;->a(LX/9Ua;LX/3Jx;[F)V

    .line 548114
    return-void
.end method

.method public abstract a(LX/9Ua;)V
.end method

.method public abstract a(LX/9Ua;LX/3Jx;[F)V
.end method
