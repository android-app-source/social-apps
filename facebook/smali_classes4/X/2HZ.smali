.class public LX/2HZ;
.super LX/2Gj;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2HZ;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/ui/browser/gating/IsInAppBrowserEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390619
    invoke-direct {p0}, LX/2Gj;-><init>()V

    .line 390620
    iput-object p1, p0, LX/2HZ;->a:LX/0Or;

    .line 390621
    iput-object p2, p0, LX/2HZ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 390622
    return-void
.end method

.method public static a(LX/0QB;)LX/2HZ;
    .locals 5

    .prologue
    .line 390623
    sget-object v0, LX/2HZ;->c:LX/2HZ;

    if-nez v0, :cond_1

    .line 390624
    const-class v1, LX/2HZ;

    monitor-enter v1

    .line 390625
    :try_start_0
    sget-object v0, LX/2HZ;->c:LX/2HZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390626
    if-eqz v2, :cond_0

    .line 390627
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 390628
    new-instance v4, LX/2HZ;

    const/16 v3, 0x36f

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v4, p0, v3}, LX/2HZ;-><init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 390629
    move-object v0, v4

    .line 390630
    sput-object v0, LX/2HZ;->c:LX/2HZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390631
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390632
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390633
    :cond_1
    sget-object v0, LX/2HZ;->c:LX/2HZ;

    return-object v0

    .line 390634
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390635
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0lF;
    .locals 1

    .prologue
    .line 390636
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 390637
    const-string v0, "browser_enabled"

    return-object v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 390638
    iget-object v0, p0, LX/2HZ;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2HZ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1C0;->a:LX/0Tn;

    invoke-interface {v0, v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
