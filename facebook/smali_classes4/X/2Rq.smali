.class public LX/2Rq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/platform/server/protocol/ResolveTaggableProfileIdsMethod$Params;",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/platform/server/handler/ParcelableString;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 410189
    const-class v0, LX/2Rq;

    sput-object v0, LX/2Rq;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410225
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 410202
    check-cast p1, Lcom/facebook/platform/server/protocol/ResolveTaggableProfileIdsMethod$Params;

    .line 410203
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 410204
    iget-object v0, p1, Lcom/facebook/platform/server/protocol/ResolveTaggableProfileIdsMethod$Params;->a:Ljava/util/List;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 410205
    new-instance v0, Lorg/json/JSONArray;

    iget-object v1, p1, Lcom/facebook/platform/server/protocol/ResolveTaggableProfileIdsMethod$Params;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 410206
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 410207
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "type"

    const-string v4, "resolvedtaggablefriend"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410208
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "taggable_ids"

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410209
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v2, "resolveTaggableFriendIds"

    .line 410210
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 410211
    move-object v0, v0

    .line 410212
    const-string v2, "GET"

    .line 410213
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 410214
    move-object v0, v0

    .line 410215
    const-string v2, "search"

    .line 410216
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 410217
    move-object v0, v0

    .line 410218
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 410219
    iput-object v2, v0, LX/14O;->k:LX/14S;

    .line 410220
    move-object v0, v0

    .line 410221
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 410222
    move-object v0, v0

    .line 410223
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 410190
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 410191
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 410192
    const-string v2, "data"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 410193
    if-eqz v0, :cond_1

    .line 410194
    invoke-virtual {v0}, LX/0lF;->G()Ljava/util/Iterator;

    move-result-object v2

    .line 410195
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 410196
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 410197
    const-string v3, "id"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 410198
    const-string v4, "taggable_id"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 410199
    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 410200
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Lcom/facebook/platform/server/handler/ParcelableString;

    invoke-virtual {v3}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Lcom/facebook/platform/server/handler/ParcelableString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 410201
    :cond_1
    return-object v1
.end method
