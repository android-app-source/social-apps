.class public LX/2tF;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2tF;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 474874
    invoke-direct {p0}, LX/398;-><init>()V

    .line 474875
    sget-object v0, LX/0ax;->dV:Ljava/lang/String;

    sget-object v1, LX/0ax;->dW:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 474876
    sget-object v0, LX/0ax;->da:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->FRIEND_REQUESTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 474877
    sget-object v0, LX/0ax;->cZ:Ljava/lang/String;

    sget-object v1, LX/0ax;->da:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 474878
    const-string v0, "requests"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->da:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 474879
    const-string v0, "profile_qr?source={%s %s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "source"

    sget-object v2, LX/FUk;->UNKNOWN:LX/FUk;

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/qrcode/QRCodeActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 474880
    return-void
.end method

.method public static a(LX/0QB;)LX/2tF;
    .locals 3

    .prologue
    .line 474862
    sget-object v0, LX/2tF;->a:LX/2tF;

    if-nez v0, :cond_1

    .line 474863
    const-class v1, LX/2tF;

    monitor-enter v1

    .line 474864
    :try_start_0
    sget-object v0, LX/2tF;->a:LX/2tF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 474865
    if-eqz v2, :cond_0

    .line 474866
    :try_start_1
    new-instance v0, LX/2tF;

    invoke-direct {v0}, LX/2tF;-><init>()V

    .line 474867
    move-object v0, v0

    .line 474868
    sput-object v0, LX/2tF;->a:LX/2tF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 474869
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 474870
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 474871
    :cond_1
    sget-object v0, LX/2tF;->a:LX/2tF;

    return-object v0

    .line 474872
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 474873
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
