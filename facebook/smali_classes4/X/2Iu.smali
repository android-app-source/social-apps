.class public abstract LX/2Iu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/0To;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field private static final e:[Ljava/lang/String;


# instance fields
.field private final c:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 392330
    new-instance v0, LX/0U1;

    const-string v1, "key"

    const-string v2, "TEXT PRIMARY KEY"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iu;->a:LX/0U1;

    .line 392331
    new-instance v0, LX/0U1;

    const-string v1, "value"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iu;->b:LX/0U1;

    .line 392332
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "value"

    aput-object v2, v0, v1

    sput-object v0, LX/2Iu;->e:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0QR;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QR",
            "<",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 392333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392334
    iput-object p1, p0, LX/2Iu;->c:LX/0QR;

    .line 392335
    iput-object p2, p0, LX/2Iu;->d:Ljava/lang/String;

    .line 392336
    return-void
.end method


# virtual methods
.method public final a(LX/0To;I)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)I"
        }
    .end annotation

    .prologue
    .line 392337
    invoke-virtual {p0, p1}, LX/2Iu;->a(LX/0To;)Ljava/lang/String;

    move-result-object v0

    .line 392338
    if-nez v0, :cond_0

    .line 392339
    :goto_0
    return p2

    .line 392340
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    goto :goto_0

    .line 392341
    :catch_0
    goto :goto_0
.end method

.method public final a(LX/0To;J)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;J)J"
        }
    .end annotation

    .prologue
    .line 392342
    invoke-virtual {p0, p1}, LX/2Iu;->a(LX/0To;)Ljava/lang/String;

    move-result-object v0

    .line 392343
    if-nez v0, :cond_0

    .line 392344
    :goto_0
    return-wide p2

    .line 392345
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    goto :goto_0

    .line 392346
    :catch_0
    goto :goto_0
.end method

.method public final a(LX/0To;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 392347
    iget-object v0, p0, LX/2Iu;->c:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    iget-object v1, p0, LX/2Iu;->d:Ljava/lang/String;

    sget-object v2, LX/2Iu;->e:[Ljava/lang/String;

    const-string v3, "key = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 392348
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392349
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 392350
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 392351
    :goto_0
    return-object v5

    .line 392352
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a(LX/0To;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 392353
    invoke-virtual {p0, p1}, LX/2Iu;->a(LX/0To;)Ljava/lang/String;

    move-result-object v0

    .line 392354
    if-nez v0, :cond_0

    .line 392355
    :goto_0
    return-object p2

    :cond_0
    move-object p2, v0

    goto :goto_0
.end method

.method public final a(LX/0To;LX/03R;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/03R;",
            ")V"
        }
    .end annotation

    .prologue
    .line 392356
    invoke-virtual {p2}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/2Iu;->b(LX/0To;I)V

    .line 392357
    return-void
.end method

.method public final a(LX/0To;Z)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)Z"
        }
    .end annotation

    .prologue
    .line 392358
    invoke-virtual {p0, p1}, LX/2Iu;->a(LX/0To;)Ljava/lang/String;

    move-result-object v0

    .line 392359
    if-nez v0, :cond_0

    .line 392360
    :goto_0
    return p2

    .line 392361
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    goto :goto_0

    .line 392362
    :catch_0
    goto :goto_0
.end method

.method public final b(LX/0To;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    .prologue
    .line 392363
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/2Iu;->b(LX/0To;Ljava/lang/String;)V

    .line 392364
    return-void
.end method

.method public final b(LX/0To;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;J)V"
        }
    .end annotation

    .prologue
    .line 392365
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/2Iu;->b(LX/0To;Ljava/lang/String;)V

    .line 392366
    return-void
.end method

.method public final b(LX/0To;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 392367
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 392368
    const-string v0, "key"

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 392369
    const-string v0, "value"

    invoke-virtual {v1, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 392370
    iget-object v0, p0, LX/2Iu;->c:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    iget-object v2, p0, LX/2Iu;->d:Ljava/lang/String;

    const/4 v3, 0x0

    const v4, 0x1527d5e4

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x4e970e5e

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392371
    return-void
.end method

.method public final b(LX/0To;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .prologue
    .line 392372
    if-eqz p2, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p0, p1, v0}, LX/2Iu;->b(LX/0To;Ljava/lang/String;)V

    .line 392373
    return-void

    .line 392374
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method
