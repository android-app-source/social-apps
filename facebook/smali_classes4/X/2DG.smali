.class public LX/2DG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2DH;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2DH",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:I

.field public final b:LX/2DI;

.field public final c:Landroid/content/Context;

.field private final d:LX/0ma;

.field public final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/HandlerThreadFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0mi;

.field private final g:LX/0mi;

.field private h:Z

.field public i:Z

.field private j:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private l:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private m:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private n:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILX/2DI;LX/0ma;Ljava/lang/Class;LX/0mi;LX/0mi;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "LX/2DI;",
            "Lcom/facebook/analytics2/logger/AppBackgroundedProvider;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/HandlerThreadFactory;",
            ">;",
            "LX/0mi;",
            "LX/0mi;",
            ")V"
        }
    .end annotation

    .prologue
    .line 383921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383922
    iput-object p1, p0, LX/2DG;->c:Landroid/content/Context;

    .line 383923
    iput p2, p0, LX/2DG;->a:I

    .line 383924
    iput-object p3, p0, LX/2DG;->b:LX/2DI;

    .line 383925
    iput-object p4, p0, LX/2DG;->d:LX/0ma;

    .line 383926
    iput-object p5, p0, LX/2DG;->e:Ljava/lang/Class;

    .line 383927
    iput-object p6, p0, LX/2DG;->f:LX/0mi;

    .line 383928
    iput-object p7, p0, LX/2DG;->g:LX/0mi;

    .line 383929
    invoke-static {p0}, LX/2DG;->h(LX/2DG;)V

    .line 383930
    return-void
.end method

.method private static a(Ljava/io/File;Ljava/io/File;)I
    .locals 1
    .param p0    # Ljava/io/File;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/io/File;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 383931
    if-nez p1, :cond_1

    .line 383932
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 383933
    :goto_0
    return v0

    .line 383934
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 383935
    :cond_1
    if-nez p0, :cond_2

    .line 383936
    const/4 v0, -0x1

    goto :goto_0

    .line 383937
    :cond_2
    invoke-virtual {p0, p1}, Ljava/io/File;->compareTo(Ljava/io/File;)I

    move-result v0

    goto :goto_0
.end method

.method private declared-synchronized a(JJ)V
    .locals 5

    .prologue
    .line 383938
    monitor-enter p0

    .line 383939
    :try_start_0
    iget-boolean v0, p0, LX/2DG;->i:Z

    if-nez v0, :cond_0

    .line 383940
    iget-object v0, p0, LX/2DG;->c:Landroid/content/Context;

    invoke-static {v0}, LX/0pF;->a(Landroid/content/Context;)LX/0pF;

    move-result-object v0

    iget-object v1, p0, LX/2DG;->e:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0pF;->b(Ljava/lang/String;)Lcom/facebook/analytics2/logger/HandlerThreadFactory;

    move-result-object v0

    .line 383941
    const-string v1, "JobRanReceiver"

    invoke-interface {v0, v1}, Lcom/facebook/analytics2/logger/HandlerThreadFactory;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 383942
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 383943
    iget-object v1, p0, LX/2DG;->c:Landroid/content/Context;

    new-instance v2, LX/2El;

    invoke-direct {v2, p0}, LX/2El;-><init>(LX/2DG;)V

    new-instance v3, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 383944
    new-instance v0, Landroid/content/IntentFilter;

    const-string v4, "com.facebook.analytics2.action.UPLOAD_JOB_RAN"

    invoke-direct {v0, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 383945
    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v4, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 383946
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2DG;->i:Z

    .line 383947
    :cond_0
    iget-object v0, p0, LX/2DG;->k:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-direct {p0}, LX/2DG;->g()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-lez v0, :cond_1

    .line 383948
    invoke-direct {p0, p1, p2, p3, p4}, LX/2DG;->b(JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383949
    :cond_1
    monitor-exit p0

    return-void

    .line 383950
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(JJ)V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 383886
    monitor-enter p0

    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    cmp-long v0, p3, v2

    if-nez v0, :cond_0

    .line 383887
    :try_start_0
    iget-object v0, p0, LX/2DG;->c:Landroid/content/Context;

    invoke-static {v0}, LX/2En;->a(Landroid/content/Context;)LX/2En;

    move-result-object v0

    iget v1, p0, LX/2DG;->a:I

    invoke-virtual {v0, v1}, LX/2En;->a(I)V

    .line 383888
    invoke-static {p0}, LX/2DG;->h(LX/2DG;)V

    .line 383889
    const-string v0, "com.facebook.analytics2.logger.UPLOAD_NOW"

    invoke-static {p0, v0}, LX/2DG;->b(LX/2DG;Ljava/lang/String;)V

    .line 383890
    invoke-static {}, LX/2bv;->a()LX/2bv;

    move-result-object v0

    iget-object v1, p0, LX/2DG;->c:Landroid/content/Context;

    const-string v2, "com.facebook.analytics2.logger.UPLOAD_NOW"

    iget-object v3, p0, LX/2DG;->b:LX/2DI;

    const/4 v4, 0x0

    iget v5, p0, LX/2DG;->a:I

    new-instance v6, LX/2Er;

    invoke-direct {v6, p1, p2, p3, p4}, LX/2Er;-><init>(JJ)V

    invoke-virtual/range {v0 .. v6}, LX/2bv;->a(Landroid/content/Context;Ljava/lang/String;LX/2DI;Landroid/os/Bundle;ILX/2Er;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383891
    :goto_0
    monitor-exit p0

    return-void

    .line 383892
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2DG;->c:Landroid/content/Context;

    invoke-static {v0}, LX/2En;->a(Landroid/content/Context;)LX/2En;

    move-result-object v1

    iget v2, p0, LX/2DG;->a:I

    iget-object v3, p0, LX/2DG;->b:LX/2DI;

    move-wide v4, p1

    move-wide v6, p3

    invoke-virtual/range {v1 .. v7}, LX/2En;->a(ILX/2DI;JJ)V

    .line 383893
    iput-wide p1, p0, LX/2DG;->j:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 383894
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/2DG;Ljava/io/File;)V
    .locals 1
    .param p0    # LX/2DG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 383957
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2DG;->n:Ljava/io/File;

    invoke-static {v0, p1}, LX/2DG;->a(Ljava/io/File;Ljava/io/File;)I

    move-result v0

    if-lez v0, :cond_1

    .line 383958
    invoke-virtual {p0}, LX/2DG;->b()V

    .line 383959
    :cond_0
    :goto_0
    invoke-static {p0}, LX/2DG;->i(LX/2DG;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383960
    monitor-exit p0

    return-void

    .line 383961
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2DG;->m:Ljava/io/File;

    invoke-static {v0, p1}, LX/2DG;->a(Ljava/io/File;Ljava/io/File;)I

    move-result v0

    if-lez v0, :cond_0

    .line 383962
    invoke-virtual {p0}, LX/2DG;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 383963
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/2DG;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 383951
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/2DG;->k:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383952
    monitor-exit p0

    return-void

    .line 383953
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 1

    .prologue
    .line 383954
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2DG;->l:Ljava/io/File;

    iput-object v0, p0, LX/2DG;->m:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383955
    monitor-exit p0

    return-void

    .line 383956
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized c(LX/2DG;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 383913
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2DG;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 383914
    iget-object v0, p0, LX/2DG;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383915
    const/4 v0, 0x0

    iput-object v0, p0, LX/2DG;->k:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383916
    :cond_0
    monitor-exit p0

    return-void

    .line 383917
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()V
    .locals 1

    .prologue
    .line 383918
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2DG;->l:Ljava/io/File;

    iput-object v0, p0, LX/2DG;->n:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383919
    monitor-exit p0

    return-void

    .line 383920
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private f()LX/0mi;
    .locals 1

    .prologue
    .line 383910
    iget-object v0, p0, LX/2DG;->d:LX/0ma;

    invoke-virtual {v0}, LX/0ma;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383911
    iget-object v0, p0, LX/2DG;->g:LX/0mi;

    .line 383912
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2DG;->f:LX/0mi;

    goto :goto_0
.end method

.method private declared-synchronized g()J
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 383905
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2DG;->h:Z

    if-nez v0, :cond_0

    .line 383906
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2DG;->h:Z

    .line 383907
    iget-object v0, p0, LX/2DG;->c:Landroid/content/Context;

    invoke-static {v0}, LX/2En;->a(Landroid/content/Context;)LX/2En;

    move-result-object v0

    iget v1, p0, LX/2DG;->a:I

    invoke-virtual {v0, v1}, LX/2En;->b(I)J

    move-result-wide v0

    iput-wide v0, p0, LX/2DG;->j:J

    .line 383908
    :cond_0
    iget-wide v0, p0, LX/2DG;->j:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    .line 383909
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized h(LX/2DG;)V
    .locals 2

    .prologue
    .line 383902
    monitor-enter p0

    const-wide v0, 0x7fffffffffffffffL

    :try_start_0
    iput-wide v0, p0, LX/2DG;->j:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383903
    monitor-exit p0

    return-void

    .line 383904
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized i(LX/2DG;)V
    .locals 1

    .prologue
    .line 383898
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/2DG;->n:Ljava/io/File;

    .line 383899
    const/4 v0, 0x0

    iput-object v0, p0, LX/2DG;->m:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383900
    monitor-exit p0

    return-void

    .line 383901
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 383895
    invoke-direct {p0}, LX/2DG;->c()V

    .line 383896
    invoke-direct {p0}, LX/2DG;->f()LX/0mi;

    move-result-object v0

    iget-wide v0, v0, LX/0mi;->a:J

    invoke-direct {p0}, LX/2DG;->f()LX/0mi;

    move-result-object v2

    iget-wide v2, v2, LX/0mi;->b:J

    invoke-direct {p0, v0, v1, v2, v3}, LX/2DG;->a(JJ)V

    .line 383897
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 383882
    check-cast p1, Ljava/io/File;

    .line 383883
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/2DG;->l:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383884
    monitor-exit p0

    return-void

    .line 383885
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 383870
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2DG;->i(LX/2DG;)V

    .line 383871
    iget-object v0, p0, LX/2DG;->c:Landroid/content/Context;

    invoke-static {v0}, LX/2En;->a(Landroid/content/Context;)LX/2En;

    move-result-object v0

    iget v1, p0, LX/2DG;->a:I

    invoke-virtual {v0, v1}, LX/2En;->a(I)V

    .line 383872
    invoke-static {p0}, LX/2DG;->h(LX/2DG;)V

    .line 383873
    const-string v0, "com.facebook.analytics2.logger.USER_LOGOUT"

    invoke-static {p0, v0}, LX/2DG;->b(LX/2DG;Ljava/lang/String;)V

    .line 383874
    new-instance v4, LX/40C;

    invoke-direct {v4, p1}, LX/40C;-><init>(Ljava/lang/String;)V

    .line 383875
    invoke-static {}, LX/2bv;->a()LX/2bv;

    move-result-object v0

    iget-object v1, p0, LX/2DG;->c:Landroid/content/Context;

    const-string v2, "com.facebook.analytics2.logger.USER_LOGOUT"

    iget-object v3, p0, LX/2DG;->b:LX/2DI;

    .line 383876
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 383877
    const-string v6, "user_id"

    iget-object p1, v4, LX/40C;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 383878
    move-object v4, v5

    .line 383879
    iget v5, p0, LX/2DG;->a:I

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/2bv;->a(Landroid/content/Context;Ljava/lang/String;LX/2DI;Landroid/os/Bundle;ILX/2Er;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383880
    monitor-exit p0

    return-void

    .line 383881
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 383867
    invoke-direct {p0}, LX/2DG;->d()V

    .line 383868
    invoke-direct {p0}, LX/2DG;->f()LX/0mi;

    move-result-object v0

    iget-wide v0, v0, LX/0mi;->c:J

    invoke-direct {p0}, LX/2DG;->f()LX/0mi;

    move-result-object v2

    iget-wide v2, v2, LX/0mi;->d:J

    invoke-direct {p0, v0, v1, v2, v3}, LX/2DG;->a(JJ)V

    .line 383869
    return-void
.end method
