.class public LX/3HK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HL",
        "<",
        "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3HJ;


# direct methods
.method public constructor <init>(LX/3HJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 543241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 543242
    iput-object p1, p0, LX/3HK;->a:LX/3HJ;

    .line 543243
    return-void
.end method

.method public static b(LX/0QB;)LX/3HK;
    .locals 2

    .prologue
    .line 543244
    new-instance v1, LX/3HK;

    invoke-static {p0}, LX/3HJ;->b(LX/0QB;)LX/3HJ;

    move-result-object v0

    check-cast v0, LX/3HJ;

    invoke-direct {v1, v0}, LX/3HK;-><init>(LX/3HJ;)V

    .line 543245
    return-object v1
.end method


# virtual methods
.method public final bridge synthetic a(LX/0jT;)LX/4VT;
    .locals 1

    .prologue
    .line 543246
    check-cast p1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    invoke-virtual {p0, p1}, LX/3HK;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;)LX/4VT;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;)LX/4VT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 543247
    iget-object v1, p0, LX/3HK;->a:LX/3HJ;

    invoke-virtual {v1}, LX/3HJ;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 543248
    :cond_0
    :goto_0
    return-object v0

    .line 543249
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    move-result-object v1

    .line 543250
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->k()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 543251
    new-instance v0, LX/6A4;

    invoke-direct {v0, v1}, LX/6A4;-><init>(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 543252
    const-class v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 543253
    const/4 v0, 0x0

    return-object v0
.end method
