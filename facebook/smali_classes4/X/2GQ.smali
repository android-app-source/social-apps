.class public LX/2GQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0Up;
.implements LX/1rV;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2GQ;


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private final b:LX/0TD;

.field private final c:LX/1qw;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;LX/0TD;LX/1qw;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 388408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388409
    iput-object p1, p0, LX/2GQ;->a:Landroid/content/ContentResolver;

    .line 388410
    iput-object p2, p0, LX/2GQ;->b:LX/0TD;

    .line 388411
    iput-object p3, p0, LX/2GQ;->c:LX/1qw;

    .line 388412
    return-void
.end method

.method public static a(LX/0QB;)LX/2GQ;
    .locals 6

    .prologue
    .line 388413
    sget-object v0, LX/2GQ;->d:LX/2GQ;

    if-nez v0, :cond_1

    .line 388414
    const-class v1, LX/2GQ;

    monitor-enter v1

    .line 388415
    :try_start_0
    sget-object v0, LX/2GQ;->d:LX/2GQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 388416
    if-eqz v2, :cond_0

    .line 388417
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 388418
    new-instance p0, LX/2GQ;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/1qw;->a(LX/0QB;)LX/1qw;

    move-result-object v5

    check-cast v5, LX/1qw;

    invoke-direct {p0, v3, v4, v5}, LX/2GQ;-><init>(Landroid/content/ContentResolver;LX/0TD;LX/1qw;)V

    .line 388419
    move-object v0, p0

    .line 388420
    sput-object v0, LX/2GQ;->d:LX/2GQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 388421
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 388422
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 388423
    :cond_1
    sget-object v0, LX/2GQ;->d:LX/2GQ;

    return-object v0

    .line 388424
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 388425
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/util/Locale;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 388426
    iget-object v0, p0, LX/2GQ;->b:LX/0TD;

    new-instance v1, Lcom/facebook/bookmark/db/BookmarkDatabaseCleaner$1;

    invoke-direct {v1, p0}, Lcom/facebook/bookmark/db/BookmarkDatabaseCleaner$1;-><init>(LX/2GQ;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final clearUserData()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 388427
    iget-object v0, p0, LX/2GQ;->a:Landroid/content/ContentResolver;

    sget-object v1, LX/0Ow;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 388428
    if-eqz v0, :cond_0

    .line 388429
    iget-object v1, p0, LX/2GQ;->a:Landroid/content/ContentResolver;

    sget-object v2, LX/0Ow;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 388430
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 388431
    :cond_0
    return-void
.end method

.method public final init()V
    .locals 1

    .prologue
    .line 388432
    iget-object v0, p0, LX/2GQ;->c:LX/1qw;

    invoke-virtual {v0, p0}, LX/1qw;->a(LX/1rV;)V

    .line 388433
    return-void
.end method
