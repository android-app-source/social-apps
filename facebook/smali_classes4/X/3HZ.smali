.class public final LX/3HZ;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ow;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V
    .locals 0

    .prologue
    .line 543777
    iput-object p1, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 543778
    const-class v0, LX/2ow;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 12

    .prologue
    .line 543685
    check-cast p1, LX/2ow;

    const/4 v1, -0x1

    .line 543686
    iget-object v0, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    .line 543687
    invoke-virtual {v0}, LX/3Ga;->g()Z

    .line 543688
    iget-object v0, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->w:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionContainerLinearLayout;

    if-eqz v0, :cond_0

    .line 543689
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 543690
    iget-object v1, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-wide v2, v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->x:D

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpg-double v1, v2, v4

    if-gez v1, :cond_2

    iget-object v1, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v1, v1, LX/2oy;->j:LX/2pb;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v1, v1, LX/2oy;->j:LX/2pb;

    .line 543691
    iget-object v2, v1, LX/2pb;->D:LX/04G;

    move-object v1, v2

    .line 543692
    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    if-eq v1, v2, :cond_2

    .line 543693
    const/16 v1, 0x30

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 543694
    iget-object v1, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->w:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionContainerLinearLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionContainerLinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 543695
    iget-object v0, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->w:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionContainerLinearLayout;

    const/4 v1, 0x1

    .line 543696
    iput-boolean v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionContainerLinearLayout;->a:Z

    .line 543697
    :cond_0
    :goto_0
    iget-object v0, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    const/high16 v6, 0x42840000    # 66.0f

    const/high16 v4, 0x42040000    # 33.0f

    const/4 v5, 0x2

    .line 543698
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->r:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->q:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    if-nez v1, :cond_b

    .line 543699
    :cond_1
    :goto_1
    iget-object v0, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v1, p1, LX/2ow;->a:LX/2oN;

    iget-object v2, p1, LX/2ow;->b:LX/2oN;

    .line 543700
    iget-object v3, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->r:Lcom/facebook/resources/ui/FbTextView;

    if-nez v3, :cond_c

    .line 543701
    :goto_2
    sget-object v0, LX/D72;->a:[I

    iget-object v1, p1, LX/2ow;->a:LX/2oN;

    invoke-virtual {v1}, LX/2oN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 543702
    :goto_3
    return-void

    .line 543703
    :cond_2
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 543704
    iget-object v1, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->w:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionContainerLinearLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionContainerLinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 543705
    iget-object v0, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->w:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionContainerLinearLayout;

    const/4 v1, 0x0

    .line 543706
    iput-boolean v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionContainerLinearLayout;->a:Z

    .line 543707
    goto :goto_0

    .line 543708
    :pswitch_0
    iget-object v0, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    .line 543709
    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->y(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543710
    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->B(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543711
    iget-object v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v6}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->c(Landroid/view/View;)V

    .line 543712
    iget-object v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->q:Landroid/widget/TextView;

    invoke-static {v6}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->c(Landroid/view/View;)V

    .line 543713
    iget-object v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v6}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->c(Landroid/view/View;)V

    .line 543714
    iget-object v6, p1, LX/2ow;->c:LX/7M6;

    iget-boolean v6, v6, LX/7M6;->b:Z

    if-eqz v6, :cond_3

    .line 543715
    new-instance v6, LX/D74;

    const-wide/16 v8, 0x5dc

    invoke-direct {v6, v0, v8, v9}, LX/D74;-><init>(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;J)V

    iput-object v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->v:LX/D74;

    .line 543716
    iget-object v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->v:LX/D74;

    invoke-virtual {v6}, LX/D74;->start()Landroid/os/CountDownTimer;

    .line 543717
    :cond_3
    goto :goto_3

    .line 543718
    :pswitch_1
    iget-object v0, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    .line 543719
    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->y(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543720
    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->D(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543721
    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->G(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543722
    goto :goto_3

    .line 543723
    :pswitch_2
    iget-object v0, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    .line 543724
    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->y(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543725
    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->B(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543726
    invoke-virtual {v0}, LX/3Ga;->g()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v1, :cond_4

    .line 543727
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 543728
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-static {v1}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->c(Landroid/view/View;)V

    .line 543729
    :cond_4
    goto :goto_3

    .line 543730
    :pswitch_3
    iget-object v0, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v1, p1, LX/2ow;->c:LX/7M6;

    iget-wide v2, v1, LX/7M6;->a:J

    .line 543731
    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->y(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543732
    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->D(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543733
    iget-object v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v6}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->c(Landroid/view/View;)V

    .line 543734
    iget-object v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->q:Landroid/widget/TextView;

    invoke-static {v6}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->c(Landroid/view/View;)V

    .line 543735
    iget-object v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v6}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->c(Landroid/view/View;)V

    .line 543736
    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->G(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543737
    invoke-virtual {v0}, LX/3Ga;->g()Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v6, :cond_5

    .line 543738
    iget-object v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 543739
    :cond_5
    invoke-virtual {v0}, LX/3Ga;->g()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 543740
    iget-object v7, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->u:LX/D73;

    if-eqz v7, :cond_6

    .line 543741
    iget-object v7, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->u:LX/D73;

    invoke-virtual {v7}, LX/D73;->cancel()V

    .line 543742
    :cond_6
    new-instance v7, LX/D73;

    invoke-direct {v7, v0, v2, v3}, LX/D73;-><init>(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;J)V

    iput-object v7, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->u:LX/D73;

    .line 543743
    iget-object v7, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->u:LX/D73;

    invoke-virtual {v7}, LX/D73;->start()Landroid/os/CountDownTimer;

    .line 543744
    iget-object v7, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v7, :cond_7

    .line 543745
    iget-object v7, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 543746
    :cond_7
    iget-object v7, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v7}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const-wide/16 v9, 0x1f4

    invoke-virtual {v7, v9, v10}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 543747
    :cond_8
    goto/16 :goto_3

    .line 543748
    :pswitch_4
    iget-object v0, p0, LX/3HZ;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    .line 543749
    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->z(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543750
    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->B(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543751
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->u:LX/D73;

    if-eqz v1, :cond_9

    .line 543752
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->u:LX/D73;

    invoke-virtual {v1}, LX/D73;->cancel()V

    .line 543753
    :cond_9
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->v:LX/D74;

    if-eqz v1, :cond_a

    .line 543754
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->v:LX/D74;

    invoke-virtual {v1}, LX/D74;->cancel()V

    .line 543755
    :cond_a
    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->D(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543756
    goto/16 :goto_3

    .line 543757
    :cond_b
    invoke-virtual {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 543758
    sget-object v2, LX/D72;->b:[I

    iget-object v3, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->z:LX/3HY;

    invoke-virtual {v3}, LX/3HY;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 543759
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    mul-float v3, v6, v1

    float-to-int v3, v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 543760
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    mul-float v3, v6, v1

    float-to-int v3, v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 543761
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->requestLayout()V

    .line 543762
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0050

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v1

    invoke-virtual {v2, v5, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    .line 543763
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->q:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0054

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v1

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 543764
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b005e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v1

    invoke-virtual {v2, v5, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    goto/16 :goto_1

    .line 543765
    :pswitch_5
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    mul-float v3, v4, v1

    float-to-int v3, v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 543766
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    mul-float v3, v4, v1

    float-to-int v3, v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 543767
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->requestLayout()V

    .line 543768
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0049

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v1

    invoke-virtual {v2, v5, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    .line 543769
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->q:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v1

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 543770
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0056

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v1

    invoke-virtual {v2, v5, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    goto/16 :goto_1

    .line 543771
    :cond_c
    sget-object v3, LX/D72;->a:[I

    invoke-virtual {v1}, LX/2oN;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_2

    goto/16 :goto_2

    .line 543772
    :pswitch_6
    sget-object v3, LX/2oN;->NONE:LX/2oN;

    if-eq v2, v3, :cond_d

    sget-object v3, LX/2oN;->TRANSITION:LX/2oN;

    if-eq v2, v3, :cond_d

    sget-object v3, LX/2oN;->WAIT_FOR_ADS:LX/2oN;

    if-ne v2, v3, :cond_e

    .line 543773
    :cond_d
    iget-object v3, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080de3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 543774
    :pswitch_7
    iget-object v3, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080de4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 543775
    :cond_e
    iget-object v3, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080de5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 543776
    :pswitch_8
    iget-object v3, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080de8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_8
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
