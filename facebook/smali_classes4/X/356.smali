.class public LX/356;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/356;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 496611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 496612
    return-void
.end method

.method public static a(LX/0QB;)LX/356;
    .locals 4

    .prologue
    .line 496613
    sget-object v0, LX/356;->b:LX/356;

    if-nez v0, :cond_1

    .line 496614
    const-class v1, LX/356;

    monitor-enter v1

    .line 496615
    :try_start_0
    sget-object v0, LX/356;->b:LX/356;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 496616
    if-eqz v2, :cond_0

    .line 496617
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 496618
    new-instance v3, LX/356;

    invoke-direct {v3}, LX/356;-><init>()V

    .line 496619
    const/16 p0, 0xac0

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 496620
    iput-object p0, v3, LX/356;->a:LX/0Or;

    .line 496621
    move-object v0, v3

    .line 496622
    sput-object v0, LX/356;->b:LX/356;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496623
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 496624
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 496625
    :cond_1
    sget-object v0, LX/356;->b:LX/356;

    return-object v0

    .line 496626
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 496627
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 496628
    iget-object v0, p0, LX/356;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    .line 496629
    const/16 v1, 0x9c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
