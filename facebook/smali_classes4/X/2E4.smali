.class public LX/2E4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2E4;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384940
    iput-object p1, p0, LX/2E4;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 384941
    return-void
.end method

.method public static a(LX/0QB;)LX/2E4;
    .locals 4

    .prologue
    .line 384942
    sget-object v0, LX/2E4;->b:LX/2E4;

    if-nez v0, :cond_1

    .line 384943
    const-class v1, LX/2E4;

    monitor-enter v1

    .line 384944
    :try_start_0
    sget-object v0, LX/2E4;->b:LX/2E4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 384945
    if-eqz v2, :cond_0

    .line 384946
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 384947
    new-instance p0, LX/2E4;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/2E4;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 384948
    move-object v0, p0

    .line 384949
    sput-object v0, LX/2E4;->b:LX/2E4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384950
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 384951
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 384952
    :cond_1
    sget-object v0, LX/2E4;->b:LX/2E4;

    return-object v0

    .line 384953
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 384954
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
