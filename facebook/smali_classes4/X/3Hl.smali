.class public final LX/3Hl;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;LX/2oy;)V
    .locals 0

    .prologue
    .line 544755
    iput-object p1, p0, LX/3Hl;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    .line 544756
    invoke-direct {p0, p2}, LX/2oa;-><init>(LX/2oy;)V

    .line 544757
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 544754
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 544746
    check-cast p1, LX/2ou;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 544747
    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/Object;

    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    aput-object v0, v3, v2

    iget-object v0, p0, LX/3Hl;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    aput-object v0, v3, v1

    const/4 v4, 0x2

    iget-object v0, p0, LX/3Hl;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-nez v0, :cond_1

    const-string v0, "null"

    :goto_0
    aput-object v0, v3, v4

    const/4 v0, 0x3

    iget-object v4, p0, LX/3Hl;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v4, v4, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v4}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x4

    iget-object v0, p0, LX/3Hl;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v3, v4

    .line 544748
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/3Hl;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_0

    .line 544749
    iget-object v0, p0, LX/3Hl;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->o:Landroid/os/Handler;

    iget-object v1, p0, LX/3Hl;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->L:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 544750
    iget-object v0, p0, LX/3Hl;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->o:Landroid/os/Handler;

    iget-object v1, p0, LX/3Hl;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->L:Ljava/lang/Runnable;

    const v2, -0x548512a3

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 544751
    :cond_0
    iget-object v0, p0, LX/3Hl;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v1, p1, LX/2ou;->b:LX/2qV;

    invoke-static {v0, v1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;LX/2qV;)V

    .line 544752
    return-void

    .line 544753
    :cond_1
    iget-object v0, p0, LX/3Hl;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method
