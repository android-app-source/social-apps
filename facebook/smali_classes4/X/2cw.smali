.class public LX/2cw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/2cw;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0SG;

.field public final c:LX/0So;

.field public final d:LX/0bW;

.field private final e:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "LX/2cx;",
            "LX/3FD;",
            ">;"
        }
    .end annotation
.end field

.field private f:J


# direct methods
.method public constructor <init>(LX/0Ot;LX/0SG;LX/0So;LX/0bW;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0SG;",
            "LX/0So;",
            "LX/0bW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 442468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442469
    const-class v0, LX/2cx;

    invoke-static {v0}, LX/0PM;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, LX/2cw;->e:Ljava/util/EnumMap;

    .line 442470
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2cw;->f:J

    .line 442471
    iput-object p1, p0, LX/2cw;->a:LX/0Ot;

    .line 442472
    iput-object p2, p0, LX/2cw;->b:LX/0SG;

    .line 442473
    iput-object p3, p0, LX/2cw;->c:LX/0So;

    .line 442474
    iput-object p4, p0, LX/2cw;->d:LX/0bW;

    .line 442475
    return-void
.end method

.method public static a(LX/0am;)LX/0lF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;)",
            "LX/0lF;"
        }
    .end annotation

    .prologue
    .line 442489
    invoke-virtual {p0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 442490
    const/4 v0, 0x0

    .line 442491
    :goto_0
    return-object v0

    .line 442492
    :cond_0
    invoke-virtual {p0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    .line 442493
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 442494
    const-string v2, "page_id"

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 442495
    const-string v2, "page_name"

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-object v0, v1

    .line 442496
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2cw;
    .locals 7

    .prologue
    .line 442476
    sget-object v0, LX/2cw;->g:LX/2cw;

    if-nez v0, :cond_1

    .line 442477
    const-class v1, LX/2cw;

    monitor-enter v1

    .line 442478
    :try_start_0
    sget-object v0, LX/2cw;->g:LX/2cw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 442479
    if-eqz v2, :cond_0

    .line 442480
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 442481
    new-instance v6, LX/2cw;

    const/16 v3, 0xbc

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/0bU;->a(LX/0QB;)LX/0bU;

    move-result-object v5

    check-cast v5, LX/0bW;

    invoke-direct {v6, p0, v3, v4, v5}, LX/2cw;-><init>(LX/0Ot;LX/0SG;LX/0So;LX/0bW;)V

    .line 442482
    move-object v0, v6

    .line 442483
    sput-object v0, LX/2cw;->g:LX/2cw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 442484
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 442485
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 442486
    :cond_1
    sget-object v0, LX/2cw;->g:LX/2cw;

    return-object v0

    .line 442487
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 442488
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a(LX/2cw;LX/2cx;ZJ)LX/3FD;
    .locals 5

    .prologue
    .line 442405
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2cw;->e:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3FD;

    .line 442406
    if-nez p2, :cond_0

    if-nez v0, :cond_1

    .line 442407
    :cond_0
    new-instance v0, LX/3FD;

    invoke-direct {v0, p3, p4}, LX/3FD;-><init>(J)V

    .line 442408
    iget-object v1, p0, LX/2cw;->e:Ljava/util/EnumMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442409
    iget-object v1, p0, LX/2cw;->d:LX/0bW;

    const-string v2, "New analytics session for source: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-interface {v1, v2, v3}, LX/0bW;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 442410
    :cond_1
    monitor-exit p0

    return-object v0

    .line 442411
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(LX/2cw;LX/3FC;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/2cx;)V
    .locals 6

    .prologue
    .line 442456
    iget-object v0, p0, LX/2cw;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 442457
    invoke-virtual {p1}, LX/3FC;->shouldResetSession()Z

    move-result v2

    invoke-static {p0, p3, v2, v0, v1}, LX/2cw;->a(LX/2cw;LX/2cx;ZJ)LX/3FD;

    move-result-object v2

    .line 442458
    const-string v3, "session_token"

    iget-object v4, v2, LX/3FD;->a:Ljava/util/UUID;

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442459
    const-string v3, "wall_time"

    iget-object v4, p0, LX/2cw;->b:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {p2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442460
    const-string v3, "up_time"

    invoke-virtual {p2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442461
    const-string v3, "trigger"

    invoke-virtual {p3}, LX/2cx;->getNameForAnalytics()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442462
    const-string v3, "time_since_session_start"

    iget-wide v4, p0, LX/2cw;->f:J

    sub-long/2addr v0, v4

    invoke-virtual {p2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442463
    iget-object v0, v2, LX/3FD;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 442464
    const-string v0, "logging_id"

    iget-object v1, v2, LX/3FD;->c:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442465
    :cond_0
    iget-object v0, p0, LX/2cw;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, p2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 442466
    iget-object v0, p0, LX/2cw;->d:LX/0bW;

    const-string v1, "Logged analytics event: %s, source: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    invoke-interface {v0, v1, v2}, LX/0bW;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 442467
    return-void
.end method

.method public static b(LX/Cdn;)LX/0lF;
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 442497
    new-instance v2, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 442498
    const-string v1, "scan_start_time"

    .line 442499
    iget-wide v9, p0, LX/Cdn;->b:J

    move-wide v4, v9

    .line 442500
    invoke-virtual {v2, v1, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 442501
    const-string v1, "scan_end_time"

    .line 442502
    iget-wide v9, p0, LX/Cdn;->c:J

    move-wide v4, v9

    .line 442503
    invoke-virtual {v2, v1, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 442504
    const-string v1, "scan_duration"

    .line 442505
    iget-wide v9, p0, LX/Cdn;->d:J

    move-wide v4, v9

    .line 442506
    invoke-virtual {v2, v1, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 442507
    iget-object v1, p0, LX/Cdn;->a:LX/0Px;

    move-object v3, v1

    .line 442508
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 442509
    const-string v1, "total_ble_devices_found"

    invoke-virtual {v2, v1, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 442510
    const-string v1, "total_ble_detection_events"

    invoke-virtual {v2, v1, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 442511
    const-string v0, "ble_devices_found"

    invoke-virtual {v2, v0}, LX/0m9;->k(Ljava/lang/String;)LX/0m9;

    .line 442512
    :cond_0
    return-object v2

    .line 442513
    :cond_1
    const-string v1, "total_ble_devices_found"

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    invoke-virtual {v2, v1, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 442514
    const-string v1, "total_ble_detection_events"

    .line 442515
    iget v4, p0, LX/Cdn;->e:I

    move v4, v4

    .line 442516
    invoke-virtual {v2, v1, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 442517
    const-string v1, "ble_devices_found"

    invoke-virtual {v2, v1}, LX/0m9;->i(Ljava/lang/String;)LX/162;

    move-result-object v4

    .line 442518
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cdm;

    .line 442519
    invoke-virtual {v4}, LX/162;->I()LX/0m9;

    move-result-object v6

    .line 442520
    const-string v7, "mac_address"

    .line 442521
    iget-object v8, v0, LX/Cdm;->a:LX/Cdl;

    move-object v8, v8

    .line 442522
    iget-object v9, v8, LX/Cdl;->a:Landroid/bluetooth/BluetoothDevice;

    move-object v8, v9

    .line 442523
    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 442524
    const-string v7, "broadcast_data"

    .line 442525
    iget-object v8, v0, LX/Cdm;->a:LX/Cdl;

    move-object v8, v8

    .line 442526
    iget-object v9, v8, LX/Cdl;->c:Ljava/lang/String;

    move-object v8, v9

    .line 442527
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 442528
    const-string v7, "detection_event_count"

    invoke-virtual {v0}, LX/Cdm;->c()I

    move-result v8

    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 442529
    const-string v7, "avg_rssi"

    invoke-virtual {v0}, LX/Cdm;->b()I

    move-result v0

    invoke-virtual {v6, v7, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 442530
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0bZ;LX/2cx;)V
    .locals 5

    .prologue
    .line 442434
    instance-of v0, p1, LX/0bb;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 442435
    check-cast v0, LX/0bb;

    .line 442436
    sget-object v1, LX/3FC;->PRESENCE_STAYED_THE_SAME:LX/3FC;

    .line 442437
    invoke-virtual {v1}, LX/3FC;->createHoneyClientEvent()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 442438
    const-string v3, "current_presence"

    invoke-virtual {v0}, LX/0bb;->d()LX/0am;

    move-result-object v4

    invoke-static {v4}, LX/2cw;->a(LX/0am;)LX/0lF;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442439
    invoke-static {p0, v1, v2, p2}, LX/2cw;->a(LX/2cw;LX/3FC;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/2cx;)V

    .line 442440
    :goto_0
    invoke-interface {p1}, LX/0bZ;->d()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442441
    iget-object v1, p0, LX/2cw;->d:LX/0bW;

    const-string v2, "Confidence Level: %s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1}, LX/0bZ;->d()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->q()LX/Cdj;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, LX/0bW;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 442442
    :cond_0
    return-void

    .line 442443
    :cond_1
    instance-of v0, p1, LX/0bX;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 442444
    check-cast v0, LX/0bX;

    .line 442445
    sget-object v1, LX/3FC;->PRESENCE_CHANGED:LX/3FC;

    .line 442446
    invoke-virtual {v1}, LX/3FC;->createHoneyClientEvent()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 442447
    const-string v3, "previous_presence"

    .line 442448
    iget-object v4, v0, LX/0bX;->a:LX/0am;

    move-object v4, v4

    .line 442449
    invoke-static {v4}, LX/2cw;->a(LX/0am;)LX/0lF;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442450
    const-string v3, "current_presence"

    .line 442451
    iget-object v4, v0, LX/0bX;->b:LX/0am;

    move-object v4, v4

    .line 442452
    invoke-static {v4}, LX/2cw;->a(LX/0am;)LX/0lF;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442453
    invoke-static {p0, v1, v2, p2}, LX/2cw;->a(LX/2cw;LX/3FC;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/2cx;)V

    .line 442454
    goto :goto_0

    .line 442455
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown PlaceTipsPresenceEvent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/2cx;)V
    .locals 2

    .prologue
    .line 442430
    sget-object v0, LX/3FC;->END_PAGE_LOOKUP_SUCCESS:LX/3FC;

    .line 442431
    invoke-virtual {v0}, LX/3FC;->createHoneyClientEvent()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 442432
    invoke-static {p0, v0, v1, p1}, LX/2cw;->a(LX/2cw;LX/3FC;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/2cx;)V

    .line 442433
    return-void
.end method

.method public final a(LX/3FC;LX/2cx;)V
    .locals 1

    .prologue
    .line 442428
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/2cw;->a(LX/3FC;LX/2cx;Ljava/lang/Throwable;)V

    .line 442429
    return-void
.end method

.method public final a(LX/3FC;LX/2cx;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 442423
    invoke-virtual {p1}, LX/3FC;->createHoneyClientEvent()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 442424
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442425
    const-string v1, "has_question"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442426
    invoke-static {p0, p1, v0, p2}, LX/2cw;->a(LX/2cw;LX/3FC;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/2cx;)V

    .line 442427
    return-void
.end method

.method public final a(LX/3FC;LX/2cx;Ljava/lang/Throwable;)V
    .locals 5
    .param p3    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 442412
    invoke-virtual {p1}, LX/3FC;->createHoneyClientEvent()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 442413
    if-eqz p3, :cond_0

    .line 442414
    const-string v1, "fail_exception"

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442415
    const-string v1, "fail_exception_message"

    invoke-virtual {p3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442416
    const-string v1, "fail_exception_stack_trace"

    invoke-static {p3}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 442417
    :cond_0
    invoke-static {p0, p1, v0, p2}, LX/2cw;->a(LX/2cw;LX/3FC;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/2cx;)V

    .line 442418
    if-eqz p3, :cond_1

    .line 442419
    instance-of v0, p3, LX/CeF;

    if-eqz v0, :cond_2

    .line 442420
    iget-object v0, p0, LX/2cw;->d:LX/0bW;

    const-string v1, "%s event included throwable: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {p3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-interface {v0, v1, v2}, LX/0bW;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 442421
    :cond_1
    :goto_0
    return-void

    .line 442422
    :cond_2
    iget-object v0, p0, LX/2cw;->d:LX/0bW;

    const-string v1, "%s event included throwable"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-interface {v0, p3, v1, v2}, LX/0bW;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
