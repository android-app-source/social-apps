.class public LX/3My;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3MV;

.field private final b:LX/3Mx;

.field private final c:LX/3Mz;

.field private final d:LX/3N1;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3MV;LX/3Mx;LX/3Mz;LX/3N1;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3MV;",
            "LX/3Mx;",
            "LX/3Mz;",
            "LX/3N1;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 557350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 557351
    iput-object p1, p0, LX/3My;->a:LX/3MV;

    .line 557352
    iput-object p2, p0, LX/3My;->b:LX/3Mx;

    .line 557353
    iput-object p3, p0, LX/3My;->c:LX/3Mz;

    .line 557354
    iput-object p4, p0, LX/3My;->d:LX/3N1;

    .line 557355
    iput-object p5, p0, LX/3My;->e:LX/0Ot;

    .line 557356
    iput-object p6, p0, LX/3My;->f:LX/0Ot;

    .line 557357
    return-void
.end method

.method private static a(LX/0Px;)LX/0Px;
    .locals 7
    .param p0    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ProfileRangesModel$RangesModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/ProfileRange;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 557341
    if-nez p0, :cond_0

    .line 557342
    const/4 v0, 0x0

    .line 557343
    :goto_0
    return-object v0

    .line 557344
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 557345
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ProfileRangesModel$RangesModel;

    .line 557346
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ProfileRangesModel$RangesModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ProfileRangesModel$RangesModel$EntityModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ProfileRangesModel$RangesModel$EntityModel;->j()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 557347
    new-instance v4, Lcom/facebook/messaging/model/messages/ProfileRange;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ProfileRangesModel$RangesModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ProfileRangesModel$RangesModel$EntityModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ProfileRangesModel$RangesModel$EntityModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ProfileRangesModel$RangesModel;->k()I

    move-result v6

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ProfileRangesModel$RangesModel;->j()I

    move-result v0

    invoke-direct {v4, v5, v6, v0}, Lcom/facebook/messaging/model/messages/ProfileRange;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 557348
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 557349
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/2uF;)LX/0Px;
    .locals 6
    .param p0    # LX/2uF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2uF;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 556994
    if-nez p0, :cond_0

    .line 556995
    const/4 v0, 0x0

    .line 556996
    :goto_0
    return-object v0

    .line 556997
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 556998
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 556999
    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    .line 557000
    if-eqz v4, :cond_1

    .line 557001
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5}, LX/15i;->o(II)LX/22e;

    move-result-object v0

    .line 557002
    new-instance v3, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;

    if-eqz v0, :cond_2

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_2
    invoke-direct {v3, v4, v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 557003
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 557004
    goto :goto_2

    .line 557005
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel$VotersModel;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel$VotersModel;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 557358
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 557359
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel$VotersModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 557360
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel$VotersModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 557361
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel$VotersModel$VotersNodesModel;

    .line 557362
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel$VotersModel$VotersNodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 557363
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 557364
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/3My;
    .locals 1

    .prologue
    .line 557378
    invoke-static {p0}, LX/3My;->b(LX/0QB;)LX/3My;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;)Lcom/facebook/messaging/model/attachment/AttachmentImageMap;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 557365
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 557366
    const/4 v0, 0x0

    .line 557367
    :goto_0
    return-object v0

    .line 557368
    :cond_0
    invoke-static {}, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->newBuilder()LX/5dP;

    move-result-object v0

    .line 557369
    if-eqz p0, :cond_1

    .line 557370
    sget-object v1, LX/5dQ;->FULL_SCREEN:LX/5dQ;

    invoke-static {p0}, LX/3My;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;)Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5dP;->a(LX/5dQ;Lcom/facebook/messaging/model/attachment/ImageUrl;)LX/5dP;

    .line 557371
    :cond_1
    if-eqz p1, :cond_2

    .line 557372
    sget-object v1, LX/5dQ;->SMALL_PREVIEW:LX/5dQ;

    invoke-static {p1}, LX/3My;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;)Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5dP;->a(LX/5dQ;Lcom/facebook/messaging/model/attachment/ImageUrl;)LX/5dP;

    .line 557373
    :cond_2
    if-eqz p2, :cond_3

    .line 557374
    sget-object v1, LX/5dQ;->MEDIUM_PREVIEW:LX/5dQ;

    invoke-static {p2}, LX/3My;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;)Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5dP;->a(LX/5dQ;Lcom/facebook/messaging/model/attachment/ImageUrl;)LX/5dP;

    .line 557375
    :cond_3
    if-eqz p3, :cond_4

    .line 557376
    sget-object v1, LX/5dQ;->LARGE_PREVIEW:LX/5dQ;

    invoke-static {p3}, LX/3My;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;)Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5dP;->a(LX/5dQ;Lcom/facebook/messaging/model/attachment/ImageUrl;)LX/5dP;

    .line 557377
    :cond_4
    invoke-virtual {v0}, LX/5dP;->b()Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;)Lcom/facebook/messaging/model/attachment/ImageUrl;
    .locals 2

    .prologue
    .line 557436
    if-nez p0, :cond_0

    .line 557437
    const/4 v0, 0x0

    .line 557438
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/5dM;

    invoke-direct {v0}, LX/5dM;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;->j()I

    move-result v1

    .line 557439
    iput v1, v0, LX/5dM;->b:I

    .line 557440
    move-object v0, v0

    .line 557441
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;->k()I

    move-result v1

    .line 557442
    iput v1, v0, LX/5dM;->a:I

    .line 557443
    move-object v0, v0

    .line 557444
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 557445
    iput-object v1, v0, LX/5dM;->c:Ljava/lang/String;

    .line 557446
    move-object v0, v0

    .line 557447
    invoke-virtual {v0}, LX/5dM;->d()Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;)Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 557379
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_POLL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 557380
    const/4 v2, 0x0

    .line 557381
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->I()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel;

    move-result-object v0

    if-nez v0, :cond_8

    .line 557382
    :cond_0
    const/4 v0, 0x0

    .line 557383
    :goto_0
    move-object v0, v0

    .line 557384
    :goto_1
    return-object v0

    .line 557385
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->INSTANT_GAME_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 557386
    invoke-static {p1}, LX/3My;->e(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;)Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;

    move-result-object v0

    goto :goto_1

    .line 557387
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MEDIA_SUBSCRIPTION_MANAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 557388
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 557389
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->H()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$PageModel;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->H()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$PageModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 557390
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->H()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$PageModel;

    move-result-object v3

    if-eqz v3, :cond_10

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->H()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$PageModel;->k()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-eqz v3, :cond_f

    const/4 v3, 0x1

    :goto_3
    if-eqz v3, :cond_3

    .line 557391
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->H()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$PageModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 557392
    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 557393
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->q()LX/0Px;

    move-result-object v3

    .line 557394
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 557395
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v4, 0x0

    move v5, v4

    :goto_4
    if-ge v5, v7, :cond_4

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    .line 557396
    invoke-static {v4}, LX/5Ta;->a(Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 557397
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_4

    .line 557398
    :cond_4
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v3, v4

    .line 557399
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->n()Ljava/lang/String;

    move-result-object v4

    .line 557400
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->C()Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    move-result-object v5

    .line 557401
    new-instance v6, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;)V

    move-object v1, v6

    .line 557402
    move-object v0, v1

    .line 557403
    goto/16 :goto_1

    .line 557404
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_EXTENSION_ADD_CART:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 557405
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->l()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    move-result-object v0

    if-nez v0, :cond_11

    .line 557406
    const/4 v0, 0x0

    .line 557407
    :goto_5
    move-object v0, v0

    .line 557408
    goto/16 :goto_1

    .line 557409
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_EXTENSION_ADD_FAVORITE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 557410
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->u()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    move-result-object v0

    if-nez v0, :cond_12

    .line 557411
    const/4 v0, 0x0

    .line 557412
    :goto_6
    move-object v0, v0

    .line 557413
    goto/16 :goto_1

    .line 557414
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 557415
    :cond_8
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->I()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel;->j()Ljava/lang/String;

    move-result-object v4

    .line 557416
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->I()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel;->l()Ljava/lang/String;

    move-result-object v5

    .line 557417
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->G()I

    move-result v6

    .line 557418
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->I()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel;->m()Z

    move-result v7

    .line 557419
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->I()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel;

    move-result-object v0

    .line 557420
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 557421
    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 557422
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v3, v2

    :goto_7
    if-ge v3, v10, :cond_d

    invoke-virtual {v9, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel;

    .line 557423
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_b

    .line 557424
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel;->a()LX/1vs;

    move-result-object v1

    iget-object p0, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 557425
    invoke-virtual {p0, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const/4 v1, 0x1

    :goto_8
    if-eqz v1, :cond_9

    .line 557426
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel;->a()LX/1vs;

    move-result-object v1

    iget-object p0, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {p0, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object p0

    .line 557427
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel$VotersModel;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel$VotersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel$VotersModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel$VotersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel$VotersModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 557428
    :goto_9
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel$VotersModel;

    move-result-object v0

    invoke-static {v0}, LX/3My;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$QuestionModel$OptionsModel$NodesModel$VotersModel;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v1, v0}, LX/6ex;->a(Ljava/lang/String;ILX/0Px;)LX/6ex;

    move-result-object v0

    .line 557429
    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 557430
    :cond_9
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_7

    :cond_a
    move v1, v2

    .line 557431
    goto :goto_8

    :cond_b
    move v1, v2

    goto :goto_8

    :cond_c
    move v1, v2

    .line 557432
    goto :goto_9

    .line 557433
    :cond_d
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v4, v5, v6, v7, v0}, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->a(Ljava/lang/String;Ljava/lang/String;IZLX/0Px;)Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;

    move-result-object v0

    goto/16 :goto_0

    :cond_e
    move-object v1, v2

    .line 557434
    goto/16 :goto_2

    :cond_f
    move v3, v4

    .line 557435
    goto/16 :goto_3

    :cond_10
    move v3, v4

    goto/16 :goto_3

    :cond_11
    new-instance v0, Lcom/facebook/messaging/model/messages/MessengerCartInfoProperties;

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->m()I

    move-result v1

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->l()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    move-result-object v2

    invoke-static {v2}, LX/5Ta;->a(Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/model/messages/MessengerCartInfoProperties;-><init>(ILcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)V

    goto/16 :goto_5

    :cond_12
    new-instance v0, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->u()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    move-result-object v2

    invoke-static {v2}, LX/5Ta;->a(Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;-><init>(Ljava/lang/String;Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)V

    goto/16 :goto_6
.end method

.method private static a(Ljava/lang/String;LX/0Px;)Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;",
            ">;)",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 557333
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 557334
    const/4 v0, 0x0

    .line 557335
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    if-eqz p1, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;->OFFSITE_AD:Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    invoke-virtual {p1, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v1, p0, v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;-><init>(Ljava/lang/String;Z)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(LX/5dN;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;)V
    .locals 3

    .prologue
    .line 557336
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->G()Z

    move-result v0

    .line 557337
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->y()Ljava/lang/String;

    move-result-object v1

    .line 557338
    new-instance v2, Lcom/facebook/messaging/model/attachment/AudioData;

    invoke-direct {v2, v0, v1}, Lcom/facebook/messaging/model/attachment/AudioData;-><init>(ZLjava/lang/String;)V

    .line 557339
    iput-object v2, p0, LX/5dN;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    .line 557340
    return-void
.end method

.method private static a(LX/5dN;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;LX/FO9;)V
    .locals 17

    .prologue
    .line 557299
    const/4 v14, 0x0

    .line 557300
    const/4 v13, 0x0

    .line 557301
    const/4 v12, 0x0

    .line 557302
    const/4 v11, 0x0

    .line 557303
    const/4 v10, 0x0

    .line 557304
    sget-object v2, LX/FO9;->ANIMATED_IMAGE:LX/FO9;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_0

    .line 557305
    sget-object v9, LX/5dT;->NONQUICKCAM:LX/5dT;

    .line 557306
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->m()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel$AnimatedImageOriginalDimensionsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel$AnimatedImageOriginalDimensionsModel;->a()D

    move-result-wide v2

    double-to-int v7, v2

    .line 557307
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->m()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel$AnimatedImageOriginalDimensionsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel$AnimatedImageOriginalDimensionsModel;->j()D

    move-result-wide v2

    double-to-int v6, v2

    .line 557308
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->n()Z

    move-result v8

    .line 557309
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->p()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v5

    .line 557310
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->s()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v4

    .line 557311
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->r()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v3

    .line 557312
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->q()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v2

    .line 557313
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v13

    .line 557314
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->o()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v12

    .line 557315
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v11

    .line 557316
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v10

    move-object v15, v13

    move-object v13, v11

    move-object v11, v5

    move-object v5, v2

    move/from16 v16, v6

    move-object v6, v3

    move v3, v7

    move-object v7, v9

    move-object v9, v14

    move-object v14, v12

    move-object v12, v10

    move-object v10, v4

    move/from16 v4, v16

    .line 557317
    :goto_0
    new-instance v2, Lcom/facebook/messaging/model/attachment/ImageData;

    invoke-static {v11, v10, v6, v5}, LX/3My;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;)Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    move-result-object v5

    invoke-static {v15, v14, v13, v12}, LX/3My;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;)Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    move-result-object v6

    invoke-direct/range {v2 .. v9}, Lcom/facebook/messaging/model/attachment/ImageData;-><init>(IILcom/facebook/messaging/model/attachment/AttachmentImageMap;Lcom/facebook/messaging/model/attachment/AttachmentImageMap;LX/5dT;ZLjava/lang/String;)V

    .line 557318
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/5dN;->a(Lcom/facebook/messaging/model/attachment/ImageData;)LX/5dN;

    .line 557319
    return-void

    .line 557320
    :cond_0
    sget-object v2, LX/FO9;->REGULAR_IMAGE:LX/FO9;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_2

    .line 557321
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->F()Lcom/facebook/graphql/enums/GraphQLMessageImageType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessageImageType;->MESSENGER_CAM:Lcom/facebook/graphql/enums/GraphQLMessageImageType;

    if-ne v2, v3, :cond_1

    .line 557322
    sget-object v2, LX/5dT;->QUICKCAM:LX/5dT;

    .line 557323
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->K()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel$OriginalDimensionsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel$OriginalDimensionsModel;->a()D

    move-result-wide v4

    double-to-int v9, v4

    .line 557324
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->K()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel$OriginalDimensionsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel$OriginalDimensionsModel;->j()D

    move-result-wide v4

    double-to-int v7, v4

    .line 557325
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->J()Ljava/lang/String;

    move-result-object v14

    .line 557326
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->M()Z

    move-result v8

    .line 557327
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->B()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v6

    .line 557328
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->E()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v5

    .line 557329
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->D()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v4

    .line 557330
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->C()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v3

    move-object v15, v13

    move-object v13, v11

    move-object v11, v6

    move-object v6, v4

    move v4, v7

    move-object v7, v2

    move/from16 v16, v9

    move-object v9, v14

    move-object v14, v12

    move-object v12, v10

    move-object v10, v5

    move-object v5, v3

    move/from16 v3, v16

    goto :goto_0

    .line 557331
    :cond_1
    sget-object v2, LX/5dT;->NONQUICKCAM:LX/5dT;

    goto :goto_1

    .line 557332
    :cond_2
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unsupported image attachment type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private static a(LX/6f7;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 557293
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->I()Ljava/lang/String;

    move-result-object v0

    .line 557294
    iput-object v0, p0, LX/6f7;->f:Ljava/lang/String;

    .line 557295
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->D()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 557296
    new-instance v0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->D()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-wide v4, v2

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/model/payment/PaymentTransactionData;-><init>(Ljava/lang/String;JJILjava/lang/String;)V

    .line 557297
    iput-object v0, p0, LX/6f7;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 557298
    :cond_0
    return-void
.end method

.method private static b(LX/2uF;)LX/0Px;
    .locals 13
    .param p0    # LX/2uF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2uF;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v7, 0x1

    const/4 v12, 0x2

    const/4 v8, 0x0

    .line 557280
    if-nez p0, :cond_0

    .line 557281
    :goto_0
    return-object v9

    .line 557282
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 557283
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v11}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v6, v0, LX/1vs;->b:I

    .line 557284
    invoke-virtual {v1, v6, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 557285
    const/4 v0, 0x3

    invoke-virtual {v1, v6, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    .line 557286
    invoke-virtual {v1, v6, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 557287
    invoke-virtual {v1, v6, v12}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1, v6, v12}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v1, v0, v8}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v7

    :goto_2
    if-eqz v0, :cond_3

    .line 557288
    invoke-virtual {v1, v6, v12}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v1, v0, v8}, LX/15i;->g(II)I

    move-result v0

    .line 557289
    invoke-virtual {v1, v0, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 557290
    :goto_3
    new-instance v1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;

    invoke-direct/range {v1 .. v6}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    :cond_1
    move v0, v8

    .line 557291
    goto :goto_2

    :cond_2
    move v0, v8

    goto :goto_2

    :cond_3
    move-object v6, v9

    goto :goto_3

    .line 557292
    :cond_4
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/3My;
    .locals 7

    .prologue
    .line 557278
    new-instance v0, LX/3My;

    invoke-static {p0}, LX/3MV;->a(LX/0QB;)LX/3MV;

    move-result-object v1

    check-cast v1, LX/3MV;

    invoke-static {p0}, LX/3Mx;->b(LX/0QB;)LX/3Mx;

    move-result-object v2

    check-cast v2, LX/3Mx;

    invoke-static {p0}, LX/3Mz;->b(LX/0QB;)LX/3Mz;

    move-result-object v3

    check-cast v3, LX/3Mz;

    invoke-static {p0}, LX/3N1;->b(LX/0QB;)LX/3N1;

    move-result-object v4

    check-cast v4, LX/3N1;

    const/16 v5, 0x2ba

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x259

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/3My;-><init>(LX/3MV;LX/3Mx;LX/3Mz;LX/3N1;LX/0Ot;LX/0Ot;)V

    .line 557279
    return-object v0
.end method

.method private b(LX/5dN;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;)V
    .locals 10

    .prologue
    .line 557247
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->O()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$StreamingImageThumbnailModel;

    move-result-object v0

    .line 557248
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->u()Ljava/lang/String;

    move-result-object v6

    .line 557249
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->K()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel$OriginalDimensionsModel;

    move-result-object v2

    .line 557250
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->L()I

    move-result v4

    .line 557251
    if-eqz v0, :cond_0

    if-eqz v6, :cond_0

    if-eqz v2, :cond_0

    if-gtz v4, :cond_1

    .line 557252
    :cond_0
    iget-object v0, p0, LX/3My;->b:LX/3Mx;

    .line 557253
    iget-object v1, v0, LX/3Mx;->a:LX/03V;

    const-string v2, "graphql_video_incomplete_model"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Got an incomplete video attachment model. streamingImageThumbnail="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->O()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$StreamingImageThumbnailModel;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", attachmentVideoUrl="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", originalDimensions="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->K()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel$OriginalDimensionsModel;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", playableDurationInMs="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->L()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 557254
    :goto_0
    return-void

    .line 557255
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->P()Lcom/facebook/graphql/enums/GraphQLMessageVideoType;

    move-result-object v1

    .line 557256
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessageVideoType;->FILE_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLMessageVideoType;

    if-eq v1, v3, :cond_5

    .line 557257
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessageVideoType;->RECORDED_VIDEO:Lcom/facebook/graphql/enums/GraphQLMessageVideoType;

    if-ne v1, v3, :cond_2

    .line 557258
    sget-object v5, LX/5dX;->QUICKCAM:LX/5dX;

    .line 557259
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel$OriginalDimensionsModel;->a()D

    move-result-wide v8

    double-to-int v1, v8

    .line 557260
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel$OriginalDimensionsModel;->j()D

    move-result-wide v2

    double-to-int v2, v2

    .line 557261
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->N()I

    move-result v3

    .line 557262
    div-int/lit16 v4, v4, 0x3e8

    .line 557263
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 557264
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$StreamingImageThumbnailModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 557265
    new-instance v0, Lcom/facebook/messaging/model/attachment/VideoData;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/model/attachment/VideoData;-><init>(IIIILX/5dX;Landroid/net/Uri;Landroid/net/Uri;)V

    .line 557266
    iput-object v0, p1, LX/5dN;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 557267
    goto :goto_0

    .line 557268
    :cond_2
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessageVideoType;->RECORDED_STICKER:Lcom/facebook/graphql/enums/GraphQLMessageVideoType;

    if-ne v1, v3, :cond_3

    .line 557269
    sget-object v5, LX/5dX;->VIDEO_STICKER:LX/5dX;

    goto :goto_1

    .line 557270
    :cond_3
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessageVideoType;->VIDEO_MAIL:Lcom/facebook/graphql/enums/GraphQLMessageVideoType;

    if-ne v1, v3, :cond_4

    .line 557271
    sget-object v5, LX/5dX;->VIDEO_MAIL:LX/5dX;

    goto :goto_1

    .line 557272
    :cond_4
    if-nez v1, :cond_6

    .line 557273
    iget-object v1, p0, LX/3My;->b:LX/3Mx;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Graphql type of video attachment is null: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->t()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/3Mx;->a(Ljava/lang/String;)V

    .line 557274
    :cond_5
    :goto_2
    sget-object v5, LX/5dX;->VIDEO_ATTACHMENT:LX/5dX;

    goto :goto_1

    .line 557275
    :cond_6
    iget-object v3, p0, LX/3My;->b:LX/3Mx;

    .line 557276
    iget-object v5, v3, LX/3Mx;->a:LX/03V;

    const-string v7, "graphql_video_type_null"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Graphql type of video attachment is null: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLMessageVideoType;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 557277
    goto :goto_2
.end method

.method private static e(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;)Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;
    .locals 16

    .prologue
    .line 557206
    if-nez p0, :cond_0

    .line 557207
    const/4 v0, 0x0

    .line 557208
    :goto_0
    return-object v0

    .line 557209
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->name()Ljava/lang/String;

    move-result-object v1

    .line 557210
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->Q()Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 557211
    sget-object v0, LX/FO8;->a:[I

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->Q()Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 557212
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->name()Ljava/lang/String;

    move-result-object v1

    .line 557213
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->K()I

    move-result v10

    .line 557214
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->o()Ljava/lang/String;

    move-result-object v6

    .line 557215
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->t()Ljava/lang/String;

    move-result-object v7

    .line 557216
    const/4 v0, 0x0

    .line 557217
    const/4 v2, 0x0

    .line 557218
    const/4 v3, 0x0

    .line 557219
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->w()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$GameModel;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 557220
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->w()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$GameModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$GameModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 557221
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->w()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$GameModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$GameModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 557222
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->w()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$GameModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$GameModel;->k()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_2

    .line 557223
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->w()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$GameModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$GameModel;->k()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 557224
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->B()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$LeaderboardModel;

    move-result-object v4

    .line 557225
    const/4 v8, 0x1

    .line 557226
    const/4 v5, 0x0

    .line 557227
    if-eqz v4, :cond_6

    .line 557228
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 557229
    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$LeaderboardModel;->j()LX/0Px;

    move-result-object v12

    .line 557230
    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v13

    const/4 v4, 0x0

    move v9, v4

    :goto_2
    if-ge v9, v13, :cond_5

    invoke-virtual {v12, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$LeaderboardModel$LeaderboardEntriesModel;

    .line 557231
    if-eqz v4, :cond_7

    .line 557232
    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$LeaderboardModel$LeaderboardEntriesModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 557233
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 557234
    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$LeaderboardModel$LeaderboardEntriesModel;->j()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 557235
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$LeaderboardModel$LeaderboardEntriesModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$LeaderboardModel$LeaderboardEntriesModel$UserModel;

    move-result-object v4

    .line 557236
    if-eqz v4, :cond_7

    .line 557237
    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$LeaderboardModel$LeaderboardEntriesModel$UserModel;->j()Ljava/lang/String;

    move-result-object v14

    .line 557238
    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$LeaderboardModel$LeaderboardEntriesModel$UserModel;->k()Ljava/lang/String;

    move-result-object v15

    .line 557239
    add-int/lit8 v4, v8, 0x1

    .line 557240
    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v14, v15, v5, v8}, LX/6ez;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6ez;

    move-result-object v5

    .line 557241
    if-eqz v5, :cond_4

    .line 557242
    invoke-virtual {v11, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 557243
    :cond_4
    :goto_3
    add-int/lit8 v5, v9, 0x1

    move v9, v5

    move v8, v4

    goto :goto_2

    .line 557244
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->Q()Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->name()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 557245
    :cond_5
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 557246
    :cond_6
    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {v0 .. v7}, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    move v4, v8

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(ILcom/facebook/user/model/User;Ljava/lang/String;ZLcom/facebook/graphql/enums/GraphQLPageAdminReplyType;Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentMessageType;)LX/2uW;
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentMessageType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 557166
    sparse-switch p1, :sswitch_data_0

    .line 557167
    iget-object v0, p0, LX/3My;->b:LX/3Mx;

    invoke-virtual {v0, p1}, LX/3Mx;->a(I)V

    .line 557168
    sget-object v0, LX/2uW;->UNKNOWN:LX/2uW;

    :goto_0
    return-object v0

    .line 557169
    :sswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->COMMERCE_LINK:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    if-ne p5, v0, :cond_0

    .line 557170
    sget-object v0, LX/2uW;->COMMERCE_LINK:LX/2uW;

    goto :goto_0

    .line 557171
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->COMMERCE_UNLINK:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    if-ne p5, v0, :cond_1

    .line 557172
    sget-object v0, LX/2uW;->COMMERCE_UNLINK:LX/2uW;

    goto :goto_0

    .line 557173
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->ACTIVITY_REPLY:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    if-ne p5, v0, :cond_2

    .line 557174
    sget-object v0, LX/2uW;->ACTIVITY_REPLY:LX/2uW;

    goto :goto_0

    .line 557175
    :cond_2
    iget-object v0, p0, LX/3My;->b:LX/3Mx;

    invoke-virtual {v0, p1}, LX/3Mx;->a(I)V

    .line 557176
    sget-object v0, LX/2uW;->UNKNOWN:LX/2uW;

    goto :goto_0

    .line 557177
    :sswitch_1
    sget-object v0, LX/2uW;->REGULAR:LX/2uW;

    goto :goto_0

    .line 557178
    :sswitch_2
    sget-object v0, LX/2uW;->SET_NAME:LX/2uW;

    goto :goto_0

    .line 557179
    :sswitch_3
    sget-object v0, LX/2uW;->SET_IMAGE:LX/2uW;

    goto :goto_0

    .line 557180
    :sswitch_4
    sget-object v0, LX/2uW;->ADD_MEMBERS:LX/2uW;

    goto :goto_0

    .line 557181
    :sswitch_5
    sget-object v0, LX/2uW;->REMOVE_MEMBERS:LX/2uW;

    goto :goto_0

    .line 557182
    :sswitch_6
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 557183
    iget-object v1, p2, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 557184
    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 557185
    sget-object p0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p0, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    .line 557186
    sget-object p0, LX/2uW;->MISSED_CALL:LX/2uW;

    .line 557187
    :goto_1
    move-object v0, p0

    .line 557188
    goto :goto_0

    .line 557189
    :sswitch_7
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 557190
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 557191
    sget-object v1, LX/2uW;->VIDEO_CALL:LX/2uW;

    .line 557192
    :goto_2
    move-object v0, v1

    .line 557193
    goto :goto_0

    .line 557194
    :sswitch_8
    if-nez p6, :cond_6

    .line 557195
    sget-object v0, LX/2uW;->P2P_PAYMENT:LX/2uW;

    .line 557196
    :goto_3
    move-object v0, v0

    .line 557197
    goto :goto_0

    .line 557198
    :sswitch_9
    sget-object v0, LX/2uW;->ADMIN:LX/2uW;

    goto :goto_0

    .line 557199
    :cond_3
    if-eqz v1, :cond_4

    .line 557200
    sget-object p0, LX/2uW;->OUTGOING_CALL:LX/2uW;

    goto :goto_1

    .line 557201
    :cond_4
    sget-object p0, LX/2uW;->INCOMING_CALL:LX/2uW;

    goto :goto_1

    :cond_5
    sget-object v1, LX/2uW;->MISSED_VIDEO_CALL:LX/2uW;

    goto :goto_2

    .line 557202
    :cond_6
    sget-object v0, LX/FO8;->b:[I

    invoke-virtual {p6}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentMessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 557203
    sget-object v0, LX/2uW;->P2P_PAYMENT:LX/2uW;

    goto :goto_3

    .line 557204
    :pswitch_0
    sget-object v0, LX/2uW;->P2P_PAYMENT_GROUP:LX/2uW;

    goto :goto_3

    .line 557205
    :pswitch_1
    sget-object v0, LX/2uW;->P2P_PAYMENT_CANCELED:LX/2uW;

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        -0x75a97664 -> :sswitch_1
        -0x4e7dd713 -> :sswitch_5
        -0x464e6939 -> :sswitch_4
        -0x2ff5db92 -> :sswitch_7
        -0xdfe04de -> :sswitch_9
        0x19fce96f -> :sswitch_8
        0x2a8d0f57 -> :sswitch_6
        0x42deca72 -> :sswitch_2
        0x51fd2176 -> :sswitch_3
        0x6ac03cea -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/messages/Message;
    .locals 28

    .prologue
    .line 557016
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-nez v2, :cond_0

    .line 557017
    new-instance v2, Ljava/security/InvalidParameterException;

    const-string v3, "Invalid input model"

    invoke-direct {v2, v3}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 557018
    :cond_0
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v23

    .line 557019
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3My;->a:LX/3MV;

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->y()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v24

    .line 557020
    invoke-virtual/range {v23 .. v24}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6f7;

    .line 557021
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->j()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->k()Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_1
    const/4 v6, 0x1

    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->H()Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->C()Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentMessageType;

    move-result-object v8

    move-object/from16 v2, p0

    move-object/from16 v4, p3

    invoke-virtual/range {v2 .. v8}, LX/3My;->a(ILcom/facebook/user/model/User;Ljava/lang/String;ZLcom/facebook/graphql/enums/GraphQLPageAdminReplyType;Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentMessageType;)LX/2uW;

    move-result-object v22

    .line 557022
    sget-object v2, LX/2uW;->P2P_PAYMENT:LX/2uW;

    move-object/from16 v0, v22

    if-eq v0, v2, :cond_2

    sget-object v2, LX/2uW;->P2P_PAYMENT_CANCELED:LX/2uW;

    move-object/from16 v0, v22

    if-eq v0, v2, :cond_2

    sget-object v2, LX/2uW;->P2P_PAYMENT_GROUP:LX/2uW;

    move-object/from16 v0, v22

    if-ne v0, v2, :cond_a

    .line 557023
    :cond_2
    move-object/from16 v0, v23

    move-object/from16 v1, p2

    invoke-static {v0, v1}, LX/3My;->a(LX/6f7;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;)V

    .line 557024
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x2a8d0f57

    if-ne v2, v3, :cond_12

    .line 557025
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_11

    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    :goto_2
    move-object/from16 v0, p2

    invoke-static {v0, v2}, LX/6hU;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;Ljava/lang/String;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v2

    .line 557026
    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)LX/6f7;

    .line 557027
    sget-object v2, LX/2uW;->CALL_LOG:LX/2uW;

    .line 557028
    :goto_3
    sget-object v3, LX/2uW;->UNKNOWN:LX/2uW;

    if-ne v2, v3, :cond_3

    .line 557029
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->u()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 557030
    sget-object v2, LX/2uW;->REGULAR:LX/2uW;

    .line 557031
    :cond_3
    :goto_4
    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->a(LX/2uW;)LX/6f7;

    .line 557032
    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, LX/6f7;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/6f7;

    .line 557033
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->w()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    .line 557034
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->N()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, LX/6f7;->a(J)LX/6f7;

    .line 557035
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->B()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->d(Ljava/lang/String;)LX/6f7;

    .line 557036
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->z()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-nez v2, :cond_15

    const/4 v2, 0x0

    .line 557037
    :goto_5
    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->e(Ljava/lang/String;)LX/6f7;

    .line 557038
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->O()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->a(Ljava/lang/Integer;)LX/6f7;

    .line 557039
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->M()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/Do8;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->a(Ljava/util/Map;)LX/6f7;

    .line 557040
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->t()Z

    move-result v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->d(Z)LX/6f7;

    .line 557041
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->m()Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->m()Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    :cond_4
    const/4 v2, 0x0

    :goto_6
    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->f(Ljava/lang/String;)LX/6f7;

    .line 557042
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->n()LX/2uF;

    move-result-object v2

    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 557043
    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    const-string v6, "border"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    const-string v4, "flowers"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 557044
    const/4 v2, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->e(Z)LX/6f7;

    .line 557045
    :cond_6
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->A()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-nez v2, :cond_17

    const/4 v2, 0x0

    .line 557046
    :goto_7
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 557047
    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->g(Ljava/lang/String;)LX/6f7;

    .line 557048
    :cond_7
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->G()Ljava/lang/String;

    move-result-object v4

    .line 557049
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 557050
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/user/model/User;->d()Lcom/facebook/user/model/UserKey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    .line 557051
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    .line 557052
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3My;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/3My;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-static {v2, v3, v4}, LX/5dt;->a(LX/03V;LX/0lC;Ljava/lang/String;)LX/0P1;

    move-result-object v2

    .line 557053
    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->b(Ljava/util/Map;)LX/6f7;

    .line 557054
    :cond_8
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 557055
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->E()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_8
    if-ge v3, v6, :cond_18

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    .line 557056
    move-object/from16 v0, p0

    iget-object v7, v0, LX/3My;->a:LX/3MV;

    invoke-virtual {v7, v2}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v2

    .line 557057
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 557058
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_8

    .line 557059
    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 557060
    :cond_a
    sget-object v2, LX/2uW;->ADMIN:LX/2uW;

    move-object/from16 v0, v22

    if-ne v0, v2, :cond_d

    .line 557061
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->I()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->b(Ljava/lang/String;)LX/6f7;

    .line 557062
    const/16 v21, 0x0

    .line 557063
    const/16 v20, 0x0

    .line 557064
    const/16 v19, 0x0

    .line 557065
    const/16 v18, 0x0

    .line 557066
    const/16 v17, 0x0

    .line 557067
    const/16 v16, 0x0

    .line 557068
    const/4 v15, 0x0

    .line 557069
    const/4 v14, 0x0

    .line 557070
    const/4 v13, 0x0

    .line 557071
    const/4 v12, 0x0

    .line 557072
    const/4 v11, 0x0

    .line 557073
    const/4 v10, 0x0

    .line 557074
    const/4 v2, 0x0

    .line 557075
    const/4 v9, 0x0

    .line 557076
    const/4 v8, 0x0

    .line 557077
    const/4 v7, 0x0

    .line 557078
    const/4 v6, 0x0

    .line 557079
    const/4 v5, 0x0

    .line 557080
    const/4 v3, 0x0

    .line 557081
    const/4 v4, 0x0

    .line 557082
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->r()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;

    move-result-object v25

    .line 557083
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->s()Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    move-result-object v26

    .line 557084
    if-eqz v25, :cond_21

    .line 557085
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->N()Ljava/lang/String;

    move-result-object v21

    .line 557086
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->O()Ljava/lang/String;

    move-result-object v20

    .line 557087
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->E()Ljava/lang/String;

    move-result-object v19

    .line 557088
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->M()Ljava/lang/String;

    move-result-object v18

    .line 557089
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->P()I

    move-result v17

    .line 557090
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->p()LX/0Px;

    move-result-object v16

    .line 557091
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->r()LX/0Px;

    move-result-object v15

    .line 557092
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->F()LX/2uF;

    move-result-object v4

    invoke-static {v4}, LX/3My;->a(LX/2uF;)LX/0Px;

    move-result-object v14

    .line 557093
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->k()LX/2uF;

    move-result-object v4

    invoke-static {v4}, LX/3My;->b(LX/2uF;)LX/0Px;

    move-result-object v13

    .line 557094
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->s()Ljava/lang/String;

    move-result-object v12

    .line 557095
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->L()Ljava/lang/String;

    move-result-object v11

    .line 557096
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->A()Z

    move-result v10

    .line 557097
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->J()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_b

    .line 557098
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->J()LX/1vs;

    move-result-object v2

    iget-object v4, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 557099
    :cond_b
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->j()LX/0Px;

    move-result-object v5

    invoke-static {v4, v5}, LX/3My;->a(Ljava/lang/String;LX/0Px;)Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    move-result-object v9

    .line 557100
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->x()Ljava/lang/String;

    move-result-object v8

    .line 557101
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->K()I

    move-result v7

    .line 557102
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->z()Z

    move-result v6

    .line 557103
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->y()Ljava/lang/String;

    move-result-object v5

    .line 557104
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->D()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$NativeBookingRequestModel;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 557105
    invoke-virtual/range {v25 .. v25}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;->D()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$NativeBookingRequestModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel$NativeBookingRequestModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 557106
    :cond_c
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/3My;->a(Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenericAdminMessageFieldsModel$ExtensibleMessageAdminTextModel;)Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    move-result-object v4

    move-object/from16 v27, v4

    move-object v4, v5

    move v5, v6

    move v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v2

    move-object/from16 v2, v27

    .line 557107
    :goto_9
    invoke-static {}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->newBuilder()LX/6ev;

    move-result-object v25

    invoke-virtual/range {v25 .. v26}, LX/6ev;->a(Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;)LX/6ev;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/6ev;->a(Ljava/lang/String;)LX/6ev;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/6ev;->b(Ljava/lang/String;)LX/6ev;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/6ev;->c(Ljava/lang/String;)LX/6ev;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/6ev;->d(Ljava/lang/String;)LX/6ev;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/6ev;->c(I)LX/6ev;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/6ev;->a(LX/0Px;)LX/6ev;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, LX/6ev;->b(LX/0Px;)LX/6ev;

    move-result-object v15

    invoke-virtual {v15, v14}, LX/6ev;->c(LX/0Px;)LX/6ev;

    move-result-object v14

    invoke-virtual {v14, v13}, LX/6ev;->d(LX/0Px;)LX/6ev;

    move-result-object v13

    invoke-virtual {v13, v12}, LX/6ev;->e(Ljava/lang/String;)LX/6ev;

    move-result-object v12

    invoke-virtual {v12, v11}, LX/6ev;->f(Ljava/lang/String;)LX/6ev;

    move-result-object v11

    invoke-virtual {v11, v10}, LX/6ev;->a(Z)LX/6ev;

    move-result-object v10

    invoke-virtual {v10, v9}, LX/6ev;->g(Ljava/lang/String;)LX/6ev;

    move-result-object v9

    invoke-virtual {v9, v8}, LX/6ev;->a(Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;)LX/6ev;

    move-result-object v8

    invoke-virtual {v8, v7}, LX/6ev;->h(Ljava/lang/String;)LX/6ev;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/6ev;->d(I)LX/6ev;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/6ev;->b(Z)LX/6ev;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/6ev;->j(Ljava/lang/String;)LX/6ev;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/6ev;->a(Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;)LX/6ev;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/6ev;->k(Ljava/lang/String;)LX/6ev;

    move-result-object v2

    .line 557108
    invoke-virtual {v2}, LX/6ev;->a()Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;)LX/6f7;

    goto/16 :goto_1

    .line 557109
    :cond_d
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->v()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$MessageModel;

    move-result-object v2

    if-eqz v2, :cond_f

    .line 557110
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->v()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$MessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$MessageModel;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_e

    .line 557111
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->v()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$MessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$MessageModel;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->b(Ljava/lang/String;)LX/6f7;

    .line 557112
    :cond_e
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->v()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$MessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$MessageModel;->a()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/3My;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->a(LX/0Px;)LX/6f7;

    goto/16 :goto_1

    .line 557113
    :cond_f
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->I()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_10

    .line 557114
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3My;->b:LX/3Mx;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Trying to convert a message without a snippet, of type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3Mx;->a(Ljava/lang/String;)V

    .line 557115
    :cond_10
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->I()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->b(Ljava/lang/String;)LX/6f7;

    goto/16 :goto_1

    .line 557116
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 557117
    :cond_12
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, -0x2ff5db92

    if-ne v2, v3, :cond_20

    .line 557118
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_13

    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    :goto_a
    move-object/from16 v0, p2

    invoke-static {v0, v2}, LX/6hU;->b(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;Ljava/lang/String;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v2

    .line 557119
    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)LX/6f7;

    .line 557120
    sget-object v2, LX/2uW;->CALL_LOG:LX/2uW;

    goto/16 :goto_3

    .line 557121
    :cond_13
    const/4 v2, 0x0

    goto :goto_a

    .line 557122
    :cond_14
    sget-object v2, LX/2uW;->ADMIN:LX/2uW;

    goto/16 :goto_4

    .line 557123
    :cond_15
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->z()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 557124
    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 557125
    :cond_16
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->m()Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->name()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 557126
    :cond_17
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->A()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_7

    .line 557127
    :cond_18
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->F()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_b
    if-ge v3, v6, :cond_19

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    .line 557128
    move-object/from16 v0, p0

    iget-object v7, v0, LX/3My;->a:LX/3MV;

    invoke-virtual {v7, v2}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v2

    .line 557129
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 557130
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_b

    .line 557131
    :cond_19
    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, LX/6f7;->c(Ljava/util/List;)LX/6f7;

    .line 557132
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->L()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$StickerModel;

    move-result-object v2

    if-eqz v2, :cond_1a

    .line 557133
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->L()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$StickerModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$StickerModel;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, LX/6f7;->c(Ljava/lang/String;)LX/6f7;

    .line 557134
    :cond_1a
    const/4 v3, 0x0

    .line 557135
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 557136
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->l()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v4, v2

    :goto_c
    if-ge v4, v7, :cond_1d

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;

    .line 557137
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    if-nez v8, :cond_1b

    .line 557138
    move-object/from16 v0, p0

    iget-object v8, v0, LX/3My;->b:LX/3Mx;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Graphql type of attachment model is null: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, LX/3Mx;->a(Ljava/lang/String;)V

    .line 557139
    :goto_d
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_c

    .line 557140
    :cond_1b
    new-instance v8, LX/5dN;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->t()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->w()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, LX/5dN;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 557141
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->t()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/5dN;->a(Ljava/lang/String;)LX/5dN;

    .line 557142
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->I()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/5dN;->b(Ljava/lang/String;)LX/5dN;

    .line 557143
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->z()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/5dN;->c(Ljava/lang/String;)LX/5dN;

    .line 557144
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->A()I

    move-result v9

    invoke-virtual {v8, v9}, LX/5dN;->a(I)LX/5dN;

    .line 557145
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v9

    .line 557146
    sparse-switch v9, :sswitch_data_0

    .line 557147
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3My;->b:LX/3Mx;

    invoke-virtual {v2, v9}, LX/3Mx;->b(I)V

    goto :goto_d

    .line 557148
    :sswitch_0
    const-string v9, "audio/mpeg"

    invoke-virtual {v8, v9}, LX/5dN;->b(Ljava/lang/String;)LX/5dN;

    .line 557149
    invoke-static {v8, v2}, LX/3My;->a(LX/5dN;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;)V

    .line 557150
    :cond_1c
    :goto_e
    :sswitch_1
    if-nez v3, :cond_1f

    .line 557151
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3My;->d:LX/3N1;

    invoke-virtual {v3, v2}, LX/3N1;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;)Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-result-object v2

    .line 557152
    :goto_f
    invoke-virtual {v8}, LX/5dN;->m()Lcom/facebook/messaging/model/attachment/Attachment;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v2

    goto :goto_d

    .line 557153
    :sswitch_2
    sget-object v9, LX/FO9;->ANIMATED_IMAGE:LX/FO9;

    invoke-static {v8, v2, v9}, LX/3My;->a(LX/5dN;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;LX/FO9;)V

    goto :goto_e

    .line 557154
    :sswitch_3
    sget-object v9, LX/FO9;->REGULAR_IMAGE:LX/FO9;

    invoke-static {v8, v2, v9}, LX/3My;->a(LX/5dN;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;LX/FO9;)V

    .line 557155
    invoke-virtual {v8}, LX/5dN;->d()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1c

    .line 557156
    const-string v9, "image/jpeg"

    invoke-virtual {v8, v9}, LX/5dN;->b(Ljava/lang/String;)LX/5dN;

    goto :goto_e

    .line 557157
    :sswitch_4
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v2}, LX/3My;->b(LX/5dN;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel$BlobAttachmentsModel;)V

    goto :goto_e

    .line 557158
    :cond_1d
    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, LX/6f7;->a(Ljava/util/List;)LX/6f7;

    .line 557159
    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, LX/6f7;->a(Lcom/facebook/messaging/model/attribution/ContentAppAttribution;)LX/6f7;

    .line 557160
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3My;->c:LX/3Mz;

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->q()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v2, v3, v0}, LX/3Mz;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;LX/6f7;)V

    .line 557161
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v3

    .line 557162
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->x()LX/2uF;

    move-result-object v2

    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v4

    :goto_10
    invoke-interface {v4}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 557163
    const/4 v6, 0x0

    invoke-virtual {v5, v2, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    const-class v8, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageReactionsFieldModel$MessageReactionsModel$UserModel;

    invoke-virtual {v5, v2, v7, v8}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageReactionsFieldModel$MessageReactionsModel$UserModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageReactionsFieldModel$MessageReactionsModel$UserModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v2

    invoke-interface {v3, v6, v2}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_10

    .line 557164
    :cond_1e
    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, LX/6f7;->a(LX/0Xu;)LX/6f7;

    .line 557165
    invoke-virtual/range {v23 .. v23}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    return-object v2

    :cond_1f
    move-object v2, v3

    goto :goto_f

    :cond_20
    move-object/from16 v2, v22

    goto/16 :goto_3

    :cond_21
    move-object/from16 v27, v4

    move-object v4, v5

    move v5, v6

    move v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v2

    move-object/from16 v2, v27

    goto/16 :goto_9

    nop

    :sswitch_data_0
    .sparse-switch
        -0x448313d1 -> :sswitch_0
        -0x4416064c -> :sswitch_3
        -0x43609b2c -> :sswitch_4
        -0x29e0fb2f -> :sswitch_2
        0x2f58b603 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 557006
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 557007
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;

    .line 557008
    invoke-virtual {p0, p1, v0, p3}, LX/3My;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 557009
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 557010
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 557011
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->reverse()LX/0Px;

    move-result-object v2

    .line 557012
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v4, 0x20d0d098

    invoke-static {v3, v0, v4}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 557013
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v1}, LX/15i;->h(II)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 557014
    :goto_1
    new-instance v1, Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-direct {v1, p1, v2, v0}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    return-object v1

    :cond_1
    move v0, v1

    .line 557015
    goto :goto_1
.end method
