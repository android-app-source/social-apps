.class public LX/26s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/26s;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1qN;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/03V;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gd;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0lC;

.field private final e:LX/0SG;


# direct methods
.method public constructor <init>(LX/0Or;LX/03V;LX/0Ot;LX/0lC;LX/0SG;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1qN;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/0gd;",
            ">;",
            "LX/0lC;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 372367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372368
    iput-object p1, p0, LX/26s;->a:LX/0Or;

    .line 372369
    iput-object p2, p0, LX/26s;->b:LX/03V;

    .line 372370
    iput-object p3, p0, LX/26s;->c:LX/0Ot;

    .line 372371
    iput-object p4, p0, LX/26s;->d:LX/0lC;

    .line 372372
    iput-object p5, p0, LX/26s;->e:LX/0SG;

    .line 372373
    return-void
.end method

.method public static a(LX/0QB;)LX/26s;
    .locals 9

    .prologue
    .line 372374
    sget-object v0, LX/26s;->f:LX/26s;

    if-nez v0, :cond_1

    .line 372375
    const-class v1, LX/26s;

    monitor-enter v1

    .line 372376
    :try_start_0
    sget-object v0, LX/26s;->f:LX/26s;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 372377
    if-eqz v2, :cond_0

    .line 372378
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 372379
    new-instance v3, LX/26s;

    const/16 v4, 0x3cd

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    const/16 v6, 0x3bf

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lC;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-direct/range {v3 .. v8}, LX/26s;-><init>(LX/0Or;LX/03V;LX/0Ot;LX/0lC;LX/0SG;)V

    .line 372380
    move-object v0, v3

    .line 372381
    sput-object v0, LX/26s;->f:LX/26s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 372382
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 372383
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 372384
    :cond_1
    sget-object v0, LX/26s;->f:LX/26s;

    return-object v0

    .line 372385
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 372386
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/26s;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    .line 372387
    iget-object v0, p0, LX/26s;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qN;

    const/4 v2, 0x0

    .line 372388
    iget-object v1, v0, LX/1qN;->e:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->b()V

    .line 372389
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 372390
    sget-object v1, LX/1qQ;->a:LX/0U1;

    invoke-virtual {v1, p1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 372391
    iget-object v3, v0, LX/1qN;->d:LX/1qR;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "pending_story"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 372392
    iget-object v1, v0, LX/1qN;->d:LX/1qR;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v3, "pending_story"

    invoke-static {v1, v3}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v3

    .line 372393
    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-nez v1, :cond_0

    .line 372394
    iget-object v1, v0, LX/1qN;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v3, LX/0dp;->f:LX/0Tn;

    invoke-interface {v1, v3, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 372395
    :cond_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 372396
    return-object v0

    :cond_1
    move v1, v2

    .line 372397
    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/publish/common/PendingStoryPersistentData;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 372398
    iget-object v0, p0, LX/26s;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 372399
    iget-object v0, p0, LX/26s;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qN;

    invoke-virtual {v0}, LX/1qN;->d()LX/0Px;

    move-result-object v3

    .line 372400
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 372401
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v7, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7m3;

    .line 372402
    :try_start_0
    iget-object v1, p0, LX/26s;->d:LX/0lC;

    .line 372403
    new-instance v11, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    iget-object v8, v0, LX/7m3;->b:Ljava/lang/String;

    const-class v9, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v8, v9}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v9, v0, LX/7m3;->c:Ljava/lang/String;

    const-class v10, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v1, v9, v10}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    iget-object v10, v0, LX/7m3;->d:Ljava/lang/String;

    const-class v12, Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    invoke-virtual {v1, v10, v12}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    invoke-direct {v11, v8, v9, v10}, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    move-object v1, v11

    .line 372404
    iget-object v8, v1, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v8}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 372405
    iget-object v8, v1, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v8}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v0, LX/7m3;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    invoke-static {v8}, LX/0PB;->checkArgument(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 372406
    iget-object v8, v1, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v8}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->b()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    .line 372407
    sub-long v8, v4, v8

    const-wide/32 v10, 0xf731400

    cmp-long v8, v8, v10

    if-lez v8, :cond_0

    .line 372408
    iget-object v1, p0, LX/26s;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gd;

    iget-object v8, v0, LX/7m3;->a:Ljava/lang/String;

    const/16 v9, 0x15

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 372409
    iget-object v0, v0, LX/7m3;->a:Ljava/lang/String;

    invoke-static {p0, v0}, LX/26s;->a(LX/26s;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    .line 372410
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 372411
    :catch_0
    move-exception v1

    .line 372412
    iget-object v8, p0, LX/26s;->b:LX/03V;

    const-string v9, "composer_publish_db_deserialize_fail"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "story="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v0, LX/7m3;->b:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", publish="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v0, LX/7m3;->c:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 372413
    iget-object v0, v0, LX/7m3;->a:Ljava/lang/String;

    invoke-static {p0, v0}, LX/26s;->a(LX/26s;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    goto :goto_1

    .line 372414
    :cond_0
    invoke-virtual {v6, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 372415
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 372416
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 372417
    const-string v1, "save_pending_story"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 372418
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 372419
    const-string v1, "pending_story_param_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    .line 372420
    iget-object v1, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->story:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 372421
    iget-object v1, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 372422
    iget-object v1, p0, LX/26s;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1qN;

    iget-object v2, p0, LX/26s;->d:LX/0lC;

    .line 372423
    new-instance v3, LX/7m3;

    iget-object v4, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v4

    iget-object p2, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->story:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, p2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    iget-object p0, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v2, p0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    iget-object p1, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->publishAttemptInfo:Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    invoke-virtual {v2, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, v4, p2, p0, p1}, LX/7m3;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 372424
    iget-object v2, v1, LX/1qN;->e:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 372425
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 372426
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 372427
    sget-object v3, LX/1qQ;->a:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, LX/7m3;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 372428
    sget-object v3, LX/1qQ;->c:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, LX/7m3;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 372429
    sget-object v3, LX/1qQ;->b:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, LX/7m3;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 372430
    sget-object v3, LX/1qQ;->d:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, LX/7m3;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 372431
    iget-object v3, v1, LX/1qN;->d:LX/1qR;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "pending_story"

    const-string p2, ""

    const p0, -0x722b5bf3

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {v3, v4, p2, v2}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v2, -0x1f341411

    invoke-static {v2}, LX/03h;->a(I)V

    .line 372432
    iget-object v2, v1, LX/1qN;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0dp;->f:LX/0Tn;

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 372433
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 372434
    move-object v0, v0

    .line 372435
    :goto_0
    return-object v0

    .line 372436
    :cond_0
    const-string v1, "delete_pending_story"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 372437
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 372438
    const-string v1, "request_id_param_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 372439
    invoke-static {p0, v0}, LX/26s;->a(LX/26s;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 372440
    goto :goto_0

    .line 372441
    :cond_1
    const-string v1, "delete_all_pending_stories"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 372442
    iget-object v0, p0, LX/26s;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qN;

    const/4 p0, 0x0

    .line 372443
    iget-object v1, v0, LX/1qN;->e:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->b()V

    .line 372444
    iget-object v1, v0, LX/1qN;->d:LX/1qR;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "pending_story"

    invoke-virtual {v1, v2, p0, p0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 372445
    iget-object v1, v0, LX/1qN;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0dp;->f:LX/0Tn;

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 372446
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 372447
    move-object v0, v0

    .line 372448
    goto :goto_0

    .line 372449
    :cond_2
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method
