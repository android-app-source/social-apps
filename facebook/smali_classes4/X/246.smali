.class public LX/246;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:I

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 366596
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, LX/246;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/5pG;)V
    .locals 1

    .prologue
    .line 366585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 366586
    const-string v0, "appID"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/246;->b:Ljava/lang/String;

    .line 366587
    const-string v0, "appName"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/246;->c:Ljava/lang/String;

    .line 366588
    const-string v0, "deviceName"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/246;->d:Ljava/lang/String;

    .line 366589
    const-string v0, "imageUri"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/246;->e:Ljava/lang/String;

    .line 366590
    const-string v0, "nonce"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/246;->f:Ljava/lang/String;

    .line 366591
    const-string v0, "scope"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/246;->g:Ljava/lang/String;

    .line 366592
    const-string v0, "timestampExpire"

    invoke-interface {p1, v0}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/246;->h:I

    .line 366593
    const-string v0, "userCode"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/246;->i:Ljava/lang/String;

    .line 366594
    const-string v0, "codeType"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/246;->j:Ljava/lang/String;

    .line 366595
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 366574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 366575
    iput-object p1, p0, LX/246;->b:Ljava/lang/String;

    .line 366576
    iput-object p2, p0, LX/246;->c:Ljava/lang/String;

    .line 366577
    iput-object p3, p0, LX/246;->d:Ljava/lang/String;

    .line 366578
    iput-object p4, p0, LX/246;->e:Ljava/lang/String;

    .line 366579
    iput-object p5, p0, LX/246;->f:Ljava/lang/String;

    .line 366580
    iput-object p6, p0, LX/246;->g:Ljava/lang/String;

    .line 366581
    iput p7, p0, LX/246;->h:I

    .line 366582
    iput-object p8, p0, LX/246;->i:Ljava/lang/String;

    .line 366583
    iput-object p9, p0, LX/246;->j:Ljava/lang/String;

    .line 366584
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 366597
    invoke-direct/range {p0 .. p9}, LX/246;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 366544
    if-eqz p1, :cond_0

    instance-of v1, p1, LX/246;

    if-eqz v1, :cond_0

    .line 366545
    check-cast p1, LX/246;

    .line 366546
    iget-object v1, p1, LX/246;->b:Ljava/lang/String;

    move-object v1, v1

    .line 366547
    iget-object v2, p0, LX/246;->b:Ljava/lang/String;

    move-object v2, v2

    .line 366548
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366549
    iget-object v1, p1, LX/246;->c:Ljava/lang/String;

    move-object v1, v1

    .line 366550
    iget-object v2, p0, LX/246;->c:Ljava/lang/String;

    move-object v2, v2

    .line 366551
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366552
    iget-object v1, p1, LX/246;->d:Ljava/lang/String;

    move-object v1, v1

    .line 366553
    iget-object v2, p0, LX/246;->d:Ljava/lang/String;

    move-object v2, v2

    .line 366554
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366555
    iget-object v1, p1, LX/246;->e:Ljava/lang/String;

    move-object v1, v1

    .line 366556
    iget-object v2, p0, LX/246;->e:Ljava/lang/String;

    move-object v2, v2

    .line 366557
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366558
    iget-object v1, p1, LX/246;->f:Ljava/lang/String;

    move-object v1, v1

    .line 366559
    iget-object v2, p0, LX/246;->f:Ljava/lang/String;

    move-object v2, v2

    .line 366560
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366561
    iget-object v1, p1, LX/246;->g:Ljava/lang/String;

    move-object v1, v1

    .line 366562
    iget-object v2, p0, LX/246;->g:Ljava/lang/String;

    move-object v2, v2

    .line 366563
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366564
    iget v1, p1, LX/246;->h:I

    move v1, v1

    .line 366565
    iget v2, p0, LX/246;->h:I

    move v2, v2

    .line 366566
    if-ne v1, v2, :cond_0

    .line 366567
    iget-object v1, p1, LX/246;->i:Ljava/lang/String;

    move-object v1, v1

    .line 366568
    iget-object v2, p0, LX/246;->i:Ljava/lang/String;

    move-object v2, v2

    .line 366569
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366570
    iget-object v1, p1, LX/246;->j:Ljava/lang/String;

    move-object v1, v1

    .line 366571
    iget-object v2, p0, LX/246;->j:Ljava/lang/String;

    move-object v2, v2

    .line 366572
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 366573
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 366543
    invoke-virtual {p0}, LX/246;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 366524
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{ appID: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 366525
    iget-object v1, p0, LX/246;->b:Ljava/lang/String;

    move-object v1, v1

    .line 366526
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " appName"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 366527
    iget-object v1, p0, LX/246;->c:Ljava/lang/String;

    move-object v1, v1

    .line 366528
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " deviceName"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 366529
    iget-object v1, p0, LX/246;->d:Ljava/lang/String;

    move-object v1, v1

    .line 366530
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " imageUri"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 366531
    iget-object v1, p0, LX/246;->e:Ljava/lang/String;

    move-object v1, v1

    .line 366532
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " nonce"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 366533
    iget-object v1, p0, LX/246;->f:Ljava/lang/String;

    move-object v1, v1

    .line 366534
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " scope"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 366535
    iget-object v1, p0, LX/246;->g:Ljava/lang/String;

    move-object v1, v1

    .line 366536
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timestampExpire"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 366537
    iget v1, p0, LX/246;->h:I

    move v1, v1

    .line 366538
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " userCode"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 366539
    iget-object v1, p0, LX/246;->i:Ljava/lang/String;

    move-object v1, v1

    .line 366540
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " codeType"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 366541
    iget-object v1, p0, LX/246;->j:Ljava/lang/String;

    move-object v1, v1

    .line 366542
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
