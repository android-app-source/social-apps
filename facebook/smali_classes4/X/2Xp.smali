.class public LX/2Xp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Xq;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 420306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420307
    iput-object p1, p0, LX/2Xp;->a:LX/0tX;

    .line 420308
    return-void
.end method

.method public static a(LX/0QB;)LX/2Xp;
    .locals 4

    .prologue
    .line 420309
    const-class v1, LX/2Xp;

    monitor-enter v1

    .line 420310
    :try_start_0
    sget-object v0, LX/2Xp;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 420311
    sput-object v2, LX/2Xp;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 420312
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 420313
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 420314
    new-instance p0, LX/2Xp;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {p0, v3}, LX/2Xp;-><init>(LX/0tX;)V

    .line 420315
    move-object v0, p0

    .line 420316
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 420317
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2Xp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 420318
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 420319
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 420320
    iget-object v0, p0, LX/2Xp;->a:LX/0tX;

    .line 420321
    new-instance p0, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$2;

    invoke-direct {p0, v0}, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$2;-><init>(LX/0tX;)V

    .line 420322
    invoke-virtual {p0}, Ljava/lang/Thread;->start()V

    .line 420323
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 420324
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 420325
    new-instance p0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    sput-object p0, LX/0tX;->t:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 420326
    return-void
.end method
