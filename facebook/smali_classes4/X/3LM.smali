.class public LX/3LM;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/3LN;

.field private final c:LX/3LP;

.field private final d:LX/0Sh;

.field public final e:LX/0TD;

.field public f:LX/2RQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 550343
    const-class v0, LX/3LM;

    sput-object v0, LX/3LM;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3LN;LX/3LP;LX/0Sh;LX/0TD;)V
    .locals 0
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 550344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550345
    iput-object p1, p0, LX/3LM;->b:LX/3LN;

    .line 550346
    iput-object p2, p0, LX/3LM;->c:LX/3LP;

    .line 550347
    iput-object p3, p0, LX/3LM;->d:LX/0Sh;

    .line 550348
    iput-object p4, p0, LX/3LM;->e:LX/0TD;

    .line 550349
    return-void
.end method

.method public static b(LX/0QB;)LX/3LM;
    .locals 5

    .prologue
    .line 550350
    new-instance v4, LX/3LM;

    invoke-static {p0}, LX/3LN;->a(LX/0QB;)LX/3LN;

    move-result-object v0

    check-cast v0, LX/3LN;

    invoke-static {p0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v1

    check-cast v1, LX/3LP;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-direct {v4, v0, v1, v2, v3}, LX/3LM;-><init>(LX/3LN;LX/3LP;LX/0Sh;LX/0TD;)V

    .line 550351
    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v0

    check-cast v0, LX/2RQ;

    .line 550352
    iput-object v0, v4, LX/3LM;->f:LX/2RQ;

    .line 550353
    return-object v4
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 550354
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 550355
    iget-object v0, p0, LX/3LM;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 550356
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-static {v0}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v2

    .line 550357
    iget-object v0, p0, LX/3LM;->f:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    sget-object v1, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 550358
    iput-object v1, v0, LX/2RR;->b:Ljava/util/Collection;

    .line 550359
    move-object v0, v0

    .line 550360
    iput-object p1, v0, LX/2RR;->d:Ljava/util/Collection;

    .line 550361
    move-object v0, v0

    .line 550362
    iget-object v1, p0, LX/3LM;->c:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 550363
    :goto_0
    :try_start_0
    invoke-interface {v1}, LX/3On;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 550364
    invoke-interface {v1}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 550365
    iget-object v3, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v3, v3

    .line 550366
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 550367
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, LX/3On;->close()V

    .line 550368
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 550369
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 550370
    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 550371
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 550372
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 550373
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 550374
    iget-object v1, p0, LX/3LM;->b:LX/3LN;

    invoke-virtual {v1, v0}, LX/3LN;->a(LX/0Px;)V

    .line 550375
    return-object v0
.end method
