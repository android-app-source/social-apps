.class public LX/2P3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2P3;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 405086
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 405087
    iput-object p1, p0, LX/2P3;->a:LX/0Or;

    .line 405088
    iput-object p2, p0, LX/2P3;->b:LX/0Or;

    .line 405089
    return-void
.end method

.method public static a(LX/0QB;)LX/2P3;
    .locals 5

    .prologue
    .line 404942
    sget-object v0, LX/2P3;->c:LX/2P3;

    if-nez v0, :cond_1

    .line 404943
    const-class v1, LX/2P3;

    monitor-enter v1

    .line 404944
    :try_start_0
    sget-object v0, LX/2P3;->c:LX/2P3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 404945
    if-eqz v2, :cond_0

    .line 404946
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 404947
    new-instance v3, LX/2P3;

    const/16 v4, 0x15e7

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const-class p0, Landroid/content/Context;

    invoke-interface {v0, p0}, LX/0QC;->getProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/2P3;-><init>(LX/0Or;LX/0Or;)V

    .line 404948
    move-object v0, v3

    .line 404949
    sput-object v0, LX/2P3;->c:LX/2P3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404950
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 404951
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 404952
    :cond_1
    sget-object v0, LX/2P3;->c:LX/2P3;

    return-object v0

    .line 404953
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 404954
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;JJLX/6f7;)LX/6f7;
    .locals 6

    .prologue
    .line 405072
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v1, Lcom/facebook/user/model/UserKey;

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 405073
    invoke-virtual {p6, p1}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    .line 405074
    iput-object p1, p6, LX/6f7;->n:Ljava/lang/String;

    .line 405075
    iput-object p0, p6, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 405076
    const-wide/16 v2, 0x3e8

    div-long v2, p4, v2

    .line 405077
    iput-wide v2, p6, LX/6f7;->c:J

    .line 405078
    iput-object v0, p6, LX/6f7;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 405079
    sget-object v0, LX/6f2;->MQTT:LX/6f2;

    .line 405080
    iput-object v0, p6, LX/6f7;->q:LX/6f2;

    .line 405081
    const-string v0, "o"

    .line 405082
    iput-object v0, p6, LX/6f7;->p:Ljava/lang/String;

    .line 405083
    iget-wide v4, p6, LX/6f7;->c:J

    move-wide v0, v4

    .line 405084
    iput-wide v0, p6, LX/6f7;->g:J

    .line 405085
    return-object p6
.end method

.method public static a(LX/DpA;Ljava/lang/String;)Lcom/facebook/messaging/model/attachment/ImageData;
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 405055
    iget-object v2, p0, LX/DpA;->image_metadata:LX/DpI;

    .line 405056
    iget-object v0, p0, LX/DpA;->thumbnail_data:[B

    if-eqz v0, :cond_0

    .line 405057
    iget-object v0, p0, LX/DpA;->thumbnail_data:[B

    invoke-static {v0, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v7

    .line 405058
    :goto_0
    new-instance v0, Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object v1, v2, LX/DpI;->width:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, v2, LX/DpI;->height:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 405059
    iget-object v3, p0, LX/DpA;->image_metadata:LX/DpI;

    .line 405060
    new-instance v5, LX/5dP;

    invoke-direct {v5}, LX/5dP;-><init>()V

    sget-object v8, LX/5dQ;->FULL_SCREEN:LX/5dQ;

    new-instance v9, LX/5dM;

    invoke-direct {v9}, LX/5dM;-><init>()V

    .line 405061
    iget-object v10, p0, LX/DpA;->download_fbid:Ljava/lang/Long;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, p1}, LX/DoH;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    move-object v10, v10

    .line 405062
    iput-object v10, v9, LX/5dM;->c:Ljava/lang/String;

    .line 405063
    move-object v9, v9

    .line 405064
    iget-object v10, v3, LX/DpI;->width:Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 405065
    iput v10, v9, LX/5dM;->a:I

    .line 405066
    move-object v9, v9

    .line 405067
    iget-object v3, v3, LX/DpI;->height:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 405068
    iput v3, v9, LX/5dM;->b:I

    .line 405069
    move-object v3, v9

    .line 405070
    invoke-virtual {v3}, LX/5dM;->d()Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v3

    invoke-virtual {v5, v8, v3}, LX/5dP;->a(LX/5dQ;Lcom/facebook/messaging/model/attachment/ImageUrl;)LX/5dP;

    move-result-object v3

    invoke-virtual {v3}, LX/5dP;->b()Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    move-result-object v3

    move-object v3, v3

    .line 405071
    sget-object v5, LX/5dT;->NONQUICKCAM:LX/5dT;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/model/attachment/ImageData;-><init>(IILcom/facebook/messaging/model/attachment/AttachmentImageMap;Lcom/facebook/messaging/model/attachment/AttachmentImageMap;LX/5dT;ZLjava/lang/String;)V

    return-object v0

    :cond_0
    move-object v7, v4

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/DpA;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/attachment/Attachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 405025
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 405026
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DpA;

    .line 405027
    iget-object v3, v0, LX/DpA;->download_fbid:Ljava/lang/Long;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 405028
    new-instance v4, LX/5dN;

    invoke-direct {v4, v3, p0}, LX/5dN;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 405029
    iput-object v3, v4, LX/5dN;->c:Ljava/lang/String;

    .line 405030
    move-object v3, v4

    .line 405031
    iget-object v4, v0, LX/DpA;->suggested_file_name:Ljava/lang/String;

    .line 405032
    iput-object v4, v3, LX/5dN;->e:Ljava/lang/String;

    .line 405033
    move-object v3, v3

    .line 405034
    iget-object v4, v0, LX/DpA;->download_size_bytes:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, LX/0a4;->a(J)I

    move-result v4

    .line 405035
    iput v4, v3, LX/5dN;->f:I

    .line 405036
    move-object v3, v3

    .line 405037
    iget-object v4, v0, LX/DpA;->file_mime_type:Ljava/lang/String;

    .line 405038
    iput-object v4, v3, LX/5dN;->d:Ljava/lang/String;

    .line 405039
    move-object v3, v3

    .line 405040
    invoke-static {v0, p0}, LX/2P3;->a(LX/DpA;Ljava/lang/String;)Lcom/facebook/messaging/model/attachment/ImageData;

    move-result-object v4

    .line 405041
    iput-object v4, v3, LX/5dN;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    .line 405042
    move-object v3, v3

    .line 405043
    iget-object v4, v0, LX/DpA;->secret_key:[B

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v4

    .line 405044
    iput-object v4, v3, LX/5dN;->j:Ljava/lang/String;

    .line 405045
    move-object v3, v3

    .line 405046
    iget-object v4, v0, LX/DpA;->download_hash:[B

    .line 405047
    iput-object v4, v3, LX/5dN;->k:[B

    .line 405048
    move-object v3, v3

    .line 405049
    iget-object v4, v0, LX/DpA;->download_mac:Ljava/lang/String;

    .line 405050
    iput-object v4, v3, LX/5dN;->l:Ljava/lang/String;

    .line 405051
    move-object v3, v3

    .line 405052
    invoke-virtual {v3}, LX/5dN;->m()Lcom/facebook/messaging/model/attachment/Attachment;

    move-result-object v3

    move-object v0, v3

    .line 405053
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 405054
    :cond_0
    return-object v1
.end method

.method public static a(LX/2P3;ILX/6f7;)V
    .locals 4

    .prologue
    .line 405017
    iget-object v0, p2, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    move-object v0, v0

    .line 405018
    iget-object v0, v0, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    sget-object v1, LX/6fP;->NONE:LX/6fP;

    if-eq v0, v1, :cond_0

    .line 405019
    :goto_0
    return-void

    .line 405020
    :cond_0
    iget-object v0, p0, LX/2P3;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 405021
    iget-object v3, p2, LX/6f7;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-object v3, v3

    .line 405022
    iget-object v3, v3, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 405023
    iput-object v0, p2, LX/6f7;->f:Ljava/lang/String;

    .line 405024
    invoke-static {p2}, LX/2P3;->c(LX/6f7;)V

    goto :goto_0
.end method

.method private a(LX/6f7;)V
    .locals 1

    .prologue
    .line 405015
    const v0, 0x7f0806be

    invoke-static {p0, v0, p1}, LX/2P3;->a(LX/2P3;ILX/6f7;)V

    .line 405016
    return-void
.end method

.method private static c(LX/6f7;)V
    .locals 1

    .prologue
    .line 405007
    sget-object v0, LX/2uW;->ADMIN:LX/2uW;

    .line 405008
    iput-object v0, p0, LX/6f7;->l:LX/2uW;

    .line 405009
    sget-object v0, Lcom/facebook/messaging/model/send/SendError;->a:Lcom/facebook/messaging/model/send/SendError;

    .line 405010
    iput-object v0, p0, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 405011
    invoke-static {}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->newBuilder()LX/6ev;

    move-result-object v0

    .line 405012
    invoke-virtual {v0}, LX/6ev;->a()Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    move-result-object v0

    .line 405013
    iput-object v0, p0, LX/6f7;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 405014
    return-void
.end method


# virtual methods
.method public final a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 3

    .prologue
    .line 405002
    iget-object v0, p0, LX/2P3;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 405003
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 405004
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 405005
    invoke-static {p1, p2, v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    return-object v0

    .line 405006
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(LX/6f7;Ljava/lang/String;LX/DpY;)V
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 404955
    if-eqz p3, :cond_0

    iget-object v2, p3, LX/DpY;->type:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v2, p3, LX/DpY;->type:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 404956
    :cond_0
    invoke-direct {p0, p1}, LX/2P3;->a(LX/6f7;)V

    .line 404957
    :cond_1
    :goto_0
    return-void

    .line 404958
    :cond_2
    :try_start_0
    iget-object v2, p3, LX/DpY;->type:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v4, :cond_4

    .line 404959
    iget-object v2, p3, LX/DpY;->body:LX/DpZ;

    if-eqz v2, :cond_3

    iget-object v2, p3, LX/DpY;->body:LX/DpZ;

    .line 404960
    iget v3, v2, LX/6kT;->setField_:I

    move v2, v3

    .line 404961
    if-ne v2, v4, :cond_3

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 404962
    iget-object v0, p3, LX/DpY;->body:LX/DpZ;

    invoke-virtual {v0}, LX/DpZ;->c()Ljava/lang/String;

    move-result-object v0

    .line 404963
    iput-object v0, p1, LX/6f7;->f:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 404964
    :goto_2
    iget-object v0, p3, LX/DpY;->ephemeral_lifetime_micros:Ljava/lang/Long;

    if-eqz v0, :cond_1

    iget-object v0, p3, LX/DpY;->ephemeral_lifetime_micros:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 404965
    iget-object v0, p3, LX/DpY;->ephemeral_lifetime_micros:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    const-wide/32 v2, 0x7fffffff

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 404966
    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 404967
    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 404968
    iput-object v0, p1, LX/6f7;->J:Ljava/lang/Integer;

    .line 404969
    iget-object v0, p1, LX/6f7;->K:Ljava/lang/Long;

    move-object v0, v0

    .line 404970
    if-nez v0, :cond_1

    .line 404971
    const-wide/16 v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 404972
    iput-object v0, p1, LX/6f7;->K:Ljava/lang/Long;

    .line 404973
    goto :goto_0

    :cond_3
    move v0, v1

    .line 404974
    goto :goto_1

    .line 404975
    :cond_4
    :try_start_1
    iget-object v2, p3, LX/DpY;->type:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v5, :cond_6

    .line 404976
    iget-object v2, p3, LX/DpY;->body:LX/DpZ;

    if-eqz v2, :cond_5

    iget-object v2, p3, LX/DpY;->body:LX/DpZ;

    .line 404977
    iget v3, v2, LX/6kT;->setField_:I

    move v2, v3

    .line 404978
    if-ne v2, v5, :cond_5

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 404979
    iget-object v0, p3, LX/DpY;->body:LX/DpZ;

    invoke-virtual {v0}, LX/DpZ;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {p2, v0}, LX/2P3;->a(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 404980
    iput-object v0, p1, LX/6f7;->i:Ljava/util/List;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    .line 404981
    goto :goto_2

    .line 404982
    :catch_0
    move-exception v0

    .line 404983
    invoke-direct {p0, p1}, LX/2P3;->a(LX/6f7;)V

    .line 404984
    throw v0

    :cond_5
    move v0, v1

    .line 404985
    goto :goto_3

    .line 404986
    :cond_6
    :try_start_2
    iget-object v2, p3, LX/DpY;->type:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v6, :cond_8

    .line 404987
    iget-object v2, p3, LX/DpY;->body:LX/DpZ;

    if-eqz v2, :cond_7

    iget-object v2, p3, LX/DpY;->body:LX/DpZ;

    .line 404988
    iget v3, v2, LX/6kT;->setField_:I

    move v2, v3

    .line 404989
    if-ne v2, v6, :cond_7

    :goto_4
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 404990
    iget-object v0, p3, LX/DpY;->body:LX/DpZ;

    invoke-virtual {v0}, LX/DpZ;->f()LX/Dpg;

    move-result-object v0

    iget-object v0, v0, LX/Dpg;->fbid:Ljava/lang/Long;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 404991
    iput-object v0, p1, LX/6f7;->k:Ljava/lang/String;

    .line 404992
    goto/16 :goto_2

    :cond_7
    move v0, v1

    .line 404993
    goto :goto_4

    .line 404994
    :cond_8
    iget-object v2, p3, LX/DpY;->type:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_a

    .line 404995
    iget-object v2, p3, LX/DpY;->body:LX/DpZ;

    if-eqz v2, :cond_9

    :goto_5
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 404996
    iget-object v0, p3, LX/DpY;->body:LX/DpZ;

    invoke-virtual {v0}, LX/DpZ;->e()Ljava/lang/String;

    move-result-object v0

    .line 404997
    iput-object v0, p1, LX/6f7;->f:Ljava/lang/String;

    .line 404998
    invoke-static {p1}, LX/2P3;->c(LX/6f7;)V

    goto/16 :goto_2

    :cond_9
    move v0, v1

    .line 404999
    goto :goto_5

    .line 405000
    :cond_a
    const v0, 0x7f0806bd

    invoke-static {p0, v0, p1}, LX/2P3;->a(LX/2P3;ILX/6f7;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0

    .line 405001
    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;JJLX/DpY;LX/6f7;)V
    .locals 7

    .prologue
    .line 404939
    move-object v0, p1

    move-object v1, p2

    move-wide v2, p3

    move-wide v4, p5

    move-object v6, p8

    invoke-static/range {v0 .. v6}, LX/2P3;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;JJLX/6f7;)LX/6f7;

    .line 404940
    invoke-virtual {p0, p8, p2, p7}, LX/2P3;->a(LX/6f7;Ljava/lang/String;LX/DpY;)V

    .line 404941
    return-void
.end method
