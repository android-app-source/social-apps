.class public final LX/357;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1yk;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public d:LX/DHB;

.field public final synthetic e:LX/1yk;


# direct methods
.method public constructor <init>(LX/1yk;)V
    .locals 1

    .prologue
    .line 496655
    iput-object p1, p0, LX/357;->e:LX/1yk;

    .line 496656
    move-object v0, p1

    .line 496657
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 496658
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 496654
    const-string v0, "HeaderLikeButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 496630
    if-ne p0, p1, :cond_1

    .line 496631
    :cond_0
    :goto_0
    return v0

    .line 496632
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 496633
    goto :goto_0

    .line 496634
    :cond_3
    check-cast p1, LX/357;

    .line 496635
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 496636
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 496637
    if-eq v2, v3, :cond_0

    .line 496638
    iget-object v2, p0, LX/357;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/357;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/357;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 496639
    goto :goto_0

    .line 496640
    :cond_5
    iget-object v2, p1, LX/357;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 496641
    :cond_6
    iget-object v2, p0, LX/357;->b:LX/1Pd;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/357;->b:LX/1Pd;

    iget-object v3, p1, LX/357;->b:LX/1Pd;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 496642
    goto :goto_0

    .line 496643
    :cond_8
    iget-object v2, p1, LX/357;->b:LX/1Pd;

    if-nez v2, :cond_7

    .line 496644
    :cond_9
    iget-object v2, p0, LX/357;->c:LX/1X1;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/357;->c:LX/1X1;

    iget-object v3, p1, LX/357;->c:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 496645
    goto :goto_0

    .line 496646
    :cond_b
    iget-object v2, p1, LX/357;->c:LX/1X1;

    if-nez v2, :cond_a

    .line 496647
    :cond_c
    iget-object v2, p0, LX/357;->d:LX/DHB;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/357;->d:LX/DHB;

    iget-object v3, p1, LX/357;->d:LX/DHB;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 496648
    goto :goto_0

    .line 496649
    :cond_d
    iget-object v2, p1, LX/357;->d:LX/DHB;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 496650
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/357;

    .line 496651
    iget-object v1, v0, LX/357;->c:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/357;->c:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/357;->c:LX/1X1;

    .line 496652
    return-object v0

    .line 496653
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
