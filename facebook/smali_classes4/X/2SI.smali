.class public LX/2SI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/2SI;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0Xl;

.field private final c:Landroid/os/Handler;

.field private final d:LX/0Uo;

.field public final e:LX/2SJ;

.field private final f:LX/2SK;

.field public final g:LX/2SL;

.field private final h:LX/0YZ;

.field private final i:LX/0YZ;

.field public j:LX/23S;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Xl;Landroid/os/Handler;LX/0Uo;LX/2SJ;LX/2SK;LX/2SL;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 412093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412094
    iput-object p1, p0, LX/2SI;->a:Landroid/content/Context;

    .line 412095
    iput-object p2, p0, LX/2SI;->b:LX/0Xl;

    .line 412096
    iput-object p3, p0, LX/2SI;->c:Landroid/os/Handler;

    .line 412097
    iput-object p4, p0, LX/2SI;->d:LX/0Uo;

    .line 412098
    iput-object p5, p0, LX/2SI;->e:LX/2SJ;

    .line 412099
    iput-object p6, p0, LX/2SI;->f:LX/2SK;

    .line 412100
    iput-object p7, p0, LX/2SI;->g:LX/2SL;

    .line 412101
    new-instance v0, LX/2SM;

    invoke-direct {v0, p0}, LX/2SM;-><init>(LX/2SI;)V

    iput-object v0, p0, LX/2SI;->h:LX/0YZ;

    .line 412102
    new-instance v0, LX/2SN;

    invoke-direct {v0, p0}, LX/2SN;-><init>(LX/2SI;)V

    iput-object v0, p0, LX/2SI;->i:LX/0YZ;

    .line 412103
    return-void
.end method

.method public static a(LX/0QB;)LX/2SI;
    .locals 14

    .prologue
    .line 412071
    sget-object v0, LX/2SI;->k:LX/2SI;

    if-nez v0, :cond_1

    .line 412072
    const-class v1, LX/2SI;

    monitor-enter v1

    .line 412073
    :try_start_0
    sget-object v0, LX/2SI;->k:LX/2SI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 412074
    if-eqz v2, :cond_0

    .line 412075
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 412076
    new-instance v3, LX/2SI;

    const-class v4, Landroid/content/Context;

    const-class v5, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v4, v5}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {v0}, LX/0Zw;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v6

    check-cast v6, Landroid/os/Handler;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v7

    check-cast v7, LX/0Uo;

    .line 412077
    new-instance v9, LX/2SJ;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-direct {v9, v8}, LX/2SJ;-><init>(LX/0Zb;)V

    .line 412078
    move-object v8, v9

    .line 412079
    check-cast v8, LX/2SJ;

    .line 412080
    new-instance v10, LX/2SK;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-direct {v10, v9}, LX/2SK;-><init>(LX/0Uh;)V

    .line 412081
    move-object v9, v10

    .line 412082
    check-cast v9, LX/2SK;

    .line 412083
    new-instance v13, LX/2SL;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    const-class v11, Landroid/content/Context;

    const-class v12, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v11, v12}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Context;

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v12

    check-cast v12, Landroid/os/Handler;

    invoke-direct {v13, v10, v11, v12}, LX/2SL;-><init>(LX/0SG;Landroid/content/Context;Landroid/os/Handler;)V

    .line 412084
    move-object v10, v13

    .line 412085
    check-cast v10, LX/2SL;

    invoke-direct/range {v3 .. v10}, LX/2SI;-><init>(Landroid/content/Context;LX/0Xl;Landroid/os/Handler;LX/0Uo;LX/2SJ;LX/2SK;LX/2SL;)V

    .line 412086
    move-object v0, v3

    .line 412087
    sput-object v0, LX/2SI;->k:LX/2SI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412088
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 412089
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 412090
    :cond_1
    sget-object v0, LX/2SI;->k:LX/2SI;

    return-object v0

    .line 412091
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 412092
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b$redex0(LX/2SI;)V
    .locals 4

    .prologue
    .line 412055
    iget-object v0, p0, LX/2SI;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, LX/2SI;->g:LX/2SL;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 412056
    iget-object v0, p0, LX/2SI;->g:LX/2SL;

    .line 412057
    iput-object p0, v0, LX/2SL;->f:LX/2SI;

    .line 412058
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 4

    .prologue
    .line 412059
    iget-object v0, p0, LX/2SI;->f:LX/2SK;

    .line 412060
    iget-object v1, v0, LX/2SK;->a:LX/0Uh;

    const/16 v2, 0xf

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 412061
    if-nez v0, :cond_1

    .line 412062
    :cond_0
    :goto_0
    return-void

    .line 412063
    :cond_1
    iget-object v0, p0, LX/2SI;->a:Landroid/content/Context;

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 412064
    if-eqz v0, :cond_0

    .line 412065
    iget-object v0, p0, LX/2SI;->d:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_2

    .line 412066
    invoke-static {p0}, LX/2SI;->b$redex0(LX/2SI;)V

    .line 412067
    :cond_2
    iget-object v0, p0, LX/2SI;->b:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    iget-object v2, p0, LX/2SI;->h:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/2SI;->c:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    .line 412068
    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 412069
    iget-object v0, p0, LX/2SI;->b:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    iget-object v2, p0, LX/2SI;->i:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/2SI;->c:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    .line 412070
    invoke-virtual {v0}, LX/0Yb;->b()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
