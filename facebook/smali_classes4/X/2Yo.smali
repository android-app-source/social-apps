.class public LX/2Yo;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 421355
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2Yo;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 421385
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;
    .locals 8

    .prologue
    .line 421357
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 421358
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 421359
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 421360
    if-nez v1, :cond_0

    .line 421361
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 421362
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 421363
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 421364
    sget-object v1, LX/2Yo;->a:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 421365
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 421366
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 421367
    :cond_1
    if-nez v1, :cond_4

    .line 421368
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 421369
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 421370
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 421371
    invoke-static {v0}, LX/2Yp;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v7

    invoke-static {v0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v1

    check-cast v1, LX/0Wd;

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v7, v1, p0}, LX/2Yq;->a(Ljava/util/Set;LX/0Wd;LX/0Ot;)Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;

    move-result-object v1

    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 421372
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 421373
    if-nez v1, :cond_2

    .line 421374
    sget-object v0, LX/2Yo;->a:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 421375
    :goto_1
    if-eqz v0, :cond_3

    .line 421376
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 421377
    :goto_3
    check-cast v0, Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 421378
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 421379
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 421380
    :catchall_1
    move-exception v0

    .line 421381
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 421382
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 421383
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 421384
    :cond_2
    :try_start_8
    sget-object v0, LX/2Yo;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 421356
    invoke-static {p0}, LX/2Yp;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v1

    invoke-static {p0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v0

    check-cast v0, LX/0Wd;

    const/16 v2, 0x259

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/2Yq;->a(Ljava/util/Set;LX/0Wd;LX/0Ot;)Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;

    move-result-object v0

    return-object v0
.end method
