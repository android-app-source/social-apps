.class public LX/37e;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/37e;


# instance fields
.field private final b:LX/03V;

.field private final c:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 501753
    const-class v0, LX/37e;

    sput-object v0, LX/37e;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 501749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501750
    iput-object p1, p0, LX/37e;->b:LX/03V;

    .line 501751
    iput-object p2, p0, LX/37e;->c:LX/0Uh;

    .line 501752
    return-void
.end method

.method public static a(LX/0QB;)LX/37e;
    .locals 5

    .prologue
    .line 501736
    sget-object v0, LX/37e;->d:LX/37e;

    if-nez v0, :cond_1

    .line 501737
    const-class v1, LX/37e;

    monitor-enter v1

    .line 501738
    :try_start_0
    sget-object v0, LX/37e;->d:LX/37e;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 501739
    if-eqz v2, :cond_0

    .line 501740
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 501741
    new-instance p0, LX/37e;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/37e;-><init>(LX/03V;LX/0Uh;)V

    .line 501742
    move-object v0, p0

    .line 501743
    sput-object v0, LX/37e;->d:LX/37e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 501744
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 501745
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 501746
    :cond_1
    sget-object v0, LX/37e;->d:LX/37e;

    return-object v0

    .line 501747
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 501748
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(LX/7JJ;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 501754
    iget-object v0, p0, LX/37e;->c:LX/0Uh;

    sget v1, LX/19n;->e:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 501755
    :goto_0
    return-void

    .line 501756
    :cond_0
    iget-object v0, p0, LX/37e;->b:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CHROMECAST_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/7JJ;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7JJ;Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    .prologue
    .line 501731
    sget-object v0, LX/37e;->a:Ljava/lang/Class;

    const-string v1, "log(%s, %s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 501732
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Code: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p2, Lcom/google/android/gms/common/api/Status;->i:I

    move v1, v1

    .line 501733
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Message: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/gms/common/api/Status;->j:Ljava/lang/String;

    move-object v1, v1

    .line 501734
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/37e;->b(LX/7JJ;Ljava/lang/String;)V

    .line 501735
    return-void
.end method

.method public final a(LX/7JJ;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 501725
    sget-object v0, LX/37e;->a:Ljava/lang/Class;

    const-string v1, "log(%s, %s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 501726
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Exception: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/37e;->b(LX/7JJ;Ljava/lang/String;)V

    .line 501727
    return-void
.end method

.method public final a(LX/7JJ;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 501728
    sget-object v0, LX/37e;->a:Ljava/lang/Class;

    const-string v1, "log(%s, %s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 501729
    invoke-direct {p0, p1, p2}, LX/37e;->b(LX/7JJ;Ljava/lang/String;)V

    .line 501730
    return-void
.end method
