.class public LX/23H;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/23H;


# instance fields
.field private final a:LX/0pf;

.field private final b:LX/0Uh;

.field private final c:LX/0W3;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/82V;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0pf;LX/0Uh;LX/0W3;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 363900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 363901
    iput-object p1, p0, LX/23H;->a:LX/0pf;

    .line 363902
    iput-object p2, p0, LX/23H;->b:LX/0Uh;

    .line 363903
    iput-object p3, p0, LX/23H;->c:LX/0W3;

    .line 363904
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/23H;->d:Ljava/util/List;

    .line 363905
    return-void
.end method

.method public static a(LX/0QB;)LX/23H;
    .locals 6

    .prologue
    .line 363873
    sget-object v0, LX/23H;->e:LX/23H;

    if-nez v0, :cond_1

    .line 363874
    const-class v1, LX/23H;

    monitor-enter v1

    .line 363875
    :try_start_0
    sget-object v0, LX/23H;->e:LX/23H;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 363876
    if-eqz v2, :cond_0

    .line 363877
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 363878
    new-instance p0, LX/23H;

    invoke-static {v0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v3

    check-cast v3, LX/0pf;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-direct {p0, v3, v4, v5}, LX/23H;-><init>(LX/0pf;LX/0Uh;LX/0W3;)V

    .line 363879
    move-object v0, p0

    .line 363880
    sput-object v0, LX/23H;->e:LX/23H;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 363881
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 363882
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 363883
    :cond_1
    sget-object v0, LX/23H;->e:LX/23H;

    return-object v0

    .line 363884
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 363885
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/23H;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)LX/82V;
    .locals 4

    .prologue
    .line 363896
    iget-object v0, p0, LX/23H;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/82V;

    .line 363897
    iget-object v2, v0, LX/82V;->b:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-object v2, v2

    .line 363898
    invoke-static {v2}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 363899
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 363895
    iget-object v0, p0, LX/23H;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;ILjava/lang/String;)Z
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 363887
    iget-object v2, p0, LX/23H;->b:LX/0Uh;

    const/16 v3, 0x301

    invoke-virtual {v2, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 363888
    iget-object v2, p0, LX/23H;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 363889
    if-eqz v2, :cond_1

    .line 363890
    :cond_0
    :goto_1
    return v0

    .line 363891
    :cond_1
    iget-object v2, p0, LX/23H;->c:LX/0W3;

    sget-wide v4, LX/0X5;->eo:J

    invoke-interface {v2, v4, v5, v1}, LX/0W4;->a(JI)I

    move-result v2

    .line 363892
    iget-object v3, p0, LX/23H;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v3, v2, :cond_0

    .line 363893
    iget-object v0, p0, LX/23H;->d:Ljava/util/List;

    new-instance v2, LX/82V;

    invoke-direct {v2, p1, p2, p3}, LX/82V;-><init>(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;ILjava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 363894
    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/82V;",
            ">;"
        }
    .end annotation

    .prologue
    .line 363886
    iget-object v0, p0, LX/23H;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
