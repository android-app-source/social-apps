.class public LX/2Lo;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 395395
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 395396
    return-void
.end method

.method public static a(LX/2Ln;)LX/02u;
    .locals 7
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 395397
    new-instance v0, LX/2Lp;

    invoke-direct {v0}, LX/2Lp;-><init>()V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 395398
    new-instance v1, LX/2Lq;

    .line 395399
    new-instance v5, LX/2Lr;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v6, 0x2fd

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-direct {v5, v2, v3, v4, v6}, LX/2Lr;-><init>(Landroid/content/Context;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V

    .line 395400
    move-object v2, v5

    .line 395401
    check-cast v2, LX/2Lr;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v5

    check-cast v5, LX/11H;

    invoke-static {p0}, LX/2Ls;->a(LX/0QB;)LX/2Ls;

    move-result-object v6

    check-cast v6, LX/2Ls;

    move-object v4, v0

    invoke-direct/range {v1 .. v6}, LX/2Lq;-><init>(LX/2Lr;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/11H;LX/2Ls;)V

    .line 395402
    move-object v0, v1

    .line 395403
    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 395404
    return-void
.end method
