.class public LX/2Z1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0tX;

.field private final d:LX/0SG;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Yj;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/2Dg;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0tX;LX/0SG;LX/0Ot;LX/2Dg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0tX;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/2Yj;",
            ">;",
            "LX/2Dg;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 421863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421864
    iput-object p1, p0, LX/2Z1;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 421865
    iput-object p2, p0, LX/2Z1;->b:LX/0Or;

    .line 421866
    iput-object p3, p0, LX/2Z1;->c:LX/0tX;

    .line 421867
    iput-object p4, p0, LX/2Z1;->d:LX/0SG;

    .line 421868
    iput-object p5, p0, LX/2Z1;->e:LX/0Ot;

    .line 421869
    iput-object p6, p0, LX/2Z1;->f:LX/2Dg;

    .line 421870
    return-void
.end method

.method public static a(LX/0QB;)LX/2Z1;
    .locals 10

    .prologue
    .line 421852
    const-class v1, LX/2Z1;

    monitor-enter v1

    .line 421853
    :try_start_0
    sget-object v0, LX/2Z1;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 421854
    sput-object v2, LX/2Z1;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 421855
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421856
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 421857
    new-instance v3, LX/2Z1;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v5, 0xb83

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    const/16 v8, 0x79

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/2Dg;->a(LX/0QB;)LX/2Dg;

    move-result-object v9

    check-cast v9, LX/2Dg;

    invoke-direct/range {v3 .. v9}, LX/2Z1;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0tX;LX/0SG;LX/0Ot;LX/2Dg;)V

    .line 421858
    move-object v0, v3

    .line 421859
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 421860
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2Z1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 421861
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 421862
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/2Z1;Landroid/os/Bundle;Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 421871
    new-instance v0, LX/2Yl;

    invoke-direct {v0}, LX/2Yl;-><init>()V

    const-string v1, "user_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 421872
    iput-object v1, v0, LX/2Yl;->k:Ljava/lang/String;

    .line 421873
    move-object v0, v0

    .line 421874
    iget-object v1, p0, LX/2Z1;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 421875
    iput-wide v2, v0, LX/2Yl;->j:J

    .line 421876
    move-object v0, v0

    .line 421877
    if-eqz p2, :cond_0

    .line 421878
    invoke-virtual {p2}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    move-result-object v1

    .line 421879
    iput-object v1, v0, LX/2Yl;->a:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 421880
    move-object v1, v0

    .line 421881
    invoke-virtual {p2}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    move-result-object v2

    .line 421882
    iput-object v2, v1, LX/2Yl;->b:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 421883
    move-object v1, v1

    .line 421884
    invoke-virtual {p2}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    move-result-object v2

    .line 421885
    iput-object v2, v1, LX/2Yl;->c:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    .line 421886
    :cond_0
    invoke-virtual {v0}, LX/2Yl;->j()Lcom/facebook/aldrin/status/AldrinUserStatus;

    move-result-object v0

    .line 421887
    iget-object v1, p0, LX/2Z1;->f:LX/2Dg;

    invoke-virtual {v1, v0}, LX/2Dg;->a(Lcom/facebook/aldrin/status/AldrinUserStatus;)V

    .line 421888
    iget-object v1, p0, LX/2Z1;->f:LX/2Dg;

    invoke-virtual {v1}, LX/2Dg;->c()V

    .line 421889
    invoke-static {p0}, LX/2Z1;->a(LX/2Z1;)V

    .line 421890
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2Z1;)V
    .locals 4

    .prologue
    .line 421850
    iget-object v0, p0, LX/2Z1;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2YT;->a:LX/0Tn;

    iget-object v2, p0, LX/2Z1;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, LX/2YT;->c:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/2YT;->b:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 421851
    return-void
.end method

.method public static b(LX/2Z1;)V
    .locals 4

    .prologue
    .line 421848
    iget-object v0, p0, LX/2Z1;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2YT;->b:LX/0Tn;

    iget-object v2, p0, LX/2Z1;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, LX/2YT;->a:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 421849
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    .line 421778
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 421779
    const-string v1, "fetch_aldrin_logged_out_status"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 421780
    :try_start_0
    iget-object v0, p0, LX/2Z1;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, LX/2Z1;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    const/4 v2, 0x0

    .line 421781
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 421782
    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/aldrin/status/AldrinUserStatus;

    .line 421783
    iget-object v1, p0, LX/2Z1;->f:LX/2Dg;

    invoke-virtual {v1, v0}, LX/2Dg;->a(Lcom/facebook/aldrin/status/AldrinUserStatus;)V

    .line 421784
    iget-object v1, p0, LX/2Z1;->f:LX/2Dg;

    invoke-virtual {v1}, LX/2Dg;->c()V

    .line 421785
    invoke-static {p0}, LX/2Z1;->a(LX/2Z1;)V

    .line 421786
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 421787
    :goto_0
    move-object v0, v0

    .line 421788
    :goto_1
    return-object v0

    .line 421789
    :cond_0
    const-string v1, "fetch_region_tos_status"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 421790
    iget-object v4, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v6, v4

    .line 421791
    new-instance v4, LX/GQH;

    invoke-direct {v4}, LX/GQH;-><init>()V

    move-object v4, v4

    .line 421792
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 421793
    iget-object v5, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v5, v5

    .line 421794
    iput-object v5, v4, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 421795
    move-object v4, v4

    .line 421796
    sget-object v5, LX/0zS;->c:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    const-wide/16 v8, 0x258

    invoke-virtual {v4, v8, v9}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    .line 421797
    iget-object v5, p0, LX/2Z1;->c:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 421798
    const v5, -0x61ee8080

    :try_start_1
    invoke-static {v4, v5}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 421799
    const/4 v5, 0x0

    .line 421800
    if-eqz v4, :cond_3

    .line 421801
    iget-object v7, v4, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v7, v7

    .line 421802
    if-eqz v7, :cond_3

    .line 421803
    iget-object v5, v4, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v5

    .line 421804
    check-cast v4, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;

    .line 421805
    :goto_2
    invoke-static {p0, v6, v4}, LX/2Z1;->a(LX/2Z1;Landroid/os/Bundle;Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v4

    .line 421806
    :goto_3
    move-object v0, v4

    .line 421807
    goto :goto_1

    .line 421808
    :cond_1
    const-string v1, "respond_to_region_tos"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 421809
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v0

    .line 421810
    new-instance v0, LX/4J9;

    invoke-direct {v0}, LX/4J9;-><init>()V

    .line 421811
    const-string v1, "region_code"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 421812
    const-string v3, "region_code"

    invoke-virtual {v0, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 421813
    const-string v1, "response_action"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 421814
    const-string v3, "response_action"

    invoke-virtual {v0, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 421815
    const-string v1, "response_version"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 421816
    const-string v3, "response_version"

    invoke-virtual {v0, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 421817
    new-instance v1, LX/GQI;

    invoke-direct {v1}, LX/GQI;-><init>()V

    move-object v1, v1

    .line 421818
    const-string v3, "0"

    invoke-virtual {v1, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 421819
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 421820
    iget-object v1, p0, LX/2Z1;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 421821
    const v1, 0x5b42ee9

    :try_start_2
    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 421822
    const/4 v1, 0x0

    .line 421823
    if-eqz v0, :cond_4

    .line 421824
    iget-object v3, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 421825
    if-eqz v3, :cond_4

    .line 421826
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 421827
    check-cast v0, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosRespondMutationModel;

    invoke-virtual {v0}, Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosRespondMutationModel;->a()Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;

    move-result-object v0

    .line 421828
    :goto_4
    invoke-static {p0, v2, v0}, LX/2Z1;->a(LX/2Z1;Landroid/os/Bundle;Lcom/facebook/aldrin/graphql/AldrinGraphQLModels$RegionTosStatusFragmentModel;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v0

    .line 421829
    :goto_5
    move-object v0, v0

    .line 421830
    goto/16 :goto_1

    .line 421831
    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 421832
    :catch_0
    move-exception v0

    .line 421833
    invoke-static {p0}, LX/2Z1;->b(LX/2Z1;)V

    .line 421834
    const-class v1, LX/2Z1;

    const-string v2, "Error fetching Aldrin logged-out settings"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 421835
    invoke-static {v0}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 421836
    :catch_1
    move-exception v4

    .line 421837
    invoke-static {p0}, LX/2Z1;->b(LX/2Z1;)V

    .line 421838
    invoke-static {v4}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    goto/16 :goto_3

    .line 421839
    :catch_2
    move-exception v4

    .line 421840
    invoke-static {p0}, LX/2Z1;->b(LX/2Z1;)V

    .line 421841
    invoke-static {v4}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    goto/16 :goto_3

    :cond_3
    move-object v4, v5

    goto/16 :goto_2

    .line 421842
    :catch_3
    move-exception v0

    .line 421843
    invoke-static {p0}, LX/2Z1;->b(LX/2Z1;)V

    .line 421844
    invoke-static {v0}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_5

    .line 421845
    :catch_4
    move-exception v0

    .line 421846
    invoke-static {p0}, LX/2Z1;->b(LX/2Z1;)V

    .line 421847
    invoke-static {v0}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_5

    :cond_4
    move-object v0, v1

    goto :goto_4
.end method
