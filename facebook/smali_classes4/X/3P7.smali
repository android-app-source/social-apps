.class public final LX/3P7;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/3OU;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/divebar/contacts/DivebarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V
    .locals 0

    .prologue
    .line 561091
    iput-object p1, p0, LX/3P7;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 561092
    sget-object v0, Lcom/facebook/divebar/contacts/DivebarFragment;->a:Ljava/lang/Class;

    const-string v1, "Failed to load nearby friends row"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 561093
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 561094
    check-cast p1, LX/3OU;

    .line 561095
    iget-object v0, p0, LX/3P7;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    .line 561096
    iget-object p0, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->W:LX/3OU;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 561097
    iput-object p1, v0, Lcom/facebook/divebar/contacts/DivebarFragment;->W:LX/3OU;

    .line 561098
    invoke-static {v0}, Lcom/facebook/divebar/contacts/DivebarFragment;->r(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    .line 561099
    return-void
.end method
