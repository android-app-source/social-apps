.class public LX/3QD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3QD;


# instance fields
.field private final a:LX/0lC;


# direct methods
.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 563386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563387
    iput-object p1, p0, LX/3QD;->a:LX/0lC;

    .line 563388
    return-void
.end method

.method public static a(LX/0QB;)LX/3QD;
    .locals 4

    .prologue
    .line 563389
    sget-object v0, LX/3QD;->b:LX/3QD;

    if-nez v0, :cond_1

    .line 563390
    const-class v1, LX/3QD;

    monitor-enter v1

    .line 563391
    :try_start_0
    sget-object v0, LX/3QD;->b:LX/3QD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 563392
    if-eqz v2, :cond_0

    .line 563393
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 563394
    new-instance p0, LX/3QD;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-direct {p0, v3}, LX/3QD;-><init>(LX/0lC;)V

    .line 563395
    move-object v0, p0

    .line 563396
    sput-object v0, LX/3QD;->b:LX/3QD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 563397
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 563398
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 563399
    :cond_1
    sget-object v0, LX/3QD;->b:LX/3QD;

    return-object v0

    .line 563400
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 563401
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0lF;)Lcom/facebook/notifications/model/SystemTrayNotification;
    .locals 2

    .prologue
    .line 563402
    iget-object v0, p0, LX/3QD;->a:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->a(LX/0lG;)LX/15w;

    move-result-object v0

    .line 563403
    invoke-virtual {v0}, LX/15w;->h()Z

    move-result v1

    if-nez v1, :cond_0

    .line 563404
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 563405
    :cond_0
    const-class v1, Lcom/facebook/notifications/model/SystemTrayNotification;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/SystemTrayNotification;

    return-object v0
.end method
