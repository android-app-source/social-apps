.class public final LX/32c;
.super LX/0Px;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Px",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final transient a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 490411
    invoke-direct {p0}, LX/0Px;-><init>()V

    .line 490412
    iput-object p1, p0, LX/32c;->a:LX/0Px;

    .line 490413
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 490410
    invoke-virtual {p0}, LX/32c;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    return v0
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 490409
    invoke-virtual {p0}, LX/32c;->size()I

    move-result v0

    sub-int/2addr v0, p1

    return v0
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 490408
    iget-object v0, p0, LX/32c;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 490406
    invoke-virtual {p0}, LX/32c;->size()I

    move-result v0

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 490407
    iget-object v0, p0, LX/32c;->a:LX/0Px;

    invoke-direct {p0, p1}, LX/32c;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 490404
    iget-object v0, p0, LX/32c;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    .line 490405
    if-ltz v0, :cond_0

    invoke-direct {p0, v0}, LX/32c;->a(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 490402
    iget-object v0, p0, LX/32c;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Py;->isPartialView()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 490403
    invoke-super {p0}, LX/0Px;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 490400
    iget-object v0, p0, LX/32c;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 490401
    if-ltz v0, :cond_0

    invoke-direct {p0, v0}, LX/32c;->a(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final bridge synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 490393
    invoke-super {p0}, LX/0Px;->listIterator()LX/0Rb;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 490399
    invoke-super {p0, p1}, LX/0Px;->listIterator(I)LX/0Rb;

    move-result-object v0

    return-object v0
.end method

.method public final reverse()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 490398
    iget-object v0, p0, LX/32c;->a:LX/0Px;

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 490397
    iget-object v0, p0, LX/32c;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final subList(II)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 490395
    invoke-virtual {p0}, LX/32c;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, LX/0PB;->checkPositionIndexes(III)V

    .line 490396
    iget-object v0, p0, LX/32c;->a:LX/0Px;

    invoke-direct {p0, p2}, LX/32c;->b(I)I

    move-result v1

    invoke-direct {p0, p1}, LX/32c;->b(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->reverse()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 490394
    invoke-virtual {p0, p1, p2}, LX/32c;->subList(II)LX/0Px;

    move-result-object v0

    return-object v0
.end method
