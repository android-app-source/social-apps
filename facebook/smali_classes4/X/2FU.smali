.class public LX/2FU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2FU;


# instance fields
.field private final a:Landroid/content/pm/PackageManager;

.field private final b:LX/0aq;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 386967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386968
    iput-object p1, p0, LX/2FU;->a:Landroid/content/pm/PackageManager;

    .line 386969
    new-instance v0, LX/0aq;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/2FU;->b:LX/0aq;

    .line 386970
    return-void
.end method

.method public static a(LX/0QB;)LX/2FU;
    .locals 4

    .prologue
    .line 386971
    sget-object v0, LX/2FU;->c:LX/2FU;

    if-nez v0, :cond_1

    .line 386972
    const-class v1, LX/2FU;

    monitor-enter v1

    .line 386973
    :try_start_0
    sget-object v0, LX/2FU;->c:LX/2FU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 386974
    if-eqz v2, :cond_0

    .line 386975
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 386976
    new-instance p0, LX/2FU;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageManager;

    invoke-direct {p0, v3}, LX/2FU;-><init>(Landroid/content/pm/PackageManager;)V

    .line 386977
    move-object v0, p0

    .line 386978
    sput-object v0, LX/2FU;->c:LX/2FU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 386979
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 386980
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 386981
    :cond_1
    sget-object v0, LX/2FU;->c:LX/2FU;

    return-object v0

    .line 386982
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 386983
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 386984
    iget-object v0, p0, LX/2FU;->b:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 386985
    if-eqz v0, :cond_0

    .line 386986
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 386987
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, LX/2FU;->b(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 386988
    const/4 v0, 0x0

    .line 386989
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 386990
    :try_start_0
    iget-object v1, p0, LX/2FU;->a:Landroid/content/pm/PackageManager;

    const/4 v4, 0x0

    invoke-virtual {v1, p1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 386991
    :goto_0
    iget-object v4, p0, LX/2FU;->b:LX/0aq;

    if-eqz v0, :cond_0

    move v1, v2

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, p1, v1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386992
    if-eqz v0, :cond_1

    :goto_2
    return v2

    :cond_0
    move v1, v3

    .line 386993
    goto :goto_1

    :cond_1
    move v2, v3

    .line 386994
    goto :goto_2

    :catch_0
    goto :goto_0
.end method
