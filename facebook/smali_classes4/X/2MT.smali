.class public LX/2MT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private final a:LX/0vX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vX",
            "<",
            "LX/6ed;",
            "LX/1ML;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final b:LX/0vX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vX",
            "<",
            "Ljava/lang/String;",
            "LX/6ed;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final c:LX/0vX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vX",
            "<",
            "LX/6ed;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 396824
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2MT;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396820
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v0

    iput-object v0, p0, LX/2MT;->a:LX/0vX;

    .line 396821
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v0

    iput-object v0, p0, LX/2MT;->b:LX/0vX;

    .line 396822
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v0

    iput-object v0, p0, LX/2MT;->c:LX/0vX;

    .line 396823
    return-void
.end method

.method private static declared-synchronized a(LX/2MT;LX/6ed;)LX/0Rf;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ed;",
            ")",
            "LX/0Rf",
            "<",
            "LX/1ML;",
            ">;"
        }
    .end annotation

    .prologue
    .line 396818
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2MT;->a:LX/0vX;

    invoke-interface {v0, p1}, LX/0vX;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(LX/0QB;)LX/2MT;
    .locals 7

    .prologue
    .line 396789
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 396790
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 396791
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 396792
    if-nez v1, :cond_0

    .line 396793
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 396794
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 396795
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 396796
    sget-object v1, LX/2MT;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 396797
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 396798
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 396799
    :cond_1
    if-nez v1, :cond_4

    .line 396800
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 396801
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 396802
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    .line 396803
    new-instance v0, LX/2MT;

    invoke-direct {v0}, LX/2MT;-><init>()V

    .line 396804
    move-object v1, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 396805
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 396806
    if-nez v1, :cond_2

    .line 396807
    sget-object v0, LX/2MT;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2MT;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 396808
    :goto_1
    if-eqz v0, :cond_3

    .line 396809
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 396810
    :goto_3
    check-cast v0, LX/2MT;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 396811
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 396812
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 396813
    :catchall_1
    move-exception v0

    .line 396814
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 396815
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 396816
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 396817
    :cond_2
    :try_start_8
    sget-object v0, LX/2MT;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2MT;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/2MT;LX/6ed;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 396785
    invoke-direct {p0, p1, p2}, LX/2MT;->b(LX/6ed;Ljava/lang/String;)LX/0Rf;

    move-result-object v0

    .line 396786
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ML;

    .line 396787
    invoke-virtual {v0}, LX/1ML;->cancelOperation()Z

    goto :goto_0

    .line 396788
    :cond_0
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/2MT;LX/6ed;LX/1ML;)V
    .locals 3

    .prologue
    .line 396778
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2MT;->a:LX/0vX;

    invoke-interface {v0, p1, p2}, LX/0Xu;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 396779
    invoke-static {p0, p1}, LX/2MT;->a(LX/2MT;LX/6ed;)LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396780
    iget-object v0, p0, LX/2MT;->c:LX/0vX;

    invoke-interface {v0, p1}, LX/0vX;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 396781
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 396782
    iget-object v2, p0, LX/2MT;->b:LX/0vX;

    invoke-interface {v2, v0, p1}, LX/0Xu;->c(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 396783
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 396784
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private static declared-synchronized b(LX/2MT;LX/6ed;)LX/0Rf;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ed;",
            ")",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 396743
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2MT;->c:LX/0vX;

    invoke-interface {v0, p1}, LX/0vX;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/2MT;Ljava/lang/String;)LX/0Rf;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Rf",
            "<",
            "LX/6ed;",
            ">;"
        }
    .end annotation

    .prologue
    .line 396777
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2MT;->b:LX/0vX;

    invoke-interface {v0, p1}, LX/0vX;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(LX/6ed;Ljava/lang/String;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ed;",
            "Ljava/lang/String;",
            ")",
            "LX/0Rf",
            "<",
            "LX/1ML;",
            ">;"
        }
    .end annotation

    .prologue
    .line 396770
    monitor-enter p0

    .line 396771
    :try_start_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 396772
    invoke-static {p0, p1, p2}, LX/2MT;->d(LX/2MT;LX/6ed;Ljava/lang/String;)V

    .line 396773
    invoke-static {p0, p1}, LX/2MT;->b(LX/2MT;LX/6ed;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 396774
    invoke-static {p0, p1}, LX/2MT;->a(LX/2MT;LX/6ed;)LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 396775
    :cond_0
    monitor-exit p0

    return-object v0

    .line 396776
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized c(LX/2MT;LX/6ed;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 396766
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2MT;->c:LX/0vX;

    invoke-interface {v0, p1, p2}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 396767
    iget-object v0, p0, LX/2MT;->b:LX/0vX;

    invoke-interface {v0, p2, p1}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396768
    monitor-exit p0

    return-void

    .line 396769
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized d(LX/2MT;LX/6ed;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 396762
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2MT;->c:LX/0vX;

    invoke-interface {v0, p1, p2}, LX/0Xu;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 396763
    iget-object v0, p0, LX/2MT;->b:LX/0vX;

    invoke-interface {v0, p2, p1}, LX/0Xu;->c(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396764
    monitor-exit p0

    return-void

    .line 396765
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 3

    .prologue
    .line 396755
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v0

    .line 396756
    invoke-static {p0, v0}, LX/2MT;->a(LX/2MT;LX/6ed;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 396757
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    .line 396758
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 396759
    invoke-static {p0, v0, v1}, LX/2MT;->c(LX/2MT;LX/6ed;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396760
    :cond_0
    monitor-exit p0

    return-void

    .line 396761
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/ui/media/attachments/MediaResource;LX/1ML;)V
    .locals 3

    .prologue
    .line 396747
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v0

    .line 396748
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    .line 396749
    iget-object v2, p0, LX/2MT;->a:LX/0vX;

    invoke-interface {v2, v0, p2}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 396750
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 396751
    invoke-static {p0, v0, v1}, LX/2MT;->c(LX/2MT;LX/6ed;Ljava/lang/String;)V

    .line 396752
    :cond_0
    new-instance v1, Lcom/facebook/messaging/media/upload/MediaOperationManager$1;

    invoke-direct {v1, p0, v0, p2}, Lcom/facebook/messaging/media/upload/MediaOperationManager$1;-><init>(LX/2MT;LX/6ed;LX/1ML;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, LX/0SQ;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396753
    monitor-exit p0

    return-void

    .line 396754
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 2

    .prologue
    .line 396744
    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v0

    .line 396745
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-static {p0, v0, v1}, LX/2MT;->a(LX/2MT;LX/6ed;Ljava/lang/String;)V

    .line 396746
    return-void
.end method
