.class public LX/3Mz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3N0;

.field public final b:LX/3Mx;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Oi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3N0;LX/3Mx;LX/0Or;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/threads/annotations/IsGenieMessagesEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3N0;",
            "LX/3Mx;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Oi;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 557448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 557449
    iput-object p1, p0, LX/3Mz;->a:LX/3N0;

    .line 557450
    iput-object p2, p0, LX/3Mz;->b:LX/3Mx;

    .line 557451
    iput-object p3, p0, LX/3Mz;->c:LX/0Or;

    .line 557452
    iput-object p4, p0, LX/3Mz;->d:LX/0Or;

    .line 557453
    return-void
.end method

.method public static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;LX/6fT;)V
    .locals 3

    .prologue
    .line 557468
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->n()Ljava/lang/String;

    move-result-object v0

    .line 557469
    iput-object v0, p1, LX/6fT;->c:Ljava/lang/String;

    .line 557470
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->dh_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$SourceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 557471
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->dh_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$SourceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$SourceModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 557472
    iput-object v0, p1, LX/6fT;->d:Ljava/lang/String;

    .line 557473
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 557474
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$DescriptionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 557475
    iput-object v0, p1, LX/6fT;->e:Ljava/lang/String;

    .line 557476
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 557477
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->p()Ljava/lang/String;

    move-result-object v0

    .line 557478
    iput-object v0, p1, LX/6fT;->f:Ljava/lang/String;

    .line 557479
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->e()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->e()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 557480
    new-instance v0, LX/6fX;

    invoke-direct {v0}, LX/6fX;-><init>()V

    .line 557481
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->e()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x4ed245b

    if-ne v1, v2, :cond_5

    .line 557482
    sget-object v1, Lcom/facebook/messaging/model/share/ShareMedia$Type;->VIDEO:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    .line 557483
    iput-object v1, v0, LX/6fX;->a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    .line 557484
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->e()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->n()Ljava/lang/String;

    move-result-object v1

    .line 557485
    iput-object v1, v0, LX/6fX;->d:Ljava/lang/String;

    .line 557486
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->e()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->e()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 557487
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->e()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->e()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 557488
    iput-object v1, v0, LX/6fX;->c:Ljava/lang/String;

    .line 557489
    :cond_3
    invoke-virtual {v0}, LX/6fX;->e()Lcom/facebook/messaging/model/share/ShareMedia;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 557490
    iput-object v0, p1, LX/6fT;->g:Ljava/util/List;

    .line 557491
    :cond_4
    return-void

    .line 557492
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->e()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x4984e12

    if-ne v1, v2, :cond_6

    .line 557493
    sget-object v1, Lcom/facebook/messaging/model/share/ShareMedia$Type;->PHOTO:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    .line 557494
    iput-object v1, v0, LX/6fX;->a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    .line 557495
    goto :goto_0

    .line 557496
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->p()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 557497
    sget-object v1, Lcom/facebook/messaging/model/share/ShareMedia$Type;->LINK:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    .line 557498
    iput-object v1, v0, LX/6fX;->a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    .line 557499
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->p()Ljava/lang/String;

    move-result-object v1

    .line 557500
    iput-object v1, v0, LX/6fX;->b:Ljava/lang/String;

    .line 557501
    goto :goto_0

    .line 557502
    :cond_7
    sget-object v1, Lcom/facebook/messaging/model/share/ShareMedia$Type;->UNKNOWN:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    .line 557503
    iput-object v1, v0, LX/6fX;->a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    .line 557504
    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;LX/6f7;)V
    .locals 9

    .prologue
    .line 557505
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x58288c56

    if-ne v0, v1, :cond_f

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 557506
    if-eqz v0, :cond_0

    .line 557507
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    .line 557508
    invoke-interface {v1}, LX/5Wr;->ad()Ljava/lang/String;

    move-result-object v2

    .line 557509
    iget-object v3, p0, LX/3Mz;->b:LX/3Mx;

    const-string v4, "Incomplete P2P data."

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v1}, LX/5Wr;->af()LX/5Wq;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-interface {v1}, LX/5Wr;->ae()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel$ReceiverModel;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-interface {v1}, LX/5Wr;->ac()LX/5Wp;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 557510
    invoke-interface {v1}, LX/5Wr;->af()LX/5Wq;

    move-result-object v3

    invoke-interface {v3}, LX/5Wq;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 557511
    invoke-interface {v1}, LX/5Wr;->ae()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel$ReceiverModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel$ReceiverModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 557512
    invoke-interface {v1}, LX/5Wr;->ac()LX/5Wp;

    move-result-object v7

    invoke-interface {v7}, LX/5Wp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 557513
    invoke-interface {v1}, LX/5Wr;->ac()LX/5Wp;

    move-result-object v1

    invoke-interface {v1}, LX/5Wp;->a()Ljava/lang/String;

    move-result-object v8

    .line 557514
    new-instance v1, Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    invoke-direct/range {v1 .. v8}, Lcom/facebook/messaging/model/payment/PaymentTransactionData;-><init>(Ljava/lang/String;JJILjava/lang/String;)V

    .line 557515
    iput-object v1, p3, LX/6f7;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 557516
    :goto_1
    return-void

    .line 557517
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0xffd29d8

    if-ne v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 557518
    if-eqz v0, :cond_1

    .line 557519
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    .line 557520
    invoke-interface {v1}, LX/5Wo;->Z()Ljava/lang/String;

    move-result-object v2

    .line 557521
    iget-object v3, p0, LX/3Mz;->b:LX/3Mx;

    const-string v4, "Incomplete P2P data."

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v1}, LX/5Wo;->ab()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerPaymentRequestFragmentModel$RequesterModel;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-interface {v1}, LX/5Wo;->aa()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerPaymentRequestFragmentModel$RequesteeModel;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-interface {v1}, LX/5Wo;->Y()LX/5Wn;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 557522
    invoke-interface {v1}, LX/5Wo;->ab()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerPaymentRequestFragmentModel$RequesterModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerPaymentRequestFragmentModel$RequesterModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 557523
    invoke-interface {v1}, LX/5Wo;->aa()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerPaymentRequestFragmentModel$RequesteeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerPaymentRequestFragmentModel$RequesteeModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 557524
    invoke-interface {v1}, LX/5Wo;->Y()LX/5Wn;

    move-result-object v7

    invoke-interface {v7}, LX/5Wn;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 557525
    invoke-interface {v1}, LX/5Wo;->Y()LX/5Wn;

    move-result-object v1

    invoke-interface {v1}, LX/5Wn;->a()Ljava/lang/String;

    move-result-object v8

    .line 557526
    new-instance v1, Lcom/facebook/messaging/model/payment/PaymentRequestData;

    invoke-direct/range {v1 .. v8}, Lcom/facebook/messaging/model/payment/PaymentRequestData;-><init>(Ljava/lang/String;JJILjava/lang/String;)V

    .line 557527
    iput-object v1, p3, LX/6f7;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    .line 557528
    goto :goto_1

    .line 557529
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_11

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_11

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x5176dcdc

    if-eq v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x1a2bf082

    if-eq v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x2b718aa2

    if-ne v0, v1, :cond_11

    :cond_2
    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 557530
    if-eqz v0, :cond_5

    .line 557531
    new-instance v1, LX/6fT;

    invoke-direct {v1}, LX/6fT;-><init>()V

    .line 557532
    invoke-static {p1, v1}, LX/3Mz;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;LX/6fT;)V

    .line 557533
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 557534
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cN()Ljava/lang/String;

    move-result-object v0

    .line 557535
    iput-object v0, v1, LX/6fT;->k:Ljava/lang/String;

    .line 557536
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 557537
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;

    .line 557538
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 557539
    iput-object v2, v1, LX/6fT;->f:Ljava/lang/String;

    .line 557540
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 557541
    iput-object v0, v1, LX/6fT;->j:Ljava/lang/String;

    .line 557542
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    .line 557543
    invoke-static {}, LX/6gB;->newBuilder()LX/6gB;

    move-result-object v2

    invoke-static {v0}, LX/FJD;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/6gB;->a(Ljava/util/List;)LX/6gB;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 557544
    iput-object v2, v0, LX/6gB;->c:Ljava/lang/String;

    .line 557545
    move-object v0, v0

    .line 557546
    invoke-virtual {v0}, LX/6gB;->e()Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    move-result-object v0

    move-object v0, v0

    .line 557547
    iput-object v0, v1, LX/6fT;->m:Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    .line 557548
    invoke-virtual {v1}, LX/6fT;->n()Lcom/facebook/messaging/model/share/Share;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 557549
    iput-object v0, p3, LX/6f7;->j:Ljava/util/List;

    .line 557550
    goto/16 :goto_1

    .line 557551
    :cond_5
    const/4 v0, 0x0

    .line 557552
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-nez v1, :cond_12

    .line 557553
    :cond_6
    :goto_4
    move v0, v0

    .line 557554
    if-eqz v0, :cond_a

    .line 557555
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 557556
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    const/4 v1, 0x0

    .line 557557
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, -0x6251887e

    if-ne v2, v3, :cond_14

    .line 557558
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 557559
    new-instance v4, LX/5TM;

    invoke-direct {v4}, LX/5TM;-><init>()V

    .line 557560
    invoke-interface {v0}, LX/5UW;->c()Ljava/lang/String;

    move-result-object v2

    .line 557561
    iput-object v2, v4, LX/5TM;->a:Ljava/lang/String;

    .line 557562
    invoke-interface {v0}, LX/5UW;->cy_()Ljava/lang/String;

    move-result-object v2

    .line 557563
    iput-object v2, v4, LX/5TM;->b:Ljava/lang/String;

    .line 557564
    invoke-interface {v0}, LX/5UW;->cJ_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/5TM;->e(Ljava/lang/String;)LX/5TM;

    .line 557565
    invoke-interface {v0}, LX/5UW;->cK_()Ljava/lang/String;

    move-result-object v2

    .line 557566
    iput-object v2, v4, LX/5TM;->h:Ljava/lang/String;

    .line 557567
    invoke-interface {v0}, LX/5UW;->cO_()Ljava/lang/String;

    move-result-object v2

    .line 557568
    iput-object v2, v4, LX/5TM;->i:Ljava/lang/String;

    .line 557569
    invoke-interface {v0}, LX/5UW;->cI_()Ljava/lang/String;

    move-result-object v2

    .line 557570
    iput-object v2, v4, LX/5TM;->d:Ljava/lang/String;

    .line 557571
    invoke-interface {v0}, LX/5UW;->cM_()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    move-result-object v2

    invoke-static {v2}, LX/5Sx;->a(Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;)Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-result-object v2

    .line 557572
    iput-object v2, v4, LX/5TM;->g:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 557573
    invoke-interface {v0}, LX/5UW;->cz_()LX/5Uc;

    move-result-object v2

    invoke-static {v2}, LX/5Sx;->a(LX/5Uc;)Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-result-object v2

    .line 557574
    iput-object v2, v4, LX/5TM;->p:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 557575
    invoke-interface {v0}, LX/5UW;->t()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel$RetailItemsModel;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-interface {v0}, LX/5UW;->t()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel$RetailItemsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel$RetailItemsModel;->b()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 557576
    invoke-interface {v0}, LX/5UW;->t()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel$RetailItemsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel$RetailItemsModel;->a()I

    move-result v2

    .line 557577
    iput v2, v4, LX/5TM;->o:I

    .line 557578
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 557579
    invoke-interface {v0}, LX/5UW;->t()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel$RetailItemsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel$RetailItemsModel;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    .line 557580
    const/4 v2, 0x0

    move v3, v2

    :goto_5
    if-ge v3, p1, :cond_7

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5UY;

    .line 557581
    invoke-static {v2}, LX/5Sa;->a(LX/5UY;)Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 557582
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5

    .line 557583
    :cond_7
    iput-object v5, v4, LX/5TM;->q:Ljava/util/List;

    .line 557584
    :cond_8
    invoke-interface {v0}, LX/5UW;->cH_()Ljava/lang/String;

    move-result-object v2

    .line 557585
    iput-object v2, v4, LX/5TM;->u:Ljava/lang/String;

    .line 557586
    invoke-virtual {v4}, LX/5TM;->v()Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    move-result-object v2

    move-object v2, v2

    .line 557587
    :goto_6
    if-eqz v2, :cond_9

    new-instance v1, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    invoke-direct {v1, v2}, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;-><init>(Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;)V

    :cond_9
    move-object v0, v1

    .line 557588
    iput-object v0, p3, LX/6f7;->H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    .line 557589
    goto/16 :goto_1

    .line 557590
    :cond_a
    iget-object v0, p0, LX/3Mz;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 557591
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieStoryAttachmentFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_25

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieStoryAttachmentFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieStoryAttachmentFieldsModel;->a()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_25

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel;->a()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel;

    move-result-object v1

    if-eqz v1, :cond_25

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel;->a()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel;->a()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel;

    move-result-object v0

    if-eqz v0, :cond_25

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_25

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_25

    const/4 v0, 0x1

    :goto_7
    move v0, v0

    .line 557592
    if-eqz v0, :cond_d

    .line 557593
    const/4 v6, 0x1

    .line 557594
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieStoryAttachmentFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieStoryAttachmentFieldsModel;->a()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel;

    move-result-object v1

    .line 557595
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel;->a()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel;->a()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel;

    move-result-object v2

    .line 557596
    new-instance v3, Lcom/facebook/user/model/UserKey;

    sget-object v0, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 557597
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 557598
    iput-object v0, p3, LX/6f7;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 557599
    iget-object v0, p0, LX/3Mz;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oi;

    .line 557600
    if-eqz v0, :cond_c

    invoke-virtual {v0, v3}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v4

    if-nez v4, :cond_c

    .line 557601
    new-instance v4, LX/0XI;

    invoke-direct {v4}, LX/0XI;-><init>()V

    sget-object v5, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v3

    new-instance v4, Lcom/facebook/user/model/Name;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 557602
    iput-object v4, v3, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 557603
    move-object v3, v3

    .line 557604
    iput-boolean v6, v3, LX/0XI;->H:Z

    .line 557605
    move-object v3, v3

    .line 557606
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel;->d()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel$ProfilePhotoModel;

    move-result-object v4

    if-eqz v4, :cond_b

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel;->d()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel$ProfilePhotoModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel$ProfilePhotoModel;->a()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel$ProfilePhotoModel$ImageModel;

    move-result-object v4

    if-eqz v4, :cond_b

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel;->d()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel$ProfilePhotoModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel$ProfilePhotoModel;->a()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel$ProfilePhotoModel$ImageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel$ProfilePhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 557607
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel;->d()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel$ProfilePhotoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel$ProfilePhotoModel;->a()Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel$ProfilePhotoModel$ImageModel;

    move-result-object v2

    .line 557608
    new-instance v4, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel$ProfilePhotoModel$ImageModel;->b()I

    move-result v5

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel$GenieSenderModel$MessagingActorModel$ProfilePhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v5, v2}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    .line 557609
    new-instance v2, Lcom/facebook/user/model/PicSquare;

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/facebook/user/model/PicSquare;-><init>(LX/0Px;)V

    .line 557610
    iput-object v2, v3, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 557611
    :cond_b
    new-array v2, v6, [Lcom/facebook/user/model/User;

    const/4 v4, 0x0

    invoke-virtual {v3}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 557612
    invoke-virtual {v0, v2}, LX/2Oi;->a(Ljava/util/Collection;)V

    .line 557613
    iget-object v0, p0, LX/3Mz;->a:LX/3N0;

    invoke-virtual {v0, v2}, LX/3N0;->a(Ljava/util/List;)V

    .line 557614
    :cond_c
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/GenieMessageQueriesModels$GenieMessageFragmentModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    .line 557615
    if-nez v0, :cond_26

    .line 557616
    const/4 v0, 0x0

    .line 557617
    iput-object v0, p3, LX/6f7;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    .line 557618
    :goto_8
    goto/16 :goto_1

    .line 557619
    :cond_d
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bJ()Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 557620
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bJ()Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;

    move-result-object v0

    const/4 v3, 0x0

    .line 557621
    invoke-static {}, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->newBuilder()LX/5dd;

    move-result-object v1

    .line 557622
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 557623
    iput-object v2, v1, LX/5dd;->c:Ljava/lang/String;

    .line 557624
    move-object v1, v1

    .line 557625
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 557626
    iput-object v2, v1, LX/5dd;->b:Ljava/lang/String;

    .line 557627
    move-object v2, v1

    .line 557628
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    move-result-object v1

    if-eqz v1, :cond_27

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    move-result-object v1

    invoke-static {v1}, LX/5dc;->fromGraphQLMessageAttributionType(Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;)LX/5dc;

    move-result-object v1

    .line 557629
    :goto_9
    iput-object v1, v2, LX/5dd;->g:LX/5dc;

    .line 557630
    move-object v1, v2

    .line 557631
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 557632
    iput-object v2, v1, LX/5dd;->c:Ljava/lang/String;

    .line 557633
    move-object v1, v1

    .line 557634
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 557635
    iput-object v2, v1, LX/5dd;->h:Ljava/lang/String;

    .line 557636
    move-object v1, v1

    .line 557637
    invoke-static {}, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->newBuilder()LX/5dZ;

    move-result-object v2

    .line 557638
    iput-boolean v3, v2, LX/5dZ;->a:Z

    .line 557639
    move-object v2, v2

    .line 557640
    iput-boolean v3, v2, LX/5dZ;->b:Z

    .line 557641
    move-object v2, v2

    .line 557642
    invoke-virtual {v2}, LX/5dZ;->h()Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    move-result-object v2

    .line 557643
    iput-object v2, v1, LX/5dd;->j:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    .line 557644
    move-object v1, v1

    .line 557645
    invoke-virtual {v1}, LX/5dd;->k()Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-result-object v1

    move-object v0, v1

    .line 557646
    iput-object v0, p3, LX/6f7;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 557647
    :cond_e
    new-instance v0, LX/6fT;

    invoke-direct {v0}, LX/6fT;-><init>()V

    .line 557648
    invoke-static {p1, v0}, LX/3Mz;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;LX/6fT;)V

    .line 557649
    invoke-virtual {v0}, LX/6fT;->n()Lcom/facebook/messaging/model/share/Share;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 557650
    iput-object v0, p3, LX/6f7;->j:Ljava/util/List;

    .line 557651
    goto/16 :goto_1

    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 557652
    :cond_12
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    .line 557653
    const v2, -0x6251887e

    if-eq v1, v2, :cond_13

    const v2, -0x3df9be07

    if-eq v1, v2, :cond_13

    const v2, 0x298e1590

    if-eq v1, v2, :cond_13

    const v2, 0x214fa273

    if-eq v1, v2, :cond_13

    const v2, 0x600959de

    if-eq v1, v2, :cond_13

    const v2, 0x6e5c4fbc

    if-ne v1, v2, :cond_6

    :cond_13
    const/4 v0, 0x1

    goto/16 :goto_4

    .line 557654
    :cond_14
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, -0x3df9be07

    if-ne v2, v3, :cond_18

    .line 557655
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 557656
    new-instance v4, LX/5TO;

    invoke-direct {v4}, LX/5TO;-><init>()V

    .line 557657
    invoke-interface {v0}, LX/5UV;->c()Ljava/lang/String;

    move-result-object v2

    .line 557658
    iput-object v2, v4, LX/5TO;->a:Ljava/lang/String;

    .line 557659
    invoke-interface {v0}, LX/5UV;->s()LX/5Sy;

    move-result-object v2

    .line 557660
    if-nez v2, :cond_20

    .line 557661
    const/4 v3, 0x0

    .line 557662
    :goto_a
    move-object v2, v3

    .line 557663
    if-eqz v2, :cond_15

    .line 557664
    invoke-virtual {v2}, LX/5TM;->v()Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    move-result-object v2

    .line 557665
    iput-object v2, v4, LX/5TO;->b:Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    .line 557666
    :cond_15
    invoke-interface {v0}, LX/5UV;->r()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderCancellationBubbleModel$CancelledItemsModel;

    move-result-object v2

    if-eqz v2, :cond_17

    .line 557667
    invoke-interface {v0}, LX/5UV;->r()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderCancellationBubbleModel$CancelledItemsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderCancellationBubbleModel$CancelledItemsModel;->a()I

    move-result v2

    .line 557668
    iput v2, v4, LX/5TO;->c:I

    .line 557669
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 557670
    invoke-interface {v0}, LX/5UV;->r()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderCancellationBubbleModel$CancelledItemsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderCancellationBubbleModel$CancelledItemsModel;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    .line 557671
    const/4 v2, 0x0

    move v3, v2

    :goto_b
    if-ge v3, p1, :cond_16

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5UY;

    .line 557672
    invoke-static {v2}, LX/5Sa;->a(LX/5UY;)Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 557673
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_b

    .line 557674
    :cond_16
    iput-object v5, v4, LX/5TO;->d:Ljava/util/List;

    .line 557675
    :cond_17
    invoke-virtual {v4}, LX/5TO;->e()Lcom/facebook/messaging/business/commerce/model/retail/ReceiptCancellation;

    move-result-object v2

    move-object v2, v2

    .line 557676
    goto/16 :goto_6

    .line 557677
    :cond_18
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x298e1590

    if-ne v2, v3, :cond_19

    .line 557678
    invoke-static {v0}, LX/5Sx;->a(LX/5Sz;)Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    move-result-object v2

    goto/16 :goto_6

    .line 557679
    :cond_19
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x214fa273

    if-ne v2, v3, :cond_1a

    .line 557680
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 557681
    invoke-static {v0}, LX/5Sx;->a(LX/5UU;)LX/5TX;

    move-result-object v2

    .line 557682
    if-nez v2, :cond_21

    .line 557683
    const/4 v2, 0x0

    .line 557684
    :goto_c
    move-object v2, v2

    .line 557685
    goto/16 :goto_6

    .line 557686
    :cond_1a
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x600959de

    if-ne v2, v3, :cond_1c

    .line 557687
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 557688
    invoke-interface {v0}, LX/5UZ;->v()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceProductSubscriptionBubbleModel$SubscribedItemModel;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 557689
    invoke-interface {v0}, LX/5UZ;->v()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceProductSubscriptionBubbleModel$SubscribedItemModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceProductSubscriptionBubbleModel$SubscribedItemModel;->a()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceProductSubscriptionBubbleModel$SubscribedItemModel$NodesModel;

    .line 557690
    new-instance v3, LX/5TZ;

    invoke-direct {v3}, LX/5TZ;-><init>()V

    invoke-interface {v0}, LX/5UZ;->c()Ljava/lang/String;

    move-result-object v4

    .line 557691
    iput-object v4, v3, LX/5TZ;->a:Ljava/lang/String;

    .line 557692
    move-object v3, v3

    .line 557693
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceProductSubscriptionBubbleModel$SubscribedItemModel$NodesModel;->cN_()Ljava/lang/String;

    move-result-object v4

    .line 557694
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_23

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    :goto_d
    iput-object v5, v3, LX/5TZ;->c:Landroid/net/Uri;

    .line 557695
    move-object v3, v3

    .line 557696
    invoke-static {v2}, LX/5Sa;->a(LX/5UY;)Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    move-result-object v2

    .line 557697
    iput-object v2, v3, LX/5TZ;->d:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    .line 557698
    move-object v2, v3

    .line 557699
    invoke-interface {v0}, LX/5UZ;->u()LX/5UX;

    move-result-object v3

    if-eqz v3, :cond_1b

    .line 557700
    new-instance v3, LX/5Sq;

    invoke-direct {v3}, LX/5Sq;-><init>()V

    invoke-interface {v0}, LX/5UZ;->u()LX/5UX;

    move-result-object v4

    invoke-interface {v4}, LX/5UX;->a()I

    move-result v4

    .line 557701
    iput v4, v3, LX/5Sq;->c:I

    .line 557702
    move-object v3, v3

    .line 557703
    invoke-interface {v0}, LX/5UZ;->u()LX/5UX;

    move-result-object v4

    invoke-interface {v4}, LX/5UX;->c()I

    move-result v4

    .line 557704
    iput v4, v3, LX/5Sq;->b:I

    .line 557705
    move-object v3, v3

    .line 557706
    invoke-interface {v0}, LX/5UZ;->u()LX/5UX;

    move-result-object v4

    invoke-interface {v4}, LX/5UX;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/5Sq;->a(Ljava/lang/String;)LX/5Sq;

    move-result-object v3

    invoke-virtual {v3}, LX/5Sq;->d()Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-result-object v3

    .line 557707
    iput-object v3, v2, LX/5TZ;->b:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 557708
    :cond_1b
    new-instance v3, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;

    invoke-direct {v3, v2}, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;-><init>(LX/5TZ;)V

    move-object v2, v3

    .line 557709
    move-object v2, v2

    .line 557710
    goto/16 :goto_6

    .line 557711
    :cond_1c
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x6e5c4fbc

    if-ne v2, v3, :cond_1f

    .line 557712
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 557713
    new-instance v2, LX/5Sw;

    invoke-direct {v2}, LX/5Sw;-><init>()V

    .line 557714
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 557715
    iput-object v3, v2, LX/5Sw;->a:Ljava/lang/String;

    .line 557716
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 557717
    iput-object v3, v2, LX/5Sw;->b:Ljava/lang/String;

    .line 557718
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aM()Ljava/lang/String;

    move-result-object v3

    .line 557719
    iput-object v3, v2, LX/5Sw;->h:Ljava/lang/String;

    .line 557720
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cD_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5Sw;->d(Ljava/lang/String;)LX/5Sw;

    .line 557721
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cq()Ljava/lang/String;

    move-result-object v3

    .line 557722
    iput-object v3, v2, LX/5Sw;->i:Ljava/lang/String;

    .line 557723
    new-instance v3, LX/5TI;

    invoke-direct {v3}, LX/5TI;-><init>()V

    .line 557724
    invoke-virtual {v2}, LX/5Sw;->p()Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    move-result-object v2

    .line 557725
    iput-object v2, v3, LX/5TI;->a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    .line 557726
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cN_()Ljava/lang/String;

    move-result-object v2

    .line 557727
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_24

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    :goto_e
    iput-object v4, v3, LX/5TI;->b:Landroid/net/Uri;

    .line 557728
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ch()Ljava/lang/String;

    move-result-object v2

    .line 557729
    iput-object v2, v3, LX/5TI;->e:Ljava/lang/String;

    .line 557730
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ci()Ljava/lang/String;

    move-result-object v2

    .line 557731
    iput-object v2, v3, LX/5TI;->f:Ljava/lang/String;

    .line 557732
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ej()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PaymentModel;

    move-result-object v2

    if-eqz v2, :cond_1e

    .line 557733
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ej()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PaymentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PaymentModel;->c()Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferStatus;

    move-result-object v2

    if-eqz v2, :cond_1d

    .line 557734
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ej()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PaymentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PaymentModel;->c()Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferStatus;->toString()Ljava/lang/String;

    move-result-object v2

    .line 557735
    iput-object v2, v3, LX/5TI;->c:Ljava/lang/String;

    .line 557736
    :cond_1d
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ej()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PaymentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PaymentModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1e

    .line 557737
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ej()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PaymentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PaymentModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 557738
    iput-object v2, v3, LX/5TI;->d:Ljava/lang/String;

    .line 557739
    :cond_1e
    new-instance v2, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;

    invoke-direct {v2, v3}, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;-><init>(LX/5TI;)V

    move-object v2, v2

    .line 557740
    move-object v2, v2

    .line 557741
    goto/16 :goto_6

    .line 557742
    :cond_1f
    const/4 v2, 0x0

    const-string v3, "Unsupported graphql model."

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    move-object v2, v1

    goto/16 :goto_6

    .line 557743
    :cond_20
    new-instance v3, LX/5TM;

    invoke-direct {v3}, LX/5TM;-><init>()V

    .line 557744
    invoke-interface {v2}, LX/5Sy;->c()Ljava/lang/String;

    move-result-object v5

    .line 557745
    iput-object v5, v3, LX/5TM;->a:Ljava/lang/String;

    .line 557746
    invoke-interface {v2}, LX/5Sy;->cy_()Ljava/lang/String;

    move-result-object v5

    .line 557747
    iput-object v5, v3, LX/5TM;->b:Ljava/lang/String;

    .line 557748
    invoke-interface {v2}, LX/5Sy;->cJ_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/5TM;->e(Ljava/lang/String;)LX/5TM;

    .line 557749
    invoke-interface {v2}, LX/5Sy;->cK_()Ljava/lang/String;

    move-result-object v5

    .line 557750
    iput-object v5, v3, LX/5TM;->h:Ljava/lang/String;

    .line 557751
    invoke-interface {v2}, LX/5Sy;->cO_()Ljava/lang/String;

    move-result-object v5

    .line 557752
    iput-object v5, v3, LX/5TM;->i:Ljava/lang/String;

    .line 557753
    invoke-interface {v2}, LX/5Sy;->cz_()LX/5Uc;

    move-result-object v5

    invoke-static {v5}, LX/5Sx;->a(LX/5Uc;)Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-result-object v5

    .line 557754
    iput-object v5, v3, LX/5TM;->p:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 557755
    invoke-interface {v2}, LX/5Sy;->cH_()Ljava/lang/String;

    move-result-object v5

    .line 557756
    iput-object v5, v3, LX/5TM;->u:Ljava/lang/String;

    .line 557757
    goto/16 :goto_a

    .line 557758
    :cond_21
    invoke-interface {v0}, LX/5Ub;->J()LX/5Sz;

    move-result-object v3

    if-eqz v3, :cond_22

    .line 557759
    invoke-interface {v0}, LX/5Ub;->J()LX/5Sz;

    move-result-object v3

    invoke-static {v3}, LX/5Sx;->a(LX/5Sz;)Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    move-result-object v3

    .line 557760
    iput-object v3, v2, LX/5TX;->g:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    .line 557761
    :cond_22
    invoke-virtual {v2}, LX/5TX;->h()Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;

    move-result-object v2

    goto/16 :goto_c

    .line 557762
    :cond_23
    const/4 v5, 0x0

    goto/16 :goto_d

    .line 557763
    :cond_24
    const/4 v4, 0x0

    goto/16 :goto_e

    :cond_25
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 557764
    :cond_26
    new-instance v1, LX/5Zl;

    invoke-direct {v1}, LX/5Zl;-><init>()V

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 557765
    iput-object v2, v1, LX/5Zl;->b:Ljava/lang/String;

    .line 557766
    move-object v1, v1

    .line 557767
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->k()Z

    move-result v2

    .line 557768
    iput-boolean v2, v1, LX/5Zl;->c:Z

    .line 557769
    move-object v1, v1

    .line 557770
    invoke-static {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    .line 557771
    iput-object v0, v1, LX/5Zl;->d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    .line 557772
    move-object v0, v1

    .line 557773
    invoke-virtual {v0}, LX/5Zl;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v0

    .line 557774
    iput-object v0, p3, LX/6f7;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    .line 557775
    goto/16 :goto_8

    :cond_27
    sget-object v1, LX/5dc;->UNRECOGNIZED:LX/5dc;

    goto/16 :goto_9
.end method

.method public static b(LX/0QB;)LX/3Mz;
    .locals 5

    .prologue
    .line 557466
    new-instance v2, LX/3Mz;

    invoke-static {p0}, LX/3N0;->a(LX/0QB;)LX/3N0;

    move-result-object v0

    check-cast v0, LX/3N0;

    invoke-static {p0}, LX/3Mx;->b(LX/0QB;)LX/3Mx;

    move-result-object v1

    check-cast v1, LX/3Mx;

    const/16 v3, 0x1531

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x12c7

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v2, v0, v1, v3, v4}, LX/3Mz;-><init>(LX/3N0;LX/3Mx;LX/0Or;LX/0Or;)V

    .line 557467
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;LX/6f7;)V
    .locals 3
    .param p1    # Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 557454
    if-nez p1, :cond_0

    .line 557455
    :goto_0
    return-void

    .line 557456
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    .line 557457
    if-nez v1, :cond_1

    .line 557458
    iget-object v0, p0, LX/3Mz;->b:LX/3Mx;

    new-instance v1, Ljava/io/InvalidObjectException;

    const-string v2, "XMA doesn\'t contain a story attachment."

    invoke-direct {v1, v2}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, LX/3Mx;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;Ljava/lang/Exception;)V

    goto :goto_0

    .line 557459
    :cond_1
    iput-object p1, p2, LX/6f7;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    .line 557460
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->k()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 557461
    :goto_1
    iput-boolean v0, p2, LX/6f7;->h:Z

    .line 557462
    :try_start_0
    invoke-direct {p0, v1, p1, p2}, LX/3Mz;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;LX/6f7;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 557463
    :catch_0
    move-exception v0

    .line 557464
    iget-object v1, p0, LX/3Mz;->b:LX/3Mx;

    invoke-virtual {v1, p1, v0}, LX/3Mx;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;Ljava/lang/Exception;)V

    goto :goto_0

    .line 557465
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
