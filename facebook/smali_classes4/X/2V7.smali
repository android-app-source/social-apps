.class public LX/2V7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 417156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417157
    return-void
.end method

.method public static b(LX/0QB;)LX/2V7;
    .locals 4

    .prologue
    .line 417158
    new-instance v2, LX/2V7;

    invoke-direct {v2}, LX/2V7;-><init>()V

    .line 417159
    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    const/16 v3, 0xdf4

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    .line 417160
    iput-object v0, v2, LX/2V7;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v1, v2, LX/2V7;->b:LX/0SG;

    iput-object v3, v2, LX/2V7;->c:LX/0Or;

    .line 417161
    return-object v2
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const-wide/16 v6, 0x0

    .line 417162
    iget-object v0, p0, LX/2V7;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    .line 417163
    sget-wide v4, LX/0X5;->gg:J

    invoke-interface {v0, v4, v5}, LX/0W4;->c(J)J

    move-result-wide v4

    .line 417164
    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v0, v2

    .line 417165
    :goto_0
    return v0

    .line 417166
    :cond_0
    iget-object v0, p0, LX/2V7;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2V4;->s:LX/0Tn;

    invoke-interface {v0, v1, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 417167
    cmp-long v3, v0, v6

    if-nez v3, :cond_1

    .line 417168
    iget-object v0, p0, LX/2V7;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 417169
    iget-object v3, p0, LX/2V7;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v6, LX/2V4;->s:LX/0Tn;

    invoke-interface {v3, v6, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 417170
    :cond_1
    iget-object v3, p0, LX/2V7;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    sub-long v0, v6, v0

    const-wide/32 v6, 0x5265c00

    mul-long/2addr v4, v6

    cmp-long v0, v0, v4

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method
