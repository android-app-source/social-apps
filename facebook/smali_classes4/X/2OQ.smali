.class public LX/2OQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/2OP;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6fD;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DdV;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2Mk;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DdG;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Uh;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oh;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/2OW;

.field private final l:LX/2OY;

.field private final m:LX/2OZ;

.field private final n:LX/2Oa;

.field private final o:LX/2Ob;

.field public final p:LX/2Oc;

.field public final q:LX/2Oc;

.field private final r:LX/2OR;

.field public final s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "LX/6ek;",
            "LX/DdE;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mThreadsCacheLock"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 401577
    const-class v0, LX/2OQ;

    sput-object v0, LX/2OQ;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2OP;LX/0Ot;LX/0Ot;LX/2Mk;LX/0Or;LX/0Or;LX/0Ot;LX/0Uh;LX/0Ot;LX/2OR;LX/0Or;)V
    .locals 2
    .param p1    # LX/2OP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerSyncEnabled;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2OP;",
            "LX/0Ot",
            "<",
            "LX/6fD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DdV;",
            ">;",
            "LX/2Mk;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DdG;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/2Oh;",
            ">;",
            "LX/2OR;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 401456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401457
    iput-object p1, p0, LX/2OQ;->b:LX/2OP;

    .line 401458
    iput-object p2, p0, LX/2OQ;->c:LX/0Ot;

    .line 401459
    iput-object p3, p0, LX/2OQ;->d:LX/0Ot;

    .line 401460
    iput-object p4, p0, LX/2OQ;->e:LX/2Mk;

    .line 401461
    iput-object p5, p0, LX/2OQ;->f:LX/0Or;

    .line 401462
    iput-object p6, p0, LX/2OQ;->g:LX/0Or;

    .line 401463
    iput-object p7, p0, LX/2OQ;->h:LX/0Ot;

    .line 401464
    iput-object p8, p0, LX/2OQ;->i:LX/0Uh;

    .line 401465
    iput-object p9, p0, LX/2OQ;->j:LX/0Ot;

    .line 401466
    new-instance v0, LX/2OW;

    invoke-direct {v0}, LX/2OW;-><init>()V

    iput-object v0, p0, LX/2OQ;->k:LX/2OW;

    .line 401467
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/2OQ;->t:LX/01J;

    .line 401468
    new-instance v0, LX/2OY;

    iget-object v1, p0, LX/2OQ;->k:LX/2OW;

    invoke-direct {v0, v1}, LX/2OY;-><init>(LX/2OW;)V

    iput-object v0, p0, LX/2OQ;->l:LX/2OY;

    .line 401469
    new-instance v0, LX/2OZ;

    iget-object v1, p0, LX/2OQ;->k:LX/2OW;

    invoke-direct {v0, v1}, LX/2OZ;-><init>(LX/2OW;)V

    iput-object v0, p0, LX/2OQ;->m:LX/2OZ;

    .line 401470
    new-instance v0, LX/2Oa;

    iget-object v1, p0, LX/2OQ;->k:LX/2OW;

    invoke-direct {v0, v1}, LX/2Oa;-><init>(LX/2OW;)V

    iput-object v0, p0, LX/2OQ;->n:LX/2Oa;

    .line 401471
    new-instance v0, LX/2Ob;

    iget-object v1, p0, LX/2OQ;->k:LX/2OW;

    invoke-direct {v0, v1}, LX/2Ob;-><init>(LX/2OW;)V

    iput-object v0, p0, LX/2OQ;->o:LX/2Ob;

    .line 401472
    new-instance v0, LX/2Oc;

    iget-object v1, p0, LX/2OQ;->k:LX/2OW;

    invoke-direct {v0, v1}, LX/2Oc;-><init>(LX/2OW;)V

    iput-object v0, p0, LX/2OQ;->p:LX/2Oc;

    .line 401473
    new-instance v0, LX/2Oc;

    iget-object v1, p0, LX/2OQ;->k:LX/2OW;

    invoke-direct {v0, v1}, LX/2Oc;-><init>(LX/2OW;)V

    iput-object v0, p0, LX/2OQ;->q:LX/2Oc;

    .line 401474
    iput-object p10, p0, LX/2OQ;->r:LX/2OR;

    .line 401475
    iput-object p11, p0, LX/2OQ;->s:LX/0Or;

    .line 401476
    return-void
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    .locals 1
    .param p2    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 401453
    if-nez p2, :cond_0

    .line 401454
    invoke-static {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object p2

    .line 401455
    :cond_0
    iget-object v0, p0, LX/2OQ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6fD;

    invoke-virtual {v0, p2, p3}, LX/6fD;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2OQ;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # LX/2OQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 401446
    iget-object v0, p0, LX/2OQ;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 401447
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move-object p2, v1

    .line 401448
    :cond_1
    :goto_0
    return-object p2

    .line 401449
    :cond_2
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 401450
    :cond_3
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move-object p2, p1

    .line 401451
    goto :goto_0

    :cond_4
    move-object p2, v1

    .line 401452
    goto :goto_0
.end method

.method private static a(LX/2OQ;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLX/DdQ;)V
    .locals 9
    .param p0    # LX/2OQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mThreadsCacheLock"
    .end annotation

    .prologue
    .line 401444
    sget-object v7, LX/6jT;->a:LX/6jT;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v8}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLX/DdQ;LX/6jT;Ljava/lang/Boolean;)V

    .line 401445
    return-void
.end method

.method public static a(LX/2OQ;Ljava/util/List;LX/2Oc;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;",
            "LX/2Oc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 401431
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401432
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 401433
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v0}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 401434
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401435
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v2, :cond_0

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_2
    throw v0

    .line 401436
    :cond_1
    :try_start_3
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v0, p1}, LX/2OY;->a(Ljava/lang/Iterable;)V

    .line 401437
    sget-object v0, LX/DdX;->a:LX/0QK;

    invoke-static {p1, v0}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v0

    .line 401438
    iget-object v3, p2, LX/2Oc;->a:LX/2OW;

    invoke-virtual {v3}, LX/2OW;->b()V

    .line 401439
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v3, p2, LX/2Oc;->b:Ljava/util/List;

    .line 401440
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/2Oc;->a(Z)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 401441
    if-eqz v2, :cond_2

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401442
    :cond_2
    return-void

    .line 401443
    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLX/DdQ;LX/6jT;Ljava/lang/Boolean;)V
    .locals 19
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mThreadsCacheLock"
    .end annotation

    .prologue
    .line 401397
    if-nez p1, :cond_0

    .line 401398
    :goto_0
    return-void

    .line 401399
    :cond_0
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401400
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object/from16 v17, v0

    .line 401401
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2OQ;->l:LX/2OY;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, LX/2OY;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/DdS;

    move-result-object v3

    .line 401402
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2OQ;->l:LX/2OY;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, LX/2OY;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v9

    .line 401403
    if-nez v9, :cond_1

    .line 401404
    invoke-direct/range {p0 .. p0}, LX/2OQ;->k()V

    .line 401405
    invoke-virtual {v3}, LX/DdS;->b()V

    goto :goto_0

    .line 401406
    :cond_1
    invoke-static/range {p1 .. p1}, LX/2Mk;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v5

    .line 401407
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2OQ;->m:LX/2OZ;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, LX/2OZ;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v11

    .line 401408
    if-nez v11, :cond_4

    .line 401409
    new-instance v11, Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v4

    const/4 v6, 0x0

    move-object/from16 v0, v17

    invoke-direct {v11, v0, v4, v6}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    .line 401410
    invoke-direct/range {p0 .. p0}, LX/2OQ;->k()V

    .line 401411
    invoke-virtual {v3}, LX/DdS;->b()V

    :cond_2
    :goto_1
    move-object/from16 v4, p0

    move-object/from16 v6, p1

    move-wide/from16 v7, p3

    move-object/from16 v10, p2

    .line 401412
    invoke-direct/range {v4 .. v11}, LX/2OQ;->a(ZLcom/facebook/messaging/model/messages/Message;JLcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Z

    move-result v4

    .line 401413
    if-eqz v4, :cond_3

    .line 401414
    invoke-direct/range {p0 .. p0}, LX/2OQ;->k()V

    .line 401415
    invoke-virtual {v3}, LX/DdS;->b()V

    .line 401416
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2OQ;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DdG;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, LX/DdG;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 401417
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v11}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v4

    .line 401418
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v11, v4}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)V

    .line 401419
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2OQ;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/DdV;

    move-object/from16 v10, p1

    move-object v11, v4

    move-wide/from16 v12, p3

    move-object/from16 v14, p5

    move-object/from16 v15, p6

    move-object/from16 v16, p7

    invoke-virtual/range {v8 .. v16}, LX/DdV;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLX/DdQ;LX/6jT;Ljava/lang/Boolean;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v5

    .line 401420
    iget-object v3, v5, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/2OQ;->f(LX/2OQ;LX/6ek;)LX/DdE;

    move-result-object v3

    .line 401421
    move-object/from16 v0, p0

    iget-object v6, v0, LX/2OQ;->l:LX/2OY;

    iget-object v7, v9, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v6, v7}, LX/2OY;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401422
    move-object/from16 v0, p0

    iget-object v6, v0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v6, v5}, LX/2OY;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401423
    invoke-virtual {v3, v5}, LX/DdE;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401424
    move-object/from16 v0, p0

    iget-object v6, v0, LX/2OQ;->m:LX/2OZ;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2OQ;->f:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    invoke-virtual {v6, v4, v3}, LX/2OZ;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/user/model/User;)V

    .line 401425
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401426
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/2OQ;->k(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401427
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, LX/2OQ;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    goto/16 :goto_0

    .line 401428
    :cond_4
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-eqz v4, :cond_2

    if-nez v5, :cond_2

    .line 401429
    invoke-direct/range {p0 .. p0}, LX/2OQ;->k()V

    .line 401430
    invoke-virtual {v3}, LX/DdS;->b()V

    goto/16 :goto_1
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)V
    .locals 4
    .param p2    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x5

    .line 401385
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 401386
    :goto_0
    return-void

    .line 401387
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 401388
    iget-object v1, p0, LX/2OQ;->b:LX/2OP;

    iget-object v1, v1, LX/2OP;->logName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Merged messages:\n  New Message:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401389
    invoke-static {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/2OQ;->a(Ljava/lang/StringBuilder;Lcom/facebook/messaging/model/messages/MessagesCollection;I)V

    .line 401390
    const-string v1, "  Recent Messages:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401391
    invoke-static {v0, p2, v3}, LX/2OQ;->a(Ljava/lang/StringBuilder;Lcom/facebook/messaging/model/messages/MessagesCollection;I)V

    .line 401392
    const-string v1, "  Loaded Messages:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401393
    invoke-static {v0, p3, v3}, LX/2OQ;->a(Ljava/lang/StringBuilder;Lcom/facebook/messaging/model/messages/MessagesCollection;I)V

    .line 401394
    const-string v1, "  Result:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401395
    const/16 v1, 0x8

    invoke-static {v0, p4, v1}, LX/2OQ;->a(Ljava/lang/StringBuilder;Lcom/facebook/messaging/model/messages/MessagesCollection;I)V

    .line 401396
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 8

    .prologue
    .line 401367
    invoke-static {p1}, LX/2Mk;->z(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 401368
    :goto_0
    return-void

    .line 401369
    :cond_0
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 401370
    iget-object v3, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->L:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 401371
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 401372
    iget-object v6, p0, LX/2OQ;->o:LX/2Ob;

    .line 401373
    iget-object v7, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v7, v7

    .line 401374
    invoke-virtual {v6, v7}, LX/2Ob;->a(Ljava/lang/String;)LX/0P1;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 401375
    iget-object v6, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->f:LX/0P1;

    move-object v6, v6

    .line 401376
    if-eqz v6, :cond_1

    .line 401377
    iget-object v6, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->f:LX/0P1;

    move-object v6, v6

    .line 401378
    invoke-virtual {v5, v6}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 401379
    :cond_1
    new-instance v6, LX/6ft;

    invoke-direct {v6, v0}, LX/6ft;-><init>(Lcom/facebook/messaging/model/threads/ThreadEventReminder;)V

    .line 401380
    invoke-static {v5}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    .line 401381
    iput-object v0, v6, LX/6ft;->f:LX/0P1;

    .line 401382
    invoke-virtual {v6}, LX/6ft;->i()Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 401383
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 401384
    :cond_2
    iget-object v0, p0, LX/2OQ;->o:LX/2Ob;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Ob;->a(LX/0Px;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/2OP;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 401276
    iget-object v0, p0, LX/2OQ;->b:LX/2OP;

    if-ne v0, p2, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Tried to use %s in %s cache"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    iget-object v2, p0, LX/2OQ;->b:LX/2OP;

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 401277
    return-void

    :cond_0
    move v0, v2

    .line 401278
    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 3
    .param p2    # Lcom/facebook/messaging/model/threads/ThreadSummary;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 401347
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 401348
    :cond_0
    :goto_0
    return-void

    .line 401349
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadSummary;->toString()Ljava/lang/String;

    move-result-object v0

    .line 401350
    if-eqz p2, :cond_2

    .line 401351
    invoke-virtual {p2}, Lcom/facebook/messaging/model/threads/ThreadSummary;->toString()Ljava/lang/String;

    move-result-object v1

    .line 401352
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 401353
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 401354
    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2OQ;->b:LX/2OP;

    iget-object v2, v2, LX/2OP;->logName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ThreadSummary: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static a(Ljava/lang/StringBuilder;Lcom/facebook/messaging/model/messages/MessagesCollection;I)V
    .locals 3

    .prologue
    .line 401340
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 401341
    :cond_0
    const-string v0, "    none\n"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401342
    :cond_1
    return-void

    .line 401343
    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_1

    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->g()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 401344
    invoke-virtual {p1, v0}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b(I)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 401345
    const-string v2, "   "

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401346
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 12
    .param p2    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 401330
    iget-object v2, p0, LX/2OQ;->i:LX/0Uh;

    const/16 v3, 0x136

    invoke-virtual {v2, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 401331
    :cond_0
    :goto_0
    return v0

    .line 401332
    :cond_1
    const/4 v6, 0x1

    .line 401333
    if-nez p2, :cond_5

    if-nez p1, :cond_5

    .line 401334
    :cond_2
    :goto_1
    move v2, v6

    .line 401335
    if-nez v2, :cond_0

    .line 401336
    if-eqz p1, :cond_3

    if-nez p2, :cond_4

    :cond_3
    move v0, v1

    .line 401337
    goto :goto_0

    .line 401338
    :cond_4
    iget-wide v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->f:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 401339
    iget-wide v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->f:J

    iget-wide v4, p2, Lcom/facebook/messaging/model/messages/Message;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    if-eqz p1, :cond_6

    if-nez p2, :cond_6

    iget-wide v8, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->f:J

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_2

    :cond_6
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private a(ZLcom/facebook/messaging/model/messages/Message;JLcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Z
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 401316
    if-eqz p1, :cond_0

    move v2, v3

    .line 401317
    :goto_0
    return v2

    .line 401318
    :cond_0
    iget-boolean v2, p2, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-eqz v2, :cond_1

    move v2, v3

    .line 401319
    goto :goto_0

    .line 401320
    :cond_1
    move-object/from16 v0, p5

    iget-wide v6, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    cmp-long v2, p3, v6

    if-nez v2, :cond_2

    move v2, v3

    .line 401321
    goto :goto_0

    .line 401322
    :cond_2
    iget-object v2, p0, LX/2OQ;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/2OQ;->i:LX/0Uh;

    const/16 v5, 0x167

    invoke-virtual {v2, v5, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v4

    .line 401323
    :goto_1
    if-nez v2, :cond_5

    .line 401324
    if-nez p6, :cond_4

    move v2, v4

    .line 401325
    goto :goto_0

    :cond_3
    move v2, v3

    .line 401326
    goto :goto_1

    .line 401327
    :cond_4
    iget-object v2, p0, LX/2OQ;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6fD;

    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-virtual {v2, v0, v1}, LX/6fD;->c(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Z

    move-result v2

    if-nez v2, :cond_5

    move v2, v4

    .line 401328
    goto :goto_0

    :cond_5
    move v2, v3

    .line 401329
    goto :goto_0
.end method

.method private b(LX/6ek;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 4
    .param p1    # LX/6ek;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 401355
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401356
    :try_start_0
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v0, p2}, LX/2OY;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 401357
    iget-object v0, p0, LX/2OQ;->n:LX/2Oa;

    invoke-virtual {v0, p2}, LX/2Oa;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    .line 401358
    iget-object v0, p0, LX/2OQ;->p:LX/2Oc;

    invoke-virtual {v0, p2}, LX/2Oc;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    .line 401359
    iget-object v0, p0, LX/2OQ;->q:LX/2Oc;

    invoke-virtual {v0, p2}, LX/2Oc;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    .line 401360
    iget-object v0, p0, LX/2OQ;->m:LX/2OZ;

    invoke-virtual {v0, p2}, LX/2OZ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401361
    if-eqz p1, :cond_0

    .line 401362
    invoke-static {p0, p1}, LX/2OQ;->f(LX/2OQ;LX/6ek;)LX/DdE;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/DdE;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401363
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401364
    :cond_1
    return-void

    .line 401365
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401366
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private b(Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 6

    .prologue
    .line 401311
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2OY;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/DdS;

    move-result-object v0

    .line 401312
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadSummary;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 401313
    iget-wide v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/DdS;->a(J)V

    .line 401314
    :goto_0
    return-void

    .line 401315
    :cond_0
    iget-wide v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    invoke-virtual {v0, v2, v3}, LX/DdS;->a(J)V

    goto :goto_0
.end method

.method private static c(LX/2OQ;LX/0Px;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 401304
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 401305
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 401306
    invoke-virtual {p0, v0}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 401307
    if-eqz v0, :cond_0

    .line 401308
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 401309
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 401310
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/2OQ;Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 5

    .prologue
    .line 401297
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401298
    :try_start_0
    iget-object v0, p0, LX/2OQ;->b:LX/2OP;

    sget-object v3, LX/2OP;->FACEBOOK:LX/2OP;

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v3, "Tried to get thread by threadID in non facebook messages cache"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 401299
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v0, p1}, LX/2OY;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 401300
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-object v0

    .line 401301
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 401302
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401303
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private static f(LX/2OQ;LX/6ek;)LX/DdE;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 401289
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401290
    :try_start_0
    iget-object v0, p0, LX/2OQ;->t:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdE;

    .line 401291
    if-nez v0, :cond_0

    .line 401292
    new-instance v0, LX/DdE;

    iget-object v3, p0, LX/2OQ;->k:LX/2OW;

    invoke-direct {v0, p1, v3}, LX/DdE;-><init>(LX/6ek;LX/2OW;)V

    .line 401293
    iget-object v3, p0, LX/2OQ;->t:LX/01J;

    invoke-virtual {v3, p1, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401294
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_1
    return-object v0

    .line 401295
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401296
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private static i(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 401279
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    .line 401280
    :try_start_0
    sget-object v0, LX/6ek;->INBOX:LX/6ek;

    invoke-static {p0, v0}, LX/2OQ;->f(LX/2OQ;LX/6ek;)LX/DdE;

    move-result-object v0

    invoke-virtual {v0}, LX/DdE;->a()LX/0qp;

    move-result-object v0

    .line 401281
    invoke-virtual {v0}, LX/0qp;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 401282
    iget-object v4, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v4

    if-eqz v4, :cond_0

    .line 401283
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401284
    :cond_1
    :goto_0
    return-object v0

    .line 401285
    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_3
    move-object v0, v1

    .line 401286
    goto :goto_0

    .line 401287
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401288
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v2, :cond_4

    if-eqz v1, :cond_5

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_4
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_5
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public static j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 1
    .param p0    # LX/2OQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 401578
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 401579
    sget-object v0, LX/2OP;->SMS:LX/2OP;

    invoke-direct {p0, p1, v0}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/2OP;)V

    .line 401580
    :cond_0
    :goto_0
    return-void

    .line 401581
    :cond_1
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 401582
    sget-object v0, LX/2OP;->TINCAN:LX/2OP;

    invoke-direct {p0, p1, v0}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/2OP;)V

    goto :goto_0

    .line 401583
    :cond_2
    if-eqz p1, :cond_0

    .line 401584
    sget-object v0, LX/2OP;->FACEBOOK:LX/2OP;

    invoke-direct {p0, p1, v0}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/2OP;)V

    goto :goto_0
.end method

.method private k()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 401673
    iget-object v1, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v1}, LX/2OW;->a()LX/2OX;

    move-result-object v3

    const/4 v1, 0x0

    .line 401674
    :try_start_0
    iget-object v2, p0, LX/2OQ;->t:LX/01J;

    invoke-virtual {v2}, LX/01J;->size()I

    move-result v4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    .line 401675
    iget-object v0, p0, LX/2OQ;->t:LX/01J;

    invoke-virtual {v0, v2}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdE;

    .line 401676
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, LX/DdE;->c(Z)V

    .line 401677
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 401678
    :cond_0
    iget-object v0, p0, LX/2OQ;->n:LX/2Oa;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/2Oa;->b(Z)V

    .line 401679
    iget-object v0, p0, LX/2OQ;->p:LX/2Oc;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/2Oc;->a(Z)V

    .line 401680
    iget-object v0, p0, LX/2OQ;->q:LX/2Oc;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/2Oc;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401681
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/2OX;->close()V

    .line 401682
    :cond_1
    return-void

    .line 401683
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401684
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v3, :cond_2

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v3}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v3}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private static k(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 401685
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401686
    iget-object v0, p0, LX/2OQ;->p:LX/2Oc;

    invoke-virtual {v0, v1}, LX/2Oc;->a(Z)V

    .line 401687
    iget-object v0, p0, LX/2OQ;->q:LX/2Oc;

    invoke-virtual {v0, v1}, LX/2Oc;->a(Z)V

    .line 401688
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 401689
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401690
    :try_start_0
    iget-object v0, p0, LX/2OQ;->m:LX/2OZ;

    invoke-virtual {v0, p1, p2}, LX/2OZ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 401691
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-object v0

    .line 401692
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401693
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 401694
    iget-object v1, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v1}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    .line 401695
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->i(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 401696
    if-eqz v1, :cond_0

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v3, :cond_2

    .line 401697
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401698
    :cond_1
    :goto_0
    return-object v0

    .line 401699
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v3

    iget-object v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    .line 401700
    new-instance v5, LX/6fg;

    invoke-direct {v5, v4}, LX/6fg;-><init>(Lcom/facebook/messaging/model/threads/MontageThreadPreview;)V

    move-object v4, v5

    .line 401701
    iput-wide p2, v4, LX/6fg;->b:J

    .line 401702
    move-object v4, v4

    .line 401703
    invoke-virtual {v4}, LX/6fg;->a()Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    move-result-object v4

    .line 401704
    iput-object v4, v3, LX/6g6;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    .line 401705
    move-object v3, v3

    .line 401706
    invoke-virtual {v3}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v3

    .line 401707
    invoke-virtual {p0, v3}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401708
    iget-object v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a()Z

    move-result v4

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a()Z

    move-result v3

    if-eq v4, v3, :cond_3

    .line 401709
    iget-object v0, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 401710
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_0

    .line 401711
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 401712
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v2, :cond_4

    if-eqz v1, :cond_5

    :try_start_3
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_4
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_5
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadResult;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 401733
    iget-object v1, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v1}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    .line 401734
    :try_start_0
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    sget-object v3, LX/6ek;->MONTAGE:LX/6ek;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eq v1, v3, :cond_2

    .line 401735
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_1
    :goto_0
    return-object v0

    .line 401736
    :cond_2
    :try_start_1
    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 401737
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    const/4 v9, 0x0

    .line 401738
    iget-object v7, p0, LX/2OQ;->s:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 401739
    if-eqz v7, :cond_3

    iget-object v8, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    const/4 v10, 0x2

    if-eq v8, v10, :cond_8

    :cond_3
    move-object v7, v9

    .line 401740
    :goto_1
    move-object v4, v7
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 401741
    if-nez v4, :cond_4

    .line 401742
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_0

    .line 401743
    :cond_4
    :try_start_2
    iget-object v1, p0, LX/2OQ;->r:LX/2OR;

    iget-object v5, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v1, v5}, LX/2OR;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 401744
    if-eqz v1, :cond_7

    .line 401745
    iget-object v7, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v7}, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a(Ljava/lang/String;)LX/6fg;

    move-result-object v7

    iget-object v8, v1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    .line 401746
    iput-object v8, v7, LX/6fg;->e:Ljava/lang/String;

    .line 401747
    move-object v7, v7

    .line 401748
    iget-object v8, v1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 401749
    iput-object v8, v7, LX/6fg;->f:Ljava/lang/String;

    .line 401750
    move-object v8, v7

    .line 401751
    iget-object v7, v1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_a

    iget-object v7, v1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 401752
    :goto_2
    iput-object v7, v8, LX/6fg;->d:Lcom/facebook/messaging/model/attachment/Attachment;

    .line 401753
    move-object v7, v8

    .line 401754
    iget-wide v9, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->k:J

    .line 401755
    iput-wide v9, v7, LX/6fg;->b:J

    .line 401756
    move-object v7, v7

    .line 401757
    iget-wide v9, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 401758
    iput-wide v9, v7, LX/6fg;->c:J

    .line 401759
    move-object v7, v7

    .line 401760
    move-object v1, v7

    .line 401761
    invoke-virtual {v1}, LX/6fg;->a()Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    move-result-object v1

    .line 401762
    :goto_3
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v5

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 401763
    iput-object v3, v5, LX/6g6;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 401764
    move-object v3, v5

    .line 401765
    iput-object v1, v3, LX/6g6;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    .line 401766
    move-object v1, v3

    .line 401767
    invoke-virtual {v1}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401768
    iget-object v0, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 401769
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_0

    .line 401770
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 401771
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_4
    if-eqz v2, :cond_5

    if-eqz v1, :cond_6

    :try_start_4
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :cond_5
    :goto_5
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_6
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_5

    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_4

    :cond_7
    move-object v1, v0

    goto :goto_3

    .line 401772
    :cond_8
    :try_start_5
    iget-object v8, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    const/4 v10, 0x0

    invoke-virtual {v8, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    invoke-virtual {v8}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b()Ljava/lang/String;

    move-result-object v10

    iget-object v8, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    const/4 v11, 0x1

    invoke-virtual {v8, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    invoke-virtual {v8}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v10, v8}, LX/2OQ;->a(LX/2OQ;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 401773
    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v11, v12, v7, v8}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v7

    .line 401774
    if-nez v7, :cond_9

    move-object v7, v9

    .line 401775
    goto/16 :goto_1

    .line 401776
    :cond_9
    invoke-virtual {p0, v7}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v7

    goto/16 :goto_1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_a
    const/4 v7, 0x0

    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 401719
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401720
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401721
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v0, p1}, LX/2OY;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 401722
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-object v0

    .line 401723
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401724
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadCriteria;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 5

    .prologue
    .line 401725
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401726
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 401727
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/2OY;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 401728
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    :goto_0
    return-object v0

    .line 401729
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/2OY;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 401730
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_0

    .line 401731
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 401732
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_2
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a()V
    .locals 8

    .prologue
    .line 401646
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v3

    const/4 v1, 0x0

    .line 401647
    :try_start_0
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    .line 401648
    iget-object v2, v0, LX/2OY;->a:LX/2OW;

    invoke-virtual {v2}, LX/2OW;->b()V

    .line 401649
    iget-object v2, v0, LX/2OY;->b:LX/01J;

    invoke-virtual {v2}, LX/01J;->clear()V

    .line 401650
    iget-object v2, v0, LX/2OY;->c:LX/01J;

    invoke-virtual {v2}, LX/01J;->clear()V

    .line 401651
    iget-object v2, v0, LX/2OY;->d:LX/01J;

    invoke-virtual {v2}, LX/01J;->clear()V

    .line 401652
    iget-object v0, p0, LX/2OQ;->m:LX/2OZ;

    .line 401653
    iget-object v2, v0, LX/2OZ;->a:LX/2OW;

    invoke-virtual {v2}, LX/2OW;->b()V

    .line 401654
    iget-object v2, v0, LX/2OZ;->b:LX/01J;

    invoke-virtual {v2}, LX/01J;->clear()V

    .line 401655
    iget-object v2, v0, LX/2OZ;->c:LX/01J;

    invoke-virtual {v2}, LX/01J;->clear()V

    .line 401656
    const/4 v0, 0x0

    iget-object v2, p0, LX/2OQ;->t:LX/01J;

    invoke-virtual {v2}, LX/01J;->size()I

    move-result v4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    .line 401657
    iget-object v0, p0, LX/2OQ;->t:LX/01J;

    invoke-virtual {v0, v2}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdE;

    .line 401658
    invoke-virtual {v0}, LX/DdE;->g()V

    .line 401659
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 401660
    :cond_0
    iget-object v0, p0, LX/2OQ;->t:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 401661
    iget-object v0, p0, LX/2OQ;->n:LX/2Oa;

    const/4 v7, 0x0

    .line 401662
    iget-object v6, v0, LX/2Oa;->a:LX/2OW;

    invoke-virtual {v6}, LX/2OW;->b()V

    .line 401663
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, v0, LX/2Oa;->b:Ljava/util/List;

    .line 401664
    iput-boolean v7, v0, LX/2Oa;->d:Z

    .line 401665
    iput-boolean v7, v0, LX/2Oa;->e:Z

    .line 401666
    const-wide/16 v6, -0x1

    iput-wide v6, v0, LX/2Oa;->c:J

    .line 401667
    iget-object v0, p0, LX/2OQ;->p:LX/2Oc;

    invoke-virtual {v0}, LX/2Oc;->d()V

    .line 401668
    iget-object v0, p0, LX/2OQ;->q:LX/2Oc;

    invoke-virtual {v0}, LX/2Oc;->d()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401669
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/2OX;->close()V

    .line 401670
    :cond_1
    return-void

    .line 401671
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401672
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v3, :cond_2

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v3}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v3}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 401713
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401714
    :try_start_0
    iget-object v0, p0, LX/2OQ;->n:LX/2Oa;

    invoke-virtual {v0, p1, p2}, LX/2Oa;->a(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401715
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401716
    :cond_0
    return-void

    .line 401717
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401718
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadEventReminder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 401640
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401641
    :try_start_0
    iget-object v0, p0, LX/2OQ;->o:LX/2Ob;

    invoke-virtual {v0, p1}, LX/2Ob;->a(LX/0Px;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401642
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401643
    :cond_0
    return-void

    .line 401644
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401645
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(LX/6ek;LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ek;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 401635
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 401636
    invoke-static {p0, v0}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401637
    invoke-direct {p0, p1, v0}, LX/2OQ;->b(LX/6ek;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401638
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 401639
    :cond_0
    return-void
.end method

.method public final a(LX/6ek;Lcom/facebook/messaging/model/folders/FolderCounts;)V
    .locals 4

    .prologue
    .line 401624
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401625
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->f(LX/2OQ;LX/6ek;)LX/DdE;

    move-result-object v0

    .line 401626
    iget-object v3, v0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v3}, LX/2OW;->b()V

    .line 401627
    if-nez p2, :cond_3

    .line 401628
    sget-object v3, LX/DdE;->a:Ljava/lang/String;

    const-string p0, "Passed in null folder counts!"

    invoke-static {v3, p0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 401629
    sget-object v3, Lcom/facebook/messaging/model/folders/FolderCounts;->a:Lcom/facebook/messaging/model/folders/FolderCounts;

    iput-object v3, v0, LX/DdE;->i:Lcom/facebook/messaging/model/folders/FolderCounts;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401630
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401631
    :cond_0
    return-void

    .line 401632
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401633
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1

    .line 401634
    :cond_3
    iput-object p2, v0, LX/DdE;->i:Lcom/facebook/messaging/model/folders/FolderCounts;

    goto :goto_0
.end method

.method public final a(LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;J)V
    .locals 9

    .prologue
    .line 401610
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v3

    const/4 v1, 0x0

    .line 401611
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->f(LX/2OQ;LX/6ek;)LX/DdE;

    move-result-object v4

    .line 401612
    iget-object v0, p2, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v5, v0

    .line 401613
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v6, :cond_0

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 401614
    iget-object v7, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v7}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401615
    invoke-virtual {v4, v0}, LX/DdE;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401616
    invoke-virtual {p0, v0, p3, p4}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 401617
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 401618
    :cond_0
    iget-boolean v0, p2, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d:Z

    move v0, v0

    .line 401619
    invoke-virtual {v4, v0}, LX/DdE;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401620
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/2OX;->close()V

    .line 401621
    :cond_1
    return-void

    .line 401622
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401623
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_1
    if-eqz v3, :cond_2

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v3}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v3}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;JZ)V
    .locals 15

    .prologue
    .line 401585
    iget-object v2, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v2}, LX/2OW;->a()LX/2OX;

    move-result-object v5

    const/4 v3, 0x0

    .line 401586
    :try_start_0
    invoke-static/range {p0 .. p1}, LX/2OQ;->f(LX/2OQ;LX/6ek;)LX/DdE;

    move-result-object v6

    .line 401587
    invoke-virtual {v6}, LX/DdE;->f()V

    .line 401588
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->b()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, v8, :cond_2

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 401589
    iget-object v9, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v9}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401590
    iget-object v9, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p0, v9}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v9

    .line 401591
    if-eqz v9, :cond_0

    iget-wide v10, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    const-wide/16 v12, -0x1

    cmp-long v10, v10, v12

    if-eqz v10, :cond_0

    iget-wide v10, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    iget-wide v12, v9, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    cmp-long v10, v10, v12

    if-gez v10, :cond_0

    .line 401592
    invoke-virtual {v6, v9}, LX/DdE;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401593
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 401594
    :cond_0
    invoke-virtual {v6, v2}, LX/DdE;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401595
    move-wide/from16 v0, p3

    invoke-virtual {p0, v2, v0, v1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_1

    .line 401596
    :catch_0
    move-exception v2

    :try_start_1
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401597
    :catchall_0
    move-exception v3

    move-object v14, v3

    move-object v3, v2

    move-object v2, v14

    :goto_2
    if-eqz v5, :cond_1

    if-eqz v3, :cond_6

    :try_start_2
    invoke-virtual {v5}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_3
    throw v2

    .line 401598
    :cond_2
    const/4 v2, 0x1

    :try_start_3
    invoke-virtual {v6, v2}, LX/DdE;->b(Z)V

    .line 401599
    if-nez p5, :cond_3

    const/4 v2, 0x1

    :goto_4
    invoke-virtual {v6, v2}, LX/DdE;->c(Z)V

    .line 401600
    move-wide/from16 v0, p3

    invoke-virtual {v6, v0, v1}, LX/DdE;->a(J)V

    .line 401601
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->b()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v2, 0x0

    move v4, v2

    :goto_5
    if-ge v4, v8, :cond_4

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 401602
    invoke-direct {p0, v2}, LX/2OQ;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401603
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_5

    .line 401604
    :cond_3
    const/4 v2, 0x0

    goto :goto_4

    .line 401605
    :cond_4
    iget-object v2, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v2}, LX/2OY;->a()V

    .line 401606
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c()Z

    move-result v2

    invoke-virtual {v6, v2}, LX/DdE;->a(Z)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 401607
    if-eqz v5, :cond_5

    invoke-virtual {v5}, LX/2OX;->close()V

    .line 401608
    :cond_5
    return-void

    .line 401609
    :catch_1
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_6
    invoke-virtual {v5}, LX/2OX;->close()V

    goto :goto_3

    :catchall_1
    move-exception v2

    goto :goto_2
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 14

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 401497
    iget-object v2, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v2}, LX/2OW;->a()LX/2OX;

    move-result-object v8

    .line 401498
    :try_start_0
    iget-object v9, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 401499
    invoke-static {p0, v9}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401500
    iget-object v2, p0, LX/2OQ;->m:LX/2OZ;

    invoke-virtual {v2, v9}, LX/2OZ;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 401501
    if-nez v4, :cond_1

    .line 401502
    if-eqz v8, :cond_0

    invoke-virtual {v8}, LX/2OX;->close()V

    .line 401503
    :cond_0
    :goto_0
    return-void

    .line 401504
    :cond_1
    :try_start_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 401505
    iget-object v2, v4, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v6, v2

    .line 401506
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v10

    move v3, v0

    move v2, v0

    :goto_1
    if-ge v3, v10, :cond_4

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 401507
    iget-object v11, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    iget-object v12, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 401508
    invoke-virtual {v5, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v1

    .line 401509
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    .line 401510
    :cond_2
    iget-object v11, v0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v12, LX/2uW;->PENDING_SEND:LX/2uW;

    if-ne v11, v12, :cond_3

    iget-object v11, p1, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v12, v0, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    invoke-static {v11, v12}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 401511
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v11

    invoke-virtual {v11, v0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    sget-object v11, LX/2uW;->FAILED_SEND:LX/2uW;

    .line 401512
    iput-object v11, v0, LX/6f7;->l:LX/2uW;

    .line 401513
    move-object v0, v0

    .line 401514
    sget-object v11, LX/6fP;->EARLIER_MESSAGE_FROM_THREAD_FAILED:LX/6fP;

    invoke-static {v11}, Lcom/facebook/messaging/model/send/SendError;->a(LX/6fP;)Lcom/facebook/messaging/model/send/SendError;

    move-result-object v11

    .line 401515
    iput-object v11, v0, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 401516
    move-object v0, v0

    .line 401517
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v2

    goto :goto_2

    .line 401518
    :cond_3
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v2

    goto :goto_2

    .line 401519
    :cond_4
    new-instance v1, Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 401520
    iget-object v0, v4, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 401521
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 401522
    iget-boolean v5, v4, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    move v4, v5

    .line 401523
    invoke-direct {v1, v0, v3, v4}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    .line 401524
    iget-object v3, p0, LX/2OQ;->m:LX/2OZ;

    iget-object v0, p0, LX/2OQ;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v3, v1, v0}, LX/2OZ;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/user/model/User;)V

    .line 401525
    if-nez v2, :cond_5

    .line 401526
    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    sget-object v6, LX/DdQ;->MESSAGE_ADDED:LX/DdQ;

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v1 .. v6}, LX/2OQ;->a(LX/2OQ;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLX/DdQ;)V

    .line 401527
    :cond_5
    invoke-virtual {p0, v9}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 401528
    if-eqz v0, :cond_6

    .line 401529
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v0

    .line 401530
    const/4 v1, 0x1

    .line 401531
    iput-boolean v1, v0, LX/6g6;->x:Z

    .line 401532
    const/4 v1, 0x1

    .line 401533
    iput-boolean v1, v0, LX/6g6;->z:Z

    .line 401534
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 401535
    :cond_6
    if-eqz v8, :cond_0

    invoke-virtual {v8}, LX/2OX;->close()V

    goto/16 :goto_0

    .line 401536
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 401537
    :catchall_0
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    :goto_3
    if-eqz v8, :cond_7

    if-eqz v1, :cond_8

    :try_start_3
    invoke-virtual {v8}, LX/2OX;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_7
    :goto_4
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_8
    invoke-virtual {v8}, LX/2OX;->close()V

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_3
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLX/6jT;Ljava/lang/Boolean;)V
    .locals 13
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 401143
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v10

    const/4 v9, 0x0

    .line 401144
    :try_start_0
    sget-object v6, LX/DdQ;->MESSAGE_ADDED:LX/DdQ;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide/from16 v4, p3

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v8}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLX/DdQ;LX/6jT;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401145
    if-eqz v10, :cond_0

    invoke-virtual {v10}, LX/2OX;->close()V

    .line 401146
    :cond_0
    return-void

    .line 401147
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401148
    :catchall_0
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_0
    if-eqz v10, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v10}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v10}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v1, v9

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLjava/lang/Boolean;)V
    .locals 9
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 401575
    sget-object v6, LX/6jT;->a:LX/6jT;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v7, p5

    invoke-virtual/range {v1 .. v7}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLX/6jT;Ljava/lang/Boolean;)V

    .line 401576
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/messages/MessagesCollection;)V
    .locals 6

    .prologue
    .line 401556
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401557
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v3, v0

    .line 401558
    invoke-static {p0, v3}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401559
    iget-object v4, p0, LX/2OQ;->m:LX/2OZ;

    iget-object v0, p0, LX/2OQ;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v4, p1, v0}, LX/2OZ;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/user/model/User;)V

    .line 401560
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v0, v3}, LX/2OY;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/DdS;

    move-result-object v0

    invoke-virtual {v0}, LX/DdS;->c()V

    .line 401561
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v0, v3}, LX/2OY;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 401562
    if-nez v0, :cond_2

    .line 401563
    invoke-direct {p0}, LX/2OQ;->k()V

    .line 401564
    :cond_0
    :goto_0
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-nez v0, :cond_5
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401565
    :goto_1
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401566
    :cond_1
    return-void

    .line 401567
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->c()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    invoke-direct {p0, v0, v3}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 401568
    invoke-direct {p0}, LX/2OQ;->k()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 401569
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 401570
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_2
    if-eqz v2, :cond_3

    if-eqz v1, :cond_4

    :try_start_3
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_3
    :goto_3
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_4
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 401571
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 401572
    const-string v3, "  "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LX/2OQ;->b:LX/2OP;

    iget-object v4, v4, LX/2OP;->logName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Messages:\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401573
    const/16 v3, 0x8

    invoke-static {v0, p1, v3}, LX/2OQ;->a(Ljava/lang/StringBuilder;Lcom/facebook/messaging/model/messages/MessagesCollection;I)V

    .line 401574
    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJ)V
    .locals 9

    .prologue
    .line 401538
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401539
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401540
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v0, p1}, LX/2OY;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/DdS;

    move-result-object v0

    .line 401541
    invoke-virtual {p0, p1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v3

    .line 401542
    if-eqz v3, :cond_0

    iget-wide v4, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    cmp-long v4, p2, v4

    if-gez v4, :cond_2

    .line 401543
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401544
    :cond_1
    :goto_0
    return-void

    .line 401545
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v4

    .line 401546
    iput-wide p4, v4, LX/6g6;->d:J

    .line 401547
    move-object v4, v4

    .line 401548
    iput-wide p2, v4, LX/6g6;->k:J

    .line 401549
    move-object v4, v4

    .line 401550
    invoke-virtual {v4}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v4

    .line 401551
    invoke-virtual {p0, v4}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401552
    iget-wide v4, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    invoke-virtual {v0, v4, v5}, LX/DdS;->b(J)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 401553
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_0

    .line 401554
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 401555
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v2, :cond_3

    if-eqz v1, :cond_4

    :try_start_3
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_3
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;LX/0Xu;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/lang/String;",
            "LX/0Xu",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/UserKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 401477
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401478
    :try_start_0
    iget-object v0, p0, LX/2OQ;->m:LX/2OZ;

    invoke-virtual {v0, p2}, LX/2OZ;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 401479
    if-nez v0, :cond_1

    .line 401480
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401481
    :cond_0
    :goto_0
    return-void

    .line 401482
    :cond_1
    :try_start_1
    invoke-virtual {p0, p1}, LX/2OQ;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v3

    .line 401483
    iget-object v4, v3, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v4, v4

    .line 401484
    invoke-virtual {v4, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v5

    .line 401485
    if-gez v5, :cond_2

    .line 401486
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_0

    .line 401487
    :cond_2
    :try_start_2
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v6

    invoke-virtual {v6, v0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/6f7;->a(LX/0Xu;)LX/6f7;

    move-result-object v0

    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v6

    .line 401488
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v4, v0}, LX/0Px;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/messaging/model/messages/Message;

    .line 401489
    aput-object v6, v0, v5

    .line 401490
    new-instance v4, Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 401491
    iget-boolean v5, v3, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    move v3, v5

    .line 401492
    invoke-direct {v4, p1, v0, v3}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    .line 401493
    iget-object v3, p0, LX/2OQ;->m:LX/2OZ;

    iget-object v0, p0, LX/2OQ;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v3, v4, v0}, LX/2OZ;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/user/model/User;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 401494
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_0

    .line 401495
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 401496
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    if-eqz v2, :cond_3

    if-eqz v1, :cond_4

    :try_start_4
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Set;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 400825
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 400826
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 400827
    iget-object v0, p0, LX/2OQ;->m:LX/2OZ;

    invoke-virtual {v0, p1}, LX/2OZ;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 400828
    if-nez v3, :cond_1

    .line 400829
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 400830
    :cond_0
    :goto_0
    return-void

    .line 400831
    :cond_1
    :try_start_1
    iget-object v4, p0, LX/2OQ;->m:LX/2OZ;

    .line 400832
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 400833
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v7, v0

    .line 400834
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v0, 0x0

    move v5, v0

    :goto_1
    if-ge v5, v8, :cond_3

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 400835
    iget-object p1, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {p2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 400836
    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 400837
    :cond_2
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    .line 400838
    :cond_3
    new-instance v0, Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 400839
    iget-object v5, v3, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v5, v5

    .line 400840
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 400841
    iget-boolean v7, v3, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    move v7, v7

    .line 400842
    invoke-direct {v0, v5, v6, v7}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    move-object v5, v0

    .line 400843
    iget-object v0, p0, LX/2OQ;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v4, v5, v0}, LX/2OZ;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/user/model/User;)V

    .line 400844
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v0, v0

    .line 400845
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/messages/Message;

    .line 400846
    iget-object v4, v3, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {p2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 400847
    iget-object v4, p0, LX/2OQ;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/DdG;

    iget-object v5, p0, LX/2OQ;->j:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2Oh;

    iget-object v7, v3, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v5, v7}, LX/2Oh;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, LX/DdG;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)V

    .line 400848
    :cond_4
    iget-object v4, p0, LX/2OQ;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/DdG;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    .line 400849
    iget-object v5, v4, LX/DdG;->i:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 400850
    goto :goto_2
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 400851
    :cond_5
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    goto/16 :goto_0

    .line 400852
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 400853
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_3
    if-eqz v2, :cond_6

    if-eqz v1, :cond_7

    :try_start_3
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_6
    :goto_4
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_7
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Z)V
    .locals 8

    .prologue
    .line 401113
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v3

    const/4 v1, 0x0

    .line 401114
    :try_start_0
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v0, p1}, LX/2OY;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 401115
    if-eqz v0, :cond_0

    .line 401116
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v0

    .line 401117
    iput-boolean p2, v0, LX/6g6;->u:Z

    .line 401118
    move-object v0, v0

    .line 401119
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 401120
    iget-object v2, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v2, v0}, LX/2OY;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401121
    :cond_0
    const/4 v0, 0x0

    iget-object v2, p0, LX/2OQ;->t:LX/01J;

    invoke-virtual {v2}, LX/01J;->size()I

    move-result v4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    .line 401122
    iget-object v0, p0, LX/2OQ;->t:LX/01J;

    invoke-virtual {v0, v2}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdE;

    .line 401123
    invoke-virtual {v0, p1}, LX/DdE;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v5

    .line 401124
    if-eqz v5, :cond_1

    .line 401125
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v5

    .line 401126
    iput-boolean p2, v5, LX/6g6;->u:Z

    .line 401127
    move-object v5, v5

    .line 401128
    invoke-virtual {v5}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v5

    .line 401129
    invoke-virtual {v0, v5}, LX/DdE;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401130
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 401131
    :cond_2
    invoke-static {p0, p1}, LX/2OQ;->k(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401132
    if-eqz v3, :cond_3

    invoke-virtual {v3}, LX/2OX;->close()V

    .line 401133
    :cond_3
    return-void

    .line 401134
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401135
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    if-eqz v3, :cond_4

    if-eqz v1, :cond_5

    :try_start_2
    invoke-virtual {v3}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_4
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_5
    invoke-virtual {v3}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 6

    .prologue
    .line 401101
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401102
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v0}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401103
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    invoke-static {p0, v0}, LX/2OQ;->f(LX/2OQ;LX/6ek;)LX/DdE;

    move-result-object v0

    .line 401104
    iget-object v3, p0, LX/2OQ;->l:LX/2OY;

    iget-object v4, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3, v4}, LX/2OY;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v3

    invoke-direct {p0, p1, v3}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401105
    iget-object v3, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v3, p1}, LX/2OY;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401106
    invoke-virtual {v0, p1}, LX/DdE;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401107
    invoke-direct {p0, p1}, LX/2OQ;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401108
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v0}, LX/2OQ;->k(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401109
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401110
    :cond_0
    return-void

    .line 401111
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401112
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V
    .locals 7

    .prologue
    .line 401093
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401094
    :try_start_0
    invoke-virtual {p0, p1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401095
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v3}, LX/2OY;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/DdS;

    move-result-object v0

    .line 401096
    iput-wide p2, v0, LX/DdS;->f:J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401097
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401098
    :cond_0
    return-void

    .line 401099
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401100
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/MessageDraft;)V
    .locals 1
    .param p2    # Lcom/facebook/messaging/model/messages/MessageDraft;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 401087
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v0

    .line 401088
    iput-object p2, v0, LX/6g6;->B:Lcom/facebook/messaging/model/messages/MessageDraft;

    .line 401089
    move-object v0, v0

    .line 401090
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 401091
    invoke-virtual {p0, v0}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401092
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/MessagesCollection;)V
    .locals 10

    .prologue
    .line 401075
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v8

    const/4 v7, 0x0

    .line 401076
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v0}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401077
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 401078
    iget-object v2, v0, LX/2OY;->a:LX/2OW;

    invoke-virtual {v2}, LX/2OW;->b()V

    .line 401079
    iget-object v2, v0, LX/2OY;->b:LX/01J;

    invoke-virtual {v2, v1}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    move v0, v2

    .line 401080
    if-eqz v0, :cond_1

    .line 401081
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b(I)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    const-wide/16 v4, -0x1

    sget-object v6, LX/DdQ;->MESSAGE_ADDED:LX/DdQ;

    move-object v1, p0

    move-object v3, p2

    invoke-static/range {v1 .. v6}, LX/2OQ;->a(LX/2OQ;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLX/DdQ;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401082
    :goto_0
    if-eqz v8, :cond_0

    invoke-virtual {v8}, LX/2OX;->close()V

    .line 401083
    :cond_0
    return-void

    .line 401084
    :cond_1
    :try_start_1
    invoke-virtual {p0, p2}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 401085
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 401086
    :catchall_0
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    :goto_1
    if-eqz v8, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v8}, LX/2OX;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_2
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v8}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/service/model/MarkThreadFields;)V
    .locals 14

    .prologue
    const-wide/16 v2, 0x0

    .line 401041
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v8

    const/4 v1, 0x0

    .line 401042
    :try_start_0
    iget-object v9, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 401043
    invoke-static {p0, v9}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401044
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    if-eqz v0, :cond_2

    move-wide v6, v2

    .line 401045
    :goto_0
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    if-eqz v0, :cond_3

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->e:J

    move-wide v4, v2

    .line 401046
    :goto_1
    invoke-virtual {p0, v9}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 401047
    if-eqz v0, :cond_0

    .line 401048
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v0

    .line 401049
    iput-wide v6, v0, LX/6g6;->m:J

    .line 401050
    move-object v0, v0

    .line 401051
    iput-wide v4, v0, LX/6g6;->k:J

    .line 401052
    move-object v0, v0

    .line 401053
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 401054
    invoke-virtual {p0, v0}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401055
    :cond_0
    const/4 v0, 0x0

    iget-object v2, p0, LX/2OQ;->t:LX/01J;

    invoke-virtual {v2}, LX/01J;->size()I

    move-result v3

    move v2, v0

    :goto_2
    if-ge v2, v3, :cond_4

    .line 401056
    iget-object v0, p0, LX/2OQ;->t:LX/01J;

    invoke-virtual {v0, v2}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdE;

    .line 401057
    invoke-virtual {v0, v9}, LX/DdE;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v10

    .line 401058
    if-eqz v10, :cond_1

    .line 401059
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v11

    invoke-virtual {v11, v10}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v10

    .line 401060
    iput-wide v6, v10, LX/6g6;->m:J

    .line 401061
    move-object v10, v10

    .line 401062
    iput-wide v4, v10, LX/6g6;->k:J

    .line 401063
    move-object v10, v10

    .line 401064
    invoke-virtual {v10}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v10

    .line 401065
    invoke-virtual {v0, v10}, LX/DdE;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 401066
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 401067
    :cond_2
    const-wide/16 v4, 0x1

    move-wide v6, v4

    goto :goto_0

    :cond_3
    move-wide v4, v2

    .line 401068
    goto :goto_1

    .line 401069
    :cond_4
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v0, v9}, LX/2OY;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/DdS;

    move-result-object v0

    .line 401070
    iget-wide v2, p1, Lcom/facebook/messaging/service/model/MarkThreadFields;->c:J

    invoke-virtual {v0, v2, v3}, LX/DdS;->b(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401071
    if-eqz v8, :cond_5

    invoke-virtual {v8}, LX/2OX;->close()V

    .line 401072
    :cond_5
    return-void

    .line 401073
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401074
    :catchall_0
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    :goto_3
    if-eqz v8, :cond_6

    if-eqz v1, :cond_7

    :try_start_2
    invoke-virtual {v8}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_6
    :goto_4
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_7
    invoke-virtual {v8}, LX/2OX;->close()V

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3
.end method

.method public final a(Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;)V
    .locals 13

    .prologue
    .line 401014
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v3

    const/4 v1, 0x0

    .line 401015
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v0}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401016
    iget-object v0, p0, LX/2OQ;->m:LX/2OZ;

    iget-object v2, p1, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v2}, LX/2OZ;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 401017
    if-nez v4, :cond_1

    .line 401018
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/2OX;->close()V

    .line 401019
    :cond_0
    :goto_0
    return-void

    .line 401020
    :cond_1
    :try_start_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 401021
    iget-object v0, v4, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v6, v0

    .line 401022
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v7, :cond_5

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 401023
    iget-object v8, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-object v9, p1, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;->b:Ljava/lang/String;

    if-eq v8, v9, :cond_2

    .line 401024
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 401025
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 401026
    :cond_2
    iget-object v8, v0, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    iget-object v9, p1, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;->c:Ljava/lang/String;

    iget-object v10, p1, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;->d:Ljava/lang/String;

    iget-object v11, p1, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;->e:Ljava/lang/String;

    invoke-static {v8, v9, v10, v11}, LX/5dt;->a(LX/0P1;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0P1;

    move-result-object v8

    .line 401027
    if-nez v8, :cond_4

    .line 401028
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 401029
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 401030
    :catchall_0
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    :goto_3
    if-eqz v3, :cond_3

    if-eqz v1, :cond_6

    :try_start_3
    invoke-virtual {v3}, LX/2OX;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_3
    :goto_4
    throw v0

    .line 401031
    :cond_4
    :try_start_4
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v9

    invoke-virtual {v9, v0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/6f7;->b(Ljava/util/Map;)LX/6f7;

    move-result-object v0

    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 401032
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 401033
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 401034
    :cond_5
    new-instance v2, Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 401035
    iget-object v0, v4, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 401036
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 401037
    iget-boolean v6, v4, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    move v4, v6

    .line 401038
    invoke-direct {v2, v0, v5, v4}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    .line 401039
    iget-object v4, p0, LX/2OQ;->m:LX/2OZ;

    iget-object v0, p0, LX/2OQ;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v4, v2, v0}, LX/2OZ;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/user/model/User;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 401040
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/2OX;->close()V

    goto :goto_0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v3}, LX/2OX;->close()V

    goto :goto_4
.end method

.method public final a(Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;)V
    .locals 11

    .prologue
    .line 400985
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v3

    const/4 v1, 0x0

    .line 400986
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 400987
    invoke-static {p0, v0}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 400988
    iget-object v2, p0, LX/2OQ;->m:LX/2OZ;

    invoke-virtual {v2, v0}, LX/2OZ;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v4

    .line 400989
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 400990
    iget-object v0, v4, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v6, v0

    .line 400991
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v7, :cond_2

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 400992
    iget-object v8, p1, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;->b:Ljava/lang/String;

    iget-object v9, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 400993
    iget-object v8, p1, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;->a:Lcom/facebook/messaging/model/send/SendError;

    .line 400994
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v9

    invoke-virtual {v9, v0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    sget-object v9, LX/2uW;->FAILED_SEND:LX/2uW;

    .line 400995
    iput-object v9, v0, LX/6f7;->l:LX/2uW;

    .line 400996
    move-object v0, v0

    .line 400997
    iput-object v8, v0, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 400998
    move-object v0, v0

    .line 400999
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 401000
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 401001
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 401002
    :cond_0
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_1

    .line 401003
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401004
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_2
    if-eqz v3, :cond_1

    if-eqz v1, :cond_4

    :try_start_2
    invoke-virtual {v3}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_3
    throw v0

    .line 401005
    :cond_2
    :try_start_3
    new-instance v2, Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 401006
    iget-object v0, v4, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 401007
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 401008
    iget-boolean v6, v4, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    move v4, v6

    .line 401009
    invoke-direct {v2, v0, v5, v4}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    .line 401010
    iget-object v4, p0, LX/2OQ;->m:LX/2OZ;

    iget-object v0, p0, LX/2OQ;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v4, v2, v0}, LX/2OZ;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/user/model/User;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 401011
    if-eqz v3, :cond_3

    invoke-virtual {v3}, LX/2OX;->close()V

    .line 401012
    :cond_3
    return-void

    .line 401013
    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_4
    invoke-virtual {v3}, LX/2OX;->close()V

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public final a(Ljava/util/List;JZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;JZ)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 400969
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v3

    const/4 v1, 0x0

    .line 400970
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 400971
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v0}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 400972
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400973
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v3, :cond_0

    if-eqz v1, :cond_4

    :try_start_2
    invoke-virtual {v3}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_2
    throw v0

    .line 400974
    :cond_1
    :try_start_3
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v0, p1}, LX/2OY;->a(Ljava/lang/Iterable;)V

    .line 400975
    iget-object v0, p0, LX/2OQ;->n:LX/2Oa;

    sget-object v4, LX/DdX;->a:LX/0QK;

    invoke-static {p1, v4}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/2Oa;->a(Ljava/lang/Iterable;)V

    .line 400976
    iget-object v0, p0, LX/2OQ;->n:LX/2Oa;

    invoke-virtual {v0, p2, p3}, LX/2Oa;->a(J)V

    .line 400977
    iget-object v0, p0, LX/2OQ;->n:LX/2Oa;

    const/4 v4, 0x1

    .line 400978
    iget-object v5, v0, LX/2Oa;->a:LX/2OW;

    invoke-virtual {v5}, LX/2OW;->b()V

    .line 400979
    iput-boolean v4, v0, LX/2Oa;->d:Z

    .line 400980
    iget-object v4, p0, LX/2OQ;->n:LX/2Oa;

    if-nez p4, :cond_3

    move v0, v2

    :goto_3
    invoke-virtual {v4, v0}, LX/2Oa;->b(Z)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 400981
    if-eqz v3, :cond_2

    invoke-virtual {v3}, LX/2OX;->close()V

    .line 400982
    :cond_2
    return-void

    .line 400983
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 400984
    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v3}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(LX/6ek;)Z
    .locals 4

    .prologue
    .line 400962
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 400963
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->f(LX/2OQ;LX/6ek;)LX/DdE;

    move-result-object v0

    .line 400964
    iget-object v3, v0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v3}, LX/2OW;->b()V

    .line 400965
    iget-boolean v3, v0, LX/DdE;->f:Z

    move v0, v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 400966
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return v0

    .line 400967
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400968
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 400934
    iget-object v1, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v1}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 400935
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 400936
    iget-object v3, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v3, p1}, LX/2OY;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 400937
    if-nez p2, :cond_2

    .line 400938
    if-eqz v3, :cond_0

    const/4 v0, 0x1

    .line 400939
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_1
    :goto_0
    return v0

    .line 400940
    :cond_2
    :try_start_1
    iget-object v4, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v4, p1}, LX/2OY;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/DdS;

    move-result-object v4

    .line 400941
    if-eqz v4, :cond_3

    .line 400942
    iget-boolean v5, v4, LX/DdS;->e:Z

    move v4, v5
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 400943
    if-eqz v4, :cond_3

    .line 400944
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_0

    .line 400945
    :cond_3
    :try_start_2
    iget-object v4, p0, LX/2OQ;->m:LX/2OZ;

    invoke-virtual {v4, p1}, LX/2OZ;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v4

    .line 400946
    if-nez v4, :cond_4

    .line 400947
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_0

    .line 400948
    :cond_4
    if-nez v3, :cond_5

    .line 400949
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_0

    .line 400950
    :cond_5
    :try_start_3
    iget-object v5, v4, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v7, v5

    .line 400951
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v5, 0x0

    move v6, v5

    :goto_1
    if-ge v6, v8, :cond_a

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/model/messages/Message;

    .line 400952
    iget-boolean p1, v5, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-nez p1, :cond_9

    .line 400953
    :goto_2
    move-object v5, v5

    .line 400954
    invoke-direct {p0, v3, v5}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;)Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v3

    if-nez v3, :cond_6

    .line 400955
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_0

    .line 400956
    :cond_6
    :try_start_4
    invoke-virtual {v4, p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(I)Z
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    .line 400957
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_0

    .line 400958
    :catch_0
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 400959
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_3
    if-eqz v2, :cond_7

    if-eqz v1, :cond_8

    :try_start_6
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    :cond_7
    :goto_4
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_8
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 400960
    :cond_9
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 400961
    :cond_a
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public final b()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 400926
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 400927
    :try_start_0
    iget-object v0, p0, LX/2OQ;->n:LX/2Oa;

    .line 400928
    iget-object v3, v0, LX/2Oa;->a:LX/2OW;

    invoke-virtual {v3}, LX/2OW;->b()V

    .line 400929
    iget-object v3, v0, LX/2Oa;->b:Ljava/util/List;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object v0, v3

    .line 400930
    invoke-static {p0, v0}, LX/2OQ;->c(LX/2OQ;LX/0Px;)LX/0Px;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 400931
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-object v0

    .line 400932
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400933
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 400921
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 400922
    :try_start_0
    iget-object v0, p0, LX/2OQ;->m:LX/2OZ;

    invoke-virtual {v0, p1}, LX/2OZ;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 400923
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-object v0

    .line 400924
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400925
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    .locals 4

    .prologue
    .line 400915
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 400916
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 400917
    iget-object v0, p0, LX/2OQ;->m:LX/2OZ;

    invoke-virtual {v0, p1}, LX/2OZ;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 400918
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-object v0

    .line 400919
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400920
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final b(LX/0Px;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 400883
    invoke-static {}, LX/4z1;->u()LX/4z1;

    move-result-object v6

    .line 400884
    invoke-virtual/range {p1 .. p1}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/Message;

    .line 400885
    iget-object v4, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 400886
    iget-object v4, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v6, v4, v1}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 400887
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 400888
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v1}, LX/2OW;->a()LX/2OX;

    move-result-object v7

    const/4 v4, 0x0

    .line 400889
    :try_start_0
    invoke-interface {v6}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 400890
    invoke-interface {v6, v1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v9

    .line 400891
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2OQ;->m:LX/2OZ;

    invoke-virtual {v2, v1}, LX/2OZ;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v10

    .line 400892
    if-nez v10, :cond_3

    .line 400893
    if-eqz v7, :cond_2

    invoke-virtual {v7}, LX/2OX;->close()V

    .line 400894
    :cond_2
    :goto_2
    return-void

    .line 400895
    :cond_3
    const/4 v3, 0x0

    .line 400896
    :try_start_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 400897
    invoke-virtual {v10}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b()LX/0Px;

    move-result-object v12

    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v13

    const/4 v2, 0x0

    move v5, v2

    :goto_3
    if-ge v5, v13, :cond_5

    invoke-virtual {v12, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/messages/Message;

    .line 400898
    iget-object v14, v2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v9, v14}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 400899
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v2

    sget-object v3, LX/2uW;->FAILED_SEND:LX/2uW;

    invoke-virtual {v2, v3}, LX/6f7;->a(LX/2uW;)LX/6f7;

    move-result-object v2

    sget-object v3, LX/6fP;->PENDING_SEND_ON_STARTUP:LX/6fP;

    invoke-static {v3}, Lcom/facebook/messaging/model/send/SendError;->a(LX/6fP;)Lcom/facebook/messaging/model/send/SendError;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6f7;->a(Lcom/facebook/messaging/model/send/SendError;)LX/6f7;

    move-result-object v2

    invoke-virtual {v2}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    invoke-virtual {v11, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 400900
    const/4 v2, 0x1

    .line 400901
    :goto_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v2

    goto :goto_3

    .line 400902
    :cond_4
    invoke-virtual {v11, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move v2, v3

    goto :goto_4

    .line 400903
    :cond_5
    if-nez v3, :cond_6

    .line 400904
    if-eqz v7, :cond_2

    invoke-virtual {v7}, LX/2OX;->close()V

    goto :goto_2

    .line 400905
    :cond_6
    :try_start_2
    new-instance v3, Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v10}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v10}, Lcom/facebook/messaging/model/messages/MessagesCollection;->e()Z

    move-result v9

    invoke-direct {v3, v2, v5, v9}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    .line 400906
    move-object/from16 v0, p0

    iget-object v5, v0, LX/2OQ;->m:LX/2OZ;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2OQ;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    invoke-virtual {v5, v3, v2}, LX/2OZ;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/user/model/User;)V

    .line 400907
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 400908
    if-eqz v1, :cond_1

    .line 400909
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v1

    .line 400910
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/6g6;->c(Z)LX/6g6;

    .line 400911
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/6g6;->d(Z)LX/6g6;

    .line 400912
    invoke-virtual {v1}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/16 :goto_1

    .line 400913
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 400914
    :catchall_0
    move-exception v2

    move-object v15, v2

    move-object v2, v1

    move-object v1, v15

    :goto_5
    if-eqz v7, :cond_7

    if-eqz v2, :cond_9

    :try_start_4
    invoke-virtual {v7}, LX/2OX;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :cond_7
    :goto_6
    throw v1

    :cond_8
    if-eqz v7, :cond_2

    invoke-virtual {v7}, LX/2OX;->close()V

    goto/16 :goto_2

    :catch_1
    move-exception v3

    invoke-static {v2, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_6

    :cond_9
    invoke-virtual {v7}, LX/2OX;->close()V

    goto :goto_6

    :catchall_1
    move-exception v1

    move-object v2, v4

    goto :goto_5
.end method

.method public final b(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 400870
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v8

    .line 400871
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v0}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 400872
    iget-object v0, p0, LX/2OQ;->m:LX/2OZ;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/2OZ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 400873
    iget-object v1, p0, LX/2OQ;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1, v0}, LX/DdV;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-nez v0, :cond_1

    .line 400874
    if-eqz v8, :cond_0

    invoke-virtual {v8}, LX/2OX;->close()V

    .line 400875
    :cond_0
    :goto_0
    return-void

    .line 400876
    :cond_1
    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    :try_start_1
    sget-object v6, LX/DdQ;->MESSAGE_ADDED:LX/DdQ;

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v1 .. v6}, LX/2OQ;->a(LX/2OQ;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLX/DdQ;)V

    .line 400877
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p0, v0}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 400878
    if-eqz v0, :cond_2

    .line 400879
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/MessageDraft;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 400880
    :cond_2
    if-eqz v8, :cond_0

    invoke-virtual {v8}, LX/2OX;->close()V

    goto :goto_0

    .line 400881
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 400882
    :catchall_0
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    :goto_1
    if-eqz v8, :cond_3

    if-eqz v1, :cond_4

    :try_start_3
    invoke-virtual {v8}, LX/2OX;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_3
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v8}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_1
.end method

.method public final b(LX/6ek;)Z
    .locals 4

    .prologue
    .line 400863
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 400864
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->f(LX/2OQ;LX/6ek;)LX/DdE;

    move-result-object v0

    .line 400865
    iget-object v3, v0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v3}, LX/2OW;->b()V

    .line 400866
    iget-boolean v3, v0, LX/DdE;->g:Z

    move v0, v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 400867
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return v0

    .line 400868
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400869
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)J
    .locals 8

    .prologue
    .line 400854
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 400855
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 400856
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v0, p1}, LX/2OY;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/DdS;

    move-result-object v0

    .line 400857
    iget-wide v4, v0, LX/DdS;->d:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 400858
    iget-wide v4, v0, LX/DdS;->d:J

    .line 400859
    :goto_0
    move-wide v0, v4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 400860
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-wide v0

    .line 400861
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400862
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_3
    iget-wide v4, v0, LX/DdS;->c:J

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 400820
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 400821
    :try_start_0
    iget-object v0, p0, LX/2OQ;->o:LX/2Ob;

    invoke-virtual {v0, p1}, LX/2Ob;->a(Ljava/lang/String;)LX/0P1;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 400822
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-object v0

    .line 400823
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400824
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final c()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 401208
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401209
    :try_start_0
    iget-object v0, p0, LX/2OQ;->p:LX/2Oc;

    invoke-virtual {v0}, LX/2Oc;->a()LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, LX/2OQ;->c(LX/2OQ;LX/0Px;)LX/0Px;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 401210
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-object v0

    .line 401211
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401212
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final c(LX/6ek;)V
    .locals 5

    .prologue
    .line 401269
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401270
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->f(LX/2OQ;LX/6ek;)LX/DdE;

    move-result-object v0

    .line 401271
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/DdE;->c(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401272
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401273
    :cond_0
    return-void

    .line 401274
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401275
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final c(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 401260
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401261
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 401262
    invoke-static {p0, v0}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 401263
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401264
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v2, :cond_0

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_2
    throw v0

    .line 401265
    :cond_1
    :try_start_3
    iget-object v0, p0, LX/2OQ;->n:LX/2Oa;

    invoke-virtual {v0, p1}, LX/2Oa;->a(Ljava/lang/Iterable;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 401266
    if-eqz v2, :cond_2

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401267
    :cond_2
    return-void

    .line 401268
    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final c(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 7

    .prologue
    .line 401252
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401253
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v0}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401254
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v3}, LX/2OY;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 401255
    if-eqz v0, :cond_1

    iget-wide v4, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->k:J

    iget-wide v0, p1, Lcom/facebook/messaging/model/messages/Message;->c:J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    cmp-long v0, v4, v0

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    .line 401256
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return v0

    .line 401257
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 401258
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401259
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 401250
    invoke-virtual {p0}, LX/2OQ;->a()V

    .line 401251
    return-void
.end method

.method public final d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)J
    .locals 6

    .prologue
    .line 401243
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401244
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401245
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v0, p1}, LX/2OY;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/DdS;

    move-result-object v0

    .line 401246
    iget-wide v4, v0, LX/DdS;->d:J

    move-wide v0, v4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401247
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-wide v0

    .line 401248
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401249
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 401238
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401239
    :try_start_0
    iget-object v0, p0, LX/2OQ;->q:LX/2Oc;

    invoke-virtual {v0}, LX/2Oc;->a()LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, LX/2OQ;->c(LX/2OQ;LX/0Px;)LX/0Px;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 401240
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-object v0

    .line 401241
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401242
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final d(LX/6ek;)Lcom/facebook/messaging/model/threads/ThreadsCollection;
    .locals 6

    .prologue
    .line 401227
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401228
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->f(LX/2OQ;LX/6ek;)LX/DdE;

    move-result-object v0

    .line 401229
    new-instance v3, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {v0}, LX/DdE;->a()LX/0qp;

    move-result-object v4

    .line 401230
    iget-object v5, v4, LX/0qp;->e:Ljava/util/List;

    move-object v4, v5

    .line 401231
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    .line 401232
    iget-object v5, v0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v5}, LX/2OW;->b()V

    .line 401233
    iget-boolean v5, v0, LX/DdE;->e:Z

    move v0, v5

    .line 401234
    invoke-direct {v3, v4, v0}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401235
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-object v3

    .line 401236
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401237
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final e()J
    .locals 6

    .prologue
    .line 401220
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401221
    :try_start_0
    iget-object v0, p0, LX/2OQ;->n:LX/2Oa;

    .line 401222
    iget-object v4, v0, LX/2Oa;->a:LX/2OW;

    invoke-virtual {v4}, LX/2OW;->b()V

    .line 401223
    iget-wide v4, v0, LX/2Oa;->c:J

    move-wide v0, v4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401224
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-wide v0

    .line 401225
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401226
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)J
    .locals 6

    .prologue
    .line 401213
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401214
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401215
    iget-object v0, p0, LX/2OQ;->l:LX/2OY;

    invoke-virtual {v0, p1}, LX/2OY;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/DdS;

    move-result-object v0

    .line 401216
    iget-wide v4, v0, LX/DdS;->c:J

    move-wide v0, v4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401217
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-wide v0

    .line 401218
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401219
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final e(LX/6ek;)Lcom/facebook/messaging/model/folders/FolderCounts;
    .locals 4

    .prologue
    .line 401136
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401137
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->f(LX/2OQ;LX/6ek;)LX/DdE;

    move-result-object v0

    .line 401138
    iget-object v3, v0, LX/DdE;->c:LX/2OW;

    invoke-virtual {v3}, LX/2OW;->b()V

    .line 401139
    iget-object v3, v0, LX/DdE;->i:Lcom/facebook/messaging/model/folders/FolderCounts;

    move-object v0, v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401140
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return-object v0

    .line 401141
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401142
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final f(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 4

    .prologue
    .line 401198
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401199
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401200
    iget-object v0, p0, LX/2OQ;->n:LX/2Oa;

    .line 401201
    iget-object v3, v0, LX/2Oa;->a:LX/2OW;

    invoke-virtual {v3}, LX/2OW;->b()V

    .line 401202
    iget-object v3, v0, LX/2Oa;->b:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 401203
    iget-object v3, v0, LX/2Oa;->b:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401204
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401205
    :cond_1
    return-void

    .line 401206
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401207
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final f()Z
    .locals 4

    .prologue
    .line 401191
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401192
    :try_start_0
    iget-object v0, p0, LX/2OQ;->n:LX/2Oa;

    .line 401193
    iget-object v3, v0, LX/2Oa;->a:LX/2OW;

    invoke-virtual {v3}, LX/2OW;->b()V

    .line 401194
    iget-boolean v3, v0, LX/2Oa;->e:Z

    move v0, v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401195
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return v0

    .line 401196
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401197
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final g(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 4

    .prologue
    .line 401184
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401185
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->j(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 401186
    iget-object v0, p0, LX/2OQ;->n:LX/2Oa;

    invoke-virtual {v0, p1}, LX/2Oa;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 401187
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    .line 401188
    :cond_0
    return-void

    .line 401189
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401190
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final g()Z
    .locals 4

    .prologue
    .line 401179
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401180
    :try_start_0
    iget-object v0, p0, LX/2OQ;->p:LX/2Oc;

    invoke-virtual {v0}, LX/2Oc;->b()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    .line 401181
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return v0

    .line 401182
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401183
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final h(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 401164
    iget-object v1, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v1}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    .line 401165
    :try_start_0
    invoke-static {p0, p1}, LX/2OQ;->i(LX/2OQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 401166
    if-nez v1, :cond_1

    .line 401167
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    :goto_0
    return-object v0

    .line 401168
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v1

    const/4 v3, 0x0

    .line 401169
    iput-object v3, v1, LX/6g6;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    .line 401170
    move-object v1, v1

    .line 401171
    const/4 v3, 0x0

    .line 401172
    iput-object v3, v1, LX/6g6;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 401173
    move-object v1, v1

    .line 401174
    invoke-virtual {v1}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 401175
    invoke-virtual {p0, v1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 401176
    if-eqz v2, :cond_2

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 401177
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 401178
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v2, :cond_3

    if-eqz v1, :cond_4

    :try_start_3
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_3
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_2

    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_1
.end method

.method public final h()Z
    .locals 4

    .prologue
    .line 401159
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401160
    :try_start_0
    iget-object v0, p0, LX/2OQ;->p:LX/2Oc;

    invoke-virtual {v0}, LX/2Oc;->c()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    .line 401161
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return v0

    .line 401162
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401163
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final i()Z
    .locals 4

    .prologue
    .line 401154
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401155
    :try_start_0
    iget-object v0, p0, LX/2OQ;->q:LX/2Oc;

    invoke-virtual {v0}, LX/2Oc;->b()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    .line 401156
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return v0

    .line 401157
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401158
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final j()Z
    .locals 4

    .prologue
    .line 401149
    iget-object v0, p0, LX/2OQ;->k:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->a()LX/2OX;

    move-result-object v2

    const/4 v1, 0x0

    .line 401150
    :try_start_0
    iget-object v0, p0, LX/2OQ;->q:LX/2Oc;

    invoke-virtual {v0}, LX/2Oc;->c()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    .line 401151
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2OX;->close()V

    :cond_0
    return v0

    .line 401152
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401153
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2OX;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/2OX;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method
