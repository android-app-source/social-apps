.class public LX/2Gg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Gh;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile l:LX/2Gg;


# instance fields
.field public final a:LX/2H7;

.field public final c:Landroid/content/Context;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/2Gs;

.field private final f:LX/0kb;

.field public final g:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

.field public final h:LX/2H0;

.field public final i:LX/0SG;

.field public final j:LX/2H4;

.field public final k:LX/2H3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 389059
    const-class v0, LX/2Gg;

    sput-object v0, LX/2Gg;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Gs;LX/0kb;Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Gq;LX/0SG;LX/2Gr;LX/2Gp;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389061
    iput-object p1, p0, LX/2Gg;->c:Landroid/content/Context;

    .line 389062
    iput-object p2, p0, LX/2Gg;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 389063
    iput-object p3, p0, LX/2Gg;->e:LX/2Gs;

    .line 389064
    iput-object p4, p0, LX/2Gg;->f:LX/0kb;

    .line 389065
    iput-object p5, p0, LX/2Gg;->g:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    .line 389066
    iput-object p7, p0, LX/2Gg;->i:LX/0SG;

    .line 389067
    sget-object v0, LX/2Ge;->ADM:LX/2Ge;

    invoke-virtual {p6, v0}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v0

    iput-object v0, p0, LX/2Gg;->h:LX/2H0;

    .line 389068
    sget-object v0, LX/2Ge;->ADM:LX/2Ge;

    invoke-virtual {p8, v0}, LX/2Gr;->a(LX/2Ge;)LX/2H3;

    move-result-object v0

    iput-object v0, p0, LX/2Gg;->k:LX/2H3;

    .line 389069
    sget-object v0, LX/2Ge;->ADM:LX/2Ge;

    iget-object v1, p0, LX/2Gg;->k:LX/2H3;

    iget-object v2, p0, LX/2Gg;->h:LX/2H0;

    invoke-virtual {p9, v0, v1, v2}, LX/2Gp;->a(LX/2Ge;LX/2H3;LX/2H0;)LX/2H4;

    move-result-object v0

    iput-object v0, p0, LX/2Gg;->j:LX/2H4;

    .line 389070
    new-instance v0, LX/Cek;

    invoke-direct {v0, p0}, LX/Cek;-><init>(LX/2Gg;)V

    iput-object v0, p0, LX/2Gg;->a:LX/2H7;

    .line 389071
    return-void
.end method

.method public static a(LX/0QB;)LX/2Gg;
    .locals 13

    .prologue
    .line 389072
    sget-object v0, LX/2Gg;->l:LX/2Gg;

    if-nez v0, :cond_1

    .line 389073
    const-class v1, LX/2Gg;

    monitor-enter v1

    .line 389074
    :try_start_0
    sget-object v0, LX/2Gg;->l:LX/2Gg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 389075
    if-eqz v2, :cond_0

    .line 389076
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 389077
    new-instance v3, LX/2Gg;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Gs;->a(LX/0QB;)LX/2Gs;

    move-result-object v6

    check-cast v6, LX/2Gs;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v7

    check-cast v7, LX/0kb;

    invoke-static {v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/0QB;)Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    move-result-object v8

    check-cast v8, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    invoke-static {v0}, LX/2Gq;->a(LX/0QB;)LX/2Gq;

    move-result-object v9

    check-cast v9, LX/2Gq;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static {v0}, LX/2Gr;->a(LX/0QB;)LX/2Gr;

    move-result-object v11

    check-cast v11, LX/2Gr;

    const-class v12, LX/2Gp;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/2Gp;

    invoke-direct/range {v3 .. v12}, LX/2Gg;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Gs;LX/0kb;Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Gq;LX/0SG;LX/2Gr;LX/2Gp;)V

    .line 389078
    move-object v0, v3

    .line 389079
    sput-object v0, LX/2Gg;->l:LX/2Gg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389080
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 389081
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 389082
    :cond_1
    sget-object v0, LX/2Gg;->l:LX/2Gg;

    return-object v0

    .line 389083
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 389084
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 389085
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    iget-object v0, p0, LX/2Gg;->h:LX/2H0;

    invoke-virtual {v0}, LX/2H0;->a()Ljava/lang/String;

    .line 389086
    if-eqz p3, :cond_0

    .line 389087
    iget-object v0, p0, LX/2Gg;->h:LX/2H0;

    invoke-virtual {v0}, LX/2H0;->h()V

    .line 389088
    iget-object v0, p0, LX/2Gg;->j:LX/2H4;

    sget-object v1, LX/Cem;->SUCCESS:LX/Cem;

    invoke-virtual {v1}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, LX/2H4;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 389089
    :goto_0
    return-void

    .line 389090
    :cond_0
    iget-object v0, p0, LX/2Gg;->j:LX/2H4;

    invoke-virtual {v0}, LX/2H4;->c()V

    .line 389091
    if-eqz p2, :cond_2

    .line 389092
    iget-object v0, p0, LX/2Gg;->h:LX/2H0;

    invoke-virtual {v0}, LX/2H0;->h()V

    .line 389093
    sget-object v0, LX/2Gg;->b:Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Registration error "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 389094
    const-string v0, "ERROR_AUTHENTICATION_FAILED"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 389095
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2Gg;->c:Landroid/content/Context;

    const-class v2, Lcom/facebook/push/adm/ADMRegistrarService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 389096
    iget-object v1, p0, LX/2Gg;->c:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 389097
    const-string v2, "app"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 389098
    const-string v2, "REQUEST"

    const-string v3, "REGISTER"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 389099
    iget-object v0, p0, LX/2Gg;->j:LX/2H4;

    invoke-virtual {v0, v1}, LX/2H4;->a(Landroid/app/PendingIntent;)V

    .line 389100
    :cond_1
    iget-object v0, p0, LX/2Gg;->j:LX/2H4;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 389101
    :cond_2
    iget-object v0, p0, LX/2Gg;->h:LX/2H0;

    invoke-virtual {v0, p1}, LX/2H0;->a(Ljava/lang/String;)V

    .line 389102
    iget-object v0, p0, LX/2Gg;->j:LX/2H4;

    sget-object v1, LX/2gP;->SUCCESS:LX/2gP;

    invoke-virtual {v1}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 389103
    iget-object v0, p0, LX/2Gg;->j:LX/2H4;

    invoke-virtual {v0}, LX/2H4;->d()V

    .line 389104
    iget-object v0, p0, LX/2Gg;->g:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->ADM:LX/2Ge;

    iget-object v2, p0, LX/2Gg;->a:LX/2H7;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 13

    .prologue
    .line 389105
    iget-object v5, p0, LX/2Gg;->h:LX/2H0;

    invoke-virtual {v5}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v5

    .line 389106
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 389107
    sget-object v5, LX/2H9;->NONE:LX/2H9;

    .line 389108
    :goto_0
    move-object v0, v5

    .line 389109
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 389110
    iget-object v1, p0, LX/2Gg;->e:LX/2Gs;

    sget-object v2, LX/3B4;->ADM:LX/3B4;

    invoke-virtual {v2}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/2H9;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/2Gg;->h:LX/2H0;

    invoke-virtual {v4}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/2Gs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 389111
    sget-object v1, LX/Cel;->a:[I

    invoke-virtual {v0}, LX/2H9;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 389112
    :goto_1
    return-void

    .line 389113
    :pswitch_0
    if-eqz p1, :cond_0

    .line 389114
    iget-object v0, p0, LX/2Gg;->g:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->ADM:LX/2Ge;

    iget-object v2, p0, LX/2Gg;->a:LX/2H7;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_1

    .line 389115
    :cond_0
    iget-object v0, p0, LX/2Gg;->g:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->ADM:LX/2Ge;

    iget-object v2, p0, LX/2Gg;->a:LX/2H7;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b(LX/2Ge;LX/2H7;)V

    goto :goto_1

    .line 389116
    :pswitch_1
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 389117
    iget-object v1, p0, LX/2Gg;->j:LX/2H4;

    sget-object v2, LX/Cem;->ATTEMPT:LX/Cem;

    invoke-virtual {v2}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/2H4;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 389118
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/2Gg;->c:Landroid/content/Context;

    const-class v3, Lcom/facebook/push/adm/ADMRegistrarService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 389119
    const-string v2, "REQUEST"

    const-string v3, "UNREGISTER"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 389120
    const-string v2, "app"

    iget-object v3, p0, LX/2Gg;->c:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-static {v3, v5, v4, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 389121
    iget-object v2, p0, LX/2Gg;->c:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 389122
    iget-object v1, p0, LX/2Gg;->g:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v2, LX/2Ge;->ADM:LX/2Ge;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/String;)Z

    .line 389123
    iget-object v1, p0, LX/2Gg;->h:LX/2H0;

    invoke-virtual {v1}, LX/2H0;->h()V

    .line 389124
    :cond_1
    :pswitch_2
    invoke-virtual {p0}, LX/2Gg;->b()V

    goto :goto_1

    .line 389125
    :pswitch_3
    iget-object v0, p0, LX/2Gg;->f:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_1

    .line 389126
    :cond_2
    iget-object v5, p0, LX/2Gg;->h:LX/2H0;

    invoke-virtual {v5}, LX/2H0;->b()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 389127
    sget-object v5, LX/2H9;->UPGRADED:LX/2H9;

    goto/16 :goto_0

    .line 389128
    :cond_3
    iget-object v5, p0, LX/2Gg;->i:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    .line 389129
    iget-object v7, p0, LX/2Gg;->h:LX/2H0;

    invoke-virtual {v7}, LX/2H0;->l()J

    move-result-wide v7

    .line 389130
    iget-object v9, p0, LX/2Gg;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v10, p0, LX/2Gg;->k:LX/2H3;

    .line 389131
    iget-object v11, v10, LX/2H3;->g:LX/0Tn;

    move-object v10, v11

    .line 389132
    const-wide/16 v11, 0x0

    invoke-interface {v9, v10, v11, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v9

    .line 389133
    sub-long v7, v5, v7

    const-wide/32 v11, 0x240c8400

    cmp-long v7, v7, v11

    if-lez v7, :cond_4

    sub-long/2addr v5, v9

    const-wide/32 v7, 0xa4cb800

    cmp-long v5, v5, v7

    if-lez v5, :cond_4

    .line 389134
    sget-object v5, LX/2H9;->EXPIRED:LX/2H9;

    goto/16 :goto_0

    .line 389135
    :cond_4
    sget-object v5, LX/2H9;->CURRENT:LX/2H9;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 389136
    iget-object v0, p0, LX/2Gg;->j:LX/2H4;

    sget-object v1, LX/2gP;->ATTEMPT:LX/2gP;

    invoke-virtual {v1}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 389137
    iget-object v0, p0, LX/2Gg;->j:LX/2H4;

    invoke-virtual {v0}, LX/2H4;->a()V

    .line 389138
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2Gg;->c:Landroid/content/Context;

    const-class v2, Lcom/facebook/push/adm/ADMRegistrarService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 389139
    iget-object v1, p0, LX/2Gg;->c:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-static {v1, v3, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 389140
    const-string v2, "app"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 389141
    const-string v1, "REQUEST"

    const-string v2, "REGISTER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 389142
    iget-object v1, p0, LX/2Gg;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    .line 389143
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startService="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 389144
    return-void
.end method

.method public final c()LX/2H7;
    .locals 1

    .prologue
    .line 389145
    iget-object v0, p0, LX/2Gg;->a:LX/2H7;

    return-object v0
.end method
