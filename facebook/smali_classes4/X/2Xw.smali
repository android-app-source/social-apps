.class public final LX/2Xw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Xu;


# instance fields
.field public final synthetic a:LX/2WV;

.field private final b:I

.field private final c:LX/2Xu;


# direct methods
.method public constructor <init>(LX/2WV;ILX/2Xu;)V
    .locals 0

    .prologue
    .line 420394
    iput-object p1, p0, LX/2Xw;->a:LX/2WV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420395
    iput p2, p0, LX/2Xw;->b:I

    .line 420396
    iput-object p3, p0, LX/2Xw;->c:LX/2Xu;

    .line 420397
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 420398
    iget-object v1, p0, LX/2Xw;->a:LX/2WV;

    monitor-enter v1

    .line 420399
    :try_start_0
    iget-object v0, p0, LX/2Xw;->a:LX/2WV;

    iget-object v0, v0, LX/2WV;->a:Landroid/util/SparseArray;

    iget v2, p0, LX/2Xw;->b:I

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Y0;

    .line 420400
    if-eqz v0, :cond_0

    .line 420401
    iget-object v2, v0, LX/2Y0;->a:LX/2Xy;

    move-object v2, v2

    .line 420402
    if-nez v2, :cond_1

    .line 420403
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "There was not a running job when onExit was called"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420404
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 420405
    :cond_1
    const/4 v2, 0x0

    .line 420406
    :try_start_1
    iput-object v2, v0, LX/2Y0;->a:LX/2Xy;

    .line 420407
    const/4 v2, 0x0

    .line 420408
    iput-object v2, v0, LX/2Y0;->a:LX/2Xy;

    .line 420409
    iget-object v2, v0, LX/2Y0;->b:Ljava/util/ArrayDeque;

    if-eqz v2, :cond_2

    .line 420410
    iget-object v2, v0, LX/2Y0;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    .line 420411
    if-eqz v2, :cond_2

    .line 420412
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 420413
    :cond_2
    iget-object v0, p0, LX/2Xw;->c:LX/2Xu;

    invoke-interface {v0}, LX/2Xu;->a()V

    .line 420414
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 420415
    iget-object v0, p0, LX/2Xw;->c:LX/2Xu;

    invoke-interface {v0, p1}, LX/2Xu;->a(Z)V

    .line 420416
    return-void
.end method
