.class public final LX/321;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "LX/32s;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:[LX/32u;

.field private b:LX/32u;

.field private c:I


# direct methods
.method public constructor <init>([LX/32u;)V
    .locals 4

    .prologue
    .line 488262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488263
    iput-object p1, p0, LX/321;->a:[LX/32u;

    .line 488264
    const/4 v1, 0x0

    .line 488265
    iget-object v0, p0, LX/321;->a:[LX/32u;

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 488266
    iget-object v3, p0, LX/321;->a:[LX/32u;

    add-int/lit8 v0, v1, 0x1

    aget-object v1, v3, v1

    .line 488267
    if-eqz v1, :cond_0

    .line 488268
    iput-object v1, p0, LX/321;->b:LX/32u;

    .line 488269
    :goto_1
    iput v0, p0, LX/321;->c:I

    .line 488270
    return-void

    :cond_0
    move v1, v0

    .line 488271
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private a()LX/32s;
    .locals 4

    .prologue
    .line 488253
    iget-object v1, p0, LX/321;->b:LX/32u;

    .line 488254
    if-nez v1, :cond_0

    .line 488255
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 488256
    :cond_0
    iget-object v0, v1, LX/32u;->next:LX/32u;

    .line 488257
    :goto_0
    if-nez v0, :cond_1

    iget v2, p0, LX/321;->c:I

    iget-object v3, p0, LX/321;->a:[LX/32u;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 488258
    iget-object v0, p0, LX/321;->a:[LX/32u;

    iget v2, p0, LX/321;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/321;->c:I

    aget-object v0, v0, v2

    goto :goto_0

    .line 488259
    :cond_1
    iput-object v0, p0, LX/321;->b:LX/32u;

    .line 488260
    iget-object v0, v1, LX/32u;->value:LX/32s;

    return-object v0
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 488272
    iget-object v0, p0, LX/321;->b:LX/32u;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 488261
    invoke-direct {p0}, LX/321;->a()LX/32s;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 488252
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
