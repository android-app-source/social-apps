.class public final LX/2tY;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/2ta;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1S9;


# direct methods
.method public constructor <init>(LX/1S9;)V
    .locals 0

    .prologue
    .line 475105
    iput-object p1, p0, LX/2tY;->a:LX/1S9;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 475106
    iget-object v0, p0, LX/2tY;->a:LX/1S9;

    iget-object v0, v0, LX/1S9;->d:LX/0if;

    sget-object v1, LX/0ig;->av:LX/0ih;

    const-string v2, "fetch_location_failed"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 475107
    iget-object v0, p0, LX/2tY;->a:LX/1S9;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1S9;->a$redex0(LX/1S9;LX/2ta;)V

    .line 475108
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 475109
    check-cast p1, LX/2ta;

    .line 475110
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/2ta;->a:LX/3Aj;

    if-nez v0, :cond_1

    .line 475111
    :cond_0
    iget-object v0, p0, LX/2tY;->a:LX/1S9;

    iget-object v0, v0, LX/1S9;->d:LX/0if;

    sget-object v1, LX/0ig;->av:LX/0ih;

    const-string v2, "fetch_location_failed_null"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 475112
    :cond_1
    iget-object v0, p0, LX/2tY;->a:LX/1S9;

    iget-object v0, v0, LX/1S9;->d:LX/0if;

    sget-object v1, LX/0ig;->av:LX/0ih;

    const-string v2, "fetch_location_successful"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 475113
    iget-object v0, p0, LX/2tY;->a:LX/1S9;

    invoke-static {v0, p1}, LX/1S9;->a$redex0(LX/1S9;LX/2ta;)V

    .line 475114
    return-void
.end method
