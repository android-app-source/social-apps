.class public LX/2lz;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 459230
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 459231
    return-void
.end method

.method public static a(Lcom/facebook/performancelogger/PerformanceLogger;LX/2l7;LX/2lw;LX/0lC;LX/11H;LX/2lx;Ljava/lang/Integer;LX/2m0;)LX/1qM;
    .locals 8
    .param p6    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/bookmark/service/BookmarkExpireTimeout;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/bookmark/client/BookmarkSyncQueue;
    .end annotation

    .annotation runtime Lcom/facebook/inject/ContextScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 459232
    new-instance v0, LX/2m1;

    new-instance v1, LX/2m2;

    invoke-direct {v1, p0}, LX/2m2;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 459233
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 459234
    new-instance v3, LX/2m1;

    new-instance v4, LX/2m3;

    invoke-direct {v4, p2, p1, p3}, LX/2m3;-><init>(LX/2lw;LX/2l7;LX/0lC;)V

    new-instance v5, LX/2m4;

    sget-object v6, LX/2m4;->VOID_PARAM_GETTER:LX/2m6;

    sget-object v7, LX/2m4;->STRING_RESULT_CONVERTER:LX/2m8;

    invoke-direct {v5, p4, p5, v6, v7}, LX/2m4;-><init>(LX/11H;LX/0e6;LX/2m6;LX/2m8;)V

    invoke-direct {v3, v4, v5}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    .line 459235
    const-string v4, "syncWithServer"

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459236
    const-string v4, "syncWithDB"

    new-instance v5, LX/2m1;

    new-instance v6, LX/2mB;

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {v6, p1, v7}, LX/2mB;-><init>(LX/2l7;I)V

    invoke-direct {v5, v6, v3}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459237
    const-string v3, "setFavoriteBookmarks"

    new-instance v4, LX/2m1;

    new-instance v5, LX/2mC;

    invoke-direct {v5, p2, p1}, LX/2mC;-><init>(LX/2lw;LX/2l7;)V

    new-instance v6, LX/2m4;

    new-instance v7, LX/2mD;

    const-string p0, "newFavoriteBookmarksGroup"

    invoke-direct {v7, p0}, LX/2mD;-><init>(Ljava/lang/String;)V

    sget-object p0, LX/2m4;->VOID_RESULT_CONVERTER:LX/2m8;

    invoke-direct {v6, p4, p7, v7, p0}, LX/2m4;-><init>(LX/11H;LX/0e6;LX/2m6;LX/2m8;)V

    invoke-direct {v4, v5, v6}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459238
    const-string v3, "updateUnreadCount"

    new-instance v4, LX/2mE;

    invoke-direct {v4, p2, p1}, LX/2mE;-><init>(LX/2lw;LX/2l7;)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459239
    new-instance v3, LX/2mF;

    invoke-direct {v3, v2}, LX/2mF;-><init>(Ljava/util/Map;)V

    move-object v2, v3

    .line 459240
    invoke-direct {v0, v1, v2}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    return-object v0
.end method

.method public static a()Ljava/lang/Integer;
    .locals 1
    .annotation runtime Lcom/facebook/bookmark/service/BookmarkExpireTimeout;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 459241
    const v0, 0x124f80

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 459242
    return-void
.end method
