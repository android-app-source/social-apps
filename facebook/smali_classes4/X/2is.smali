.class public LX/2is;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;
.implements LX/0jp;


# instance fields
.field private final a:LX/0xW;

.field private final b:LX/2it;

.field private final c:LX/1rU;


# direct methods
.method public constructor <init>(LX/0xW;LX/2it;LX/1rU;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 452153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 452154
    iput-object p1, p0, LX/2is;->a:LX/0xW;

    .line 452155
    iput-object p2, p0, LX/2is;->b:LX/2it;

    .line 452156
    iput-object p3, p0, LX/2is;->c:LX/1rU;

    .line 452157
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 452158
    iget-object v0, p0, LX/2is;->c:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 452159
    new-instance v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    invoke-direct {v0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;-><init>()V

    .line 452160
    :goto_0
    return-object v0

    .line 452161
    :cond_0
    iget-object v0, p0, LX/2is;->b:LX/2it;

    invoke-virtual {v0}, LX/2it;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/2is;->a:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 452162
    goto :goto_1

    .line 452163
    :cond_1
    new-instance v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-direct {v0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;-><init>()V

    goto :goto_0

    .line 452164
    :goto_1
    new-instance v0, Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-direct {v0}, Lcom/facebook/notifications/widget/NotificationsFragment;-><init>()V

    goto :goto_0
.end method

.method public final a(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0oh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 452165
    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oh;

    const-class v1, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-virtual {v0, v1}, LX/0oh;->a(Ljava/lang/Class;)LX/1Av;

    .line 452166
    return-void
.end method
