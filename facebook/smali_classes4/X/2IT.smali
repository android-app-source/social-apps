.class public LX/2IT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2IU;

.field public final b:LX/2IY;


# direct methods
.method public constructor <init>(LX/2IU;LX/2IY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391808
    iput-object p1, p0, LX/2IT;->a:LX/2IU;

    .line 391809
    iput-object p2, p0, LX/2IT;->b:LX/2IY;

    .line 391810
    return-void
.end method

.method public static a(LX/2IT;LX/0ux;)LX/0Px;
    .locals 8
    .param p0    # LX/2IT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ux;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/assetdownload/AssetDownloadConfiguration;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 391792
    iget-object v0, p0, LX/2IT;->a:LX/2IU;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 391793
    const-string v1, "configurations"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, LX/2IX;->a:LX/0U1;

    .line 391794
    iget-object v6, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v6

    .line 391795
    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, LX/2IX;->c:LX/0U1;

    .line 391796
    iget-object v6, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v6

    .line 391797
    aput-object v4, v2, v3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    :goto_1
    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 391798
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 391799
    :try_start_0
    sget-object v2, LX/2IX;->c:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 391800
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 391801
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    .line 391802
    iget-object v4, p0, LX/2IT;->b:LX/2IY;

    invoke-virtual {v4, v3}, LX/2IY;->a([B)Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 391803
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move-object v3, v5

    .line 391804
    goto :goto_0

    :cond_1
    move-object v4, v5

    goto :goto_1

    .line 391805
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 391806
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2IT;Landroid/database/sqlite/SQLiteDatabase;ILcom/facebook/assetdownload/AssetDownloadConfiguration;)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 391754
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 391755
    sget-object v2, LX/2IX;->a:LX/0U1;

    .line 391756
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 391757
    iget-object v3, p3, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    move-object v3, v3

    .line 391758
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 391759
    sget-object v2, LX/2IX;->b:LX/0U1;

    .line 391760
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 391761
    iget-object v3, p3, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mNamespace:Ljava/lang/String;

    move-object v3, v3

    .line 391762
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 391763
    sget-object v2, LX/2IX;->c:LX/0U1;

    .line 391764
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 391765
    iget-object v3, p0, LX/2IT;->b:LX/2IY;

    .line 391766
    :try_start_0
    iget-object v4, v3, LX/2IY;->a:LX/0lp;

    .line 391767
    new-instance v5, LX/2SG;

    invoke-static {}, LX/0lp;->b()LX/12B;

    move-result-object v6

    invoke-direct {v5, v6}, LX/2SG;-><init>(LX/12B;)V

    .line 391768
    invoke-virtual {v4, v5}, LX/0lp;->a(Ljava/io/OutputStream;)LX/0nX;

    move-result-object v6

    invoke-virtual {v6, p3}, LX/0nX;->a(Ljava/lang/Object;)V

    .line 391769
    invoke-virtual {v5}, LX/2SG;->c()[B

    move-result-object v6

    .line 391770
    invoke-virtual {v5}, LX/2SG;->b()V

    .line 391771
    move-object v4, v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 391772
    :goto_0
    move-object v3, v4

    .line 391773
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 391774
    sget-object v2, LX/2IX;->d:LX/0U1;

    .line 391775
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 391776
    iget v3, p3, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mPriority:I

    move v3, v3

    .line 391777
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 391778
    sget-object v2, LX/2IX;->e:LX/0U1;

    .line 391779
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 391780
    iget-object v3, p3, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mConnectionConstraint:LX/6BU;

    move-object v3, v3

    .line 391781
    invoke-virtual {v3}, LX/6BU;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 391782
    move-object v1, v1

    .line 391783
    :try_start_1
    const-string v2, "configurations"

    const/4 v3, 0x0

    const v4, -0x513de991

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {p1, v2, v3, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    const v1, -0x4a8f1abd

    invoke-static {v1}, LX/03h;->a(I)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0

    .line 391784
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    .line 391785
    :cond_0
    return v0

    .line 391786
    :catch_0
    move-exception v1

    .line 391787
    const/4 v2, 0x3

    if-eq p2, v2, :cond_0

    .line 391788
    throw v1

    .line 391789
    :catch_1
    move-exception v4

    .line 391790
    iget-object v5, v3, LX/2IY;->c:LX/03V;

    const-string v6, "download_task_marshal_io_exception"

    const-string v7, "Cannot marshal %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object p3, v8, p0

    invoke-static {v7, v8}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 391791
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Landroid/content/ContentValues;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 391750
    const-string v2, "identifier must not be null"

    invoke-static {p1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391751
    iget-object v2, p0, LX/2IT;->a:LX/2IU;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 391752
    const-string v3, "configurations"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/2IX;->a:LX/0U1;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v0, [Ljava/lang/String;

    aput-object p1, v5, v1

    invoke-virtual {v2, v3, p2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 391753
    if-lez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static b(LX/2IT;LX/0ux;)I
    .locals 5
    .param p0    # LX/2IT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 391811
    iget-object v0, p0, LX/2IT;->a:LX/2IU;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 391812
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "SELECT COUNT (*) FROM configurations"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, " WHERE "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 391813
    if-eqz p1, :cond_1

    invoke-virtual {p1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 391814
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 391815
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 391816
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return v0

    .line 391817
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 391818
    :cond_1
    new-array v0, v4, [Ljava/lang/String;

    goto :goto_1

    .line 391819
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static b(LX/0QB;)LX/2IT;
    .locals 3

    .prologue
    .line 391748
    new-instance v2, LX/2IT;

    invoke-static {p0}, LX/2IU;->a(LX/0QB;)LX/2IU;

    move-result-object v0

    check-cast v0, LX/2IU;

    invoke-static {p0}, LX/2IY;->a(LX/0QB;)LX/2IY;

    move-result-object v1

    check-cast v1, LX/2IY;

    invoke-direct {v2, v0, v1}, LX/2IT;-><init>(LX/2IU;LX/2IY;)V

    .line 391749
    return-object v2
.end method


# virtual methods
.method public final a(ILX/6BU;JJJ)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/6BU;",
            "JJJ)",
            "LX/0Px",
            "<",
            "Lcom/facebook/assetdownload/AssetDownloadConfiguration;",
            ">;"
        }
    .end annotation

    .prologue
    .line 391732
    const-string v0, "connectionConstraint must not be null"

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391733
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "olderThanTimestamp must be greater 0"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 391734
    if-lez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "maxNumberOfConfigurations must be greater 0"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 391735
    iget-object v0, p0, LX/2IT;->a:LX/2IU;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 391736
    const/4 v1, 0x3

    new-array v1, v1, [LX/0ux;

    const/4 v2, 0x0

    const/4 v3, 0x2

    new-array v3, v3, [LX/0ux;

    const/4 v4, 0x0

    sget-object v5, LX/2IX;->f:LX/0U1;

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0U1;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, LX/2IX;->f:LX/0U1;

    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0U1;->d(Ljava/lang/String;)LX/0ux;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x2

    new-array v3, v3, [LX/0ux;

    const/4 v4, 0x0

    sget-object v5, LX/2IX;->g:LX/0U1;

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0U1;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, LX/2IX;->g:LX/0U1;

    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0U1;->d(Ljava/lang/String;)LX/0ux;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, LX/2IX;->e:LX/0U1;

    invoke-virtual {p2}, LX/6BU;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v4

    .line 391737
    const-string v1, "configurations"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v5, LX/2IX;->a:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    const/4 v3, 0x1

    sget-object v5, LX/2IX;->c:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, LX/2IX;->d:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 391738
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 391739
    :try_start_0
    sget-object v2, LX/2IX;->c:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 391740
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 391741
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    .line 391742
    iget-object v4, p0, LX/2IT;->b:LX/2IY;

    invoke-virtual {v4, v3}, LX/2IY;->a([B)Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 391743
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 391744
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 391745
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 391746
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 391747
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;J)Z
    .locals 4

    .prologue
    .line 391726
    const-string v0, "identifier must not be null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391727
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 391728
    sget-object v1, LX/2IX;->f:LX/0U1;

    .line 391729
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 391730
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 391731
    invoke-direct {p0, p1, v0}, LX/2IT;->a(Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;J)Z
    .locals 4

    .prologue
    .line 391720
    const-string v0, "identifier must not be null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391721
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 391722
    sget-object v1, LX/2IX;->g:LX/0U1;

    .line 391723
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 391724
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 391725
    invoke-direct {p0, p1, v0}, LX/2IT;->a(Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v0

    return v0
.end method
