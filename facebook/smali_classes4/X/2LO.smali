.class public LX/2LO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2LP;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/03V;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Landroid/content/Intent;

.field private f:LX/03R;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/03V;Ljava/lang/String;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/launcherbadges/AppLaunchClass;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 394908
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394909
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.BADGE_COUNT_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/2LO;->e:Landroid/content/Intent;

    .line 394910
    iput-object p1, p0, LX/2LO;->a:Landroid/content/Context;

    .line 394911
    iput-object p2, p0, LX/2LO;->b:LX/03V;

    .line 394912
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2LO;->c:Ljava/lang/String;

    .line 394913
    iput-object p3, p0, LX/2LO;->d:Ljava/lang/String;

    .line 394914
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2LO;->f:LX/03R;

    .line 394915
    return-void
.end method


# virtual methods
.method public final a(I)LX/03R;
    .locals 4

    .prologue
    .line 394916
    iget-object v0, p0, LX/2LO;->f:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_1

    .line 394917
    iget-object v0, p0, LX/2LO;->e:Landroid/content/Intent;

    const/4 v1, 0x0

    .line 394918
    iget-object v2, p0, LX/2LO;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 394919
    invoke-virtual {v2, v0, v1}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 394920
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 394921
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/2LO;->f:LX/03R;

    .line 394922
    :cond_1
    iget-object v0, p0, LX/2LO;->f:LX/03R;

    sget-object v1, LX/03R;->NO:LX/03R;

    if-ne v0, v1, :cond_2

    .line 394923
    sget-object v0, LX/03R;->NO:LX/03R;

    .line 394924
    :goto_0
    return-object v0

    .line 394925
    :cond_2
    :try_start_0
    iget-object v0, p0, LX/2LO;->e:Landroid/content/Intent;

    const-string v1, "badge_count"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 394926
    iget-object v0, p0, LX/2LO;->e:Landroid/content/Intent;

    const-string v1, "badge_count_package_name"

    iget-object v2, p0, LX/2LO;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 394927
    iget-object v0, p0, LX/2LO;->e:Landroid/content/Intent;

    const-string v1, "badge_count_class_name"

    iget-object v2, p0, LX/2LO;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 394928
    iget-object v0, p0, LX/2LO;->a:Landroid/content/Context;

    iget-object v1, p0, LX/2LO;->e:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 394929
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    .line 394930
    :catch_0
    move-exception v0

    .line 394931
    iget-object v1, p0, LX/2LO;->b:LX/03V;

    const-string v2, "generic_launcher_badging"

    const-string v3, "exception"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 394932
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method
