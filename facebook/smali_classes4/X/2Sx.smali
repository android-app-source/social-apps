.class public LX/2Sx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2Sx;


# instance fields
.field private final a:LX/1MZ;

.field private final b:LX/0SG;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/7G9;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/7G9;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/7G9;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1MZ;LX/0SG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 413603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413604
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2Sx;->c:Ljava/util/Map;

    .line 413605
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2Sx;->d:Ljava/util/Map;

    .line 413606
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2Sx;->e:Ljava/util/Map;

    .line 413607
    iput-object p1, p0, LX/2Sx;->a:LX/1MZ;

    .line 413608
    iput-object p2, p0, LX/2Sx;->b:LX/0SG;

    .line 413609
    return-void
.end method

.method public static a(LX/0QB;)LX/2Sx;
    .locals 5

    .prologue
    .line 413610
    sget-object v0, LX/2Sx;->f:LX/2Sx;

    if-nez v0, :cond_1

    .line 413611
    const-class v1, LX/2Sx;

    monitor-enter v1

    .line 413612
    :try_start_0
    sget-object v0, LX/2Sx;->f:LX/2Sx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413613
    if-eqz v2, :cond_0

    .line 413614
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 413615
    new-instance p0, LX/2Sx;

    invoke-static {v0}, LX/1MZ;->a(LX/0QB;)LX/1MZ;

    move-result-object v3

    check-cast v3, LX/1MZ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/2Sx;-><init>(LX/1MZ;LX/0SG;)V

    .line 413616
    move-object v0, p0

    .line 413617
    sput-object v0, LX/2Sx;->f:LX/2Sx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413618
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413619
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413620
    :cond_1
    sget-object v0, LX/2Sx;->f:LX/2Sx;

    return-object v0

    .line 413621
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413622
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized d(LX/7G9;)J
    .locals 2

    .prologue
    .line 413623
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Sx;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413624
    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_0

    .line 413625
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e(LX/7G9;)Z
    .locals 2

    .prologue
    .line 413626
    monitor-enter p0

    :try_start_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, LX/2Sx;->e:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/7G9;J)V
    .locals 2

    .prologue
    .line 413627
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Sx;->c:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413628
    iget-object v0, p0, LX/2Sx;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413629
    monitor-exit p0

    return-void

    .line 413630
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 413631
    iget-object v0, p0, LX/2Sx;->a:LX/1MZ;

    invoke-virtual {v0}, LX/1MZ;->d()Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized a(LX/7G9;)Z
    .locals 6

    .prologue
    .line 413632
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, LX/2Sx;->d(LX/7G9;)J

    move-result-wide v0

    .line 413633
    iget-object v2, p0, LX/2Sx;->a:LX/1MZ;

    invoke-virtual {v2}, LX/1MZ;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, p1}, LX/2Sx;->e(LX/7G9;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/2Sx;->a:LX/1MZ;

    invoke-virtual {v2}, LX/1MZ;->c()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_0

    iget-object v2, p0, LX/2Sx;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    const-wide/32 v4, 0x124f80

    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 413634
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/7G9;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 413635
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Sx;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413636
    if-nez v0, :cond_0

    move v0, v1

    .line 413637
    :goto_0
    monitor-exit p0

    return v0

    .line 413638
    :cond_0
    :try_start_1
    iget-object v2, p0, LX/2Sx;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v6, 0xea60

    add-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 413639
    iget-object v0, p0, LX/2Sx;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 413640
    goto :goto_0

    .line 413641
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 413642
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(LX/7G9;)V
    .locals 2

    .prologue
    .line 413643
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Sx;->e:Ljava/util/Map;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413644
    monitor-exit p0

    return-void

    .line 413645
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
