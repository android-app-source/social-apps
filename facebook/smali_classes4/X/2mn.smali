.class public LX/2mn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/2mn;


# instance fields
.field private final a:Landroid/view/Display;

.field public final b:Landroid/content/Context;

.field public final c:LX/1V8;

.field public final d:LX/0ad;

.field public final e:LX/0sV;

.field public final f:LX/0hI;

.field private final g:LX/19m;


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;Landroid/content/Context;LX/1V8;LX/0ad;LX/0sV;LX/19m;LX/0hI;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 462083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462084
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, LX/2mn;->a:Landroid/view/Display;

    .line 462085
    iput-object p2, p0, LX/2mn;->b:Landroid/content/Context;

    .line 462086
    iput-object p3, p0, LX/2mn;->c:LX/1V8;

    .line 462087
    iput-object p4, p0, LX/2mn;->d:LX/0ad;

    .line 462088
    iput-object p5, p0, LX/2mn;->e:LX/0sV;

    .line 462089
    iput-object p7, p0, LX/2mn;->f:LX/0hI;

    .line 462090
    iput-object p6, p0, LX/2mn;->g:LX/19m;

    .line 462091
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;I)I
    .locals 2

    .prologue
    .line 462081
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 462082
    int-to-float v1, p1

    div-float v0, v1, v0

    float-to-int v0, v0

    return v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLMedia;IFZ)I
    .locals 10

    .prologue
    .line 462008
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-eqz v0, :cond_0

    .line 462009
    int-to-float v0, p2

    div-float/2addr v0, p3

    float-to-int v0, v0

    .line 462010
    :goto_0
    return v0

    .line 462011
    :cond_0
    const/high16 v8, 0x3f800000    # 1.0f

    .line 462012
    if-eqz p4, :cond_2

    iget-object v6, p0, LX/2mn;->d:LX/0ad;

    sget v7, LX/0ws;->n:F

    invoke-interface {v6, v7, v8}, LX/0ad;->a(FF)F

    move-result v6

    float-to-double v6, v6

    .line 462013
    :goto_1
    const-wide/16 v8, 0x0

    cmpl-double v8, v6, v8

    if-nez v8, :cond_1

    .line 462014
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 462015
    :cond_1
    move-wide v0, v6

    .line 462016
    invoke-static {p1, p2}, LX/2mn;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)I

    move-result v2

    int-to-double v4, p2

    mul-double/2addr v0, v4

    double-to-int v0, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 462017
    :cond_2
    iget-object v6, p0, LX/2mn;->d:LX/0ad;

    sget v7, LX/0ws;->m:F

    invoke-interface {v6, v7, v8}, LX/0ad;->a(FF)F

    move-result v6

    float-to-double v6, v6

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/2mn;
    .locals 11

    .prologue
    .line 462068
    sget-object v0, LX/2mn;->h:LX/2mn;

    if-nez v0, :cond_1

    .line 462069
    const-class v1, LX/2mn;

    monitor-enter v1

    .line 462070
    :try_start_0
    sget-object v0, LX/2mn;->h:LX/2mn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 462071
    if-eqz v2, :cond_0

    .line 462072
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 462073
    new-instance v3, LX/2mn;

    invoke-static {v0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v6

    check-cast v6, LX/1V8;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v8

    check-cast v8, LX/0sV;

    invoke-static {v0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object v9

    check-cast v9, LX/19m;

    invoke-static {v0}, LX/0hI;->a(LX/0QB;)LX/0hI;

    move-result-object v10

    check-cast v10, LX/0hI;

    invoke-direct/range {v3 .. v10}, LX/2mn;-><init>(Landroid/view/WindowManager;Landroid/content/Context;LX/1V8;LX/0ad;LX/0sV;LX/19m;LX/0hI;)V

    .line 462074
    move-object v0, v3

    .line 462075
    sput-object v0, LX/2mn;->h:LX/2mn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 462076
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 462077
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 462078
    :cond_1
    sget-object v0, LX/2mn;->h:LX/2mn;

    return-object v0

    .line 462079
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 462080
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Lcom/facebook/feed/rows/core/props/FeedProps;FLandroid/graphics/Point;)LX/3FO;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;F",
            "Landroid/graphics/Point;",
            ")",
            "LX/3FO;"
        }
    .end annotation

    .prologue
    .line 462060
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 462061
    move-object v3, v0

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 462062
    iget v1, p3, Landroid/graphics/Point;->x:I

    .line 462063
    iget v5, p3, Landroid/graphics/Point;->y:I

    .line 462064
    invoke-static {p1}, LX/2mn;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    .line 462065
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2mn;->c(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462066
    const/high16 p2, 0x3f800000    # 1.0f

    .line 462067
    :cond_0
    new-instance v0, LX/3FO;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-direct {p0, v4, v1, p2, v2}, LX/2mn;->a(Lcom/facebook/graphql/model/GraphQLMedia;IFZ)I

    move-result v2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-static {v3, v1}, LX/2mn;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)I

    move-result v4

    move v3, v1

    invoke-direct/range {v0 .. v5}, LX/3FO;-><init>(IIIII)V

    return-object v0
.end method

.method private static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 462058
    invoke-static {p0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 462059
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 462050
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 462051
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 462052
    invoke-static {p1}, LX/2mn;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    .line 462053
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2mn;->g:LX/19m;

    .line 462054
    iget-boolean p1, v1, LX/19m;->j:Z

    move v1, p1

    .line 462055
    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->at()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/2mn;->g:LX/19m;

    .line 462056
    iget-boolean v1, v0, LX/19m;->d:Z

    move v0, v1

    .line 462057
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/facebook/graphql/model/GraphQLMedia;)Z
    .locals 1

    .prologue
    .line 462047
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->at()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2mn;->g:LX/19m;

    .line 462048
    iget-boolean p0, v0, LX/19m;->e:Z

    move v0, p0

    .line 462049
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;F)",
            "LX/3FO;"
        }
    .end annotation

    .prologue
    .line 462027
    invoke-virtual {p0}, LX/2mn;->a()Landroid/graphics/Point;

    move-result-object v1

    .line 462028
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 462029
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 462030
    invoke-direct {p0, p1}, LX/2mn;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 462031
    iget-object v0, p0, LX/2mn;->g:LX/19m;

    .line 462032
    iget v2, v0, LX/19m;->m:F

    move v0, v2

    .line 462033
    invoke-direct {p0, p1, v0, v1}, LX/2mn;->b(Lcom/facebook/feed/rows/core/props/FeedProps;FLandroid/graphics/Point;)LX/3FO;

    move-result-object v0

    .line 462034
    :goto_0
    return-object v0

    .line 462035
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v3

    if-le v2, v3, :cond_3

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 462036
    if-eqz v2, :cond_2

    iget-object v2, p0, LX/2mn;->e:LX/0sV;

    iget-boolean v2, v2, LX/0sV;->t:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    move v0, v2

    .line 462037
    if-eqz v0, :cond_1

    .line 462038
    iget-object v3, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 462039
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 462040
    invoke-virtual {p0}, LX/2mn;->a()Landroid/graphics/Point;

    move-result-object v4

    .line 462041
    iget v8, v4, Landroid/graphics/Point;->y:I

    .line 462042
    iget v4, v4, Landroid/graphics/Point;->x:I

    .line 462043
    iget-object v5, p0, LX/2mn;->f:LX/0hI;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/0hI;->a(Landroid/view/Window;)I

    move-result v5

    sub-int v5, v8, v5

    .line 462044
    invoke-static {v3, v4}, LX/2mn;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)I

    move-result v3

    invoke-static {v5, v3}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 462045
    new-instance v3, LX/3FO;

    move v6, v4

    move v7, v5

    invoke-direct/range {v3 .. v8}, LX/3FO;-><init>(IIIII)V

    move-object v0, v3

    .line 462046
    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2, v1}, LX/2mn;->b(Lcom/facebook/feed/rows/core/props/FeedProps;FLandroid/graphics/Point;)LX/3FO;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;F)LX/3FO;
    .locals 7

    .prologue
    .line 462025
    invoke-virtual {p0}, LX/2mn;->a()Landroid/graphics/Point;

    move-result-object v5

    .line 462026
    new-instance v0, LX/3FO;

    iget v1, v5, Landroid/graphics/Point;->x:I

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    iget v3, v5, Landroid/graphics/Point;->x:I

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, p2, v4}, LX/2mn;->a(Lcom/facebook/graphql/model/GraphQLMedia;IFZ)I

    move-result v2

    iget v3, v5, Landroid/graphics/Point;->x:I

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    iget v6, v5, Landroid/graphics/Point;->x:I

    invoke-static {v4, v6}, LX/2mn;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)I

    move-result v4

    iget v5, v5, Landroid/graphics/Point;->y:I

    invoke-direct/range {v0 .. v5}, LX/3FO;-><init>(IIIII)V

    return-object v0
.end method

.method public final a()Landroid/graphics/Point;
    .locals 2

    .prologue
    .line 462022
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 462023
    iget-object v1, p0, LX/2mn;->a:Landroid/view/Display;

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 462024
    return-object v0
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;F)",
            "LX/3FO;"
        }
    .end annotation

    .prologue
    .line 462018
    invoke-virtual {p0}, LX/2mn;->a()Landroid/graphics/Point;

    move-result-object v0

    .line 462019
    iget v1, v0, Landroid/graphics/Point;->x:I

    .line 462020
    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 462021
    invoke-direct {p0, p1, p2, v0}, LX/2mn;->b(Lcom/facebook/feed/rows/core/props/FeedProps;FLandroid/graphics/Point;)LX/3FO;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;F)",
            "LX/3FO;"
        }
    .end annotation

    .prologue
    .line 461989
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 461990
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 461991
    invoke-static {v0}, LX/1VO;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/1VO;->l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    .line 461992
    :goto_0
    if-nez v1, :cond_1

    invoke-static {v0}, LX/2v7;->i(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 461993
    :cond_1
    invoke-virtual {p0}, LX/2mn;->a()Landroid/graphics/Point;

    move-result-object v1

    .line 461994
    iget-object v2, p0, LX/2mn;->c:LX/1V8;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-virtual {v2, v3}, LX/1V8;->a(LX/1Ua;)LX/1Ub;

    move-result-object v2

    .line 461995
    iget-object v3, v2, LX/1Ub;->d:LX/1Uc;

    move-object v2, v3

    .line 461996
    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/1Uc;->a(I)F

    move-result v2

    .line 461997
    iget-object v3, p0, LX/2mn;->b:Landroid/content/Context;

    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v2, v0

    invoke-static {v3, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    .line 461998
    iget v3, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v2

    iput v3, v1, Landroid/graphics/Point;->x:I

    .line 461999
    iget v3, v1, Landroid/graphics/Point;->y:I

    sub-int v2, v3, v2

    iput v2, v1, Landroid/graphics/Point;->y:I

    .line 462000
    move-object v1, v1

    .line 462001
    :goto_1
    move-object v0, v1

    .line 462002
    invoke-direct {p0, p1}, LX/2mn;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 462003
    iget-object v1, p0, LX/2mn;->g:LX/19m;

    .line 462004
    iget v2, v1, LX/19m;->l:F

    move p2, v2

    .line 462005
    :cond_2
    invoke-direct {p0, p1, p2, v0}, LX/2mn;->b(Lcom/facebook/feed/rows/core/props/FeedProps;FLandroid/graphics/Point;)LX/3FO;

    move-result-object v0

    return-object v0

    .line 462006
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 462007
    :cond_4
    invoke-virtual {p0}, LX/2mn;->a()Landroid/graphics/Point;

    move-result-object v1

    goto :goto_1
.end method

.method public final d(Lcom/facebook/feed/rows/core/props/FeedProps;F)D
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;F)D"
        }
    .end annotation

    .prologue
    .line 461985
    invoke-virtual {p0, p1, p2}, LX/2mn;->c(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;

    move-result-object v0

    .line 461986
    iget v1, v0, LX/3FO;->d:I

    if-eqz v1, :cond_0

    .line 461987
    iget v1, v0, LX/3FO;->c:I

    int-to-double v2, v1

    iget v0, v0, LX/3FO;->d:I

    int-to-double v0, v0

    div-double v0, v2, v0

    .line 461988
    :goto_0
    return-wide v0

    :cond_0
    float-to-double v0, p2

    goto :goto_0
.end method
