.class public LX/27G;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 372891
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 372892
    return-void
.end method

.method public static a(LX/2Cl;)LX/03R;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/annotations/IsLoginErrorRegEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 372890
    sget-object v0, LX/2Cm;->LOGIN_ERROR_REG:LX/2Cm;

    invoke-virtual {p0, v0}, LX/2Cl;->a(LX/2Cm;)LX/2KF;

    move-result-object v0

    invoke-virtual {v0}, LX/2KF;->a()LX/03R;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/login/LoginActivityComponent;
    .end annotation

    .prologue
    .line 372889
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.facebook.katana.dbl.activity.FacebookLoginActivity"

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(LX/2Cl;)LX/03R;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/annotations/IsLoginErrorRegEmailEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 372888
    sget-object v0, LX/2Cm;->LOGIN_ERROR_REG_EMAIL:LX/2Cm;

    invoke-virtual {p0, v0}, LX/2Cl;->a(LX/2Cm;)LX/2KF;

    move-result-object v0

    invoke-virtual {v0}, LX/2KF;->a()LX/03R;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/login/LogoutActivityComponent;
    .end annotation

    .prologue
    .line 372887
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.facebook.katana.LogoutActivity"

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b()Ljava/lang/Boolean;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/login/UseAuthLogin;
    .end annotation

    .prologue
    .line 372886
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/2Cl;)LX/03R;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/annotations/IsLoginErrorRegPhoneEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 372880
    sget-object v0, LX/2Cm;->LOGIN_ERROR_REG_PHONE:LX/2Cm;

    invoke-virtual {p0, v0}, LX/2Cl;->a(LX/2Cm;)LX/2KF;

    move-result-object v0

    invoke-virtual {v0}, LX/2KF;->a()LX/03R;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/2Cl;)LX/03R;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/annotations/IsFullWidthRegButtonEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 372885
    sget-object v0, LX/2Cm;->FULL_WIDTH_REG_BUTTON:LX/2Cm;

    invoke-virtual {p0, v0}, LX/2Cl;->a(LX/2Cm;)LX/2KF;

    move-result-object v0

    invoke-virtual {v0}, LX/2KF;->a()LX/03R;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/2Cl;)LX/03R;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/annotations/IsReuseLoginFieldsForRegEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 372884
    sget-object v0, LX/2Cm;->LOGIN_TO_REG_PREFILL:LX/2Cm;

    invoke-virtual {p0, v0}, LX/2Cl;->a(LX/2Cm;)LX/2KF;

    move-result-object v0

    invoke-virtual {v0}, LX/2KF;->a()LX/03R;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/2Cl;)LX/03R;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/annotations/IsWhiteLoginPageEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 372883
    sget-object v0, LX/2Cm;->WHITE_LOGIN_PAGE:LX/2Cm;

    invoke-virtual {p0, v0}, LX/2Cl;->a(LX/2Cm;)LX/2KF;

    move-result-object v0

    invoke-virtual {v0}, LX/2KF;->a()LX/03R;

    move-result-object v0

    return-object v0
.end method

.method public static g(LX/2Cl;)LX/03R;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/annotations/IsHideRegisterButtonEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 372882
    sget-object v0, LX/2Cm;->HIDE_REGISTER_BUTTON:LX/2Cm;

    invoke-virtual {p0, v0}, LX/2Cl;->a(LX/2Cm;)LX/2KF;

    move-result-object v0

    invoke-virtual {v0}, LX/2KF;->a()LX/03R;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 372881
    return-void
.end method
