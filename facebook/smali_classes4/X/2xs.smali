.class public LX/2xs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0tE;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0tG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 479489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 479490
    return-void
.end method

.method public static a(LX/0QB;)LX/2xs;
    .locals 4

    .prologue
    .line 479491
    new-instance v3, LX/2xs;

    invoke-direct {v3}, LX/2xs;-><init>()V

    .line 479492
    invoke-static {p0}, LX/0tE;->b(LX/0QB;)LX/0tE;

    move-result-object v0

    check-cast v0, LX/0tE;

    invoke-static {p0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v1

    check-cast v1, LX/0tG;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    .line 479493
    iput-object v0, v3, LX/2xs;->a:LX/0tE;

    iput-object v1, v3, LX/2xs;->b:LX/0tG;

    iput-object v2, v3, LX/2xs;->c:LX/0ad;

    .line 479494
    move-object v0, v3

    .line 479495
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/ufiservices/common/AddCommentParams;)LX/399;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/AddCommentParams;",
            ")",
            "LX/399",
            "<",
            "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 479496
    new-instance v2, LX/4DN;

    invoke-direct {v2}, LX/4DN;-><init>()V

    .line 479497
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->g:Ljava/lang/String;

    .line 479498
    const-string v1, "idempotence_token"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 479499
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/4DN;->d(Ljava/lang/String;)LX/4DN;

    .line 479500
    new-instance v3, LX/4Jk;

    invoke-direct {v3}, LX/4Jk;-><init>()V

    .line 479501
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 479502
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->e:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/4Jk;->a(Ljava/lang/String;)LX/4Jk;

    .line 479503
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 479504
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->e:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_0

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 479505
    new-instance v7, LX/4Jj;

    invoke-direct {v7}, LX/4Jj;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v8

    .line 479506
    const-string v9, "id"

    invoke-virtual {v7, v9, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 479507
    move-object v7, v7

    .line 479508
    new-instance v8, LX/4Jl;

    invoke-direct {v8}, LX/4Jl;-><init>()V

    .line 479509
    const-string v9, "entity"

    invoke-virtual {v8, v9, v7}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 479510
    move-object v7, v8

    .line 479511
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 479512
    const-string v9, "length"

    invoke-virtual {v7, v9, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 479513
    move-object v7, v7

    .line 479514
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 479515
    const-string v8, "offset"

    invoke-virtual {v7, v8, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 479516
    move-object v0, v7

    .line 479517
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 479518
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 479519
    :cond_0
    invoke-virtual {v2, v3}, LX/4DN;->a(LX/4Jk;)LX/4DN;

    .line 479520
    const-string v0, "ranges"

    invoke-virtual {v3, v0, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 479521
    :cond_1
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 479522
    new-instance v0, LX/4D4;

    invoke-direct {v0}, LX/4D4;-><init>()V

    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4D4;->a(Ljava/lang/String;)LX/4D4;

    move-result-object v0

    .line 479523
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 479524
    new-instance v3, LX/4DM;

    invoke-direct {v3}, LX/4DM;-><init>()V

    invoke-virtual {v3, v0}, LX/4DM;->a(LX/4D4;)LX/4DM;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 479525
    invoke-virtual {v2, v1}, LX/4DN;->a(Ljava/util/List;)LX/4DN;

    .line 479526
    :cond_2
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-eqz v0, :cond_5

    .line 479527
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f()LX/0Px;

    move-result-object v0

    .line 479528
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 479529
    const-string v1, "tracking"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 479530
    :cond_3
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 479531
    iget-object v1, v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v0, v1

    .line 479532
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 479533
    const-string v1, "feedback_source"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 479534
    :cond_4
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 479535
    iget-object v1, v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->d:Ljava/lang/String;

    move-object v0, v1

    .line 479536
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 479537
    const-string v1, "feedback_referrer"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 479538
    :cond_5
    iget-boolean v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->j:Z

    if-eqz v0, :cond_7

    iget v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->k:I

    if-lez v0, :cond_7

    .line 479539
    iget v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->k:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 479540
    const-string v1, "live_video_timestamp"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 479541
    :goto_1
    move-object v0, v2

    .line 479542
    new-instance v1, LX/58u;

    invoke-direct {v1}, LX/58u;-><init>()V

    new-instance v2, LX/58x;

    invoke-direct {v2}, LX/58x;-><init>()V

    iget-object v3, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->a:Ljava/lang/String;

    .line 479543
    iput-object v3, v2, LX/58x;->a:Ljava/lang/String;

    .line 479544
    move-object v2, v2

    .line 479545
    iget-object v3, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->b:Ljava/lang/String;

    .line 479546
    iput-object v3, v2, LX/58x;->b:Ljava/lang/String;

    .line 479547
    move-object v2, v2

    .line 479548
    const/4 v8, 0x1

    const/4 v11, 0x0

    const/4 v6, 0x0

    .line 479549
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 479550
    iget-object v5, v2, LX/58x;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 479551
    iget-object v7, v2, LX/58x;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 479552
    iget-object v9, v2, LX/58x;->c:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel$TopLevelCommentsModel;

    invoke-static {v4, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 479553
    const/4 v10, 0x3

    invoke-virtual {v4, v10}, LX/186;->c(I)V

    .line 479554
    invoke-virtual {v4, v11, v5}, LX/186;->b(II)V

    .line 479555
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 479556
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 479557
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 479558
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 479559
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 479560
    invoke-virtual {v5, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 479561
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 479562
    new-instance v5, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    invoke-direct {v5, v4}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;-><init>(LX/15i;)V

    .line 479563
    move-object v2, v5

    .line 479564
    iput-object v2, v1, LX/58u;->b:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    .line 479565
    move-object v1, v1

    .line 479566
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->e:Lcom/facebook/graphql/model/GraphQLComment;

    .line 479567
    iput-object v2, v1, LX/58u;->a:Lcom/facebook/graphql/model/GraphQLComment;

    .line 479568
    move-object v1, v1

    .line 479569
    invoke-virtual {v1}, LX/58u;->a()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    move-result-object v1

    .line 479570
    new-instance v2, LX/58m;

    invoke-direct {v2}, LX/58m;-><init>()V

    .line 479571
    iget-object v3, p0, LX/2xs;->b:LX/0tG;

    invoke-virtual {v3, v2}, LX/0tG;->a(LX/0gW;)V

    .line 479572
    const-string v3, "input"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 479573
    iget-object v0, p0, LX/2xs;->a:LX/0tE;

    invoke-virtual {v0, v2}, LX/0tE;->b(LX/0gW;)V

    .line 479574
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->e:Lcom/facebook/graphql/model/GraphQLComment;

    .line 479575
    if-eqz v0, :cond_8

    .line 479576
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    .line 479577
    if-eqz v3, :cond_8

    .line 479578
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    .line 479579
    :goto_2
    move-object v0, v3

    .line 479580
    new-instance v3, LX/399;

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    :goto_3
    invoke-direct {v3, v2, v0}, LX/399;-><init>(LX/0zP;LX/0Rf;)V

    .line 479581
    invoke-virtual {v3, v1}, LX/399;->a(LX/0jT;)LX/399;

    .line 479582
    return-object v3

    .line 479583
    :cond_6
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 479584
    goto :goto_3

    .line 479585
    :cond_7
    iget v0, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->k:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/4DN;->b(Ljava/lang/Integer;)LX/4DN;

    goto/16 :goto_1

    :cond_8
    const/4 v3, 0x0

    goto :goto_2
.end method
