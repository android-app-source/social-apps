.class public LX/2k3;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2k3;


# direct methods
.method public constructor <init>()V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 455287
    const-string v0, "graph_cursors"

    const/16 v1, 0x28

    new-instance v2, LX/2k4;

    invoke-direct {v2}, LX/2k4;-><init>()V

    new-instance v3, LX/2k6;

    invoke-direct {v3}, LX/2k6;-><init>()V

    new-instance v4, LX/2k8;

    invoke-direct {v4}, LX/2k8;-><init>()V

    new-instance v5, LX/2kA;

    invoke-direct {v5}, LX/2kA;-><init>()V

    invoke-static {v2, v3, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 455288
    return-void
.end method

.method public static a(LX/0QB;)LX/2k3;
    .locals 3

    .prologue
    .line 455289
    sget-object v0, LX/2k3;->a:LX/2k3;

    if-nez v0, :cond_1

    .line 455290
    const-class v1, LX/2k3;

    monitor-enter v1

    .line 455291
    :try_start_0
    sget-object v0, LX/2k3;->a:LX/2k3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 455292
    if-eqz v2, :cond_0

    .line 455293
    :try_start_1
    new-instance v0, LX/2k3;

    invoke-direct {v0}, LX/2k3;-><init>()V

    .line 455294
    move-object v0, v0

    .line 455295
    sput-object v0, LX/2k3;->a:LX/2k3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 455296
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 455297
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 455298
    :cond_1
    sget-object v0, LX/2k3;->a:LX/2k3;

    return-object v0

    .line 455299
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 455300
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 455301
    invoke-static {p2}, LX/2k0;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object p0

    invoke-static {p1, p0}, LX/2k0;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/File;)V

    .line 455302
    return-void
.end method
