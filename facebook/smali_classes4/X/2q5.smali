.class public LX/2q5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/ScheduledExecutorService;

.field private c:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 1

    .prologue
    .line 469321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 469322
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/2q5;->a:Ljava/util/Queue;

    .line 469323
    iput-object p1, p0, LX/2q5;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 469324
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/2q5;)V
    .locals 3

    .prologue
    .line 469325
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2q5;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, LX/2q5;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 469326
    iget-object v0, p0, LX/2q5;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/2q5;->c:Ljava/lang/Runnable;

    const v2, 0x31583510

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469327
    :cond_0
    monitor-exit p0

    return-void

    .line 469328
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 469329
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2q5;->a:Ljava/util/Queue;

    new-instance v1, Lcom/facebook/video/engine/CommandQueue$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/CommandQueue$1;-><init>(LX/2q5;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 469330
    iget-object v0, p0, LX/2q5;->c:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 469331
    invoke-static {p0}, LX/2q5;->a$redex0(LX/2q5;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469332
    :cond_0
    monitor-exit p0

    return-void

    .line 469333
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
