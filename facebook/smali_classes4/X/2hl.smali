.class public LX/2hl;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2hm;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/17W;

.field public final c:Landroid/content/Context;

.field public final d:LX/0gc;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 450800
    sget-object v0, LX/2hm;->CONTACTS:LX/2hm;

    sget-object v1, LX/2hm;->SUGGESTIONS:LX/2hm;

    sget-object v2, LX/2hm;->SEE_ALL_FRIENDS:LX/2hm;

    sget-object v3, LX/2hm;->SEARCH:LX/2hm;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2hl;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/17W;LX/0ad;LX/0Ot;Landroid/content/Context;LX/0gc;)V
    .locals 2
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p4    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17W;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Landroid/content/Context;",
            "LX/0gc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 450801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450802
    iput-object p1, p0, LX/2hl;->b:LX/17W;

    .line 450803
    iput-object p3, p0, LX/2hl;->e:LX/0Ot;

    .line 450804
    iput-object p4, p0, LX/2hl;->c:Landroid/content/Context;

    .line 450805
    iput-object p5, p0, LX/2hl;->d:LX/0gc;

    .line 450806
    sget-short v0, LX/2hr;->c:S

    invoke-interface {p2, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2hl;->f:Z

    .line 450807
    sget-short v0, LX/2hr;->d:S

    invoke-interface {p2, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2hl;->g:Z

    .line 450808
    return-void
.end method

.method public static a(LX/2hl;LX/2hm;)LX/Ez3;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 450809
    new-instance v0, LX/Ez3;

    iget-object v1, p0, LX/2hl;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p1, LX/2hm;->colorRes:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iget v2, p1, LX/2hm;->drawableRes:I

    iget-object v3, p0, LX/2hl;->c:Landroid/content/Context;

    iget v4, p1, LX/2hm;->labelRes:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, LX/2hm;->analyticsName:Ljava/lang/String;

    invoke-virtual {p1, p0}, LX/2hm;->getSproutLauncher(LX/2hl;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/Ez3;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-object v0
.end method

.method public static a$redex0(LX/2hl;Ljava/lang/String;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 450810
    new-instance v0, Lcom/facebook/friending/jewel/FriendingJewelSproutLauncher$2;

    invoke-direct {v0, p0, p1}, Lcom/facebook/friending/jewel/FriendingJewelSproutLauncher$2;-><init>(LX/2hl;Ljava/lang/String;)V

    return-object v0
.end method
