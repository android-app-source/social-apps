.class public LX/3Qa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Pw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/3Qa;


# instance fields
.field private final b:LX/0Xl;

.field public final c:LX/0Uh;

.field public d:LX/2Gc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 565377
    const-class v0, LX/3Qa;

    sput-object v0, LX/3Qa;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Xl;LX/0Uh;LX/2Gc;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 565372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565373
    iput-object p1, p0, LX/3Qa;->b:LX/0Xl;

    .line 565374
    iput-object p2, p0, LX/3Qa;->c:LX/0Uh;

    .line 565375
    iput-object p3, p0, LX/3Qa;->d:LX/2Gc;

    .line 565376
    return-void
.end method

.method public static a(LX/0QB;)LX/3Qa;
    .locals 6

    .prologue
    .line 565359
    sget-object v0, LX/3Qa;->e:LX/3Qa;

    if-nez v0, :cond_1

    .line 565360
    const-class v1, LX/3Qa;

    monitor-enter v1

    .line 565361
    :try_start_0
    sget-object v0, LX/3Qa;->e:LX/3Qa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 565362
    if-eqz v2, :cond_0

    .line 565363
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 565364
    new-instance p0, LX/3Qa;

    invoke-static {v0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/2Gc;->a(LX/0QB;)LX/2Gc;

    move-result-object v5

    check-cast v5, LX/2Gc;

    invoke-direct {p0, v3, v4, v5}, LX/3Qa;-><init>(LX/0Xl;LX/0Uh;LX/2Gc;)V

    .line 565365
    move-object v0, p0

    .line 565366
    sput-object v0, LX/3Qa;->e:LX/3Qa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565367
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 565368
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 565369
    :cond_1
    sget-object v0, LX/3Qa;->e:LX/3Qa;

    return-object v0

    .line 565370
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 565371
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 565358
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0lF;Lcom/facebook/push/PushProperty;)V
    .locals 4

    .prologue
    .line 565340
    sget-object v0, LX/3B4;->C2DM:LX/3B4;

    iget-object v1, p2, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v0, v1}, LX/3B4;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 565341
    const-string v0, "type"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 565342
    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->WEBRTC_VOIP_CALL:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v0}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 565343
    :cond_0
    :goto_0
    return-void

    .line 565344
    :cond_1
    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->ZP:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v0}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 565345
    iget-object v3, p0, LX/3Qa;->c:LX/0Uh;

    const/16 p2, 0x5d0

    invoke-virtual {v3, p2, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 565346
    :cond_2
    :goto_1
    move v1, v1

    .line 565347
    if-eqz v1, :cond_3

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->WAKEUP_MQTT:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v0}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 565348
    :cond_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 565349
    const-string v2, "com.facebook.rti.mqtt.intent.ACTION_WAKEUP"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 565350
    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->WAKEUP_MQTT:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v2, v0}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 565351
    const-string v0, "params"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v2, "exp_mqtt_sid"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 565352
    if-eqz v0, :cond_4

    .line 565353
    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 565354
    const-string v0, "EXPIRED_SESSION"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 565355
    :cond_4
    iget-object v0, p0, LX/3Qa;->b:LX/0Xl;

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 565356
    :cond_5
    iget-object v3, p0, LX/3Qa;->d:LX/2Gc;

    invoke-virtual {v3}, LX/2Gc;->a()Ljava/util/Set;

    move-result-object v3

    .line 565357
    sget-object p2, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-interface {v3, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_6

    sget-object p2, LX/2Ge;->FBNS:LX/2Ge;

    invoke-interface {v3, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_6
    move v1, v2

    goto :goto_1
.end method
