.class public LX/2Xc;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:J


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/2WV;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 420053
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/2Xc;->a:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 420049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420050
    iput-object p1, p0, LX/2Xc;->b:Landroid/content/Context;

    .line 420051
    new-instance v0, LX/2WV;

    invoke-direct {v0, p1}, LX/2WV;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/2Xc;->c:LX/2WV;

    .line 420052
    return-void
.end method

.method private b(Landroid/content/Intent;LX/2WW;)I
    .locals 11

    .prologue
    const/4 v6, 0x3

    .line 420020
    if-nez p1, :cond_0

    .line 420021
    new-instance v0, LX/403;

    const-string v1, "Received a null intent in runJobFromService, did you ever return START_STICKY?"

    invoke-direct {v0, v1}, LX/403;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420022
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, LX/2Xc;->b:Landroid/content/Context;

    invoke-static {v0, v1}, LX/2Es;->a(Landroid/os/Bundle;Landroid/content/Context;)LX/2Es;

    move-result-object v1

    .line 420023
    iget-object v7, v1, LX/2Es;->h:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "power"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/PowerManager;

    .line 420024
    const/4 v8, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "UploadServiceLogic-"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "-service-"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v1, LX/2Es;->f:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v7

    iput-object v7, v1, LX/2Es;->j:Landroid/os/PowerManager$WakeLock;

    .line 420025
    iget-object v7, v1, LX/2Es;->j:Landroid/os/PowerManager$WakeLock;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 420026
    iget-object v7, v1, LX/2Es;->j:Landroid/os/PowerManager$WakeLock;

    sget-wide v9, LX/2Xc;->a:J

    invoke-virtual {v7, v9, v10}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 420027
    iget-object v7, v1, LX/2Es;->b:Landroid/os/Messenger;

    if-eqz v7, :cond_1

    .line 420028
    :try_start_0
    iget-object v7, v1, LX/2Es;->b:Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 420029
    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 420030
    const-string v2, "com.facebook.analytics2.logger.UPLOAD_NOW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 420031
    iget-object v0, p0, LX/2Xc;->c:LX/2WV;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2WV;

    .line 420032
    iget v2, v1, LX/2Es;->f:I

    move v2, v2

    .line 420033
    iget-object v3, v1, LX/2Es;->d:LX/2DI;

    move-object v3, v3

    .line 420034
    iget-object v4, v1, LX/2Es;->e:Ljava/lang/String;

    move-object v4, v4

    .line 420035
    new-instance v5, LX/2WX;

    const/4 v7, 0x0

    invoke-direct {v5, v2, v3, v4, v7}, LX/2WX;-><init>(ILX/2DI;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v5

    .line 420036
    new-instance v3, LX/2Xt;

    invoke-virtual {v1}, LX/2Es;->b()LX/2Xu;

    move-result-object v1

    invoke-direct {v3, v1, p2}, LX/2Xt;-><init>(LX/2Xu;LX/2WW;)V

    invoke-virtual {v0, v2, v3}, LX/2WV;->b(LX/2WX;LX/2Xu;)V

    .line 420037
    :goto_1
    return v6

    .line 420038
    :cond_2
    const-string v2, "com.facebook.analytics2.logger.USER_LOGOUT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 420039
    new-instance v2, LX/40C;

    .line 420040
    iget-object v0, v1, LX/2Es;->c:Landroid/os/Bundle;

    move-object v0, v0

    .line 420041
    invoke-direct {v2, v0}, LX/40C;-><init>(Landroid/os/Bundle;)V

    .line 420042
    iget-object v0, p0, LX/2Xc;->c:LX/2WV;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2WV;

    .line 420043
    iget v3, v1, LX/2Es;->f:I

    move v3, v3

    .line 420044
    iget-object v4, v1, LX/2Es;->d:LX/2DI;

    move-object v4, v4

    .line 420045
    iget-object v5, v1, LX/2Es;->e:Ljava/lang/String;

    move-object v5, v5

    .line 420046
    iget-object v2, v2, LX/40C;->a:Ljava/lang/String;

    invoke-static {v3, v4, v5, v2}, LX/2WX;->a(ILX/2DI;Ljava/lang/String;Ljava/lang/String;)LX/2WX;

    move-result-object v2

    new-instance v3, LX/2Xt;

    invoke-virtual {v1}, LX/2Es;->b()LX/2Xu;

    move-result-object v1

    invoke-direct {v3, v1, p2}, LX/2Xt;-><init>(LX/2Xu;LX/2WW;)V

    invoke-virtual {v0, v2, v3}, LX/2WV;->b(LX/2WX;LX/2Xu;)V

    goto :goto_1

    .line 420047
    :cond_3
    new-instance v0, LX/403;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown action="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/403;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420048
    :catch_0
    const-string v7, "UploadServiceLogic"

    const-string v8, "The peer died unexpectedly, possible wakelock gap detected."

    invoke-static {v7, v8}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;LX/2WW;)I
    .locals 3

    .prologue
    .line 420006
    :try_start_0
    invoke-direct {p0, p1, p2}, LX/2Xc;->b(Landroid/content/Intent;LX/2WW;)I
    :try_end_0
    .catch LX/403; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 420007
    :goto_0
    return v0

    .line 420008
    :catch_0
    move-exception v0

    .line 420009
    const-string v1, "UploadServiceLogic"

    const-string v2, "Misunderstood service intent: %s"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 420010
    invoke-virtual {p2}, LX/2WW;->a()V

    .line 420011
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 420018
    iget-object v0, p0, LX/2Xc;->c:LX/2WV;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2WV;

    invoke-virtual {v0, p1}, LX/2WV;->a(I)V

    .line 420019
    return-void
.end method

.method public final a(ILX/2DI;LX/2fJ;)V
    .locals 3

    .prologue
    .line 420012
    iget-object v0, p0, LX/2Xc;->c:LX/2WV;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2WV;

    const/4 v2, 0x0

    .line 420013
    new-instance v1, LX/2WX;

    invoke-direct {v1, p1, p2, v2, v2}, LX/2WX;-><init>(ILX/2DI;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v1

    .line 420014
    new-instance v2, LX/2fK;

    invoke-direct {v2, p3}, LX/2fK;-><init>(LX/2fJ;)V

    invoke-virtual {v0, v1, v2}, LX/2WV;->a(LX/2WX;LX/2Xu;)Z

    move-result v0

    .line 420015
    if-nez v0, :cond_0

    .line 420016
    const/4 v0, 0x1

    invoke-interface {p3, v0}, LX/2fJ;->a(Z)V

    .line 420017
    :cond_0
    return-void
.end method
