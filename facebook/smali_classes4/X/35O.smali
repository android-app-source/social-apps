.class public final LX/35O;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/35O;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/35Q;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/35P;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 497117
    const/4 v0, 0x0

    sput-object v0, LX/35O;->a:LX/35O;

    .line 497118
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/35O;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 497114
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 497115
    new-instance v0, LX/35P;

    invoke-direct {v0}, LX/35P;-><init>()V

    iput-object v0, p0, LX/35O;->c:LX/35P;

    .line 497116
    return-void
.end method

.method public static declared-synchronized q()LX/35O;
    .locals 2

    .prologue
    .line 497119
    const-class v1, LX/35O;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/35O;->a:LX/35O;

    if-nez v0, :cond_0

    .line 497120
    new-instance v0, LX/35O;

    invoke-direct {v0}, LX/35O;-><init>()V

    sput-object v0, LX/35O;->a:LX/35O;

    .line 497121
    :cond_0
    sget-object v0, LX/35O;->a:LX/35O;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 497122
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 497112
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    const p0, 0x7f0217ed

    invoke-virtual {v0, p0}, LX/1o5;->h(I)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    sget-object p0, LX/35P;->a:Landroid/util/SparseArray;

    invoke-interface {v0, p0}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v0

    const p0, 0x7f0810fb

    invoke-interface {v0, p0}, LX/1Di;->A(I)LX/1Di;

    move-result-object v0

    const p0, 0x7f0b00e9

    invoke-interface {v0, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const/4 p0, 0x1

    const p2, 0x7f0b00e6

    invoke-interface {v0, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    const/4 p0, 0x3

    const p2, 0x7f0b00e7

    invoke-interface {v0, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 497113
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 497110
    invoke-static {}, LX/1dS;->b()V

    .line 497111
    const/4 v0, 0x0

    return-object v0
.end method
