.class public LX/2nE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2nE;


# instance fields
.field public a:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "LX/7hz;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1Bn;

.field public final c:LX/1Br;


# direct methods
.method public constructor <init>(LX/1Bn;LX/1Br;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463581
    iput-object p1, p0, LX/2nE;->b:LX/1Bn;

    .line 463582
    iput-object p2, p0, LX/2nE;->c:LX/1Br;

    .line 463583
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, LX/2nE;->a:Landroid/util/LruCache;

    .line 463584
    return-void
.end method

.method public static a(LX/0QB;)LX/2nE;
    .locals 5

    .prologue
    .line 463585
    sget-object v0, LX/2nE;->d:LX/2nE;

    if-nez v0, :cond_1

    .line 463586
    const-class v1, LX/2nE;

    monitor-enter v1

    .line 463587
    :try_start_0
    sget-object v0, LX/2nE;->d:LX/2nE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 463588
    if-eqz v2, :cond_0

    .line 463589
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 463590
    new-instance p0, LX/2nE;

    invoke-static {v0}, LX/1Bn;->a(LX/0QB;)LX/1Bn;

    move-result-object v3

    check-cast v3, LX/1Bn;

    invoke-static {v0}, LX/1Br;->a(LX/0QB;)LX/1Br;

    move-result-object v4

    check-cast v4, LX/1Br;

    invoke-direct {p0, v3, v4}, LX/2nE;-><init>(LX/1Bn;LX/1Br;)V

    .line 463591
    move-object v0, p0

    .line 463592
    sput-object v0, LX/2nE;->d:LX/2nE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463593
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 463594
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 463595
    :cond_1
    sget-object v0, LX/2nE;->d:LX/2nE;

    return-object v0

    .line 463596
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 463597
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
