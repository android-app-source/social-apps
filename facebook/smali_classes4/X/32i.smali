.class public LX/32i;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/0Tn;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/1E1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 490875
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 490876
    sput-object v0, LX/32i;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/1kp;->p:LX/0Tn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490877
    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1E1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 490871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490872
    iput-object p1, p0, LX/32i;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 490873
    iput-object p2, p0, LX/32i;->c:LX/1E1;

    .line 490874
    return-void
.end method

.method public static d(Ljava/lang/String;)LX/0Tn;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490878
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/32i;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    goto :goto_0
.end method

.method public static e(LX/32i;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 490869
    invoke-static {p1}, LX/32i;->d(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 490870
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/32i;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public static f(LX/32i;Ljava/lang/String;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 490856
    invoke-static {p1}, LX/32i;->d(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 490857
    if-nez v0, :cond_0

    .line 490858
    :goto_0
    return-void

    .line 490859
    :cond_0
    iget-object v1, p0, LX/32i;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public static h(LX/32i;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 490860
    if-nez p1, :cond_1

    .line 490861
    :cond_0
    :goto_0
    return v0

    .line 490862
    :cond_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 490863
    iget-object v0, p0, LX/32i;->c:LX/1E1;

    .line 490864
    iget-object v1, v0, LX/1E1;->a:LX/0ad;

    sget-short p0, LX/32h;->e:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 490865
    goto :goto_0

    .line 490866
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 490867
    const/4 v0, 0x0

    move v0, v0

    .line 490868
    goto :goto_0
.end method
