.class public LX/3IE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/3IA;

.field public final b:LX/3ID;

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:I

.field public final g:Landroid/view/VelocityTracker;

.field public h:F

.field public i:F

.field public j:F

.field public k:F


# direct methods
.method public constructor <init>(LX/3IA;LX/3ID;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 545749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 545750
    iput-boolean v0, p0, LX/3IE;->c:Z

    .line 545751
    iput-boolean v0, p0, LX/3IE;->d:Z

    .line 545752
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, LX/3IE;->g:Landroid/view/VelocityTracker;

    .line 545753
    iput-object p2, p0, LX/3IE;->b:LX/3ID;

    .line 545754
    iput-object p1, p0, LX/3IE;->a:LX/3IA;

    .line 545755
    return-void
.end method

.method public static b(LX/3IE;)V
    .locals 1

    .prologue
    .line 545756
    iget-object v0, p0, LX/3IE;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 545757
    return-void
.end method

.method public static b(LX/3IE;Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 545758
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 545759
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 545760
    iget-object v1, p0, LX/3IE;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v1, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 545761
    return-void
.end method
