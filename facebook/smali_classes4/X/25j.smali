.class public LX/25j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hV;


# instance fields
.field private final a:LX/25m;

.field private final b:LX/25q;

.field private final c:LX/25u;


# direct methods
.method public constructor <init>(LX/25m;LX/25q;LX/25u;)V
    .locals 0

    .prologue
    .line 369924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369925
    iput-object p1, p0, LX/25j;->a:LX/25m;

    .line 369926
    iput-object p2, p0, LX/25j;->b:LX/25q;

    .line 369927
    iput-object p3, p0, LX/25j;->c:LX/25u;

    .line 369928
    return-void
.end method


# virtual methods
.method public final a(ILX/0XJ;)Ljava/lang/String;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 369931
    iget-object v0, p0, LX/25j;->a:LX/25m;

    invoke-virtual {v0, p1, p2}, LX/25m;->a(ILX/0XJ;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(ILX/0XJ;)LX/12t;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/PluralsRes;
        .end annotation
    .end param

    .prologue
    .line 369930
    iget-object v0, p0, LX/25j;->b:LX/25q;

    invoke-virtual {v0, p1, p2}, LX/25q;->a(ILX/0XJ;)LX/12t;

    move-result-object v0

    return-object v0
.end method

.method public final c(ILX/0XJ;)[Ljava/lang/String;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ArrayRes;
        .end annotation
    .end param

    .prologue
    .line 369929
    iget-object v0, p0, LX/25j;->c:LX/25u;

    invoke-virtual {v0, p1, p2}, LX/25u;->a(ILX/0XJ;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
