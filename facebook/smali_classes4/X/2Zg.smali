.class public LX/2Zg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/02k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/02k;",
        "Lcom/facebook/manageddatastore/NetworkRequestCallback",
        "<TK;TV;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static a:I

.field public static b:I


# instance fields
.field public final c:LX/2Zb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Zb",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final d:LX/2Zh;

.field public final e:Landroid/content/Context;

.field public final f:LX/2Zl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Zl",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public g:LX/2Zm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Zm",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public h:I

.field public i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/Long;

.field public k:LX/0Yw;

.field public l:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/manageddatastore/MDSSerialExecutor;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 422888
    const v0, 0xea60

    sput v0, LX/2Zg;->a:I

    .line 422889
    const/4 v0, 0x0

    sput v0, LX/2Zg;->b:I

    return-void
.end method

.method public constructor <init>(LX/2Zb;LX/2Zh;Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Zb",
            "<TK;TV;>;",
            "LX/2Zh;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 422873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422874
    iput-object p1, p0, LX/2Zg;->c:LX/2Zb;

    .line 422875
    iput-object p2, p0, LX/2Zg;->d:LX/2Zh;

    .line 422876
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/2Zg;->e:Landroid/content/Context;

    .line 422877
    new-instance v0, LX/2Zl;

    invoke-direct {v0}, LX/2Zl;-><init>()V

    iput-object v0, p0, LX/2Zg;->f:LX/2Zl;

    .line 422878
    new-instance v0, LX/2Zm;

    iget-object v1, p0, LX/2Zg;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/2Zm;-><init>(LX/2Zb;Landroid/content/ContentResolver;)V

    iput-object v0, p0, LX/2Zg;->g:LX/2Zm;

    .line 422879
    sget-object v0, LX/2Zh;->SINGLE_REQUEST_BY_KEY:LX/2Zh;

    if-ne p2, v0, :cond_0

    .line 422880
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2Zg;->i:Ljava/util/Map;

    .line 422881
    :cond_0
    const-class v1, LX/2Zg;

    monitor-enter v1

    .line 422882
    :try_start_0
    sget v0, LX/2Zg;->b:I

    iput v0, p0, LX/2Zg;->h:I

    .line 422883
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422884
    const-class v0, LX/2Zg;

    invoke-static {v0, p0}, LX/2Zg;->a(Ljava/lang/Class;LX/02k;)V

    .line 422885
    new-instance v0, LX/0Yw;

    const/16 v1, 0x4000

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, LX/0Yw;-><init>(II)V

    iput-object v0, p0, LX/2Zg;->k:LX/0Yw;

    .line 422886
    return-void

    .line 422887
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 422870
    const-class v1, LX/2Zg;

    monitor-enter v1

    .line 422871
    :try_start_0
    sget v0, LX/2Zg;->b:I

    add-int/lit8 v0, v0, 0x1

    sput v0, LX/2Zg;->b:I

    .line 422872
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/2Zg;

    invoke-static {p0}, LX/2Zi;->a(LX/0QB;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p0

    check-cast p0, LX/03V;

    iput-object v1, p1, LX/2Zg;->l:Ljava/util/concurrent/ExecutorService;

    iput-object p0, p1, LX/2Zg;->m:LX/03V;

    return-void
.end method

.method private static b(LX/2Zg;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 422865
    iget-object v0, p0, LX/2Zg;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v1, LX/FCF;

    invoke-direct {v1, p0}, LX/FCF;-><init>(LX/2Zg;)V

    const v2, -0x70e30fdd

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 422866
    const v1, 0x39cfd379

    :try_start_0
    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 422867
    :goto_0
    return-void

    .line 422868
    :catch_0
    goto :goto_0

    .line 422869
    :catch_1
    goto :goto_0
.end method

.method private static c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 422864
    if-nez p0, :cond_0

    const-string v0, "(null)"

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 422748
    const-class v1, LX/2Zg;

    monitor-enter v1

    .line 422749
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 422750
    :try_start_1
    iget v0, p0, LX/2Zg;->h:I

    sget v2, LX/2Zg;->b:I

    if-eq v0, v2, :cond_0

    .line 422751
    iget-object v0, p0, LX/2Zg;->f:LX/2Zl;

    invoke-virtual {v0}, LX/2Zl;->a()V

    .line 422752
    sget v0, LX/2Zg;->b:I

    iput v0, p0, LX/2Zg;->h:I

    .line 422753
    :cond_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 422754
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    .line 422755
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 422756
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method private d(Ljava/lang/Object;)LX/FCG;
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LX/FCG",
            "<TV;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 422848
    sget-object v1, LX/FCI;->INVALID:LX/FCI;

    .line 422849
    monitor-enter p0

    .line 422850
    :try_start_0
    iget-object v2, p0, LX/2Zg;->f:LX/2Zl;

    invoke-virtual {v2, p1}, LX/2Zl;->a(Ljava/lang/Object;)LX/2zY;

    move-result-object v2

    .line 422851
    if-eqz v2, :cond_2

    .line 422852
    iget-object v1, v2, LX/2zY;->a:Ljava/lang/Object;

    .line 422853
    iget-wide v4, v2, LX/2zY;->b:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, v2, LX/2zY;->b:J

    iget-object v3, p0, LX/2Zg;->c:LX/2Zb;

    iget-object v2, v2, LX/2zY;->a:Ljava/lang/Object;

    invoke-interface {v3, p1, v2}, LX/2Zb;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    add-long/2addr v2, v6

    cmp-long v2, v4, v2

    if-gez v2, :cond_0

    .line 422854
    iget-object v0, p0, LX/2Zg;->k:LX/0Yw;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "successful read of key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, LX/2Zg;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from memory"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Yw;->a(Ljava/lang/String;)V

    .line 422855
    new-instance v0, LX/FCG;

    sget-object v2, LX/FCI;->VALID_AND_FRESH:LX/FCI;

    invoke-direct {v0, v2, v1}, LX/FCG;-><init>(LX/FCI;Ljava/lang/Object;)V

    monitor-exit p0

    .line 422856
    :goto_0
    return-object v0

    .line 422857
    :cond_0
    iget-object v2, p0, LX/2Zg;->c:LX/2Zb;

    invoke-interface {v2, p1, v1}, LX/2Zb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 422858
    iget-object v1, p0, LX/2Zg;->f:LX/2Zl;

    invoke-virtual {v1, p1}, LX/2Zl;->b(Ljava/lang/Object;)V

    .line 422859
    sget-object v1, LX/FCI;->INVALID:LX/FCI;

    move-object v2, v1

    move-object v1, v0

    .line 422860
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422861
    new-instance v0, LX/FCG;

    invoke-direct {v0, v2, v1}, LX/FCG;-><init>(LX/FCI;Ljava/lang/Object;)V

    goto :goto_0

    .line 422862
    :cond_1
    :try_start_1
    sget-object v0, LX/FCI;->VALID_AND_EXPIRED:LX/FCI;

    move-object v2, v0

    goto :goto_1

    .line 422863
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object v2, v1

    move-object v1, v0

    goto :goto_1
.end method

.method private e(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 422822
    sget-object v0, LX/2sy;->a:[I

    iget-object v3, p0, LX/2Zg;->d:LX/2Zh;

    invoke-virtual {v3}, LX/2Zh;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 422823
    :goto_0
    if-eqz v1, :cond_0

    .line 422824
    iget-object v1, p0, LX/2Zg;->c:LX/2Zb;

    iget-object v2, p0, LX/2Zg;->e:Landroid/content/Context;

    invoke-interface {v1, v2, p1, p0}, LX/2Zb;->a(Landroid/content/Context;Ljava/lang/Object;LX/2Zg;)V

    .line 422825
    :cond_0
    if-eqz v0, :cond_1

    .line 422826
    iget-object v0, p0, LX/2Zg;->c:LX/2Zb;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    .line 422827
    iget-object v1, p0, LX/2Zg;->m:LX/03V;

    iget-object v2, p0, LX/2Zg;->k:LX/0Yw;

    invoke-virtual {v2}, LX/0Yw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 422828
    iget-object v0, p0, LX/2Zg;->k:LX/0Yw;

    invoke-virtual {v0}, LX/0Yw;->c()V

    .line 422829
    :cond_1
    return-void

    :pswitch_0
    move v0, v1

    move v1, v2

    .line 422830
    goto :goto_0

    .line 422831
    :pswitch_1
    iget-object v3, p0, LX/2Zg;->i:Ljava/util/Map;

    monitor-enter v3

    .line 422832
    :try_start_0
    iget-object v0, p0, LX/2Zg;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 422833
    if-nez v0, :cond_3

    move v0, v1

    move v1, v2

    .line 422834
    :goto_1
    if-eqz v1, :cond_2

    .line 422835
    iget-object v2, p0, LX/2Zg;->i:Ljava/util/Map;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 422836
    :cond_2
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 422837
    :cond_3
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget v6, LX/2Zg;->a:I

    int-to-long v6, v6

    add-long/2addr v4, v6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_7

    .line 422838
    iget-object v1, p0, LX/2Zg;->k:LX/0Yw;

    const-string v4, "per-key lock had to be stolen (old lock timestamp: %d; key: %s)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    invoke-static {p1}, LX/2Zg;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {v1, v4, v5}, LX/0Yw;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v2

    move v1, v2

    goto :goto_1

    .line 422839
    :pswitch_2
    monitor-enter p0

    .line 422840
    :try_start_2
    iget-object v0, p0, LX/2Zg;->j:Ljava/lang/Long;

    if-nez v0, :cond_5

    .line 422841
    iget-object v0, p0, LX/2Zg;->k:LX/0Yw;

    const-string v3, "successfully acquired global lock (key: %s)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, LX/2Zg;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, LX/0Yw;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    move v1, v2

    .line 422842
    :goto_2
    if-eqz v1, :cond_4

    .line 422843
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, LX/2Zg;->j:Ljava/lang/Long;

    .line 422844
    iget-object v2, p0, LX/2Zg;->k:LX/0Yw;

    const-string v3, "mGlobalLock set to %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/2Zg;->j:Ljava/lang/Long;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, LX/0Yw;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 422845
    :cond_4
    monitor-exit p0

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 422846
    :cond_5
    :try_start_3
    iget-object v0, p0, LX/2Zg;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget v0, LX/2Zg;->a:I

    int-to-long v6, v0

    add-long/2addr v4, v6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-gez v0, :cond_6

    .line 422847
    iget-object v0, p0, LX/2Zg;->k:LX/0Yw;

    const-string v1, "global lock had to be stolen (old lock timestamp: %d; key: %s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/2Zg;->j:Ljava/lang/Long;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, LX/2Zg;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, LX/0Yw;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v0, v2

    move v1, v2

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_2

    :cond_7
    move v0, v1

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 422796
    iget-object v0, p0, LX/2Zg;->e:Landroid/content/Context;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 422797
    iget-object v0, p0, LX/2Zg;->k:LX/0Yw;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "request for key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, LX/2Zg;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Yw;->a(Ljava/lang/String;)V

    .line 422798
    invoke-direct {p0}, LX/2Zg;->c()V

    .line 422799
    invoke-direct {p0, p1}, LX/2Zg;->d(Ljava/lang/Object;)LX/FCG;

    move-result-object v0

    .line 422800
    iget-object v1, v0, LX/FCG;->a:LX/FCI;

    sget-object v2, LX/FCI;->VALID_AND_FRESH:LX/FCI;

    if-ne v1, v2, :cond_0

    .line 422801
    iget-object v0, v0, LX/FCG;->b:Ljava/lang/Object;

    .line 422802
    :goto_0
    return-object v0

    .line 422803
    :cond_0
    iget-object v3, p0, LX/2Zg;->g:LX/2Zm;

    invoke-virtual {v3, p1}, LX/2Zm;->a(Ljava/lang/Object;)LX/FCE;

    move-result-object v5

    .line 422804
    iget-object v4, v0, LX/FCG;->a:LX/FCI;

    .line 422805
    iget-object v3, v0, LX/FCG;->b:Ljava/lang/Object;

    .line 422806
    if-eqz v5, :cond_3

    .line 422807
    iget-wide v7, v5, LX/FCE;->b:J

    const-wide/16 v9, -0x1

    cmp-long v6, v7, v9

    if-eqz v6, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iget-wide v9, v5, LX/FCE;->b:J

    iget-object v6, p0, LX/2Zg;->c:LX/2Zb;

    iget-object v11, v5, LX/FCE;->a:Ljava/lang/Object;

    invoke-interface {v6, p1, v11}, LX/2Zb;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    int-to-long v11, v6

    add-long/2addr v9, v11

    cmp-long v6, v7, v9

    if-gez v6, :cond_2

    .line 422808
    iget-object v3, p0, LX/2Zg;->f:LX/2Zl;

    iget-object v4, v5, LX/FCE;->a:Ljava/lang/Object;

    iget-wide v7, v5, LX/FCE;->b:J

    invoke-virtual {v3, p1, v4, v7, v8}, LX/2Zl;->a(Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 422809
    iget-object v3, p0, LX/2Zg;->k:LX/0Yw;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "successful read of key "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " from disk"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Yw;->a(Ljava/lang/String;)V

    .line 422810
    new-instance v3, LX/FCG;

    sget-object v4, LX/FCI;->VALID_AND_FRESH:LX/FCI;

    iget-object v5, v5, LX/FCE;->a:Ljava/lang/Object;

    invoke-direct {v3, v4, v5}, LX/FCG;-><init>(LX/FCI;Ljava/lang/Object;)V

    .line 422811
    :goto_1
    move-object v0, v3

    .line 422812
    iget-object v1, v0, LX/FCG;->a:LX/FCI;

    sget-object v2, LX/FCI;->VALID_AND_FRESH:LX/FCI;

    if-ne v1, v2, :cond_1

    .line 422813
    iget-object v0, v0, LX/FCG;->b:Ljava/lang/Object;

    goto :goto_0

    .line 422814
    :cond_1
    invoke-direct {p0, p1}, LX/2Zg;->e(Ljava/lang/Object;)V

    .line 422815
    iget-object v0, v0, LX/FCG;->b:Ljava/lang/Object;

    goto :goto_0

    .line 422816
    :cond_2
    iget-object v6, p0, LX/2Zg;->c:LX/2Zb;

    iget-object v7, v5, LX/FCE;->a:Ljava/lang/Object;

    invoke-interface {v6, p1, v7}, LX/2Zb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 422817
    iget-object v5, p0, LX/2Zg;->g:LX/2Zm;

    const/4 v8, 0x0

    .line 422818
    iget-object v6, v5, LX/2Zm;->c:Landroid/content/ContentResolver;

    invoke-static {v5, p1}, LX/2Zm;->d(LX/2Zm;Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7, v8, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 422819
    :cond_3
    :goto_2
    new-instance v5, LX/FCG;

    invoke-direct {v5, v4, v3}, LX/FCG;-><init>(LX/FCI;Ljava/lang/Object;)V

    move-object v3, v5

    goto :goto_1

    .line 422820
    :cond_4
    sget-object v4, LX/FCI;->VALID_AND_EXPIRED:LX/FCI;

    .line 422821
    iget-object v3, v5, LX/FCE;->a:Ljava/lang/Object;

    goto :goto_2
.end method

.method public final declared-synchronized a(LX/FCH;)V
    .locals 4

    .prologue
    .line 422787
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/FCH;->CLEAR_DISK:LX/FCH;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/FCH;->CLEAR_ALL:LX/FCH;

    if-ne p1, v0, :cond_1

    .line 422788
    :cond_0
    invoke-static {p0}, LX/2Zg;->b(LX/2Zg;)V

    .line 422789
    iget-object v0, p0, LX/2Zg;->g:LX/2Zm;

    const/4 v3, 0x0

    .line 422790
    sget-object v1, LX/2Ze;->d:Landroid/net/Uri;

    invoke-static {v0}, LX/2Zm;->b(LX/2Zm;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 422791
    iget-object v2, v0, LX/2Zm;->c:Landroid/content/ContentResolver;

    invoke-virtual {v2, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 422792
    :cond_1
    sget-object v0, LX/FCH;->CLEAR_MEMORY:LX/FCH;

    if-eq p1, v0, :cond_2

    sget-object v0, LX/FCH;->CLEAR_ALL:LX/FCH;

    if-ne p1, v0, :cond_3

    .line 422793
    :cond_2
    iget-object v0, p0, LX/2Zg;->f:LX/2Zl;

    invoke-virtual {v0}, LX/2Zl;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422794
    :cond_3
    monitor-exit p0

    return-void

    .line 422795
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/FCH;J)V
    .locals 7

    .prologue
    .line 422778
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/FCH;->CLEAR_DISK:LX/FCH;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/FCH;->CLEAR_ALL:LX/FCH;

    if-ne p1, v0, :cond_1

    .line 422779
    :cond_0
    invoke-static {p0}, LX/2Zg;->b(LX/2Zg;)V

    .line 422780
    iget-object v0, p0, LX/2Zg;->g:LX/2Zm;

    const/4 v6, 0x0

    .line 422781
    sget-object v2, LX/2Ze;->c:Landroid/net/Uri;

    invoke-static {v0}, LX/2Zm;->b(LX/2Zm;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p2

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 422782
    iget-object v3, v0, LX/2Zm;->c:Landroid/content/ContentResolver;

    invoke-virtual {v3, v2, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 422783
    :cond_1
    sget-object v0, LX/FCH;->CLEAR_MEMORY:LX/FCH;

    if-eq p1, v0, :cond_2

    sget-object v0, LX/FCH;->CLEAR_ALL:LX/FCH;

    if-ne p1, v0, :cond_3

    .line 422784
    :cond_2
    iget-object v0, p0, LX/2Zg;->f:LX/2Zl;

    invoke-virtual {v0, p2, p3}, LX/2Zl;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422785
    :cond_3
    monitor-exit p0

    return-void

    .line 422786
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Landroid/content/Context;ZLjava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 7
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "ZTK;",
            "Ljava/lang/String;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 422758
    iget-object v0, p0, LX/2Zg;->k:LX/0Yw;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "got callback at MDS for key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, LX/2Zg;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Yw;->a(Ljava/lang/String;)V

    .line 422759
    sget-object v0, LX/2sy;->a:[I

    iget-object v1, p0, LX/2Zg;->d:LX/2Zh;

    invoke-virtual {v1}, LX/2Zh;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 422760
    :goto_0
    if-nez p2, :cond_1

    .line 422761
    :cond_0
    :goto_1
    return-void

    .line 422762
    :pswitch_0
    iget-object v1, p0, LX/2Zg;->i:Ljava/util/Map;

    monitor-enter v1

    .line 422763
    :try_start_0
    iget-object v0, p0, LX/2Zg;->i:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 422764
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 422765
    :pswitch_1
    monitor-enter p0

    .line 422766
    :try_start_1
    iget-object v0, p0, LX/2Zg;->k:LX/0Yw;

    const-string v1, "released global lock (key: %s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p3}, LX/2Zg;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, LX/0Yw;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 422767
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Zg;->j:Ljava/lang/Long;

    .line 422768
    monitor-exit p0

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 422769
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 422770
    iget-object v0, p0, LX/2Zg;->c:LX/2Zb;

    invoke-interface {v0, p3, p5}, LX/2Zb;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 422771
    if-lez v0, :cond_2

    .line 422772
    iget-object v1, p0, LX/2Zg;->f:LX/2Zl;

    invoke-virtual {v1, p3, p5, v4, v5}, LX/2Zl;->a(Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 422773
    :cond_2
    iget-object v1, p0, LX/2Zg;->c:LX/2Zb;

    invoke-interface {v1, p3, p5}, LX/2Zb;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_0

    .line 422774
    iget-object v1, p0, LX/2Zg;->k:LX/0Yw;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Writing key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to disk"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Yw;->a(Ljava/lang/String;)V

    .line 422775
    if-lez v0, :cond_3

    .line 422776
    iget-object v6, p0, LX/2Zg;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/manageddatastore/ManagedDataStore$1;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/manageddatastore/ManagedDataStore$1;-><init>(LX/2Zg;Ljava/lang/Object;Ljava/lang/String;J)V

    const v1, 0x4809bec1

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_1

    .line 422777
    :cond_3
    iget-object v0, p0, LX/2Zg;->g:LX/2Zm;

    invoke-virtual {v0, p3, p4, v4, v5}, LX/2Zm;->a(Ljava/lang/Object;Ljava/lang/String;J)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 422757
    iget-object v0, p0, LX/2Zg;->e:Landroid/content/Context;

    return-object v0
.end method
