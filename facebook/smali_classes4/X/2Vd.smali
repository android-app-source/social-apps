.class public final LX/2Vd;
.super LX/2An;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _index:I

.field public final _owner:LX/2Au;

.field public final _type:Ljava/lang/reflect/Type;


# direct methods
.method public constructor <init>(LX/2Au;Ljava/lang/reflect/Type;LX/0lP;I)V
    .locals 0

    .prologue
    .line 417942
    invoke-direct {p0, p3}, LX/2An;-><init>(LX/0lP;)V

    .line 417943
    iput-object p1, p0, LX/2Vd;->_owner:LX/2Au;

    .line 417944
    iput-object p2, p0, LX/2Vd;->_type:Ljava/lang/reflect/Type;

    .line 417945
    iput p4, p0, LX/2Vd;->_index:I

    .line 417946
    return-void
.end method


# virtual methods
.method public final a(LX/0lP;)LX/2Vd;
    .locals 2

    .prologue
    .line 417950
    iget-object v0, p0, LX/2An;->b:LX/0lP;

    if-ne p1, v0, :cond_0

    .line 417951
    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, LX/2Vd;->_owner:LX/2Au;

    iget v1, p0, LX/2Vd;->_index:I

    invoke-virtual {v0, v1, p1}, LX/2Au;->a(ILX/0lP;)LX/2Vd;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Ljava/lang/annotation/Annotation;",
            ">(",
            "Ljava/lang/Class",
            "<TA;>;)TA;"
        }
    .end annotation

    .prologue
    .line 417949
    iget-object v0, p0, LX/2An;->b:LX/0lP;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2An;->b:LX/0lP;

    invoke-virtual {v0, p1}, LX/0lP;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/reflect/AnnotatedElement;
    .locals 1

    .prologue
    .line 417948
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 417947
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot call setValue() on constructor parameter of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/2An;->i()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 417941
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot call getValue() on constructor parameter of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/2An;->i()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 417940
    const-string v0, ""

    return-object v0
.end method

.method public final c()Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 417952
    iget-object v0, p0, LX/2Vd;->_type:Ljava/lang/reflect/Type;

    return-object v0
.end method

.method public final d()Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 417933
    iget-object v0, p0, LX/2Vd;->_type:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 417934
    iget-object v0, p0, LX/2Vd;->_type:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/Class;

    .line 417935
    :goto_0
    return-object v0

    .line 417936
    :cond_0
    sget-object v0, LX/0li;->a:LX/0li;

    move-object v0, v0

    .line 417937
    iget-object v1, p0, LX/2Vd;->_type:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v1}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v0

    .line 417938
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 417939
    goto :goto_0
.end method

.method public final f()Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 417932
    iget-object v0, p0, LX/2Vd;->_type:Ljava/lang/reflect/Type;

    return-object v0
.end method

.method public final g()LX/2Au;
    .locals 1

    .prologue
    .line 417925
    iget-object v0, p0, LX/2Vd;->_owner:LX/2Au;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 417931
    iget v0, p0, LX/2Vd;->_index:I

    return v0
.end method

.method public final i()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 417930
    iget-object v0, p0, LX/2Vd;->_owner:LX/2Au;

    invoke-virtual {v0}, LX/2An;->i()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/reflect/Member;
    .locals 1

    .prologue
    .line 417929
    iget-object v0, p0, LX/2Vd;->_owner:LX/2Au;

    invoke-virtual {v0}, LX/2An;->j()Ljava/lang/reflect/Member;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 417926
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[parameter #"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 417927
    iget v1, p0, LX/2Vd;->_index:I

    move v1, v1

    .line 417928
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", annotations: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2An;->b:LX/0lP;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
