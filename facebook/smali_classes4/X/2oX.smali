.class public LX/2oX;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:I

.field public c:Landroid/view/View;

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/2p5;",
            "Ljava/util/PriorityQueue",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "LX/7Ka;",
            ">;"
        }
    .end annotation
.end field

.field public f:[I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field private l:I

.field private m:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 466958
    new-instance v0, LX/2oY;

    invoke-direct {v0}, LX/2oY;-><init>()V

    sput-object v0, LX/2oX;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 467136
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/2oX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 467137
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 467134
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/2oX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 467135
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 467125
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 467126
    const/4 v0, 0x0

    iput v0, p0, LX/2oX;->b:I

    .line 467127
    iput v1, p0, LX/2oX;->l:I

    .line 467128
    iput v1, p0, LX/2oX;->m:I

    .line 467129
    if-eqz p2, :cond_0

    .line 467130
    sget-object v0, LX/03r;->AnchorLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 467131
    const/16 v1, 0x0

    const/4 p3, 0x0

    invoke-virtual {v0, v1, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, LX/2oX;->b:I

    .line 467132
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 467133
    :cond_0
    return-void
.end method

.method public static a(LX/2oX;LX/2p5;I)I
    .locals 3

    .prologue
    .line 467121
    sget-object v0, LX/7KY;->a:[I

    invoke-virtual {p1}, LX/2p5;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 467122
    iget-object v0, p0, LX/2oX;->f:[I

    sget-object v1, LX/2p5;->INNER_END:LX/2p5;

    invoke-virtual {v1}, LX/2p5;->ordinal()I

    move-result v1

    aget v0, v0, v1

    iget-object v1, p0, LX/2oX;->f:[I

    sget-object v2, LX/2p5;->INNER_START:LX/2p5;

    invoke-virtual {v2}, LX/2p5;->ordinal()I

    move-result v2

    aget v1, v1, v2

    sub-int p2, v0, v1

    :goto_0
    :pswitch_0
    return p2

    .line 467123
    :pswitch_1
    iget-object v0, p0, LX/2oX;->f:[I

    sget-object v1, LX/2p5;->BOTTOM:LX/2p5;

    invoke-virtual {v1}, LX/2p5;->ordinal()I

    move-result v1

    aget v0, v0, v1

    iget-object v1, p0, LX/2oX;->f:[I

    sget-object v2, LX/2p5;->TOP:LX/2p5;

    invoke-virtual {v2}, LX/2p5;->ordinal()I

    move-result v2

    aget v1, v1, v2

    sub-int p2, v0, v1

    goto :goto_0

    .line 467124
    :pswitch_2
    iget-object v0, p0, LX/2oX;->f:[I

    sget-object v1, LX/2p5;->INNER_BOTTOM:LX/2p5;

    invoke-virtual {v1}, LX/2p5;->ordinal()I

    move-result v1

    aget v0, v0, v1

    iget-object v1, p0, LX/2oX;->f:[I

    sget-object v2, LX/2p5;->INNER_TOP:LX/2p5;

    invoke-virtual {v2}, LX/2p5;->ordinal()I

    move-result v2

    aget v1, v1, v2

    sub-int p2, v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/2oX;LX/2p5;LX/7Ka;)V
    .locals 13

    .prologue
    .line 467077
    const/4 v2, 0x0

    .line 467078
    invoke-static {}, LX/2p6;->values()[LX/2p6;

    move-result-object v5

    array-length v6, v5

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_2

    aget-object v1, v5, v4

    .line 467079
    sget-object v0, LX/7KZ;->b:[I

    invoke-virtual {v1}, LX/2p6;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    move v0, v2

    .line 467080
    :goto_1
    iget-object v3, p2, LX/7Ka;->d:[Ljava/util/LinkedList;

    invoke-virtual {v1}, LX/2p6;->ordinal()I

    move-result v1

    aget-object v1, v3, v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v0

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 467081
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/2p4;

    .line 467082
    iget-object v8, p2, LX/7Ka;->a:LX/2p5;

    invoke-virtual {v8}, LX/2p5;->isHorizontal()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 467083
    new-instance v8, Landroid/graphics/Point;

    iget v9, p2, LX/7Ka;->l:I

    add-int/2addr v9, v3

    iget v10, p2, LX/7Ka;->i:I

    iget v11, p2, LX/7Ka;->h:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    sub-int/2addr v11, v12

    div-int/lit8 v11, v11, 0x2

    add-int/2addr v10, v11

    iget v11, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v10, v11

    invoke-direct {v8, v9, v10}, Landroid/graphics/Point;-><init>(II)V

    iput-object v8, v1, LX/2p4;->e:Landroid/graphics/Point;

    .line 467084
    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v8

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    move v3, v0

    goto :goto_2

    .line 467085
    :pswitch_0
    iget v0, p2, LX/7Ka;->b:I

    iget-object v3, p2, LX/7Ka;->e:[I

    invoke-virtual {v1}, LX/2p6;->ordinal()I

    move-result v7

    aget v3, v3, v7

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 467086
    :pswitch_1
    iget v0, p2, LX/7Ka;->b:I

    iget-object v3, p2, LX/7Ka;->e:[I

    invoke-virtual {v1}, LX/2p6;->ordinal()I

    move-result v7

    aget v3, v3, v7

    sub-int/2addr v0, v3

    .line 467087
    goto :goto_1

    .line 467088
    :cond_0
    new-instance v8, Landroid/graphics/Point;

    iget v9, p2, LX/7Ka;->l:I

    iget v10, p2, LX/7Ka;->h:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    sub-int/2addr v10, v11

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    iget v10, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v9, v10

    iget v10, p2, LX/7Ka;->i:I

    add-int/2addr v10, v3

    invoke-direct {v8, v9, v10}, Landroid/graphics/Point;-><init>(II)V

    iput-object v8, v1, LX/2p4;->e:Landroid/graphics/Point;

    .line 467089
    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v8

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    move v3, v0

    .line 467090
    goto :goto_2

    .line 467091
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    .line 467092
    :cond_2
    sget-object v0, LX/7KY;->a:[I

    invoke-virtual {p1}, LX/2p5;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 467093
    iget v0, p0, LX/2oX;->g:I

    .line 467094
    iget v1, p2, LX/7Ka;->h:I

    move v1, v1

    .line 467095
    add-int/2addr v0, v1

    iput v0, p0, LX/2oX;->g:I

    .line 467096
    :goto_3
    return-void

    .line 467097
    :pswitch_2
    iget-object v0, p0, LX/2oX;->f:[I

    sget-object v1, LX/2p5;->TOP:LX/2p5;

    invoke-virtual {v1}, LX/2p5;->ordinal()I

    move-result v1

    .line 467098
    iget v2, p2, LX/7Ka;->k:I

    move v2, v2

    .line 467099
    aput v2, v0, v1

    .line 467100
    :pswitch_3
    iget-object v0, p0, LX/2oX;->f:[I

    sget-object v1, LX/2p5;->INNER_TOP:LX/2p5;

    invoke-virtual {v1}, LX/2p5;->ordinal()I

    move-result v1

    .line 467101
    iget v2, p2, LX/7Ka;->k:I

    move v2, v2

    .line 467102
    iget v3, p0, LX/2oX;->h:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    aput v2, v0, v1

    goto :goto_3

    .line 467103
    :pswitch_4
    iget-object v0, p0, LX/2oX;->f:[I

    sget-object v1, LX/2p5;->BOTTOM:LX/2p5;

    invoke-virtual {v1}, LX/2p5;->ordinal()I

    move-result v1

    .line 467104
    iget v2, p2, LX/7Ka;->i:I

    move v2, v2

    .line 467105
    aput v2, v0, v1

    .line 467106
    :pswitch_5
    iget-object v0, p0, LX/2oX;->f:[I

    sget-object v1, LX/2p5;->INNER_BOTTOM:LX/2p5;

    invoke-virtual {v1}, LX/2p5;->ordinal()I

    move-result v1

    .line 467107
    iget v2, p2, LX/7Ka;->i:I

    move v2, v2

    .line 467108
    iget v3, p0, LX/2oX;->i:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    aput v2, v0, v1

    goto :goto_3

    .line 467109
    :pswitch_6
    iget-object v0, p0, LX/2oX;->f:[I

    sget-object v1, LX/2p5;->START:LX/2p5;

    invoke-virtual {v1}, LX/2p5;->ordinal()I

    move-result v1

    .line 467110
    iget v2, p2, LX/7Ka;->j:I

    move v2, v2

    .line 467111
    aput v2, v0, v1

    .line 467112
    :pswitch_7
    iget-object v0, p0, LX/2oX;->f:[I

    sget-object v1, LX/2p5;->INNER_START:LX/2p5;

    invoke-virtual {v1}, LX/2p5;->ordinal()I

    move-result v1

    .line 467113
    iget v2, p2, LX/7Ka;->j:I

    move v2, v2

    .line 467114
    iget v3, p0, LX/2oX;->j:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    aput v2, v0, v1

    goto :goto_3

    .line 467115
    :pswitch_8
    iget-object v0, p0, LX/2oX;->f:[I

    sget-object v1, LX/2p5;->END:LX/2p5;

    invoke-virtual {v1}, LX/2p5;->ordinal()I

    move-result v1

    .line 467116
    iget v2, p2, LX/7Ka;->l:I

    move v2, v2

    .line 467117
    aput v2, v0, v1

    .line 467118
    :pswitch_9
    iget-object v0, p0, LX/2oX;->f:[I

    sget-object v1, LX/2p5;->INNER_END:LX/2p5;

    invoke-virtual {v1}, LX/2p5;->ordinal()I

    move-result v1

    .line 467119
    iget v2, p2, LX/7Ka;->l:I

    move v2, v2

    .line 467120
    iget v3, p0, LX/2oX;->k:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    aput v2, v0, v1

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_4
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_9
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 467076
    instance-of v0, p1, LX/2p4;

    return v0
.end method

.method public final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 467075
    new-instance v0, LX/2p4;

    invoke-direct {v0, v1, v1}, LX/2p4;-><init>(II)V

    return-object v0
.end method

.method public final bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 467138
    invoke-virtual {p0, p1}, LX/2oX;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 2

    .prologue
    .line 467074
    new-instance v0, LX/2p4;

    invoke-virtual {p0}, LX/2oX;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/2p4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final invalidate()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 467070
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->invalidate()V

    .line 467071
    iput v0, p0, LX/2oX;->m:I

    .line 467072
    iput v0, p0, LX/2oX;->l:I

    .line 467073
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 467067
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 467068
    invoke-virtual {p0}, LX/2oX;->requestLayout()V

    .line 467069
    return-void
.end method

.method public onFinishInflate()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x6232a60e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 467061
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onFinishInflate()V

    .line 467062
    iget v1, p0, LX/2oX;->b:I

    if-eqz v1, :cond_0

    .line 467063
    iget v1, p0, LX/2oX;->b:I

    invoke-virtual {p0, v1}, LX/2oX;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/2oX;->c:Landroid/view/View;

    .line 467064
    iget-object v1, p0, LX/2oX;->c:Landroid/view/View;

    if-nez v1, :cond_0

    .line 467065
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid inner view resourceId specified in layout."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x2d

    const v3, 0x74ab2371

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1

    .line 467066
    :cond_0
    const v1, 0x39b0fc75

    invoke-static {v1, v0}, LX/02F;->g(II)V

    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 467045
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomRelativeLayout;->onLayout(ZIIII)V

    move v1, v2

    .line 467046
    :goto_0
    invoke-virtual {p0}, LX/2oX;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 467047
    invoke-virtual {p0, v1}, LX/2oX;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 467048
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v3, 0x8

    if-eq v0, v3, :cond_1

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2oX;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 467049
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/2p4;

    .line 467050
    iget-object v3, v0, LX/2p4;->a:LX/2p5;

    if-eqz v3, :cond_1

    iget-object v3, v0, LX/2p4;->a:LX/2p5;

    sget-object v4, LX/2p5;->NONE:LX/2p5;

    if-eq v3, v4, :cond_1

    .line 467051
    iget-object v3, v0, LX/2p4;->e:Landroid/graphics/Point;

    if-eqz v3, :cond_4

    .line 467052
    iget-object v3, v0, LX/2p4;->e:Landroid/graphics/Point;

    iget v4, v3, Landroid/graphics/Point;->x:I

    .line 467053
    iget-object v3, v0, LX/2p4;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    .line 467054
    :goto_1
    invoke-virtual {p0}, LX/2oX;->getPaddingLeft()I

    move-result v6

    add-int/2addr v4, v6

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v4, v6

    .line 467055
    invoke-virtual {p0}, LX/2oX;->getPaddingTop()I

    move-result v6

    add-int/2addr v3, v6

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v3, v6

    .line 467056
    iget-object v6, v0, LX/2p4;->a:LX/2p5;

    sget-object v7, LX/2p5;->CENTER:LX/2p5;

    if-eq v6, v7, :cond_0

    iget-object v0, v0, LX/2p4;->a:LX/2p5;

    sget-object v6, LX/2p5;->INNER_CENTER:LX/2p5;

    if-ne v0, v6, :cond_3

    .line 467057
    :cond_0
    iget v0, p0, LX/2oX;->g:I

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v3, v0

    .line 467058
    :goto_2
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v4

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v5, v4, v0, v3, v6}, Landroid/view/View;->layout(IIII)V

    .line 467059
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 467060
    :cond_2
    return-void

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    move v3, v2

    move v4, v2

    goto :goto_1
.end method

.method public onMeasure(II)V
    .locals 9

    .prologue
    .line 466970
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;->onMeasure(II)V

    .line 466971
    invoke-virtual {p0}, LX/2oX;->getChildCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 466972
    :goto_0
    return-void

    .line 466973
    :cond_0
    invoke-virtual {p0}, LX/2oX;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, LX/2oX;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, LX/2oX;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 466974
    invoke-virtual {p0}, LX/2oX;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, LX/2oX;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, LX/2oX;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    .line 466975
    iget v2, p0, LX/2oX;->l:I

    if-ne v0, v2, :cond_1

    iget v2, p0, LX/2oX;->m:I

    if-eq v1, v2, :cond_c

    .line 466976
    :cond_1
    iget-object v2, p0, LX/2oX;->d:Ljava/util/Map;

    if-nez v2, :cond_3

    .line 466977
    new-instance v2, Ljava/util/EnumMap;

    const-class v3, LX/2p5;

    invoke-direct {v2, v3}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v2, p0, LX/2oX;->d:Ljava/util/Map;

    .line 466978
    invoke-static {}, LX/2p5;->values()[LX/2p5;

    move-result-object v2

    array-length v3, v2

    .line 466979
    new-array v2, v3, [Ljava/util/ArrayList;

    iput-object v2, p0, LX/2oX;->e:[Ljava/util/ArrayList;

    .line 466980
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    .line 466981
    iget-object p1, p0, LX/2oX;->e:[Ljava/util/ArrayList;

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    aput-object p2, p1, v2

    .line 466982
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 466983
    :cond_2
    new-array v2, v3, [I

    iput-object v2, p0, LX/2oX;->f:[I

    .line 466984
    :cond_3
    const/4 v3, 0x0

    .line 466985
    iget-object v2, p0, LX/2oX;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/PriorityQueue;

    .line 466986
    invoke-virtual {v2}, Ljava/util/PriorityQueue;->clear()V

    goto :goto_2

    :cond_4
    move v4, v3

    .line 466987
    :goto_3
    invoke-virtual {p0}, LX/2oX;->getChildCount()I

    move-result v2

    if-ge v4, v2, :cond_7

    .line 466988
    invoke-virtual {p0, v4}, LX/2oX;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 466989
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v5, 0x8

    if-eq v2, v5, :cond_6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/2oX;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 466990
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, LX/2p4;

    .line 466991
    iget-object v5, v2, LX/2p4;->a:LX/2p5;

    if-eqz v5, :cond_6

    iget-object v5, v2, LX/2p4;->a:LX/2p5;

    sget-object p1, LX/2p5;->NONE:LX/2p5;

    if-eq v5, p1, :cond_6

    .line 466992
    add-int/lit8 v5, v3, 0x1

    iput v3, v2, LX/2p4;->d:I

    .line 466993
    iget-object v3, p0, LX/2oX;->d:Ljava/util/Map;

    iget-object p1, v2, LX/2p4;->a:LX/2p5;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/PriorityQueue;

    .line 466994
    if-nez v3, :cond_5

    .line 466995
    new-instance v3, Ljava/util/PriorityQueue;

    const/4 p1, 0x4

    sget-object p2, LX/2oX;->a:Ljava/util/Comparator;

    invoke-direct {v3, p1, p2}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    .line 466996
    iget-object p1, p0, LX/2oX;->d:Ljava/util/Map;

    iget-object v2, v2, LX/2p4;->a:LX/2p5;

    invoke-interface {p1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 466997
    :cond_5
    invoke-virtual {v3, v6}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    move v3, v5

    .line 466998
    :cond_6
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 466999
    :cond_7
    const/4 v3, 0x0

    .line 467000
    iget-object v4, p0, LX/2oX;->e:[Ljava/util/ArrayList;

    array-length v5, v4

    move v2, v3

    :goto_4
    if-ge v2, v5, :cond_8

    aget-object v6, v4, v2

    .line 467001
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 467002
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 467003
    :cond_8
    iget-object v2, p0, LX/2oX;->c:Landroid/view/View;

    if-eqz v2, :cond_e

    .line 467004
    iget-object v2, p0, LX/2oX;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, LX/2p4;

    .line 467005
    invoke-virtual {v2}, LX/2p4;->getRules()[I

    move-result-object v2

    const/16 v4, 0xa

    aget v2, v2, v4

    const/4 v4, -0x1

    if-ne v2, v4, :cond_d

    .line 467006
    iput v3, p0, LX/2oX;->h:I

    .line 467007
    :goto_5
    iget v2, p0, LX/2oX;->h:I

    iget-object v4, p0, LX/2oX;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v2, v4

    iput v2, p0, LX/2oX;->i:I

    .line 467008
    :goto_6
    iget-object v2, p0, LX/2oX;->c:Landroid/view/View;

    if-nez v2, :cond_f

    move v2, v3

    :goto_7
    iput v2, p0, LX/2oX;->j:I

    .line 467009
    iget-object v2, p0, LX/2oX;->c:Landroid/view/View;

    if-nez v2, :cond_10

    move v2, v0

    :goto_8
    iput v2, p0, LX/2oX;->k:I

    .line 467010
    iget-object v2, p0, LX/2oX;->f:[I

    sget-object v4, LX/2p5;->TOP:LX/2p5;

    invoke-virtual {v4}, LX/2p5;->ordinal()I

    move-result v4

    aput v3, v2, v4

    .line 467011
    iget-object v2, p0, LX/2oX;->f:[I

    sget-object v4, LX/2p5;->BOTTOM:LX/2p5;

    invoke-virtual {v4}, LX/2p5;->ordinal()I

    move-result v4

    aput v1, v2, v4

    .line 467012
    iget-object v2, p0, LX/2oX;->f:[I

    sget-object v4, LX/2p5;->START:LX/2p5;

    invoke-virtual {v4}, LX/2p5;->ordinal()I

    move-result v4

    aput v3, v2, v4

    .line 467013
    iget-object v2, p0, LX/2oX;->f:[I

    sget-object v4, LX/2p5;->END:LX/2p5;

    invoke-virtual {v4}, LX/2p5;->ordinal()I

    move-result v4

    aput v0, v2, v4

    .line 467014
    iget-object v2, p0, LX/2oX;->f:[I

    sget-object v4, LX/2p5;->INNER_TOP:LX/2p5;

    invoke-virtual {v4}, LX/2p5;->ordinal()I

    move-result v4

    iget v5, p0, LX/2oX;->h:I

    aput v5, v2, v4

    .line 467015
    iget-object v2, p0, LX/2oX;->f:[I

    sget-object v4, LX/2p5;->INNER_BOTTOM:LX/2p5;

    invoke-virtual {v4}, LX/2p5;->ordinal()I

    move-result v4

    iget v5, p0, LX/2oX;->i:I

    aput v5, v2, v4

    .line 467016
    iget-object v2, p0, LX/2oX;->f:[I

    sget-object v4, LX/2p5;->INNER_START:LX/2p5;

    invoke-virtual {v4}, LX/2p5;->ordinal()I

    move-result v4

    iget v5, p0, LX/2oX;->j:I

    aput v5, v2, v4

    .line 467017
    iget-object v2, p0, LX/2oX;->f:[I

    sget-object v4, LX/2p5;->INNER_END:LX/2p5;

    invoke-virtual {v4}, LX/2p5;->ordinal()I

    move-result v4

    iget v5, p0, LX/2oX;->k:I

    aput v5, v2, v4

    .line 467018
    iget-object v2, p0, LX/2oX;->f:[I

    sget-object v4, LX/2p5;->INNER_CENTER:LX/2p5;

    invoke-virtual {v4}, LX/2p5;->ordinal()I

    move-result v4

    iget v5, p0, LX/2oX;->h:I

    iget v6, p0, LX/2oX;->i:I

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    aput v5, v2, v4

    .line 467019
    iget-object v2, p0, LX/2oX;->f:[I

    sget-object v4, LX/2p5;->CENTER:LX/2p5;

    invoke-virtual {v4}, LX/2p5;->ordinal()I

    move-result v4

    div-int/lit8 v5, v1, 0x2

    aput v5, v2, v4

    .line 467020
    iput v3, p0, LX/2oX;->g:I

    .line 467021
    invoke-static {}, LX/2p5;->values()[LX/2p5;

    move-result-object v6

    array-length v7, v6

    const/4 v2, 0x0

    move v5, v2

    :goto_9
    if-ge v5, v7, :cond_c

    aget-object v8, v6, v5

    .line 467022
    iget-object v2, p0, LX/2oX;->d:Ljava/util/Map;

    invoke-interface {v2, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/PriorityQueue;

    .line 467023
    if-eqz v2, :cond_b

    .line 467024
    invoke-virtual {v8}, LX/2p5;->isHorizontal()Z

    move-result v3

    if-eqz v3, :cond_9

    new-instance v3, Landroid/graphics/Point;

    div-int/lit8 v4, v0, 0x2

    iget-object p1, p0, LX/2oX;->f:[I

    invoke-virtual {v8}, LX/2p5;->ordinal()I

    move-result p2

    aget p1, p1, p2

    invoke-direct {v3, v4, p1}, Landroid/graphics/Point;-><init>(II)V

    .line 467025
    :goto_a
    invoke-static {p0, v8, v0}, LX/2oX;->a(LX/2oX;LX/2p5;I)I

    move-result p1

    .line 467026
    new-instance v4, LX/7Ka;

    invoke-direct {v4, v8, p1, v3}, LX/7Ka;-><init>(LX/2p5;ILandroid/graphics/Point;)V

    .line 467027
    iget-object v3, p0, LX/2oX;->e:[Ljava/util/ArrayList;

    invoke-virtual {v8}, LX/2p5;->ordinal()I

    move-result p1

    aget-object v3, v3, p1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467028
    :goto_b
    invoke-virtual {v2}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_a

    .line 467029
    invoke-virtual {v2}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 467030
    invoke-virtual {v4, v3}, LX/7Ka;->a(Landroid/view/View;)LX/7Ka;

    move-result-object v3

    .line 467031
    if-eqz v3, :cond_11

    .line 467032
    invoke-static {p0, v8, v4}, LX/2oX;->a(LX/2oX;LX/2p5;LX/7Ka;)V

    .line 467033
    iget-object v4, p0, LX/2oX;->e:[Ljava/util/ArrayList;

    invoke-virtual {v8}, LX/2p5;->ordinal()I

    move-result p1

    aget-object v4, v4, p1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_c
    move-object v4, v3

    .line 467034
    goto :goto_b

    .line 467035
    :cond_9
    new-instance v3, Landroid/graphics/Point;

    iget-object v4, p0, LX/2oX;->f:[I

    invoke-virtual {v8}, LX/2p5;->ordinal()I

    move-result p1

    aget v4, v4, p1

    div-int/lit8 p1, v1, 0x2

    invoke-direct {v3, v4, p1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_a

    .line 467036
    :cond_a
    invoke-static {p0, v8, v4}, LX/2oX;->a(LX/2oX;LX/2p5;LX/7Ka;)V

    .line 467037
    :cond_b
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_9

    .line 467038
    :cond_c
    iput v1, p0, LX/2oX;->m:I

    .line 467039
    iput v0, p0, LX/2oX;->l:I

    goto/16 :goto_0

    .line 467040
    :cond_d
    iget-object v2, p0, LX/2oX;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v1, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, LX/2oX;->h:I

    goto/16 :goto_5

    .line 467041
    :cond_e
    iput v3, p0, LX/2oX;->h:I

    .line 467042
    iput v1, p0, LX/2oX;->i:I

    goto/16 :goto_6

    .line 467043
    :cond_f
    iget-object v2, p0, LX/2oX;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    goto/16 :goto_7

    .line 467044
    :cond_10
    iget-object v2, p0, LX/2oX;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    goto/16 :goto_8

    :cond_11
    move-object v3, v4

    goto :goto_c
.end method

.method public final requestLayout()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 466966
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->requestLayout()V

    .line 466967
    iput v0, p0, LX/2oX;->m:I

    .line 466968
    iput v0, p0, LX/2oX;->l:I

    .line 466969
    return-void
.end method

.method public setInnerResource(I)V
    .locals 2

    .prologue
    .line 466959
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 466960
    iput p1, p0, LX/2oX;->b:I

    .line 466961
    iget v0, p0, LX/2oX;->b:I

    invoke-virtual {p0, v0}, LX/2oX;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/2oX;->c:Landroid/view/View;

    .line 466962
    iget-object v0, p0, LX/2oX;->c:Landroid/view/View;

    if-nez v0, :cond_1

    .line 466963
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid inner view resourceId specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 466964
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 466965
    :cond_1
    return-void
.end method
