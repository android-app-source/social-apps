.class public final LX/30T;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Uo;

.field public final b:LX/2a0;

.field public final c:LX/0WJ;

.field public final d:LX/0u7;


# direct methods
.method public constructor <init>(LX/0Uo;LX/2a0;LX/0WJ;LX/0u7;)V
    .locals 0
    .param p1    # LX/0Uo;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # LX/2a0;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # LX/0WJ;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # LX/0u7;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 484491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484492
    iput-object p1, p0, LX/30T;->a:LX/0Uo;

    .line 484493
    iput-object p2, p0, LX/30T;->b:LX/2a0;

    .line 484494
    iput-object p3, p0, LX/30T;->c:LX/0WJ;

    .line 484495
    iput-object p4, p0, LX/30T;->d:LX/0u7;

    .line 484496
    return-void
.end method

.method private b(LX/2Jg;)V
    .locals 2

    .prologue
    .line 484497
    iget-object v0, p0, LX/30T;->b:LX/2a0;

    .line 484498
    invoke-static {v0}, LX/2a0;->b(LX/2a0;)LX/2Im;

    move-result-object p0

    move-object v0, p0

    .line 484499
    move-object v0, v0

    .line 484500
    if-eqz v0, :cond_0

    .line 484501
    sget-object v1, LX/2Im;->CONNECTED:LX/2Im;

    invoke-virtual {p1, v1}, LX/2Jg;->a(Ljava/lang/Enum;)V

    .line 484502
    invoke-virtual {p1, v0}, LX/2Jg;->a(Ljava/lang/Enum;)V

    .line 484503
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/2Jj;
    .locals 3

    .prologue
    .line 484504
    new-instance v0, LX/2Jg;

    invoke-direct {v0}, LX/2Jg;-><init>()V

    .line 484505
    invoke-virtual {p0}, LX/30T;->b()LX/2Jh;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Jg;->a(Ljava/lang/Enum;)V

    .line 484506
    invoke-direct {p0, v0}, LX/30T;->b(LX/2Jg;)V

    .line 484507
    iget-object v1, p0, LX/30T;->c:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/30T;->c:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->d()Z

    move-result v1

    if-nez v1, :cond_2

    .line 484508
    sget-object v1, LX/2Jq;->LOGGED_IN:LX/2Jq;

    .line 484509
    :goto_0
    move-object v1, v1

    .line 484510
    if-eqz v1, :cond_0

    .line 484511
    sget-object v1, LX/2Jq;->LOGGED_IN:LX/2Jq;

    invoke-virtual {v0, v1}, LX/2Jg;->a(Ljava/lang/Enum;)V

    .line 484512
    :cond_0
    iget-object v1, p0, LX/30T;->d:LX/0u7;

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, LX/0u7;->a(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 484513
    sget-object v1, LX/2Ji;->NOT_LOW:LX/2Ji;

    .line 484514
    :goto_1
    move-object v1, v1

    .line 484515
    if-eqz v1, :cond_1

    .line 484516
    sget-object v1, LX/2Ji;->NOT_LOW:LX/2Ji;

    invoke-virtual {v0, v1}, LX/2Jg;->a(Ljava/lang/Enum;)V

    .line 484517
    :cond_1
    new-instance v1, LX/2Jj;

    invoke-direct {v1, v0}, LX/2Jj;-><init>(LX/2Jg;)V

    return-object v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final b()LX/2Jh;
    .locals 1

    .prologue
    .line 484518
    iget-object v0, p0, LX/30T;->a:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 484519
    sget-object v0, LX/2Jh;->FOREGROUND:LX/2Jh;

    .line 484520
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/2Jh;->BACKGROUND:LX/2Jh;

    goto :goto_0
.end method
