.class public LX/23G;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile k:LX/23G;


# instance fields
.field public final b:LX/0Zb;

.field private final c:LX/03V;

.field private final d:LX/17Q;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:LX/0aG;

.field public final g:LX/0pf;

.field public final h:LX/23H;

.field private final i:LX/0Uh;

.field public j:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 363749
    const-class v0, LX/23G;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/23G;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/03V;LX/17Q;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/0pf;LX/23H;LX/0Uh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 363846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 363847
    iput-object p1, p0, LX/23G;->b:LX/0Zb;

    .line 363848
    iput-object p2, p0, LX/23G;->c:LX/03V;

    .line 363849
    iput-object p3, p0, LX/23G;->d:LX/17Q;

    .line 363850
    iput-object p4, p0, LX/23G;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 363851
    iput-object p5, p0, LX/23G;->f:LX/0aG;

    .line 363852
    iput-object p6, p0, LX/23G;->g:LX/0pf;

    .line 363853
    iput-object p7, p0, LX/23G;->h:LX/23H;

    .line 363854
    iput-object p8, p0, LX/23G;->i:LX/0Uh;

    .line 363855
    const/4 v0, 0x0

    iput v0, p0, LX/23G;->j:I

    .line 363856
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;ZZ)C
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 363814
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 363815
    if-nez v1, :cond_0

    .line 363816
    :goto_0
    return v3

    .line 363817
    :cond_0
    if-eqz p2, :cond_1

    .line 363818
    const/16 v3, 0x43

    goto :goto_0

    .line 363819
    :cond_1
    instance-of v0, v1, Lcom/facebook/graphql/model/Sponsorable;

    if-eqz v0, :cond_5

    move-object v0, v1

    .line 363820
    check-cast v0, Lcom/facebook/graphql/model/Sponsorable;

    .line 363821
    invoke-interface {v0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v2

    .line 363822
    if-eqz v2, :cond_2

    sget-object v4, Lcom/facebook/graphql/model/SponsoredImpression;->n:Lcom/facebook/graphql/model/SponsoredImpression;

    if-eq v2, v4, :cond_2

    .line 363823
    iget v4, v2, Lcom/facebook/graphql/model/SponsoredImpression;->t:I

    move v2, v4

    .line 363824
    :goto_1
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, v4, :cond_4

    .line 363825
    if-lez v2, :cond_3

    const/16 v3, 0x58

    goto :goto_0

    :cond_2
    move v2, v3

    .line 363826
    goto :goto_1

    .line 363827
    :cond_3
    const/16 v3, 0x47

    goto :goto_0

    .line 363828
    :cond_4
    if-lez v2, :cond_5

    .line 363829
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    goto :goto_0

    .line 363830
    :cond_5
    invoke-static {v1}, LX/23G;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 363831
    const/16 v5, 0x49

    .line 363832
    :goto_2
    move v3, v5

    .line 363833
    goto :goto_0

    .line 363834
    :cond_6
    if-eqz p3, :cond_7

    .line 363835
    const/16 v5, 0x4e

    goto :goto_2

    .line 363836
    :cond_7
    iget-object v5, p0, LX/23G;->g:LX/0pf;

    invoke-virtual {v5, v1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v5

    .line 363837
    if-eqz v5, :cond_b

    .line 363838
    iget-boolean v6, v5, LX/1g0;->f:Z

    move v6, v6

    .line 363839
    if-eqz v6, :cond_8

    .line 363840
    const/16 v5, 0x4f

    goto :goto_2

    .line 363841
    :cond_8
    iget-boolean v7, v5, LX/1g0;->d:Z

    if-nez v7, :cond_9

    iget-wide v7, v5, LX/1g0;->a:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-lez v7, :cond_c

    :cond_9
    const/4 v7, 0x1

    :goto_3
    move v5, v7

    .line 363842
    if-eqz v5, :cond_a

    .line 363843
    const/16 v5, 0x6f

    goto :goto_2

    .line 363844
    :cond_a
    const/16 v5, 0x2e

    goto :goto_2

    .line 363845
    :cond_b
    const/16 v5, 0x3f

    goto :goto_2

    :cond_c
    const/4 v7, 0x0

    goto :goto_3
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 363813
    invoke-static {p0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static a(LX/0QB;)LX/23G;
    .locals 12

    .prologue
    .line 363800
    sget-object v0, LX/23G;->k:LX/23G;

    if-nez v0, :cond_1

    .line 363801
    const-class v1, LX/23G;

    monitor-enter v1

    .line 363802
    :try_start_0
    sget-object v0, LX/23G;->k:LX/23G;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 363803
    if-eqz v2, :cond_0

    .line 363804
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 363805
    new-instance v3, LX/23G;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v6

    check-cast v6, LX/17Q;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v8

    check-cast v8, LX/0aG;

    invoke-static {v0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v9

    check-cast v9, LX/0pf;

    invoke-static {v0}, LX/23H;->a(LX/0QB;)LX/23H;

    move-result-object v10

    check-cast v10, LX/23H;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-direct/range {v3 .. v11}, LX/23G;-><init>(LX/0Zb;LX/03V;LX/17Q;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/0pf;LX/23H;LX/0Uh;)V

    .line 363806
    move-object v0, v3

    .line 363807
    sput-object v0, LX/23G;->k:LX/23G;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 363808
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 363809
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 363810
    :cond_1
    sget-object v0, LX/23G;->k:LX/23G;

    return-object v0

    .line 363811
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 363812
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/0fz;Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0fz;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 363781
    const-string v1, "SponsoredFeedUnitValidator.serializeFeedUnitCollection"

    const v2, -0x1d3fb768

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 363782
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, LX/0fz;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 363783
    :goto_0
    invoke-virtual {p1}, LX/0fz;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 363784
    invoke-virtual {p1, v0}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v2

    .line 363785
    invoke-virtual {p1, v0}, LX/0fz;->f(I)Z

    move-result v3

    invoke-interface {p2, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-direct {p0, v2, v3, v4}, LX/23G;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;ZZ)C

    move-result v2

    .line 363786
    if-eqz v2, :cond_0

    .line 363787
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 363788
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 363789
    :cond_1
    const/16 v0, 0x7c

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 363790
    iget-object v0, p0, LX/23G;->h:LX/23H;

    invoke-virtual {v0}, LX/23H;->b()Ljava/util/Iterator;

    move-result-object v2

    .line 363791
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 363792
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/82V;

    .line 363793
    iget-object v3, v0, LX/82V;->b:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-object v0, v3

    .line 363794
    const/4 v3, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-direct {p0, v0, v3, v4}, LX/23G;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;ZZ)C

    move-result v0

    .line 363795
    if-eqz v0, :cond_2

    .line 363796
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 363797
    :catchall_0
    move-exception v0

    const v1, -0x58d88ea

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 363798
    :cond_3
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 363799
    const v1, -0x724f5392

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0
.end method

.method private a(LX/0fz;Ljava/util/Set;Ljava/util/Set;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0fz;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 363763
    move v9, v2

    move v8, v2

    move v7, v2

    move v6, v2

    move v10, v2

    move v1, v2

    .line 363764
    :goto_0
    invoke-virtual {p1}, LX/0fz;->size()I

    move-result v0

    if-ge v9, v0, :cond_3

    .line 363765
    invoke-virtual {p1, v9}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 363766
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 363767
    if-eqz v3, :cond_4

    invoke-static {v3}, LX/23G;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 363768
    invoke-virtual {p1, v9}, LX/0fz;->f(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 363769
    add-int/lit8 v6, v6, 0x1

    .line 363770
    add-int/lit8 v8, v8, 0x1

    move v7, v2

    move v1, v2

    move v3, v2

    .line 363771
    :goto_1
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    move v10, v1

    move v1, v3

    goto :goto_0

    .line 363772
    :cond_0
    invoke-static {v3}, LX/23G;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 363773
    invoke-static {v0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    .line 363774
    if-nez v10, :cond_1

    const/4 v3, 0x1

    :goto_2
    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-interface {p3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    move-object v0, p0

    invoke-static/range {v0 .. v8}, LX/23G;->a(LX/23G;IZZZZIII)V

    .line 363775
    add-int/lit8 v10, v10, 0x1

    move v0, v2

    .line 363776
    :goto_3
    add-int/lit8 v6, v6, 0x1

    .line 363777
    add-int/lit8 v7, v7, 0x1

    move v1, v10

    move v3, v0

    goto :goto_1

    :cond_1
    move v3, v2

    .line 363778
    goto :goto_2

    .line 363779
    :cond_2
    add-int/lit8 v0, v1, 0x1

    goto :goto_3

    .line 363780
    :cond_3
    return-void

    :cond_4
    move v3, v1

    move v1, v10

    goto :goto_1
.end method

.method private static a(LX/23G;IZZZZIII)V
    .locals 2

    .prologue
    .line 363750
    iget-object v0, p0, LX/23G;->b:LX/0Zb;

    .line 363751
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "ad_distance"

    invoke-direct {v1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "distance"

    invoke-virtual {v1, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string p0, "client_invalidation"

    invoke-virtual {v1, p0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string p0, "first_ad"

    invoke-virtual {v1, p0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string p0, "new_unit"

    invoke-virtual {v1, p0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string p0, "reinserted_unit"

    invoke-virtual {v1, p0, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string p0, "native_newsfeed"

    .line 363752
    iput-object p0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 363753
    move-object v1, v1

    .line 363754
    if-ltz p6, :cond_0

    .line 363755
    const-string p0, "position"

    invoke-virtual {v1, p0, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 363756
    :cond_0
    if-ltz p7, :cond_1

    .line 363757
    const-string p0, "position_after_gap"

    invoke-virtual {v1, p0, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 363758
    :cond_1
    if-ltz p8, :cond_2

    .line 363759
    const-string p0, "gaps_above"

    invoke-virtual {v1, p0, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 363760
    :cond_2
    move-object v1, v1

    .line 363761
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 363762
    return-void
.end method

.method public static a(LX/23G;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;I)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/HasGapRule;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 363531
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 363532
    check-cast v0, Lcom/facebook/graphql/model/HasGapRule;

    .line 363533
    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-static {v0, v1}, LX/6X8;->a(Lcom/facebook/graphql/model/HideableUnit;Lcom/facebook/graphql/enums/StoryVisibility;)V

    .line 363534
    invoke-static {v0}, LX/6X8;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/6X8;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v0, v1}, LX/6X8;->a(Lcom/facebook/graphql/enums/StoryVisibility;)LX/6X8;

    move-result-object v0

    invoke-virtual {v0}, LX/6X8;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/facebook/graphql/model/HasGapRule;

    .line 363535
    invoke-virtual {p1, v8}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 363536
    invoke-static {v2}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    .line 363537
    instance-of v0, v8, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-eqz v0, :cond_3

    move-object v0, v8

    .line 363538
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 363539
    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v1

    .line 363540
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 363541
    instance-of v5, v1, LX/16g;

    if-eqz v5, :cond_1

    .line 363542
    check-cast v1, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 363543
    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 363544
    :goto_1
    if-eqz v1, :cond_0

    .line 363545
    invoke-interface {v8}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-static {v1, v5, p2, p3}, LX/17Q;->a(LX/0lF;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 363546
    iget-object v5, p0, LX/23G;->b:LX/0Zb;

    invoke-interface {v5, v1}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 363547
    :cond_1
    instance-of v5, v1, LX/16h;

    if-eqz v5, :cond_2

    .line 363548
    check-cast v1, LX/16h;

    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    goto :goto_1

    .line 363549
    :cond_2
    instance-of v5, v1, LX/4ZX;

    if-eqz v5, :cond_6

    .line 363550
    check-cast v1, LX/4ZX;

    invoke-virtual {v1}, LX/4ZX;->a()LX/162;

    move-result-object v1

    goto :goto_1

    .line 363551
    :cond_3
    invoke-interface {v8}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {v3, v0, p2, p3}, LX/17Q;->a(LX/0lF;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 363552
    iget-object v1, p0, LX/23G;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 363553
    :cond_4
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 363554
    new-instance v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;

    invoke-interface {v8}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-interface {v8}, Lcom/facebook/graphql/model/HideableUnit;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, LX/162;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    const/4 v5, 0x0

    invoke-interface {v8}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v8}, Lcom/facebook/graphql/model/HideableUnit;->H_()I

    move-result v7

    instance-of v11, v8, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    if-eqz v11, :cond_5

    check-cast v8, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-interface {v8}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object v8

    :goto_2
    invoke-direct/range {v0 .. v8}, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;-><init>(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;ZLjava/lang/String;ILjava/lang/String;)V

    .line 363555
    const-string v1, "hideFeedStoryParams"

    invoke-virtual {v10, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 363556
    iget-object v0, p0, LX/23G;->f:LX/0aG;

    const-string v1, "feed_hide_story"

    const v2, 0x196dc336

    invoke-static {v0, v1, v10, v2}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 363557
    return-void

    :cond_5
    move-object v8, v9

    .line 363558
    goto :goto_2

    :cond_6
    move-object v1, v9

    goto :goto_1
.end method

.method public static a(LX/23G;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;IIZ)V
    .locals 7

    .prologue
    .line 363746
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 363747
    iget-object v6, p0, LX/23G;->b:LX/0Zb;

    const/4 v3, 0x1

    const/4 v5, 0x0

    move v1, p2

    move v2, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, LX/17Q;->a(LX/162;IIZZLjava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v6, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 363748
    return-void
.end method

.method public static a(LX/23G;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;IIZLjava/lang/String;)V
    .locals 7

    .prologue
    .line 363743
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 363744
    iget-object v6, p0, LX/23G;->b:LX/0Zb;

    const/4 v3, 0x0

    move v1, p2

    move v2, p3

    move v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, LX/17Q;->a(LX/162;IIZZLjava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v6, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 363745
    return-void
.end method

.method private a(LX/0fz;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 363732
    const-string v2, "SponsoredFeedUnitValidator.invalidateFirstPositionSponsorable"

    const v3, -0x744bf809

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    move v3, v0

    move v2, v0

    .line 363733
    :goto_0
    :try_start_0
    invoke-virtual {p1}, LX/0fz;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 363734
    invoke-virtual {p1, v3}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 363735
    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, LX/23G;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 363736
    invoke-static {v0}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 363737
    invoke-static {p0}, LX/23G;->c(LX/23G;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-static {p0, p1, v3, v4}, LX/23G;->a(LX/23G;LX/0fz;II)Z

    move-result v4

    if-nez v4, :cond_3

    .line 363738
    :cond_0
    check-cast v0, Lcom/facebook/graphql/model/Sponsorable;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 363739
    const-string v2, "ad_first_position"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v4}, LX/23G;->a(LX/23G;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;I)V

    move v0, v1

    .line 363740
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 363741
    :cond_1
    invoke-static {v0}, LX/23G;->a(Lcom/facebook/graphql/model/FeedUnit;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v2

    goto :goto_1

    .line 363742
    :cond_3
    const v0, 0x42159c03

    invoke-static {v0}, LX/02m;->a(I)V

    return v2

    :catchall_0
    move-exception v0

    const v1, 0x5fd86c9c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(LX/23G;LX/0fz;II)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 363857
    invoke-virtual {p1, p2}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v1

    .line 363858
    add-int/lit8 v0, p2, 0x1

    move v2, v4

    :goto_0
    invoke-virtual {p1}, LX/0fz;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 363859
    invoke-virtual {p1, v0}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 363860
    if-eqz v3, :cond_2

    invoke-virtual {p0, v3}, LX/23G;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 363861
    invoke-static {v3}, LX/23G;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 363862
    sub-int v3, p3, v2

    .line 363863
    const-string v5, "ad_present"

    move-object v0, p0

    move v2, p2

    invoke-static/range {v0 .. v5}, LX/23G;->a(LX/23G;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;IIZLjava/lang/String;)V

    .line 363864
    :goto_1
    return v4

    .line 363865
    :cond_0
    invoke-static {v3}, LX/23G;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_2

    add-int/lit8 v2, v2, 0x1

    if-ne v2, p3, :cond_2

    .line 363866
    invoke-virtual {p1, v1, v0, v4}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 363867
    invoke-static {p0, v1, p2, p3, v4}, LX/23G;->a(LX/23G;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;IIZ)V

    .line 363868
    const/4 v4, 0x1

    goto :goto_1

    .line 363869
    :cond_1
    const-string v5, "move_failure"

    move-object v0, p0

    move v2, p2

    move v3, v4

    invoke-static/range {v0 .. v5}, LX/23G;->a(LX/23G;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;IIZLjava/lang/String;)V

    goto :goto_1

    .line 363870
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 363871
    :cond_3
    sub-int v3, p3, v2

    .line 363872
    const-string v5, "end_of_feed"

    move-object v0, p0

    move v2, p2

    invoke-static/range {v0 .. v5}, LX/23G;->a(LX/23G;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;IIZLjava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 2

    .prologue
    .line 363731
    instance-of v0, p0, Lcom/facebook/graphql/model/HideableUnit;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/facebook/graphql/model/HideableUnit;

    invoke-interface {p0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/SponsoredImpression;)Z
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 363725
    iget-object v2, p0, LX/23G;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0pP;->p:LX/0Tn;

    invoke-interface {v2, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v0

    .line 363726
    :goto_0
    if-eqz v2, :cond_1

    .line 363727
    iget-boolean v2, p1, Lcom/facebook/graphql/model/SponsoredImpression;->z:Z

    move v2, v2

    .line 363728
    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 363729
    goto :goto_0

    :cond_1
    move v0, v1

    .line 363730
    goto :goto_1
.end method

.method private b()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 363718
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 363719
    iget-object v0, p0, LX/23G;->h:LX/23H;

    invoke-virtual {v0}, LX/23H;->b()Ljava/util/Iterator;

    move-result-object v2

    .line 363720
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363721
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/82V;

    .line 363722
    iget-object p0, v0, LX/82V;->b:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-object v0, p0

    .line 363723
    invoke-static {v0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 363724
    :cond_0
    return-object v1
.end method

.method private b(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 363708
    const-string v0, "SponsoredFeedUnitValidator.logValidateNewGapRuleFeedUnits"

    const v1, 0x6f0e62a3

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 363709
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 363710
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    .line 363711
    invoke-static {v2}, LX/23G;->c(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/23G;->h:LX/23H;

    .line 363712
    invoke-static {v3, v0}, LX/23H;->c(LX/23H;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)LX/82V;

    move-result-object p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    :goto_1
    move v0, p1

    .line 363713
    if-nez v0, :cond_0

    .line 363714
    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 363715
    iget-object v2, p0, LX/23G;->b:LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/162;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 363716
    :catchall_0
    move-exception v0

    const v1, -0x6de533f8

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    const v0, -0x55157b8d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 363717
    return-void

    :cond_2
    const/4 p1, 0x0

    goto :goto_1
.end method

.method private b(LX/0fz;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 363689
    move v0, v1

    .line 363690
    :goto_0
    invoke-virtual {p1}, LX/0fz;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 363691
    invoke-virtual {p1, v0}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v2

    .line 363692
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 363693
    if-eqz v3, :cond_1

    invoke-static {v3}, LX/23G;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 363694
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 363695
    const/4 v2, 0x0

    .line 363696
    invoke-virtual {p1, v0}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v3

    .line 363697
    add-int/lit8 v1, v0, 0x1

    :goto_1
    invoke-virtual {p1}, LX/0fz;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 363698
    invoke-virtual {p1, v1}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v4

    .line 363699
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object p0

    .line 363700
    if-eqz p0, :cond_3

    invoke-static {p0}, LX/23G;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 363701
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p1, v3, v1, v2}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 363702
    :goto_2
    move v1, v1

    .line 363703
    :cond_0
    return v1

    .line 363704
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v1, v2

    .line 363705
    goto :goto_2

    .line 363706
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move v1, v2

    .line 363707
    goto :goto_2
.end method

.method private b(LX/0fz;Ljava/util/Set;)Z
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0fz;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v2, 0x1

    const/4 v6, -0x1

    const/4 v3, 0x0

    .line 363624
    move v9, v3

    move v1, v3

    move-object v10, v11

    move v5, v3

    .line 363625
    :goto_0
    invoke-virtual {p1}, LX/0fz;->size()I

    move-result v0

    if-ge v9, v0, :cond_6

    .line 363626
    invoke-virtual {p1, v9}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v4

    .line 363627
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 363628
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, LX/23G;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 363629
    invoke-virtual {p1, v9}, LX/0fz;->f(I)Z

    move-result v7

    if-eqz v7, :cond_1

    move v1, v3

    move-object v10, v11

    .line 363630
    :cond_0
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 363631
    :cond_1
    invoke-static {v0}, LX/23G;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 363632
    check-cast v0, Lcom/facebook/graphql/model/HasGapRule;

    .line 363633
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v12

    .line 363634
    if-eqz v10, :cond_4

    .line 363635
    invoke-interface {v0}, Lcom/facebook/graphql/model/HasGapRule;->n()I

    move-result v7

    invoke-interface {v10}, Lcom/facebook/graphql/model/HasGapRule;->n()I

    move-result v8

    invoke-static {v7, v8}, LX/23G;->a(II)I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    .line 363636
    if-ge v1, v7, :cond_3

    .line 363637
    sub-int v0, v7, v1

    .line 363638
    invoke-static {p0}, LX/23G;->c(LX/23G;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 363639
    invoke-static {p0, p1, v9, v0}, LX/23G;->a(LX/23G;LX/0fz;II)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 363640
    add-int/lit8 v1, v1, 0x1

    move v0, v2

    .line 363641
    :goto_2
    if-nez v0, :cond_b

    .line 363642
    invoke-static {p0}, LX/23G;->d(LX/23G;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/23G;->h:LX/23H;

    invoke-virtual {v0, v4, v9, v11}, LX/23H;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 363643
    invoke-virtual {p1, v4}, LX/0fz;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 363644
    add-int/lit8 v0, v9, -0x1

    move-object v4, v10

    :goto_3
    move v9, v0

    move-object v10, v4

    .line 363645
    goto :goto_1

    .line 363646
    :cond_2
    invoke-static {v4}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    move-object v0, p0

    move v5, v3

    move v7, v6

    move v8, v6

    invoke-static/range {v0 .. v8}, LX/23G;->a(LX/23G;IZZZZIII)V

    .line 363647
    const-string v0, "ad_spacing_violation"

    invoke-static {p0, v12, v0, v1}, LX/23G;->a(LX/23G;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;I)V

    move v0, v2

    :goto_4
    move v5, v0

    .line 363648
    goto :goto_1

    :cond_3
    move v1, v3

    move-object v4, v0

    move v0, v9

    .line 363649
    goto :goto_3

    :cond_4
    move v1, v3

    move-object v10, v0

    move v0, v5

    .line 363650
    goto :goto_4

    .line 363651
    :cond_5
    invoke-static {v0}, LX/23G;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363652
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 363653
    :cond_6
    invoke-static {p0}, LX/23G;->d(LX/23G;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 363654
    iget-object v0, p0, LX/23G;->h:LX/23H;

    .line 363655
    iget-object v1, v0, LX/23H;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    .line 363656
    iget-object v1, v0, LX/23H;->d:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/82V;

    .line 363657
    :goto_5
    move-object v0, v1

    .line 363658
    if-eqz v0, :cond_a

    .line 363659
    iget-object v1, v0, LX/82V;->b:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-object v1, v1

    .line 363660
    iget v2, v0, LX/82V;->c:I

    move v2, v2

    .line 363661
    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 363662
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 363663
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    invoke-static {v3}, LX/23G;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 363664
    const/4 v9, 0x0

    .line 363665
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/HasGapRule;

    invoke-interface {v3}, Lcom/facebook/graphql/model/HasGapRule;->n()I

    move-result v11

    move v7, v4

    move v6, v4

    move v8, v4

    .line 363666
    :goto_6
    invoke-virtual {p1}, LX/0fz;->size()I

    move-result v3

    if-ge v7, v3, :cond_8

    .line 363667
    invoke-virtual {p1, v7}, LX/0fz;->f(I)Z

    move-result v3

    if-nez v3, :cond_8

    .line 363668
    invoke-virtual {p1, v7}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 363669
    if-eqz v3, :cond_f

    invoke-virtual {p0, v3}, LX/23G;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v12

    if-nez v12, :cond_f

    .line 363670
    invoke-static {v3}, LX/23G;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 363671
    check-cast v3, Lcom/facebook/graphql/model/HasGapRule;

    move v6, v4

    move-object v8, v3

    move v3, v4

    .line 363672
    :goto_7
    add-int/lit8 v7, v7, 0x1

    move-object v9, v8

    move v8, v6

    move v6, v3

    goto :goto_6

    .line 363673
    :cond_7
    invoke-static {v3}, LX/23G;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 363674
    add-int/lit8 v3, v6, 0x1

    .line 363675
    if-eqz v9, :cond_e

    if-nez v8, :cond_e

    .line 363676
    invoke-interface {v9}, Lcom/facebook/graphql/model/HasGapRule;->n()I

    move-result v6

    invoke-static {v6, v11}, LX/23G;->a(II)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    .line 363677
    if-lt v3, v6, :cond_e

    move v6, v7

    move-object v8, v9

    .line 363678
    goto :goto_7

    .line 363679
    :cond_8
    if-lez v8, :cond_9

    invoke-virtual {p1, v1, v8, v4}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;IZ)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 363680
    sub-int v3, v8, v2

    invoke-static {p0, v1, v2, v3, v10}, LX/23G;->a(LX/23G;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;IIZ)V

    move v4, v10

    .line 363681
    :cond_9
    move v1, v4

    .line 363682
    if-eqz v1, :cond_a

    .line 363683
    iget-object v1, p0, LX/23G;->h:LX/23H;

    .line 363684
    iget-object v2, v0, LX/82V;->b:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-object v0, v2

    .line 363685
    invoke-static {v1, v0}, LX/23H;->c(LX/23H;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)LX/82V;

    move-result-object v2

    .line 363686
    if-eqz v2, :cond_a

    .line 363687
    iget-object v3, v1, LX/23H;->d:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 363688
    :cond_a
    return v5

    :cond_b
    move v0, v5

    goto/16 :goto_4

    :cond_c
    move v0, v3

    goto/16 :goto_2

    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_e
    move v6, v8

    move-object v8, v9

    goto :goto_7

    :cond_f
    move v3, v6

    move v6, v8

    move-object v8, v9

    goto :goto_7
.end method

.method private static c(LX/23G;)Z
    .locals 3

    .prologue
    .line 363623
    iget-object v0, p0, LX/23G;->i:LX/0Uh;

    const/16 v1, 0x302

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static c(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 2

    .prologue
    .line 363622
    invoke-static {p0}, LX/23G;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p0, Lcom/facebook/graphql/model/HasGapRule;

    invoke-interface {p0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/23G;)Z
    .locals 3

    .prologue
    .line 363621
    iget-object v0, p0, LX/23G;->i:LX/0Uh;

    const/16 v1, 0x2ff

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static d(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 363620
    invoke-static {p0}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/facebook/graphql/model/HasGapRule;

    invoke-interface {p0}, Lcom/facebook/graphql/model/HasGapRule;->n()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0fz;Ljava/util/List;ZLX/0qw;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0fz;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;Z",
            "LX/0qw;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 363572
    const-string v2, "SponsoredFeedUnitValidator.processSponsoredValidations"

    const v3, 0x2c84ce39

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 363573
    if-nez p1, :cond_0

    .line 363574
    const v0, 0x3a13da9

    invoke-static {v0}, LX/02m;->a(I)V

    .line 363575
    :goto_0
    return-void

    .line 363576
    :cond_0
    :try_start_0
    invoke-static {p0}, LX/23G;->d(LX/23G;)Z

    .line 363577
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 363578
    invoke-direct {p0, p1}, LX/23G;->a(LX/0fz;)Z

    .line 363579
    invoke-direct {p0, p1}, LX/23G;->b(LX/0fz;)Z

    .line 363580
    :cond_1
    sget-object v2, LX/0qw;->CHUNKED_REMAINDER:LX/0qw;

    if-eq p4, v2, :cond_2

    sget-object v2, LX/0qw;->FULL:LX/0qw;

    if-ne p4, v2, :cond_3

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    const/4 v1, 0x1

    .line 363581
    :cond_3
    iget-object v2, p0, LX/23G;->i:LX/0Uh;

    const/16 v3, 0x300

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_7

    if-eqz v1, :cond_7

    .line 363582
    iget-object v1, p0, LX/23G;->h:LX/23H;

    invoke-virtual {v1}, LX/23H;->a()I

    move-result v4

    .line 363583
    iget-object v1, p0, LX/23G;->i:LX/0Uh;

    const/16 v2, 0x303

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 363584
    if-eqz v2, :cond_9

    .line 363585
    if-eqz p3, :cond_4

    invoke-direct {p0, p1, p2}, LX/23G;->a(LX/0fz;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    :cond_4
    move-object v1, v0

    .line 363586
    :goto_1
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 363587
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 363588
    invoke-static {v0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 363589
    :cond_5
    move-object v0, v3

    .line 363590
    invoke-direct {p0}, LX/23G;->b()Ljava/util/Set;

    move-result-object v5

    .line 363591
    invoke-direct {p0, p1, v0}, LX/23G;->b(LX/0fz;Ljava/util/Set;)Z

    move-result v3

    .line 363592
    invoke-direct {p0}, LX/23G;->b()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 363593
    invoke-interface {v5, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 363594
    invoke-direct {p0, p1, v0, v5}, LX/23G;->a(LX/0fz;Ljava/util/Set;Ljava/util/Set;)V

    .line 363595
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 363596
    :cond_6
    if-eqz p3, :cond_7

    if-eqz v2, :cond_7

    .line 363597
    iget-object v0, p0, LX/23G;->h:LX/23H;

    invoke-virtual {v0}, LX/23H;->a()I

    move-result v5

    .line 363598
    invoke-direct {p0, p1, p2}, LX/23G;->a(LX/0fz;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    .line 363599
    iget-object p4, v0, LX/23G;->b:LX/0Zb;

    iget p1, v0, LX/23G;->j:I

    move-object v6, v1

    move-object v7, v2

    move v8, v3

    move v9, v4

    move v10, v5

    .line 363600
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "feed_unit_collection"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "previous_state"

    invoke-virtual {v1, v2, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "current_state"

    invoke-virtual {v1, v2, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "client_invalidation"

    invoke-virtual {v1, v2, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "previous_cache_size"

    invoke-virtual {v1, v2, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "current_cache_size"

    invoke-virtual {v1, v2, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "sequence"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "native_newsfeed"

    .line 363601
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 363602
    move-object v1, v1

    .line 363603
    move-object v6, v1

    .line 363604
    invoke-interface {p4, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 363605
    iget v6, v0, LX/23G;->j:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v0, LX/23G;->j:I

    .line 363606
    :cond_7
    if-eqz p3, :cond_8

    .line 363607
    invoke-direct {p0, p2}, LX/23G;->b(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363608
    :cond_8
    :goto_3
    const v0, 0x7434dfed

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 363609
    :catch_0
    move-exception v0

    .line 363610
    :try_start_1
    iget-object v1, p0, LX/23G;->c:LX/03V;

    sget-object v2, LX/23G;->a:Ljava/lang/String;

    const-string v3, "Exception during ad invalidation checks"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 363611
    :catchall_0
    move-exception v0

    const v1, -0x3d1c5919

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_9
    move-object v1, v0

    goto/16 :goto_1

    .line 363612
    :cond_a
    const/4 v0, 0x0

    :goto_4
    invoke-virtual {p1}, LX/0fz;->size()I

    move-result v6

    if-ge v0, v6, :cond_6

    .line 363613
    invoke-virtual {p1, v0}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v6

    .line 363614
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v7

    .line 363615
    invoke-static {v6}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v6

    .line 363616
    if-eqz v7, :cond_b

    invoke-static {v7}, LX/23G;->c(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result p4

    if-eqz p4, :cond_b

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 363617
    invoke-static {v7}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    invoke-static {v6}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v6

    .line 363618
    iget-object v7, p0, LX/23G;->b:LX/0Zb;

    invoke-static {v6}, LX/17Q;->a(LX/162;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 363619
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 363559
    instance-of v2, p1, Lcom/facebook/graphql/model/Sponsorable;

    if-nez v2, :cond_1

    .line 363560
    :cond_0
    :goto_0
    return v0

    .line 363561
    :cond_1
    check-cast p1, Lcom/facebook/graphql/model/Sponsorable;

    .line 363562
    invoke-interface {p1}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v2, v3, :cond_2

    move v0, v1

    .line 363563
    goto :goto_0

    .line 363564
    :cond_2
    invoke-interface {p1}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v2

    .line 363565
    if-eqz v2, :cond_0

    sget-object v3, Lcom/facebook/graphql/model/SponsoredImpression;->n:Lcom/facebook/graphql/model/SponsoredImpression;

    if-eq v2, v3, :cond_0

    .line 363566
    iget-boolean v3, v2, Lcom/facebook/graphql/model/SponsoredImpression;->u:Z

    move v3, v3

    .line 363567
    if-eqz v3, :cond_3

    move v0, v1

    .line 363568
    goto :goto_0

    .line 363569
    :cond_3
    iget-boolean v3, v2, Lcom/facebook/graphql/model/SponsoredImpression;->q:Z

    move v3, v3

    .line 363570
    if-eqz v3, :cond_0

    invoke-direct {p0, v2}, LX/23G;->a(Lcom/facebook/graphql/model/SponsoredImpression;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 363571
    goto :goto_0
.end method
