.class public LX/2SD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0SG;

.field private final c:LX/0W3;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 411934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 411935
    iput-object p1, p0, LX/2SD;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 411936
    iput-object p2, p0, LX/2SD;->b:LX/0SG;

    .line 411937
    iput-object p3, p0, LX/2SD;->c:LX/0W3;

    .line 411938
    return-void
.end method

.method public static f()LX/0Tn;
    .locals 3

    .prologue
    .line 411939
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "rtc_voicemail_asset_url_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/2SD;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static g()LX/0Tn;
    .locals 3

    .prologue
    .line 411933
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "rtc_voicemail_asset_url_download_time_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/2SD;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 411932
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 411919
    iget-object v0, p0, LX/2SD;->c:LX/0W3;

    invoke-virtual {v0}, LX/0W3;->a()LX/0W4;

    move-result-object v0

    sget-wide v2, LX/0X5;->hO:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 411920
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 411921
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 411922
    :cond_0
    :goto_0
    move-object v0, v1

    .line 411923
    return-object v0

    .line 411924
    :cond_1
    invoke-static {}, LX/2SD;->h()Ljava/lang/String;

    move-result-object v4

    .line 411925
    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 411926
    array-length v6, v5

    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_0

    aget-object v7, v5, v2

    .line 411927
    const-string p0, "="

    invoke-virtual {v7, p0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 411928
    aget-object p0, v7, v3

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 411929
    const/4 v1, 0x1

    aget-object v1, v7, v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 411930
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 411931
    iget-object v0, p0, LX/2SD;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {}, LX/2SD;->f()LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
