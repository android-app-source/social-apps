.class public abstract LX/2SP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QR",
        "<",
        "LX/0Px",
        "<+",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;


# instance fields
.field private final b:LX/2Sg;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 412254
    const-class v0, LX/2SP;

    sput-object v0, LX/2SP;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 412251
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/2SP;-><init>(LX/2Sg;)V

    .line 412252
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 412253
    return-void
.end method

.method public constructor <init>(LX/2Sg;)V
    .locals 0

    .prologue
    .line 412224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412225
    iput-object p1, p0, LX/2SP;->b:LX/2Sg;

    .line 412226
    return-void
.end method

.method public static a(LX/0Px;)LX/7BE;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/2SP;",
            ">;)",
            "LX/7BE;"
        }
    .end annotation

    .prologue
    .line 412255
    sget-object v1, LX/7BE;->NOT_READY:LX/7BE;

    .line 412256
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 412257
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 412258
    invoke-virtual {v0}, LX/2SP;->a()LX/7BE;

    move-result-object v0

    sget-object v4, LX/7BE;->READY:LX/7BE;

    if-ne v0, v4, :cond_1

    .line 412259
    sget-object v0, LX/7BE;->PARTIAL:LX/7BE;

    .line 412260
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 412261
    :cond_0
    sget-object v1, LX/7BE;->READY:LX/7BE;

    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(LX/EPu;)Z
    .locals 1

    .prologue
    .line 412262
    sget-object v0, LX/EPu;->NETWORK_ONLY:LX/EPu;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/EPu;->NETWORK_BYPASS_CACHE_ONLY:LX/EPu;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()LX/7BE;
.end method

.method public a(LX/2SR;LX/2Sp;)V
    .locals 0

    .prologue
    .line 412263
    return-void
.end method

.method public final a(LX/Cw5;)V
    .locals 5

    .prologue
    .line 412264
    iget-object v0, p0, LX/2SP;->b:LX/2Sg;

    if-nez v0, :cond_0

    .line 412265
    :goto_0
    return-void

    .line 412266
    :cond_0
    iget-object v0, p0, LX/2SP;->b:LX/2Sg;

    invoke-virtual {p0}, LX/2SP;->e()Ljava/lang/String;

    move-result-object v1

    .line 412267
    invoke-static {v0, v1}, LX/2Sg;->d(LX/2Sg;Ljava/lang/String;)LX/11o;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "data_freshness"

    .line 412268
    iget-object p0, p1, LX/Cw5;->c:LX/0ta;

    move-object p0, p0

    .line 412269
    invoke-virtual {p0}, LX/0ta;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {v4, p0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v4

    const p0, 0x79a3ef0c

    invoke-static {v2, v1, v3, v4, p0}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 412270
    goto :goto_0
.end method

.method public a(LX/Cwb;)V
    .locals 0

    .prologue
    .line 412249
    return-void
.end method

.method public a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 0
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 412250
    return-void
.end method

.method public a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 0

    .prologue
    .line 412248
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 412247
    const/4 v0, 0x1

    return v0
.end method

.method public abstract c()V
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 412246
    const-string v0, ""

    return-object v0
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 412241
    iget-object v0, p0, LX/2SP;->b:LX/2Sg;

    if-nez v0, :cond_0

    .line 412242
    :goto_0
    return-void

    .line 412243
    :cond_0
    iget-object v0, p0, LX/2SP;->b:LX/2Sg;

    invoke-virtual {p0}, LX/2SP;->e()Ljava/lang/String;

    move-result-object v1

    .line 412244
    invoke-static {v0, v1}, LX/2Sg;->d(LX/2Sg;Ljava/lang/String;)LX/11o;

    move-result-object v2

    const p0, 0x74c801cf

    invoke-static {v2, v1, p0}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 412245
    goto :goto_0
.end method

.method public final h()V
    .locals 5

    .prologue
    .line 412236
    iget-object v0, p0, LX/2SP;->b:LX/2Sg;

    if-nez v0, :cond_0

    .line 412237
    :goto_0
    return-void

    .line 412238
    :cond_0
    iget-object v0, p0, LX/2SP;->b:LX/2Sg;

    invoke-virtual {p0}, LX/2SP;->e()Ljava/lang/String;

    move-result-object v1

    .line 412239
    invoke-static {v0, v1}, LX/2Sg;->d(LX/2Sg;Ljava/lang/String;)LX/11o;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "fetch_timeout"

    invoke-static {v4, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v4

    const p0, -0x440fd538

    invoke-static {v2, v1, v3, v4, p0}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 412240
    goto :goto_0
.end method

.method public kE_()LX/CwU;
    .locals 1

    .prologue
    .line 412235
    sget-object v0, LX/CwU;->NULL_STATE:LX/CwU;

    return-object v0
.end method

.method public final kF_()V
    .locals 4

    .prologue
    .line 412227
    iget-object v0, p0, LX/2SP;->b:LX/2Sg;

    if-nez v0, :cond_0

    .line 412228
    :goto_0
    return-void

    .line 412229
    :cond_0
    iget-object v0, p0, LX/2SP;->b:LX/2Sg;

    invoke-virtual {p0}, LX/2SP;->e()Ljava/lang/String;

    move-result-object v1

    .line 412230
    const-string v2, "pre_fetch"

    invoke-static {v0, v2}, LX/2Sg;->d(LX/2Sg;Ljava/lang/String;)LX/11o;

    move-result-object v2

    const-string v3, "pre_fetch"

    const p0, -0x30bba308

    invoke-static {v2, v3, p0}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 412231
    iget-object v2, v0, LX/2Sg;->e:LX/11i;

    invoke-static {v0}, LX/2Sg;->l(LX/2Sg;)LX/2Sh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v2

    .line 412232
    if-eqz v2, :cond_1

    invoke-interface {v2, v1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 412233
    :cond_1
    :goto_1
    goto :goto_0

    .line 412234
    :cond_2
    const v3, -0x6762628a

    invoke-static {v2, v1, v3}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    goto :goto_1
.end method
