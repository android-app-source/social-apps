.class public LX/2Pp;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static volatile sInitialized:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 407238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized ensure()V
    .locals 2

    .prologue
    .line 407239
    const-class v1, LX/2Pp;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, LX/2Pp;->sInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 407240
    :goto_0
    monitor-exit v1

    return-void

    .line 407241
    :cond_0
    :try_start_1
    const-string v0, "omnistore"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 407242
    const/4 v0, 0x1

    sput-boolean v0, LX/2Pp;->sInitialized:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 407243
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
