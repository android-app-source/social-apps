.class public LX/2Sj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2Sj;


# instance fields
.field private final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 413142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413143
    iput-object p1, p0, LX/2Sj;->a:LX/0SG;

    .line 413144
    return-void
.end method

.method public static a(LX/0QB;)LX/2Sj;
    .locals 4

    .prologue
    .line 413145
    sget-object v0, LX/2Sj;->b:LX/2Sj;

    if-nez v0, :cond_1

    .line 413146
    const-class v1, LX/2Sj;

    monitor-enter v1

    .line 413147
    :try_start_0
    sget-object v0, LX/2Sj;->b:LX/2Sj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413148
    if-eqz v2, :cond_0

    .line 413149
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 413150
    new-instance p0, LX/2Sj;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p0, v3}, LX/2Sj;-><init>(LX/0SG;)V

    .line 413151
    move-object v0, p0

    .line 413152
    sput-object v0, LX/2Sj;->b:LX/2Sj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413153
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413154
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413155
    :cond_1
    sget-object v0, LX/2Sj;->b:LX/2Sj;

    return-object v0

    .line 413156
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413157
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JJ)Z
    .locals 5

    .prologue
    .line 413158
    iget-object v0, p0, LX/2Sj;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long/2addr v0, p3

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
