.class public LX/2C1;
.super LX/16B;
.source ""

# interfaces
.implements LX/0Up;
.implements LX/2C2;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<",
            "LX/2C3;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile i:LX/2C1;


# instance fields
.field private final b:LX/0Xl;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1fU;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2AX;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:LX/0Uh;

.field private final f:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
    .end annotation
.end field

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1se;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 381984
    const/4 v0, 0x1

    new-array v0, v0, [LX/2C3;

    const/4 v1, 0x0

    sget-object v2, LX/2C3;->ALWAYS:LX/2C3;

    aput-object v2, v0, v1

    .line 381985
    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 381986
    new-instance v2, LX/2C4;

    invoke-direct {v2, v1}, LX/2C4;-><init>(LX/0Px;)V

    move-object v0, v2

    .line 381987
    sput-object v0, LX/2C1;->a:LX/0Rl;

    return-void
.end method

.method public constructor <init>(LX/0Xl;LX/0Ot;LX/299;LX/0Uh;Landroid/os/Handler;)V
    .locals 1
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xl;",
            "LX/0Ot",
            "<",
            "LX/1fU;",
            ">;",
            "Lcom/facebook/push/mqtt/service/MqttTopicListProvider;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 381959
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 381960
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/2C1;->g:Ljava/util/Set;

    .line 381961
    iput-object p2, p0, LX/2C1;->c:LX/0Ot;

    .line 381962
    iput-object p1, p0, LX/2C1;->b:LX/0Xl;

    .line 381963
    iget-object v0, p3, LX/299;->a:LX/0Ot;

    move-object v0, v0

    .line 381964
    iput-object v0, p0, LX/2C1;->d:LX/0Ot;

    .line 381965
    iput-object p4, p0, LX/2C1;->e:LX/0Uh;

    .line 381966
    iput-object p5, p0, LX/2C1;->f:Landroid/os/Handler;

    .line 381967
    return-void
.end method

.method public static a(LX/0QB;)LX/2C1;
    .locals 9

    .prologue
    .line 381968
    sget-object v0, LX/2C1;->i:LX/2C1;

    if-nez v0, :cond_1

    .line 381969
    const-class v1, LX/2C1;

    monitor-enter v1

    .line 381970
    :try_start_0
    sget-object v0, LX/2C1;->i:LX/2C1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 381971
    if-eqz v2, :cond_0

    .line 381972
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 381973
    new-instance v3, LX/2C1;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    const/16 v5, 0x1012

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/299;->a(LX/0QB;)LX/299;

    move-result-object v6

    check-cast v6, LX/299;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/1rR;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    invoke-direct/range {v3 .. v8}, LX/2C1;-><init>(LX/0Xl;LX/0Ot;LX/299;LX/0Uh;Landroid/os/Handler;)V

    .line 381974
    move-object v0, v3

    .line 381975
    sput-object v0, LX/2C1;->i:LX/2C1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 381976
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 381977
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 381978
    :cond_1
    sget-object v0, LX/2C1;->i:LX/2C1;

    return-object v0

    .line 381979
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 381980
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/1se;LX/2C3;)LX/308;
    .locals 3

    .prologue
    .line 381981
    sget-object v0, LX/307;->a:[I

    invoke-virtual {p1}, LX/2C3;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 381982
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported persistence="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 381983
    :pswitch_0
    new-instance v0, LX/308;

    const-string v1, "com.facebook.push.mqtt.category.SAME_APP"

    invoke-direct {v0, p0, v1}, LX/308;-><init>(LX/1se;Ljava/lang/String;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private declared-synchronized a(Ljava/lang/Boolean;)V
    .locals 8

    .prologue
    .line 382004
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/2C1;->k()LX/0P1;

    move-result-object v0

    .line 382005
    invoke-direct {p0, v0}, LX/2C1;->a(Ljava/util/Map;)V

    .line 382006
    invoke-static {p0, v0}, LX/2C1;->b(LX/2C1;Ljava/util/Map;)Ljava/util/Set;

    move-result-object v2

    .line 382007
    iget-object v1, p0, LX/2C1;->g:Ljava/util/Set;

    invoke-static {v2, v1}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v3

    .line 382008
    iget-object v1, p0, LX/2C1;->g:Ljava/util/Set;

    invoke-static {v1, v2}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v4

    .line 382009
    if-eqz p1, :cond_0

    .line 382010
    iget-object v1, p0, LX/2C1;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1fU;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 382011
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    .line 382012
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    .line 382013
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 382014
    iget-object p1, v1, LX/1fU;->a:LX/1fX;

    new-instance v0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;

    invoke-direct {v0, v1, v5, v6, v7}, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;-><init>(LX/1fU;ZLX/0Px;LX/0Px;)V

    invoke-interface {p1, v0}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 382015
    :goto_0
    iget-object v1, p0, LX/2C1;->g:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 382016
    iget-object v1, p0, LX/2C1;->g:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382017
    monitor-exit p0

    return-void

    .line 382018
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 382019
    :cond_0
    iget-object v1, p0, LX/2C1;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1fU;

    invoke-virtual {v1, v3, v4}, LX/1fU;->a(Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private a(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/1se;",
            "LX/2C3;",
            ">;)V"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 381922
    sget-object v0, LX/2C1;->a:LX/0Rl;

    invoke-static {p1, v0}, LX/0PM;->a(Ljava/util/Map;LX/0Rl;)Ljava/util/Map;

    move-result-object v0

    .line 381923
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 381924
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 381925
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1se;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2C3;

    invoke-static {v1, v0}, LX/2C1;->a(LX/1se;LX/2C3;)LX/308;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 381926
    :cond_0
    iget-object v0, p0, LX/2C1;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fU;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 381927
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 381928
    iget-object v3, v0, LX/1fU;->a:LX/1fX;

    new-instance p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$3;

    invoke-direct {p0, v0, v2}, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$3;-><init>(LX/1fU;Ljava/util/List;)V

    invoke-interface {v3, p0}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 381929
    return-void
.end method

.method public static b(LX/2C1;Ljava/util/Map;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/1se;",
            "LX/2C3;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "LX/1se;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381988
    invoke-static {p0}, LX/2C1;->l(LX/2C1;)LX/2C3;

    move-result-object v0

    .line 381989
    sget-object v1, LX/2C1;->a:LX/0Rl;

    invoke-static {v1}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v1

    .line 381990
    new-instance p0, LX/309;

    invoke-direct {p0, v0}, LX/309;-><init>(Ljava/lang/Comparable;)V

    move-object v0, p0

    .line 381991
    invoke-static {v1, v0}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v0

    invoke-static {p1, v0}, LX/0PM;->a(Ljava/util/Map;LX/0Rl;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private k()LX/0P1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "LX/1se;",
            "LX/2C3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381992
    new-instance v1, LX/0UE;

    invoke-direct {v1}, LX/0UE;-><init>()V

    .line 381993
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 381994
    iget-object v0, p0, LX/2C1;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2AX;

    .line 381995
    invoke-interface {v0}, LX/2AX;->get()LX/0P1;

    move-result-object v4

    .line 381996
    invoke-virtual {v4}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1se;

    .line 381997
    iget-object v6, v0, LX/1se;->a:Ljava/lang/String;

    move-object v6, v6

    .line 381998
    invoke-virtual {v1, v6}, LX/0UE;->add(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 381999
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Duplicate topics not allowed at this time: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 382000
    iget-object v3, v0, LX/1se;->a:Ljava/lang/String;

    move-object v0, v3

    .line 382001
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 382002
    :cond_1
    invoke-virtual {v4, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v2, v0, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 382003
    :cond_2
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized l(LX/2C1;)LX/2C3;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 381952
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2C1;->h:Z

    if-eqz v0, :cond_0

    .line 381953
    sget-object v0, LX/2C3;->APP_USE:LX/2C3;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381954
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, LX/2C3;->ALWAYS:LX/2C3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 381955
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 381956
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, LX/2C1;->a(Ljava/lang/Boolean;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381957
    monitor-exit p0

    return-void

    .line 381958
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/facebook/auth/component/AuthenticationResult;)V
    .locals 0
    .param p1    # Lcom/facebook/auth/component/AuthenticationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 381950
    invoke-virtual {p0}, LX/2C1;->a()V

    .line 381951
    return-void
.end method

.method public final declared-synchronized b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/1se;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 381949
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2C1;->g:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 381947
    invoke-virtual {p0}, LX/2C1;->a()V

    .line 381948
    return-void
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 381944
    new-instance v0, LX/2Hh;

    invoke-direct {v0, p0}, LX/2Hh;-><init>(LX/2C1;)V

    .line 381945
    iget-object v1, p0, LX/2C1;->b:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/2C1;->f:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 381946
    return-void
.end method

.method public final declared-synchronized onAppActive()V
    .locals 1

    .prologue
    .line 381940
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/2C1;->h:Z

    .line 381941
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2C1;->a(Ljava/lang/Boolean;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381942
    monitor-exit p0

    return-void

    .line 381943
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onAppStopped()V
    .locals 1

    .prologue
    .line 381936
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/2C1;->h:Z

    .line 381937
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2C1;->a(Ljava/lang/Boolean;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381938
    monitor-exit p0

    return-void

    .line 381939
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onDeviceActive()V
    .locals 1

    .prologue
    .line 381933
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/2C1;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381934
    monitor-exit p0

    return-void

    .line 381935
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onDeviceStopped()V
    .locals 1

    .prologue
    .line 381930
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/2C1;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381931
    monitor-exit p0

    return-void

    .line 381932
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
