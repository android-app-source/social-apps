.class public LX/3C7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3C7;


# instance fields
.field private a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/blescan/BleScanResult;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0SG;

.field private c:LX/0W3;


# direct methods
.method public constructor <init>(LX/0SG;LX/0W3;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 529837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529838
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/3C7;->a:Ljava/util/HashMap;

    .line 529839
    iput-object p1, p0, LX/3C7;->b:LX/0SG;

    .line 529840
    iput-object p2, p0, LX/3C7;->c:LX/0W3;

    .line 529841
    return-void
.end method

.method public static a(LX/0QB;)LX/3C7;
    .locals 5

    .prologue
    .line 529808
    sget-object v0, LX/3C7;->d:LX/3C7;

    if-nez v0, :cond_1

    .line 529809
    const-class v1, LX/3C7;

    monitor-enter v1

    .line 529810
    :try_start_0
    sget-object v0, LX/3C7;->d:LX/3C7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 529811
    if-eqz v2, :cond_0

    .line 529812
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 529813
    new-instance p0, LX/3C7;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-direct {p0, v3, v4}, LX/3C7;-><init>(LX/0SG;LX/0W3;)V

    .line 529814
    move-object v0, p0

    .line 529815
    sput-object v0, LX/3C7;->d:LX/3C7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529816
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 529817
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 529818
    :cond_1
    sget-object v0, LX/3C7;->d:LX/3C7;

    return-object v0

    .line 529819
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 529820
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(J)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/blescan/BleScanResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 529824
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 529825
    iget-object v0, p0, LX/3C7;->c:LX/0W3;

    sget-wide v4, LX/0X5;->aB:J

    invoke-interface {v0, v4, v5}, LX/0W4;->c(J)J

    move-result-wide v4

    .line 529826
    iget-object v3, p0, LX/3C7;->a:Ljava/util/HashMap;

    monitor-enter v3

    .line 529827
    :try_start_0
    iget-object v0, p0, LX/3C7;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 529828
    iget-object v0, p0, LX/3C7;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v8

    .line 529829
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 529830
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 529831
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/blescan/BleScanResult;

    iget-wide v10, v1, Lcom/facebook/blescan/BleScanResult;->a:J

    cmp-long v1, v10, p1

    if-lez v1, :cond_1

    .line 529832
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 529833
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/blescan/BleScanResult;

    iget-wide v0, v0, Lcom/facebook/blescan/BleScanResult;->a:J

    add-long/2addr v0, v4

    cmp-long v0, v8, v0

    if-lez v0, :cond_0

    .line 529834
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 529835
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529836
    return-object v2
.end method

.method public final a(Lcom/facebook/blescan/BleScanResult;)V
    .locals 3

    .prologue
    .line 529821
    iget-object v1, p0, LX/3C7;->a:Ljava/util/HashMap;

    monitor-enter v1

    .line 529822
    :try_start_0
    iget-object v0, p0, LX/3C7;->a:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/facebook/blescan/BleScanResult;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 529823
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 529805
    iget-object v1, p0, LX/3C7;->a:Ljava/util/HashMap;

    monitor-enter v1

    .line 529806
    :try_start_0
    iget-object v0, p0, LX/3C7;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 529807
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
