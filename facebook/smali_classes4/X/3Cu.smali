.class public LX/3Cu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 530920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530921
    return-void
.end method

.method public static b(LX/2kW;Ljava/lang/String;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            "*>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 530922
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530923
    const/4 v0, 0x0

    .line 530924
    :goto_0
    return-object v0

    .line 530925
    :cond_0
    new-instance v0, LX/BE2;

    invoke-direct {v0, p1}, LX/BE2;-><init>(Ljava/lang/String;)V

    .line 530926
    invoke-virtual {p0, p1, v0}, LX/2kW;->a(Ljava/lang/String;LX/3Cf;)V

    .line 530927
    iget-object p0, v0, LX/BE2;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-object v0, p0

    .line 530928
    goto :goto_0
.end method

.method public static b(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;)Z
    .locals 2

    .prologue
    .line 530929
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    .line 530930
    if-eqz v0, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/2kW;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            "*>;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 530931
    new-instance v0, LX/BE3;

    invoke-direct {v0}, LX/BE3;-><init>()V

    .line 530932
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, LX/2kW;->a(Ljava/lang/String;LX/3Cf;)V

    .line 530933
    iget-object v1, v0, LX/BE3;->a:LX/0Pz;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 530934
    return-object v0
.end method
