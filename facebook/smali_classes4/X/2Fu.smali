.class public LX/2Fu;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/2Fv;

.field private final c:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 387383
    const-class v0, LX/2Fu;

    sput-object v0, LX/2Fu;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2Fv;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387385
    iput-object p1, p0, LX/2Fu;->b:LX/2Fv;

    .line 387386
    iput-object p2, p0, LX/2Fu;->c:LX/0W3;

    .line 387387
    return-void
.end method

.method public static b(LX/0QB;)LX/2Fu;
    .locals 10

    .prologue
    .line 387388
    new-instance v2, LX/2Fu;

    .line 387389
    new-instance v3, LX/2Fv;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/2Fw;->b(LX/0QB;)LX/2Fw;

    move-result-object v5

    check-cast v5, LX/2Fw;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    .line 387390
    new-instance v9, LX/2zf;

    const-class v7, Landroid/content/Context;

    const-class v8, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v7, v8}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {p0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v8

    check-cast v8, LX/0aU;

    invoke-direct {v9, v7, v8}, LX/2zf;-><init>(Landroid/content/Context;LX/0aU;)V

    .line 387391
    move-object v7, v9

    .line 387392
    check-cast v7, LX/2zf;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v8

    check-cast v8, LX/0W3;

    invoke-direct/range {v3 .. v8}, LX/2Fv;-><init>(Landroid/content/Context;LX/2Fw;LX/03V;LX/2zf;LX/0W3;)V

    .line 387393
    move-object v0, v3

    .line 387394
    check-cast v0, LX/2Fv;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v1

    check-cast v1, LX/0W3;

    invoke-direct {v2, v0, v1}, LX/2Fu;-><init>(LX/2Fv;LX/0W3;)V

    .line 387395
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 387396
    iget-object v0, p0, LX/2Fu;->c:LX/0W3;

    sget-wide v2, LX/0X5;->D:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387397
    iget-object v0, p0, LX/2Fu;->b:LX/2Fv;

    invoke-virtual {v0}, LX/2Fv;->a()V

    .line 387398
    :cond_0
    return-void
.end method
