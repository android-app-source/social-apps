.class public LX/2AW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2AX;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2AW;


# instance fields
.field public final a:Z

.field public final b:Z


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 377379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 377380
    const/16 v0, 0x483

    invoke-virtual {p1, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2AW;->a:Z

    .line 377381
    const/16 v0, 0x1dc

    invoke-virtual {p1, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2AW;->b:Z

    .line 377382
    return-void
.end method

.method public static a(LX/0QB;)LX/2AW;
    .locals 4

    .prologue
    .line 377360
    sget-object v0, LX/2AW;->c:LX/2AW;

    if-nez v0, :cond_1

    .line 377361
    const-class v1, LX/2AW;

    monitor-enter v1

    .line 377362
    :try_start_0
    sget-object v0, LX/2AW;->c:LX/2AW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 377363
    if-eqz v2, :cond_0

    .line 377364
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 377365
    new-instance p0, LX/2AW;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/2AW;-><init>(LX/0Uh;)V

    .line 377366
    move-object v0, p0

    .line 377367
    sput-object v0, LX/2AW;->c:LX/2AW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377368
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 377369
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 377370
    :cond_1
    sget-object v0, LX/2AW;->c:LX/2AW;

    return-object v0

    .line 377371
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 377372
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "LX/1se;",
            "LX/2C3;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 377373
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 377374
    iget-boolean v1, p0, LX/2AW;->a:Z

    if-nez v1, :cond_0

    .line 377375
    iget-boolean v1, p0, LX/2AW;->b:Z

    if-eqz v1, :cond_1

    .line 377376
    new-instance v1, LX/1se;

    const-string v2, "/t_sp"

    invoke-direct {v1, v2, v3}, LX/1se;-><init>(Ljava/lang/String;I)V

    sget-object v2, LX/2C3;->APP_USE:LX/2C3;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377377
    :cond_0
    :goto_0
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0

    .line 377378
    :cond_1
    new-instance v1, LX/1se;

    const-string v2, "/t_p"

    invoke-direct {v1, v2, v3}, LX/1se;-><init>(Ljava/lang/String;I)V

    sget-object v2, LX/2C3;->APP_USE:LX/2C3;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
