.class public LX/2N4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/2N4;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dC;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dt;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dB;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2N5;

.field private final f:LX/2NH;

.field public final g:LX/2My;

.field public final h:LX/2Mv;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FDp;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/2N6;

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;LX/2N5;LX/2NH;LX/2My;LX/2Mv;LX/0Or;LX/0Or;LX/2N6;LX/0Or;LX/0Or;)V
    .locals 0
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsShouldRedirectToMessenger;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsDiodeReduceFetchThreadListEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/6dC;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6dt;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6dB;",
            ">;",
            "LX/2N5;",
            "LX/2NH;",
            "LX/2My;",
            "LX/2Mv;",
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FDp;",
            ">;",
            "LX/2N6;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 398428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398429
    iput-object p1, p0, LX/2N4;->b:LX/0Or;

    .line 398430
    iput-object p2, p0, LX/2N4;->c:LX/0Or;

    .line 398431
    iput-object p3, p0, LX/2N4;->d:LX/0Or;

    .line 398432
    iput-object p4, p0, LX/2N4;->e:LX/2N5;

    .line 398433
    iput-object p5, p0, LX/2N4;->f:LX/2NH;

    .line 398434
    iput-object p6, p0, LX/2N4;->g:LX/2My;

    .line 398435
    iput-object p7, p0, LX/2N4;->h:LX/2Mv;

    .line 398436
    iput-object p8, p0, LX/2N4;->a:LX/0Or;

    .line 398437
    iput-object p9, p0, LX/2N4;->i:LX/0Or;

    .line 398438
    iput-object p10, p0, LX/2N4;->j:LX/2N6;

    .line 398439
    iput-object p11, p0, LX/2N4;->k:LX/0Or;

    .line 398440
    iput-object p12, p0, LX/2N4;->l:LX/0Or;

    .line 398441
    return-void
.end method

.method public static a(LX/0QB;)LX/2N4;
    .locals 3

    .prologue
    .line 398418
    sget-object v0, LX/2N4;->m:LX/2N4;

    if-nez v0, :cond_1

    .line 398419
    const-class v1, LX/2N4;

    monitor-enter v1

    .line 398420
    :try_start_0
    sget-object v0, LX/2N4;->m:LX/2N4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 398421
    if-eqz v2, :cond_0

    .line 398422
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2N4;->b(LX/0QB;)LX/2N4;

    move-result-object v0

    sput-object v0, LX/2N4;->m:LX/2N4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398423
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 398424
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 398425
    :cond_1
    sget-object v0, LX/2N4;->m:LX/2N4;

    return-object v0

    .line 398426
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 398427
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2N4;LX/0ux;Ljava/lang/String;I)LX/FDm;
    .locals 10
    .param p1    # LX/0ux;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 398401
    const-string v0, "DbFetchThreadHandler.doMessagesQuery"

    const v1, -0x1dbef990

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 398402
    :try_start_0
    iget-object v0, p0, LX/2N4;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 398403
    const v0, -0x728e5c4c

    invoke-static {v7, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 398404
    :try_start_1
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v8

    .line 398405
    iget-object v0, p0, LX/2N4;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dB;

    iget-object v0, v0, LX/6dB;->d:LX/6d9;

    invoke-virtual {v0}, LX/6d9;->a()Landroid/net/Uri;

    move-result-object v1

    .line 398406
    iget-object v9, p0, LX/2N4;->f:LX/2NH;

    iget-object v0, p0, LX/2N4;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dC;

    sget-object v2, LX/2NH;->a:[Ljava/lang/String;

    invoke-virtual {p1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    move-object v5, p2

    invoke-virtual/range {v0 .. v6}, LX/2Rg;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 398407
    new-instance v1, LX/6d6;

    iget-object v2, v9, LX/2NH;->q:LX/2Lz;

    invoke-direct {v1, v9, v0, v2}, LX/6d6;-><init>(LX/2NH;Landroid/database/Cursor;LX/2Lz;)V

    move-object v1, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 398408
    :goto_0
    :try_start_2
    invoke-virtual {v1}, LX/6d6;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 398409
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v8, v2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 398410
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, LX/6d6;->b()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 398411
    :catchall_1
    move-exception v0

    const v1, -0x2662f9f0

    :try_start_4
    invoke-static {v7, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 398412
    :catchall_2
    move-exception v0

    const v1, 0x196830ce

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 398413
    :cond_0
    :try_start_5
    invoke-virtual {v1}, LX/6d6;->b()V

    .line 398414
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 398415
    new-instance v0, LX/FDm;

    invoke-direct {v0, v8}, LX/FDm;-><init>(Ljava/util/LinkedHashMap;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 398416
    const v1, -0x4f478eea

    :try_start_6
    invoke-static {v7, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 398417
    const v1, -0x371dccef

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0
.end method

.method public static a(LX/2N4;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)LX/FDn;
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 398230
    const-string v0, "DbFetchThreadHandler.doThreadQuery"

    const v1, 0x19f7e97c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 398231
    :try_start_0
    const-string v0, "#threads"

    const v1, -0x329717b7

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 398232
    :try_start_1
    iget-object v0, p0, LX/2N4;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dt;

    iget-object v1, p0, LX/2N4;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6dB;

    iget-object v1, v1, LX/6dB;->c:LX/6dA;

    invoke-virtual {v1}, LX/6dA;->a()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/2N5;->a:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/2Rg;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 398233
    iget-object v1, p0, LX/2N4;->e:LX/2N5;

    invoke-virtual {v1, v0}, LX/2N5;->a(Landroid/database/Cursor;)LX/6dO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 398234
    :try_start_2
    invoke-virtual {v1}, LX/6dO;->c()LX/6dP;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v7

    .line 398235
    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 398236
    const v0, -0x65f1e23f

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 398237
    if-nez v7, :cond_0

    .line 398238
    const v0, -0x686ff6c6

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v6

    :goto_0
    return-object v0

    .line 398239
    :catchall_0
    move-exception v1

    :try_start_5
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 398240
    :catchall_1
    move-exception v0

    const v1, 0x6bbfee3b

    :try_start_6
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 398241
    :catchall_2
    move-exception v0

    const v1, 0x4c897e53    # 7.2086168E7f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 398242
    :cond_0
    :try_start_7
    const-string v0, "#messages"

    const v1, -0x799f6b2b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 398243
    :try_start_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 398244
    const/4 v2, 0x3

    new-array v2, v2, [LX/0ux;

    const/4 v3, 0x0

    const-string v4, "thread_key"

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "msg_type"

    sget-object v5, LX/2uW;->FAILED_SEND:LX/2uW;

    iget v5, v5, LX/2uW;->dbKeyValue:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "timestamp_ms"

    const-wide/32 v8, 0x5265c00

    sub-long/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, LX/0uu;->e(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v2}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v4

    .line 398245
    iget-object v0, p0, LX/2N4;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dC;

    iget-object v1, p0, LX/2N4;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6dB;

    iget-object v1, v1, LX/6dB;->d:LX/6d9;

    invoke-virtual {v1}, LX/6d9;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "thread_key"

    aput-object v5, v2, v3

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/2Rg;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    move-result-object v1

    .line 398246
    :try_start_9
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 398247
    iget-object v0, v7, LX/6dP;->a:LX/6g6;

    const/4 v2, 0x1

    .line 398248
    iput-boolean v2, v0, LX/6g6;->x:Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 398249
    :cond_1
    :try_start_a
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 398250
    const v0, 0x67998ffa

    :try_start_b
    invoke-static {v0}, LX/02m;->a(I)V

    .line 398251
    new-instance v0, LX/FDn;

    iget-object v1, v7, LX/6dP;->a:LX/6g6;

    invoke-virtual {v1}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    iget-wide v2, v7, LX/6dP;->b:J

    invoke-direct {v0, v1, v2, v3}, LX/FDn;-><init>(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 398252
    const v1, 0x7ab8ed3e

    invoke-static {v1}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 398253
    :catchall_3
    move-exception v0

    :try_start_c
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 398254
    :catchall_4
    move-exception v0

    const v1, 0x38d0e73f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private a(Lcom/facebook/messaging/model/threads/ThreadCriteria;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 2

    .prologue
    .line 398395
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 398396
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 398397
    :goto_0
    return-object v0

    .line 398398
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 398399
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/2N4;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0

    .line 398400
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No threadid or userkey specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(LX/FDn;I)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 11
    .param p1    # LX/FDn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 398347
    if-nez p1, :cond_0

    .line 398348
    sget-object v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->a:Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 398349
    :goto_0
    return-object v0

    .line 398350
    :cond_0
    iget-object v5, p1, LX/FDn;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 398351
    invoke-static {}, LX/FDx;->newBuilder()LX/FDw;

    move-result-object v0

    iget-object v1, v5, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 398352
    iput-object v1, v0, LX/FDw;->d:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 398353
    move-object v0, v0

    .line 398354
    iput p2, v0, LX/FDw;->a:I

    .line 398355
    move-object v0, v0

    .line 398356
    invoke-virtual {v0}, LX/FDw;->a()LX/FDx;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2N4;->a(LX/FDx;)Ljava/util/LinkedHashMap;

    move-result-object v6

    .line 398357
    new-instance v2, LX/0UE;

    invoke-direct {v2}, LX/0UE;-><init>()V

    .line 398358
    iget-object v3, v5, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    move v1, v4

    :goto_1
    if-ge v1, v7, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 398359
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 398360
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 398361
    :cond_1
    iget-object v3, v5, Lcom/facebook/messaging/model/threads/ThreadSummary;->i:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    move v1, v4

    :goto_2
    if-ge v1, v7, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 398362
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 398363
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 398364
    :cond_2
    iget-object v3, v5, Lcom/facebook/messaging/model/threads/ThreadSummary;->n:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    move v1, v4

    :goto_3
    if-ge v1, v7, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 398365
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 398366
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 398367
    :cond_3
    iget-object v0, p0, LX/2N4;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDp;

    invoke-virtual {v0, v2}, LX/FDp;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    .line 398368
    iget-wide v0, v5, Lcom/facebook/messaging/model/threads/ThreadSummary;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    .line 398369
    sget-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-object v1, v0

    .line 398370
    :goto_4
    iget-object v0, v5, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 398371
    const/4 v0, 0x1

    .line 398372
    iget-object v2, v5, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 398373
    :goto_5
    new-instance v2, Lcom/facebook/messaging/model/messages/MessagesCollection;

    iget-object v3, v5, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    invoke-direct {v2, v3, v6, v0}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    .line 398374
    iget-object v0, v2, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v0, v0

    .line 398375
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    if-lez v3, :cond_4

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 398376
    iget-object v3, p0, LX/2N4;->j:LX/2N6;

    iget-wide v8, v0, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v3, v8, v9}, LX/2N6;->a(J)V

    .line 398377
    :cond_4
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v0

    .line 398378
    iput-object v1, v0, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 398379
    move-object v0, v0

    .line 398380
    iput-object v5, v0, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 398381
    move-object v0, v0

    .line 398382
    iput-object v2, v0, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 398383
    move-object v0, v0

    .line 398384
    iput-object v7, v0, LX/6iO;->e:LX/0Px;

    .line 398385
    move-object v0, v0

    .line 398386
    iget-wide v2, p1, LX/FDn;->b:J

    .line 398387
    iput-wide v2, v0, LX/6iO;->g:J

    .line 398388
    move-object v0, v0

    .line 398389
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    goto/16 :goto_0

    .line 398390
    :cond_5
    const-wide/16 v2, -0x1

    .line 398391
    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 398392
    iget-boolean v8, v0, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-nez v8, :cond_6

    .line 398393
    iget-wide v0, v0, Lcom/facebook/messaging/model/messages/Message;->g:J

    .line 398394
    :goto_6
    iget-wide v2, v5, Lcom/facebook/messaging/model/threads/ThreadSummary;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_7

    sget-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-object v1, v0

    goto :goto_4

    :cond_7
    sget-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_STALE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-object v1, v0

    goto :goto_4

    :cond_8
    move v0, v4

    goto :goto_5

    :cond_9
    move-wide v0, v2

    goto :goto_6
.end method

.method private static b(LX/0QB;)LX/2N4;
    .locals 13

    .prologue
    .line 398345
    new-instance v0, LX/2N4;

    const/16 v1, 0x2748

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x274f

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x2747

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/2N5;->a(LX/0QB;)LX/2N5;

    move-result-object v4

    check-cast v4, LX/2N5;

    invoke-static {p0}, LX/2NH;->a(LX/0QB;)LX/2NH;

    move-result-object v5

    check-cast v5, LX/2NH;

    invoke-static {p0}, LX/2My;->a(LX/0QB;)LX/2My;

    move-result-object v6

    check-cast v6, LX/2My;

    invoke-static {p0}, LX/2Mv;->b(LX/0QB;)LX/2Mv;

    move-result-object v7

    check-cast v7, LX/2Mv;

    const/16 v8, 0x274b

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x2737

    invoke-static {p0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p0}, LX/2N6;->a(LX/0QB;)LX/2N6;

    move-result-object v10

    check-cast v10, LX/2N6;

    const/16 v11, 0x14f0

    invoke-static {p0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x14df

    invoke-static {p0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-direct/range {v0 .. v12}, LX/2N4;-><init>(LX/0Or;LX/0Or;LX/0Or;LX/2N5;LX/2NH;LX/2My;LX/2Mv;LX/0Or;LX/0Or;LX/2N6;LX/0Or;LX/0Or;)V

    .line 398346
    return-object v0
.end method

.method private c(J)J
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 398335
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 398336
    const-string v0, "last_visible_action_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->f(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 398337
    iget-object v0, p0, LX/2N4;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dt;

    iget-object v1, p0, LX/2N4;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6dB;

    iget-object v1, v1, LX/6dB;->c:LX/6dA;

    invoke-virtual {v1}, LX/6dA;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "last_visible_action_id"

    aput-object v3, v2, v5

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const-string v5, "last_visible_action_id DESC"

    invoke-virtual/range {v0 .. v5}, LX/2Rg;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 398338
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398339
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 398340
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 398341
    :goto_0
    return-wide v0

    .line 398342
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 398343
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 398344
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 398329
    iget-object v0, p0, LX/2N4;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dt;

    iget-object v1, p0, LX/2N4;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6dB;

    iget-object v1, v1, LX/6dB;->c:LX/6dA;

    invoke-virtual {v1}, LX/6dA;->a()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "thread_key"

    aput-object v3, v2, v6

    const-string v3, "legacy_thread_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    invoke-virtual/range {v0 .. v5}, LX/2Rg;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 398330
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398331
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 398332
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 398333
    return-object v5

    .line 398334
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJI)Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;
    .locals 7

    .prologue
    .line 398442
    invoke-static {}, LX/FDx;->newBuilder()LX/FDw;

    move-result-object v0

    .line 398443
    iput-object p1, v0, LX/FDw;->d:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 398444
    move-object v0, v0

    .line 398445
    iput p6, v0, LX/FDw;->a:I

    .line 398446
    move-object v0, v0

    .line 398447
    iput-wide p2, v0, LX/FDw;->b:J

    .line 398448
    move-object v0, v0

    .line 398449
    iput-wide p4, v0, LX/FDw;->c:J

    .line 398450
    move-object v0, v0

    .line 398451
    invoke-virtual {v0}, LX/FDw;->a()LX/FDx;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2N4;->a(LX/FDx;)Ljava/util/LinkedHashMap;

    move-result-object v1

    .line 398452
    const/4 v0, 0x0

    .line 398453
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 398454
    const/4 v0, 0x1

    .line 398455
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 398456
    :cond_0
    new-instance v2, Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v2, p1, v1, v0}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    .line 398457
    new-instance v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;

    sget-object v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    const-wide/16 v4, -0x1

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;-><init>(Lcom/facebook/fbservice/results/DataFetchDisposition;Lcom/facebook/messaging/model/messages/MessagesCollection;J)V

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 6

    .prologue
    .line 398310
    const-string v0, "DbFetchThreadHandler.fetchThreadFromDb"

    const v1, 0x4e39eba1    # 7.7980678E8f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 398311
    :try_start_0
    const-string v0, "thread_key"

    invoke-static {p0, p1, v0}, LX/2N4;->a(LX/2N4;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)LX/FDn;

    move-result-object v0

    move-object v0, v0

    .line 398312
    if-nez v0, :cond_0

    .line 398313
    iget-object v2, p0, LX/2N4;->h:LX/2Mv;

    invoke-virtual {v2, p1}, LX/2Mv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/2N4;->g:LX/2My;

    .line 398314
    iget-object v3, v2, LX/2My;->a:LX/0Uh;

    const/16 v4, 0x19f

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v2, v3

    .line 398315
    if-eqz v2, :cond_1

    .line 398316
    new-instance v2, LX/FDn;

    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v3

    .line 398317
    iput-object p1, v3, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 398318
    move-object v3, v3

    .line 398319
    sget-object v4, LX/6ek;->MONTAGE:LX/6ek;

    .line 398320
    iput-object v4, v3, LX/6g6;->A:LX/6ek;

    .line 398321
    move-object v3, v3

    .line 398322
    const/4 v4, 0x1

    .line 398323
    iput-boolean v4, v3, LX/6g6;->H:Z

    .line 398324
    move-object v3, v3

    .line 398325
    invoke-virtual {v3}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v3

    iget-object v4, p0, LX/2N4;->j:LX/2N6;

    invoke-virtual {v4}, LX/2N6;->a()J

    move-result-wide v4

    invoke-direct {v2, v3, v4, v5}, LX/FDn;-><init>(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 398326
    :goto_0
    move-object v0, v2

    .line 398327
    :cond_0
    invoke-direct {p0, v0, p2}, LX/2N4;->a(LX/FDn;I)Lcom/facebook/messaging/service/model/FetchThreadResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 398328
    const v1, 0x4f6fad9e    # 4.0211328E9f

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x52064980

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadCriteria;I)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 1

    .prologue
    .line 398306
    invoke-direct {p0, p1}, LX/2N4;->a(Lcom/facebook/messaging/model/threads/ThreadCriteria;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 398307
    if-nez v0, :cond_0

    .line 398308
    sget-object v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->a:Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 398309
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0, p2}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/FDx;)Ljava/util/LinkedHashMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDx;",
            ")",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 398293
    const-string v0, "DbFetchThreadHandler.doMessagesQuery"

    const v1, 0x45fb2647

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 398294
    :try_start_0
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v0

    .line 398295
    iget-object v1, p1, LX/FDx;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v1, :cond_0

    .line 398296
    const-string v1, "thread_key"

    iget-object v2, p1, LX/FDx;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 398297
    :cond_0
    iget-object v1, p1, LX/FDx;->g:LX/6ek;

    if-eqz v1, :cond_1

    .line 398298
    const-string v1, "folder"

    iget-object v2, p1, LX/FDx;->g:LX/6ek;

    iget-object v2, v2, LX/6ek;->dbName:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 398299
    :cond_1
    iget-wide v2, p1, LX/FDx;->d:J

    sget v1, LX/FDx;->b:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 398300
    const-string v1, "timestamp_ms"

    iget-wide v2, p1, LX/FDx;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->f(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 398301
    :cond_2
    iget-wide v2, p1, LX/FDx;->e:J

    sget v1, LX/FDx;->b:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 398302
    const-string v1, "timestamp_ms"

    iget-wide v2, p1, LX/FDx;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->c(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 398303
    :cond_3
    const-string v1, "timestamp_ms DESC"

    iget v2, p1, LX/FDx;->c:I

    invoke-static {p0, v0, v1, v2}, LX/2N4;->a(LX/2N4;LX/0ux;Ljava/lang/String;I)LX/FDm;

    move-result-object v0

    .line 398304
    iget-object v0, v0, LX/FDm;->a:Ljava/util/LinkedHashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398305
    const v1, -0x2fda0643

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x742885c7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final varargs a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JI[LX/2uW;)Ljava/util/LinkedHashMap;
    .locals 6
    .param p1    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "JI[",
            "LX/2uW;",
            ")",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 398280
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v1

    .line 398281
    if-eqz p1, :cond_0

    .line 398282
    const-string v0, "thread_key"

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 398283
    :cond_0
    const-wide/16 v2, -0x1

    cmp-long v0, p2, v2

    if-eqz v0, :cond_1

    .line 398284
    const-string v0, "timestamp_ms"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0uu;->c(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 398285
    :cond_1
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 398286
    array-length v3, p5

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, p5, v0

    .line 398287
    iget v4, v4, LX/2uW;->dbKeyValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 398288
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 398289
    :cond_2
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 398290
    const-string v0, "msg_type"

    invoke-static {v0, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 398291
    :cond_3
    const-string v0, "timestamp_ms"

    invoke-static {p0, v1, v0, p4}, LX/2N4;->a(LX/2N4;LX/0ux;Ljava/lang/String;I)LX/FDm;

    move-result-object v0

    .line 398292
    iget-object v0, v0, LX/FDm;->a:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method public final a(J)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 398268
    const-wide/16 v4, -0x1

    cmp-long v0, p1, v4

    if-eqz v0, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 398269
    :goto_0
    return v0

    .line 398270
    :cond_1
    invoke-direct {p0, p1, p2}, LX/2N4;->c(J)J

    move-result-wide v4

    cmp-long v0, v4, p1

    if-eqz v0, :cond_2

    move v0, v1

    .line 398271
    goto :goto_0

    .line 398272
    :cond_2
    const/4 v0, 0x0

    .line 398273
    const-string v3, "action_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v3

    .line 398274
    const/4 v4, 0x1

    invoke-static {p0, v3, v0, v4}, LX/2N4;->a(LX/2N4;LX/0ux;Ljava/lang/String;I)LX/FDm;

    move-result-object v3

    iget-object v3, v3, LX/FDm;->a:Ljava/util/LinkedHashMap;

    .line 398275
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 398276
    :goto_1
    move-object v3, v0

    .line 398277
    iget-object v0, p0, LX/2N4;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/2N4;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 398278
    if-eqz v3, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 398279
    :cond_4
    if-eqz v3, :cond_5

    iget-boolean v0, v3, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-nez v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 398265
    const-string v0, "msg_id"

    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 398266
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, LX/2N4;->a(LX/2N4;LX/0ux;Ljava/lang/String;I)LX/FDm;

    move-result-object v0

    iget-object v0, v0, LX/FDm;->a:Ljava/util/LinkedHashMap;

    .line 398267
    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    return-object v0
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 3

    .prologue
    .line 398259
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 398260
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid montage thread key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 398261
    :cond_1
    const-string v0, "DbFetchThreadHandler.fetchThreadFromDbByMontageThread"

    const v1, 0xe73d3ba

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 398262
    :try_start_0
    const-string v0, "montage_thread_key"

    invoke-static {p0, p1, v0}, LX/2N4;->a(LX/2N4;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)LX/FDn;

    move-result-object v0

    move-object v0, v0

    .line 398263
    invoke-direct {p0, v0, p2}, LX/2N4;->a(LX/FDn;I)Lcom/facebook/messaging/service/model/FetchThreadResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 398264
    const v1, 0x2932c70c

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x4cc6a8eb

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 398255
    const-string v1, "offline_threading_id"

    invoke-static {v1, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 398256
    const/4 v2, 0x1

    invoke-static {p0, v1, v0, v2}, LX/2N4;->a(LX/2N4;LX/0ux;Ljava/lang/String;I)LX/FDm;

    move-result-object v1

    iget-object v1, v1, LX/FDm;->a:Ljava/util/LinkedHashMap;

    .line 398257
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 398258
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    goto :goto_0
.end method
