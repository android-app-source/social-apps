.class public final enum LX/3B2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3B2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3B2;

.field public static final enum ADD_TO_TRAY:LX/3B2;

.field public static final enum CLEAR_FROM_TRAY:LX/3B2;

.field public static final enum CLICK_FROM_TRAY:LX/3B2;

.field public static final enum COMMENT_FROM_TRAY:LX/3B2;

.field public static final enum DROPPED_BY_INDIVIDUAL_SETTING:LX/3B2;

.field public static final enum DROPPED_BY_OVERALL_SETTING:LX/3B2;

.field public static final enum DROPPED_BY_READ_STATE:LX/3B2;

.field public static final enum DROPPED_BY_SEEN_STATE:LX/3B2;

.field public static final enum DROPPED_BY_STALENESS:LX/3B2;

.field public static final enum DROPPED_BY_WHITELIST_TYPES:LX/3B2;

.field public static final enum NON_EXIST_GRAPHQL_STORY:LX/3B2;

.field public static final enum PUSH_NOTIFICATION_RECEIVED:LX/3B2;

.field public static final enum click_primary_action:LX/3B2;

.field public static final enum click_secondary_action:LX/3B2;

.field public static final enum dropped_by_invalid_intent:LX/3B2;

.field public static final enum graph_notification_click:LX/3B2;

.field public static final enum next_notification_fallback:LX/3B2;

.field public static final enum next_notification_loaded:LX/3B2;

.field public static final enum next_notification_next_clicked:LX/3B2;

.field public static final enum next_notification_prev_clicked:LX/3B2;

.field public static final enum next_notification_started:LX/3B2;

.field public static final enum rich_notification_curation:LX/3B2;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 526819
    new-instance v0, LX/3B2;

    const-string v1, "PUSH_NOTIFICATION_RECEIVED"

    invoke-direct {v0, v1, v3}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->PUSH_NOTIFICATION_RECEIVED:LX/3B2;

    .line 526820
    new-instance v0, LX/3B2;

    const-string v1, "ADD_TO_TRAY"

    invoke-direct {v0, v1, v4}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->ADD_TO_TRAY:LX/3B2;

    .line 526821
    new-instance v0, LX/3B2;

    const-string v1, "CLEAR_FROM_TRAY"

    invoke-direct {v0, v1, v5}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->CLEAR_FROM_TRAY:LX/3B2;

    .line 526822
    new-instance v0, LX/3B2;

    const-string v1, "CLICK_FROM_TRAY"

    invoke-direct {v0, v1, v6}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->CLICK_FROM_TRAY:LX/3B2;

    .line 526823
    new-instance v0, LX/3B2;

    const-string v1, "click_primary_action"

    invoke-direct {v0, v1, v7}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->click_primary_action:LX/3B2;

    .line 526824
    new-instance v0, LX/3B2;

    const-string v1, "click_secondary_action"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->click_secondary_action:LX/3B2;

    .line 526825
    new-instance v0, LX/3B2;

    const-string v1, "graph_notification_click"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->graph_notification_click:LX/3B2;

    .line 526826
    new-instance v0, LX/3B2;

    const-string v1, "rich_notification_curation"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->rich_notification_curation:LX/3B2;

    .line 526827
    new-instance v0, LX/3B2;

    const-string v1, "DROPPED_BY_INDIVIDUAL_SETTING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->DROPPED_BY_INDIVIDUAL_SETTING:LX/3B2;

    .line 526828
    new-instance v0, LX/3B2;

    const-string v1, "DROPPED_BY_OVERALL_SETTING"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->DROPPED_BY_OVERALL_SETTING:LX/3B2;

    .line 526829
    new-instance v0, LX/3B2;

    const-string v1, "DROPPED_BY_READ_STATE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->DROPPED_BY_READ_STATE:LX/3B2;

    .line 526830
    new-instance v0, LX/3B2;

    const-string v1, "DROPPED_BY_SEEN_STATE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->DROPPED_BY_SEEN_STATE:LX/3B2;

    .line 526831
    new-instance v0, LX/3B2;

    const-string v1, "DROPPED_BY_WHITELIST_TYPES"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->DROPPED_BY_WHITELIST_TYPES:LX/3B2;

    .line 526832
    new-instance v0, LX/3B2;

    const-string v1, "DROPPED_BY_STALENESS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->DROPPED_BY_STALENESS:LX/3B2;

    .line 526833
    new-instance v0, LX/3B2;

    const-string v1, "dropped_by_invalid_intent"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->dropped_by_invalid_intent:LX/3B2;

    .line 526834
    new-instance v0, LX/3B2;

    const-string v1, "COMMENT_FROM_TRAY"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->COMMENT_FROM_TRAY:LX/3B2;

    .line 526835
    new-instance v0, LX/3B2;

    const-string v1, "NON_EXIST_GRAPHQL_STORY"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->NON_EXIST_GRAPHQL_STORY:LX/3B2;

    .line 526836
    new-instance v0, LX/3B2;

    const-string v1, "next_notification_started"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->next_notification_started:LX/3B2;

    .line 526837
    new-instance v0, LX/3B2;

    const-string v1, "next_notification_next_clicked"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->next_notification_next_clicked:LX/3B2;

    .line 526838
    new-instance v0, LX/3B2;

    const-string v1, "next_notification_prev_clicked"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->next_notification_prev_clicked:LX/3B2;

    .line 526839
    new-instance v0, LX/3B2;

    const-string v1, "next_notification_loaded"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->next_notification_loaded:LX/3B2;

    .line 526840
    new-instance v0, LX/3B2;

    const-string v1, "next_notification_fallback"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/3B2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B2;->next_notification_fallback:LX/3B2;

    .line 526841
    const/16 v0, 0x16

    new-array v0, v0, [LX/3B2;

    sget-object v1, LX/3B2;->PUSH_NOTIFICATION_RECEIVED:LX/3B2;

    aput-object v1, v0, v3

    sget-object v1, LX/3B2;->ADD_TO_TRAY:LX/3B2;

    aput-object v1, v0, v4

    sget-object v1, LX/3B2;->CLEAR_FROM_TRAY:LX/3B2;

    aput-object v1, v0, v5

    sget-object v1, LX/3B2;->CLICK_FROM_TRAY:LX/3B2;

    aput-object v1, v0, v6

    sget-object v1, LX/3B2;->click_primary_action:LX/3B2;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3B2;->click_secondary_action:LX/3B2;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3B2;->graph_notification_click:LX/3B2;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3B2;->rich_notification_curation:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/3B2;->DROPPED_BY_INDIVIDUAL_SETTING:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/3B2;->DROPPED_BY_OVERALL_SETTING:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/3B2;->DROPPED_BY_READ_STATE:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/3B2;->DROPPED_BY_SEEN_STATE:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/3B2;->DROPPED_BY_WHITELIST_TYPES:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/3B2;->DROPPED_BY_STALENESS:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/3B2;->dropped_by_invalid_intent:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/3B2;->COMMENT_FROM_TRAY:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/3B2;->NON_EXIST_GRAPHQL_STORY:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/3B2;->next_notification_started:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/3B2;->next_notification_next_clicked:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/3B2;->next_notification_prev_clicked:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/3B2;->next_notification_loaded:LX/3B2;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/3B2;->next_notification_fallback:LX/3B2;

    aput-object v2, v0, v1

    sput-object v0, LX/3B2;->$VALUES:[LX/3B2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 526842
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3B2;
    .locals 1

    .prologue
    .line 526843
    const-class v0, LX/3B2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3B2;

    return-object v0
.end method

.method public static values()[LX/3B2;
    .locals 1

    .prologue
    .line 526844
    sget-object v0, LX/3B2;->$VALUES:[LX/3B2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3B2;

    return-object v0
.end method
