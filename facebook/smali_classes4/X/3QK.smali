.class public LX/3QK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/2Mk;

.field private final c:LX/0Zm;

.field private final d:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Zb;LX/2Mk;LX/0Zm;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 564053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 564054
    iput-object p1, p0, LX/3QK;->a:LX/0Zb;

    .line 564055
    iput-object p2, p0, LX/3QK;->b:LX/2Mk;

    .line 564056
    iput-object p3, p0, LX/3QK;->c:LX/0Zm;

    .line 564057
    iput-object p4, p0, LX/3QK;->d:LX/0Uh;

    .line 564058
    return-void
.end method

.method public static a(LX/0QB;)LX/3QK;
    .locals 7

    .prologue
    .line 564059
    const-class v1, LX/3QK;

    monitor-enter v1

    .line 564060
    :try_start_0
    sget-object v0, LX/3QK;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 564061
    sput-object v2, LX/3QK;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 564062
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564063
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 564064
    new-instance p0, LX/3QK;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v4

    check-cast v4, LX/2Mk;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v5

    check-cast v5, LX/0Zm;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3QK;-><init>(LX/0Zb;LX/2Mk;LX/0Zm;LX/0Uh;)V

    .line 564065
    move-object v0, p0

    .line 564066
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 564067
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3QK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 564068
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 564069
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 564070
    iget-object v2, p0, LX/3QK;->c:LX/0Zm;

    const-string v3, "message_text_linkified"

    invoke-virtual {v2, v3}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, LX/2Mk;->n(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, LX/2Mk;->t(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 564071
    :cond_0
    :goto_0
    return-void

    .line 564072
    :cond_1
    const-string v2, "linkify_message"

    const v3, 0x727f7761

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 564073
    :try_start_0
    new-instance v2, Landroid/text/SpannableString;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 564074
    const/4 v3, 0x1

    invoke-static {v2, v3}, LX/7Gw;->a(Landroid/text/Spannable;I)Z

    move-result v3

    .line 564075
    const/4 v4, 0x2

    invoke-static {v2, v4}, LX/7Gw;->a(Landroid/text/Spannable;I)Z

    move-result v4

    .line 564076
    const/4 v5, 0x4

    invoke-static {v2, v5}, LX/7Gw;->a(Landroid/text/Spannable;I)Z

    move-result v5

    .line 564077
    const/16 v6, 0x8

    invoke-static {v2, v6}, LX/7Gw;->a(Landroid/text/Spannable;I)Z

    move-result v6

    .line 564078
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "message_text_linkified"

    invoke-direct {v7, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v8, "web_url"

    if-eqz v3, :cond_2

    move v2, v0

    :goto_1
    invoke-virtual {v7, v8, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v7, "email_address"

    if-eqz v4, :cond_3

    move v2, v0

    :goto_2
    invoke-virtual {v3, v7, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "phone_number"

    if-eqz v5, :cond_4

    move v2, v0

    :goto_3
    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "map_address"

    if-eqz v6, :cond_5

    :goto_4
    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "message_id"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 564079
    iget-object v1, p0, LX/3QK;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 564080
    const v0, -0x52d3eb57

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :cond_2
    move v2, v1

    .line 564081
    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2

    :cond_4
    move v2, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4

    .line 564082
    :catchall_0
    move-exception v0

    const v1, 0x3aede0fa

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/messaging/model/messages/MessagesCollection;)V
    .locals 3

    .prologue
    .line 564083
    if-nez p2, :cond_1

    .line 564084
    :cond_0
    :goto_0
    return-void

    .line 564085
    :cond_1
    iget-object v0, p0, LX/3QK;->d:LX/0Uh;

    const/16 v1, 0xef

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564086
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "msg_received"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 564087
    const-string v1, "src"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 564088
    const-string v1, "message_id"

    invoke-static {p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 564089
    iget-object v1, p0, LX/3QK;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 564090
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "msg_received"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 564091
    const-string v1, "src"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 564092
    const-string v1, "message_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 564093
    iget-object v1, p0, LX/3QK;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 564094
    return-void
.end method
