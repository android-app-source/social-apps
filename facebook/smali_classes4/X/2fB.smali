.class public LX/2fB;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 446200
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/2fB;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 446201
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 446202
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/2fB;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;B)V

    .line 446203
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;B)V
    .locals 1

    .prologue
    .line 446204
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 446205
    const v0, 0x7f030640

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 446206
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 446207
    iget-boolean v0, p0, LX/2fB;->a:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x68526621

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 446208
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 446209
    const/4 v1, 0x1

    .line 446210
    iput-boolean v1, p0, LX/2fB;->a:Z

    .line 446211
    const/16 v1, 0x2d

    const v2, -0x2f540bd2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x16d4e1f1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 446212
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 446213
    const/4 v1, 0x0

    .line 446214
    iput-boolean v1, p0, LX/2fB;->a:Z

    .line 446215
    const/16 v1, 0x2d

    const v2, -0x33345001

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
