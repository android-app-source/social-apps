.class public LX/29F;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/29F;


# instance fields
.field private final a:LX/1MH;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/content/ComponentName;",
            "LX/2XT;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1MH;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 376004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376005
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/29F;->b:Ljava/util/Map;

    .line 376006
    iput-object p1, p0, LX/29F;->a:LX/1MH;

    .line 376007
    return-void
.end method

.method public static a(LX/0QB;)LX/29F;
    .locals 4

    .prologue
    .line 376008
    sget-object v0, LX/29F;->c:LX/29F;

    if-nez v0, :cond_1

    .line 376009
    const-class v1, LX/29F;

    monitor-enter v1

    .line 376010
    :try_start_0
    sget-object v0, LX/29F;->c:LX/29F;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 376011
    if-eqz v2, :cond_0

    .line 376012
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 376013
    new-instance p0, LX/29F;

    invoke-static {v0}, LX/1MH;->b(LX/0QB;)LX/1MH;

    move-result-object v3

    check-cast v3, LX/1MH;

    invoke-direct {p0, v3}, LX/29F;-><init>(LX/1MH;)V

    .line 376014
    move-object v0, p0

    .line 376015
    sput-object v0, LX/29F;->c:LX/29F;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 376016
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 376017
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 376018
    :cond_1
    sget-object v0, LX/29F;->c:LX/29F;

    return-object v0

    .line 376019
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 376020
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(LX/2XT;Landroid/content/ServiceConnection;)Z
    .locals 1

    .prologue
    .line 376021
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, LX/2XT;->c:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Landroid/content/ComponentName;)LX/2XT;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 376022
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/29F;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2XT;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(LX/29F;Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1    # Landroid/content/ComponentName;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 376023
    const/4 v0, 0x0

    .line 376024
    monitor-enter p0

    .line 376025
    :try_start_0
    invoke-direct {p0, p1}, LX/29F;->b(Landroid/content/ComponentName;)LX/2XT;

    move-result-object v1

    .line 376026
    if-eqz v1, :cond_0

    .line 376027
    iput-object p2, v1, LX/2XT;->f:Landroid/os/IBinder;

    .line 376028
    iget-object v0, v1, LX/2XT;->c:Ljava/util/Set;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    .line 376029
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376030
    if-eqz v0, :cond_3

    .line 376031
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ServiceConnection;

    .line 376032
    invoke-direct {p0, v1, v0}, LX/29F;->a(LX/2XT;Landroid/content/ServiceConnection;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 376033
    if-eqz p2, :cond_2

    .line 376034
    invoke-interface {v0, p1, p2}, Landroid/content/ServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    goto :goto_0

    .line 376035
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 376036
    :cond_2
    invoke-interface {v0, p1}, Landroid/content/ServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    goto :goto_0

    .line 376037
    :cond_3
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/content/Intent;Landroid/content/ServiceConnection;I)LX/2XW;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 376038
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 376039
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 376040
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    .line 376041
    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    const-string v4, "Bindings are cached by specific service components but none was specified"

    invoke-static {v0, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 376042
    iget-object v0, p0, LX/29F;->b:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2XT;

    .line 376043
    if-nez v0, :cond_2

    .line 376044
    new-instance v0, LX/2XT;

    new-instance v1, LX/2XU;

    invoke-direct {v1, p0}, LX/2XU;-><init>(LX/29F;)V

    invoke-direct {v0, v3, v1, p3}, LX/2XT;-><init>(Landroid/content/ComponentName;LX/2XU;I)V

    .line 376045
    iget-object v1, p0, LX/29F;->b:Ljava/util/Map;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 376046
    :goto_1
    iget-object v0, v1, LX/2XT;->c:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 376047
    iget-boolean v0, v1, LX/2XT;->e:Z

    if-nez v0, :cond_4

    .line 376048
    iget-object v0, p0, LX/29F;->a:LX/1MH;

    iget-object v2, v1, LX/2XT;->b:LX/2XU;

    iget v4, v1, LX/2XT;->d:I

    invoke-virtual {v0, p1, v2, v4}, LX/1MH;->a(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    .line 376049
    const/4 v0, 0x1

    iput-boolean v0, v1, LX/2XT;->e:Z

    .line 376050
    if-nez v2, :cond_0

    .line 376051
    iget-object v0, p0, LX/29F;->b:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 376052
    :cond_0
    new-instance v0, LX/2XW;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v1}, LX/2XW;-><init>(ZLandroid/os/IBinder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376053
    :goto_2
    monitor-exit p0

    return-object v0

    :cond_1
    move v0, v2

    .line 376054
    goto :goto_0

    .line 376055
    :cond_2
    :try_start_1
    iget v4, v0, LX/2XT;->d:I

    if-ne v4, p3, :cond_3

    :goto_3
    const-string v2, "Inconsistent binding flags provided: got %d, expected %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, v0, LX/2XT;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v2, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_3

    .line 376056
    :cond_4
    new-instance v0, LX/2XW;

    const/4 v2, 0x1

    iget-object v1, v1, LX/2XT;->f:Landroid/os/IBinder;

    invoke-direct {v0, v2, v1}, LX/2XW;-><init>(ZLandroid/os/IBinder;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 376057
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/content/ServiceConnection;)V
    .locals 3

    .prologue
    .line 376058
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/29F;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 376059
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 376060
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2XT;

    .line 376061
    iget-object v2, v0, LX/2XT;->c:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v2

    .line 376062
    if-eqz v2, :cond_0

    .line 376063
    iget-object v2, v0, LX/2XT;->c:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 376064
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 376065
    iget-object v2, p0, LX/29F;->a:LX/1MH;

    iget-object v0, v0, LX/2XT;->b:LX/2XU;

    invoke-virtual {v2, v0}, LX/1MH;->a(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 376066
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 376067
    :cond_1
    monitor-exit p0

    return-void
.end method
