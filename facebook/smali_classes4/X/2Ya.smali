.class public LX/2Ya;
.super LX/2ZH;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Ww;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Uh;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Yb;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/init/MobileConfigInit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 421084
    const-class v0, LX/2Ya;

    sput-object v0, LX/2Ya;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/0Uh;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p3    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0Ww;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "LX/2Yb;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/init/MobileConfigInit;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 421073
    invoke-direct {p0}, LX/2ZH;-><init>()V

    .line 421074
    iput-object p1, p0, LX/2Ya;->b:LX/0Or;

    .line 421075
    iput-object p2, p0, LX/2Ya;->c:LX/0Or;

    .line 421076
    iput-object p3, p0, LX/2Ya;->d:LX/0Uh;

    .line 421077
    iput-object p4, p0, LX/2Ya;->f:LX/0Or;

    .line 421078
    iput-object p5, p0, LX/2Ya;->g:LX/0Or;

    .line 421079
    iput-object p6, p0, LX/2Ya;->i:LX/0Ot;

    .line 421080
    iput-object p7, p0, LX/2Ya;->h:LX/0Ot;

    .line 421081
    iput-object p8, p0, LX/2Ya;->e:LX/0Ot;

    .line 421082
    iput-object p9, p0, LX/2Ya;->j:LX/0Ot;

    .line 421083
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 421092
    :try_start_0
    iget-object v0, p0, LX/2Ya;->d:LX/0Uh;

    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 421093
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 421094
    if-nez v0, :cond_1

    .line 421095
    :cond_0
    :goto_0
    return-void

    .line 421096
    :cond_1
    iget-object v0, p0, LX/2Ya;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ww;

    .line 421097
    iget-object v1, p0, LX/2Ya;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/mobileconfig/init/MobileConfigInit;

    .line 421098
    iget-object v2, p0, LX/2Ya;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v3, p0, LX/2Ya;->f:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/tigon/iface/TigonServiceHolder;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->a(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/tigon/iface/TigonServiceHolder;Z)V

    .line 421099
    iget-object v1, p0, LX/2Ya;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0W3;

    .line 421100
    invoke-virtual {v1}, LX/0W3;->c()V

    .line 421101
    iget-object v1, p0, LX/2Ya;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;

    invoke-virtual {v0, v1}, LX/0Ww;->registerConfigChangeListener(Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;)Z

    .line 421102
    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, LX/0Ww;->tryUpdateConfigsSynchronously(I)Z

    move-result v0

    .line 421103
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 421104
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 421105
    instance-of v0, v1, Ljava/io/IOException;

    if-nez v0, :cond_0

    .line 421106
    iget-object v0, p0, LX/2Ya;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, LX/2Ya;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 421085
    iget-object v0, p0, LX/2Ya;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ww;

    .line 421086
    iget-object v1, p0, LX/2Ya;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/mobileconfig/init/MobileConfigInit;

    .line 421087
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, LX/0Ww;->deleteOldUserData(I)V

    .line 421088
    invoke-virtual {v1}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->a()V

    .line 421089
    iget-object v0, p0, LX/2Ya;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    .line 421090
    invoke-virtual {v0}, LX/0W3;->c()V

    .line 421091
    return-void
.end method
