.class public LX/360;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/366;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 497798
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/360;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 497799
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 497800
    iput-object p1, p0, LX/360;->b:LX/0Ot;

    .line 497801
    return-void
.end method

.method public static a(LX/0QB;)LX/360;
    .locals 4

    .prologue
    .line 497802
    const-class v1, LX/360;

    monitor-enter v1

    .line 497803
    :try_start_0
    sget-object v0, LX/360;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 497804
    sput-object v2, LX/360;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 497805
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497806
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 497807
    new-instance v3, LX/360;

    const/16 p0, 0x7f7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/360;-><init>(LX/0Ot;)V

    .line 497808
    move-object v0, v3

    .line 497809
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 497810
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/360;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 497811
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 497812
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 10

    .prologue
    .line 497813
    check-cast p2, LX/365;

    .line 497814
    iget-object v0, p0, LX/360;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;

    iget v2, p2, LX/365;->f:I

    iget-object v3, p2, LX/365;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/365;->b:LX/26M;

    iget-object v5, p2, LX/365;->c:LX/1bf;

    iget v6, p2, LX/365;->g:I

    iget v7, p2, LX/365;->h:I

    iget-object v8, p2, LX/365;->i:LX/1Pm;

    iget-object v9, p2, LX/365;->d:LX/1aZ;

    move-object v1, p1

    invoke-virtual/range {v0 .. v9}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->a(Landroid/view/View;ILcom/facebook/feed/rows/core/props/FeedProps;LX/26M;LX/1bf;IILX/1Pm;LX/1aZ;)V

    .line 497815
    return-void
.end method

.method private b(Landroid/view/View;LX/1X1;)V
    .locals 7

    .prologue
    .line 497816
    check-cast p2, LX/365;

    .line 497817
    iget-object v0, p0, LX/360;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;

    iget v2, p2, LX/365;->f:I

    iget-object v3, p2, LX/365;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/365;->c:LX/1bf;

    iget-object v5, p2, LX/365;->b:LX/26M;

    iget-object v6, p2, LX/365;->d:LX/1aZ;

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->a(Landroid/view/View;ILcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;LX/26M;LX/1aZ;)V

    .line 497818
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 497819
    check-cast p2, LX/365;

    .line 497820
    iget-object v0, p0, LX/360;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;

    iget-object v2, p2, LX/365;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/365;->b:LX/26M;

    iget-object v4, p2, LX/365;->c:LX/1bf;

    iget-object v5, p2, LX/365;->d:LX/1aZ;

    iget-object v6, p2, LX/365;->e:Landroid/graphics/PointF;

    iget v7, p2, LX/365;->f:I

    iget v8, p2, LX/365;->g:I

    iget v9, p2, LX/365;->h:I

    iget-object v10, p2, LX/365;->i:LX/1Pm;

    iget-object v11, p2, LX/365;->j:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    iget-boolean v12, p2, LX/365;->k:Z

    move-object v1, p1

    invoke-virtual/range {v0 .. v12}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/26M;LX/1bf;LX/1aZ;Landroid/graphics/PointF;IIILX/1Pm;Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;Z)LX/1Dg;

    move-result-object v0

    .line 497821
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 497822
    invoke-static {}, LX/1dS;->b()V

    .line 497823
    iget v0, p1, LX/1dQ;->b:I

    .line 497824
    sparse-switch v0, :sswitch_data_0

    .line 497825
    :goto_0
    return-object v2

    .line 497826
    :sswitch_0
    check-cast p2, LX/Aj8;

    .line 497827
    iget-object v0, p2, LX/Aj8;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/360;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    .line 497828
    :sswitch_1
    check-cast p2, LX/Aj8;

    .line 497829
    iget-object v0, p2, LX/Aj8;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/360;->b(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    .line 497830
    :sswitch_2
    check-cast p2, LX/Aj8;

    .line 497831
    iget-object v0, p2, LX/Aj8;->a:Landroid/view/View;

    .line 497832
    iget-object p1, p0, LX/360;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;

    .line 497833
    iget-object p0, p1, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->b:LX/26D;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/26D;->a(Landroid/content/Context;)V

    .line 497834
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2470a0af -> :sswitch_2
        -0x19b97078 -> :sswitch_1
        0x1f7564dd -> :sswitch_0
    .end sparse-switch
.end method

.method public final c(LX/1De;)LX/366;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 497835
    new-instance v1, LX/365;

    invoke-direct {v1, p0}, LX/365;-><init>(LX/360;)V

    .line 497836
    sget-object v2, LX/360;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/366;

    .line 497837
    if-nez v2, :cond_0

    .line 497838
    new-instance v2, LX/366;

    invoke-direct {v2}, LX/366;-><init>()V

    .line 497839
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/366;->a$redex0(LX/366;LX/1De;IILX/365;)V

    .line 497840
    move-object v1, v2

    .line 497841
    move-object v0, v1

    .line 497842
    return-object v0
.end method
