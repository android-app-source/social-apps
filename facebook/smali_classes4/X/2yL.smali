.class public LX/2yL;
.super Landroid/util/LruCache;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Short;",
        ">;"
    }
.end annotation


# instance fields
.field private a:S


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481649
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Landroid/util/LruCache;-><init>(I)V

    .line 481650
    const/4 v0, 0x0

    iput-short v0, p0, LX/2yL;->a:S

    .line 481651
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)S
    .locals 2

    .prologue
    .line 481652
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/2yL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Short;

    .line 481653
    if-nez v0, :cond_0

    .line 481654
    iget-short v0, p0, LX/2yL;->a:S

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    .line 481655
    iget-short v1, p0, LX/2yL;->a:S

    add-int/lit8 v1, v1, 0x1

    int-to-short v1, v1

    iput-short v1, p0, LX/2yL;->a:S

    .line 481656
    invoke-virtual {p0, p1, v0}, LX/2yL;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481657
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 481658
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 481659
    check-cast p3, Ljava/lang/Short;

    .line 481660
    invoke-virtual {p3}, Ljava/lang/Short;->shortValue()S

    move-result v0

    iput-short v0, p0, LX/2yL;->a:S

    .line 481661
    return-void
.end method
