.class public LX/30e;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final n:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Sh;

.field private final b:LX/30d;

.field private final c:LX/0SG;

.field private final d:LX/2do;

.field private final e:LX/30f;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/EjK;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/EjL;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/F7U;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/F8b;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:LX/2Ir;

.field private k:J

.field public l:I

.field public m:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 485087
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/30e;->n:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/30d;LX/0SG;LX/2do;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 485073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 485074
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/30e;->f:Ljava/util/Map;

    .line 485075
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/30e;->g:Ljava/util/Map;

    .line 485076
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/30e;->k:J

    .line 485077
    iput v2, p0, LX/30e;->l:I

    .line 485078
    iput v2, p0, LX/30e;->m:I

    .line 485079
    iput-object p1, p0, LX/30e;->a:LX/0Sh;

    .line 485080
    iput-object p2, p0, LX/30e;->b:LX/30d;

    .line 485081
    iput-object p3, p0, LX/30e;->c:LX/0SG;

    .line 485082
    iput-object p4, p0, LX/30e;->d:LX/2do;

    .line 485083
    new-instance v0, LX/30f;

    invoke-direct {v0, p0}, LX/30f;-><init>(LX/30e;)V

    iput-object v0, p0, LX/30e;->e:LX/30f;

    .line 485084
    iget-object v0, p0, LX/30e;->d:LX/2do;

    iget-object v1, p0, LX/30e;->e:LX/30f;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 485085
    sget-object v0, LX/2Ir;->DEFAULT:LX/2Ir;

    iput-object v0, p0, LX/30e;->j:LX/2Ir;

    .line 485086
    return-void
.end method

.method public static a(LX/0QB;)LX/30e;
    .locals 10

    .prologue
    .line 485044
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 485045
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 485046
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 485047
    if-nez v1, :cond_0

    .line 485048
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 485049
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 485050
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 485051
    sget-object v1, LX/30e;->n:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 485052
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 485053
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 485054
    :cond_1
    if-nez v1, :cond_4

    .line 485055
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 485056
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 485057
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 485058
    new-instance p0, LX/30e;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {v0}, LX/30d;->b(LX/0QB;)LX/30d;

    move-result-object v7

    check-cast v7, LX/30d;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v9

    check-cast v9, LX/2do;

    invoke-direct {p0, v1, v7, v8, v9}, LX/30e;-><init>(LX/0Sh;LX/30d;LX/0SG;LX/2do;)V

    .line 485059
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 485060
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 485061
    if-nez v1, :cond_2

    .line 485062
    sget-object v0, LX/30e;->n:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30e;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 485063
    :goto_1
    if-eqz v0, :cond_3

    .line 485064
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 485065
    :goto_3
    check-cast v0, LX/30e;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 485066
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 485067
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 485068
    :catchall_1
    move-exception v0

    .line 485069
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 485070
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 485071
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 485072
    :cond_2
    :try_start_8
    sget-object v0, LX/30e;->n:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30e;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private declared-synchronized d(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/EjK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 485035
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 485036
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EjK;

    .line 485037
    iget-object v3, p0, LX/30e;->f:Ljava/util/Map;

    iget-wide v4, v0, LX/EjK;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 485038
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 485039
    iget-object v3, p0, LX/30e;->f:Ljava/util/Map;

    iget-wide v4, v0, LX/EjK;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 485040
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 485041
    :cond_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 485042
    iget-object v0, p0, LX/30e;->a:LX/0Sh;

    new-instance v2, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$4;

    invoke-direct {v2, p0, v1}, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$4;-><init>(LX/30e;Ljava/util/List;)V

    invoke-virtual {v0, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 485043
    :cond_2
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized e(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/EjL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 485026
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 485027
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EjL;

    .line 485028
    iget-object v3, p0, LX/30e;->g:Ljava/util/Map;

    iget-wide v4, v0, LX/EjL;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 485029
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 485030
    iget-object v3, p0, LX/30e;->g:Ljava/util/Map;

    iget-wide v4, v0, LX/EjL;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 485031
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 485032
    :cond_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 485033
    iget-object v0, p0, LX/30e;->a:LX/0Sh;

    new-instance v2, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$5;

    invoke-direct {v2, p0, v1}, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$5;-><init>(LX/30e;Ljava/util/List;)V

    invoke-virtual {v0, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 485034
    :cond_2
    monitor-exit p0

    return-void
.end method

.method private static declared-synchronized j(LX/30e;)V
    .locals 1

    .prologue
    .line 485020
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/30e;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 485021
    iget-object v0, p0, LX/30e;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 485022
    const/4 v0, 0x0

    iput v0, p0, LX/30e;->m:I

    .line 485023
    const/4 v0, 0x0

    iput v0, p0, LX/30e;->l:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 485024
    monitor-exit p0

    return-void

    .line 485025
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 485017
    monitor-enter p0

    :try_start_0
    iput p1, p0, LX/30e;->l:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 485018
    monitor-exit p0

    return-void

    .line 485019
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/3Sb;)V
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "addInvitableContactsOneBatch"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 485009
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, LX/3Sb;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 485010
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 485011
    :cond_1
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, LX/3Sb;->c()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 485012
    invoke-interface {p1}, LX/3Sb;->b()LX/2sN;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 485013
    const/4 v4, 0x2

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 485014
    new-instance v4, LX/EjL;

    invoke-direct {v4, v3, v2}, LX/EjL;-><init>(LX/15i;I)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 485015
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 485016
    :cond_3
    :try_start_2
    invoke-direct {p0, v0}, LX/30e;->e(Ljava/util/List;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$FriendableContactsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 484965
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 484966
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 484967
    :cond_1
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 484968
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$FriendableContactsModel;

    .line 484969
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$FriendableContactsModel;->j()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$FriendableContactsModel;->l()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 484970
    new-instance v3, LX/EjK;

    invoke-direct {v3, v0}, LX/EjK;-><init>(Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$FriendableContactsModel;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 484971
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 484972
    :cond_3
    :try_start_2
    invoke-direct {p0, v1}, LX/30e;->d(Ljava/util/List;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 485008
    iget-object v0, p0, LX/30e;->j:LX/2Ir;

    sget-object v1, LX/2Ir;->FINISHED:LX/2Ir;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/30e;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/30e;->k:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x927c0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized b(I)V
    .locals 2

    .prologue
    .line 485004
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/30e;->m:I

    add-int/2addr v0, p1

    iput v0, p0, LX/30e;->m:I

    .line 485005
    iget-object v0, p0, LX/30e;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$1;

    invoke-direct {v1, p0}, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$1;-><init>(LX/30e;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 485006
    monitor-exit p0

    return-void

    .line 485007
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 484996
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 484997
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 484998
    :cond_1
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 484999
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;

    .line 485000
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;->j()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;->l()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 485001
    new-instance v3, LX/EjK;

    invoke-direct {v3, v0}, LX/EjK;-><init>(Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 485002
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 485003
    :cond_3
    :try_start_2
    invoke-direct {p0, v1}, LX/30e;->d(Ljava/util/List;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 484995
    iget-object v0, p0, LX/30e;->j:LX/2Ir;

    sget-object v1, LX/2Ir;->UPLOADING:LX/2Ir;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 484991
    invoke-static {p0}, LX/30e;->j(LX/30e;)V

    .line 484992
    iget-object v0, p0, LX/30e;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/30e;->k:J

    .line 484993
    sget-object v0, LX/2Ir;->UPLOADING:LX/2Ir;

    iput-object v0, p0, LX/30e;->j:LX/2Ir;

    .line 484994
    return-void
.end method

.method public final declared-synchronized c(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$InvitableContactsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 484983
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 484984
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 484985
    :cond_1
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 484986
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$InvitableContactsModel;

    .line 484987
    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$InvitableContactsModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 484988
    new-instance v3, LX/EjL;

    invoke-direct {v3, v0}, LX/EjL;-><init>(Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$InvitableContactsModel;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 484989
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 484990
    :cond_3
    :try_start_2
    invoke-direct {p0, v1}, LX/30e;->e(Ljava/util/List;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized f()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/EjK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 484982
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/30e;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/EjL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 484981
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/30e;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()V
    .locals 8

    .prologue
    .line 484976
    sget-object v0, LX/2Ir;->FINISHED:LX/2Ir;

    iput-object v0, p0, LX/30e;->j:LX/2Ir;

    .line 484977
    iget-object v0, p0, LX/30e;->b:LX/30d;

    iget-object v1, p0, LX/30e;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    iget-object v2, p0, LX/30e;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    iget-object v3, p0, LX/30e;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, p0, LX/30e;->k:J

    sub-long/2addr v4, v6

    .line 484978
    iget-object v3, v0, LX/30d;->a:LX/0Zb;

    sget-object v6, LX/Ejs;->CCU_FRIENDABLE_INVITABLE_CACHE:LX/Ejs;

    invoke-virtual {v6}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/30d;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "friendable_count"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "invitable_count"

    invoke-virtual {v6, v7, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "time_spent"

    invoke-virtual {v6, v7, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v3, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 484979
    iget-object v0, p0, LX/30e;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$2;

    invoke-direct {v1, p0}, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$2;-><init>(LX/30e;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 484980
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 484973
    sget-object v0, LX/2Ir;->FAILED:LX/2Ir;

    iput-object v0, p0, LX/30e;->j:LX/2Ir;

    .line 484974
    iget-object v0, p0, LX/30e;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$3;

    invoke-direct {v1, p0}, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$3;-><init>(LX/30e;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 484975
    return-void
.end method
