.class public final LX/28J;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/2JA;


# direct methods
.method public constructor <init>(LX/2JA;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 373852
    iput-object p1, p0, LX/28J;->b:LX/2JA;

    iput-object p2, p0, LX/28J;->a:Ljava/util/List;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 373877
    iget-object v1, p0, LX/28J;->b:LX/2JA;

    monitor-enter v1

    .line 373878
    :try_start_0
    iget-object v0, p0, LX/28J;->b:LX/2JA;

    iget-object v0, v0, LX/2JA;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27j;

    .line 373879
    iget-object p0, v0, LX/27j;->a:LX/0Zb;

    const-string p1, "language_switcher_login_suggestions_failed"

    invoke-static {p1}, LX/27j;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 373880
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 373853
    check-cast p1, LX/0Px;

    .line 373854
    iget-object v1, p0, LX/28J;->b:LX/2JA;

    monitor-enter v1

    .line 373855
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_1

    .line 373856
    :cond_0
    iget-object v0, p0, LX/28J;->b:LX/2JA;

    iget-object v0, v0, LX/2JA;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27j;

    .line 373857
    iget-object v2, v0, LX/27j;->a:LX/0Zb;

    const-string p0, "language_switcher_login_no_suggestions"

    invoke-static {p0}, LX/27j;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v2, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 373858
    :goto_0
    monitor-exit v1

    return-void

    .line 373859
    :cond_1
    iget-object v0, p0, LX/28J;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 373860
    iget-object v0, p0, LX/28J;->b:LX/2JA;

    iget-object v2, p0, LX/28J;->a:Ljava/util/List;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 373861
    iput-object v2, v0, LX/2JA;->g:LX/0Px;

    .line 373862
    iget-object v0, p0, LX/28J;->b:LX/2JA;

    iget-object v0, v0, LX/2JA;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27j;

    iget-object v2, p0, LX/28J;->b:LX/2JA;

    iget-object v2, v2, LX/2JA;->g:LX/0Px;

    .line 373863
    const-string v3, "language_switcher_login_suggestions_fetched"

    invoke-static {v3}, LX/27j;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 373864
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 373865
    new-instance v5, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v3}, LX/162;-><init>(LX/0mC;)V

    .line 373866
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 373867
    invoke-virtual {v5, v3}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_1

    .line 373868
    :cond_2
    const-string v3, "locale_list"

    invoke-virtual {v4, v3, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v5, "locale_list_count"

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v3, v5, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 373869
    :cond_3
    iget-object v3, v0, LX/27j;->a:LX/0Zb;

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 373870
    iget-object v0, p0, LX/28J;->b:LX/2JA;

    iget-object v2, p0, LX/28J;->a:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373871
    :try_start_1
    iget-object v3, v0, LX/2JA;->d:LX/0lB;

    invoke-virtual {v3, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 373872
    iget-object v4, v0, LX/2JA;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/288;->b:LX/0Tn;

    invoke-interface {v4, v5, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V
    :try_end_1
    .catch LX/28F; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 373873
    :goto_2
    :try_start_2
    goto :goto_0

    .line 373874
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 373875
    :catch_0
    move-exception v3

    .line 373876
    iget-object v4, v0, LX/2JA;->f:LX/03V;

    const-string v5, "save_suggested_locales_failed"

    const-string p0, "Couldn\'t save suggested locales in prefkeys."

    invoke-virtual {v4, v5, p0, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method
