.class public final LX/23y;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/23t;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Landroid/graphics/Rect;

.field public d:Z

.field public e:LX/Byw;

.field public final synthetic f:LX/23t;


# direct methods
.method public constructor <init>(LX/23t;)V
    .locals 1

    .prologue
    .line 366285
    iput-object p1, p0, LX/23y;->f:LX/23t;

    .line 366286
    move-object v0, p1

    .line 366287
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 366288
    sget-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->a:Landroid/graphics/Rect;

    iput-object v0, p0, LX/23y;->c:Landroid/graphics/Rect;

    .line 366289
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 366290
    const-string v0, "CollageAttachmentComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/23t;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 366291
    check-cast p1, LX/23y;

    .line 366292
    iget-object v0, p1, LX/23y;->e:LX/Byw;

    iput-object v0, p0, LX/23y;->e:LX/Byw;

    .line 366293
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 366294
    if-ne p0, p1, :cond_1

    .line 366295
    :cond_0
    :goto_0
    return v0

    .line 366296
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 366297
    goto :goto_0

    .line 366298
    :cond_3
    check-cast p1, LX/23y;

    .line 366299
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 366300
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 366301
    if-eq v2, v3, :cond_0

    .line 366302
    iget-object v2, p0, LX/23y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/23y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/23y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 366303
    goto :goto_0

    .line 366304
    :cond_5
    iget-object v2, p1, LX/23y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 366305
    :cond_6
    iget-object v2, p0, LX/23y;->b:LX/1Pe;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/23y;->b:LX/1Pe;

    iget-object v3, p1, LX/23y;->b:LX/1Pe;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 366306
    goto :goto_0

    .line 366307
    :cond_8
    iget-object v2, p1, LX/23y;->b:LX/1Pe;

    if-nez v2, :cond_7

    .line 366308
    :cond_9
    iget-object v2, p0, LX/23y;->c:Landroid/graphics/Rect;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/23y;->c:Landroid/graphics/Rect;

    iget-object v3, p1, LX/23y;->c:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 366309
    goto :goto_0

    .line 366310
    :cond_b
    iget-object v2, p1, LX/23y;->c:Landroid/graphics/Rect;

    if-nez v2, :cond_a

    .line 366311
    :cond_c
    iget-boolean v2, p0, LX/23y;->d:Z

    iget-boolean v3, p1, LX/23y;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 366312
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 366313
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/23y;

    .line 366314
    const/4 v1, 0x0

    iput-object v1, v0, LX/23y;->e:LX/Byw;

    .line 366315
    return-object v0
.end method
