.class public LX/29x;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private a:Lcom/facebook/katana/service/AppSession;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 376801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376802
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/29x;->b:Z

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 376803
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/29x;->b(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    .line 376804
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    move-object v1, v1

    .line 376805
    sget-object v2, LX/2A1;->STATUS_LOGGED_OUT:LX/2A1;

    if-eq v1, v2, :cond_0

    if-eqz p2, :cond_1

    invoke-virtual {p0, p1}, LX/29x;->a(Landroid/content/Context;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 376806
    :cond_0
    const/4 v0, 0x0

    .line 376807
    :cond_1
    monitor-exit p0

    return-object v0

    .line 376808
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 376809
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 376810
    sget-boolean v2, LX/007;->i:Z

    move v2, v2

    .line 376811
    if-eqz v2, :cond_1

    iget-object v2, p0, LX/29x;->a:Lcom/facebook/katana/service/AppSession;

    invoke-virtual {v2}, Lcom/facebook/katana/service/AppSession;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 376812
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    const/16 v3, 0x18

    invoke-virtual {v2, v3}, LX/0Uh;->a(I)LX/03R;

    move-result-object v2

    move-object v0, v2

    .line 376813
    check-cast v0, LX/03R;

    .line 376814
    sget-object v2, LX/03R;->NO:LX/03R;

    if-ne v0, v2, :cond_1

    .line 376815
    iget-boolean v0, p0, LX/29x;->b:Z

    if-eqz v0, :cond_0

    .line 376816
    iput-boolean v1, p0, LX/29x;->b:Z

    .line 376817
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "This beta build is only enabled for employees and authorized users."

    invoke-static {v0, v2}, LX/0kL;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    :cond_0
    move v0, v1

    .line 376818
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized b(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;
    .locals 2

    .prologue
    .line 376819
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/29x;->a:Lcom/facebook/katana/service/AppSession;

    if-nez v0, :cond_0

    .line 376820
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v0

    check-cast v0, LX/0WJ;

    .line 376821
    new-instance v1, Lcom/facebook/katana/service/AppSession;

    invoke-direct {v1, p1}, Lcom/facebook/katana/service/AppSession;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/29x;->a:Lcom/facebook/katana/service/AppSession;

    .line 376822
    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376823
    const-string v0, "AppSession.doLogin"

    const v1, 0x5375573c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 376824
    :try_start_1
    iget-object v0, p0, LX/29x;->a:Lcom/facebook/katana/service/AppSession;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/facebook/katana/service/AppSession;->c(Landroid/content/Context;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 376825
    const v0, 0x5fa41960

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V

    .line 376826
    :cond_0
    iget-object v0, p0, LX/29x;->a:Lcom/facebook/katana/service/AppSession;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-object v0

    .line 376827
    :catchall_0
    move-exception v0

    const v1, 0x2e8dad8

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 376828
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
