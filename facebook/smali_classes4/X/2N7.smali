.class public LX/2N7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2N8;

.field private final b:LX/2N9;


# direct methods
.method public constructor <init>(LX/2N8;LX/2N9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 398557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398558
    iput-object p1, p0, LX/2N7;->a:LX/2N8;

    .line 398559
    iput-object p2, p0, LX/2N7;->b:LX/2N9;

    .line 398560
    return-void
.end method

.method public static a(LX/0QB;)LX/2N7;
    .locals 1

    .prologue
    .line 398556
    invoke-static {p0}, LX/2N7;->b(LX/0QB;)LX/2N7;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2N7;LX/0lF;)Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .locals 7

    .prologue
    .line 398550
    const-string v0, "user_key"

    invoke-virtual {p1, v0}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->a(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    .line 398551
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    const-string v2, "name"

    invoke-virtual {p1, v2}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "email"

    invoke-virtual {p1, v3}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "phone"

    invoke-virtual {p1, v4}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "smsParticipantFbid"

    invoke-virtual {p1, v5}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "is_commerce"

    invoke-virtual {p1, v6}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->g(LX/0lF;)Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 398552
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 398553
    iget-object v2, p0, LX/2N7;->b:LX/2N9;

    const-string v3, "DbParticipantsSerialization.deserializeParticipantInfoInternal"

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v3, v1}, LX/2N9;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 398554
    :cond_0
    return-object v0

    .line 398555
    :cond_1
    const-string v1, "null_key"

    goto :goto_0
.end method

.method public static final a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 398545
    if-nez p0, :cond_0

    .line 398546
    const/4 v0, 0x0

    .line 398547
    :goto_0
    return-object v0

    .line 398548
    :cond_0
    invoke-static {p0}, LX/2N7;->b(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/0lF;

    move-result-object v0

    .line 398549
    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 398541
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 398542
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 398543
    invoke-static {v0}, LX/2N7;->b(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/0lF;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 398544
    :cond_0
    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/0lF;
    .locals 3

    .prologue
    .line 398561
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 398562
    const-string v1, "email"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398563
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    if-eqz v1, :cond_0

    .line 398564
    const-string v1, "user_key"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398565
    :cond_0
    const-string v1, "name"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398566
    return-object v0
.end method

.method public static b(LX/0QB;)LX/2N7;
    .locals 3

    .prologue
    .line 398539
    new-instance v2, LX/2N7;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v0

    check-cast v0, LX/2N8;

    invoke-static {p0}, LX/2N9;->b(LX/0QB;)LX/2N9;

    move-result-object v1

    check-cast v1, LX/2N9;

    invoke-direct {v2, v0, v1}, LX/2N7;-><init>(LX/2N8;LX/2N9;)V

    .line 398540
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 398534
    if-nez p1, :cond_0

    .line 398535
    const/4 v0, 0x0

    .line 398536
    :goto_0
    return-object v0

    .line 398537
    :cond_0
    iget-object v0, p0, LX/2N7;->a:LX/2N8;

    invoke-virtual {v0, p1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 398538
    invoke-static {p0, v0}, LX/2N7;->a(LX/2N7;LX/0lF;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 398529
    iget-object v0, p0, LX/2N7;->a:LX/2N8;

    invoke-virtual {v0, p1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 398530
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 398531
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 398532
    invoke-static {p0, v0}, LX/2N7;->a(LX/2N7;LX/0lF;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 398533
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation

    .prologue
    .line 398508
    iget-object v0, p0, LX/2N7;->a:LX/2N8;

    invoke-virtual {v0, p1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 398509
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 398510
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 398511
    invoke-static {p0, v0}, LX/2N7;->a(LX/2N7;LX/0lF;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v3

    .line 398512
    const-string v4, "lastReadReceiptTimestampMs"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->c(LX/0lF;)J

    move-result-wide v5

    .line 398513
    new-instance v4, LX/6fz;

    invoke-direct {v4}, LX/6fz;-><init>()V

    .line 398514
    iput-object v3, v4, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 398515
    move-object v3, v4

    .line 398516
    iput-wide v5, v3, LX/6fz;->b:J

    .line 398517
    move-object v3, v3

    .line 398518
    iput-wide v5, v3, LX/6fz;->c:J

    .line 398519
    move-object v3, v3

    .line 398520
    const-string v4, "lastDeliveredReceiptMsgId"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 398521
    iput-object v4, v3, LX/6fz;->d:Ljava/lang/String;

    .line 398522
    move-object v3, v3

    .line 398523
    const-string v4, "lastDeliveredReceiptTimestampMs"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->c(LX/0lF;)J

    move-result-wide v5

    .line 398524
    iput-wide v5, v3, LX/6fz;->e:J

    .line 398525
    move-object v3, v3

    .line 398526
    invoke-virtual {v3}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v3

    move-object v0, v3

    .line 398527
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 398528
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
