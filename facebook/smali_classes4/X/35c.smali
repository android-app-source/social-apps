.class public LX/35c;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/35c;


# instance fields
.field public final b:I

.field public final c:LX/35d;

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:J


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/16 v6, 0xa

    .line 497299
    new-instance v0, LX/35c;

    const/16 v1, 0x64

    sget-object v2, LX/35d;->MINIMAL:LX/35d;

    const/4 v3, 0x1

    const/16 v4, 0x2710

    const/4 v5, 0x3

    const-wide/32 v8, 0x5265c00

    move v7, v6

    invoke-direct/range {v0 .. v9}, LX/35c;-><init>(ILX/35d;IIIIIJ)V

    sput-object v0, LX/35c;->a:LX/35c;

    return-void
.end method

.method private constructor <init>(ILX/35d;IIIIIJ)V
    .locals 0

    .prologue
    .line 497300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497301
    iput p1, p0, LX/35c;->b:I

    .line 497302
    iput-object p2, p0, LX/35c;->c:LX/35d;

    .line 497303
    iput p3, p0, LX/35c;->d:I

    .line 497304
    iput p4, p0, LX/35c;->e:I

    .line 497305
    iput p5, p0, LX/35c;->f:I

    .line 497306
    iput p6, p0, LX/35c;->g:I

    .line 497307
    iput p7, p0, LX/35c;->h:I

    .line 497308
    iput-wide p8, p0, LX/35c;->i:J

    .line 497309
    return-void
.end method

.method public static a(LX/15i;I)LX/35c;
    .locals 10
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromSettingModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 497310
    new-instance v0, LX/35c;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, LX/15i;->j(II)I

    move-result v1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    invoke-virtual {p0, p1, v4, v2, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    if-nez v2, :cond_0

    sget-object v2, LX/35d;->MINIMAL:LX/35d;

    :goto_0
    const/4 v3, 0x2

    invoke-virtual {p0, p1, v3}, LX/15i;->j(II)I

    move-result v3

    const/4 v4, 0x3

    invoke-virtual {p0, p1, v4}, LX/15i;->j(II)I

    move-result v4

    const/4 v5, 0x6

    invoke-virtual {p0, p1, v5}, LX/15i;->j(II)I

    move-result v5

    const/4 v6, 0x4

    invoke-virtual {p0, p1, v6}, LX/15i;->j(II)I

    move-result v6

    const/4 v7, 0x5

    invoke-virtual {p0, p1, v7}, LX/15i;->j(II)I

    move-result v7

    const/4 v8, 0x7

    invoke-virtual {p0, p1, v8}, LX/15i;->j(II)I

    move-result v8

    int-to-long v8, v8

    invoke-direct/range {v0 .. v9}, LX/35c;-><init>(ILX/35d;IIIIIJ)V

    return-object v0

    :cond_0
    const-class v2, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    invoke-virtual {p0, p1, v4, v2, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/35d;->valueOf(Ljava/lang/String;)LX/35d;

    move-result-object v2

    goto :goto_0
.end method
