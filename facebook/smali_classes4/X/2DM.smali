.class public LX/2DM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2DN;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile t:LX/2DM;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2F0;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2F4;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2G0;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2GZ;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gd;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2He;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Hf;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2IS;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2If;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/30J;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ig;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ij;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ik;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Vt;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Vw;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2W5;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0A9;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/09x;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2WB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2F0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2F4;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2G0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2GZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Gd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2He;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Hf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2IS;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2If;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/30J;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Ig;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Ij;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Ik;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Vt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Vw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2W5;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0A9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/09x;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2WB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384108
    iput-object p1, p0, LX/2DM;->a:LX/0Ot;

    .line 384109
    iput-object p2, p0, LX/2DM;->b:LX/0Ot;

    .line 384110
    iput-object p3, p0, LX/2DM;->c:LX/0Ot;

    .line 384111
    iput-object p4, p0, LX/2DM;->d:LX/0Ot;

    .line 384112
    iput-object p5, p0, LX/2DM;->e:LX/0Ot;

    .line 384113
    iput-object p6, p0, LX/2DM;->f:LX/0Ot;

    .line 384114
    iput-object p7, p0, LX/2DM;->g:LX/0Ot;

    .line 384115
    iput-object p8, p0, LX/2DM;->h:LX/0Ot;

    .line 384116
    iput-object p9, p0, LX/2DM;->i:LX/0Ot;

    .line 384117
    iput-object p10, p0, LX/2DM;->j:LX/0Ot;

    .line 384118
    iput-object p11, p0, LX/2DM;->k:LX/0Ot;

    .line 384119
    iput-object p12, p0, LX/2DM;->l:LX/0Ot;

    .line 384120
    iput-object p13, p0, LX/2DM;->m:LX/0Ot;

    .line 384121
    iput-object p14, p0, LX/2DM;->n:LX/0Ot;

    .line 384122
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2DM;->o:LX/0Ot;

    .line 384123
    move-object/from16 v0, p16

    iput-object v0, p0, LX/2DM;->p:LX/0Ot;

    .line 384124
    move-object/from16 v0, p17

    iput-object v0, p0, LX/2DM;->q:LX/0Ot;

    .line 384125
    move-object/from16 v0, p18

    iput-object v0, p0, LX/2DM;->r:LX/0Ot;

    .line 384126
    move-object/from16 v0, p19

    iput-object v0, p0, LX/2DM;->s:LX/0Ot;

    .line 384127
    return-void
.end method

.method public static a(LX/0QB;)LX/2DM;
    .locals 3

    .prologue
    .line 384097
    sget-object v0, LX/2DM;->t:LX/2DM;

    if-nez v0, :cond_1

    .line 384098
    const-class v1, LX/2DM;

    monitor-enter v1

    .line 384099
    :try_start_0
    sget-object v0, LX/2DM;->t:LX/2DM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 384100
    if-eqz v2, :cond_0

    .line 384101
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2DM;->b(LX/0QB;)LX/2DM;

    move-result-object v0

    sput-object v0, LX/2DM;->t:LX/2DM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384102
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 384103
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 384104
    :cond_1
    sget-object v0, LX/2DM;->t:LX/2DM;

    return-object v0

    .line 384105
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 384106
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/2DM;
    .locals 22

    .prologue
    .line 384128
    new-instance v2, LX/2DM;

    const/16 v3, 0xa2

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xa4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xa5

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xc1

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xc2

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xc3

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xc4

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x148

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x44a

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x53e

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xb85

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0xba7

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0xcbd

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0xcc6

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0xcd9

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0xf1e

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x12ec

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x12f0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x1357

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    invoke-direct/range {v2 .. v21}, LX/2DM;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 384129
    return-object v2
.end method


# virtual methods
.method public final a(LX/2CZ;LX/2Ew;Ljava/lang/String;J)V
    .locals 12

    .prologue
    .line 384077
    iget-object v5, p0, LX/2DM;->a:LX/0Ot;

    const-string v6, "DBSizePeriodicReporter-db_size_info"

    const-string v7, "DBSizePeriodicReporter"

    const-string v8, "db_size_info"

    const/4 v9, 0x0

    const-wide/32 v10, 0x5265c00

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384078
    iget-object v6, p0, LX/2DM;->b:LX/0Ot;

    const-string v7, "DeviceInfoPeriodicReporter-device_info"

    const-string v8, "DeviceInfoPeriodicReporter"

    const-string v9, "device_info"

    const/4 v10, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide/from16 v4, p4

    invoke-static/range {v1 .. v10}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 384079
    iget-object v6, p0, LX/2DM;->c:LX/0Ot;

    const-string v7, "DeviceStatusPeriodicReporter-device_status"

    const-string v8, "DeviceStatusPeriodicReporter"

    const-string v9, "device_status"

    const/4 v10, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide/from16 v4, p4

    invoke-static/range {v1 .. v10}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 384080
    iget-object v5, p0, LX/2DM;->d:LX/0Ot;

    const-string v6, "AppInstallationPeriodicReporter-app_installations"

    const-string v7, "AppInstallationPeriodicReporter"

    const-string v8, "app_installations"

    const/4 v9, 0x1

    const-wide/32 v10, 0x5265c00

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384081
    iget-object v5, p0, LX/2DM;->e:LX/0Ot;

    const-string v6, "PeriodicFeatureStatusReporter-features_status"

    const-string v7, "PeriodicFeatureStatusReporter"

    const-string v8, "features_status"

    const/4 v9, 0x1

    const-wide/32 v10, 0x2932e00

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384082
    iget-object v5, p0, LX/2DM;->f:LX/0Ot;

    const-string v6, "PistolFirePeriodicReporter-fbandroid_pistol_fire_crash"

    const-string v7, "PistolFirePeriodicReporter"

    const-string v8, "fbandroid_pistol_fire_crash"

    const/4 v9, 0x0

    const-wide/32 v10, 0x5265c00

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384083
    iget-object v6, p0, LX/2DM;->g:LX/0Ot;

    const-string v7, "ProcessStatusPeriodicReporter-process_status"

    const-string v8, "ProcessStatusPeriodicReporter"

    const-string v9, "process_status"

    const/4 v10, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide/from16 v4, p4

    invoke-static/range {v1 .. v10}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 384084
    iget-object v5, p0, LX/2DM;->h:LX/0Ot;

    const-string v6, "AssetDownloadPeriodicEventReporter-assetdownload_db_and_fs_stats"

    const-string v7, "AssetDownloadPeriodicEventReporter"

    const-string v8, "assetdownload_db_and_fs_stats"

    const/4 v9, 0x0

    const-wide/32 v10, 0x5265c00

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384085
    iget-object v5, p0, LX/2DM;->i:LX/0Ot;

    const-string v6, "ContactsUploadPeriodicReporter-contacts_upload_state"

    const-string v7, "ContactsUploadPeriodicReporter"

    const-string v8, "contacts_upload_state"

    const/4 v9, 0x1

    const-wide/32 v10, 0x5265c00

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384086
    iget-object v5, p0, LX/2DM;->j:LX/0Ot;

    const-string v6, "JSCPerfPeriodicReporter-jsc_perf_event"

    const-string v7, "JSCPerfPeriodicReporter"

    const-string v8, "jsc_perf_event"

    const/4 v9, 0x0

    const-wide/32 v10, 0x36ee80

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384087
    iget-object v5, p0, LX/2DM;->k:LX/0Ot;

    const-string v6, "ImagePushEfficiencyPeriodicReporter-android_image_push_fetch_efficiency"

    const-string v7, "ImagePushEfficiencyPeriodicReporter"

    const-string v8, "android_image_push_fetch_efficiency"

    const/4 v9, 0x1

    const-wide/32 v10, 0x5265c00

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384088
    iget-object v5, p0, LX/2DM;->l:LX/0Ot;

    const-string v6, "ImagePipelinePeriodicReporter-image_pipeline_counters"

    const-string v7, "ImagePipelinePeriodicReporter"

    const-string v8, "image_pipeline_counters"

    const/4 v9, 0x0

    const-wide/32 v10, 0xdbba00

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384089
    iget-object v5, p0, LX/2DM;->m:LX/0Ot;

    const-string v6, "OtherAppMemoryUsageReporter-other_app_memory_usage"

    const-string v7, "OtherAppMemoryUsageReporter"

    const-string v8, "other_app_memory_usage"

    const/4 v9, 0x0

    const-wide/32 v10, 0x5265c00

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384090
    iget-object v5, p0, LX/2DM;->n:LX/0Ot;

    const-string v6, "MultiAccountsPeriodicReporter-mswitch_accounts_state"

    const-string v7, "MultiAccountsPeriodicReporter"

    const-string v8, "mswitch_accounts_state"

    const/4 v9, 0x1

    const-wide/32 v10, 0x5265c00

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384091
    iget-object v5, p0, LX/2DM;->o:LX/0Ot;

    const-string v6, "AudioCache-media_cache_size"

    const-string v7, "AudioCache"

    const-string v8, "media_cache_size"

    const/4 v9, 0x0

    const-wide/32 v10, 0x36ee80

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384092
    iget-object v5, p0, LX/2DM;->p:LX/0Ot;

    const-string v6, "ImageFetchEfficiencyReporter-android_image_fetch_efficiency"

    const-string v7, "ImageFetchEfficiencyReporter"

    const-string v8, "android_image_fetch_efficiency"

    const/4 v9, 0x1

    const-wide/32 v10, 0x5265c00

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384093
    iget-object v5, p0, LX/2DM;->q:LX/0Ot;

    const-string v6, "DeviceVideoCapabilitiesPeriodicReporter-device_video_capabilities"

    const-string v7, "DeviceVideoCapabilitiesPeriodicReporter"

    const-string v8, "device_video_capabilities"

    const/4 v9, 0x0

    const-wide/32 v10, 0x240c8400

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384094
    iget-object v5, p0, LX/2DM;->r:LX/0Ot;

    const-string v6, "VideoCachePeriodicReporter-video_cache_counters"

    const-string v7, "VideoCachePeriodicReporter"

    const-string v8, "video_cache_counters"

    const/4 v9, 0x0

    const-wide/32 v10, 0x112a880

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384095
    iget-object v5, p0, LX/2DM;->s:LX/0Ot;

    const-string v6, "VideoPerformancePeriodicReporter-video_daily_data_usage"

    const-string v7, "VideoPerformancePeriodicReporter"

    const-string v8, "video_daily_data_usage"

    const/4 v9, 0x0

    const-wide/32 v10, 0x5265c00

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p4

    invoke-static/range {v0 .. v11}, LX/2Ex;->a(LX/2CZ;LX/2Ew;Ljava/lang/String;JLX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 384096
    return-void
.end method
