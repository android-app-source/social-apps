.class public LX/2PH;
.super LX/2PI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2PI",
        "<",
        "Lcom/facebook/messaging/tincan/outbound/TincanPreKeyUploadListener;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2PH;


# instance fields
.field private final c:LX/2PJ;

.field private final d:LX/0SF;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 406121
    const-class v0, LX/2PH;

    sput-object v0, LX/2PH;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2PJ;LX/0SF;LX/0Or;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2PJ;",
            "LX/0SF;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406122
    const/16 v0, 0xf

    invoke-direct {p0, v0}, LX/2PI;-><init>(I)V

    .line 406123
    iput-object p1, p0, LX/2PH;->c:LX/2PJ;

    .line 406124
    iput-object p2, p0, LX/2PH;->d:LX/0SF;

    .line 406125
    iput-object p3, p0, LX/2PH;->e:LX/0Or;

    .line 406126
    return-void
.end method

.method public static a(LX/0QB;)LX/2PH;
    .locals 6

    .prologue
    .line 406127
    sget-object v0, LX/2PH;->f:LX/2PH;

    if-nez v0, :cond_1

    .line 406128
    const-class v1, LX/2PH;

    monitor-enter v1

    .line 406129
    :try_start_0
    sget-object v0, LX/2PH;->f:LX/2PH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406130
    if-eqz v2, :cond_0

    .line 406131
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 406132
    new-instance v5, LX/2PH;

    invoke-static {v0}, LX/2PJ;->a(LX/0QB;)LX/2PJ;

    move-result-object v3

    check-cast v3, LX/2PJ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SF;

    const/16 p0, 0x15e8

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/2PH;-><init>(LX/2PJ;LX/0SF;LX/0Or;)V

    .line 406133
    move-object v0, v5

    .line 406134
    sput-object v0, LX/2PH;->f:LX/2PH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406135
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406136
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406137
    :cond_1
    sget-object v0, LX/2PH;->f:LX/2PH;

    return-object v0

    .line 406138
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406139
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 406140
    return-void
.end method

.method public final a(LX/Dph;)V
    .locals 3

    .prologue
    .line 406141
    if-nez p1, :cond_0

    .line 406142
    sget-object v0, LX/2PH;->b:Ljava/lang/Class;

    const-string v1, "Could not deserialise pre-key upload response"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 406143
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    .line 406144
    invoke-virtual {v0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->c()V

    goto :goto_0

    .line 406145
    :cond_0
    iget-object v0, p1, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_1

    .line 406146
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    .line 406147
    invoke-virtual {v0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a()V

    goto :goto_1

    .line 406148
    :cond_1
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    .line 406149
    invoke-virtual {v0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->b()V

    goto :goto_2

    .line 406150
    :cond_2
    return-void
.end method

.method public final declared-synchronized a([BLjava/util/List;LX/Ebk;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/util/List",
            "<",
            "LX/Ebg;",
            ">;",
            "LX/Ebk;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 406151
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/2PI;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 406152
    sget-object v0, LX/2PH;->b:Ljava/lang/Class;

    const-string v1, "Stored procedure sender not available to upload pre-keys"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406153
    const/4 v0, 0x0

    .line 406154
    :goto_0
    monitor-exit p0

    return v0

    .line 406155
    :cond_0
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 406156
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ebg;

    .line 406157
    new-instance v3, LX/DpU;

    invoke-virtual {v0}, LX/Ebg;->b()LX/Eau;

    move-result-object v4

    .line 406158
    iget-object v5, v4, LX/Eau;->a:LX/Eat;

    move-object v4, v5

    .line 406159
    invoke-virtual {v4}, LX/Eat;->a()[B

    move-result-object v4

    invoke-virtual {v0}, LX/Ebg;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, LX/DpU;-><init>([BLjava/lang/Integer;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 406160
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 406161
    :cond_1
    :try_start_2
    new-instance v0, LX/Dpf;

    new-instance v2, LX/DpU;

    invoke-virtual {p3}, LX/Ebk;->b()LX/Eau;

    move-result-object v3

    .line 406162
    iget-object v4, v3, LX/Eau;->a:LX/Eat;

    move-object v3, v4

    .line 406163
    invoke-virtual {v3}, LX/Eat;->a()[B

    move-result-object v3

    invoke-virtual {p3}, LX/Ebk;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/DpU;-><init>([BLjava/lang/Integer;)V

    invoke-virtual {p3}, LX/Ebk;->c()[B

    move-result-object v3

    invoke-direct {v0, v2, v3}, LX/Dpf;-><init>(LX/DpU;[B)V

    .line 406164
    new-instance v7, LX/DpS;

    invoke-direct {v7, v1, v0}, LX/DpS;-><init>(Ljava/util/List;LX/Dpf;)V

    .line 406165
    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, LX/DpM;

    iget-object v0, p0, LX/2PH;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v4, p0, LX/2PH;->c:LX/2PJ;

    invoke-virtual {v4}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, LX/DpM;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    iget-object v0, p0, LX/2PH;->d:LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v4

    const-wide/16 v8, 0x3e8

    mul-long/2addr v4, v8

    const/16 v6, 0xa

    .line 406166
    new-instance v0, LX/DpO;

    invoke-direct {v0}, LX/DpO;-><init>()V

    .line 406167
    invoke-static {v0, v7}, LX/DpO;->b(LX/DpO;LX/DpS;)V

    .line 406168
    move-object v7, v0

    .line 406169
    move-object v8, p1

    invoke-static/range {v1 .. v8}, LX/Dpm;->a(LX/Dpe;LX/DpM;LX/DpM;JILX/DpO;[B)LX/DpN;

    move-result-object v0

    .line 406170
    invoke-static {v0}, LX/Dpn;->a(LX/1u2;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2PI;->a([B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 406171
    const/4 v0, 0x1

    goto/16 :goto_0
.end method
