.class public LX/2Qx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field public a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 409312
    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "FNumber"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "ExposureTime"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ISOSpeedRatings"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "GPSAltitude"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "GPSAltitudeRef"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "FocalLength"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "GPSDateStamp"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "GPSTimeStamp"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "GPSProcessingMethod"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "DateTime"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Flash"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Orientation"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "GPSLatitude"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "GPSLatitudeRef"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "GPSLongitude"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "GPSLongitudeRef"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Make"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Model"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "WhiteBalance"

    aput-object v2, v0, v1

    sput-object v0, LX/2Qx;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409310
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2Qx;->a:Z

    .line 409311
    return-void
.end method

.method private static a(IIIIIZ)F
    .locals 4

    .prologue
    .line 409301
    if-eqz p5, :cond_2

    const/16 v0, 0x5a

    if-eq p2, v0, :cond_0

    const/16 v0, 0x10e

    if-ne p2, v0, :cond_2

    .line 409302
    :cond_0
    :goto_0
    if-gt p1, p3, :cond_1

    if-le p0, p4, :cond_3

    .line 409303
    :cond_1
    int-to-float v0, p1

    int-to-float v1, p3

    div-float/2addr v0, v1

    .line 409304
    int-to-float v1, p0

    int-to-float v2, p4

    div-float/2addr v1, v2

    .line 409305
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 409306
    :goto_1
    return v0

    :cond_2
    move v3, p1

    move p1, p0

    move p0, v3

    .line 409307
    goto :goto_0

    .line 409308
    :cond_3
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)I
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    const/4 v6, -0x1

    const/4 v3, 0x0

    .line 409290
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "media"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 409291
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v7, [Ljava/lang/String;

    const-string v1, "orientation"

    aput-object v1, v2, v4

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 409292
    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eq v0, v7, :cond_1

    .line 409293
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v6

    .line 409294
    :goto_0
    return v0

    .line 409295
    :cond_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 409296
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 409297
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 409298
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 409299
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Qx;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_3
    move v0, v6

    .line 409300
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2Qx;
    .locals 1

    .prologue
    .line 409287
    new-instance v0, LX/2Qx;

    invoke-direct {v0}, LX/2Qx;-><init>()V

    .line 409288
    move-object v0, v0

    .line 409289
    return-object v0
.end method

.method public static a(Ljava/lang/String;)LX/434;
    .locals 3

    .prologue
    .line 409283
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 409284
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 409285
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 409286
    new-instance v1, LX/434;

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {v1, v2, v0}, LX/434;-><init>(II)V

    return-object v1
.end method

.method private a(Landroid/content/Context;LX/432;IIIZ)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const v7, 0x3f9851ec    # 1.19f

    .line 409261
    :try_start_0
    invoke-virtual {p2, p1}, LX/432;->a(Landroid/content/Context;)Landroid/graphics/BitmapFactory$Options;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 409262
    const/4 v6, 0x0

    .line 409263
    iget v0, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v1, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-static/range {v0 .. v5}, LX/2Qx;->a(IIIIIZ)F

    move-result v2

    .line 409264
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    move v3, v2

    move v2, v1

    move v1, v0

    move-object v0, v6

    .line 409265
    :goto_0
    const/high16 v4, 0x40000000    # 2.0f

    cmpg-float v4, v2, v4

    if-gez v4, :cond_2

    .line 409266
    float-to-int v4, v3

    if-ne v4, v1, :cond_1

    .line 409267
    :cond_0
    float-to-int v1, v3

    .line 409268
    mul-float/2addr v3, v7

    .line 409269
    mul-float/2addr v2, v7

    goto :goto_0

    .line 409270
    :catch_0
    move-exception v0

    .line 409271
    new-instance v1, LX/430;

    invoke-direct {v1, v0}, LX/430;-><init>(Ljava/lang/OutOfMemoryError;)V

    throw v1

    .line 409272
    :catch_1
    move-exception v0

    .line 409273
    new-instance v1, LX/42z;

    invoke-direct {v1, v0}, LX/42z;-><init>(Ljava/lang/reflect/UndeclaredThrowableException;)V

    throw v1

    .line 409274
    :cond_1
    :try_start_1
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 409275
    float-to-int v1, v3

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 409276
    invoke-virtual {p2, p1, v0}, LX/432;->a(Landroid/content/Context;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 409277
    invoke-static {v0, p3, p6}, LX/2Qx;->a(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    return-object v0

    .line 409278
    :catch_2
    move-exception v0

    .line 409279
    iget-boolean v1, p0, LX/2Qx;->a:Z

    if-nez v1, :cond_0

    .line 409280
    new-instance v1, LX/430;

    invoke-direct {v1, v0}, LX/430;-><init>(Ljava/lang/OutOfMemoryError;)V

    throw v1

    .line 409281
    :cond_2
    const-string v1, "BitmapUtils"

    const-string v2, "unable to resize image, even after additional subsampling"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 409282
    new-instance v1, LX/430;

    invoke-direct {v1, v0}, LX/430;-><init>(Ljava/lang/OutOfMemoryError;)V

    throw v1
.end method

.method public static a(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    .line 409252
    if-eqz p2, :cond_0

    if-gtz p1, :cond_1

    .line 409253
    :cond_0
    :goto_0
    return-object p0

    .line 409254
    :cond_1
    :try_start_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 409255
    int-to-float v0, p1

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 409256
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 409257
    if-eqz p0, :cond_2

    if-eq p0, v0, :cond_2

    .line 409258
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_2
    move-object p0, v0

    goto :goto_0

    .line 409259
    :catchall_0
    move-exception v0

    if-eqz p0, :cond_3

    if-eqz p0, :cond_3

    .line 409260
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    throw v0
.end method

.method public static a(Landroid/net/Uri;Landroid/graphics/Rect;IIIZ)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 409245
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v7}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/lang/String;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v6

    .line 409246
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, LX/2Qx;->a(IIIIIZ)F

    move-result v0

    .line 409247
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 409248
    float-to-int v0, v0

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 409249
    iput-boolean v7, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 409250
    invoke-virtual {v6, p1, v1}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 409251
    invoke-static {v0, p2, p5}, LX/2Qx;->a(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 409231
    invoke-static {p0}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v1

    .line 409232
    iget v0, v1, LX/434;->b:I

    if-gt v0, p1, :cond_0

    iget v0, v1, LX/434;->a:I

    if-gt v0, p2, :cond_0

    .line 409233
    invoke-static {p0, v4}, LX/2Qx;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 409234
    :goto_0
    return-object v0

    .line 409235
    :cond_0
    iget v0, v1, LX/434;->b:I

    int-to-float v0, v0

    int-to-float v2, p1

    div-float/2addr v0, v2

    .line 409236
    iget v2, v1, LX/434;->a:I

    int-to-float v2, v2

    int-to-float v3, p2

    div-float/2addr v2, v3

    .line 409237
    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    .line 409238
    int-to-float v0, p1

    .line 409239
    :goto_1
    iget v2, v1, LX/434;->b:I

    int-to-float v2, v2

    cmpl-float v2, v2, v0

    if-lez v2, :cond_2

    .line 409240
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 409241
    iget v1, v1, LX/434;->b:I

    float-to-int v0, v0

    div-int v0, v1, v0

    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 409242
    invoke-static {p0, v2}, LX/2Qx;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 409243
    :cond_1
    iget v0, v1, LX/434;->b:I

    int-to-float v0, v0

    div-float/2addr v0, v2

    goto :goto_1

    .line 409244
    :cond_2
    invoke-static {p0, v4}, LX/2Qx;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1    # Landroid/graphics/BitmapFactory$Options;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 409150
    if-eqz p1, :cond_0

    :try_start_0
    iget-boolean v0, p1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 409151
    :goto_0
    if-eqz v0, :cond_2

    invoke-static {p0, p1}, LX/436;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 409152
    :goto_1
    if-nez v0, :cond_3

    .line 409153
    new-instance v0, LX/42x;

    invoke-direct {v0}, LX/42x;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 409154
    :catch_0
    move-exception v0

    .line 409155
    new-instance v1, LX/430;

    invoke-direct {v1, v0}, LX/430;-><init>(Ljava/lang/OutOfMemoryError;)V

    throw v1

    .line 409156
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 409157
    :cond_2
    :try_start_1
    invoke-static {p0, p1}, LX/436;->b(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 409158
    :cond_3
    return-object v0
.end method

.method public static a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;ILjava/io/File;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 409218
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 409219
    :try_start_1
    invoke-virtual {p0, p1, p2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 409220
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cannot compress bitmap to file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 409221
    :catch_0
    move-exception v0

    .line 409222
    :goto_0
    :try_start_2
    new-instance v2, LX/42y;

    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, LX/42y;-><init>(Ljava/lang/String;Ljava/io/IOException;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 409223
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 409224
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 409225
    :cond_0
    :goto_2
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 409226
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    :cond_1
    throw v0

    .line 409227
    :cond_2
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 409228
    return-void

    :catch_1
    goto :goto_2

    .line 409229
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    .line 409230
    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 409216
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/2Qx;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 409217
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 409203
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 409204
    new-instance v2, Landroid/media/ExifInterface;

    invoke-direct {v2, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 409205
    sget-object v3, LX/2Qx;->b:[Ljava/lang/String;

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 409206
    invoke-virtual {v1, v5}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 409207
    if-eqz v6, :cond_0

    .line 409208
    invoke-virtual {v2, v5, v6}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 409209
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 409210
    :cond_1
    if-eqz p2, :cond_2

    .line 409211
    const-string v0, "Orientation"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 409212
    :cond_2
    invoke-virtual {v2}, Landroid/media/ExifInterface;->saveAttributes()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 409213
    return-void

    .line 409214
    :catch_0
    move-exception v0

    .line 409215
    new-instance v1, LX/42y;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "copyExif from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/42y;-><init>(Ljava/lang/String;Ljava/io/IOException;)V

    throw v1
.end method

.method public static b(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 409195
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 409196
    const-string v2, "Orientation"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 409197
    :goto_0
    :pswitch_0
    return v0

    .line 409198
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 409199
    :pswitch_2
    const/16 v0, 0x5a

    goto :goto_0

    .line 409200
    :pswitch_3
    const/16 v0, 0xb4

    goto :goto_0

    .line 409201
    :pswitch_4
    const/16 v0, 0x10e

    goto :goto_0

    .line 409202
    :catch_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/net/Uri;IIZ)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    .line 409190
    new-instance v2, LX/432;

    invoke-direct {v2, p2}, LX/432;-><init>(Landroid/net/Uri;)V

    .line 409191
    :try_start_0
    invoke-static {p1, p2}, LX/2Qx;->a(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v5, p4

    move v6, p5

    .line 409192
    invoke-direct/range {v0 .. v6}, LX/2Qx;->a(Landroid/content/Context;LX/432;IIIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 409193
    :catch_0
    move-exception v0

    .line 409194
    new-instance v1, LX/42y;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/42y;-><init>(Ljava/lang/String;Ljava/io/IOException;)V

    throw v1
.end method

.method public final a(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/16 v3, 0x3c0

    .line 409189
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, v3

    move v5, p3

    invoke-virtual/range {v0 .. v5}, LX/2Qx;->a(Landroid/content/Context;Landroid/net/Uri;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/io/File;I)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 409174
    :try_start_0
    invoke-static {p2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, LX/2Qx;->a(Landroid/content/Context;Landroid/net/Uri;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 409175
    if-nez v0, :cond_2

    .line 409176
    if-eqz v0, :cond_0

    if-eqz v0, :cond_0

    .line 409177
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    move-object v0, v6

    :cond_1
    :goto_0
    return-object v0

    .line 409178
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-gt v1, p3, :cond_3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-le v1, p3, :cond_1

    .line 409179
    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-double v2, v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-double v4, v1

    div-double/2addr v2, v4

    .line 409180
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_5

    .line 409181
    int-to-double v4, p3

    div-double v2, v4, v2

    double-to-int v1, v2

    move v7, v1

    move v1, p3

    move p3, v7

    .line 409182
    :goto_1
    const/4 v2, 0x1

    invoke-static {v0, v1, p3, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 409183
    if-eqz v0, :cond_4

    if-eq v0, v1, :cond_4

    .line 409184
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_4
    move-object v0, v1

    goto :goto_0

    .line 409185
    :cond_5
    int-to-double v4, p3

    mul-double/2addr v2, v4

    double-to-int v1, v2

    goto :goto_1

    .line 409186
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v6, :cond_6

    if-eqz v6, :cond_6

    .line 409187
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    :cond_6
    throw v0

    .line 409188
    :catchall_1
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    goto :goto_2
.end method

.method public final a(Landroid/content/Context;Ljava/io/File;Ljava/io/File;III)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 409159
    :try_start_0
    invoke-static {p2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p4

    move v4, p5

    invoke-virtual/range {v0 .. v5}, LX/2Qx;->a(Landroid/content/Context;Landroid/net/Uri;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 409160
    if-eqz v1, :cond_4

    .line 409161
    :try_start_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-static {v1, v0, p6, p3}, LX/2Qx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;ILjava/io/File;)V

    .line 409162
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/2Qx;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 409163
    const/4 v0, 0x1

    .line 409164
    :goto_0
    if-eqz v1, :cond_0

    .line 409165
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 409166
    :cond_0
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 409167
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 409168
    :cond_1
    return v0

    .line 409169
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_2

    .line 409170
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 409171
    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 409172
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    :cond_3
    throw v0

    .line 409173
    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_4
    move v0, v7

    move-object v6, p3

    goto :goto_0
.end method
