.class public final LX/3Bt;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 529209
    const-class v1, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel;

    const v0, 0x58a69c3e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "PeopleYouMayKnowQuery"

    const-string v6, "9271d960ce92138078a4bae4a95fc0da"

    const-string v7, "viewer"

    const-string v8, "10155192144306729"

    const-string v9, "10155259089691729"

    .line 529210
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 529211
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 529212
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 529213
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 529214
    sparse-switch v0, :sswitch_data_0

    .line 529215
    :goto_0
    return-object p1

    .line 529216
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 529217
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 529218
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 529219
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 529220
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 529221
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2ed7555e -> :sswitch_5
        -0x295975c2 -> :sswitch_1
        0x1a564 -> :sswitch_3
        0x21beac6a -> :sswitch_0
        0x714f9fb5 -> :sswitch_2
        0x73a026b5 -> :sswitch_4
    .end sparse-switch
.end method
