.class public LX/2NH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static volatile r:LX/2NH;


# instance fields
.field public final b:LX/2N7;

.field public final c:LX/2NB;

.field public final d:LX/2NI;

.field public final e:LX/2NJ;

.field public final f:LX/2Nm;

.field public final g:LX/2No;

.field public final h:LX/2Np;

.field public final i:LX/2Nq;

.field public final j:LX/2NC;

.field public final k:LX/2Nr;

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6ci;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6cj;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6cs;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0lB;

.field public final p:LX/2Ns;

.field public final q:LX/2Lz;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 399034
    const/16 v0, 0x41

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "thread_key"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "msg_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "action_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "text"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "is_not_forwardable"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "sender"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "timestamp_ms"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "timestamp_sent_ms"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "msg_type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "affected_users"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "attachments"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "shares"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "sticker_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "offline_threading_id"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "source"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "channel_source"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "is_non_authoritative"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "pending_send_media_attachment"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "sent_share_attachment"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "client_tags"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "send_error"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "send_error_message"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "send_error_number"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "send_error_timestamp_ms"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "send_error_error_url"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "send_channel"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "publicity"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "send_queue_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "payment_transaction"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "payment_request"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "has_unavailable_attachment"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "app_attribution"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "content_app_attribution"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "xma"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "admin_text_type"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "admin_text_theme_color"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "admin_text_thread_icon_emoji"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "admin_text_nickname"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "admin_text_target_id"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "admin_text_thread_message_lifetime"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "admin_text_thread_journey_color_choices"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "admin_text_thread_journey_emoji_choices"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "admin_text_thread_journey_nickname_choices"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "admin_text_thread_journey_bot_choices"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "message_lifetime"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "admin_text_thread_rtc_event"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "admin_text_thread_rtc_server_info_data"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "admin_text_thread_rtc_is_video_call"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "admin_text_thread_ride_provider_name"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "is_sponsored"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "admin_text_thread_ad_properties"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "admin_text_game_score_data"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "admin_text_thread_event_reminder_properties"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "commerce_message_type"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "customizations"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "admin_text_joinable_event_type"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "metadata_at_text_ranges"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "platform_metadata"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "admin_text_is_joinable_promo"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "admin_text_agent_intent_id"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "montage_reply_message_id"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "admin_text_booking_request_id"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "generic_admin_message_extensible_data"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "reactions"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "profile_ranges"

    aput-object v2, v0, v1

    sput-object v0, LX/2NH;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2N7;LX/2NB;LX/2NI;LX/2NJ;LX/2Nm;LX/2No;LX/2Np;LX/2Nq;LX/2NC;LX/2Nr;LX/0Ot;LX/0Ot;LX/0Ot;LX/0lB;LX/2Ns;LX/2Lz;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2N7;",
            "LX/2NB;",
            "LX/2NI;",
            "LX/2NJ;",
            "LX/2Nm;",
            "LX/2No;",
            "LX/2Np;",
            "LX/2Nq;",
            "LX/2NC;",
            "LX/2Nr;",
            "LX/0Ot",
            "<",
            "LX/6ci;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6cj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6cs;",
            ">;",
            "LX/0lB;",
            "LX/2Ns;",
            "LX/2Lz;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 399035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399036
    iput-object p1, p0, LX/2NH;->b:LX/2N7;

    .line 399037
    iput-object p2, p0, LX/2NH;->c:LX/2NB;

    .line 399038
    iput-object p3, p0, LX/2NH;->d:LX/2NI;

    .line 399039
    iput-object p4, p0, LX/2NH;->e:LX/2NJ;

    .line 399040
    iput-object p5, p0, LX/2NH;->f:LX/2Nm;

    .line 399041
    iput-object p6, p0, LX/2NH;->g:LX/2No;

    .line 399042
    iput-object p7, p0, LX/2NH;->h:LX/2Np;

    .line 399043
    iput-object p8, p0, LX/2NH;->i:LX/2Nq;

    .line 399044
    iput-object p9, p0, LX/2NH;->j:LX/2NC;

    .line 399045
    iput-object p10, p0, LX/2NH;->k:LX/2Nr;

    .line 399046
    iput-object p11, p0, LX/2NH;->l:LX/0Ot;

    .line 399047
    iput-object p12, p0, LX/2NH;->m:LX/0Ot;

    .line 399048
    iput-object p13, p0, LX/2NH;->n:LX/0Ot;

    .line 399049
    iput-object p14, p0, LX/2NH;->o:LX/0lB;

    .line 399050
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2NH;->p:LX/2Ns;

    .line 399051
    move-object/from16 v0, p16

    iput-object v0, p0, LX/2NH;->q:LX/2Lz;

    .line 399052
    return-void
.end method

.method public static a(LX/0QB;)LX/2NH;
    .locals 3

    .prologue
    .line 399053
    sget-object v0, LX/2NH;->r:LX/2NH;

    if-nez v0, :cond_1

    .line 399054
    const-class v1, LX/2NH;

    monitor-enter v1

    .line 399055
    :try_start_0
    sget-object v0, LX/2NH;->r:LX/2NH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 399056
    if-eqz v2, :cond_0

    .line 399057
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2NH;->b(LX/0QB;)LX/2NH;

    move-result-object v0

    sput-object v0, LX/2NH;->r:LX/2NH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 399058
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 399059
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 399060
    :cond_1
    sget-object v0, LX/2NH;->r:LX/2NH;

    return-object v0

    .line 399061
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 399062
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2NH;Ljava/lang/String;LX/266;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "LX/266",
            "<",
            "LX/0Px",
            "<TT;>;>;)",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 399063
    const-string v0, "[]"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399064
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 399065
    :goto_0
    return-object v0

    .line 399066
    :cond_0
    if-eqz p1, :cond_1

    .line 399067
    :try_start_0
    iget-object v0, p0, LX/2NH;->o:LX/0lB;

    invoke-virtual {v0, p1, p2}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 399068
    :catch_0
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/2NH;
    .locals 18

    .prologue
    .line 399069
    new-instance v1, LX/2NH;

    invoke-static/range {p0 .. p0}, LX/2N7;->a(LX/0QB;)LX/2N7;

    move-result-object v2

    check-cast v2, LX/2N7;

    invoke-static/range {p0 .. p0}, LX/2NB;->a(LX/0QB;)LX/2NB;

    move-result-object v3

    check-cast v3, LX/2NB;

    invoke-static/range {p0 .. p0}, LX/2NI;->a(LX/0QB;)LX/2NI;

    move-result-object v4

    check-cast v4, LX/2NI;

    invoke-static/range {p0 .. p0}, LX/2NJ;->a(LX/0QB;)LX/2NJ;

    move-result-object v5

    check-cast v5, LX/2NJ;

    invoke-static/range {p0 .. p0}, LX/2Nm;->a(LX/0QB;)LX/2Nm;

    move-result-object v6

    check-cast v6, LX/2Nm;

    invoke-static/range {p0 .. p0}, LX/2No;->a(LX/0QB;)LX/2No;

    move-result-object v7

    check-cast v7, LX/2No;

    invoke-static/range {p0 .. p0}, LX/2Np;->a(LX/0QB;)LX/2Np;

    move-result-object v8

    check-cast v8, LX/2Np;

    invoke-static/range {p0 .. p0}, LX/2Nq;->a(LX/0QB;)LX/2Nq;

    move-result-object v9

    check-cast v9, LX/2Nq;

    invoke-static/range {p0 .. p0}, LX/2NC;->a(LX/0QB;)LX/2NC;

    move-result-object v10

    check-cast v10, LX/2NC;

    invoke-static/range {p0 .. p0}, LX/2Nr;->a(LX/0QB;)LX/2Nr;

    move-result-object v11

    check-cast v11, LX/2Nr;

    const/16 v12, 0x273c

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x273d

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x2740

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v15

    check-cast v15, LX/0lB;

    invoke-static/range {p0 .. p0}, LX/2Ns;->a(LX/0QB;)LX/2Ns;

    move-result-object v16

    check-cast v16, LX/2Ns;

    invoke-static/range {p0 .. p0}, LX/2Lz;->a(LX/0QB;)LX/2Lz;

    move-result-object v17

    check-cast v17, LX/2Lz;

    invoke-direct/range {v1 .. v17}, LX/2NH;-><init>(LX/2N7;LX/2NB;LX/2NI;LX/2NJ;LX/2Nm;LX/2No;LX/2Np;LX/2Nq;LX/2NC;LX/2Nr;LX/0Ot;LX/0Ot;LX/0Ot;LX/0lB;LX/2Ns;LX/2Lz;)V

    .line 399070
    return-object v1
.end method

.method public static b$redex0(LX/2NH;Ljava/lang/String;LX/266;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "LX/266",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 399071
    if-eqz p1, :cond_0

    .line 399072
    :try_start_0
    iget-object v0, p0, LX/2NH;->o:LX/0lB;

    invoke-virtual {v0, p1, p2}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 399073
    :goto_0
    return-object v0

    :catch_0
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 399074
    if-eqz p0, :cond_0

    .line 399075
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/16N;->b(Lorg/json/JSONArray;)LX/0Px;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 399076
    :goto_0
    return-object v0

    :catch_0
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)LX/6f2;
    .locals 4

    .prologue
    .line 399077
    :try_start_0
    const-class v0, LX/6f2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6f2;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 399078
    :catch_0
    move-exception v0

    .line 399079
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid ChannelSource string: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
