.class public LX/2Ls;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/io/File;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2Ls;


# instance fields
.field private final a:LX/0dC;


# direct methods
.method public constructor <init>(LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395511
    iput-object p1, p0, LX/2Ls;->a:LX/0dC;

    .line 395512
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ls;
    .locals 4

    .prologue
    .line 395513
    sget-object v0, LX/2Ls;->b:LX/2Ls;

    if-nez v0, :cond_1

    .line 395514
    const-class v1, LX/2Ls;

    monitor-enter v1

    .line 395515
    :try_start_0
    sget-object v0, LX/2Ls;->b:LX/2Ls;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 395516
    if-eqz v2, :cond_0

    .line 395517
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 395518
    new-instance p0, LX/2Ls;

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v3

    check-cast v3, LX/0dC;

    invoke-direct {p0, v3}, LX/2Ls;-><init>(LX/0dC;)V

    .line 395519
    move-object v0, p0

    .line 395520
    sput-object v0, LX/2Ls;->b:LX/2Ls;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 395521
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 395522
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 395523
    :cond_1
    sget-object v0, LX/2Ls;->b:LX/2Ls;

    return-object v0

    .line 395524
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 395525
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 395485
    check-cast p1, Ljava/io/File;

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 395486
    new-instance v0, LX/4d5;

    const-string v1, "application/x-gzip"

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/GzQ;

    invoke-direct {v3, p0}, LX/GzQ;-><init>(LX/2Ls;)V

    invoke-direct {v0, p1, v1, v2, v3}, LX/4d5;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;LX/4cr;)V

    .line 395487
    const/4 v1, 0x2

    new-array v1, v1, [Lorg/apache/http/NameValuePair;

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "type"

    const-string v4, "loom"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v5

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "device_id"

    iget-object v4, p0, LX/2Ls;->a:LX/0dC;

    invoke-virtual {v4}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 395488
    new-instance v2, LX/4cQ;

    const-string v3, "file"

    invoke-direct {v2, v3, v0}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 395489
    new-instance v0, LX/14O;

    invoke-direct {v0}, LX/14O;-><init>()V

    const-string v3, "loomUpload"

    .line 395490
    iput-object v3, v0, LX/14O;->b:Ljava/lang/String;

    .line 395491
    move-object v0, v0

    .line 395492
    const-string v3, "me/traces"

    .line 395493
    iput-object v3, v0, LX/14O;->d:Ljava/lang/String;

    .line 395494
    move-object v0, v0

    .line 395495
    const-string v3, "POST"

    .line 395496
    iput-object v3, v0, LX/14O;->c:Ljava/lang/String;

    .line 395497
    move-object v0, v0

    .line 395498
    sget-object v3, LX/14S;->JSON:LX/14S;

    .line 395499
    iput-object v3, v0, LX/14O;->k:LX/14S;

    .line 395500
    move-object v0, v0

    .line 395501
    new-array v3, v6, [LX/4cQ;

    aput-object v2, v3, v5

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 395502
    iput-object v2, v0, LX/14O;->l:Ljava/util/List;

    .line 395503
    move-object v0, v0

    .line 395504
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 395505
    move-object v0, v0

    .line 395506
    sget-object v1, LX/14R;->MULTI_PART_ENTITY:LX/14R;

    .line 395507
    iput-object v1, v0, LX/14O;->w:LX/14R;

    .line 395508
    move-object v0, v0

    .line 395509
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 395480
    const/4 v0, 0x0

    .line 395481
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 395482
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    .line 395483
    iget v2, p2, LX/1pN;->b:I

    move v2, v2

    .line 395484
    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    const-string v2, "success"

    invoke-virtual {v1, v2}, LX/0lF;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "success"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0lF;->a(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
