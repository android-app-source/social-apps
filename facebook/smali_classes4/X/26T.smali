.class public LX/26T;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/26U;

.field private static final b:LX/26U;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 372236
    new-instance v0, LX/26U;

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, Lcom/google/inject/ScopeAnnotation;

    aput-object v2, v1, v3

    const-class v2, Ljavax/inject/Scope;

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, LX/26U;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/26T;->a:LX/26U;

    .line 372237
    new-instance v0, LX/26U;

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, Lcom/google/inject/BindingAnnotation;

    aput-object v2, v1, v3

    const-class v2, Ljavax/inject/Qualifier;

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, LX/26U;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/26T;->b:LX/26U;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 372234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372235
    return-void
.end method
