.class public LX/3Kl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/3Kl;


# instance fields
.field private final a:LX/3Km;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Lb;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z

.field private final f:LX/0Uh;

.field private final g:LX/3Kn;


# direct methods
.method public constructor <init>(LX/3Km;LX/0Or;LX/0Or;LX/0Or;Ljava/lang/Boolean;LX/0Uh;LX/3Kn;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAMessengerOnlyUser;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsPartialAccount;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Km;",
            "LX/0Or",
            "<",
            "LX/3Lb;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/3Kn;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 549171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549172
    iput-object p1, p0, LX/3Kl;->a:LX/3Km;

    .line 549173
    iput-object p2, p0, LX/3Kl;->b:LX/0Or;

    .line 549174
    iput-object p3, p0, LX/3Kl;->c:LX/0Or;

    .line 549175
    iput-object p4, p0, LX/3Kl;->d:LX/0Or;

    .line 549176
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/3Kl;->e:Z

    .line 549177
    iput-object p6, p0, LX/3Kl;->f:LX/0Uh;

    .line 549178
    iput-object p7, p0, LX/3Kl;->g:LX/3Kn;

    .line 549179
    return-void
.end method

.method public static a(LX/0QB;)LX/3Kl;
    .locals 11

    .prologue
    .line 549180
    sget-object v0, LX/3Kl;->h:LX/3Kl;

    if-nez v0, :cond_1

    .line 549181
    const-class v1, LX/3Kl;

    monitor-enter v1

    .line 549182
    :try_start_0
    sget-object v0, LX/3Kl;->h:LX/3Kl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 549183
    if-eqz v2, :cond_0

    .line 549184
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 549185
    new-instance v3, LX/3Kl;

    invoke-static {v0}, LX/3Km;->b(LX/0QB;)LX/3Km;

    move-result-object v4

    check-cast v4, LX/3Km;

    const/16 v5, 0xd08

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x1450

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x1453

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {v0}, LX/3Kn;->a(LX/0QB;)LX/3Kn;

    move-result-object v10

    check-cast v10, LX/3Kn;

    invoke-direct/range {v3 .. v10}, LX/3Kl;-><init>(LX/3Km;LX/0Or;LX/0Or;LX/0Or;Ljava/lang/Boolean;LX/0Uh;LX/3Kn;)V

    .line 549186
    move-object v0, v3

    .line 549187
    sput-object v0, LX/3Kl;->h:LX/3Kl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 549188
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 549189
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 549190
    :cond_1
    sget-object v0, LX/3Kl;->h:LX/3Kl;

    return-object v0

    .line 549191
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 549192
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/3Lb;
    .locals 2

    .prologue
    .line 549193
    sget-object v0, LX/3LZ;->ONLINE_FRIENDS:LX/3LZ;

    sget-object v1, LX/3LZ;->TOP_FRIENDS:LX/3LZ;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    .line 549194
    new-instance v1, LX/3La;

    invoke-direct {v1, v0}, LX/3La;-><init>(Ljava/util/EnumSet;)V

    .line 549195
    iget-object v0, p0, LX/3Kl;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Lb;

    .line 549196
    iput-object v1, v0, LX/3Lb;->z:LX/3La;

    .line 549197
    return-object v0
.end method
