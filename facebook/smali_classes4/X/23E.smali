.class public final enum LX/23E;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/23E;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/23E;

.field public static final enum AFTER:LX/23E;

.field public static final enum BEFORE:LX/23E;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 363523
    new-instance v0, LX/23E;

    const-string v1, "AFTER"

    invoke-direct {v0, v1, v2}, LX/23E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/23E;->AFTER:LX/23E;

    .line 363524
    new-instance v0, LX/23E;

    const-string v1, "BEFORE"

    invoke-direct {v0, v1, v3}, LX/23E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/23E;->BEFORE:LX/23E;

    .line 363525
    const/4 v0, 0x2

    new-array v0, v0, [LX/23E;

    sget-object v1, LX/23E;->AFTER:LX/23E;

    aput-object v1, v0, v2

    sget-object v1, LX/23E;->BEFORE:LX/23E;

    aput-object v1, v0, v3

    sput-object v0, LX/23E;->$VALUES:[LX/23E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 363526
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/23E;
    .locals 1

    .prologue
    .line 363527
    const-class v0, LX/23E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/23E;

    return-object v0
.end method

.method public static values()[LX/23E;
    .locals 1

    .prologue
    .line 363528
    sget-object v0, LX/23E;->$VALUES:[LX/23E;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/23E;

    return-object v0
.end method
