.class public abstract LX/2Md;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/graphics/RectF;


# instance fields
.field public b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 397040
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, LX/2Md;->a:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 397038
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397039
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2Md;->b:Z

    return-void
.end method

.method private static a(LX/2Md;IIIIILandroid/graphics/RectF;LX/7Sv;LX/7Sy;Ljava/util/List;)LX/7Sx;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII",
            "Landroid/graphics/RectF;",
            "LX/7Sv;",
            "LX/7Sy;",
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;)",
            "LX/7Sx;"
        }
    .end annotation

    .prologue
    .line 397003
    invoke-virtual/range {p0 .. p0}, LX/2Md;->a()LX/2Mg;

    move-result-object v8

    .line 397004
    if-lez p5, :cond_0

    iget v1, v8, LX/2Mg;->b:I

    move/from16 v0, p5

    if-ge v1, v0, :cond_d

    .line 397005
    :cond_0
    iget v11, v8, LX/2Mg;->b:I

    .line 397006
    :goto_0
    iget v1, v8, LX/2Mg;->e:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    const/16 v16, 0x1

    .line 397007
    :goto_1
    if-lez p1, :cond_1

    if-gtz p2, :cond_3

    .line 397008
    :cond_1
    new-instance v1, LX/7Sx;

    const/16 v2, 0x280

    const/16 v3, 0x1e0

    const/16 v5, 0x280

    const/16 v6, 0x1e0

    const/4 v7, 0x0

    sget-object v9, LX/2Md;->a:Landroid/graphics/RectF;

    iget v4, v8, LX/2Mg;->c:F

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v12

    iget v13, v8, LX/2Mg;->d:I

    iget v0, v8, LX/2Mg;->e:I

    move/from16 v17, v0

    iget v0, v8, LX/2Mg;->f:I

    move/from16 v18, v0

    iget-boolean v0, v8, LX/2Mg;->g:Z

    move/from16 v19, v0

    move/from16 v4, p3

    move/from16 v8, p4

    move-object/from16 v10, p7

    move-object/from16 v14, p8

    move-object/from16 v15, p9

    invoke-direct/range {v1 .. v19}, LX/7Sx;-><init>(IIIIIIILandroid/graphics/RectF;LX/7Sv;IIILX/7Sy;Ljava/util/List;ZIIZ)V

    .line 397009
    :goto_2
    return-object v1

    .line 397010
    :cond_2
    const/16 v16, 0x0

    goto :goto_1

    .line 397011
    :cond_3
    const/16 v1, 0x5a

    move/from16 v0, p3

    if-eq v0, v1, :cond_4

    const/16 v1, 0x10e

    move/from16 v0, p3

    if-ne v0, v1, :cond_8

    :cond_4
    move/from16 v7, p2

    .line 397012
    :goto_3
    const/16 v1, 0x5a

    move/from16 v0, p3

    if-eq v0, v1, :cond_5

    const/16 v1, 0x10e

    move/from16 v0, p3

    if-ne v0, v1, :cond_9

    :cond_5
    move/from16 v1, p1

    .line 397013
    :goto_4
    int-to-float v2, v7

    invoke-virtual/range {p6 .. p6}, Landroid/graphics/RectF;->width()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v5, v2

    .line 397014
    int-to-float v2, v1

    invoke-virtual/range {p6 .. p6}, Landroid/graphics/RectF;->height()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v3, v2

    .line 397015
    if-le v5, v3, :cond_a

    iget v2, v8, LX/2Mg;->a:I

    if-le v5, v2, :cond_a

    .line 397016
    iget v4, v8, LX/2Mg;->a:I

    .line 397017
    mul-int v2, v3, v4

    div-int/2addr v2, v5

    .line 397018
    :goto_5
    rem-int/lit8 v6, v4, 0x10

    .line 397019
    if-eqz v6, :cond_6

    .line 397020
    rsub-int/lit8 v6, v6, 0x10

    add-int/2addr v6, v4

    .line 397021
    int-to-float v2, v2

    int-to-float v9, v6

    int-to-float v4, v4

    div-float v4, v9, v4

    mul-float/2addr v2, v4

    float-to-int v2, v2

    move v4, v6

    .line 397022
    :cond_6
    rem-int/lit8 v6, v2, 0x10

    .line 397023
    if-eqz v6, :cond_7

    .line 397024
    rsub-int/lit8 v6, v6, 0x10

    add-int/2addr v2, v6

    .line 397025
    :cond_7
    int-to-float v6, v4

    int-to-float v5, v5

    div-float v5, v6, v5

    .line 397026
    int-to-float v6, v2

    int-to-float v3, v3

    div-float v3, v6, v3

    .line 397027
    new-instance v9, Landroid/graphics/RectF;

    move-object/from16 v0, p6

    iget v6, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p6

    iget v10, v0, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p6

    iget v12, v0, Landroid/graphics/RectF;->left:F

    int-to-float v13, v4

    int-to-float v7, v7

    mul-float/2addr v5, v7

    div-float v5, v13, v5

    add-float/2addr v5, v12

    move-object/from16 v0, p6

    iget v7, v0, Landroid/graphics/RectF;->top:F

    int-to-float v12, v2

    int-to-float v1, v1

    mul-float/2addr v1, v3

    div-float v1, v12, v1

    add-float/2addr v1, v7

    invoke-direct {v9, v6, v10, v5, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 397028
    const/4 v7, 0x0

    .line 397029
    if-le v2, v4, :cond_c

    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/2Md;->b:Z

    if-eqz v1, :cond_c

    .line 397030
    const/16 v7, 0x5a

    move v6, v4

    move v5, v2

    .line 397031
    :goto_6
    new-instance v1, LX/7Sx;

    iget v2, v8, LX/2Mg;->c:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v12

    iget v13, v8, LX/2Mg;->d:I

    iget v0, v8, LX/2Mg;->e:I

    move/from16 v17, v0

    iget v0, v8, LX/2Mg;->f:I

    move/from16 v18, v0

    iget-boolean v0, v8, LX/2Mg;->g:Z

    move/from16 v19, v0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v8, p4

    move-object/from16 v10, p7

    move-object/from16 v14, p8

    move-object/from16 v15, p9

    invoke-direct/range {v1 .. v19}, LX/7Sx;-><init>(IIIIIIILandroid/graphics/RectF;LX/7Sv;IIILX/7Sy;Ljava/util/List;ZIIZ)V

    goto/16 :goto_2

    :cond_8
    move/from16 v7, p1

    .line 397032
    goto/16 :goto_3

    :cond_9
    move/from16 v1, p2

    .line 397033
    goto/16 :goto_4

    .line 397034
    :cond_a
    if-le v3, v5, :cond_b

    iget v2, v8, LX/2Mg;->a:I

    if-le v3, v2, :cond_b

    .line 397035
    iget v2, v8, LX/2Mg;->a:I

    .line 397036
    mul-int v4, v5, v2

    div-int/2addr v4, v3

    goto/16 :goto_5

    :cond_b
    move v2, v3

    move v4, v5

    .line 397037
    goto/16 :goto_5

    :cond_c
    move v6, v2

    move v5, v4

    goto :goto_6

    :cond_d
    move/from16 v11, p5

    goto/16 :goto_0
.end method


# virtual methods
.method public abstract a()LX/2Mg;
.end method

.method public final a(IIILandroid/graphics/RectF;LX/7Sv;)LX/7Sx;
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 396997
    const/4 v4, 0x0

    const/4 v5, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v6, p4

    move-object v7, p5

    move-object v9, v8

    invoke-static/range {v0 .. v9}, LX/2Md;->a(LX/2Md;IIIIILandroid/graphics/RectF;LX/7Sv;LX/7Sy;Ljava/util/List;)LX/7Sx;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/60x;ILandroid/graphics/RectF;LX/7Sv;LX/7Sy;Ljava/util/List;)LX/7Sx;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/60x;",
            "I",
            "Landroid/graphics/RectF;",
            "LX/7Sv;",
            "LX/7Sy;",
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;)",
            "LX/7Sx;"
        }
    .end annotation

    .prologue
    .line 397000
    if-eqz p2, :cond_0

    const/16 v0, 0x5a

    if-eq p2, v0, :cond_0

    const/16 v0, 0xb4

    if-eq p2, v0, :cond_0

    const/16 v0, 0x10e

    if-ne p2, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "OutputRotationDegreesClockwise Must be one of 0, 90, 180, 270"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 397001
    iget v1, p1, LX/60x;->b:I

    iget v2, p1, LX/60x;->c:I

    iget v3, p1, LX/60x;->d:I

    iget v5, p1, LX/60x;->e:I

    move-object v0, p0

    move v4, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    move-object/from16 v9, p6

    invoke-static/range {v0 .. v9}, LX/2Md;->a(LX/2Md;IIIIILandroid/graphics/RectF;LX/7Sv;LX/7Sy;Ljava/util/List;)LX/7Sx;

    move-result-object v0

    return-object v0

    .line 397002
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 396998
    iput-boolean p1, p0, LX/2Md;->b:Z

    .line 396999
    return-void
.end method
